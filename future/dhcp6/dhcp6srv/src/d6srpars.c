/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * $Id: d6srpars.c,v 1.16 2013/12/16 15:26:32 siva Exp $
 * Description: This file contains the routines for PDU reception, parsing
 *              formation and transmission.
 *
 **************************************************************************/

#include   "d6srinc.h"

PRIVATE VOID D6SrParseSendReply PROTO ((tDhcp6ParseInfo *));
PRIVATE INT4 D6SrParseValidateIfIndex PROTO ((tDhcp6ParseInfo *, UINT4));
PRIVATE INT4 D6SrParseValidatePort PROTO ((UINT2, UINT2));
PRIVATE VOID D6SrParseProcessORO PROTO ((tDhcp6ParseInfo *, UINT1 *, UINT2));
PRIVATE VOID        D6SrParseProcessVendorOpts
PROTO ((tDhcp6ParseInfo *, UINT1 *, UINT2));
PRIVATE VOID        D6SrParseProcessVendorClass
PROTO ((tDhcp6ParseInfo *, UINT1 *, UINT2));
PRIVATE VOID        D6SrParseProcessUserClass
PROTO ((tDhcp6ParseInfo *, UINT1 *, UINT2));
PRIVATE VOID        D6SrParseProcessElaspedTime
PROTO ((tDhcp6ParseInfo *, UINT1 *, UINT2));
PRIVATE VOID        D6SrParseProcessUnknown
PROTO ((tDhcp6ParseInfo *, UINT1 *, UINT2, UINT2));
PRIVATE INT4 D6SrParseIsAuthenticated PROTO ((tDhcp6ParseInfo *));
PRIVATE tDhcp6SrvPoolInfo *D6SrvGetRelatedPool PROTO ((tDhcp6ParseInfo *));

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessPkt                                        */
/*                                                                           */
/* Description  : The function validates the header and processes message    */
/*                based on the message type                                  */
/*                                                                           */
/* Input        : pRcvdBuf - DHCP6 packet received information received      */
/*                pUdp6Dhcp6sParamspRcvdBuf - ptr passed from UDP6 module    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6SrParseProcessPkt (tDhcp6ParseInfo * pParseInfo, UINT4 u4IfIndex)
{
    tIp6Addr            LinkAddr;
    UINT1              *pu1TempBuf = NULL;
    UINT2               u2OptionLen = 0;
    UINT2               u2OptionType = 0;
    UINT1               au1InterfaceId[DHCP6_SRV_IF_MAX_LEN];
    UINT1               au1LinkAddr[DHCP6_SRV_IPV6_ADDRESS_SIZE];
    UINT1               au1PeerAddr[DHCP6_SRV_IPV6_ADDRESS_SIZE];
    UINT1               u1MsgType = 0;
    UINT1               u1Hops = 0;
    UINT1               u1IfOptionPresent = DHCP6_SRV_NOT_FOUND;

    MEMSET (au1LinkAddr, 0, DHCP6_SRV_IPV6_ADDRESS_SIZE);
    MEMSET (au1PeerAddr, 0, DHCP6_SRV_IPV6_ADDRESS_SIZE);
    MEMSET (au1InterfaceId, 0, DHCP6_SRV_IF_MAX_LEN);

    /* Validation the received interface */
    if (D6SrParseValidateIfIndex (pParseInfo, u4IfIndex) == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Invalid Interface Received\r\n");
        return;
    }

    /* Validation the port */
    D6SrParseValidatePort (pParseInfo->udp6Hdr.u2SrcPort,
                           pParseInfo->udp6Hdr.u2DstPort);

    pParseInfo->pu1WriteBuffer = DHCP6_SRV_WRITE_PDU;
    pu1TempBuf = pParseInfo->pu1ReadBuffer;

    DHCP6_SRV_GET_1BYTE (u1MsgType, pu1TempBuf);

    pParseInfo->dhcp6.u1PduType = u1MsgType;
    if (u1MsgType == DHCP6_SRV_RELAY_MSG)
    {
        pParseInfo->pIfInfo->u4RelayForwIn++;
    }
    else if (u1MsgType == DHCP6_SRV_INFO_MSG)
    {
        pParseInfo->pIfInfo->u4InformIn++;
    }
    else
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Invalid MessageType\r\n");
        pParseInfo->pIfInfo->u4InvalidPktIn++;
        D6SrTrapSnmpSendTrap (&pParseInfo->pIfInfo->u4IfIndex,
                              DHCP6_SRV_INVALID_MSG_TRAP);
        return;
    }
    pParseInfo->pPool = D6SrvGetRelatedPool (pParseInfo);
    if (pParseInfo->pPool == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                       "Pool related to this request not found \r\n");
        return;
    }
    while (u1MsgType == DHCP6_SRV_RELAY_MSG)
    {
        u1MsgType = (UINT1) DHCP6_SRV_RELAY_REPLY;
        DHCP6_SRV_PUT_1BYTE (pParseInfo->pu1WriteBuffer, u1MsgType);

        DHCP6_SRV_GET_1BYTE (u1Hops, pu1TempBuf);
        DHCP6_SRV_PUT_1BYTE (pParseInfo->pu1WriteBuffer, u1Hops);

        DHCP6_SRV_GET_NBYTE (au1LinkAddr, pu1TempBuf,
                             DHCP6_SRV_IPV6_ADDRESS_SIZE);

        MEMCPY (&LinkAddr, (tIp6Addr *) (VOID *) au1LinkAddr,
                sizeof (tIp6Addr));

        /* The interface ID is set for all interfaces */
        u1IfOptionPresent = DHCP6_SRV_FOUND;

        DHCP6_SRV_PUT_NBYTE (pParseInfo->pu1WriteBuffer, au1LinkAddr,
                             DHCP6_SRV_IPV6_ADDRESS_SIZE);
        DHCP6_SRV_GET_NBYTE (au1PeerAddr, pu1TempBuf,
                             DHCP6_SRV_IPV6_ADDRESS_SIZE);
        DHCP6_SRV_PUT_NBYTE (pParseInfo->pu1WriteBuffer, au1PeerAddr,
                             DHCP6_SRV_IPV6_ADDRESS_SIZE);
        if (u1IfOptionPresent == DHCP6_SRV_FOUND)
        {
            DHCP6_SRV_GET_2BYTE (u2OptionType, pu1TempBuf);

            if (u2OptionType != DHCP6_SRV_IF_MSG_OPTION)
            {
                return;
            }

            DHCP6_SRV_PUT_2BYTE (pParseInfo->pu1WriteBuffer, u2OptionType);

            DHCP6_SRV_GET_2BYTE (u2OptionLen, pu1TempBuf);
            DHCP6_SRV_PUT_2BYTE (pParseInfo->pu1WriteBuffer, u2OptionLen);

            DHCP6_SRV_GET_NBYTE (au1InterfaceId, pu1TempBuf,
                                 (UINT4) u2OptionLen);
            DHCP6_SRV_PUT_NBYTE (pParseInfo->pu1WriteBuffer, au1InterfaceId,
                                 (UINT4) u2OptionLen);
        }

        DHCP6_SRV_GET_2BYTE (u2OptionType, pu1TempBuf);

        if (u2OptionType != DHCP6_SRV_RELAY_MSG_OPTION)
        {
            return;
        }

        DHCP6_SRV_PUT_2BYTE (pParseInfo->pu1WriteBuffer, u2OptionType);

        DHCP6_SRV_GET_2BYTE (u2OptionLen, pu1TempBuf);
        DHCP6_SRV_PUT_2BYTE (pParseInfo->pu1WriteBuffer, u2OptionLen);

        pParseInfo->u2ReadLen = (UINT2) (CLI_PTR_TO_U4 (pu1TempBuf) -
                                         CLI_PTR_TO_U4 (pParseInfo->
                                                        pu1ReadBuffer));
        pParseInfo->u2WrittenLen =
            (UINT2) ((FS_ULONG) (pParseInfo->pu1WriteBuffer) -
                     (FS_ULONG) (DHCP6_SRV_WRITE_PDU));
        DHCP6_SRV_GET_1BYTE (u1MsgType, pu1TempBuf);
        pParseInfo->u2RelayHdrSize = pParseInfo->u2ReadLen;
    }
    pParseInfo->u2RelayMsgLenOffset =
        (UINT2) (pParseInfo->u2WrittenLen - sizeof (u2OptionLen));
    pParseInfo->pu1WriteBuffer = DHCP6_SRV_WRITE_PDU;
    D6SrParseSendReply (pParseInfo);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseValidateIfIndex                                   */
/*                                                                           */
/* Description  : The function validate the received interface               */
/*                                                                           */
/* Input        : u4IfIndex - Interface Index                                */
/*                pParseInfo - parsed PDU info                               */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILUE                                   */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6SrParseValidateIfIndex (tDhcp6ParseInfo * pParseInfo, UINT4 u4IfIndex)
{
    tDhcp6SrvIfInfo    *pDhcp6SrvIfInfo = NULL;

    pDhcp6SrvIfInfo = D6SrIfGetNode (u4IfIndex);

    if (pDhcp6SrvIfInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Interface Does Not exists \r\n");
        return OSIX_FAILURE;
    }
    /* Check interface status */
    if (pDhcp6SrvIfInfo->u1RowStatus != ACTIVE)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Interface Status is Inactive \r\n");
        return OSIX_FAILURE;
    }

    /* Check interface oper status */
    if (pDhcp6SrvIfInfo->u1OperStatus == OSIX_FALSE)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Interface Status is Down \r\n");
        return OSIX_FAILURE;
    }
    pParseInfo->pIfInfo = pDhcp6SrvIfInfo;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseValidatePort                                      */
/*                                                                           */
/* Description  : The function validate the SourcePort and Destination Port  */
/*                                                                           */
/* Input        : pUdp6Dhcp6sParams - Pointer to UDP header                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILUE                                   */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6SrParseValidatePort (UINT2 u2SrcPort, UINT2 u2DestPort)
{
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u2DestPort);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseSendReply                                         */
/*                                                                           */
/* Description  : The function send a DHCP6 offer for a received dhcp6       */
/*                message.                                                   */
/*                                                                           */
/* Input        : pRcvBuf  - Received DHCP6 packet (Linear buffer).          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseSendReply (tDhcp6ParseInfo * pParseInfo)
{
    tDhcp6SrvOptionInfo *pOptionNode = NULL;
    UINT1              *pu1ReplyPdu = NULL;
    UINT1              *pu1RelayPdu = NULL;
    UINT1              *pu1AuthOptLen = NULL;
    UINT4               u4TransId = 0;
    UINT2               u2ServerType = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2OptionType = 0;
    UINT2               u2DuidLength = 0;
    UINT2               u2StatusCodeType = 0;
    UINT2               u2StatusCodeLength = 0;
    UINT2               u2RelayLen = 0;
    UINT2               u2SavedLengthValue = 0;
    UINT2               u2CodeValue = 0;
    UINT2               u2MsgOptionType = 0;
    UINT1              *pu1InfoReqPdu = NULL;
    UINT1               au1StatusCode[128];
    UINT1               u1MsgType = 0;

    MEMSET (au1StatusCode, 0, 128);
    pu1InfoReqPdu = pParseInfo->pu1ReadBuffer + pParseInfo->u2ReadLen;
    pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;

    /* Message Type is already read */
    pu1InfoReqPdu += 1;
    pParseInfo->u2ReadLen++;
    DHCP6_SRV_PUT_1BYTE (pu1ReplyPdu, DHCP6_SRV_INFO_REPLY);
    pParseInfo->u2WrittenLen++;

    /* Read Transaction ID */
    DHCP6_SRV_GET_NBYTE (&u4TransId, pu1InfoReqPdu, DHCP6_SRV_TRANS_ID_SIZE);

    pParseInfo->u2ReadLen += DHCP6_SRV_TRANS_ID_SIZE;
    DHCP6_SRV_PUT_NBYTE (pu1ReplyPdu, &u4TransId, DHCP6_SRV_TRANS_ID_SIZE);
    pParseInfo->u2WrittenLen += DHCP6_SRV_TRANS_ID_SIZE;

    /* Check for the authentication status of the PDU */
    if (D6SrParseIsAuthenticated (pParseInfo) != OSIX_SUCCESS)
    {
        pParseInfo->pIfInfo->u4HmacFailCount++;
        D6SrTrapSnmpSendTrap (&pParseInfo->pIfInfo->u4IfIndex,
                              DHCP6_SRV_HMAC_AUTH_FAIL_TRAP);
        return;
    }

    /* Update the wrie buffer */
    pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;

    /* Fill the mandatory TLVs */
    /* Server DUID */
    u2ServerType = DHCP6_SRV_OPTION_SERVER_ID;
    u2DuidLength = (UINT2) pParseInfo->pPool->u1DuidLength;

    DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2ServerType);
    DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2DuidLength);
    DHCP6_SRV_PUT_NBYTE (pu1ReplyPdu, pParseInfo->pPool->au1Duid,
                         pParseInfo->pPool->u1DuidLength);
    pParseInfo->u2WrittenLen += 4 + pParseInfo->pPool->u1DuidLength;

    if ((pParseInfo->dhcp6.u1PduType == DHCP6_SRV_INFO_MSG)
        && (!(IS_ADDR_MULTI (pParseInfo->ip6Hdr.dstAddr))))
    {
        pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;
        u2StatusCodeType = DHCP6_SRV_OPTION_STATUS_CODE;
        u2StatusCodeLength = 2 + DHCP6_SRV_USE_MUL_STATUS_LENGTH_VALUE;
        u2CodeValue = DHCP6_SRV_USE_MULT_STATUS_VALUE;

        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2StatusCodeType);
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2StatusCodeLength);
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2CodeValue);

        MEMCPY (au1StatusCode, DHCP6_SRV_USE_MUL_STATUS_CODE_VALUE,
                DHCP6_SRV_USE_MUL_STATUS_LENGTH_VALUE);
        DHCP6_SRV_PUT_NBYTE (pu1ReplyPdu, au1StatusCode,
                             DHCP6_SRV_USE_MUL_STATUS_LENGTH_VALUE);
        pParseInfo->u2WrittenLen += 6 + DHCP6_SRV_USE_MUL_STATUS_LENGTH_VALUE;
        pu1ReplyPdu = DHCP6_SRV_WRITE_PDU + pParseInfo->u2RelayMsgLenOffset;
        /* Increment Offset value by 2 Bytes to add option length */
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu,
                             (pParseInfo->u2WrittenLen -
                              (pParseInfo->u2RelayMsgLenOffset +
                               sizeof (UINT2))));
        D6SrSktSendToDst (pParseInfo);
        return;
    }

    /* Add preprerence option if configured by the user */
    if (pParseInfo->pPool->u2Preference != 0)
    {
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, DHCP6_SRV_OPTION_PREFERENCE);
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, DHCP6_SRV_OPTION_PREFERENCE_SIZE);
        DHCP6_SRV_PUT_1BYTE (pu1ReplyPdu,
                             (UINT1) pParseInfo->pPool->u2Preference);
        pParseInfo->u2WrittenLen +=
            DHCP6_SRV_TLV_TYPE_SIZE + DHCP6_SRV_TLV_LEN_SIZE +
            DHCP6_SRV_OPTION_PREFERENCE_SIZE;
    }

    u2SavedLengthValue = pParseInfo->u2WrittenLen;
    while (pParseInfo->u2ReadLen < pParseInfo->ip6Hdr.u4Len)
    {
        DHCP6_SRV_GET_2BYTE (u2OptionType, pu1InfoReqPdu);
        pParseInfo->u2ReadLen += DHCP6_SRV_TLV_TYPE_SIZE;
        DHCP6_SRV_GET_2BYTE (u2OptionLen, pu1InfoReqPdu);
        pParseInfo->u2ReadLen += DHCP6_SRV_TLV_LEN_SIZE;
        switch (u2OptionType)
        {
            case DHCP6_SRV_OPTION_ORO:
                D6SrParseProcessORO (pParseInfo, pu1InfoReqPdu, u2OptionLen);
                break;
            case DHCP6_SRV_OPTION_ELAPSED_TIME:
                D6SrParseProcessElaspedTime (pParseInfo,
                                             pu1InfoReqPdu, u2OptionLen);
                break;
            case DHCP6_SRV_OPTION_USER_CLASS:
                D6SrParseProcessUserClass (pParseInfo,
                                           pu1InfoReqPdu, u2OptionLen);
                break;
            case DHCP6_SRV_OPTION_VENDOR_CLASS:
                D6SrParseProcessVendorClass (pParseInfo,
                                             pu1InfoReqPdu, u2OptionLen);
                break;
            case DHCP6_SRV_OPTION_VENDOR_OPTS:
                D6SrParseProcessVendorOpts (pParseInfo,
                                            pu1InfoReqPdu, u2OptionLen);
                break;
            case DHCP6_SRV_OPTION_CLIENT_ID:
            case DHCP6_SRV_OPTION_AUTH:
                break;
            case DHCP6_SRV_OPTION_IA_TA:
            case DHCP6_SRV_OPTION_IA_NA:
            case DHCP6_SRV_OPTION_IA_ADDR:
                pParseInfo->pIfInfo->u4InvalidPktIn++;
                D6SrTrapSnmpSendTrap (&pParseInfo->pIfInfo->u4IfIndex,
                                      DHCP6_SRV_INVALID_MSG_TRAP);
                return;
                break;

            default:
                D6SrParseProcessUnknown (pParseInfo,
                                         pu1InfoReqPdu, u2OptionType,
                                         u2OptionLen);
                break;
        }
        pParseInfo->u2ReadLen += u2OptionLen;
        pu1InfoReqPdu = pu1InfoReqPdu + u2OptionLen;
    }
    if (u2SavedLengthValue == pParseInfo->u2WrittenLen)
    {
        pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;
        u2StatusCodeType = DHCP6_SRV_OPTION_STATUS_CODE;
        u2StatusCodeLength = 2 + DHCP6_SRV_UNSPECFAIL_STATUS_LENGTH_VALUE;
        u2CodeValue = DHCP6_SRV_UNSPECFAIL_STATUS_VALUE;

        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2StatusCodeType);
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2StatusCodeLength);
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2CodeValue);

        MEMCPY (au1StatusCode, DHCP6_SRV_UNSPECFAIL_STATUS_CODE_VALUE,
                DHCP6_SRV_UNSPECFAIL_STATUS_LENGTH_VALUE);
        DHCP6_SRV_PUT_NBYTE (pu1ReplyPdu, au1StatusCode,
                             DHCP6_SRV_UNSPECFAIL_STATUS_LENGTH_VALUE);
        pParseInfo->u2WrittenLen +=
            6 + DHCP6_SRV_UNSPECFAIL_STATUS_LENGTH_VALUE;
        pu1ReplyPdu = DHCP6_SRV_WRITE_PDU + pParseInfo->u2RelayMsgLenOffset;
        /* Increment Offset value by 2 Bytes to add option length */
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu,
                             (pParseInfo->u2WrittenLen -
                              (pParseInfo->u2RelayMsgLenOffset +
                               sizeof (UINT2))));
        D6SrSktSendToDst (pParseInfo);
        return;
    }
    /*Put the Refresh Timer TLV if configured by user */
    pOptionNode = D6SrPoolOptionListGetFirstNode (pParseInfo->pPool);
    while (pOptionNode != NULL)
    {
        if (pOptionNode->u2Type == DHCP6_SRV_REFRESH_TIMER_TYPE)
        {
            pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;
            DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, DHCP6_SRV_REFRESH_TIMER_TYPE);
            DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, DHCP6_SRV_REFRESH_TIMER_SIZE);
            MEMCPY (pu1ReplyPdu, pOptionNode->pu1Value,
                    DHCP6_SRV_REFRESH_TIMER_SIZE);
            pu1ReplyPdu += DHCP6_SRV_REFRESH_TIMER_SIZE;
            pParseInfo->u2WrittenLen +=
                (DHCP6_SRV_TLV_TYPE_SIZE + DHCP6_SRV_TLV_LEN_SIZE +
                 DHCP6_SRV_REFRESH_TIMER_SIZE);
            break;
        }
        pOptionNode = D6SrPoolOptionListGetNextNode (pOptionNode,
                                                     pParseInfo->pPool);
    }

    /*Put the  Status Code TLV */
    pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;
    u2StatusCodeType = DHCP6_SRV_OPTION_STATUS_CODE;
    u2StatusCodeLength = 2 + DHCP6_SRV_SUCCESS_STATUS_LENGTH_VALUE;
    u2CodeValue = DHCP6_SRV_SUCCESS_STATUS_VALUE;

    DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2StatusCodeType);
    DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2StatusCodeLength);
    DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, u2CodeValue);

    MEMCPY (au1StatusCode, DHCP6_SRV_SUCCESS_STATUS_CODE_VALUE,
            DHCP6_SRV_SUCCESS_STATUS_LENGTH_VALUE);
    DHCP6_SRV_PUT_NBYTE (pu1ReplyPdu, au1StatusCode,
                         DHCP6_SRV_SUCCESS_STATUS_LENGTH_VALUE);
    pParseInfo->u2WrittenLen += 6 + DHCP6_SRV_SUCCESS_STATUS_LENGTH_VALUE;

    /* Check if authentication TLV needs to be added */
    if ((pParseInfo->dhcp6.pRealm != NULL) && (pParseInfo->dhcp6.pKey != NULL))
    {
        pu1ReplyPdu = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;
        DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu, DHCP6_SRV_OPTION_AUTH);
        pu1AuthOptLen = pu1ReplyPdu;
        pu1ReplyPdu += DHCP6_SRV_TLV_LEN_SIZE;
        DHCP6_SRV_PUT_1BYTE (pu1ReplyPdu, DHCP6_SRV_AUTH_PROTOCOL);
        DHCP6_SRV_PUT_1BYTE (pu1ReplyPdu, DHCP6_SRV_AUTH_ALGO);
        DHCP6_SRV_PUT_1BYTE (pu1ReplyPdu, 0);
        DHCP6_SRV_PUT_4BYTE (pu1ReplyPdu, 0);
        DHCP6_SRV_PUT_4BYTE (pu1ReplyPdu, D6SrUtlGetTimeFromEpoch ());
        DHCP6_SRV_PUT_NBYTE (pu1ReplyPdu, pParseInfo->dhcp6.pRealm->au1Name,
                             pParseInfo->dhcp6.pRealm->u1NameLength);
        DHCP6_SRV_PUT_4BYTE (pu1ReplyPdu, pParseInfo->dhcp6.pKey->u4KeyId);
        MEMSET (pu1ReplyPdu, 0x00, DHCP6_SRV_AUTH_HMAC_SIZE);
        pu1ReplyPdu += DHCP6_SRV_AUTH_HMAC_SIZE;
        DHCP6_SRV_PUT_2BYTE (pu1AuthOptLen,
                             (pu1ReplyPdu -
                              (pParseInfo->pu1WriteBuffer +
                               pParseInfo->u2WrittenLen +
                               DHCP6_SRV_TLV_LEN_SIZE +
                               DHCP6_SRV_TLV_TYPE_SIZE)));
        pParseInfo->u2WrittenLen +=
            pu1ReplyPdu - (pParseInfo->pu1WriteBuffer +
                           pParseInfo->u2WrittenLen);

        arHmac_MD5 ((pParseInfo->pu1WriteBuffer + pParseInfo->u2RelayHdrSize),
                    (pParseInfo->u2WrittenLen - pParseInfo->u2RelayHdrSize),
                    pParseInfo->dhcp6.pKey->au1Key,
                    pParseInfo->dhcp6.pKey->u1KeyLength,
                    (pu1ReplyPdu - DHCP6_SRV_AUTH_HMAC_SIZE));
    }

    if (pParseInfo->u2RelayHdrSize > 0)
    {

        pu1RelayPdu = pParseInfo->pu1WriteBuffer;

        DHCP6_SRV_GET_1BYTE (u1MsgType, pu1RelayPdu);
        while (u1MsgType == DHCP6_SRV_RELAY_REPLY)
        {
            /*skip the Hop Count Interface ID Link Address and peer Address */

            pu1RelayPdu =
                pu1RelayPdu + 1 + DHCP6_SRV_IPV6_ADDRESS_SIZE +
                DHCP6_SRV_IPV6_ADDRESS_SIZE;

            DHCP6_SRV_GET_2BYTE (u2MsgOptionType, pu1RelayPdu);

            if (u2MsgOptionType == DHCP6_SRV_IF_MSG_OPTION)
            {
                pu1RelayPdu = pu1RelayPdu + 6;
                DHCP6_SRV_GET_2BYTE (u2MsgOptionType, pu1RelayPdu);
            }

            if (u2MsgOptionType == DHCP6_SRV_RELAY_MSG_OPTION)
            {
                /*update the Length */
                u2RelayLen =
                    (UINT2) ((pParseInfo->pu1WriteBuffer +
                              pParseInfo->u2WrittenLen) - pu1RelayPdu);
                u2RelayLen -= DHCP6_SRV_TLV_LEN_SIZE;
                DHCP6_SRV_PUT_2BYTE (pu1RelayPdu, u2RelayLen);
                DHCP6_SRV_GET_1BYTE (u1MsgType, pu1RelayPdu);
            }
            else
            {
                break;
            }
        }
    }
    pu1ReplyPdu = DHCP6_SRV_WRITE_PDU + pParseInfo->u2RelayMsgLenOffset;
    /* Increment Offset value by 2 Bytes to add option length */
    DHCP6_SRV_PUT_2BYTE (pu1ReplyPdu,
                         (pParseInfo->u2WrittenLen -
                          (pParseInfo->u2RelayMsgLenOffset + sizeof (UINT2))));
    D6SrSktSendToDst (pParseInfo);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrvGetRelatedPool                                        */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE tDhcp6SrvPoolInfo *
D6SrvGetRelatedPool (tDhcp6ParseInfo * pParseInfo)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    tDhcp6SrvPoolInfo  *pPool = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    pIfInfo = pParseInfo->pIfInfo;
    if (pIfInfo == NULL)
    {
        return NULL;
    }
    if (pIfInfo->pPool == NULL)
    {
        /* Get the context ID related to the u4IfIndex */
        if (D6SrPortVcmGetContextInfoFromIfIndex (pIfInfo->u4IfIndex,
                                                  &u4ContextId,
                                                  &u2LocalPort) != OSIX_SUCCESS)
        {
            u4ContextId = 0;
        }
        /* Get IP Prefix Entry */
        pInPrefixInfo = D6SrInPrefixLookupNode (u4ContextId,
                                                pParseInfo->ip6Hdr.srcAddr.
                                                ip6_addr_u.u1ByteAddr);
        if (pInPrefixInfo == NULL)
        {
            return NULL;
        }
        pPool = pInPrefixInfo->pPool;
    }
    else
    {
        pPool = pIfInfo->pPool;
    }

    return pPool;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseIsAuthenticated                                   */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6SrParseIsAuthenticated (tDhcp6ParseInfo * pParseInfo)
{
    UINT1              *pu1InfoReqPdu = NULL;
    UINT1              *pu1WriteBuffer = NULL;
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    UINT2               u2OptionType = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2ReadLen = 0;
    UINT2               u2ClientId = 0;
    BOOL1               b1ClientIdRcvd = OSIX_FALSE;
    BOOL1               b1AuthInfoRcvd = OSIX_FALSE;
    UINT1               u1RealmSize = 0;
    UINT1               au1HmacDigest[DHCP6_SRV_AUTH_HMAC_SIZE];

    pu1InfoReqPdu = pParseInfo->pu1ReadBuffer + pParseInfo->u2ReadLen;
    u2ReadLen = pParseInfo->u2ReadLen;

    while (u2ReadLen < pParseInfo->ip6Hdr.u4Len)
    {
        DHCP6_SRV_GET_2BYTE (u2OptionType, pu1InfoReqPdu);
        u2ReadLen += DHCP6_SRV_TLV_TYPE_SIZE;
        DHCP6_SRV_GET_2BYTE (u2OptionLen, pu1InfoReqPdu);
        u2ReadLen += DHCP6_SRV_TLV_LEN_SIZE;
        switch (u2OptionType)
        {
            case DHCP6_SRV_OPTION_AUTH:
                /* Read authentication information */
                DHCP6_SRV_GET_1BYTE (pParseInfo->dhcp6.u1AuthProtocol,
                                     pu1InfoReqPdu);
                DHCP6_SRV_GET_1BYTE (pParseInfo->dhcp6.u1AuthAlgo,
                                     pu1InfoReqPdu);
                DHCP6_SRV_GET_1BYTE (pParseInfo->dhcp6.u1AuthRDM,
                                     pu1InfoReqPdu);
                DHCP6_SRV_GET_NBYTE (pParseInfo->dhcp6.au1RDValue,
                                     pu1InfoReqPdu, DHCP6_SRV_AUTH_RD_SIZE);
                u1RealmSize = (UINT1) (u2OptionLen -
                                       (DHCP6_SRV_AUTH_KEYID_SIZE +
                                        DHCP6_SRV_AUTH_HMAC_SIZE +
                                        DHCP6_SRV_AUTH_RD_SIZE +
                                        DHCP6_SRV_AUTH_RDM_SIZE +
                                        DHCP6_SRV_AUTH_ALGO_SIZE +
                                        DHCP6_SRV_AUTH_PROTO_SIZE));

                /*Get the realm name */
                DHCP6_SRV_GET_NBYTE (pParseInfo->dhcp6.au1RcvdRealm,
                                     pu1InfoReqPdu, u1RealmSize);
                /*Get the KEY-ID */
                DHCP6_SRV_GET_4BYTE (pParseInfo->dhcp6.u4RcvdKeyId,
                                     pu1InfoReqPdu);

                /*Get the H-MAC Value */
                DHCP6_SRV_GET_NBYTE (pParseInfo->dhcp6.au1RcvdHmac,
                                     pu1InfoReqPdu, DHCP6_SRV_AUTH_HMAC_SIZE);
                /* Set the H-MAC to all zeros */
                MEMSET ((pu1InfoReqPdu - DHCP6_SRV_AUTH_HMAC_SIZE),
                        0x00, DHCP6_SRV_AUTH_HMAC_SIZE);
                b1AuthInfoRcvd = OSIX_TRUE;
                break;
            case DHCP6_SRV_OPTION_CLIENT_ID:
                /* Read Client ID */
                if (u2OptionLen <= DHCP6_SRV_DUID_SIZE_MAX)
                {
                    DHCP6_SRV_GET_NBYTE (pParseInfo->dhcp6.au1ClientId,
                                         pu1InfoReqPdu, u2OptionLen);
                    pParseInfo->dhcp6.u2ClientIdLen = u2OptionLen;
                    b1ClientIdRcvd = OSIX_TRUE;
                }
                break;
            default:
                pu1InfoReqPdu += u2OptionLen;
                break;
        }
        u2ReadLen += u2OptionLen;
    }
    if ((b1AuthInfoRcvd == OSIX_TRUE) && (b1ClientIdRcvd == OSIX_FALSE))
    {
        return OSIX_FAILURE;
    }
    /* If client-id is received then ew have to send client-id in reply */
    if (b1ClientIdRcvd == OSIX_TRUE)
    {
        pu1WriteBuffer = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;

        u2ClientId = DHCP6_SRV_OPTION_CLIENT_ID;
        DHCP6_SRV_PUT_2BYTE (pu1WriteBuffer, u2ClientId);
        DHCP6_SRV_PUT_2BYTE (pu1WriteBuffer, pParseInfo->dhcp6.u2ClientIdLen);
        DHCP6_SRV_PUT_NBYTE (pu1WriteBuffer, pParseInfo->dhcp6.au1ClientId,
                             pParseInfo->dhcp6.u2ClientIdLen);
        pParseInfo->u2WrittenLen += (DHCP6_SRV_TLV_TYPE_SIZE +
                                     DHCP6_SRV_TLV_LEN_SIZE +
                                     pParseInfo->dhcp6.u2ClientIdLen);
    }
    if (b1AuthInfoRcvd == OSIX_TRUE)
    {
        /* Check for the Authentication protocol to be 2 */
        if (pParseInfo->dhcp6.u1AuthProtocol != DHCP6_SRV_AUTH_PROTOCOL)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: invalid auth protocol \r\n");
            return OSIX_FAILURE;
        }
        /* Check for the Authentication algorithm to be 1 */
        if (pParseInfo->dhcp6.u1AuthAlgo != DHCP6_SRV_AUTH_ALGO)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: invalid auth algo \r\n");
            return OSIX_FAILURE;
        }
        /* Check for RDM field to be zero */
        if (pParseInfo->dhcp6.u1AuthRDM != 0)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: auth RDM not 0 \r\n");
            return OSIX_FAILURE;
        }
        if (MEMCMP
            (pParseInfo->dhcp6.au1RDValue, pParseInfo->pIfInfo->au1LastRDValue,
             DHCP6_SRV_AUTH_RD_SIZE) == 0)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: same RD value received \r\n");
            return OSIX_FAILURE;
        }
        MEMCPY (pParseInfo->pIfInfo->au1LastRDValue,
                pParseInfo->dhcp6.au1RDValue, DHCP6_SRV_AUTH_RD_SIZE);
        /* Check if the client-id is configured in server table */
        pClientInfo = D6SrClientLookupNode (pParseInfo->dhcp6.au1ClientId);
        if (pClientInfo == NULL)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: client info not found \r\n");
            return OSIX_FAILURE;
        }
        /* Get the realm name and check in the realm table */
        pRealmInfo = D6SrRealmLookupNode (pParseInfo->dhcp6.au1RcvdRealm);
        if (pRealmInfo == NULL)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: realm info not found \r\n");
            return OSIX_FAILURE;
        }
        if (pClientInfo->pRealm != pRealmInfo)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: client not mapped to realm \r\n");
            return OSIX_FAILURE;
        }
        /* Get the KEY-ID and fetch the KEY from the KEY table using realm index
         * and KEY-ID */
        pKeyInfo = D6SrKeyGetNode (pRealmInfo->u4RealmIndex,
                                   pParseInfo->dhcp6.u4RcvdKeyId);
        if (pKeyInfo == NULL)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: key info not found \r\n");
            return OSIX_FAILURE;
        }
        /* Calculate the H-MMAC compare the result with received HMAC */
        arHmac_MD5 ((pParseInfo->pu1ReadBuffer + pParseInfo->u2RelayHdrSize),
                    (pParseInfo->ip6Hdr.u4Len - pParseInfo->u2RelayHdrSize),
                    pKeyInfo->au1Key, pKeyInfo->u1KeyLength, au1HmacDigest);
        if (MEMCMP (au1HmacDigest, pParseInfo->dhcp6.au1RcvdHmac,
                    DHCP6_SRV_AUTH_HMAC_SIZE) != 0)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                           "D6SrParseIsAuthenticated: hmac comparison failure \r\n");
            return OSIX_FAILURE;
        }
        pParseInfo->dhcp6.pRealm = pRealmInfo;
        pParseInfo->dhcp6.pKey = pKeyInfo;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessUnknown                                    */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseProcessUnknown (tDhcp6ParseInfo * pParseInfo,
                         UINT1 *pu1Value, UINT2 u2Type, UINT2 u2Len)
{
    pParseInfo->pIfInfo->u2LastUnknownTlvType = u2Type;
    D6SrTrapSnmpSendTrap (&u2Type, DHCP6_SRV_UNKNOW_TLV_TRAP);
    UNUSED_PARAM (pu1Value);
    UNUSED_PARAM (u2Len);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessElaspedTime                                */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseProcessElaspedTime (tDhcp6ParseInfo * pParseInfo,
                             UINT1 *pu1Value, UINT2 u2Len)
{
    UNUSED_PARAM (pParseInfo);
    UNUSED_PARAM (pu1Value);
    UNUSED_PARAM (u2Len);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessUserClass                                  */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseProcessUserClass (tDhcp6ParseInfo * pParseInfo,
                           UINT1 *pu1Value, UINT2 u2Len)
{
    UNUSED_PARAM (pParseInfo);
    UNUSED_PARAM (pu1Value);
    UNUSED_PARAM (u2Len);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessVendorOpts                                 */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseProcessVendorOpts (tDhcp6ParseInfo * pParseInfo,
                            UINT1 *pu1Value, UINT2 u2Len)
{
    UNUSED_PARAM (pParseInfo);
    UNUSED_PARAM (pu1Value);
    UNUSED_PARAM (u2Len);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessVendorClass                                */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo      - DHCP6 packet                             */
/*                pu1Value  - Option Value                                   */
/*                u2Len     - Option Length                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseProcessVendorClass (tDhcp6ParseInfo * pParseInfo,
                             UINT1 *pu1Value, UINT2 u2Len)
{
    UNUSED_PARAM (pParseInfo);
    UNUSED_PARAM (pu1Value);
    UNUSED_PARAM (u2Len);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrParseProcessORO                                        */
/*                                                                           */
/* Description  : The function add dhcp6 option to the outgoing packet.      */
/*                                                                           */
/* Input        : pParseInfo    - DHCP6 packet                               */
/*                pu1Value        - Option Value                             */
/*                u2Len      - Option Length                                 */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrParseProcessORO (tDhcp6ParseInfo * pParseInfo, UINT1 *pu1Value, UINT2 u2Len)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    UINT4               u4EnterpriseNo = DHCP6_SRV_ENTERPRISE_NUMBER;
    UINT1              *pu1TlvLenOffset = NULL;
    UINT1              *pu1WriteBuffer = NULL;
    UINT2               u2ReadLen = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2VendorType = DHCP6_SRV_OPTION_VENDOR_OPTS;
    UINT2               u2OROCode = 0;

    pu1WriteBuffer = pParseInfo->pu1WriteBuffer + pParseInfo->u2WrittenLen;

    while (u2ReadLen < u2Len)
    {
        DHCP6_SRV_GET_2BYTE (u2OROCode, pu1Value);
        u2OptionLen = 0;
        pu1TlvLenOffset = NULL;

        pOptionInfo = D6SrPoolOptionListGetFirstNode (pParseInfo->pPool);

        while (pOptionInfo != NULL)
        {
            if ((pOptionInfo->u2Type == u2OROCode) &&
                (pOptionInfo->u2Type == DHCP6_SRV_OPTION_VENDOR_OPTS))
            {
                pSubOptionInfo = D6SrSubOptionListGetFirstNode (pOptionInfo);

                while (pSubOptionInfo != NULL)
                {
                    if (pu1TlvLenOffset == NULL)
                    {
                        /* Option Type */
                        DHCP6_SRV_PUT_2BYTE (pu1WriteBuffer, u2VendorType);
                        /* Option Length */
                        pu1TlvLenOffset = pu1WriteBuffer;
                        pu1WriteBuffer += DHCP6_SRV_TLV_LEN_SIZE;
                        /* Enterprise Number */
                        DHCP6_SRV_PUT_4BYTE (pu1WriteBuffer, u4EnterpriseNo);
                        u2OptionLen += 4;
                    }
                    /* Option Code */
                    DHCP6_SRV_PUT_2BYTE (pu1WriteBuffer,
                                         pSubOptionInfo->u2Type);
                    /* Option Length */
                    DHCP6_SRV_PUT_2BYTE (pu1WriteBuffer,
                                         pSubOptionInfo->u2Length);
                    /* Option Data */
                    DHCP6_SRV_PUT_NBYTE (pu1WriteBuffer,
                                         pSubOptionInfo->pu1Value,
                                         pSubOptionInfo->u2Length);
                    /* Option Code + Option Length + Length of Option Data */
                    u2OptionLen += pSubOptionInfo->u2Length + 4;

                    pSubOptionInfo =
                        D6SrSubOptionListGetNextNode (pSubOptionInfo,
                                                      pOptionInfo);
                }
            }
            if ((pOptionInfo->u2Type == u2OROCode) &&
                (pOptionInfo->u2Type != DHCP6_SRV_OPTION_VENDOR_OPTS))
            {

                if ((u2OptionLen + pParseInfo->u2WrittenLen) >
                    DHCP6_SRV_PDU_SIZE_MAX)
                {
                    return;
                }
                if (pu1TlvLenOffset == NULL)
                {
                    /* Option Type */
                    DHCP6_SRV_PUT_2BYTE (pu1WriteBuffer, pOptionInfo->u2Type);
                    /* Option Length */
                    pu1TlvLenOffset = pu1WriteBuffer;
                    pu1WriteBuffer += DHCP6_SRV_TLV_LEN_SIZE;
                }

                if ((pOptionInfo->u2Type != DHCP6_SRV_ORO_DOMAIN_LIST) &&
                    (pOptionInfo->u2Type != DHCP6_SRV_ORO_SIP_SERVER_D))
                {
                    /* Option Value */
                    DHCP6_SRV_PUT_NBYTE (pu1WriteBuffer, pOptionInfo->pu1Value,
                                         pOptionInfo->u2Length);
                    u2OptionLen += pOptionInfo->u2Length;
                }
                else
                {
                    /*when Option type is Domain name and SIP name then lenth
                     * fiesd is added at start and zer0 value added at the end.*/
                    DHCP6_SRV_PUT_1BYTE (pu1WriteBuffer, pOptionInfo->u2Length);
                    DHCP6_SRV_PUT_NBYTE (pu1WriteBuffer, pOptionInfo->pu1Value,
                                         pOptionInfo->u2Length);
                    DHCP6_SRV_PUT_1BYTE (pu1WriteBuffer, 0);
                    u2OptionLen += pOptionInfo->u2Length + 2;
                    /*length value 2 is incremented. */
                }
            }
            pOptionInfo =
                D6SrPoolOptionListGetNextNode (pOptionInfo, pParseInfo->pPool);
        }
        if (pu1TlvLenOffset != NULL)
        {
            /* Fill Option Length */
            DHCP6_SRV_PUT_2BYTE (pu1TlvLenOffset, u2OptionLen);
            pParseInfo->u2WrittenLen += (DHCP6_SRV_TLV_TYPE_SIZE +
                                         DHCP6_SRV_TLV_LEN_SIZE + u2OptionLen);

        }
        u2ReadLen += DHCP6_SRV_ORO_CODE_SIZE;
    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srpars.c                      */
/*-----------------------------------------------------------------------*/
