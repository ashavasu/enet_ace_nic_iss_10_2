/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srmain.c,v 1.9 2014/02/13 12:09:29 siva Exp $
 *
 * Description: This file contains DHCP6 task main loop and initialization
 *              routines.
 * ************************************************************************/

#include   "d6srinc.h"

PRIVATE INT4        D6SrMainTaskInit (VOID);
PRIVATE INT4        D6SrMainCreateBuddyMemPool (VOID);
PRIVATE VOID        D6SrMainSetDefaultValues (VOID);
PRIVATE VOID        D6SrMainTaskInitFailure (VOID);

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainTask                                           */
/*                                                                           */
/* Description  : This is the initialization function for the DHCP6          */
/*                module, called at bootup time.                             */
/*                                                                           */
/* Input        : pArg - Pointer to the arguments                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
D6SrMainTask (INT1 *pArg)
{
    UINT4               u4Event;

    UNUSED_PARAM (pArg);

    if (D6SrMainTaskInit () != OSIX_SUCCESS)
    {
        D6SrMainTaskInitFailure ();
        DHCP6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Registering the MIBS with SNMP Agent */
    RegisterFSDH6S ();
#endif

    /* Register with IP6, for the Interface Change Notification. */
    NetIpv6RegisterHigherLayerProtocol (IP6_DHCP6_SRV_PROTOID,
                                        NETIPV6_INTERFACE_PARAMETER_CHANGE,
                                        (VOID *) D6SrApiIp6IfStatusNotify);

    DHCP6_INIT_COMPLETE (OSIX_SUCCESS);
    while (1)
    {

        if (OsixEvtRecv (DHCP6_SRV_TASK_ID, DHCP6_SRV_DATA_EVENT |
                         DHCP6_SRV_CONTROL_EVENT, OSIX_WAIT,
                         &u4Event) != OSIX_SUCCESS)
        {
            continue;
        }
        D6SrMainLock ();
        if (u4Event & DHCP6_SRV_CONTROL_EVENT)
        {
            Dhcp6SrvProcessQMsg ();
        }

        /* Add the Socket Descriptor to Select utility for Packet Reception */
        if (u4Event & DHCP6_SRV_DATA_EVENT)
        {
            Dhcp6SrvRcvAndProcessPkt ();
            if (DHCP6_SRV_SOCKET_FD != -1)
            {
                if (SelAddFd (DHCP6_SRV_SOCKET_FD, D6SrSktPktInSocket)
                    != OSIX_SUCCESS)
                {
                    DHCP6_SRV_TRC (OS_RESOURCE_TRC, "D6SrMainTask: Socket"
                                   "Select called Failed \r\n");
                }
            }
        }
        D6SrMainUnLock ();
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainTaskInitFailure                                    */
/*                                                                           */
/* Description  : This function handles the task init failure condition      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrMainTaskInitFailure (VOID)
{
    tDhcp6SrvQMsg      *pMsg = NULL;
    /* Clear the configuration queue */

    if (DHCP6_SRV_QUEUE_ID != 0)
    {
        /* Dequeue Configuration messages from the Config Queue */
        while (OsixQueRecv (DHCP6_SRV_QUEUE_ID, (UINT1 *) &pMsg,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)

        {
            if (pMsg == NULL)

            {
                continue;
            }
            DHCP6_SRV_FREE_MEM_MSGQ (pMsg);
        }
        /* Delete Queue */
        OsixQueDel (DHCP6_SRV_QUEUE_ID);
        DHCP6_SRV_QUEUE_ID = 0;
    }

    /* Delete the memory pool and tables */
    D6srSizingMemDeleteMemPools ();
    D6SrIfDeleteTable ();
    D6SrKeyDeleteTable ();
    D6SrRealmDeleteTable ();
    D6SrClientDeleteTable ();
    D6SrInPrefixDeleteTable ();
    D6SrSubOptionDeleteTable ();
    D6SrOptionDeleteTable ();
    D6SrPoolDeleteTable ();
    /* Delete the semaphore */
    if (DHCP6_SRV_SEM_ID != 0)
    {
        OsixSemDel (DHCP6_SRV_SEM_ID);
        DHCP6_SRV_SEM_ID = 0;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainTaskInit                                           */
/*                                                                           */
/* Description  : This function creates the task queue, allocates MemPools   */
/*                for task queue messages and creates protocol semaphore.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6SrMainTaskInit (VOID)
{
    INT4                i4SysLogId = 0;

    MEMSET (&gDhcp6SrvGblInfo, 0, DHCP6_SRV_GLOBAL_INFO_SIZE);

    if (OsixCreateSem (DHCP6_SRV_SEM_NAME, DHCP6_SRV_SEM_CREATE_INIT_CNT,
                       OSIX_DEFAULT_SEM_MODE,
                       &(DHCP6_SRV_SEM_ID)) == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "D6SrMainTaskInit: Semaphore creation"
                       "Failed \r\n");
        return OSIX_FAILURE;
    }

    if (OsixQueCrt (DHCP6_SRV_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_D6SR_SRV_QMSGS, &(DHCP6_SRV_QUEUE_ID)) == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "D6SrMainTaskInit: Task Queue creation"
                       "Failed \r\n");
        return OSIX_FAILURE;
    }

    /* Creating the Global RB Tree. */
    if (D6SrIfCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrIfCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrKeyCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrKeyCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrRealmCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrRealmCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrClientCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrClientCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrInPrefixCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrInPrefixCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrSubOptionCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrSubOptionCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrOptionCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrOptionCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }
    if (D6SrPoolCreateTable () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Global "
                       "D6SrPoolCreateTable Failed \r\n");
        return OSIX_FAILURE;
    }

    /* Create configuration queue related Mem pools seperately. */
    if (D6srSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "D6SrMainTaskInit: Mem Pools Creation Failed\r\n ");
        return OSIX_FAILURE;
    }

    if (D6SrMainCreateBuddyMemPool () == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "D6SrMainTaskInit: Buddy Mempool"
                       "creation Failed \r\n");
        return OSIX_FAILURE;
    }

    /* Initialize the default variables */
    D6SrMainSetDefaultValues ();
#ifdef SYSLOG_WANTED
    /* Register with SysLog */
    i4SysLogId = SYS_LOG_REGISTER (DHCP6_SRV_TASK, SYSLOG_CRITICAL_LEVEL);
    if (i4SysLogId <= 0)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC,
                       "D6SrMainTaskInit: Sys log" "Failed \r\n");
        return OSIX_FAILURE;
    }
#endif /* SYSLOG_WANTED */

    /* Save the syslog id in the MIB */
    DHCP6_SRV_SYSLOG_ID = (UINT4) (i4SysLogId);

    DHCP6_SRV_SOCKET_FD = -1;

    if (OsixTskIdSelf (&(DHCP6_SRV_TASK_ID)) == OSIX_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "D6SrMainTaskInit: Obtaining Task Id"
                       "Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainCreateBuddyMemPool                                 */
/*                                                                           */
/* Description  : This function creates the required Buddy mempools          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6SrMainCreateBuddyMemPool (VOID)
{
    UINT4               u4MinBlkAdjusted = 0;
    UINT4               u4MaxBlkAdjusted = 0;

    UtilCalculateBuddyMemPoolAdjust (DHCP6_SRV_BUDDY_SIZE_MIN,
                                     DHCP6_SRV_BUDDY_SIZE_MAX,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);

    DHCP6_SRV_BUDDY_MEM = MemBuddyCreate (u4MaxBlkAdjusted,
                                          u4MinBlkAdjusted,
                                          DHCP6_SRV_BUDDY_BLOCK,
                                          BUDDY_CONT_BUF);

    if (DHCP6_SRV_BUDDY_MEM == BUDDY_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "D6SrMainCreateBuddyMemPool: "
                       "Buddy Mempool creation Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainDeleteBuddyMemPool                                 */
/*                                                                           */
/* Description  : This function deletes the required Buddy mempools          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrMainDeleteBuddyMemPool (VOID)
{
    if (DHCP6_SRV_BUDDY_MEM != BUDDY_FAILURE)
    {
        MemBuddyDestroy ((UINT1) DHCP6_SRV_BUDDY_MEM);
        DHCP6_SRV_BUDDY_MEM = BUDDY_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainSetDefaultValues                                   */
/*                                                                           */
/* Description  : This procedure initialises the default values.             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrMainSetDefaultValues ()
{
    DHCP6_SRV_DEBUG_TRACE = DHCP6_SRV_CRITICAL_TRC;
    DHCP6_SRV_SYSLOG_ID = OSIX_FALSE;
    DHCP6_SRV_TRAP_CONTROL = OSIX_FALSE;
    DHCP6_SRV_LISTEN_PORT = DHCP6_SRV_LISTEN_PORT_DEF;
    DHCP6_SRV_CLNT_TRANSMIT_PORT = DHCP6_SRV_CLNT_TRANSMIT_PORT_DEF;
    DHCP6_SRV_RLY_TRANSMIT_PORT = DHCP6_SRV_RLY_TRANSMIT_PORT_DEF;
    DHCP6_SRV_SYSLOG_ADMIN_STATUS = DHCP6_DISABLED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainLock                                               */
/*                                                                           */
/* Description  : Takes a task Sempaphore.                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrMainLock (VOID)
{
    if (OsixSemTake (DHCP6_SRV_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrMainUnLock                                             */
/*                                                                           */
/* Description  : Releases a task Sempaphore.                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrMainUnLock (VOID)
{
    if (OsixSemGive (DHCP6_SRV_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvJoinMcastGroup                                     */
/*                                                                           */
/* Description  : Joins DHCP6 server to multicast group address              */
/*                in the specified interface                                 */
/*                                                                           */
/* Input        : i4SocketId,u4IfIndex,pMcastGroupAddr                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
Dhcp6SrvJoinMcastGroup (INT4 i4SocketId, UINT4 u4IfIndex,
                        tIp6Addr * pMcastGroupAddr)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
    INT4                i4ReturnValue = 0;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMCPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr, IPVX_IPV6_ADDR_LEN);
#ifdef LNXIP6_WANTED
    i4ReturnValue = NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo);
    if (i4ReturnValue == NETIPV6_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "info not present for index\r\n");
        return OSIX_FAILURE;
    }
    ip6Mreq.ipv6mr_interface = NetIpv6IfInfo.u4IpPort;

#else
    ip6Mreq.ipv6mr_interface = u4IfIndex;
#endif
    if (setsockopt
        (i4SocketId, IPPROTO_IPV6, IPV6_JOIN_GROUP,
         &(ip6Mreq), sizeof (struct ipv6_mreq)) < 0)
    {
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (i4SocketId);
    if (NetIpv6McastJoin (u4IfIndex, pMcastGroupAddr) != NETIPV6_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvLeaveMcastGroup                                    */
/*                                                                           */
/* Description  : Leaves DHCP6 server from multicast group address           */
/*                in the specified interface                                 */
/*                                                                           */
/* Input        : i4SocketId,u4IfIndex,pMcastGroupAddr                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
Dhcp6SrvLeaveMcastGroup (INT4 i4SocketId, UINT4 u4IfIndex,
                         tIp6Addr * pMcastGroupAddr)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
    INT4                i4ReturnValue = 0;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMCPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr, IPVX_IPV6_ADDR_LEN);
#ifdef LNXIP6_WANTED
    i4ReturnValue = NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo);
    if (i4ReturnValue == NETIPV6_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "info not present for index\r\n");
        return OSIX_FAILURE;
    }
    ip6Mreq.ipv6mr_interface = NetIpv6IfInfo.u4IpPort;
#else
    ip6Mreq.ipv6mr_interface = u4IfIndex;
#endif

    if (setsockopt (i4SocketId, IPPROTO_IPV6, IPV6_LEAVE_GROUP,
                    &ip6Mreq, sizeof (struct ipv6_mreq)) < 0)
    {
        return OSIX_FAILURE;
    }
#endif
    UNUSED_PARAM (i4SocketId);
    if (NetIpv6McastLeave (u4IfIndex, pMcastGroupAddr) != NETIPV6_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
