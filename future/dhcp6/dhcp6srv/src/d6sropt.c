/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access option table.
 * ************************************************************************/

#include "d6srinc.h"
PRIVATE INT4 D6SrOptionRBCmp PROTO ((tRBElem *, tRBElem *));
/***************************************************************************
 * FUNCTION NAME    : D6SrOptionCreateTable
 *
 * DESCRIPTION      : This function creats the option table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrOptionCreateTable (VOID)
{
    DHCP6_SRV_OPTION_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvOptionInfo, GblRBLink),
                              D6SrOptionRBCmp);
    if (DHCP6_SRV_OPTION_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrOptionCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionDeleteTable
 *
 * DESCRIPTION      : This function Deletes the option table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrOptionDeleteTable (VOID)
{
    if (DHCP6_SRV_OPTION_TABLE != NULL)
    {
        D6SrOptionDrainTable ();
        RBTreeDestroy (DHCP6_SRV_OPTION_TABLE, NULL, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the option table. 
 *                    Indices of this table are -
 *                    o Option Index
 *                    o Option Index
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrOptionRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvOptionInfo *pOptionEntryA = NULL;
    tDhcp6SrvOptionInfo *pOptionEntryB = NULL;
    pOptionEntryA = (tDhcp6SrvOptionInfo *) pRBElem1;
    pOptionEntryB = (tDhcp6SrvOptionInfo *) pRBElem2;
    if (pOptionEntryA->pPool->u4PoolIndex != pOptionEntryB->pPool->u4PoolIndex)
    {
        if (pOptionEntryA->pPool->u4PoolIndex <
            pOptionEntryB->pPool->u4PoolIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    if (pOptionEntryA->u4OptionIndex != pOptionEntryB->u4OptionIndex)
    {
        if (pOptionEntryA->u4OptionIndex < pOptionEntryB->u4OptionIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionCreateNode 
 *
 * DESCRIPTION      : This function creates a option table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvOptionInfo * - Pointer to the created option node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvOptionInfo *
D6SrOptionCreateNode (VOID)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    DHCP6_SRV_ALLOC_MEM_OPTION (pOptionInfo);
    if (pOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrOptionCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }
    MEMSET (pOptionInfo, 0x00, sizeof (tDhcp6SrvOptionInfo));
    return pOptionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionAddNode
 *
 * DESCRIPTION      : This function add a node to the option table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pOptionInfo - pointer to option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrOptionAddNode (tDhcp6SrvOptionInfo * pOptionInfo)
{
    if (RBTreeAdd (DHCP6_SRV_OPTION_TABLE, pOptionInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrOptionAddNode: "
                       " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }

    /* Add in the node  in pool option list also */
    D6SrPoolOptionListAddNode (pOptionInfo);

    TMO_SLL_Init ((&pOptionInfo->SubOptionList));

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionDelNode
 *
 * DESCRIPTION      : This function delete a option node from the
 *                    option table and release the memory used by that
 *                    node to the memory option. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pOptionInfo - pointer to option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrOptionDelNode (tDhcp6SrvOptionInfo * pOptionInfo)
{
    /* Remove the option node from pool option list */
    D6SrPoolOptionListDelNode (pOptionInfo);
    RBTreeRem (DHCP6_SRV_OPTION_TABLE, pOptionInfo);
    DHCP6_SRV_FREE_MEM_OPTION (pOptionInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a option node, and then release the
 *                    memory used by the node to the memory option.
 *
 * INPUT            : pRBElem - pointer to the option node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrOptionRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    /* Remove the option node from pool option list */
    D6SrPoolOptionListDelNode ((tDhcp6SrvOptionInfo *) pRBElem);
    DHCP6_SRV_FREE_MEM_OPTION (pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionDrainTable
 *
 * DESCRIPTION      : This function deletes all the option nodes
 *                    associated with the option table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrOptionDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_OPTION_TABLE, D6SrOptionRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionGetFirstNode
 *
 * DESCRIPTION      : This function returns the first option node
 *                    from the option table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvOptionInfo * - pointer to the first option info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvOptionInfo *
D6SrOptionGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_OPTION_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionGetNextNode
 *
 * DESCRIPTION      : This function returns the next option info
 *                    from the option table.
 *
 * INPUT            : pCurrentOptionInfo - pointer to the option info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvOptionInfo * - Pointer to the next option info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvOptionInfo *
D6SrOptionGetNextNode (tDhcp6SrvOptionInfo * pCurrentOptionInfo)
{
    return RBTreeGetNext (DHCP6_SRV_OPTION_TABLE, pCurrentOptionInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionGetNode
 *
 * DESCRIPTION      : This function helps to find a option node from the option 
 *                    table.
 *
 * INPUT            : u4OptionIndex - Option Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvOptionInfo * - Pointer to the next option info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvOptionInfo *
D6SrOptionGetNode (UINT4 u4PoolIndex, UINT4 u4OptionIndex)
{
    tDhcp6SrvOptionInfo Key;
    Key.u4OptionIndex = u4OptionIndex;
    Key.pPool = D6SrPoolGetNode (u4PoolIndex);
    if (Key.pPool == NULL)
    {
        return NULL;
    }
    return RBTreeGet (DHCP6_SRV_OPTION_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionGetFreeIndex
 *
 * DESCRIPTION      : This function helps to find a free index which can be
 *                    used to create a new entry in the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4OptionIndex - Pointer to the option index
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrOptionGetFreeIndex (UINT4 u4PoolIndex, UINT4 *pu4FreeOptionIndex)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    UINT4               u4OptionIndex = 0;
    UINT4               u4MaxOptionIndex = DHCP6_SRV_OPTION_INDEX_MAX;

    pPoolInfo = D6SrPoolGetNode (u4PoolIndex);

    if (pPoolInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    for (u4OptionIndex = 1; u4OptionIndex <= u4MaxOptionIndex; u4OptionIndex++)
    {
        pOptionInfo = D6SrOptionGetNode (u4PoolIndex, u4OptionIndex);

        if (pOptionInfo == NULL)
        {
            *pu4FreeOptionIndex = u4OptionIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionSubOptionListAddNode
 *
 * DESCRIPTION      : This function add a node in the sub-option list 
 *
 * INPUT            : pSubOptionInfo - pointer to sub-option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrOptionSubOptionListAddNode (tDhcp6SrvSubOptionInfo * pSubOptionInfo)
{
    TMO_SLL_Init_Node (&(pSubOptionInfo->OptionSllLink));
    TMO_SLL_Add (&(pSubOptionInfo->pOption->SubOptionList),
                 &(pSubOptionInfo->OptionSllLink));
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrOptionSubOptionListDelNode
 *
 * DESCRIPTION      : This function delete a sub-option node from the
 *                    option list.
 *
 * INPUT            : pSubOptionInfo - pointer to sub-option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrOptionSubOptionListDelNode (tDhcp6SrvSubOptionInfo * pSubOptionInfo)
{
    TMO_SLL_Delete (&(pSubOptionInfo->pOption->SubOptionList),
                    &(pSubOptionInfo->OptionSllLink));
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionListGetFirstNode
 *
 * DESCRIPTION      : This function gets the first node in the option
 *                    list.
 *
 * INPUT            : pPoolInfo - pointer to pool information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : Pointer to the first node in the list
 *
 **************************************************************************/
PUBLIC tDhcp6SrvSubOptionInfo *
D6SrSubOptionListGetFirstNode (tDhcp6SrvOptionInfo * pOptionInfo)
{
    return (tDhcp6SrvSubOptionInfo *)
        TMO_SLL_First (&(pOptionInfo->SubOptionList));
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionListGetNextNode
 *
 * DESCRIPTION      : This function gets the next node in the option
 *                    list.
 *
 * INPUT            : pPoolInfo - pointer to pool information node
 *                    pOptionInfo - Option Info
 *
 * OUTPUT           : None
 *
 * RETURNS          : Pointer to the next node in the list
 *
 **************************************************************************/
PUBLIC tDhcp6SrvSubOptionInfo *
D6SrSubOptionListGetNextNode (tDhcp6SrvSubOptionInfo * pSubOptionInfo,
                              tDhcp6SrvOptionInfo * pOptionInfo)
{
    return (tDhcp6SrvSubOptionInfo *)
        TMO_SLL_Next (&(pOptionInfo->SubOptionList),
                      &(pSubOptionInfo->OptionSllLink));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srOpt.c                       */
/*-----------------------------------------------------------------------*/
