/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access realm table.
 * ************************************************************************/

#include "d6srinc.h"

PRIVATE INT4 D6SrRealmRBCmp PROTO ((tRBElem *, tRBElem *));

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmCreateTable
 *
 * DESCRIPTION      : This function creats the realm table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrRealmCreateTable (VOID)
{
    DHCP6_SRV_REALM_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvRealmInfo, GblRBLink),
                              D6SrRealmRBCmp);
    if (DHCP6_SRV_REALM_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrRealmCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmDeleteTable
 *
 * DESCRIPTION      : This function Deletes the realm table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrRealmDeleteTable (VOID)
{
    if (DHCP6_SRV_REALM_TABLE != NULL)
    {
        D6SrRealmDrainTable ();
        RBTreeDestroy (DHCP6_SRV_REALM_TABLE, NULL, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the realm table. 
 *                    Indices of this table are -
 *                    o Realm Index
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrRealmRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvRealmInfo *pRealmEntryA = NULL;
    tDhcp6SrvRealmInfo *pRealmEntryB = NULL;

    pRealmEntryA = (tDhcp6SrvRealmInfo *) pRBElem1;
    pRealmEntryB = (tDhcp6SrvRealmInfo *) pRBElem2;

    if (pRealmEntryA->u4RealmIndex != pRealmEntryB->u4RealmIndex)
    {
        if (pRealmEntryA->u4RealmIndex < pRealmEntryB->u4RealmIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmCreateNode 
 *
 * DESCRIPTION      : This function creates a realm table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvRealmInfo * - Pointer to the created realm node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvRealmInfo *
D6SrRealmCreateNode (VOID)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;

    DHCP6_SRV_ALLOC_MEM_REALM (pRealmInfo);

    if (pRealmInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrRealmCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }

    MEMSET (pRealmInfo, 0x00, sizeof (tDhcp6SrvRealmInfo));

    return pRealmInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmAddNode
 *
 * DESCRIPTION      : This function add a node to the realm table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pRealmInfo - pointer to realm information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrRealmAddNode (tDhcp6SrvRealmInfo * pRealmInfo)
{
    if (RBTreeAdd (DHCP6_SRV_REALM_TABLE, pRealmInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrRealmAddNode: "
                       " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmDelNode
 *
 * DESCRIPTION      : This function delete a realm node from the
 *                    realm table and release the memory used by that
 *                    node to the memory realm. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pRealmInfo - pointer to realm information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrRealmDelNode (tDhcp6SrvRealmInfo * pRealmInfo)
{
    /* Remove the mapping of client with this realm */
    D6SrRealmUnMapClient (pRealmInfo);

    RBTreeRem (DHCP6_SRV_REALM_TABLE, pRealmInfo);

    DHCP6_SRV_FREE_MEM_REALM (pRealmInfo);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a realm node, and then release the
 *                    memory used by the node to the memory realm.
 *
 * INPUT            : pRBElem - pointer to the realm node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrRealmRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    /* Remove the mapping of client with this realm */
    D6SrRealmUnMapClient ((tDhcp6SrvRealmInfo *) pRBElem);

    DHCP6_SRV_FREE_MEM_REALM (pRBElem);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmDrainTable
 *
 * DESCRIPTION      : This function deletes all the realm nodes
 *                    associated with the realm table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrRealmDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_REALM_TABLE, D6SrRealmRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmGetFirstNode
 *
 * DESCRIPTION      : This function returns the first realm node
 *                    from the realm table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvRealmInfo * - pointer to the first realm info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvRealmInfo *
D6SrRealmGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_REALM_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmGetNextNode
 *
 * DESCRIPTION      : This function returns the next realm info
 *                    from the realm table.
 *
 * INPUT            : pCurrentRealmInfo - pointer to the realm info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvRealmInfo * - Pointer to the next realm info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvRealmInfo *
D6SrRealmGetNextNode (tDhcp6SrvRealmInfo * pCurrentRealmInfo)
{
    return RBTreeGetNext (DHCP6_SRV_REALM_TABLE, pCurrentRealmInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmGetNode
 *
 * DESCRIPTION      : This function helps to find a realm node from the realm 
 *                    table.
 *
 * INPUT            : u4RealmIndex - Realm Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvRealmInfo * - Pointer to the next realm info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvRealmInfo *
D6SrRealmGetNode (UINT4 u4RealmIndex)
{
    tDhcp6SrvRealmInfo  Key;

    Key.u4RealmIndex = u4RealmIndex;

    return RBTreeGet (DHCP6_SRV_REALM_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmGetFreeIndex
 *
 * DESCRIPTION      : This function helps to find a free index which can be
 *                    used to create a new entry in the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4RealmIndex - Pointer to the realm index
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrRealmGetFreeIndex (UINT4 *pu4FreeRealmIndex)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    UINT4               u4RealmIndex = 0;
    UINT4               u4MaxRealmIndex = DHCP6_SRV_REALM_INDEX_MAX;

    for (u4RealmIndex = 1; u4RealmIndex <= u4MaxRealmIndex; u4RealmIndex++)
    {
        pRealmInfo = D6SrRealmGetNode (u4RealmIndex);

        if (pRealmInfo == NULL)
        {
            *pu4FreeRealmIndex = u4RealmIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmUnMapClient
 *
 * DESCRIPTION      : This function remove the mapping of client with this 
 *                    realm table.
 *
 * INPUT            : pRealmInfo - pointer to realm information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrRealmUnMapClient (tDhcp6SrvRealmInfo * pRealmInfo)
{
    UNUSED_PARAM (pRealmInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrRealmLookupNode
 *
 * DESCRIPTION      : This function helps to find a realm node from the realm 
 *                    table for a given realm name
 *
 * INPUT            : pu1Realm - Realm name
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvRealmInfo * - Pointer to the realm info. 
 *
 **************************************************************************/
PUBLIC tDhcp6SrvRealmInfo *
D6SrRealmLookupNode (UINT1 *pu1Realm)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetFirstNode ();
    while (pRealmInfo != NULL)
    {
        if (MEMCMP (pu1Realm, pRealmInfo->au1Name, DHCP6_SRV_REALM_NAME_MAX)
            == 0)
        {
            break;
        }
        pRealmInfo = D6SrRealmGetNextNode (pRealmInfo);
    }
    return pRealmInfo;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srrlm.c                      */
/*-----------------------------------------------------------------------*/
