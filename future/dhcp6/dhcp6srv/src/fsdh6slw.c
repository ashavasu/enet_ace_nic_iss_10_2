/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsdh6slw.c,v 1.21 2016/04/29 09:10:20 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include   "d6srinc.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvDebugTrace
Input       :  The Indices

The Object 
retValFsDhcp6SrvDebugTrace
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvDebugTrace (tSNMP_OCTET_STRING_TYPE *
                            pRetValFsDhcp6SrvDebugTrace)
{
    pRetValFsDhcp6SrvDebugTrace->i4_Length =
        Dhcp6SrvUtilGetTraceInputValue (pRetValFsDhcp6SrvDebugTrace->
                                        pu1_OctetList, DHCP6_SRV_DEBUG_TRACE);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvRealmTableNextIndex
Input       :  The Indices

The Object 
retValFsDhcp6SrvRealmTableNextIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvRealmTableNextIndex (UINT4
                                     *pu4RetValFsDhcp6SrvRealmTableNextIndex)
{
    if (D6SrRealmGetFreeIndex (pu4RetValFsDhcp6SrvRealmTableNextIndex)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvClientTableNextIndex
Input       :  The Indices

The Object 
retValFsDhcp6SrvClientTableNextIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvClientTableNextIndex (UINT4
                                      *pu4RetValFsDhcp6SrvClientTableNextIndex)
{
    if (D6SrClientGetFreeIndex (pu4RetValFsDhcp6SrvClientTableNextIndex)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolTableNextIndex
Input       :  The Indices

The Object 
retValFsDhcp6SrvPoolTableNextIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolTableNextIndex (UINT4
                                    *pu4RetValFsDhcp6SrvPoolTableNextIndex)
{
    if (D6SrPoolGetFreeIndex (pu4RetValFsDhcp6SrvPoolTableNextIndex)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvTrapAdminControl
Input       :  The Indices

The Object 
retValFsDhcp6SrvTrapAdminControl
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvTrapAdminControl (tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsDhcp6SrvTrapAdminControl)
{
    pRetValFsDhcp6SrvTrapAdminControl->pu1_OctetList[0] =
        DHCP6_SRV_TRAP_CONTROL;
    pRetValFsDhcp6SrvTrapAdminControl->i4_Length = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvSysLogAdminStatus
Input       :  The Indices

The Object 
retValFsDhcp6SrvSysLogAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvSysLogAdminStatus (INT4 *pi4RetValFsDhcp6SrvSysLogAdminStatus)
{
    if (DHCP6_SRV_SYSLOG_ADMIN_STATUS == OSIX_TRUE)
    {
        *pi4RetValFsDhcp6SrvSysLogAdminStatus = DHCP6_ENABLED;
    }
    else
    {
        *pi4RetValFsDhcp6SrvSysLogAdminStatus = DHCP6_DISABLED;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6SrvListenPort
 Input       :  The Indices

                The Object 
                retValFsDhcp6SrvListenPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6SrvListenPort (INT4 *pi4RetValFsDhcp6SrvListenPort)
{
    *pi4RetValFsDhcp6SrvListenPort = DHCP6_SRV_LISTEN_PORT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6SrvClientTransmitPort
 Input       :  The Indices

                The Object 
                retValFsDhcp6SrvClientTransmitPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6SrvClientTransmitPort (INT4 *pi4RetValFsDhcp6SrvClientTransmitPort)
{
    *pi4RetValFsDhcp6SrvClientTransmitPort = DHCP6_SRV_CLNT_TRANSMIT_PORT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6SrvRelayTransmitPort
 Input       :  The Indices

                The Object 
                retValFsDhcp6SrvRelayTransmitPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6SrvRelayTransmitPort (INT4 *pi4RetValFsDhcp6SrvRelayTransmitPort)
{
    *pi4RetValFsDhcp6SrvRelayTransmitPort = DHCP6_SRV_RLY_TRANSMIT_PORT;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvDebugTrace
Input       :  The Indices

The Object 
setValFsDhcp6SrvDebugTrace
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvDebugTrace (tSNMP_OCTET_STRING_TYPE *
                            pSetValFsDhcp6SrvDebugTrace)
{
    UINT4               u4TrcOption = 0;

    u4TrcOption = Dhcp6SrvUtilGetTraceOptionValue
        (pSetValFsDhcp6SrvDebugTrace->pu1_OctetList,
         pSetValFsDhcp6SrvDebugTrace->i4_Length);

    if ((STRNCMP (pSetValFsDhcp6SrvDebugTrace->pu1_OctetList,
                  "enable", STRLEN ("enable"))) == 0)
    {
        DHCP6_SRV_DEBUG_TRACE |= u4TrcOption;
    }
    else if ((STRNCMP (pSetValFsDhcp6SrvDebugTrace->pu1_OctetList,
                       "disable", STRLEN ("disable"))) == 0)
    {
        DHCP6_SRV_DEBUG_TRACE &= ~u4TrcOption;
    }
    /* Default case. if nothing is appended, assume critical is done */
    else if ((STRNCMP (pSetValFsDhcp6SrvDebugTrace->pu1_OctetList,
                       "critical", STRLEN ("critical"))) == 0)
    {
        DHCP6_SRV_DEBUG_TRACE = u4TrcOption;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvTrapAdminControl
Input       :  The Indices

The Object 
setValFsDhcp6SrvTrapAdminControl
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvTrapAdminControl (tSNMP_OCTET_STRING_TYPE
                                  * pSetValFsDhcp6SrvTrapAdminControl)
{
    DHCP6_SRV_TRAP_CONTROL =
        pSetValFsDhcp6SrvTrapAdminControl->pu1_OctetList[0];
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvSysLogAdminStatus
Input       :  The Indices

The Object 
setValFsDhcp6SrvSysLogAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvSysLogAdminStatus (INT4 i4SetValFsDhcp6SrvSysLogAdminStatus)
{

    if (i4SetValFsDhcp6SrvSysLogAdminStatus == SNMP_SUCCESS)
    {
        DHCP6_SRV_SYSLOG_ADMIN_STATUS = OSIX_TRUE;
    }
    else
    {
        DHCP6_SRV_SYSLOG_ADMIN_STATUS = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6SrvListenPort
 Input       :  The Indices

                The Object 
                setValFsDhcp6SrvListenPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6SrvListenPort (INT4 i4SetValFsDhcp6SrvListenPort)
{
    INT4                i4SavedPort = 0;
    if (i4SetValFsDhcp6SrvListenPort == DHCP6_SRV_LISTEN_PORT)
    {
        return SNMP_SUCCESS;
    }
    i4SavedPort = DHCP6_SRV_LISTEN_PORT;
    DHCP6_SRV_LISTEN_PORT = (UINT2) i4SetValFsDhcp6SrvListenPort;
    /* If no interface is created in relay then don't create the socket
     */
    if (D6SrIfGetFirstNode () == NULL)
    {
        return SNMP_SUCCESS;
    }
    D6SrSktSockDeInit ();
    if (D6SrSktSockInit () != OSIX_SUCCESS)
    {
        DHCP6_SRV_LISTEN_PORT = (UINT2) i4SavedPort;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6SrvClientTransmitPort
 Input       :  The Indices

                The Object 
                setValFsDhcp6SrvClientTransmitPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6SrvClientTransmitPort (INT4 i4SetValFsDhcp6SrvClientTransmitPort)
{
    DHCP6_SRV_CLNT_TRANSMIT_PORT = (UINT2) i4SetValFsDhcp6SrvClientTransmitPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6SrvRelayTransmitPort
 Input       :  The Indices

                The Object 
                setValFsDhcp6SrvRelayTransmitPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6SrvRelayTransmitPort (INT4 i4SetValFsDhcp6SrvRelayTransmitPort)
{
    DHCP6_SRV_RLY_TRANSMIT_PORT = (UINT2) i4SetValFsDhcp6SrvRelayTransmitPort;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvDebugTrace
Input       :  The Indices

The Object 
testValFsDhcp6SrvDebugTrace
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvDebugTrace (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsDhcp6SrvDebugTrace)
{
    UINT4               u4TrcOption = 0;

    if ((pTestValFsDhcp6SrvDebugTrace->i4_Length >= DHCP6_SRV_TRC_MAX_SIZE) ||
        (pTestValFsDhcp6SrvDebugTrace->i4_Length < DHCP6_SRV_TRC_MIN_SIZE))
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Trace Input"
                       "length \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u4TrcOption = Dhcp6SrvUtilGetTraceOptionValue
        (pTestValFsDhcp6SrvDebugTrace->pu1_OctetList,
         pTestValFsDhcp6SrvDebugTrace->i4_Length);

    if (u4TrcOption == DHCP6_SRV_INVALID_TRC)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Trace Input"
                       "String \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvTrapAdminControl
Input       :  The Indices

The Object 
testValFsDhcp6SrvTrapAdminControl
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvTrapAdminControl (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsDhcp6SrvTrapAdminControl)
{
    if ((UINT4) pTestValFsDhcp6SrvTrapAdminControl->i4_Length > sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6SrvTrapAdminControl->pu1_OctetList[0] >
        (UINT1) DHCP6_SRV_TRAP_CONTROL_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvSysLogAdminStatus
Input       :  The Indices

The Object 
testValFsDhcp6SrvSysLogAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvSysLogAdminStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsDhcp6SrvSysLogAdminStatus)
{
    if ((i4TestValFsDhcp6SrvSysLogAdminStatus != DHCP6_ENABLED) &&
        (i4TestValFsDhcp6SrvSysLogAdminStatus != DHCP6_DISABLED))
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Sys Log Admin "
                       "Status \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6SrvListenPort
 Input       :  The Indices

                The Object 
                testValFsDhcp6SrvListenPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvListenPort (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsDhcp6SrvListenPort)
{
    if ((i4TestValFsDhcp6SrvListenPort < DHCP6_SRV_UDP_PORT_MIN) ||
        (i4TestValFsDhcp6SrvListenPort > DHCP6_SRV_UDP_PORT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvReplyTransmitPort
Input       :  The Indices

The Object 
testValFsDhcp6SrvReplyTransmitPort
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvClientTransmitPort (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsDhcp6SrvClientTransmitPort)
{
    if ((i4TestValFsDhcp6SrvClientTransmitPort < DHCP6_SRV_UDP_PORT_MIN) ||
        (i4TestValFsDhcp6SrvClientTransmitPort > DHCP6_SRV_UDP_PORT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6SrvRelayTransmitPort
 Input       :  The Indices

                The Object 
                testValFsDhcp6SrvRelayTransmitPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvRelayTransmitPort (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsDhcp6SrvRelayTransmitPort)
{
    if ((i4TestValFsDhcp6SrvRelayTransmitPort < DHCP6_SRV_UDP_PORT_MIN) ||
        (i4TestValFsDhcp6SrvRelayTransmitPort > DHCP6_SRV_UDP_PORT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvDebugTrace
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvDebugTrace (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvTrapAdminControl
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvTrapAdminControl (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvSysLogAdminStatus
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvSysLogAdminStatus (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6SrvListenPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvListenPort (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6SrvClientTransmitPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvClientTransmitPort (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6SrvRelayTransmitPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvRelayTransmitPort (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6SrvPoolTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvPoolTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvPoolTable (UINT4 u4FsDhcp6SrvPoolIndex)
{
    if (D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvPoolTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvPoolTable (UINT4 *pu4FsDhcp6SrvPoolIndex)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetFirstNode ();

    if (pPoolInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4FsDhcp6SrvPoolIndex = pPoolInfo->u4PoolIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvPoolTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
nextFsDhcp6SrvPoolIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvPoolTable (UINT4 u4FsDhcp6SrvPoolIndex,
                                    UINT4 *pu4NextFsDhcp6SrvPoolIndex)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;
    tDhcp6SrvPoolInfo  *pNextPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);
    if (pPoolInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextPoolInfo = D6SrPoolGetNextNode (pPoolInfo);
    if (pNextPoolInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDhcp6SrvPoolIndex = pNextPoolInfo->u4PoolIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolName
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolName
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolName (UINT4 u4FsDhcp6SrvPoolIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6SrvPoolName)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        pRetValFsDhcp6SrvPoolName->i4_Length = (INT4) pPoolInfo->u1NameLength;
        MEMCPY (pRetValFsDhcp6SrvPoolName->pu1_OctetList,
                pPoolInfo->au1Name, pRetValFsDhcp6SrvPoolName->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolPreference
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolPreference
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolPreference (UINT4 u4FsDhcp6SrvPoolIndex,
                                INT4 *pi4RetValFsDhcp6SrvPoolPreference)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        *pi4RetValFsDhcp6SrvPoolPreference = (INT4) pPoolInfo->u2Preference;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolDuidType
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolDuidType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolDuidType (UINT4 u4FsDhcp6SrvPoolIndex,
                              INT4 *pi4RetValFsDhcp6SrvPoolDuidType)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        *pi4RetValFsDhcp6SrvPoolDuidType = (INT4) pPoolInfo->u1DuidType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolDuid
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolDuid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolDuid (UINT4 u4FsDhcp6SrvPoolIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6SrvPoolDuid)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        pRetValFsDhcp6SrvPoolDuid->i4_Length = (INT4) pPoolInfo->u1DuidLength;
        MEMCPY (pRetValFsDhcp6SrvPoolDuid->pu1_OctetList,
                pPoolInfo->au1Duid, pRetValFsDhcp6SrvPoolDuid->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolDuidIfIndex
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolDuidIfIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolDuidIfIndex (UINT4 u4FsDhcp6SrvPoolIndex,
                                 INT4 *pi4RetValFsDhcp6SrvPoolDuidIfIndex)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        *pi4RetValFsDhcp6SrvPoolDuidIfIndex = (INT4) pPoolInfo->u4DuidIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolOptionTableNextIndex
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolOptionTableNextIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolOptionTableNextIndex (UINT4 u4FsDhcp6SrvPoolIndex,
                                          UINT4
                                          *pu4RetValFsDhcp6SrvPoolOptionTableNextIndex)
{
    if (D6SrOptionGetFreeIndex (u4FsDhcp6SrvPoolIndex,
                                pu4RetValFsDhcp6SrvPoolOptionTableNextIndex)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvPoolRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
retValFsDhcp6SrvPoolRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvPoolRowStatus (UINT4 u4FsDhcp6SrvPoolIndex,
                               INT4 *pi4RetValFsDhcp6SrvPoolRowStatus)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        *pi4RetValFsDhcp6SrvPoolRowStatus = (INT4) pPoolInfo->u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvPoolName
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
setValFsDhcp6SrvPoolName
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvPoolName (UINT4 u4FsDhcp6SrvPoolIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsDhcp6SrvPoolName)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        pPoolInfo->u1NameLength = (UINT1) pSetValFsDhcp6SrvPoolName->i4_Length;
        MEMSET (pPoolInfo->au1Name, 0, pPoolInfo->u1NameLength);
        MEMCPY (pPoolInfo->au1Name, pSetValFsDhcp6SrvPoolName->pu1_OctetList,
                pPoolInfo->u1NameLength);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvPoolPreference
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
setValFsDhcp6SrvPoolPreference
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvPoolPreference (UINT4 u4FsDhcp6SrvPoolIndex,
                                INT4 i4SetValFsDhcp6SrvPoolPreference)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        pPoolInfo->u2Preference = (UINT2) i4SetValFsDhcp6SrvPoolPreference;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvPoolDuidType
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
setValFsDhcp6SrvPoolDuidType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvPoolDuidType (UINT4 u4FsDhcp6SrvPoolIndex,
                              INT4 i4SetValFsDhcp6SrvPoolDuidType)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        pPoolInfo->u1DuidType = (UINT1) i4SetValFsDhcp6SrvPoolDuidType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvPoolDuidIfIndex
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
setValFsDhcp6SrvPoolDuidIfIndex
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvPoolDuidIfIndex (UINT4 u4FsDhcp6SrvPoolIndex,
                                 INT4 i4SetValFsDhcp6SrvPoolDuidIfIndex)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo != NULL)
    {
        pPoolInfo->u4DuidIfIndex = (UINT4) i4SetValFsDhcp6SrvPoolDuidIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvPoolRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
setValFsDhcp6SrvPoolRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvPoolRowStatus (UINT4 u4FsDhcp6SrvPoolIndex,
                               INT4 i4SetValFsDhcp6SrvPoolRowStatus)
{
    tDhcp6SrvPoolInfo  *pPoolInfo;

    switch (i4SetValFsDhcp6SrvPoolRowStatus)
    {
        case CREATE_AND_WAIT:

            pPoolInfo = D6SrPoolCreateNode ();
            if (pPoolInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more pools can be created \r\n");
                return SNMP_FAILURE;
            }
            pPoolInfo->u4PoolIndex = u4FsDhcp6SrvPoolIndex;
            pPoolInfo->u1DuidType = DHCP6_SRV_DUID_LLT;
            pPoolInfo->u1RowStatus = NOT_READY;
            if (D6SrPoolAddNode (pPoolInfo) != OSIX_SUCCESS)
            {
                D6SrPoolDelNode (pPoolInfo);
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

            if (pPoolInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            /* Update Server Id */
            if (D6SrUtlUpdateServerId (pPoolInfo) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pPoolInfo->u1RowStatus = (UINT1) i4SetValFsDhcp6SrvPoolRowStatus;
            break;

        case DESTROY:

            pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

            if (pPoolInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrPoolDelNode (pPoolInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvPoolName
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
testValFsDhcp6SrvPoolName
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvPoolName (UINT4 *pu4ErrorCode,
                             UINT4 u4FsDhcp6SrvPoolIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsDhcp6SrvPoolName)
{
    if (D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Port Index" "\r\n");
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6SrvPoolName->i4_Length < 0 ||
        pTestValFsDhcp6SrvPoolName->i4_Length > DHCP6_SRV_POOL_NAME_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6SrvPoolName->pu1_OctetList == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvPoolPreference
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
testValFsDhcp6SrvPoolPreference
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvPoolPreference (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsDhcp6SrvPoolIndex,
                                   INT4 i4TestValFsDhcp6SrvPoolPreference)
{
    if (D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Port Index" "\r\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDhcp6SrvPoolPreference < DHCP6_SRV_POOL_PREFERENCE_MIN) ||
        (i4TestValFsDhcp6SrvPoolPreference > DHCP6_SRV_POOL_PREFERENCE_MAX))
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                       "Input value is wrong" "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvPoolDuidType
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
testValFsDhcp6SrvPoolDuidType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvPoolDuidType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsDhcp6SrvPoolIndex,
                                 INT4 i4TestValFsDhcp6SrvPoolDuidType)
{
    if (D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Port Index" "\r\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDhcp6SrvPoolDuidType != DHCP6_SRV_DUID_LLT)
        && (i4TestValFsDhcp6SrvPoolDuidType != DHCP6_SRV_DUID_EN)
        && (i4TestValFsDhcp6SrvPoolDuidType != DHCP6_SRV_DUID_LL))
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                       "Input value is wrong" "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvPoolDuidIfIndex
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
testValFsDhcp6SrvPoolDuidIfIndex
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvPoolDuidIfIndex (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsDhcp6SrvPoolIndex,
                                    INT4 i4TestValFsDhcp6SrvPoolDuidIfIndex)
{
    if (D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Port Index" "\r\n");
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6SrvPoolDuidIfIndex < DHCP6_SRV_MIN_INTERFACES
        || i4TestValFsDhcp6SrvPoolDuidIfIndex > DHCP6_SRV_MAX_INTERFACES)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                       "Input value is wrong" "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvPoolRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex

The Object 
testValFsDhcp6SrvPoolRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvPoolRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsDhcp6SrvPoolIndex,
                                  INT4 i4TestValFsDhcp6SrvPoolRowStatus)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;
    tDhcp6SrvOptionInfo OptionInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvPoolTable (u4FsDhcp6SrvPoolIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvPoolRowStatus == DESTROY)
        {
            /* Check if any option is present in this table */
            pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);
            MEMSET (&OptionInfo, 0x00, sizeof (tDhcp6SrvOptionInfo));
            OptionInfo.pPool = pPoolInfo;
            OptionInfo.u4OptionIndex = 0;
            if (D6SrOptionGetNextNode (&OptionInfo) != NULL)
            {
                i1RetVal = SNMP_FAILURE;;
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }
        }
        else if (i4TestValFsDhcp6SrvPoolRowStatus == ACTIVE ||
                 i4TestValFsDhcp6SrvPoolRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvPoolRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvPoolTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvPoolTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6IncludePrefixTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6IncludePrefixTable
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvIncludePrefixTable (INT4
                                                      i4FsDhcp6SrvIncludePrefixContextId,
                                                      INT4
                                                      i4FsDhcp6SrvIncludePrefixIndex)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6IncludePrefixTable
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable (INT4
                                              *pi4FsDhcp6SrvIncludePrefixContextId,
                                              INT4
                                              *pi4FsDhcp6SrvIncludePrefixIndex)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetFirstNode ();
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsDhcp6SrvIncludePrefixContextId = pInPrefixInfo->u4ContextId;
    *pi4FsDhcp6SrvIncludePrefixIndex = pInPrefixInfo->u4InPrefixIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6IncludePrefixTable
Input       :  The Indices
FsDhcp6IncludePrefixContextId
nextFsDhcp6IncludePrefixContextId
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvIncludePrefixTable (INT4
                                             i4FsDhcp6SrvIncludePrefixContextId,
                                             INT4
                                             *pi4NextFsDhcp6SrvIncludePrefixContextId,
                                             INT4
                                             i4FsDhcp6SrvIncludePrefixIndex,
                                             INT4
                                             *pi4NextFsDhcp6SrvIncludePrefixIndex)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    tDhcp6SrvInPrefixInfo *pNextInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextInPrefixInfo = D6SrInPrefixGetNextNode (pInPrefixInfo);
    if (pNextInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsDhcp6SrvIncludePrefixContextId = pNextInPrefixInfo->u4ContextId;
    *pi4NextFsDhcp6SrvIncludePrefixIndex = pNextInPrefixInfo->u4InPrefixIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsDhcp6IncludePrefix
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
retValFsDhcp6IncludePrefix
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIncludePrefix (INT4 i4FsDhcp6SrvIncludePrefixContextId,
                               INT4 i4FsDhcp6SrvIncludePrefixIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsDhcp6SrvIncludePrefix)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsDhcp6SrvIncludePrefix->i4_Length = pInPrefixInfo->u1AddressLength;
    MEMCPY (pRetValFsDhcp6SrvIncludePrefix->pu1_OctetList,
            pInPrefixInfo->au1Address, pInPrefixInfo->u1AddressLength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6IncludePrefixPool
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
retValFsDhcp6IncludePrefixPool
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIncludePrefixPool (INT4 i4FsDhcp6SrvIncludePrefixContextId,
                                   INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                   INT4 *pi4RetValFsDhcp6SrvIncludePrefixPool)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pInPrefixInfo->pPool == NULL)
    {
        *pi4RetValFsDhcp6SrvIncludePrefixPool = 0;
    }
    else
    {
        *pi4RetValFsDhcp6SrvIncludePrefixPool =
            pInPrefixInfo->pPool->u4PoolIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6IncludePrefixRowStatus
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
retValFsDhcp6IncludePrefixRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIncludePrefixRowStatus (INT4 i4FsDhcp6SrvIncludePrefixContextId,
                                        INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                        INT4
                                        *pi4RetValFsDhcp6SrvIncludePrefixRowStatus)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvIncludePrefixRowStatus = pInPrefixInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6IncludePrefix
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
setValFsDhcp6IncludePrefix
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvIncludePrefix (INT4 i4FsDhcp6SrvIncludePrefixContextId,
                               INT4 i4FsDhcp6SrvIncludePrefixIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsDhcp6SrvIncludePrefix)
{

    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pInPrefixInfo->u1AddressLength =
        (UINT1) pSetValFsDhcp6SrvIncludePrefix->i4_Length;
    MEMCPY (pInPrefixInfo->au1Address,
            pSetValFsDhcp6SrvIncludePrefix->pu1_OctetList,
            pInPrefixInfo->u1AddressLength);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDhcp6IncludePrefixPool
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
setValFsDhcp6IncludePrefixPool
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvIncludePrefixPool (INT4 i4FsDhcp6SrvIncludePrefixContextId,
                                   INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                   INT4 i4SetValFsDhcp6SrvIncludePrefixPool)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValFsDhcp6SrvIncludePrefixPool == 0)
    {
        pInPrefixInfo = NULL;
        return SNMP_SUCCESS;
    }
    pPoolInfo = D6SrPoolGetNode (i4SetValFsDhcp6SrvIncludePrefixPool);

    if (pPoolInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pInPrefixInfo->pPool = pPoolInfo;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDhcp6IncludePrefixRowStatus
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
setValFsDhcp6IncludePrefixRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvIncludePrefixRowStatus (INT4 i4FsDhcp6SrvIncludePrefixContextId,
                                        INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                        INT4
                                        i4SetValFsDhcp6SrvIncludePrefixRowStatus)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo;

    switch (i4SetValFsDhcp6SrvIncludePrefixRowStatus)
    {
        case CREATE_AND_WAIT:

            pInPrefixInfo = D6SrInPrefixCreateNode ();
            if (pInPrefixInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more pools can be created \r\n");
                return SNMP_FAILURE;
            }
            pInPrefixInfo->u4ContextId = i4FsDhcp6SrvIncludePrefixContextId;
            pInPrefixInfo->u4InPrefixIndex = i4FsDhcp6SrvIncludePrefixIndex;
            pInPrefixInfo->u1RowStatus = NOT_READY;
            if (D6SrInPrefixAddNode (pInPrefixInfo) != OSIX_SUCCESS)
            {
                D6SrInPrefixDelNode (pInPrefixInfo);
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:

            pInPrefixInfo =
                D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                     i4FsDhcp6SrvIncludePrefixIndex);
            if (pInPrefixInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }

            pInPrefixInfo->u1RowStatus =
                (UINT1) i4SetValFsDhcp6SrvIncludePrefixRowStatus;
            break;

        case DESTROY:
            pInPrefixInfo =
                D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                     i4FsDhcp6SrvIncludePrefixIndex);
            if (pInPrefixInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrInPrefixDelNode (pInPrefixInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDhcp6IncludePrefix
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
testValFsDhcp6IncludePrefix
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvIncludePrefix (UINT4 *pu4ErrorCode,
                                  INT4 i4FsDhcp6SrvIncludePrefixContextId,
                                  INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsDhcp6SrvIncludePrefix)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValFsDhcp6SrvIncludePrefix->i4_Length == 0)
        || (pTestValFsDhcp6SrvIncludePrefix->pu1_OctetList == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6IncludePrefixPool
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
testValFsDhcp6IncludePrefixPool
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvIncludePrefixPool (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDhcp6SrvIncludePrefixContextId,
                                      INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                      INT4 i4TestValFsDhcp6SrvIncludePrefixPool)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;
    pInPrefixInfo = D6SrInPrefixGetNode (i4FsDhcp6SrvIncludePrefixContextId,
                                         i4FsDhcp6SrvIncludePrefixIndex);
    if (pInPrefixInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPoolInfo = D6SrPoolGetNode (i4TestValFsDhcp6SrvIncludePrefixPool);
    if (pPoolInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6IncludePrefixRowStatus
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex

The Object 
testValFsDhcp6IncludePrefixRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4FsDhcp6SrvIncludePrefixContextId,
                                           INT4 i4FsDhcp6SrvIncludePrefixIndex,
                                           INT4
                                           i4TestValFsDhcp6SrvIncludePrefixRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvIncludePrefixTable
        (i4FsDhcp6SrvIncludePrefixContextId, i4FsDhcp6SrvIncludePrefixIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvIncludePrefixRowStatus == ACTIVE ||
            i4TestValFsDhcp6SrvIncludePrefixRowStatus == DESTROY ||
            i4TestValFsDhcp6SrvIncludePrefixRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvIncludePrefixRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6IncludePrefixTable
Input       :  The Indices
FsDhcp6IncludePrefixContextId
FsDhcp6IncludePrefixIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvIncludePrefixTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIncludePrefixTableNextIndex
Input       :  The Indices

The Object 
retValFsDhcp6SrvIncludePrefixTableNextIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIncludePrefixTableNextIndex (UINT4
                                             *pu4RetValFsDhcp6SrvIncludePrefixTableNextIndex)
{
    if (D6SrInPrefixGetFreeIndex
        (pu4RetValFsDhcp6SrvIncludePrefixTableNextIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsDhcp6SrvOptionTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvOptionTable (UINT4 u4FsDhcp6SrvPoolIndex,
                                               UINT4 u4FsDhcp6SrvOptionIndex)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvOptionTable (UINT4 *pu4FsDhcp6SrvPoolIndex,
                                       UINT4 *pu4FsDhcp6SrvOptionIndex)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo = D6SrOptionGetFirstNode ();

    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4FsDhcp6SrvPoolIndex = pOptionInfo->pPool->u4PoolIndex;
    *pu4FsDhcp6SrvOptionIndex = pOptionInfo->u4OptionIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
nextFsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
nextFsDhcp6SrvOptionIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvOptionTable (UINT4 u4FsDhcp6SrvPoolIndex,
                                      UINT4 *pu4NextFsDhcp6SrvPoolIndex,
                                      UINT4 u4FsDhcp6SrvOptionIndex,
                                      UINT4 *pu4NextFsDhcp6SrvOptionIndex)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    tDhcp6SrvOptionInfo *pNextOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextOptionInfo = D6SrOptionGetNextNode (pOptionInfo);
    if (pNextOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDhcp6SrvPoolIndex = pNextOptionInfo->pPool->u4PoolIndex;
    *pu4NextFsDhcp6SrvOptionIndex = pNextOptionInfo->u4OptionIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvOptionType
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
retValFsDhcp6SrvOptionType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvOptionType (UINT4 u4FsDhcp6SrvPoolIndex,
                            UINT4 u4FsDhcp6SrvOptionIndex,
                            INT4 *pi4RetValFsDhcp6SrvOptionType)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvOptionType = pOptionInfo->u2Type;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvOptionLength
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
retValFsDhcp6SrvOptionLength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvOptionLength (UINT4 u4FsDhcp6SrvPoolIndex,
                              UINT4 u4FsDhcp6SrvOptionIndex,
                              INT4 *pi4RetValFsDhcp6SrvOptionLength)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvOptionLength = pOptionInfo->u2Length;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvOptionValue
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
retValFsDhcp6SrvOptionValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvOptionValue (UINT4 u4FsDhcp6SrvPoolIndex,
                             UINT4 u4FsDhcp6SrvOptionIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsDhcp6SrvOptionValue)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pOptionInfo->u2Length != 0) && (pOptionInfo->pu1Value != NULL))
    {
        pRetValFsDhcp6SrvOptionValue->i4_Length = pOptionInfo->u2Length;
        MEMCPY (pRetValFsDhcp6SrvOptionValue->pu1_OctetList,
                pOptionInfo->pu1Value, pOptionInfo->u2Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvOptionPreference
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
retValFsDhcp6SrvOptionPreference
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvOptionPreference (UINT4 u4FsDhcp6SrvPoolIndex,
                                  UINT4 u4FsDhcp6SrvOptionIndex,
                                  INT4 *pi4RetValFsDhcp6SrvOptionPreference)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvOptionPreference = pOptionInfo->u2Preference;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvOptionRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
retValFsDhcp6SrvOptionRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvOptionRowStatus (UINT4 u4FsDhcp6SrvPoolIndex,
                                 UINT4 u4FsDhcp6SrvOptionIndex,
                                 INT4 *pi4RetValFsDhcp6SrvOptionRowStatus)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvOptionRowStatus = pOptionInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvOptionType
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
setValFsDhcp6SrvOptionType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvOptionType (UINT4 u4FsDhcp6SrvPoolIndex,
                            UINT4 u4FsDhcp6SrvOptionIndex,
                            INT4 i4SetValFsDhcp6SrvOptionType)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pOptionInfo->u2Type = (UINT2) i4SetValFsDhcp6SrvOptionType;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvOptionLength
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
setValFsDhcp6SrvOptionLength
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvOptionLength (UINT4 u4FsDhcp6SrvPoolIndex,
                              UINT4 u4FsDhcp6SrvOptionIndex,
                              INT4 i4SetValFsDhcp6SrvOptionLength)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pOptionInfo->u2Length = (UINT2) i4SetValFsDhcp6SrvOptionLength;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvOptionValue
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
setValFsDhcp6SrvOptionValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvOptionValue (UINT4 u4FsDhcp6SrvPoolIndex,
                             UINT4 u4FsDhcp6SrvOptionIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsDhcp6SrvOptionValue)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Check is user wants to clear the value */
    if (pSetValFsDhcp6SrvOptionValue->i4_Length == 0)
    {
        if ((pOptionInfo->pu1Value != NULL))
        {
            DHCP6_SRV_BUDDY_FREE (pOptionInfo->pu1Value);
            pOptionInfo->pu1Value = NULL;
        }
        return SNMP_SUCCESS;
    }
    if (pOptionInfo->pu1Value != NULL)
    {
        DHCP6_SRV_BUDDY_FREE (pOptionInfo->pu1Value);
        pOptionInfo->pu1Value = NULL;
    }
    pOptionInfo->pu1Value = DHCP6_SRV_BUDDY_ALLOC (pOptionInfo->u2Length);
    MEMSET (pOptionInfo->pu1Value, 0, pOptionInfo->u2Length);
    MEMCPY (pOptionInfo->pu1Value, pSetValFsDhcp6SrvOptionValue->pu1_OctetList,
            pOptionInfo->u2Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvOptionPreference
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
setValFsDhcp6SrvOptionPreference
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvOptionPreference (UINT4 u4FsDhcp6SrvPoolIndex,
                                  UINT4 u4FsDhcp6SrvOptionIndex,
                                  INT4 i4SetValFsDhcp6SrvOptionPreference)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);
    if (pOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pOptionInfo->u2Preference = (UINT2) i4SetValFsDhcp6SrvOptionPreference;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvOptionRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
setValFsDhcp6SrvOptionRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvOptionRowStatus (UINT4 u4FsDhcp6SrvPoolIndex,
                                 UINT4 u4FsDhcp6SrvOptionIndex,
                                 INT4 i4SetValFsDhcp6SrvOptionRowStatus)
{
    tDhcp6SrvPoolInfo  *pPoolInfo;
    tDhcp6SrvOptionInfo *pOptionInfo;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                       "Entry is not yet created \r\n");
        return SNMP_FAILURE;
    }

    switch (i4SetValFsDhcp6SrvOptionRowStatus)
    {
        case CREATE_AND_WAIT:

            pOptionInfo = D6SrOptionCreateNode ();
            if (pOptionInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more Option can be created \r\n");
                return SNMP_FAILURE;
            }
            pOptionInfo->u4OptionIndex = u4FsDhcp6SrvOptionIndex;
            pOptionInfo->pPool = pPoolInfo;
            pOptionInfo->u1RowStatus = NOT_READY;
            if (D6SrOptionAddNode (pOptionInfo) != OSIX_SUCCESS)
            {
                D6SrOptionDelNode (pOptionInfo);
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case ACTIVE:
            pOptionInfo = D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                             u4FsDhcp6SrvOptionIndex);
            if (pOptionInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            DHCP6_SRV_TRC (MGMT_TRC, "Entry created \r\n");
            pOptionInfo->u1RowStatus =
                (UINT1) i4SetValFsDhcp6SrvOptionRowStatus;
            break;
        case DESTROY:

            pOptionInfo = D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                             u4FsDhcp6SrvOptionIndex);

            if (pOptionInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrOptionDelNode (pOptionInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvOptionType
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
testValFsDhcp6SrvOptionType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvOptionType (UINT4 *pu4ErrorCode,
                               UINT4 u4FsDhcp6SrvPoolIndex,
                               UINT4 u4FsDhcp6SrvOptionIndex,
                               INT4 i4TestValFsDhcp6SrvOptionType)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;

    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);

    if (pOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Option Index \r\n");
        return SNMP_FAILURE;
    }
    if (i4TestValFsDhcp6SrvOptionType < DHCP6_SRV_OPTION_TYPE_MIN)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    /*When Option type is Refresh TLV and if it is created then return Failure */
    pOptionInfo = D6SrOptionGetFirstNode ();

    while (pOptionInfo != NULL)
    {
        if ((pOptionInfo->pPool->u4PoolIndex == u4FsDhcp6SrvPoolIndex) &&
            (i4TestValFsDhcp6SrvOptionType == DHCP6_SRV_REFRESH_TIMER_TYPE) &&
            (pOptionInfo->u2Type == DHCP6_SRV_REFRESH_TIMER_TYPE))
        {
            return SNMP_FAILURE;
        }
        pOptionInfo = D6SrOptionGetNextNode (pOptionInfo);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvOptionLength
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
testValFsDhcp6SrvOptionLength
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvOptionLength (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsDhcp6SrvPoolIndex,
                                 UINT4 u4FsDhcp6SrvOptionIndex,
                                 INT4 i4TestValFsDhcp6SrvOptionLength)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;

    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);

    if (pOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Option Index \r\n");
        return SNMP_FAILURE;
    }
    if ((pOptionInfo->u2Type == DHCP6_SRV_ORO_DNS_SERVERS) ||
        (pOptionInfo->u2Type == DHCP6_SRV_ORO_SIP_SERVER_A))
    {
        if (i4TestValFsDhcp6SrvOptionLength % 16 != 0)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Length \r\n");
            return SNMP_FAILURE;
        }
    }
    if (i4TestValFsDhcp6SrvOptionLength < DHCP6_SRV_OPTION_LENGTH_MIN ||
        i4TestValFsDhcp6SrvOptionLength > DHCP6_SRV_OPTION_LENGTH_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (pOptionInfo->u2Type == DHCP6_SRV_REFRESH_TIMER_TYPE)
    {
        if (i4TestValFsDhcp6SrvOptionLength != DHCP6_SRV_REFRESH_TIMER_SIZE)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                           "Input value is wrong\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            CLI_SET_ERR (DHCP6_SRV_CLI_INFO_REFRESH_INV_LEN);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvOptionValue
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
testValFsDhcp6SrvOptionValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvOptionValue (UINT4 *pu4ErrorCode,
                                UINT4 u4FsDhcp6SrvPoolIndex,
                                UINT4 u4FsDhcp6SrvOptionIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsDhcp6SrvOptionValue)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    UINT1              *pu1Value = NULL;
    UINT4               u4RefreshValue = 0;

    /* User can also pass zero length octect list to delete the value. */
    if (pTestValFsDhcp6SrvOptionValue->i4_Length < 0 ||
        pTestValFsDhcp6SrvOptionValue->i4_Length > DHCP6_SRV_OPTION_LENGTH_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);

    if (pOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Option Index \r\n");
        return SNMP_FAILURE;
    }
    if (pOptionInfo->u2Length == 0)
    {
        return SNMP_FAILURE;
    }
    if (pOptionInfo->u2Type == DHCP6_SRV_REFRESH_TIMER_TYPE)
    {
        pu1Value = pTestValFsDhcp6SrvOptionValue->pu1_OctetList;
        DHCP6_SRV_GET_4BYTE (u4RefreshValue, pu1Value);
        if ((u4RefreshValue < DHCP6_SRV_MIN_REFRESH_TIME) ||
            (u4RefreshValue > DHCP6_SRV_MAX_REFRESH_TIME))
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                           "Input value is wrong\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            CLI_SET_ERR (DHCP6_SRV_CLI_INFO_REFRESH_INV_VAL);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvOptionPreference
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
testValFsDhcp6SrvOptionPreference
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvOptionPreference (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsDhcp6SrvPoolIndex,
                                     UINT4 u4FsDhcp6SrvOptionIndex,
                                     INT4 i4TestValFsDhcp6SrvOptionPreference)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;

    pOptionInfo =
        D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex, u4FsDhcp6SrvOptionIndex);

    if (pOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Option Index \r\n");
        return SNMP_FAILURE;
    }

    if (((pOptionInfo->u2Type != (UINT2) DHCP6_SRV_ORO_DOMAIN_LIST) ||
         (pOptionInfo->u2Type != (UINT2) DHCP6_SRV_ORO_SIP_SERVER_A)) &&
        (i4TestValFsDhcp6SrvOptionPreference > 1))
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                       "Input value is wrong for the Option Type\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6SrvOptionPreference < DHCP6_SRV_OPTION_PREFERENCE_MIN ||
        i4TestValFsDhcp6SrvOptionPreference > DHCP6_SRV_OPTION_PREFERENCE_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvOptionRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex

The Object 
testValFsDhcp6SrvOptionRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvOptionRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsDhcp6SrvPoolIndex,
                                    UINT4 u4FsDhcp6SrvOptionIndex,
                                    INT4 i4TestValFsDhcp6SrvOptionRowStatus)
{
    tDhcp6SrvOptionInfo *pOptionInfo = NULL;
    tDhcp6SrvSubOptionInfo SubOptionInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvOptionTable (u4FsDhcp6SrvPoolIndex,
                                                       u4FsDhcp6SrvOptionIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvOptionRowStatus == DESTROY)
        {
            /* Check if any suboption is created within this option */
            pOptionInfo =
                D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                   u4FsDhcp6SrvOptionIndex);
            MEMSET (&SubOptionInfo, 0x00, sizeof (tDhcp6SrvSubOptionInfo));
            SubOptionInfo.pOption = pOptionInfo;
            SubOptionInfo.u2Type = 0;
            if (D6SrSubOptionGetNextNode (&SubOptionInfo) != NULL)
            {
                i1RetVal = SNMP_FAILURE;
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }
        }
        else if (i4TestValFsDhcp6SrvOptionRowStatus == ACTIVE ||
                 i4TestValFsDhcp6SrvOptionRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvOptionRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvOptionTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6SrvSubOptionTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvSubOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvSubOptionTable (UINT4 u4FsDhcp6SrvPoolIndex,
                                                  UINT4 u4FsDhcp6SrvOptionIndex,
                                                  UINT4
                                                  u4FsDhcp6SrvSubOptionType)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvSubOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvSubOptionTable (UINT4 *pu4FsDhcp6SrvPoolIndex,
                                          UINT4 *pu4FsDhcp6SrvOptionIndex,
                                          UINT4 *pu4FsDhcp6SrvSubOptionType)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetFirstNode ();
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4FsDhcp6SrvPoolIndex = pSubOptionInfo->pOption->pPool->u4PoolIndex;
    *pu4FsDhcp6SrvOptionIndex = pSubOptionInfo->pOption->u4OptionIndex;
    *pu4FsDhcp6SrvSubOptionType = pSubOptionInfo->u2Type;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvSubOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
nextFsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
nextFsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType
nextFsDhcp6SrvSubOptionType
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvSubOptionTable (UINT4 u4FsDhcp6SrvPoolIndex,
                                         UINT4 *pu4NextFsDhcp6SrvPoolIndex,
                                         UINT4 u4FsDhcp6SrvOptionIndex,
                                         UINT4 *pu4NextFsDhcp6SrvOptionIndex,
                                         UINT4 u4FsDhcp6SrvSubOptionType,
                                         UINT4 *pu4NextFsDhcp6SrvSubOptionType)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    tDhcp6SrvSubOptionInfo *pNextSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextSubOptionInfo = D6SrSubOptionGetNextNode (pSubOptionInfo);
    if (pNextSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDhcp6SrvPoolIndex =
        pNextSubOptionInfo->pOption->pPool->u4PoolIndex;
    *pu4NextFsDhcp6SrvOptionIndex = pNextSubOptionInfo->pOption->u4OptionIndex;
    *pu4NextFsDhcp6SrvSubOptionType = pNextSubOptionInfo->u2Type;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvSubOptionLength
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
retValFsDhcp6SrvSubOptionLength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvSubOptionLength (UINT4 u4FsDhcp6SrvPoolIndex,
                                 UINT4 u4FsDhcp6SrvOptionIndex,
                                 UINT4 u4FsDhcp6SrvSubOptionType,
                                 INT4 *pi4RetValFsDhcp6SrvSubOptionLength)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvSubOptionLength = pSubOptionInfo->u2Length;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvSubOptionValue
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
retValFsDhcp6SrvSubOptionValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvSubOptionValue (UINT4 u4FsDhcp6SrvPoolIndex,
                                UINT4 u4FsDhcp6SrvOptionIndex,
                                UINT4 u4FsDhcp6SrvSubOptionType,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsDhcp6SrvSubOptionValue)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pSubOptionInfo->u2Length != 0) && (pSubOptionInfo->pu1Value != NULL))
    {
        pRetValFsDhcp6SrvSubOptionValue->i4_Length = pSubOptionInfo->u2Length;
        MEMCPY (pRetValFsDhcp6SrvSubOptionValue->pu1_OctetList,
                pSubOptionInfo->pu1Value,
                pRetValFsDhcp6SrvSubOptionValue->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvSubOptionRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
retValFsDhcp6SrvSubOptionRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvSubOptionRowStatus (UINT4 u4FsDhcp6SrvPoolIndex,
                                    UINT4 u4FsDhcp6SrvOptionIndex,
                                    UINT4 u4FsDhcp6SrvSubOptionType,
                                    INT4 *pi4RetValFsDhcp6SrvSubOptionRowStatus)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvSubOptionRowStatus = pSubOptionInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvSubOptionLength
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
setValFsDhcp6SrvSubOptionLength
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvSubOptionLength (UINT4 u4FsDhcp6SrvPoolIndex,
                                 UINT4 u4FsDhcp6SrvOptionIndex,
                                 UINT4 u4FsDhcp6SrvSubOptionType,
                                 INT4 i4SetValFsDhcp6SrvSubOptionLength)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pSubOptionInfo->u2Length = (UINT2) i4SetValFsDhcp6SrvSubOptionLength;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvSubOptionValue
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
setValFsDhcp6SrvSubOptionValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvSubOptionValue (UINT4 u4FsDhcp6SrvPoolIndex,
                                UINT4 u4FsDhcp6SrvOptionIndex,
                                UINT4 u4FsDhcp6SrvSubOptionType,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsDhcp6SrvSubOptionValue)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;
    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Check is user wants to clear the value */
    if (pSetValFsDhcp6SrvSubOptionValue->i4_Length == 0)
    {
        if ((pSubOptionInfo->pu1Value != NULL))
        {
            DHCP6_SRV_BUDDY_FREE (pSubOptionInfo->pu1Value);
            pSubOptionInfo->pu1Value = NULL;
        }
        return SNMP_SUCCESS;
    }
    /* Check is user wants to set a new value */
    if ((pSubOptionInfo->u2Length == 0) ||
        (pSubOptionInfo->u2Length < pSetValFsDhcp6SrvSubOptionValue->i4_Length))
    {
        return SNMP_FAILURE;
    }
    if (pSubOptionInfo->pu1Value != NULL)
    {
        DHCP6_SRV_BUDDY_FREE (pSubOptionInfo->pu1Value);
        pSubOptionInfo->pu1Value = NULL;
    }
    pSubOptionInfo->pu1Value = DHCP6_SRV_BUDDY_ALLOC (pSubOptionInfo->u2Length);
    if (pSubOptionInfo->pu1Value == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pSubOptionInfo->pu1Value, 0, pSubOptionInfo->u2Length);
    MEMCPY (pSubOptionInfo->pu1Value,
            pSetValFsDhcp6SrvSubOptionValue->pu1_OctetList,
            pSubOptionInfo->u2Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvSubOptionRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
setValFsDhcp6SrvSubOptionRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvSubOptionRowStatus (UINT4 u4FsDhcp6SrvPoolIndex,
                                    UINT4 u4FsDhcp6SrvOptionIndex,
                                    UINT4 u4FsDhcp6SrvSubOptionType,
                                    INT4 i4SetValFsDhcp6SrvSubOptionRowStatus)
{
    tDhcp6SrvPoolInfo  *pPoolInfo;
    tDhcp6SrvOptionInfo *pOptionInfo;
    tDhcp6SrvSubOptionInfo *pSubOptionInfo;

    pPoolInfo = D6SrPoolGetNode (u4FsDhcp6SrvPoolIndex);

    if (pPoolInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                       "Entry is not yet created \r\n");
        return SNMP_FAILURE;
    }

    pOptionInfo = D6SrOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                     u4FsDhcp6SrvOptionIndex);

    if (pOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                       "Entry is not yet created \r\n");
        return SNMP_FAILURE;
    }

    switch (i4SetValFsDhcp6SrvSubOptionRowStatus)
    {
        case CREATE_AND_WAIT:

            pSubOptionInfo = D6SrSubOptionCreateNode ();
            if (pSubOptionInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more Option can be created \r\n");
                return SNMP_FAILURE;
            }
            pSubOptionInfo->u2Type = (UINT2) u4FsDhcp6SrvSubOptionType;
            pSubOptionInfo->pOption = pOptionInfo;
            pSubOptionInfo->u1RowStatus = NOT_READY;
            if (D6SrSubOptionAddNode (pSubOptionInfo) != OSIX_SUCCESS)
            {
                D6SrSubOptionDelNode (pSubOptionInfo);
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                                   u4FsDhcp6SrvOptionIndex,
                                                   (UINT2)
                                                   u4FsDhcp6SrvSubOptionType);
            if (pSubOptionInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            pSubOptionInfo->u1RowStatus =
                (UINT1) i4SetValFsDhcp6SrvSubOptionRowStatus;
            break;
        case DESTROY:

            pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                                   u4FsDhcp6SrvOptionIndex,
                                                   (UINT2)
                                                   u4FsDhcp6SrvSubOptionType);
            if (pSubOptionInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrSubOptionDelNode (pSubOptionInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvSubOptionLength
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
testValFsDhcp6SrvSubOptionLength
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvSubOptionLength (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsDhcp6SrvPoolIndex,
                                    UINT4 u4FsDhcp6SrvOptionIndex,
                                    UINT4 u4FsDhcp6SrvSubOptionType,
                                    INT4 i4TestValFsDhcp6SrvSubOptionLength)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;

    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Option Index \r\n");
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6SrvSubOptionLength < DHCP6_SRV_OPTION_LENGTH_MIN ||
        i4TestValFsDhcp6SrvSubOptionLength > DHCP6_SRV_OPTION_LENGTH_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvSubOptionValue
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
testValFsDhcp6SrvSubOptionValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvSubOptionValue (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsDhcp6SrvPoolIndex,
                                   UINT4 u4FsDhcp6SrvOptionIndex,
                                   UINT4 u4FsDhcp6SrvSubOptionType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsDhcp6SrvSubOptionValue)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;

    pSubOptionInfo = D6SrSubOptionGetNode (u4FsDhcp6SrvPoolIndex,
                                           u4FsDhcp6SrvOptionIndex,
                                           (UINT2) u4FsDhcp6SrvSubOptionType);
    if (pSubOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Option Index \r\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6SrvSubOptionValue->i4_Length < 0 ||
        pTestValFsDhcp6SrvSubOptionValue->i4_Length >
        DHCP6_SRV_OPTION_LENGTH_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvSubOptionRowStatus
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType

The Object 
testValFsDhcp6SrvSubOptionRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvSubOptionRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsDhcp6SrvPoolIndex,
                                       UINT4 u4FsDhcp6SrvOptionIndex,
                                       UINT4 u4FsDhcp6SrvSubOptionType,
                                       INT4
                                       i4TestValFsDhcp6SrvSubOptionRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvSubOptionTable (u4FsDhcp6SrvPoolIndex,
                                                          u4FsDhcp6SrvOptionIndex,
                                                          u4FsDhcp6SrvSubOptionType);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvSubOptionRowStatus == ACTIVE ||
            i4TestValFsDhcp6SrvSubOptionRowStatus == DESTROY ||
            i4TestValFsDhcp6SrvSubOptionRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (u4FsDhcp6SrvSubOptionType >= 1)
        {
            if (i4TestValFsDhcp6SrvSubOptionRowStatus == CREATE_AND_WAIT)
            {
                i1RetVal = SNMP_SUCCESS;
            }
        }
    }
    if (i1RetVal != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvSubOptionTable
Input       :  The Indices
FsDhcp6SrvPoolIndex
FsDhcp6SrvOptionIndex
FsDhcp6SrvSubOptionType
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvSubOptionTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6SrvClientTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvClientTable
Input       :  The Indices
FsDhcp6SrvClientIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvClientTable (UINT4 u4FsDhcp6SrvClientIndex)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvClientTable
Input       :  The Indices
FsDhcp6SrvClientIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvClientTable (UINT4 *pu4FsDhcp6SrvClientIndex)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetFirstNode ();
    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4FsDhcp6SrvClientIndex = pClientInfo->u4ClientIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvClientTable
Input       :  The Indices
FsDhcp6SrvClientIndex
nextFsDhcp6SrvClientIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvClientTable (UINT4 u4FsDhcp6SrvClientIndex,
                                      UINT4 *pu4NextFsDhcp6SrvClientIndex)
{
    tDhcp6SrvClientInfo ClientInfo;
    tDhcp6SrvClientInfo *pNextClientInfo = NULL;
    MEMSET (&ClientInfo, 0x00, sizeof (tDhcp6SrvClientInfo));
    ClientInfo.u4ClientIndex = u4FsDhcp6SrvClientIndex;
    pNextClientInfo = D6SrClientGetNextNode (&ClientInfo);
    if (pNextClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDhcp6SrvClientIndex = pNextClientInfo->u4ClientIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvClientId
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
retValFsDhcp6SrvClientId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvClientId (UINT4 u4FsDhcp6SrvClientIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6SrvClientId)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsDhcp6SrvClientId->i4_Length = pClientInfo->u1ClientIdLength;
    MEMCPY (pRetValFsDhcp6SrvClientId->pu1_OctetList, pClientInfo->au1ClientId,
            pClientInfo->u1ClientIdLength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvClientIdType
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
retValFsDhcp6SrvClientIdType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvClientIdType (UINT4 u4FsDhcp6SrvClientIndex,
                              INT4 *pi4RetValFsDhcp6SrvClientIdType)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvClientIdType = pClientInfo->u1ClientIdType;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvClientRealm
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
retValFsDhcp6SrvClientRealm
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvClientRealm (UINT4 u4FsDhcp6SrvClientIndex,
                             UINT4 *pu4RetValFsDhcp6SrvClientRealm)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pClientInfo->pRealm == NULL)
    {
        *pu4RetValFsDhcp6SrvClientRealm = 0;
    }
    else
    {
        *pu4RetValFsDhcp6SrvClientRealm = pClientInfo->pRealm->u4RealmIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvClientRowStatus
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
retValFsDhcp6SrvClientRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvClientRowStatus (UINT4 u4FsDhcp6SrvClientIndex,
                                 INT4 *pi4RetValFsDhcp6SrvClientRowStatus)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvClientRowStatus = pClientInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvClientId
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
setValFsDhcp6SrvClientId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvClientId (UINT4 u4FsDhcp6SrvClientIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsDhcp6SrvClientId)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;

    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);

    if (pClientInfo != NULL)
    {
        pClientInfo->u1ClientIdLength =
            (UINT1) pSetValFsDhcp6SrvClientId->i4_Length;
        MEMCPY (pClientInfo->au1ClientId,
                pSetValFsDhcp6SrvClientId->pu1_OctetList,
                pClientInfo->u1ClientIdLength);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvClientIdType
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
setValFsDhcp6SrvClientIdType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvClientIdType (UINT4 u4FsDhcp6SrvClientIndex,
                              INT4 i4SetValFsDhcp6SrvClientIdType)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;

    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);

    if (pClientInfo != NULL)
    {
        pClientInfo->u1ClientIdType = (UINT1) i4SetValFsDhcp6SrvClientIdType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvClientRealm
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
setValFsDhcp6SrvClientRealm
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvClientRealm (UINT4 u4FsDhcp6SrvClientIndex,
                             UINT4 u4SetValFsDhcp6SrvClientRealm)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pClientInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);

    if (pClientInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (u4SetValFsDhcp6SrvClientRealm == 0)
    {
        pClientInfo->pRealm = NULL;
        return SNMP_SUCCESS;
    }
    pRealmInfo = D6SrRealmGetNode (u4SetValFsDhcp6SrvClientRealm);
    if (pRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pClientInfo->pRealm = pRealmInfo;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvClientRowStatus
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
setValFsDhcp6SrvClientRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvClientRowStatus (UINT4 u4FsDhcp6SrvClientIndex,
                                 INT4 i4SetValFsDhcp6SrvClientRowStatus)
{
    tDhcp6SrvClientInfo *pClntInfo;

    switch (i4SetValFsDhcp6SrvClientRowStatus)
    {
        case CREATE_AND_WAIT:

            pClntInfo = D6SrClientCreateNode ();
            if (pClntInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more pools can be created \r\n");
                return SNMP_FAILURE;
            }
            pClntInfo->u4ClientIndex = u4FsDhcp6SrvClientIndex;
            pClntInfo->u1RowStatus = NOT_READY;
            if (D6SrClientAddNode (pClntInfo) != OSIX_SUCCESS)
            {
                D6SrClientDelNode (pClntInfo);
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case ACTIVE:
            pClntInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
            if (pClntInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            pClntInfo->u1RowStatus = (UINT1) i4SetValFsDhcp6SrvClientRowStatus;
            break;
        case DESTROY:
            pClntInfo = D6SrClientGetNode (u4FsDhcp6SrvClientIndex);
            if (pClntInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrClientDelNode (pClntInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvClientId
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
testValFsDhcp6SrvClientId
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvClientId (UINT4 *pu4ErrorCode,
                             UINT4 u4FsDhcp6SrvClientIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsDhcp6SrvClientId)
{
    if (D6SrClientGetNode (u4FsDhcp6SrvClientIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Clnt Index" "\r\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6SrvClientId->i4_Length < DHCP6_SRV_DUID_SIZE_MIN ||
        pTestValFsDhcp6SrvClientId->i4_Length > DHCP6_SRV_DUID_SIZE_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6SrvClientId->pu1_OctetList == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvClientIdType
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
testValFsDhcp6SrvClientIdType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvClientIdType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsDhcp6SrvClientIndex,
                                 INT4 i4TestValFsDhcp6SrvClientIdType)
{
    if (D6SrClientGetNode (u4FsDhcp6SrvClientIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Clnt Index" "\r\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDhcp6SrvClientIdType != DHCP6_SRV_DUID_LLT)
        && (i4TestValFsDhcp6SrvClientIdType != DHCP6_SRV_DUID_EN)
        && (i4TestValFsDhcp6SrvClientIdType != DHCP6_SRV_DUID_LL))
    {
        DHCP6_SRV_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                       "Input value is wrong" "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvClientRealm
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
testValFsDhcp6SrvClientRealm
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvClientRealm (UINT4 *pu4ErrorCode,
                                UINT4 u4FsDhcp6SrvClientIndex,
                                UINT4 u4TestValFsDhcp6SrvClientRealm)
{
    if (D6SrClientGetNode (u4FsDhcp6SrvClientIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Clnt Index" "\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (D6SrRealmGetNode (u4TestValFsDhcp6SrvClientRealm) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvClientRowStatus
Input       :  The Indices
FsDhcp6SrvClientIndex

The Object 
testValFsDhcp6SrvClientRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvClientRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsDhcp6SrvClientIndex,
                                    INT4 i4TestValFsDhcp6SrvClientRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvClientTable (u4FsDhcp6SrvClientIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvClientRowStatus == ACTIVE ||
            i4TestValFsDhcp6SrvClientRowStatus == DESTROY ||
            i4TestValFsDhcp6SrvClientRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvClientRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvClientTable
Input       :  The Indices
FsDhcp6SrvClientIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvClientTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6SrvRealmTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvRealmTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvRealmTable (UINT4 u4FsDhcp6SrvRealmIndex)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);
    if (pRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvRealmTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvRealmTable (UINT4 *pu4FsDhcp6SrvRealmIndex)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetFirstNode ();
    if (pRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4FsDhcp6SrvRealmIndex = pRealmInfo->u4RealmIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvRealmTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
nextFsDhcp6SrvRealmIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvRealmTable (UINT4 u4FsDhcp6SrvRealmIndex,
                                     UINT4 *pu4NextFsDhcp6SrvRealmIndex)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    tDhcp6SrvRealmInfo *pNextRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);
    if (pRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextRealmInfo = D6SrRealmGetNextNode (pRealmInfo);
    if (pNextRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDhcp6SrvRealmIndex = pNextRealmInfo->u4RealmIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvRealmName
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
retValFsDhcp6SrvRealmName
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvRealmName (UINT4 u4FsDhcp6SrvRealmIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6SrvRealmName)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);
    if (pRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsDhcp6SrvRealmName->i4_Length = pRealmInfo->u1NameLength;
    MEMCPY (pRetValFsDhcp6SrvRealmName->pu1_OctetList, pRealmInfo->au1Name,
            pRealmInfo->u1NameLength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvRealmKeyTableNextIndex
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
retValFsDhcp6SrvRealmKeyTableNextIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvRealmKeyTableNextIndex (UINT4 u4FsDhcp6SrvRealmIndex,
                                        UINT4
                                        *pu4RetValFsDhcp6SrvRealmKeyTableNextIndex)
{
    if (D6SrKeyGetFreeIndex
        (u4FsDhcp6SrvRealmIndex,
         pu4RetValFsDhcp6SrvRealmKeyTableNextIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvRealmRowStatus
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
retValFsDhcp6SrvRealmRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvRealmRowStatus (UINT4 u4FsDhcp6SrvRealmIndex,
                                INT4 *pi4RetValFsDhcp6SrvRealmRowStatus)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);
    if (pRealmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvRealmRowStatus = pRealmInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvRealmName
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
setValFsDhcp6SrvRealmName
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvRealmName (UINT4 u4FsDhcp6SrvRealmIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsDhcp6SrvRealmName)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);

    if (pRealmInfo != NULL)
    {
        pRealmInfo->u1NameLength =
            (UINT1) pSetValFsDhcp6SrvRealmName->i4_Length;
        MEMCPY (pRealmInfo->au1Name, pSetValFsDhcp6SrvRealmName->pu1_OctetList,
                pRealmInfo->u1NameLength);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvRealmRowStatus
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
setValFsDhcp6SrvRealmRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvRealmRowStatus (UINT4 u4FsDhcp6SrvRealmIndex,
                                INT4 i4SetValFsDhcp6SrvRealmRowStatus)
{
    tDhcp6SrvRealmInfo *pRealmInfo;

    switch (i4SetValFsDhcp6SrvRealmRowStatus)
    {
        case CREATE_AND_WAIT:
            pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);

            if (pRealmInfo != NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is already created \r\n");
                return SNMP_FAILURE;
            }

            pRealmInfo = D6SrRealmCreateNode ();
            if (pRealmInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more pools can be created \r\n");
                return SNMP_FAILURE;
            }
            pRealmInfo->u4RealmIndex = u4FsDhcp6SrvRealmIndex;
            pRealmInfo->u1RowStatus = NOT_READY;
            if (D6SrRealmAddNode (pRealmInfo) != OSIX_SUCCESS)
            {
                D6SrRealmDelNode (pRealmInfo);
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);

            if (pRealmInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            pRealmInfo->u1RowStatus = (UINT1) i4SetValFsDhcp6SrvRealmRowStatus;
            break;
        case DESTROY:
            pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);

            if (pRealmInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrRealmDelNode (pRealmInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvRealmName
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
testValFsDhcp6SrvRealmName
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvRealmName (UINT4 *pu4ErrorCode,
                              UINT4 u4FsDhcp6SrvRealmIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsDhcp6SrvRealmName)
{
    if (D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Realm Index"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6SrvRealmName->i4_Length < DHCP6_SRV_REALM_NAME_MIN ||
        pTestValFsDhcp6SrvRealmName->i4_Length > DHCP6_SRV_REALM_NAME_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6SrvRealmName->pu1_OctetList == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvRealmRowStatus
Input       :  The Indices
FsDhcp6SrvRealmIndex

The Object 
testValFsDhcp6SrvRealmRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvRealmRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsDhcp6SrvRealmIndex,
                                   INT4 i4TestValFsDhcp6SrvRealmRowStatus)
{
    tDhcp6SrvRealmInfo *pRealmIno = NULL;
    tDhcp6SrvKeyInfo    KeyInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvRealmTable (u4FsDhcp6SrvRealmIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvRealmRowStatus == DESTROY)
        {
            /*Check if any key is configured within this realm */
            pRealmIno = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);
            MEMSET (&KeyInfo, 0x00, sizeof (tDhcp6SrvKeyInfo));
            KeyInfo.pRealm = pRealmIno;
            KeyInfo.u4KeyId = 0;
            if (D6SrKeyGetNextNode (&KeyInfo) != NULL)
            {
                i1RetVal = SNMP_FAILURE;
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }
        }
        else if (i4TestValFsDhcp6SrvRealmRowStatus == ACTIVE ||
                 i4TestValFsDhcp6SrvRealmRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvRealmRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvRealmTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvRealmTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6SrvKeyTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvKeyTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvKeyTable (UINT4 u4FsDhcp6SrvRealmIndex,
                                            UINT4 u4FsDhcp6SrvKeyIdentifier)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    pKeyInfo =
        D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex, u4FsDhcp6SrvKeyIdentifier);
    if (pKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvKeyTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDhcp6SrvKeyTable (UINT4 *pu4FsDhcp6SrvRealmIndex,
                                    UINT4 *pu4FsDhcp6SrvKeyIdentifier)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    pKeyInfo = D6SrKeyGetFirstNode ();
    if (pKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4FsDhcp6SrvRealmIndex = pKeyInfo->pRealm->u4RealmIndex;
    *pu4FsDhcp6SrvKeyIdentifier = pKeyInfo->u4KeyId;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvKeyTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
nextFsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier
nextFsDhcp6SrvKeyIdentifier
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvKeyTable (UINT4 u4FsDhcp6SrvRealmIndex,
                                   UINT4 *pu4NextFsDhcp6SrvRealmIndex,
                                   UINT4 u4FsDhcp6SrvKeyIdentifier,
                                   UINT4 *pu4NextFsDhcp6SrvKeyIdentifier)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    tDhcp6SrvKeyInfo   *pNextKeyInfo = NULL;
    pKeyInfo =
        D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex, u4FsDhcp6SrvKeyIdentifier);
    if (pKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextKeyInfo = D6SrKeyGetNextNode (pKeyInfo);
    if (pNextKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFsDhcp6SrvRealmIndex = pNextKeyInfo->pRealm->u4RealmIndex;
    *pu4NextFsDhcp6SrvKeyIdentifier = pNextKeyInfo->u4KeyId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvKey
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier

The Object 
retValFsDhcp6SrvKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvKey (UINT4 u4FsDhcp6SrvRealmIndex,
                     UINT4 u4FsDhcp6SrvKeyIdentifier,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6SrvKey)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    pKeyInfo =
        D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex, u4FsDhcp6SrvKeyIdentifier);
    if (pKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsDhcp6SrvKey->i4_Length = pKeyInfo->u1KeyLength;
    MEMCPY (pRetValFsDhcp6SrvKey->pu1_OctetList, pKeyInfo->au1Key,
            pKeyInfo->u1KeyLength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvKeyRowStatus
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier

The Object 
retValFsDhcp6SrvKeyRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvKeyRowStatus (UINT4 u4FsDhcp6SrvRealmIndex,
                              UINT4 u4FsDhcp6SrvKeyIdentifier,
                              INT4 *pi4RetValFsDhcp6SrvKeyRowStatus)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    pKeyInfo =
        D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex, u4FsDhcp6SrvKeyIdentifier);
    if (pKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvKeyRowStatus = pKeyInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvKey
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier

The Object 
setValFsDhcp6SrvKey
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvKey (UINT4 u4FsDhcp6SrvRealmIndex,
                     UINT4 u4FsDhcp6SrvKeyIdentifier,
                     tSNMP_OCTET_STRING_TYPE * pSetValFsDhcp6SrvKey)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    pKeyInfo =
        D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex, u4FsDhcp6SrvKeyIdentifier);
    if (pKeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pKeyInfo->u1KeyLength = (UINT1) pSetValFsDhcp6SrvKey->i4_Length;
    MEMSET (pKeyInfo->au1Key, 0, pKeyInfo->u1KeyLength);
    MEMCPY (pKeyInfo->au1Key, pSetValFsDhcp6SrvKey->pu1_OctetList,
            pKeyInfo->u1KeyLength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvKeyRowStatus
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier

The Object 
setValFsDhcp6SrvKeyRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvKeyRowStatus (UINT4 u4FsDhcp6SrvRealmIndex,
                              UINT4 u4FsDhcp6SrvKeyIdentifier,
                              INT4 i4SetValFsDhcp6SrvKeyRowStatus)
{
    tDhcp6SrvRealmInfo *pRealmInfo;
    tDhcp6SrvKeyInfo   *pKeyInfo;
    pRealmInfo = D6SrRealmGetNode (u4FsDhcp6SrvRealmIndex);
    if (pRealmInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                       "Entry is not yet created \r\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValFsDhcp6SrvKeyRowStatus)
    {
        case CREATE_AND_WAIT:
            pKeyInfo = D6SrKeyCreateNode ();
            if (pKeyInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more Option can be created \r\n");
                return SNMP_FAILURE;
            }
            pKeyInfo->pRealm = pRealmInfo;
            pKeyInfo->u4KeyId = u4FsDhcp6SrvKeyIdentifier;
            pKeyInfo->u1RowStatus = NOT_READY;
            if (D6SrKeyAddNode (pKeyInfo) != OSIX_SUCCESS)
            {
                D6SrKeyDelNode (pKeyInfo);
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case ACTIVE:

            pKeyInfo = D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex,
                                       u4FsDhcp6SrvKeyIdentifier);

            if (pKeyInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            pKeyInfo->u1RowStatus = (UINT1) i4SetValFsDhcp6SrvKeyRowStatus;
            break;
        case DESTROY:
            pKeyInfo = D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex,
                                       u4FsDhcp6SrvKeyIdentifier);

            if (pKeyInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrKeyDelNode (pKeyInfo);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvKey
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier

The Object 
testValFsDhcp6SrvKey
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvKey (UINT4 *pu4ErrorCode,
                        UINT4 u4FsDhcp6SrvRealmIndex,
                        UINT4 u4FsDhcp6SrvKeyIdentifier,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsDhcp6SrvKey)
{
    if (D6SrKeyGetNode (u4FsDhcp6SrvRealmIndex,
                        u4FsDhcp6SrvKeyIdentifier) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Realm Index"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6SrvKey->i4_Length < DHCP6_SRV_KEY_SIZE_MIN ||
        pTestValFsDhcp6SrvKey->i4_Length > DHCP6_SRV_KEY_SIZE_MAX)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong!"
                       "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6SrvKey->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Input value is wrong!"
                       "\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvKeyRowStatus
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier

The Object 
testValFsDhcp6SrvKeyRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvKeyRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsDhcp6SrvRealmIndex,
                                 UINT4 u4FsDhcp6SrvKeyIdentifier,
                                 INT4 i4TestValFsDhcp6SrvKeyRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6SrvKeyTable (u4FsDhcp6SrvRealmIndex,
                                                    u4FsDhcp6SrvKeyIdentifier);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvKeyRowStatus == ACTIVE ||
            i4TestValFsDhcp6SrvKeyRowStatus == DESTROY ||
            i4TestValFsDhcp6SrvKeyRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvKeyRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvKeyTable
Input       :  The Indices
FsDhcp6SrvRealmIndex
FsDhcp6SrvKeyIdentifier
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvKeyTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6SrvIfTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDhcp6SrvIfTable
Input       :  The Indices
FsDhcp6SrvIfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6SrvIfTable (INT4 i4FsDhcp6SrvIfIndex)
{
    if (D6SrIfGetNode (i4FsDhcp6SrvIfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDhcp6SrvIfTable
Input       :  The Indices
FsDhcp6SrvIfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6SrvIfTable (INT4 *pi4FsDhcp6SrvIfIndex)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetFirstNode ();
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsDhcp6SrvIfIndex = pIfInfo->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDhcp6SrvIfTable
Input       :  The Indices
FsDhcp6SrvIfIndex
nextFsDhcp6SrvIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6SrvIfTable (INT4 i4FsDhcp6SrvIfIndex,
                                  INT4 *pi4NextFsDhcp6SrvIfIndex)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    tDhcp6SrvIfInfo    *pNextIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pNextIfInfo = D6SrIfGetNextNode (pIfInfo);
    if (pNextIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsDhcp6SrvIfIndex = pNextIfInfo->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfPool
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfPool
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfPool (INT4 i4FsDhcp6SrvIfIndex,
                        INT4 *pi4RetValFsDhcp6SrvIfPool)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfInfo->pPool == NULL)
    {
        *pi4RetValFsDhcp6SrvIfPool = 0;
    }
    else
    {
        *pi4RetValFsDhcp6SrvIfPool = pIfInfo->pPool->u4PoolIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfInformIn
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfInformIn
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfInformIn (INT4 i4FsDhcp6SrvIfIndex,
                            UINT4 *pu4RetValFsDhcp6SrvIfInformIn)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6SrvIfInformIn = pIfInfo->u4InformIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfRelayForwIn
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfRelayForwIn
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfRelayForwIn (INT4 i4FsDhcp6SrvIfIndex,
                               UINT4 *pu4RetValFsDhcp6SrvIfRelayForwIn)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6SrvIfRelayForwIn = pIfInfo->u4RelayForwIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfReplyOut
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfReplyOut
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfReplyOut (INT4 i4FsDhcp6SrvIfIndex,
                            UINT4 *pu4RetValFsDhcp6SrvIfReplyOut)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6SrvIfReplyOut = pIfInfo->u4ReplyOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfRelayReplyOut
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfRelayReplyOut
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfRelayReplyOut (INT4 i4FsDhcp6SrvIfIndex,
                                 UINT4 *pu4RetValFsDhcp6SrvIfRelayReplyOut)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6SrvIfRelayReplyOut = pIfInfo->u4RelayReplyOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfInvalidPktIn
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfInvalidPktIn
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfInvalidPktIn (INT4 i4FsDhcp6SrvIfIndex,
                                UINT4 *pu4RetValFsDhcp6SrvIfInvalidPktIn)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6SrvIfInvalidPktIn = pIfInfo->u4InvalidPktIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfUnknownTlvType
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfUnknownTlvType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfUnknownTlvType (INT4 i4FsDhcp6SrvIfIndex,
                                  INT4 *pi4RetValFsDhcp6SrvIfUnknownTlvType)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvIfUnknownTlvType = pIfInfo->u2LastUnknownTlvType;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfHmacFailCount
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfHmacFailCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfHmacFailCount (INT4 i4FsDhcp6SrvIfIndex,
                                 UINT4 *pu4RetValFsDhcp6SrvIfHmacFailCount)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6SrvIfHmacFailCount = pIfInfo->u4HmacFailCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfCounterReset
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfCounterReset
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfCounterReset (INT4 i4FsDhcp6SrvIfIndex,
                                INT4 *pi4RetValFsDhcp6SrvIfCounterReset)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvIfCounterReset = DHCP6_SNMP_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDhcp6SrvIfRowStatus
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
retValFsDhcp6SrvIfRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDhcp6SrvIfRowStatus (INT4 i4FsDhcp6SrvIfIndex,
                             INT4 *pi4RetValFsDhcp6SrvIfRowStatus)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6SrvIfRowStatus = pIfInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvIfPool
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
setValFsDhcp6SrvIfPool
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvIfPool (INT4 i4FsDhcp6SrvIfIndex, INT4 i4SetValFsDhcp6SrvIfPool)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
    if (pIfInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Interface Does Not"
                       "exist!\r\n");
        return SNMP_FAILURE;
    }
    if (i4SetValFsDhcp6SrvIfPool == 0)
    {
        pIfInfo->pPool = NULL;
        return SNMP_SUCCESS;
    }
    pPoolInfo = D6SrPoolGetNode (i4SetValFsDhcp6SrvIfPool);
    if (pPoolInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfInfo->pPool = pPoolInfo;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvIfCounterReset
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
setValFsDhcp6SrvIfCounterReset
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvIfCounterReset (INT4 i4FsDhcp6SrvIfIndex,
                                INT4 i4SetValFsDhcp6SrvIfCounterReset)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;

    pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);

    if (pIfInfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Interface Does Not"
                       "exist!\r\n");
        return SNMP_FAILURE;
    }
    if (i4SetValFsDhcp6SrvIfCounterReset == OSIX_TRUE)
    {
        pIfInfo->u4InformIn = 0;
        pIfInfo->u4RelayForwIn = 0;
        pIfInfo->u4ReplyOut = 0;
        pIfInfo->u4RelayReplyOut = 0;
        pIfInfo->u4HmacFailCount = 0;
        pIfInfo->u4InvalidPktIn = 0;
        pIfInfo->u2LastUnknownTlvType = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDhcp6SrvIfRowStatus
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
setValFsDhcp6SrvIfRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDhcp6SrvIfRowStatus (INT4 i4FsDhcp6SrvIfIndex,
                             INT4 i4SetValFsDhcp6SrvIfRowStatus)
{
    tIp6Addr            McastAddr;
    tDhcp6SrvIfInfo    *pIfInfo;

    switch (i4SetValFsDhcp6SrvIfRowStatus)
    {
        case CREATE_AND_WAIT:

            pIfInfo = D6SrIfCreateNode ();
            if (pIfInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "No more pools can be created \r\n");
                return SNMP_FAILURE;
            }
            pIfInfo->u4IfIndex = (UINT4) i4FsDhcp6SrvIfIndex;
            pIfInfo->u1OperStatus = DHCP6_ENABLED;
            pIfInfo->u1RowStatus = NOT_READY;
            if (D6SrIfAddNode (pIfInfo) != OSIX_SUCCESS)
            {
                D6SrIfDelNode (pIfInfo);
                return SNMP_FAILURE;

            }

            /* Check if socket is created, if not then create */
            if (DHCP6_SRV_SOCKET_FD == -1)
            {
                if (D6SrSktSockInit () != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

            }

            INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &McastAddr);
            Dhcp6SrvJoinMcastGroup (DHCP6_SRV_SOCKET_FD, i4FsDhcp6SrvIfIndex,
                                    &McastAddr);
            INET_ATON6 (DHCP6_ALL_SERVER, &McastAddr);
            Dhcp6SrvJoinMcastGroup (DHCP6_SRV_SOCKET_FD, i4FsDhcp6SrvIfIndex,
                                    &McastAddr);

            break;
        case NOT_IN_SERVICE:
        case ACTIVE:
            pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
            if (pIfInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_FAILURE;
            }
            pIfInfo->u1RowStatus = (UINT1) i4SetValFsDhcp6SrvIfRowStatus;

            if ((pIfInfo->u1RowStatus == ACTIVE) && (DHCP6_SRV_SOCKET_FD == -1))
            {
                if (D6SrSktSockInit () != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;
        case DESTROY:
            pIfInfo = D6SrIfGetNode (i4FsDhcp6SrvIfIndex);
            if (pIfInfo == NULL)
            {
                DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                               "Entry is not yet created \r\n");
                return SNMP_SUCCESS;
            }
            D6SrIfDelNode (pIfInfo);
            /* If no interface is present in the relay then delete
             * the socket  */
            if (D6SrIfGetFirstNode () == NULL)
            {
                D6SrSktSockDeInit ();
            }
            INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &McastAddr);
            Dhcp6SrvLeaveMcastGroup (DHCP6_SRV_SOCKET_FD, i4FsDhcp6SrvIfIndex,
                                     &McastAddr);

            INET_ATON6 (DHCP6_ALL_SERVER, &McastAddr);
            Dhcp6SrvLeaveMcastGroup (DHCP6_SRV_SOCKET_FD, i4FsDhcp6SrvIfIndex,
                                     &McastAddr);
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvIfPool
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
testValFsDhcp6SrvIfPool
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvIfPool (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6SrvIfIndex,
                           INT4 i4TestValFsDhcp6SrvIfPool)
{
    if (D6SrIfGetNode (i4FsDhcp6SrvIfIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Interface Does Not"
                       "exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (D6SrPoolGetNode ((UINT4) i4TestValFsDhcp6SrvIfPool) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Invalid Port Index" "\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvIfCounterReset
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
testValFsDhcp6SrvIfCounterReset
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvIfCounterReset (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6SrvIfIndex,
                                   INT4 i4TestValFsDhcp6SrvIfCounterReset)
{
    if (D6SrIfGetNode (i4FsDhcp6SrvIfIndex) == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC, "Interface Does Not"
                       "exist!\r\n");
        return SNMP_FAILURE;
    }
    if (i4TestValFsDhcp6SrvIfCounterReset != OSIX_TRUE &&
        i4TestValFsDhcp6SrvIfCounterReset != OSIX_FALSE)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                       "Input value is" "wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDhcp6SrvIfRowStatus
Input       :  The Indices
FsDhcp6SrvIfIndex

The Object 
testValFsDhcp6SrvIfRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDhcp6SrvIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6SrvIfIndex,
                                INT4 i4TestValFsDhcp6SrvIfRowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhValidateIndexInstanceFsDhcp6SrvIfTable (i4FsDhcp6SrvIfIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6SrvIfRowStatus == ACTIVE ||
            i4TestValFsDhcp6SrvIfRowStatus == DESTROY ||
            i4TestValFsDhcp6SrvIfRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsDhcp6SrvIfRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDhcp6SrvIfTable
Input       :  The Indices
FsDhcp6SrvIfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDhcp6SrvIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
