/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srapi.c,v 1.9 2014/05/19 13:14:32 siva Exp $
 *
 * Description: This file contains the procedures called by other
 *              modules to access te functionality of DHCPv6 Server
 *
 **************************************************************************/

#include   "d6srinc.h"
#include   "d6srglob.h"

PRIVATE VOID        D6SrApiIfDelete (UINT4 u4IfIndex);
PRIVATE VOID        D6SrApiIfOperStat (UINT4 u4IfIndex, UINT1 u1Status);

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrApiIp6IfStatusNotify                                    */
/*                                                                           */
/* Description  : Whenever IPv6 senses changes in Iface configurations, it   */
/*                calls this function.                                       */
/*                                                                           */
/* Input        : pIp6HliParams : Pointer to tIp6HliParams                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6SrApiIp6IfStatusNotify (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tNetIpv6IfStatChange *pIfStatusChange = NULL;
    tDhcp6SrvQMsg      *pDhcp6QMsg = NULL;

    pIfStatusChange = &pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange;

    DHCP6_SRV_ALLOC_MEM_MSGQ (pDhcp6QMsg);

    if (NULL == pDhcp6QMsg)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC,
                       "D6SrApiIp6IfStatusNotify: Alloc Failure \r\n");
        return OSIX_FALSE;
    }

    pDhcp6QMsg->u4Command = DHCP6_SRV_IPV6_IF_STAT_CHG_EVENT;
    pDhcp6QMsg->unDhcp6SMsgType.dhcp6SIpIfStatParam.u4Index
        = pIfStatusChange->u4Index;
    pDhcp6QMsg->unDhcp6SMsgType.dhcp6SIpIfStatParam.u4Mask
        = pIfStatusChange->u4Mask;
    pDhcp6QMsg->unDhcp6SMsgType.dhcp6SIpIfStatParam.u4IfStat
        = pIfStatusChange->u4IfStat;
    pDhcp6QMsg->unDhcp6SMsgType.dhcp6SIpIfStatParam.u4OperStatus =
        pIfStatusChange->u4OperStatus;

    /* Enqueue the buffer to the DHCP6_SRV Task */
    if (OsixSendToQ ((UINT4) 0, DHCP6_SRV_QUEUE_NAME,
                     (tCRU_BUF_CHAIN_HEADER *) (VOID *) pDhcp6QMsg,
                     OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {

        DHCP6_SRV_FREE_MEM_MSGQ (pDhcp6QMsg);
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC, "D6SrApiIp6IfStatusNotify: Enqueu"
                       "To DHCP6 Failed \r\n");
        return OSIX_FALSE;
    }

    if (OsixSendEvent (SELF, DHCP6_SRV_TASK, DHCP6_SRV_CONTROL_EVENT) !=
        OSIX_SUCCESS)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC, "D6SrApiIp6IfStatusNotify: Send Evt "
                       "Failed \r\n");
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrApirvIfStatusChgHdlr                                    */
/*                                                                           */
/* Description  : Function for handling interface attribute changes whenever */
/*                IPv6 sense change in Iface configurations, it intimates    */
/*                DHCP6_SRV by calling this function.                           */
/*                                                                           */
/* Input        : pIfStatusChange - Pointer to the tNetIpv6IfStatChange      */
/*                                   structure, which contains all the       */
/*                                   interface state change information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6SrApirvIfStatusChgHdlr (tNetIpv6IfStatChange * pIfStatusChange)
{
    if (pIfStatusChange->u4Mask == NETIPV6_INTERFACE_STATUS_CHANGE)
    {
        if (pIfStatusChange->u4IfStat == NETIPV6_IF_DELETE)
        {
            D6SrApiIfDelete (pIfStatusChange->u4Index);
        }
        else
        {
            D6SrApiIfOperStat (pIfStatusChange->u4Index,
                               (UINT1) pIfStatusChange->u4IfStat);
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrApiIfDelete                                             */
/*                                                                           */
/* Description  : This function Handles the Deletes Interface information    */
/*                message received from IPv6 Module.                         */
/*                                                                           */
/* Input        : u4IfIndex - Interface Index.                               */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrApiIfDelete (UINT4 u4IfIndex)
{
    /*Check If Interface is created the delete the interface.
     *When Interface does not exists then return */

    tDhcp6SrvIfInfo    *pIfInfo = NULL;

    if (u4IfIndex > DHCP6_SRV_MAX_INTERFACES)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC, "D6SrApiIfDelete: Interface Number"
                       "wrong \r\n");
        return;
    }
    else
    {
        pIfInfo = D6SrIfGetNode (u4IfIndex);
    
        if (pIfInfo == NULL)
        {
            DHCP6_SRV_TRC (CONTROL_PLANE_TRC, "D6SrApiIfDelete: Interface not"
                           "created for DHCP6_SRV \r\n");
            return;
        }
        nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex, DESTROY);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrApiIfOperStat                                           */
/*                                                                           */
/* Description  : This function Handles the operstatus of  Interface         */
/*                message received from IPv6 Module.                         */
/*                                                                           */
/* Input        : u4IfIndex - Interface Index.                               */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrApiIfOperStat (UINT4 u4IfIndex, UINT1 u1Status)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;

    if (u4IfIndex > DHCP6_SRV_MAX_INTERFACES)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC, "D6SrApiIfDelete: Interface Number"
                       "wrong \r\n");
        return;
    }
    pIfInfo = D6SrIfGetNode (u4IfIndex);

    if (pIfInfo == NULL)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC, "D6SrApiIfDelete: Interface not"
                       "created for DHCP6_SRV \r\n");
        return;
    }

    pIfInfo->u1OperStatus = u1Status;
    return;
}

#ifdef CLI_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrApiShowGlobalInfo                                   */
/*                                                                           */
/* Description  : This function will Display the Server Global Information   */
/*                                                                           */
/* Input        : CliHandle - CLI Handle.                                    */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6SrApiShowGlobalInfo (tCliHandle CliHandle)
{

    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);

    if (D6SrMainLock () == SNMP_FAILURE)
    {
        return;
    }
    /*Show Global Information of Server */
    D6SrCliShowGlobalInfo (CliHandle);
    D6SrMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrApiShowInterfaceInfo                                   */
/*                                                                           */
/* Description  : This function will Display the Server Interface Information   */
/*                                                                           */
/* Input        : CliHandle - CLI Handle.                                    */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6SrApiShowInterfaceInfo (tCliHandle CliHandle, INT4 i4Index)
{

    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);

    if (D6SrMainLock () == SNMP_FAILURE)
    {
        return;
    }
    /*Show Interface Information of Server */
    D6SrCliShowInterfaceInfo (CliHandle, i4Index);
    D6SrMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}
#endif
