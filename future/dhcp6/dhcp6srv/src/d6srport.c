/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srport.c,v 1.4 2014/01/30 12:19:36 siva Exp $
 *
 * Description: This file contains wrappers of external interface functions 
 *              for DHCPv6 Server modules. 
 * ************************************************************************/

#include "d6srinc.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrPortNetIpv6GetIfInfo                                   */
/*                                                                           */
/* Description  : This function gets the Ip-Address of the interface         */
/*                                                                           */
/* Input        : u4IfIndex      - Interface index of the received message   */
/*                pNetIpv6IfInfo - pointer to Strcture.                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
UINT4
D6SrPortNetIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{

    if (NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "D6SrPortNetIpv6GetIfInfo: "
                       "NetIpv6GetIfInfo Function Return Failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrPortGlobalAddr                                         */
/*                                                                           */
/* Description  : The function return the Source Ip Addres of interface      */
/*                                                                           */
/* Input        : u4IfIndex  - Interface Id                                  */
/*                pDstAddr   - Destination IP address                        */
/*                pSrcAddr   - Source IP Address                             */
/*                                                                           */
/* Output       : IP Address                                                 */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrPortGlobalAddr (UINT4 u4IfIndex, tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tNetIpv6AddrInfo    netIp6FirstAddrInfo;
    tNetIpv6AddrInfo    netIp6NextAddrInfo;
    tIp6Addr           *pAddr = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    INT4                i4Status, i4Count;

    i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);

    if (i4Status != NETIPV6_FAILURE)
    {
        do
        {
            if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                              netIp6FirstAddrInfo.u4PrefixLength) == TRUE)
            {
                pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
            else
            {
                pAddr = &netIp6FirstAddrInfo.Ip6Addr;
            }
            i4Status = NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
                                             &netIp6NextAddrInfo);
            if (i4Status != NETIPV6_FAILURE)
                MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                        sizeof (tNetIpv6AddrInfo));
        }
        while (i4Status != NETIPV6_FAILURE);

        MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
        return OSIX_SUCCESS;
    }
    else
    {
        for (i4Count = 0; i4Count < IP6_MAX_LOGICAL_IFACES; i4Count++)
        {
            i4Status = NetIpv6GetFirstIfAddr (i4Count, &netIp6FirstAddrInfo);

            if (i4Status != NETIPV6_FAILURE)
            {
                do
                {
                    if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                                      netIp6FirstAddrInfo.u4PrefixLength) ==
                        TRUE)
                    {
                        pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                        MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                        return OSIX_SUCCESS;
                    }
                    else
                    {
                        pAddr = &netIp6FirstAddrInfo.Ip6Addr;
                    }
                    i4Status = NetIpv6GetNextIfAddr (u4IfIndex,
                                                     &netIp6FirstAddrInfo,
                                                     &netIp6NextAddrInfo);
                    if (i4Status != NETIPV6_FAILURE)
                    {
                        MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                                sizeof (tNetIpv6AddrInfo));
                    }
                }
                while (i4Status != NETIPV6_FAILURE);

                MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmGetContextInfoFromIfIndex                         */
/*                                                                           */
/* Description        : This function is used to get the Context-Id and      */
/*                      the LocalPort-Id for the given IfIndex.              */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interface Index                     */
/*                                                                           */
/* Output(s)          : pu4ContextId   - Corresponding Context-Id            */
/*                      pu2LocalPortId - Corresponding Local Port Id         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrPortVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                      UINT2 *pu2LocalPortId)
{
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId, pu2LocalPortId)
        == VCM_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrPortGetHadwareType                                     */
/*                                                                           */
/* Description  : The function is the porting function and return the Hadware*/
/*                type value.                                                */
/*                                                                           */
/* Input        : pu2HadwareType - Pointer to hadware type                   */
/*                                                                           */
/* Output       : pu2HadwareType - Pointer to hadware type                   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6SrPortGetHadwareType (UINT2 *pu2HadwareType)
{
    *pu2HadwareType = (UINT2) DHCP6_SRV_GET_HADWARE_TYPE;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrPortGetEnterpriseNumber                                */
/*                                                                           */
/* Description  : The function will return the Enterprise Number value of    */
/*                hadware.                                                   */
/*                                                                           */
/* Input        : pu4EnterpriseNumber - Pointer to Enterprise number         */
/*                                                                           */
/* Output       : pu4EnterpriseNumber - Pointer to Enterprise number         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6SrPortGetEnterpriseNumber (UINT4 *pu4EnterpriseNumber)
{
    *pu4EnterpriseNumber = DHCP6_SRV_GET_ENTERPRISE_NUMBER;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrPortGetIdentifier                                      */
/*                                                                           */
/* Description  : The function will return the Dhcp Identifier value.        */
/*                                                                           */
/* Input        : pu1IdentifierLength - Length of DhcpIdentifier             */
/*                pau1Identifier      - DhcpIdentifier Value                 */
/*                                                                           */
/* Output       : pu1IdentifierLength - Length of DhcpIdentifier             */
/*                pau1Identifier      - DhcpIdentifier Value                 */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6SrPortGetIdentifier (UINT1 *pu1IdentifierLength, UINT1 *pau1Identifier)
{
    *pu1IdentifierLength = (UINT1) DHCP6_SRV_GET_IDENTIFIER_LENGTH;

    if (*pu1IdentifierLength > DHCP6_SRV_MAX_IDENTIFIER_SIZE)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "D6SrUtlUpdateClientId: D6SrPortGetIdentifier "
                       "Function Return Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pau1Identifier, DHCP6_SRV_GET_IDENTIFIER_VALUE,
            DHCP6_SRV_GET_IDENTIFIER_LENGTH);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
