/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the procedure for Authorization sub 
 *              Module.
 *
 **************************************************************************/

#include   "d6srinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvValidateAutOption                                  */
/*                                                                           */
/* Description  : This function call the HMAC-MD5 api and return the digest. */
/*                calls this function.                                       */
/*                                                                           */
/* Input        : pu1AuthBuf  - Pointer to the received buffer               */
/*                u4ByteCount - Length of DHCP PDU.                          */
/*                pu1Key      - Key Value.                                   */
/*                u4KeyLen    - Key Length.                                  */
/*                pu1OutputDigest - Digest Pointer.                          */
/*                                                                           */
/* Output       : pu1OutputDigest - Digest value                             */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Dhcp6SrvValidateAutOption (UINT1 *pu1AuthBuf, INT4 u4ByteCount, UINT1 *pu1Key,
                           UINT4 u4KeyLen, UINT1 *pu1OutputDigest)
{
    arHmac_MD5 (pu1AuthBuf, u4ByteCount, pu1Key, u4KeyLen, pu1OutputDigest);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvCalculateAuthData                                  */
/*                                                                           */
/* Description  : This function call the HMAC-MD5 api and return the digest. */
/*                calls this function.                                       */
/*                                                                           */
/* Input        : pu1AuthBuf  - Pointer to the received buffer               */
/*                u4ByteCount - Length of DHCP PDU.                          */
/*                pu1Key      - Key Value.                                   */
/*                u4KeyLen    - Key Length.                                  */
/*                pu1OutputDigest - Digest Pointer.                          */
/*                                                                           */
/* Output       : pu1OutputDigest - Digest value                             */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Dhcp6SrvCalculateAuthData (UINT1 *pu1AuthBuf, INT4 u4ByteCount,
                           UINT1 *pu1Key,
                           UINT4 u4KeyLen, UINT1 *pu1OutputDigest)
{
    arHmac_MD5 (pu1AuthBuf, u4ByteCount, pu1Key, u4KeyLen, pu1OutputDigest);
    return;
}
