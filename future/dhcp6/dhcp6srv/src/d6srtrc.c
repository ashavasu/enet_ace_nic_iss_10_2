/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srtrc.c,v 1.5 2012/01/16 13:01:21 siva Exp $
 *
 * Description: This file contains debugging related functions.
 * ************************************************************************/

#include "d6srinc.h"

PRIVATE VOID Dhcp6SrvUtilSetTraceOption PROTO ((UINT1 *, UINT4 *));
PRIVATE VOID Dhcp6SrvUtilSplitStrToTokens PROTO ((UINT1 *, INT4,
                                                  UINT1, UINT2,
                                                  UINT1 *apu1Token[], UINT1 *));

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvUtilGetTraceInputValue                             */
/*                                                                           */
/* Description  : This function gives the trace string depending upon the    */
/*                trace option.                                              */
/*                                                                           */
/* Input        : pu1TraceInput - Trace string                               */
/*                u4TraceOption - Trace option                               */
/*                                                                           */
/* Output       : Gives the string format trace option.                      */
/*                                                                           */
/* Returns      : Trace string length.                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Dhcp6SrvUtilGetTraceInputValue (UINT1 *pu1TraceInput, UINT4 u4TraceOption)
{
    UINT1               u1BitNumber = 0;
    UINT1               u1MaxBits = 0;
    UINT4               u4TrcLen = 0;

    /* trace string */
    UINT1
         
         
         
         
         
         
         
        au1TraceString[DHCP6_SRV_MAX_TRC_STR_COUNT][DHCP6_SRV_MAX_TRC_STR_LEN] =
    {
        "init-shut ", "mgmt ", "", "ctrl ", "pkt-dump ", "resource ",
        "all-fail ", "buffer ", "critical"
    };

    /* All trace */
    if ((u4TraceOption & DHCP6_SRV_ALL_TRC) == DHCP6_SRV_ALL_TRC)
    {
        STRNCPY (pu1TraceInput, "all", STRLEN ("all"));
        u4TrcLen = STRLEN ("all");
        return u4TrcLen;
    }

    u1MaxBits = sizeof (u4TraceOption) * BITS_PER_BYTE;
    u1MaxBits =
        (u1MaxBits >
         DHCP6_SRV_MAX_TRC_STR_COUNT) ? DHCP6_SRV_MAX_TRC_STR_COUNT : u1MaxBits;
    for (u1BitNumber = 0; u1BitNumber < u1MaxBits; u1BitNumber++)
    {
        if ((u4TraceOption >> u1BitNumber) & OSIX_TRUE)
        {
            STRNCPY (pu1TraceInput, au1TraceString[u1BitNumber],
                     STRLEN (au1TraceString[u1BitNumber]));
            pu1TraceInput += STRLEN (au1TraceString[u1BitNumber]);
            u4TrcLen += STRLEN (au1TraceString[u1BitNumber]);
        }
    }

    return u4TrcLen;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvUtilGetTraceOptionValue                            */
/*                                                                           */
/* Description  : This function rocess given trace input and sets the        */
/*                corresponding option bit.                                  */
/*                                                                           */
/* Input        : pu1TraceInput - Trace string                               */
/*                                                                           */
/* Output       : Gives the string format trace option.                      */
/*                                                                           */
/* Returns      : Trace string length.                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Dhcp6SrvUtilGetTraceOptionValue (UINT1 *pu1TraceInput, INT4 i4Tracelen)
{
    UINT1
         
         
         
         
         
         
         
        aau1Tokens[DHCP6_SRV_MAX_TRACE_TOKENS][DHCP6_SRV_MAX_TRACE_TOKEN_SIZE];
    UINT1              *apu1Tokens[DHCP6_SRV_MAX_TRACE_TOKENS];
    UINT1               u1Count = 0;
    UINT1               u1TokenCount = 0;
    UINT4               u4TraceOption = DHCP6_SRV_INVALID_TRC;

    MEMSET (aau1Tokens, 0, sizeof (aau1Tokens));

    /* Enable */
    if ((STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable "))) == 0)
    {
        pu1TraceInput += STRLEN ("enable ");
        i4Tracelen -= STRLEN ("enable ");
    }
    /* Disable */
    else if ((STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable "))) == 0)
    {
        pu1TraceInput += STRLEN ("disable ");
        i4Tracelen -= STRLEN ("disable ");
    }
    if ((STRNCMP (pu1TraceInput, "all", STRLEN ("all"))) == 0)
    {
        /* For traces,
         * All trace         : all
         * All failure trace : all-fail 
         * the first 3 characters are common(all), so verify that
         * the trace length is equal to 3(strlen("all")), if yes then set
         * all trace and return, else if the trace length is greater
         * than STRLEN ("all"), then verify whether the trace is "all-fail" 
         * or not. If the trace is not "all-fail" then the trace is considered
         * as invalid invalid trace so return invalid trace */
        if (i4Tracelen == STRLEN ("all"))
        {
            u4TraceOption = DHCP6_SRV_ALL_TRC;
            return u4TraceOption;
        }
        else if (i4Tracelen > ((INT4) STRLEN ("all")))
        {
            if (STRNCMP (pu1TraceInput, "all-fail", STRLEN ("all-fail")) == 0)
            {
                return ALL_FAILURE_TRC;
            }
            else
            {
                return DHCP6_SRV_INVALID_TRC;
            }
        }
    }

    /* assign memory address for all the pointers in token array(apu1Tokens) */
    for (u1Count = 0; u1Count < DHCP6_SRV_MAX_TRACE_TOKENS; u1Count++)
    {
        apu1Tokens[u1Count] = aau1Tokens[u1Count];
    }

    /* get the tokens from the trace input buffer */
    Dhcp6SrvUtilSplitStrToTokens (pu1TraceInput, i4Tracelen,
                                  (UINT1) DHCP6_SRV_TRACE_TOKEN_DELIMITER,
                                  (UINT2) DHCP6_SRV_MAX_TRACE_TOKENS,
                                  apu1Tokens, &u1TokenCount);

    /* get tokens one by one from the token array and set the 
     * trace options based on the tokens */
    for (u1Count = 0; ((u1Count < DHCP6_SRV_MAX_TRACE_TOKENS) &&
                       (u1Count < u1TokenCount)); u1Count++)
    {
        /* set the trace option based on the give token */
        Dhcp6SrvUtilSetTraceOption (apu1Tokens[u1Count], &u4TraceOption);

        /* if invalid trace option is returned by the function 
         * Dhcp6SrvUtilSetTraceOption, then dont continue the for loop, just
         * return with invalid trace option */
        if (u4TraceOption == DHCP6_SRV_INVALID_TRC)
        {
            return u4TraceOption;
        }
    }
    return u4TraceOption;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvUtilSetTraceOption                                 */
/*                                                                           */
/* Description  : This function sets the trace option based on the given     */
/*                trace token.                                               */
/*                                                                           */
/* Input        : apu1Token - Poiner to token array                          */
/*                pu4TraceOption - Pointer to trace option                   */
/*                                                                           */
/* Output       : pu4TraceOption - Pointer to trace option.                  */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Dhcp6SrvUtilSetTraceOption (UINT1 *pu1Token, UINT4 *pu4TraceOption)
{
    if ((STRNCMP (pu1Token, "init-shut", STRLEN ("init-shut"))) == 0)
    {
        *pu4TraceOption |= INIT_SHUT_TRC;
    }
    else if ((STRNCMP (pu1Token, "mgmt", STRLEN ("mgmt"))) == 0)
    {
        *pu4TraceOption |= MGMT_TRC;
    }
    else if ((STRNCMP (pu1Token, "ctrl", STRLEN ("ctrl"))) == 0)
    {
        *pu4TraceOption |= CONTROL_PLANE_TRC;
    }
    else if ((STRNCMP (pu1Token, "pkt-dump", STRLEN ("pkt-dump"))) == 0)
    {
        *pu4TraceOption |= DUMP_TRC;
    }
    else if ((STRNCMP (pu1Token, "resource", STRLEN ("resource"))) == 0)
    {
        *pu4TraceOption |= OS_RESOURCE_TRC;
    }
    else if ((STRNCMP (pu1Token, "all-fail", STRLEN ("all-fail"))) == 0)
    {
        *pu4TraceOption |= ALL_FAILURE_TRC;
    }
    else if ((STRNCMP (pu1Token, "buffer", STRLEN ("buffer"))) == 0)
    {
        *pu4TraceOption |= BUFFER_TRC;
    }
    else if ((STRNCMP (pu1Token, "critical", STRLEN ("critical"))) == 0)
    {
        *pu4TraceOption |= DHCP6_SRV_CRITICAL_TRC;
    }
    else if ((STRNCMP (pu1Token, "all", STRLEN ("all"))) == 0)
    {
        *pu4TraceOption = DHCP6_SRV_ALL_TRC;
    }
    else
    {
        *pu4TraceOption = DHCP6_SRV_INVALID_TRC;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvUtilSplitStrToTokens                               */
/*                                                                           */
/* Description  : This function splits the given string into tokens based on */
/*                the given token delimiter and stores the tokens in token   */
/*                array and returns the token array.                         */
/*                                                                           */
/* Input(s)     : pu1InputStr  - Pointer to input string                     */
/*                i4Strlen     - String length                               */
/*                u1Delimiter  - Delimiter by which the token has to be      */
/*                               seperated                                   */
/*                u2MaxToken   - Max token count                             */
/*                apu1Token    - Poiner to token array                       */
/*                pu1TokenCount - Token count                                */
/*                                                                           */
/* Output(s)    : apu1Token     - Poiner to token array                      */
/*                pu1TokenCount - Token count                                */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Dhcp6SrvUtilSplitStrToTokens (UINT1 *pu1InputStr, INT4 i4Strlen,
                              UINT1 u1Delimiter, UINT2 u2MaxToken,
                              UINT1 *apu1Token[], UINT1 *pu1TokenCount)
{
    UINT1               u1TokenSize = 0;
    UINT1               u1TokenCount = 0;

    while ((i4Strlen > 0) && (u1TokenCount < u2MaxToken))
    {
        /* reset token size to zero */
        u1TokenSize = 0;
        /* scan for delimiter */
        while ((i4Strlen > 0) && (*(pu1InputStr + u1TokenSize) != u1Delimiter))
        {
            i4Strlen--;
            u1TokenSize++;
        }
        /* since the loop breaks whenever delimiter is found the string length
         * and token size shoud be updated once(outside the loop) by the size
         * of delimiter(which is 1byte) */
        i4Strlen--;
        u1TokenSize++;
        /* copy the token excluding delimiter */
        STRNCPY (apu1Token[u1TokenCount], pu1InputStr,
                 (u1TokenSize - sizeof (u1Delimiter)));
        /* move the input string pointer to point to next token */
        pu1InputStr += u1TokenSize;
        /* increment the token count */
        u1TokenCount++;
    }
    /* update the token count */
    *pu1TokenCount = u1TokenCount;
    return;
}
