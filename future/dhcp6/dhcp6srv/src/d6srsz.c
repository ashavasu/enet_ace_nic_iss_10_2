/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srsz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _D6SRSZ_C
#include "d6srinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
D6srSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < D6SR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsD6SRSizingParams[i4SizingId].u4StructSize,
                              FsD6SRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(D6SRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            D6srSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
D6srSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsD6SRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, D6SRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
D6srSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < D6SR_MAX_SIZING_ID; i4SizingId++)
    {
        if (D6SRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (D6SRMemPoolIds[i4SizingId]);
            D6SRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
