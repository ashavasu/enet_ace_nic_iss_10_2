/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access ip prefix table.
 * ************************************************************************/

#include "d6srinc.h"

PRIVATE INT4 D6SrInPrefixRBCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE UINT1 D6SrInPrefixGetMatchCount PROTO ((UINT1 *, UINT1, UINT1 *));

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixCreateTable
 *
 * DESCRIPTION      : This function creats the include-prefix table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrInPrefixCreateTable (VOID)
{

    DHCP6_SRV_INPREFIX_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvInPrefixInfo, GblRBLink),
                              D6SrInPrefixRBCmp);
    if (DHCP6_SRV_INPREFIX_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrInPrefixCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixDeleteTable
 *
 * DESCRIPTION      : This function Deletes the include-prefix table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrInPrefixDeleteTable (VOID)
{
    if (DHCP6_SRV_INPREFIX_TABLE != NULL)
    {
        D6SrInPrefixDrainTable ();
        RBTreeDestroy (DHCP6_SRV_INPREFIX_TABLE, D6SrInPrefixRBFree, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the
 *                    include-prefix table. 
 *                    Indices of this table are -
 *                    o Context Id
 *                    o Prefix Address
 *
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrInPrefixRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvInPrefixInfo *pInPrefixEntryA = NULL;
    tDhcp6SrvInPrefixInfo *pInPrefixEntryB = NULL;

    pInPrefixEntryA = (tDhcp6SrvInPrefixInfo *) pRBElem1;
    pInPrefixEntryB = (tDhcp6SrvInPrefixInfo *) pRBElem2;

    if (pInPrefixEntryA->u4ContextId != pInPrefixEntryB->u4ContextId)
    {
        if (pInPrefixEntryA->u4ContextId < pInPrefixEntryB->u4ContextId)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    if (pInPrefixEntryA->u4InPrefixIndex != pInPrefixEntryB->u4InPrefixIndex)
    {
        if (pInPrefixEntryA->u4InPrefixIndex < pInPrefixEntryB->u4InPrefixIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }

    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixCreateNode 
 *
 * DESCRIPTION      : This function creates a include-prefix table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvInPrefixInfo * - Pointer to the created include-prefix node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvInPrefixInfo *
D6SrInPrefixCreateNode (VOID)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;

    DHCP6_SRV_ALLOC_MEM_INPREFIX (pInPrefixInfo);

    if (pInPrefixInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrInPrefixCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }

    MEMSET (pInPrefixInfo, 0x00, sizeof (tDhcp6SrvInPrefixInfo));

    return pInPrefixInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixAddNode
 *
 * DESCRIPTION      : This function add a node to the include-prefix table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pInPrefixInfo - pointer to include-prefix information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrInPrefixAddNode (tDhcp6SrvInPrefixInfo * pInPrefixInfo)
{
    if (RBTreeAdd (DHCP6_SRV_INPREFIX_TABLE, pInPrefixInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrInPrefixAddNode: " " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }
    /*Check if trie root is present for this context, if  not then create a
     * tri2 2 root*/
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixDelNode
 *
 * DESCRIPTION      : This function delete a include-prefix node from the
 *                    include-prefix table and release the memory used by that
 *                    node to the memory include-prefix. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pInPrefixInfo - pointer to include-prefix information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrInPrefixDelNode (tDhcp6SrvInPrefixInfo * pInPrefixInfo)
{
    RBTreeRem (DHCP6_SRV_INPREFIX_TABLE, pInPrefixInfo);

    DHCP6_SRV_FREE_MEM_INPREFIX (pInPrefixInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a include-prefix node, and then release the
 *                    memory used by the node to the memory include-prefix.
 *
 * INPUT            : pRBElem - pointer to the include-prefix node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrInPrefixRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    DHCP6_SRV_FREE_MEM_INPREFIX (pRBElem);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixDrainTable
 *
 * DESCRIPTION      : This function deletes all the include-prefix nodes
 *                    associated with the include-prefix table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrInPrefixDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_INPREFIX_TABLE, D6SrInPrefixRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixGetFirstNode
 *
 * DESCRIPTION      : This function returns the first include-prefix node
 *                    from the include-prefix table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvInPrefixInfo * - pointer to the first
 *                    include-prefix info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvInPrefixInfo *
D6SrInPrefixGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_INPREFIX_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixGetNextNode
 *
 * DESCRIPTION      : This function returns the next include-prefix info
 *                    from the include-prefix table.
 *
 * INPUT            : pCurrentInPrefixInfo - pointer to the include-prefix 
 *                    info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvInPrefixInfo * - Pointer to the next
 *                    include-prefix info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvInPrefixInfo *
D6SrInPrefixGetNextNode (tDhcp6SrvInPrefixInfo * pCurrentInPrefixInfo)
{
    return RBTreeGetNext (DHCP6_SRV_INPREFIX_TABLE, pCurrentInPrefixInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixGetNode
 *
 * DESCRIPTION      : This function helps to find a include-prefix node from 
 *                    the include-prefix table.
 *
 * INPUT            : u4InPrefixIndex - InPrefix Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvInPrefixInfo * - Pointer to the next
 *                    include-prefix info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvInPrefixInfo *
D6SrInPrefixGetNode (UINT4 u4ContextId, UINT4 u4InPrefixIndex)
{
    tDhcp6SrvInPrefixInfo Key;

    Key.u4ContextId = u4ContextId;
    Key.u4InPrefixIndex = u4InPrefixIndex;

    return RBTreeGet (DHCP6_SRV_INPREFIX_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixGetNode
 *
 * DESCRIPTION      : This function helps to find a include-prefix node from 
 *                    the include-prefix table.
 *
 * INPUT            : u4InPrefixIndex - InPrefix Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvInPrefixInfo * - Pointer to the next
 *                    include-prefix info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvInPrefixInfo *
D6SrInPrefixLookupNode (UINT4 u4ContextId, UINT1 *pu1SrcAddr)
{
    tDhcp6SrvInPrefixInfo InPrefixInfo;
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    UINT1               u1MatchCount = 0;

    MEMSET (&InPrefixInfo, 0x00, sizeof (tDhcp6SrvInPrefixInfo));
    InPrefixInfo.u4ContextId = u4ContextId;
    pInPrefixInfo = D6SrInPrefixGetNextNode (&InPrefixInfo);
    while (pInPrefixInfo != NULL)
    {
        u1MatchCount =
            D6SrInPrefixGetMatchCount (pInPrefixInfo->au1Address,
                                       pInPrefixInfo->u1AddressLength,
                                       pu1SrcAddr);
        if (u1MatchCount == pInPrefixInfo->u1AddressLength)
        {
            return pInPrefixInfo;
        }
        pInPrefixInfo = D6SrInPrefixGetNextNode (pInPrefixInfo);
        if ((pInPrefixInfo == NULL) ||
            (pInPrefixInfo->u4ContextId != u4ContextId))
        {
            break;
        }
    }
    return NULL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixGetMatchCount
 *
 * DESCRIPTION      : This function finds the number of matching bytes b/w
 *                    IP prefix and source address
 *
 * INPUT            : au1Prefix       - IPv6 Prefix 
 *                    u1PrefixLength  - IPv6 Prefix Length
 *                    au1SrcAddr      - IPv6 Source Address
 *
 * OUTPUT           : None
 *
 * RETURNS          : Number of matching bytes b/w IPv6 prefix and source 
 *                    address
 *
 **************************************************************************/
PRIVATE UINT1
D6SrInPrefixGetMatchCount (UINT1 *pu1Prefix,
                           UINT1 u1PrefixLength, UINT1 *pu1SrcAddr)
{
    UINT1               u1MatchCount = 0;
    UINT1               u1Counter = 0;
    while (u1Counter < u1PrefixLength)
    {
        if (pu1SrcAddr[u1Counter] == pu1Prefix[u1Counter])
        {
            u1MatchCount++;
        }
        u1Counter++;
    }
    return u1MatchCount;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrInPrefixGetFreeIndex
 *
 * DESCRIPTION      : This function helps to find a free index which can be
 *                    used to create a new entry in the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4InPrefixIndex - Pointer to the pool index
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrInPrefixGetFreeIndex (UINT4 *pu4FreeInPrefixIndex)
{
    tDhcp6SrvInPrefixInfo *pInPrefixInfo = NULL;
    UINT4               u4InPrefixIndex = 0;
    UINT4               u4MaxInPrefixIndex = DHCP6_SRV_INPREFIX_INDEX_MAX;
    UINT4               u4ContextId = 0;

    for (u4InPrefixIndex = 1; u4InPrefixIndex <= u4MaxInPrefixIndex;
         u4InPrefixIndex++)
    {
        pInPrefixInfo = D6SrInPrefixGetNode (u4ContextId, u4InPrefixIndex);

        if (pInPrefixInfo == NULL)
        {
            *pu4FreeInPrefixIndex = u4InPrefixIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6sripfx.c                      */
/*-----------------------------------------------------------------------*/
