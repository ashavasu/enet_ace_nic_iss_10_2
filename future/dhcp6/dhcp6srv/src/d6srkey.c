/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access key table.
 * ************************************************************************/

#include "d6srinc.h"
PRIVATE INT4 D6SrKeyRBCmp PROTO ((tRBElem *, tRBElem *));
/***************************************************************************
 * FUNCTION NAME    : D6SrKeyCreateTable
 *
 * DESCRIPTION      : This function creats the key table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrKeyCreateTable (VOID)
{
    DHCP6_SRV_KEY_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvKeyInfo, GblRBLink),
                              D6SrKeyRBCmp);
    if (DHCP6_SRV_KEY_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrKeyCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyDeleteTable
 *
 * DESCRIPTION      : This function Deletes the key table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrKeyDeleteTable (VOID)
{
    if (DHCP6_SRV_KEY_TABLE != NULL)
    {
        D6SrKeyDrainTable ();
        RBTreeDestroy (DHCP6_SRV_KEY_TABLE, D6SrKeyRBFree, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the key table. 
 *                    Indices of this table are -
 *                    o Realm Index
 *                    o Key Index
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrKeyRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvKeyInfo   *pKeyEntryA = NULL;
    tDhcp6SrvKeyInfo   *pKeyEntryB = NULL;

    pKeyEntryA = (tDhcp6SrvKeyInfo *) pRBElem1;
    pKeyEntryB = (tDhcp6SrvKeyInfo *) pRBElem2;

    if (pKeyEntryA->pRealm->u4RealmIndex != pKeyEntryB->pRealm->u4RealmIndex)
    {
        if (pKeyEntryA->pRealm->u4RealmIndex < pKeyEntryB->pRealm->u4RealmIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    if (pKeyEntryA->u4KeyId != pKeyEntryB->u4KeyId)
    {
        if (pKeyEntryA->u4KeyId < pKeyEntryB->u4KeyId)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyCreateNode 
 *
 * DESCRIPTION      : This function creates a key table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvKeyInfo * - Pointer to the created key node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvKeyInfo *
D6SrKeyCreateNode (VOID)
{
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    DHCP6_SRV_ALLOC_MEM_KEY (pKeyInfo);
    if (pKeyInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrKeyCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }
    MEMSET (pKeyInfo, 0x00, sizeof (tDhcp6SrvKeyInfo));
    return pKeyInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyAddNode
 *
 * DESCRIPTION      : This function add a node to the key table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pKeyInfo - pointer to key information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrKeyAddNode (tDhcp6SrvKeyInfo * pKeyInfo)
{
    if (RBTreeAdd (DHCP6_SRV_KEY_TABLE, pKeyInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrKeyAddNode: "
                       " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyDelNode
 *
 * DESCRIPTION      : This function delete a key node from the
 *                    key table and release the memory used by that
 *                    node to the memory key. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pKeyInfo - pointer to key information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrKeyDelNode (tDhcp6SrvKeyInfo * pKeyInfo)
{
    RBTreeRem (DHCP6_SRV_KEY_TABLE, pKeyInfo);
    DHCP6_SRV_FREE_MEM_KEY (pKeyInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a key node, and then release the
 *                    memory used by the node to the memory key.
 *
 * INPUT            : pRBElem - pointer to the key node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrKeyRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    DHCP6_SRV_FREE_MEM_KEY (pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyDrainTable
 *
 * DESCRIPTION      : This function deletes all the key nodes
 *                    associated with the key table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrKeyDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_KEY_TABLE, D6SrKeyRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyGetFirstNode
 *
 * DESCRIPTION      : This function returns the first key node
 *                    from the key table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvKeyInfo * - pointer to the first key info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvKeyInfo *
D6SrKeyGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_KEY_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyGetNextNode
 *
 * DESCRIPTION      : This function returns the next key info
 *                    from the key table.
 *
 * INPUT            : pCurrentKeyInfo - pointer to the key info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvKeyInfo * - Pointer to the next key info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvKeyInfo *
D6SrKeyGetNextNode (tDhcp6SrvKeyInfo * pCurrentKeyInfo)
{
    return RBTreeGetNext (DHCP6_SRV_KEY_TABLE, pCurrentKeyInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyGetNode
 *
 * DESCRIPTION      : This function helps to find a key node from the key 
 *                    table.
 *
 * INPUT            : u4KeyId - Key Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvKeyInfo * - Pointer to the next key info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvKeyInfo *
D6SrKeyGetNode (UINT4 u4RealmIndex, UINT4 u4KeyId)
{
    tDhcp6SrvKeyInfo    Key;
    Key.u4KeyId = u4KeyId;
    Key.pRealm = D6SrRealmGetNode (u4RealmIndex);
    if (Key.pRealm == NULL)
    {
        return NULL;
    }
    return RBTreeGet (DHCP6_SRV_KEY_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrKeyGetFreeIndex
 *
 * DESCRIPTION      : This function helps to find a free index which can be
 *                    used to create a new entry in the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4KeyId - Pointer to the key index
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrKeyGetFreeIndex (UINT4 u4RealmIndex, UINT4 *pu4FreeKeyIndex)
{
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    UINT4               u4KeyIndex = 0;
    UINT4               u4MaxKeyIndex = DHCP6_SRV_KEY_INDEX_MAX;

    pRealmInfo = D6SrRealmGetNode (u4RealmIndex);

    if (pRealmInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    for (u4KeyIndex = 1; u4KeyIndex <= u4MaxKeyIndex; u4KeyIndex++)
    {
        pKeyInfo = D6SrKeyGetNode (u4RealmIndex, u4KeyIndex);

        if (pKeyInfo == NULL)
        {
            *pu4FreeKeyIndex = u4KeyIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srkey.c                      */
/*-----------------------------------------------------------------------*/
