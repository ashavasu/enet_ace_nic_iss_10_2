/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srskt.c,v 1.17 2017/09/08 12:22:45 siva Exp $
 *
 * Description:  This file contains the routines for interfacing with the
 *               socket library.
 * ************************************************************************/

#include "d6srinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrSktSockInit                                           */
/*                                                                           */
/* Description  : This function creates a socket for UDP port 67 and binds   */
/*                an address to the port                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrSktSockInit ()
{
#ifdef NPAPI_WANTED
    tHwCfaFilterInfo    HwCfaFilterInfo;
    tIp6Addr            McastAddr;
#endif

    struct sockaddr_in6 SockAddr;
    INT4                i4Sock = 0;
    INT4                i4Optval = 1;

    MEMSET (&SockAddr, 0, sizeof (SockAddr));
    /* Bind a address and port to the socket */
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (DHCP6_SRV_LISTEN_PORT);
    SockAddr.sin6_flowinfo = 0;
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);
    i4Sock = socket (AF_INET6, SOCK_DGRAM, 0);
    if (i4Sock < 0)
    {
        /* Opening of DHCP SRV Socket failed */
        return OSIX_FAILURE;
    }
    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) < 0)
    {
        /* Binding of DHCP SRV socket failed */
        return OSIX_FAILURE;
    }
    if (setsockopt (i4Sock, IPPROTO_IPV6, IP_PKTINFO, (INT2 *) (VOID *)
                    &i4Optval, sizeof (UINT4)) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    /*Set the option not to receive multicast packet on the interface on which
     * it is sent*/
    i4Optval = FALSE;
    if (setsockopt (i4Sock, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4Optval, sizeof (i4Optval)) < 0)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                       "DHCP6 SRV: Unable to set setsockopt for IPV6_MULTICAST_LOOP.\r\n");
        return OSIX_FAILURE;
    }
    if (SelAddFd (i4Sock, D6SrSktPktInSocket) != OSIX_SUCCESS)
    {
        i4Sock = -1;
        close (i4Sock);
        return OSIX_FAILURE;
    }

    if (fcntl (i4Sock, F_SETFL, O_NONBLOCK) < 0)
    {
        i4Sock = -1;
        close (i4Sock);
        return OSIX_FAILURE;
    }
    if (DHCP6_SRV_SOCKET_FD >= 0)
    {
        SelRemoveFd (DHCP6_SRV_SOCKET_FD);
        close (DHCP6_SRV_SOCKET_FD);
    }
    DHCP6_SRV_SOCKET_FD = i4Sock;

#ifdef NPAPI_WANTED
    INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &McastAddr);
    MEMSET (&HwCfaFilterInfo, 0, sizeof (tHwCfaFilterInfo));
    HwCfaFilterInfo.u4IfIndex = 0;
    HwCfaFilterInfo.u2Addr1Type = CFA_NP_IPV6_PROTO;
    MEMCPY (HwCfaFilterInfo.au1Addr1, &McastAddr.u1_addr, sizeof (tIp6Addr));
    if (FsCfaHwCreatePktFilter
        (&HwCfaFilterInfo, &gDhcp6SrvGblInfo.u4RlySrvFilterId) != FNP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
#ifdef NPAPI_WANTED
    INET_ATON6 (DHCP6_ALL_SERVER, &McastAddr);
    MEMSET (&HwCfaFilterInfo, 0, sizeof (tHwCfaFilterInfo));
    HwCfaFilterInfo.u4IfIndex = 0;
    HwCfaFilterInfo.u2Addr1Type = CFA_NP_IPV6_PROTO;
    MEMCPY (HwCfaFilterInfo.au1Addr1, &McastAddr.u1_addr, sizeof (tIp6Addr));
    if (FsCfaHwCreatePktFilter
        (&HwCfaFilterInfo, &gDhcp6SrvGblInfo.u4SrvFilterId) != FNP_SUCCESS)
    {
        FsCfaHwDeletePktFilter (gDhcp6SrvGblInfo.u4RlySrvFilterId);
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrSktSockDeInit                                          */
/*                                                                           */
/* Description  : This function delets the server socket                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
D6SrSktSockDeInit ()
{
    if (DHCP6_SRV_SOCKET_FD != -1)
    {
        SelRemoveFd (DHCP6_SRV_SOCKET_FD);
        close (DHCP6_SRV_SOCKET_FD);
    }
    DHCP6_SRV_SOCKET_FD = -1;

#ifdef NPAPI_WANTED
    FsCfaHwDeletePktFilter (gDhcp6SrvGblInfo.u4RlySrvFilterId);
    FsCfaHwDeletePktFilter (gDhcp6SrvGblInfo.u4SrvFilterId);
#endif

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrSktPktInSocket                                        */
/*                                                                           */
/* Description  : Call back function from SelAddFd(), when DHCP packet is    */
/*                received on DHCP Server socket.                            */
/*                                                                           */
/* Input        : DHCP_TASK_ID                                               */
/*                                                                           */
/* Output       : Sends an event to DHCP Server task                         */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
D6SrSktPktInSocket (INT4 i4SockFd)
{
    if (i4SockFd == DHCP6_SRV_SOCKET_FD)
    {
        if (OsixSendEvent (SELF, DHCP6_SRV_TASK,
                           DHCP6_SRV_DATA_EVENT) != OSIX_SUCCESS)
        {
            DHCP6_SRV_TRC (OS_RESOURCE_TRC, "Event send Failed. \r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrSktSendToDst                                          */
/*                                                                           */
/* Description  : The function sends the packet to the destination on the    */
/*                appropriate port depending on the type of destination.     */
/*                                                                           */
/* Input        : pPktInfo - Local structure to be filled with header        */
/*                           information.                                    */
/*                pu1Buf  - Received DHCP6 packet (Linear buffer).           */
/*                u4Len   -  Length of received packet.                      */
/*                u4RepType - Type to indicate whether the destinaion is     */
/*                            Relay or Client.                               */
/*                                                                           */
/* Output       : pPktInfo                                                   */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6SrSktSendToDst (tDhcp6ParseInfo * pParseInfo)
{
    tIp6Addr            SourceAddress;
    tIp6Addr            SrcAddress;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    UINT2               u2DstPort = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4HopLimit = 255;
    struct cmsghdr      CmsgInfo;
#else
    UINT1               CmsgInfo[112];
    INT4                i4ReturnValue = 0;
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    MEMSET (&SrcAddress, 0, sizeof (tIp6Addr));
    MEMSET (&SourceAddress, 0, sizeof (tIp6Addr));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    if (pParseInfo->u2WrittenLen > DHCP6_MAX_MTU)
    {
        return;
    }

    if (pParseInfo->dhcp6.u1PduType == DHCP6_SRV_RELAY_MSG)
    {
        u2DstPort = DHCP6_SRV_RLY_TRANSMIT_PORT;
    }
    else
    {
        u2DstPort = DHCP6_SRV_CLNT_TRANSMIT_PORT;
    }

    /* Set src addr as link local addr for this logical interface */
    if (D6SrPortNetIpv6GetIfInfo
        (pParseInfo->pIfInfo->u4IfIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
    {
        return;
    }

    if (MEMCMP (&NetIpv6IfInfo.Ip6Addr, &SrcAddress, sizeof (tIp6Addr)) == 0)
    {
        if (D6SrPortGlobalAddr
            (pParseInfo->pIfInfo->u4IfIndex, &(pParseInfo->ip6Hdr.dstAddr),
             &SourceAddress) == OSIX_FAILURE)
        {
            return;
        }
        Ip6AddrCopy (((tIp6Addr *) & SrcAddress), &SourceAddress);
    }
    else
    {
        Ip6AddrCopy (((tIp6Addr *) & SrcAddress), &NetIpv6IfInfo.Ip6Addr);
    }

    PeerAddr.sin6_family = AF_INET6;
    PeerAddr.sin6_port = OSIX_HTONS (u2DstPort);
#ifdef BSDCOMP_SLI_WANTED
    i4ReturnValue =
        NetIpv6GetIfInfo ((pParseInfo->pIfInfo->u4IfIndex), &NetIpv6IfInfo);
    if (i4ReturnValue == NETIPV6_FAILURE)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC, "info not present for index\r\n");
        return;
    }
    PeerAddr.sin6_scope_id = NetIpv6IfInfo.u4IpPort;
#else
    PeerAddr.sin6_scope_id = pParseInfo->pIfInfo->u4IfIndex;
#endif

    Ip6AddrCopy ((tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr,
                 &pParseInfo->ip6Hdr.srcAddr);
#ifndef BSDCOMP_SLI_WANTED

    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        return;
    }

    MEMCPY ((&pIn6Pktinfo->ipi6_ifindex), (&pParseInfo->pIfInfo->u4IfIndex),
            sizeof (UINT4));

    Ip6AddrCopy (((tIp6Addr *) (VOID *)
                  &pIn6Pktinfo->ipi6_addr.s6_addr), &SrcAddress);

    sendmsg (DHCP6_SRV_SOCKET_FD, &Ip6MsgHdr, 0);

    if (setsockopt (DHCP6_SRV_SOCKET_FD, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                    &i4HopLimit, sizeof (INT4)) < 0)
    {
        return;
    }
    if (sendto (DHCP6_SRV_SOCKET_FD, DHCP6_SRV_WRITE_PDU, pParseInfo->
                u2WrittenLen, 0, (struct sockaddr *) &PeerAddr,
                sizeof (struct sockaddr)) < 0)
    {
        return;
    }
#else
    pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;

    /* EVALRIPNG cc below :: changed from sizeof (Cmsginfo) */

    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    /* EVALRIPNG cc */
    Iov.iov_base = DHCP6_SRV_WRITE_PDU;
    Iov.iov_len = pParseInfo->u2WrittenLen;

    Ip6MsgHdr.msg_name = (VOID *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    /* EVALRIPNG end */
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    pIn6Pktinfo->ipi6_ifindex = PeerAddr.sin6_scope_id;;

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 &SrcAddress);

    /* EVALRIPNG :: using a single call */
    if (sendmsg (DHCP6_SRV_SOCKET_FD, &Ip6MsgHdr, 0) < 0)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                       "D6SrSktSendToDst: Error in sending message !\r\n");
        return;
    }
#endif
    /* Update the counters */
    if (pParseInfo->dhcp6.u1PduType == DHCP6_SRV_RELAY_MSG)
    {
        pParseInfo->pIfInfo->u4RelayReplyOut++;
    }
    else
    {
        pParseInfo->pIfInfo->u4ReplyOut++;
    }
    return;
}
