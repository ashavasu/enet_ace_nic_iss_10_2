/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srque.c,v 1.10 2014/04/01 11:17:16 siva Exp $
 *
 * Description: This file contains the procedure related to
 *              processing of QMsgs and Enquing QMsg from external
 *              module to DHCPv6 Server Task
 * ************************************************************************/

#include "d6srinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvRcvAndProcessPkt                                   */
/*                                                                           */
/* Description  : The function process the packet                            */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
Dhcp6SrvRcvAndProcessPkt ()
{
    tDhcp6ParseInfo     ParseInfo;
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    tIp6Addr            Ip6Addr;
    UINT4               u4IfIndex;
#ifdef SLI_WANTED
    struct cmsghdr      CmsgInfo;
#endif
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4OptEnable = 1;
    struct cmsghdr     *pCmsgInfo;
    struct iovec        Iov;
    UINT1               au1Cmsg[DHCP6_MAX_MTU];    /* For storing Auxillary Data -
                                                   IP6 Packet INFO. */
#endif
    UINT1              *pDhcpPdu = DHCP6_SRV_READ_PDU;
    UINT4               u4BufLen;
#ifdef SLI_WANTED
    INT4                i4AddrLen = sizeof (PeerAddr);
#endif
    INT4                i4DataLen;
    INT4                i4RetVal = 0;

    SET_ADDR_UNSPECIFIED (Ip6Addr);

    Ip6AddrCopy ((tIp6Addr *) (VOID *) PeerAddr.sin6_addr.s6_addr, &Ip6Addr);
    PeerAddr.sin6_port = 0;

    u4BufLen = DHCP6_MAX_MTU;

#ifdef BSDCOMP_SLI_WANTED
    if (setsockopt (DHCP6_SRV_SOCKET_FD, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                    &(i4OptEnable), sizeof (INT4)) < 0)
    {
        return;
    }
#endif

#ifdef SLI_WANTED
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;
    Ip6MsgHdr.msg_controllen = DHCP6_MAX_MTU;

    /* Call recvfrom to receive the packet */
    if ((i4DataLen =
         recvfrom (DHCP6_SRV_SOCKET_FD, (char *) pDhcpPdu,
                   u4BufLen, 0, (struct sockaddr *) &PeerAddr, &i4AddrLen)) < 0)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                       "Dhcp6SrvRcvAndProcessPkt: recvfrom failed\r\n");
    }
    else
    {
        /* Get the interface index from which the packet is received */
        if (recvmsg (DHCP6_SRV_SOCKET_FD, &Ip6MsgHdr, 0) < 0)
        {
            DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Recv Msg Failed\r\n");
            return;
        }
    }
#else
    /* Wait to receive data on the socket */

    /* Using DHCP6_SRV_PDU_SIZE_MAX as the size for pDhcpPdu */
    MEMSET (pDhcpPdu, 0, DHCP6_SRV_PDU_SIZE_MAX);
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    Ip6MsgHdr.msg_name = (void *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (PeerAddr);
    Iov.iov_base = pDhcpPdu;

    /* Using DHCP6_SRV_PDU_SIZE_MAX as the size for pDhcpPdu */
    Iov.iov_len = DHCP6_SRV_PDU_SIZE_MAX;
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (VOID *) &au1Cmsg;
    Ip6MsgHdr.msg_controllen = DHCP6_MAX_MTU;

    pCmsgInfo = CMSG_FIRSTHDR (&Ip6MsgHdr);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
    UNUSED_PARAM (u4BufLen);
    if ((i4DataLen = recvmsg (DHCP6_SRV_SOCKET_FD, &Ip6MsgHdr, 0)) < 0)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC,
                       "Dhcp6SrvRcvAndProcessPkt: recvmsg error\r\n");
        return;
    }
#endif
    pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        DHCP6_SRV_TRC (ALL_FAILURE_TRC, "Pkt Info Failed\r\n");
        return;
    }
    MEMSET (&ParseInfo, 0x00, sizeof (tDhcp6ParseInfo));
    /* store the required values from SOCKET LAYER to Params Structure */
#ifdef BSDCOMP_SLI_WANTED
    NetIpv4GetCfaIfIndexFromPort ((UINT4) pIn6Pktinfo->ipi6_ifindex,
                                  &u4IfIndex);
#else
    u4IfIndex = (UINT4) pIn6Pktinfo->ipi6_ifindex;
#endif
    ParseInfo.udp6Hdr.u2SrcPort = OSIX_NTOHS (PeerAddr.sin6_port);
    ParseInfo.udp6Hdr.u2DstPort = 0;
    ParseInfo.ip6Hdr.u4Len = (UINT4) i4DataLen;
    i4RetVal = (INT4) sizeof (UINT1);

    if (getsockopt (DHCP6_SRV_SOCKET_FD, IPPROTO_IPV6,
                    IPV6_UNICAST_HOPS, &ParseInfo.ip6Hdr.i4Hoplimt,
                    &i4RetVal) < 0)
    {
        DHCP6_SRV_TRC (DHCP6_SRV_ALL_TRC, "DHCP6R : getsockopt failed\n");
        return;
    }

    Ip6AddrCopy (&ParseInfo.ip6Hdr.srcAddr,
                 (tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr);
    Ip6AddrCopy (&ParseInfo.ip6Hdr.dstAddr,
                 (tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr);
    ParseInfo.pu1ReadBuffer = pDhcpPdu;
    ParseInfo.u2ReadLen = 0;

    D6SrParseProcessPkt (&ParseInfo, (UINT4) u4IfIndex);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvProcessQMsg                                        */
/*                                                                           */
/* Description  : Process the queue for Ospfv3 packets                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Dhcp6SrvProcessQMsg (VOID)
{
    tDhcp6SrvQMsg      *pDhcp6QMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;

    while (OsixReceiveFromQ
           ((UINT4) 0, DHCP6_SRV_QUEUE_NAME, OSIX_NO_WAIT, (UINT4) 0,
            &pOsixMsg) == OSIX_SUCCESS)
    {
        pDhcp6QMsg = (tDhcp6SrvQMsg *) pOsixMsg;

        if (pDhcp6QMsg->u4Command == DHCP6_SRV_IPV6_IF_STAT_CHG_EVENT)
        {
            D6SrApirvIfStatusChgHdlr (&(pDhcp6QMsg->unDhcp6SMsgType.
                                        dhcp6SIpIfStatParam));
        }
        else
        {
            DHCP6_SRV_TRC (DHCP6_SRV_ALL_TRC, "Wrong Message Type \r\n");
        }

        DHCP6_SRV_FREE_MEM_MSGQ (pDhcp6QMsg);
    }
}
