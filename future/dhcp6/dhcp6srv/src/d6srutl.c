/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server task.
 * ************************************************************************/

#include "d6srinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvGetInterface                                       */
/*                                                                           */
/* Description  : This function gets the local interface to be used for      */
/*                sending DHCP6 messages to 'au1IpV6Addr' address.           */
/*                                                                           */
/* Input        : au1IpV6Addr - destination Ip-Address.                      */
/*                pu4IfIndex  - pointer to interface number.                 */
/*                                                                           */
/* Output       : pu4IfIndex                                                 */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Dhcp6SrvGetInterface (UINT1 *au1IpV6Addr, UINT4 *pu4IfIndex)
{
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtIPv6Query;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtIPv6Query, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    MEMCPY (RtIPv6Query.Ip6Dst.u1_addr, au1IpV6Addr,
            DHCP6_SRV_IPV6_ADDRESS_SIZE);

    RtIPv6Query.u1Prefixlen = (UINT1) DHCP6_SRV_IPV6_ADDRESS_SIZE;
    if (NetIpv6GetRoute (&RtIPv6Query, &NetIp6RtInfo) == NETIPV6_FAILURE)
    {
        *pu4IfIndex = NetIp6RtInfo.u4Index;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrUtlGetInterfaceAddressIdentifier                       */
/*                                                                           */
/* Description  : This function gets the Server Identifier or Ip-Address of  */
/*                the interface.                                             */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pNetIpv6IfInfo - pointer to Strcture.                      */
/*                                                                           */
/* Output       : au1IpV6Addr                                                */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
D6SrUtlGetInterfaceAddressIdentifier (UINT4 u4IfIndex,
                                      tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    if (NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "D6SrUtlGetInterfaceAddressIdentifier "
                       "Function Return Failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrUtlUpdateServerId                                      */
/*                                                                           */
/* Description  : This function will create the Server Id which is the DUID  */
/*                used by DHCPv6 server module while transmittion of         */
/*                information reply message.                                 */
/*                                                                           */
/* Input        : pPoolInfo   - Pointer to POOL Information.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6SrUtlUpdateServerId (tDhcp6SrvPoolInfo * pPoolInfo)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT4               u4CurrentTimeValue = 0;
    UINT4               u4EnterpriseNumber = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4FirstIfIndex = 0;
    UINT2               u2HadwareType = 0;
    UINT1               u1IdentifierLength = 0;
    UINT1               u1Index = 0;
    UINT1               u1Size = 0;
    UINT1               u1RetVal = OSIX_TRUE;
    UINT1               au1Identifier[DHCP6_SRV_MAX_IDENTIFIER_SIZE];

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (au1Identifier, 0, DHCP6_SRV_MAX_IDENTIFIER_SIZE);

    switch (pPoolInfo->u1DuidType)
    {
        case DHCP6_SRV_DUID_LLT:

            /* 2 Byte Fixed Value 1 
             * 2 Byte Hardware type from the System
             * 4 Byte current Time Value
             * Varible Length Link Layer Address*/

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_ZERO] =
                (UINT1) DHCP6_SRV_VALUE_ZERO;
            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_ONE] =
                (UINT1) DHCP6_SRV_VALUE_ONE;

            /*Get Hadware Type Value */
            D6SrPortGetHadwareType (&u2HadwareType);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_TWO] =
                (UINT1) (u2HadwareType >> DHCP6_SRV_SHIFT_8BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_THREE] =
                (UINT1) u2HadwareType;

            /*Get Current Time Value */
            u4CurrentTimeValue = D6SrUtlGetTimeFromEpoch ();

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_FOUR] =
                (UINT1) (u4CurrentTimeValue >> DHCP6_SRV_SHIFT_24BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_FIVE] =
                (UINT1) (u4CurrentTimeValue >> DHCP6_SRV_SHIFT_16BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_SIX] =
                (UINT1) (u4CurrentTimeValue >> DHCP6_SRV_SHIFT_8BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_SEVEN] =
                (UINT1) u4CurrentTimeValue;

            u1Index = (UINT1) DHCP6_SRV_INDEX_BY_EIGHT;

            if (pPoolInfo->u4DuidIfIndex != 0)
            {
                u4IfIndex = pPoolInfo->u4DuidIfIndex;
            }
            else
            {
                D6SrIfGetFirstOperUpIndex (&u4FirstIfIndex);
                u4IfIndex = u4FirstIfIndex;
            }
            /*Get Link Address  Value */
            if (u4IfIndex != 0)
            {
                if (D6SrUtlGetInterfaceAddressIdentifier
                    (u4IfIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
                {
                    DHCP6_SRV_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                   "D6SrUtlUpdateServerId: "
                                   "D6SrUtlGetInterfaceAddressIdentifier "
                                   "Function Return Failure \r\n");
                    return OSIX_FAILURE;
                }
            }
            else
            {
                MEMSET (&NetIpv6IfInfo.Ip6Addr, 0x00, sizeof (tIp6Addr));
            }
            u1Size = sizeof (tIp6Addr);
            MEMCPY (&pPoolInfo->au1Duid[u1Index],
                    &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
            u1Index = (UINT1) (u1Index + u1Size);
            pPoolInfo->u1DuidLength = u1Index;
            break;
        case DHCP6_SRV_DUID_EN:

            /* 2 Byte Fixed Value 2 
             * 4 Byte enterprise-number from the System
             * Varible Length identifier */

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_ZERO] =
                (UINT1) DHCP6_SRV_VALUE_ZERO;
            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_ONE] =
                (UINT1) DHCP6_SRV_VALUE_TWO;

            /*Get Enterprise Number */
            D6SrPortGetEnterpriseNumber (&u4EnterpriseNumber);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_TWO] =
                (UINT1) (u4EnterpriseNumber >> DHCP6_SRV_SHIFT_24BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_THREE] =
                (UINT1) (u4EnterpriseNumber >> DHCP6_SRV_SHIFT_16BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_FOUR] =
                (UINT1) (u4EnterpriseNumber >> DHCP6_SRV_SHIFT_8BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_FIVE] =
                (UINT1) u4EnterpriseNumber;

            u1Index = (UINT1) DHCP6_SRV_INDEX_BY_SIX;

            D6SrPortGetIdentifier (&u1IdentifierLength, au1Identifier);

            MEMCPY (&pPoolInfo->au1Duid[u1Index], &au1Identifier,
                    u1IdentifierLength);
            u1Index = (UINT1) (u1Index + u1IdentifierLength);
            pPoolInfo->u1DuidLength = u1Index;

            break;
        case DHCP6_SRV_DUID_LL:

            /* 2 Byte Fixed Value 3
             * 2 Byte Hardware type from the System
             * Varible Length Link Layer Address*/

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_ZERO] =
                (UINT1) DHCP6_SRV_VALUE_ZERO;
            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_ONE] =
                (UINT1) DHCP6_SRV_VALUE_THREE;

            /*Get Hadware Type Value */
            D6SrPortGetHadwareType (&u2HadwareType);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_TWO] =
                (UINT1) (u2HadwareType >> DHCP6_SRV_SHIFT_8BITS);

            pPoolInfo->au1Duid[DHCP6_SRV_INDEX_BY_THREE] =
                (UINT1) u2HadwareType;

            u1Index = (UINT1) DHCP6_SRV_INDEX_BY_FOUR;

            if (pPoolInfo->u4DuidIfIndex != 0)
            {
                u4IfIndex = pPoolInfo->u4DuidIfIndex;
            }
            else
            {
                D6SrIfGetFirstOperUpIndex (&u4FirstIfIndex);
                u4IfIndex = u4FirstIfIndex;
            }
            /*Get Link Address  Value */
            if (u4IfIndex != 0)
            {
                if (D6SrUtlGetInterfaceAddressIdentifier
                    (u4IfIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
                {
                    DHCP6_SRV_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                   "D6SrUtlUpdateServerId: "
                                   "D6SrUtlGetInterfaceAddressIdentifier "
                                   "Function Return Failure \r\n");
                    return OSIX_FAILURE;
                }
            }
            else
            {
                MEMSET (&NetIpv6IfInfo.Ip6Addr, 0x00, sizeof (tIp6Addr));
            }
            u1Size = sizeof (tIp6Addr);
            MEMCPY (&pPoolInfo->au1Duid[u1Index],
                    &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
            u1Index = (UINT1) (u1Index + u1Size);
            pPoolInfo->u1DuidLength = u1Index;
            break;
        default:
            u1RetVal = OSIX_FALSE;
            break;
    }
    if (u1RetVal == OSIX_FALSE)
    {
        DHCP6_SRV_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "D6SrUtlUpdateServerId: Function Return Failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrUtlGetTimeFromEpoch                                    */
/*                                                                           */
/* Description  : This function will returns the number of seconds passed    */
/*                from epoch.                                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Number of seconds passed from epoch                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
D6SrUtlGetTimeFromEpoch (VOID)
{
    tOsixSysTime        SysTime;

    OsixGetSysTime (&SysTime);
    return (SysTime);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrUtlGetIfName                                            */
/*                                                                           */
/* Description  : This function gets the Interface name of the gives         */
/*                interface                                                  */
/*                                                                           */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pu1fName - pointer to IP Address.                       */
/*                                                                           */
/* Output       : au1IpV6Addr                                                */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6SrUtlGetIfName (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    if (CfaCliGetIfName (u4IfIndex, (INT1 *) pu1IfName) == NETIPV6_FAILURE)

    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrUtlCliConfGetIfName                                    */
/*                                                                           */
/* Description  : This function gets the Interface name of the gives         */
/*                interface                                                  */
/*                                                                           */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pu1fName - pointer to IP Address.                       */
/*                                                                           */
/* Output       : Interface Name                                             */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6SrUtlCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return OSIX_SUCCESS;
#endif
}
