/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access sub-option table.
 * ************************************************************************/

#include "d6srinc.h"

PRIVATE INT4 D6SrSubOptionRBCmp PROTO ((tRBElem *, tRBElem *));

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionCreateTable
 *
 * DESCRIPTION      : This function creats the sub-option table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrSubOptionCreateTable (VOID)
{
    DHCP6_SRV_SUBOPTION_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvSubOptionInfo, GblRBLink),
                              D6SrSubOptionRBCmp);
    if (DHCP6_SRV_SUBOPTION_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrSubOptionCreateTable: "
                       " RB Tree Creation failed \r\n");
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionDeleteTable
 *
 * DESCRIPTION      : This function Deletes the sub-option table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrSubOptionDeleteTable (VOID)
{
    if (DHCP6_SRV_SUBOPTION_TABLE != NULL)
    {
        D6SrSubOptionDrainTable ();
        RBTreeDestroy (DHCP6_SRV_SUBOPTION_TABLE, NULL, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the sub-option table. 
 *                    Indices of this table are -
 *                    o Pool Index
 *                    o Option Index
 *                    o SubOption Type
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrSubOptionRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvSubOptionInfo *pSubOptionEntryA = NULL;
    tDhcp6SrvSubOptionInfo *pSubOptionEntryB = NULL;

    pSubOptionEntryA = (tDhcp6SrvSubOptionInfo *) pRBElem1;
    pSubOptionEntryB = (tDhcp6SrvSubOptionInfo *) pRBElem2;

    if (pSubOptionEntryA->pOption->pPool->u4PoolIndex !=
        pSubOptionEntryB->pOption->pPool->u4PoolIndex)
    {
        if (pSubOptionEntryA->pOption->pPool->u4PoolIndex <
            pSubOptionEntryB->pOption->pPool->u4PoolIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }

    if (pSubOptionEntryA->pOption->u4OptionIndex !=
        pSubOptionEntryB->pOption->u4OptionIndex)
    {
        if (pSubOptionEntryA->pOption->u4OptionIndex <
            pSubOptionEntryB->pOption->u4OptionIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }

    if (pSubOptionEntryA->u2Type != pSubOptionEntryB->u2Type)
    {
        if (pSubOptionEntryA->u2Type < pSubOptionEntryB->u2Type)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionCreateNode 
 *
 * DESCRIPTION      : This function creates a sub-option table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvSubOptionInfo * - Pointer to the created sub-option node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvSubOptionInfo *
D6SrSubOptionCreateNode (VOID)
{
    tDhcp6SrvSubOptionInfo *pSubOptionInfo = NULL;

    DHCP6_SRV_ALLOC_MEM_SUBOPTION (pSubOptionInfo);

    if (pSubOptionInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrSubOptionCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }

    MEMSET (pSubOptionInfo, 0x00, sizeof (tDhcp6SrvSubOptionInfo));

    return pSubOptionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionAddNode
 *
 * DESCRIPTION      : This function add a node to the sub-option table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pSubOptionInfo - pointer to sub-option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrSubOptionAddNode (tDhcp6SrvSubOptionInfo * pSubOptionInfo)
{
    if (RBTreeAdd (DHCP6_SRV_SUBOPTION_TABLE, pSubOptionInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrSubOptionAddNode: " " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }

    /* Add in the node  in option sub-option list also */
    D6SrOptionSubOptionListAddNode (pSubOptionInfo);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionDelNode
 *
 * DESCRIPTION      : This function delete a sub-option node from the
 *                    sub-option table and release the memory used by that
 *                    node to the memory sub-option. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pSubOptionInfo - pointer to sub-option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrSubOptionDelNode (tDhcp6SrvSubOptionInfo * pSubOptionInfo)
{
    /* Remove the sub-option node from option list */
    D6SrOptionSubOptionListDelNode (pSubOptionInfo);

    RBTreeRem (DHCP6_SRV_SUBOPTION_TABLE, pSubOptionInfo);

    DHCP6_SRV_FREE_MEM_SUBOPTION (pSubOptionInfo);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a sub-option node, and then release the
 *                    memory used by the node to the memory sub-option.
 *
 * INPUT            : pRBElem - pointer to the sub-option node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrSubOptionRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    /* Remove the sub-option node from option list */
    D6SrOptionSubOptionListDelNode ((tDhcp6SrvSubOptionInfo *) pRBElem);

    DHCP6_SRV_FREE_MEM_SUBOPTION (pRBElem);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionDrainTable
 *
 * DESCRIPTION      : This function deletes all the sub-option nodes
 *                    associated with the sub-option table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrSubOptionDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_SUBOPTION_TABLE, D6SrSubOptionRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionGetFirstNode
 *
 * DESCRIPTION      : This function returns the first sub-option node
 *                    from the sub-option table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvSubOptionInfo * - pointer to the first sub-option info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvSubOptionInfo *
D6SrSubOptionGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_SUBOPTION_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionGetNextNode
 *
 * DESCRIPTION      : This function returns the next sub-option info
 *                    from the sub-option table.
 *
 * INPUT            : pCurrentSubOptionInfo - pointer to the sub-option info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvSubOptionInfo * - Pointer to the next sub-option info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvSubOptionInfo *
D6SrSubOptionGetNextNode (tDhcp6SrvSubOptionInfo * pCurrentSubOptionInfo)
{
    return RBTreeGetNext (DHCP6_SRV_SUBOPTION_TABLE, pCurrentSubOptionInfo,
                          NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrSubOptionGetNode
 *
 * DESCRIPTION      : This function helps to find a sub-option node from the
 * sub-option 
 *                    table.
 *
 * INPUT            : u4SubOptionType - SubOption Type
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvSubOptionInfo * - Pointer to the next sub-option info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvSubOptionInfo *
D6SrSubOptionGetNode (UINT4 u4PoolIndex, UINT4 u4OptionIndex,
                      UINT2 u4SubOptionType)
{
    tDhcp6SrvSubOptionInfo Key;

    Key.u2Type = u4SubOptionType;
    Key.pOption = D6SrOptionGetNode (u4PoolIndex, u4OptionIndex);

    if (Key.pOption == NULL)
    {
        return NULL;
    }

    return RBTreeGet (DHCP6_SRV_SUBOPTION_TABLE, &Key);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srsopt.c                      */
/*-----------------------------------------------------------------------*/
