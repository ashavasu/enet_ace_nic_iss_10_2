/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstst.h
 *
 * Description: Header file for DHCP6C test module
 *********************************************************************/
#ifndef _DHC6S_TEST_H_
#define _DHC6S_TEST_H_

/* Test function prototypes */

VOID D6SrTestExecUT (INT4 i4UtId);

#endif /* _DHC6S_TEST_H_ */
