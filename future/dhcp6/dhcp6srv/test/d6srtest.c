/********************************************************************
 *  Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * *
 * * $Id: d6srtest.c,v 1.3 2009/10/07 05:46:32 prabuc Exp $
 * *
 * * Description: DHCP6 Test routines. Mainly used for debugging purpose.
 * * NOTE: This file should not be include in release packaging.
 * ***********************************************************************/
#ifndef _DHC6S_TEST_C_
#define _DHC6S_TEST_C_

#include "d6srinc.h"
#include "d6srtest.h"

PRIVATE INT1        D6SrValidateAllDepObject (VOID);
/* FsDhcp6SrvIfTable. */
PRIVATE INT1        D6SrValidateAllIfSetObject (VOID);
PRIVATE INT1        D6SrValidateAllIfGetObject (VOID);
PRIVATE INT1        D6SrValidateAllIfTestObject (VOID);
/* FsDhcp6SrvPoolTable. */
PRIVATE INT1        D6SrValidateAllPoolSetObject (VOID);
PRIVATE INT1        D6SrValidateAllPoolGetObject (VOID);
PRIVATE INT1        D6SrValidateAllPoolTestObject (VOID);
/* FsDhcp6IncludePrefixTable.. */
PRIVATE INT1        D6SrValidateAllInPrefixSetObject (VOID);
PRIVATE INT1        D6SrValidateAllInPrefixGetObject (VOID);
PRIVATE INT1        D6SrValidateAllInPrefixPoolTestObject (VOID);
/* --------------------------------- */
PRIVATE INT1        D6SrvScalarObjects (VOID);
PRIVATE INT1        D6SrvGenerateTrap (VOID);
/* --------------Pop Up ------------- */
PRIVATE INT1        D6SrvPoolTablePopUp (VOID);
PRIVATE INT1        D6SrvIfTablePopUp (VOID);
PRIVATE INT1        D6SrvOptionTablePopUp (VOID);
PRIVATE INT1        D6SrvSubOptionTablePopUp (VOID);
PRIVATE INT1        D6SrvRealmTablePopUp (VOID);
PRIVATE INT1        D6SrvKeyTablePopUp (VOID);
PRIVATE INT1        D6SrvClientTablePopUp (VOID);
PRIVATE INT1        D6SrvInPrefixTablePopUp (VOID);
PRIVATE INT1        D6SrvParsingPdu (VOID);

VOID
D6SrTestExecUT (INT4 i4UtId)
{
    INT1                i1RetVal = OSIX_FAILURE;

    switch (i4UtId)
    {
        case 0:
            if (D6SrValidateAllDepObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 1:
            if (D6SrValidateAllIfSetObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 2:
            if (D6SrValidateAllIfGetObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 3:
            if (D6SrValidateAllIfTestObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 4:
            if (D6SrValidateAllPoolSetObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 5:
            if (D6SrValidateAllPoolGetObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 6:
            if (D6SrValidateAllPoolTestObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 7:
            if (D6SrValidateAllInPrefixSetObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 8:
            if (D6SrValidateAllInPrefixGetObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 9:
            if (D6SrValidateAllInPrefixPoolTestObject () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 10:
            if (D6SrvScalarObjects () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 11:
            if (D6SrvGenerateTrap () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            break;
        case 100:
            /* Pool Table */
            if (D6SrvPoolTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            /* IF Table */
            if (D6SrvIfTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            /* Option Table */
            if (D6SrvOptionTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            /* Sub Option Table */
            if (D6SrvSubOptionTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            if (D6SrvRealmTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            if (D6SrvKeyTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            if (D6SrvClientTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            if (D6SrvInPrefixTablePopUp () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
            }
            else
            {
                i1RetVal = OSIX_FAILURE;
                break;
            }
            break;
        case 101:
            if (D6SrvParsingPdu () == OSIX_TRUE)
            {
                i1RetVal = OSIX_SUCCESS;
                break;
            }
        default:
            printf ("%% No test case is defined for ID: %d\r\n", i4UtId);
            break;
    }
    /* Display the result */
    if (i1RetVal == OSIX_FAILURE)
    {
        printf ("UNIT TEST ID: %-2d %-2s\r\n", i4UtId, "FAILED");
    }
    else
    {
        printf ("UNIT TEST ID: %-2d %-2s\r\n", i4UtId, "PASSED");
    }
}

PRIVATE INT1
D6SrValidateAllDepObject ()
{
    tSnmpIndexList     *pSnmpIndexList = NULL;
    tSNMP_VAR_BIND     *pSnmpVarBind = NULL;
    UINT4               u4ErrorCode;

    nmhDepv2FsDhcp6SrvDebugTrace (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6SrvTrapAdminControl (&u4ErrorCode, pSnmpIndexList,
                                        pSnmpVarBind);
    nmhDepv2FsDhcp6SrvSysLogAdminStatus (&u4ErrorCode, pSnmpIndexList,
                                         pSnmpVarBind);
    nmhDepv2FsDhcp6SrvInfoRequestListenPort (&u4ErrorCode, pSnmpIndexList,
                                             pSnmpVarBind);
    nmhDepv2FsDhcp6SrvRlyForwardListenPort (&u4ErrorCode, pSnmpIndexList,
                                            pSnmpVarBind);
    nmhDepv2FsDhcp6SrvReplyTransmitPort (&u4ErrorCode, pSnmpIndexList,
                                         pSnmpVarBind);
    nmhDepv2FsDhcp6SrvRlyReplyTransmitPort (&u4ErrorCode, pSnmpIndexList,
                                            pSnmpVarBind);
    nmhDepv2FsDhcp6SrvPoolTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6IncludePrefixTable (&u4ErrorCode, pSnmpIndexList,
                                       pSnmpVarBind);
    nmhDepv2FsDhcp6SrvOptionTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6SrvSubOptionTable (&u4ErrorCode, pSnmpIndexList,
                                      pSnmpVarBind);
    nmhDepv2FsDhcp6SrvClientTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6SrvRealmTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6SrvKeyTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6SrvIfTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);

    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllIfSetObject ()
{
    /* IfIndex == 3
     * IfPoolIndex == 6
     * Counter Reset == 3
     * */
    if (nmhSetFsDhcp6SrvIfPool (3, 6) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvIfCounterReset (3, 3) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvIfRowStatus (3, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvIfRowStatus (3, 2) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllIfGetObject ()
{
    UINT4               u4IfInformIn;
    UINT4               u4IfRelayForwIn;
    UINT4               u4IfRelayReplyOut;
    UINT4               u4IfInvalidPktIn;
    UINT4               u4IfHmacFailCount;
    INT4                i4IfUnknownTlvType;
    INT4                i4IfCounterReset;
    INT4                i4IfRowStatus;
    INT4                i4First;
    INT4                i4Next;
    INT4                i4PoolIndex;

    if (nmhValidateIndexInstanceFsDhcp6SrvIfTable (6) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFirstIndexFsDhcp6SrvIfTable (&i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6SrvIfTable (i4First, &i4Next) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6SrvIfTable (1, &i4Next) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfPool (6, &i4PoolIndex) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfInformIn (6, &u4IfInformIn) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfRelayForwIn (6, &u4IfRelayForwIn) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfRelayReplyOut (6, &u4IfRelayReplyOut) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfInvalidPktIn (6, &u4IfInvalidPktIn) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfUnknownTlvType (6, &i4IfUnknownTlvType) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfHmacFailCount (6, &u4IfHmacFailCount) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfCounterReset (6, &i4IfCounterReset) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvIfRowStatus (6, &i4IfRowStatus) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllIfTestObject ()
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsDhcp6SrvIfPool (&u4ErrorCode, 6, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfCounterReset (&u4ErrorCode, 6, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, 6, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, 6, 2) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, 6, 3) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, 6, 4) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, 6, 7) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllPoolSetObject ()
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE SrvDuid;
    UINT4               u4PoolIndex = 1;
    UINT1               au1PoolName[] = "pool1";
    UINT1               au1SrvDuid[15];

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PoolName.i4_Length = STRLEN (au1PoolName);
    PoolName.pu1_OctetList = au1PoolName;

    MEMSET (&SrvDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SrvDuid, 0, sizeof (au1SrvDuid));

    SPRINTF ((CHR1 *) au1SrvDuid, "%s", "ServerDuid");

    SrvDuid.i4_Length = STRLEN (au1SrvDuid);
    SrvDuid.pu1_OctetList = au1SrvDuid;
    /* IfIndex == 6
     * PoolName == pool1
     * PoolPreference == 1
     * PoolDuidType == 1
     * */

    if (nmhSetFsDhcp6SrvPoolName (u4PoolIndex, &PoolName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvPoolPreference (u4PoolIndex, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvPoolDuidType (u4PoolIndex, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvPoolDuid (u4PoolIndex, &SrvDuid) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvPoolDuidIfIndex (u4PoolIndex, 6) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, 2) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllPoolGetObject ()
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE Duid;
    UINT4               u4PoolIndex = 1;
    UINT4               u4First;
    UINT4               u4Next;
    UINT4               u4NextOptionIndex;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT1               au1Duid[DHCP6_SRV_DUID_SIZE_MAX];
    INT4                i4Preference;
    INT4                i4DuidType;
    INT4                i4IfIndex;
    INT4                i4RowStatus;

    MEMSET (au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);
    PoolName.pu1_OctetList = au1PoolName;

    MEMSET (au1Duid, 0, DHCP6_SRV_DUID_SIZE_MAX);
    Duid.pu1_OctetList = au1Duid;

    if (nmhValidateIndexInstanceFsDhcp6SrvPoolTable (u4PoolIndex) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6SrvPoolTable (u4First, &u4Next) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6SrvPoolTable (u4PoolIndex, &u4Next) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolName (u4PoolIndex, &PoolName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolPreference (u4PoolIndex, &i4Preference) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolDuidType (u4PoolIndex, &i4DuidType) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolDuid (u4PoolIndex, &Duid) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolDuidIfIndex (u4PoolIndex, &i4IfIndex) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolOptionTableNextIndex
        (u4PoolIndex, &u4NextOptionIndex) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6SrvPoolRowStatus (u4PoolIndex, &i4RowStatus) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllPoolTestObject ()
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE SrvDuid;
    UINT4               u4PoolIndex = 1;
    UINT4               u4ErrorCode;
    UINT1               au1PoolName[] = "pool1";
    UINT1               au1SrvDuid[15];

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PoolName.i4_Length = STRLEN (au1PoolName);
    PoolName.pu1_OctetList = au1PoolName;

    MEMSET (&SrvDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SrvDuid, 0, sizeof (au1SrvDuid));

    SPRINTF ((CHR1 *) au1SrvDuid, "%s", "ServerDuid");

    SrvDuid.i4_Length = STRLEN (au1SrvDuid);
    SrvDuid.pu1_OctetList = au1SrvDuid;

    if (nmhTestv2FsDhcp6SrvPoolName (&u4ErrorCode, u4PoolIndex, &PoolName) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolPreference (&u4ErrorCode, u4PoolIndex, 1) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolDuidType (&u4ErrorCode, u4PoolIndex, 1) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolDuid (&u4ErrorCode, u4PoolIndex, &SrvDuid) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolDuidIfIndex (&u4ErrorCode, u4PoolIndex, 6) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex, 1) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex, 2) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex, 3) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4PoolIndex, 4) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4PoolIndex, 5) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4PoolIndex, 6) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4PoolIndex, 7) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllInPrefixSetObject ()
{
    tSNMP_OCTET_STRING_TYPE InPrefix;
    UINT4               u4PoolIndex = 1;
    UINT1               au1InPrefix[] = "3ff1:1::";
    INT4                i4ContextId = 0;

    MEMSET (&InPrefix, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPrefix.i4_Length = STRLEN (au1InPrefix);
    InPrefix.pu1_OctetList = au1InPrefix;

    if (nmhSetFsDhcp6IncludePrefixPool (i4ContextId, &InPrefix, u4PoolIndex) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 1) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 2) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 3) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 4) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 5) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 6) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 7) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllInPrefixGetObject ()
{
    tSNMP_OCTET_STRING_TYPE InPrefix;
    tSNMP_OCTET_STRING_TYPE InPrefixNext;
    UINT1               au1InPrefix[DHCP6_SRV_IPV6_ADDRESS_SIZE];
    UINT1               au1InPrefixNext[DHCP6_SRV_IPV6_ADDRESS_SIZE];
    INT4                i4RowStatus;
    INT4                i4ContextId = 0;
    INT4                i4PoolIndex;
    INT4                i4First;
    INT4                i4Next;

    MEMSET (au1InPrefix, 0, DHCP6_SRV_IPV6_ADDRESS_SIZE);
    InPrefix.pu1_OctetList = au1InPrefix;

    MEMSET (au1InPrefixNext, 0, DHCP6_SRV_IPV6_ADDRESS_SIZE);
    InPrefixNext.pu1_OctetList = au1InPrefixNext;

    if (nmhValidateIndexInstanceFsDhcp6IncludePrefixTable
        (i4ContextId, &InPrefix) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFirstIndexFsDhcp6IncludePrefixTable (&i4First, &InPrefix) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6IncludePrefixTable
        (i4First, &i4Next, &InPrefix, &InPrefixNext) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (nmhGetFsDhcp6IncludePrefixPool (i4First, &InPrefix, &i4PoolIndex) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6IncludePrefixRowStatus (i4First, &InPrefix, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrValidateAllInPrefixPoolTestObject ()
{
    tSNMP_OCTET_STRING_TYPE InPrefix;
    UINT4               u4ErrorCode;
    INT4                i4ContextId = 0;
    INT4                i4PoolIndex = 1;

    MEMSET (&InPrefix, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    InPrefix.i4_Length = 3;

    if (nmhTestv2FsDhcp6IncludePrefixPool
        (&u4ErrorCode, i4ContextId, &InPrefix, i4PoolIndex) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 2) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 3) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 4) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 5) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 6) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6IncludePrefixRowStatus
        (&u4ErrorCode, i4ContextId, &InPrefix, 7) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvScalarObjects ()
{
    tSNMP_OCTET_STRING_TYPE Trace;
    tSNMP_OCTET_STRING_TYPE TrapControl;
    UINT4               u4RealmIndex;
    UINT4               u4ClientIndex;
    UINT4               u4PoolIndex;
    UINT4               u4ErrorCode;
    INT4                i4Syslog;
    INT4                i4ListenPort;
    INT4                i4ForwardListenPort;
    INT4                i4ReplyTransmitPort;
    INT4                i4RlyReplyTransmitPort;
    UINT1               au1Trace[DHCP6_SRV_TRC_MAX_SIZE];
    UINT1               au1TrapControl[DHCP6_SRV_TRAP_CONTROL_MAX];

    MEMSET (au1Trace, 0, DHCP6_SRV_TRC_MAX_SIZE);
    Trace.pu1_OctetList = au1Trace;

    MEMSET (au1TrapControl, 0, DHCP6_SRV_TRAP_CONTROL_MAX);
    TrapControl.pu1_OctetList = au1TrapControl;

    nmhGetFsDhcp6SrvDebugTrace (&Trace);
    Trace.pu1_OctetList[Trace.i4_Length] = '\0';
    printf ("Trace : %s \r\n", Trace.pu1_OctetList);

    nmhGetFsDhcp6SrvRealmTableNextIndex (&u4RealmIndex);
    printf ("RealIndex : %d \r\n", u4RealmIndex);

    nmhGetFsDhcp6SrvClientTableNextIndex (&u4ClientIndex);
    printf ("ClientIndex : %d \r\n", u4ClientIndex);

    nmhGetFsDhcp6SrvPoolTableNextIndex (&u4PoolIndex);
    printf ("PoolIndex : %d \r\n", u4PoolIndex);

    nmhGetFsDhcp6SrvTrapAdminControl (&TrapControl);
    TrapControl.pu1_OctetList[TrapControl.i4_Length] = '\0';
    printf ("TrapControl : %s \r\n", TrapControl.pu1_OctetList);

    nmhGetFsDhcp6SrvSysLogAdminStatus (&i4Syslog);
    printf ("Syslog : %d \r\n", i4Syslog);

    nmhGetFsDhcp6SrvInfoRequestListenPort (&i4ListenPort);
    printf ("ListenPort : %d \r\n", i4ListenPort);

    nmhGetFsDhcp6SrvRlyForwardListenPort (&i4ForwardListenPort);
    printf ("ForwardListenPort : %d \r\n", i4ForwardListenPort);

    nmhGetFsDhcp6SrvReplyTransmitPort (&i4ReplyTransmitPort);
    printf ("ReplyTransmitPort : %d \r\n", i4ReplyTransmitPort);

    nmhGetFsDhcp6SrvRlyReplyTransmitPort (&i4RlyReplyTransmitPort);
    printf ("RlyReplyTransmitPort : %d \r\n", i4RlyReplyTransmitPort);

    SPRINTF ((CHR1 *) au1Trace, "%s", "disable ");
    Trace.pu1_OctetList = au1Trace;
    Trace.i4_Length = STRLEN (Trace.pu1_OctetList);
    if (nmhTestv2FsDhcp6SrvDebugTrace (&u4ErrorCode, &Trace) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    SPRINTF ((CHR1 *) au1Trace, "%s", "enable ");
    Trace.pu1_OctetList = au1Trace;
    Trace.i4_Length = 4;
    if (nmhTestv2FsDhcp6SrvDebugTrace (&u4ErrorCode, &Trace) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvDebugTrace (&Trace);

    SPRINTF ((CHR1 *) au1TrapControl, "%d", 4);
    TrapControl.pu1_OctetList = au1TrapControl;
    if (nmhTestv2FsDhcp6SrvTrapAdminControl (&u4ErrorCode, &TrapControl) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    au1TrapControl[0] = 1;
    TrapControl.pu1_OctetList = au1TrapControl;
    if (nmhTestv2FsDhcp6SrvTrapAdminControl (&u4ErrorCode, &TrapControl) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvTrapAdminControl (&TrapControl);
    D6SrvGenerateTrap ();

    if (nmhTestv2FsDhcp6SrvSysLogAdminStatus (&u4ErrorCode, 3) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvSysLogAdminStatus (&u4ErrorCode, 1) == SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvSysLogAdminStatus (1);

    if (nmhTestv2FsDhcp6SrvInfoRequestListenPort (&u4ErrorCode, 11) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvInfoRequestListenPort (&u4ErrorCode, 2) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvInfoRequestListenPort (2);

    if (nmhTestv2FsDhcp6SrvRlyForwardListenPort (&u4ErrorCode, 11) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvRlyForwardListenPort (&u4ErrorCode, 3) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvRlyForwardListenPort (3);

    if (nmhTestv2FsDhcp6SrvReplyTransmitPort (&u4ErrorCode, 11) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvReplyTransmitPort (&u4ErrorCode, 4) == SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvReplyTransmitPort (4);

    if (nmhTestv2FsDhcp6SrvRlyReplyTransmitPort (&u4ErrorCode, 11) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6SrvRlyReplyTransmitPort (&u4ErrorCode, 5) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6SrvRlyReplyTransmitPort (5);

    printf ("\r\n ----------------- After Input ------------- \r\n");

    nmhGetFsDhcp6SrvDebugTrace (&Trace);
    Trace.pu1_OctetList[Trace.i4_Length] = '\0';
    printf ("Trace : %s \r\n", Trace.pu1_OctetList);

    nmhGetFsDhcp6SrvRealmTableNextIndex (&u4RealmIndex);
    printf ("RealIndex : %d \r\n", u4RealmIndex);

    nmhGetFsDhcp6SrvClientTableNextIndex (&u4ClientIndex);
    printf ("ClientIndex : %d \r\n", u4ClientIndex);

    nmhGetFsDhcp6SrvPoolTableNextIndex (&u4PoolIndex);
    printf ("PoolIndex : %d \r\n", u4PoolIndex);

    nmhGetFsDhcp6SrvTrapAdminControl (&TrapControl);
    TrapControl.pu1_OctetList[Trace.i4_Length] = '\0';
    printf ("TrapControl : %s \r\n", TrapControl.pu1_OctetList);

    nmhGetFsDhcp6SrvSysLogAdminStatus (&i4Syslog);
    printf ("Syslog : %d \r\n", i4Syslog);

    nmhGetFsDhcp6SrvInfoRequestListenPort (&i4ListenPort);
    printf ("ListenPort : %d \r\n", i4ListenPort);

    nmhGetFsDhcp6SrvRlyForwardListenPort (&i4ForwardListenPort);
    printf ("ForwardListenPort : %d \r\n", i4ForwardListenPort);

    nmhGetFsDhcp6SrvReplyTransmitPort (&i4ReplyTransmitPort);
    printf ("ReplyTransmitPort : %d \r\n", i4ReplyTransmitPort);

    nmhGetFsDhcp6SrvRlyReplyTransmitPort (&i4RlyReplyTransmitPort);
    printf ("RlyReplyTransmitPort : %d \r\n", i4RlyReplyTransmitPort);

    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvGenerateTrap ()
{
    tDhcp6SInvalidMsgRxdTrap Dhcp6CInvalidMsgRxdTrap;
    tDhcp6SHmacFailMsgRxdTrap Dhcp6CHmacFailMsgRxdTrap;

    Dhcp6CInvalidMsgRxdTrap.u4IfIndex = 10;
    Dhcp6CHmacFailMsgRxdTrap.u4IfIndex = 10;

    Dhcp6SSnmpSendTrap (&Dhcp6CInvalidMsgRxdTrap, 1);
    Dhcp6SSnmpSendTrap (&Dhcp6CHmacFailMsgRxdTrap, 2);

    return OSIX_TRUE;
}

/* ---------------------------------------------------------------------- */
PRIVATE INT1
D6SrvPoolTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE SrvDuid;
    UINT4               u4PoolIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrorCode;
    UINT1               au1PoolName[10];
    UINT1               au1SrvDuid[15];
    INT4                i = 0;

    for (i = 1; i <= 10; i++)
    {
        MEMSET (au1PoolName, 0, sizeof (au1PoolName));
        MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

        u4PoolIndex = i;
        u4IfIndex = i;

        SPRINTF ((CHR1 *) au1PoolName, "%s%d", "pool", i);

        PoolName.i4_Length = STRLEN (au1PoolName);
        PoolName.pu1_OctetList = au1PoolName;

        MEMSET (&SrvDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1SrvDuid, 0, sizeof (au1SrvDuid));

        SPRINTF ((CHR1 *) au1SrvDuid, "%s%d", "ServerDuid", i);

        SrvDuid.i4_Length = STRLEN (au1SrvDuid);
        SrvDuid.pu1_OctetList = au1SrvDuid;

        if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex, 5) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, 5) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvPoolName (&u4ErrorCode, u4PoolIndex, &PoolName)
            == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolName (u4PoolIndex, &PoolName) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvPoolPreference (&u4ErrorCode, u4PoolIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolPreference (u4PoolIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvPoolDuid (&u4ErrorCode, u4PoolIndex, &SrvDuid) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolDuid (u4PoolIndex, &SrvDuid) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvPoolDuidType (&u4ErrorCode, u4PoolIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolDuidType (u4PoolIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolDuidIfIndex (u4PoolIndex, u4IfIndex) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvIfTablePopUp ()
{
    UINT4               u4IfIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4ErrorCode;
    INT4                i = 0;

    for (i = 1; i <= 10; i++)
    {
        u4IfIndex = i;
        u4PoolIndex = i;

        if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4IfIndex, 5) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex, 5) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvIfPool (&u4ErrorCode, u4IfIndex, u4PoolIndex) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvIfPool (u4IfIndex, u4PoolIndex) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvIfCounterReset (&u4ErrorCode, u4IfIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvIfCounterReset (u4IfIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4IfIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvOptionTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT4               u4OptionIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4ErrorCode;
    INT4                i4Length = 0;
    INT4                i = 0;

    UINT1               au1OptionValue[10];

    for (i = 1; i <= 3; i++)
    {
        u4OptionIndex = i;
        u4PoolIndex = i;

        MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1OptionValue, 0, sizeof (au1OptionValue));

        SPRINTF ((CHR1 *) au1OptionValue, "%s%d", "aricent", i);

        OptionValue.i4_Length = STRLEN (au1OptionValue);
        OptionValue.pu1_OctetList = au1OptionValue;

        i4Length = OptionValue.i4_Length;

        if (nmhTestv2FsDhcp6SrvOptionRowStatus
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex, 5) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, 5) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvOptionType
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex, i) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvOptionType (u4PoolIndex, u4OptionIndex, i) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvOptionLength
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex,
             i4Length) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvOptionLength (u4PoolIndex, u4OptionIndex, i4Length)
            == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvOptionValue
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex,
             &OptionValue) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvOptionValue
            (u4PoolIndex, u4OptionIndex, &OptionValue) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvOptionPreference
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvOptionPreference (u4PoolIndex, u4OptionIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhTestv2FsDhcp6SrvOptionRowStatus
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex, 1) == SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
        if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, 1) ==
            SNMP_FAILURE)
        {
            return OSIX_FALSE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvSubOptionTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT4               u4OptionIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4OptionType = 0;
    UINT4               u4ErrorCode;
    INT4                i4Length = 0;
    UINT1               au1OptionValue[10];
    INT4                i = 0;

    for (i = 1; i <= 3; i++)
    {
        u4OptionIndex = i;
        u4PoolIndex = i;
        u4OptionType = i;

        MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1OptionValue, 0, sizeof (au1OptionValue));

        SPRINTF ((CHR1 *) au1OptionValue, "%s%d", "future", i);

        OptionValue.i4_Length = STRLEN (au1OptionValue);
        OptionValue.pu1_OctetList = au1OptionValue;

        i4Length = OptionValue.i4_Length;

        if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrorCode, u4PoolIndex,
                                                   u4OptionIndex, u4OptionType,
                                                   5) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvSubOptionRowStatus
            (u4PoolIndex, u4OptionIndex, u4OptionType, 5) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvSubOptionLength
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex, u4OptionType,
             i4Length) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvSubOptionLength
            (u4PoolIndex, u4OptionIndex, u4OptionType,
             i4Length) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvSubOptionValue
            (&u4ErrorCode, u4PoolIndex, u4OptionIndex, u4OptionType,
             &OptionValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvSubOptionValue
            (u4PoolIndex, u4OptionIndex, u4OptionType,
             &OptionValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrorCode, u4PoolIndex,
                                                   u4OptionIndex, u4OptionType,
                                                   1) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvSubOptionRowStatus
            (u4PoolIndex, u4OptionIndex, u4OptionType, 1) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvRealmTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    UINT4               u4Realmndex = 0;
    UINT4               u4ErrorCode;
    UINT1               au1RealmName[10];
    INT4                i4Length = 0;
    INT4                i = 0;

    for (i = 1; i <= 3; i++)
    {
        u4Realmndex = i;

        MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RealmName, 0, sizeof (au1RealmName));

        SPRINTF ((CHR1 *) au1RealmName, "%s%d", "Realm", i);

        RealmName.i4_Length = STRLEN (au1RealmName);
        RealmName.pu1_OctetList = au1RealmName;

        i4Length = RealmName.i4_Length;

        if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrorCode, u4Realmndex, 5) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvRealmRowStatus (u4Realmndex, 5) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvRealmName (&u4ErrorCode, u4Realmndex, &RealmName)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvRealmName (u4Realmndex, &RealmName) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrorCode, u4Realmndex, 1) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvRealmRowStatus (u4Realmndex, 1) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvKeyTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE Key;
    UINT4               u4Realmndex = 0;
    UINT4               u4KeyIndex = 0;
    UINT4               u4ErrorCode;
    UINT1               au1Key[15];
    INT4                i = 0;

    for (i = 1; i <= 3; i++)
    {
        u4Realmndex = i;
        u4KeyIndex = i;

        MEMSET (&Key, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1Key, 0, sizeof (au1Key));

        SPRINTF ((CHR1 *) au1Key, "%s%d", "AricentKey", i);

        Key.i4_Length = STRLEN (au1Key);
        Key.pu1_OctetList = au1Key;

        if (nmhTestv2FsDhcp6SrvKeyRowStatus
            (&u4ErrorCode, u4Realmndex, u4KeyIndex, 5) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvKeyRowStatus (u4Realmndex, u4KeyIndex, 5) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvKey (&u4ErrorCode, u4Realmndex, u4KeyIndex, &Key)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvKey (u4Realmndex, u4KeyIndex, &Key) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvKeyRowStatus
            (&u4ErrorCode, u4Realmndex, u4KeyIndex, 1) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvKeyRowStatus (u4Realmndex, u4KeyIndex, 1) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvClientTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE ClientId;
    UINT4               u4ClntIndex = 0;
    UINT4               u4Realmndex = 0;
    UINT4               u4ErrorCode;
    UINT1               au1ClientId[10];
    INT4                i = 0;

    for (i = 1; i <= 3; i++)
    {
        u4Realmndex = i;
        u4ClntIndex = i;

        MEMSET (&ClientId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1ClientId, 0, sizeof (au1ClientId));

        SPRINTF ((CHR1 *) au1ClientId, "%s%d", "Client1", i);

        ClientId.i4_Length = STRLEN (au1ClientId);
        ClientId.pu1_OctetList = au1ClientId;

        if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrorCode, u4ClntIndex, 5) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, 5) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvClientId (&u4ErrorCode, u4ClntIndex, &ClientId)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvClientId (u4ClntIndex, &ClientId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvClientIdType (&u4ErrorCode, u4ClntIndex, 1) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvClientIdType (u4ClntIndex, 1) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvClientRealm
            (&u4ErrorCode, u4ClntIndex, u4Realmndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvClientRealm (u4ClntIndex, u4Realmndex) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrorCode, u4ClntIndex, 1) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, 1) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvInPrefixTablePopUp ()
{
    tSNMP_OCTET_STRING_TYPE InPrefix;
    UINT4               u4PoolIndex = 1;
    UINT1               au1InPrefix[16];
    INT4                i4ContextId = 0;

    MEMSET (au1InPrefix, 0, sizeof (tIp6Addr));

    au1InPrefix[0] = 3;
    au1InPrefix[1] = 15;
    au1InPrefix[2] = 15;
    au1InPrefix[3] = 14;
    au1InPrefix[15] = 1;

    MEMSET (&InPrefix, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPrefix.i4_Length = 16;
    InPrefix.pu1_OctetList = au1InPrefix;

    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 5) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixPool (i4ContextId, &InPrefix, u4PoolIndex) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6IncludePrefixRowStatus (i4ContextId, &InPrefix, 1) ==
        SNMP_FAILURE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE INT1
D6SrvParsingPdu ()
{
    tDhcp6SrvUdp6Params Udp6Dhcp6sParams;
    UINT4               u4Var;
    UINT2               u2Var;
    UINT1               u1Var;
    UINT1               au1SrvPdu[150];
    UINT1              *pu1SendBuf = NULL;

    MEMSET (au1SrvPdu, 0, 150);

    Udp6Dhcp6sParams.u4Index = 1;
    Udp6Dhcp6sParams.u4Len = 132;

    MEMSET (&Udp6Dhcp6sParams.dstAddr, 0, sizeof (tIp6Addr));

    Udp6Dhcp6sParams.dstAddr.ip6_addr_u.u1ByteAddr[0] = 3;
    Udp6Dhcp6sParams.dstAddr.ip6_addr_u.u1ByteAddr[1] = 15;
    Udp6Dhcp6sParams.dstAddr.ip6_addr_u.u1ByteAddr[2] = 15;
    Udp6Dhcp6sParams.dstAddr.ip6_addr_u.u1ByteAddr[3] = 14;
    Udp6Dhcp6sParams.dstAddr.ip6_addr_u.u1ByteAddr[15] = 1;

    pu1SendBuf = au1SrvPdu;
    /* 132 */
    /* Relay 1 */
    u1Var = (UINT1) DHCP6_SRV_RELAY_MSG;
    DHCP6_SRV_PUT_1BYTE (pu1SendBuf, u1Var);

    /* 131 */
    /* Hops */
    pu1SendBuf[0] = 1;
    pu1SendBuf++;

    /* Link Addr */
    pu1SendBuf += 16;

    /* Peer Addr */
    pu1SendBuf[0] = 3;
    pu1SendBuf[1] = 15;
    pu1SendBuf[2] = 15;
    pu1SendBuf[3] = 14;
    pu1SendBuf[15] = 3;

    pu1SendBuf += 16;

    /* 98 */
    /* Interface option */
    /* Type */
    u2Var = DHCP6_SRV_IF_MSG_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* 96 */
    /* Opt length */
    u2Var = 4;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* 94 */
    /* OPtion value */
    u4Var = 5;
    DHCP6_SRV_PUT_4BYTE (pu1SendBuf, u2Var);

    /* 90 */
    /* RElay Message option */
    u2Var = DHCP6_SRV_RELAY_MSG_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Opt length */
    u2Var = 88;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* Relay 2 */

    /* 88 */
    u1Var = DHCP6_SRV_RELAY_MSG;
    DHCP6_SRV_PUT_1BYTE (pu1SendBuf, u1Var);
    /* 87 */
    /* Hops */
    pu1SendBuf[0] = 0;
    pu1SendBuf++;

    /* 86 */
    /* Link Addr */
    pu1SendBuf[0] = 3;
    pu1SendBuf[1] = 15;
    pu1SendBuf[2] = 15;
    pu1SendBuf[3] = 14;
    pu1SendBuf[15] = 5;

    pu1SendBuf += 16;

    /* Peer Addr */
    pu1SendBuf += 16;

    /* RElay Message option */
    u2Var = DHCP6_SRV_RELAY_MSG_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Opt length */
    u2Var = 52;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* Client Message */
    /* Type */
    u1Var = DHCP6_SRV_INFO_MSG;
    DHCP6_SRV_PUT_1BYTE (pu1SendBuf, u1Var);

    /* Trans-id */
    DHCP6_SRV_PUT_1BYTE (pu1SendBuf, u1Var);
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_CLIENT_OPTION_ID  Option */
    /* Type */
    u2Var = DHCP6_SRV_CLIENT_OPTION_ID;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_DNS_RECURSIVE_OPTION  Option */
    /* Type */
    u2Var = DHCP6_SRV_DNS_RECURSIVE_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_SIP_IPV6_ADDR_OPTION  Option */
    /* Type */
    u2Var = DHCP6_SRV_SIP_IPV6_ADDR_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_SIP_DOMAIN_NAME_OPTION Option */
    /* Type */
    u2Var = DHCP6_SRV_SIP_DOMAIN_NAME_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_SIP_DOMAIN_NAME_OPTION  Option */
    /* Type */
    u2Var = DHCP6_SRV_SIP_DOMAIN_NAME_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_SERVER_OPTION  Option */
    /* Type */
    u2Var = DHCP6_SRV_SERVER_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_VENDOR_OPTION  Option */
    /* Type */
    u2Var = DHCP6_SRV_VENDOR_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    /* DHCP6_SRV_IA_OPTION  Option */
    /* Type */
    u2Var = DHCP6_SRV_IA_OPTION;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Length */
    u2Var = 2;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);
    /* Value */
    u2Var = 22;
    DHCP6_SRV_PUT_2BYTE (pu1SendBuf, u2Var);

    Dhcp6SrvProcessPkt (&Udp6Dhcp6sParams, au1SrvPdu);

    return OSIX_TRUE;
}
#endif /* _DHC6S_TEST_C_ */
