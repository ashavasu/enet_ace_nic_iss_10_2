/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: d6srtdfs.h,v 1.11 2013/11/26 11:00:38 siva Exp $
 * 
 * Description:This file contains type definitions relating to Dhcp6 Server
 *   protocol.
 * 
 ********************************************************************/

#ifndef  __DHCP6_SRV_TDFS_H
#define  __DHCP6_SRV_TDFS_H
typedef struct __Dhcp6SrvGblInfo
{
   tOsixTaskId          TaskId;
   tOsixQId             TaskQId;
   tOsixSemId           SemId;
   tRBTree              IfTable;
   tRBTree              PoolTable;
   tRBTree              OptionTable;
   tRBTree              SubOptionTable;
   tRBTree              ClientTable;
   tRBTree              RealmTable;
   tRBTree              KeyTable;
   tRBTree              InPrefixTable;
   UINT4                u4DebugTrace;
   UINT4                u4SysLogId;
   INT4                 BuddyMemPool;
#ifdef NPAPI_WANTED   
   UINT4           u4SrvFilterId;
   UINT4           u4RlySrvFilterId;
#endif
   INT4                 i4SocketFd;
   UINT1                au1ReadPdu[DHCP6_SRV_PDU_SIZE_MAX];
   UINT1                au1WritePdu[DHCP6_SRV_PDU_SIZE_MAX];
   UINT2                u2ListenPort;
   UINT2                u2ClientTransmitPort;
   UINT2                u2RelayTransmitPort;
   UINT1                u1SysLogAdminStatus;
   BOOL1                b1IsInitialized;
   UINT1                u1TrapControl;
   UINT1                au1Pad[3];
}tDhcp6SrvGblInfo;
/*****************************************************************************/
typedef struct __Dhcp6SrvPoolInfo
{
   /* Indices
    * 1. PoolIndex   = pPool->u4PoolIndex
    **/
   tRBNodeEmbd     GblRBLink;
   tTMO_SLL        OptionList;
   UINT4           u4PoolIndex;
   UINT4           u4DuidIfIndex;
   UINT1           au1Duid[DHCP6_SRV_DUID_SIZE_MAX];
   UINT1           au1Name[DHCP6_SRV_POOL_NAME_MAX];
   UINT1           u1NameLength;
   UINT2           u2Preference;
   UINT1           u1DuidType;
   UINT1           u1DuidLength;
   UINT1           u1RowStatus;
   UINT1           au1Pad[1];
}tDhcp6SrvPoolInfo;
/*****************************************************************************/
typedef struct  __Dhcp6SrvOptionInfo
{
   /* Indices
    * 1. PoolIndex   = pPool->u4PoolIndex
    * 2. OptionIndex = u4OptionIndex
    **/
   tTMO_SLL_NODE   PoolSllLink;
   tRBNodeEmbd     GblRBLink;
   tTMO_SLL        SubOptionList;
   tDhcp6SrvPoolInfo *pPool;           
   UINT1           *pu1Value;
   UINT4           u4OptionIndex;
   UINT2           u2Type;
   UINT2           u2Length;
   UINT2           u2Preference;
   UINT1           u1RowStatus;
   UINT1           u1Pad;
}tDhcp6SrvOptionInfo;
/*****************************************************************************/
typedef struct  __Dhcp6SrvSubOptionInfo
{
   /* Indices
    * 1. PoolIndex     = pOption->pPool->u4PoolIndex
    * 2. OptionIndex   = pOption->u4OptionIndex
    * 3. SubOptionType = u2Type
    **/
   tTMO_SLL_NODE        OptionSllLink;
   tRBNodeEmbd          GblRBLink;
   tDhcp6SrvOptionInfo  *pOption;   
   UINT1                *pu1Value;
   UINT2                u2Type;
   UINT2                u2Length;
   UINT1                u1RowStatus;
   UINT1                au1Pad[3];
}tDhcp6SrvSubOptionInfo;
/*****************************************************************************/
typedef struct  __Dhcp6SrvInPrefixInfo
{
   /* Indices
    * 1. InPrefixIndex  = u4InPrefixIndex
    * 2. ContextId   = u4ContextId
    **/
   tRBNodeEmbd     GblRBLink;
   tDhcp6SrvPoolInfo *pPool;          /* Pool mapped to this prefix
                                       * NULL if no pool is mapped
                                       */
   UINT4           u4ContextId;
   UINT4           u4InPrefixIndex;
   UINT1           u1AddressLength ;
   UINT1           au1Address[DHCP6_SRV_IPV6_ADDRESS_SIZE];
   UINT1           u1RowStatus;
   UINT1           au1Pad[2];
}tDhcp6SrvInPrefixInfo;
/*****************************************************************************/
typedef struct __Dhcp6SrvIfInfo
{
   /* Indices
    * 1. IfIndex  = u4IfIndex
    **/
   tRBNodeEmbd     GblRBLink;
   tDhcp6SrvPoolInfo *pPool;            /* Pool mapped to this interface, 
                                      * NULL if no pool is mapped 
                                      * */
   UINT4           u4IfIndex;
   UINT4           u4InformIn;
   UINT4           u4RelayForwIn;
   UINT4           u4ReplyOut;
   UINT4           u4RelayReplyOut;
   UINT4           u4HmacFailCount;
   UINT4           u4InvalidPktIn;
   UINT1           au1LastRDValue[DHCP6_SRV_AUTH_RD_SIZE];
   UINT2           u2LastUnknownTlvType;
   UINT1           u1OperStatus;
   UINT1           u1RowStatus;
}tDhcp6SrvIfInfo;
/*****************************************************************************/
typedef struct __Dhcp6SrvRealmInfo
{
   /* Indices
    * 1. RealmIndex  = u4RealmIndex
    **/
   tRBNodeEmbd     GblRBLink;
   UINT4           u4RealmIndex;
   UINT1           u1NameLength;
   UINT1           u1RowStatus;
   UINT1           au1Name[DHCP6_SRV_REALM_NAME_MAX];
   UINT1           au1Pad[2];
}tDhcp6SrvRealmInfo;
/*****************************************************************************/
typedef struct __Dhcp6SrvClientInfo
{
   /* Indices
    * 1. ClientIndex  = u4ClientIndex
    **/
   tRBNodeEmbd         GblRBLink;
   tDhcp6SrvRealmInfo *pRealm;          /* Realm mapped to this client
                                         * NULL if no realm is mapped
                                         */
   UINT4               u4ClientIndex;
   UINT1               u1ClientIdType;
   UINT1               u1ClientIdLength;
   UINT1               u1RowStatus;
   UINT1               au1ClientId[DHCP6_SRV_DUID_SIZE_MAX];
   UINT1               u1Pad[1];
}tDhcp6SrvClientInfo;

/*****************************************************************************/
typedef struct __Dhcp6SrvKeyInfo
{
   /* Indices
    * 1. RealmIndex    = pRealm->u4RealmIndex
    * 2. KeyIdentifier = u4KeyId       
    **/
   tRBNodeEmbd         GblRBLink;
   tDhcp6SrvRealmInfo *pRealm;
   UINT4               u4KeyId;
   UINT1               u1KeyLength;
   UINT1               u1RowStatus;
   UINT1               au1Key[DHCP6_SRV_KEY_SIZE_MAX];
   UINT1               au1Pad[2];
}tDhcp6SrvKeyInfo;
/*****************************************************************************/
typedef struct __Dhcp6SrvQMsg
{
    union
    {
        tNetIpv6AddrChange     dhcp6SIpIfAddrParam;
                                 /* IPv6 address information */
        tNetIpv6IfStatChange   dhcp6SIpIfStatParam;
                                 /* IPv6 interface information */
    }unDhcp6SMsgType;
    UINT4          u4Command;
#define DHCP6_SRV_ADDR_ADD   10
#define DHCP6_SRV_ADDR_DEL   11
}tDhcp6SrvQMsg;
/*****************************************************************************/
typedef struct __Dhcp6SrvParseInfo
{
   /* IPv6 Header Information */
   struct
   {
       UINT4       u4Len;       /* Length of application data */
       INT4        i4Hoplimt;   /* Hop Limit of the packet */
       tIp6Addr    srcAddr;     /* Source address in the packet */
       tIp6Addr    dstAddr;     /* Destination address in the packet */
   }ip6Hdr;
   
   /* UDPv6 Header Information */
   struct
   {
       UINT2       u2SrcPort;   /* Source port number over which packet
                                 * came */
       UINT2       u2DstPort;   /* Destination port number over which
                                 * packet is to be sent */
   }udp6Hdr;
   
   /* DHCPv6 Header Information */
   struct
   {
       tDhcp6SrvRealmInfo  *pRealm;
       tDhcp6SrvKeyInfo    *pKey;
       UINT4     u4TransId;     /* Transaction ID    */
       UINT4     u4RcvdKeyId;
       UINT2     u2ClientIdLen;
       UINT2     u2RcvdHmacLen;
       UINT1     u1HopCount;
       UINT1     au1Pad[3];
       tIp6Addr  LinkAddr;    /* Link Ipv6-Address */
       tIp6Addr  PeerAddr;    /* Peer Ipv6-Address */
       UINT1     u1PduType;   /* Pdu Type as Information-Request/Relay-Forward*/
       UINT1     u1AuthProtocol;
       UINT1     u1AuthAlgo;
       UINT1     u1AuthRDM;
       UINT1     au1ClientId[DHCP6_SRV_DUID_SIZE_MAX];
       UINT1     au1RDValue[DHCP6_SRV_AUTH_RD_SIZE];
       UINT1     au1RcvdRealm[DHCP6_SRV_REALM_NAME_MAX];
       UINT1     au1RcvdHmac[DHCP6_SRV_AUTH_HMAC_SIZE];
   }dhcp6;

   tDhcp6SrvIfInfo     *pIfInfo;
   tDhcp6SrvPoolInfo   *pPool;
   UINT1               *pu1ReadBuffer;   /* Stores the input buffer, information 
                                          * request or reply message 
                                          */
   UINT1               *pu1WriteBuffer;  /* Output buffer, reply or relay forward */
   UINT2                u2ReadLen;
   UINT2                u2WrittenLen;
   UINT2                u2RelayHdrSize;
   UINT2                u2RelayMsgLenOffset;
}tDhcp6ParseInfo;
/*****************************************************************************/
#endif
