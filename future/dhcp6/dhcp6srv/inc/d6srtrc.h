/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srtrc.h,v 1.4 2010/12/14 10:27:49 siva Exp $
 *
 * Description: This file contains macros for tracing and debugging.
 *
 *******************************************************************/

#ifndef  __DHCP6_SRV_TRC_H
#define  __DHCP6_SRV_TRC_H

PUBLIC INT4 Dhcp6SrvUtilGetTraceInputValue PROTO ((UINT1 *, UINT4));
PUBLIC INT4 Dhcp6SrvUtilGetTraceOptionValue PROTO ((UINT1 *, INT4));

#define DHCP6_SRV_TRC_FLAG    gDhcp6SrvGblInfo.u4DebugTrace 
#define DHCP6_SRV_MOD_NAME    "D6SR" 

/* Trace String */
#define DHCP6_SRV_MAX_TRC_STR_COUNT        9
#define DHCP6_SRV_MAX_TRC_STR_LEN          15


#define DHCP6_SRV_MAX_TRACE_TOKENS         12
#define DHCP6_SRV_MAX_TRACE_TOKEN_SIZE     12
#define DHCP6_SRV_TRACE_TOKEN_DELIMITER    ' '    /* space */

#define DHCP6_SRV_TRC_MIN_SIZE         1
#define DHCP6_SRV_TRC_MAX_SIZE         255

#define DHCP6_SRV_ALL_TRC           (INIT_SHUT_TRC        |\
                                  MGMT_TRC             |\
                                  CONTROL_PLANE_TRC    |\
                                  DUMP_TRC             |\
                                  OS_RESOURCE_TRC      |\
                                  ALL_FAILURE_TRC      |\
                                  BUFFER_TRC           |\
                                  DHCP6_SRV_CRITICAL_TRC)

#define DHCP6_SRV_INVALID_TRC   0x00000000
#define DHCP6_SRV_CRITICAL_TRC  0x00000100

#define DHCP6_SRV_MIN_TRC_VALUE INIT_SHUT_TRC
#define DHCP6_SRV_MAX_TRC_VALUE ELPS_TRC_CRITICAL

#define DHCP6_SRV_PRINT       UtlTrcLog

#ifdef TRACE_WANTED
#define DHCP6_SRV_TRC_BUF_SIZE    2000

#define DHCP6_SRV_TRC(TraceType, Str)\
        DHCP6_SRV_PRINT(DHCP6_SRV_TRC_FLAG, TraceType, DHCP6_SRV_MOD_NAME, Str)

#define DHCP6_SRV_TRC_ARG1(TraceType, Str, Arg1)\
        DHCP6_SRV_PRINT(DHCP6_SRV_TRC_FLAG, TraceType, DHCP6_SRV_MOD_NAME, Str, Arg1)

#define DHCP6_SRV_PKT_DUMP(TraceType, pBuf, Length, Str)\
        MOD_PKT_DUMP(DHCP6_SRV_TRC_FLAG, TraceType, DHCP6_SRV_MOD_NAME, pBuf, Length, Str)

#else /* TRACE_WANTED */
#define DHCP6_SRV_TRC(TraceType, Str)      
#define DHCP6_SRV_TRC_ARG1(TraceType, Str, Arg1)

#endif /* TRACE_WANTED */

#endif
