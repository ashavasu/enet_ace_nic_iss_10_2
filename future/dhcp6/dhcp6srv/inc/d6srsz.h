enum {
    MAX_D6SR_SRV_CLIENT_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_IF_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_INPREFIX_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_KEY_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_OPTION_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_POOL_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_QMSGS_SIZING_ID,
    MAX_D6SR_SRV_REALM_ENTRY_SIZING_ID,
    MAX_D6SR_SRV_SUBOPTION_ENTRY_SIZING_ID,
    D6SR_MAX_SIZING_ID
};


#ifdef  _D6SRSZ_C
tMemPoolId D6SRMemPoolIds[ D6SR_MAX_SIZING_ID];
INT4  D6srSizingMemCreateMemPools(VOID);
VOID  D6srSizingMemDeleteMemPools(VOID);
INT4  D6srSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _D6SRSZ_C  */
extern tMemPoolId D6SRMemPoolIds[ ];
extern INT4  D6srSizingMemCreateMemPools(VOID);
extern VOID  D6srSizingMemDeleteMemPools(VOID);
extern INT4  D6srSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _D6SRSZ_C  */


#ifdef  _D6SRSZ_C
tFsModSizingParams FsD6SRSizingParams [] = {
{ "tDhcp6SrvClientInfo", "MAX_D6SR_SRV_CLIENT_ENTRY", sizeof(tDhcp6SrvClientInfo),MAX_D6SR_SRV_CLIENT_ENTRY, MAX_D6SR_SRV_CLIENT_ENTRY,0 },
{ "tDhcp6SrvIfInfo", "MAX_D6SR_SRV_IF_ENTRY", sizeof(tDhcp6SrvIfInfo),MAX_D6SR_SRV_IF_ENTRY, MAX_D6SR_SRV_IF_ENTRY,0 },
{ "tDhcp6SrvInPrefixInfo", "MAX_D6SR_SRV_INPREFIX_ENTRY", sizeof(tDhcp6SrvInPrefixInfo),MAX_D6SR_SRV_INPREFIX_ENTRY, MAX_D6SR_SRV_INPREFIX_ENTRY,0 },
{ "tDhcp6SrvKeyInfo", "MAX_D6SR_SRV_KEY_ENTRY", sizeof(tDhcp6SrvKeyInfo),MAX_D6SR_SRV_KEY_ENTRY, MAX_D6SR_SRV_KEY_ENTRY,0 },
{ "tDhcp6SrvOptionInfo", "MAX_D6SR_SRV_OPTION_ENTRY", sizeof(tDhcp6SrvOptionInfo),MAX_D6SR_SRV_OPTION_ENTRY, MAX_D6SR_SRV_OPTION_ENTRY,0 },
{ "tDhcp6SrvPoolInfo", "MAX_D6SR_SRV_POOL_ENTRY", sizeof(tDhcp6SrvPoolInfo),MAX_D6SR_SRV_POOL_ENTRY, MAX_D6SR_SRV_POOL_ENTRY,0 },
{ "tDhcp6SrvQMsg", "MAX_D6SR_SRV_QMSGS", sizeof(tDhcp6SrvQMsg),MAX_D6SR_SRV_QMSGS, MAX_D6SR_SRV_QMSGS,0 },
{ "tDhcp6SrvRealmInfo", "MAX_D6SR_SRV_REALM_ENTRY", sizeof(tDhcp6SrvRealmInfo),MAX_D6SR_SRV_REALM_ENTRY, MAX_D6SR_SRV_REALM_ENTRY,0 },
{ "tDhcp6SrvSubOptionInfo", "MAX_D6SR_SRV_SUBOPTION_ENTRY", sizeof(tDhcp6SrvSubOptionInfo),MAX_D6SR_SRV_SUBOPTION_ENTRY, MAX_D6SR_SRV_SUBOPTION_ENTRY,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _D6SRSZ_C  */
extern tFsModSizingParams FsD6SRSizingParams [];
#endif /*  _D6SRSZ_C  */


