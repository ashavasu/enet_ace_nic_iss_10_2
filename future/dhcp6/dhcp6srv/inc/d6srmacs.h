/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the macro definition for the DHCPv6
 *              Server module.
 *
 **************************************************************************/
#ifndef  __DHCP6_SRV_MAC_H
#define  __DHCP6_SRV_MAC_H
/*****************************************************************************/
/* Macros for gloabl access */
#define DHCP6_SRV_QUEUE_ID             gDhcp6SrvGblInfo.TaskQId
#define DHCP6_SRV_SEM_ID               gDhcp6SrvGblInfo.SemId
#define DHCP6_SRV_TASK_ID              gDhcp6SrvGblInfo.TaskId
#define DHCP6_SRV_IS_INITIALISED       gDhcp6SrvGblInfo.b1IsInitialized
#define DHCP6_SRV_TRAP_CONTROL         gDhcp6SrvGblInfo.u1TrapControl
#define DHCP6_SRV_DEBUG_TRACE          gDhcp6SrvGblInfo.u4DebugTrace
#define DHCP6_SRV_SYSLOG_ADMIN_STATUS  gDhcp6SrvGblInfo.u1SysLogAdminStatus
#define DHCP6_SRV_SYSLOG_ID            gDhcp6SrvGblInfo.u4SysLogId
#define DHCP6_SRV_READ_PDU             gDhcp6SrvGblInfo.au1ReadPdu
#define DHCP6_SRV_WRITE_PDU            gDhcp6SrvGblInfo.au1WritePdu
#define DHCP6_SRV_SOCKET_FD            gDhcp6SrvGblInfo.i4SocketFd

#define DHCP6_SRV_BUDDY_MEM            gDhcp6SrvGblInfo.BuddyMemPool
#define DHCP6_SRV_MSGQ_MEM             \
             D6SRMemPoolIds[MAX_D6SR_SRV_QMSGS_SIZING_ID]
#define DHCP6_SRV_IF_MEM               \
             D6SRMemPoolIds[MAX_D6SR_SRV_IF_ENTRY_SIZING_ID]
#define DHCP6_SRV_POOL_MEM             \
             D6SRMemPoolIds[MAX_D6SR_SRV_POOL_ENTRY_SIZING_ID]
#define DHCP6_SRV_OPTION_MEM           \
             D6SRMemPoolIds[MAX_D6SR_SRV_OPTION_ENTRY_SIZING_ID]
#define DHCP6_SRV_SUBOPTION_MEM        \
             D6SRMemPoolIds[MAX_D6SR_SRV_SUBOPTION_ENTRY_SIZING_ID]
#define DHCP6_SRV_INPREFIX_MEM         \
             D6SRMemPoolIds[MAX_D6SR_SRV_INPREFIX_ENTRY_SIZING_ID]
#define DHCP6_SRV_CLIENT_MEM           \
             D6SRMemPoolIds[MAX_D6SR_SRV_CLIENT_ENTRY_SIZING_ID]
#define DHCP6_SRV_REALM_MEM            \
             D6SRMemPoolIds[MAX_D6SR_SRV_REALM_ENTRY_SIZING_ID]
#define DHCP6_SRV_KEY_MEM             \
             D6SRMemPoolIds[MAX_D6SR_SRV_KEY_ENTRY_SIZING_ID]

#define DHCP6_SRV_IF_TABLE             gDhcp6SrvGblInfo.IfTable
#define DHCP6_SRV_POOL_TABLE           gDhcp6SrvGblInfo.PoolTable
#define DHCP6_SRV_OPTION_TABLE         gDhcp6SrvGblInfo.OptionTable
#define DHCP6_SRV_SUBOPTION_TABLE      gDhcp6SrvGblInfo.SubOptionTable
#define DHCP6_SRV_CLIENT_TABLE         gDhcp6SrvGblInfo.ClientTable
#define DHCP6_SRV_REALM_TABLE          gDhcp6SrvGblInfo.RealmTable
#define DHCP6_SRV_KEY_TABLE            gDhcp6SrvGblInfo.KeyTable
#define DHCP6_SRV_INPREFIX_TABLE       gDhcp6SrvGblInfo.InPrefixTable
#define DHCP6_SRV_INPREFIX_ROOT        gDhcp6SrvGblInfo.PrefixTableRoot

#define DHCP6_SRV_LISTEN_PORT          gDhcp6SrvGblInfo.u2ListenPort
#define DHCP6_SRV_RLY_TRANSMIT_PORT    gDhcp6SrvGblInfo.u2RelayTransmitPort
#define DHCP6_SRV_CLNT_TRANSMIT_PORT   gDhcp6SrvGblInfo.u2ClientTransmitPort

/* System specific Constatnts */
#define DHCP6_SRV_GLOBAL_INFO_SIZE       (sizeof(tDhcp6SrvGblInfo))

#define DHCP6_SRV_IS_SYSLOG_ENABLED()\
        (DHCP6_SRV_SYSLOG_ADMIN_STATUS==DHCP6_ENABLED)

/*****************************************************************************/
/* FSAP2 MemPoolAllocation Macros <--> Routines  */
#define DHCP6_SRV_BUDDY_CREATE(u4MaxBlkSize,u4MinBlkSize,u4NumBlks,u1CFlag) \
        MemBuddyCreate(u4MaxBlkSize,u4MinBlkSize,u4NumBlks,u1CFlag)
#define DHCP6_SRV_BUDDY_DESTROY(u1Id) \
        MemBuddyDestroy(u1Id)
#define DHCP6_SRV_BUDDY_ALLOC(size)  \
        MemBuddyAlloc((UINT1)(DHCP6_SRV_BUDDY_MEM),size)
#define DHCP6_SRV_BUDDY_FREE(ptr) \
        MemBuddyFree((UINT1)(DHCP6_SRV_BUDDY_MEM),(UINT1*)ptr)

#define DHCP6_SRV_CREATE_MEM_POOL(u4BlockSize,u4NumOfBlocks,u4TypeOfMem,pPoolId)\
        MemCreateMemPool(u4BlockSize,u4NumOfBlocks,u4TypeOfMem,pPoolId)
#define DHCP6_SRV_DELETE_MEM_POOL(PoolId)\
        MemDeleteMemPool ((tMemPoolId)PoolId)\

#define DHCP6_SRV_ALLOC_MEM_MSGQ(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_MSGQ_MEM)))
#define DHCP6_SRV_ALLOC_MEM_IF(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_IF_MEM)))
#define DHCP6_SRV_ALLOC_MEM_POOL(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_POOL_MEM)))
#define DHCP6_SRV_ALLOC_MEM_OPTION(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_OPTION_MEM)))
#define DHCP6_SRV_ALLOC_MEM_SUBOPTION(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_SUBOPTION_MEM)))
#define DHCP6_SRV_ALLOC_MEM_INPREFIX(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_INPREFIX_MEM)))
#define DHCP6_SRV_ALLOC_MEM_CLIENT(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_CLIENT_MEM)))
#define DHCP6_SRV_ALLOC_MEM_REALM(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_REALM_MEM)))
#define DHCP6_SRV_ALLOC_MEM_KEY(pBlock)\
    (pBlock=MemAllocMemBlk((DHCP6_SRV_KEY_MEM)))

#define DHCP6_SRV_FREE_MEM_MSGQ(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_MSGQ_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_IF(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_IF_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_POOL(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_POOL_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_OPTION(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_OPTION_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_SUBOPTION(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_SUBOPTION_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_INPREFIX(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_INPREFIX_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_CLIENT(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_CLIENT_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_REALM(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_REALM_MEM),(UINT1*)pBlock)
#define DHCP6_SRV_FREE_MEM_KEY(pBlock)\
     MemReleaseMemBlock ((DHCP6_SRV_KEY_MEM),(UINT1*)pBlock)

/*  Getting the base pointers */
#define DHCP6_SRV_GET_BASE_PTR(type, memberName, pMember) \
           (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName))
/*****************************************************************************/
/* Macros used to differentiate the messages in the message queues */
#define DHCP6_SRV_BUFFER_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
       UINT1  u1LinearBuf = (UINT1) u1Value;\
       MEMCPY((pMsg + u4Offset), ((UINT1 *) &u1LinearBuf), 1);\
}

#define DHCP6_SRV_BUFFER_ASSIGN_2_BYTE(pMsg, u4Offset, u2Value) \
{\
       UINT2  u2LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
       MEMCPY((pMsg + u4Offset), ((UINT1 *) &u2LinearBuf), 2);\
}

#define DHCP6_SRV_BUFFER_ASSIGN_4_BYTE(pMsg, u4Offset, u4Value) \
{\
       UINT4  u4LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
       MEMCPY((pMsg + u4Offset), ((UINT1 *) &u4LinearBuf), 4);\
}

#define DHCP6_SRV_LGET3BYTE(addr)  DHCP6_SRV_GET_THREE_BYTE(addr); addr += 3;

#define DHCP6_SRV_GET_THREE_BYTE(addr) \
    ( ((*((UINT1 *)(addr)   ) << 16) & 0x00ff0000) |\
      ((*((UINT1 *)(addr) +1) << 8 ) & 0x0000ff00) |\
      ((*((UINT1 *)(addr) +2)      ) & 0x000000ff) )

#define DHCP6_SRV_GET_1BYTE(u1Val, pu1PktBuf)     \
   do {                                           \
        u1Val = *pu1PktBuf;                       \
        pu1PktBuf += 1;                           \
      } while(0)

#define DHCP6_SRV_GET_2BYTE(u2Val, pu1PktBuf)     \
   do {                                           \
        MEMCPY (&u2Val, pu1PktBuf, 2);            \
        u2Val = (UINT2) OSIX_NTOHS(u2Val);        \
        pu1PktBuf += 2;                           \
      } while(0)

#define DHCP6_SRV_GET_4BYTE(u4Val, pu1PktBuf)     \
   do {                                           \
        MEMCPY (&u4Val, pu1PktBuf, 4);            \
        u4Val =  OSIX_NTOHL(u4Val);        \
        pu1PktBuf += 4;                           \
      } while(0)

#define DHCP6_SRV_GET_NBYTE(pu1Data, pu1PktBuf, u4Size)\
   do {                                           \
        MEMCPY (pu1Data, pu1PktBuf, u4Size);      \
        pu1PktBuf += u4Size;                      \
      } while(0)

#define DHCP6_SRV_PUT_1BYTE(pu1PktBuf, u1Val)     \
   do {                                           \
        *pu1PktBuf = u1Val;                       \
        pu1PktBuf += 1;                           \
      } while(0)

#define DHCP6_SRV_PUT_2BYTE(pu1PktBuf, u2Val)     \
   do {                                           \
       *((UINT2 *) ((VOID*)pu1PktBuf))=OSIX_HTONS(u2Val);\
       pu1PktBuf += 2;\
      } while(0)

#define DHCP6_SRV_PUT_4BYTE(pu1PktBuf, u4Val)     \
   do {                                           \
       *((UINT4 *) ((VOID*)pu1PktBuf))=OSIX_HTONL(u4Val);\
        pu1PktBuf += 4;                           \
      } while(0)

#define DHCP6_SRV_PUT_NBYTE(pu1PktBuf, pu1Data, u4Size)\
   do {                                           \
        MEMCPY (pu1PktBuf, pu1Data, u4Size);      \
        pu1PktBuf += u4Size;                      \
      } while(0)

#define DHCP6_SRV_GET_OFFSET_1BYTE(pu1PktBuf, u4Offset, u1Val) \
   do {                                                        \
        MEMCPY (&u1Val, pu1PktBuf + u4Offset, 1);              \
        u4Offset += 1;                                         \
      } while(0)

#define DHCP6_SRV_GET_OFFSET_2BYTE(pu1PktBuf, u4Offset, u2Val) \
   do {                                                        \
        MEMCPY (&u2Val, pu1PktBuf + u4Offset, 2);              \
        u2Val = (UINT2) OSIX_NTOHS(u2Val);                     \
        u4Offset += 2;                                         \
      } while(0)

#define DHCP6_SRV_GET_OFFSET_4BYTE(pu1PktBuf, u4Offset, u4Val) \
   do {                                                        \
        MEMCPY (&u4Val, pu1PktBuf + u4Offset, 4);              \
        u4Val = (UINT2) OSIX_NTOHL(u4Val);                     \
        u4Offset += 4;                                         \
      } while(0)

#define DHCP6_SRV_GET_OFFSET_NBYTE(pu1Data, pu1PktBuf, u4Offset, u4Size)\
   do {                                                        \
        MEMCPY (pu1Data, pu1PktBuf + u4Offset, u4Size);        \
        pu1PktBuf += u4Size;                                   \
        u4Offset += u4Size;                                    \
      } while(0)

#define DHCP6_SRV_PUT_OFFSET_1BYTE(pMsg, u4Offset, u1Value)    \
   do {                                                        \
        UINT1  u1LinearBuf = (UINT1) u1Value;                  \
        MEMCPY((pMsg + u4Offset), ((UINT1 *) &u1LinearBuf), 1); \
        u4Offset += 1;                                         \
      } while(0)

/*****************************************************************************/      
#endif
