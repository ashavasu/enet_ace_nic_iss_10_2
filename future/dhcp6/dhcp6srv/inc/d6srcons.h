/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains constant definitions used in 
 *              DHCP6 Module. 
 *
 **************************************************************************/

#ifndef _DHCP6_SRV_CONS_H
#define _DHCP6_SRV_CONS_H

#define DHCP6_SRV_RB_GREATER           1
#define DHCP6_SRV_RB_EQUAL             0
#define DHCP6_SRV_RB_LESS             -1
#define DHCP6_SRV_FOUND                1
#define DHCP6_SRV_NOT_FOUND            0
#define DHCP6_SRV_OPT_CLIENT_ID        1
#define DHCP6_SRV_OPT_SERVER_ID        2
#define DHCP6_SRV_DEFAULT_CXT_ID       0
#define DHCP6_SRV_INFO_REPLY           7
#define DHCP6_SRV_RELAY_MSG_OPTION     9
#define DHCP6_SRV_INFO_MSG             11
#define DHCP6_SRV_RELAY_MSG            12
#define DHCP6_SRV_RELAY_REPLY          13
#define DHCP6_SRV_IF_MSG_OPTION        18
#define DHCP6_SRV_LEN_FIXED_RELAY_HDR  38
#define DHCP6_SRV_OPER_UP              0x01
#define DHCP6_SRV_SNMP_SUCCESS         1
#define DHCP6_SRV_SNMP_FAILURE         2
/*****************************************************************************/
/* System Specific Constants */
#define DHCP6_SRV_TASK                 ((UINT1 *)"D6SR")
#define DHCP6_SRV_QUEUE_NAME           ((UINT1 *)"D6SRQ")
#define DHCP6_SRV_SEM_NAME             ((const UINT1 *)"D6SRS")
#define DHCP6_SRV_SEM_CREATE_INIT_CNT  1
#define DHCP6_SRV_DATA_EVENT           0x01
#define DHCP6_SRV_CONTROL_EVENT        0x02
#define DHCP6_SRV_MIN_INTERFACES       1
#define DHCP6_SRV_MAX_INTERFACES       CFA_MAX_INTERFACES_IN_SYS
#define DHCP6_SRV_CONTEXT_MAX          8 /* get from where.*/
#define DHCP6_SRV_PDU_SIZE_MAX         1536 /* get from where*/
#define DHCP6_SRV_IPV6_IF_ADDR_CHG_EVENT     4
#define DHCP6_SRV_IPV6_IF_STAT_CHG_EVENT     8
#define DHCP6_SRV_LISTEN_PORT_DEF         547
#define DHCP6_SRV_CLNT_TRANSMIT_PORT_DEF  546
#define DHCP6_SRV_RLY_TRANSMIT_PORT_DEF   547
/* The protocol field in IP6 header should be set to 67 */
#define DHCP6_SRV_PROTO_ID                67
/*****************************************************************************/
/* Scalability Constants */
#define DHCP6_SRV_BUDDY_SIZE_MIN       8
#define DHCP6_SRV_BUDDY_SIZE_MAX       256
#define DHCP6_SRV_BUDDY_BLOCK          (MAX_D6SR_SRV_OPTION_ENTRY+MAX_D6SR_SRV_SUBOPTION_ENTRY)
/*****************************************************************************/
/* Default Values, min max */
#define DHCP6_SRV_AUTH_RD_SIZE        8
#define DHCP6_SRV_AUTH_RDM_SIZE       1
#define DHCP6_SRV_AUTH_ALGO_SIZE      1
#define DHCP6_SRV_AUTH_PROTO_SIZE     1
#define DHCP6_SRV_AUTH_KEYID_SIZE     4
#define DHCP6_SRV_AUTH_HMAC_SIZE      16
#define DHCP6_SRV_AUTH_PROTOCOL       2
#define DHCP6_SRV_AUTH_ALGO           1

#define DHCP6_SRV_ENTERPRISE_NUMBER  2
#define DHCP6_SRV_POOL_PREFERENCE_MIN   0
#define DHCP6_SRV_POOL_PREFERENCE_MAX   255 
#define DHCP6_SRV_POOL_NAME_MAX      65
#define DHCP6_SRV_POOL_NAME_MIN      1
#define DHCP6_SRV_DUID_SIZE_MAX      128
#define DHCP6_SRV_DUID_SIZE_MIN      1
#define DHCP6_SRV_REALM_NAME_MAX     128     
#define DHCP6_SRV_REALM_NAME_MIN     1
#define DHCP6_SRV_KEY_SIZE_MAX       64
#define DHCP6_SRV_HMAC_SIZE_MAX      16
#define DHCP6_SRV_CLNT_NAME_MAX      (DHCP6_SRV_DUID_SIZE_MAX)
#define DHCP6_SRV_CLNT_NAME_MIN      (DHCP6_SRV_DUID_SIZE_MIN)
#define DHCP6_SRV_KEY_SIZE_MIN       1
#define DHCP6_SRV_IPV6_ADDRESS_SIZE  16
#define DHCP6_SRV_IF_MAX_LEN         9  
#define DHCP6_SRV_TRAP_CONTROL_MAX   0x07
#define DHCP6_SRV_DUID_LLT           1
#define DHCP6_SRV_DUID_EN            2
#define DHCP6_SRV_DUID_LL            3
#define DHCP6_SRV_OPTION_TYPE_MIN    1
#define DHCP6_SRV_OPTION_TYPE_MAX    0xFFFFFFFF
#define DHCP6_SRV_OPTION_PREFERENCE_MIN 1
#define DHCP6_SRV_OPTION_PREFERENCE_MAX 255
#define DHCP6_SRV_OPTION_LENGTH_MIN  1
#define DHCP6_SRV_OPTION_LENGTH_MAX  0xFF
#define DHCP6_SRV_POOL_INDEX_MIN     1
#define DHCP6_SRV_POOL_INDEX_MAX     0xFFFFFFFF
#define DHCP6_SRV_OPTION_INDEX_MIN   1
#define DHCP6_SRV_OPTION_INDEX_MAX   0xFFFFFFFF
#define DHCP6_SRV_CLIENT_INDEX_MIN   1
#define DHCP6_SRV_CLIENT_INDEX_MAX   0xFFFFFFFF
#define DHCP6_SRV_REALM_INDEX_MIN    1
#define DHCP6_SRV_REALM_INDEX_MAX    0xFFFFFFFF
#define DHCP6_SRV_KEY_INDEX_MIN      1
#define DHCP6_SRV_KEY_INDEX_MAX      0xFFFFFFFF
#define DHCP6_SRV_INPREFIX_INDEX_MIN 1
#define DHCP6_SRV_INPREFIX_INDEX_MAX 0xFFFFFFFF
#define DHCP6_SRV_UDP_PORT_MIN       1
#define DHCP6_SRV_UDP_PORT_MAX       65535
#define DHCP6_SRV_NUM_HOURS_1DAY     24
#define DHCP6_SRV_NUM_MINUTES_1HOUR  60
#define DHCP6_SRV_NUM_SECONDS_1MIN   60
#define DHCP6_SRV_MIN_REFRESH_TIME           600
#define DHCP6_SRV_MAX_REFRESH_TIME          0xffffffff 
#define DHCP6_SRV_DEFAULT_REFRESH_TIME      86400


/*****************************************************************************/
#define DHCP6_SRV_TLV_TYPE_SIZE          2
#define DHCP6_SRV_TLV_LEN_SIZE           2
#define DHCP6_SRV_TRANS_ID_SIZE          3
#define DHCP6_SRV_UDP_HDR_SIZE           8
#define DHCP6_SRV_REFRESH_TIMER_SIZE     4
#define DHCP6_SRV_ORO_CODE_SIZE          2
#define DHCP6_SRV_OPTION_PREFERENCE_SIZE 1 

#define DHCP6_SRV_OPTION_CLIENT_ID       1
#define DHCP6_SRV_OPTION_SERVER_ID       2
#define DHCP6_SRV_OPTION_IA_NA           3
#define DHCP6_SRV_OPTION_IA_TA           4
#define DHCP6_SRV_OPTION_IA_ADDR         5
#define DHCP6_SRV_OPTION_ORO             6
#define DHCP6_SRV_OPTION_PREFERENCE      7
#define DHCP6_SRV_OPTION_ELAPSED_TIME    8
#define DHCP6_SRV_OPTION_RELAY_MSG       9
#define DHCP6_SRV_OPTION_AUTH            11
#define DHCP6_SRV_OPTION_UNICAST         12
#define DHCP6_SRV_OPTION_STATUS_CODE     13
#define DHCP6_SRV_OPTION_RAPID_COMMIT    14
#define DHCP6_SRV_OPTION_USER_CLASS      15
#define DHCP6_SRV_OPTION_VENDOR_CLASS    16
#define DHCP6_SRV_OPTION_VENDOR_OPTS     17
#define DHCP6_SRV_OPTION_INTERFACE_ID    18
#define DHCP6_SRV_OPTION_RECONF_MSG      19
#define DHCP6_SRV_OPTION_RECONF_ACCEPT   20
#define DHCP6_SRV_SUCCESS_STATUS_VALUE    0
#define DHCP6_SRV_UNSPECFAIL_STATUS_VALUE 1
#define DHCP6_SRV_USE_MULT_STATUS_VALUE   5
#define DHCP6_SRV_CLEAR_COUNTER_INTERFACE 1
/* ORO Codes*/
#define DHCP6_SRV_ORO_DNS_SERVERS        23
#define DHCP6_SRV_ORO_DOMAIN_LIST        24
#define DHCP6_SRV_ORO_SIP_SERVER_D       21
#define DHCP6_SRV_ORO_SIP_SERVER_A       22
#define DHCP6_SRV_REFRESH_TIMER_TYPE     32
/*****************************************************************************/
#define DHCP6_SRV_INITIALIZE_ZERO        0
#define DHCP6_SRV_INITIALIZE_ONE         1

#define DHCP6_SRV_INDEX_BY_ZERO          0
#define DHCP6_SRV_INDEX_BY_ONE           1
#define DHCP6_SRV_INDEX_BY_TWO           2
#define DHCP6_SRV_INDEX_BY_THREE         3 
#define DHCP6_SRV_INDEX_BY_FOUR          4
#define DHCP6_SRV_INDEX_BY_FIVE          5 
#define DHCP6_SRV_INDEX_BY_SIX           6
#define DHCP6_SRV_INDEX_BY_SEVEN         7
#define DHCP6_SRV_INDEX_BY_EIGHT         8

#define DHCP6_SRV_VALUE_ZERO             0
#define DHCP6_SRV_VALUE_ONE              1
#define DHCP6_SRV_VALUE_TWO              2
#define DHCP6_SRV_VALUE_THREE            3
#define DHCP6_SRV_SHIFT_24BITS           24
#define DHCP6_SRV_SHIFT_16BITS           16
#define DHCP6_SRV_SHIFT_8BITS            8
#define DHCP6_SRV_SNMP_SUCCESS           1
#define DHCP6_SRV_SNMP_FAILURE           2
#define DHCP6_SRV_MAX_IF_NAME_LEN        IP6_MAX_IF_NAME_LEN
#define DHCP6_SRV_IP6_ADDRESS_SIZE_MAX  17

#define DHCP6_SRV_6LENGTH_VAL           6
#define DHCP6_SRV_2LENGTH_VAL           2
#define DHCP6_SRV_12LENGTH_VAL          12
#define DHCP6_SRV_16LENGTH_VAL          16
#define DHCP6_SRV_20LENGTH_VAL          20
#define DHCP6_SRV_28LENGTH_VAL          28
#define DHCP6_SRV_50LENGTH_VAL          50
#define DHCP6_SRV_56LENGTH_VAL          56      
#define DHCP6_SRV_VENDOR_LEN            4

/*****************************************************************************/
/* Porting Macro */
#define DHCP6_SRV_GET_HADWARE_TYPE       2              
#define DHCP6_SRV_GET_ENTERPRISE_NUMBER  122              
#define DHCP6_SRV_MAX_IDENTIFIER_SIZE    122              
#define DHCP6_SRV_GET_IDENTIFIER_LENGTH  7              
#define DHCP6_SRV_GET_IDENTIFIER_VALUE   "ARICENT"              
#define DHCP6_SRV_SUCCESS_STATUS_CODE_VALUE "SUCCESS"              
#define DHCP6_SRV_SUCCESS_STATUS_LENGTH_VALUE 7              
#define DHCP6_SRV_UNSPECFAIL_STATUS_CODE_VALUE "UNSPECFAIL"              
#define DHCP6_SRV_UNSPECFAIL_STATUS_LENGTH_VALUE 10              
#define DHCP6_SRV_USE_MUL_STATUS_CODE_VALUE "USEMULTICAST"              
#define DHCP6_SRV_USE_MUL_STATUS_LENGTH_VALUE 12              



/*****************************************************************************/
#endif
