/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the macro definitions and function 
 *              prototypes,which may change during porting. 
 *
 **************************************************************************/

INT4 D6SrPortVcmGetContextInfoFromIfIndex PROTO ((UINT4, UINT4 *, UINT2 *));
