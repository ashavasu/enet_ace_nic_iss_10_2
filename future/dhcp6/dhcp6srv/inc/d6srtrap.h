/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srtrap.h,v 1.7 2014/09/25 12:16:18 siva Exp $
 * Description: This file contains macros for snmp traps.
 *
 **************************************************************************/

#ifndef  __DHCP6_SRV_TRP_H
#define  __DHCP6_SRV_TRP_H

PUBLIC VOID D6SrTrapSnmpSendTrap PROTO((VOID *, UINT1));

enum
{
   DHCP6_SRV_INVALID_MSG_TRAP=1,
   DHCP6_SRV_HMAC_AUTH_FAIL_TRAP,
   DHCP6_SRV_UNKNOW_TLV_TRAP
};

#define DHCP6_SRV_TRAPS_OID                   "1.3.6.1.4.1.29601.2.42.0"
#define DHCP6_SRV_MIB_OBJ_INVALID_MSG_TRAP    "fsDhcp6SrvIfInvalidPktIn"
#define DHCP6_SRV_MIB_OBJ_AUTH_FAIL_TRAP      "fsDhcp6SrvIfHmacFailCount"
#define DHCP6_SRV_MIB_OBJ_UNKNOW_TLV_TRAP     "fsDhcp6SrvIfUnknownTlvType"

#define DHCP6_SRV_TRAPS_OID_LEN                            13 
#define DHCP6_SRV_4BIT_MAX                                 0xf
#define DHCP6_SRV_MAX_OBJ_LEN                              256

#define DHCP6_SRV_RCVD_INVALID_MSG_TRAP_DISABLED()\
    ((DHCP6_SRV_TRAP_CONTROL&0x0002)?(OSIX_FALSE):(OSIX_TRUE))

#define DHCP6_SRV_HMAC_AUTH_FAIL_TRAP_DISABLED()\
    ((DHCP6_SRV_TRAP_CONTROL&0x0004)?(OSIX_FALSE):(OSIX_TRUE))

#define DHCP6_SRV_UNKNOWN_TLV_TRAP_DISABLED()\
    ((DHCP6_SRV_TRAP_CONTROL&0x0001)?(OSIX_FALSE):(OSIX_TRUE))

#define DHCP6_SRV_INVALID_PKT_REC_TRAP_ENABLED(u1Trap)\
               ((u1Trap & 0x02)?(OSIX_TRUE):(OSIX_FALSE))
#define DHCP6_SRV_HMAC_FAIL_TRAP_ENABLED(u1Trap)\
               ((u1Trap & 0x04)?(OSIX_TRUE):(OSIX_FALSE))
#define DHCP6_SRV_UNKNOWN_TLV_TRAP_ENABLED(u1Trap)\
               ((u1Trap & 0x01)?(OSIX_TRUE):(OSIX_FALSE))
#define DHCP6_SRV_ALL_TRAP_TRAP_ENABLED(u1Trap)\
               ((u1Trap  == 0x07 ) ?(OSIX_TRUE):(OSIX_FALSE))

#endif /* Trap End */
