/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srprot.h,v 1.9 2012/12/31 10:22:15 siva Exp $
 *
 * Description: This file contains the prototypes for all the PUBLIC 
 *              procedures.
 *
 **************************************************************************/
#ifndef  __DHCP6_SRV_PROT_H
#define  __DHCP6_SRV_PROT_H

/* Function prototypes */
PUBLIC VOID D6SrSktSockDeInit PROTO ((VOID));
PUBLIC VOID Dhcp6SrvProcessQMsg PROTO ((VOID));
PUBLIC VOID Dhcp6SrvRcvAndProcessPkt PROTO ((VOID));
PUBLIC VOID D6SrSktPktInSocket PROTO ((INT4));
PUBLIC VOID D6SrSktSendToDst PROTO ((tDhcp6ParseInfo*));
PUBLIC VOID D6SrParseProcessPkt PROTO ((tDhcp6ParseInfo*,UINT4));
PUBLIC VOID D6SrApirvIfStatusChgHdlr PROTO((tNetIpv6IfStatChange* ));
PUBLIC INT4 D6SrSktSockInit PROTO ((VOID));
PUBLIC INT4 D6SrMainDeleteBuddyMemPool PROTO ((VOID));
PUBLIC UINT4 D6SrPortNetIpv6GetIfInfo PROTO ((UINT4, tNetIpv6IfInfo *));
PUBLIC INT4 D6SrPortGlobalAddr PROTO ((UINT4, tIp6Addr *,tIp6Addr *));
PUBLIC INT4 D6SrPortGetHadwareType PROTO ((UINT2 *));
PUBLIC INT4 D6SrPortGetEnterpriseNumber PROTO ((UINT4 *));
PUBLIC INT4 D6SrPortGetIdentifier PROTO ((UINT1 *, UINT1 *));
PUBLIC INT4 Dhcp6SrvJoinMcastGroup (INT4, UINT4, tIp6Addr *);
PUBLIC INT4 Dhcp6SrvLeaveMcastGroup (INT4, UINT4, tIp6Addr *);
/* Pool Table Routines */
PUBLIC INT4 D6SrPoolCreateTable PROTO ((VOID));
PUBLIC VOID D6SrPoolDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvPoolInfo *D6SrPoolCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrPoolAddNode PROTO ((tDhcp6SrvPoolInfo *));
PUBLIC VOID D6SrPoolDelNode PROTO ((tDhcp6SrvPoolInfo *));
PUBLIC INT4 D6SrPoolRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrPoolDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvPoolInfo * D6SrPoolGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvPoolInfo * D6SrPoolGetNextNode PROTO ((tDhcp6SrvPoolInfo *));
PUBLIC tDhcp6SrvPoolInfo * D6SrPoolGetNode PROTO ((UINT4));
PUBLIC INT4 D6SrPoolGetFreeIndex PROTO ((UINT4 *));
PUBLIC INT4 D6SrPoolOptionListAddNode PROTO ((tDhcp6SrvOptionInfo *));
PUBLIC VOID D6SrPoolOptionListDelNode PROTO ((tDhcp6SrvOptionInfo *));
PUBLIC VOID D6SrPoolOptionListDelAll PROTO ((tDhcp6SrvPoolInfo *));
tDhcp6SrvOptionInfo* D6SrPoolOptionListGetFirstNode PROTO ((tDhcp6SrvPoolInfo *));
tDhcp6SrvOptionInfo* D6SrPoolOptionListGetNextNode PROTO ((tDhcp6SrvOptionInfo*, tDhcp6SrvPoolInfo *));

/* Option Table Routines */
PUBLIC INT4 D6SrOptionCreateTable PROTO ((VOID));
PUBLIC VOID D6SrOptionDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvOptionInfo * D6SrOptionCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrOptionAddNode PROTO ((tDhcp6SrvOptionInfo *));
PUBLIC VOID D6SrOptionDelNode PROTO ((tDhcp6SrvOptionInfo *));
PUBLIC INT4 D6SrOptionRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrOptionDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvOptionInfo * D6SrOptionGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvOptionInfo * D6SrOptionGetNextNode PROTO ((tDhcp6SrvOptionInfo *));
PUBLIC tDhcp6SrvOptionInfo * D6SrOptionGetNode PROTO ((UINT4, UINT4));
PUBLIC INT4 D6SrOptionGetFreeIndex PROTO ((UINT4, UINT4 *));
PUBLIC tDhcp6SrvSubOptionInfo * D6SrSubOptionListGetFirstNode PROTO ((tDhcp6SrvOptionInfo *));
PUBLIC tDhcp6SrvSubOptionInfo * D6SrSubOptionListGetNextNode PROTO ((tDhcp6SrvSubOptionInfo *, tDhcp6SrvOptionInfo *));

/* Sub Option Table Routines */
PUBLIC INT4 D6SrSubOptionCreateTable PROTO ((VOID));
PUBLIC VOID D6SrSubOptionDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvSubOptionInfo * D6SrSubOptionCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrSubOptionAddNode PROTO ((tDhcp6SrvSubOptionInfo *));
PUBLIC INT4 D6SrOptionSubOptionListAddNode PROTO ((tDhcp6SrvSubOptionInfo *));
PUBLIC VOID D6SrSubOptionDelNode PROTO ((tDhcp6SrvSubOptionInfo *));
PUBLIC VOID D6SrOptionSubOptionListDelNode PROTO ((tDhcp6SrvSubOptionInfo *));
PUBLIC INT4 D6SrSubOptionRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrSubOptionDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvSubOptionInfo * D6SrSubOptionGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvSubOptionInfo * D6SrSubOptionGetNextNode PROTO ((tDhcp6SrvSubOptionInfo *));
PUBLIC tDhcp6SrvSubOptionInfo * D6SrSubOptionGetNode PROTO ((UINT4, UINT4, UINT2));
/* Include prefix Table Routines */
PUBLIC INT4 D6SrInPrefixCreateTable PROTO ((VOID));
PUBLIC VOID D6SrInPrefixDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvInPrefixInfo * D6SrInPrefixCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrInPrefixAddNode PROTO ((tDhcp6SrvInPrefixInfo * pInPrefixInfo));
PUBLIC VOID D6SrInPrefixDelNode PROTO ((tDhcp6SrvInPrefixInfo *));
PUBLIC INT4 D6SrInPrefixRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrInPrefixDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvInPrefixInfo * D6SrInPrefixGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvInPrefixInfo * D6SrInPrefixGetNextNode PROTO ((tDhcp6SrvInPrefixInfo *));
PUBLIC tDhcp6SrvInPrefixInfo * D6SrInPrefixGetNode PROTO ((UINT4, UINT4));
PUBLIC tDhcp6SrvInPrefixInfo * D6SrInPrefixLookupNode PROTO ((UINT4, UINT1 * ));
PUBLIC VOID D6SrPoolUnMapInPrefix PROTO ((tDhcp6SrvPoolInfo *));
PUBLIC INT4 D6SrInPrefixGetFreeIndex PROTO ((UINT4 *));

/* Client Table Routines */
PUBLIC INT4 D6SrClientCreateTable PROTO ((VOID));
PUBLIC VOID D6SrClientDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvClientInfo *D6SrClientCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrClientAddNode PROTO ((tDhcp6SrvClientInfo *));
PUBLIC VOID D6SrClientDelNode PROTO ((tDhcp6SrvClientInfo *));
PUBLIC INT4 D6SrClientRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrClientDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvClientInfo *D6SrClientGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvClientInfo * D6SrClientGetNextNode PROTO ((tDhcp6SrvClientInfo *));
PUBLIC tDhcp6SrvClientInfo * D6SrClientGetNode PROTO ((UINT4));
PUBLIC INT4 D6SrClientGetFreeIndex PROTO ((UINT4 *));
PUBLIC tDhcp6SrvClientInfo * D6SrClientLookupNode PROTO ((UINT1 *pu1Duid));

/* Realm Table Routines */
PUBLIC INT4 D6SrRealmCreateTable PROTO ((VOID));
PUBLIC VOID D6SrRealmDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvRealmInfo *D6SrRealmCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrRealmAddNode PROTO ((tDhcp6SrvRealmInfo *));
PUBLIC VOID D6SrRealmDelNode PROTO ((tDhcp6SrvRealmInfo *));
PUBLIC VOID D6SrRealmUnMapClient PROTO ((tDhcp6SrvRealmInfo *));
PUBLIC INT4 D6SrRealmRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrRealmDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvRealmInfo * D6SrRealmGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvRealmInfo * D6SrRealmGetNextNode PROTO ((tDhcp6SrvRealmInfo *));
PUBLIC tDhcp6SrvRealmInfo * D6SrRealmGetNode PROTO ((UINT4));
PUBLIC INT4 D6SrRealmGetFreeIndex PROTO ((UINT4 *));
PUBLIC tDhcp6SrvRealmInfo *D6SrRealmLookupNode PROTO ((UINT1 *pu1Realm));

/* Key Table Routines */
PUBLIC INT4 D6SrKeyCreateTable PROTO ((VOID));
PUBLIC VOID D6SrKeyDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvKeyInfo * D6SrKeyCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrKeyAddNode PROTO ((tDhcp6SrvKeyInfo *));
PUBLIC VOID D6SrKeyDelNode PROTO ((tDhcp6SrvKeyInfo *));
PUBLIC INT4 D6SrKeyRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrKeyDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvKeyInfo * D6SrKeyGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvKeyInfo * D6SrKeyGetNextNode PROTO ((tDhcp6SrvKeyInfo *));
PUBLIC tDhcp6SrvKeyInfo * D6SrKeyGetNode PROTO ((UINT4, UINT4));
PUBLIC INT4 D6SrKeyGetFreeIndex PROTO ((UINT4, UINT4 *));
/* Interface Table Routines */
PUBLIC INT4 D6SrIfCreateTable PROTO ((VOID));
PUBLIC VOID D6SrIfDeleteTable PROTO ((VOID));
PUBLIC tDhcp6SrvIfInfo * D6SrIfCreateNode PROTO ((VOID));
PUBLIC INT4 D6SrIfAddNode PROTO ((tDhcp6SrvIfInfo *));
PUBLIC VOID D6SrIfDelNode PROTO ((tDhcp6SrvIfInfo *));
PUBLIC INT4 D6SrIfRBFree PROTO ((tRBElem *, UINT4));
PUBLIC VOID D6SrIfDrainTable PROTO ((VOID));
PUBLIC tDhcp6SrvIfInfo * D6SrIfGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6SrvIfInfo * D6SrIfGetNextNode PROTO ((tDhcp6SrvIfInfo *));
PUBLIC tDhcp6SrvIfInfo * D6SrIfGetNode PROTO ((UINT4));
PUBLIC VOID D6SrPoolUnMapIf PROTO ((tDhcp6SrvPoolInfo *));
PUBLIC INT4 D6SrIfGetFirstOperUpIndex PROTO ((UINT4 *));
/* Authentication */
PUBLIC VOID Dhcp6SrvValidateAutOption PROTO ((UINT1 *,INT4,UINT1 *, 
                                            UINT4 , UINT1 *));
PUBLIC VOID Dhcp6SrvCalculateAuthData PROTO ((UINT1 *,INT4,UINT1 *, UINT4 , UINT1 *));
/* Util */
PUBLIC INT4 Dhcp6SrvGetInterface PROTO ((UINT1 *, UINT4 *));
PUBLIC UINT4 D6SrUtlGetInterfaceAddressIdentifier PROTO ((UINT4 , 
                                                          tNetIpv6IfInfo *));
PUBLIC INT4 D6SrUtlUpdateServerId PROTO ((tDhcp6SrvPoolInfo *));
PUBLIC UINT4 D6SrUtlGetTimeFromEpoch PROTO ((VOID));
PUBLIC INT4 D6SrUtlGetIfName PROTO ((UINT4, UINT1 *));
PUBLIC INT4 D6SrUtlCliConfGetIfName PROTO ((UINT4 , INT1 *));

#endif
