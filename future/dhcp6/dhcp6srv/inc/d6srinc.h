/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the header files included in DHCP6 
 *              Server Module.
 *
 **************************************************************************/
#ifndef  __DHCP6_SRV_INC_H
#define  __DHCP6_SRV_INC_H

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "snmputil.h"
#include "fssnmp.h"

#include "arp.h"
#include "trace.h"
#include "cli.h"
#include "osxstd.h"
#include "utldll.h"
#include "utlmacro.h"
#include "utilcli.h"
#include "fssocket.h"
#include "tcp.h"
#include "fssyslog.h"
#include "fsbuddy.h"
#include "ipv6.h"
#include "ip6util.h"

#include "arMD5_api.h"
#include "arHmac_api.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#endif
#ifdef L3_SWITCHING_WANTED
#include "npapi.h"
#include "ipnp.h"
#endif
#include    "vcm.h"
#include    "dhcp6.h"
#include    "d6srcli.h"
#include    "d6srcons.h"
#include    "d6srtdfs.h"
#include    "d6srextn.h"
#include    "d6srmacs.h"
#include    "d6srport.h"
#include    "d6srprot.h"
#include    "d6srtrap.h"
#include    "d6srtrc.h"
#include    "fsdh6slw.h"
#include    "fsdh6swr.h"
#include    "d6srsz.h"
#endif

