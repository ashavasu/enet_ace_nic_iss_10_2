/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the global structure used in DHCP6
 *              Server module.
 *
 **************************************************************************/
#ifndef  __DHCP6_SRV_GLOB_H
#define  __DHCP6_SRV_GLOB_H
tDhcp6SrvGblInfo       gDhcp6SrvGblInfo;
#endif
