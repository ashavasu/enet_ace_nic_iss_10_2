/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6sdb.h,v 1.5 2012/02/01 13:12:44 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDH6SDB_H
#define _FSDH6SDB_H

UINT1 FsDhcp6SrvPoolTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDhcp6SrvIncludePrefixTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDhcp6SrvOptionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDhcp6SrvSubOptionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDhcp6SrvClientTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDhcp6SrvRealmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDhcp6SrvKeyTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDhcp6SrvIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsdh6s [] ={1,3,6,1,4,1,29601,2,42};
tSNMP_OID_TYPE fsdh6sOID = {9, fsdh6s};


UINT4 FsDhcp6SrvDebugTrace [ ] ={1,3,6,1,4,1,29601,2,42,1,1};
UINT4 FsDhcp6SrvRealmTableNextIndex [ ] ={1,3,6,1,4,1,29601,2,42,1,2};
UINT4 FsDhcp6SrvClientTableNextIndex [ ] ={1,3,6,1,4,1,29601,2,42,1,3};
UINT4 FsDhcp6SrvPoolTableNextIndex [ ] ={1,3,6,1,4,1,29601,2,42,1,4};
UINT4 FsDhcp6SrvIncludePrefixTableNextIndex [ ] ={1,3,6,1,4,1,29601,2,42,1,5};
UINT4 FsDhcp6SrvTrapAdminControl [ ] ={1,3,6,1,4,1,29601,2,42,1,6};
UINT4 FsDhcp6SrvSysLogAdminStatus [ ] ={1,3,6,1,4,1,29601,2,42,1,7};
UINT4 FsDhcp6SrvListenPort [ ] ={1,3,6,1,4,1,29601,2,42,1,8};
UINT4 FsDhcp6SrvClientTransmitPort [ ] ={1,3,6,1,4,1,29601,2,42,1,9};
UINT4 FsDhcp6SrvRelayTransmitPort [ ] ={1,3,6,1,4,1,29601,2,42,1,10};
UINT4 FsDhcp6SrvPoolIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,1};
UINT4 FsDhcp6SrvPoolName [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,2};
UINT4 FsDhcp6SrvPoolPreference [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,3};
UINT4 FsDhcp6SrvPoolDuidType [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,4};
UINT4 FsDhcp6SrvPoolDuid [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,5};
UINT4 FsDhcp6SrvPoolDuidIfIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,6};
UINT4 FsDhcp6SrvPoolOptionTableNextIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,7};
UINT4 FsDhcp6SrvPoolRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,1,1,8};
UINT4 FsDhcp6SrvIncludePrefixContextId [ ] ={1,3,6,1,4,1,29601,2,42,2,2,1,1};
UINT4 FsDhcp6SrvIncludePrefixIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,2,1,2};
UINT4 FsDhcp6SrvIncludePrefix [ ] ={1,3,6,1,4,1,29601,2,42,2,2,1,3};
UINT4 FsDhcp6SrvIncludePrefixPool [ ] ={1,3,6,1,4,1,29601,2,42,2,2,1,4};
UINT4 FsDhcp6SrvIncludePrefixRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,2,1,5};
UINT4 FsDhcp6SrvOptionIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,3,1,1};
UINT4 FsDhcp6SrvOptionType [ ] ={1,3,6,1,4,1,29601,2,42,2,3,1,2};
UINT4 FsDhcp6SrvOptionLength [ ] ={1,3,6,1,4,1,29601,2,42,2,3,1,3};
UINT4 FsDhcp6SrvOptionValue [ ] ={1,3,6,1,4,1,29601,2,42,2,3,1,4};
UINT4 FsDhcp6SrvOptionPreference [ ] ={1,3,6,1,4,1,29601,2,42,2,3,1,5};
UINT4 FsDhcp6SrvOptionRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,3,1,6};
UINT4 FsDhcp6SrvSubOptionType [ ] ={1,3,6,1,4,1,29601,2,42,2,4,1,1};
UINT4 FsDhcp6SrvSubOptionLength [ ] ={1,3,6,1,4,1,29601,2,42,2,4,1,2};
UINT4 FsDhcp6SrvSubOptionValue [ ] ={1,3,6,1,4,1,29601,2,42,2,4,1,3};
UINT4 FsDhcp6SrvSubOptionRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,4,1,4};
UINT4 FsDhcp6SrvClientIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,7,1,1};
UINT4 FsDhcp6SrvClientId [ ] ={1,3,6,1,4,1,29601,2,42,2,7,1,2};
UINT4 FsDhcp6SrvClientIdType [ ] ={1,3,6,1,4,1,29601,2,42,2,7,1,3};
UINT4 FsDhcp6SrvClientRealm [ ] ={1,3,6,1,4,1,29601,2,42,2,7,1,4};
UINT4 FsDhcp6SrvClientRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,7,1,5};
UINT4 FsDhcp6SrvRealmIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,6,1,1};
UINT4 FsDhcp6SrvRealmName [ ] ={1,3,6,1,4,1,29601,2,42,2,6,1,2};
UINT4 FsDhcp6SrvRealmKeyTableNextIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,6,1,3};
UINT4 FsDhcp6SrvRealmRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,6,1,4};
UINT4 FsDhcp6SrvKeyIdentifier [ ] ={1,3,6,1,4,1,29601,2,42,2,8,1,1};
UINT4 FsDhcp6SrvKey [ ] ={1,3,6,1,4,1,29601,2,42,2,8,1,2};
UINT4 FsDhcp6SrvKeyRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,8,1,3};
UINT4 FsDhcp6SrvIfIndex [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,1};
UINT4 FsDhcp6SrvIfPool [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,2};
UINT4 FsDhcp6SrvIfInformIn [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,3};
UINT4 FsDhcp6SrvIfRelayForwIn [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,4};
UINT4 FsDhcp6SrvIfReplyOut [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,5};
UINT4 FsDhcp6SrvIfRelayReplyOut [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,6};
UINT4 FsDhcp6SrvIfInvalidPktIn [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,7};
UINT4 FsDhcp6SrvIfUnknownTlvType [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,8};
UINT4 FsDhcp6SrvIfHmacFailCount [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,9};
UINT4 FsDhcp6SrvIfCounterReset [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,10};
UINT4 FsDhcp6SrvIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,42,2,5,1,11};


tMbDbEntry fsdh6sMibEntry[]= {

{{11,FsDhcp6SrvDebugTrace}, NULL, FsDhcp6SrvDebugTraceGet, FsDhcp6SrvDebugTraceSet, FsDhcp6SrvDebugTraceTest, FsDhcp6SrvDebugTraceDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "critical"},

{{11,FsDhcp6SrvRealmTableNextIndex}, NULL, FsDhcp6SrvRealmTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDhcp6SrvClientTableNextIndex}, NULL, FsDhcp6SrvClientTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDhcp6SrvPoolTableNextIndex}, NULL, FsDhcp6SrvPoolTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsDhcp6SrvIncludePrefixTableNextIndex}, NULL, FsDhcp6SrvIncludePrefixTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
{{11,FsDhcp6SrvTrapAdminControl}, NULL, FsDhcp6SrvTrapAdminControlGet, FsDhcp6SrvTrapAdminControlSet, FsDhcp6SrvTrapAdminControlTest, FsDhcp6SrvTrapAdminControlDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "\0"},

{{11,FsDhcp6SrvSysLogAdminStatus}, NULL, FsDhcp6SrvSysLogAdminStatusGet, FsDhcp6SrvSysLogAdminStatusSet, FsDhcp6SrvSysLogAdminStatusTest, FsDhcp6SrvSysLogAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDhcp6SrvListenPort}, NULL, FsDhcp6SrvListenPortGet, FsDhcp6SrvListenPortSet, FsDhcp6SrvListenPortTest, FsDhcp6SrvListenPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "547"},

{{11,FsDhcp6SrvClientTransmitPort}, NULL, FsDhcp6SrvClientTransmitPortGet, FsDhcp6SrvClientTransmitPortSet, FsDhcp6SrvClientTransmitPortTest, FsDhcp6SrvClientTransmitPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "546"},

{{11,FsDhcp6SrvRelayTransmitPort}, NULL, FsDhcp6SrvRelayTransmitPortGet, FsDhcp6SrvRelayTransmitPortSet, FsDhcp6SrvRelayTransmitPortTest, FsDhcp6SrvRelayTransmitPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "547"},


{{13,FsDhcp6SrvPoolIndex}, GetNextIndexFsDhcp6SrvPoolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvPoolName}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolNameGet, FsDhcp6SrvPoolNameSet, FsDhcp6SrvPoolNameTest, FsDhcp6SrvPoolTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvPoolPreference}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolPreferenceGet, FsDhcp6SrvPoolPreferenceSet, FsDhcp6SrvPoolPreferenceTest, FsDhcp6SrvPoolTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, "0"},

{{13,FsDhcp6SrvPoolDuidType}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolDuidTypeGet, FsDhcp6SrvPoolDuidTypeSet, FsDhcp6SrvPoolDuidTypeTest, FsDhcp6SrvPoolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, "1"},

{{13,FsDhcp6SrvPoolDuid}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolDuidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvPoolDuidIfIndex}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolDuidIfIndexGet, FsDhcp6SrvPoolDuidIfIndexSet, FsDhcp6SrvPoolDuidIfIndexTest, FsDhcp6SrvPoolTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, "1"},

{{13,FsDhcp6SrvPoolOptionTableNextIndex}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolOptionTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDhcp6SrvPoolTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvPoolRowStatus}, GetNextIndexFsDhcp6SrvPoolTable, FsDhcp6SrvPoolRowStatusGet, FsDhcp6SrvPoolRowStatusSet, FsDhcp6SrvPoolRowStatusTest, FsDhcp6SrvPoolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvPoolTableINDEX, 1, 0, 1, NULL},

{{13,FsDhcp6SrvIncludePrefixContextId}, GetNextIndexFsDhcp6SrvIncludePrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6SrvIncludePrefixTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvIncludePrefixIndex}, GetNextIndexFsDhcp6SrvIncludePrefixTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6SrvIncludePrefixTableINDEX, 2, 0, 0, NULL},
{{13,FsDhcp6SrvIncludePrefix}, GetNextIndexFsDhcp6SrvIncludePrefixTable, FsDhcp6SrvIncludePrefixGet, FsDhcp6SrvIncludePrefixSet, FsDhcp6SrvIncludePrefixTest, FsDhcp6SrvIncludePrefixTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvIncludePrefixTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvIncludePrefixPool}, GetNextIndexFsDhcp6SrvIncludePrefixTable, FsDhcp6SrvIncludePrefixPoolGet, FsDhcp6SrvIncludePrefixPoolSet, FsDhcp6SrvIncludePrefixPoolTest, FsDhcp6SrvIncludePrefixTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvIncludePrefixTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvIncludePrefixRowStatus}, GetNextIndexFsDhcp6SrvIncludePrefixTable, FsDhcp6SrvIncludePrefixRowStatusGet, FsDhcp6SrvIncludePrefixRowStatusSet, FsDhcp6SrvIncludePrefixRowStatusTest, FsDhcp6SrvIncludePrefixTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvIncludePrefixTableINDEX, 2, 0, 1, NULL},

{{13,FsDhcp6SrvOptionIndex}, GetNextIndexFsDhcp6SrvOptionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDhcp6SrvOptionTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvOptionType}, GetNextIndexFsDhcp6SrvOptionTable, FsDhcp6SrvOptionTypeGet, FsDhcp6SrvOptionTypeSet, FsDhcp6SrvOptionTypeTest, FsDhcp6SrvOptionTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvOptionTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvOptionLength}, GetNextIndexFsDhcp6SrvOptionTable, FsDhcp6SrvOptionLengthGet, FsDhcp6SrvOptionLengthSet, FsDhcp6SrvOptionLengthTest, FsDhcp6SrvOptionTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvOptionTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvOptionValue}, GetNextIndexFsDhcp6SrvOptionTable, FsDhcp6SrvOptionValueGet, FsDhcp6SrvOptionValueSet, FsDhcp6SrvOptionValueTest, FsDhcp6SrvOptionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvOptionTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvOptionPreference}, GetNextIndexFsDhcp6SrvOptionTable, FsDhcp6SrvOptionPreferenceGet, FsDhcp6SrvOptionPreferenceSet, FsDhcp6SrvOptionPreferenceTest, FsDhcp6SrvOptionTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvOptionTableINDEX, 2, 0, 0, "0"},

{{13,FsDhcp6SrvOptionRowStatus}, GetNextIndexFsDhcp6SrvOptionTable, FsDhcp6SrvOptionRowStatusGet, FsDhcp6SrvOptionRowStatusSet, FsDhcp6SrvOptionRowStatusTest, FsDhcp6SrvOptionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvOptionTableINDEX, 2, 0, 1, NULL},

{{13,FsDhcp6SrvSubOptionType}, GetNextIndexFsDhcp6SrvSubOptionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDhcp6SrvSubOptionTableINDEX, 3, 0, 0, NULL},

{{13,FsDhcp6SrvSubOptionLength}, GetNextIndexFsDhcp6SrvSubOptionTable, FsDhcp6SrvSubOptionLengthGet, FsDhcp6SrvSubOptionLengthSet, FsDhcp6SrvSubOptionLengthTest, FsDhcp6SrvSubOptionTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvSubOptionTableINDEX, 3, 0, 0, NULL},

{{13,FsDhcp6SrvSubOptionValue}, GetNextIndexFsDhcp6SrvSubOptionTable, FsDhcp6SrvSubOptionValueGet, FsDhcp6SrvSubOptionValueSet, FsDhcp6SrvSubOptionValueTest, FsDhcp6SrvSubOptionTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvSubOptionTableINDEX, 3, 0, 0, NULL},

{{13,FsDhcp6SrvSubOptionRowStatus}, GetNextIndexFsDhcp6SrvSubOptionTable, FsDhcp6SrvSubOptionRowStatusGet, FsDhcp6SrvSubOptionRowStatusSet, FsDhcp6SrvSubOptionRowStatusTest, FsDhcp6SrvSubOptionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvSubOptionTableINDEX, 3, 0, 1, NULL},

{{13,FsDhcp6SrvIfIndex}, GetNextIndexFsDhcp6SrvIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfPool}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfPoolGet, FsDhcp6SrvIfPoolSet, FsDhcp6SrvIfPoolTest, FsDhcp6SrvIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfInformIn}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfInformInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfRelayForwIn}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfRelayForwInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfReplyOut}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfReplyOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfRelayReplyOut}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfRelayReplyOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfInvalidPktIn}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfInvalidPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfUnknownTlvType}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfUnknownTlvTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfHmacFailCount}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfHmacFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfCounterReset}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfCounterResetGet, FsDhcp6SrvIfCounterResetSet, FsDhcp6SrvIfCounterResetTest, FsDhcp6SrvIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvIfRowStatus}, GetNextIndexFsDhcp6SrvIfTable, FsDhcp6SrvIfRowStatusGet, FsDhcp6SrvIfRowStatusSet, FsDhcp6SrvIfRowStatusTest, FsDhcp6SrvIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvIfTableINDEX, 1, 0, 1, NULL},

{{13,FsDhcp6SrvRealmIndex}, GetNextIndexFsDhcp6SrvRealmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDhcp6SrvRealmTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvRealmName}, GetNextIndexFsDhcp6SrvRealmTable, FsDhcp6SrvRealmNameGet, FsDhcp6SrvRealmNameSet, FsDhcp6SrvRealmNameTest, FsDhcp6SrvRealmTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvRealmTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvRealmKeyTableNextIndex}, GetNextIndexFsDhcp6SrvRealmTable, FsDhcp6SrvRealmKeyTableNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDhcp6SrvRealmTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvRealmRowStatus}, GetNextIndexFsDhcp6SrvRealmTable, FsDhcp6SrvRealmRowStatusGet, FsDhcp6SrvRealmRowStatusSet, FsDhcp6SrvRealmRowStatusTest, FsDhcp6SrvRealmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvRealmTableINDEX, 1, 0, 1, NULL},

{{13,FsDhcp6SrvClientIndex}, GetNextIndexFsDhcp6SrvClientTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDhcp6SrvClientTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvClientId}, GetNextIndexFsDhcp6SrvClientTable, FsDhcp6SrvClientIdGet, FsDhcp6SrvClientIdSet, FsDhcp6SrvClientIdTest, FsDhcp6SrvClientTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvClientTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvClientIdType}, GetNextIndexFsDhcp6SrvClientTable, FsDhcp6SrvClientIdTypeGet, FsDhcp6SrvClientIdTypeSet, FsDhcp6SrvClientIdTypeTest, FsDhcp6SrvClientTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvClientTableINDEX, 1, 0, 0, "1"},

{{13,FsDhcp6SrvClientRealm}, GetNextIndexFsDhcp6SrvClientTable, FsDhcp6SrvClientRealmGet, FsDhcp6SrvClientRealmSet, FsDhcp6SrvClientRealmTest, FsDhcp6SrvClientTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDhcp6SrvClientTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6SrvClientRowStatus}, GetNextIndexFsDhcp6SrvClientTable, FsDhcp6SrvClientRowStatusGet, FsDhcp6SrvClientRowStatusSet, FsDhcp6SrvClientRowStatusTest, FsDhcp6SrvClientTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvClientTableINDEX, 1, 0, 1, NULL},

{{13,FsDhcp6SrvKeyIdentifier}, GetNextIndexFsDhcp6SrvKeyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDhcp6SrvKeyTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvKey}, GetNextIndexFsDhcp6SrvKeyTable, FsDhcp6SrvKeyGet, FsDhcp6SrvKeySet, FsDhcp6SrvKeyTest, FsDhcp6SrvKeyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6SrvKeyTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6SrvKeyRowStatus}, GetNextIndexFsDhcp6SrvKeyTable, FsDhcp6SrvKeyRowStatusGet, FsDhcp6SrvKeyRowStatusSet, FsDhcp6SrvKeyRowStatusTest, FsDhcp6SrvKeyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6SrvKeyTableINDEX, 2, 0, 1, NULL},
};
tMibData fsdh6sEntry = { 56, fsdh6sMibEntry };
#endif /* _FSDH6SDB_H */

