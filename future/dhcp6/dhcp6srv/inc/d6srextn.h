/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the common extern variables used in 
 *              DHCP6 server module.
 *
 ***************************************************************************/

#ifndef  __DHCP6_SRV_EXTN_H
#define  __DHCP6_SRV_EXTN_H
extern tDhcp6SrvGblInfo    gDhcp6SrvGblInfo;
#endif

