/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6slw.h,v 1.4 2009/10/30 05:21:12 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvRealmTableNextIndex ARG_LIST((UINT4 *));

INT1
nmhGetFsDhcp6SrvClientTableNextIndex ARG_LIST((UINT4 *));

INT1
nmhGetFsDhcp6SrvPoolTableNextIndex ARG_LIST((UINT4 *));

INT1
nmhGetFsDhcp6SrvIncludePrefixTableNextIndex ARG_LIST((UINT4 *));
INT1
nmhGetFsDhcp6SrvTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvSysLogAdminStatus ARG_LIST((INT4 *));

INT1

nmhGetFsDhcp6SrvListenPort ARG_LIST((INT4 *));

INT1
nmhGetFsDhcp6SrvClientTransmitPort ARG_LIST((INT4 *));

INT1
nmhGetFsDhcp6SrvRelayTransmitPort ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvSysLogAdminStatus ARG_LIST((INT4 ));

INT1

nmhSetFsDhcp6SrvListenPort ARG_LIST((INT4 ));

INT1
nmhSetFsDhcp6SrvClientTransmitPort ARG_LIST((INT4 ));

INT1
nmhSetFsDhcp6SrvRelayTransmitPort ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvDebugTrace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvTrapAdminControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvSysLogAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1

nmhTestv2FsDhcp6SrvListenPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvClientTransmitPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvRelayTransmitPort ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvDebugTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6SrvTrapAdminControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6SrvSysLogAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1

nmhDepv2FsDhcp6SrvListenPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6SrvClientTransmitPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6SrvRelayTransmitPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvPoolTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvPoolTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvPoolTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvPoolTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvPoolTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvPoolName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvPoolPreference ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvPoolDuidType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvPoolDuid ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvPoolDuidIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvPoolOptionTableNextIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvPoolRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvPoolName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvPoolPreference ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvPoolDuidType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvPoolDuidIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvPoolRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvPoolName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvPoolPreference ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvPoolDuidType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvPoolDuidIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvPoolRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvPoolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6IncludePrefixTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvIncludePrefixTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6IncludePrefixTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvIncludePrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvIncludePrefix ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
INT1
nmhGetFsDhcp6SrvIncludePrefixPool ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvIncludePrefixRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvIncludePrefix ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
INT1
nmhSetFsDhcp6SrvIncludePrefixPool ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvIncludePrefixRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvIncludePrefix ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
INT1
nmhTestv2FsDhcp6SrvIncludePrefixPool ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvIncludePrefixRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvIncludePrefixTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvOptionTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvOptionTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvOptionTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvOptionTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvOptionTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvOptionType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvOptionLength ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvOptionValue ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvOptionPreference ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvOptionRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvOptionType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvOptionLength ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvOptionValue ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvOptionPreference ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvOptionRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvOptionType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvOptionLength ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvOptionValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvOptionPreference ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvOptionRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvOptionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvSubOptionTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvSubOptionTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvSubOptionTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvSubOptionTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvSubOptionTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvSubOptionLength ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvSubOptionValue ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvSubOptionRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvSubOptionLength ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvSubOptionValue ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvSubOptionRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvSubOptionLength ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvSubOptionValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvSubOptionRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvSubOptionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvClientTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvClientTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvClientTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvClientTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvClientTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvClientId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvClientIdType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvClientRealm ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvClientRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvClientId ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvClientIdType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvClientRealm ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsDhcp6SrvClientRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvClientId ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvClientIdType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvClientRealm ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsDhcp6SrvClientRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvClientTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvRealmTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvRealmTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvRealmTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvRealmTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvRealmTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvRealmName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvRealmKeyTableNextIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvRealmRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvRealmName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvRealmRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvRealmName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvRealmRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvRealmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvKeyTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvKeyTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvKeyTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvKeyTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvKeyTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvKey ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6SrvKeyRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvKey ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6SrvKeyRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvKey ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6SrvKeyRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvKeyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6SrvIfTable. */
INT1
nmhValidateIndexInstanceFsDhcp6SrvIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6SrvIfTable  */

INT1
nmhGetFirstIndexFsDhcp6SrvIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6SrvIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6SrvIfPool ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvIfInformIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvIfRelayForwIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvIfReplyOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvIfRelayReplyOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvIfInvalidPktIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvIfUnknownTlvType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvIfHmacFailCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6SrvIfCounterReset ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6SrvIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6SrvIfPool ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvIfCounterReset ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6SrvIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6SrvIfPool ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvIfCounterReset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6SrvIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6SrvIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
