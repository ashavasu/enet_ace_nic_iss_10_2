#!/bin/csh
# (C) 2009 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent			                  		     |	
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30 Aug 2009                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

DHCP6_SRV_BASE_DIR = ${BASE_DIR}/dhcp6/dhcp6srv
DHCP6_SRV_SRC_DIR  = ${DHCP6_SRV_BASE_DIR}/src
DHCP6_SRV_INC_DIR  = ${DHCP6_SRV_BASE_DIR}/inc
DHCP6_SRV_OBJ_DIR  = ${DHCP6_SRV_BASE_DIR}/obj
ifeq (DDHCP6_SRV_TEST_WANTED, $(findstring DDHCP6_SRV_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
DHCP6_SRV_TST_DIR  = ${DHCP6_SRV_BASE_DIR}/test
DHCP6_SRV_TST_INC_DIR  = ${DHCP6_SRV_BASE_DIR}/test
endif

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${DHCP6_SRV_INC_DIR} -I${DHCP6_SRV_TST_INC_DIR} -I${COMMON_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
