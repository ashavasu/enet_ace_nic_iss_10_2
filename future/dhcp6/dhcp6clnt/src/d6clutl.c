/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6clutl.c,v 1.8 2013/09/12 12:10:03 siva Exp $
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 client
 *               task.
 * ************************************************************************/

#include "d6clinc.h"
#include "utilrand.h"
/******************************************************************************
 * Function Name      : D6ClUtlRBTreeCreateEmbedded
 *
 * Description        : This routine is used to create the RBTree.
 *
 * Input(s)           : u4Offset - Offset 
 *                      Cmp      - Compare Function required for RBTree Creation
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to RBTree
 *****************************************************************************/

PUBLIC              tRBTree
D6ClUtlRBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp)
{
    tRBTree             T;
    T = RBTreeCreateEmbedded (u4Offset, Cmp);
    if (T == NULL)
    {
        return NULL;
    }
    RBTreeDisableMutualExclusion (T);
    return T;
}

/****************************************************************************
 *  
 *  FUNCTION NAME    : D6ClUtlFreeEntryFn
 * 
 *  DESCRIPTION      : This function releases the memory allocated to
 *                     each node of the specified Table.
 *  
 *  INPUT            : pElem - pointer to the node to be freed,
 *                     u4Arg - From which table node needs to be freed.     
 *  
 *  OUTPUT           : None.
 *
 *  RETURNS          : None
 * **************************************************************************/

PUBLIC INT4
D6ClUtlFreeEntryFn (tRBElem * pElem, UINT4 u4Arg)
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo;
    tDhcp6ClntOptionInfo *pDhcp6OptInfo;
    switch (u4Arg)
    {
        case DHCP6_CLNT_INTERFACE_POOL:
            pDhcp6ClntIfInfo = (tDhcp6ClntIfInfo *) pElem;

            D6ClTmrStopAllTimer (pDhcp6ClntIfInfo);

            MemReleaseMemBlock (DHCP6_CLNT_INTERFACE_INFO_POOL,
                                (UINT1 *) (pDhcp6ClntIfInfo));
            break;

        case DHCP6_CLNT_OPTION_POOL:

            pDhcp6OptInfo = (tDhcp6ClntOptionInfo *) pElem;

            MemReleaseMemBlock (DHCP6_CLNT_OPTION_TABLE_POOL,
                                (UINT1 *) (pDhcp6OptInfo));
            break;
    }
    return 1;
}

/*****************************************************************************
 * Name               : D6ClUtlUpdateClientId
 *
 * Description        : This routine will create the Client Id which is the 
 *                      DUID used by DHCPv6 client module while transmittion of
 *                      information request message.
 *
 * Input(s)           : pInterfaceNode - Pointer to Interface Information
 *                                  
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PUBLIC INT4
D6ClUtlUpdateClientId (tDhcp6ClntIfInfo * pInterfaceInfo)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT4               u4CurrentTimeValue = 0;
    UINT4               u4EnterpriseNumber = 0;
    UINT2               u2HadwareType = 0;
    UINT1               u1IdentifierLength = 0;
    UINT1               u1Index = 0;
    UINT1               u1Size = 0;
    UINT1               u1RetVal = OSIX_TRUE;
    UINT1               au1Identifier[DHCP6_CLNT_MAX_IDENTIFIER_SIZE];
    UINT1              *pu1Value = NULL;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (au1Identifier, 0, DHCP6_CLNT_MAX_IDENTIFIER_SIZE);
    switch (pInterfaceInfo->u1ClientIdType)
    {
        case DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME:
            /* 2 Byte Fixed Value 1 
             * 2 Byte Hardware type from the System
             * 4 Byte current Time Value
             * Varible Length Link Layer Address*/
            pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_ZERO] =
                (UINT1) DHCP6_CLNT_VALUE_ZERO;
            pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_ONE] =
                (UINT1) DHCP6_CLNT_VALUE_ONE;

            /*Get Hadware Type Value */
            D6ClPortGetHadwareType (&u2HadwareType);
            pu1Value = &(pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_TWO]);
            DHCP6_CLNT_PUT_2BYTE (pu1Value, u2HadwareType);

            /*Get Current Time Value */
            u4CurrentTimeValue = D6ClCUtlGetTimeFromEpoch ();
            pu1Value = NULL;

            pu1Value = &(pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_FOUR]);
            DHCP6_CLNT_PUT_4BYTE (pu1Value, u4CurrentTimeValue);

            u1Index = (UINT1) DHCP6_CLNT_INDEX_BY_EIGHT;

            /*Get Link Address  Value */
            if (D6ClPortGetInterfaceAddressIdentifier
                (pInterfaceInfo->u4IfDuidIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClUtlUpdateClientId: D6clUtlGetInterfaceAddressIdentifier "
                                "Function Return Failure. \r\n");
                return OSIX_FAILURE;
            }
            u1Size = sizeof (tIp6Addr);
            MEMCPY (&pInterfaceInfo->au1ClientId[u1Index],
                    &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
            u1Index = (UINT1) (u1Index + u1Size);
            pInterfaceInfo->u1ClientIdLength = u1Index;
            break;
        case DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID:

            /* 2 Byte Fixed Value 2 
             * 4 Byte enterprise-number from the System
             * Varible Length identifier */

            pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_ZERO] =
                (UINT1) DHCP6_CLNT_VALUE_ZERO;
            pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_ONE] =
                (UINT1) DHCP6_CLNT_VALUE_TWO;

            /*Get Enterprise Number */
            D6ClPortGetEnterpriseNumber (&u4EnterpriseNumber);

            pu1Value = &(pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_TWO]);
            DHCP6_CLNT_PUT_4BYTE (pu1Value, u4EnterpriseNumber);

            u1Index = (UINT1) DHCP6_CLNT_INDEX_BY_SIX;

            D6ClPortGetIdentifier (&u1IdentifierLength, au1Identifier);
            if (u1IdentifierLength > DHCP6_CLNT_MAX_IDENTIFIER_SIZE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClUtlUpdateClientId: D6clUtlGetIdentifier "
                                "Return Maxmium Identifier Value. \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pInterfaceInfo->au1ClientId[u1Index],
                    &au1Identifier, u1IdentifierLength);
            u1Index = (UINT1) (u1Index + u1IdentifierLength);
            pInterfaceInfo->u1ClientIdLength = u1Index;

            break;
        case DHCP6_CLNT_LINK_LAYER_ADD:

            /* 2 Byte Fixed Value 3
             * 2 Byte Hardware type from the System
             * Varible Length Link Layer Address*/
            pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_ZERO] =
                (UINT1) DHCP6_CLNT_VALUE_ZERO;
            pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_ONE] =
                (UINT1) DHCP6_CLNT_VALUE_THREE;

            D6ClPortGetHadwareType (&u2HadwareType);

            pu1Value = &(pInterfaceInfo->au1ClientId[DHCP6_CLNT_INDEX_BY_TWO]);
            DHCP6_CLNT_PUT_2BYTE (pu1Value, u2HadwareType);

            u1Index = (UINT1) DHCP6_CLNT_INDEX_BY_FOUR;

            if (D6ClPortGetInterfaceAddressIdentifier
                (pInterfaceInfo->u4IfDuidIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClUtlUpdateClientId: D6clUtlGetInterfaceAddressIdentifier "
                                "Function Return Failure. \r\n");
                return OSIX_FAILURE;
            }
            u1Size = sizeof (tIp6Addr);
            MEMCPY (&pInterfaceInfo->au1ClientId[u1Index],
                    &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
            u1Index = (UINT1) (u1Index + u1Size);
            pInterfaceInfo->u1ClientIdLength = u1Index;

            break;

        default:
            u1RetVal = OSIX_FALSE;
            break;
    }
    if (u1RetVal == OSIX_FALSE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClUtlUpdateClientId: Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : D6ClCUtlGetTimeFromEpoch                      */
/*                                                                          */
/*    Description         : This function returns the number of seconds     */
/*                          passed from epoch.                             */
/*    Input(s)            : None.                                           */
/*                                                                          */
/*    Output(s)           : None                                            */
/*                                                                          */
/*    Returns             : Number of seconds passed from epoch             */
/*                                                                          */
/* **************************************************************************/
PUBLIC UINT4
D6ClCUtlGetTimeFromEpoch (VOID)
{
    tOsixSysTime        SysTime;
    OsixGetSysTime (&SysTime);
    return (SysTime);
}

/*****************************************************************************/
/* Function Name      : D6ClUtlGenerateRandPseudoNumber                       */
/*                                                                           */
/* Description        : This function calls the opensource pseudo random     */
/*                      generator to get the random number between -1 and 1. */
/*                                                                           */
/* Input(s)           : pi2Number - Random Number                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6ClUtlGenerateRandPseudoNumber (INT2 *pi2Number, INT2 i2MinVal, INT2 i2MaxVal)
{
    INT2                i2RandValue = 0;

    i2RandValue = (UINT2) OSIX_RAND (i2MinVal, i2MaxVal);
    MEMCPY (pi2Number, &i2RandValue, DHCP6_CLNT_VALUE_TWO);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : D6ClUtlCfgQueMsg                                         *
 *                                                                            *
 * DESCRIPTION      : Function to post configuration message and events to    * 
 *                    DHCP6 Client Task.                                      *
 *                                                                            *
 * INPUT            : pMsg - Pointer to msg to be posted                      *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                            *
 ******************************************************************************/

PUBLIC INT4
D6ClUtlCfgQueMsg (tDhcp6ClntQMsg * pMsg)
{
    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClUtlCfgQueMsg: DHCP6C not initialised. \r\n");
        return OSIX_FAILURE;
    }

    if (OsixQueSend (DHCP6_CLNT_CFG_QUEUE_ID, (UINT1 *) (&pMsg),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClUtlCfgQueMsg: DHCP6C Enque Failed. \r\n");
        return OSIX_FAILURE;
    }

    /* Sending Event */
    OsixEvtSend (DHCP6_CLNT_TASK_ID, DHCP6_CLNT_EV_DHCP6_MSG_IN_QUEUE);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClUtlIsClientRowCreated
 *                                                                          
 *    DESCRIPTION      : This function returns OSIX_FAILURE when Interface Row
 *                       is not created in the Client Mode.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClUtlIsClientRowCreated (UINT4 u4InterfaceId)
{
    tDhcp6ClntIfInfo   *pInterfaceInfo = NULL;
    pInterfaceInfo = D6ClIfGetInterfaceInfo (u4InterfaceId);

    if (pInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClUtIsClientRowCreated: Interface does not exists in"
                        " client Mode.\r\n");
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClUtlCalculateRemTime
 *                                                                          
 *    DESCRIPTION      : This function is called when Row status is set as
 *                       not in service and calculate the Remaining value
 *                       for the Refresh and Retransmission Timer .
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_FALSE/OSIX_TRUE
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClUtlCalculateRemTime (UINT4 u4InterfaceId)
{
    tDhcp6ClntIfInfo   *pInterfaceInfo = NULL;
    UINT4               u4RefRemainingTime = 0;
    UINT4               u4RetRemainingTime = 0;
    pInterfaceInfo = D6ClIfGetInterfaceInfo (u4InterfaceId);

    if (pInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClUtlCalculateRemTime: Interface does not exists in"
                        " client Mode.\r\n");
        return OSIX_FALSE;
    }
    TmrGetRemainingTime (gDhcp6ClntGblInfo.TimerListId,
                         &(pInterfaceInfo->RefreshTimer.TimerNode),
                         &u4RefRemainingTime);

    if (u4RefRemainingTime != 0)
    {
        u4RefRemainingTime =
            DHCP6_CLNT_CONVERT_TIME_TICKS_TO_MSEC (u4RefRemainingTime);
        pInterfaceInfo->u2RefMiliSecRemTime =
            (UINT2) (u4RefRemainingTime % DHCP6_CLNT_TMR_COVERT_MSEC);
        pInterfaceInfo->u4RefSecRemTime =
            u4RefRemainingTime / DHCP6_CLNT_TMR_COVERT_SEC;
    }
    else
    {
        pInterfaceInfo->u2RefMiliSecRemTime = 0;
        pInterfaceInfo->u4RefSecRemTime = 0;
    }
    TmrGetRemainingTime (gDhcp6ClntGblInfo.TimerListId,
                         &(pInterfaceInfo->ReTransmissionTimer.TimerNode),
                         &u4RetRemainingTime);
    if (u4RetRemainingTime != 0)
    {
        u4RetRemainingTime =
            DHCP6_CLNT_CONVERT_TIME_TICKS_TO_MSEC (u4RetRemainingTime);
        pInterfaceInfo->u2RetMiliSecRemTime =
            (UINT2) (u4RetRemainingTime % DHCP6_CLNT_TMR_COVERT_MSEC);
        pInterfaceInfo->u4RetSecRemTime =
            u4RetRemainingTime / DHCP6_CLNT_TMR_COVERT_SEC;
    }
    else
    {
        pInterfaceInfo->u2RetMiliSecRemTime = 0;
        pInterfaceInfo->u4RetSecRemTime = 0;
    }
    return OSIX_TRUE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClUtlStartTimeWithRem
 *                                                                          
 *    DESCRIPTION      : This function is called when Row status is set as
 *                       active again after doing the Not in service.This
 *                       routine will start the Refresh or Retransmission Timer
 *                       with the Remaining value which was calculated when Row
 *                       status is set Not in service.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_FALSE/OSIX_TRUE
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClUtlStartTimeWithRem (UINT4 u4InterfaceId)
{

    tDhcp6ClntIfInfo   *pInterfaceInfo = NULL;

    pInterfaceInfo = D6ClIfGetInterfaceInfo (u4InterfaceId);

    if (pInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClUtlCalculateRemTime: Interface does not exists in"
                        " client Mode.\r\n");
        return OSIX_FALSE;
    }

    if ((pInterfaceInfo->u4RetSecRemTime != 0)
        || (pInterfaceInfo->u2RetMiliSecRemTime != 0))
    {
        D6ClTmrStart (pInterfaceInfo, DHCP6_CLNT_TMR_RE_TRANSMISSION,
                      pInterfaceInfo->u4RetSecRemTime);
    }
    if ((pInterfaceInfo->u4RefSecRemTime != 0)
        || (pInterfaceInfo->u2RefMiliSecRemTime != 0))
    {
        D6ClTmrStart (pInterfaceInfo, DHCP6_CLNT_TMR_REFRESH_TIMER,
                      pInterfaceInfo->u4RefSecRemTime);
    }
    pInterfaceInfo->u2RetMiliSecRemTime = 0;
    pInterfaceInfo->u2RefMiliSecRemTime = 0;
    pInterfaceInfo->u4RefSecRemTime = 0;
    pInterfaceInfo->u4RetSecRemTime = 0;
    return OSIX_TRUE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClUtlValidateIfPropery
 *                                                                          
 *    DESCRIPTION      : This function is used to validate the Interace
 *                       properties row status ,Oper status and IP address 
 *                       status.
 *
 *    INPUT            : pInterfaceInfo Pointer toi interface  
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClUtlValidateIfPropery (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo)
{
    tNetIpv6IfInfo      NetIp6IfInfo;
    tIp6Addr            Ip6Addr;
    INT4                i4RetVal = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIp6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    /*Check If Interface Status is Up or Not */
    /*Check If Interface is Assigned to IP Address or Not */
    /*Check If Row Status is Active or Not */

    i4RetVal = NetIpv6GetIfInfo (pDhcp6ClntIfInfo->u4IfIndex, &NetIp6IfInfo);

    if (i4RetVal == NETIPV6_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_MGMT_TRC,
                        "D6ClUtlValidateIfPropery: NetIpv6GetIfInfo Return Failure.!\r\n");
        return OSIX_FAILURE;
    }

    if ((MEMCMP (&NetIp6IfInfo.Ip6Addr, &Ip6Addr, sizeof (tIp6Addr))) == 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClUtlValidateIfPropery: IP address is not assigned to Interface. \r\n");
        return OSIX_FAILURE;
    }
    if (pDhcp6ClntIfInfo->u1IfRowStatus != ACTIVE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClUtlValidateIfPropery: Interface Sttaus is Inactive.\r\n");
        return OSIX_FAILURE;
    }

    if (pDhcp6ClntIfInfo->u4OperStatus == OSIX_FALSE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClUtlValidateIfPropery: DHCP6 Interface Status is Down. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClUtlCliConfGetIfName                                    */
/*                                                                           */
/* Description  : This function gets the Interface name of the gives         */
/*                interface                                                  */
/*                                                                           */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pu1fName - pointer to IP Address.                       */
/*                                                                           */
/* Output       : Interface Name                                             */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6ClUtlCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return OSIX_SUCCESS;
#endif
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clutl.c                       */
/*-----------------------------------------------------------------------*/
