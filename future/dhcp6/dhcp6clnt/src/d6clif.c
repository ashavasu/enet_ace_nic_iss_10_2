/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6clif.c,v 1.12 2013/12/17 12:17:11 siva Exp $
 *
 * Description: This file contains external interface functions for 
 *              Interface Table.
 * ************************************************************************/

#include "d6clinc.h"

PRIVATE INT4 D6ClIfTableCmp PROTO ((tRBElem * pE1, tRBElem * pE2));
/****************************************************************************
 *    FUNCTION NAME    : D6ClIfCreateTable 
 *                                                                          
 *    DESCRIPTION      : This function will create the interface Memory Pools.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClIfCreateTable ()
{
    /* Create Global RB-Trees for Interface Table */
    DHCP6_CLNT_INTERFACE_INFO_TABLE =
        D6ClUtlRBTreeCreateEmbedded (FSAP_OFFSETOF
                                     (tDhcp6ClntIfInfo, RbLink),
                                     D6ClIfTableCmp);

    if (DHCP6_CLNT_INTERFACE_INFO_TABLE == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfCreateTable: unable to create global"
                        " RB-Tree for Interface Table.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClIfTableCmp                                    */
/*                                                                           */
/* Description  : This s the compare function used for RBTree.               */
/*                It compares the two RBTree Nodes.                          */
/*                                                                           */
/* Input        : *pE1 - pointer to MD Node1                                 */
/*                *pE2 - pointer to MD Node2                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : If not Equal, returns 0, returns 1 if pE1 > pE2            */
/*                else returns -1                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
D6ClIfTableCmp (tRBElem * pE1, tRBElem * pE2)
{
    tDhcp6ClntIfInfo   *pClntIfEntryA = NULL;
    tDhcp6ClntIfInfo   *pClntIfEntryB = NULL;

    pClntIfEntryA = (tDhcp6ClntIfInfo *) pE1;
    pClntIfEntryB = (tDhcp6ClntIfInfo *) pE2;
    if (pClntIfEntryA->u4IfIndex != pClntIfEntryB->u4IfIndex)
    {
        if (pClntIfEntryA->u4IfIndex < pClntIfEntryB->u4IfIndex)
        {
            return DHCP6_CLNT_RB_LESS;
        }

        else
        {
            return DHCP6_CLNT_RB_GREATER;
        }
    }
    return DHCP6_CLNT_RB_EQUAL;
}

/****************************************************************************
 *    FUNCTION NAME    : D6ClIfDelateMemoryPools 
 *                                                                          
 *    DESCRIPTION      : This function will delete the interface Memory Pools.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PUBLIC VOID
D6ClIfDelateTable ()
{
    if (DHCP6_CLNT_INTERFACE_INFO_TABLE != NULL)
    {
        RBTreeDestroy (DHCP6_CLNT_INTERFACE_INFO_TABLE,
                       (tRBKeyFreeFn) D6ClUtlFreeEntryFn,
                       DHCP6_CLNT_INTERFACE_POOL);
        DHCP6_CLNT_INTERFACE_INFO_TABLE = NULL;
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfGetInterfaceInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Interface
 *                       entry from the global structure, Interface Table.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                       which the entry is required.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Interface Entry`
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntIfInfo *
D6ClIfGetInterfaceInfo (UINT4 u4InterfaceId)
{
    PRIVATE tDhcp6ClntIfInfo InterfaceInfo;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;
    MEMSET (&InterfaceInfo, 0, DHCP6_CLNT_INTERFACE_INFO_SIZE);

    /* Get Interface entry, based on u4InterfaceId */
    InterfaceInfo.u4IfIndex = u4InterfaceId;
    pInterfaceNode =
        RBTreeGet (DHCP6_CLNT_INTERFACE_INFO_TABLE,
                   (tRBElem *) & InterfaceInfo);
    return pInterfaceNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfGetNextInterfaceInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Next
 *                       Interface entry from the global structure,
 *                       Interface Table.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                       which the entry is required.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Interface Entry`
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC tDhcp6ClntIfInfo *
D6ClIfGetNextInterfaceInfo (UINT4 u4InterfaceId)
{
    PRIVATE tDhcp6ClntIfInfo InterfaceInfo;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;
    MEMSET (&InterfaceInfo, 0, DHCP6_CLNT_INTERFACE_INFO_SIZE);

    /* Get Interface entry, based on u4InterfaceId */
    InterfaceInfo.u4IfIndex = u4InterfaceId;
    pInterfaceNode =
        RBTreeGetNext (DHCP6_CLNT_INTERFACE_INFO_TABLE,
                       (tRBElem *) & InterfaceInfo, NULL);
    return pInterfaceNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfGetFirstInterfaceInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Interface
 *                       entry from the global RBTree.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Interface  Entry`
 *    
 ****************************************************************************/

PUBLIC tDhcp6ClntIfInfo *
D6ClIfGetFirstInterfaceInfo ()
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = RBTreeGetFirst (DHCP6_CLNT_INTERFACE_INFO_TABLE);
    return pInterfaceNode;
}

/*****************************************************************************
 * Name               : D6ClIfGetNextInterfaceIndex
 *
 * Description        : This routine return the Next Valid Interface Index Value 
 *                      from the 4Dhcp6CInterfaceIndex.
 *
 * Input(s)           : i4Dhcp6CInterfaceIndex - Interface index
 *                      pi4Dhcp6CNextInterfaceIndex - Next Interface Index Pointer          
 *                                  
 * Output(s)          : pi4Dhcp6CNextInterfaceIndex - Next Interface Index Pointer
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PUBLIC INT4
D6ClIfGetNextInterfaceIndex (INT4 i4Dhcp6CInterfaceIndex,
                             INT4 *pi4Dhcp6CNextInterfaceIndex)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    /* Get Next Interface entry, based on u4InterfaceId */
    pInterfaceNode = D6ClIfGetNextInterfaceInfo (i4Dhcp6CInterfaceIndex);

    if (pInterfaceNode != NULL)
    {
        *pi4Dhcp6CNextInterfaceIndex = pInterfaceNode->u4IfIndex;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Name               : D6ClIfValidateInterfaceEntry
 *
 * Description        : This routine Validate the i4Dhcp6CInterfaceIndex and
 *                      return OSIX_SUCCESS when Interface exists for the
 *                      DHCPv6 client module.
 *
 * Input(s)           : i4Dhcp6CInterfaceIndex - Interface Index
 *                                  
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PUBLIC INT4
D6ClIfValidateInterfaceEntry (UINT4 i4Dhcp6CInterfaceIndex)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    if (i4Dhcp6CInterfaceIndex < DHCP6_CLNT_MIN_IFACES
        || i4Dhcp6CInterfaceIndex > MAX_D6CL_CLNT_IFACES)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfValidateInterfaceEntry: Invalid Interface. \r\n");
        return OSIX_FAILURE;
    }

    pInterfaceNode = D6ClIfGetInterfaceInfo (i4Dhcp6CInterfaceIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfValidateInterfaceEntry: Interface does not exists.\r\n");
        return (INT1) OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClIfValidateInterfaceIndex 
 *
 * Description        : This routine validates the Interface Id Value.
 *
 * Input(s)           : i4Dhcp6CInterfaceIndex - Interface index
 *                                  
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PUBLIC INT4
D6ClIfValidateInterfaceIndex (INT4 i4Dhcp6CInterfaceIndex)
{
    if (i4Dhcp6CInterfaceIndex < DHCP6_CLNT_MIN_IFACES
        || i4Dhcp6CInterfaceIndex > MAX_D6CL_CLNT_IFACES)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfValidateInterfaceIndex: "
                        "Invalid Interface Index. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfReSetAppIdFromOpt 
 *                                                                          
 *    DESCRIPTION      : This function reset the Application id from all the 
 *                       Options from the interface.
 *
 *    INPUT            : pInterfaceNode -Pointer to Interface .
 *                       u2AppId - Application Id.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC VOID
D6ClIfReSetAppIdFromOpt (tDhcp6ClntIfInfo * pInterfaceNode, UINT2 u2AppId)
{
    tDhcp6ClntOptionInfo *pOptionInfo = NULL;
    tDhcp6ClntOptionInfo *pDhcp6ClntGlobOptionInfo = NULL;
    UINT4               u4Ifindex = 0;

    u4Ifindex = pInterfaceNode->u4IfIndex;
    pOptionInfo = D6ClOptGetFirstInterOptionInfo (u4Ifindex);

    while (pOptionInfo != NULL)
    {
        if (u2AppId < MAX_D6CL_CLNT_APPS && u2AppId > DHCP6_CLNT_RESERVE_APP_ID)
        {
            if (pOptionInfo->au1ApplicationMap[u2AppId] != 0)
            {
                /*Reset the u2AppId From the Database */
                DHCP6_CLNT_RESET_MEMBER_APP (pOptionInfo->
                                             au1ApplicationMap, u2AppId);
                pDhcp6ClntGlobOptionInfo =
                    D6ClOptGetGlobOptionInfo (u4Ifindex, pOptionInfo->u2Type);
                if (pDhcp6ClntGlobOptionInfo != NULL)
                {
                    DHCP6_CLNT_RESET_MEMBER_APP (pDhcp6ClntGlobOptionInfo->
                                                 au1ApplicationMap, u2AppId);
                    /*Reset the u2AppId From the Database */
                }
            }
        }
        pOptionInfo = D6ClOptGetNextInterOptionInfo (pOptionInfo, u4Ifindex);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfDeleteInterface 
 *                                                                          
 *    DESCRIPTION      : This function delate the Interface from the DHCPv6
 *                       Module.
 *
 *    INPUT            : u4InterfaceId - Interface Index
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None
 *    
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
D6ClIfDeleteInterface (UINT4 u4InterfaceId)
{
    PRIVATE tDhcp6ClntIfInfo InterfaceInfo;
    tDhcp6ClntOptionInfo *pOptionInfo = NULL;
    tDhcp6ClntOptionInfo *pSavedNextOptionInfo = NULL;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;
    UINT4               u4Counter = 0;
    UINT2               u2AppId = 0;

    MEMSET (&InterfaceInfo, 0, DHCP6_CLNT_INTERFACE_INFO_SIZE);

    /* Get Interface entry, based on u4InterfaceId */
    InterfaceInfo.u4IfIndex = u4InterfaceId;
    /*Delaet all the Information exists on the interface */

    pInterfaceNode =
        RBTreeGet (DHCP6_CLNT_INTERFACE_INFO_TABLE,
                   (tRBElem *) & InterfaceInfo);
    if (pInterfaceNode != NULL)
    {
        /*Delete the Configuration Info List */

        for (u2AppId = 0; u2AppId < MAX_D6CL_CLNT_APPS; u2AppId++)
        {
            for (u4Counter = 1; u4Counter < DHCP6_CLNT_USER_CLASS_MAX;
                 u4Counter++)
            {
                if (pInterfaceNode->UserClass[u2AppId][u4Counter].u2Length != 0)
                {
                    pInterfaceNode->UserClass[u2AppId][u4Counter].u2Code = 0;
                    pInterfaceNode->UserClass[u2AppId][u4Counter].u2Length = 0;
                    MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                  (UINT1 *) pInterfaceNode->
                                  UserClass[u2AppId][u4Counter].pu1Value);
                    pInterfaceNode->UserClass[u2AppId][u4Counter].pu1Value =
                        NULL;
                }
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_CLASS_MAX;
                 u4Counter++)
            {
                if (pInterfaceNode->VendorClass[u2AppId][u4Counter].u2Length !=
                    0)
                {
                    pInterfaceNode->VendorClass[u2AppId][u4Counter].u2Code = 0;
                    pInterfaceNode->VendorClass[u2AppId][u4Counter].u2Length =
                        0;
                    MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                  (UINT1 *) pInterfaceNode->
                                  VendorClass[u2AppId][u4Counter].pu1Value);
                    pInterfaceNode->VendorClass[u2AppId][u4Counter].pu1Value =
                        NULL;
                }
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_SPEC_MAX;
                 u4Counter++)
            {
                if (pInterfaceNode->VendorSpec[u2AppId][u4Counter].u2Length !=
                    0)
                {
                    pInterfaceNode->VendorSpec[u2AppId][u4Counter].u2Code = 0;
                    pInterfaceNode->VendorSpec[u2AppId][u4Counter].u2Length = 0;
                    MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                  (UINT1 *) pInterfaceNode->
                                  VendorSpec[u2AppId][u4Counter].pu1Value);
                    pInterfaceNode->VendorSpec[u2AppId][u4Counter].pu1Value =
                        NULL;
                }
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_REQ_OPT_MAX; u4Counter++)
            {
                pInterfaceNode->aau1RequestOption[u2AppId][u4Counter] = 0;
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_OPT_MAX; u4Counter++)
            {
                pInterfaceNode->aau1Option[u2AppId][u4Counter] = 0;
            }
        }
        D6ClIfReSetAppIdFromOpt (pInterfaceNode, u2AppId);
        pOptionInfo = D6ClOptGetFirstInterOptionInfo (u4InterfaceId);
        while (pOptionInfo != NULL)
        {
            pSavedNextOptionInfo = D6ClOptGetNextInterOptionInfo (pOptionInfo,
                                                                  u4InterfaceId);
            D6ClOptDelOptionNode (pOptionInfo);
            pOptionInfo = pSavedNextOptionInfo;
        }

        if (D6ClTmrStopAllTimer (pInterfaceNode) == OSIX_FAILURE)
        {
            return;
        }

        RBTreeRem (DHCP6_CLNT_INTERFACE_INFO_TABLE, (tRBElem *) pInterfaceNode);
        MemReleaseMemBlock (DHCP6_CLNT_INTERFACE_INFO_POOL,
                            (UINT1 *) pInterfaceNode);
    }
    return;
}

/******************************************************************************
 * Function Name      : D6ClIfCreateInterface
 *
 * Description        : This routine is used to create the Interface
 *
 * Input(s)           : u4IfIndex - Interface Index
 *                      pNetIp6IfInfo- Pointer to Interface Info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to Node
 *****************************************************************************/
PUBLIC INT4
D6ClIfCreateInterface (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIp6IfInfo)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    UINT4               u4Counter = 0;
    UINT2               u2AppId = 0;

    if (pNetIp6IfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfCreateInterface: Interface does not exists in"
                        " IPV6. \r\n");
        return (INT1) OSIX_FAILURE;
    }
    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (u4IfIndex);
    if (pClntInterfaceInfo != NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfCreateInterface: Interface already exists. \r\n");
        return (INT1) OSIX_SUCCESS;
    }

    pClntInterfaceInfo = (tDhcp6ClntIfInfo *) MemAllocMemBlk
        (DHCP6_CLNT_INTERFACE_INFO_POOL);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClIfCreateInterface: Memory Allocation Failure. \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pClntInterfaceInfo, 0x00, sizeof (tDhcp6ClntIfInfo));
    MEMSET (&pClntInterfaceInfo->CurrentSrvAddr, 0, sizeof (tIp6Addr));
    pClntInterfaceInfo->u4KeyIdentifier = (UINT4) DHCP6_CLNT_VALUE_ONE;
    pClntInterfaceInfo->u4IfIndex = pNetIp6IfInfo->u4IfIndex;
    pClntInterfaceInfo->u4IfDuidIndex = u4IfIndex;
    pClntInterfaceInfo->u4InformOut = 0;
    pClntInterfaceInfo->u4ReplyIn = 0;
    pClntInterfaceInfo->u4HmacFailCount = 0;
    pClntInterfaceInfo->u4InvalidPktIn = 0;
    pClntInterfaceInfo->u4TransactionId = 0;
    pClntInterfaceInfo->u2MaxRetDelay = 0;
    pClntInterfaceInfo->u2MaxRetTime =
        DHCP6_CLNT_DEF_INIT_MAX_RETRANSMISSION_TIME;
    pClntInterfaceInfo->u2CurrentRetTime = 0;
    pClntInterfaceInfo->u2CurrentRetDelay = 0;
    pClntInterfaceInfo->u2InitRetTime = DHCP6_CLNT_DEF_INIT_RETRANSMISSION_TIME;
    pClntInterfaceInfo->u4RefreshTime = 0;
    pClntInterfaceInfo->u4LastTransId = 0;
    pClntInterfaceInfo->u1ClientIdType =
        DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME;
    pClntInterfaceInfo->u1MaxRetCount = 0;
    pClntInterfaceInfo->u1CurrRetCount = 0;
    pClntInterfaceInfo->u4OperStatus = pNetIp6IfInfo->u4Oper;;
    pClntInterfaceInfo->u1PreferanceValue = 0;
    pClntInterfaceInfo->u1IfRowStatus = 0;
    pClntInterfaceInfo->b1IsClientIdTrsmt = OSIX_FALSE;
    pClntInterfaceInfo->b1IsInterfaceLck = OSIX_FALSE;
    pClntInterfaceInfo->b1IsRetansMessage = OSIX_FALSE;
    pClntInterfaceInfo->b1IsInfoExists = OSIX_FALSE;
    pClntInterfaceInfo->b1FirstInfoMessage = OSIX_FALSE;
    pClntInterfaceInfo->u1RealmLength = 0;
    pClntInterfaceInfo->u1TransType = 0;
    pClntInterfaceInfo->u1KeyLength = 0;
    pClntInterfaceInfo->u4MinRefreshTime = DHCP6_CLNT_DEFAULT_REFRESH_TIME;
    pClntInterfaceInfo->u2CurrAppReqCode = 0;
    pClntInterfaceInfo->u2RefMiliSecRemTime = 0;
    pClntInterfaceInfo->u2RetMiliSecRemTime = 0;

    D6ClOptInitOptionList (pClntInterfaceInfo);
    for (u2AppId = 0; u2AppId < MAX_D6CL_CLNT_APPS; u2AppId++)
    {
        for (u4Counter = 0; u4Counter < DHCP6_CLNT_OPT_MAX; u4Counter++)
        {
            if (u4Counter == DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE)
            {
                pClntInterfaceInfo->aau1Option[u2AppId][u4Counter] =
                    DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE;
            }
            if (u4Counter == DHCP6_CLNT_ELPASED_OPTION_TYPE)
            {
                pClntInterfaceInfo->aau1Option[u2AppId][u4Counter] =
                    DHCP6_CLNT_ELPASED_OPTION_TYPE;
            }
        }
    }

    if ((D6ClUtlUpdateClientId (pClntInterfaceInfo)) == OSIX_FAILURE)
    {
        D6ClTmrStopAllTimer (pClntInterfaceInfo);
        MemReleaseMemBlock (DHCP6_CLNT_INTERFACE_INFO_POOL, (UINT1 *)
                            pClntInterfaceInfo);
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClIfCreateInterface: D6clUtlUpdateClientId Function "
                        "Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (DHCP6_CLNT_INTERFACE_INFO_TABLE, pClntInterfaceInfo) ==
        RB_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClIfCreateInterface: RBTree Addition Failure. \r\n");
        D6ClTmrStopAllTimer (pClntInterfaceInfo);
        MemReleaseMemBlock (DHCP6_CLNT_INTERFACE_INFO_POOL,
                            (UINT1 *) pClntInterfaceInfo);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#ifdef DHCP6_RELAY
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfIsRelayRowCreated 
 *                                                                          
 *    DESCRIPTION      : This function returns OSIX_FAILURE when Interface Row
 *                       is already created in the Relay Mode.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClIfIsRelayRowCreated (UINT4 u4InterfaceId)
{
    if (D6ClUtlIsRelayRowCreated (u4InterfaceId) == OSIX_FALSE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClIfIsRelayRowCreated: Interface is already created "
                        "in Relay Mode. \r\n");
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}
#endif

#ifdef DHCP6_SERVER
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClIfIsServerRowCreated 
 *                                                                          
 *    DESCRIPTION      : This function returns OSIX_FAILURE when Interface Row
 *                       is already created in the Server Mode.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClIfIsServerRowCreated (UINT4 u4InterfaceId)
{
    if (D6ClUtlIsRelayRowCreated (u4InterfaceId) == OSIX_FALSE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClIfIsServerRowCreated: Interface is already created "
                        "in Server Mode. \r\n");
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clif.c                       */
/*-----------------------------------------------------------------------*/
