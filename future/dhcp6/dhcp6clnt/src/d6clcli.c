/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6clcli.c,v 1.19 2017/11/24 12:40:09 siva Exp $ 
 *
 * Description: This file contains the CLI SET/GET/TEST and GET NEXT
 *              routines for the MIB objects specified in fsdhcp6c.mib
 *  
 *   
 * ************************************************************************/
#ifndef _DHCP6_CLNT_CLI_C
#define _DHCP6_CLNT_CLI_C
#include "d6clinc.h"
#include "fsdh6ccli.h"
PRIVATE VOID D6SrCliPrintChar PROTO ((tCliHandle, UINT1 *, INT4, INT4));
PRIVATE VOID D6SrCliPrintHex PROTO ((tCliHandle, UINT1 *, INT4, INT4));
PRIVATE VOID D6SrCliPrintOption PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrCliPrintDomainName PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrCliPrintVendorSpec PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrCliPrintDnsServers PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrCliPrintVendorClass PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrCliPrintUserClass PROTO ((tCliHandle, UINT1 *, INT4));

/*******************************************************************************
 *    FUNCTION NAME    : cli_process_d6cl_cmd                                *
 *                                                                             *
 *    DESCRIPTION      : Protocol CLI Message Handler Function to handle       *
 *                       configuration messages only.                          *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Command - Command identifier                        *
 *                       ... - Variable command argument list                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/

INT4
cli_process_d6cl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[DHCP6_CLNT_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT4               u4Value = 0;
    UINT4               u4CmdType = 0;
    UINT4               u4Index = 0;
    UINT4               u4DuidIfIndex = 0;
    INT4                i4RetVal = 0;

    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r%% DHCP6_CLNT Client module is not initialised\r\n");
        return CLI_FAILURE;

    }
    DHCP6_CLNT_REGISTER_CLI_LOCK ();

    va_start (ap, u4Command);

    /*Third Argument is always the interface Index */

    u4InterfaceIndex = va_arg (ap, UINT4);

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == (INT1) DHCP6_CLNT_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);
    D6ClApiLock ();
    switch (u4Command)
    {
        case CLI_DHCP6_CLNT_INTERFACE_PROPERTIES:
            /*args[0]-> type of command  */
            /*args[1]-> value of command  */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            u4Index = CLI_GET_IFINDEX ();

            if (u4CmdType == DHCP6_CLNT_INITIAL_RETRANSMISSION_TIME)
            {
                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal =
                    D6ClCliSetInitRetraTime (CliHandle, u4Index, u4Value);

            }
            else if (u4CmdType == DHCP6_CLNT_MAX_RETRANSMISSION_TIME)
            {
                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal =
                    D6ClCliSetMaxRetranTime (CliHandle, u4Index, u4Value);
            }
            else if (u4CmdType == DHCP6_CLNT_MAX_RETRANSMISSION_COUNT)
            {
                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal =
                    D6ClCliSetMaxRetranCount (CliHandle, u4Index, u4Value);
            }
            else
            {
                u4Value = CLI_PTR_TO_U4 (args[1]);
                i4RetVal = D6ClCliSetMaxRetranDelay (CliHandle,
                                                     u4Index, u4Value);
            }
            break;
        case CLI_DHCP6_CLNT_SYSLOG_STATUS:

            i4RetVal = D6ClCliSetSysLogStatus
                (CliHandle, CLI_PTR_TO_U4 (args[DHCP6_CLNT_INDEX_BY_ZERO]));

            break;
        case CLI_DHCP6_CLNT_REFRESH_TIMER_VAL:

            u4Index = CLI_GET_IFINDEX ();
            u4Value = CLI_PTR_TO_U4 (args[0]);
            i4RetVal = D6ClCliSetRefreshTimerVal (CliHandle, u4Index, u4Value);
            break;
        case CLI_DHCP6_CLNT_NO_REFRESH_TIMER_VAL:

            u4Index = CLI_GET_IFINDEX ();
            u4Value = DHCP6_CLNT_DEFAULT_REFRESH_TIME;
            i4RetVal = D6ClCliSetRefreshTimerVal (CliHandle, u4Index, u4Value);
            break;
        case CLI_DHCP6_CLNT_NO_INTERFACE_PROPERTIES:
            /*args[0]-> type of command  */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            u4Index = CLI_GET_IFINDEX ();

            if (u4CmdType == DHCP6_CLNT_INITIAL_RETRANSMISSION_TIME)
            {
                u4Value = DHCP6_CLNT_DEF_INIT_RETRANSMISSION_TIME;
                i4RetVal =
                    D6ClCliSetInitRetraTime (CliHandle, u4Index, u4Value);

            }
            else if (u4CmdType == DHCP6_CLNT_MAX_RETRANSMISSION_TIME)
            {
                u4Value = DHCP6_CLNT_DEF_INIT_MAX_RETRANSMISSION_TIME;
                i4RetVal =
                    D6ClCliSetMaxRetranTime (CliHandle, u4Index, u4Value);
            }
            else if (u4CmdType == DHCP6_CLNT_MAX_RETRANSMISSION_COUNT)
            {
                u4Value = DHCP6_CLNT_DEF_MAX_RETRANSMISSION_COUNT;
                i4RetVal =
                    D6ClCliSetMaxRetranCount (CliHandle, u4Index, u4Value);
            }
            else if (u4CmdType == DHCP6_CLNT_MAX_RETRANSMISSION_DELAY)
            {
                u4Value = DHCP6_CLNT_DEF_MAX_RETRANSMISSION_DELAY;
                i4RetVal = D6ClCliSetMaxRetranDelay (CliHandle,
                                                     u4Index, u4Value);
            }
            else
            {
                u4Value = DHCP6_CLNT_DEF_INIT_RETRANSMISSION_TIME;
                i4RetVal =
                    D6ClCliSetInitRetraTime (CliHandle, u4Index, u4Value);
                u4Value = DHCP6_CLNT_DEF_INIT_MAX_RETRANSMISSION_TIME;
                i4RetVal =
                    D6ClCliSetMaxRetranTime (CliHandle, u4Index, u4Value);

                u4Value = DHCP6_CLNT_DEF_MAX_RETRANSMISSION_COUNT;
                i4RetVal =
                    D6ClCliSetMaxRetranCount (CliHandle, u4Index, u4Value);

                u4Value = DHCP6_CLNT_DEF_MAX_RETRANSMISSION_DELAY;
                i4RetVal = D6ClCliSetMaxRetranDelay (CliHandle,
                                                     u4Index, u4Value);
            }

            break;
        case CLI_DHCP6_CLNT_DUID_INTERFACE_INDEX:
            u4Index = CLI_GET_IFINDEX ();
            u4DuidIfIndex = CLI_PTR_TO_U4 (args[0]);
            i4RetVal = D6ClCliSetDuidIfIndex (CliHandle,
                                              u4Index, u4DuidIfIndex);
            break;
        case CLI_DHCP6_CLNT_DUID_TYPE:

            /*args[0]-> type of command  */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            u4Index = CLI_GET_IFINDEX ();

            if (u4CmdType == DHCP6_CLNT_LINK_LAYER_ADD_TIME)
            {
                u4Value = DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME;
            }
            else if (u4CmdType == DHCP6_CLNT_VENDOR_ASS_UNIQUE_ID)
            {
                u4Value = DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID;
            }
            else
            {
                u4Value = DHCP6_CLNT_LINK_LAYER_ADD;
            }
            i4RetVal = D6ClCliSetDuidType (CliHandle, u4Index, u4Value);
            break;
        case CLI_DHCP6_CLNT_AUTH_REALM_KEY_ID:

            /*args[0]-> Type
             * args[1]-> Realm/Key name   */

            u4Index = CLI_GET_IFINDEX ();
            u4CmdType = CLI_PTR_TO_U4 (args[DHCP6_CLNT_INDEX_BY_ZERO]);
            u4Value = CLI_PTR_TO_U4 (args[1]);
            if (u4CmdType != DHCP6_CLNT_KEY_ID)
            {
                i4RetVal = D6ClCliSetRealmKey (CliHandle, u4Index,
                                               u4CmdType,
                                               (UINT1 *)
                                               args[DHCP6_CLNT_INDEX_BY_ONE]);
            }
            else
            {
                i4RetVal = D6ClCliSetKeyId (CliHandle, u4Index, u4Value);

            }

            break;
        case CLI_DHCP6_CLNT_ENABLE_CLIENT_FUNC:
            u4Index = CLI_GET_IFINDEX ();
            i4RetVal = D6ClCliEnableClientFun (CliHandle, u4Index);
            break;
        case CLI_DHCP6_CLNT_DISABLE_CLIENT_FUNC:

            u4Index = CLI_GET_IFINDEX ();
            i4RetVal = D6ClCliDisableClientFun (CliHandle, u4Index);

            break;
        case CLI_DHCP6_CLNT_ENABLE_TRAP:
            i4RetVal = D6ClCliSetTraps
                (CliHandle, u4Command,
                 CLI_PTR_TO_U4 (args[DHCP6_CLNT_INDEX_BY_ZERO]));

            break;
        case CLI_DHCP6_CLNT_DISABLE_TRAP:

            i4RetVal = D6ClCliSetTraps
                (CliHandle, u4Command,
                 CLI_PTR_TO_U4 (args[DHCP6_CLNT_INDEX_BY_ZERO]));

            break;
        case CLI_DHCP6_CLNT_UDP_SOURCE_DEST_PORT:

            i4RetVal = D6ClCliSetUdpPorts
                (CliHandle, CLI_PTR_TO_U4 (args[DHCP6_CLNT_INDEX_BY_ZERO]),
                 CLI_PTR_TO_U4 (args[DHCP6_CLNT_INDEX_BY_ONE]));

            break;
        case CLI_DHCP6_CLNT_DEBUG:
            i4RetVal = D6ClCliSetDebugs (CliHandle, (UINT1 *) args
                                         [DHCP6_CLNT_INDEX_BY_ZERO]);
            break;
        case CLI_DHCP6_CLNT_NO_DEBUG:
            i4RetVal = D6ClCliSetDebugs (CliHandle, (UINT1 *) args
                                         [DHCP6_CLNT_INDEX_BY_ZERO]);
            break;

        case CLI_DHCP6_CLNT_CLEAR_ALL_INTERFACE_STATS:
            D6ClCliClearStatsAllInterface (CliHandle);
            break;
        case CLI_DHCP6_CLNT_CLEAR_INTERFACE_STATS:
            i4RetVal = D6ClCliClearStatsInterface
                (CliHandle, u4InterfaceIndex,
                 DHCP6_CLNT_CLEAR_COUNTER_INTERFACE);
            break;
        default:
            /* Given command does not match with any configuration command. */
            break;
    }

    if ((i4RetVal == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < DHCP6_CLNT_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaD6ClCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);
    D6ClApiUnLock ();

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetKeyId                                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Key Id for the Interface    */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value- Key id Value                              */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetKeyId (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfKeyId
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfKeyId (i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6ClCliSetDebugs 
 *
 * DESCRIPTION      : This function sets and resets the Debug Value for the 
 *                    DHCP6 Client.
 *
 * INPUT            : CliHandle --CLI handle
 *                    pu1TraceInput - Input Trace string
 *                    
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE 
 * 
 **************************************************************************/
INT4
D6ClCliSetDebugs (tCliHandle CliHandle, UINT1 *pu1TraceInput)
{

    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE TraceInput;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);

    if (nmhTestv2FsDhcp6ClntDebugTrace (&u4ErrorCode, &TraceInput) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (DHCP6_CLNT_CLI_INV_DEBUG_STRING);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntDebugTrace (&TraceInput) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetSysLogStatus                             */
/*                                                                           */
/*     DESCRIPTION      : This function is used to set the Sys Log Status   */
/*                        for DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        u4SysLogStatus - SYS LOG status Value              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetSysLogStatus (tCliHandle CliHandle, UINT4 u4SysLogStatus)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsDhcp6ClntSysLogAdminStatus
        (&u4ErrCode, u4SysLogStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsDhcp6ClntSysLogAdminStatus (u4SysLogStatus);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetUdpPorts                               */
/*                                                                           */
/*     DESCRIPTION      : This function is used to set the UDP Src and dest  */
/*                        port For DHCP6c Client.                            */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        u4Type - Type                                      */
/*                        u4Value- Port Value                                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetUdpPorts (tCliHandle CliHandle, UINT4 u4Type, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);
    if (u4Type == DHCP6_CLNT_UDP_SOURCE_PORT_NUMBER)
    {
        if (nmhTestv2FsDhcp6ClntListenPort (&u4ErrCode, u4Value) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsDhcp6ClntListenPort (u4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (u4Type == DHCP6_CLNT_UDP_DEST_PORT_NUMBER)
    {
        if (nmhTestv2FsDhcp6ClntTransmitPort (&u4ErrCode, u4Value) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsDhcp6ClntTransmitPort (u4Value);
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetTraps                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Set and Reset the SNMP Traps Value   */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        u4Command - Command Value                          */
/*                        u4TrapValue- Trap Value                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetTraps (tCliHandle CliHandle, UINT4 u4Command, UINT4 u4TrapValue)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT4               u4ErrCode = 0;
    UINT1               u1Trap = 0;
    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1Trap;
    TrapOption.i4_Length = 1;

    if (nmhGetFsDhcp6ClntTrapAdminControl (&TrapOption) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    /* Set the corresponding Trap value which is to be set */

    u1Trap = TrapOption.pu1_OctetList[DHCP6_CLNT_INDEX_BY_ZERO];
    if (u4TrapValue != 0 || (u4Command == CLI_DHCP6_CLNT_DISABLE_TRAP))
    {
        if (u4Command == CLI_DHCP6_CLNT_ENABLE_TRAP)
        {
            u1Trap = (UINT1) (u1Trap | u4TrapValue);
        }
        else
        {
            u1Trap = (UINT1) (u1Trap & u4TrapValue);
        }

        TrapOption.pu1_OctetList[DHCP6_CLNT_INDEX_BY_ZERO] = u1Trap;
        TrapOption.i4_Length = 1;

        if (nmhTestv2FsDhcp6ClntTrapAdminControl (&u4ErrCode, &TrapOption) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        nmhSetFsDhcp6ClntTrapAdminControl (&TrapOption);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliClearStatsAllInterface                    */
/*                                                                           */
/*     DESCRIPTION      : This function clear used to cleant the stats for   */
/*                        all the Interface of DHCP6c Client.                */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliClearStatsAllInterface (tCliHandle CliHandle)
{
    INT1                i1OutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;

    i1OutCome = nmhGetFirstIndexFsDhcp6ClntIfTable (&i4NextPort);

    while ((i1OutCome != SNMP_FAILURE) && (i4NextPort <= MAX_D6CL_CLNT_IFACES))
    {
        if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4NextPort) !=
            SNMP_SUCCESS)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "Port Does Not exist!\n");
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);
            continue;
        }
        D6ClCliClearStatsInterface (CliHandle, i4NextPort,
                                    DHCP6_CLNT_CLEAR_COUNTER_INTERFACE);

        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliClearStatsInterface                       */
/*                                                                           */
/*     DESCRIPTION      : This function clear the stats for the Interface   */
/*                        of DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value-  Value                                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliClearStatsInterface (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4Index) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Interface Does exists in client mode.\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfCounterRest
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfCounterRest (i4Index, (INT4) u4Value) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliDisableClientFun                           */
/*                                                                           */
/*     DESCRIPTION      : This function Disable the client functionality  on  */
/*                        the Interface.                                     */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliDisableClientFun (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliEnableClientFun                           */
/*                                                                           */
/*     DESCRIPTION      : This function Enable the client functionality  on  */
/*                        the Interface.                                      */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliEnableClientFun (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {

        if (nmhTestv2FsDhcp6ClntIfRowStatus
            (&u4ErrCode, i4Index, CREATE_AND_GO) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6ClntIfRowStatus
            (i4Index, CREATE_AND_GO) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsDhcp6ClntIfRowStatus
            (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, DESTROY);
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsDhcp6ClntIfRowStatus
            (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6ClntIfRowStatus
            (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsDhcp6ClntIfRowStatus
            (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetRealmKey                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets Realm and Key on the interface */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Type- Realm or Key Type                          */
/*                        *pu1RelKeyName- Pointer to Key or Realm Value      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetRealmKey (tCliHandle CliHandle, INT4 i4Index,
                    UINT4 u4Type, UINT1 *pu1RelKeyName)
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RealmName.pu1_OctetList = pu1RelKeyName;
    RealmName.i4_Length = STRLEN (pu1RelKeyName);

    KeyName.pu1_OctetList = pu1RelKeyName;
    KeyName.i4_Length = STRLEN (pu1RelKeyName);

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (u4Type == DHCP6_CLNT_REALM_TYPE)
    {
        if (nmhTestv2FsDhcp6ClntIfRealmName
            (&u4ErrCode, i4Index, &RealmName) == SNMP_FAILURE)
        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
            return (CLI_FAILURE);
        }

        if (nmhSetFsDhcp6ClntIfRealmName (i4Index, &RealmName) == SNMP_FAILURE)
        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsDhcp6ClntIfKey
            (&u4ErrCode, i4Index, &KeyName) == SNMP_FAILURE)
        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
            return (CLI_FAILURE);
        }

        if (nmhSetFsDhcp6ClntIfKey (i4Index, &KeyName) == SNMP_FAILURE)
        {
            nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetDuidType                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DUID Type on the Interface */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4DuidType- DUID Type                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetDuidType (tCliHandle CliHandle, INT4 i4Index, UINT4 u4DuidType)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfDuidType
        (&u4ErrCode, i4Index, (INT4) u4DuidType) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfDuidType (i4Index, (INT4) u4DuidType) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetDuidIfIndex                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maxmium Retransmittion Delay*/
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4DuidIfValue- DUID Index                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetDuidIfIndex (tCliHandle CliHandle, INT4 i4Index, UINT4 u4DuidIfValue)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfDuidIfIndex
        (&u4ErrCode, i4Index, (INT4) u4DuidIfValue) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfDuidIfIndex (i4Index, (INT4) u4DuidIfValue) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetRefreshTimerVal                        */
/*                                                                           */
/*     DESCRIPTION      : This function set the Refresh Timer Value on the   */
/*                        interface for DHCP6c Client.                       */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value- Refresh Timer Value                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetRefreshTimerVal (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfMinRefreshTime
        (&u4ErrCode, i4Index, u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfMinRefreshTime (i4Index, u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetMaxRetranDelay                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maxmium Retransmittion Delay*/
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value- Maxmium Retransmittion Delay Value        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetMaxRetranDelay (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfMaxRetDelay
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfMaxRetDelay (i4Index, (INT4) u4Value) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetMaxRetranCount                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maxmium Retransmittion Count */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value- Maxmium Retransmittion Count Value       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetMaxRetranCount (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfMaxRetCount
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfMaxRetCount (i4Index, (INT4) u4Value) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetInitRetraTime                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Initial Retransmittion Time */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value- Initial Retransmittion Time Value        */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetInitRetraTime (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfInitRetTime
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfInitRetTime (i4Index, (INT4) u4Value) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliSetMaxRetranTime                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Maxmium Retransmittion Time */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value- Maxmium Retransmittion Time Value        */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliSetMaxRetranTime (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6ClntIfMaxRetTime
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6ClntIfMaxRetTime (i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrCode, i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (i4Index, ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDhcp6ClntIfRowStatus (i4Index, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *    FUNCTION NAME    : cli_process_d6cl_show_cmd                           *
 *                                                                             *
 *    DESCRIPTION      : Protocol CLI Message Handler Function to handle       *
 *                       Output only.                          *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Command - Command identifier                        *
 *                       ... - Variable command argument list                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/

INT4
cli_process_d6cl_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[DHCP6_CLNT_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4InterfaceIndex = 0;
    INT4                i4RetVal = 0;

    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r%% DHCP6_CLNT Client module is not initialised\r\n");
        return CLI_FAILURE;
    }
    DHCP6_CLNT_REGISTER_CLI_LOCK ();

    va_start (ap, u4Command);

/*Third Argument is always the interface Index*/

    u4InterfaceIndex = va_arg (ap, UINT4);

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == (INT1) DHCP6_CLNT_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);
    D6ClApiLock ();
    switch (u4Command)
    {
        case CLI_DHCP6_CLNT_SHOW_ALL_INTERFACE_STATS:
            i4RetVal = D6ClCliShowAllInterfaceStats (CliHandle);
            break;
        case CLI_DHCP6_CLNT_SHOW_INTERFACE_STATS:
            i4RetVal = D6ClCliShowInterfaceStats (CliHandle, u4InterfaceIndex);
            break;
        case CLI_DHCP6_CLNT_SHOW_INTERFACE_CONF:
            i4RetVal = D6ClCliShowIfConf (CliHandle, u4InterfaceIndex);
            break;
        case CLI_DHCP6_CLNT_SHOW_ALL_INTERFACE_CONF:
            i4RetVal = D6ClCliShowAllInterfaceConf (CliHandle);
            break;
        case CLI_DHCP6_CLNT_SHOW_GLOBAL_CONF:
            i4RetVal = D6ClCliShowGlobalConf (CliHandle);
            break;
        default:
            /* Given command does not match with any show command. */
            break;
    }

    if ((i4RetVal == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < DHCP6_CLNT_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaD6ClCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);
    D6ClApiUnLock ();

    UNUSED_PARAM (args);

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowInterfaceConf                      */
/*                                                                           */
/*     DESCRIPTION      : This function will Disaplay the Current Interface  */
/*                        related Configuration                              */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliShowInterfaceConf (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE ClientDuid;
    tSNMP_OCTET_STRING_TYPE KeyName;
    tSNMP_OCTET_STRING_TYPE OptionVal;
    tSNMP_OCTET_STRING_TYPE SrvAddress;
    tIp6Addr            Ip6Addr;
    tIp6Addr            Ip6Addr1;
    UINT4               u4KeyId = 0;
    INT4                i4IfNextIndex = 0;
    INT4                i4Counter = 0;
    INT4                i4CurrOptionIndex = 0;
    INT4                i4NextOptionIndex = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4RetValue = SNMP_SUCCESS;
    UINT4               u4RetValue = SNMP_SUCCESS;
    UINT2               u2StatusCode = 0;
    UINT1               au1SrvAdd[DHCP6_CLNT_IP6_ADDRESS_SIZE_MAX];
    UINT1               au1SrvAddCmp[DHCP6_CLNT_IP6_ADDRESS_SIZE_MAX];
    UINT1               au1RetKeyName[DHCP6_CLNT_KEY_MAX_ARRAY_SIZE];
    UINT1               au1RetDiidName[DHCP6_CLNT_DUID_MAX_ARRAY_SIZE];
    UINT1               au1RetRealmName[DHCP6_CLNT_REALM_MAX_ARRAY_SIZE];
    UINT1               au1OptionVal[DHCP6_CLNT_OPT_MAX_ARRAY_SIZE];
    INT1                ai1IfName[DHCP6_CLNT_MAX_IF_NAME_LEN];

    MEMSET (au1RetRealmName, 0, DHCP6_CLNT_REALM_MAX_ARRAY_SIZE);
    MEMSET (au1RetDiidName, 0, DHCP6_CLNT_DUID_MAX_ARRAY_SIZE);
    MEMSET (au1RetKeyName, 0, DHCP6_CLNT_KEY_MAX_ARRAY_SIZE);
    MEMSET (au1OptionVal, 0, DHCP6_CLNT_OPT_MAX_ARRAY_SIZE);
    MEMSET (au1SrvAdd, 0, DHCP6_CLNT_IP6_ADDRESS_SIZE_MAX);
    MEMSET (au1SrvAddCmp, 0, DHCP6_CLNT_IP6_ADDRESS_SIZE_MAX);
    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ClientDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OptionVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SrvAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ip6Addr, 0x00, sizeof (tIp6Addr));
    MEMSET (&Ip6Addr1, 0x00, sizeof (tIp6Addr));
    MEMSET (ai1IfName, 0, DHCP6_CLNT_MAX_IF_NAME_LEN);

    RealmName.pu1_OctetList = au1RetRealmName;
    RealmName.i4_Length = 0;

    ClientDuid.pu1_OctetList = au1RetDiidName;
    ClientDuid.i4_Length = 0;
    KeyName.pu1_OctetList = au1RetKeyName;
    KeyName.i4_Length = 0;

    OptionVal.pu1_OctetList = au1OptionVal;
    OptionVal.i4_Length = 0;

    SrvAddress.pu1_OctetList = au1SrvAdd;
    SrvAddress.i4_Length = 0;

    if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4Index) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\n");
        if (D6ClUtlCliConfGetIfName (i4Index, ai1IfName) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\n%s ", ai1IfName);
        CliPrintf (CliHandle, "is in client mode\n");
        if ((nmhGetFsDhcp6ClntIfRowStatus (i4Index, &i4RetValue)) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhGetFsDhcp6ClntIfDuidType (i4Index, &i4RetValue);
        CliPrintf (CliHandle, "  DHCPv6 unique type(DUID Type) : ");
        switch (i4RetValue)
        {
            case DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME:
                CliPrintf (CliHandle, "Link-layer Address Plus Time\r\n");
                break;
            case DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID:
                CliPrintf (CliHandle, "Vendor Based on Enterprise Number\r\n");
                break;
            case DHCP6_CLNT_LINK_LAYER_ADD:
                CliPrintf (CliHandle, "Link-layer Address \r\n");
                break;
            default:
                break;
        }
        CliPrintf (CliHandle, "  DHCPv6 unique identifier(DUID): ");
        nmhGetFsDhcp6ClntIfDuid (i4Index, &ClientDuid);

        for (i4Counter = 0; i4Counter < ClientDuid.i4_Length; i4Counter++)
        {
            if ((i4Counter != 0) && (i4Counter % DHCP6_CLNT_23BYTE_LENGTH == 0))
            {
                CliPrintf (CliHandle, "\n                                  ");
            }
            CliPrintf (CliHandle, "%02x", *(ClientDuid.pu1_OctetList));
            ClientDuid.pu1_OctetList =
                ClientDuid.pu1_OctetList + DHCP6_CLNT_INCR_ONE;
        }
        CliPrintf (CliHandle, "\r\n");
        nmhGetFsDhcp6ClntIfMinRefreshTime (i4Index, &u4RetValue);
        CliPrintf (CliHandle, "  Minimum Refresh Time          : %-5u sec\n",
                   u4RetValue);
        nmhGetFsDhcp6ClntIfCurrRefreshTime (i4Index, &u4RetValue);
        CliPrintf (CliHandle, "  Current Refresh Time          : %-5u sec\n\n",
                   u4RetValue);
        CliPrintf (CliHandle, "  Retransmission counters:\n");
        nmhGetFsDhcp6ClntIfMaxRetCount (i4Index, &i4RetValue);
        CliPrintf (CliHandle, "    Maximum Ret Count : %-3d\n", i4RetValue);
        nmhGetFsDhcp6ClntIfMaxRetDelay (i4Index, &i4RetValue);
        CliPrintf (CliHandle, "    Maximum Ret Delay : %-3d sec\n", i4RetValue);
        nmhGetFsDhcp6ClntIfMaxRetTime (i4Index, &i4RetValue);
        CliPrintf (CliHandle, "    Maximum Ret Time  : %-3d sec\n", i4RetValue);
        nmhGetFsDhcp6ClntIfInitRetTime (i4Index, &i4RetValue);
        CliPrintf (CliHandle, "    Initial Ret Time  : %-3d sec\n", i4RetValue);
        nmhGetFsDhcp6ClntIfCurrRetTime (i4Index, &i4RetValue);
        CliPrintf (CliHandle, "    Current Ret Time  : %-3d sec\n\n",
                   i4RetValue);
        CliPrintf (CliHandle, "  Authentication information:\n");
        nmhGetFsDhcp6ClntIfRealmName (i4Index, &RealmName);

        CliPrintf (CliHandle, "    Realm Name     : ");
        if (RealmName.i4_Length != 0)
        {
            D6SrCliPrintChar (CliHandle, RealmName.pu1_OctetList,
                              RealmName.i4_Length, DHCP6_CLNT_58BYTE_LENGTH);
            CliPrintf (CliHandle, "\n");
        }
        else
        {
            CliPrintf (CliHandle, "-\n");
        }
        nmhGetFsDhcp6ClntIfKey (i4Index, &KeyName);
        CliPrintf (CliHandle, "    Key Identifier : ");
        if (KeyName.i4_Length != 0)
        {
            D6SrCliPrintChar (CliHandle, KeyName.pu1_OctetList,
                              KeyName.i4_Length, DHCP6_CLNT_58BYTE_LENGTH);
            CliPrintf (CliHandle, "\n");
        }
        else
        {
            CliPrintf (CliHandle, "-\n");
        }
        nmhGetFsDhcp6ClntIfKeyId (i4Index, &u4KeyId);
        CliPrintf (CliHandle, "    Key Value      : %d\n", u4KeyId);
        CliPrintf (CliHandle, "\n  List of known servers:\n");
        nmhGetFsDhcp6ClntIfSrvAddress (i4Index, &SrvAddress);
        CliPrintf (CliHandle, "    Address    : ");

        if ((MEMCMP (au1SrvAddCmp, au1SrvAdd, sizeof (tIp6Addr))) == 0)
        {
            CliPrintf (CliHandle, "none\n");
        }
        else
        {
            CliPrintf (CliHandle, "%s\n",
                       Ip6PrintAddr ((tIp6Addr *) (VOID *) au1SrvAdd));
        }
        i4IfNextIndex = i4Index;
        while (i4IfNextIndex == i4Index)
        {
            i4RetVal = nmhGetNextIndexFsDhcp6ClntOptionTable
                (i4Index, &i4IfNextIndex, i4CurrOptionIndex,
                 &i4NextOptionIndex);
            if (i4RetVal == SNMP_SUCCESS)
            {
                /* Show the Informtion */
                nmhGetFsDhcp6ClntOptionLength
                    (i4IfNextIndex, i4NextOptionIndex, &i4RetValue);
                if (i4RetValue != 0)
                {
                    switch (i4NextOptionIndex)
                    {
                        case DHCP6_CLNT_SIP_SERVER_DOMAIN_NAME:
                            CliPrintf (CliHandle, "      SIP domain list : ");
                            break;
                        case DHCP6_CLNT_SIP_SERVER_ADDRESS_LIST:
                            CliPrintf (CliHandle, "      SIP servers     : ");
                            break;
                        case DHCP6_CLNT_DNS_RECUSIVE_NAME:
                            CliPrintf (CliHandle, "      DNS servers     : ");
                            break;
                        case DHCP6_CLNT_DNS_SEARCH_LIST:
                            CliPrintf (CliHandle, "      DNS search list : ");
                            break;
                        case DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE:
                            CliPrintf (CliHandle, "    DUID       : ");
                            break;
                        case DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE:
                            CliPrintf (CliHandle, "    Preference : ");
                            break;
                        case DHCP6_CLNT_USER_CLASS_CODE_OPTION_TYPE:
                            CliPrintf (CliHandle, "      User Class      : ");
                            break;
                        case DHCP6_CLNT_VENDOR_CLASS_CODE_OPTION_TYPE:
                            CliPrintf (CliHandle, "      Vendor Class    : ");
                            break;
                        case DHCP6_CLNT_VENDOR_SPECIFIC_OPTION_TYPE:
                            CliPrintf (CliHandle, "      Vendor Specific : ");
                            break;
                        case DHCP6_CLNT_STATUS_CODE_OPTION_TYPE:
                            CliPrintf (CliHandle, "      Status Code     : ");
                            break;
                        default:
                            CliPrintf (CliHandle, "      Option (%-02d)     : ",
                                       i4NextOptionIndex);
                            break;
                    }
                    nmhGetFsDhcp6ClntOptionValue
                        (i4IfNextIndex, i4NextOptionIndex, &OptionVal);
                    switch (i4NextOptionIndex)
                    {
                        case DHCP6_CLNT_STATUS_CODE_OPTION_TYPE:
                            MEMCPY (&u2StatusCode, OptionVal.pu1_OctetList, 2);
                            OptionVal.pu1_OctetList =
                                OptionVal.pu1_OctetList + 2;

                            switch (u2StatusCode)
                            {
                                case DHCP6_CLNT_STATUS_CODE_USE_MULTICAST:
                                    CliPrintf (CliHandle, "(UseMulticast)-");
                                    break;
                                case DHCP6_CLNT_STATUS_CODE_UNSPECFIC_VALUE:
                                    CliPrintf (CliHandle, "(UnspecFail)-");
                                    break;
                                case DHCP6_CLNT_STATUS_CODE_SUCCESS:
                                    CliPrintf (CliHandle, "(Success)-");
                                    break;
                                default:
                                    break;
                            }
                            /*Two Byte Status code already printed */
                            OptionVal.i4_Length = OptionVal.i4_Length - 2;

                            D6SrCliPrintChar
                                (CliHandle, OptionVal.pu1_OctetList,
                                 OptionVal.i4_Length, DHCP6_CLNT_43BYTE_LENGTH);
                            break;
                        case DHCP6_CLNT_VENDOR_SPECIFIC_OPTION_TYPE:
                            D6SrCliPrintVendorSpec
                                (CliHandle, OptionVal.pu1_OctetList,
                                 OptionVal.i4_Length);
                            break;
                        case DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE:
                            D6SrCliPrintHex
                                (CliHandle, OptionVal.pu1_OctetList, i4RetValue,
                                 DHCP6_CLNT_31BYTE_LENGTH);
                            break;
                        case DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE:
                            CliPrintf (CliHandle, "%d",
                                       *(OptionVal.pu1_OctetList));
                            break;
                        case DHCP6_CLNT_SIP_SERVER_ADDRESS_LIST:
                        case DHCP6_CLNT_DNS_RECUSIVE_NAME:
                            D6SrCliPrintDnsServers
                                (CliHandle, OptionVal.pu1_OctetList,
                                 OptionVal.i4_Length);
                            break;
                        case DHCP6_CLNT_SIP_SERVER_DOMAIN_NAME:
                        case DHCP6_CLNT_DNS_SEARCH_LIST:
                            D6SrCliPrintDomainName
                                (CliHandle, OptionVal.pu1_OctetList,
                                 OptionVal.i4_Length);
                            break;
                        case DHCP6_CLNT_USER_CLASS_CODE_OPTION_TYPE:
                            D6SrCliPrintUserClass (CliHandle,
                                                   OptionVal.pu1_OctetList,
                                                   OptionVal.i4_Length);
                            break;
                        case DHCP6_CLNT_VENDOR_CLASS_CODE_OPTION_TYPE:
                            D6SrCliPrintVendorClass
                                (CliHandle, OptionVal.pu1_OctetList,
                                 OptionVal.i4_Length);
                            break;
                        default:
                            D6SrCliPrintOption (CliHandle,
                                                OptionVal.pu1_OctetList,
                                                OptionVal.i4_Length);
                            break;
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            i4CurrOptionIndex = i4NextOptionIndex;
            i4RetVal = nmhGetNextIndexFsDhcp6ClntOptionTable
                (i4Index, &i4IfNextIndex, i4CurrOptionIndex,
                 &i4NextOptionIndex);
            if (i4RetVal == SNMP_FAILURE)
            {
                break;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintVendorClass                                    */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen of Option with */
/*                code vendor class.                                         */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintVendorClass (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    UINT2               u2Value = 0;
    UINT1               u1Count = 0;

    CliPrintf (CliHandle, "(");
    for (i4Counter = 1; i4Counter <= DHCP6_CLNT_FOUR_BYTE_LENGTH; i4Counter++)
    {
        CliPrintf (CliHandle, "%02x", *(pu1Buf));
        pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
    }
    CliPrintf (CliHandle, ") ");
    u1Count = DHCP6_CLNT_FIVE_BYTE_LENGTH;
    i4Value = i4Value - DHCP6_CLNT_FOUR_BYTE_LENGTH;
    for (i4Counter = 0; i4Counter < i4Value;)
    {
        u2Value = 0;
        DHCP6_CLNT_GET_2BYTE (u2Value, pu1Buf);
        if ((i4Counter != 0) && (u2Value != 0))
        {
            CliPrintf (CliHandle, ", ");
            u1Count = (UINT1) (u1Count + 1);
        }
        i4Counter = i4Counter + DHCP6_CLNT_TWO_BYTE_LENGTH;
        while ((i4Counter < i4Value) && (u2Value != 0))
        {
            if (u1Count >= DHCP6_CLNT_47BYTE_LENGTH)
            {
                CliPrintf (CliHandle, "\n                        ");
                u1Count = 0;
            }
            if (u2Value == 1)
            {
                CliPrintf (CliHandle, "%02x", *(pu1Buf));
                u1Count = (UINT1) (u1Count + DHCP6_CLNT_TWO_BYTE_LENGTH);
            }
            else
            {
                CliPrintf (CliHandle, "%02x:", *(pu1Buf));
                u1Count = (UINT1) (u1Count + DHCP6_CLNT_THREE_BYTE_LENGTH);
            }
            pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
            u2Value = (UINT2) (u2Value - 1);
            i4Counter = i4Counter + 1;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintUserClass                                    */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen of Option with */
/*                code User class.                                         */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintUserClass (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    UINT2               u2Value = 0;
    UINT1               u1Count = 0;

    for (i4Counter = 0; i4Counter < i4Value;)
    {
        u2Value = 0;
        DHCP6_CLNT_GET_2BYTE (u2Value, pu1Buf);
        if ((i4Counter != 0) && (u2Value != 0))
        {
            CliPrintf (CliHandle, ", ");
            u1Count = (UINT1) (u1Count + 1);
        }
        i4Counter = i4Counter + DHCP6_CLNT_TWO_BYTE_LENGTH;
        while ((i4Counter < i4Value) && (u2Value != 0))
        {
            if (u1Count >= DHCP6_CLNT_50BYTE_LENGTH)
            {
                CliPrintf (CliHandle, "\n                        ");
                u1Count = 0;
            }
            if (u2Value == 1)
            {
                CliPrintf (CliHandle, "%02x", *(pu1Buf));
                u1Count = (UINT1) (u1Count + DHCP6_CLNT_TWO_BYTE_LENGTH);
            }
            else
            {
                CliPrintf (CliHandle, "%02x:", *(pu1Buf));
                u1Count = (UINT1) (u1Count + DHCP6_CLNT_THREE_BYTE_LENGTH);
            }
            pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
            u2Value = (UINT2) (u2Value - 1);
            i4Counter = i4Counter + 1;
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintVendorSpec                                       */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen of Option with */
/*                code vendor specific.                                      */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintVendorSpec (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    INT4                i4CouValu = 0;
    UINT4               u1Count = 0;
    UINT2               u2Value = 0;

    CliPrintf (CliHandle, "(");
    for (i4Counter = 1; i4Counter <= DHCP6_CLNT_FOUR_BYTE_LENGTH; i4Counter++)
    {
        CliPrintf (CliHandle, "%02x", *(pu1Buf));
        pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
    }
    CliPrintf (CliHandle, ") ");
    u1Count = DHCP6_CLNT_FIVE_BYTE_LENGTH;
    i4Value = i4Value - DHCP6_CLNT_FOUR_BYTE_LENGTH;

    for (i4Counter = 0; i4Counter < i4Value;)
    {
        if (i4Counter != 0)
        {
            CliPrintf (CliHandle, ", ");
            u1Count = u1Count + 1;
        }
        for (i4CouValu = 1; i4CouValu <= DHCP6_CLNT_TWO_BYTE_LENGTH;
             i4CouValu++)
        {
            CliPrintf (CliHandle, "%02x", *(pu1Buf));
            pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
            i4Counter = i4Counter + 1;
        }
        u1Count = u1Count + DHCP6_CLNT_THREE_BYTE_LENGTH;
        CliPrintf (CliHandle, "-");
        DHCP6_CLNT_GET_2BYTE (u2Value, pu1Buf);
        i4Counter = i4Counter + DHCP6_CLNT_TWO_BYTE_LENGTH;
        while ((i4Counter < i4Value) && (u2Value != 0))
        {
            if (u1Count >= DHCP6_CLNT_41BYTE_LENGTH)
            {
                CliPrintf (CliHandle, "\n                        ");
                u1Count = 0;
            }
            if (u2Value == 1)
            {
                CliPrintf (CliHandle, "%02x", *(pu1Buf));
                u1Count = u1Count + DHCP6_CLNT_TWO_BYTE_LENGTH;
            }
            else
            {
                CliPrintf (CliHandle, "%02x:", *(pu1Buf));
                u1Count = u1Count + DHCP6_CLNT_THREE_BYTE_LENGTH;
            }
            pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
            u2Value = (UINT2) (u2Value - 1);
            i4Counter = i4Counter + 1;
        }
        u2Value = 0;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintDomainName                                       */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen of Option with */
/*                code 21 and 24.                                            */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintDomainName (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        if (*(pu1Buf) == '\0')
        {
            if (i4Counter < (i4Value - 1))
            {
                CliPrintf (CliHandle, ",");
                CliPrintf (CliHandle, "\n                        ");
                i4Counter = i4Counter + DHCP6_CLNT_INCR_ONE;
                pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE + DHCP6_CLNT_INCR_ONE;
            }
            continue;
        }
        if (i4Counter != 0)
        {
            CliPrintf (CliHandle, "%c", *(pu1Buf));
        }
        pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintDnsServers                                       */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen of Option with */
/*                code 22 and 23.                                            */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintDnsServers (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    UINT1               u1Count = 0;
    UINT1               u1Value = 0;
    UINT1               au1IpAdd[DHCP6_CLNT_16BYTE_LENGTH];

    MEMSET (&au1IpAdd, 0x00, sizeof (tIp6Addr));
    u1Count = (UINT1) (i4Value / DHCP6_CLNT_16BYTE_LENGTH);
    while (u1Count != 0)
    {
        for (i4Counter = 0; i4Counter < DHCP6_CLNT_16BYTE_LENGTH; i4Counter++)
        {
            au1IpAdd[i4Counter] = *(pu1Buf);
            pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
        }
        CliPrintf (CliHandle, "%s",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *) au1IpAdd));
        u1Count = (UINT1) (u1Count - 1);
        if (u1Count != 0)
        {
            CliPrintf (CliHandle, ", ");
        }
        u1Value = (UINT1) (u1Value + 1);
        if ((u1Value == DHCP6_CLNT_TWO_BYTE_LENGTH) && (u1Count != 0))
        {
            CliPrintf (CliHandle, "\n                      : ");
            u1Value = 0;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintOption                                       */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen of Option value. */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintOption (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        if ((i4Counter != 0) && (i4Counter % DHCP6_CLNT_18BYTE_LENGTH == 0))
        {
            CliPrintf (CliHandle, "\n                        ");
        }
        if (i4Counter == (i4Value - 1))
        {
            CliPrintf (CliHandle, "%02x", *(pu1Buf));
        }
        else
        {
            CliPrintf (CliHandle, "%02x:", *(pu1Buf));
        }
        pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintHex                                       */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen in Hex format. */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                i4NextLine    - Next Line value Value                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintHex (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value,
                 INT4 i4NextLine)
{
    INT4                i4Counter = 0;

    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        if ((i4Counter != 0) && (i4Counter % i4NextLine == 0))
        {
            CliPrintf (CliHandle, "\n                 ");
        }
        CliPrintf (CliHandle, "%02x", *(pu1Buf));
        pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintChar                                       */
/*                                                                           */
/* Description  : This function will Print the Ouput on Screen in character  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                i4Value       - Length Value                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintChar (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value,
                  INT4 i4NextLine)
{
    INT4                i4Counter = 0;
    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        if ((i4Counter != 0) && (i4Counter % i4NextLine == 0))
        {
            CliPrintf (CliHandle, "\n                     ");
        }
        CliPrintf (CliHandle, "%c", *(pu1Buf));
        pu1Buf = pu1Buf + DHCP6_CLNT_INCR_ONE;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowGlobalInfo                              */
/*                                                                           */
/*     DESCRIPTION      : This function Display global Configuration         */
/*                        Information for DHCPv6 Client.                     */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/

VOID
D6ClCliShowGlobalInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpTrapOption;
    INT4                i4SysLogStatus = 0;
    INT4                i4SourcePort = 0;
    INT4                i4DestPort = 0;
    UINT1               u1CurTrapOption = 0;
    UINT1               u1FlagValue = OSIX_FALSE;
    MEMSET (&DhcpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpTrapOption.pu1_OctetList = &u1CurTrapOption;
    DhcpTrapOption.i4_Length = DHCP6_CLNT_VALUE_ONE;
    nmhGetFsDhcp6ClntTrapAdminControl (&DhcpTrapOption);
    u1CurTrapOption = DhcpTrapOption.pu1_OctetList[DHCP6_CLNT_INDEX_BY_ZERO];
    nmhGetFsDhcp6ClntSysLogAdminStatus (&i4SysLogStatus);
    nmhGetFsDhcp6ClntListenPort (&i4SourcePort);
    nmhGetFsDhcp6ClntTransmitPort (&i4DestPort);
    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "Client information:\r\n");
    CliPrintf (CliHandle, "  Listen UDP port   : %d\n", i4SourcePort);
    CliPrintf (CliHandle, "  Transmit UDP port : %d\r\n", i4DestPort);
    CliPrintf (CliHandle, "  Sys log status    : ");
    if (i4SysLogStatus == DHCP6_CLNT_SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "enabled\t\n");
    }
    else
    {
        CliPrintf (CliHandle, "disabled\t\n");
    }
    CliPrintf (CliHandle, "  SNMP traps        : ");
    if (u1CurTrapOption != 0)
    {
        if (DHCP6_CLNT_INVALID_PKT_REC_TRAP_ENABLED (u1CurTrapOption) ==
            OSIX_TRUE)
        {
            u1FlagValue = OSIX_TRUE;
            CliPrintf (CliHandle, "invalid-pkt");
        }
        if (DHCP6_CLNT_HMAC_FAIL_TRAP_ENABLED (u1CurTrapOption) == OSIX_TRUE)
        {
            if (u1FlagValue == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ",");
            }
            CliPrintf (CliHandle, "auth-fail");
            u1FlagValue = OSIX_FALSE;
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "none\r\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowIfConf                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Display Configuration Information   */
/*                        Interface of client .                              */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliShowIfConf (tCliHandle CliHandle, INT4 i4IfIndex)
{
    D6ClCliShowInterfaceConf (CliHandle, i4IfIndex);

    CliUnRegisterLock (CliHandle);
    D6ClApiUnLock ();

#ifdef DHCP6_SRV_WANTED
    D6SrApiShowInterfaceInfo (CliHandle, i4IfIndex);
#endif
#ifdef DHCP6_RLY_WANTED
    D6RlApiShowInterfaceInfo (CliHandle, i4IfIndex);
#endif
    DHCP6_CLNT_REGISTER_CLI_LOCK ();
    D6ClApiLock ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowGlobalConf                              */
/*                                                                           */
/*     DESCRIPTION      : This function Display Configuration Information   */
/*                        of client .                                        */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliShowGlobalConf (tCliHandle CliHandle)
{

/*Show Global Information of Client*/
    D6ClCliShowGlobalInfo (CliHandle);

    CliUnRegisterLock (CliHandle);
    D6ClApiUnLock ();

#ifdef DHCP6_SRV_WANTED
    D6SrApiShowGlobalInfo (CliHandle);
#endif
#ifdef DHCP6_RLY_WANTED
    D6RlApiShowGlobalInfo (CliHandle);
#endif
    DHCP6_CLNT_REGISTER_CLI_LOCK ();
    D6ClApiLock ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowAllInterfaceConf                   */
/*                                                                           */
/*     DESCRIPTION      : This function Display Configuration Information   */
/*                        from all the Interfaces.                           */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliShowAllInterfaceConf (tCliHandle CliHandle)
{
    UINT4               u4IfIndex = 0;

    for (u4IfIndex = 1; u4IfIndex <= MAX_D6CL_CLNT_IFACES; u4IfIndex++)
    {
        D6ClCliShowIfConf (CliHandle, u4IfIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowInterfaceStats                        */
/*                                                                           */
/*     DESCRIPTION      : This function will Disaplay the Interface related */
/*                        Stast For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliShowInterfaceStats (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4ReplyIn = 0;
    UINT4               u4InvalidPktIn = 0;
    UINT4               u4HmacFailCount = 0;
    UINT4               u4InformOut = 0;
    INT1                ai1IfName[DHCP6_CLNT_MAX_IF_NAME_LEN];

    MEMSET (ai1IfName, 0, DHCP6_CLNT_MAX_IF_NAME_LEN);

    if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4Index) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Interface Does exists in client mode.\r\n");
        return CLI_FAILURE;
    }
   /**Get all the Stats */
    nmhGetFsDhcp6ClntIfReplyIn (i4Index, &u4ReplyIn);
    nmhGetFsDhcp6ClntIfInvalidPktIn (i4Index, &u4InvalidPktIn);
    nmhGetFsDhcp6ClntIfHmacFailCount (i4Index, &u4HmacFailCount);
    nmhGetFsDhcp6ClntIfInformOut (i4Index, &u4InformOut);

    if (D6ClUtlCliConfGetIfName (i4Index, ai1IfName) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\n%s\r\n", ai1IfName);
    CliPrintf (CliHandle, "  Transmitted:\r\n");
    CliPrintf (CliHandle, "   information-request : %d\n\n", u4InformOut);
    CliPrintf (CliHandle, "  Received:\r\n");
    CliPrintf (CliHandle, "   reply        : %d\n", u4ReplyIn);
    CliPrintf (CliHandle, "   invalid      : %d\n", u4InvalidPktIn);
    CliPrintf (CliHandle, "   hmac-failure : %d\n", u4HmacFailCount);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6ClCliShowAllInterfaceStats                    */
/*                                                                           */
/*     DESCRIPTION      : This function Display stats from all the Interface   */
/*                        For DHCP6c Client.                                 */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6ClCliShowAllInterfaceStats (tCliHandle CliHandle)
{
    INT1                i1OutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;

    i1OutCome = nmhGetFirstIndexFsDhcp6ClntIfTable (&i4NextPort);

    while ((i1OutCome != SNMP_FAILURE) && (i4NextPort <= MAX_D6CL_CLNT_IFACES))
    {
        if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4NextPort) !=
            SNMP_SUCCESS)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "Port Does Not exist!\n");
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);
            continue;
        }
        D6ClCliShowInterfaceStats (CliHandle, i4NextPort);

        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6ClCliShowRunningConfig                             */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP6 Client.                                           */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     u4Module - module Value                               */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
D6ClCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UNUSED_PARAM (u4Module);
    CliRegisterLock (CliHandle, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    if (D6ClCliShowRunningScalars (CliHandle) == CLI_SUCCESS)
    {
        D6ClCliShowRunningConfigInterface (CliHandle);
    }

    D6ClApiUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : D6ClCliShowRunningScalars                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in DHCP6_CLNT for*/
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
D6ClCliShowRunningScalars (tCliHandle CliHandle)
{
    UINT1               u1TrapOption = 0;
    UINT1               u1CurTrapOption = 0;
    INT4                i4SysLogStatus = 0;
    INT4                i4SourcePort = 0;
    INT4                i4DestPort = 0;
    tSNMP_OCTET_STRING_TYPE DhcpTrapOption;
    MEMSET (&DhcpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpTrapOption.pu1_OctetList = &u1TrapOption;
    DhcpTrapOption.i4_Length = 1;

    nmhGetFsDhcp6ClntTrapAdminControl (&DhcpTrapOption);
    u1CurTrapOption = DhcpTrapOption.pu1_OctetList[DHCP6_CLNT_INDEX_BY_ZERO];

    if (u1CurTrapOption != 0)
    {
        CliPrintf (CliHandle, "snmp-server enable traps ipv6 dhcp client");
        if (DHCP6_CLNT_INVALID_PKT_REC_TRAP_ENABLED (u1CurTrapOption) ==
            OSIX_TRUE)
        {
            CliPrintf (CliHandle, " invalid-pkt");
        }
        if (DHCP6_CLNT_HMAC_FAIL_TRAP_ENABLED (u1CurTrapOption) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " auth-fail");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    nmhGetFsDhcp6ClntSysLogAdminStatus (&i4SysLogStatus);
    nmhGetFsDhcp6ClntListenPort (&i4SourcePort);
    nmhGetFsDhcp6ClntTransmitPort (&i4DestPort);

    if (i4SysLogStatus == DHCP6_CLNT_SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "ipv6 dhcp client syslog enable\r\n");
    }
    if (i4SourcePort != DHCP6_CLNT_DEFAULT_SOURCE_PORT)
    {
        CliPrintf (CliHandle, "ipv6 dhcp client port listen %d\r\n",
                   i4SourcePort);
    }

    if (i4DestPort != DHCP6_CLNT_DEFAULT_DEST_PORT)
    {
        CliPrintf (CliHandle, "ipv6 dhcp client port transmit %d\r\n",
                   i4DestPort);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6ClCliShowRunningConfigInterface                  */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP6 Client Interface .                               */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/
VOID
D6ClCliShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT1                i1OutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1IfName[DHCP6_CLNT_MAX_IF_NAME_LEN];

    MEMSET (ai1IfName, 0, DHCP6_CLNT_MAX_IF_NAME_LEN);
    i1OutCome = nmhGetFirstIndexFsDhcp6ClntIfTable (&i4NextPort);

    while ((i1OutCome != SNMP_FAILURE) && (i4NextPort <= MAX_D6CL_CLNT_IFACES))
    {
        if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4NextPort) !=
            SNMP_SUCCESS)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "Port Does Not exist!\n");
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);
            continue;
        }
        if (D6ClUtlCliConfGetIfName (i4NextPort, ai1IfName) == OSIX_FAILURE)
        {
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);
            continue;

        }

        D6ClApiUnLock ();
        CliUnRegisterLock (CliHandle);

        D6ClCliShowRunningConfigInterfaceDetails (CliHandle, i4NextPort);

        CliRegisterLock (CliHandle, D6ClApiLock, D6ClApiUnLock);
        D6ClApiLock ();

        CliPrintf (CliHandle, "!");
        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsDhcp6ClntIfTable (i4CurrentPort, &i4NextPort);

    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : D6ClCliShowRunningConfigInterfaceDetails            */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in DHCP   */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
PUBLIC VOID
D6ClCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4RowStatus = 0;
    INT4                i4IfDuidType = 0;
    INT4                i4IfDuidIfIndex = 0;
    INT4                i4IfMaxRetCount = 0;
    INT4                i4IfMaxRetDelay = 0;
    INT4                i4IfMaxRetTime = 0;
    UINT4               u4IfMinRefreshTime = 0;
    UINT4               u4IfKeyId = 0;
    INT4                i4RetValue = 0;
    UINT1               au1RetKeyName[DHCP6_CLNT_KEY_MAX_ARRAY_SIZE];
    UINT1               au1RetRealmName[DHCP6_CLNT_REALM_MAX_ARRAY_SIZE];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE RetKeyName;
    tSNMP_OCTET_STRING_TYPE RetRealmName;

    MEMSET (&RetKeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RetKeyName, 0, DHCP6_CLNT_KEY_MAX_ARRAY_SIZE);
    RetKeyName.pu1_OctetList = au1RetKeyName;
    RetKeyName.i4_Length = 0;

    MEMSET (&RetRealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RetRealmName, 0, DHCP6_CLNT_REALM_MAX_ARRAY_SIZE);
    RetRealmName.pu1_OctetList = au1RetRealmName;
    RetRealmName.i4_Length = 0;

    /* Needed this check when called from Cfa */

    CliRegisterLock (CliHandle, D6ClApiLock, D6ClApiUnLock);
    D6ClApiLock ();

    if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (i4IfIndex) == SNMP_FAILURE)
    {
        D6ClApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    /*Check if the Interface Status is client */
    if ((nmhGetFsDhcp6ClntIfRowStatus (i4IfIndex, &i4RowStatus) ==
         SNMP_FAILURE))
    {
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1IfName);
        CliPrintf (CliHandle, "interface %s \n", au1IfName);
        CliPrintf (CliHandle, " ipv6 address dhcp\r\n");

        nmhGetFsDhcp6ClntIfDuidType (i4IfIndex, &i4IfDuidType);
        if (i4IfDuidType != DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME)
        {
            CliPrintf (CliHandle, " ipv6 dhcp client-id type ");
            if (i4IfDuidType == DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID)
            {
                CliPrintf (CliHandle, "en\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "ll\r\n");
            }
        }
        nmhGetFsDhcp6ClntIfDuidIfIndex (i4IfIndex, &i4IfDuidIfIndex);
        if (i4IfDuidIfIndex != i4IfIndex)
        {
            CliPrintf (CliHandle,
                       " ipv6 dhcp client-id interface gigabitethernet 0/%d\r\n",
                       i4IfDuidIfIndex);
        }
        nmhGetFsDhcp6ClntIfMaxRetCount (i4IfIndex, &i4IfMaxRetCount);
        if (i4IfMaxRetCount != DHCP6_CLNT_DEF_MAX_RETRANSMISSION_COUNT)
        {
            CliPrintf (CliHandle, " ipv6 dhcp timer mrc %d\r\n",
                       i4IfMaxRetCount);
        }
        nmhGetFsDhcp6ClntIfMaxRetDelay (i4IfIndex, &i4IfMaxRetDelay);
        if (i4IfMaxRetDelay != DHCP6_CLNT_DEF_MAX_RETRANSMISSION_DELAY)
        {
            CliPrintf (CliHandle, " ipv6 dhcp timer mrd %d\r\n",
                       i4IfMaxRetDelay);
        }

        nmhGetFsDhcp6ClntIfMaxRetTime (i4IfIndex, &i4IfMaxRetTime);
        if (i4IfMaxRetTime != DHCP6_CLNT_DEF_INIT_MAX_RETRANSMISSION_TIME)
        {
            CliPrintf (CliHandle, " ipv6 dhcp timer mrt %d\r\n",
                       i4IfMaxRetTime);
        }
        nmhGetFsDhcp6ClntIfInitRetTime (i4IfIndex, &i4RetValue);

        if (i4RetValue != DHCP6_CLNT_DEF_INIT_RETRANSMISSION_TIME)
        {
            CliPrintf (CliHandle, " ipv6 dhcp timer irt %d\r\n", i4RetValue);
        }

        nmhGetFsDhcp6ClntIfMinRefreshTime (i4IfIndex, &u4IfMinRefreshTime);
        if (u4IfMinRefreshTime != DHCP6_CLNT_DEFAULT_REFRESH_TIME)
        {
            CliPrintf (CliHandle,
                       " ipv6 dhcp client information refresh minimum %u\r\n",
                       u4IfMinRefreshTime);
        }

        nmhGetFsDhcp6ClntIfRealmName (i4IfIndex, &RetRealmName);
        if (RetRealmName.i4_Length != 0)
        {
            CliPrintf (CliHandle,
                       " ipv6 dhcp authentication client realm %s\r\n",
                       RetRealmName.pu1_OctetList);
        }

        nmhGetFsDhcp6ClntIfKey (i4IfIndex, &RetKeyName);
        if (RetKeyName.i4_Length != 0)
        {
            CliPrintf (CliHandle, " ipv6 dhcp authentication client key %s\r\n",
                       RetKeyName.pu1_OctetList);
        }
        nmhGetFsDhcp6ClntIfKeyId (i4IfIndex, &u4IfKeyId);
        if (u4IfKeyId != DHCP6_CLNT_DEFAULT_KEY_ID)
        {
            CliPrintf (CliHandle,
                       " ipv6 dhcp authentication client keyid %d\r\n",
                       u4IfKeyId);

        }
    }

    D6ClApiUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

#ifdef DHCP6_CLNT_TEST_WANTED
VOID
D6ClTestCliExecUT (INT4 i4UtId)
{
    D6ClTestExecUT (i4UtId);
}
#endif

/*****************************************************************************/
/*     FUNCTION NAME    : IssDhcp6ClientShowDebugging                        */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DHCP6 client  debug level */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/

VOID
IssDhcp6ClientShowDebugging (tCliHandle CliHandle)
{
    if (DHCP6_CLNT_TRACE_OPTION == DHCP6_CLNT_INVALID_TRC)
    {
        return;
    }
    CliPrintf (CliHandle, "\rDHCP6 CLIENT :");
    if ((DHCP6_CLNT_TRACE_OPTION & DHCP6_CLNT_ALL_TRC) == DHCP6_CLNT_ALL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client all debugging is on");
        CliPrintf (CliHandle, "\r\n");
        return;
    }
    if ((DHCP6_CLNT_TRACE_OPTION & INIT_SHUT_TRC) == INIT_SHUT_TRC)
    {
        CliPrintf (CliHandle,
                   "\r\n  DHCP6 client init shutdown debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & MGMT_TRC) == MGMT_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client management debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & CONTROL_PLANE_TRC) == CONTROL_PLANE_TRC)
    {
        CliPrintf (CliHandle,
                   "\r\n  DHCP6 client control plane debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & DUMP_TRC) == DUMP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client packet dump debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & OS_RESOURCE_TRC) == OS_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client resource debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & ALL_FAILURE_TRC) == ALL_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client all failure debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & BUFFER_TRC) == BUFFER_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client buffer debugging is on");
    }
    if ((DHCP6_CLNT_TRACE_OPTION & DHCP6_CLNT_CRITICAL_TRC) ==
        DHCP6_CLNT_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 client critical debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6ClientShowTraps                                       */
/*                                                                           */
/* Description  : This function is to display the traps enabled              */
/*                in dhcp6 client                                            */
/*                                                                           */
/* Input        : CliHandle - Handle to the cli context                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Dhcp6ClientShowTraps (tCliHandle CliHandle)
{
    UINT1               u1ClntTrapOption = 0;
    UINT1               u1CurClntTrapOption = 0;
    tSNMP_OCTET_STRING_TYPE DhcpClntTrapOption;
    MEMSET (&DhcpClntTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpClntTrapOption.pu1_OctetList = &u1ClntTrapOption;
    DhcpClntTrapOption.i4_Length = 1;
    nmhGetFsDhcp6ClntTrapAdminControl (&DhcpClntTrapOption);
    u1CurClntTrapOption = DhcpClntTrapOption.pu1_OctetList[0];
    if (u1CurClntTrapOption != 0)
    {
        CliPrintf (CliHandle, "\nipv6 dhcp client");
        if (DHCP6_CLNT_INVALID_PKT_REC_TRAP_ENABLED (u1CurClntTrapOption) ==
            OSIX_TRUE)
        {
            CliPrintf (CliHandle, " invalid-pkt,");
        }
        if (DHCP6_CLNT_HMAC_FAIL_TRAP_ENABLED (u1CurClntTrapOption) ==
            OSIX_TRUE)
        {
            CliPrintf (CliHandle, " auth-fail");
        }
    }
}
#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clcli.c                       */
/*-----------------------------------------------------------------------*/
