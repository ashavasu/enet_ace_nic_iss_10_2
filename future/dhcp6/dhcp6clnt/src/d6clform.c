/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $ $Id: d6clform.c,v 1.10 2014/05/19 13:12:54 siva Exp $
 *
 * Description: This file contains the functionality of the tranmsitter sub
 *              module.
 * ************************************************************************/

#include "d6clinc.h"
PRIVATE INT4        D6ClFormPdu (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo,
                                 UINT4 *pu4Length);
PRIVATE VOID        D6ClFormCalculateTransId (tDhcp6ClntIfInfo *
                                              pDhcp6ClntIfInfo,
                                              UINT4 *pu4MesgTypeTransId);
PRIVATE INT4        D6ClFormCalculateRT (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo);
PRIVATE VOID D6ClFormCalculateElapsedTime PROTO ((tDhcp6ClntIfInfo *, UINT2 *));

/******************************************************************************
 * Function Name      : D6ClFormAppliTxPdu
 *
 * Description        : This routine is called when Application request for the
 *                      configuration information and information does not 
 *                      exists on the interface then DHCPv6 client will initiate the
 *                      information request message.
 *
 * Input(s)           : pDhcp6ClntIfInfo - Pointer to interface Strcture 
 *                      u2CodeValue - User requested Type
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PUBLIC INT4
D6ClFormAppliTxPdu (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo, UINT2 u2CodeValue)
{
    tDhcp6ClntOptionInfo *pOptionInfo = NULL;
    tNetIpv6IfInfo      netIp6IfInfo;
    tIp6Addr            Ip6Addr;
    UINT2               u2AppId = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&netIp6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    /*Set the Interface specific varibles which will be used while parsing */
    pDhcp6ClntIfInfo->b1IsRetansMessage = OSIX_FALSE;
    pDhcp6ClntIfInfo->u1TransType = DHCP6_CLNT_APP_INIT_REQUEST;
    pDhcp6ClntIfInfo->u2CurrentRetTime = 0;
    pDhcp6ClntIfInfo->u1CurrRetCount = 0;
    pDhcp6ClntIfInfo->u2CurrentRetDelay = 0;
    pDhcp6ClntIfInfo->u2CurrAppReqCode = u2CodeValue;

    if (NetIpv6GetIfInfo ((UINT4) pDhcp6ClntIfInfo->u4IfIndex, &netIp6IfInfo)
        == NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if ((MEMCMP (&netIp6IfInfo.Ip6Addr, &Ip6Addr, sizeof (tIp6Addr))) == 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClFormAppliTxPdu: IP Address is not assigned to"
                        " Interface. \r\n");
        return OSIX_FAILURE;
    }

    /* Populate on the Global and Interface RBTree with this code */
    u2AppId = pDhcp6ClntIfInfo->u2CurrentApp;
    pOptionInfo =
        D6ClOptGetGlobOptionInfo (pDhcp6ClntIfInfo->u4IfIndex, u2CodeValue);
    if (pOptionInfo == NULL)
    {
        pOptionInfo = D6ClOptCreateNode ();
        if (pOptionInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        pOptionInfo->u2Type = u2CodeValue;
        pOptionInfo->u4IfIndex = pDhcp6ClntIfInfo->u4IfIndex;
        pOptionInfo->u2Length = 0;

        if (u2AppId < MAX_D6CL_CLNT_APPS)
        {
            DHCP6_CLNT_SET_MEMBER_APP (pOptionInfo->au1ApplicationMap, u2AppId);
        }

        /*Add the Bit Mask to this */
        if (D6ClOptAddOptionNode (pOptionInfo) != OSIX_SUCCESS)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClFormAppliTxPdu: D6ClOptAddOptionNode"
                            " Function Return Failure. \r\n");
            return OSIX_FAILURE;
        }
    }
    if ((u2AppId >= MAX_D6CL_CLNT_APPS)
        || (u2CodeValue >= DHCP6_CLNT_REQ_OPT_MAX))
    {
        return OSIX_FAILURE;
    }

    if (u2AppId < MAX_D6CL_CLNT_APPS)
    {
        DHCP6_CLNT_SET_MEMBER_APP (pOptionInfo->au1ApplicationMap, u2AppId);
    }

    pDhcp6ClntIfInfo->aau1RequestOption[u2AppId][u2CodeValue] =
        (UINT1) u2CodeValue;
    if (D6ClFormInterfaceTxPdu (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClFormAppliTxPdu: D6ClFormInterfaceTxPdu "
                        " Function Return Failure. \r\n");
        D6ClOptDelOptionNode (pOptionInfo);
        /*Delete this node from the interface and global RBtree */
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : D6ClFormUserTxPdu
 *
 * Description        : This routine is called to transmit the information
 *                      request message when row status of interface sis set
 *                      active.
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to interface Strcture 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PUBLIC INT4
D6ClFormUserTxPdu (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo)
{
    tDhcp6ClntOptionInfo *pOptionInfo = NULL;
    INT2                i2RandomNumber = 0;
    /*Set the Interface specific varibles which will be used while parsing */
    pDhcp6ClntIfInfo->b1IsRetansMessage = OSIX_FALSE;

    pDhcp6ClntIfInfo->u1CurrRetCount = 0;
    pDhcp6ClntIfInfo->u2CurrentRetTime = 0;
    pDhcp6ClntIfInfo->u2CurrentRetDelay = 0;
    pDhcp6ClntIfInfo->u1TransType = DHCP6_CLNT_USER_INIT_REQUEST;

    /* Add empty SIP server domain name option */
    pOptionInfo =
        D6ClOptGetGlobOptionInfo (pDhcp6ClntIfInfo->u4IfIndex,
                                  DHCP6_CLNT_SIP_SERVER_DOMAIN_NAME);
    if (pOptionInfo == NULL)
    {
        pOptionInfo = D6ClOptCreateNode ();
        if (pOptionInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        pOptionInfo->u4IfIndex = pDhcp6ClntIfInfo->u4IfIndex;
        pOptionInfo->u2Type = DHCP6_CLNT_SIP_SERVER_DOMAIN_NAME;
        if (D6ClOptAddOptionNode (pOptionInfo) != OSIX_SUCCESS)
        {
            D6ClOptDelOptionNode (pOptionInfo);
            return OSIX_FAILURE;
        }
    }

    /* Add empty SIP server address list option */
    pOptionInfo =
        D6ClOptGetGlobOptionInfo (pDhcp6ClntIfInfo->u4IfIndex,
                                  DHCP6_CLNT_SIP_SERVER_ADDRESS_LIST);
    if (pOptionInfo == NULL)
    {
        pOptionInfo = D6ClOptCreateNode ();
        if (pOptionInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        pOptionInfo->u4IfIndex = pDhcp6ClntIfInfo->u4IfIndex;
        pOptionInfo->u2Type = DHCP6_CLNT_SIP_SERVER_ADDRESS_LIST;
        if (D6ClOptAddOptionNode (pOptionInfo) != OSIX_SUCCESS)
        {
            D6ClOptDelOptionNode (pOptionInfo);
            return OSIX_FAILURE;
        }
    }

    pOptionInfo =
        D6ClOptGetGlobOptionInfo (pDhcp6ClntIfInfo->u4IfIndex,
                                  DHCP6_CLNT_DNS_RECUSIVE_NAME);
    if (pOptionInfo == NULL)
    {
        /* Add empty DNS recursive name option */
        pOptionInfo = D6ClOptCreateNode ();
        if (pOptionInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        pOptionInfo->u4IfIndex = pDhcp6ClntIfInfo->u4IfIndex;
        pOptionInfo->u2Type = DHCP6_CLNT_DNS_RECUSIVE_NAME;
        if (D6ClOptAddOptionNode (pOptionInfo) != OSIX_SUCCESS)
        {
            D6ClOptDelOptionNode (pOptionInfo);
            return OSIX_FAILURE;
        }
    }

    /* Add empty DNS search list option */

    pOptionInfo =
        D6ClOptGetGlobOptionInfo (pDhcp6ClntIfInfo->u4IfIndex,
                                  DHCP6_CLNT_DNS_SEARCH_LIST);
    if (pOptionInfo == NULL)
    {
        pOptionInfo = D6ClOptCreateNode ();
        if (pOptionInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        pOptionInfo->u4IfIndex = pDhcp6ClntIfInfo->u4IfIndex;
        pOptionInfo->u2Type = DHCP6_CLNT_DNS_SEARCH_LIST;
        if (D6ClOptAddOptionNode (pOptionInfo) != OSIX_SUCCESS)
        {
            D6ClOptDelOptionNode (pOptionInfo);
            return OSIX_FAILURE;
        }
    }
    /*First Information Message should go woth small delay */
    /*Calculate the Random Number between 0 and 1 and start the timer */
    D6ClUtlGenerateRandPseudoNumber (&i2RandomNumber, 0, DHCP6_CLNT_VALUE_ONE);
    if (i2RandomNumber != 0)
    {
        pDhcp6ClntIfInfo->b1FirstInfoMessage = OSIX_TRUE;
        if (D6ClTmrStart
            (pDhcp6ClntIfInfo, DHCP6_CLNT_TMR_RE_TRANSMISSION,
             i2RandomNumber) == OSIX_FAILURE)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClFormInterfaceTxPdu:D6clTmrStart Return Failure\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (D6ClFormInterfaceTxPdu (pDhcp6ClntIfInfo) == OSIX_FAILURE)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClFormUserTxPdu: D6ClFormInterfaceTxPdu "
                            " Function Return Failure. \r\n");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : D6ClFormInterfaceTxPdu
 *
 * Description        : This routine is used to Transmit the Information request
 *                      Message. 
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to interface Strcture 
 *                      u2CodeValue - Type Value
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PUBLIC INT4
D6ClFormInterfaceTxPdu (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo)
{
    tIp6Addr            dstAddr;
    UINT4               u4Length = 0;

    if (D6ClUtlValidateIfPropery (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClUtlValidateIfPropery: Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    if (D6ClTmrStopTimer (pDhcp6ClntIfInfo, DHCP6_CLNT_TMR_REFRESH_TIMER) ==
        OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClTmrStopTimer function returned FAILURE.!\n");
        return OSIX_FAILURE;;
    }
    if (D6ClTmrStopTimer (pDhcp6ClntIfInfo, DHCP6_CLNT_TMR_RE_TRANSMISSION)
        == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClTmrStopTimer function returned FAILURE.!\n");
        return OSIX_FAILURE;;
    }

    /*Calcutalte the RT */
    if (D6ClFormCalculateRT (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClFormInterfaceTxPdu: D6clFormCalculateRT function"
                        " returned FAILURE.\n");
        return OSIX_FAILURE;;
    }
    MEMSET (DHCP6_CLNT_PDU, 0x00, sizeof (DHCP6_CLNT_PDU));
    if (D6ClFormPdu (pDhcp6ClntIfInfo, &u4Length) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClFormInterfaceTxPdu: D6clFormPdu function"
                        " returned FAILURE.\n");
        return OSIX_FAILURE;
    }
    INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &dstAddr);
    /*Call the API to Send the Packet */

    DHCP6_CLNT_TRC_ARG1 (DHCP6_CLNT_DATA_PATH_TRC,
                         "D6ClFormInterfaceTxPdu: "
                         "DHCP PDU going to transmit from IfIndex =[%d]\r\n",
                         pDhcp6ClntIfInfo->u4IfIndex);

    DHCP6_CLNT_PKT_DUMP (DHCP6_CLNT_DUMP_TRC, DHCP6_CLNT_PDU, u4Length,
                         "D6ClFormInterfaceTxPdu: "
                         "Dumping DHCPv6 Information Request message going "
                         "to transmit..\r\n");

    if (D6ClSktSendToDst (pDhcp6ClntIfInfo, &dstAddr, DHCP6_CLNT_PDU, u4Length)
        == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClFormInterfaceTxPdu: D6ClSktSendToDst Return Failure.\r\n");
        return OSIX_FAILURE;
    }
    if (D6ClTmrStart
        (pDhcp6ClntIfInfo, (UINT1) DHCP6_CLNT_TMR_RE_TRANSMISSION,
         pDhcp6ClntIfInfo->u2CurrentRetTime) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClFormInterfaceTxPdu:D6clTmrStart Return Failure\r\n");
        return OSIX_FAILURE;
    }
    if (pDhcp6ClntIfInfo->b1IsRetansMessage == OSIX_FALSE)
    {
        OsixGetSysTime (&pDhcp6ClntIfInfo->TimeStamp);
    }
    pDhcp6ClntIfInfo->u4InformOut += DHCP6_CLNT_INCR_ONE;
    DHCP6_CLNT_TRC_ARG1 (DHCP6_CLNT_DATA_PATH_TRC,
                         "D6ClFormInterfaceTxPdu:Information Request Message out"
                         " from the interface [%d]\r\n",
                         pDhcp6ClntIfInfo->u4IfIndex);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : D6ClFormPdu
 *
 * Description        : This routine is used to Form the Information request
 *                      Message. 
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to interface Strcture
 *                      pu4Length Pointer to Pdu Length
 *
 * Output(s)          : pu4Length Pointer to Pdu Length
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
D6ClFormPdu (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo, UINT4 *pu4Length)
{
    tDhcp6ClntOptionInfo *pOptionInfo = NULL;
    UINT4               u4RdmValue = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4TransactionId = 0;
    UINT4               u4TimeValue = 0;
    UINT4               u4EnterpriseNumber = 0;
    UINT4               u4Counter = 0;
    UINT2               u2VendorClassLen = 0;
    UINT2               u2VendorClassOption = 0;
    UINT2               u2Length = 0;
    UINT2               u2ClientIdLen = 0;
    UINT2               u2CurrentApp = 0;
    UINT2               u2UserClassLen = 0;
    UINT2               u2UserClassOption = 0;
    UINT2               u2VendorSpecOption = 0;
    UINT2               u2VendorSpecLen = 0;
    UINT2               u2ElapsedTimeVal = 0;
    UINT2               u2OptionReqCode = 0;
    UINT2               u2OptionCodeLen = 0;
    UINT2               u2ClientIdOption = 0;
    UINT2               u2AuthOptionType = 0;
    UINT2               u2AuthLength = 0;
    UINT2               u2ElapsedTimeLen = 0;
    UINT2               u2ElapsedTimeOpt = 0;
    UINT2               u2OptValue = 0;
    UINT2               u2CodeValue = 0;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT1               u1Count = 0;

    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClFormPdu function returned FAILURE.!\n");
        return OSIX_FAILURE;
    }
    u2CurrentApp = pDhcp6ClntIfInfo->u2CurrentApp;
    pu1PduStart = DHCP6_CLNT_PDU;
    pu1PduEnd = DHCP6_CLNT_PDU;

    D6ClFormCalculateTransId (pDhcp6ClntIfInfo, &u4TransactionId);
    pDhcp6ClntIfInfo->u4TransactionId = u4TransactionId;

    /*Message Type is always Fix with value 1 */
    u4TransactionId = u4TransactionId | DHCP6_CLNT_INFORM_MESSAGE_TYPE;

    DHCP6_CLNT_PUT_4BYTE (pu1PduEnd, u4TransactionId);

    /*Put Client Identifer Option when asked by Application */
    if (pDhcp6ClntIfInfo->u1TransType == DHCP6_CLNT_APP_INIT_REQUEST)
    {
        if (pDhcp6ClntIfInfo->aau1Option[u2CurrentApp]
            [DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE] ==
            DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE)
        {
            u2ClientIdLen = (UINT2) pDhcp6ClntIfInfo->u1ClientIdLength;
            u2ClientIdOption = (UINT2) DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE;
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ClientIdOption);
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ClientIdLen);
            if (u2ClientIdLen <= DHCP6_CLNT_DUID_SIZE_MAX)
            {
                MEMCPY (pu1PduEnd, pDhcp6ClntIfInfo->au1ClientId,
                        u2ClientIdLen);
            }
            pu1PduEnd = pu1PduEnd + u2ClientIdLen;
            pDhcp6ClntIfInfo->b1IsClientIdTrsmt = OSIX_TRUE;
        }
        else
        {
            pDhcp6ClntIfInfo->b1IsClientIdTrsmt = OSIX_FALSE;
        }
    }

    /*When the Interface comes up in the client Mode then initiate the request
     * with the basic option types 21,22,23,24 with client Id only*/
    if ((pDhcp6ClntIfInfo->u1TransType == DHCP6_CLNT_USER_INIT_REQUEST) ||
        (pDhcp6ClntIfInfo->u1TransType == DHCP6_CLNT_REFRESH_TMR_EXP_REQUEST))
    {
        u2ClientIdLen = (UINT2) pDhcp6ClntIfInfo->u1ClientIdLength;
        u2ClientIdOption = (UINT2) DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ClientIdOption);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ClientIdLen);
        if (u2ClientIdLen <= DHCP6_CLNT_DUID_SIZE_MAX)
        {
            MEMCPY (pu1PduEnd, pDhcp6ClntIfInfo->au1ClientId, u2ClientIdLen);
        }
        pu1PduEnd = pu1PduEnd + u2ClientIdLen;
        pDhcp6ClntIfInfo->b1IsClientIdTrsmt = OSIX_TRUE;
    }

    if (pDhcp6ClntIfInfo->u1TransType == DHCP6_CLNT_USER_INIT_REQUEST)
    {
        u2OptionReqCode = DHCP6_CLNT_OPTION_REQUEST_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptionReqCode);
        u2OptionCodeLen = DHCP6_CLNT_BASIC_OPTION_LENGTH;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptionCodeLen);
        u2OptValue = DHCP6_CLNT_SIP_SERVER_DOMAIN_NAME;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptValue);
        u2OptValue = DHCP6_CLNT_SIP_SERVER_ADDRESS_LIST;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptValue);
        u2OptValue = DHCP6_CLNT_DNS_RECUSIVE_NAME;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptValue);
        u2OptValue = DHCP6_CLNT_DNS_SEARCH_LIST;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptValue);

        D6ClFormCalculateElapsedTime (pDhcp6ClntIfInfo, &u2ElapsedTimeVal);
        u2ElapsedTimeOpt = DHCP6_CLNT_ELPASED_OPTION_TYPE;
        u2ElapsedTimeLen = DHCP6_CLNT_ELAPSED_TIME_FIXED_LEN;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeOpt);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeLen);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeVal);
        *pu4Length = pu1PduEnd - pu1PduStart;
        return OSIX_SUCCESS;
    }
    /*When the Refresh Timer expires then client will send the information
     * request message with client Id and all the options populated on the interface.*/
    else if ((pDhcp6ClntIfInfo->u2CurrAppReqCode == 0)
             && (pDhcp6ClntIfInfo->u1TransType ==
                 DHCP6_CLNT_REFRESH_TMR_EXP_REQUEST))
    {
        /*Put option request field given by application */
        u2OptionReqCode = (UINT1) DHCP6_CLNT_OPTION_REQUEST_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptionReqCode);

        u4IfIndex = pDhcp6ClntIfInfo->u4IfIndex;
        pOptionInfo = D6ClOptGetFirstInterOptionInfo (u4IfIndex);

        while (pOptionInfo != NULL)
        {
            if (pOptionInfo != NULL)
            {
                if ((pOptionInfo->u2Type ==
                     DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE)
                    || (pOptionInfo->u2Type ==
                        DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE)
                    || (pOptionInfo->u2Type ==
                        DHCP6_CLNT_STATUS_CODE_OPTION_TYPE))
                {
                    pOptionInfo = D6ClOptGetNextInterOptionInfo
                        (pOptionInfo, u4IfIndex);
                    continue;
                }
                else
                {
                    u2OptionCodeLen =
                        (UINT2) (u2OptionCodeLen + DHCP6_CLNT_VALUE_TWO);
                }
            }
            pOptionInfo = D6ClOptGetNextInterOptionInfo
                (pOptionInfo, u4IfIndex);
        }
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptionCodeLen);

        pOptionInfo = D6ClOptGetFirstInterOptionInfo (u4IfIndex);
        while (pOptionInfo != NULL)
        {
            if (pOptionInfo != NULL)
            {
                if ((pOptionInfo->u2Type ==
                     DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE)
                    || (pOptionInfo->u2Type ==
                        DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE)
                    || (pOptionInfo->u2Type ==
                        DHCP6_CLNT_STATUS_CODE_OPTION_TYPE))
                {
                    pOptionInfo = D6ClOptGetNextInterOptionInfo
                        (pOptionInfo, u4IfIndex);
                    continue;
                }
                else
                {
                    DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, pOptionInfo->u2Type);
                }
            }
            pOptionInfo = D6ClOptGetNextInterOptionInfo
                (pOptionInfo, u4IfIndex);
        }

        D6ClFormCalculateElapsedTime (pDhcp6ClntIfInfo, &u2ElapsedTimeVal);
        u2ElapsedTimeOpt = DHCP6_CLNT_ELPASED_OPTION_TYPE;
        u2ElapsedTimeLen = DHCP6_CLNT_ELAPSED_TIME_FIXED_LEN;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeOpt);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeLen);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeVal);
        *pu4Length = pu1PduEnd - pu1PduStart;

        *pu4Length = pu1PduEnd - pu1PduStart;
        return OSIX_SUCCESS;
    }
    else
    {
        u2OptionReqCode = (UINT1) DHCP6_CLNT_OPTION_REQUEST_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptionReqCode);
        for (u1Count = 0; u1Count < DHCP6_CLNT_REQ_OPT_MAX; u1Count++)
        {
            if (pDhcp6ClntIfInfo->aau1RequestOption[u2CurrentApp][u1Count] != 0)
            {
                u2OptionCodeLen =
                    (UINT2) (u2OptionCodeLen + DHCP6_CLNT_VALUE_TWO);
            }
        }
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2OptionCodeLen);

        for (u1Count = 0; u1Count < DHCP6_CLNT_REQ_OPT_MAX; u1Count++)
        {
            if (pDhcp6ClntIfInfo->aau1RequestOption[u2CurrentApp][u1Count] != 0)
            {
                u2CodeValue =
                    pDhcp6ClntIfInfo->aau1RequestOption[u2CurrentApp][u1Count];
                DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2CodeValue);
                u2CodeValue = 0;
            }
        }
    }

    /*Put Elapsed Option when asked by Application */
    if (u2CurrentApp < MAX_D6CL_CLNT_APPS)
    {
        if (pDhcp6ClntIfInfo->aau1Option[u2CurrentApp]
            [DHCP6_CLNT_ELPASED_OPTION_TYPE] == DHCP6_CLNT_ELPASED_OPTION_TYPE)
        {
            D6ClFormCalculateElapsedTime (pDhcp6ClntIfInfo, &u2ElapsedTimeVal);
            u2ElapsedTimeOpt = DHCP6_CLNT_ELPASED_OPTION_TYPE;
            u2ElapsedTimeLen = DHCP6_CLNT_ELAPSED_TIME_FIXED_LEN;
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeOpt);
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeLen);
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2ElapsedTimeVal);

        }
    }
    else
    {
        return OSIX_FAILURE;
    }

    /*Add User Class Vendor Class and Vendor Specific Information 
     * */
    for (u4Counter = 0; u4Counter < DHCP6_CLNT_USER_CLASS_MAX; u4Counter++)
    {
        if ((pDhcp6ClntIfInfo->UserClass[u2CurrentApp][u4Counter].u2Length) !=
            0)
        {
            u2UserClassLen =
                (UINT2) (u2UserClassLen +
                         pDhcp6ClntIfInfo->UserClass[u2CurrentApp][u4Counter].
                         u2Length);
            u2UserClassLen = u2UserClassLen + DHCP6_CLNT_USER_CLASS_LENGTH;
        }
    }
    if (u2UserClassLen != 0)
    {
        u2UserClassOption = (UINT2) DHCP6_CLNT_USER_CLASS_CODE_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2UserClassOption);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2UserClassLen);

        for (u4Counter = 0; u4Counter < DHCP6_CLNT_USER_CLASS_MAX; u4Counter++)
        {
            if ((pDhcp6ClntIfInfo->UserClass[u2CurrentApp][u4Counter].
                 u2Length) != 0)
            {

                u2Length = 0;
                u2Length =
                    pDhcp6ClntIfInfo->UserClass[u2CurrentApp][u4Counter].
                    u2Length;
                DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2Length);
                MEMCPY
                    (pu1PduEnd,
                     pDhcp6ClntIfInfo->UserClass[u2CurrentApp][u4Counter].
                     pu1Value, u2Length);
                pu1PduEnd = pu1PduEnd + u2Length;
            }
        }
    }

    for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_CLASS_MAX; u4Counter++)
    {
        if ((pDhcp6ClntIfInfo->VendorClass[u2CurrentApp][u4Counter].u2Length) !=
            0)
        {
            u2VendorClassLen =
                (UINT2) (u2VendorClassLen +
                         pDhcp6ClntIfInfo->VendorClass[u2CurrentApp][u4Counter].
                         u2Length);
            u2VendorClassLen =
                (UINT2) (u2VendorClassLen + DHCP6_CLNT_VENDOR_CLASS_LENGTH);
        }
    }
    if (u2VendorClassLen != 0)
    {
        u2VendorClassLen =
            (UINT2) (u2VendorClassLen + DHCP6_CLNT_FOUR_BYTE_LENGTH);
        D6ClPortGetEnterpriseNumber (&u4EnterpriseNumber);
        u2VendorClassOption = (UINT2) DHCP6_CLNT_VENDOR_CLASS_CODE_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2VendorClassOption);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2VendorClassLen);
        DHCP6_CLNT_PUT_4BYTE (pu1PduEnd, u4EnterpriseNumber);

        for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_CLASS_MAX;
             u4Counter++)
        {
            if ((pDhcp6ClntIfInfo->VendorClass[u2CurrentApp][u4Counter].
                 u2Length) != 0)
            {
                u2Length = 0;
                u2Length =
                    pDhcp6ClntIfInfo->VendorClass[u2CurrentApp][u4Counter].
                    u2Length;
                DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2Length);
                MEMCPY (pu1PduEnd,
                        pDhcp6ClntIfInfo->VendorClass[u2CurrentApp][u4Counter].
                        pu1Value, u2Length);
                pu1PduEnd = pu1PduEnd + u2Length;
            }
        }
    }

    for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_SPEC_MAX; u4Counter++)
    {
        if ((pDhcp6ClntIfInfo->VendorSpec[u2CurrentApp][u4Counter].u2Length) !=
            0)
        {
            u2VendorSpecLen =
                (UINT2) (u2VendorSpecLen +
                         pDhcp6ClntIfInfo->VendorSpec[u2CurrentApp][u4Counter].
                         u2Length);
            u2VendorSpecLen =
                (UINT2) (u2VendorSpecLen + DHCP6_CLNT_VENDOR_SPEC_LENGTH +
                         DHCP6_CLNT_VENDOR_SPEC_CODE_LENGTH);
        }
    }
    if (u2VendorSpecLen != 0)
    {
        u2VendorSpecLen =
            (UINT2) (u2VendorSpecLen + DHCP6_CLNT_FOUR_BYTE_LENGTH);
        /*Get Enterprise Number */
        D6ClPortGetEnterpriseNumber (&u4EnterpriseNumber);
        u2VendorSpecOption = (UINT2) DHCP6_CLNT_VENDOR_SPECIFIC_OPTION_TYPE;
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2VendorSpecOption);
        DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2VendorSpecLen);
        DHCP6_CLNT_PUT_4BYTE (pu1PduEnd, u4EnterpriseNumber);

        for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_SPEC_MAX; u4Counter++)
        {
            if ((pDhcp6ClntIfInfo->VendorSpec[u2CurrentApp][u4Counter].
                 u2Length) != 0)
            {

                DHCP6_CLNT_PUT_2BYTE (pu1PduEnd,
                                      pDhcp6ClntIfInfo->
                                      VendorSpec[u2CurrentApp][u4Counter].
                                      u2Code);
                u2Length = 0;
                u2Length =
                    pDhcp6ClntIfInfo->VendorSpec[u2CurrentApp][u4Counter].
                    u2Length;
                DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2Length);
                MEMCPY
                    (pu1PduEnd,
                     pDhcp6ClntIfInfo->VendorSpec[u2CurrentApp][u4Counter].
                     pu1Value, u2Length);
                pu1PduEnd = pu1PduEnd + u2Length;
            }
        }
    }

    if (u2CurrentApp < MAX_D6CL_CLNT_APPS)
    {
        /*Put Authenication option when asked by Application */
        if (pDhcp6ClntIfInfo->aau1Option[u2CurrentApp]
            [DHCP6_CLNT_AUTH_IDENTIFIER_OPTION_TYPE] ==
            DHCP6_CLNT_AUTH_IDENTIFIER_OPTION_TYPE)
        {
            if (pDhcp6ClntIfInfo->aau1Option[u2CurrentApp]
                [DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE] !=
                DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC |
                                DHCP6_CLNT_BUFFER_TRC,
                                "D6ClFormPdu: Client can not transmit "
                                "Authenication option with out Client Id.\r\n");
                return OSIX_FAILURE;
            }
            if ((pDhcp6ClntIfInfo->u1RealmLength == 0) ||
                (pDhcp6ClntIfInfo->u1KeyLength == 0))
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC |
                                DHCP6_CLNT_BUFFER_TRC,
                                "D6ClFormPdu: Client can not transmit "
                                "Authenication option Realm and Key Id not"
                                " configured.\r\n");
                return OSIX_FAILURE;
            }

            u2AuthLength =
                (UINT2) (DHCP6_CLNT_AUTH_FIXED_LEN +
                         pDhcp6ClntIfInfo->u1RealmLength +
                         DHCP6_CLNT_FOUR_BYTE_KEY_ID +
                         DHCP6_CLNT_ALGO_DIGEST_SIZE);
            u2AuthOptionType = (UINT2) DHCP6_CLNT_AUTH_IDENTIFIER_OPTION_TYPE;
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2AuthOptionType);
            DHCP6_CLNT_PUT_2BYTE (pu1PduEnd, u2AuthLength);
            DHCP6_CLNT_PUT_1BYTE (pu1PduEnd, DHCP6_CLNT_DELAYED_AUTH_PROTOCOL);
            DHCP6_CLNT_PUT_1BYTE (pu1PduEnd, DHCP6_CLNT_HMAC_MD5_ALGORITHM);
            DHCP6_CLNT_PUT_1BYTE (pu1PduEnd, DHCP6_CLNT_RDM_VALUE);
            /*Put 8 bytes Replay detection Current System Time Value */
            u4TimeValue = D6ClCUtlGetTimeFromEpoch ();

            DHCP6_CLNT_PUT_4BYTE (pu1PduEnd, u4RdmValue);
            DHCP6_CLNT_PUT_4BYTE (pu1PduEnd, u4TimeValue);
            if (pDhcp6ClntIfInfo->u1RealmLength <= DHCP6_CLNT_REALM_SIZE_MAX)
            {
                MEMCPY (pu1PduEnd, pDhcp6ClntIfInfo->au1Realm,
                        pDhcp6ClntIfInfo->u1RealmLength);
            }
            pu1PduEnd = pu1PduEnd + pDhcp6ClntIfInfo->u1RealmLength;
            DHCP6_CLNT_PUT_4BYTE (pu1PduEnd, pDhcp6ClntIfInfo->u4KeyIdentifier);
            MEMSET (pu1PduEnd, 0x00, DHCP6_CLNT_ALGO_DIGEST_SIZE);
            pu1PduEnd = pu1PduEnd + DHCP6_CLNT_ALGO_DIGEST_SIZE;
            /*Call the HMAC-MD5 Auth API */
            D6ClAuthCalculateData (pu1PduStart, (pu1PduEnd - pu1PduStart),
                                   pDhcp6ClntIfInfo->au1Key,
                                   pDhcp6ClntIfInfo->u1KeyLength,
                                   (pu1PduEnd - DHCP6_CLNT_ALGO_DIGEST_SIZE));
        }
    }
    *pu4Length = pu1PduEnd - pu1PduStart;
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : D6ClFormCalculateTransId
 *
 * Description        : This routine is used to Calculate the Transaction Id 
 *                      Value. 
 *
 * Input(s)           : pu4MesgTypeTransId Pointer to Transaction id Value
 *                      pDhcp6ClntIfInfo Pointor to Interface
 *
 * Output(s)          : pu4MesgTypeTransId Pointer to Transaction id Value
 *
 * Return Value(s)    : None
 *****************************************************************************/

PRIVATE VOID
D6ClFormCalculateTransId (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo,
                          UINT4 *pu4MesgTypeTransId)
{
    if (pDhcp6ClntIfInfo->b1IsRetansMessage == OSIX_TRUE)
    {
        *pu4MesgTypeTransId = pDhcp6ClntIfInfo->u4TransactionId;
        return;
    }
    if (pDhcp6ClntIfInfo->u4TransactionId == DHCP_MAX_TRANS_ID_VAL)
    {
        *pu4MesgTypeTransId = DHCP_MIN_TRANS_ID_VAL;
        return;
    }
    if (pDhcp6ClntIfInfo->u4TransactionId == 0)
    {
        *pu4MesgTypeTransId = DHCP_MIN_TRANS_ID_VAL;
        return;
    }
    else
    {
        *pu4MesgTypeTransId =
            pDhcp6ClntIfInfo->u4TransactionId + DHCP6_CLNT_INCR_ONE;
    }
    return;
}

/******************************************************************************
 * Function Name      : D6ClFormCalculateElapsedTime
 *
 * Description        : This routine is used to Calculate the Retransmittion 
 *                      Value. 
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to Interface Strcture
 *
 * Output(s)          : pu2ElapsedTimeVal - Pointer to value
 *
 * Return Value(s)    : None
 *****************************************************************************/

PRIVATE VOID
D6ClFormCalculateElapsedTime (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo,
                              UINT2 *pu2ElapsedTimeVal)
{
    tOsixSysTime        CurrentTimeStamp;
    tOsixSysTime        ElapsedTimeStamp;
    UINT4               u4ElapsedTimeVal = 0;

    /*First Time Client is transmitting the message then */
    if (pDhcp6ClntIfInfo->b1IsRetansMessage == OSIX_FALSE)
    {
        *pu2ElapsedTimeVal = 0;
    }
    else
    {
        /*Calculate Elapsed Time value */
        OsixGetSysTime (&CurrentTimeStamp);

        ElapsedTimeStamp = CurrentTimeStamp - pDhcp6ClntIfInfo->TimeStamp;
        u4ElapsedTimeVal =
            DHCP6_CLNT_CONVERT_TIME_TICKS_TO_10MSEC (ElapsedTimeStamp);

        if (u4ElapsedTimeVal >= DHCP6_CLNT_MAX_ELAPSED_TIME)
        {
            *pu2ElapsedTimeVal = DHCP6_CLNT_MAX_ELAPSED_TIME;
        }
        else
        {
            *pu2ElapsedTimeVal = (UINT2) u4ElapsedTimeVal;
        }
    }
    return;
}

/******************************************************************************
 * Function Name      : D6ClFormCalculateRT
 *
 * Description        : This routine is used to Calculate the Retransmittion 
 *                      Value. 
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to Interface Strcture
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/

PRIVATE INT4
D6ClFormCalculateRT (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo)
{
    UINT2               u2CurrentRetrDelay = 0;
    UINT2               u2LocaltRetrDelay = 0;
    INT2                i2RandomNumber = 0;
    UINT1               u1CurrentRetrCount = 0;

    u1CurrentRetrCount = pDhcp6ClntIfInfo->u1CurrRetCount;
    u2CurrentRetrDelay = pDhcp6ClntIfInfo->u2CurrentRetDelay;

    D6ClUtlGenerateRandPseudoNumber
        (&i2RandomNumber, DHCP6_CLNT_MIN_RAND_RANGE, DHCP6_CLNT_MAX_RAND_RANGE);
    if ((pDhcp6ClntIfInfo->u2CurrentRetTime == 0))
    {
        /* First time calculate the RT by the relation
         * RT = IRT + RAND*IRT */
        u2LocaltRetrDelay = (UINT2) (pDhcp6ClntIfInfo->u2InitRetTime +
                                     i2RandomNumber *
                                     pDhcp6ClntIfInfo->u2InitRetTime);
    }
    else
    {
        /* First time calculate the RT by the relation
         * RT = 2*RTprev + RAND*RTprev */
        u2LocaltRetrDelay =
            (UINT2) (pDhcp6ClntIfInfo->u2CurrentRetTime *
                     DHCP6_CLNT_MULTIPLE_TWO +
                     i2RandomNumber * pDhcp6ClntIfInfo->u2CurrentRetTime);
    }
    if ((pDhcp6ClntIfInfo->u2MaxRetDelay == 0) &&
        (pDhcp6ClntIfInfo->u1MaxRetCount == 0))
    {
        pDhcp6ClntIfInfo->u2CurrentRetTime = u2LocaltRetrDelay;
        return OSIX_SUCCESS;;
    }

    /* When (RT > MRT)
     * RT = MRT + RAND*MRT
     * */

    if (u2LocaltRetrDelay > pDhcp6ClntIfInfo->u2MaxRetTime)
    {
        u2LocaltRetrDelay = pDhcp6ClntIfInfo->u2MaxRetTime +
            pDhcp6ClntIfInfo->u2MaxRetTime * i2RandomNumber;
    }
    u1CurrentRetrCount =
        (UINT1) (u1CurrentRetrCount + (UINT2) DHCP6_CLNT_INCR_ONE);
    u2CurrentRetrDelay = (UINT2) (u2CurrentRetrDelay + u2LocaltRetrDelay);

    if ((pDhcp6ClntIfInfo->b1IsRetansMessage == OSIX_TRUE) &&
        (pDhcp6ClntIfInfo->u1MaxRetCount != 0) &&
        (u1CurrentRetrCount > (UINT2) pDhcp6ClntIfInfo->u1MaxRetCount))
    {
        DHCP6_CLNT_TRC_ARG2 (DHCP6_CLNT_ALL_FAILURE_TRC,
                             "D6ClFormCalculateRT: Ret count exceeded %d %d.!\n",
                             u1CurrentRetrCount,
                             pDhcp6ClntIfInfo->u1MaxRetCount);
        return OSIX_FAILURE;;
    }
    if ((pDhcp6ClntIfInfo->b1IsRetansMessage == OSIX_TRUE) &&
        (pDhcp6ClntIfInfo->u2MaxRetDelay != 0) &&
        (u2CurrentRetrDelay > (UINT2) pDhcp6ClntIfInfo->u2MaxRetDelay))
    {
        DHCP6_CLNT_TRC_ARG2 (DHCP6_CLNT_ALL_FAILURE_TRC,
                             "D6ClFormCalculateRT: Ret delay exceeded %d %d.!\n",
                             u2CurrentRetrDelay,
                             pDhcp6ClntIfInfo->u2MaxRetDelay);
        return OSIX_FAILURE;;
    }
    pDhcp6ClntIfInfo->u2CurrentRetTime = u2LocaltRetrDelay;
    pDhcp6ClntIfInfo->u1CurrRetCount = u1CurrentRetrCount;
    pDhcp6ClntIfInfo->u2CurrentRetDelay = u2CurrentRetrDelay;
    return OSIX_SUCCESS;;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clform.c                       */
/*-----------------------------------------------------------------------*/
