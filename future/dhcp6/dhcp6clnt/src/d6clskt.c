/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains DHCP6 client task main loop and 
 *              initialization   routines.
 *  $Id: d6clskt.c,v 1.14 2017/09/08 12:22:45 siva Exp $
 * ************************************************************************/
#ifdef GNU_CC
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#endif

#include "d6clinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClSktCreatePktRcv                                        */
/*                                                                           */
/* Description  : This function Creates a socket, binds and sets option to   */
/*                receive the packets sent by UDP6 to DHCP6 through the Socket*/
/*                Layer.                                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6ClSktCreatePktRcv ()
{
    struct sockaddr_in6 SockAddr;
    INT4                i4Sock = -1;
    INT4                i4Optval = 1;

    i4Sock = socket (AF_INET6, SOCK_DGRAM, 0);
    MEMSET (&SockAddr, 0, sizeof (SockAddr));
    /* Bind a address and port to the socket */
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (DHCP6_CLNT_SOURCE_PORT);
    SockAddr.sin6_flowinfo = 0;
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);

    if (i4Sock < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClSktCreatePktRcv: Failed to create socket.\r\n");
        return OSIX_FAILURE;
    }

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClSktCreatePktRcv: Unable to Bind Socket.\r\n");
        return OSIX_FAILURE;
    }

    if (setsockopt (i4Sock, IPPROTO_IPV6, IP_PKTINFO, (INT2 *) (VOID *)
                    &i4Optval, sizeof (i4Optval)) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClSktCreatePktRcv: Unable to set setsockopt.\r\n");
        return OSIX_FAILURE;
    }
    DHCP6_CLNT_SOCKET_ID = i4Sock;

    /*Set the option not to receive multicast packet on the interface on which
     * it is sent*/
    i4Optval = FALSE;
    if (setsockopt (i4Sock, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4Optval, sizeof (i4Optval)) < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClSktCreatePktRcv: Unable to set setsockopt for IPV6_MULTICAST_LOOP.\r\n");
        return OSIX_FAILURE;
    }
    if (SelAddFd (i4Sock, D6ClSktPacketOnClientSocket) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClSktCreatePktRcv: Adding Socket Descriptor to"
                        " Select utility Failed.\r\n");
        DHCP6_CLNT_SOCKET_ID = DHCP6_CLNT_MINUS_ONE;
        close (DHCP6_CLNT_SOCKET_ID);
        return OSIX_FAILURE;
    }

    if (fcntl (i4Sock, F_SETFL, O_NONBLOCK) < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClSktCreatePktRcv: Adding Socket Descriptor to"
                        " Select utility Failed.\r\n");
        DHCP6_CLNT_SOCKET_ID = DHCP6_CLNT_MINUS_ONE;
        close (DHCP6_CLNT_SOCKET_ID);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClSktDeletePktRcv                                        */
/*                                                                           */
/* Description  : This function will Remove and close the socket added for   */
/*                DHCP6C packet reception.                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
D6ClSktDeletePktRcv ()
{
    if (DHCP6_CLNT_SOCKET_ID != DHCP6_CLNT_MINUS_ONE)
    {
        SelRemoveFd (DHCP6_CLNT_SOCKET_ID);
        close (DHCP6_CLNT_SOCKET_ID);
    }
    DHCP6_CLNT_SOURCE_PORT = 0;
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClSktPacketOnClientSocket                                */
/*                                                                           */
/* Description  : Call back function from SelAddFd(), when Dhcp6C packet is  */
/*                received.                                                  */
/*                                                                           */
/* Input        : i4SockFd Socket Id                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
D6ClSktPacketOnClientSocket (INT4 i4SockFd)
{
    if (i4SockFd == DHCP6_CLNT_SOCKET_ID)
    {
        if (OsixSendEvent
            (DHCP6_CLNT_SELF, (const UINT1 *) DHCP6_CLNT_TASK_NAME,
             DHCP6_CLNT_EV_DHCP6_PDU_EVENT) != OSIX_SUCCESS)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC |
                            DHCP6_CLNT_OS_RESOURCE_TRC,
                            "D6ClSktPacketOnClientSocket: Event send Failed.\r\n");
        }
        DHCP6_CLNT_TRC (DHCP6_CLNT_DATA_PATH_TRC,
                        "D6ClSktPacketOnClientSocket: Message Received Send"
                        " on DHCP6C Task.\r\n");
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClSktSendToDst                                            */
/*                                                                           */
/* Description  : The function sends the information request message         */
/*                                                                           */
/* Input        : pDhcp6ClntIfInfo -        */
/*                pu1Buf  -  DHCP6 packet (Linear buffer).                   */
/*                u4Len   -  Length of received packet.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClSktSendToDst (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo, tIp6Addr * pdstAddr,
                  UINT1 *pu1Pdu, UINT4 u4LengthVal)
{
    tIp6Addr            SrcAddress;
    tIp6Addr            SourceAddress;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    UINT2               u2DstPort = 0;
    INT4                i4ReturnValue = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4HopLimit = 0;
    struct cmsghdr      CmsgInfo;
#else
    UINT1               CmsgInfo[112];
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    MEMSET (&SrcAddress, 0, sizeof (tIp6Addr));
    MEMSET (&SourceAddress, 0, sizeof (tIp6Addr));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    if (u4LengthVal > DHCP6_CLNT_MAX_PDU_SIZE)
    {
        return OSIX_FAILURE;
    }
    u2DstPort = (UINT2) DHCP6_CLNT_DEST_PORT;

    /* Set src addr as link local addr for this logical interface */

    if (D6ClPortGetInterfaceAddressIdentifier
        (pDhcp6ClntIfInfo->u4IfIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (MEMCMP (&NetIpv6IfInfo.Ip6Addr, &SrcAddress, sizeof (tIp6Addr)) == 0)
    {
        if (D6ClPortInGlobalAddr (pDhcp6ClntIfInfo->u4IfIndex, pdstAddr,
                                  &SourceAddress) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        Ip6AddrCopy (((tIp6Addr *) & SrcAddress), &SourceAddress);
    }
    else
    {
        Ip6AddrCopy (((tIp6Addr *) & SrcAddress), &NetIpv6IfInfo.Ip6Addr);
    }

    PeerAddr.sin6_family = AF_INET6;
    PeerAddr.sin6_port = OSIX_HTONS (u2DstPort);
#ifdef BSDCOMP_SLI_WANTED
    i4ReturnValue =
        NetIpv6GetIfInfo (pDhcp6ClntIfInfo->u4IfIndex, &NetIpv6IfInfo);
    if (i4ReturnValue == NETIPV6_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        " info for the index not found\r\n");
        return OSIX_FAILURE;
    }

    PeerAddr.sin6_scope_id = NetIpv6IfInfo.u4IpPort;
#else
    PeerAddr.sin6_scope_id = pDhcp6ClntIfInfo->u4IfIndex;
#endif
    UNUSED_PARAM (i4ReturnValue);
    Ip6AddrCopy ((tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr, pdstAddr);

#ifndef BSDCOMP_SLI_WANTED

    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMCPY (&pIn6Pktinfo->ipi6_ifindex, &pDhcp6ClntIfInfo->u4IfIndex,
            sizeof (UINT4));

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 &SrcAddress);

    sendmsg (DHCP6_CLNT_SOCKET_ID, &Ip6MsgHdr, 0);

    if (setsockopt (DHCP6_CLNT_SOCKET_ID, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                    &i4HopLimit, sizeof (INT4)) < 0)
    {
        return OSIX_FAILURE;
    }
    if (setsockopt (DHCP6_CLNT_SOCKET_ID, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4HopLimit, sizeof (INT4)) < 0)
    {
        return OSIX_FAILURE;
    }

    if (sendto (DHCP6_CLNT_SOCKET_ID, pu1Pdu, u4LengthVal, 0,
                (struct sockaddr *) &PeerAddr, sizeof (struct sockaddr)) < 0)
    {
        return OSIX_FAILURE;
    }
#else
    pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;

    /* EVALRIPNG cc below :: changed from sizeof (Cmsginfo) */

    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    /* EVALRIPNG cc */
    Iov.iov_base = pu1Pdu;
    Iov.iov_len = u4LengthVal;

    Ip6MsgHdr.msg_name = (VOID *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    /* EVALRIPNG end */
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    pIn6Pktinfo->ipi6_ifindex = PeerAddr.sin6_scope_id;

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 &SrcAddress);

    /* EVALRIPNG :: using a single call */
    if (sendmsg (DHCP6_CLNT_SOCKET_ID, &Ip6MsgHdr, 0) < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClSktSendToDst: Error in sending message !\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clskt.c                       */
/*-----------------------------------------------------------------------*/
