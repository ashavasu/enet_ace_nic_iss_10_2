/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains DHCP6 client task main loop and 
 *              initialization   routines.
 * ************************************************************************/
#include "d6clinc.h"

PRIVATE INT4 D6ClMainTaskInit PROTO ((VOID));
PRIVATE INT4 D6ClMainCreateBuddyMemPool PROTO ((VOID));
PRIVATE INT4 D6ClMainDeleteBuddyMemPool PROTO ((VOID));
PRIVATE INT4 D6ClMainMempoolInit PROTO ((VOID));
PRIVATE VOID D6ClMainTaskInitFailure PROTO ((VOID));
PRIVATE VOID D6ClMainMempoolDeInit PROTO ((VOID));
PRIVATE INT4 D6ClMainInitGlobalInfo PROTO ((VOID));
PRIVATE VOID D6ClMainDeInitGlobalInfo PROTO ((VOID));

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainMempoolInit                                          */
/*                                                                           */
/* Description  : This Function creates Memory Pools for DHCPv6 client Task  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6ClMainMempoolInit ()
{

    /* Creating Mempool */
    if (D6clSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClMainMempoolInit: Creation of Mem Pool"
                        " for DHCP6 Client Task FAILED !!! \r\n");
        return OSIX_FAILURE;
    }

    if (D6ClIfCreateTable () == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClIfCreatememoryPools: Creation of RBTree"
                        "for DHCP6 Client Task FAILED !!! \r\n");
        return OSIX_FAILURE;
    }

    if (D6ClOptCreateTable () == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptCreateTable: Creation of RBTree"
                        "for DHCP6 Client Task FAILED !!! \r\n");
        return OSIX_FAILURE;
    }
    if (D6ClTmrInit () != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClMainMempoolInit: D6clTmrInit Return Failure !!! \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainInitGlobalInfo                                       */
/*                                                                           */
/* Description  : This function initalizes global structure, sets the Trace  */
/*                option.                                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                                        */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6ClMainInitGlobalInfo ()
{
    DHCP6_CLNT_TRACE_OPTION = (UINT4) DHCP6_CLNT_CRITICAL_TRC;
    DHCP6_CLNT_IS_INITIALISED = OSIX_TRUE;
    DHCP6_CLNT_TRAP_CONTROL_OPTION = 0;
    DHCP6_CLNT_SYS_LOG_OPTION = OSIX_FALSE;
    DHCP6_CLNT_SOURCE_PORT = (UINT2) DHCP6_CLNT_DEFAULT_SOURCE_PORT;
    DHCP6_CLNT_DEST_PORT = (UINT2) DHCP6_CLNT_DEFAULT_DEST_PORT;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainDeInitGlobalInfo                                     */
/*                                                                           */
/* Description  : This function Deinitalizes global structure,sets the Trace */
/*                option.                                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6ClMainDeInitGlobalInfo ()
{
    DHCP6_CLNT_SOURCE_PORT = 0;
    DHCP6_CLNT_DEST_PORT = 0;
    DHCP6_CLNT_TRACE_OPTION = 0;
    DHCP6_CLNT_IS_INITIALISED = OSIX_FALSE;
    DHCP6_CLNT_TRAP_CONTROL_OPTION = 0;
    DHCP6_CLNT_SYS_LOG_OPTION = (UINT1) OSIX_FALSE;

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainTaskInit                                             */
/*                                                                           */
/* Description  : This function creates the task queue, allocates            */
/*                MemPools for task queue messages and creates protocol      */
/*                semaphore,Timer List.                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6ClMainTaskInit ()
{
    INT4                i4SysLogId = 0;

    MEMSET (&gDhcp6ClntGblInfo, 0, DHCP6_CLNT_GLOBAL_INFO_SIZE);

    OsixTskIdSelf (&DHCP6_CLNT_TASK_ID);

    /* Protocol semaphore - created with initial value 0. */
    if (OsixSemCrt
        ((UINT1 *) DHCP6_CLNT_SEM_NAME, &DHCP6_CLNT_SEM_ID) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC
                        | DHCP6_CLNT_CRITICAL_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClMainTaskInit: Protocol semaphore creation Failure"
                        " Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem is 
     * called before using the semaphore. */
    OsixSemGive (DHCP6_CLNT_SEM_ID);

    /* Creating Configuration Message queue for configuration messages */
    if (OsixQueCrt
        ((UINT1 *) DHCP6_CLNT_CFG_QUEUE_NAME, OSIX_DEF_MSG_LEN,
         MAX_D6CL_CLNT_QMSGS, &DHCP6_CLNT_CFG_QUEUE_ID) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (D6ClMainMempoolInit () != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC
                        | DHCP6_CLNT_CRITICAL_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClMainTaskInit: D6clManMempoolInit"
                        " Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    if (D6ClMainCreateBuddyMemPool () == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "Dhcp6TaskInit: Buddy Mempool creation Failed. \r\n");
        return OSIX_FAILURE;
    }
    D6ClMainInitGlobalInfo ();

#ifdef SYSLOG_WANTED
    /* Register with SysLog */
    i4SysLogId = SYS_LOG_REGISTER (DHCP6_CLNT_NAME, SYSLOG_CRITICAL_LEVEL);
    if (i4SysLogId <= 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClMainTaskInit: Sys log failed. \r\n");
        return OSIX_FAILURE;
    }
#endif /* SYSLOG_WANTED */

    /* Save the syslog id in the MIB */
    DHCP6_CLNT_SYSLOG_ID = (UINT4) (i4SysLogId);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainMempoolDeInit                                       */
/*                                                                           */
/* Description  : This Function deletes all mempools for DHCPv6 client Task */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6ClMainMempoolDeInit ()
{
    NetIpv6DeRegisterHigherLayerProtocol (IP6_DHCP6_CLNT_PROTOID);
    /* DeInitialize Global Info */
    D6ClMainDeInitGlobalInfo ();
    D6ClOptDeleteTable ();
    D6ClIfDelateTable ();
    D6clSizingMemDeleteMemPools ();
    /* Delete TImer List and Timer DHCPv6 Client Task */
    D6ClMainDeleteBuddyMemPool ();
    D6ClTmrDeInit ();

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainTaskInitFailure                                      */
/*                                                                           */
/* Description  : This function called when the initialization of  DHCPv6    */
/*                Task fails at any point during initialization.             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
D6ClMainTaskInitFailure ()
{
    tDhcp6ClntQMsg     *pMsg = NULL;

    /* Clearing Config Queue for CC Task */
    if (DHCP6_CLNT_CFG_QUEUE_ID != 0)
    {
        /* Dequeue Configuration messages from the Config Queue */
        while (OsixQueRecv (DHCP6_CLNT_CFG_QUEUE_ID, (UINT1 *) &pMsg,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)

        {
            if (pMsg == NULL)

            {
                continue;
            }
        }
        /* Delete Queue */
        OsixQueDel (DHCP6_CLNT_CFG_QUEUE_ID);
        DHCP6_CLNT_CFG_QUEUE_ID = 0;
    }

#ifdef SYSLOG_WANTED
    SYS_LOG_DEREGISTER (DHCP6_CLNT_SYSLOG_ID);
    DHCP6_CLNT_SYSLOG_ID = 0;
#endif
    /* Delete Semaphore */
    if (DHCP6_CLNT_SEM_ID != 0)
    {
        OsixSemDel (DHCP6_CLNT_SEM_ID);
        DHCP6_CLNT_SEM_ID = 0;
    }
    D6ClMainMempoolDeInit ();
    D6ClSktDeletePktRcv ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainTask                                          */
/*                                                                           */
/* Description  : This is the initialization function for the DHCP6 client   */
/*                module called at bootup time.This is the entry routine for*/
/*                the DHCPv6 client . It is continuosly waiting for the      */
/*                events,which on reception calls Pakcet/Configuration       */
/*                message Queue handler or Timer Expiry Handler              */
/*                based on the event received.                               */
/*                                                                           */
/* Input        : pArg - Pointer to the arguments                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6ClMainTask (INT1 *pArg)
{
    tDhcp6ClntQMsg     *pQMsg = NULL;
    UINT4               u4Event = 0;
    INT4                i4ip6local = OSIX_FALSE;
    INT1                i1RegFlag = OSIX_FALSE;
    UNUSED_PARAM (pArg);

    /*Initilize the DHCP6 Client  Task */
    if (D6ClMainTaskInit () != OSIX_SUCCESS)
    {
        D6ClMainTaskInitFailure ();

        /* Indicate the status to the main module lrInitComplete */
        lrInitComplete (OSIX_FAILURE);

        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClMainTask: D6clManTaskInit"
                        " Function Return Failure. \r\n");
        return;
    }

    if (OsixTskIdSelf (&(DHCP6_CLNT_TASK_ID)) != OSIX_SUCCESS)
    {
        D6ClMainTaskInitFailure ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the Client Mib only */
    RegisterFSDH6C ();
#endif /*  */

    /* Wait to Register with IP6, until IP6 Task Comes up */
    do
    {
        i4ip6local = NetIpv6RegisterHigherLayerProtocol (IP6_DHCP6_CLNT_PROTOID,
                                                         NETIPV6_INTERFACE_PARAMETER_CHANGE,
                                                         (VOID *)
                                                         D6ClApiIp6IfStatusNotify);
        if (i4ip6local == NETIPV6_SUCCESS)
        {
            i1RegFlag = OSIX_TRUE;
        }
    }
    while (i1RegFlag != OSIX_TRUE);

    if (D6ClSktCreatePktRcv () == OSIX_FAILURE)
    {
        return;
    }

    /* Indicate the status to the main module lrInitComplete */
    lrInitComplete (OSIX_SUCCESS);
    while (1)
    {
        if (OsixEvtRecv
            (DHCP6_CLNT_TASK_ID, DHCP6_CLNT_EV_ALL, OSIX_WAIT,
             &u4Event) != OSIX_SUCCESS)
        {
            continue;
        }
        /* Mutual exclusion flag ON */
        D6ClApiLock ();
        /* Handle DHCP6 PDUs received in the Packet Queue */
        if (u4Event & DHCP6_CLNT_EV_DHCP6_PDU_EVENT)
        {
            D6ClQuePktHandler ();
            /* Check to see the socket was deleted by CLI and is
             * initialized to -1 */
            if ((DHCP6_CLNT_SOCKET_ID != -1) &&
                (u4Event & DHCP6_CLNT_EV_DHCP6_PDU_EVENT))
            {
                if (SelAddFd
                    (DHCP6_CLNT_SOCKET_ID,
                     D6ClSktPacketOnClientSocket) != OSIX_SUCCESS)
                {
                    DHCP6_CLNT_TRC (DHCP6_CLNT_OS_RESOURCE_TRC |
                                    DHCP6_CLNT_ALL_FAILURE_TRC,
                                    "D6ClMainTask: Adding Socket Descriptor"
                                    " to Select utility Failed. \r\n");
                }
            }

        }
        /* Handle Configuration Messages received in the Configuration Queue
         * for DHCP6C Task */
        if (u4Event & DHCP6_CLNT_EV_DHCP6_MSG_IN_QUEUE)
        {
            while (OsixQueRecv
                   (DHCP6_CLNT_CFG_QUEUE_ID, (UINT1 *) &pQMsg,
                    OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
            {
                D6ClQueueCfgHandler (pQMsg);
                pQMsg = NULL;
            }
        }
        if (u4Event & DHCP6_CLNT_EV_TMR_EXP)
        {
            D6ClTmrExpHandler ();
        }

        /* Mutual exclusion flag OFF */
        D6ClApiUnLock ();
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainCreateBuddyMemPool                                    */
/*                                                                           */
/* Description  : This function creates the required Buddy mempools          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6ClMainCreateBuddyMemPool (VOID)
{
    UINT4               u4MinBlkAdjusted = 0;
    UINT4               u4MaxBlkAdjusted = 0;

    UtilCalculateBuddyMemPoolAdjust (DHCP6_CLNT_BUDDY_SIZE_MIN,
                                     DHCP6_CLNT_BUDDY_SIZE_MAX,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);

    gDhcp6ClntGblInfo.BuddyMemPool = MemBuddyCreate (u4MaxBlkAdjusted,
                                                     u4MinBlkAdjusted,
                                                     DHCP6_CLNT_BUDDY_BLK_MAX,
                                                     BUDDY_CONT_BUF);

    if (gDhcp6ClntGblInfo.BuddyMemPool == BUDDY_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClMainCreateBuddyMemPool: "
                        "Buddy Mempool creation Failed. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClMainDeleteBuddyMemPool                                    */
/*                                                                           */
/* Description  : This function deletes the required Buddy mempools          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6ClMainDeleteBuddyMemPool (VOID)
{
    if (DHCP6_CLNT_BUDDY_MEM_POOL_ID != BUDDY_FAILURE)
    {
        MemBuddyDestroy ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID);
        DHCP6_CLNT_BUDDY_MEM_POOL_ID = BUDDY_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clmain.c                       */
/*-----------------------------------------------------------------------*/
