/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * 
 * $Id: d6cltmr.c,v 1.4 2013/09/26 12:53:06 siva Exp $
 * 
 * Description: This file contains the routines for Timer functionality 
 *              for DHCPv6 Client module.
 * ************************************************************************/
#include "d6clinc.h"
PRIVATE VOID D6ClTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID D6ClTmrRefreshExpired PROTO ((VOID *));
PRIVATE VOID D6ClTmrRetransmissionExpired PROTO ((VOID *));

/************************************************************************/
/*  Function Name   : D6ClTmrInit                                     */
/*                                                                      */
/*  Description     : This function creates the timer pool & List for   */
/*                    DHCPv6 Client Task.                               */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/

PUBLIC INT4
D6ClTmrInit ()
{
    /* Timer Initialization */
    if (TmrCreateTimerList
        ((UINT1 *) DHCP6_CLNT_TASK_NAME, DHCP6_CLNT_EV_TMR_EXP, NULL,
         &(DHCP6_CLNT_TMRLIST_ID)) == TMR_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                        ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                        "D6ClTmrInit: Timer list creation FAILED "
                        "for DHCP6 Client Task \r\n");
        return OSIX_FAILURE;
    }
    D6ClTmrInitTmrDesc ();

    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : D6ClTmrDeInit                                   */
/*                                                                      */
/*  Description     : This function deletes the timer list created for  */
/*                    DHCPv6 Client Task.                               */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC INT4
D6ClTmrDeInit ()
{
    INT4                i4RetVal = OSIX_SUCCESS;
    if (DHCP6_CLNT_TMRLIST_ID != 0)
    {
        /* Deleting the Timer List */
        if (TmrDeleteTimerList (DHCP6_CLNT_TMRLIST_ID) == TMR_FAILURE)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                            DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_OS_RESOURCE_TRC,
                            "D6ClTmrDeInit: "
                            "Timer list deleteion FAILED for DHCP6 Client Task \r\n");
            i4RetVal = OSIX_FAILURE;
        }
        MEMSET (gDhcp6ClntGblInfo.aTmrDesc, 0,
                (sizeof (tTmrDesc) * DHCP6_CLNT_MAX_TMR_TYPES));

        DHCP6_CLNT_TMRLIST_ID = 0;
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClTmrDeInit: DHCP6 Task Timers DeInitialized \r\n");
    }
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : D6ClTmrInitTmrDesc                              */
/*                                                                      */
/*  Description     :  This function creates a timer list for all       */
/*                     the timers DHCPv6 Client Task.                   */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/

PRIVATE VOID
D6ClTmrInitTmrDesc (VOID)
{
    gDhcp6ClntGblInfo.aTmrDesc[DHCP6_CLNT_RETRANS_MISSION_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDhcp6ClntIfInfo, ReTransmissionTimer);

    gDhcp6ClntGblInfo.aTmrDesc[DHCP6_CLNT_RETRANS_MISSION_TMR].TmrExpFn =
        D6ClTmrRetransmissionExpired;

    gDhcp6ClntGblInfo.aTmrDesc[DHCP6_CLNT_REFRESH_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tDhcp6ClntIfInfo, RefreshTimer);
    gDhcp6ClntGblInfo.aTmrDesc[DHCP6_CLNT_REFRESH_TMR].TmrExpFn
        = D6ClTmrRefreshExpired;
    return;
}

/************************************************************************/
/*  Function Name   : D6ClTmrStart                                  */
/*                                                                      */
/*  Description     : This function is called to start timer for DHCPv6 */
/*                    Client Task.                                      */
/*                                                                      */
/*  Input(s)        : pDhcp6ClntIfInfo Pointer to Interface             */
/*                    u1TimerType - Type of timer ot be started.        */
/*                  : u4Duration  - Duration for which the timer needs  */
/*                                  to be started in units of sec.     */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC INT4
D6ClTmrStart (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo, UINT1 u1TimerType,
              UINT4 u4Duration)
{
    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClTmrStart Return Failure!!\r\n");
        return OSIX_FAILURE;
    }
    switch (u1TimerType)
    {
        case DHCP6_CLNT_TMR_RE_TRANSMISSION:
            if (TmrStart
                (DHCP6_CLNT_TMRLIST_ID,
                 &(pDhcp6ClntIfInfo->ReTransmissionTimer), u1TimerType,
                 u4Duration,
                 pDhcp6ClntIfInfo->u2RetMiliSecRemTime) == TMR_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                                ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                                "D6ClTmrStart: Failed to start Timer "
                                "!! \r\n");
                return OSIX_FAILURE;
            }
            DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "D6ClTmrStart: Retransmission Timer Start.\r\n");

            break;
        case DHCP6_CLNT_TMR_REFRESH_TIMER:
            if (TmrStart
                (DHCP6_CLNT_TMRLIST_ID, &(pDhcp6ClntIfInfo->RefreshTimer),
                 u1TimerType, u4Duration,
                 pDhcp6ClntIfInfo->u2RefMiliSecRemTime) == TMR_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                                ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                                "D6ClTmrStart: Failed to start Timer "
                                "!! \r\n");
                return OSIX_FAILURE;
            }
            DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "D6ClTmrStart: Refresh Timer Start.\r\n");
            break;
        default:
            DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                            ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                            "D6ClTmrStart: Invalid Timer Type !! \r\n");
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : D6ClTmrStopTimer                                   */
/*                                                                      */
/*  Description     : This function is called to stop timer for DHCPv6  */
/*                    Client Task.                                      */
/*                                                                      */
/*  Input(s)        : pDhcp6ClntIfInfo Pointer to Interface             */
/*                  : u1TimerType - Type of timer ot be started.        */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC INT4
D6ClTmrStopTimer (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo, UINT1 u1TimerType)
{
    UINT4               u4RefRemainingTime = 0;
    UINT4               u4RetRemainingTime = 0;

    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClTmrStopTimer Return Failure!!\r\n");
        return OSIX_FAILURE;
    }

    switch (u1TimerType)
    {
        case DHCP6_CLNT_TMR_RE_TRANSMISSION:

            TmrGetRemainingTime (gDhcp6ClntGblInfo.TimerListId,
                                 &(pDhcp6ClntIfInfo->ReTransmissionTimer.
                                   TimerNode), &u4RetRemainingTime);

            if (u4RetRemainingTime != 0)
            {
                if (TmrStop
                    (DHCP6_CLNT_TMRLIST_ID,
                     &(pDhcp6ClntIfInfo->ReTransmissionTimer)) == TMR_FAILURE)
                {
                    DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                                    ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                                    "D6ClTmrStopTimer: Failed to stop Timer "
                                    "!! \r\n");
                    return OSIX_FAILURE;
                }
            }
            DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "D6ClTmrStopTimer: Retransmission Timer Stop.\r\n");
            break;
        case DHCP6_CLNT_TMR_REFRESH_TIMER:
            TmrGetRemainingTime (gDhcp6ClntGblInfo.TimerListId,
                                 &(pDhcp6ClntIfInfo->RefreshTimer.TimerNode),
                                 &u4RefRemainingTime);

            if (u4RefRemainingTime != 0)
            {
                if (TmrStop
                    (DHCP6_CLNT_TMRLIST_ID,
                     &(pDhcp6ClntIfInfo->RefreshTimer)) == TMR_FAILURE)
                {
                    DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                                    ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                                    "D6ClTmrStopTimer: Failed to stop Timer "
                                    "!! \r\n");
                    return OSIX_FAILURE;
                }
            }
            DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC | DHCP6_CLNT_MGMT_TRC,
                            "D6ClTmrStopTimer: Refresh Timer Stop.\r\n");
            break;
        default:
            DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                            ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                            "D6ClTmrStopTimer: Invalid Timer Type!! \r\n");
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : D6ClTmrStopAllTimer                                */
/*                                                                      */
/*  Description     : This function is called to stop all timer running */
/*                    at Interface.                                     */
/*                                                                      */
/*  Input(s)        : pDhcp6ClntIfInfo Pointer to Interface             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC INT4
D6ClTmrStopAllTimer (tDhcp6ClntIfInfo * pDhcp6ClntIfInfo)
{
    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClTmrStopAllTimer Return Failure!!\r\n");
        return OSIX_FAILURE;
    }
    if (D6ClTmrStopTimer (pDhcp6ClntIfInfo, DHCP6_CLNT_TMR_RE_TRANSMISSION) !=
        OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClTmrStopAllTimer: Failed to stop Retransmission"
                        "Timer !!\r\n");
    }
    if (D6ClTmrStopTimer (pDhcp6ClntIfInfo, DHCP6_CLNT_TMR_REFRESH_TIMER) !=
        OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_OS_RESOURCE_TRC,
                        "D6ClTmrStopAllTimer: Failed to stop Refresh"
                        "Timer !!\r\n");
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : D6ClTmrExpHandler                               */
/*                                                                      */
/*  Description     : This function is called from DHCPv6 Main when     */
/*                    Task receives the timer expiry event.             */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC VOID
D6ClTmrExpHandler ()
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (DHCP6_CLNT_TMRLIST_ID)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < DHCP6_CLNT_MAX_TMR_TYPES)
        {
            i2Offset = gDhcp6ClntGblInfo.aTmrDesc[u1TimerId].i2Offset;

            /* Call the registered expired handler function */
            (*(gDhcp6ClntGblInfo.aTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    return;
}

/******************************************************************************
 * Function Name      : D6ClTmrRetransmissionExpired
 *
 * Description        : This routine is called when Retransmission timer running
 *                      on the interface got expired.
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to interface Strcture 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PRIVATE VOID
D6ClTmrRetransmissionExpired (VOID *pArg)
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;
    pDhcp6ClntIfInfo = (tDhcp6ClntIfInfo *) pArg;

    if (pDhcp6ClntIfInfo->b1FirstInfoMessage != OSIX_TRUE)
    {
        pDhcp6ClntIfInfo->b1IsRetansMessage = OSIX_TRUE;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "D6ClTmrRetransmissionExpired: Retransmission Timer Expired.\r\n");
    if (pDhcp6ClntIfInfo->b1IsInterfaceLck == OSIX_TRUE)
    {
        OsixSemGive (pDhcp6ClntIfInfo->SemId);
        return;
    }

    /*Set the Interface specific varibles which will be used while parsing */
    if (D6ClFormInterfaceTxPdu (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClTmrRetransmissionExpired: D6clFormInterfaceTxPdu "
                        " Function Return Failure \r\n");
        return;
    }
    if (pDhcp6ClntIfInfo->b1FirstInfoMessage == OSIX_TRUE)
    {
        pDhcp6ClntIfInfo->b1FirstInfoMessage = OSIX_FALSE;
    }
    return;
}

/******************************************************************************
 * Function Name      : D6ClTmrRefreshExpired
 *
 * Description        : This routine is called when Retransmission timer running
 *                      on the interface got expired.
 *
 * Input(s)           : pDhcp6ClntIfInfo Pointer to interface Strcture 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PRIVATE VOID
D6ClTmrRefreshExpired (VOID *pArg)
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;
    pDhcp6ClntIfInfo = (tDhcp6ClntIfInfo *) pArg;

    pDhcp6ClntIfInfo->b1IsRetansMessage = OSIX_FALSE;
    pDhcp6ClntIfInfo->b1IsInterfaceLck = OSIX_FALSE;
    pDhcp6ClntIfInfo->u1TransType = DHCP6_CLNT_REFRESH_TMR_EXP_REQUEST;
    pDhcp6ClntIfInfo->u2CurrentRetTime = 0;
    pDhcp6ClntIfInfo->u1CurrRetCount = 0;
    pDhcp6ClntIfInfo->u2CurrentRetDelay = 0;

    DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "D6ClTmrRefreshExpired: Refresh Timer Expired.\r\n");

    /*Set the Interface specific varibles which will be used while parsing */
    if (D6ClFormInterfaceTxPdu (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClTmrRetransmissionExpired: D6clFormInterfaceTxPdu"
                        " Function Return Failure \r\n");
        return;
    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6cltmr.c                       */
/*-----------------------------------------------------------------------*/
