/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6clw.c,v 1.12 2016/04/29 09:10:20 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "d6clinc.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntTrapAdminControl
 Input       :  The Indices

                The Object 
                retValFsDhcp6ClntTrapAdminControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntTrapAdminControl (tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsDhcp6ClntTrapAdminControl)
{
    pRetValFsDhcp6ClntTrapAdminControl->pu1_OctetList[0] =
        DHCP6_CLNT_TRAP_CONTROL_OPTION;
    pRetValFsDhcp6ClntTrapAdminControl->i4_Length = DHCP6_CLNT_INDEX_BY_ONE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntDebugTrace
 Input       :  The Indices

                The Object 
                retValFsDhcp6ClntDebugTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntDebugTrace (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsDhcp6ClntDebugTrace)
{
    pRetValFsDhcp6ClntDebugTrace->i4_Length =
        D6ClTrcGetTraceInputValue (pRetValFsDhcp6ClntDebugTrace->
                                   pu1_OctetList, DHCP6_CLNT_TRACE_OPTION);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntSysLogAdminStatus
 Input       :  The Indices

                The Object 
                retValFsDhcp6ClntSysLogAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntSysLogAdminStatus (INT4 *pi4RetValFsDhcp6ClntSysLogAdminStatus)
{
    if (DHCP6_CLNT_SYS_LOG_OPTION == OSIX_TRUE)
    {
        *pi4RetValFsDhcp6ClntSysLogAdminStatus = DHCP6_CLNT_SNMP_SUCCESS;
    }
    else
    {
        *pi4RetValFsDhcp6ClntSysLogAdminStatus = DHCP6_CLNT_SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntSourcePort
 Input       :  The Indices

                The Object 
                retValFsDhcp6ClntSourcePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntListenPort (INT4 *pi4RetValFsDhcp6ClntListenPort)
{
    *pi4RetValFsDhcp6ClntListenPort = DHCP6_CLNT_SOURCE_PORT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntDestinationPort
 Input       :  The Indices

                The Object 
                retValFsDhcp6ClntDestinationPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntTransmitPort (INT4 *pi4RetValFsDhcp6ClntTransmitPort)
{
    *pi4RetValFsDhcp6ClntTransmitPort = DHCP6_CLNT_DEST_PORT;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntTrapAdminControl
 Input       :  The Indices

                The Object 
                setValFsDhcp6ClntTrapAdminControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntTrapAdminControl (tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsDhcp6ClntTrapAdminControl)
{
    DHCP6_CLNT_TRAP_CONTROL_OPTION =
        pSetValFsDhcp6ClntTrapAdminControl->pu1_OctetList[0];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntDebugTrace
 Input       :  The Indices

                The Object 
                setValFsDhcp6ClntDebugTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntDebugTrace (tSNMP_OCTET_STRING_TYPE *
                             pSetValFsDhcp6ClntDebugTrace)
{
    UINT4               u4TrcOption = 0;
    u4TrcOption = D6ClTrcGetTraceOptionValue
        (pSetValFsDhcp6ClntDebugTrace->pu1_OctetList,
         pSetValFsDhcp6ClntDebugTrace->i4_Length);

    if ((STRNCMP (pSetValFsDhcp6ClntDebugTrace->pu1_OctetList,
                  "enable", STRLEN ("enable"))) == 0)
    {
        DHCP6_CLNT_TRACE_OPTION |= u4TrcOption;
    }
    else if ((STRNCMP (pSetValFsDhcp6ClntDebugTrace->pu1_OctetList,
                       "disable", STRLEN ("disable"))) == 0)
    {
        DHCP6_CLNT_TRACE_OPTION &= ~u4TrcOption;
    }
    /* Default case. if nothing is appended, assume critical is done */
    else if ((STRNCMP (pSetValFsDhcp6ClntDebugTrace->pu1_OctetList,
                       "critical", STRLEN ("critical"))) == 0)
    {
        DHCP6_CLNT_TRACE_OPTION = u4TrcOption;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntSysLogAdminStatus
 Input       :  The Indices

                The Object 
                setValFsDhcp6ClntSysLogAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntSysLogAdminStatus (INT4 i4SetValFsDhcp6ClntSysLogAdminStatus)
{
    if (i4SetValFsDhcp6ClntSysLogAdminStatus == DHCP6_CLNT_SNMP_SUCCESS)
    {
        DHCP6_CLNT_SYS_LOG_OPTION = OSIX_TRUE;
    }
    else
    {
        DHCP6_CLNT_SYS_LOG_OPTION = OSIX_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntSourcePort
 Input       :  The Indices

                The Object 
                setValFsDhcp6ClntSourcePort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntListenPort (INT4 i4SetValFsDhcp6ClntListenPort)
{
    D6ClSktDeletePktRcv ();
    DHCP6_CLNT_SOURCE_PORT = (UINT2) i4SetValFsDhcp6ClntListenPort;
    if (D6ClSktCreatePktRcv () == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntDestinationPort
 Input       :  The Indices

                The Object 
                setValFsDhcp6ClntDestinationPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntTransmitPort (INT4 i4SetValFsDhcp6ClntTransmitPort)
{
    DHCP6_CLNT_DEST_PORT = (UINT2) i4SetValFsDhcp6ClntTransmitPort;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntTrapAdminControl
 Input       :  The Indices

                The Object 
                testValFsDhcp6ClntTrapAdminControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntTrapAdminControl (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsDhcp6ClntTrapAdminControl)
{
    if ((UINT4) pTestValFsDhcp6ClntTrapAdminControl->i4_Length > sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "SNMP: Invalid Value\r\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6ClntTrapAdminControl->pu1_OctetList[0] >
        (UINT1) DHCP6_CLNT_TRAP_CONTROL_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "SNMP: Invalid Value\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntDebugTrace
 Input       :  The Indices

                The Object 
                testValFsDhcp6ClntDebugTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntDebugTrace (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsDhcp6ClntDebugTrace)
{
    UINT4               u4TrcOption = 0;
    
    if ((pTestValFsDhcp6ClntDebugTrace->i4_Length >= DHCP6_CLNT_TRC_MAX_SIZE) ||
        (pTestValFsDhcp6ClntDebugTrace->i4_Length < DHCP6_CLNT_TRC_MIN_SIZE))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "Invalid Trace Input length \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u4TrcOption = D6ClTrcGetTraceOptionValue
        (pTestValFsDhcp6ClntDebugTrace->pu1_OctetList,
         pTestValFsDhcp6ClntDebugTrace->i4_Length);

    if (u4TrcOption == DHCP6_CLNT_INVALID_TRC)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "Invalid Trace Input string \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntSysLogAdminStatus
 Input       :  The Indices

                The Object 
                testValFsDhcp6ClntSysLogAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntSysLogAdminStatus (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsDhcp6ClntSysLogAdminStatus)
{
    if ((i4TestValFsDhcp6ClntSysLogAdminStatus != DHCP6_CLNT_SNMP_SUCCESS) &&
        (i4TestValFsDhcp6ClntSysLogAdminStatus != DHCP6_CLNT_SNMP_FAILURE))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "Invalid Value Received  \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntSourcePort
 Input       :  The Indices

                The Object 
                testValFsDhcp6ClntSourcePort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntListenPort (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsDhcp6ClntListenPort)
{
    if ((i4TestValFsDhcp6ClntListenPort >
         DHCP6_CLNT_MAX_SOURCE_PORT_NUMBER) ||
        (i4TestValFsDhcp6ClntListenPort < DHCP6_CLNT_MIN_SOURCE_PORT_NUMBER))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "Invalid Value Input \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntDestinationPort
 Input       :  The Indices

                The Object 
                testValFsDhcp6ClntDestinationPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntTransmitPort (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsDhcp6ClntTransmitPort)
{
    if ((i4TestValFsDhcp6ClntTransmitPort >
         DHCP6_CLNT_MAX_DEST_PORT_NUMBER) ||
        (i4TestValFsDhcp6ClntTransmitPort < DHCP6_CLNT_MIN_DEST_PORT_NUMBER))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "Invalid Value Input\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6ClntTrapAdminControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6ClntTrapAdminControl (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6ClntDebugTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6ClntDebugTrace (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6ClntSysLogAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6ClntSysLogAdminStatus (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6ClntSourcePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6ClntListenPort (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6ClntDestinationPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6ClntTransmitPort (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6ClntIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDhcp6ClntIfTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6ClntIfTable (INT4 i4FsDhcp6ClntIfIndex)
{
    if (D6ClIfValidateInterfaceEntry (i4FsDhcp6ClntIfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDhcp6ClntIfTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6ClntIfTable (INT4 *pi4FsDhcp6ClntIfIndex)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = D6ClIfGetFirstInterfaceInfo ();

    if (pInterfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsDhcp6ClntIfIndex = pInterfaceNode->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDhcp6ClntIfTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
                nextFsDhcp6ClntIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6ClntIfTable (INT4 i4FsDhcp6ClntIfIndex,
                                   INT4 *pi4NextFsDhcp6ClntIfIndex)
{
    if (i4FsDhcp6ClntIfIndex < 0)
    {
        return SNMP_FAILURE;
    }

    if (D6ClIfGetNextInterfaceIndex (i4FsDhcp6ClntIfIndex,
                                     pi4NextFsDhcp6ClntIfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
INT1
nmhGetFsDhcp6ClntIfSrvAddress (INT4 i4FsDhcp6ClntIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsDhcp6ClntIfSrvAddress)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    if (D6ClIfValidateInterfaceEntry (i4FsDhcp6ClntIfIndex) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo != NULL)
    {
        pRetValFsDhcp6ClntIfSrvAddress->i4_Length = sizeof (tIp6Addr);
        MEMCPY (pRetValFsDhcp6ClntIfSrvAddress->pu1_OctetList,
                (UINT1 *) &pClntInterfaceInfo->CurrentSrvAddr,
                pRetValFsDhcp6ClntIfSrvAddress->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfDuidType
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfDuidType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfDuidType (INT4 i4FsDhcp6ClntIfIndex,
                             INT4 *pi4RetValFsDhcp6ClntIfDuidType)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    if (D6ClIfValidateInterfaceEntry (i4FsDhcp6ClntIfIndex) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfDuidType = pClntInterfaceInfo->u1ClientIdType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfDuid
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfDuid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfDuid (INT4 i4FsDhcp6ClntIfIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6ClntIfDuid)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    if (D6ClIfValidateInterfaceEntry (i4FsDhcp6ClntIfIndex) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        pRetValFsDhcp6ClntIfDuid->i4_Length =
            pClntInterfaceInfo->u1ClientIdLength;
        MEMCPY (pRetValFsDhcp6ClntIfDuid->pu1_OctetList,
                pClntInterfaceInfo->au1ClientId,
                pRetValFsDhcp6ClntIfDuid->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfDuidIfIndex
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfDuidIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfDuidIfIndex (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 *pi4RetValFsDhcp6ClntIfDuidIfIndex)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfDuidIfIndex = pClntInterfaceInfo->u4IfDuidIndex;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfMaxRetCount
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfMaxRetCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfMaxRetCount (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 *pi4RetValFsDhcp6ClntIfMaxRetCount)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfMaxRetCount = pClntInterfaceInfo->u1MaxRetCount;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfMaxRetDelay
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfMaxRetDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfMaxRetDelay (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 *pi4RetValFsDhcp6ClntIfMaxRetDelay)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfMaxRetDelay = pClntInterfaceInfo->u2MaxRetDelay;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfMaxRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfMaxRetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfMaxRetTime (INT4 i4FsDhcp6ClntIfIndex,
                               INT4 *pi4RetValFsDhcp6ClntIfMaxRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfMaxRetTime = pClntInterfaceInfo->u2MaxRetTime;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntInitRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntInitRetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfInitRetTime (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 *pi4RetValFsDhcp6ClntIfInitRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfInitRetTime = pClntInterfaceInfo->u2InitRetTime;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfCurrRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfCurrRetTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfCurrRetTime (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 *pi4RetValFsDhcp6ClntIfCurrRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfCurrRetTime =
            pClntInterfaceInfo->u2CurrentRetTime;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfMinRefreshTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfMinRefreshTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfMinRefreshTime (INT4 i4FsDhcp6ClntIfIndex,
                                   UINT4 *pu4RetValFsDhcp6ClntIfMinRefreshTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfMinRefreshTime =
            pClntInterfaceInfo->u4MinRefreshTime;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfCurrRefreshTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfCurrRefreshTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsDhcp6ClntIfCurrRefreshTime (INT4 i4FsDhcp6ClntIfIndex,
                                    UINT4
                                    *pu4RetValFsDhcp6ClntIfCurrRefreshTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfCurrRefreshTime =
            pClntInterfaceInfo->u4RefreshTime;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfRealmName
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfRealmName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfRealmName (INT4 i4FsDhcp6ClntIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsDhcp6ClntIfRealmName)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        pRetValFsDhcp6ClntIfRealmName->i4_Length =
            pClntInterfaceInfo->u1RealmLength;
        MEMCPY (pRetValFsDhcp6ClntIfRealmName->pu1_OctetList,
                pClntInterfaceInfo->au1Realm,
                pRetValFsDhcp6ClntIfRealmName->i4_Length);
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfKey
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfKey (INT4 i4FsDhcp6ClntIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsDhcp6ClntIfKey)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        pRetValFsDhcp6ClntIfKey->i4_Length = pClntInterfaceInfo->u1KeyLength;
        MEMCPY (pRetValFsDhcp6ClntIfKey->pu1_OctetList,
                pClntInterfaceInfo->au1Key, pRetValFsDhcp6ClntIfKey->i4_Length);
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfKeyId
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfKeyId (INT4 i4FsDhcp6ClntIfIndex,
                          UINT4 *pu4RetValFsDhcp6ClntIfKeyId)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfKeyId = pClntInterfaceInfo->u4KeyIdentifier;
        return SNMP_SUCCESS;
    }

    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfInformOut
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfInformOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfInformOut (INT4 i4FsDhcp6ClntIfIndex,
                              UINT4 *pu4RetValFsDhcp6ClntIfInformOut)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfInformOut = pClntInterfaceInfo->u4InformOut;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfReplyIn
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfReplyIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfReplyIn (INT4 i4FsDhcp6ClntIfIndex,
                            UINT4 *pu4RetValFsDhcp6ClntIfReplyIn)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfReplyIn = pClntInterfaceInfo->u4ReplyIn;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfInvalidPktIn
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfInvalidPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfInvalidPktIn (INT4 i4FsDhcp6ClntIfIndex,
                                 UINT4 *pu4RetValFsDhcp6ClntIfInvalidPktIn)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfInvalidPktIn =
            pClntInterfaceInfo->u4InvalidPktIn;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfHmacFailCount
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfHmacFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfHmacFailCount (INT4 i4FsDhcp6ClntIfIndex,
                                  UINT4 *pu4RetValFsDhcp6ClntIfHmacFailCount)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pu4RetValFsDhcp6ClntIfHmacFailCount =
            pClntInterfaceInfo->u4HmacFailCount;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfCounterRest
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfCounterRest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfCounterRest (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 *pi4RetValFsDhcp6ClntIfCounterRest)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfCounterRest = DHCP6_CLNT_SNMP_FAILURE;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntIfRowStatus
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                retValFsDhcp6ClntIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntIfRowStatus (INT4 i4FsDhcp6ClntIfIndex,
                              INT4 *pi4RetValFsDhcp6ClntIfRowStatus)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo != NULL)
    {
        *pi4RetValFsDhcp6ClntIfRowStatus = pClntInterfaceInfo->u1IfRowStatus;
        return SNMP_SUCCESS;
    }
    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                    "SNMP: Interface Does Not exist!\r\n");

    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfDuidType
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfDuidType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfDuidType (INT4 i4FsDhcp6ClntIfIndex,
                             INT4 i4SetValFsDhcp6ClntIfDuidType)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    /*Set the DUID Type and update the DUID value */
    pClntInterfaceInfo->u1ClientIdType = (UINT1) i4SetValFsDhcp6ClntIfDuidType;

    if (D6ClUtlUpdateClientId (pClntInterfaceInfo) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfDuidIfIndex
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfDuidIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfDuidIfIndex (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 i4SetValFsDhcp6ClntIfDuidIfIndex)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    /*Set the DUID Type and update the DUID value */
    pClntInterfaceInfo->u4IfDuidIndex = i4SetValFsDhcp6ClntIfDuidIfIndex;

    if (D6ClUtlUpdateClientId (pClntInterfaceInfo) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfMaxRetCount
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfMaxRetCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfMaxRetCount (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 i4SetValFsDhcp6ClntIfMaxRetCount)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u1MaxRetCount =
        (UINT1) i4SetValFsDhcp6ClntIfMaxRetCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfMaxRetDelay
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfMaxRetDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfMaxRetDelay (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 i4SetValFsDhcp6ClntIfMaxRetDelay)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u2MaxRetDelay =
        (UINT2) i4SetValFsDhcp6ClntIfMaxRetDelay;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfMaxRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfMaxRetTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfMaxRetTime (INT4 i4FsDhcp6ClntIfIndex,
                               INT4 i4SetValFsDhcp6ClntIfMaxRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u2MaxRetTime = (UINT2) i4SetValFsDhcp6ClntIfMaxRetTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntInitRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntInitRetTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfInitRetTime (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 i4SetValFsDhcp6ClntIfInitRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u2InitRetTime =
        (UINT2) i4SetValFsDhcp6ClntIfInitRetTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfMinRefreshTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfMinRefreshTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfMinRefreshTime (INT4 i4FsDhcp6ClntIfIndex,
                                   UINT4 u4SetValFsDhcp6ClntIfMinRefreshTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u4MinRefreshTime = u4SetValFsDhcp6ClntIfMinRefreshTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfRealmName
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfRealmName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfRealmName (INT4 i4FsDhcp6ClntIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsDhcp6ClntIfRealmName)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u1RealmLength =
        (UINT1) pSetValFsDhcp6ClntIfRealmName->i4_Length;
    if (pClntInterfaceInfo->u1RealmLength <= DHCP6_CLNT_REALM_SIZE_MAX)
    {
        MEMSET (pClntInterfaceInfo->au1Realm, 0,
                pClntInterfaceInfo->u1RealmLength);
        MEMCPY (pClntInterfaceInfo->au1Realm,
                pSetValFsDhcp6ClntIfRealmName->pu1_OctetList,
                pClntInterfaceInfo->u1RealmLength);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfKey
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfKey (INT4 i4FsDhcp6ClntIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsDhcp6ClntIfKey)
{
    UINT1               u1Length = 0;
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u1KeyLength =
        (UINT1) pSetValFsDhcp6ClntIfKey->i4_Length;

    MEMSET (pClntInterfaceInfo->au1Key, 0, DHCP6_CLNT_KEY_MAX_ARRAY_SIZE);
    u1Length = pClntInterfaceInfo->u1KeyLength;
    if (u1Length <= (UINT1) DHCP6_CLNT_KEY_SIZE_MAX)
    {
        MEMCPY (pClntInterfaceInfo->au1Key,
                pSetValFsDhcp6ClntIfKey->pu1_OctetList,
                pClntInterfaceInfo->u1KeyLength);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfKeyId
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfKeyId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfKeyId (INT4 i4FsDhcp6ClntIfIndex,
                          UINT4 u4SetValFsDhcp6ClntIfKeyId)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    pClntInterfaceInfo->u4KeyIdentifier = u4SetValFsDhcp6ClntIfKeyId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfCounterRest
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfCounterRest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfCounterRest (INT4 i4FsDhcp6ClntIfIndex,
                                INT4 i4SetValFsDhcp6ClntIfCounterRest)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    if (i4SetValFsDhcp6ClntIfCounterRest == DHCP6_CLNT_SNMP_SUCCESS)
    {
        pClntInterfaceInfo->u4InformOut = 0;
        pClntInterfaceInfo->u4ReplyIn = 0;
        pClntInterfaceInfo->u4HmacFailCount = 0;
        pClntInterfaceInfo->u4InvalidPktIn = 0;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6ClntIfRowStatus
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                setValFsDhcp6ClntIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6ClntIfRowStatus (INT4 i4FsDhcp6ClntIfIndex,
                              INT4 i4SetValFsDhcp6ClntIfRowStatus)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    tNetIpv6IfInfo      netIp6IfInfo;
    tIp6Addr            Ip6Addr;
    INT4                i4RetVal = NETIPV6_FAILURE;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&netIp6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    if (D6ClIfValidateInterfaceIndex (i4FsDhcp6ClntIfIndex) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Invalid Interface Number. \r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4SetValFsDhcp6ClntIfRowStatus)
    {
        case CREATE_AND_GO:
            i4RetVal = NetIpv6GetIfInfo (i4FsDhcp6ClntIfIndex, &netIp6IfInfo);
            if (i4RetVal == NETIPV6_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: NetIpv6GetIfInfo Return Failure!\r\n");
                return (SNMP_FAILURE);
            }
            /*Check Row can be created or not */
            if ((D6ClIfCreateInterface (i4FsDhcp6ClntIfIndex, &netIp6IfInfo)) ==
                OSIX_FAILURE)

            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: D6ClIfCreateInterface Return Failure!\r\n");
                return SNMP_FAILURE;
            }
            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
            if (pClntInterfaceInfo == NULL)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: Interface Does Not exist!\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
                return SNMP_FAILURE;
            }
            pClntInterfaceInfo->u1IfRowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetVal = NetIpv6GetIfInfo (i4FsDhcp6ClntIfIndex, &netIp6IfInfo);
            if (i4RetVal == NETIPV6_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: NetIpv6GetIfInfo Return Failure!\r\n");
                return (SNMP_FAILURE);
            }
            /*Check Row can be created or not */
            if ((D6ClIfCreateInterface (i4FsDhcp6ClntIfIndex,
                                        &netIp6IfInfo)) == OSIX_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: D6ClIfCreateInterface Return Failure!\r\n");
                return SNMP_FAILURE;
            }
            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
            if (pClntInterfaceInfo == NULL)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: Interface Does Not exist!\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
                return SNMP_FAILURE;
            }
            pClntInterfaceInfo->u1IfRowStatus = NOT_READY;

            break;

        case ACTIVE:
            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

            if (pClntInterfaceInfo == NULL)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: Interface Does Not exist!\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
                return SNMP_FAILURE;
            }

            pClntInterfaceInfo->u1IfRowStatus = ACTIVE;
            if ((pClntInterfaceInfo->u4RetSecRemTime != 0) ||
                (pClntInterfaceInfo->u4RefSecRemTime != 0) ||
                (pClntInterfaceInfo->u2RetMiliSecRemTime != 0) ||
                (pClntInterfaceInfo->u2RefMiliSecRemTime != 0))
            {
                D6ClUtlStartTimeWithRem (i4FsDhcp6ClntIfIndex);
            }
            else
            {
                if (NetIpv6GetIfInfo
                    (i4FsDhcp6ClntIfIndex, &netIp6IfInfo) == NETIPV6_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                if ((MEMCMP
                     (&netIp6IfInfo.Ip6Addr, &Ip6Addr, sizeof (tIp6Addr))) != 0)
                {
                    if (D6ClFormUserTxPdu (pClntInterfaceInfo) == OSIX_TRUE)
                    {
                        return SNMP_SUCCESS;
                    }
                }
            }

            break;

        case DESTROY:
            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
            if (pClntInterfaceInfo == NULL)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: Interface Does Not exist!\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
                return SNMP_FAILURE;
            }

            D6ClIfDeleteInterface (i4FsDhcp6ClntIfIndex);
            break;

        case NOT_IN_SERVICE:

            i4RetVal = NetIpv6GetIfInfo (i4FsDhcp6ClntIfIndex, &netIp6IfInfo);
            if (i4RetVal == NETIPV6_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: NetIpv6GetIfInfoReturn Failure !\r\n");
                return (SNMP_FAILURE);
            }

            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
            if (pClntInterfaceInfo == NULL)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_MGMT_TRC,
                                "SNMP: Interface Does Not exist!\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
                return SNMP_FAILURE;
            }
            /*Stop Refresh and Retransmission Timer on the interface */
            if (D6ClUtlCalculateRemTime (i4FsDhcp6ClntIfIndex) == OSIX_FALSE)
            {
                return SNMP_FAILURE;
            }
            pClntInterfaceInfo->u1IfRowStatus = (UINT1) NOT_IN_SERVICE;
            break;
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfDuidType
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfDuidType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfDuidType (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6ClntIfIndex,
                                INT4 i4TestValFsDhcp6ClntIfDuidType)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDhcp6ClntIfDuidType !=
         DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME)
        && (i4TestValFsDhcp6ClntIfDuidType !=
            DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID)
        && (i4TestValFsDhcp6ClntIfDuidType != DHCP6_CLNT_LINK_LAYER_ADD))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfDuidIfIndex
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfDuidIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfDuidIfIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6ClntIfIndex,
                                   INT4 i4TestValFsDhcp6ClntIfDuidIfIndex)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    if (i4TestValFsDhcp6ClntIfDuidIfIndex < DHCP6_CLNT_MIN_IFACES
        || i4TestValFsDhcp6ClntIfDuidIfIndex > MAX_D6CL_CLNT_IFACES)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pClntInterfaceInfo->u1ClientIdType !=
        DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID)
    {
        if (D6ClPortGetInterfaceAddressIdentifier
            (i4TestValFsDhcp6ClntIfDuidIfIndex, &NetIpv6IfInfo) == OSIX_FAILURE)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "nmhTestv2FsDhcp6ClntIfDuidIfIndex :D6ClPortGetInterfaceAddressIdentifier "
                            "Function Return Failure \r\n");
            CLI_SET_ERR (DHCP6_CLNT_CLI_INV_IF_TYPE);
            return SNMP_FAILURE;
        }
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfMaxRetCount
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfMaxRetCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfMaxRetCount (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6ClntIfIndex,
                                   INT4 i4TestValFsDhcp6ClntIfMaxRetCount)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDhcp6ClntIfMaxRetCount <
         DHCP6_CLNT_MIN_RETRANS_COUNT)
        || (i4TestValFsDhcp6ClntIfMaxRetCount > DHCP6_CLNT_MAX_RETRANS_COUNT))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfMaxRetDelay
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfMaxRetDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfMaxRetDelay (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6ClntIfIndex,
                                   INT4 i4TestValFsDhcp6ClntIfMaxRetDelay)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6ClntIfMaxRetDelay < DHCP6_CLNT_MIN_RETRANS_DELAY
        || i4TestValFsDhcp6ClntIfMaxRetDelay > DHCP6_CLNT_MAX_RETRANS_DELAY)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfMaxRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfMaxRetTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfMaxRetTime (UINT4 *pu4ErrorCode,
                                  INT4 i4FsDhcp6ClntIfIndex,
                                  INT4 i4TestValFsDhcp6ClntIfMaxRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6ClntIfMaxRetTime < DHCP6_CLNT_MIN_RETRANS_TIME ||
        i4TestValFsDhcp6ClntIfMaxRetTime > DHCP6_CLNT_MAX_RETRANS_TIME)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntInitRetTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntInitRetTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfInitRetTime (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6ClntIfIndex,
                                   INT4 i4TestValFsDhcp6ClntIfInitRetTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }
    if (i4TestValFsDhcp6ClntIfInitRetTime <
        DHCP6_CLNT_INIT_MIN_RETRANS_TIME
        || i4TestValFsDhcp6ClntIfInitRetTime > DHCP6_CLNT_INIT_MAX_RETRANS_TIME)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfMinRefreshTime
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfMinRefreshTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfMinRefreshTime (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDhcp6ClntIfIndex,
                                      UINT4
                                      u4TestValFsDhcp6ClntIfMinRefreshTime)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsDhcp6ClntIfMinRefreshTime < DHCP6_CLNT_MIN_REFRESH_TIME) ||
        (u4TestValFsDhcp6ClntIfMinRefreshTime > DHCP6_CLNT_MAX_REFRESH_TIME))
    {
        CLI_SET_ERR (DHCP6_CLNT_CLI_INFO_REFRESH_ERR);
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfRealmName
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfRealmName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfRealmName (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6ClntIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsDhcp6ClntIfRealmName)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6ClntIfRealmName->i4_Length <=
        DHCP6_CLNT_MIN_REALM_SIZE
        || pTestValFsDhcp6ClntIfRealmName->i4_Length >=
        DHCP6_CLNT_MAX_REALM_SIZE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6ClntIfRealmName->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfKey
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfKey (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6ClntIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsDhcp6ClntIfKey)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6ClntIfKey->i4_Length < DHCP6_CLNT_MIN_KEY_SIZE ||
        pTestValFsDhcp6ClntIfKey->i4_Length > DHCP6_CLNT_MAX_KEY_SIZE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6ClntIfKey->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfKeyId
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfKeyId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfKeyId (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6ClntIfIndex,
                             UINT4 u4TestValFsDhcp6ClntIfKeyId)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if (u4TestValFsDhcp6ClntIfKeyId < DHCP6_CLNT_MIN_KEY_ID)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfCounterRest
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfCounterRest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfCounterRest (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6ClntIfIndex,
                                   INT4 i4TestValFsDhcp6ClntIfCounterRest)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;

    pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
    if (pClntInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Does Not exist!\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (DHCP6_CLNT_CLI_ERR_IP6);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6ClntIfCounterRest != DHCP6_CLNT_SNMP_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pClntInterfaceInfo->u1IfRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Row Status is Active!\r\n");
        CLI_SET_ERR (DHCP6_CLNT_CLI_ACT_ROW_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6ClntIfRowStatus
 Input       :  The Indices
                FsDhcp6ClntIfIndex

                The Object 
                testValFsDhcp6ClntIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6ClntIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsDhcp6ClntIfIndex,
                                 INT4 i4TestValFsDhcp6ClntIfRowStatus)
{
    tDhcp6ClntIfInfo   *pClntInterfaceInfo = NULL;
    tNetIpv6IfInfo      netIp6IfInfo;

    MEMSET (&netIp6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    if (D6ClIfValidateInterfaceIndex (i4FsDhcp6ClntIfIndex) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Interface Number wrong !\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDhcp6ClntIfRowStatus < ACTIVE)
        || (i4TestValFsDhcp6ClntIfRowStatus > DESTROY))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Input value is wrong!\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDhcp6ClntIfRowStatus == DESTROY)
    {
        pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

        if (pClntInterfaceInfo == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (DHCP6_CLNT_CLI_INV_ENTRY);
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }

    switch ((UINT1) i4TestValFsDhcp6ClntIfRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);
            if (pClntInterfaceInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC,
                                "SNMP: nmhTestv2FsDhcp6ClntIfRowStatus Fails,"
                                " Interface already exists\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            if (NetIpv6GetIfInfo (i4FsDhcp6ClntIfIndex, &netIp6IfInfo)
                == NETIPV6_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC,
                                "SNMP: nmhTestv2FsDhcp6ClntIfRowStatus Fails\r\n");
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;

        case ACTIVE:

            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

            if (pClntInterfaceInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (DHCP6_CLNT_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pClntInterfaceInfo->u1IfRowStatus == (UINT1) ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            else if ((pClntInterfaceInfo->u1IfRowStatus == (UINT1)
                      NOT_IN_SERVICE) ||
                     (pClntInterfaceInfo->u1IfRowStatus == (UINT1) NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (DHCP6_CLNT_CLI_INV_STATUS);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:

            pClntInterfaceInfo = D6ClIfGetInterfaceInfo (i4FsDhcp6ClntIfIndex);

            if (pClntInterfaceInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                DHCP6_CLNT_TRC (DHCP6_CLNT_MGMT_TRC,
                                "SNMP: nmhTestv2FsDhcp6ClntIfRowStatus Fails,"
                                " Interface already exists\r\n");
                CLI_SET_ERR (DHCP6_CLNT_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
            break;
        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (DHCP6_CLNT_CLI_INV_STATUS);
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6ClntIfTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6ClntIfTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6ClntOptionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDhcp6ClntOptionTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
                FsDhcp6ClntOptionType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6ClntOptionTable (INT4 i4FsDhcp6ClntIfIndex,
                                                INT4 i4FsDhcp6ClntOptionType)
{
    tDhcp6ClntOptionInfo *pDhcp6TempClntOptionNode = NULL;

    pDhcp6TempClntOptionNode =
        D6ClOptGetGlobOptionInfo (i4FsDhcp6ClntIfIndex,
                                  (UINT2) i4FsDhcp6ClntOptionType);
    if (pDhcp6TempClntOptionNode == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDhcp6ClntOptionTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
                FsDhcp6ClntOptionType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6ClntOptionTable (INT4 *pi4FsDhcp6ClntIfIndex,
                                        INT4 *pi4FsDhcp6ClntOptionType)
{
    tDhcp6ClntOptionInfo *pDhcp6NextClntOptionNode = NULL;
    pDhcp6NextClntOptionNode = D6ClOptGetFirstGlobOptionInfo ();

    if (pDhcp6NextClntOptionNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsDhcp6ClntIfIndex = pDhcp6NextClntOptionNode->u4IfIndex;
    *pi4FsDhcp6ClntOptionType = pDhcp6NextClntOptionNode->u2Type;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDhcp6ClntOptionTable
 Input       :  The Indices
                FsDhcp6ClntIfIndex
                nextFsDhcp6ClntIfIndex
                FsDhcp6ClntOptionType
                nextFsDhcp6ClntOptionType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6ClntOptionTable (INT4 i4FsDhcp6ClntIfIndex,
                                       INT4 *pi4NextFsDhcp6ClntIfIndex,
                                       INT4 i4FsDhcp6ClntOptionType,
                                       INT4 *pi4NextFsDhcp6ClntOptionType)
{

    tDhcp6ClntOptionInfo *pDhcp6NextClntOptionNode = NULL;

    pDhcp6NextClntOptionNode = D6ClOptGetNextGlobOptionInfo
        (i4FsDhcp6ClntIfIndex, i4FsDhcp6ClntOptionType);

    if (pDhcp6NextClntOptionNode == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsDhcp6ClntIfIndex = pDhcp6NextClntOptionNode->u4IfIndex;
        *pi4NextFsDhcp6ClntOptionType = pDhcp6NextClntOptionNode->u2Type;
        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntOptionLength
 Input       :  The Indices
                FsDhcp6ClntIfIndex
                FsDhcp6ClntOptionType

                The Object 
                retValFsDhcp6ClntOptionLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntOptionLength (INT4 i4FsDhcp6ClntIfIndex,
                               INT4 i4FsDhcp6ClntOptionType,
                               INT4 *pi4RetValFsDhcp6ClntOptionLength)
{
    tDhcp6ClntOptionInfo *pDhcp6ClntOptionNode = NULL;

    pDhcp6ClntOptionNode = D6ClOptGetGlobOptionInfo
        (i4FsDhcp6ClntIfIndex, (UINT2) i4FsDhcp6ClntOptionType);
    if (pDhcp6ClntOptionNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Option Does Not exist!\r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6ClntOptionLength = pDhcp6ClntOptionNode->u2Length;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6ClntOptionValue
 Input       :  The Indices
                FsDhcp6ClntIfIndex
                FsDhcp6ClntOptionType

                The Object 
                retValFsDhcp6ClntOptionValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6ClntOptionValue (INT4 i4FsDhcp6ClntIfIndex,
                              INT4 i4FsDhcp6ClntOptionType,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsDhcp6ClntOptionValue)
{
    tDhcp6ClntOptionInfo *pDhcp6ClntOptionNode = NULL;

    pDhcp6ClntOptionNode = D6ClOptGetGlobOptionInfo
        (i4FsDhcp6ClntIfIndex, (UINT2) i4FsDhcp6ClntOptionType);
    if (pDhcp6ClntOptionNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_MGMT_TRC,
                        "SNMP: Option Does Not exist!\r\n");
        return SNMP_FAILURE;
    }
    if (pDhcp6ClntOptionNode->u2Length != 0)
    {
        pRetValFsDhcp6ClntOptionValue->i4_Length =
            pDhcp6ClntOptionNode->u2Length;
        MEMCPY (pRetValFsDhcp6ClntOptionValue->pu1_OctetList,
                pDhcp6ClntOptionNode->pu1Value,
                pRetValFsDhcp6ClntOptionValue->i4_Length);
    }

    return SNMP_SUCCESS;
}
