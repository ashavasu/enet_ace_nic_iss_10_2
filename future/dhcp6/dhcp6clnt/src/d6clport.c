/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the functionality of the parser sub
 *              module.
 * ************************************************************************/

#include "d6clinc.h"

/*****************************************************************************/
/* Function Name      : D6ClPortGetHadwareType                                */
/*                                                                           */
/* Description        : This function is the porting function and return the */
/*                      Hadware type value.                                  */
/*                                                                           */
/* Input(s)           : pu2HadwareType - Pointer to hadware type             */
/*                                                                           */
/* Output(s)          : pu2HadwareType - Pointer to hadware type             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClPortGetHadwareType (UINT2 *pu2HadwareType)
{
    *pu2HadwareType = (UINT2) DHCP6_CLNT_GET_HADWARE_TYPE;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : D6ClPortGetEnterpriseNumber                           */
/*                                                                           */
/* Description        : This function will return the Enterprise Number      */
/*                      value of hadware.                                   */
/*                                                                           */
/* Input(s)           : pu4EnterpriseNumber - Pointer to Enterprise number   */
/*                                                                           */
/* Output(s)          : pu4EnterpriseNumber - Pointer to Enterprise number   */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClPortGetEnterpriseNumber (UINT4 *pu4EnterpriseNumber)
{
    *pu4EnterpriseNumber = DHCP6_CLNT_GET_ENTERPRISE_NUMBER;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : D6ClPortGetIdentifier                                */
/*                                                                           */
/* Description        : This function wil return the Dhcp Identifier value.  */
/*                                                                           */
/* Input(s)           : pu1IdentifierLength - Length of DhcpIdentifier       */
/*                      pau1Identifier - DhcpIdentifier Value                */
/*                                                                           */
/* Output(s)          : pu1IdentifierLength - Length of DhcpIdentifier       */
/*                      pau1Identifier - DhcpIdentifier Value                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClPortGetIdentifier (UINT1 *pu1IdentifierLength, UINT1 *pau1Identifier)
{
    *pu1IdentifierLength = (UINT1) DHCP6_CLNT_GET_IDENTIFIER_LENGTH;
    if (*pu1IdentifierLength > DHCP6_CLNT_MAX_IDENTIFIER_SIZE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClPortGetIdentifier: Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pau1Identifier, DHCP6_CLNT_GET_IDENTIFIER_VALUE,
            DHCP6_CLNT_GET_IDENTIFIER_LENGTH);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : D6ClPortIsRelayRowCreated                               */
/*                                                                           */
/* Description        : This function returns OSIX_TRUE when Interface is    */
/*                      already created in the Relay Mode.                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClPortIsRelayRowCreated (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* Check if the Interface is created in Relay mode or not */
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : D6ClPortIsServerRowCreated                           */
/*                                                                           */
/* Description        : This function returns OSIX_TRUE when Interface is    */
/*                      already created in the Server Mode.                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClPortIsServerRowCreated (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* Check if the Interface is created in Server mode or not */
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClPortGetInterfaceAddressIdentifier                         */
/*                                                                           */
/* Description  : This function gets the Ip-Address of the interface.        */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pNetIpv6IfInfo - pointer to Strcture.                       */
/*                                                                           */
/* Output       : au1IpV6Addr                                                */
/*                                                                           */
/* Returns      : DHCP6_SUCCESS or DHCP6_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
D6ClPortGetInterfaceAddressIdentifier (UINT4 u4IfIndex,
                                       tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    if (NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo) == NETIPV6_FAILURE)

    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClPortGetInterfaceAddressIdentifier: NetIpv6GetIfInfo "
                        "Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClPortInGlobalAddr                                        */
/*                                                                           */
/* Description  : The function return the Source Ip Addres of interface      */
/*                                                                           */
/* Input        : u4IfIndex  -   Interface Id                               */
/*                pDstAddr - Destination IP address                        */
/*                pSrcAddr - Source IP Address                               */
/*                                                                           */
/* Output       : IP Address                                                  */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6ClPortInGlobalAddr (UINT4 u4IfIndex, tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tNetIpv6AddrInfo    netIp6FirstAddrInfo;
    tNetIpv6AddrInfo    netIp6NextAddrInfo;
    tIp6Addr           *pAddr = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    INT4                i4Status, i4Count;
    i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);
    if (i4Status != NETIPV6_FAILURE)

    {

        do

        {
            if (Ip6AddrMatch
                (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                 netIp6FirstAddrInfo.u4PrefixLength) == TRUE)

            {
                pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }

            else

            {
                pAddr = &netIp6FirstAddrInfo.Ip6Addr;
            }
            i4Status =
                NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
                                      &netIp6NextAddrInfo);
            if (i4Status != NETIPV6_FAILURE)
                MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                        sizeof (tNetIpv6AddrInfo));
        }
        while (i4Status != NETIPV6_FAILURE);
        MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
        return OSIX_SUCCESS;
    }

    else

    {
        for (i4Count = 0; i4Count < IP6_MAX_LOGICAL_IFACES; i4Count++)

        {
            i4Status = NetIpv6GetFirstIfAddr (i4Count, &netIp6FirstAddrInfo);
            if (i4Status != NETIPV6_FAILURE)

            {

                do

                {
                    if (Ip6AddrMatch
                        (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                         netIp6FirstAddrInfo.u4PrefixLength) == TRUE)

                    {
                        pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                        MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                        return OSIX_SUCCESS;
                    }

                    else

                    {
                        pAddr = &netIp6FirstAddrInfo.Ip6Addr;
                    }
                    i4Status =
                        NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
                                              &netIp6NextAddrInfo);
                    if (i4Status != NETIPV6_FAILURE)

                    {
                        MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                                sizeof (tNetIpv6AddrInfo));
                    }
                }
                while (i4Status != NETIPV6_FAILURE);
                MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clport.c                       */
/*-----------------------------------------------------------------------*/
