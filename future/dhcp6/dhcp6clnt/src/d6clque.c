/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the procedure related to
 *              processing of QMsgs and Enquing QMsg from external
 *              module to DHCPv6 Client Task
 * $Id: d6clque.c,v 1.11 2014/05/19 13:12:53 siva Exp $
 * ************************************************************************/
#ifdef GNU_CC
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#endif

#include "d6clinc.h"
#include "fsdh6ccli.h"
#include "ip.h"

PRIVATE VOID        D6ClQueProcessIfStausChange (UINT4 u4IfIndex,
                                                 UINT4 u4IfState);
PRIVATE VOID        D6ClQueIfDelete (UINT4 u4Index);
PRIVATE VOID        D6ClQueIfUp (UINT4 u4Index);
PRIVATE VOID        D6ClQueCIfDown (UINT4 u4Index);

/************************************************************************/
/*  Function Name   : D6ClQuePktHandler                             */
/*                                                                      */
/*  Description     : This function receives and processes the received */
/*                    PDUSon DHCPv6 Client task                         */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Returns         : None                                             */
/************************************************************************/

PUBLIC VOID
D6ClQuePktHandler ()
{
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    tDhcp6ClntUdpHeader Udp6Dhcp6sParams;
    tIp6Addr            Ip6Addr;
    tDhcp6ClntIfInfo   *pInterfaceInfo = NULL;
    UINT4               u4IfIndex;
#ifdef SLI_WANTED
    struct cmsghdr      CmsgInfo;
#endif
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4OptEnable = 1;
    struct iovec        Iov;
    UINT1               au1Cmsg[DHCP6_MAX_MTU];
#endif
    UINT4               u4BufLen;
#ifdef SLI_WANTED
    INT4                i4AddrLen = sizeof (PeerAddr);
#endif
    INT4                i4DataLen;
    INT4                i4RetVal = 0;

    SET_ADDR_UNSPECIFIED (Ip6Addr);

    MEMSET (&Udp6Dhcp6sParams, 0, sizeof (tDhcp6ClntUdpHeader));
    Ip6AddrCopy ((tIp6Addr *) (VOID *) PeerAddr.sin6_addr.s6_addr, &Ip6Addr);
    PeerAddr.sin6_port = 0;

    u4BufLen = DHCP6_MAX_MTU;

#ifdef BSDCOMP_SLI_WANTED
    if (setsockopt (DHCP6_CLNT_SOCKET_ID, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                    &(i4OptEnable), sizeof (INT4)) < 0)
    {
        return;
    }
#endif

#ifdef SLI_WANTED
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;
    Ip6MsgHdr.msg_controllen = DHCP6_MAX_MTU;

    /* Call recvfrom to receive the packet */
    if ((i4DataLen =
         recvfrom (DHCP6_CLNT_SOCKET_ID, (char *) DHCP6_CLNT_PDU,
                   u4BufLen, 0, (struct sockaddr *) &PeerAddr, &i4AddrLen)) < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC, "recvfrom failed\r\n");
    }
    else
    {
        /* Get the interface index from which the packet is received */
        if (recvmsg (DHCP6_CLNT_SOCKET_ID, &Ip6MsgHdr, 0) < 0)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC, "Recv Msg Failed\r\n");
            return;
        }
    }
#else
    /* Wait to receive data on the socket */
    MEMSET (DHCP6_CLNT_PDU, 0, sizeof (DHCP6_CLNT_PDU));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    Ip6MsgHdr.msg_name = (void *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (PeerAddr);
    Iov.iov_base = DHCP6_CLNT_PDU;
    Iov.iov_len = sizeof (DHCP6_CLNT_PDU);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (VOID *) &au1Cmsg;
    Ip6MsgHdr.msg_controllen = DHCP6_MAX_MTU;

    if ((i4DataLen = recvmsg (DHCP6_CLNT_SOCKET_ID, &Ip6MsgHdr, 0)) < 0)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQuePktHandler :recvmsg error\r\n");
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC, "Recv Msg Failed\r\n");
    }
    UNUSED_PARAM (u4BufLen);
#endif
    pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC, "Pkt Info Failed\r\n");
        return;
    }

    /* store the required values from SOCKET LAYER to Params Structure */

#ifdef BSDCOMP_SLI_WANTED
    NetIpv4GetCfaIfIndexFromPort ((UINT4) pIn6Pktinfo->ipi6_ifindex,
                                  &u4IfIndex);
#else
    u4IfIndex = (UINT4) pIn6Pktinfo->ipi6_ifindex;
#endif

    Udp6Dhcp6sParams.u4Index = u4IfIndex;
    Udp6Dhcp6sParams.u2DstPort = OSIX_NTOHS (PeerAddr.sin6_port);
    Udp6Dhcp6sParams.u2SrcPort = DHCP6_CLNT_SOURCE_PORT;
    Udp6Dhcp6sParams.u4Len = (UINT4) i4DataLen;

    i4RetVal = (INT4) sizeof (UINT1);

    getsockopt (DHCP6_CLNT_SOCKET_ID, IPPROTO_IPV6,
                IPV6_UNICAST_HOPS, &Udp6Dhcp6sParams.i4Hlim, &i4RetVal);

    Ip6AddrCopy (&Udp6Dhcp6sParams.srcAddr,
                 (tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr);
    Ip6AddrCopy (&Udp6Dhcp6sParams.dstAddr,
                 (tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr);

    pInterfaceInfo = D6ClIfGetInterfaceInfo (Udp6Dhcp6sParams.u4Index);

    if (pInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQuePktHandler: DHCP-PDU Received on invalid port.\r\n");
        return;
    }

    if (i4DataLen < 0)
    {
        return;
    }
    D6ClProcessPacket (&Udp6Dhcp6sParams, DHCP6_CLNT_PDU,
                       Udp6Dhcp6sParams.u4Index, i4DataLen);
}

/************************************************************************/
/*  Function Name   : D6ClQueueCfgHandler                             */
/*                                                                      */
/*  Description     : This function receives and processe the           */
/*                    configuration messages received on theDHCPv6      */
/*                    Client task.                                       */
/*                                                                      */
/*  Input(s)        : pQMsg - Message information received.             */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
D6ClQueueCfgHandler (tDhcp6ClntQMsg * pQMsg)
{
    switch (pQMsg->u4MsgType)
    {
        case DHCP6_CLNT_IP6_IF_STAUS_CHG_MSG:
            D6ClQueProcessIfStausChange (pQMsg->u4IfIndex, pQMsg->u4IfState);
            break;
        default:
            DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClQueueCfgHandler: Invalid Event \n");
    }
    return;
}

/************************************************************************/
/*  Function Name   : D6ClQueProcessIfStausChange                        */
/*                                                                      */
/*  Description     : This function Handles the Interface Status change */
/*                    information message received from IPv6 Module.    */
/*                                                                      */
/*  Input(s)        : u4IfIndex - Interface Index.                      */
/*                    u4IntState- u4IfState                             */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/
PRIVATE VOID
D6ClQueProcessIfStausChange (UINT4 u4IfIndex, UINT4 u4IfState)
{
    switch (u4IfState)
    {
        case NETIPV6_IF_DELETE:
            D6ClQueIfDelete (u4IfIndex);
            break;

        case NETIPV6_IF_UP:
            D6ClQueIfUp (u4IfIndex);
            break;

        case NETIPV6_IF_DOWN:
            D6ClQueCIfDown (u4IfIndex);
            break;
        default:
            break;
    }
    return;
}

/************************************************************************/
/*  Function Name   : D6ClQueIfDelete                                    */
/*                                                                      */
/*  Description     : This function Handles the Deletes Interface       */
/*                    information message received from IPv6 Module.    */
/*                                                                      */
/*  Input(s)        : u4IfIndex - Interface Index.                      */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/

PRIVATE VOID
D6ClQueIfDelete (UINT4 u4IfIndex)
{
    /*Check If Interface is created the delete the interface.
     *When Interface does not exists then return */

    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;
    UINT4               u4ErrorCode = 0;
    if (u4IfIndex > MAX_D6CL_CLNT_IFACES)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueIfDelete: Interface Number wrong. \r\n");
        return;
    }
    pDhcp6ClntIfInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueIfDelete: Interface not created for DHCP6C. \r\n");
        return;
    }
    if (D6ClTmrStopAllTimer (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        return;
    }

    if (nmhTestv2FsDhcp6ClntIfRowStatus
        (&u4ErrorCode, u4IfIndex, DESTROY) != SNMP_SUCCESS)
    {
        return;
    }

    if (nmhSetFsDhcp6ClntIfRowStatus (u4IfIndex, DESTROY) != SNMP_SUCCESS)
    {
        return;
    }
    return;
}

/************************************************************************/
/*  Function Name   : D6ClQueIfUp                                        */
/*                                                                      */
/*  Description     : This function Handles the Interface Up            */
/*                    information message received from IPv6 Module.    */
/*                                                                      */
/*  Input(s)        : u4IfIndex - Interface Index.                      */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/

PRIVATE VOID
D6ClQueIfUp (UINT4 u4IfIndex)
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;

    if (u4IfIndex > MAX_D6CL_CLNT_IFACES)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueIfUp: Interface Number wrong. \r\n");
        return;
    }
    pDhcp6ClntIfInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueIfUp: Interface not created for DHCP6C. \r\n");
        return;
    }
    /* when interface is already Up then Return */
    if (pDhcp6ClntIfInfo->u4OperStatus == OSIX_TRUE)
    {
        return;
    }
    else
    {
        pDhcp6ClntIfInfo->u4OperStatus = OSIX_TRUE;
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClQueIfUp: Oper Status set UP. \r\n");
        return;
    }
}

/************************************************************************/
/*  Function Name   : D6ClQueCIfDown                                      */
/*                                                                      */
/*  Description     : This function Handles the Interface Down          */
/*                    information message received from IPv6 Module.    */
/*                                                                      */
/*  Input(s)        : u4IfIndex - Interface Index.                      */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/

PRIVATE VOID
D6ClQueCIfDown (UINT4 u4IfIndex)
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;
    if (u4IfIndex > MAX_D6CL_CLNT_IFACES)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueCIfDown: Interface Number wrong. \r\n");
        return;
    }
    pDhcp6ClntIfInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueCIfDown: Interface not created for DHCP6C. \r\n");
        return;
    }
    if (pDhcp6ClntIfInfo->u4OperStatus == OSIX_FALSE)
    {
        return;
    }
    else
    {
        /*Stop All the Timers if running Running on the interface */
        if (D6ClTmrStopAllTimer (pDhcp6ClntIfInfo) == OSIX_FAILURE)
        {
            return;
        }
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClQueCIfDown: Oper Status set Down. \r\n");
        pDhcp6ClntIfInfo->u4OperStatus = OSIX_FALSE;

    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clque.c                       */
/*-----------------------------------------------------------------------*/
