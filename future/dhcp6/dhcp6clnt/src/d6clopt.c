/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $ $
 *
 * Description: This file contains external interface functions for 
 *              Option Table.
 * ************************************************************************/

#include "d6clinc.h"

PRIVATE INT4 D6ClOptTableCmp PROTO ((tRBElem * pE1, tRBElem * pE2));
PRIVATE VOID D6ClOptionDrainTable PROTO ((VOID));
PRIVATE INT4 D6ClOptAddOptionInfoInterface PROTO ((tDhcp6ClntOptionInfo *));
PRIVATE INT4 D6ClOptDelOptionInfoInterface PROTO ((tDhcp6ClntOptionInfo *));

/***************************************************************************
 * FUNCTION NAME    : D6ClOptCreateTable
 *
 * DESCRIPTION      : This function creats the option table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6ClOptCreateTable (VOID)
{
    /* Create Global RB-Trees for Option Table */
    DHCP6_CLNT_OPTION_INFO_TABLE =
        D6ClUtlRBTreeCreateEmbedded (FSAP_OFFSETOF
                                     (tDhcp6ClntOptionInfo, GblRBLink),
                                     D6ClOptTableCmp);

    if (DHCP6_CLNT_OPTION_INFO_TABLE == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_INIT_SHUT_TRC | DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_OS_RESOURCE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptionCreateTable: Unable to create global"
                        " RB-Tree for Option Table. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6ClOptDeleteTable
 *
 * DESCRIPTION      : This function Deletes the option table.
 *                    
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6ClOptDeleteTable (VOID)
{
    if (DHCP6_CLNT_OPTION_INFO_TABLE != NULL)
    {
        RBTreeDestroy (DHCP6_CLNT_OPTION_INFO_TABLE,
                       (tRBKeyFreeFn) D6ClUtlFreeEntryFn,
                       DHCP6_CLNT_OPTION_POOL);

        DHCP6_CLNT_OPTION_INFO_TABLE = NULL;
    }

    if (DHCP6_CLNT_OPTION_INFO_TABLE != NULL)
    {
        D6ClOptionDrainTable ();
        RBTreeDestroy (DHCP6_CLNT_OPTION_INFO_TABLE, NULL, 0);
        DHCP6_CLNT_OPTION_INFO_TABLE = NULL;
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6ClOptionDrainTable
 *
 * DESCRIPTION      : This function deletes all the option nodes
 *                    associated with the option table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PRIVATE VOID
D6ClOptionDrainTable (VOID)
{
    RBTreeDrain (DHCP6_CLNT_OPTION_INFO_TABLE,
                 (tRBKeyFreeFn) D6ClUtlFreeEntryFn, 0);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6ClOptTableCmp                                    */
/*                                                                           */
/* Description  : This s the compare function used for RBTree.               */
/*                It compares the two RBTree Nodes.                          */
/*                                                                           */
/* Input        : *pE1 - pointer to MD Node1                                 */
/*                *pE2 - pointer to MD Node2                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : If not Equal, returns 0, returns 1 if pE1 > pE2            */
/*                else returns -1                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
D6ClOptTableCmp (tRBElem * pE1, tRBElem * pE2)
{
    tDhcp6ClntOptionInfo *pOptionEntryA = NULL;
    tDhcp6ClntOptionInfo *pOptionEntryB = NULL;

    pOptionEntryA = (tDhcp6ClntOptionInfo *) pE1;
    pOptionEntryB = (tDhcp6ClntOptionInfo *) pE2;

    if (pOptionEntryA->u4IfIndex != pOptionEntryB->u4IfIndex)
    {
        if (pOptionEntryA->u4IfIndex < pOptionEntryB->u4IfIndex)
        {
            return DHCP6_CLNT_RB_LESS;
        }

        else
        {
            return DHCP6_CLNT_RB_GREATER;
        }
    }

    if (pOptionEntryA->u2Type != pOptionEntryB->u2Type)
    {
        if (pOptionEntryA->u2Type < pOptionEntryB->u2Type)
        {
            return DHCP6_CLNT_RB_LESS;
        }

        else
        {
            return DHCP6_CLNT_RB_GREATER;
        }
    }

    return DHCP6_CLNT_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6ClOptCreateNode 
 *
 * DESCRIPTION      : This function creates a option table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6ClntOptionInfo * - Pointer to the created option node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6ClntOptionInfo *
D6ClOptCreateNode (VOID)
{
    tDhcp6ClntOptionInfo *pOptionInfo = NULL;

    if ((pOptionInfo =
         (tDhcp6ClntOptionInfo *) MemAllocMemBlk (DHCP6_CLNT_OPTION_TABLE_POOL))
        == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClOptCreateNode: Memory Allocation Failure. \r\n");
        return NULL;
    }

    MEMSET (pOptionInfo, 0x00, sizeof (tDhcp6ClntOptionInfo));
    return pOptionInfo;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptUpdateValueMemory 
 *                                                                          
 *    DESCRIPTION      : This function will allocate the memory for thr buffer
 *                       from the Buddy Pool.
 *
 *    INPUT            : pDhcp6ClntOptionInfo - Node Pointer 
 *                       u4Length - Buffer Length
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClOptUpdateValueMemory (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo,
                          UINT2 u2Length)
{
    if (pDhcp6ClntOptionInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pDhcp6ClntOptionInfo->u2Length != 0)
        && (pDhcp6ClntOptionInfo->pu1Value != NULL))
    {
        /*Free the Memory of Buddy Pool */
        MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                      (UINT1 *) pDhcp6ClntOptionInfo->pu1Value);

    }
    /*Allocate the Memory */
    pDhcp6ClntOptionInfo->pu1Value =
        (UINT1 *) MemBuddyAlloc ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                 (u2Length));
    if (pDhcp6ClntOptionInfo->pu1Value == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
                        DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClOptUpdateValueMemory: Memory Allocation Failure. \r\n");
        pDhcp6ClntOptionInfo->u2Length = 0;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptAddOptionNode 
 *                                                                          
 *    DESCRIPTION      : This function will add the Option Nodes in to global 
 *                       Option RBTRee.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClOptAddOptionNode (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo)
{
    if (pDhcp6ClntOptionInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    if (RBTreeAdd (DHCP6_CLNT_OPTION_INFO_TABLE, pDhcp6ClntOptionInfo) ==
        RB_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptAddOptionNode: Node Addition at RBTree "
                        "Failed. \r\n");
        return OSIX_FAILURE;
    }
    if (D6ClOptAddOptionInfoInterface (pDhcp6ClntOptionInfo) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptAddOptionInfoInterface: Node Addition at Interface "
                        "Failed. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptDelOptionNode 
 *                                                                          
 *    DESCRIPTION      : This function will Delete the Option Nodes in to global 
 *                       Option RBTRee.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PUBLIC INT4
D6ClOptDelOptionNode (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo)
{
    /*Delete From the Interface Info */
    if (D6ClOptDelOptionInfoInterface (pDhcp6ClntOptionInfo) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptDelOptionNode: Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    if ((pDhcp6ClntOptionInfo->u2Length != 0) &&
        (pDhcp6ClntOptionInfo->pu1Value != NULL))
    {
        MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                      (UINT1 *) pDhcp6ClntOptionInfo->pu1Value);
    }
    RBTreeRem (DHCP6_CLNT_OPTION_INFO_TABLE, pDhcp6ClntOptionInfo);
    MemReleaseMemBlock (DHCP6_CLNT_OPTION_TABLE_POOL,
                        (UINT1 *) pDhcp6ClntOptionInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptDelOptionInfoInterface 
 *                                                                          
 *    DESCRIPTION      : This function will delate the Option Nodes from the 
 *                       interface List..
 *
 *    INPUT            : pDhcp6ClntOptionInfo - Pointer to Option Info
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
D6ClOptDelOptionInfoInterface (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    if (pDhcp6ClntOptionInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptDelOptionInfoInterface: Option Info not exists. \r\n");
        return OSIX_FAILURE;
    }
    pInterfaceNode = D6ClIfGetInterfaceInfo (pDhcp6ClntOptionInfo->u4IfIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptDelOptionInfoInterface: Interface not created. \r\n");
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (&(pInterfaceNode->OptionTableSll),
                    &(pDhcp6ClntOptionInfo->OptSllLink));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptAddOptionInfoInterface 
 *                                                                          
 *    DESCRIPTION      : This function will add the Option Node from the
 *                       interface List.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for
 *                       pDhcp6ClntOptionInfo - Pointer to Option Info
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
D6ClOptAddOptionInfoInterface (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = D6ClIfGetInterfaceInfo (pDhcp6ClntOptionInfo->u4IfIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptAddOptionInfoInterface: Interface not created. \r\n");
        return OSIX_FAILURE;
    }

    TMO_SLL_Init_Node (&(pDhcp6ClntOptionInfo->OptSllLink));
    TMO_SLL_Add (&(pInterfaceNode->OptionTableSll),
                 (tTMO_SLL_NODE *) & (pDhcp6ClntOptionInfo->OptSllLink));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptGetGlobOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Option
 *                       entry from the global structure, Option Table.
 *
 *    INPUT            : u4IfIndex - Index of Interface Table for 
 *                       which the entry is required.
 *                       u2Type - Index of Option
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Option Entry`
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntOptionInfo *
D6ClOptGetGlobOptionInfo (UINT4 u4IfIndex, UINT2 u2Type)
{
    tDhcp6ClntOptionInfo OptionInfo;
    tDhcp6ClntOptionInfo *pOptionNode = NULL;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptGetGlobOptionInfo: Interface not created. \r\n");
        return NULL;
    }

    MEMSET (&OptionInfo, 0, DHCP6_CLNT_OPTION_INFO_SIZE);

    /* Get Interface entry, based on u4InterfaceId and u2Type */
    OptionInfo.u2Type = u2Type;
    OptionInfo.u4IfIndex = u4IfIndex;
    pOptionNode =
        RBTreeGet (DHCP6_CLNT_OPTION_INFO_TABLE, (tRBElem *) & OptionInfo);
    return pOptionNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptGetFirstGlobOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Option
 *                       for First entry from the global Option Table.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Option  Entry`
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntOptionInfo *
D6ClOptGetFirstGlobOptionInfo ()
{
    tDhcp6ClntOptionInfo OptionInfo;
    tDhcp6ClntOptionInfo *pOptionNode = NULL;
    MEMSET (&OptionInfo, 0, DHCP6_CLNT_OPTION_INFO_SIZE);

    /* Get Interface entry, based on u4InterfaceId and u2Type */
    pOptionNode = RBTreeGetFirst (DHCP6_CLNT_OPTION_INFO_TABLE);
    return pOptionNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptGetNextGlobOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Next
 *                       Option entry from the global structure,
 *                       Option Table.
 *
 *    INPUT            : u4InterfaceId - Index of Interface Table for 
 *                       which the entry is required.
 *                       u2Type - Index of Option
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Interface Entry`
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntOptionInfo *
D6ClOptGetNextGlobOptionInfo (UINT4 u4IfIndex, UINT2 u2Type)
{
    tDhcp6ClntOptionInfo OptionInfo;
    tDhcp6ClntOptionInfo *pOptionNode = NULL;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptGetNextGlobOptionInfo: Interface not created. \r\n");
        return NULL;
    }

    MEMSET (&OptionInfo, 0, DHCP6_CLNT_OPTION_INFO_SIZE);

    /* Get Interface entry, based on u4InterfaceId and u2Type */
    OptionInfo.u2Type = u2Type;
    OptionInfo.u4IfIndex = u4IfIndex;

    pOptionNode =
        RBTreeGetNext (DHCP6_CLNT_OPTION_INFO_TABLE, (tRBElem *) & OptionInfo,
                       NULL);
    return pOptionNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptGetFirstInterOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the First Option
 *                       entry from the Interface List.
 *
 *    INPUT            : u4InterfaceId - Index of Interface  
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Option Entry`
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntOptionInfo *
D6ClOptGetFirstInterOptionInfo (UINT4 u4IfIndex)
{
    tDhcp6ClntOptionInfo *pOptionNode = NULL;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptGetFirstInterOptionInfo: Interface not created. \r\n");
        return NULL;
    }

    pOptionNode = (tDhcp6ClntOptionInfo *) TMO_SLL_First
        (&(pInterfaceNode->OptionTableSll));
    return pOptionNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptInitOptionList 
 *                                                                          
 *    DESCRIPTION      : This function will initiliase the List for the Option
 *                       and called when interface is created.
 *
 *    INPUT            : pClntInterfaceInfo - Pointer to Interface Info
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/

PUBLIC VOID
D6ClOptInitOptionList (tDhcp6ClntIfInfo * pClntInterfaceInfo)
{
    TMO_SLL_Init (&(pClntInterfaceInfo->OptionTableSll));
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptGetNextInterOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the next Option
 *                       entry from the interface Option Table.
 *
 *    INPUT            : u4InterfaceId - Index of Interface
 *                       pPreOptionNode - Pointer to previous node
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Option Entry
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntOptionInfo *
D6ClOptGetNextInterOptionInfo (tDhcp6ClntOptionInfo * pPreOptionNode,
                               UINT4 u4IfIndex)
{
    tDhcp6ClntOptionInfo *pOptionNode = NULL;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    if (pPreOptionNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptGetNextInterOptionInfo: Option Info not exists. \r\n");
        return NULL;
    }

    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptGetNextInterOptionInfo: Interface not created. \r\n");
        return NULL;
    }
    pOptionNode = (tDhcp6ClntOptionInfo *)
        TMO_SLL_Next (&(pInterfaceNode->OptionTableSll),
                      &(pPreOptionNode->OptSllLink));

    return pOptionNode;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClOptGetInterOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Option
 *                       entry from the Interface List.
 *
 *    INPUT            : u4InterfaceId - Index of Interface  
 *                       u2Type - Opting Type 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : pointer to Option Entry`
 *    
 *                                                                          
 ****************************************************************************/

PUBLIC tDhcp6ClntOptionInfo *
D6ClOptGetInterOptionInfo (UINT4 u4IfIndex, UINT2 u2Type)
{
    tDhcp6ClntOptionInfo *pOptionNode = NULL;
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;

    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClOptGetInterOptionInfo: Interface not created. \r\n");
        return NULL;
    }
    pOptionNode = D6ClOptGetFirstInterOptionInfo (u4IfIndex);
    while (pOptionNode != NULL)
    {
        if (pOptionNode->u2Type == u2Type)
        {
            break;
        }
        else
        {
            pOptionNode =
                D6ClOptGetNextInterOptionInfo (pOptionNode, u4IfIndex);
        }
    }
    return pOptionNode;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clopt.c                       */
/*-----------------------------------------------------------------------*/
