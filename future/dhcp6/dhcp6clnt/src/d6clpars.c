/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6clpars.c,v 1.12 2012/06/26 13:03:20 siva Exp $
 * Description: This file contains the functionality of the parser sub
 *              module.
 * ************************************************************************/

#include "d6clinc.h"
PRIVATE INT4        D6ClParsValidateOption
PROTO ((tDhcp6ClntPktInfo *, tDhcp6ClntOptionInfo *, UINT1 *));
PRIVATE INT4        D6Cl6ParsePduInfo
PROTO ((UINT1 *, tDhcp6ClntPktInfo *, UINT4, UINT4 *));
PRIVATE INT4 D6ClParsValidateMessageType PROTO ((UINT1 *, UINT4, UINT4));
PRIVATE INT4 D6ClParsRxValidatePduHdr PROTO ((tDhcp6ClntUdpHeader *));
PRIVATE INT4 D6ClParsAddOption PROTO ((UINT1 *, UINT4, UINT4));
PRIVATE INT4        D6ClParsePacket
PROTO ((UINT1 *, tDhcp6ClntUdpHeader *, UINT4, UINT4));
PRIVATE INT4 D6ClValidatePacket PROTO ((UINT1 *, UINT4, tDhcp6ClntUdpHeader *));
PRIVATE INT4 D6ClParsValidateReplyInfo PROTO ((tDhcp6ClntPktInfo *));
PRIVATE INT4        D6ClAddOptionInterface
PROTO ((tDhcp6ClntOptionInfo *, UINT1 *, UINT4));
PRIVATE INT4 D6ClParsPassOptionInfo PROTO ((tDhcp6ClntIfInfo *));
PRIVATE INT4        D6ClParsPassValidOptionInfo
PROTO ((tDhcp6ClntOptionInfo *, UINT2));

/*****************************************************************************
 * Name               : D6ClProcessPacket 
 *
 * Description        : Receives the incoming frame from lower layer and 
 *                      validates the DHCP-PDU Header and  
 *
 * Input(s)           : pDhcp6ClntUdpHeaderInfo - Pointer to UDP Header Info
 *                      pBuf - Pointer to the received buffer
 *                      u4IfIndex - Index of Interface on which DHCP-PDU was 
 *                      received.
 *                      u4ByteCount - Length of Message
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCES/OSIX_FAILURE
 *****************************************************************************/

PUBLIC INT4
D6ClProcessPacket (tDhcp6ClntUdpHeader * pDhcp6ClntUdpHeaderInfo,
                   UINT1 *pBuf, UINT4 u4IfIndex, INT4 u4ByteCount)
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;    /* Pointer to Port specific info */

    DHCP6_CLNT_TRC_ARG1 (DHCP6_CLNT_DATA_PATH_TRC,
                         "D6ClProcessPacket: "
                         "DHCP PDU received on IfIndex = [%d]\r\n", u4IfIndex);

    DHCP6_CLNT_PKT_DUMP (DHCP6_CLNT_DUMP_TRC, pBuf, u4ByteCount,
                         "D6ClProcessPacket: "
                         "Dumping DHCPv6 received from lower layer...\r\n");

    /*Reset the PDU info to all zeros */

    pDhcp6ClntIfInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pDhcp6ClntIfInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_DATA_PATH_TRC | DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClProcessPacket: Interface Does Not exists. \r\n");
        return OSIX_FAILURE;
    }

    /*Check If Interface Status is Up or Not */
    /*Check If Interface is Assigned to IP Address or Not */
    /*Check If Row Status is Active or Not */
    /*when all the above condition is TRUE then only Parse the PDU otherwise
     * discard the received message */

    if (D6ClUtlValidateIfPropery (pDhcp6ClntIfInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClUtlValidateIfPropery: Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    if (D6ClParsePacket (pBuf, pDhcp6ClntUdpHeaderInfo,
                         u4ByteCount, u4IfIndex) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClProcessPacket: D6ClParsePacket returned Failure.\r\n");
        MEMSET (DHCP6_CLNT_PDU, 0, DHCP6_CLNT_MAX_PDU_SIZE);
        return OSIX_FAILURE;
    }
    Ip6AddrCopy (&pDhcp6ClntIfInfo->CurrentSrvAddr,
                 (tIp6Addr *) & pDhcp6ClntUdpHeaderInfo->srcAddr);
    MEMSET (DHCP6_CLNT_PDU, 0, DHCP6_CLNT_MAX_PDU_SIZE);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClParsePacket 
 *
 * Description        : This Function will Parse the incoming frame and 
 *                      validates the DHCP-PDU Header and pass the information
 *                      to Application.
 *
 * Input(s)           : pu1RcvBuf - Pointer to Message
 *                      pDhcp6ClntUdpHeaderInfo - pointer to UDP Header
 *                      u4ByteCount - Length of received message
 *                      u4IfIndex - Interface Index
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
D6ClParsePacket (UINT1 *pu1RcvBuf,
                 tDhcp6ClntUdpHeader * pDhcp6ClntUdpHeaderInfo,
                 UINT4 u4ByteCount, UINT4 u4IfIndex)
{
    tDhcp6ClntPktInfo  *pReplyPktInfo = NULL;
    tDhcp6ClntIfInfo   *pClntInfoTxInterfaceInfo = NULL;
    UINT1              *pOrigBuff = pu1RcvBuf;
    tDhcp6CInvalidMsgRxdTrap Dhcp6CInvalidMsgRxdTrap;
    tDhcp6CHmacFailMsgRxdTrap Dhcp6CHmacFailMsgRxdTrap;
    UINT4               u4PduLength = 0;
    UINT4               u4LengthValue = 0;
    UINT4               u4AuthBufSize = 0;
    UINT4               u4RetRemainingTime = 0;
    UINT4               u4RetSecRemTime = 0;
    UINT1               au1AuthDigest[DHCP6_CLNT_ALGO_DIGEST_SIZE];

    MEMSET (&Dhcp6CHmacFailMsgRxdTrap, 0, sizeof (tDhcp6CHmacFailMsgRxdTrap));
    MEMSET (&Dhcp6CInvalidMsgRxdTrap, 0, sizeof (tDhcp6CInvalidMsgRxdTrap));
    MEMSET (au1AuthDigest, 0, DHCP6_CLNT_ALGO_DIGEST_SIZE);

    pClntInfoTxInterfaceInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    TmrGetRemainingTime (gDhcp6ClntGblInfo.TimerListId,
                         &(pClntInfoTxInterfaceInfo->ReTransmissionTimer.
                           TimerNode), &u4RetRemainingTime);

    if (D6ClTmrStopAllTimer (pClntInfoTxInterfaceInfo) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsePacket: D6clTmrStopAllTimer Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    if (D6ClValidatePacket
        (pu1RcvBuf, pClntInfoTxInterfaceInfo->u4TransactionId,
         pDhcp6ClntUdpHeaderInfo) == OSIX_FAILURE)
    {
        Dhcp6CInvalidMsgRxdTrap.u4IfIndex = u4IfIndex;
        D6ClTrapSnmpSend (&Dhcp6CInvalidMsgRxdTrap,
                          DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_VAL);
        pClntInfoTxInterfaceInfo->u4InvalidPktIn++;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsePacket: D6ClValidatePacket"
                        " Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    if (pClntInfoTxInterfaceInfo->u4LastTransId ==
        (pClntInfoTxInterfaceInfo->u4TransactionId))
    {
        pClntInfoTxInterfaceInfo->b1IsInfoExists = OSIX_TRUE;
    }

    u4AuthBufSize = u4ByteCount;
    u4ByteCount = u4ByteCount - DHCP6_CLNT_FOUR_BYTE_LENGTH;
    u4LengthValue = u4ByteCount;
    pu1RcvBuf = pu1RcvBuf + DHCP6_CLNT_FOUR_BYTE_LENGTH;

    pReplyPktInfo = (tDhcp6ClntPktInfo *) MemAllocMemBlk
		(DHCP6_CLNT_PKT_INFO_POOL);
    if (pReplyPktInfo == NULL)
    {
	DHCP6_CLNT_TRC (DHCP6_CLNT_CONTROL_PLANE_TRC |
			DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
			"D6ClParsePacket: Memory Allocation Failure. \r\n");
        printf ("\n D6ClParsePacket: Memory Allocation Failure. \r\n");
	return OSIX_FAILURE;
    }
    MEMSET (pReplyPktInfo, 0, sizeof (tDhcp6ClntPktInfo));

    pReplyPktInfo->pDhcp6ClntIfInfo = pClntInfoTxInterfaceInfo;

    if (D6Cl6ParsePduInfo
        (pu1RcvBuf, pReplyPktInfo, u4ByteCount, &u4PduLength) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsePacket: D6cl6ParsePduInfo"
                        " Function Return Failure. \r\n");
	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
        return OSIX_FAILURE;
    }
    u4ByteCount = u4ByteCount - u4PduLength;

    if (u4ByteCount != 0)
    {
        Dhcp6CInvalidMsgRxdTrap.u4IfIndex = u4IfIndex;
        D6ClTrapSnmpSend (&Dhcp6CInvalidMsgRxdTrap,
                          DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_VAL);
        pClntInfoTxInterfaceInfo->u4InvalidPktIn++;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "Invalid length Message Received. \r\n");
	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
        return OSIX_FAILURE;
    }

    if (D6ClParsValidateReplyInfo (pReplyPktInfo) == OSIX_FAILURE)
    {
        Dhcp6CInvalidMsgRxdTrap.u4IfIndex = u4IfIndex;
        D6ClTrapSnmpSend (&Dhcp6CInvalidMsgRxdTrap,
                          DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_VAL);
        pClntInfoTxInterfaceInfo->u4InvalidPktIn++;
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsePacket Return Failure. \r\n");
	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
        return OSIX_FAILURE;
    }
    /*Check Status code and initiate again the infomration request message */
    if ((pReplyPktInfo->u2StatusCode == DHCP6_CLNT_STATUS_CODE_USE_MULTICAST)
        || (pReplyPktInfo->u2StatusCode ==
            DHCP6_CLNT_STATUS_CODE_UNSPECFIC_VALUE))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsePacket: Wrong Status Code value received.\r\n");

        /*Start the Retransmittion Timer when Remaining value is non zero */
        if (u4RetRemainingTime != 0)
        {
            u4RetRemainingTime =
                DHCP6_CLNT_CONVERT_TIME_TICKS_TO_MSEC (u4RetRemainingTime);
            pClntInfoTxInterfaceInfo->u2RetMiliSecRemTime =
                (UINT2) (u4RetRemainingTime % DHCP6_CLNT_TMR_COVERT_MSEC);

            u4RetSecRemTime = u4RetRemainingTime / DHCP6_CLNT_TMR_COVERT_SEC;
            D6ClTmrStart
                (pClntInfoTxInterfaceInfo, DHCP6_CLNT_TMR_RE_TRANSMISSION,
                 u4RetSecRemTime);
            pClntInfoTxInterfaceInfo->u2RetMiliSecRemTime = 0;
	    MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
            return OSIX_FAILURE;
        }

        /*Retransmit again the Information Request message and Retuen Failure */
        D6ClFormInterfaceTxPdu (pClntInfoTxInterfaceInfo);
	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
        return OSIX_FAILURE;
    }
    else if (pReplyPktInfo->u2StatusCode == 0)
    {
        /*Authenticate the received message when Authentication TLV received */
        if (pReplyPktInfo->b1IsAuthOptReceived == OSIX_TRUE)
        {
            /*Call the HMAC-MD5 Auth API */
            D6ClAuthValidateOption (pOrigBuff, u4AuthBufSize,
                                    pClntInfoTxInterfaceInfo->au1Key,
                                    pClntInfoTxInterfaceInfo->u1KeyLength,
                                    au1AuthDigest);

            if (MEMCMP (au1AuthDigest,
                        pReplyPktInfo->au1AuthValue,
                        DHCP6_CLNT_ALGO_DIGEST_SIZE) != 0)
            {
                Dhcp6CHmacFailMsgRxdTrap.u4IfIndex = u4IfIndex;
                pClntInfoTxInterfaceInfo->u4HmacFailCount +=
                    DHCP6_CLNT_INCR_ONE;
                D6ClTrapSnmpSend (&Dhcp6CHmacFailMsgRxdTrap,
                                  DHCP6_CLNT_RCVD_HMAC_AUTH_FAIL_TRAP_VAL);
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsePacket: Authenication Failure. \r\n");
		MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
                return OSIX_FAILURE;
            }
        }
        pClntInfoTxInterfaceInfo->u1TransType = 0;
        pClntInfoTxInterfaceInfo->u1PreferanceValue =
            pReplyPktInfo->u1PreferanceVal;
        pClntInfoTxInterfaceInfo->b1IsInfoExists = OSIX_FALSE;
        pClntInfoTxInterfaceInfo->TimeStamp = 0;
        pClntInfoTxInterfaceInfo->u4LastTransId =
            (pClntInfoTxInterfaceInfo->u4TransactionId);
        /*Save the received configuration information in the Interace Database */
        if (D6ClParsAddOption (pu1RcvBuf, u4IfIndex, u4LengthValue) ==
            OSIX_FAILURE)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClParsePacket: D6clParsAddOptionInInterface Failure. \r\n");
	    MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
            return OSIX_FAILURE;
        }
        if ((pClntInfoTxInterfaceInfo->u2CurrentApp != 0) &&
            (pClntInfoTxInterfaceInfo->b1IsInterfaceLck) == OSIX_TRUE)
        {
            OsixSemGive (pClntInfoTxInterfaceInfo->SemId);
	    MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
            return OSIX_SUCCESS;
        }
        else
        {
            D6ClParsPassOptionInfo (pClntInfoTxInterfaceInfo);
        }
        pClntInfoTxInterfaceInfo->u2CurrentRetTime = 0;
        pClntInfoTxInterfaceInfo->u2CurrentRetDelay = 0;
        pClntInfoTxInterfaceInfo->u1CurrRetCount = 0;
        pClntInfoTxInterfaceInfo->u2CurrentApp = 0;
        pClntInfoTxInterfaceInfo->u2CurrAppReqCode = 0;
        /*Run the Refresh timer with the smaller value from receivied value or
         * configured by user **/
        if (pReplyPktInfo->b1IsRefreshTlv == OSIX_TRUE)
        {
            if (pReplyPktInfo->u4RefreshTime >
                pClntInfoTxInterfaceInfo->u4MinRefreshTime)
            {
                pClntInfoTxInterfaceInfo->u4RefreshTime =
                    pClntInfoTxInterfaceInfo->u4MinRefreshTime;
            }
            else
            {
                pClntInfoTxInterfaceInfo->u4RefreshTime =
                    pReplyPktInfo->u4RefreshTime;
            }
            if (D6ClTmrStart
                (pClntInfoTxInterfaceInfo, DHCP6_CLNT_TMR_REFRESH_TIMER,
                 pClntInfoTxInterfaceInfo->u4RefreshTime) == OSIX_FAILURE)
            {
	    	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
                return OSIX_FAILURE;
            }
            /*Start the timer with lower value  Received value */
        }
        else
        {
            pClntInfoTxInterfaceInfo->u4RefreshTime =
                pClntInfoTxInterfaceInfo->u4MinRefreshTime;
            if (D6ClTmrStart
                (pClntInfoTxInterfaceInfo, DHCP6_CLNT_TMR_REFRESH_TIMER,
                 pClntInfoTxInterfaceInfo->u4MinRefreshTime) == OSIX_FAILURE)
            {
	    	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
			(UINT1 *) pReplyPktInfo);
                return OSIX_FAILURE;
            }
            /*Start the timer with user configured value */
        }
    }
    else
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsePacket: Status Code Invalid value received.\r\n");
    	MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
		(UINT1 *) pReplyPktInfo);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (DHCP6_CLNT_PKT_INFO_POOL, 
	(UINT1 *) pReplyPktInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClParsValidateReplyInfo 
 *
 * Description        : This function will validate the received Information.
 *
 * Input(s)           : pReplyPktInfo - Pointer to Pkt Info Strcture
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
D6ClParsValidateReplyInfo (tDhcp6ClntPktInfo * pReplyPktInfo)
{
    tDhcp6ClntIfInfo   *pClntInfoTxInterfaceInfo = NULL;
    pClntInfoTxInterfaceInfo = pReplyPktInfo->pDhcp6ClntIfInfo;

    if (pClntInfoTxInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateReplyInfo Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    if ((pClntInfoTxInterfaceInfo->b1IsClientIdTrsmt == OSIX_TRUE)
        && (pReplyPktInfo->b1ClientIdRcvd == OSIX_FALSE))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateReplyInfo: Discard Reply"
                        " message Client ID Option TLV does not received but"
                        " transmitted by client. \r\n");
        return OSIX_FAILURE;
    }
    if (pReplyPktInfo->b1ServerIdRcvd == OSIX_FALSE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateReplyInfo: Discard Reply"
                        " message Server ID Option TLV does not received. \r\n");
        return OSIX_FAILURE;
    }
    /*when Information already exists and now preferance option does not
     * received*/
    if ((pReplyPktInfo->b1PreferOptionRcvd == OSIX_FALSE) &&
        (pClntInfoTxInterfaceInfo->b1IsInfoExists == OSIX_TRUE))
    {
        D6ClTmrStart (pClntInfoTxInterfaceInfo, DHCP6_CLNT_TMR_REFRESH_TIMER,
                      pClntInfoTxInterfaceInfo->u4RefreshTime);
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateReplyInfo: Higher Preferance "
                        "Information already exists.  \r\n");
        pClntInfoTxInterfaceInfo->b1IsInfoExists = OSIX_FALSE;
        return OSIX_FAILURE;
    }
    /*When Status Code option does not appear in a message in which the 
     * option could appear, the status of the message is assumed to be
     * Success.*/
    if (pReplyPktInfo->b1IsStatusCode == OSIX_FALSE)
    {
        pReplyPktInfo->u2StatusCode = 0;
        MEMCPY (pReplyPktInfo->au1StatusCodeVal, DHCP6_CLNT_STATUS_CODE_VAL,
                DHCP6_CLNT_STATUS_CODE_VAL_LENGTH);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClParsValidateOption 
 *
 * Description        : This function will validate all the TLV received in the
 *                      reply message.
 *
 * Input(s)           : pReplyPktInfo - Pointer to Pkt Info Strcture
 *                      pDhcp6ClntOptionInfo - Pointer to Option Info
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
D6ClParsValidateOption (tDhcp6ClntPktInfo * pReplyPktInfo,
                        tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo,
                        UINT1 *pRcvBuf)
{
    tDhcp6ClntIfInfo   *pClntInfoTxInterfaceInfo = NULL;
    UINT1              *pu1Value = NULL;
    UINT4               u4KeyId = 0;
    UINT4               u4RefreshTime = 0;
    UINT2               u2AuthInfoLength = 0;
    UINT2               u2StatusCode = 0;
    UINT2               u2StatusCodeLength = 0;
    UINT1               u1AuthProtocol = 0;
    UINT1               u1AuthAlgo = 0;
    UINT1               u1AuthRDM = 0;
    UINT1               u1RealmSize = 0;
    UINT1               au1ClientId[DHCP6_CLNT_DUID_MAX_ARRAY_SIZE];
    UINT1               au1RxClientId[DHCP6_CLNT_DUID_MAX_ARRAY_SIZE];
    UINT1               au1Realm[DHCP6_CLNT_REALM_MAX_ARRAY_SIZE];
    UINT1               au1RdValue[DHCP6_CLNT_AUTH_RD_SIZE];
    MEMSET (au1ClientId, 0, DHCP6_CLNT_DUID_MAX_ARRAY_SIZE);
    MEMSET (au1RxClientId, 0, DHCP6_CLNT_DUID_MAX_ARRAY_SIZE);
    MEMSET (au1Realm, 0, DHCP6_CLNT_REALM_MAX_ARRAY_SIZE);

    pClntInfoTxInterfaceInfo = pReplyPktInfo->pDhcp6ClntIfInfo;

    if (pClntInfoTxInterfaceInfo == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateOption Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    switch (pDhcp6ClntOptionInfo->u2Type)
    {
        case DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE:
            /* Discard the received message when client Id Info is not
             * transmitted by the Client But received*/
            if (pClntInfoTxInterfaceInfo->b1IsClientIdTrsmt == OSIX_FALSE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Discard Reply"
                                " message Client ID Option TLV received but not"
                                " transmitted by client. \r\n");
                return OSIX_FAILURE;
            }
            /*Client Id does not match */
            if (pDhcp6ClntOptionInfo->u2Length <= DHCP6_CLNT_DUID_SIZE_MAX)
            {
                if (pClntInfoTxInterfaceInfo->u1ClientIdLength <=
                    DHCP6_CLNT_DUID_SIZE_MAX)
                {
                    MEMCPY (au1ClientId, pClntInfoTxInterfaceInfo->au1ClientId,
                            pClntInfoTxInterfaceInfo->u1ClientIdLength);
                    if (pDhcp6ClntOptionInfo->u2Length <=
                        DHCP6_CLNT_DUID_SIZE_MAX)
                    {
                        MEMCPY (au1RxClientId,
                                pReplyPktInfo->au1DuidValue,
                                pDhcp6ClntOptionInfo->u2Length);
                    }

                    if (MEMCMP (au1ClientId, au1RxClientId,
                                pClntInfoTxInterfaceInfo->u1ClientIdLength) !=
                        0)
                    {
                        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                                        "D6ClParsValidateOption: Discard Reply"
                                        " message wrong Client Id received. \r\n");
                        return OSIX_FAILURE;
                    }
                }
            }
            pReplyPktInfo->b1ClientIdRcvd = OSIX_TRUE;
            break;
        case DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE:
            pReplyPktInfo->b1ServerIdRcvd = OSIX_TRUE;
            break;
        case DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE:
            /* Check Information already exists */
            if (pClntInfoTxInterfaceInfo->b1IsInfoExists == OSIX_TRUE)
            {
                /*Higher Preferance option already exists so discard the
                 * received message */
                if (pClntInfoTxInterfaceInfo->u1PreferanceValue >=
                    pReplyPktInfo->au1Value[0])
                {
                    D6ClTmrStart (pClntInfoTxInterfaceInfo,
                                  DHCP6_CLNT_TMR_REFRESH_TIMER,
                                  pClntInfoTxInterfaceInfo->u4RefreshTime);
                    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                    DHCP6_CLNT_CONTROL_PLANE_TRC,
                                    "D6ClParsValidateOption: Higher Preferance "
                                    " Information already exists.   \r\n");
                    pClntInfoTxInterfaceInfo->b1IsInfoExists = OSIX_FALSE;
                    return OSIX_FAILURE;
                }
            }
            pReplyPktInfo->b1PreferOptionRcvd = OSIX_TRUE;
            pReplyPktInfo->u1PreferanceVal = pReplyPktInfo->au1Value[0];
            break;
        case DHCP6_CLNT_AUTH_IDENTIFIER_OPTION_TYPE:
            /* Discard the Message when Key and Realm Does not does not
             * configured But Auth Option received */
            if ((pClntInfoTxInterfaceInfo->u1RealmLength == 0) ||
                (pClntInfoTxInterfaceInfo->u1KeyLength == 0))
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Auth Option received "
                                " Realm and Key not configured.  \r\n");
                return OSIX_FAILURE;
            }
            /*Check the Protocol value must be 3 Delayed Authentication
             * Protocol,Algorithm value must be 1 HMAC-MD5 and
             * RDM Value must be zero
             * Replay detection value must be non zero.*/
            if (pDhcp6ClntOptionInfo->u2Length <= DHCP6_CLNT_MIN_AUTH_LENGTH)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Authentication Option"
                                "contains invalid Length. \r\n");
                return OSIX_FAILURE;
            }
            pu1Value = pReplyPktInfo->au1Value;
            DHCP6_CLNT_GET_1BYTE (u1AuthProtocol, pu1Value);
            if (u1AuthProtocol != DHCP6_CLNT_DELAYED_AUTH_PROTOCOL)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Wrong Prorocol "
                                "received in Authentication Option.  \r\n");
                return OSIX_FAILURE;

            }
            DHCP6_CLNT_GET_1BYTE (u1AuthAlgo, pu1Value);
            if (u1AuthAlgo != DHCP6_CLNT_HMAC_MD5_ALGORITHM)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Wrong Algorithm "
                                "received in Authentication Option.  \r\n");
                return OSIX_FAILURE;

            }

            DHCP6_CLNT_GET_1BYTE (u1AuthRDM, pu1Value);
            if (u1AuthRDM != DHCP6_CLNT_RDM_VALUE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Wrong RDM "
                                "received in Authentication Option.  \r\n");
                return OSIX_FAILURE;
            }
            /* skip RD value */
            DHCP6_CLNT_GET_NBYTE (au1RdValue, pu1Value,
                                  DHCP6_CLNT_REPLAY_DET_VALUE);
            if (MEMCMP
                (au1RdValue, pClntInfoTxInterfaceInfo->au1LastRdValue,
                 DHCP6_CLNT_REPLAY_DET_VALUE) == 0)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Wrong RD "
                                "received in Authentication Option.  \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (pClntInfoTxInterfaceInfo->au1LastRdValue, au1RdValue,
                    DHCP6_CLNT_REPLAY_DET_VALUE);
            u1RealmSize =
                (UINT1) (pDhcp6ClntOptionInfo->u2Length -
                         (DHCP6_CLNT_AUTH_KEYID_SIZE +
                          DHCP6_CLNT_ALGO_DIGEST_SIZE +
                          DHCP6_CLNT_AUTH_RD_SIZE + DHCP6_CLNT_AUTH_RDM_SIZE +
                          DHCP6_CLNT_AUTH_ALGO_SIZE +
                          DHCP6_CLNT_AUTH_PROTO_SIZE));
            u1RealmSize =
                MEM_MAX_BYTES (u1RealmSize, DHCP6_CLNT_REALM_MAX_ARRAY_SIZE);
            DHCP6_CLNT_GET_NBYTE (au1Realm, pu1Value, u1RealmSize);
            if (MEMCMP (au1Realm, pClntInfoTxInterfaceInfo->au1Realm,
                        DHCP6_CLNT_REALM_SIZE_MAX) != 0)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Wrong realm "
                                "received in Authentication Option.  \r\n");
                return OSIX_FAILURE;
            }
            DHCP6_CLNT_GET_4BYTE (u4KeyId, pu1Value);
            if (u4KeyId != pClntInfoTxInterfaceInfo->u4KeyIdentifier)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClParsValidateOption: Wrong key-id "
                                "received in Authentication Option.  \r\n");
                return OSIX_FAILURE;
            }
            DHCP6_CLNT_GET_NBYTE (pReplyPktInfo->au1AuthValue,
                                  pu1Value, DHCP6_CLNT_ALGO_DIGEST_SIZE);

            MEMSET ((pRcvBuf - DHCP6_CLNT_ALGO_DIGEST_SIZE), 0x00,
                    DHCP6_CLNT_ALGO_DIGEST_SIZE);
            pReplyPktInfo->u2AuthInfoLength = u2AuthInfoLength;
            pReplyPktInfo->b1IsAuthOptReceived = OSIX_TRUE;
            break;
        case DHCP6_CLNT_STATUS_CODE_OPTION_TYPE:

            pReplyPktInfo->b1IsStatusCode = OSIX_TRUE;
            pu1Value = pReplyPktInfo->au1Value;
            DHCP6_CLNT_GET_2BYTE (u2StatusCode, pu1Value);

            pReplyPktInfo->u2StatusCode = u2StatusCode;

            u2StatusCodeLength = (UINT2) (pDhcp6ClntOptionInfo->u2Length -
                                          DHCP6_CLNT_INDEX_BY_TWO);
            if (u2StatusCodeLength <= DHCP6_CLNT_STATUS_CODE_SIZE_MAX)
            {
                MEMCPY (&pReplyPktInfo->au1StatusCodeVal,
                        &(pReplyPktInfo->
                          au1Value[DHCP6_CLNT_INDEX_BY_TWO]),
                        u2StatusCodeLength);
            }

            break;
        case DHCP6_CLNT_IA_NA_OPTION_TYPE:
        case DHCP6_CLNT_IA_TA_OPTION_TYPE:
        case DHCP6_CLNT_IA_ADDR_OPTION_TYPE:

            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClParsValidateOption: IA Option received "
                            " Discard the reply message.  \r\n");
            return OSIX_FAILURE;
            break;
        case DHCP6_CLNT_REFRESH_TIMER_OPTION_TYPE:
            pReplyPktInfo->b1IsRefreshTlv = OSIX_TRUE;
            pu1Value = pReplyPktInfo->au1Value;
            DHCP6_CLNT_GET_4BYTE (u4RefreshTime, pu1Value);
            pReplyPktInfo->u4RefreshTime = u4RefreshTime;

            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6Cl6ParsePduInfo 
 *
 * Description        : This function Parse and store all the options in the
 *                      Replay strcture.
 *
 * Input(s)           : pu1RcvBuf - Pointer to Buffer
 *                    : pReplyPktInfo - Pointer to Pkt Info Strcture
 *                    : u4ByteCount - Length of Message
 *                      pu4PduLength - Pointer to length
 *                      
 * Output(s)          : pu4PduLength - Pointer to length
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PRIVATE INT4
D6Cl6ParsePduInfo (UINT1 *pu1RcvBuf, tDhcp6ClntPktInfo * pReplyPktInfo,
                   UINT4 u4ByteCount, UINT4 *pu4PduLength)
{
    tDhcp6ClntOptionInfo Dhcp6ClntOptionInfo;
    tDhcp6CInvalidMsgRxdTrap Dhcp6CInvalidMsgRxdTrap;

    MEMSET (&Dhcp6ClntOptionInfo, 0, sizeof (tDhcp6ClntOptionInfo));
    MEMSET (&Dhcp6CInvalidMsgRxdTrap, 0, sizeof (tDhcp6CInvalidMsgRxdTrap));

    while (u4ByteCount != 0)
    {
        /*Read Two byte Type value from the Buffer */
        DHCP6_CLNT_GET_2BYTE (Dhcp6ClntOptionInfo.u2Type, pu1RcvBuf);
        DHCP6_CLNT_GET_2BYTE (Dhcp6ClntOptionInfo.u2Length, pu1RcvBuf);

        if ((Dhcp6ClntOptionInfo.u2Length != 0) &&
            (Dhcp6ClntOptionInfo.u2Length <= DHCP6_CLNT_OPTION_SIZE_MAX))
        {
            if (Dhcp6ClntOptionInfo.u2Type ==
                DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE)
            {
                if (Dhcp6ClntOptionInfo.u2Length <= DHCP6_CLNT_DUID_SIZE_MAX)
                {
                    MEMCPY (pReplyPktInfo->au1DuidValue, pu1RcvBuf,
                            Dhcp6ClntOptionInfo.u2Length);
                    pu1RcvBuf = pu1RcvBuf + Dhcp6ClntOptionInfo.u2Length;
                }
            }
            else
            {
                MEMCPY (pReplyPktInfo->au1Value, pu1RcvBuf,
                        Dhcp6ClntOptionInfo.u2Length);
                pu1RcvBuf = pu1RcvBuf + Dhcp6ClntOptionInfo.u2Length;
            }
        }
        else
        {
            Dhcp6CInvalidMsgRxdTrap.u4IfIndex =
                pReplyPktInfo->pDhcp6ClntIfInfo->u4IfIndex;
            D6ClTrapSnmpSend (&Dhcp6CInvalidMsgRxdTrap,
                              DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_VAL);
            pReplyPktInfo->pDhcp6ClntIfInfo->u4InvalidPktIn++;

            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                            "D6Cl6ParsePduInfo: Length Of received Option is"
                            " greater then maxmium length. \r\n");
            return OSIX_FAILURE;
        }
        if (D6ClParsValidateOption
            (pReplyPktInfo, &Dhcp6ClntOptionInfo, pu1RcvBuf) == OSIX_FAILURE)
        {
            Dhcp6CInvalidMsgRxdTrap.u4IfIndex =
                pReplyPktInfo->pDhcp6ClntIfInfo->u4IfIndex;
            D6ClTrapSnmpSend (&Dhcp6CInvalidMsgRxdTrap,
                              DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_VAL);
            pReplyPktInfo->pDhcp6ClntIfInfo->u4InvalidPktIn++;
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6Cl6ParsePduInfo: Return Failure. \r\n");
            return OSIX_FAILURE;
        }
        MEMSET (pReplyPktInfo->au1Value, 0, DHCP6_CLNT_OPT_MAX_ARRAY_SIZE);
        MEMSET (pReplyPktInfo->au1DuidValue, 0, DHCP6_CLNT_DUID_MAX_ARRAY_SIZE);
        u4ByteCount =
            u4ByteCount - (Dhcp6ClntOptionInfo.u2Length +
                           (UINT2) DHCP6_CLNT_FOUR_BYTE_LENGTH);
        *pu4PduLength = *pu4PduLength + DHCP6_CLNT_FOUR_BYTE_LENGTH +
            Dhcp6ClntOptionInfo.u2Length;
        MEMSET (&Dhcp6ClntOptionInfo, 0, sizeof (tDhcp6ClntOptionInfo));
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClValidatePacket 
 *
 * Description        : This function will validate SourcePort and Destination Port  
 *                      and Message Type value of received DHCP Message 
 *                      to configured UDP Port
 *
 * Input(s)           : pDhcp6ClntUdpHeaderInfo  - Pointer to UDP header
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PRIVATE INT4
D6ClValidatePacket (UINT1 *pu1Buf, UINT4 u4TransactionId,
                    tDhcp6ClntUdpHeader * pDhcp6ClntUdpHeaderInfo)
{
    UINT4               u4Index = 0;

    u4Index = pDhcp6ClntUdpHeaderInfo->u4Index;
    if (D6ClParsRxValidatePduHdr (pDhcp6ClntUdpHeaderInfo) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClValidatePacket: D6clParsRxValidatePduHdr"
                        " Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    /*Validate the Message Type and Transaction Id Value */
    if (D6ClParsValidateMessageType (pu1Buf, u4TransactionId, u4Index) ==
        OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClValidatePacket: D6ClParsValidateMessageType"
                        " Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClParsRxValidatePduHdr 
 *
 * Description        : This function validate SourcePort and Destination Port  
 *                      value of received DHCP Message to configured UDP Port
 *
 * Input(s)           : pDhcp6ClntUdpHeaderInfo  - Pointer to UDP header
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PRIVATE INT4
D6ClParsRxValidatePduHdr (tDhcp6ClntUdpHeader * pDhcp6ClntUdpHeaderInfo)
{
    if (pDhcp6ClntUdpHeaderInfo->u2SrcPort != DHCP6_CLNT_SOURCE_PORT)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsRxValidatePduHdr: Source Port "
                        "Comparison Failure. \r\n");
        return OSIX_FAILURE;
    }
    if (pDhcp6ClntUdpHeaderInfo->u2DstPort != DHCP6_CLNT_DEST_PORT)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsRxValidatePduHdr: Destination Port "
                        "Comparison Failure. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClParsValidateMessageType 
 *
 * Description        : This function validate Mesage type and Transaction Id 
 *                      value of received DHCP Message
 *
 * Input(s)           : pu1Buf  - Pointer to Pkt Info Strcture
 *                      u4TransactionId - u4TransactionId Value
 *                      u4Index - Interface Index
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PRIVATE INT4
D6ClParsValidateMessageType (UINT1 *pu1Buf, UINT4 u4TransactionId,
                             UINT4 u4IfIndex)
{

    tDhcp6ClntIfInfo   *pClntInfoTxInterfaceInfo = NULL;
    UINT4               u4RxMesgTypeTransId = 0;
    UINT4               u4RxTransId = 0;
    UINT4               u4u4MsgType = 0;

    DHCP6_CLNT_GET_4BYTE (u4RxMesgTypeTransId, pu1Buf);
    u4u4MsgType = u4RxMesgTypeTransId & DHCP6_CLNT_VALIDATE_MES_TYPE;

    if (u4u4MsgType != DHCP6_CLNT_REPLY_MES_TYPE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateMessageType: Invalid Message Type received "
                        "on the Interface. \r\n");
        return OSIX_FAILURE;
    }
    pClntInfoTxInterfaceInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pClntInfoTxInterfaceInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    pClntInfoTxInterfaceInfo->u4ReplyIn += DHCP6_CLNT_INCR_ONE;
    u4RxTransId = (u4RxMesgTypeTransId & DHCP6_CLNT_VALIDATE_TRANS_ID);

    if (u4TransactionId != u4RxTransId)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClParsValidateMessageType: Invalid Transaction Id received "
                        "in the Replay Message. \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClParsAddOption 
 *
 * Description        : This function Parse and validate IPv6 and UDP 
 *                      header of received DHCP Message
 *
 * Input(s)           : pReplyPktInfo - Pointer to Pkt Info Strcture
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
D6ClParsAddOption (UINT1 *pu1RcvBuf, UINT4 u4IfIndex, UINT4 u4ByteCount)
{
    tDhcp6ClntOptionInfo Dhcp6ClntOptionInfo;
    tDhcp6ClntIfInfo   *pClntTxInterfaceInfo = NULL;
    UINT1               au1Value[DHCP6_CLNT_OPT_MAX_ARRAY_SIZE];

    MEMSET (au1Value, 0, DHCP6_CLNT_OPT_MAX_ARRAY_SIZE);
    MEMSET (&Dhcp6ClntOptionInfo, 0, sizeof (tDhcp6ClntOptionInfo));
    pClntTxInterfaceInfo = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pClntTxInterfaceInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    while (u4ByteCount != 0)
    {
        /*Read Two byte Type value from the Buffer */
        DHCP6_CLNT_GET_2BYTE (Dhcp6ClntOptionInfo.u2Type, pu1RcvBuf);
        DHCP6_CLNT_GET_2BYTE (Dhcp6ClntOptionInfo.u2Length, pu1RcvBuf);
        Dhcp6ClntOptionInfo.u4IfIndex = u4IfIndex;
        if ((Dhcp6ClntOptionInfo.u2Length != 0) &&
            (Dhcp6ClntOptionInfo.u2Length <= DHCP6_CLNT_OPTION_SIZE_MAX))
        {
            MEMCPY (au1Value, pu1RcvBuf, Dhcp6ClntOptionInfo.u2Length);
            pu1RcvBuf = pu1RcvBuf + Dhcp6ClntOptionInfo.u2Length;
        }
        else
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CRITICAL_TRC,
                            "D6ClParsAddOption: Length Of received Option is"
                            " greater then maxmium length. \r\n");
            return OSIX_FAILURE;
        }
        if (Dhcp6ClntOptionInfo.u2Type != 0)
        {
            if (D6ClAddOptionInterface
                (&Dhcp6ClntOptionInfo, au1Value, u4IfIndex) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        u4ByteCount = u4ByteCount - (Dhcp6ClntOptionInfo.u2Length +
                                     DHCP6_CLNT_FOUR_BYTE_LENGTH);
        MEMSET (&Dhcp6ClntOptionInfo, 0, sizeof (tDhcp6ClntOptionInfo));
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Name               : D6ClAddOptionInterface 
 *
 * Description        : This function will Add the Option in the Interface
 *                      strcture.
 *
 * Input(s)           : pDhcp6ClntOptionInfo - Pointer to Option Info
 *                      u4IfIndex - Interface Index Value
 *                      
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

PRIVATE INT4
D6ClAddOptionInterface (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo,
                        UINT1 *pu1Value, UINT4 u4IfIndex)
{
    tDhcp6ClntOptionInfo *pDhcp6ClntIntOptionInfo = NULL;
    if (pDhcp6ClntOptionInfo != NULL)
    {
        if ((pDhcp6ClntOptionInfo->u2Type ==
             DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE)
            || (pDhcp6ClntOptionInfo->u2Type ==
                DHCP6_CLNT_AUTH_IDENTIFIER_OPTION_TYPE)
            || (pDhcp6ClntOptionInfo->u2Type == DHCP6_CLNT_IA_NA_OPTION_TYPE)
            || (pDhcp6ClntOptionInfo->u2Type ==
                DHCP6_CLNT_REFRESH_TIMER_OPTION_TYPE))
        {
            return OSIX_SUCCESS;
        }
        else
        {
            pDhcp6ClntIntOptionInfo =
                D6ClOptGetInterOptionInfo (u4IfIndex,
                                           pDhcp6ClntOptionInfo->u2Type);
            if (pDhcp6ClntIntOptionInfo != NULL)
            {
                pDhcp6ClntIntOptionInfo->u2Length =
                    pDhcp6ClntOptionInfo->u2Length;
                pDhcp6ClntIntOptionInfo->u4IfIndex = u4IfIndex;
                /*Update The Memory */
                if (D6ClOptUpdateValueMemory
                    (pDhcp6ClntIntOptionInfo,
                     pDhcp6ClntIntOptionInfo->u2Length) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
                if (pDhcp6ClntIntOptionInfo->u2Length <=
                    (UINT2) DHCP6_CLNT_OPTION_SIZE_MAX)
                {
                    MEMCPY (pDhcp6ClntIntOptionInfo->pu1Value,
                            pu1Value, pDhcp6ClntIntOptionInfo->u2Length);
                }
            }
            else
            {
                pDhcp6ClntIntOptionInfo = D6ClOptCreateNode ();
                if (pDhcp6ClntIntOptionInfo == NULL)
                {
                    return OSIX_FAILURE;
                }
                pDhcp6ClntIntOptionInfo->u4IfIndex =
                    pDhcp6ClntOptionInfo->u4IfIndex;
                pDhcp6ClntIntOptionInfo->u2Type = pDhcp6ClntOptionInfo->u2Type;
                pDhcp6ClntIntOptionInfo->u2Length =
                    pDhcp6ClntOptionInfo->u2Length;
                /*Allocate the Memory */

                if (D6ClOptUpdateValueMemory
                    (pDhcp6ClntIntOptionInfo,
                     pDhcp6ClntOptionInfo->u2Length) == OSIX_FAILURE)
                {
                    D6ClOptDelOptionNode (pDhcp6ClntIntOptionInfo);
                    return OSIX_FAILURE;
                }

                MEMCPY (pDhcp6ClntIntOptionInfo->pu1Value,
                        pu1Value, pDhcp6ClntIntOptionInfo->u2Length);
                if (D6ClOptAddOptionNode (pDhcp6ClntIntOptionInfo) ==
                    OSIX_FAILURE)
                {
                    DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                    DHCP6_CLNT_CRITICAL_TRC,
                                    "D6ClOptAddOptionNode: Function Return"
                                    " Failure.\r\n");
                    D6ClOptDelOptionNode (pDhcp6ClntIntOptionInfo);
                    return OSIX_FAILURE;
                }
            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClParsPassOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function will Pass the Information to all the
 *                       Application registered to client.
 *
 *    INPUT            : pInterfaceNode :- Pointer to Interface Info. 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *    
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
D6ClParsPassOptionInfo (tDhcp6ClntIfInfo * pInterfaceNode)
{
    tDhcp6ClntOptionInfo *pDhcp6ClntOptionInfo = NULL;
    UINT2               u2Length = 0;
    UINT2               u2Type = 0;
    UINT2               u2Counter = 0;
    UINT1               u1Result = OSIX_FALSE;
    UINT1               au1OptBuf[DHCP6_CLNT_OPT_MAX_ARRAY_SIZE];

    MEMSET (au1OptBuf, 0, DHCP6_CLNT_OPT_MAX_ARRAY_SIZE);
    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_CRITICAL_TRC,
                        "D6ClParsPassOptionInfo: Interface does not"
                        " exists. \r\n");
        return OSIX_FAILURE;
    }

    pDhcp6ClntOptionInfo = D6ClOptGetFirstInterOptionInfo
        (pInterfaceNode->u4IfIndex);

    while (pDhcp6ClntOptionInfo != NULL)
    {
        if ((pInterfaceNode->u2CurrentApp != 0)
            && (pInterfaceNode->u2CurrentApp < MAX_D6CL_CLNT_APPS))
        {
            if (gDhcp6ClntGblInfo.apAppParam[pInterfaceNode->u2CurrentApp] !=
                NULL)
            {
                u2Length = pDhcp6ClntOptionInfo->u2Length;
                u2Type = pDhcp6ClntOptionInfo->u2Type;
                if (D6ClParsPassValidOptionInfo (pDhcp6ClntOptionInfo, u2Length)
                    == OSIX_SUCCESS)
                {
                    /*Pass the Information to current Application only */
                    MEMCPY (au1OptBuf, pDhcp6ClntOptionInfo->pu1Value,
                            u2Length);
                    DHCP6_CLNT_IS_MEMBER_APP (pDhcp6ClntOptionInfo->
                                              au1ApplicationMap,
                                              pInterfaceNode->u2CurrentApp,
                                              u1Result);

                    if ((u1Result == OSIX_TRUE) && (gDhcp6ClntGblInfo.
                                                    apAppParam[pInterfaceNode->
                                                               u2CurrentApp]->
                                                    fpDhcp6ClntCallback !=
                                                    NULL))
                    {
                        gDhcp6ClntGblInfo.apAppParam[pInterfaceNode->
                                                     u2CurrentApp]->
                            fpDhcp6ClntCallback (pInterfaceNode->u2CurrentApp,
                                                 pInterfaceNode->u4IfIndex,
                                                 &u2Type, &u2Length, au1OptBuf);
                    }
                }
            }
        }
        else
        {
            for (u2Counter = 0; u2Counter < MAX_D6CL_CLNT_APPS; u2Counter++)
            {
                /*Check if the Appliction exists and Bit value is set the pass the
                 * received Information with Poiner function. */
                if (u2Counter < MAX_D6CL_CLNT_APPS &&
                    u2Counter > DHCP6_CLNT_RESERVE_APP_ID)
                {
                    if (gDhcp6ClntGblInfo.apAppParam[u2Counter] != NULL)
                    {
                        DHCP6_CLNT_IS_MEMBER_APP (pDhcp6ClntOptionInfo->
                                                  au1ApplicationMap, u2Counter,
                                                  u1Result);

                        if ((u1Result == OSIX_TRUE)
                            && (gDhcp6ClntGblInfo.apAppParam[u2Counter]->
                                fpDhcp6ClntCallback != NULL))
                        {
                            u2Length = pDhcp6ClntOptionInfo->u2Length;
                            u2Type = pDhcp6ClntOptionInfo->u2Type;
                            if (D6ClParsPassValidOptionInfo
                                (pDhcp6ClntOptionInfo,
                                 u2Length) == OSIX_SUCCESS)
                            {
                                MEMCPY (au1OptBuf,
                                        pDhcp6ClntOptionInfo->pu1Value,
                                        u2Length);
                                gDhcp6ClntGblInfo.apAppParam[u2Counter]->
                                    fpDhcp6ClntCallback (u2Counter,
                                                         pInterfaceNode->
                                                         u4IfIndex, &u2Type,
                                                         &u2Length, au1OptBuf);
                            }
                        }
                    }
                }
            }
        }
        pDhcp6ClntOptionInfo = D6ClOptGetNextInterOptionInfo
            (pDhcp6ClntOptionInfo, pInterfaceNode->u4IfIndex);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : D6ClParsValidatePassOptionInfo 
 *                                                                          
 *    DESCRIPTION      : This function will .
 *
 *    INPUT            : pDhcp6ClntOptionInfo :- Pointer to Option Info.
 *                       u2Length :- Length Value
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *    
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
D6ClParsPassValidOptionInfo (tDhcp6ClntOptionInfo * pDhcp6ClntOptionInfo,
                             UINT2 u2Length)
{
    if ((u2Length != 0) && (u2Length <= DHCP6_CLNT_OPTION_SIZE_MAX))
    {
        if ((pDhcp6ClntOptionInfo->u2Type !=
             DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE)
            && (pDhcp6ClntOptionInfo->u2Type !=
                DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE)
            && (pDhcp6ClntOptionInfo->u2Type !=
                DHCP6_CLNT_STATUS_CODE_OPTION_TYPE))
        {
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clpars.c                       */
/*-----------------------------------------------------------------------*/
