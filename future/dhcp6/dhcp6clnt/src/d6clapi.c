/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6clapi.c,v 1.10 2012/01/16 13:01:18 siva Exp $
 * Description: This file contains the procedures called by other
 *              modules to access te functionality of this module
 * ************************************************************************/

#include "d6clinc.h"
#include "d6clglob.h"

/*******************************************************************************
 *    FUNCTION NAME    : D6ClApiLock                                            *
 *                                                                             *
 *    DESCRIPTION      : Function to take the mutual exclusion protocol        *
 *                       semaphore.                                            *
 *                                                                             *
 *    INPUT            : None                                                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                             *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
D6ClApiLock ()
{
    if (OsixSemTake (DHCP6_CLNT_SEM_ID) == OSIX_FAILURE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiLock: Return Failure. \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 *    FUNCTION NAME    : D6ClApiUnLock                                         *
 *                                                                            *
 *    DESCRIPTION      : Function to give the mutual exclusion protocol       *
 *                       semaphore.                                           *
 *                                                                            *
 *    INPUT            : None                                                 *
 *                                                                            *
 *    OUTPUT           : None                                                 *
 *                                                                            *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                            *
 *****************************************************************************/

PUBLIC INT4
D6ClApiUnLock ()
{
    OsixSemGive (DHCP6_CLNT_SEM_ID);
    return SNMP_SUCCESS;
}

/*******************************************************************************
 *                                                                             *
 *    Function Name    : D6ClApiIp6IfStatusNotify                           *
 *                                                                             *
 *    Description      : This Callback function is registered with ipv6        *
 *                       module(using NetIpv6RegisterHigherLayerProtocol()     *
 *                       API)to get the notification about the IF status       *
 *                       change.                                               *
 *                       Whenever status of any If status changed this         *
 *                       callback routine is invoked.                          *
 *                       After getting this notification  DHCP6C task updates  *
 *                       its local database.                                   *
 *                                                                             *
 *    Input(s)         : pNetIpv6HlParams - Pointer to If Config Record        *
 *                                          Structure                          *
 *                                                                             *
 *    Output(s)        : None                                                  *
 *                                                                             *
 *    Returns          : OSIX_FALSE/OSIX_TRUE                                                  *
 ******************************************************************************/

PUBLIC INT4
D6ClApiIp6IfStatusNotify (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tDhcp6ClntQMsg     *pMsg = NULL;
    UINT4               u4Mask = 0;

    u4Mask = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask;

    /* Check for System Status */
    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_INIT_SHUT_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiIp6IfStatusNotify: Dhcp6 Client Systen "
                        "not initialised. \r\n");
        return OSIX_FALSE;
    }

    if (pNetIpv6HlParams->u4Command != NETIPV6_INTERFACE_PARAMETER_CHANGE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiIp6IfStatusNotify: Return Failure. \r\n");
        return OSIX_FALSE;
    }

    /* Allocating MEM Block for the Message */
    if ((pMsg =
         (tDhcp6ClntQMsg *) MemAllocMemBlk (DHCP6_CLNT_MSGQ_POOL)) == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiIp6IfStatusNotify: Memory "
                        "Allocation Failure. \r\n");
        return OSIX_FALSE;
    }
    MEMSET (pMsg, 0, sizeof (tDhcp6ClntQMsg));
    pMsg->u4MsgType = DHCP6_CLNT_IP6_IF_STAUS_CHG_MSG;
    pMsg->u4BitMap = u4Mask;
    pMsg->u4IfIndex = pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4Index;
    pMsg->u4IfState = pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4IfStat;
    pMsg->u4OperStatus =
        pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4OperStatus;

    if (D6ClUtlCfgQueMsg (pMsg) != OSIX_SUCCESS)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiIp6IfStatusNotify: Message Queuing"
                        "Failed. \r\n");
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

/*****************************************************************************
 * Function Name     : D6ClApiRegisterApplication                            *
 *                                                                            *
 * Description        : This Routine if used by Application to register with  *
 *                      DHCP6 Client Module.Application can also pass the     *
 *                      pointer function in pAppParam.                        *
 *                                                                            *
 * Input(s)           : u2AppId - Application Id                              * 
 *                      pAppParam - Reg. params to be provided by Application *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_TRUE / OSIX_FALSE                           *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
D6ClApiRegisterApplication (UINT2 u2AppId, tDhcp6ClntAppParam * pAppParam)
{
    /* Check for System Status */
    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_INIT_SHUT_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiRegisterApplication: Dhcp6 Client Systen "
                        "not initialised. \r\n");
        return OSIX_FAILURE;
    }

    if ((u2AppId == 0) || (u2AppId >= (UINT2) MAX_D6CL_CLNT_APPS))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiRegisterApplication: Invalid Application Id. \r\n");
        return OSIX_FAILURE;
    }

    if (pAppParam == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClApiRegisterApplication: Function Return"
                        " Failure. \r\n");
        return OSIX_FAILURE;
    }

    D6ClApiLock ();
    /* Allocate memory for the new entry */

    if ((gDhcp6ClntGblInfo.apAppParam[u2AppId] =
         (tDhcp6ClntAppParam *) MemAllocMemBlk (DHCP6_CLNT_APPL_INFO_POOL)) ==
        NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiRegisterApplication: Memory Allocation"
                        " Failure. \r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }

    /* Update the application specific info */
    gDhcp6ClntGblInfo.apAppParam[u2AppId]->u2AppId = u2AppId;

    if (pAppParam->fpDhcp6ClntCallback != NULL)
    {
        gDhcp6ClntGblInfo.apAppParam[u2AppId]->fpDhcp6ClntCallback =
            pAppParam->fpDhcp6ClntCallback;
    }
    else
    {
        gDhcp6ClntGblInfo.apAppParam[u2AppId]->fpDhcp6ClntCallback = NULL;
    }
    D6ClApiUnLock ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : D6ClApiDeRegisterApplication                         *
 *                                                                           *
 * Description        : This Routine used by Application to de-register      *
 *                      from DHCP6 Client Module.On reception client will    *
 *                      free the memory pool allocated by the client.        *
 *                                                                           *
 * Input(s)           : u2AppId - Application Id.                            *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_TRUE / OSIX_FALSE                          *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
D6ClApiDeRegisterApplication (UINT2 u2AppId)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4Counter = 0;
    /* Check for System Status */
    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_INIT_SHUT_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiDeRegisterApplication:Dhcp6 Client Systen "
                        "not initialised. \r\n");
        return OSIX_FAILURE;
    }

    if ((u2AppId == 0) || (u2AppId >= (UINT2) MAX_D6CL_CLNT_APPS))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiDeRegisterApplication: Invalid Application Id. \r\n");
        return OSIX_FAILURE;
    }

    D6ClApiLock ();
    if (gDhcp6ClntGblInfo.apAppParam[u2AppId] == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiDeRegisterApplication: Application Id does not exits. \r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }
    /* Remove This Application Id Information from all the Options and from all the
     * interfaces.*/

    for (u4IfIndex = 1; u4IfIndex <= MAX_D6CL_CLNT_IFACES; u4IfIndex++)
    {
        pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);

        if (pInterfaceNode != NULL)
        {
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_USER_CLASS_MAX;
                 u4Counter++)
            {
                pInterfaceNode->UserClass[u2AppId][u4Counter].u2Code = 0;
                pInterfaceNode->UserClass[u2AppId][u4Counter].u2Length = 0;

                MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                              (UINT1 *) pInterfaceNode->
                              UserClass[u2AppId][u4Counter].pu1Value);
                pInterfaceNode->UserClass[u2AppId][u4Counter].pu1Value = NULL;
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_CLASS_MAX;
                 u4Counter++)
            {
                pInterfaceNode->VendorClass[u2AppId][u4Counter].u2Code = 0;
                pInterfaceNode->VendorClass[u2AppId][u4Counter].u2Length = 0;
                MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                              (UINT1 *) pInterfaceNode->
                              VendorClass[u2AppId][u4Counter].pu1Value);
                pInterfaceNode->VendorClass[u2AppId][u4Counter].pu1Value = NULL;
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_VENDOR_SPEC_MAX;
                 u4Counter++)
            {
                pInterfaceNode->VendorSpec[u2AppId][u4Counter].u2Code = 0;
                pInterfaceNode->VendorSpec[u2AppId][u4Counter].u2Length = 0;
                MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                              (UINT1 *) pInterfaceNode->
                              VendorSpec[u2AppId][u4Counter].pu1Value);
                pInterfaceNode->VendorSpec[u2AppId][u4Counter].pu1Value = NULL;
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_REQ_OPT_MAX; u4Counter++)
            {
                pInterfaceNode->aau1RequestOption[u2AppId][u4Counter] = 0;
            }
            for (u4Counter = 0; u4Counter < DHCP6_CLNT_OPT_MAX; u4Counter++)
            {
                pInterfaceNode->aau1Option[u2AppId][u4Counter] = 0;

                if ((pInterfaceNode->aau1Option[u2AppId][u4Counter] ==
                     DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE)
                    || pInterfaceNode->aau1Option[u2AppId][u4Counter] ==
                    DHCP6_CLNT_ELPASED_OPTION_TYPE)
                {
                    continue;
                    /*By default Client Option will be Set */
                }
            }
            D6ClIfReSetAppIdFromOpt (pInterfaceNode, u2AppId);

            pInterfaceNode = NULL;
        }
    }

    /* Free the memory of this entry */
    if (MemReleaseMemBlock (DHCP6_CLNT_APPL_INFO_POOL,
                            (UINT1 *) gDhcp6ClntGblInfo.apAppParam[u2AppId]) ==
        MEM_FAILURE)

    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiDeRegisterApplication: Memory Release Failure. \r\n");
        D6ClApiUnLock ();
        return OSIX_FALSE;
    }
    gDhcp6ClntGblInfo.apAppParam[u2AppId] = NULL;
    D6ClApiUnLock ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : D6ClApiOptionSet                                      *
 *                                                                            *
 * Description        : This Routine used by Application to set and Reset the *
 *                      option code which client will transmit in the         *
 *                      information request message.User class, Vendor Class  *
 *                      and vendor specific values are also set and reset by  *
 *                      this routine.                                         *
 *                                                                            *
 * Input(s)           : u2AppId - Application Id                              * 
 *                      pAppParam - Reg. params to be provided by Application *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : OSIX_TRUE / OSIX_FALSE                                *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
D6ClApiOptionSet (UINT2 u2AppId, tDhcp6ClntAppOptionParam * pOptionParam)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;
    tDhcp6ClntOptionInfo *pDhcp6ClntOptionInfo = NULL;
    tDhcp6ClntOptionInfo *pDhcp6ClntRxOptionInfo = NULL;
    UINT4               u4IfIndex = 0;

    /* Check for System Status */
    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_INIT_SHUT_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionSet: Dhcp6 Client Systen is"
                        "not initialised. \r\n");
        return OSIX_FAILURE;
    }

    if ((u2AppId == 0) || (u2AppId >= (UINT2) MAX_D6CL_CLNT_APPS))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiOptionSet: Invalid Application Id. \r\n");
        return OSIX_FAILURE;
    }

    if (pOptionParam == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClApiOptionSet: Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    D6ClApiLock ();
    if (gDhcp6ClntGblInfo.apAppParam[u2AppId] == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionSet: Application Id not Registered. \r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }
    u4IfIndex = pOptionParam->u4IfIndex;

    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionSet: Interface does not exits. \r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }

    if ((u2AppId >= MAX_D6CL_CLNT_APPS)
        || (pOptionParam->u2Code >= DHCP6_CLNT_REQ_OPT_MAX))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionSet: Wrong Application Id or wrong Code "
                        "Requested. \r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }
    if (pOptionParam->u1Action == (UINT1) DHCP6_CLNT_APP_INFO_ADD)
    {
        switch (pOptionParam->u1OptionType)
        {
            case DHCP6_CLNT_REQUEST_OPTION_CODE:
                /*Preferance Option Code and server Id code are invalid in
                 * option Request Code*/
                if ((pOptionParam->u2Code ==
                     DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE)
                    || (pOptionParam->u2Code ==
                        DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE)
                    || (pOptionParam->u2Code ==
                        DHCP6_CLNT_STATUS_CODE_OPTION_TYPE))
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }

                pInterfaceNode->aau1RequestOption[u2AppId][pOptionParam->
                                                           u2Code] =
                    (UINT1) (pOptionParam->u2Code);

                /*Set the Application Id if the Information of the code exists on the interface */
                pDhcp6ClntOptionInfo =
                    D6ClOptGetGlobOptionInfo (u4IfIndex, pOptionParam->u2Code);

                if (pDhcp6ClntOptionInfo != NULL)
                {
                    DHCP6_CLNT_SET_MEMBER_APP (pDhcp6ClntOptionInfo->
                                               au1ApplicationMap, u2AppId);
                    break;
                }
                /*Add the Option in the global and Interface Table */
                pDhcp6ClntRxOptionInfo = D6ClOptCreateNode ();
                if (pDhcp6ClntRxOptionInfo == NULL)
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }
                pDhcp6ClntRxOptionInfo->u2Type = pOptionParam->u2Code;
                pDhcp6ClntRxOptionInfo->u4IfIndex = u4IfIndex;
                pDhcp6ClntRxOptionInfo->u2Length = 0;
                DHCP6_CLNT_SET_MEMBER_APP (pDhcp6ClntRxOptionInfo->
                                           au1ApplicationMap, u2AppId);

                if (D6ClOptAddOptionNode (pDhcp6ClntRxOptionInfo) ==
                    OSIX_FAILURE)
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }

                break;
            case DHCP6_CLNT_USER_CLASS_VALUE:

                if (pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    u2Length != 0)
                {
                    pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                        u2Length = 0;
                    MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                  (UINT1 *) pInterfaceNode->
                                  UserClass[u2AppId][pOptionParam->u2Code].
                                  pu1Value);
                    pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                        pu1Value = NULL;
                }
                pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    u2Code = pOptionParam->u2Code;
                pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    u2Length = pOptionParam->u2ValueLength;
                pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    pu1Value =
                    (UINT1 *) MemBuddyAlloc ((UINT1)
                                             DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                             (pOptionParam->u2ValueLength));
                if (pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    pu1Value == NULL)
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }
                if (pOptionParam->u2ValueLength <= DHCP6_CLNT_OPTION_SIZE_MAX)
                {
                    MEMCPY (pInterfaceNode->
                            UserClass[u2AppId][pOptionParam->u2Code].
                            pu1Value, pOptionParam->au1Value,
                            pOptionParam->u2ValueLength);
                }

                break;
            case DHCP6_CLNT_VENDOR_CLASS_VALUE:

                if (pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    u2Length != 0)
                {
                    pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                        u2Length = 0;
                    MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                  (UINT1 *) pInterfaceNode->
                                  VendorClass[u2AppId][pOptionParam->u2Code].
                                  pu1Value);
                    pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                        pu1Value = NULL;
                }

                pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    u2Code = pOptionParam->u2Code;
                pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    u2Length = pOptionParam->u2ValueLength;
                pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    pu1Value =
                    (UINT1 *) MemBuddyAlloc ((UINT1)
                                             DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                             (pOptionParam->u2ValueLength));
                if (pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    pu1Value == NULL)
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }

                if (pOptionParam->u2ValueLength <= DHCP6_CLNT_OPTION_SIZE_MAX)
                {
                    MEMCPY (pInterfaceNode->
                            VendorClass[u2AppId][pOptionParam->u2Code].
                            pu1Value, pOptionParam->au1Value,
                            pOptionParam->u2ValueLength);
                }
                break;
            case DHCP6_CLNT_VENDOR_SPECF_VALUE:

                if (pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    u2Length != 0)
                {
                    pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                        u2Length = 0;
                    MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                  (UINT1 *) pInterfaceNode->
                                  VendorSpec[u2AppId][pOptionParam->u2Code].
                                  pu1Value);
                    pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                        pu1Value = NULL;
                }

                pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    u2Code = pOptionParam->u2Code;
                pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    u2Length = pOptionParam->u2ValueLength;
                pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    pu1Value =
                    (UINT1 *) MemBuddyAlloc ((UINT1)
                                             DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                                             (pOptionParam->u2ValueLength));
                if (pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    pu1Value == NULL)
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }
                if (pOptionParam->u2ValueLength <= DHCP6_CLNT_OPTION_SIZE_MAX)
                {
                    MEMCPY (pInterfaceNode->
                            VendorSpec[u2AppId][pOptionParam->u2Code].
                            pu1Value, pOptionParam->au1Value,
                            pOptionParam->u2ValueLength);
                }
                break;
            case DHCP6_CLNT_OPTION_CODE:
                pInterfaceNode->aau1Option[u2AppId][pOptionParam->u2Code] =
                    pOptionParam->u2Code;
                break;

            default:
                D6ClApiUnLock ();
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClApiOptionSet: Function Return Failure. \r\n");
                return OSIX_FAILURE;
                break;
        }
    }
    else if (pOptionParam->u1Action == (UINT1) DHCP6_CLNT_APP_INFO_REMOVE)
    {
        switch (pOptionParam->u1OptionType)
        {
            case DHCP6_CLNT_REQUEST_OPTION_CODE:
                /* Preferance Option Code and server Id code are invalid in
                 * option Request Code*/
                if ((pOptionParam->u2Code ==
                     DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE)
                    || (pOptionParam->u2Code ==
                        DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE)
                    || (pOptionParam->u2Code ==
                        DHCP6_CLNT_STATUS_CODE_OPTION_TYPE))
                {
                    D6ClApiUnLock ();
                    return OSIX_FAILURE;
                }

                pInterfaceNode->aau1RequestOption[u2AppId][pOptionParam->
                                                           u2Code] = 0;

                pDhcp6ClntOptionInfo =
                    D6ClOptGetGlobOptionInfo (u4IfIndex, pOptionParam->u2Code);

                if (pDhcp6ClntOptionInfo != NULL)
                {
                    DHCP6_CLNT_RESET_MEMBER_APP (pDhcp6ClntOptionInfo->
                                                 au1ApplicationMap, u2AppId);
                }
                break;
            case DHCP6_CLNT_USER_CLASS_VALUE:

                pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    u2Code = 0;
                pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    u2Length = 0;
                MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                              (UINT1 *) pInterfaceNode->
                              UserClass[u2AppId][pOptionParam->u2Code].
                              pu1Value);

                pInterfaceNode->UserClass[u2AppId][pOptionParam->u2Code].
                    pu1Value = NULL;
                break;
            case DHCP6_CLNT_VENDOR_CLASS_VALUE:

                pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    u2Code = 0;
                pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    u2Length = 0;
                MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                              (UINT1 *) pInterfaceNode->
                              VendorClass[u2AppId][pOptionParam->u2Code].
                              pu1Value);
                pInterfaceNode->VendorClass[u2AppId][pOptionParam->u2Code].
                    pu1Value = NULL;
                break;
            case DHCP6_CLNT_VENDOR_SPECF_VALUE:

                pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    u2Code = 0;
                pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    u2Length = 0;
                MemBuddyFree ((UINT1) DHCP6_CLNT_BUDDY_MEM_POOL_ID,
                              (UINT1 *) pInterfaceNode->
                              VendorSpec[u2AppId][pOptionParam->u2Code].
                              pu1Value);
                pInterfaceNode->VendorSpec[u2AppId][pOptionParam->u2Code].
                    pu1Value = NULL;
                break;
            case DHCP6_CLNT_OPTION_CODE:
                pInterfaceNode->aau1Option[u2AppId][pOptionParam->u2Code] = 0;
                break;
            default:
                D6ClApiUnLock ();
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CONTROL_PLANE_TRC,
                                "D6ClApiOptionSet: Function Return Failure. \r\n");
                return OSIX_FAILURE;
                break;
        }
    }
    else
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionSet: Invalid Action Value.\r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }
    D6ClApiUnLock ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : D6ClApiOptionGet                                        *
 *                                                                            *
 * DESCRIPTION      : This API is called by the application to get the        * 
 *                    configuration information from DHCP6 Client.            *
 *                                                                            *
 * INPUT            : u2AppId - Application Id                                *
 *                    u4IfIndex - Interface Index                             *
 *                    pOption - Pointer to Option                             *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_TRUE / OSIX_FALSE                                  *
 *                                                                            *
 ******************************************************************************/

PUBLIC INT4
D6ClApiOptionGet (UINT2 u2AppId, UINT4 u4IfIndex, tDhcp6OptionInfo * pOption)
{
    tDhcp6ClntIfInfo   *pInterfaceNode = NULL;
    tDhcp6ClntOptionInfo *pDhcp6ClntOptionInfo = NULL;
    UINT1               u1Result = OSIX_FAILURE;

    /* Check for System Status */
    if (DHCP6_CLNT_IS_INITIALISED != OSIX_TRUE)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_INIT_SHUT_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionGet: Dhcp6 Client Systen is"
                        "not initialised. \r\n");
        return OSIX_FAILURE;
    }

    if ((u2AppId == 0) || (u2AppId >= (UINT2) MAX_D6CL_CLNT_APPS))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiOptionGet: Invalid Application Id. \r\n");
        return OSIX_FAILURE;
    }

    if (pOption == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC,
                        "D6ClApiOptionGet: Function Return Failure. \r\n");
        return OSIX_FAILURE;
    }

    if ((pOption->u2Code == 0) ||
        (pOption->u2Code == DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE) ||
        (pOption->u2Code == DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE) ||
        (pOption->u2Code == DHCP6_CLNT_STATUS_CODE_OPTION_TYPE))
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC | DHCP6_CLNT_BUFFER_TRC,
                        "D6ClApiOptionGet: Invalid Option Type. \r\n");
        return OSIX_FAILURE;
    }
    pInterfaceNode = D6ClIfGetInterfaceInfo (u4IfIndex);

    if (pInterfaceNode == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionGet: Interface does not exits. \r\n");
        return OSIX_FAILURE;
    }

    D6ClApiLock ();
    if (gDhcp6ClntGblInfo.apAppParam[u2AppId] == NULL)
    {
        DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                        DHCP6_CLNT_CONTROL_PLANE_TRC,
                        "D6ClApiOptionGet: Application Id not Registered. \r\n");
        D6ClApiUnLock ();
        return OSIX_FAILURE;
    }
    pDhcp6ClntOptionInfo =
        D6ClOptGetGlobOptionInfo (u4IfIndex, pOption->u2Code);

    if ((pDhcp6ClntOptionInfo != NULL) && (pDhcp6ClntOptionInfo->u2Length != 0))
    {
        pOption->u2Length = pDhcp6ClntOptionInfo->u2Length;
        if (pOption->u2Length <= (UINT2) DHCP6_CLNT_OPTION_SIZE_MAX)
        {
            MEMCPY (pOption->au1Value, pDhcp6ClntOptionInfo->pu1Value,
                    pOption->u2Length);
        }

        /*Check If the Bit Mask is set for the Application if not set then set */
        DHCP6_CLNT_IS_MEMBER_APP (pDhcp6ClntOptionInfo->au1ApplicationMap,
                                  u2AppId, u1Result);
        if (u1Result == OSIX_FAILURE)
        {
            DHCP6_CLNT_SET_MEMBER_APP (pDhcp6ClntOptionInfo->au1ApplicationMap,
                                       u2AppId);
        }
        D6ClApiUnLock ();
        return OSIX_SUCCESS;
    }
    else
    {
        if (D6ClUtlValidateIfPropery (pInterfaceNode) == OSIX_FAILURE)
        {
            DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                            DHCP6_CLNT_CONTROL_PLANE_TRC,
                            "D6ClUtlValidateIfPropery: Function Return Failure. \r\n");
            D6ClApiUnLock ();
            return OSIX_FAILURE;
        }

        if (gDhcp6ClntGblInfo.apAppParam[u2AppId]->fpDhcp6ClntCallback == NULL)
        {
            /*Create the Interface Sem */
            if (OsixSemCrt ((UINT1 *) DHCP6_CLNT_INTERFACE_SEM_NAME,
                            &(pInterfaceNode->SemId)) != OSIX_SUCCESS)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CRITICAL_TRC,
                                "D6ClApiGetConfiguration: Semaphore creation"
                                " Failed. \r\n");
                D6ClApiUnLock ();
                return OSIX_FAILURE;
            }

            /* Semaphore is by default created with initial value 0. So GiveSem is 
             * called before using the semaphore. */

            pInterfaceNode->b1IsInterfaceLck = OSIX_TRUE;
            pInterfaceNode->u2CurrentApp = u2AppId;
            if (D6ClFormAppliTxPdu (pInterfaceNode, pOption->u2Code) ==
                OSIX_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CRITICAL_TRC,
                                "D6ClApiOptionGet: D6ClFormAppliTxPdu "
                                " Routine Failed. \r\n");
                D6ClApiUnLock ();
                OsixSemDel (pInterfaceNode->SemId);
                pInterfaceNode->SemId = 0;
                return OSIX_FAILURE;
            }
            /* Pass the Information by return parameter */
            /* Delete Semaphore */
            D6ClApiUnLock ();
            OsixSemTake (pInterfaceNode->SemId);
            D6ClApiLock ();
            OsixSemDel (pInterfaceNode->SemId);
            pInterfaceNode->SemId = 0;

            pInterfaceNode->b1IsInterfaceLck = OSIX_FALSE;
            pInterfaceNode->u2CurrentApp = 0;

            pDhcp6ClntOptionInfo =
                D6ClOptGetGlobOptionInfo (u4IfIndex, pOption->u2Code);

            if ((pDhcp6ClntOptionInfo != NULL)
                && (pDhcp6ClntOptionInfo->u2Length != 0))
            {
                pOption->u2Length = pDhcp6ClntOptionInfo->u2Length;
                if (pOption->u2Length <= (UINT2) DHCP6_CLNT_OPTION_SIZE_MAX)
                {
                    MEMCPY (pOption->au1Value, pDhcp6ClntOptionInfo->pu1Value,
                            pOption->u2Length);
                }
                D6ClApiUnLock ();
                return OSIX_SUCCESS;
            }
            D6ClApiUnLock ();
            return OSIX_FAILURE;
            /*Wait untill control return */
        }
        else
        {
            pInterfaceNode->u2CurrentApp = u2AppId;
            if (D6ClFormAppliTxPdu (pInterfaceNode, pOption->u2Code) ==
                OSIX_FAILURE)
            {
                DHCP6_CLNT_TRC (DHCP6_CLNT_ALL_FAILURE_TRC |
                                DHCP6_CLNT_CRITICAL_TRC,
                                "D6ClApiGetConfiguration: D6ClFormAppliTxPdu "
                                " Routine Failed. \r\n");
                D6ClApiUnLock ();
                return OSIX_FAILURE;
            }
            D6ClApiUnLock ();
            return OSIX_SUCCESS;
        }
    }
}

#ifdef CLI_WANTED
/******************************************************************************
 * FUNCTION NAME    : D6ClApiShowGlobalInfo                                   *
 *                                                                            *
 * DESCRIPTION      : This API is called by DHCP client or Relay to show the  * 
 *                    global information.                                     *
 *                                                                            *
 * INPUT            : CliHandle - Cli Handle Value                            *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_TRUE / OSIX_FALSE                                  *
 *                                                                            *
 ******************************************************************************/

PUBLIC VOID
D6ClApiShowGlobalInfo (tCliHandle CliHandle)
{
    DHCP6_CLNT_REGISTER_CLI_LOCK ();
    D6ClApiLock ();
    D6ClCliShowGlobalInfo (CliHandle);
    CliUnRegisterLock (CliHandle);
    D6ClApiUnLock ();
    return;
}

/******************************************************************************
 * FUNCTION NAME    : D6ClApiShowInterfaceInfo                                *
 *                                                                            *
 * DESCRIPTION      : This API is called by DHCP client or Relay to show the  * 
 *                    Interface information.                                  *
 *                                                                            *
 * INPUT            : CliHandle - Cli Handle Value                            *
 *                    i4IfIndex - Interface Index                             *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_TRUE / OSIX_FALSE                                  *
 *                                                                            *
 ******************************************************************************/

PUBLIC VOID
D6ClApiShowInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    DHCP6_CLNT_REGISTER_CLI_LOCK ();
    D6ClApiLock ();
    D6ClCliShowIfConf (CliHandle, i4IfIndex);
    CliUnRegisterLock (CliHandle);
    D6ClApiUnLock ();
    return;
}
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file d6clapi.c                       */
/*-----------------------------------------------------------------------*/
