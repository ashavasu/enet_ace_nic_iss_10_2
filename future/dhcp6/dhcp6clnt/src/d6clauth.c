/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $ $
 *
 * Description: This file contains the procedure for Authorization sub 
 *              Module.
 * ************************************************************************/

#include "d6clinc.h"

/*****************************************************************************
 * Name               : D6ClAuthValidateOption 
 *
 * Description        : This function call the HMAC-MD5 api and return the 
 *                      digest.  
 *
 * Input(s)           : pu1AuthBuf - Pointer to the received buffer
 *                      u4ByteCount - Length of DHCP PDU.
 *                      pu1Key - Key Value
 *                      u4KeyLen - Key Length 
 *                      pu1OutputDigest Digest Pointer 
 *                      
 * Output(s)          : pu1OutputDigest - Digest value
 *
 * Return Value(s)    : None
 *****************************************************************************/

PUBLIC VOID
D6ClAuthValidateOption (UINT1 *pu1AuthBuf, INT4 u4ByteCount,
                        UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1OutputDigest)
{
    arHmac_MD5 (pu1AuthBuf, u4ByteCount, pu1Key, u4KeyLen, pu1OutputDigest);
    return;
}

/*****************************************************************************
 * Name               : D6ClAuthCalculateData 
 *
 * Description        : This function call the HMAC-MD5 api and return the 
 *                      digest with the healp of key.  
 *
 * Input(s)           : pu1AuthBuf - Pointer to the received buffer
 *                      u4ByteCount - Length of DHCP PDU.
 *                      pu1Key - Key Value
 *                      u4KeyLen - Key Length 
 *                      pu1OutputDigest Digest Pointer 
 *                      
 * Output(s)          : pu1OutputDigest - Digest value
 *
 * Return Value(s)    : None
 *****************************************************************************/

PUBLIC VOID
D6ClAuthCalculateData (UINT1 *pu1AuthBuf, INT4 u4ByteCount,
                       UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1OutputDigest)
{
    arHmac_MD5 (pu1AuthBuf, u4ByteCount, pu1Key, u4KeyLen, pu1OutputDigest);
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6clauth.c                       */
/*-----------------------------------------------------------------------*/
