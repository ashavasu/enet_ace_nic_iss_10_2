/* $Id: d6clsz.h,v 1.2 2012/06/26 13:03:03 siva Exp $ */
enum {
    MAX_D6CL_CLNT_APPS_SIZING_ID,
    MAX_D6CL_CLNT_IFACES_SIZING_ID,
    MAX_D6CL_CLNT_OPTION_SIZING_ID,
    MAX_D6CL_CLNT_PKTINFO_SIZING_ID,
    MAX_D6CL_CLNT_QMSGS_SIZING_ID,
    D6CL_MAX_SIZING_ID
};


#ifdef  _D6CLSZ_C
tMemPoolId D6CLMemPoolIds[ D6CL_MAX_SIZING_ID];
INT4  D6clSizingMemCreateMemPools(VOID);
VOID  D6clSizingMemDeleteMemPools(VOID);
INT4  D6clSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _D6CLSZ_C  */
extern tMemPoolId D6CLMemPoolIds[ ];
extern INT4  D6clSizingMemCreateMemPools(VOID);
extern VOID  D6clSizingMemDeleteMemPools(VOID);
extern INT4  D6clSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _D6CLSZ_C  */


#ifdef  _D6CLSZ_C
tFsModSizingParams FsD6CLSizingParams [] = {
{ "tDhcp6ClntAppParam", "MAX_D6CL_CLNT_APPS", sizeof(tDhcp6ClntAppParam),MAX_D6CL_CLNT_APPS, MAX_D6CL_CLNT_APPS,0 },
{ "tDhcp6ClntIfInfo", "MAX_D6CL_CLNT_IFACES", sizeof(tDhcp6ClntIfInfo),MAX_D6CL_CLNT_IFACES, MAX_D6CL_CLNT_IFACES,0 },
{ "tDhcp6ClntOptionInfo", "MAX_D6CL_CLNT_OPTION", sizeof(tDhcp6ClntOptionInfo),MAX_D6CL_CLNT_OPTION, MAX_D6CL_CLNT_OPTION,0 },
{ "tDhcp6ClntPktInfo", "MAX_D6CL_CLNT_PKTINFO", sizeof(tDhcp6ClntPktInfo),MAX_D6CL_CLNT_PKTINFO, MAX_D6CL_CLNT_PKTINFO,0 },
{ "tDhcp6ClntQMsg", "MAX_D6CL_CLNT_QMSGS", sizeof(tDhcp6ClntQMsg),MAX_D6CL_CLNT_QMSGS, MAX_D6CL_CLNT_QMSGS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _D6CLSZ_C  */
extern tFsModSizingParams FsD6CLSizingParams [];
#endif /*  _D6CLSZ_C  */


