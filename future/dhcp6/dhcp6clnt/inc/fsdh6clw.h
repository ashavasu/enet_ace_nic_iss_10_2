/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6clw.h,v 1.4 2009/10/28 07:09:52 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6ClntTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6ClntDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6ClntSysLogAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsDhcp6ClntListenPort ARG_LIST((INT4 *));

INT1
nmhGetFsDhcp6ClntTransmitPort ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6ClntTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6ClntDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6ClntSysLogAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsDhcp6ClntListenPort ARG_LIST((INT4 ));

INT1
nmhSetFsDhcp6ClntTransmitPort ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6ClntTrapAdminControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6ClntDebugTrace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6ClntSysLogAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntListenPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntTransmitPort ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6ClntTrapAdminControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6ClntDebugTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6ClntSysLogAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6ClntListenPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6ClntTransmitPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6ClntIfTable. */
INT1
nmhValidateIndexInstanceFsDhcp6ClntIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6ClntIfTable  */

INT1
nmhGetFirstIndexFsDhcp6ClntIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6ClntIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsDhcp6ClntIfSrvAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6ClntIfDuidType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfDuid ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6ClntIfDuidIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfMaxRetCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfMaxRetDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfMaxRetTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfInitRetTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfCurrRetTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfMinRefreshTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfCurrRefreshTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfRealmName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6ClntIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6ClntIfKeyId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfInformOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfReplyIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfInvalidPktIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfHmacFailCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6ClntIfCounterRest ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6ClntIfDuidType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfDuidIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfMaxRetCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfMaxRetDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfMaxRetTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfInitRetTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfMinRefreshTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDhcp6ClntIfRealmName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6ClntIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6ClntIfKeyId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDhcp6ClntIfCounterRest ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6ClntIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6ClntIfDuidType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfDuidIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfMaxRetCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfMaxRetDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfMaxRetTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfInitRetTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfMinRefreshTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDhcp6ClntIfRealmName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6ClntIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6ClntIfKeyId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDhcp6ClntIfCounterRest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6ClntIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6ClntIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6ClntOptionTable. */
INT1
nmhValidateIndexInstanceFsDhcp6ClntOptionTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6ClntOptionTable  */

INT1
nmhGetFirstIndexFsDhcp6ClntOptionTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6ClntOptionTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6ClntOptionLength ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsDhcp6ClntOptionValue ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
