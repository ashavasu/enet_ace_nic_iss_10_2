/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains header files included in DHCP6 Client Module. 
 *
 **************************************************************************/
#ifndef  _DHCP6_CLNT_INC_H
#define  _DHCP6_CLNT_INC_H

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "snmputil.h"
#include "fssnmp.h" 

#include "trace.h"
#include "cli.h"
#include "osxstd.h"
#include "utldll.h"
#include "utlmacro.h"
#include "utilcli.h"
#include "fssocket.h"
#include "tcp.h"
#include "fssyslog.h"
#include "fsbuddy.h"
#include "ipv6.h"
#include "ip6util.h"

#ifdef L3_SWITCHING_WANTED
#include "npapi.h"
#include "ipnp.h"
#endif
#include "arHmac_api.h"
#include "d6clcli.h"
#include "dhcp6.h"
#include "d6clcons.h"
#include "d6cltdfs.h"
#include "d6clport.h"
#include "d6clprot.h"
#include "d6clmacs.h"
#include "d6cltrc.h"
#include "d6cltrap.h"
#include "fsdh6clw.h"
#include "fsdh6cwr.h"
#include "d6clextn.h"
#include "d6clsz.h"


#ifdef DHCP6_CLNT_TEST_WANTED
#include "d6cltest.h"
#endif

#endif

