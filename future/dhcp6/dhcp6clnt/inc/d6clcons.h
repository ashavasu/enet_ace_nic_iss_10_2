/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains constant definitions used in 
 *              DHCP6 Client Module. 
 *
 **************************************************************************/

#ifndef _DHCP6_CLNT_CONS_H
#define _DHCP6_CLNT_CONS_H

/*****************************************************************************/
/*Various Possible Events*/
#define DHCP6_CLNT_EV_DHCP6_PDU_EVENT           0x01
#define DHCP6_CLNT_EV_DHCP6_MSG_IN_QUEUE        0x02
#define DHCP6_CLNT_EV_TMR_EXP                   0x04
#define DHCP6_CLNT_EV_ALL                       (DHCP6_CLNT_EV_DHCP6_PDU_EVENT|\
                                                DHCP6_CLNT_EV_DHCP6_MSG_IN_QUEUE|\
                                                DHCP6_CLNT_EV_TMR_EXP)
/*****************************************************************************/
/* System Specific Constants */
#define DHCP6_CLNT_CFG_QUEUE_NAME                "D6CLQ"
#define DHCP6_CLNT_TASK_NAME                     "D6CL"
#define DHCP6_CLNT_INTERFACE_POOL                1       
#define DHCP6_CLNT_OPTION_POOL                   2       
#define INVALID_POOL_ID                          0
/*****************************************************************************/
/*Maxmium Message Length*/
#define DHCP6_CLNT_MAX_PDU_SIZE                  1536
/*****************************************************************************/
/* Various Timer Types  */
#define DHCP6_CLNT_TMR_RE_TRANSMISSION           0   /* RT timer */
#define DHCP6_CLNT_TMR_REFRESH_TIMER             1   /* Refresh Timer
                                                    */
#define DHCP6_CLNT_TMR_COVERT_SEC                1000   
#define DHCP6_CLNT_TMR_COVERT_MSEC               1000   
/*****************************************************************************/
/* Default Values, min max */
#define DHCP6_CLNT_DUID_SIZE_MAX                         128 
#define DHCP6_CLNT_REALM_SIZE_MAX                        128
#define DHCP6_CLNT_KEY_SIZE_MAX                          64
#define DHCP6_CLNT_MIN_RETRANS_COUNT                     0
#define DHCP6_CLNT_MAX_RETRANS_COUNT                     10
#define DHCP6_CLNT_MIN_RETRANS_DELAY                     0
#define DHCP6_CLNT_MAX_RETRANS_DELAY                     100
#define DHCP6_CLNT_MIN_RETRANS_TIME                      0
#define DHCP6_CLNT_MAX_RETRANS_TIME                      120
#define DHCP6_CLNT_INIT_MIN_RETRANS_TIME                 1 
#define DHCP6_CLNT_INIT_MAX_RETRANS_TIME                 255
#define DHCP6_CLNT_MIN_REFRESH_TIME                      600
#define DHCP6_CLNT_MAX_REFRESH_TIME                      0xffffffff 
#define DHCP6_CLNT_DEFAULT_REFRESH_TIME                  86400
#define DHCP6_CLNT_DEFAULT_KEY_ID                        1
#define DHCP6_CLNT_MIN_KEY_ID                            1
#define DHCP6_CLNT_MAX_REALM_SIZE                        128
#define DHCP6_CLNT_MAX_KEY_SIZE                          64
#define DHCP6_CLNT_MIN_REALM_SIZE                        1
#define DHCP6_CLNT_MIN_KEY_SIZE                          1
#define DHCP6_CLNT_USER_INIT_REQUEST                     1
#define DHCP6_CLNT_REFRESH_TMR_EXP_REQUEST               2
#define DHCP6_CLNT_RT_TMR_EXP_REQUEST                    3
#define DHCP6_CLNT_APP_INIT_REQUEST                      4
#define DHCP6_CLNT_AUTH_SIZE_MAX                         (DHCP6_CLNT_REALM_SIZE_MAX \
                                                         +DHCP6_CLNT_KEY_ID_LENGTH \
                                                         +DHCP6_CLNT_ALGO_DIGEST_SIZE)
#define DHCP6_CLNT_STATUS_CODE_SIZE_MAX                  20
#define DHCP6_CLNT_MAX_IDENTIFIER_SIZE                   122
#define DHCP6_CLNT_INFORM_MESSAGE_TYPE                   0x0b000000
#define DHCP6_CLNT_REPLY_MESSAGE_TYPE                   7
#define DHCP6_CLNT_DEF_INIT_RETRANSMISSION_TIME          1
#define DHCP6_CLNT_DEF_MAX_RETRANSMISSION_COUNT          0
#define DHCP6_CLNT_DEF_INIT_MAX_RETRANSMISSION_TIME      120
#define DHCP6_CLNT_DEF_MAX_RETRANSMISSION_DELAY          0
#define DHCP6_CLNT_MIN_SOURCE_PORT_NUMBER                1
#define DHCP6_CLNT_MAX_SOURCE_PORT_NUMBER                65535 
#define DHCP6_CLNT_MIN_DEST_PORT_NUMBER                  1
#define DHCP6_CLNT_MAX_DEST_PORT_NUMBER                  65535
#define DHCP6_CLNT_DEFAULT_SOURCE_PORT                   546
#define DHCP6_CLNT_DEFAULT_DEST_PORT                     547
#define DHCP6_CLNT_DEFAULT_DUID_IF_INDEX                 1
#define DHCP6_CLNT_MAX_RAND_RANGE                        1
#define DHCP6_CLNT_MIN_RAND_RANGE                       -1
#define DHCP6_CLNT_RAND_NUMBER_SIZE                      2
#define DHCP6_CLNT_USER_CLASS_LENGTH                     2
#define DHCP6_CLNT_VENDOR_CLASS_LENGTH                   2
#define DHCP6_CLNT_VENDOR_SPEC_LENGTH                    2
#define DHCP6_CLNT_VENDOR_SPEC_CODE_LENGTH               2
#define DHCP6_CLNT_TRANS_ID_SIZE                         3
/*****************************************************************************/
/*Common  TLV Values*/
#define  DHCP6_CLNT_TLV_TYPE_SIZE                        2
#define  DHCP6_CLNT_TLV_LEN_SIZE                         2
/*****************************************************************************/
/*Option  TLV Values*/
#define  DHCP6_CLNT_CLIENT_IDENTIFIER_OPTION_TYPE        1
#define  DHCP6_CLNT_SERVER_IDENTIFIER_OPTION_TYPE        2
#define  DHCP6_CLNT_OPTION_REQUEST_OPTION_TYPE           6
#define  DHCP6_CLNT_IA_NA_OPTION_TYPE                    3
#define  DHCP6_CLNT_IA_TA_OPTION_TYPE                    4
#define  DHCP6_CLNT_IA_ADDR_OPTION_TYPE                  5
#define  DHCP6_CLNT_PREFERENCE_IDENTIFIER_OPTION_TYPE    7
#define  DHCP6_CLNT_ELPASED_OPTION_TYPE                  8
#define  DHCP6_CLNT_AUTH_IDENTIFIER_OPTION_TYPE          11
#define  DHCP6_CLNT_STATUS_CODE_OPTION_TYPE              13
#define  DHCP6_CLNT_USER_CLASS_CODE_OPTION_TYPE          15
#define  DHCP6_CLNT_VENDOR_CLASS_CODE_OPTION_TYPE        16
#define  DHCP6_CLNT_VENDOR_SPECIFIC_OPTION_TYPE          17
#define  DHCP6_CLNT_REFRESH_TIMER_OPTION_TYPE            32
#define  DHCP6_CLNT_AUTH_FIXED_LEN                       11
/*****************************************************************************/
/* Scalability Constants */

#define DHCP6_CLNT_MIN_IFACES                          1
#define DHCP6_CLNT_VENDOR_CLASS_MAX                    32 
#define DHCP6_CLNT_USER_CLASS_MAX                      32 
#define DHCP6_CLNT_VENDOR_SPEC_MAX                     32 
#define DHCP6_CLNT_REQ_OPT_MAX                         32 
#define DHCP6_CLNT_OPT_MAX                             32 
#define DHCP6_CLNT_BUDDY_SIZE_MIN                      8 
#define DHCP6_CLNT_BUDDY_SIZE_MAX                      256 
#define DHCP6_CLNT_BUDDY_BLK_MAX                      (MAX_D6CL_CLNT_OPTION*MAX_D6CL_CLNT_IFACES)
/*****************************************************************************/
/*Message Receivce Event*/
#define DHCP6_CLNT_IP6_IF_STAUS_CHG_MSG                 1
/*****************************************************************************/
#define DHCP6_CLNT_INITIALIZE_ZERO                      0
#define DHCP6_CLNT_INITIALIZE_ONE                       1
#define DHCP6_CLNT_INDEX_BY_ZERO                        0
#define DHCP6_CLNT_INDEX_BY_ONE                         1
#define DHCP6_CLNT_INDEX_BY_TWO                         2
#define DHCP6_CLNT_INDEX_BY_THREE                       3 
#define DHCP6_CLNT_INDEX_BY_FOUR                        4
#define DHCP6_CLNT_INDEX_BY_FIVE                        5 
#define DHCP6_CLNT_INDEX_BY_SIX                         6
#define DHCP6_CLNT_INDEX_BY_SEVEN                       7
#define DHCP6_CLNT_INDEX_BY_EIGHT                       8
#define DHCP6_CLNT_INDEX_BY_TWELVE                      12
#define DHCP6_CLNT_INDEX_BY_THIRTEEN                    13
#define DHCP6_CLNT_INDEX_BY_FOURTEEN                    14
#define DHCP6_CLNT_INDEX_BY_FIFTEEN                     15
#define DHCP6_CLNT_VALUE_ZERO                           0
#define DHCP6_CLNT_VALUE_ONE                            1
#define DHCP6_CLNT_VALUE_TWO                            2
#define DHCP6_CLNT_VALUE_THREE                          3
#define DHCP6_CLNT_VALUE_SEVEN                          7
#define DHCP6_CLNT_TWO_BYTE_LENGTH                      2
#define DHCP6_CLNT_THREE_BYTE_LENGTH                    3
#define DHCP6_CLNT_FOUR_BYTE_LENGTH                     4
#define DHCP6_CLNT_FIVE_BYTE_LENGTH                     5
#define DHCP6_CLNT_16BYTE_LENGTH                        16
#define DHCP6_CLNT_18BYTE_LENGTH                        18
#define DHCP6_CLNT_23BYTE_LENGTH                        23
#define DHCP6_CLNT_28BYTE_LENGTH                        28
#define DHCP6_CLNT_31BYTE_LENGTH                        31
#define DHCP6_CLNT_41BYTE_LENGTH                        41
#define DHCP6_CLNT_43BYTE_LENGTH                        43
#define DHCP6_CLNT_47BYTE_LENGTH                        47
#define DHCP6_CLNT_50BYTE_LENGTH                        50
#define DHCP6_CLNT_56BYTE_LENGTH                        56
#define DHCP6_CLNT_58BYTE_LENGTH                        58

#define DHCP6_CLNT_SHIFT_24BITS                         24
#define DHCP6_CLNT_SHIFT_16BITS                         16
#define DHCP6_CLNT_SHIFT_12BITS                         12
#define DHCP6_CLNT_SHIFT_8BITS                          8
#define DHCP6_CLNT_SHIFT_4BITS                          4
#define DHCP6_CLNT_DECR_TWO                             2
#define DHCP6_CLNT_DECR_ONE                             1
#define DHCP6_CLNT_FOUR_BYTE_KEY_ID                     4
#define DHCP6_CLNT_INCR_ONE                             1
#define DHCP6_CLNT_MULTIPLE_TWO                         2
#define DHCP6_CLNT_MINUS_ONE                            -1
#define DHCP6_CLNT_DUMP_BUF_SIZE                        100
#define DHCP6_CLNT_KEY_ID_LENGTH                        4
#define DHCP6_CLNT_MAX_TRANS_BYTE                       3
#define DHCP6_CLNT_VALIDATE_MES_TYPE                    0x07000000
#define DHCP6_CLNT_REPLY_MES_TYPE                       0x07000000
#define DHCP6_CLNT_VALIDATE_TRANS_ID                    0x00ffffff
#define DHCP6_CLNT_MAX_ELAPSED_TIME                     0xffff
#define DHCP6_CLNT_MAX_IF_NAME_LEN        IP6_MAX_IF_NAME_LEN
/*****************************************************************************/
/*Some Protocol constants*/
#define DHCP6_CLNT_ELAPSED_TIME_FIXED_LEN                2
#define DHCP6_CLNT_ENTERPRISE_LENGTH                     4
#define DHCP6_CLNT_DELAYED_AUTH_PROTOCOL                 2 
#define DHCP6_CLNT_AUTH_KEYID_SIZE                       4
#define DHCP6_CLNT_AUTH_RD_SIZE                          8
#define DHCP6_CLNT_AUTH_RDM_SIZE                         1
#define DHCP6_CLNT_AUTH_ALGO_SIZE                        1
#define DHCP6_CLNT_AUTH_PROTO_SIZE                       1
#define DHCP6_CLNT_HMAC_MD5_ALGORITHM                    1 
#define DHCP6_CLNT_RDM_VALUE                             0
#define DHCP6_CLNT_REPLAY_DET_VALUE                      8
#define DHCP6_CLNT_MIN_AUTH_LENGTH                       11
#define DHCP6_CLNT_STATUS_CODE_SUCCESS                   0
#define DHCP6_CLNT_STATUS_CODE_UNSPECFIC_VALUE           1
#define DHCP6_CLNT_STATUS_CODE_USE_MULTICAST             5
#define DHCP6_CLNT_ALGO_DIGEST_SIZE                      16
#define DHCP6_CLNT_CLIENT_ENABLE                         1 
#define DHCP6_CLNT_CLIENT_DISABLE                        2
#define DHCP6_CLNT_CLEAR_COUNTER_INTERFACE                     1
#define DHCP6_CLNT_SNMP_SUCCESS                          1
#define DHCP6_CLNT_SNMP_FAILURE                          2
#define DHCP6_CLNT_TRAP_CONTROL_MAX_VAL                  0x03
#define DHCP6_CLNT_LINK_LAYER_ADD_PLUS_CURRENT_TIME      1
#define DHCP6_CLNT_VENDOR_ASSIGNED_UNIQUE_ID             2
#define DHCP6_CLNT_LINK_LAYER_ADD                        3
#define DHCP_MAX_TRANS_ID_VAL                            0x00FFFFFF
#define DHCP_MIN_TRANS_ID_VAL                            0x00000001
#define DHCP6_CLNT_BASIC_OPTION_LENGTH                   8
#define DHCP6_CLNT_SIP_SERVER_DOMAIN_NAME                21
#define DHCP6_CLNT_SIP_SERVER_ADDRESS_LIST               22
#define DHCP6_CLNT_DNS_RECUSIVE_NAME                     23
#define DHCP6_CLNT_DNS_SEARCH_LIST                       24
#define DHCP6_CLNT_APP_PER_BYTE                          8
#define DHCP6_CLNT_DUID_MAX_ARRAY_SIZE                   (DHCP6_CLNT_DUID_SIZE_MAX + 1)
#define DHCP6_CLNT_REALM_MAX_ARRAY_SIZE                  (DHCP6_CLNT_REALM_SIZE_MAX + 1)
#define DHCP6_CLNT_KEY_MAX_ARRAY_SIZE                    (DHCP6_CLNT_KEY_SIZE_MAX + 1)
#define DHCP6_CLNT_OPT_MAX_ARRAY_SIZE                    (DHCP6_CLNT_OPTION_SIZE_MAX + 1)
#define DHCP6_CLNT_STATUS_CODE_MAX_ARRAY_SIZE            (DHCP6_CLNT_STATUS_CODE_SIZE_MAX + 1)
#define DHCP6_CLNT_AUTH_CODE_MAX_ARRAY_SIZE              (DHCP6_CLNT_AUTH_SIZE_MAX \
                                                          +DHCP6_CLNT_AUTH_FIXED_LEN + 1)
#define DHCP6_CLNT_RB_GREATER                             1
#define DHCP6_CLNT_RB_EQUAL                               0
#define DHCP6_CLNT_RB_LESS                                -1
#define DHCP6_CLNT_IP6_ADDRESS_SIZE_MAX                   17

/*****************************************************************************/
/*Porting Macro*/
#define DHCP6_CLNT_STATUS_CODE_VAL                     "Success"
#define DHCP6_CLNT_STATUS_CODE_VAL_LENGTH              8
#define DHCP6_CLNT_GET_HADWARE_TYPE                    2
#define DHCP6_CLNT_GET_ENTERPRISE_NUMBER               122
#define DHCP6_CLNT_GET_IDENTIFIER_LENGTH               11
#define DHCP6_CLNT_GET_IDENTIFIER_VALUE                "GURGARICENT"

#endif
