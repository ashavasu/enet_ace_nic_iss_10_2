/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * Description: This file contains macros for snmp traps.
 *************************************************************************/


#ifndef  _DHCP6_CLNT_TRAP_H
#define  _DHCP6_CLNT_TRAP_H

PUBLIC VOID D6ClTrapSnmpSend PROTO((VOID *, UINT1));

typedef struct _Dhcp6CInvalidMsgRxdTrap{
  UINT4   u4IfIndex;
}tDhcp6CInvalidMsgRxdTrap;    

typedef struct _Dhcp6CHmacFailMsgRxdTrap{
  UINT4   u4IfIndex;
}tDhcp6CHmacFailMsgRxdTrap;    



#define DHCP6_CLNT_TRAPS_OID                     "1.3.6.1.4.1.29601.2.361"
#define DHCP6_CLNT_MIB_OBJ_ERR_MSG_TRAP          "fsDhcp6ClntIfInvalidPktIn"
#define DHCP6_CLNT_MIB_OBJ_AUTH_FAIL_MSG_TRAP    "fsDhcp6ClntIfHmacFailCount"

#define DHCP6_CLNT_VAL_10                                   10
#define DHCP6_CLNT_4BIT_MAX                                 0xf
#define DHCP6_CLNT_ARRAY_SIZE_257                           257
#define DHCP6_CLNT_ARRAY_SIZE_256                           256
#define DHCP6_CLNT_ARRAR_SIZE_68                           68

#define DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_VAL                1
#define DHCP6_CLNT_RCVD_HMAC_AUTH_FAIL_TRAP_VAL             2

#define DHCP6_CLNT_RCVD_INVALID_MSG_TRAP_DISABLED()\
((DHCP6_CLNT_TRAP_CONTROL_OPTION & 0x0001)?(OSIX_FALSE):(OSIX_TRUE))

#define DHCP6_CLNT_HMAC_AUTH_FAIL_TRAP_DISABLED()\
((DHCP6_CLNT_TRAP_CONTROL_OPTION & 0x0002)?(OSIX_FALSE):(OSIX_TRUE))

#define DHCP6_CLNT_INVALID_PKT_REC_TRAP_ENABLED(u1Trap)\
           ((u1Trap & 0x01)?(OSIX_TRUE):(OSIX_FALSE))
#define DHCP6_CLNT_HMAC_FAIL_TRAP_ENABLED(u1Trap)\
           ((u1Trap & 0x02)?(OSIX_TRUE):(OSIX_FALSE))

#define DHCP6_CLNT_ALL_TRAP_TRAP_ENABLED(u1Trap)\
           ((u1Trap  == 0x03 ) ?(OSIX_TRUE):(OSIX_FALSE))


#endif
