/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: d6cltrc.h,v 1.5 2010/12/14 10:27:48 siva Exp $
 *
 * Description: This file contains macros for tracing and debugging.
 *
 *******************************************************************/

#ifndef  _DHCP6_CLNT_TRC_H
#define  _DHCP6_CLNT_TRC_H

PUBLIC INT4
D6ClTrcGetTraceInputValue PROTO ((UINT1 *,UINT4));
PUBLIC INT4
D6ClTrcGetTraceOptionValue PROTO ((UINT1 *,INT4));
PUBLIC VOID
D6ClTrcSetTraceOptionValue PROTO ((UINT1 *,UINT4 *));
PUBLIC VOID D6ClTrcDumpPkt PROTO ((UINT1 *,UINT4 ));


#define DHCP6_CLNT_TRC_FLAG    gDhcp6ClntGblInfo.u4DebugTrace 
#define DHCP6_CLNT_MOD_NAME    "D6CL" 

/* Trace String */
#define DHCP6_CLNT_MAX_TRC_STR_COUNT        9
#define DHCP6_CLNT_MAX_TRC_STR_LEN          15

#define DHCP6_CLNT_MAX_TRACE_TOKENS         12
#define DHCP6_CLNT_MAX_TRACE_TOKEN_SIZE     12
#define DHCP6_CLNT_TRACE_TOKEN_DELIMITER    ' '    /* space */

#define   DHCP6_CLNT_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   DHCP6_CLNT_MGMT_TRC           MGMT_TRC          
#define   DHCP6_CLNT_DATA_PATH_TRC      DATA_PATH_TRC     
#define   DHCP6_CLNT_CONTROL_PLANE_TRC   CONTROL_PLANE_TRC 
#define   DHCP6_CLNT_DUMP_TRC           DUMP_TRC          
#define   DHCP6_CLNT_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   DHCP6_CLNT_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   DHCP6_CLNT_BUFFER_TRC         BUFFER_TRC        

#define DHCP6_CLNT_INVALID_TRC   0x00000000
#define DHCP6_CLNT_CRITICAL_TRC  0x00000100

#define DHCP6_CLNT_ALL_TRC (INIT_SHUT_TRC | MGMT_TRC | \
                      CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                      ALL_FAILURE_TRC | BUFFER_TRC | DHCP6_CLNT_CRITICAL_TRC)

#define DHCP6_CLNT_MIN_TRC_VALUE INIT_SHUT_TRC
#define DHCP6_CLNT_MAX_TRC_VALUE DHCP6_TRC_CRITICAL


#ifdef TRACE_WANTED

#define DHCP6_CLNT_TRACE_WANTED

#define DHCP6_CLNT_PRINT       UtlTrcLog
#define DHCP6_CLNT_TRC_BUF_SIZE    2000

#define DHCP6_CLNT_TRC(TraceType,Str)                              \
        UtlTrcLog(DHCP6_CLNT_TRC_FLAG,TraceType,DHCP6_CLNT_MOD_NAME,Str)

#define DHCP6_CLNT_TRC_ARG1(TraceType, Str, Arg1)\
        UtlTrcLog(DHCP6_CLNT_TRC_FLAG, TraceType, DHCP6_CLNT_MOD_NAME, Str, \
                Arg1)

#define DHCP6_CLNT_TRC_ARG2(TraceType, Str, Arg1,Arg2)\
        UtlTrcLog(DHCP6_CLNT_TRC_FLAG, TraceType, DHCP6_CLNT_MOD_NAME, Str, \
                Arg1,Arg2)

#define  DHCP6_CLNT_MOD_PKT_DUMP(u4DbgVar, mask, moduleName, pBuf, Length, fmt) \
		{ UtlTrcLog(u4DbgVar, mask , moduleName, fmt); \
		  if ((u4DbgVar & mask) == DUMP_TRC)	\
			D6ClTrcDumpPkt(pBuf, Length); \
		}		

#define DHCP6_CLNT_PKT_DUMP(TraceType, pBuf, Length, Str)\
        DHCP6_CLNT_MOD_PKT_DUMP(DHCP6_CLNT_TRC_FLAG,TraceType,DHCP6_CLNT_MOD_NAME,pBuf,Length,\
                Str)
#else /* TRACE_WANTED */
#define DHCP6_CLNT_TRC(TraceType, Str)
#define DHCP6_CLNT_TRC_ARG1(TraceType, Str, Arg1)
#define DHCP6_CLNT_TRC_ARG2(TraceType, Str, Arg1,Arg2)
#define DHCP6_CLNT_PKT_DUMP(TraceType, pBuf, Length, Str)
#endif /* TRACE_WANTED */

#define DHCP6_CLNT_TRC_MIN_SIZE              1

 

#endif

