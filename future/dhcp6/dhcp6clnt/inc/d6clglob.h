/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains the global structure used in DHCP6
 *              cleint module.
 *
 ***************************************************************************/

#ifndef _DHCP6_CLNT_GLOB_H
#define _DHCP6_CLNT_GLOB_H

tDhcp6ClntGblInfo          gDhcp6ClntGblInfo;

UINT1       gau1AppBitMaskMap[DHCP6_CLNT_APP_PER_BYTE] =
            { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };

#endif

