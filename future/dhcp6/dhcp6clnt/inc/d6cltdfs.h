/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 * $Id: d6cltdfs.h,v 1.8 2012/11/22 12:01:37 siva Exp $
 * 
 * Description: This file contains data structures defined for DHCPv6
 *    client Module.
 *  
 ********************************************************************/

#ifndef  _DHCP6_CLNT_TDFS_H
#define  _DHCP6_CLNT_TDFS_H

/*------------------------------------------------------------------------
 *           Common Enumerations
 *-----------------------------------------------------------------------*/
enum
{
   DHCP6_CLNT_RETRANS_MISSION_TMR = 0,/*Retransmission Timer */
   DHCP6_CLNT_REFRESH_TMR = 1,        /*Refresh Timer */
   DHCP6_CLNT_MAX_TMR_TYPES = 2
};
/*------------------------------------------------------------------------
 * Global Info Structures
 * This structure is used to maintain the global Information required for DHCPv6
 * Client Operation.
 * -----------------------------------------------------------------------*/

typedef struct __Dhcp6ClntGblInfo
{
   tOsixTaskId          TaskId;
                          /* Main task ID. */
   tOsixQId             TaskQId;
                          /* DHCPv6 client task queue ID. */
   tOsixSemId           SemId;
                          /*DHCPv6 client task semaphore ID. */
   tTimerListId         TimerListId;
                          /* Timer list Id. */
   tOsixQId             CfgQId; 
   tDhcp6ClntAppParam   *apAppParam[MAX_D6CL_CLNT_APPS_LIMIT];
                          /* description of all applications are  stored in
                           * this array with app Id as the array index. */
   tTmrDesc             aTmrDesc[DHCP6_CLNT_MAX_TMR_TYPES];
                          /* description of all timers are stored in this
                           * array with TimerId as the array index. */
   tRBTree              IfTable;
                          /* RBTree head for tDhcp6ClntIfInfo structure.
                           * Interface information are stored in
                           * this table.*/
   tRBTree              OptionTable;
                          /* RBTree head for tDhcp6ClntOptionInfo structure.
                           * configuration options are stored in
                           * this table.*/
   UINT4                u4DebugTrace;
                          /* Trace option. */
   UINT4                u4SysLogId;
                          /* SysLog identifier.*/
   INT4                 BuddyMemPool;
                          /* Buddy pool Id.*/
   INT4                 gi4Dhcp6ClntSockId;
                          /* DHCPv6 client socket for transmittion and reception
                           * of messages. */
   UINT2                u2UdpSourcePort;
                          /* UDP destination port number.*/
   UINT2                u2UdpDestPort;
                          /* UDP source port number. */
   UINT1                u1TrapControl;
                          /* Trap enable or disable status. */
   BOOL1                b1SysLogEnabled;
                          /* Syslog enable or disable status. */
   BOOL1                b1IsInitialized;
                          /* Indicates whether DHCPv6 client module is initialised. This
                           * is to avoid posting messages to DHCPv6 module before
                           * it is initialised.*/
   UINT1                au1DhcpPdu[DHCP6_CLNT_MAX_PDU_SIZE];
                          /* Buffer for transmittion of DHCPv6 message.*/
   UINT1                u1Pad;
                          /* 4 Byte padding allignment. */
}tDhcp6ClntGblInfo;

/*------------------------------------------------------------------------
 * Interface Info Structures
 * This structure is used to maintain the Interface Information required for DHCPv6
 * Client Operation.
 * -----------------------------------------------------------------------*/

typedef struct __Dhcp6ClntIfInfo
{
   tTMO_SLL                OptionTableSll;
                             /* SLLs that are used to store all the configuration
                              * Information per interface. */
   tRBNodeEmbd             RbLink;
                             /* Embedded RBTree Node which is associated with
                              * the RBTree head IfTable. */
   tOsixSemId              SemId;
                             /* Semaphore Id for blocking option read calls. */
   tTmrBlk                 RefreshTimer;
                             /* Refresh timer block. */
   tTmrBlk                 ReTransmissionTimer;
                             /* ReTransmission timer block. */
   tOsixSysTime            TimeStamp;   
   tDhcp6ClntSubOptionInfo UserClass[MAX_D6CL_CLNT_APPS_LIMIT][DHCP6_CLNT_USER_CLASS_MAX];
                             /* User class TLV value used to store the user
                              * class per application. */
   tDhcp6ClntSubOptionInfo VendorClass[MAX_D6CL_CLNT_APPS_LIMIT][DHCP6_CLNT_VENDOR_CLASS_MAX];
                             /* Vendor class TLV value used to store the vendor
                              * class per application. */
   tDhcp6ClntSubOptionInfo VendorSpec[MAX_D6CL_CLNT_APPS_LIMIT][DHCP6_CLNT_VENDOR_SPEC_MAX];                       
                             /* Vendor specific TLV value used to store the
                              * vendor spec inforrmation per application. */
   tIp6Addr                CurrentSrvAddr;
                             /* Server address received in last reply message.*/
   UINT4                   u4RetSecRemTime;
                             /* Remaining retransmission time interval in
                              * seconds.*/
   UINT4                   u4RefSecRemTime;
                             /* Remaining refresh time interval in
                              * seconds.*/
   UINT4                   u4KeyIdentifier;
                             /* User configured key If for HMAC MD5 algorithm. */
   UINT4                   u4IfIndex;
                             /* Interfax index. */
   UINT4                   u4IfDuidIndex;
                             /* Interface index for generation of client DUID.*/
   UINT4                   u4InformOut;
                             /* Number of information request message out
                              * counter from the interface.*/
   UINT4                   u4ReplyIn;
                             /* Number of reply message received
                              * counter on the interface.*/
   UINT4                   u4HmacFailCount;
                             /* Number of Unauthenticated message received
                              * Counter on the interface.*/
   UINT4                   u4InvalidPktIn;
                             /* Number of invalid message received
                              * counter on the interface.*/
   UINT4                   u4TransactionId;
                             /* Transaction id value for information request
                              * message.*/
   UINT4                   u4RefreshTime;
                             /* Refresh timer value received on interface in
                              * reply message.*/
   UINT4                   u4MinRefreshTime;
                             /* Minimum refresh timer value on interface 
                              * configured by user.*/
   UINT4                   u4LastTransId;
                             /* Last valid transaction id of reply message.*/
   UINT4                   u4OperStatus;
                             /* Operational Status of the interface(Up/Down)*/
   UINT2                   u2MaxRetDelay;
                             /* Maxmium retransmission deley timer value.*/
   UINT2                   u2MaxRetTime;
                             /* Maxmium retransmission time value.*/
   UINT2                   u2InitRetTime;
                             /* Initial retransmission timer value.*/
   UINT2                   u2CurrentRetTime;
                             /* Current retransmission timer value.*/
   UINT2                   u2CurrentRetDelay;
                             /* Current retransmission delay value.*/
   UINT2                   u2CurrAppReqCode;
                             /* Current requested application code value.*/
   UINT2                   u2CurrentApp;
                             /* Application Id value.*/
   UINT2                   u2RetMiliSecRemTime;
                             /* Remaining retransmission time interval in
                              * mili seconds.*/
   UINT2                   u2RefMiliSecRemTime;
                             /* Remaining refresh time interval in
                              * mili seconds.*/
   UINT1                   u1MaxRetCount;
                             /* Maxmium retransmission counter */
   UINT1                   u1CurrRetCount;
                             /* Current retransmission counter */
   UINT1                   u1ClientIdType;
                             /* Client duid type (llt | en | ll)*/
   UINT1                   u1ClientIdLength;
                             /* Client DUID length.*/
   UINT1                   au1ClientId[DHCP6_CLNT_DUID_MAX_ARRAY_SIZE];
                             /* Client DUID value.*/
   UINT1                   au1Realm[DHCP6_CLNT_REALM_MAX_ARRAY_SIZE];
                             /* User configured realm value.*/
   UINT1                   u1RealmLength;
                             /* Client configured Realm length.*/
   UINT1                   au1Key[DHCP6_CLNT_KEY_MAX_ARRAY_SIZE];
                             /* User configured key value.*/
   UINT1                   u1KeyLength;
                             /* Client configured key length.*/
   UINT1                   u1PreferanceValue;
                             /* preferance value of received reply message.*/
   UINT1                   u1IfRowStatus;
                             /* Inteface Row status.*/
   UINT1                   aau1RequestOption [MAX_D6CL_CLNT_APPS_LIMIT][DHCP6_CLNT_REQ_OPT_MAX];
                             /* Option request code populated by application
                              * for option request TLV.*/
   UINT1                   aau1Option [MAX_D6CL_CLNT_APPS_LIMIT][DHCP6_CLNT_OPT_MAX];
                             /* Option TLVs information index by application id. */
   UINT1                   u1TransType;
                             /* Current transaction type 
                              * 1.User initiate 
                              * 2.Application initiate
                              * 3.Refresh timer expiry.*/
   UINT1                   au1LastRdValue[DHCP6_CLNT_AUTH_RD_SIZE];                             
   BOOL1                   b1IsInfoExists;
                             /* Same preferance value information status. */
   BOOL1                   b1IsClientIdTrsmt;
                             /* Client Id transmittion status. */
   BOOL1                   b1IsInterfaceLck;
                             /* Interface Lock status. */
   BOOL1                   b1IsRetansMessage;
                             /* Retransmission Message status. */
   BOOL1                   b1FirstInfoMessage;
                             /* First Retransmission Message status. */
   UINT1                   au1Pad[1];
                             /* 4 Byte padding allignment. */
}tDhcp6ClntIfInfo;
/*------------------------------------------------------------------------
 * Option Info Structures
 * This structure is used to maintain the configuration Information received
 * on the interface in reply messase.
 * -----------------------------------------------------------------------*/

typedef struct  __Dhcp6ClntOptionInfo
{
   tTMO_SLL_NODE        OptSllLink;
                          /* Option Link node. */
   tRBNodeEmbd          GblRBLink;
                          /* Embedded RBTree Node which is
                           * associated with the RBTree head OptionTable.*/
   UINT4                u4IfIndex;
                          /* Interface index value.*/
   UINT2                u2Type;
                          /* Option type value.*/
   UINT2                u2Length;
                          /* Option length value.*/
   UINT1                au1ApplicationMap [MAX_D6CL_CLNT_APPS_LIMIT];
                          /* Array of application mapped to this option.*/
   UINT1                *pu1Value;
                          /* Option value pointer .*/
}tDhcp6ClntOptionInfo;
/*------------------------------------------------------------------------
 * Udpheader Info Structures
 * This structure is used to comapre the source port and destination port stored
 * in global information.
 * -----------------------------------------------------------------------*/

typedef struct  __Dhcp6ClntUdpHeader
{
   tIp6Addr             srcAddr;           /* Source address in the packet */
   tIp6Addr             dstAddr;           /* Destination address in the
                                              packet */
   UINT4                u4Index;      
   UINT4                u4Len;           /* Length of application data */
   UINT2                u2SrcPort;       /* Source port number over which packet
                                         * came*/
   UINT2                u2DstPort;       /* Destination port number over which
                                         * packet is to be sent */
   INT4                 i4Hlim;          /* Hop Limit of the packet */

}tDhcp6ClntUdpHeader;
/*****************************************************************************/
/* This is the main DHCPv6 queue message structure */
typedef struct Dhcp6ClntQMsg
{ 
    UINT4               u4BitMap;
    UINT4               u4IfState;     /* Interface status*/
    UINT4               u4OperStatus;  /* Oper status of interface.*/
    UINT4               u4MsgType;     /* Message Type.*/
    UINT4               u4IfIndex;     /* Interface Index of the port*/
 }tDhcp6ClntQMsg;
/*****************************************************************************/
typedef struct Dhcp6ClntPktInfo
{
   tDhcp6ClntIfInfo     *pDhcp6ClntIfInfo;  /* Pointer to Port info table*/
   UINT4                u4RefreshTime;      /* Received refresh timer value*/
   UINT2                u2AuthInfoLength;   /* Received auth info length. */
   UINT2                u2StatusCode;       /* Received status code TLV value.*/
   UINT1                u1PreferanceVal;    /* Received preferance option TLV
                                             * value. */
   UINT1                au1AuthValue[DHCP6_CLNT_AUTH_CODE_MAX_ARRAY_SIZE];
                                            /* Received auth information.*/
   UINT1                au1StatusCodeVal[DHCP6_CLNT_STATUS_CODE_MAX_ARRAY_SIZE];
                                            /* Received status code TLV string
                                             * value. */
   UINT1                au1Value[DHCP6_CLNT_OPT_MAX_ARRAY_SIZE ];
                                            /* Received conifguration option
                                             * value. */
   UINT1                au1DuidValue[DHCP6_CLNT_DUID_MAX_ARRAY_SIZE];
   BOOL1                b1IsAuthOptReceived;/* Authnecation option TLV status. */
   BOOL1                b1IsStatusCode;     /* status code TLV status. */
   BOOL1                b1IsRefreshTlv;     /* refresh timer TLV status. */
   BOOL1                b1ClientIdRcvd;     /* client id option status. */
   BOOL1                b1ServerIdRcvd;     /* server id TLV status. */
   BOOL1                b1PreferOptionRcvd; /* preferance option TLV status.*/
   UINT1                au1Reserved[2];     /* Padding */
} tDhcp6ClntPktInfo;
/*****************************************************************************/
#endif
