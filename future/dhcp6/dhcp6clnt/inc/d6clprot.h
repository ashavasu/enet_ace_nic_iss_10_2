/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * $Id: d6clprot.h,v 1.8 2011/09/26 07:39:21 siva Exp $
 *
 * Description: This file contains the prototypes for all the
 *              PUBLIC procedures.
 *
 *
 **************************************************************************/
#ifndef  _DHCP6_CLNT_PROT_H
#define  _DHCP6_CLNT_PROT_H
/******************************************************************************/

/**Function Proto Type For d6clutl.c File*/

PUBLIC INT4 D6ClUtlCfgQueMsg PROTO ((tDhcp6ClntQMsg * ));
PUBLIC INT4 D6ClUtlGenerateRandPseudoNumber PROTO ((INT2 *,INT2,INT2));
PUBLIC INT4 D6ClUtlUpdateClientId PROTO ((tDhcp6ClntIfInfo *));
PUBLIC UINT4 D6ClCUtlGetTimeFromEpoch PROTO ((VOID));
PUBLIC INT4 D6ClUtlFreeEntryFn PROTO ((tRBElem *,UINT4));
PUBLIC tRBTree D6ClUtlRBTreeCreateEmbedded PROTO ((UINT4,tRBCompareFn));
PUBLIC INT4 D6ClUtlValidateIfPropery PROTO ((tDhcp6ClntIfInfo*));
PUBLIC INT4 D6ClUtlCliConfGetIfName PROTO ((UINT4 , INT1 *));

/**Function Proto Type For d6clif.c File*/
PUBLIC VOID D6ClIfReSetAppIdFromOpt PROTO ((tDhcp6ClntIfInfo *,UINT2));
PUBLIC INT4 D6ClIfCreateInterface PROTO ((UINT4 , tNetIpv6IfInfo *));
PUBLIC VOID D6ClIfDeleteInterface PROTO ((UINT4));
PUBLIC tDhcp6ClntIfInfo * D6ClGetFirstInterfaceInfo PROTO ((VOID));
PUBLIC tDhcp6ClntIfInfo * D6ClIfGetNextInterfaceInfo PROTO ((UINT4));
PUBLIC INT4 D6ClIfValidateInterfaceIndex PROTO ((INT4));
PUBLIC INT4 D6ClIfValidateInterfaceEntry PROTO ((UINT4 ));
PUBLIC INT4 D6ClIfGetNextInterfaceIndex PROTO ((INT4,INT4 *));
PUBLIC tDhcp6ClntIfInfo * D6ClIfGetInterfaceInfo PROTO ((UINT4));
PUBLIC INT4 D6ClIfGetFirstValidIndex PROTO ((INT4 *));
PUBLIC tDhcp6ClntIfInfo * D6ClIfGetFirstInterfaceInfo PROTO ((VOID));
PUBLIC INT4 D6ClIfIsRelayRowCreated PROTO ((UINT4));
PUBLIC INT4 D6ClIfIsServerRowCreated PROTO ((UINT4));
PUBLIC VOID D6ClIfDelateTable PROTO ((VOID));
PUBLIC INT4 D6ClIfCreateTable PROTO ((VOID));
PUBLIC VOID D6ClOptInitOptionList PROTO ((tDhcp6ClntIfInfo *));

/**Function Proto Type For d6clopt.c File*/

PUBLIC tDhcp6ClntOptionInfo *  D6ClOptGetNextInterOptionInfo PROTO((tDhcp6ClntOptionInfo *,UINT4));
PUBLIC tDhcp6ClntOptionInfo * D6ClOptGetFirstInterOptionInfo (UINT4 u4IfIndex);
PUBLIC tDhcp6ClntOptionInfo * D6ClOptGetFirstGlobOptionInfo PROTO ((VOID));
PUBLIC tDhcp6ClntOptionInfo * D6ClOptGetNextGlobOptionInfo PROTO ((UINT4, UINT2));
PUBLIC tDhcp6ClntOptionInfo * D6ClOptGetGlobOptionInfo PROTO ((UINT4,UINT2));
PUBLIC tDhcp6ClntOptionInfo * D6ClOptGetInterOptionInfo PROTO ((UINT4,UINT2));
PUBLIC INT4 D6ClOptCreateTable PROTO ((VOID));
PUBLIC VOID D6ClOptDeleteTable PROTO ((VOID));
PUBLIC tDhcp6ClntOptionInfo * D6ClOptCreateNode PROTO((VOID));
PUBLIC INT4 D6ClOptAddOptionNode PROTO ((tDhcp6ClntOptionInfo *));
PUBLIC INT4 D6ClOptDelOptionNode PROTO ((tDhcp6ClntOptionInfo *));
PUBLIC INT4 D6ClOptUpdateValueMemory PROTO ((tDhcp6ClntOptionInfo *,UINT2));
/**Function Proto Type For d6claut.c File*/
PUBLIC VOID D6ClAuthCalculateData PROTO ((UINT1 *,INT4,UINT1 *,
                                          UINT4 , UINT1 *));
PUBLIC VOID D6ClAuthValidateOption PROTO ((UINT1 *,INT4,UINT1 *,
                                          UINT4 , UINT1 *));


/**Function Proto Type For d6cltmr.c File*/
PUBLIC VOID D6ClTmrExpHandler PROTO ((VOID));
PUBLIC INT4 D6ClTmrStopAllTimer PROTO ((tDhcp6ClntIfInfo *));
PUBLIC INT4 D6ClTmrStopTimer PROTO ((tDhcp6ClntIfInfo *, UINT1 ));
PUBLIC INT4 D6ClTmrInit PROTO ((VOID));
PUBLIC INT4 D6ClTmrDeInit PROTO ((VOID));
PUBLIC INT4 D6ClTmrStart PROTO ((tDhcp6ClntIfInfo *,UINT1,UINT4));
PUBLIC INT4 D6ClUtlStartTimeWithRem PROTO ((UINT4 ));
PUBLIC INT4 D6ClUtlCalculateRemTime PROTO ((UINT4 ));

/**Function Proto Type For d6clque.c File*/
PUBLIC VOID D6ClQuePktHandler PROTO ((VOID));
PUBLIC VOID D6ClQueueCfgHandler PROTO ((tDhcp6ClntQMsg *));


/**Function Proto Type For d6clform.c File*/

PUBLIC INT4 D6ClFormAppliTxPdu PROTO ((tDhcp6ClntIfInfo *,UINT2));
PUBLIC INT4 D6ClFormUserTxPdu PROTO ((tDhcp6ClntIfInfo *));
PUBLIC INT4 D6ClFormInterfaceTxPdu PROTO ((tDhcp6ClntIfInfo *));

/**Function Proto Type For d6clpars.c File*/
PUBLIC INT4 D6ClProcessPacket PROTO ((tDhcp6ClntUdpHeader *,UINT1 *,UINT4,INT4));

/*Function Proto Type For d6clskt.c File*/
PUBLIC INT4  D6ClSktCreatePktRcv PROTO ((VOID));
PUBLIC VOID  D6ClSktPacketOnClientSocket PROTO ((INT4 ));
PUBLIC VOID  D6ClSktDeletePktRcv PROTO ((VOID));
PUBLIC INT4  D6ClSktSendPkt PROTO ((tDhcp6ClntIfInfo *,tIp6Addr *,
                                    UINT1 *,UINT4 ));
PUBLIC INT4  D6ClSktSendToDst PROTO ((tDhcp6ClntIfInfo *,tIp6Addr *,
                                      UINT1 *, UINT4));


#endif
