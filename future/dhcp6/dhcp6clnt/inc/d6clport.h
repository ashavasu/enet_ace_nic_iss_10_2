/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the prototypes for all the
 *              PUBLIC procedures which need to be ported.
 *
 **************************************************************************/
#ifndef  _DHCP6_CLNT_PORT_H
#define  _DHCP6_CLNT_PORT_H
/******************************************************************************/
PUBLIC INT4 D6ClPortGetHadwareType PROTO ((UINT2 *));
PUBLIC INT4 D6ClPortGetEnterpriseNumber PROTO ((UINT4 *));
PUBLIC INT4 D6ClPortGetIdentifier PROTO ((UINT1 *,UINT1 *));
PUBLIC UINT4 D6ClPortGetInterfaceAddressIdentifier PROTO ((UINT4,tNetIpv6IfInfo *));

PUBLIC INT4 D6ClPortInGlobalAddr (UINT4 u4IfIndex, tIp6Addr * pDstAddr,
                                      tIp6Addr * pSrcAddr);

PUBLIC INT4 D6ClPortIsRelayRowCreated (UINT4 u4IfIndex);
PUBLIC INT4 D6ClPortIsServerRowCreated (UINT4 u4IfIndex);

#endif
