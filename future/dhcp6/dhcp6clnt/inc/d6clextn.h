/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains the common extern variables used in 
 *              DHCP6 cleint module.
 *
 ***************************************************************************/

#ifndef _DHCP6_CLNT_EXTN_H
#define _DHCP6_CLNT_EXTN_H

extern tDhcp6ClntGblInfo          gDhcp6ClntGblInfo; 
extern UINT1                      gau1AppBitMaskMap[DHCP6_CLNT_APP_PER_BYTE];


#endif

