/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * $Id: d6clmacs.h,v 1.8 2012/06/26 13:03:02 siva Exp $
 * Description: This file contains the macro definition for the DHCPv6 
 *              client Module.
 *
 *
 **************************************************************************/
#ifndef  _DHCP6_CLNT_MACS_H
#define  _DHCP6_CLNT_MACS_H
/******************************************************************************/


/* Macros for gloabl access */
#define DHCP6_CLNT_CFG_QUEUE_ID         gDhcp6ClntGblInfo.CfgQId
#define DHCP6_CLNT_SEM_ID               gDhcp6ClntGblInfo.SemId
#define DHCP6_CLNT_TASK_ID              gDhcp6ClntGblInfo.TaskId
#define DHCP6_CLNT_TMRLIST_ID           gDhcp6ClntGblInfo.TimerListId
#define DHCP6_CLNT_BUDDY_MEM_POOL_ID    gDhcp6ClntGblInfo.BuddyMemPool
#define DHCP6_CLNT_INTERFACE_INFO_TABLE gDhcp6ClntGblInfo.IfTable
#define DHCP6_CLNT_OPTION_INFO_TABLE    gDhcp6ClntGblInfo.OptionTable
#define DHCP6_CLNT_PDU                  gDhcp6ClntGblInfo.au1DhcpPdu

#define DHCP6_CLNT_IS_INITIALISED       gDhcp6ClntGblInfo.b1IsInitialized
#define DHCP6_CLNT_TRAP_CONTROL_OPTION  gDhcp6ClntGblInfo.u1TrapControl
#define DHCP6_CLNT_TRACE_OPTION         gDhcp6ClntGblInfo.u4DebugTrace
#define DHCP6_CLNT_SYS_LOG_OPTION       gDhcp6ClntGblInfo.b1SysLogEnabled
#define DHCP6_CLNT_SOURCE_PORT          gDhcp6ClntGblInfo.u2UdpSourcePort 
#define DHCP6_CLNT_DEST_PORT            gDhcp6ClntGblInfo.u2UdpDestPort
#define DHCP6_CLNT_SYSLOG_ID            gDhcp6ClntGblInfo.u4SysLogId
#define DHCP6_CLNT_SOCKET_ID            gDhcp6ClntGblInfo.gi4Dhcp6ClntSockId

/* macros for  Memory control*/
#define DHCP6_CLNT_MSGQ_POOL            \
                  D6CLMemPoolIds[MAX_D6CL_CLNT_QMSGS_SIZING_ID]
#define DHCP6_CLNT_INTERFACE_INFO_POOL  \
                  D6CLMemPoolIds[MAX_D6CL_CLNT_IFACES_SIZING_ID]
#define DHCP6_CLNT_OPTION_TABLE_POOL    \
                  D6CLMemPoolIds[MAX_D6CL_CLNT_OPTION_SIZING_ID]
#define DHCP6_CLNT_APPL_INFO_POOL       \
                  D6CLMemPoolIds[MAX_D6CL_CLNT_APPS_SIZING_ID]
#define DHCP6_CLNT_PKT_INFO_POOL \
    D6CLMemPoolIds[MAX_D6CL_CLNT_PKTINFO_SIZING_ID]

#define DHCP6_CLNT_NAME                 (CONST UINT1 *)"DHCP6_CLNT"
#define DHCP6_CLNT_INTERFACE_SEM_NAME   "D6CInSem"
#define DHCP6_CLNT_SEM_NAME             "Dhcp6CSem"
#define DHCP6_CLNT_SELF                 SELF

/* System specific Constatnts */
#define DHCP6_CLNT_MSG_INFO_SIZE          (sizeof(tDhcp6ClntQMsg))
#define DHCP6_CLNT_INTERFACE_INFO_SIZE    (sizeof(tDhcp6ClntIfInfo))
#define DHCP6_CLNT_OPTION_INFO_SIZE       (sizeof(tDhcp6ClntOptionInfo))
#define DHCP6_CLNT_TMR_INFO_SIZE          (sizeof(tDhcp6CTimer))
#define DHCP6_CLNT_GLOBAL_INFO_SIZE       (sizeof(tDhcp6ClntGblInfo))
#define DHCP6_CLNT_APPL_INFO_SIZE         (sizeof(tDhcp6ClntAppParam))
#define DHCP6_CLNT_SUB_OPTION_SIZE         (sizeof(tDhcp6ClntSubOptionInfo))


#define DHCP6_CLNT_TMR_MAX             (DHCP6_CLNT_MAX_INTERFACE * 2) 


#define DHCP6_CLNT_INCR_INVALID_RX_PDU_COUNT(pInterfaceInfo)\
        pInterfaceInfo->u4InvalidPktIn++

#define DHCP6_CLNT_INCR_TX_PDU_COUNT(pInterfaceInfo)\
        pInterfaceInfo->u4InformOut++

#define DHCP6_CLNT_INCR_RX_UN_AUTH_PDU_COUNT(pInterfaceInfo)\
        pInterfaceInfo->u4HmacFailCount++

#define DHCP6_CLNT_NUM_OF_TIME_UNITS_IN_A_SEC   SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define DHCP6_CLNT_GET_1BYTE(u1Val, pu1PktBuf) \
   do {                             \
      u1Val = *pu1PktBuf;           \
         pu1PktBuf += 1;        \
   } while(0)

#define DHCP6_CLNT_GET_2BYTE(u2Val, pu1PktBuf)            \
do {                                        \
      MEMCPY (&u2Val, pu1PktBuf, 2);\
      u2Val = (UINT2)OSIX_NTOHS (u2Val);               \
      pu1PktBuf += 2;                   \
} while(0)

#define DHCP6_CLNT_GET_4BYTE(u4Val, pu1PktBuf)            \
do {                                        \
      MEMCPY (&u4Val, pu1PktBuf, 4);\
      u4Val = (UINT4)OSIX_NTOHL(u4Val);               \
      pu1PktBuf += 4;                   \
} while(0)

#define DHCP6_CLNT_GET_NBYTE(pu1Data, pu1PktBuf, u4Size)\
    do {                                           \
        MEMCPY (pu1Data, pu1PktBuf, u4Size);      \
        pu1PktBuf += u4Size;                      \
    } while(0)
#define DHCP6_CLNT_PUT_1BYTE(pu1PktBuf, u1Val) \
        do {                             \
           *pu1PktBuf = u1Val;           \
           pu1PktBuf += 1;        \
        } while(0)

#define DHCP6_CLNT_PUT_2BYTE(pu1PktBuf, u2Val)        \
        do {                                    \
            u2Val = (UINT2)OSIX_HTONS(u2Val);                  \
            MEMCPY (pu1PktBuf,&u2Val, 2); \
            u2Val = (UINT2)OSIX_NTOHS (u2Val);                  \
            pu1PktBuf += 2;              \
        } while(0)

#define DHCP6_CLNT_PUT_4BYTE(pu1PktBuf, u4Val)       \
        do {                                   \
           u4Val = OSIX_HTONL(u4Val);   \
           MEMCPY (pu1PktBuf,&u4Val, 4); \
           u4Val = OSIX_NTOHL(u4Val); \
           pu1PktBuf += 4;             \
        }while(0)

#define DHCP6_CLNT_PUT_NBYTE(pu1PktBuf, u4Data, u4Size)\
   do {                                           \
        u4Data = OSIX_HTONL(u4Data);              \
        MEMCPY (pu1PktBuf, &u4Data, u4Size);      \
        u4Data = OSIX_NTOHL(u4Data);              \
        pu1PktBuf += u4Size;                      \
      } while(0)


#define DHCP6_CLNT_NUM_OF_TIME_UNITS_IN_A_SEC SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define DHCP6_CLNT_NUM_OF_MSEC_IN_A_TIME_UNIT (1000/DHCP6_CLNT_NUM_OF_TIME_UNITS_IN_A_SEC)

#define DHCP6_CLNT_CONVERT_TIME_TICKS_TO_MSEC(u4TimeTicks)\
        (u4TimeTicks*(DHCP6_CLNT_NUM_OF_MSEC_IN_A_TIME_UNIT))

#define DHCP6_CLNT_CONVERT_TIME_TICKS_TO_10MSEC(u4TimeTicks)\
        (u4TimeTicks)
#define DHCP6_CLNT_IS_MEMBER_APP(au1AppArray,u2AppId,u1Result) \
        do{\
           UINT2 u2AppBytePos;\
           UINT2 u2AppBitPos;\
           u2AppBytePos=(UINT2)(u2AppId/(DHCP6_CLNT_APP_PER_BYTE));\
           u2AppBitPos=(UINT2)((u2AppId%DHCP6_CLNT_APP_PER_BYTE));\
    if (u2AppBitPos ==0){u2AppBytePos -= 1;} \
           \
           if ((au1AppArray [u2AppBytePos] \
                &gau1AppBitMaskMap[u2AppBitPos])!= 0) {\
           \
              u1Result = OSIX_TRUE;\
           }\
           else {\
           \
              u1Result = OSIX_FALSE; \
           } \
        }while(0)


    /* This macro adds u2AppId as a member port in the list pointed by
     * au1AppArray
     */


 /*Warning!!! - Do not call the macro with u2AppId as 0 or Invalid AppId.*/
#define DHCP6_CLNT_SET_MEMBER_APP(au1AppArray, u2AppId) \
          do {\
              UINT2 u2AppBytePos;\
              UINT2 u2AppBitPos;\
              u2AppBytePos = (UINT2)(u2AppId / DHCP6_CLNT_APP_PER_BYTE);\
              u2AppBitPos  = (UINT2)(u2AppId % DHCP6_CLNT_APP_PER_BYTE);\
       if (u2AppBitPos  == 0 && u2AppId != 0)\
              {\
                   u2AppBytePos = (UINT2)(u2AppBytePos - 1);\
              }\
              au1AppArray[u2AppBytePos] = (UINT1)(au1AppArray[u2AppBytePos] \
                                | gau1AppBitMaskMap[u2AppBitPos]);\
           }while(0)


/* 
 * The macro DHCP6_CLNT_RESET_MEMBER_PORT (), removes u2AppId from the member 
 * list of au1AppArray. 
 * Warning!!! - Do not call the macro with u2AppId as 0 or Invalid AppId.
 */
#define DHCP6_CLNT_RESET_MEMBER_APP(au1AppArray, u2AppId) \
              do{\
                 UINT2 u2AppBytePos;\
                 UINT2 u2AppBitPos;\
                 u2AppBytePos = (UINT2)(u2AppId / DHCP6_CLNT_APP_PER_BYTE);\
                 u2AppBitPos  = (UINT2)(u2AppId % DHCP6_CLNT_APP_PER_BYTE);\
   if (u2AppBitPos  == 0) {u2AppBytePos -= 1;} \
                 \
                 au1AppArray[u2AppBytePos] = (UINT1)(au1AppArray[u2AppBytePos] \
                               & ~gau1AppBitMaskMap[u2AppBitPos]);\
              }while(0)


#endif
