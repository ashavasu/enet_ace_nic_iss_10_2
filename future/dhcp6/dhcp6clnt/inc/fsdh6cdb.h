/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6cdb.h,v 1.6 2013/12/18 12:10:57 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDH6CDB_H
#define _FSDH6CDB_H

UINT1 FsDhcp6ClntIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDhcp6ClntOptionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsdh6c [] ={1,3,6,1,4,1,29601,2,43};
tSNMP_OID_TYPE fsdh6cOID = {9, fsdh6c};


UINT4 FsDhcp6ClntTrapAdminControl [ ] ={1,3,6,1,4,1,29601,2,43,1,1};
UINT4 FsDhcp6ClntDebugTrace [ ] ={1,3,6,1,4,1,29601,2,43,1,2};
UINT4 FsDhcp6ClntSysLogAdminStatus [ ] ={1,3,6,1,4,1,29601,2,43,1,3};
UINT4 FsDhcp6ClntListenPort [ ] ={1,3,6,1,4,1,29601,2,43,1,4};
UINT4 FsDhcp6ClntTransmitPort [ ] ={1,3,6,1,4,1,29601,2,43,1,5};
UINT4 FsDhcp6ClntIfIndex [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,1};
UINT4 FsDhcp6ClntIfSrvAddress [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,2};
UINT4 FsDhcp6ClntIfDuidType [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,3};
UINT4 FsDhcp6ClntIfDuid [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,4};
UINT4 FsDhcp6ClntIfDuidIfIndex [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,5};
UINT4 FsDhcp6ClntIfMaxRetCount [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,6};
UINT4 FsDhcp6ClntIfMaxRetDelay [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,7};
UINT4 FsDhcp6ClntIfMaxRetTime [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,8};
UINT4 FsDhcp6ClntIfInitRetTime [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,9};
UINT4 FsDhcp6ClntIfCurrRetTime [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,10};
UINT4 FsDhcp6ClntIfMinRefreshTime [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,11};
UINT4 FsDhcp6ClntIfCurrRefreshTime [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,12};
UINT4 FsDhcp6ClntIfRealmName [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,13};
UINT4 FsDhcp6ClntIfKey [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,14};
UINT4 FsDhcp6ClntIfKeyId [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,15};
UINT4 FsDhcp6ClntIfInformOut [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,16};
UINT4 FsDhcp6ClntIfReplyIn [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,17};
UINT4 FsDhcp6ClntIfInvalidPktIn [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,18};
UINT4 FsDhcp6ClntIfHmacFailCount [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,19};
UINT4 FsDhcp6ClntIfCounterRest [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,20};
UINT4 FsDhcp6ClntIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,43,2,1,1,21};
UINT4 FsDhcp6ClntOptionType [ ] ={1,3,6,1,4,1,29601,2,43,2,2,1,1};
UINT4 FsDhcp6ClntOptionLength [ ] ={1,3,6,1,4,1,29601,2,43,2,2,1,2};
UINT4 FsDhcp6ClntOptionValue [ ] ={1,3,6,1,4,1,29601,2,43,2,2,1,3};


tMbDbEntry fsdh6cMibEntry[]= {

{{11,FsDhcp6ClntTrapAdminControl}, NULL, FsDhcp6ClntTrapAdminControlGet, FsDhcp6ClntTrapAdminControlSet, FsDhcp6ClntTrapAdminControlTest, FsDhcp6ClntTrapAdminControlDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "\0"},

{{11,FsDhcp6ClntDebugTrace}, NULL, FsDhcp6ClntDebugTraceGet, FsDhcp6ClntDebugTraceSet, FsDhcp6ClntDebugTraceTest, FsDhcp6ClntDebugTraceDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "critical"},

{{11,FsDhcp6ClntSysLogAdminStatus}, NULL, FsDhcp6ClntSysLogAdminStatusGet, FsDhcp6ClntSysLogAdminStatusSet, FsDhcp6ClntSysLogAdminStatusTest, FsDhcp6ClntSysLogAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDhcp6ClntListenPort}, NULL, FsDhcp6ClntListenPortGet, FsDhcp6ClntListenPortSet, FsDhcp6ClntListenPortTest, FsDhcp6ClntListenPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "546"},

{{11,FsDhcp6ClntTransmitPort}, NULL, FsDhcp6ClntTransmitPortGet, FsDhcp6ClntTransmitPortSet, FsDhcp6ClntTransmitPortTest, FsDhcp6ClntTransmitPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "547"},

{{13,FsDhcp6ClntIfIndex}, GetNextIndexFsDhcp6ClntIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfSrvAddress}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfSrvAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfDuidType}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfDuidTypeGet, FsDhcp6ClntIfDuidTypeSet, FsDhcp6ClntIfDuidTypeTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "1"},

{{13,FsDhcp6ClntIfDuid}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfDuidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfDuidIfIndex}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfDuidIfIndexGet, FsDhcp6ClntIfDuidIfIndexSet, FsDhcp6ClntIfDuidIfIndexTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfMaxRetCount}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfMaxRetCountGet, FsDhcp6ClntIfMaxRetCountSet, FsDhcp6ClntIfMaxRetCountTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "0"},

{{13,FsDhcp6ClntIfMaxRetDelay}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfMaxRetDelayGet, FsDhcp6ClntIfMaxRetDelaySet, FsDhcp6ClntIfMaxRetDelayTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "0"},

{{13,FsDhcp6ClntIfMaxRetTime}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfMaxRetTimeGet, FsDhcp6ClntIfMaxRetTimeSet, FsDhcp6ClntIfMaxRetTimeTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "120"},

{{13,FsDhcp6ClntIfInitRetTime}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfInitRetTimeGet, FsDhcp6ClntIfInitRetTimeSet, FsDhcp6ClntIfInitRetTimeTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "1"},

{{13,FsDhcp6ClntIfCurrRetTime}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfCurrRetTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfMinRefreshTime}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfMinRefreshTimeGet, FsDhcp6ClntIfMinRefreshTimeSet, FsDhcp6ClntIfMinRefreshTimeTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "86400"},

{{13,FsDhcp6ClntIfCurrRefreshTime}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfCurrRefreshTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfRealmName}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfRealmNameGet, FsDhcp6ClntIfRealmNameSet, FsDhcp6ClntIfRealmNameTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfKey}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfKeyGet, FsDhcp6ClntIfKeySet, FsDhcp6ClntIfKeyTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfKeyId}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfKeyIdGet, FsDhcp6ClntIfKeyIdSet, FsDhcp6ClntIfKeyIdTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, "1"},

{{13,FsDhcp6ClntIfInformOut}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfInformOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfReplyIn}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfReplyInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfInvalidPktIn}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfInvalidPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfHmacFailCount}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfHmacFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfCounterRest}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfCounterRestGet, FsDhcp6ClntIfCounterRestSet, FsDhcp6ClntIfCounterRestTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6ClntIfRowStatus}, GetNextIndexFsDhcp6ClntIfTable, FsDhcp6ClntIfRowStatusGet, FsDhcp6ClntIfRowStatusSet, FsDhcp6ClntIfRowStatusTest, FsDhcp6ClntIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6ClntIfTableINDEX, 1, 0, 1, NULL},

{{13,FsDhcp6ClntOptionType}, GetNextIndexFsDhcp6ClntOptionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6ClntOptionTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6ClntOptionLength}, GetNextIndexFsDhcp6ClntOptionTable, FsDhcp6ClntOptionLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDhcp6ClntOptionTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6ClntOptionValue}, GetNextIndexFsDhcp6ClntOptionTable, FsDhcp6ClntOptionValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDhcp6ClntOptionTableINDEX, 2, 0, 0, NULL},
};
tMibData fsdh6cEntry = { 29, fsdh6cMibEntry };
#endif /* _FSDH6CDB_H */

