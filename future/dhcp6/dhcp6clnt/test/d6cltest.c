/********************************************************************
 * * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * *
 * * $Id: d6cltest.c,v 1.9 2010/09/23 09:24:58 prabuc Exp $
 * *
 * * Description: ELPS Test routines. Mainly used for debugging purpose.
 * * NOTE: This file should not be include in release packaging.
 * ***********************************************************************/
#ifndef _DHC6CTEST_C_
#define _DHC6CTEST_C_

#include "d6clinc.h"
#include "d6cltest.h"

#define APPLICATION_DUMMY1 1;
#define APPLICATION_DUMMY2 2;
#define APPLICATION_DUMMY3 3;

UINT1               gu1Dhcp6CTestHwFailureFlg;
PUBLIC VOID         D6ClCallFunction (VOID);

PRIVATE UINT4       D6ClValidateAllSetObject (VOID);
PRIVATE UINT4       D6ClValidateAllTestObject (VOID);
PRIVATE UINT4       D6ClAllDepObject (VOID);
PRIVATE UINT4       D6ClGetAllObjectsObject (VOID);
PRIVATE UINT4       D6ClGetAllCliObjectsObject (VOID);
PRIVATE UINT4       D6ClGenerateTrap (VOID);
PRIVATE UINT4       D6ClTimerFunction (VOID);
PRIVATE UINT4       D6ClSetTimerFunction (VOID);
PRIVATE UINT4       D6ClApiRegisterTest (VOID);
PRIVATE UINT4       D6ClApiUnRegisterTest (VOID);
PRIVATE UINT4       D6ClTestApiOptionSet (VOID);
PRIVATE UINT4       D6ClTestApiOptionGet (VOID);
PRIVATE UINT4       D6ClTestApiOptionWithGet (VOID);
PRIVATE UINT4       D6ClTestParse (VOID);
PRIVATE UINT4       D6ClTestParse1 (VOID);
PRIVATE UINT4       D6ClTestParse2 (VOID);
PUBLIC UINT4        D6ClTestParse3 (VOID);
PUBLIC UINT4        D6ClTestParse4 (VOID);
PUBLIC UINT4        D6ClTestParse5 (VOID);
PUBLIC UINT4        D6ClTestParse7 (VOID);
PUBLIC UINT4        D6ClTestParse6 (VOID);
PUBLIC UINT4        D6ClTestParse8 (VOID);
PUBLIC UINT4        D6ClTestParse9 (VOID);
PUBLIC UINT4        D6ClTestParse10 (VOID);
PUBLIC UINT4        D6ClTestParse11 (VOID);
PUBLIC UINT4        D6ClTestParse12 (VOID);
PUBLIC UINT4        D6ClTestParse13 (VOID);
PUBLIC UINT4        D6ClTestParse14 (VOID);
PUBLIC INT4         fpDhcp6ClntCallback2 (UINT2 u2AppIdId, UINT4 u4IfIndex,
                                          UINT2 *pu2Type,
                                          UINT2 *pu2OptionLength,
                                          UINT1 *pu1OptionValue);
PUBLIC INT4         fpDhcp6ClntCallback (UINT2 u2AppIdId, UINT4 u4IfIndex,
                                         UINT2 *pu2Type,
                                         UINT2 *pu2OptionLength,
                                         UINT1 *pu1OptionValue);
PUBLIC INT4         fpDhcp6ClntCallback3 (UINT2 u2AppIdId, UINT4 u4IfIndex,
                                          UINT2 *pu2Type,
                                          UINT2 *pu2OptionLength,
                                          UINT1 *pu1OptionValue);

PUBLIC VOID         Dummy1Register (VOID);
PUBLIC VOID         Dummy2Register (VOID);
PUBLIC VOID         Dummy3Register (VOID);
PUBLIC VOID         Dummy1DeRegister (VOID);
PUBLIC VOID         Dummy1RegisterNonFunc (VOID);
PUBLIC VOID         Dummy2DeRegister (VOID);
PUBLIC VOID         Dummy3DeRegister (VOID);
PUBLIC VOID         Dummy1UserClassSet (UINT4 u4IfIndex, UINT2 u2ModeId,
                                        UINT1 *pau1UserClass, UINT2 u2Length,
                                        UINT2 u2Code);
PUBLIC VOID         Dummy1UserClassReSet (UINT4 u4IfIndex, UINT2 u2ModId,
                                          UINT2 u2Code);
PUBLIC VOID         Dummy1VendorClassSet (UINT4 u4IfIndex, UINT2 u2ModeId,
                                          UINT1 *pau1VendorClass,
                                          UINT2 u2Length, UINT2 u2Code);
PUBLIC VOID         Dummy1VendorClassReSet (UINT4 u4IfIndex, UINT2 u2ModId,
                                            UINT2 u2Code);
PUBLIC VOID         Dummy1VendorSpecSet (UINT4 u4IfIndex, UINT2 u2ModeId,
                                         UINT1 *pau1UserClass, UINT2 u2Length,
                                         UINT2 u2Code);
PUBLIC VOID         Dummy1VendorSpecReSet (UINT4 u4IfIndex, UINT2 u2ModId,
                                           UINT2 u2Code);
PUBLIC VOID         Dummy1SetOptionRequestCode (UINT4 u4IfIndex, UINT2 u2ModId,
                                                UINT2 u2Code);
PUBLIC VOID         Dummy1ReSetOptionRequestCode (UINT4 u4IfIndex,
                                                  UINT2 u2ModId, UINT2 u2Code);
PUBLIC VOID         Dummy1SetOptionCode (VOID);
PUBLIC VOID         Dummy1ReSetOptionCode (VOID);
PUBLIC VOID         Dummy1SetOption (UINT4 u4IfIndex, UINT2, UINT2);
PUBLIC VOID         Dummy1ReSetOption (UINT4 u4IfIndex, UINT2 u2ModId,
                                       UINT2 u2Code);
PUBLIC VOID         Dummy1GetOptionBlock (VOID);
PUBLIC VOID         Dummy1GetOptionNonBlock (VOID);
PUBLIC INT4         D6CLNT_FUNC_1_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_1_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_10_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_10_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_12_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_14_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_15_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_15_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_16_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_16_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_18_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_18_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_17_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_25_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_7_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_7_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_9_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_9_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_9_C (VOID);
PUBLIC INT4         D6CLNT_FUNC_11_A (VOID);
PUBLIC INT4         D6CLNT_CONF_4_A (VOID);
PUBLIC INT4         D6CLNT_CONF_8_A (VOID);
PUBLIC INT4         D6CLNT_CONF_8_B (VOID);
PUBLIC INT4         D6CLNT_CONF_8_C (VOID);
PUBLIC INT4         D6CLNT_CONF_8_D (VOID);
PUBLIC INT4         D6CLNT_CONF_8_E (VOID);
PUBLIC INT4         D6CLNT_CONF_8_F (VOID);
PUBLIC INT4         D6CLNT_FUNC_3_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_3_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_3_C (VOID);
PUBLIC INT4         D6CLNT_FUNC_4_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_21_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_21_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_21_C (VOID);
PUBLIC INT4         D6CLNT_FUNC_21_D (VOID);
PUBLIC INT4         D6CLNT_FUNC_21_E (VOID);
PUBLIC INT4         D6CLNT_FUNC_21_F (VOID);
PUBLIC INT4         D6CLNT_FUNC_27_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_40_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_41_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_42_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_42_B (VOID);
PUBLIC INT4         D6CLNT_FUNC_43_A (VOID);
PUBLIC INT4         D6CLNT_FUNC_43_B (VOID);
PUBLIC INT4         D6CLNT_WALK_1_A (VOID);
PUBLIC INT4         D6CLNT_WALK_1_B (VOID);
PUBLIC INT4         D6CLNT_API_A (VOID);

PUBLIC INT4         D6SRV_FUNC_1 (VOID);
PUBLIC INT4         D6RL_FUNC_1 (VOID);
PUBLIC INT4         D6SR_FUNC_4 (VOID);
PUBLIC INT4         D6SRV_FUNC_3 (VOID);
PUBLIC INT4         D6SRV_FUNC_19 (VOID);
PUBLIC INT4         D6SRV_FUNC_17 (VOID);
PUBLIC INT4         D6SRV_FUNC_20 (VOID);
PUBLIC INT4         D6SRV_FUNC_21 (VOID);
PUBLIC INT4         D6SRV_FUNC_22 (VOID);
PUBLIC INT4         D6SRV_FUNC_22A (VOID);
PUBLIC INT4         D6SRV_FUNC_23 (VOID);
PUBLIC INT4         D6SRV_FUNC_23B (VOID);
PUBLIC INT4         D6SRV_FUNC_232B (VOID);
PUBLIC INT4         D6SRV_FUNC_24 (VOID);
PUBLIC INT4         D6SRV_FUNC_31 (VOID);
PUBLIC INT4         D6SRV_FUNC_ALL (VOID);
PUBLIC INT4         D6SRV_CONF_7 (VOID);
PUBLIC INT4         D6CLNT_LS_A (VOID);
PUBLIC INT4         D6CLNT_SEC_1 (VOID);
VOID
Dummy1GetOptionNonBlock (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    UINT1               au1UserClass[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT1               au1VendorClass[DHCP6_CLNT_OPTION_SIZE_MAX];

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    MEMSET (au1UserClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);

    u2ModId = APPLICATION_DUMMY2;
    MEMCPY (au1UserClass, "CLIENT", 6);
    MEMCPY (au1VendorClass, "CLIENT", 6);
    u4IfIndex = 1;
    Dummy1Register ();
    Dummy2Register ();
    Dummy1UserClassSet (u4IfIndex, 1, au1UserClass, 6, 4);
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 6, 4);
    Dummy1SetOption (u4IfIndex, 1, 11);
    OptionParam.u2Code = 13;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    else
    {
        PRINTF ("D6ClApiOptionGet Printing one of values received\n ");

        PRINTF ("u2AppIdId%d\n", u2ModId);
        PRINTF ("u4IfIndex%d\n", u4IfIndex);
        PRINTF ("u2OptionLength%d\n", OptionParam.u2Length);
        PRINTF ("Code%d\n", OptionParam.u2Code);
        PRINTF ("au1Value%s\n", OptionParam.au1Value);
    }
}

VOID
Dummy1GetOptionBlock (VOID)
{
    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 9;
    Dummy1RegisterNonFunc ();
    OptionParam.u2Code = 13;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    else
    {
        PRINTF ("D6ClApiOptionGet Printing one of values received\n ");

        PRINTF ("u2AppIdId%d\n", u2ModId);
        PRINTF ("u4IfIndex%d\n", u4IfIndex);
        PRINTF ("u2OptionLength%d\n", OptionParam.u2Length);
        PRINTF ("Code%d\n", OptionParam.u2Code);
        PRINTF ("au1Value%s\n", OptionParam.au1Value);
    }
}

VOID
Dummy1SetOption (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_ADD;
    OptionParam.u1OptionType = DHCP6_CLNT_OPTION_CODE;
    OptionParam.u2Code = u2Code;
    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Option Not Set\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Option Set\n", u2ModId);
    }
}
VOID
Dummy1ReSetOption (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_REMOVE;
    OptionParam.u1OptionType = DHCP6_CLNT_OPTION_CODE;
    OptionParam.u2Code = u2Code;
    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Option Not ReSet\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Option ReSet\n", u2ModId);
    }
}

VOID
Dummy1ReSetOptionRequestCode (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));
    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_REMOVE;
    OptionParam.u1OptionType = DHCP6_CLNT_REQUEST_OPTION_CODE;
    OptionParam.u2Code = u2Code;
    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Option Code Not ReSet\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Option Code Set\n", u2ModId);
    }
}

VOID
Dummy1SetOptionRequestCode (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_ADD;
    OptionParam.u1OptionType = DHCP6_CLNT_REQUEST_OPTION_CODE;
    OptionParam.u2Code = u2Code;
    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Option Code Not Set\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Option Code Set\n", u2ModId);
    }
}

VOID
Dummy1UserClassSet (UINT4 u4IfIndex, UINT2 u2ModId, UINT1 *pau1UserClass,
                    UINT2 u2Length, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_ADD;
    OptionParam.u1OptionType = DHCP6_CLNT_USER_CLASS_VALUE;
    OptionParam.u2ValueLength = u2Length;
    OptionParam.u2Code = u2Code;
    if (OptionParam.u2ValueLength <= 254)
    {
        MEMCPY (OptionParam.au1Value, pau1UserClass, OptionParam.u2ValueLength);
    }

    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d User Class Not Set\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d User Class Set\n", u2ModId);
    }
}

VOID
Dummy1UserClassReSet (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_REMOVE;
    OptionParam.u1OptionType = DHCP6_CLNT_USER_CLASS_VALUE;
    OptionParam.u2Code = u2Code;
    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d User Class Not ReSet\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d User Class ReSet\n", u2ModId);
    }
}

VOID
Dummy1VendorClassSet (UINT4 u4IfIndex, UINT2 u2ModId, UINT1 *pau1VendorClass,
                      UINT2 u2Length, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_ADD;
    OptionParam.u1OptionType = DHCP6_CLNT_VENDOR_CLASS_VALUE;
    OptionParam.u2ValueLength = u2Length;
    OptionParam.u2Code = u2Code;
    if (OptionParam.u2ValueLength <= 254)
    {
        MEMCPY (OptionParam.au1Value, pau1VendorClass,
                OptionParam.u2ValueLength);
    }

    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Vendor Class Not Set\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Vendor Class Set\n", u2ModId);
    }
}

VOID
Dummy1VendorClassReSet (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_REMOVE;
    OptionParam.u1OptionType = DHCP6_CLNT_VENDOR_CLASS_VALUE;
    OptionParam.u2Code = u2Code;

    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Vendor Class Not ReSet\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Vendor Class ReSet\n", u2ModId);
    }
}

VOID
Dummy1VendorSpecSet (UINT4 u4IfIndex, UINT2 u2ModId, UINT1 *pau1VendorClass,
                     UINT2 u2Length, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_ADD;
    OptionParam.u1OptionType = DHCP6_CLNT_VENDOR_SPECF_VALUE;
    OptionParam.u2ValueLength = u2Length;
    OptionParam.u2Code = u2Code;
    if (OptionParam.u2ValueLength <= 254)
    {
        MEMCPY (OptionParam.au1Value, pau1VendorClass,
                OptionParam.u2ValueLength);
    }

    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Vendor Specific Not Set\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Vendor Specific Set\n", u2ModId);
    }
}
VOID
Dummy1VendorSpecReSet (UINT4 u4IfIndex, UINT2 u2ModId, UINT2 u2Code)
{
    tDhcp6ClntAppOptionParam OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));

    OptionParam.u4IfIndex = u4IfIndex;
    OptionParam.u1Action = DHCP6_CLNT_APP_INFO_REMOVE;
    OptionParam.u1OptionType = DHCP6_CLNT_VENDOR_SPECF_VALUE;
    OptionParam.u2Code = u2Code;

    if (D6ClApiOptionSet (u2ModId, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy %d Vendor Specific Not ReSet\n", u2ModId);
    }
    else
    {
        PRINTF ("Dummy %d Vendor Specific ReSet\n", u2ModId);
    }
}

VOID
Dummy1RegisterNonFunc (VOID)
{
    UINT2               u2ModId = 0;
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));

    u2ModId = APPLICATION_DUMMY1;
    AppParam.fpDhcp6ClntCallback = NULL;
    if (D6ClApiRegisterApplication (u2ModId, &AppParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 Module Not Regisetered with out pointer function\n");
    }
    else
    {
        PRINTF ("Dummy 1 Registered with out pointer function\n");
    }
}

VOID
Dummy1Register (VOID)
{
    UINT2               u2ModId = 0;
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));

    u2ModId = APPLICATION_DUMMY1;
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiRegisterApplication (u2ModId, &AppParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 Module Not Regisetered\n");
    }
    else
    {
        PRINTF ("Dummy 1 Registered\n");
    }
}

VOID
Dummy2Register (VOID)
{
    UINT2               u2ModId = 0;
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));

    u2ModId = APPLICATION_DUMMY2;
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback2;
    if (D6ClApiRegisterApplication (u2ModId, &AppParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 2 Module Not Regisetered\n");
    }
    else
    {
        PRINTF ("Dummy 2 Registered\n");
    }
}

VOID
Dummy3Register (VOID)
{
    UINT2               u2ModId = 0;
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback3;

    u2ModId = APPLICATION_DUMMY3;
    if (D6ClApiRegisterApplication (u2ModId, &AppParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 3 Module Not Regisetered\n");
    }
    else
    {
        PRINTF ("Dummy 3 Registered\n");
    }
}

VOID
Dummy1DeRegister (VOID)
{
    UINT2               u2ModId;

    u2ModId = APPLICATION_DUMMY1;

    if (D6ClApiDeRegisterApplication (u2ModId) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 Module Not DeRegisetered\n");
    }
    else
    {
        PRINTF ("Dummy 1 DeRegistered\n");
    }
}
VOID
Dummy2DeRegister (VOID)
{
    UINT2               u2ModId;

    u2ModId = APPLICATION_DUMMY2;

    if (D6ClApiDeRegisterApplication (u2ModId) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 2 Module Not DeRegisetered\n");
    }
    else
    {
        PRINTF ("Dummy 2 DeRegistered\n");
    }
}
VOID
Dummy3DeRegister (VOID)
{
    UINT2               u2ModId;

    u2ModId = APPLICATION_DUMMY3;

    if (D6ClApiDeRegisterApplication (u2ModId) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 3 Module Not DeRegisetered\n");
    }
    else
    {
        PRINTF ("Dummy 3 DeRegistered\n");
    }
}

VOID
D6ClTestExecUT (INT4 i4UtId)
{
    UINT4               u4RetVal = OSIX_FAILURE;

    switch (i4UtId)
    {
            /* SEM related UT cases */
        case 0:
            if (D6ClValidateAllSetObject () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 1:

            if (D6ClValidateAllTestObject () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 2:

            if (D6ClAllDepObject () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 3:

            if (D6ClGetAllObjectsObject () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 4:
            if (D6ClGetAllCliObjectsObject () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 5:

            if (D6ClGenerateTrap () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

            break;
        case 6:
            if (D6ClTimerFunction () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 7:
            if (D6ClSetTimerFunction () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 8:
            if (D6ClApiRegisterTest () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 9:
            if (D6ClApiUnRegisterTest () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 10:
            if (D6ClTestApiOptionSet () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 11:
            if (D6ClTestApiOptionGet () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 12:
            if (D6ClTestApiOptionWithGet () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 13:
            if (D6ClTestParse () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 14:
            if (D6ClTestParse1 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 15:

            if (D6ClTestParse2 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;

        case 16:

            if (D6ClTestParse3 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;

        case 17:

            if (D6ClTestParse4 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 18:

            if (D6ClTestParse5 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 19:

            if (D6ClTestParse6 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;

        case 20:

            if (D6ClTestParse7 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;

        case 21:

            if (D6ClTestParse8 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 22:

            if (D6ClTestParse9 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 23:

            if (D6ClTestParse10 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 24:

            if (D6ClTestParse11 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 25:

            if (D6ClTestParse12 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        case 26:
            if (D6ClTestParse13 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 27:
            if (D6ClTestParse14 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 28:
            if (D6CLNT_FUNC_1_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 29:
            if (D6CLNT_FUNC_1_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 30:
            if (D6CLNT_FUNC_10_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 31:
            if (D6CLNT_FUNC_10_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 32:
            if (D6CLNT_FUNC_12_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 33:
            if (D6CLNT_FUNC_14_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 34:
            if (D6CLNT_FUNC_15_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 35:
            if (D6CLNT_FUNC_15_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 36:
            if (D6CLNT_FUNC_16_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 37:
            if (D6CLNT_FUNC_16_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 38:
            if (D6CLNT_FUNC_18_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 39:
            if (D6CLNT_FUNC_18_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 40:
            if (D6CLNT_FUNC_17_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 41:
            if (D6CLNT_FUNC_25_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 42:
            if (D6CLNT_FUNC_7_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 43:
            if (D6CLNT_FUNC_7_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 44:
            if (D6CLNT_FUNC_9_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 45:
            if (D6CLNT_FUNC_9_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 46:
            if (D6CLNT_FUNC_9_C () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 47:
            if (D6CLNT_FUNC_11_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 48:
            if (D6CLNT_CONF_4_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 49:
            if (D6CLNT_CONF_8_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 50:
            if (D6CLNT_CONF_8_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 51:
            if (D6CLNT_CONF_8_C () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 52:
            if (D6CLNT_CONF_8_D () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 53:
            if (D6CLNT_CONF_8_E () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 54:
            if (D6CLNT_CONF_8_F () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 55:

            if (D6CLNT_FUNC_3_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 56:

            if (D6CLNT_FUNC_3_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 57:

            if (D6CLNT_FUNC_3_C () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 58:

            if (D6CLNT_FUNC_4_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 59:

            if (D6CLNT_FUNC_21_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 60:

            if (D6CLNT_FUNC_21_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 61:

            if (D6CLNT_FUNC_21_C () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 62:

            if (D6CLNT_FUNC_21_D () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 63:

            if (D6CLNT_FUNC_21_E () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 64:

            if (D6CLNT_FUNC_21_F () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 65:

            if (D6CLNT_FUNC_27_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 66:

            if (D6CLNT_FUNC_40_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 67:

            if (D6CLNT_FUNC_41_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 68:

            if (D6CLNT_FUNC_42_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 69:

            if (D6CLNT_FUNC_42_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 70:

            if (D6CLNT_FUNC_43_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 71:

            if (D6CLNT_FUNC_43_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 72:

            if (D6CLNT_WALK_1_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 73:

            if (D6CLNT_WALK_1_B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 74:

            if (D6CLNT_LS_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 75:

            if (D6CLNT_SEC_1 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 99:

            if (D6CLNT_API_A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 100:
            if (D6SRV_FUNC_1 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 101:
            if (D6SRV_FUNC_3 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 102:
            if (D6SRV_FUNC_19 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 103:
            if (D6SRV_FUNC_20 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 104:
            if (D6SRV_FUNC_21 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 105:
            if (D6SRV_FUNC_22 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 106:
            if (D6SRV_FUNC_23 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 107:
            if (D6SRV_FUNC_24 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 108:
            if (D6SRV_FUNC_31 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 110:
            if (D6SRV_FUNC_ALL () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 111:
            if (D6SRV_CONF_7 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 112:
            if (D6RL_FUNC_1 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 113:
            if (D6SR_FUNC_4 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 114:
            if (D6SRV_FUNC_23B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 115:
            if (D6SRV_FUNC_232B () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 116:
            if (D6SRV_FUNC_22A () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case 117:
            if (D6SRV_FUNC_17 () == OSIX_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        default:
            PRINTF ("%% No test case is defined for ID: %d\r\n", i4UtId);
            break;
    }

    /* Display the result */
    if (u4RetVal == OSIX_FAILURE)
    {
        PRINTF ("UNIT TEST ID: %-2d %-2s\r\n", i4UtId, "FAILED");
    }
    else
    {
        PRINTF ("UNIT TEST ID: %-2d %-2s\r\n", i4UtId, "PASSED");
    }
}

PUBLIC INT4
D6CLNT_API_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 17);
    Dummy2Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 2, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 2, 22);
    Dummy3Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 3, 17);
    Dummy1SetOptionRequestCode (u4IfIndex, 3, 23);

    OptionParam.u2Code = 19;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_43_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY2;
    u4IfIndex = 3;
    Dummy2Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 2, 21);

    OptionParam.u2Code = 26;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_WALK_1_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 4;
    Dummy1Register ();

    OptionParam.u2Code = 19;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_WALK_1_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();

    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_43_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);

    OptionParam.u2Code = 26;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_42_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    INT4                i4Counter = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;

    OptionParam.u2Code = 19;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }

    PRINTF ("Printing Values Received.\t\n");
    PRINTF ("-------------------------------\t\n");
    PRINTF ("AppIdId = %d\t\n", u2ModId);
    PRINTF ("IfIndex = %d\t\n", u4IfIndex);
    PRINTF ("Type = %d\t\n", OptionParam.u2Code);
    PRINTF ("OptionLength = %d\t\n", OptionParam.u2Length);
    PRINTF ("OptionValue = ");
    for (i4Counter = 0; i4Counter < OptionParam.u2Length; i4Counter++)
    {
        if (i4Counter == OptionParam.u2Length - 1)
        {
            if (OptionParam.au1Value[i4Counter] <= 15)
            {
                PRINTF ("0%x", OptionParam.au1Value[i4Counter]);
            }
            else
            {
                PRINTF ("%-2x", OptionParam.au1Value[i4Counter]);
            }
        }
        else
        {
            if (*(OptionParam.au1Value) <= 15)
            {
                PRINTF ("0%x:", OptionParam.au1Value[i4Counter]);
            }
            else
            {
                PRINTF ("%-2x:", OptionParam.au1Value[i4Counter]);
            }
        }
    }
    PRINTF ("\n ");

    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_42_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1RegisterNonFunc ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);

    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_LS_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    UINT1               au1UserClass[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT1               au1VendorClass[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT1               au1VendorSpec[DHCP6_CLNT_OPTION_SIZE_MAX];

    for (u4IfIndex = 1; u4IfIndex <= 24; u4IfIndex++)
    {

        MEMSET (au1UserClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
        MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
        MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

        u2ModId = APPLICATION_DUMMY1;
        Dummy1Register ();
        MEMCPY (au1UserClass, "CLIENT", 6);
        Dummy1UserClassSet (u4IfIndex, 1, au1UserClass, 6, 4);
        MEMSET (au1UserClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
        MEMCPY (au1UserClass, "SERVER", 6);
        Dummy1UserClassSet (u4IfIndex, 1, au1UserClass, 6, 2);

        MEMCPY (au1VendorClass, "CLIENT", 6);
        Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 6, 4);
        MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
        MEMCPY (au1VendorClass, "SERVER", 6);
        Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 6, 2);

        MEMSET (au1VendorSpec, 0, 234);
        MEMCPY (au1VendorSpec, "ARICENT", 7);
        Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 7, 1);

        Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
        Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
        Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
        Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);

        Dummy1SetOption (u4IfIndex, 1, 11);
        OptionParam.u2Code = 17;

        if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
        {
            PRINTF ("D6CLNT_LS_A :Dummy 1 D6ClApiOptionGet Return Failure \n");
        }
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_SEC_1 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    UINT1               au1UserClass[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT1               au1VendorClass[DHCP6_CLNT_OPTION_SIZE_MAX];

    u4IfIndex = 3;

    MEMSET (au1UserClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    Dummy1Register ();

    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);

    Dummy1SetOption (u4IfIndex, 1, 11);
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("D6CLNT_SEC_1 :Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_41_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    UINT1               au1UserClass[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT1               au1VendorClass[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT1               au1VendorSpec[DHCP6_CLNT_OPTION_SIZE_MAX];

    MEMSET (au1UserClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    MEMCPY (au1UserClass, "GURGARICENT", 11);
    Dummy1UserClassSet (u4IfIndex, 1, au1UserClass, 11, 4);
    MEMSET (au1UserClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMCPY (au1UserClass, "CHEARICENT", 10);
    Dummy1UserClassSet (u4IfIndex, 1, au1UserClass, 11, 2);

    MEMCPY (au1VendorClass, "BANGARICENT", 11);
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 11, 4);
    MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMCPY (au1VendorClass, "GURGARICENT", 11);
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 11, 2);

    MEMSET (au1VendorSpec, 0, 254);
    MEMCPY (au1VendorSpec, "USARICENT", 9);
    Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 9, 1);
    MEMSET (au1VendorSpec, 0, 254);
    MEMCPY (au1VendorSpec, "CHIARICENT", 10);
    Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 10, 2);

    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);

    Dummy1SetOption (u4IfIndex, 1, 11);
    Dummy1SetOption (u4IfIndex, 1, 8);
    OptionParam.u2Code = 19;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_40_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 30;

    Dummy1Register ();
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_27_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 31;

    Dummy1Register ();
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_21_F (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 23;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_21_E (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 27;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_21_D (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 26;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_21_C (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 30;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_21_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 29;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_21_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOption (u4IfIndex, 1, 11);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 17);
    OptionParam.u2Code = 18;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_8_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_8_E (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 27;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_8_F (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 26;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_8_D (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 25;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_8_C (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 24;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_8_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 23;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }

    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_3_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_3_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6CLNT_FUNC_3_C (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_4_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    Dummy1Register ();
    u4IfIndex = 3;

    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 15);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 16);

    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_CONF_4_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_11_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    Dummy1ReSetOption (u4IfIndex, 1, 1);
    OptionParam.u2Code = 22;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_9_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1ReSetOption (u4IfIndex, 1, 1);
    OptionParam.u2Code = 22;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6CLNT_FUNC_9_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    Dummy1SetOption (u4IfIndex, 1, 1);
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6CLNT_FUNC_9_C (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    Dummy1ReSetOption (u4IfIndex, 1, 1);
    OptionParam.u2Code = 17;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_7_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    /*Auth TLV set */
    OptionParam.u2Code = 22;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_7_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY2;

    u4IfIndex = 1;
    Dummy2Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 2, 23);
    Dummy1ReSetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 25);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 26);
    /*Auth TLV set */
    OptionParam.u2Code = 22;

    if (D6ClApiOptionGet (1, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_25_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    OptionParam.u2Code = 22;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_17_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1SetOption (u4IfIndex, 1, 11);
    OptionParam.u2Code = 22;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_18_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    UINT1               au1VendorSpec[DHCP6_CLNT_OPTION_SIZE_MAX];

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy2Register ();

    MEMSET (au1VendorSpec, 0, 254);
    MEMCPY (au1VendorSpec, "GURGARICENT", 11);
    Dummy1VendorSpecSet (u4IfIndex, 2, au1VendorSpec, 11, 2);
    MEMSET (au1VendorSpec, 0, 254);
    MEMCPY (au1VendorSpec, "SERVER1", 7);
    Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 7, 3);
    MEMSET (au1VendorSpec, 0, 254);
    Dummy1VendorSpecReSet (u4IfIndex, 1, 5);
    MEMCPY (au1VendorSpec, "CHEARICENT", 10);
    Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 10, 2);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_18_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    UINT1               au1VendorSpec[DHCP6_CLNT_OPTION_SIZE_MAX];

    MEMSET (au1VendorSpec, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMCPY (au1VendorSpec, "CLIENT", 6);
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 6, 3);
    MEMSET (au1VendorSpec, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMCPY (au1VendorSpec, "ARICENT", 7);
    Dummy1VendorSpecSet (u4IfIndex, 1, au1VendorSpec, 7, 5);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_16_B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    UINT1               au1VendorClass[DHCP6_CLNT_OPTION_SIZE_MAX];

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy2Register ();

    MEMSET (au1VendorClass, 0, 254);
    MEMCPY (au1VendorClass, "GURGARICENT", 11);
    Dummy1VendorClassSet (u4IfIndex, 2, au1VendorClass, 11, 2);
    MEMSET (au1VendorClass, 0, 254);
    MEMCPY (au1VendorClass, "SERVER1", 7);
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 7, 3);
    MEMSET (au1VendorClass, 0, 254);
    Dummy1VendorClassReSet (u4IfIndex, 1, 5);
    MEMCPY (au1VendorClass, "CHEARICENT", 10);
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 10, 2);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_16_A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    UINT1               au1VendorClass[DHCP6_CLNT_OPTION_SIZE_MAX];

    MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMCPY (au1VendorClass, "CLIENT", 6);
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 6, 3);
    MEMSET (au1VendorClass, 0, DHCP6_CLNT_OPTION_SIZE_MAX);
    MEMCPY (au1VendorClass, "ARICENT", 7);
    Dummy1VendorClassSet (u4IfIndex, 1, au1VendorClass, 7, 5);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_15_B (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;
    UINT1               au1UserClass[254];

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy2Register ();

    MEMSET (au1UserClass, 0, 254);
    MEMCPY (au1UserClass, "GURGARICENT", 11);
    Dummy1UserClassSet (1, 2, au1UserClass, 11, 2);
    MEMSET (au1UserClass, 0, 254);
    MEMCPY (au1UserClass, "SERVER1", 7);
    Dummy1UserClassSet (1, 1, au1UserClass, 7, 3);
    MEMSET (au1UserClass, 0, 254);
    Dummy1UserClassReSet (1, 1, 5);
    MEMCPY (au1UserClass, "CHEARICENT", 10);
    Dummy1UserClassSet (1, 1, au1UserClass, 10, 2);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_15_A (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;
    UINT1               au1UserClass[254];

    MEMSET (au1UserClass, 0, 254);
    MEMCPY (au1UserClass, "CLIENT", 6);
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1UserClassSet (1, 1, au1UserClass, 6, 3);
    MEMSET (au1UserClass, 0, 254);
    MEMCPY (au1UserClass, "ARICENT", 7);
    Dummy1UserClassSet (1, 1, au1UserClass, 7, 5);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_14_A (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1SetOption (u4IfIndex, 1, 8);
    OptionParam.u2Code = 29;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_12_A (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1SetOption (u4IfIndex, 1, 8);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_10_A (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    Dummy1Register ();
    /*Auth TLV set */
    Dummy1SetOption (u4IfIndex, 1, 11);
    OptionParam.u2Code = 21;

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6CLNT_FUNC_10_B (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    OptionParam.u2Code = 21;

    /*Configure the Realm and Key */
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_1_A (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    OptionParam.u2Code = 27;
    Dummy1Register ();

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6CLNT_FUNC_1_B (VOID)
{

    UINT2               u2ModId = 0;
    UINT4               u4IfIndex = 0;
    tDhcp6OptionInfo    OptionParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));

    u2ModId = APPLICATION_DUMMY1;

    u4IfIndex = 1;
    OptionParam.u2Code = 29;
    Dummy1Register ();

    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
fpDhcp6ClntCallback2 (UINT2 u2AppIdId, UINT4 u4IfIndex, UINT2 *pu2Type,
                      UINT2 *pu2OptionLength, UINT1 *pu1OptionValue)
{
    INT4                i4Counter = 0;
    UINT2               u2len = 0;
    PRINTF ("\r\nCall Back Arrived to Dummy3.\t \n");
    PRINTF ("Printing Values Received.\t\n");
    PRINTF ("-------------------------------\t\n");
    PRINTF ("AppIdId = %d\t\n", u2AppIdId);
    PRINTF ("IfIndex = %d\t\n", u4IfIndex);
    PRINTF ("Type = %d\t\n", *pu2Type);
    PRINTF ("OptionLength = %d\t\n", *pu2OptionLength);
    PRINTF ("OptionValue = ");
    u2len = *pu2OptionLength;
    for (i4Counter = 0; i4Counter < u2len; i4Counter++)
    {
        if (i4Counter == u2len - 1)
        {
            if (*(pu1OptionValue) <= 15)
            {
                PRINTF ("0%x", *(pu1OptionValue));
            }
            else
            {
                PRINTF ("%-2x", *(pu1OptionValue));
            }
        }
        else
        {
            if (*(pu1OptionValue) <= 15)
            {
                PRINTF ("0%x:", *(pu1OptionValue));
            }
            else
            {
                PRINTF ("%-2x:", *(pu1OptionValue));
            }
        }
        pu1OptionValue = pu1OptionValue + DHCP6_CLNT_INCR_ONE;
    }
    PRINTF ("\n ");

    return OSIX_SUCCESS;
}
PUBLIC INT4
fpDhcp6ClntCallback3 (UINT2 u2AppIdId, UINT4 u4IfIndex, UINT2 *pu2Type,
                      UINT2 *pu2OptionLength, UINT1 *pu1OptionValue)
{
    INT4                i4Counter = 0;
    UINT2               u2len = 0;
    PRINTF ("\r\nCall Back Arrived to Dummy2 \t\n");
    PRINTF ("Printing Values Received\t\n");
    PRINTF ("-------------------------------\t\n");
    PRINTF ("AppIdId = %d\t\n", u2AppIdId);
    PRINTF ("IfIndex = %d\t\n", u4IfIndex);
    PRINTF ("Type = %d\t\n", *pu2Type);
    PRINTF ("OptionLength = %d\t\n", *pu2OptionLength);
    PRINTF ("OptionValue = ");
    u2len = *pu2OptionLength;
    for (i4Counter = 0; i4Counter < u2len; i4Counter++)
    {
        if (i4Counter == u2len - 1)
        {
            if (*(pu1OptionValue) <= 15)
            {
                PRINTF ("0%x", *(pu1OptionValue));
            }
            else
            {
                PRINTF ("%-2x", *(pu1OptionValue));
            }
        }
        else
        {
            if (*(pu1OptionValue) <= 15)
            {
                PRINTF ("0%x:", *(pu1OptionValue));
            }
            else
            {
                PRINTF ("%-2x:", *(pu1OptionValue));
            }
        }
        pu1OptionValue = pu1OptionValue + DHCP6_CLNT_INCR_ONE;
    }
    PRINTF ("\n ");
    return OSIX_SUCCESS;
}
PUBLIC INT4
fpDhcp6ClntCallback (UINT2 u2AppIdId, UINT4 u4IfIndex,
                     UINT2 *pu2Type,
                     UINT2 *pu2OptionLength, UINT1 *pu1OptionValue)
{
    INT4                i4Counter = 0;
    UINT2               u2len = 0;
    PRINTF ("\r\nCall Back Arrived to Dummy1\t\n");
    PRINTF ("Printing Values Received\t\n");
    PRINTF ("-------------------------------\t\n");
    PRINTF ("AppIdId = %d\t\n", u2AppIdId);
    PRINTF ("IfIndex = %d\t\n", u4IfIndex);
    PRINTF ("Type = %d\t\n", *pu2Type);
    PRINTF ("OptionLength = %d\t\n", *pu2OptionLength);
    PRINTF ("OptionValue = ");
    u2len = *pu2OptionLength;
    for (i4Counter = 0; i4Counter < u2len; i4Counter++)
    {
        if (i4Counter == u2len - 1)
        {
            if (*(pu1OptionValue) <= 15)
            {
                PRINTF ("0%x", *(pu1OptionValue));
            }
            else
            {
                PRINTF ("%-2x", *(pu1OptionValue));
            }
        }
        else
        {
            if (*(pu1OptionValue) <= 15)
            {
                PRINTF ("0%x:", *(pu1OptionValue));
            }
            else
            {
                PRINTF ("%-2x:", *(pu1OptionValue));
            }
        }
        pu1OptionValue = pu1OptionValue + DHCP6_CLNT_INCR_ONE;
    }
    PRINTF ("\n ");

    return OSIX_SUCCESS;
}

PUBLIC UINT4
D6ClTestParse14 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    tDhcp6ClntAppParam  AppParam;
    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    UINT1               au1Pdu[100];
    tDhcp6OptionInfo    Option;
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (&Option, 0, sizeof (tDhcp6OptionInfo));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x02;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x03;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 17) ==
        OSIX_FAILURE)
    {
        /*    return OSIX_FAILURE; */
    }
    Option.u2Code = 5;
    D6ClApiOptionGet (2, 9, &Option);
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FAILURE)
    {
        return OSIX_FALSE;
    }
    D6ClApiOptionGet (2, 9, &Option);

    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse13 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    tDhcp6ClntAppParam  AppParam;
    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    UINT1               au1Pdu[100];
    tDhcp6OptionInfo    Option;
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (&Option, 0, sizeof (tDhcp6OptionInfo));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x02;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x03;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 17) ==
        OSIX_FAILURE)
    {
        /*    return OSIX_FALSE; */
    }
    Option.u2Code = 2;
    D6ClApiOptionGet (2, 9, &Option);
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FAILURE)
    {
        return OSIX_FALSE;
    }
    D6ClApiOptionGet (2, 9, &Option);

    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse12 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x04;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    au1Pdu[5] = 0x20;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse11 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x02;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x0b;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x20;
    /*Value */
    au1Pdu[14] = 0x03;
    au1Pdu[15] = 0x01;
    au1Pdu[16] = 0x00;

    au1Pdu[17] = 0x01;
    au1Pdu[18] = 0x01;
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;
    au1Pdu[24] = 0x20;

    au1Pdu[25] = 0x01;
    au1Pdu[26] = 0x01;
    au1Pdu[27] = 0x00;

    au1Pdu[28] = 0x01;
    au1Pdu[29] = 0x01;
    au1Pdu[30] = 0x00;
    au1Pdu[31] = 0x03;
    au1Pdu[32] = 0x02;
    au1Pdu[33] = 0x02;
    au1Pdu[34] = 0x02;
    au1Pdu[35] = 0x20;
    au1Pdu[36] = 0x01;
    au1Pdu[37] = 0x00;

    au1Pdu[38] = 0x01;
    au1Pdu[39] = 0x01;
    au1Pdu[40] = 0x00;
    au1Pdu[41] = 0x03;
    au1Pdu[42] = 0x02;
    au1Pdu[43] = 0x02;
    au1Pdu[44] = 0x02;
    au1Pdu[45] = 0x20;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 46) ==
        OSIX_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse10 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x04;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    au1Pdu[5] = 0x20;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;

}

PUBLIC UINT4
D6ClTestParse8 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x0b;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    au1Pdu[7] = 0x0c;

    au1Pdu[8] = 0x01;
    au1Pdu[9] = 0x01;

    /*Type */
    au1Pdu[10] = 0x01;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;

}

PUBLIC UINT4
D6ClTestParse7 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x0b;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    au1Pdu[7] = 0x0c;
    au1Pdu[8] = 0x01;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x01;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;

}

PUBLIC UINT4
D6ClTestParse6 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x0b;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    au1Pdu[7] = 0x0c;

    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x01;

    /*Type */
    au1Pdu[10] = 0x00;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[7] = 0x0c;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;

}

PUBLIC UINT4
D6ClTestParse9 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x0b;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[7] = 0x0c;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;

}

PUBLIC UINT4
D6ClTestParse5 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x03;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x0b;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    au1Pdu[7] = 0x0c;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[7] = 0x0c;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 20) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;

}

PUBLIC UINT4
D6ClTestParse4 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x01;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x40;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x01;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) != OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[11] = 0x04;
    /*with out server TLV */
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    au1Pdu[11] = 0x02;
    au1Pdu[18] = 0x0b;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse3 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x01;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x40;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x02;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    /*Type */
    au1Pdu[17] = 0x00;
    au1Pdu[18] = 0x38;
    /*Length */
    au1Pdu[19] = 0x00;
    au1Pdu[20] = 0x03;
    /*Value */
    au1Pdu[21] = 0x02;
    au1Pdu[22] = 0x02;
    au1Pdu[23] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 6, 24) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse2 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x01;

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x02;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x03;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;
    /*Value */
    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 17) !=
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse1 ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[1] = 0x00;
    au1Pdu[2] = 0x00;
    au1Pdu[3] = 0x01;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[4] = 0x01;
    au1Pdu[5] = 0xf2;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[4] = 0x03;
    au1Pdu[5] = 0xff;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    au1Pdu[4] = 0x01;
    au1Pdu[5] = 0x02;
    au1Pdu[6] = 0x02;
    au1Pdu[7] = 0x02;

    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x03;
    au1Pdu[10] = 0x02;
    au1Pdu[11] = 0x02;
    au1Pdu[12] = 0x02;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 13) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x02;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x03;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;

    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 19) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    /*Type */
    au1Pdu[4] = 0x00;
    au1Pdu[5] = 0x02;
    /*Length */
    au1Pdu[6] = 0x00;
    au1Pdu[7] = 0x02;

    /*Value */
    au1Pdu[8] = 0x02;
    au1Pdu[9] = 0x02;

    /*Type */
    au1Pdu[10] = 0x00;
    au1Pdu[11] = 0x03;
    /*Length */
    au1Pdu[12] = 0x00;
    au1Pdu[13] = 0x03;

    au1Pdu[14] = 0x02;
    au1Pdu[15] = 0x02;
    au1Pdu[16] = 0x02;
    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 17) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestParse ()
{
    tDhcp6ClntUdpHeader Dhcp6ClntUdpHeaderInfo;
    UINT1               au1Pdu[100];
    MEMSET (&Dhcp6ClntUdpHeaderInfo, 0, sizeof (tDhcp6ClntUdpHeader));
    MEMSET (au1Pdu, 0, 100);

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 1, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 541;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 547;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 546;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    nmhSetFsDhcp6ClntIfRowStatus (9, NOT_IN_SERVICE);

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    nmhSetFsDhcp6ClntIfRowStatus (9, ACTIVE);

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    Dhcp6ClntUdpHeaderInfo.u2SrcPort = 546;
    Dhcp6ClntUdpHeaderInfo.u2DstPort = 547;

    au1Pdu[0] = 0x0b;
    au1Pdu[0] = 0x00;
    au1Pdu[0] = 0x00;
    au1Pdu[0] = 0x01;

    if (D6ClProcessPacket (&Dhcp6ClntUdpHeaderInfo, au1Pdu, 9, 100) ==
        OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}
PUBLIC UINT4
D6ClTestApiOptionWithGet ()
{
    tDhcp6OptionInfo    Option;
    tDhcp6ClntAppParam  AppParam;
    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    MEMSET (&Option, 0, sizeof (tDhcp6OptionInfo));

    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    Option.u2Code = 9;
    Option.u2Length = 9;

    if (D6ClApiOptionGet (2, 9, &Option) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}

PUBLIC UINT4
D6ClTestApiOptionGet ()
{
    tDhcp6OptionInfo   *pOption = NULL;
    tDhcp6OptionInfo    Option;
    tDhcp6ClntAppParam  AppParam;
    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    MEMSET (&Option, 0, sizeof (tDhcp6OptionInfo));

    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiOptionGet (0, 1, pOption) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiOptionGet (110, 1, pOption) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiOptionGet (1, 1, pOption) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiOptionGet (1, 9, pOption) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    Option.u2Code = 9;
    Option.u2Length = 9;

    if (D6ClApiOptionGet (2, 9, &Option) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6ClntIfRowStatus (9, NOT_IN_SERVICE);
    if (D6ClApiOptionGet (2, 9, &Option) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    nmhSetFsDhcp6ClntIfRowStatus (9, ACTIVE);
    if (D6ClApiOptionGet (2, 9, &Option) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}

PRIVATE UINT4
D6ClTestApiOptionSet ()
{
    tDhcp6ClntAppOptionParam OptionParam;
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&OptionParam, 0, sizeof (tDhcp6ClntAppOptionParam));
    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));

    if (D6ClApiOptionSet (0, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiOptionSet (220, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiOptionSet (1, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u4IfIndex = 9;
    OptionParam.u1OptionType = 1;
    OptionParam.u1Action = 1;
    OptionParam.u2Code = 1;
    OptionParam.u2ValueLength = 3;
    MEMCPY (OptionParam.au1Value, "SSS", OptionParam.u2ValueLength);

    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    OptionParam.u4IfIndex = 0;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u4IfIndex = 9;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 2;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 3;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 4;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 5;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 6;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    OptionParam.u1Action = 2;
    OptionParam.u4IfIndex = 0;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u4IfIndex = 9;
    OptionParam.u1OptionType = 1;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 2;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 3;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 4;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 5;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1OptionType = 6;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    OptionParam.u1Action = 89;

    OptionParam.u1OptionType = 5;
    if (D6ClApiOptionSet (2, &OptionParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC VOID
D6ClCallFunction ()
{
}
PRIVATE UINT4
D6ClApiUnRegisterTest ()
{
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    if (D6ClApiDeRegisterApplication (0) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiDeRegisterApplication (22220) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiDeRegisterApplication (2) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE UINT4
D6ClApiRegisterTest ()
{
    tDhcp6ClntAppParam *pAppParam = NULL;
    tDhcp6ClntAppParam  AppParam;

    MEMSET (&AppParam, 0, sizeof (tDhcp6ClntAppParam));
    if (D6ClApiRegisterApplication (0, &AppParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiRegisterApplication (22220, &AppParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    if (D6ClApiRegisterApplication (2, pAppParam) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }
    AppParam.u2AppId = 2;
    AppParam.fpDhcp6ClntCallback = fpDhcp6ClntCallback;
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    AppParam.fpDhcp6ClntCallback = NULL;
    if (D6ClApiRegisterApplication (2, &AppParam) == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PRIVATE UINT4
D6ClSetTimerFunction ()
{
    PRIVATE tDhcp6ClntIfInfo Dhcp6ClntIfInfo;
    MEMSET (&Dhcp6ClntIfInfo, 0, sizeof (tDhcp6ClntIfInfo));
    D6ClTmrStart (&Dhcp6ClntIfInfo, 1, 2);
    D6ClTmrStopTimer (&Dhcp6ClntIfInfo, 1);
    D6ClTmrStopAllTimer (&Dhcp6ClntIfInfo);

    D6ClTmrStart (&Dhcp6ClntIfInfo, 100, 2);
    D6ClTmrStopTimer (&Dhcp6ClntIfInfo, 100);
    D6ClTmrStopAllTimer (&Dhcp6ClntIfInfo);

    return OSIX_TRUE;
}

PRIVATE UINT4
D6ClTimerFunction ()
{
    tDhcp6ClntIfInfo   *pDhcp6ClntIfInfo = NULL;
    D6ClTmrStart (pDhcp6ClntIfInfo, 1, 2);
    D6ClTmrStopTimer (pDhcp6ClntIfInfo, 1);
    D6ClTmrStopAllTimer (pDhcp6ClntIfInfo);
    return OSIX_TRUE;
}

PRIVATE UINT4
D6ClGenerateTrap ()
{
    tDhcp6CInvalidMsgRxdTrap Dhcp6CInvalidMsgRxdTrap;
    tDhcp6CHmacFailMsgRxdTrap Dhcp6CHmacFailMsgRxdTrap;

    Dhcp6CInvalidMsgRxdTrap.u4IfIndex = 9;
    Dhcp6CHmacFailMsgRxdTrap.u4IfIndex = 9;
    D6ClTrapSnmpSend (&Dhcp6CInvalidMsgRxdTrap, 1);
    D6ClTrapSnmpSend (&Dhcp6CHmacFailMsgRxdTrap, 2);
    return OSIX_TRUE;
}

/*------ UTILITIES -------------------*/
UINT4
D6ClGetAllCliObjectsObject ()
{
    tCliHandle          CliHandle;
    UINT1               au1[100];

    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;

    MEMSET (&CliHandle, 0, sizeof (tCliHandle));
    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RealmName.i4_Length = 3;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    KeyName.i4_Length = 3;

    if (D6ClCliSetInitRetraTime (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (D6ClCliSetMaxRetranTime (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranCount (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranDelay (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetSysLogStatus (CliHandle, 12) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetRefreshTimerVal (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetRefreshTimerVal (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetInitRetraTime (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (D6ClCliSetMaxRetranTime (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranCount (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranDelay (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetInitRetraTime (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranTime (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranCount (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetMaxRetranDelay (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetDuidIfIndex (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (D6ClCliSetDuidType (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetRealmKey (CliHandle, 1, 1, au1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliEnableClientFun (CliHandle, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliDisableClientFun (CliHandle, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliClearStatsInterface (CliHandle, 1, 1) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetTraps (CliHandle, 333, 22221) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (D6ClCliSetUdpPorts (CliHandle, 5, 222221) == CLI_SUCCESS)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;

}

UINT4
D6ClGetAllObjectsObject ()
{
    INT4                u4First;
    UINT4               i4First;

    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RealmName.i4_Length = 3;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    KeyName.i4_Length = 3;

    if (nmhValidateIndexInstanceFsDhcp6ClntIfTable (1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFirstIndexFsDhcp6ClntIfTable (&u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6ClntIfTable (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetNextIndexFsDhcp6ClntIfTable (2, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (nmhGetFsDhcp6ClntIfDuidType (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfDuid (1, &RealmName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfDuidIfIndex (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfMaxRetCount (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfMaxRetDelay (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfMaxRetTime (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfInitRetTime (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (nmhGetFsDhcp6ClntIfCurrRetTime (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfMinRefreshTime (1, &i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (nmhGetFsDhcp6ClntIfRealmName (1, &RealmName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfKey (1, &KeyName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfKeyId (1, &i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfInformOut (1, &i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfReplyIn (1, &i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    if (nmhGetFsDhcp6ClntIfInvalidPktIn (1, &i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfHmacFailCount (1, &i4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfCounterRest (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhGetFsDhcp6ClntIfRowStatus (1, &u4First) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}

UINT4
D6ClAllDepObject ()
{
    UINT4               u4ErrorCode;
    tSnmpIndexList     *pSnmpIndexList = NULL;
    tSNMP_VAR_BIND     *pSnmpVarBind = NULL;

    nmhDepv2FsDhcp6ClntTrapAdminControl (&u4ErrorCode, pSnmpIndexList,
                                         pSnmpVarBind);
    nmhDepv2FsDhcp6ClntDebugTrace (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6ClntSysLogAdminStatus (&u4ErrorCode, pSnmpIndexList,
                                          pSnmpVarBind);
    nmhDepv2FsDhcp6ClntListenPort (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    nmhDepv2FsDhcp6ClntIfTable (&u4ErrorCode, pSnmpIndexList, pSnmpVarBind);
    return OSIX_TRUE;
}

UINT4
D6ClValidateAllSetObject ()
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RealmName.i4_Length = 3;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    KeyName.i4_Length = 3;

    if (nmhSetFsDhcp6ClntIfDuidType (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfDuidIfIndex (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfMaxRetCount (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfMaxRetDelay (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfMaxRetTime (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfInitRetTime (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfMinRefreshTime (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRealmName (22, &RealmName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfKey (22, &KeyName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfKeyId (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfCounterRest (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 2) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 3) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 4) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 5) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 6) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhSetFsDhcp6ClntIfRowStatus (22, 7) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

UINT4
D6ClValidateAllTestObject ()
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT4               u4ErrorCode;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RealmName.i4_Length = 3;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    KeyName.i4_Length = 3;

    if (nmhTestv2FsDhcp6ClntIfDuidType (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfDuidIfIndex (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfMaxRetCount (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfMaxRetDelay (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfMaxRetTime (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfInitRetTime (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfMinRefreshTime (&u4ErrorCode, 22, 1) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRealmName (&u4ErrorCode, 22, &RealmName) ==
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfKey (&u4ErrorCode, 22, &KeyName) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfKeyId (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfCounterRest (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 1) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 2) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 3) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 4) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 5) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 6) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    if (nmhTestv2FsDhcp6ClntIfRowStatus (&u4ErrorCode, 22, 7) == SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SR_FUNC_4 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1ReSetOption (u4IfIndex, 1, 1);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    OptionParam.u2Code = 22;
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6RL_FUNC_1 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_1 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6SRV_FUNC_3 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_17 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);

    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_19 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 21;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6SRV_FUNC_20 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6SRV_FUNC_21 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_22A (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_22 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    OptionParam.u2Code = 24;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_232B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_23B (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_23 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    OptionParam.u2Code = 22;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6SRV_FUNC_24 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 24);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 17);
    Dummy1SetOption (u4IfIndex, 1, 11);
    Dummy1SetOption (u4IfIndex, 1, 8);
    OptionParam.u2Code = 19;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
PUBLIC INT4
D6SRV_FUNC_31 (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 23);
    OptionParam.u2Code = 24;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_CONF_7 ()
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOption (u4IfIndex, 1, 11);
    OptionParam.u2Code = 31;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}

PUBLIC INT4
D6SRV_FUNC_ALL (VOID)
{
    tDhcp6OptionInfo    OptionParam;
    UINT4               u4IfIndex = 0;
    UINT2               u2ModId = 0;
    MEMSET (&OptionParam, 0, sizeof (tDhcp6OptionInfo));
    u2ModId = APPLICATION_DUMMY1;
    u4IfIndex = 3;
    Dummy1Register ();
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 21);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 22);
    Dummy1SetOptionRequestCode (u4IfIndex, 1, 17);
    OptionParam.u2Code = 24;
    if (D6ClApiOptionGet (u2ModId, u4IfIndex, &OptionParam) == OSIX_FAILURE)
    {
        PRINTF ("Dummy 1 D6ClApiOptionGet Return Failure \n");
    }
    return OSIX_TRUE;
}
#endif
