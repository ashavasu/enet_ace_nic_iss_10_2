/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstst.h
 *
 * Description: Header file for DHCP6C test module
 *********************************************************************/
#ifndef _DHC6C_TEST_H
#define _DHC6C_TEST_H

/* Test function prototypes */
VOID D6ClTestExecUT (INT4 i4UtId);

#endif /* _DHC6CTEST_H_ */
