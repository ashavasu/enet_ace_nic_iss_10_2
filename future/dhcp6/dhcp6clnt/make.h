#!/bin/csh
# (C) 2006 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :Aricent					     |	
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10 May 2006                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

DHCP6_CLNT_BASE_DIR = ${BASE_DIR}/dhcp6/dhcp6clnt
DHCP6_CLNT_SRC_DIR  = ${DHCP6_CLNT_BASE_DIR}/src
DHCP6_CLNT_INC_DIR  = ${DHCP6_CLNT_BASE_DIR}/inc
DHCP6_CLNT_OBJ_DIR  = ${DHCP6_CLNT_BASE_DIR}/obj
ifeq (DDHCP6_CLNT_TEST_WANTED, $(findstring DDHCP6_CLNT_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
DHCP6_CLNT_TEST_DIR  = ${DHCP6_CLNT_BASE_DIR}/test
DHCP6_CLNT_TEST_INC_DIR  = ${DHCP6_CLNT_BASE_DIR}/test
endif

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${DHCP6_CLNT_INC_DIR} -I${DHCP6_CLNT_TEST_INC_DIR} -I${COMMON_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
