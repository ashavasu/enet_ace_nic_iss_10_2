#!/bin/csh
# (C) 2009 Future Software Pvt. Ltd.
# $Id: make.h,v 1.3 2014/06/23 11:25:51 siva Exp $  
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent                                       |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 14 Sep 2009                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

DHCP6_RLY_BASE_DIR = ${BASE_DIR}/dhcp6/dhcp6rly
DHCP6_RLY_SRC_DIR  = ${DHCP6_RLY_BASE_DIR}/src
DHCP6_RLY_INC_DIR  = ${DHCP6_RLY_BASE_DIR}/inc
DHCP6_RLY_OBJ_DIR  = ${DHCP6_RLY_BASE_DIR}/obj


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${DHCP6_RLY_INC_DIR} -I${DHCP6_RLY_TEST_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################

