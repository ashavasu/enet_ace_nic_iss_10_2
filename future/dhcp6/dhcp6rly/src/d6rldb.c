/*******************************************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rldb.c,v 1.2 2014/07/10 12:23:57 siva Exp $
 *
 * DESC : This File contains RB Tree  processing of Prefix Delegation options to
 *       maintaining  prefixes that has been delegated from server to client.
 *******************************************************************************************/

#include "d6rlinc.h"

/***************************************************************************
 * FUNCTION NAME    : D6RlPDCreateEntry
 *
 * DESCRIPTION      : This function creates the Prefix Delegation table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlPDCreateEntry (VOID)
{
    DHCP6_RLY_PD_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6RlyPDEntry, RbPDRouteNode),
                              D6RlPDRBCmp);
    if (DHCP6_RLY_PD_TABLE == NULL)
    {
        DHCP6_RLY_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "DHCP6_RLY_PD_TABLE: "
                       " RB Tree Creation failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDDeleteEntry
 *
 * DESCRIPTION      : This function Deletes the PD table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlPDDeleteEntry (VOID)
{
    if (DHCP6_RLY_PD_TABLE != NULL)
    {
        RBTreeDestroy (DHCP6_RLY_PD_TABLE, D6RlPDRBFree, 0);
        DHCP6_RLY_PD_TABLE = NULL;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlPDRBCmp                                                */
/*                                                                           */
/* Description  : This function compare the interface ids to RB tree         */
/*                operations in interface table                              */
/*                                                                           */
/* Input        : e1        Pointer to PD node1                       */
/*                e2        Pointer to PD node2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. 0 if keys of both the elements are same.   */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/

PUBLIC INT4
D6RlPDRBCmp (tRBElem * e1, tRBElem * e2)
{
    tDhcp6RlyPDEntry   *pPDEntry1 = (tDhcp6RlyPDEntry *) e1;
    tDhcp6RlyPDEntry   *pPDEntry2 = (tDhcp6RlyPDEntry *) e2;
    INT4                i4RetVal = 0;

    i4RetVal =
        MEMCMP (&(pPDEntry1->Prefix), &(pPDEntry2->Prefix), sizeof (tIp6Addr));

    if (i4RetVal > 0)
    {
        return DHCP6_RLY_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return DHCP6_RLY_LESS;
    }

    if (pPDEntry1->u1PrefixLen > pPDEntry2->u1PrefixLen)
    {
        return DHCP6_RLY_GREATER;
    }
    else if (pPDEntry1->u1PrefixLen < pPDEntry2->u1PrefixLen)
    {
        return DHCP6_RLY_LESS;
    }

    return DHCP6_RLY_EQUAL;

}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDCreateNode 
 *
 * DESCRIPTION      : This function creates a PD table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyPDEntry * - Pointer to the created Prefix 
 *                    Delegation  node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyPDEntry *
D6RlPDCreateNode (VOID)
{
    tDhcp6RlyPDEntry   *pPDEntry = NULL;

    pPDEntry = (tDhcp6RlyPDEntry *) MemAllocMemBlk (DHCP6_RLY_PD_TBL_POOL_ID);
    if (pPDEntry == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to allocate memory for Prefix delegation table\r\n");

        return NULL;
    }
    MEMSET (pPDEntry, 0x00, sizeof (tDhcp6RlyPDEntry));
    /*Initialize DbNode for redundancy */
    Dhcp6RlyRedDbNodeInit (&(pPDEntry->RbPrefixDelegationDbNode));
    return pPDEntry;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDAddNode
 *
 * DESCRIPTION      : This function add a node to the Prefix Delegation table.
 *
 * INPUT            : pPDEntry - pointer to Prefix delegation information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlPDAddNode (tDhcp6RlyPDEntry * pPDEntry)
{
    if (RBTreeAdd (DHCP6_RLY_PD_TABLE, pPDEntry) != RB_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to allocate memory for Prefix Delegation table\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDDelNode
 *
 * DESCRIPTION      : This function delete a PD node from the
 *                    Prefix Delegation table and release the memory used by that
 *                    node to the memory pool. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pPDEntry - pointer to Prefix Delegation information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlPDDelNode (tDhcp6RlyPDEntry * pPDEntry)
{
    if (OSIX_FAILURE == D6RlTmrStopTimer (&(pPDEntry->Dhcp6RlyTmrBlk)))
    {
        DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                            "D6RlPDDelNode : Unable to stop timer for prefix : %s, prefix length: %d, nexthop %s\r\n",
                            Ip6PrintAddr (&pPDEntry->Prefix),
                            pPDEntry->u1PrefixLen,
                            Ip6PrintAddr (&pPDEntry->NextHop));

        DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "D6RlPDDelNode : Unable to stop timer for prefix: %s, prefix length: %d, nexthop %s\r\n",
                            Ip6PrintAddr (&pPDEntry->Prefix),
                            pPDEntry->u1PrefixLen,
                            Ip6PrintAddr (&pPDEntry->NextHop)));

        return;
    }
    RBTreeRem (DHCP6_RLY_PD_TABLE, pPDEntry);
    MemReleaseMemBlock (DHCP6_RLY_PD_TBL_POOL_ID, (UINT1 *) pPDEntry);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a PD node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the I/face node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlPDRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    MemReleaseMemBlock (DHCP6_RLY_PD_TBL_POOL_ID, (UINT1 *) pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDGetFirstNode
 *
 * DESCRIPTION      : This function returns the first PD node
 *                    from the Prefix Delegation table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyPDEntry * - pointer to the first PD info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyPDEntry *
D6RlPDGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_RLY_PD_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDGetNextNode
 *
 * DESCRIPTION      : This function returns the next PD info
 *                    from the Prefix delegation table.
 *
 * INPUT            : pIp6Prefix - Ipv6 Prefix delegated.
 *                    u1Prefixlength -Prefix length delegated
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyPDEntry * - Pointer to the next Prefix delegation
 *                     info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyPDEntry *
D6RlPDGetNextNode (tIp6Addr * pIp6Prefix, UINT1 u1Prefixlength)
{
    tDhcp6RlyPDEntry    PDEntry;
    MEMSET (&PDEntry, 0, sizeof (tDhcp6RlyPDEntry));

    MEMCPY (&PDEntry.Prefix, pIp6Prefix, sizeof (tIp6Addr));
    PDEntry.u1PrefixLen = u1Prefixlength;
    return RBTreeGetNext (DHCP6_RLY_PD_TABLE, (tRBElem *) & PDEntry, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlPDGetNode
 *
 * DESCRIPTION      : This function helps to find a PD node from
 *                    the Prefix Delegation table.
 *
 * INPUT            : pIp6Prefix - Ipv6 Prefix delegated.
 *                    u1Prefixlength -Prefix length delegated 
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyPDEntry * - Pointer to the PD info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyPDEntry *
D6RlPDGetNode (tIp6Addr * pIp6Prefix, UINT1 u1Prefixlength)
{
    tDhcp6RlyPDEntry    Key;
    MEMSET (&Key, 0, sizeof (tDhcp6RlyPDEntry));

    MEMCPY (&(Key.Prefix), pIp6Prefix, sizeof (tIp6Addr));
    Key.u1PrefixLen = u1Prefixlength;
    return RBTreeGet (DHCP6_RLY_PD_TABLE, (tRBElem *) & Key);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6rldb.c                      */
/*-----------------------------------------------------------------------*/
