/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6rlw.c,v 1.36 2016/04/29 09:10:20 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fssnmp.h"
# include  "d6rlinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyDebugTrace
 Input       :  The Indices

                The Object 
                retValFsDhcp6RlyDebugTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyDebugTrace (tSNMP_OCTET_STRING_TYPE *
                            pRetValFsDhcp6RlyDebugTrace)
{
    pRetValFsDhcp6RlyDebugTrace->i4_Length =
        D6RlTrcGetTraceInputValue (pRetValFsDhcp6RlyDebugTrace->
                                   pu1_OctetList, DHCP6_RLY_DEBUG_TRACE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyTrapAdminControl
 Input       :  The Indices

                The Object 
                retValFsDhcp6RlyTrapAdminControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyTrapAdminControl (tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsDhcp6RlyTrapAdminControl)
{
    pRetValFsDhcp6RlyTrapAdminControl->pu1_OctetList[0] =
        DHCP6_RLY_TRAP_CONTROL;
    pRetValFsDhcp6RlyTrapAdminControl->i4_Length = 1;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlySysLogAdminStatus
 Input       :  The Indices

                The Object 
                retValFsDhcp6RlySysLogAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlySysLogAdminStatus (INT4 *pi4RetValFsDhcp6RlySysLogAdminStatus)
{
    *pi4RetValFsDhcp6RlySysLogAdminStatus = DHCP6_RLY_SYS_LOG_OPTION;
    return SNMP_SUCCESS;
}

INT1
nmhGetFsDhcp6RlyListenPort (INT4 *pi4RetValFsDhcp6RlyListenPort)
{
    *pi4RetValFsDhcp6RlyListenPort = DHCP6_RLY_LISTEN_PORT;
    return SNMP_SUCCESS;
}

INT1
nmhGetFsDhcp6RlyClientTransmitPort (INT4 *pi4RetValFsDhcp6RlyClientTransmitPort)
{
    *pi4RetValFsDhcp6RlyClientTransmitPort = DHCP6_RLY_CLNT_TRANSMIT_PORT;
    return SNMP_SUCCESS;
}

INT1
nmhGetFsDhcp6RlyServerTransmitPort (INT4 *pi4RetValFsDhcp6RlyServerTransmitPort)
{
    *pi4RetValFsDhcp6RlyServerTransmitPort = DHCP6_RLY_SRV_TRANSMIT_PORT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyOption37Control
 Input       :  The Indices

                The Object
                retValFsDhcp6RlyOption37Control
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyOption37Control (INT4 *pi4RetValFsDhcp6RlyOption37Control)
{
    *pi4RetValFsDhcp6RlyOption37Control = DHCP6_RLY_REMOTEID_STATUS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyPDRouteControl
 Input       :  The Indices

                The Object
                retValFsDhcp6RlyPDRouteControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyPDRouteControl (INT4 *pi4RetValFsDhcp6RlyPDRouteControl)
{
    *pi4RetValFsDhcp6RlyPDRouteControl = DHCP6_RLY_PDROUTE_STATUS;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyDebugTrace
 Input       :  The Indices

                The Object 
                setValFsDhcp6RlyDebugTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyDebugTrace (tSNMP_OCTET_STRING_TYPE *
                            pSetValFsDhcp6RlyDebugTrace)
{
    UINT4               u4TrcOption = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    u4TrcOption = D6RlTrcGetTraceOptionValue
        (pSetValFsDhcp6RlyDebugTrace->pu1_OctetList,
         pSetValFsDhcp6RlyDebugTrace->i4_Length);

    if ((STRNCMP (pSetValFsDhcp6RlyDebugTrace->pu1_OctetList,
                  "enable", STRLEN ("enable"))) == 0)
    {
        DHCP6_RLY_DEBUG_TRACE |= u4TrcOption;
        i1RetVal = SNMP_SUCCESS;
    }
    else if ((STRNCMP (pSetValFsDhcp6RlyDebugTrace->pu1_OctetList,
                       "disable", STRLEN ("disable"))) == 0)
    {
        DHCP6_RLY_DEBUG_TRACE &= ~u4TrcOption;
        i1RetVal = SNMP_SUCCESS;
    }
    /* Default case. if nothing is appended, assume critical is done */
    else if ((STRNCMP
              (pSetValFsDhcp6RlyDebugTrace->pu1_OctetList, "critical",
               STRLEN ("critical"))) == 0)
    {
        DHCP6_RLY_DEBUG_TRACE = u4TrcOption;
        i1RetVal = SNMP_SUCCESS;
    }
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyTrapAdminControl
 Input       :  The Indices

                The Object 
                setValFsDhcp6RlyTrapAdminControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyTrapAdminControl (tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsDhcp6RlyTrapAdminControl)
{
    DHCP6_RLY_TRAP_CONTROL =
        pSetValFsDhcp6RlyTrapAdminControl->pu1_OctetList[0];
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlySysLogAdminStatus
 Input       :  The Indices

                The Object 
                setValFsDhcp6RlySysLogAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlySysLogAdminStatus (INT4 i4SetValFsDhcp6RlySysLogAdminStatus)
{
    DHCP6_RLY_SYS_LOG_OPTION = i4SetValFsDhcp6RlySysLogAdminStatus;
    return SNMP_SUCCESS;
}

INT1
nmhSetFsDhcp6RlyListenPort (INT4 i4SetValFsDhcp6RlyListenPort)
{
    INT4                i4SavedPort = 0;
    if (i4SetValFsDhcp6RlyListenPort == DHCP6_RLY_LISTEN_PORT)
    {
        return SNMP_SUCCESS;
    }
    i4SavedPort = DHCP6_RLY_LISTEN_PORT;
    DHCP6_RLY_LISTEN_PORT = i4SetValFsDhcp6RlyListenPort;
    /* If no interface is created in relay then don't create the socket 
     */
    if (D6RlIfGetFirstNode () == NULL)
    {
        return SNMP_SUCCESS;
    }
    D6RlSktDeInit ();
    if (D6RlSktInit () == OSIX_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC |
                       DHCP6_RLY_MGMT_TRC, "Cannot create" "Socket \r\n");
        DHCP6_RLY_LISTEN_PORT = i4SavedPort;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

INT1
nmhSetFsDhcp6RlyClientTransmitPort (INT4 i4SetValFsDhcp6RlyClientTransmitPort)
{
    DHCP6_RLY_CLNT_TRANSMIT_PORT = i4SetValFsDhcp6RlyClientTransmitPort;
    return SNMP_SUCCESS;
}

INT1
nmhSetFsDhcp6RlyServerTransmitPort (INT4 i4SetValFsDhcp6RlyServerTransmitPort)
{
    DHCP6_RLY_SRV_TRANSMIT_PORT = i4SetValFsDhcp6RlyServerTransmitPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyOption37Control
 Input       :  The Indices

                The Object
                setValFsDhcp6RlyOption37Control
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyOption37Control (INT4 i4SetValFsDhcp6RlyOption37Control)
{
    DHCP6_RLY_REMOTEID_STATUS = i4SetValFsDhcp6RlyOption37Control;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyPDRouteControl
 Input       :  The Indices

                The Object
                setValFsDhcp6RlyPDRouteControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyPDRouteControl (INT4 i4SetValFsDhcp6RlyPDRouteControl)
{
    if (DHCP6_RLY_PDROUTE_STATUS == (BOOL1) i4SetValFsDhcp6RlyPDRouteControl)
    {
        return SNMP_SUCCESS;
    }

    DHCP6_RLY_PDROUTE_STATUS = (BOOL1) i4SetValFsDhcp6RlyPDRouteControl;

    if (i4SetValFsDhcp6RlyPDRouteControl == DHCP6_RLY_PDROUTE_DISABLE)
    {
        Dhcp6RlyRouteControlDisable ();
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyDebugTrace
 Input       :  The Indices

                The Object 
                testValFsDhcp6RlyDebugTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyDebugTrace (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsDhcp6RlyDebugTrace)
{
    UINT4               u4TrcOption = 0;

    if ((pTestValFsDhcp6RlyDebugTrace->i4_Length >= DHCP6_RLY_TRC_MAX_SIZE) ||
        (pTestValFsDhcp6RlyDebugTrace->i4_Length < DHCP6_RLY_TRC_MIN_SIZE))
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC |
                       DHCP6_RLY_MGMT_TRC, "Invalid Trace Input" "length \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u4TrcOption = D6RlTrcGetTraceOptionValue
        (pTestValFsDhcp6RlyDebugTrace->pu1_OctetList,
         pTestValFsDhcp6RlyDebugTrace->i4_Length);

    if (u4TrcOption == DHCP6_RLY_INVALID_TRC)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC |
                       DHCP6_RLY_MGMT_TRC, "Invalid Trace Input String \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyTrapAdminControl
 Input       :  The Indices

                The Object 
                testValFsDhcp6RlyTrapAdminControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyTrapAdminControl (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsDhcp6RlyTrapAdminControl)
{
    if ((UINT4) pTestValFsDhcp6RlyTrapAdminControl->i4_Length > sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    if (pTestValFsDhcp6RlyTrapAdminControl->pu1_OctetList[0] >
        (UINT1) DHCP6_RLY_TRAP_CONTROL_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlySysLogAdminStatus
 Input       :  The Indices

                The Object 
                testValFsDhcp6RlySysLogAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlySysLogAdminStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsDhcp6RlySysLogAdminStatus)
{
    if ((i4TestValFsDhcp6RlySysLogAdminStatus != DHCP6_RLY_ENABLE) &&
        (i4TestValFsDhcp6RlySysLogAdminStatus != DHCP6_RLY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

INT1
nmhTestv2FsDhcp6RlyListenPort (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsDhcp6RlyListenPort)
{
    if ((i4TestValFsDhcp6RlyListenPort < DHCP6_RLY_UDP_PORT_MIN) ||
        (i4TestValFsDhcp6RlyListenPort > DHCP6_RLY_UDP_PORT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DHCP6_RLY_INVALID_PORT);
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

INT1
nmhTestv2FsDhcp6RlyClientTransmitPort (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsDhcp6RlyClientTransmitPort)
{
    if ((i4TestValFsDhcp6RlyClientTransmitPort < 1) ||
        (i4TestValFsDhcp6RlyClientTransmitPort > DHCP6_RLY_UDP_PORT_MAX))
    {
        CLI_SET_ERR (DHCP6_RLY_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

INT1
nmhTestv2FsDhcp6RlyServerTransmitPort (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsDhcp6RlyServerTransmitPort)
{
    if ((i4TestValFsDhcp6RlyServerTransmitPort < DHCP6_RLY_UDP_PORT_MIN) ||
        (i4TestValFsDhcp6RlyServerTransmitPort > DHCP6_RLY_UDP_PORT_MAX))
    {
        CLI_SET_ERR (DHCP6_RLY_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyOption37Control
 Input       :  The Indices

                The Object
                testValFsDhcp6RlyOption37Control
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyOption37Control (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsDhcp6RlyOption37Control)
{
    if ((i4TestValFsDhcp6RlyOption37Control != DHCP6_RLY_REMOTEID_ENABLE) &&
        (i4TestValFsDhcp6RlyOption37Control != DHCP6_RLY_REMOTEID_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyPDRouteControl
 Input       :  The Indices

                The Object
                testValFsDhcp6RlyPDRouteControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyPDRouteControl (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsDhcp6RlyPDRouteControl)
{
    if ((i4TestValFsDhcp6RlyPDRouteControl != DHCP6_RLY_PDROUTE_ENABLE) &&
        (i4TestValFsDhcp6RlyPDRouteControl != DHCP6_RLY_PDROUTE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (DHCP6_RLY_PDROUTE_CONTROL_WRONG_VALUE);
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid Value \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlyDebugTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2FsDhcp6RlyDebugTrace (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT1
nmhDepv2FsDhcp6RlyTrapAdminControl (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT1
nmhDepv2FsDhcp6RlySysLogAdminStatus (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlyTrapAdminControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlyListenPort (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT1
nmhDepv2FsDhcp6RlyClientTransmitPort (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlySysLogAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlyServerTransmitPort (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlyOption37Control
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlyOption37Control (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlyPDRouteControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlyPDRouteControl (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6RlyIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDhcp6RlyIfTable
 Input       :  The Indices
                FsDhcp6RlyIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6RlyIfTable (INT4 i4FsDhcp6RlyIfIndex)
{
    if (D6RlIfGetNode (i4FsDhcp6RlyIfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDhcp6RlyIfTable
 Input       :  The Indices
                FsDhcp6RlyIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6RlyIfTable (INT4 *pi4FsDhcp6RlyIfIndex)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetFirstNode ()) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsDhcp6RlyIfIndex = (INT4) pInterface->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDhcp6RlyIfTable
 Input       :  The Indices
                FsDhcp6RlyIfIndex
                nextFsDhcp6RlyIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6RlyIfTable (INT4 i4FsDhcp6RlyIfIndex,
                                  INT4 *pi4NextFsDhcp6RlyIfIndex)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;
    if ((pInterface = D6RlIfGetNextNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "No Next Interface present\r\n");
        return SNMP_FAILURE;
    }
    *pi4NextFsDhcp6RlyIfIndex = (INT4) pInterface->u4IfIndex;
    return SNMP_SUCCESS;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfServersOnly
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfServersOnly
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfHopThreshold
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfHopThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfHopThreshold (INT4 i4FsDhcp6RlyIfIndex,
                                INT4 *pi4RetValFsDhcp6RlyIfHopThreshold)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6RlyIfHopThreshold = (INT4) pInterface->u1HopThreshold;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfInformIn
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfInformIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfInformIn (INT4 i4FsDhcp6RlyIfIndex,
                            UINT4 *pu4RetValFsDhcp6RlyIfInformIn)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6RlyIfInformIn = pInterface->u4InformIn;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRelayForwIn
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfRelayForwIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRelayForwIn (INT4 i4FsDhcp6RlyIfIndex,
                               UINT4 *pu4RetValFsDhcp6RlyIfRelayForwIn)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6RlyIfRelayForwIn = pInterface->u4RelayForwIn;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRelayReplyIn
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfRelayReplyIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRelayReplyIn (INT4 i4FsDhcp6RlyIfIndex,
                                UINT4 *pu4RetValFsDhcp6RlyIfRelayReplyIn)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6RlyIfRelayReplyIn = pInterface->u4RelayRepIn;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfReplyOut
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfReplyOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRelayReplyOut
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfRelayReplyOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRelayReplyOut (INT4 i4FsDhcp6RlyIfIndex,
                                 UINT4 *pu4RetValFsDhcp6RlyIfRelayReplyOut)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode ((UINT4) i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6RlyIfRelayReplyOut = pInterface->u4RelayReplyOut;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRelayForwOut
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfRelayForwOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRelayForwOut (INT4 i4FsDhcp6RlyIfIndex,
                                UINT4 *pu4RetValFsDhcp6RlyIfRelayForwOut)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode ((UINT4) i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsDhcp6RlyIfRelayForwOut = pInterface->u4RelayForwOut;
    return SNMP_SUCCESS;
}
 

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfInvalidPktIn
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfInvalidPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfInvalidPktIn (INT4 i4FsDhcp6RlyIfIndex,
                                UINT4 *pu4RetValFsDhcp6RlyIfInvalidPktIn)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsDhcp6RlyIfInvalidPktIn = pInterface->u4InvalidPktIn;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfCounterRest
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfCounterRest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfCounterRest (INT4 i4FsDhcp6RlyIfIndex,
                               INT4 *pi4RetValFsDhcp6RlyIfCounterRest)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsDhcp6RlyIfCounterRest = (INT4) pInterface->u1CounterReset;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRowStatus
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRowStatus (INT4 i4FsDhcp6RlyIfIndex,
                             INT4 *pi4RetValFsDhcp6RlyIfRowStatus)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsDhcp6RlyIfRowStatus = (INT4) pInterface->u1RowStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRemoteIdOption
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                retValFsDhcp6RlyIfRemoteIdOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRemoteIdOption (INT4 i4FsDhcp6RlyIfIndex,
                                  INT4 *pi4RetValFsDhcp6RlyIfRemoteIdOption)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsDhcp6RlyIfRemoteIdOption = pInterface->i4RemoteIdOption;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRemoteIdDUID
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                retValFsDhcp6RlyIfRemoteIdDUID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRemoteIdDUID (INT4 i4FsDhcp6RlyIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsDhcp6RlyIfRemoteIdDUID)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    /* If remote id is not configured over this interface, return with
     * zero length string */
    if (!(pInterface->Dhcp6RemIdOpt.i4RemIdLen))
    {
        pRetValFsDhcp6RlyIfRemoteIdDUID->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    MEMCPY (pRetValFsDhcp6RlyIfRemoteIdDUID->pu1_OctetList,
            pInterface->Dhcp6RemIdOpt.au1Duid,
            pInterface->Dhcp6RemIdOpt.i4RemIdLen);

    pRetValFsDhcp6RlyIfRemoteIdDUID->i4_Length =
        pInterface->Dhcp6RemIdOpt.i4RemIdLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRemoteIdOptionValue
 Input       :  The Indices FsDhcp6RlyIfIndex
                                                                                                        The Object
                retValFsDhcp6RlyIfRemoteIdOptionValue
 Output      :  The Get Low Lev Routine Take the Indices &                                                store the Value requested in the Return val.                               Returns     :  SNMP_SUCCESS or SNMP_FAILURE                                              ****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRemoteIdOptionValue (INT4 i4FsDhcp6RlyIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsDhcp6RlyIfRemoteIdOptionValue)
{

    INT4                i4RetRemIdOptionType = 0;
    UINT1              *pu1SwitchName = NULL;
    UINT1               au1IpAddrStr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1              *pu1IpAddrStr = NULL;
    UINT4               u4DefIpAddr = 0;
    tDhcp6RlyIfInfo    *pInterface = NULL;

    MEMSET (au1IpAddrStr, 0, DHCP6_RLY_IP6_ADDR_LEN);

    pu1IpAddrStr = &au1IpAddrStr[0];

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    i4RetRemIdOptionType = pInterface->i4RemoteIdOption;

    if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_DUID)
    {
        if (!(pInterface->Dhcp6RemIdOpt.i4RemIdLen))
        {
            pRetValFsDhcp6RlyIfRemoteIdOptionValue->i4_Length = 0;
            return SNMP_SUCCESS;
        }

        MEMCPY (pRetValFsDhcp6RlyIfRemoteIdOptionValue->pu1_OctetList,
                pInterface->Dhcp6RemIdOpt.au1Duid,
                pInterface->Dhcp6RemIdOpt.i4RemIdLen);
        pRetValFsDhcp6RlyIfRemoteIdOptionValue->i4_Length =
            pInterface->Dhcp6RemIdOpt.i4RemIdLen;
        return SNMP_SUCCESS;
    }
    else if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_SW_NAME)
    {
        pu1SwitchName = IssSysGetSwitchName ();
        STRNCPY (pRetValFsDhcp6RlyIfRemoteIdOptionValue->pu1_OctetList,
                pu1SwitchName, STRLEN(pu1SwitchName));
	pRetValFsDhcp6RlyIfRemoteIdOptionValue->pu1_OctetList[STRLEN(pu1SwitchName)] = '\0';
        pRetValFsDhcp6RlyIfRemoteIdOptionValue->i4_Length =
            STRLEN (pu1SwitchName);
        return SNMP_SUCCESS;
    }
    else if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_MGMT_IP)
    {
        u4DefIpAddr = IssSysGetDefSwitchIp ();
        DHCP6_RLY_CONVERT_IPADDR_TO_STR (pu1IpAddrStr, u4DefIpAddr);
        STRNCPY (pRetValFsDhcp6RlyIfRemoteIdOptionValue->pu1_OctetList,
                pu1IpAddrStr, STRLEN(pu1IpAddrStr));
	pRetValFsDhcp6RlyIfRemoteIdOptionValue->pu1_OctetList[STRLEN(pu1IpAddrStr)] = '\0';
        pRetValFsDhcp6RlyIfRemoteIdOptionValue->i4_Length =
            STRLEN (pu1IpAddrStr);
        return SNMP_SUCCESS;
    }
    else if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_USERDEFINED)
    {
        if (!(pInterface->Dhcp6RemIdOpt.i4RemIdLen))
        {
            pRetValFsDhcp6RlyIfRemoteIdOptionValue->i4_Length = 0;
            return SNMP_SUCCESS;
        }

        MEMCPY (pRetValFsDhcp6RlyIfRemoteIdOptionValue->pu1_OctetList,
                pInterface->Dhcp6RemIdOpt.au1UserDefined,
                STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined));
        pRetValFsDhcp6RlyIfRemoteIdOptionValue->i4_Length =
            STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRemoteIdUserDefined
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                retValFsDhcp6RlyIfRemoteIdUserDefined
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyIfRemoteIdUserDefined (INT4 i4FsDhcp6RlyIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsDhcp6RlyIfRemoteIdUserDefined)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    /* If remote id is not configured over this interface, return with
     * zero length string */
    if (!(STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined)))
    {
        pRetValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    MEMCPY (pRetValFsDhcp6RlyIfRemoteIdUserDefined->pu1_OctetList,
            pInterface->Dhcp6RemIdOpt.au1UserDefined,
            STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined));

    pRetValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length =
        STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyIfRelayState
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                retValFsDhcp6RlyIfRelayState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDhcp6RlyIfRelayState(INT4 i4FsDhcp6RlyIfIndex , 
                                  INT4 *pi4RetValFsDhcp6RlyIfRelayState)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsDhcp6RlyIfRelayState = (INT4) pInterface->u1Flag;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfServersOnly
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                setValFsDhcp6RlyIfServersOnly
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfHopThreshold
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                setValFsDhcp6RlyIfHopThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyIfHopThreshold (INT4 i4FsDhcp6RlyIfIndex,
                                INT4 i4SetValFsDhcp6RlyIfHopThreshold)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    pInterface->u1HopThreshold = (UINT1) i4SetValFsDhcp6RlyIfHopThreshold;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfCounterRest
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                setValFsDhcp6RlyIfCounterRest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyIfCounterRest (INT4 i4FsDhcp6RlyIfIndex,
                               INT4 i4SetValFsDhcp6RlyIfCounterRest)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    pInterface->u1CounterReset = (UINT1) i4SetValFsDhcp6RlyIfCounterRest;

    if (i4SetValFsDhcp6RlyIfCounterRest == OSIX_TRUE)
    {
        pInterface->u4InformIn = 0;
        pInterface->u4RelayForwIn = 0;
        pInterface->u4RelayRepIn = 0;
        pInterface->u4InvalidPktIn = 0;
        pInterface->u4RelayReplyOut = 0;
        pInterface->u4RelayForwOut = 0;
	pInterface->u4ReplyOut =0;
	pInterface->u4Solicit =0;
	pInterface->u4Request=0;
	pInterface->u4Rebind=0;
	pInterface->u4Renew=0;
	pInterface->u4Release=0;
	pInterface->u4Decline=0;
	pInterface->u4Reconfig=0;
	pInterface->u4Confirm=0;
	pInterface->u4SrvAdv=0;
	pInterface->u4RelayReply=0;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfRowStatus
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                setValFsDhcp6RlyIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyIfRowStatus (INT4 i4FsDhcp6RlyIfIndex,
                             INT4 i4SetValFsDhcp6RlyIfRowStatus)
{
    tIp6Addr            McastAddr;
    tDhcp6RlyIfInfo    *pInterface = NULL;

    switch (i4SetValFsDhcp6RlyIfRowStatus)
    {
        case CREATE_AND_WAIT:

            /* Verify if index is already present in Relay IF table */
            if ((D6RlIfGetNode ((UINT4) i4FsDhcp6RlyIfIndex)) != NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index already present\r\n");
                UNUSED_PARAM (pInterface);
                return SNMP_FAILURE;
            }

            pInterface = D6RlIfCreateNode ();
            if (pInterface == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If table row creation failed\r\n");
                return SNMP_FAILURE;
            }
            /* Initialize the default values */
            pInterface->u4IfIndex = (UINT4) i4FsDhcp6RlyIfIndex;
            pInterface->u1CounterReset = OSIX_FALSE;
            pInterface->u1HopThreshold = DHCP6_RLY_HOP_THRESH_DEF;
            pInterface->i4RemoteIdOption = DHCP6_RLY_REMOTEID_SW_NAME;
            pInterface->Dhcp6RemIdOpt.i4RemIdLen = DHCP6_RLY_REM_ID_MAX_LEN;
            pInterface->Dhcp6RemIdOpt.au1Duid[DHCP6_RLY_REM_ID_MAX_LEN - 1]
                = DHCP6_RLY_DUID_DEF;
            pInterface->u1RowStatus = NOT_READY;
            if (D6RlIfAddNode (pInterface) != OSIX_SUCCESS)
            {
                D6RlIfDelNode (pInterface);
                return SNMP_FAILURE;
            }

            /* Check if socket is created, if not then create */
            if (DHCP6_RLY_SOCKET_FD == -1)
            {
                if (D6RlSktInit () != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

            }
            INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &McastAddr);
#ifndef LNXIP6_WANTED
            NetIpv6McastJoin (i4FsDhcp6RlyIfIndex, &McastAddr);
#else
            NetIpv6McastJoinOnSocket (DHCP6_RLY_SOCKET_FD,
                                      i4FsDhcp6RlyIfIndex, &McastAddr);
#endif
            INET_ATON6 (DHCP6_ALL_SERVER, &McastAddr);
#ifndef LNXIP6_WANTED
            NetIpv6McastJoin (i4FsDhcp6RlyIfIndex, &McastAddr);
#else
            NetIpv6McastJoinOnSocket (DHCP6_RLY_SOCKET_FD, i4FsDhcp6RlyIfIndex,
                                      &McastAddr);
#endif
            pInterface->u1RowStatus = (UINT1) NOT_READY;
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            /* set the row-status of all the server same, use nmh for MSR */
            if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index not present\r\n");
                return SNMP_FAILURE;
            }

            /* Check if socket is created, if not then create */
            if ((pInterface->u1RowStatus == ACTIVE) &&
                (DHCP6_RLY_SOCKET_FD == -1))
            {
                if (D6RlSktInit () != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

            }
            pInterface->u1RowStatus = (UINT1) i4SetValFsDhcp6RlyIfRowStatus;
            break;

        case DESTROY:
            if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index not present\r\n");
                return SNMP_FAILURE;
            }
            D6RlIfDelNode (pInterface);
            /* If no interface is present in the relay then delete the socket
             */
            if (D6RlIfGetFirstNode () == NULL)
            {
                D6RlSktDeInit ();
            }
            INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &McastAddr);
#ifndef LNXIP6_WANTED
            NetIpv6McastLeave (i4FsDhcp6RlyIfIndex, &McastAddr);
#else
            NetIpv6McastLeaveOnSocket (DHCP6_RLY_SOCKET_FD,
                                       i4FsDhcp6RlyIfIndex, &McastAddr);
#endif
            INET_ATON6 (DHCP6_ALL_SERVER, &McastAddr);
#ifndef LNXIP6_WANTED
            NetIpv6McastLeave (i4FsDhcp6RlyIfIndex, &McastAddr);
#else
            NetIpv6McastLeaveOnSocket (DHCP6_RLY_SOCKET_FD,
                                       i4FsDhcp6RlyIfIndex, &McastAddr);
#endif
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfRemoteIdOption
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                setValFsDhcp6RlyIfRemoteIdOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyIfRemoteIdOption (INT4 i4FsDhcp6RlyIfIndex,
                                  INT4 i4SetValFsDhcp6RlyIfRemoteIdOption)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");

        return SNMP_FAILURE;
    }

    pInterface->i4RemoteIdOption = i4SetValFsDhcp6RlyIfRemoteIdOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfRemoteIdDUID
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                setValFsDhcp6RlyIfRemoteIdDUID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyIfRemoteIdDUID (INT4 i4FsDhcp6RlyIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsDhcp6RlyIfRemoteIdDUID)
{

    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    if ((INT4) (pInterface->Dhcp6RemIdOpt.i4RemIdLen) ==
        (pSetValFsDhcp6RlyIfRemoteIdDUID->i4_Length))
    {
        if (!MEMCMP (pInterface->Dhcp6RemIdOpt.au1Duid,
                     pSetValFsDhcp6RlyIfRemoteIdDUID->pu1_OctetList,
                     pSetValFsDhcp6RlyIfRemoteIdDUID->i4_Length))
        {
            /* The same DUID id is set again.So nothing needs to be updated. */
            return SNMP_SUCCESS;
        }
    }

    MEMSET (pInterface->Dhcp6RemIdOpt.au1Duid, 0, DHCP6_RLY_REM_ID_MAX_LEN);
    MEMCPY (pInterface->Dhcp6RemIdOpt.au1Duid,
            pSetValFsDhcp6RlyIfRemoteIdDUID->pu1_OctetList,
            pSetValFsDhcp6RlyIfRemoteIdDUID->i4_Length);

    pInterface->Dhcp6RemIdOpt.i4RemIdLen =
        pSetValFsDhcp6RlyIfRemoteIdDUID->i4_Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfRemoteIdUserDefined
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                setValFsDhcp6RlyIfRemoteIdUserDefined
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyIfRemoteIdUserDefined (INT4 i4FsDhcp6RlyIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsDhcp6RlyIfRemoteIdUserDefined)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
        return SNMP_FAILURE;
    }

    if ((INT4) (STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined)) ==
        (pSetValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length))
    {
        if (!MEMCMP (pInterface->Dhcp6RemIdOpt.au1UserDefined,
                     pSetValFsDhcp6RlyIfRemoteIdUserDefined->pu1_OctetList,
                     pSetValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length))
        {
            /* The same ascii value is set again.So nothing needs to be updated. */
            return SNMP_SUCCESS;
        }
    }

    MEMSET (pInterface->Dhcp6RemIdOpt.au1UserDefined, 0,
            DHCP6_RLY_REM_ID_MAX_LEN);
    MEMCPY (pInterface->Dhcp6RemIdOpt.au1UserDefined,
            pSetValFsDhcp6RlyIfRemoteIdUserDefined->pu1_OctetList,
            pSetValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyIfRelayState
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                setValFsDhcp6RlyIfRelayState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDhcp6RlyIfRelayState(INT4 i4FsDhcp6RlyIfIndex , INT4 i4SetValFsDhcp6RlyIfRelayState)
{
    tDhcp6RlyIfInfo    *pIfInfo = NULL;
    pIfInfo = D6RlIfGetNode (i4FsDhcp6RlyIfIndex);
    if (NULL != pIfInfo)
    {
        pIfInfo->u1Flag = pIfInfo->u1Flag | i4SetValFsDhcp6RlyIfRelayState;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfServersOnly
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                testValFsDhcp6RlyIfServersOnly
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfHopThreshold
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                testValFsDhcp6RlyIfHopThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyIfHopThreshold (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6RlyIfIndex,
                                   INT4 i4TestValFsDhcp6RlyIfHopThreshold)
{
    if (D6RlIfCheckRowCanSet (i4FsDhcp6RlyIfIndex, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDhcp6RlyIfHopThreshold > DHCP6_RLY_MAX_HOPS) ||
        (i4TestValFsDhcp6RlyIfHopThreshold < 0))
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Wrong VAlue\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfCounterRest
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                testValFsDhcp6RlyIfCounterRest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyIfCounterRest (UINT4 *pu4ErrorCode,
                                  INT4 i4FsDhcp6RlyIfIndex,
                                  INT4 i4TestValFsDhcp6RlyIfCounterRest)
{
    if (D6RlIfCheckRowCanSet (i4FsDhcp6RlyIfIndex, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDhcp6RlyIfCounterRest != OSIX_TRUE) &&
        (i4TestValFsDhcp6RlyIfCounterRest != OSIX_FALSE))
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Wrong VAlue\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfRowStatus
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                testValFsDhcp6RlyIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyIfRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsDhcp6RlyIfIndex,
                                INT4 i4TestValFsDhcp6RlyIfRowStatus)
{
    tIp6Addr            SrvAddr;
    tDhcp6RlyIfInfo    *pInterface = NULL;
    tDhcp6RlySrvAddrInfo *pDhcp6RlySrvAddrInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhValidateIndexInstanceFsDhcp6RlyIfTable (i4FsDhcp6RlyIfIndex);

    /* Error is thrown for invalid interface */ 
    if ((D6RlUtlCfaValidateIfIndex ((UINT4) (i4FsDhcp6RlyIfIndex))) != 
         OSIX_SUCCESS) 
    { 
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE; 
        CLI_SET_ERR (DHCP6_RLY_INVALID_INTERFACE); 
     
        return SNMP_FAILURE; 
    } 

    if (i1RetVal == SNMP_SUCCESS) 
    { 
        if (i4TestValFsDhcp6RlyIfRowStatus == CREATE_AND_WAIT) 
        { 
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE; 
            CLI_SET_ERR (DHCP6_RLY_INTERFACE_ALREADY_CREATED); 
            return SNMP_FAILURE; 
        } 

        if (i4TestValFsDhcp6RlyIfRowStatus == ACTIVE ||
            i4TestValFsDhcp6RlyIfRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            if (i4TestValFsDhcp6RlyIfRowStatus == DESTROY)
            {
                MEMSET (&SrvAddr, 0x00, sizeof (SrvAddr));
                pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex);
                if (pInterface != NULL)
                {
                    pDhcp6RlySrvAddrInfo =
                        D6RlSrvGetFirstNodeFromList (pInterface);
                    if (pDhcp6RlySrvAddrInfo != NULL)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (DHCP6_RLY_CANNOT_DELETE);
                        i1RetVal = SNMP_FAILURE;
                    }
                    else
                    {
                        i1RetVal = SNMP_SUCCESS;
                    }
                }
            }
        }
    }
    else
    {
        if (i4TestValFsDhcp6RlyIfRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (DHCP6_RLY_NOT_ENABLED);
        }
    }
    if (i1RetVal != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfRemoteIdOption
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                testValFsDhcp6RlyIfRemoteIdOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyIfRemoteIdOption (UINT4 *pu4ErrorCode,
                                     INT4 i4FsDhcp6RlyIfIndex,
                                     INT4 i4TestValFsDhcp6RlyIfRemoteIdOption)
{

    INT4                i4Status = 0;

    i4Status = (INT4) DHCP6_RLY_REMOTEID_STATUS;
    if (i4Status == DHCP6_RLY_REMOTEID_DISABLE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Remote-Id Option is not enabled.\r\n");

        CLI_SET_ERR (DHCP6_RLY_REMID_OPTION_DISABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (D6RlIfCheckRowCanSet (i4FsDhcp6RlyIfIndex, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDhcp6RlyIfRemoteIdOption != DHCP6_RLY_REMOTEID_DUID)
        && (i4TestValFsDhcp6RlyIfRemoteIdOption != DHCP6_RLY_REMOTEID_SW_NAME)
        && (i4TestValFsDhcp6RlyIfRemoteIdOption != DHCP6_RLY_REMOTEID_MGMT_IP)
        && (i4TestValFsDhcp6RlyIfRemoteIdOption !=
            DHCP6_RLY_REMOTEID_USERDEFINED))
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "Input Value is wrong !!\r\n");
        CLI_SET_ERR (DHCP6_RLY_WRONG_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfRemoteIdDUID
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object
                testValFsDhcp6RlyIfRemoteIdDUID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyIfRemoteIdDUID (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6RlyIfIndex,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTestValFsDhcp6RlyIfRemoteIdDUID)
{

    INT4                i4Status = 0;
    INT4                i4RemoteIdType = 0;

    i4Status = (INT4) DHCP6_RLY_REMOTEID_STATUS;
    if (i4Status == DHCP6_RLY_REMOTEID_DISABLE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Remote-Id Option is not enabled.\r\n");

        CLI_SET_ERR (DHCP6_RLY_REMID_OPTION_DISABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    nmhGetFsDhcp6RlyIfRemoteIdOption (i4FsDhcp6RlyIfIndex, &i4RemoteIdType);

    if (i4RemoteIdType != DHCP6_RLY_REMOTEID_DUID)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Remote-Id Option is invalid type.\r\n");

        CLI_SET_ERR (DHCP6_RLY_INCONSISTENT_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (D6RlIfCheckRowCanSet (i4FsDhcp6RlyIfIndex, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6RlyIfRemoteIdDUID->i4_Length > DHCP6_RLY_REM_ID_MAX_LEN)
    {
        CLI_SET_ERR (DHCP6_RLY_WRONG_LENGTH);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Unique DUID test */
    if (D6RCheckIfDuidIsValid (pTestValFsDhcp6RlyIfRemoteIdDUID,
                               pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                testValFsDhcp6RlyIfRemoteIdUserDefined
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDhcp6RlyIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsDhcp6RlyIfRemoteIdUserDefined)
{
    INT4                i4Status = 0;
    INT4                i4RemoteIdType = 0;

    i4Status = (INT4) DHCP6_RLY_REMOTEID_STATUS;
    if (i4Status == DHCP6_RLY_REMOTEID_DISABLE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Remote-Id Option is not enabled.\r\n");

        CLI_SET_ERR (DHCP6_RLY_REMID_OPTION_DISABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    nmhGetFsDhcp6RlyIfRemoteIdOption (i4FsDhcp6RlyIfIndex, &i4RemoteIdType);

    if (i4RemoteIdType != DHCP6_RLY_REMOTEID_USERDEFINED)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Remote-Id Option is invalid type.\r\n");

        CLI_SET_ERR (DHCP6_RLY_INCONSISTENT_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (D6RlIfCheckRowCanSet (i4FsDhcp6RlyIfIndex, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pTestValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length >
        DHCP6_RLY_REM_ID_MAX_LEN)
    {
        CLI_SET_ERR (DHCP6_RLY_WRONG_LENGTH);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Unique ascii value test */
    if (D6RCheckIfUserDefinedAsciiIsValid
        (pTestValFsDhcp6RlyIfRemoteIdUserDefined, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyIfRelayState
 Input       :  The Indices
                FsDhcp6RlyIfIndex

                The Object 
                testValFsDhcp6RlyIfRelayState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDhcp6RlyIfRelayState(UINT4 *pu4ErrorCode , INT4 i4FsDhcp6RlyIfIndex , INT4 i4TestValFsDhcp6RlyIfRelayState)
{
     UNUSED_PARAM(i4FsDhcp6RlyIfIndex);
     if((i4TestValFsDhcp6RlyIfRelayState == DHCP6_RLY_DYNAMIC) || 
        (i4TestValFsDhcp6RlyIfRelayState == DHCP6_RLY_STATIC) ||
        (i4TestValFsDhcp6RlyIfRelayState == (DHCP6_RLY_STATIC|DHCP6_RLY_DYNAMIC)))
     {
        return SNMP_SUCCESS;
     }
     *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
     return SNMP_FAILURE;
}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlyIfTable
 Input       :  The Indices
                FsDhcp6RlyIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlyIfTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6RlySrvAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDhcp6RlySrvAddressTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6RlySrvAddressTable (INT4
                                                   i4FsDhcp6RlyInIfIndex,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsDhcp6RlySrvAddress)
{
    tIp6Addr            Temp6Addr;

    MEMSET (&Temp6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Temp6Addr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsDhcp6RlySrvAddress->i4_Length,
                           sizeof (tIp6Addr)));

    if (D6RlSrvGetNode (i4FsDhcp6RlyInIfIndex, &Temp6Addr) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Invalid If Index given to validate\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDhcp6RlySrvAddressTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6RlySrvAddressTable (INT4 *pi4FsDhcp6RlyInIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsDhcp6RlySrvAddress)
{
    tDhcp6RlySrvAddrInfo *pServerInfo = NULL;
    if ((pServerInfo = D6RlSrvGetFirstNode ()) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "No Entries present\r\n");
        return SNMP_FAILURE;
    }
    *pi4FsDhcp6RlyInIfIndex = (INT4) pServerInfo->pIf->u4IfIndex;
    MEMCPY ((tIp6Addr *) (VOID *) pFsDhcp6RlySrvAddress->pu1_OctetList,
            &pServerInfo->Addr, sizeof (tIp6Addr));
    pFsDhcp6RlySrvAddress->i4_Length = sizeof (tIp6Addr);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDhcp6RlySrvAddressTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                nextFsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                nextFsDhcp6RlySrvAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6RlySrvAddressTable (INT4 i4FsDhcp6RlyInIfIndex,
                                          INT4 *pi4NextFsDhcp6RlyInIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsDhcp6RlySrvAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsDhcp6RlySrvAddress)
{
    tIp6Addr            Temp6Addr;
    tDhcp6RlySrvAddrInfo *pServerNode = NULL;

    MEMSET (&Temp6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Temp6Addr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);

    if ((pServerNode = D6RlSrvGetNextNode
         (i4FsDhcp6RlyInIfIndex, &Temp6Addr)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "No next node available\r\n");
        return SNMP_FAILURE;
    }

    *pi4NextFsDhcp6RlyInIfIndex = (INT4) pServerNode->pIf->u4IfIndex;
    MEMCPY ((tIp6Addr *) (VOID *) pNextFsDhcp6RlySrvAddress->pu1_OctetList,
            &pServerNode->Addr, sizeof (tIp6Addr));
    pNextFsDhcp6RlySrvAddress->i4_Length = sizeof (tIp6Addr);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlySrvAddressRowStatus
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress

                The Object 
                retValFsDhcp6RlySrvAddressRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlySrvAddressRowStatus (INT4 i4FsDhcp6RlyInIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsDhcp6RlySrvAddress,
                                     INT4
                                     *pi4RetValFsDhcp6RlySrvAddressRowStatus)
{
    tIp6Addr            Temp6Addr;
    tDhcp6RlySrvAddrInfo *pServerNode = NULL;

    MEMSET (&Temp6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Temp6Addr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);

    if ((pServerNode =
         D6RlSrvGetNode (i4FsDhcp6RlyInIfIndex, &Temp6Addr)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "No next node available\r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsDhcp6RlySrvAddressRowStatus = (INT4) pServerNode->u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlySrvAddressRowStatus
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress

                The Object 
                setValFsDhcp6RlySrvAddressRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlySrvAddressRowStatus (INT4 i4FsDhcp6RlyInIfIndex,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pFsDhcp6RlySrvAddress,
                                     INT4 i4SetValFsDhcp6RlySrvAddressRowStatus)
{
    tDhcp6RlySrvAddrInfo *pServerEntry = NULL;
    tDhcp6RlyIfInfo    *pInterface = NULL;
    tIp6Addr            ServAddr;

    MEMSET (&ServAddr, 0, sizeof (tIp6Addr));

    MEMCPY (&ServAddr,
            (tIp6Addr *) (VOID *) pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);
    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyInIfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValFsDhcp6RlySrvAddressRowStatus)
    {
        case CREATE_AND_WAIT:
            pServerEntry = D6RlSrvCreateNode ();
            if (pServerEntry == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "Server table row creation failed\r\n");
                CLI_SET_ERR (DHCP6_RLY_MAX_SERVER); 
                return SNMP_FAILURE;
            }
            /* Initialize the default values */
            pServerEntry->pIf = pInterface;
            MEMCPY (&pServerEntry->Addr,
                    (tIp6Addr *) (VOID *) pFsDhcp6RlySrvAddress->pu1_OctetList,
                    pFsDhcp6RlySrvAddress->i4_Length);

            if (D6RlSrvAddNode (pServerEntry) != OSIX_SUCCESS)
            {
                D6RlSrvDelNode (pServerEntry);
                return SNMP_FAILURE;
            }

            pServerEntry->u1RowStatus = (UINT1) NOT_READY;
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            /* set the row-status of all the out-if same, use nmh for MSR */

            if ((pServerEntry = D6RlSrvGetNode
                 (i4FsDhcp6RlyInIfIndex, &ServAddr)) == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index not present\r\n");
                return SNMP_FAILURE;
            }

            pServerEntry->u1RowStatus = (UINT1)
                i4SetValFsDhcp6RlySrvAddressRowStatus;
            break;

        case DESTROY:
            if ((pServerEntry = D6RlSrvGetNode (i4FsDhcp6RlyInIfIndex,
                                                &ServAddr)) == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index not present\r\n");
                return SNMP_FAILURE;
            }
            D6RlSrvDelNode (pServerEntry);
            break;

        default:
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlySrvAddressRowStatus
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress

                The Object 
                testValFsDhcp6RlySrvAddressRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlySrvAddressRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDhcp6RlyInIfIndex,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pFsDhcp6RlySrvAddress,
                                        INT4
                                        i4TestValFsDhcp6RlySrvAddressRowStatus)
{
    INT4                i1RetVal = SNMP_FAILURE;
    tIp6Addr            SrvAddr;
    tIp6Addr            TempSrvAddr;
    UINT4               u4Index = 0;

    /*Check for selfip */
    MEMSET (&TempSrvAddr, 0, sizeof (tIp6Addr));
    MEMCPY ((UINT1 *) &TempSrvAddr,
            pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);
    if (!IS_ADDR_MULTI (TempSrvAddr))
    {
        if (NetIpv6IsOurAddress (&TempSrvAddr, &u4Index) == NETIPV6_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (DHCP6_RLY_INVALID_SERVER);
            return SNMP_FAILURE;
        }
    }
    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6RlySrvAddressTable
        (i4FsDhcp6RlyInIfIndex, pFsDhcp6RlySrvAddress);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6RlySrvAddressRowStatus == ACTIVE ||
            i4TestValFsDhcp6RlySrvAddressRowStatus == DESTROY)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            if (i4TestValFsDhcp6RlySrvAddressRowStatus == DESTROY)
            {
                MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
                MEMCPY ((UINT1 *) &SrvAddr,
                        pFsDhcp6RlySrvAddress->pu1_OctetList,
                        pFsDhcp6RlySrvAddress->i4_Length);
                if (D6RlSrvGetNode (i4FsDhcp6RlyInIfIndex, &SrvAddr) != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetVal = SNMP_FAILURE;
                }
                else
                {
                    i1RetVal = SNMP_SUCCESS;
                }
            }
        }

    }
    else
    {
        if (i4TestValFsDhcp6RlySrvAddressRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlySrvAddressTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlySrvAddressTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDhcp6RlyOutIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDhcp6RlyOutIfTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDhcp6RlyOutIfTable (INT4 i4FsDhcp6RlyInIfIndex,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pFsDhcp6RlySrvAddress,
                                              INT4 i4FsDhcp6RlyOutIfIndex)
{
    tIp6Addr            ServAddr;

    MEMSET (&ServAddr, 0, sizeof (tIp6Addr));
    MEMCPY (&ServAddr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsDhcp6RlySrvAddress->i4_Length,
                           sizeof (tIp6Addr)));

    if ((D6RlOuIfGetNode (i4FsDhcp6RlyInIfIndex,
                          &ServAddr, i4FsDhcp6RlyOutIfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDhcp6RlyOutIfTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDhcp6RlyOutIfTable (INT4 *pi4FsDhcp6RlyInIfIndex,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pFsDhcp6RlySrvAddress,
                                      INT4 *pi4FsDhcp6RlyOutIfIndex)
{
    tDhcp6RlyOutIfInfo *pOutIfEntry = NULL;

    if ((pOutIfEntry = D6RlOuIfGetFirstNode ()) == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pFsDhcp6RlySrvAddress->pu1_OctetList,
            (UINT1 *) &pOutIfEntry->pSrvAddr->Addr, sizeof (tIp6Addr));
    pFsDhcp6RlySrvAddress->i4_Length = sizeof (tIp6Addr);
    *pi4FsDhcp6RlyOutIfIndex = pOutIfEntry->u4OutIfIndex;
    *pi4FsDhcp6RlyInIfIndex = pOutIfEntry->pSrvAddr->pIf->u4IfIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDhcp6RlyOutIfTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                nextFsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                nextFsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex
                nextFsDhcp6RlyOutIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDhcp6RlyOutIfTable (INT4 i4FsDhcp6RlyInIfIndex,
                                     INT4 *pi4NextFsDhcp6RlyInIfIndex,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pFsDhcp6RlySrvAddress,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pNextFsDhcp6RlySrvAddress,
                                     INT4 i4FsDhcp6RlyOutIfIndex,
                                     INT4 *pi4NextFsDhcp6RlyOutIfIndex)
{
    tIp6Addr            SrvAddr;
    tDhcp6RlyOutIfInfo *pOutIfEntry = NULL;

    MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
    MEMCPY ((UINT1 *) &SrvAddr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);

    if ((pOutIfEntry = D6RlOuIfGetNextNode (i4FsDhcp6RlyInIfIndex,
                                            &SrvAddr, i4FsDhcp6RlyOutIfIndex))
        == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "No Next entry\r\n");

        return SNMP_FAILURE;
    }

    MEMCPY (pNextFsDhcp6RlySrvAddress->pu1_OctetList, (UINT1 *)
            &pOutIfEntry->pSrvAddr->Addr, sizeof (tIp6Addr));
    pNextFsDhcp6RlySrvAddress->i4_Length = sizeof (tIp6Addr);

    *pi4NextFsDhcp6RlyOutIfIndex = pOutIfEntry->u4OutIfIndex;
    *pi4NextFsDhcp6RlyInIfIndex = pOutIfEntry->pSrvAddr->pIf->u4IfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDhcp6RlyOutIfRowStatus
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex

                The Object 
                retValFsDhcp6RlyOutIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDhcp6RlyOutIfRowStatus (INT4 i4FsDhcp6RlyInIfIndex,
                                tSNMP_OCTET_STRING_TYPE
                                * pFsDhcp6RlySrvAddress,
                                INT4 i4FsDhcp6RlyOutIfIndex,
                                INT4 *pi4RetValFsDhcp6RlyOutIfRowStatus)
{
    tIp6Addr            SrvAddr;
    tDhcp6RlyOutIfInfo *pOutIfEntry = NULL;

    MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
    MEMCPY ((UINT1 *) &SrvAddr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);

    if ((pOutIfEntry = D6RlOuIfGetNode (i4FsDhcp6RlyInIfIndex,
                                        &SrvAddr,
                                        i4FsDhcp6RlyOutIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "No entry\r\n");

        return SNMP_FAILURE;
    }

    *pi4RetValFsDhcp6RlyOutIfRowStatus = (INT4) pOutIfEntry->u1RowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDhcp6RlyOutIfRowStatus
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex

                The Object 
                setValFsDhcp6RlyOutIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDhcp6RlyOutIfRowStatus (INT4 i4FsDhcp6RlyInIfIndex,
                                tSNMP_OCTET_STRING_TYPE
                                * pFsDhcp6RlySrvAddress,
                                INT4 i4FsDhcp6RlyOutIfIndex,
                                INT4 i4SetValFsDhcp6RlyOutIfRowStatus)
{
    tDhcp6RlyOutIfInfo *pOutIfEntry = NULL;
    tDhcp6RlySrvAddrInfo *pSrvAddr;
    tIp6Addr            ServAddr;

    MEMSET (&ServAddr, 0, sizeof (tIp6Addr));

    MEMCPY (&ServAddr,
            (tIp6Addr *) (VOID *) pFsDhcp6RlySrvAddress->pu1_OctetList,
            pFsDhcp6RlySrvAddress->i4_Length);
    pSrvAddr = D6RlSrvGetNode (i4FsDhcp6RlyInIfIndex, &ServAddr);
    if (pSrvAddr == NULL)
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValFsDhcp6RlyOutIfRowStatus)
    {
        case CREATE_AND_WAIT:
            pOutIfEntry = D6RlOuIfCreateNode ();
            if (pOutIfEntry == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "OutIf table row creation failed\r\n");
                CLI_SET_ERR(DHCP6_RLY_INTERFACE_CREATION_FAILED);
                return SNMP_FAILURE;
            }
            /* Initialize the default values */
            pOutIfEntry->u4OutIfIndex = (UINT4) i4FsDhcp6RlyOutIfIndex;
            pOutIfEntry->pSrvAddr = pSrvAddr;
            if (D6RlOuIfAddNode (pOutIfEntry) != OSIX_SUCCESS)
            {
                D6RlOuIfDelNode (pOutIfEntry);
                return SNMP_FAILURE;
            }

            pOutIfEntry->u1RowStatus = (UINT1) NOT_READY;
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:

            if ((pOutIfEntry = D6RlOuIfGetNode (i4FsDhcp6RlyInIfIndex,
                                                &ServAddr,
                                                i4FsDhcp6RlyOutIfIndex))
                == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index not present\r\n");
                return SNMP_FAILURE;
            }

            pOutIfEntry->u1RowStatus = (UINT1) i4SetValFsDhcp6RlyOutIfRowStatus;
            break;

        case DESTROY:
            if ((pOutIfEntry =
                 D6RlOuIfGetNode (i4FsDhcp6RlyInIfIndex,
                                  &ServAddr, i4FsDhcp6RlyOutIfIndex)) == NULL)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                               DHCP6_RLY_FAILURE_TRC,
                               "If Index not present\r\n");
                return SNMP_FAILURE;
            }
            D6RlOuIfDelNode (pOutIfEntry);
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDhcp6RlyOutIfRowStatus
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex

                The Object 
                testValFsDhcp6RlyOutIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDhcp6RlyOutIfRowStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDhcp6RlyInIfIndex,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pFsDhcp6RlySrvAddress,
                                   INT4 i4FsDhcp6RlyOutIfIndex,
                                   INT4 i4TestValFsDhcp6RlyOutIfRowStatus)
{

    INT1                i1RetVal = SNMP_FAILURE;
    tIp6Addr            SrvAddr;

    MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
    MEMCPY ((UINT1 *) &SrvAddr, pFsDhcp6RlySrvAddress->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsDhcp6RlySrvAddress->i4_Length,
                           sizeof (tIp6Addr)));

    i1RetVal =
        nmhValidateIndexInstanceFsDhcp6RlyOutIfTable (i4FsDhcp6RlyInIfIndex,
                                                      pFsDhcp6RlySrvAddress,
                                                      i4FsDhcp6RlyOutIfIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4TestValFsDhcp6RlyOutIfRowStatus == ACTIVE ||
            i4TestValFsDhcp6RlyOutIfRowStatus == NOT_IN_SERVICE)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            if (i4TestValFsDhcp6RlyOutIfRowStatus == DESTROY)
            {
                MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
                MEMCPY ((UINT1 *) &SrvAddr,
                        pFsDhcp6RlySrvAddress->pu1_OctetList,
                        pFsDhcp6RlySrvAddress->i4_Length);

                if (D6RlOuIfGetNode ((UINT4) i4FsDhcp6RlyInIfIndex, &SrvAddr,
                                     (UINT4) i4FsDhcp6RlyOutIfIndex) != NULL)
                {
                    i1RetVal = SNMP_SUCCESS;
                }
            }
        }
    }
    else
    {
        if (i4TestValFsDhcp6RlyOutIfRowStatus == CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDhcp6RlyOutIfTable
 Input       :  The Indices
                FsDhcp6RlyInIfIndex
                FsDhcp6RlySrvAddress
                FsDhcp6RlyOutIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDhcp6RlyOutIfTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
