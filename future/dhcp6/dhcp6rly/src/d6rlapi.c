/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlapi.c,v 1.6 2011/04/21 08:38:05 siva Exp $
 *
 * DESC : Contains the Api routines called by external modules
 ***************************************************************************/

#include "d6rlinc.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlApiIp6IfStatusNotify                                    */
/*                                                                           */
/* Description  : Whenever IPv6 senses changes in Iface configurations, it   */
/*                calls this function.                                       */
/*                                                                           */
/* Input        : pIp6HliParams : Pointer to tIp6HliParams                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6RlApiIp6IfStatusNotify (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tNetIpv6IfStatChange *pIfStatusChange = NULL;
    tDhcp6RlyQMsg      *pDhcp6QMsg = NULL;

    pIfStatusChange = &pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange;

    if (DHCP6_RLY_IS_INITIALIZED == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    /* Need to handle only interface delete event in Relay */
    if (pIfStatusChange->u4IfStat == NETIPV6_IF_DELETE)
    {
        pDhcp6QMsg = (tDhcp6RlyQMsg *) MemAllocMemBlk (DHCP6_RLY_Q_MSG_POOL_ID);
        if (pDhcp6QMsg == NULL)
        {
            return OSIX_FALSE;
        }
        pDhcp6QMsg->i4IfIndex = pIfStatusChange->u4Index;
        pDhcp6QMsg->i4Command = DHCP6_RLY_INTF_DELETE;

        /* Enqueue the buffer to the DHCP6_RLY Task */
        if (OsixSendToQ ((UINT4) 0, DHCP6_RLY_Q_NAME,
                         (tOsixMsg *) (VOID *) pDhcp6QMsg,
                         OSIX_MSG_URGENT) != OSIX_SUCCESS)
        {

            MemReleaseMemBlock (DHCP6_RLY_Q_MSG_POOL_ID, (UINT1 *) pDhcp6QMsg);
            DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC |
                           DHCP6_RLY_FAILURE_TRC,
                           "Unable to allocate memory for Q message\r\n");
            return OSIX_FALSE;
        }

        OsixEvtSend (DHCP6_RLY_TASK_ID, DHCP6_RLY_QMSG_EVENT);

    }
    return OSIX_TRUE;
}

#ifdef CLI_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlApiShowGlobalInfo                                   */
/*                                                                           */
/* Description  : This function will Display the Relay Global Information   */
/*                                                                           */
/* Input        : CliHandle - CLI Handle.                                    */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6RlApiShowGlobalInfo (tCliHandle CliHandle)
{

    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);
    DHCP6_RLY_LOCK ();

    /*Show Global Information of Relay */
    D6RlCliShowGlobalInfo (CliHandle);
    D6RlMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlApiShowInterfaceInfo                                   */
/*                                                                           */
/* Description  : This function will Display the Relay Interface Information   */
/*                                                                           */
/* Input        : CliHandle - CLI Handle.                                    */
/*                i4Index - Interface Index                                  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6RlApiShowInterfaceInfo (tCliHandle CliHandle, INT4 i4Index)
{
    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);
    DHCP6_RLY_LOCK ();

    /*Show Global Information of Relay */
    D6RlCliShowInterface (CliHandle, i4Index);
    D6RlMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}
#endif
