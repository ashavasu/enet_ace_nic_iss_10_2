/*******************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: d6rlutl.c,v 1.9 2014/10/29 12:50:15 siva Exp $
*
* Description: This file contains the DHCP6 API related funtions
*********************************************************************/

#include "d6rlinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlUtlGetLinkAddr                                         */
/*                                                                           */
/* Description  : This function gets the Server Identifier or Ip-Address of  */
/*                the interface on which the DHCP6_RLY Message has been received */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                au1IpV6Addr - pointer to IP Address.                       */
/*                                                                           */
/* Output       : au1IpV6Addr                                                */
/*                                                                           */
/* Returns      : DHCP6_RLYR_SUCCESS or DHCP6R_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6RlUtlGetLinkAddr (UINT4 u4IfIndex, UINT1 *pu1IpV6Addr)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;

    if (NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMCPY (pu1IpV6Addr, (UINT1 *) &(NetIpv6IfInfo.Ip6Addr.u1_addr),
            DHCP6_RLY_IP6_ADDR_LEN);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlUtlGetIfName                                            */
/*                                                                           */
/* Description  : This function gets the Interface name of the gives         */
/*                interface                                                  */
/*                                                                           */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pu1fName - pointer to IP Address.                       */
/*                                                                           */
/* Output       : au1IpV6Addr                                                */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6RlUtlGetIfName (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    if (CfaCliGetIfName (u4IfIndex, (INT1 *) pu1IfName) == NETIPV6_FAILURE)

    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlUtlIp6AddrCompare                                      */
/*                                                                           */
/* Description  : This function compares the given two IPv6 Addresses        */
/*                                                                           */
/* Input        : pAddr1   - pointer to IPv6 Address 1                       */
/*                pAddr2   - pointer to IPv6 Address 2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : DHCP6_RLY_EQUAL/DHCP6_RLY_LESS/DHCP6_RLY_GREATER           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6RlUtlIp6AddrCompare (tIp6Addr * pAddr1, tIp6Addr * pAddr2)
{

    INT4                i4Val = 0;

    i4Val = MEMCMP (pAddr1, pAddr2, sizeof (tIp6Addr));

    if (i4Val < 0)
    {
        return DHCP6_RLY_LESS;
    }
    else if (i4Val > 0)
    {
        return DHCP6_RLY_GREATER;
    }

    return DHCP6_RLY_EQUAL;
}
PUBLIC VOID
D6RlUtlDeleteIfFromAll (UINT4 u4Index)
{
    tDhcp6RlyIfInfo    *pIfEntry = NULL;
    tDhcp6RlySrvAddrInfo *pSrvEntry = NULL;
    tDhcp6RlySrvAddrInfo *pSavedSrvEntry = NULL;
    tDhcp6RlyOutIfInfo *pOutIfEntry = NULL;
    tDhcp6RlyOutIfInfo *pSavedOutIfEntry = NULL;

    if ((pIfEntry = D6RlIfGetNode (u4Index)) == NULL)
    {
        return;
    }
    pSrvEntry = D6RlIfServerListGetFirstNode (pIfEntry);
    while (pSrvEntry != NULL)
    {
        pSavedSrvEntry = D6RlIfServerListGetNextNode (pIfEntry, pSrvEntry);
        pOutIfEntry = D6RlSrvOutIfListGetFirstNode (pSrvEntry);
        while (pOutIfEntry != NULL)
        {
            pSavedOutIfEntry =
                D6RlSrvOutIfListGetNextNode (pSrvEntry, pOutIfEntry);
            D6RlOuIfDelNode (pOutIfEntry);
            pOutIfEntry = pSavedOutIfEntry;
        }
        D6RlSrvDelNode (pSrvEntry);
        pSrvEntry = pSavedSrvEntry;
    }
    D6RlIfDelNode (pIfEntry);
    return;
}
PUBLIC INT4
D6RlUtlGetGlobalAddr (UINT4 u4IfIndex, tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tNetIpv6AddrInfo    netIp6FirstAddrInfo;
    tNetIpv6AddrInfo    netIp6NextAddrInfo;
    tIp6Addr           *pAddr = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    INT4                i4Status, i4Count;
    i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);
    if (i4Status != NETIPV6_FAILURE)
    {
        do
        {
            if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                              netIp6FirstAddrInfo.u4PrefixLength) == TRUE)
            {
                pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
            else
            {
                pAddr = &netIp6FirstAddrInfo.Ip6Addr;
            }
            i4Status = NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
                                             &netIp6NextAddrInfo);
            if (i4Status != NETIPV6_FAILURE)
                MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                        sizeof (tNetIpv6AddrInfo));
        }
        while (i4Status != NETIPV6_FAILURE);
        MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
        return OSIX_SUCCESS;
    }
    else
    {
        for (i4Count = 0; i4Count < IP6_MAX_LOGICAL_IFACES; i4Count++)
        {
            i4Status = NetIpv6GetFirstIfAddr (i4Count, &netIp6FirstAddrInfo);
            if (i4Status != NETIPV6_FAILURE)
            {
                do
                {
                    if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                                      netIp6FirstAddrInfo.u4PrefixLength) ==
                        TRUE)
                    {
                        pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                        MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                        return OSIX_SUCCESS;
                    }
                    else
                    {
                        pAddr = &netIp6FirstAddrInfo.Ip6Addr;
                    }
                    i4Status = NetIpv6GetNextIfAddr (u4IfIndex,
                                                     &netIp6FirstAddrInfo,
                                                     &netIp6NextAddrInfo);
                    if (i4Status != NETIPV6_FAILURE)
                    {
                        MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                                sizeof (tNetIpv6AddrInfo));
                    }
                }
                while (i4Status != NETIPV6_FAILURE);
                MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlUtlGetRoute                                            */
/*                                                                           */
/* Description  : This function gets the local interface to be used for      */
/*                sending DHCP6 messages to 'au1IpV6Addr' address.           */
/*                                                                           */
/* Input        : au1IpV6Addr - destination Ip-Address.                      */
/*                pu4IfIndex  - pointer to interface number.                 */
/*                                                                           */
/* Output       : pu4IfIndex                                                 */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6RlUtlGetRoute (UINT1 *au1IpV6Addr, UINT4 *pu4IfIndex)
{
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtIPv6Query;

    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtIPv6Query, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    MEMCPY (RtIPv6Query.Ip6Dst.u1_addr, au1IpV6Addr, DHCP6_RLY_IP6_ADDR_LEN);

    RtIPv6Query.u1Prefixlen = (UINT1) DHCP6_RLY_IP6_ADDR_LEN;
    if (NetIpv6GetRoute (&RtIPv6Query, &NetIp6RtInfo) == NETIPV6_FAILURE)
    {
        *pu4IfIndex = NetIp6RtInfo.u4Index;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlUtlCliConfGetIfName                                    */
/*                                                                           */
/* Description  : This function gets the Interface name of the gives         */
/*                interface                                                  */
/*                                                                           */
/*                                                                           */
/* Input        : u4IfIndex   - Interface index of the received message      */
/*                pu1fName - pointer to IP Address.                       */
/*                                                                           */
/* Output       : Interface Name                                             */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6RlUtlCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return OSIX_SUCCESS;
#endif
}
/*****************************************************************************/ 
/*                                                                           */ 
/* Function     : D6RlUtlCfaValidateIfIndex                                  */ 
/*                                                                           */ 
/* Description  : This function  Checks whether the interface exists         */ 
/*                with this Interface index.                                 */ 
/*                                                                           */ 
/* Input        : u4IfIndex   - Interface index                              */ 
/*                                                                           */ 
/* Output       : None                                                       */ 
/*                                                                           */ 
/* Returns      : CFA_SUCCESS if the ifIndex is valid,otherwise CFA_FAILURE. */ 
/*                                                                           */ 
/*****************************************************************************/ 
 
PUBLIC INT4 
D6RlUtlCfaValidateIfIndex (UINT4 u4IfIndex) 
{ 
    INT4                i4RetVal = OSIX_SUCCESS; 
 
    if (CfaValidateIfIndex (u4IfIndex) != CFA_SUCCESS) 
    { 
        i4RetVal = OSIX_FAILURE; 
    } 
 
    return (i4RetVal); 
} 

