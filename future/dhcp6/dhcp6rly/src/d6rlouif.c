/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *                Relay to access OutIf table.
 * ************************************************************************/
#include "d6rlinc.h"
/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfCreateTable
 *
 * DESCRIPTION      : This function creats the interface table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlOuIfCreateTable (VOID)
{
    DHCP6_RLY_OUTIF_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6RlySrvAddrInfo, RbNode),
                              D6RlOuIfRBCmp);
    if (DHCP6_RLY_OUTIF_TABLE == NULL)
    {
        DHCP6_RLY_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6RlOuIfCreateTable: " " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfDeleteTable
 *
 * DESCRIPTION      : This function Deletes the interface table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlOuIfDeleteTable (VOID)
{
    if (DHCP6_RLY_OUTIF_TABLE != NULL)
    {
        D6RlOuIfDrainTable ();
        RBTreeDestroy (DHCP6_RLY_OUTIF_TABLE, D6RlOuIfRBFree, 0);

    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlOuIfRBCmp                                              */
/*                                                                           */
/* Description  : This function compare the Out If tbl index to RB tree      */
/*                operations in interface table                              */
/*                                                                           */
/* Input        : e1        Pointer to Out If node1                          */
/*                e2        Pointer to Out If node2                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. 0 if keys of both the elements are same.   */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
D6RlOuIfRBCmp (tRBElem * e1, tRBElem * e2)
{
    tDhcp6RlyOutIfInfo *pOutIfEntry1 = (tDhcp6RlyOutIfInfo *) e1;
    tDhcp6RlyOutIfInfo *pOutIfEntry2 = (tDhcp6RlyOutIfInfo *) e2;
    INT4                i4RetVal = 0;
    if (pOutIfEntry1->pSrvAddr->pIf->u4IfIndex >
        pOutIfEntry2->pSrvAddr->pIf->u4IfIndex)
    {
        return DHCP6_RLY_GREATER;
    }
    else if (pOutIfEntry1->pSrvAddr->pIf->u4IfIndex <
             pOutIfEntry2->pSrvAddr->pIf->u4IfIndex)
    {
        return DHCP6_RLY_LESS;
    }

    i4RetVal = D6RlUtlIp6AddrCompare (&pOutIfEntry1->pSrvAddr->Addr,
                                      &pOutIfEntry2->pSrvAddr->Addr);

    if (i4RetVal == DHCP6_RLY_EQUAL)
    {
        if (pOutIfEntry1->u4OutIfIndex > pOutIfEntry2->u4OutIfIndex)
        {
            return DHCP6_RLY_GREATER;
        }
        else if (pOutIfEntry1->u4OutIfIndex == pOutIfEntry2->u4OutIfIndex)
        {
            return DHCP6_RLY_EQUAL;
        }
        else
        {
            return DHCP6_RLY_LESS;
        }
    }

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfCreateNode 
 *
 * DESCRIPTION      : This function creates a Out if table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyOutIfInfo * - Pointer to the created outif node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyOutIfInfo *
D6RlOuIfCreateNode (VOID)
{
    tDhcp6RlyOutIfInfo *pOutIf = NULL;

    pOutIf = (tDhcp6RlyOutIfInfo *) MemAllocMemBlk
        (DHCP6_RLY_OUTIF_TBL_POOL_ID);
    if (pOutIf == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to allocate memory for  Out I/f table\r\n");

        return NULL;
    }
    MEMSET (pOutIf, 0x00, sizeof (tDhcp6RlyOutIfInfo));
    return pOutIf;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfAddNode
 *
 * DESCRIPTION      : This function add a node to the Out I/f  table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pOutIf - pointer to Out If information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlOuIfAddNode (tDhcp6RlyOutIfInfo * pOutIf)
{
    if (RBTreeAdd (DHCP6_RLY_OUTIF_TABLE, pOutIf) != RB_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to add node to  Out I/f table\r\n");
        return OSIX_FAILURE;
    }
    D6RlSrvOutIfListAddNode (pOutIf);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfDelNode
 *
 * DESCRIPTION      : This function delete a  Out I/f node from the
 *                     Out I/f table and release the memory used by that
 *                    node to the memory pool. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pOutIf - pointer to  Out I/f node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlOuIfDelNode (tDhcp6RlyOutIfInfo * pOutIf)
{
    D6RlSrvOutIfListDelNode (pOutIf);
    RBTreeRem (DHCP6_RLY_OUTIF_TABLE, pOutIf);
    MemReleaseMemBlock (DHCP6_RLY_OUTIF_TBL_POOL_ID, (UINT1 *) pOutIf);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a OutIf node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the  Out I/f node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlOuIfRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    D6RlSrvOutIfListDelNode ((tDhcp6RlyOutIfInfo *) pRBElem);
    MemReleaseMemBlock (DHCP6_RLY_OUTIF_TBL_POOL_ID, (UINT1 *) pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfDrainTable
 *
 * DESCRIPTION      : This function deletes all the  Out I/f nodes
 *                    associated with the  Out I/f table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlOuIfDrainTable (VOID)
{
    RBTreeDrain (DHCP6_RLY_OUTIF_TABLE, D6RlOuIfRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfGetFirstNode
 *
 * DESCRIPTION      : This function returns the first  Out I/f node
 *                    from the  Out I/f table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyOutIfInfo * -pointer to the first  Out I/f info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyOutIfInfo *
D6RlOuIfGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_RLY_OUTIF_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfGetNextNode
 *
 * DESCRIPTION      : This function returns the next  Out I/f info
 *                    from the  Out I/f table.
 *
 * INPUT            :  pServAddr - Ponter to  Serv Address
 *                     u4Index - If Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyOutIfInfo * - Pointer to the next Outif info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyOutIfInfo *
D6RlOuIfGetNextNode (UINT4 u4InIfIndex, tIp6Addr * pServAddr, UINT4 u4Index)
{
    tDhcp6RlyOutIfInfo  OutIf;
    MEMSET (&OutIf, 0, sizeof (tDhcp6RlyOutIfInfo));
    OutIf.pSrvAddr = D6RlSrvGetNode (u4InIfIndex, pServAddr);
    if (OutIf.pSrvAddr == NULL)
    {
        return NULL;
    }
    OutIf.u4OutIfIndex = u4Index;
    return RBTreeGetNext (DHCP6_RLY_OUTIF_TABLE, &OutIf, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlOuIfGetNode
 *
 * DESCRIPTION      : This function helps to find a outif node from the Outif 
 *                    table.
 *
 * INPUT            : pServAddr    - Pointer to Server Addr
 *                    u4IfIndex - If Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyOutIfInfo * - Pointer to the  Out I/f info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyOutIfInfo *
D6RlOuIfGetNode (UINT4 u4InIfIndex, tIp6Addr * pServAddr, UINT4 u4Index)
{
    tDhcp6RlyOutIfInfo  Key;
    Key.pSrvAddr = D6RlSrvGetNode (u4InIfIndex, pServAddr);
    if (Key.pSrvAddr == NULL)
    {
        return NULL;
    }
    Key.u4OutIfIndex = u4Index;
    return RBTreeGet (DHCP6_RLY_OUTIF_TABLE, &Key);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file D6RlOuIf.c                      */
/*-----------------------------------------------------------------------*/
