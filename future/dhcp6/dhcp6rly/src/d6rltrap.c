
/* $Id: d6rltrap.c,v 1.11 2015/02/20 12:05:45 siva Exp $*/
/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains snmp trap specific procedures.
 * ************************************************************************/

#include "d6rlinc.h"
#include "fsdh6r.h"

tSNMP_OID_TYPE     *D6RlTrapMakeObjIdFromDotNew (INT1 *pi1TextStr);
PRIVATE INT4        D6RlTrapParseSubIdNew (INT1 **ppi1TempPtr, UINT4 *pu4Value);

/*****************************************************************************/
/* Function Name      : D6RlTrapParseSubIdNew                                  */
/*                                                                           */
/* Description        : This function Parse the string format in             */
/*                      number.number..format.                               */
/*                                                                           */
/* Input(s)           : ppi1TempPtr - pointer to the string.                 */
/*                      pu4Value    - Pointer the OID List value.            */
/*                                                                           */
/* Output(s)          : ppu1TempPtr - value of ppu1TempPtr                    */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                       */
/*****************************************************************************/

PRIVATE INT4
D6RlTrapParseSubIdNew (INT1 **ppi1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    INT1               *pi1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pi1Tmp = *ppi1TempPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                                 ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                                 ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F')));
         pi1Tmp++)
    {
        u4Value = (u4Value * DHCP6_RLY_VAL_10) + (*pi1Tmp & DHCP6_RLY_4BIT_MAX);
    }

    if (*ppi1TempPtr == pi1Tmp)
    {
        return OSIX_FAILURE;
    }
    *ppi1TempPtr = pi1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : D6RlTrapMakeObjIdFromDotNew                            */
/*                                                                           */
/* Description        : This Function retuns the OID  of the given string for*/
/* the                  proprietary MIB.                                     */
/*                      number.number..format.                               */
/*                                                                           */
/* Input(s)           : pi1TextStr - pointer to the string.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pOidPtr or NULL                                      */
/*****************************************************************************/

tSNMP_OID_TYPE     *
D6RlTrapMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount;
    static INT1         ai1TempBuffer[DHCP6_RLY_ARRAY_SIZE_257];
    UINT4               u4Len = 0;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr) != OSIX_FALSE)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0;
             ((pi1TempPtr < pi1DotPtr)
              && (u2Index < DHCP6_RLY_ARRAY_SIZE_256)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0;
             ((u2Index <
               (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
              && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if (u2Index > DHCP6_RLY_ARRAY_SIZE_68)
            {
                return (NULL);
            }
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                u4Len =
                    ((STRLEN (orig_mib_oid_table[u2Index].pNumber) <
                      sizeof (ai1TempBuffer)) ?
                     STRLEN (orig_mib_oid_table[u2Index].
                             pNumber) : sizeof (ai1TempBuffer) - 1); 
                STRNCPY ((INT1 *) ai1TempBuffer,
                        orig_mib_oid_table[u2Index].pNumber, u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }
        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))

        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr, STRLEN((INT1 *) pi1TextStr));
	ai1TempBuffer[STRLEN((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         ((u2Index < 256) && (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }

    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }
    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {

        if (u2Index > SNMP_MAX_OID_LENGTH)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if ((D6RlTrapParseSubIdNew
             ((INT1 **) (&(pi1TempPtr)),
              &(pOidPtr->pu4_OidList[u2Index]))) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/*****************************************************************************/
/* Function Name      : D6RlTrapSnmpSend                                 */
/*                                                                           */
/* Description        : This function will prepare the TRAP PDU and send it  */
/*                      to the administrator if particular trap is enabled   */
/*                                                                           */
/* Input(s)           : pTrapInfo - Pointer  to the information required     */
/*                      for TRAP                                             */
/*                      u1TrapId - TrapIdentifier                            */
/*                      u4InterfaceId -Interface Inndex                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
D6RlTrapSnmpSend (VOID *pTrapInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tDhcp6RlyInvalidMsgRcvdTrap *pDhcp6RlyInvMsgTrap = NULL;
    tDhcp6RlyHopThresholdTrap *pDhcp6RlyHopThresholdTrap = NULL;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4InterfaceId;
    UINT1               au1Buf[DHCP6_RLY_ARRAY_SIZE_257];
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;

    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    /*Check Relay functionality is available on the Interface */
    switch (u1TrapId)
    {
        case DHCP6_RLY_RCVD_INVALID_MSG_TRAP_VAL:

            if (DHCP6_RLY_RCVD_INVALID_MSG_TRAP_DISABLED () == OSIX_TRUE)
            {
                return;
            }
            pDhcp6RlyInvMsgTrap = (tDhcp6RlyInvalidMsgRcvdTrap *) pTrapInfo;
            u4InterfaceId = pDhcp6RlyInvMsgTrap->u4IfIndex;
            if (DHCP6_RLY_SYS_LOG_OPTION == OSIX_TRUE)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                              "interface-id: %u invalid message received. ",
                              u4InterfaceId));
            }
            break;

        case DHCP6_RLY_HOP_THRESHOLD_TRAP_TRAP_VAL:
            pDhcp6RlyHopThresholdTrap = (tDhcp6RlyHopThresholdTrap *) pTrapInfo;
            u4InterfaceId = pDhcp6RlyHopThresholdTrap->u4IfIndex;

            if (DHCP6_RLY_HOP_THRESHOLD_TRAP_DISABLED () == OSIX_TRUE)
            {
                return;
            }
            if (DHCP6_RLY_SYS_LOG_OPTION == OSIX_TRUE)
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                              "interface-id: %u unable to forward received "
                              "relay-forward message. ", u4InterfaceId));
            }
            break;

        default:
            return;
    }

    MEMSET (au1Buf, 0, DHCP6_RLY_ARRAY_SIZE_257);

   /*----------------- TRAP PDU DHCP6R MIB trap
    * DHCP6_RLY_RCVD_INVALID_MSG_TRAP_VAL------------------------------------*
    * Enterprise_OID|fsDhcp6RlyIfInvalidPktIn_Oid|InterfaceId|1              *
    *------------------------------------------------------------------------*/
    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) DHCP6_RLY_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }
    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = u1TrapId;

    if (u1TrapId == DHCP6_RLY_RCVD_INVALID_MSG_TRAP_VAL)
    {
        SPRINTF ((char *) au1Buf, DHCP6_RLY_MIB_OBJ_INV_MSG_TYPE);
        pOid = D6RlTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_COUNTER32,
                                        (UINT4) u1TrapId,
                                        0, NULL, NULL, SnmpCounter64Type);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        pStartVb = pVbList;
    }
    else if (u1TrapId == DHCP6_RLY_HOP_THRESHOLD_TRAP_TRAP_VAL)
    {
        SPRINTF ((char *) au1Buf, DHCP6_RLY_MIB_OBJ_MAX_HOP_TRAP);
        pOid = D6RlTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);
        if (pOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        pVbList =
            SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_COUNTER32,
                                  (UINT4) u1TrapId,
                                  0, NULL, NULL, SnmpCounter64Type);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        pStartVb = pVbList;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);
#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}
