/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access Server Address table.
 * ************************************************************************/
#include "d6rlinc.h"
/***************************************************************************
 * FUNCTION NAME    : D6RlSrvCreateTable
 *
 * DESCRIPTION      : This function creats the interface table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlSrvCreateTable (VOID)
{
    DHCP6_RLY_SRV_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6RlySrvAddrInfo, RbNode),
                              D6RlSrvRBCmp);
    if (DHCP6_RLY_SRV_TABLE == NULL)
    {
        DHCP6_RLY_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6RlSrvCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvDeleteTable
 *
 * DESCRIPTION      : This function Deletes the interface table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlSrvDeleteTable (VOID)
{
    if (DHCP6_RLY_SRV_TABLE != NULL)
    {
        D6RlSrvDrainTable ();
        RBTreeDestroy (DHCP6_RLY_SRV_TABLE, D6RlSrvRBFree, 0);

    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlSrvRBCmp                                               */
/*                                                                           */
/* Description  : This function compare the server tbl index to RB tree      */
/*                operations in interface table                              */
/*                                                                           */
/* Input        : e1        Pointer to Server node1                          */
/*                e2        Pointer to Server node2                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. 0 if keys of both the elements are same.   */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
D6RlSrvRBCmp (tRBElem * e1, tRBElem * e2)
{
    tDhcp6RlySrvAddrInfo *pServEntry1 = (tDhcp6RlySrvAddrInfo *) e1;
    tDhcp6RlySrvAddrInfo *pServEntry2 = (tDhcp6RlySrvAddrInfo *) e2;

    if (pServEntry1->pIf->u4IfIndex > pServEntry2->pIf->u4IfIndex)
    {
        return DHCP6_RLY_GREATER;
    }
    else if (pServEntry1->pIf->u4IfIndex == pServEntry2->pIf->u4IfIndex)
    {
        return (D6RlUtlIp6AddrCompare (&pServEntry1->Addr, &pServEntry2->Addr));
    }
    else
    {
        return DHCP6_RLY_LESS;
    }
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvCreateNode 
 *
 * DESCRIPTION      : This function creates a Serv table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlySrvAddrInfo * - Pointer to the created Srv node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlSrvCreateNode (VOID)
{
    tDhcp6RlySrvAddrInfo *pSrvAddr = NULL;

    pSrvAddr = (tDhcp6RlySrvAddrInfo *) MemAllocMemBlk
        (DHCP6_RLY_SRV_TBL_POOL_ID);
    if (pSrvAddr == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to allocate memory for Server table\r\n");

        return NULL;
    }
    MEMSET (pSrvAddr, 0x00, sizeof (tDhcp6RlySrvAddrInfo));
    return pSrvAddr;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvAddNode
 *
 * DESCRIPTION      : This function add a node to theServer Addr table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pSrvAddr - pointer to Interface information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlSrvAddNode (tDhcp6RlySrvAddrInfo * pSrvAddr)
{
    if (RBTreeAdd (DHCP6_RLY_SRV_TABLE, pSrvAddr) != RB_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to add node to Server table\r\n");
        return OSIX_FAILURE;
    }
    D6RlIfServerListAddNode (pSrvAddr);
    TMO_SLL_Init (&(pSrvAddr->OutIfList));
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvDelNode
 *
 * DESCRIPTION      : This function delete a Server node from the
 *                    Server table and release the memory used by that
 *                    node to the memory pool. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pSrvAddr - pointer to Server information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlSrvDelNode (tDhcp6RlySrvAddrInfo * pSrvAddr)
{
    D6RlIfServerListDelNode (pSrvAddr);
    RBTreeRem (DHCP6_RLY_SRV_TABLE, pSrvAddr);
    MemReleaseMemBlock (DHCP6_RLY_SRV_TBL_POOL_ID, (UINT1 *) pSrvAddr);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a Serevr node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the Server node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlSrvRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    D6RlIfServerListDelNode ((tDhcp6RlySrvAddrInfo *) pRBElem);
    MemReleaseMemBlock (DHCP6_RLY_SRV_TBL_POOL_ID, (UINT1 *) pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvDrainTable
 *
 * DESCRIPTION      : This function deletes all the Server nodes
 *                    associated with the Server table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlSrvDrainTable (VOID)
{
    RBTreeDrain (DHCP6_RLY_SRV_TABLE, D6RlSrvRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvGetFirstNode
 *
 * DESCRIPTION      : This function returns the first Server node
 *                    from the Server table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlySrvAddrInfo * -pointer to the first Server info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlSrvGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_RLY_SRV_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvGetNextNode
 *
 * DESCRIPTION      : This function returns the next Server info
 *                    from the Server table.
 *
 * INPUT            :  u4Index - If Index 
 *                     pSrvAddr - Ponter to server Address
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlySrvAddrInfo * - Pointer to the next Serv info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlSrvGetNextNode (UINT4 u4Index, tIp6Addr * pSrvAddr)
{
    tDhcp6RlySrvAddrInfo SrvAddr;
    MEMSET (&SrvAddr, 0, sizeof (tDhcp6RlySrvAddrInfo));
    SrvAddr.pIf = D6RlIfGetNode (u4Index);
    if (SrvAddr.pIf == NULL)
    {
        return NULL;
    }
    MEMCPY (&SrvAddr.Addr, pSrvAddr, sizeof (tIp6Addr));
    return RBTreeGetNext (DHCP6_RLY_SRV_TABLE, &SrvAddr, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvGetNode
 *
 * DESCRIPTION      : This function helps to find a if node from the Serv 
 *                    table.
 *
 * INPUT            : u4IfIndex - If Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlySrvAddrInfo * - Pointer to the Server info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlSrvGetNode (UINT4 u4IfIndex, tIp6Addr * pSrvAddr)
{
    tDhcp6RlySrvAddrInfo Key;
    Key.pIf = D6RlIfGetNode (u4IfIndex);
    if (Key.pIf == NULL)
    {
        return NULL;
    }
    MEMCPY (&Key.Addr, pSrvAddr, sizeof (tIp6Addr));
    return RBTreeGet (DHCP6_RLY_SRV_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvOutIfListDelNode
 *
 * DESCRIPTION      : This function delete the out interface from the list. 
 *
 * INPUT            : pOutIfInfo - out If info
 *
 * OUTPUT           : None
 *
 * RETURNS          : None 
 *
 **************************************************************************/

PUBLIC VOID
D6RlSrvOutIfListDelNode (tDhcp6RlyOutIfInfo * pOutIfInfo)
{
    TMO_SLL_Delete (&(pOutIfInfo->pSrvAddr->OutIfList),
                    &(pOutIfInfo->SrvAddrSllLink));
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvOutIfListAddNode
 *
 * DESCRIPTION      : This function add the out interface to the list. 
 *
 * INPUT            : pOutIfInfo - out If info
 *
 * OUTPUT           : None
 *
 * RETURNS          : None 
 *
 **************************************************************************/

PUBLIC INT4
D6RlSrvOutIfListAddNode (tDhcp6RlyOutIfInfo * pOutIfInfo)
{
    TMO_SLL_Init_Node (&(pOutIfInfo->SrvAddrSllLink));
    TMO_SLL_Add (&(pOutIfInfo->pSrvAddr->OutIfList),
                 &(pOutIfInfo->SrvAddrSllLink));
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvOutIfListGetFirstNode
 *
 * DESCRIPTION      : This function return the first out interface from the list. 
 *
 * INPUT            : pOutIfInfo - out If info
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyOutIfInfo-> pointer to out inerface
 *
 **************************************************************************/

PUBLIC tDhcp6RlyOutIfInfo *
D6RlSrvOutIfListGetFirstNode (tDhcp6RlySrvAddrInfo * pSrvInfo)
{
    return (tDhcp6RlyOutIfInfo *) TMO_SLL_First (&(pSrvInfo->OutIfList));
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvOutIfListGetNextNode
 *
 * DESCRIPTION      : This function helps to find a next out interface of 
 *                    server node map to the interface.
 *
 * INPUT            : pSrvInfo - Pointer to server Info
 *                    pOutIfInfo - Pointer to interface Info
 *
 * OUTPUT           : None
 *
 * RETURNS          : D6RlSrvOutIfListGetNextNode * - Pointer to the out info. 
 *                    NULL will be returned if no node is found in this 
 *                    table.
 *
 **************************************************************************/

PUBLIC tDhcp6RlyOutIfInfo *
D6RlSrvOutIfListGetNextNode (tDhcp6RlySrvAddrInfo * pSrvInfo,
                             tDhcp6RlyOutIfInfo * pOutIfInfo)
{
    return (tDhcp6RlyOutIfInfo *) TMO_SLL_Next (&(pSrvInfo->OutIfList),
                                                &(pOutIfInfo->SrvAddrSllLink));
}

/***************************************************************************
 * FUNCTION NAME    : D6RlSrvGetFirstNodeFromList
 *
 * DESCRIPTION      : This function helps to find a first server node map to the
 *                    interface.
 *
 * INPUT            : pDhcp6RlyIfInfo - Pointer to interface
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlySrvAddrInfo * - Pointer to the Server info. 
 *                    NULL will be returned if no node is found in this 
 *                    table.
 *
 **************************************************************************/

PUBLIC tDhcp6RlySrvAddrInfo *
D6RlSrvGetFirstNodeFromList (tDhcp6RlyIfInfo * pDhcp6RlyIfInfo)
{

    return (tDhcp6RlySrvAddrInfo *)
        TMO_SLL_First (&(pDhcp6RlyIfInfo->SrvAddrList));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file D6RlSrv.c                      */
/*-----------------------------------------------------------------------*/
