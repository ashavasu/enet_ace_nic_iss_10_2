/* $Id: d6rlif.c,v 1.8 2012/06/08 13:30:20 siva Exp $   */

/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access Interface table.
 * ************************************************************************/
#include "d6rlinc.h"
/***************************************************************************
 * FUNCTION NAME    : D6RlIfCreateTable
 *
 * DESCRIPTION      : This function creats the interface table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlIfCreateTable (VOID)
{
    DHCP6_RLY_IF_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6RlyIfInfo, RbNode),
                              D6RlIfRBCmp);
    if (DHCP6_RLY_IF_TABLE == NULL)
    {
        DHCP6_RLY_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6RlIfCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfDeleteTable
 *
 * DESCRIPTION      : This function Deletes the interface table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlIfDeleteTable (VOID)
{
    if (DHCP6_RLY_IF_TABLE != NULL)
    {
        D6RlIfDrainTable ();
        RBTreeDestroy (DHCP6_RLY_IF_TABLE, D6RlIfRBFree, 0);

    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlIfRBCmp                                                */
/*                                                                           */
/* Description  : This function compare the interface ids to RB tree         */
/*                operations in interface table                              */
/*                                                                           */
/* Input        : e1        Pointer to Interface node1                       */
/*                e2        Pointer to Interface node2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. 0 if keys of both the elements are same.   */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/

PUBLIC INT4
D6RlIfRBCmp (tRBElem * e1, tRBElem * e2)
{
    tDhcp6RlyIfInfo    *pIface1 = (tDhcp6RlyIfInfo *) e1;
    tDhcp6RlyIfInfo    *pIface2 = (tDhcp6RlyIfInfo *) e2;

    if (pIface1->u4IfIndex > pIface2->u4IfIndex)
    {
        return DHCP6_RLY_GREATER;
    }
    else if (pIface1->u4IfIndex == pIface2->u4IfIndex)
    {
        return DHCP6_RLY_EQUAL;
    }
    else
    {
        return DHCP6_RLY_LESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlIfCheckRowCanSet                                       */
/*                                                                           */
/* Description  : This function checks whether a row of interface table can  */
/*                Set object on it                                           */
/*                                                                           */
/* Input        : i4FsDhcp6RlyIfIndex - Interface index                      */
/*                pu4ErrorCode - pointer to Error code.                      */
/*                                                                           */
/* Output       : au1IpV6Addr                                                */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6RlIfCheckRowCanSet (INT4 i4FsDhcp6RlyIfIndex, UINT4 *pu4ErrorCode)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    if ((pInterface = D6RlIfGetNode (i4FsDhcp6RlyIfIndex)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "If Index not present\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if ((pInterface->u1RowStatus != NOT_IN_SERVICE) &&
        (pInterface->u1RowStatus != NOT_READY))
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Row status is not NOT_IN_SERVICE or NOT_READY\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RCheckIfDuidIsValid                                      */
/*                                                                           */
/* Description  : This function checks for the unique duid for interface     */
/*                                                                               */
/*                                                                           */
/* Input        : i4FsDhcp6RlyIfIndex - Interface index                         */
/*                  Duid Value to be set                                         */
/*                pu4ErrorCode - pointer to Error code.                      */
/*                                                                           */
/* Output       :                                                             */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6RCheckIfDuidIsValid (tSNMP_OCTET_STRING_TYPE
                       * pTestValFsDhcp6RlyIfRemoteIdDUID, UINT4 *pu4ErrorCode)
{
    tDhcp6RlyIfInfo    *pFirstInterface = NULL;
    tDhcp6RlyIfInfo    *pNextInterface = NULL;
    tDhcp6RlyIfInfo    *pCurInterface = NULL;
    INT4                i4NextIfIndex = 0;
    INT4                i4FirstIfIndex = 0;

    if ((pFirstInterface = D6RlIfGetFirstNode ()) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "If Index not present\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    i4FirstIfIndex = (INT4) pFirstInterface->u4IfIndex;
    i4NextIfIndex = i4FirstIfIndex;
   
    if ((pCurInterface = D6RlIfGetNode (i4NextIfIndex)) == NULL)                                                                 {
            DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |                                                                                                         DHCP6_RLY_FAILURE_TRC, "If Index not present\r\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;                                                                                        return OSIX_FAILURE;
    }
    pNextInterface = pCurInterface;
    	   
    do
    {  

        /* Test if this id have already been mapped to any other interface */
        if ((pNextInterface->Dhcp6RemIdOpt.i4RemIdLen) ==
            ( pTestValFsDhcp6RlyIfRemoteIdDUID->i4_Length))
        {
            if (!MEMCMP (pNextInterface->Dhcp6RemIdOpt.au1Duid,
                         pTestValFsDhcp6RlyIfRemoteIdDUID->pu1_OctetList,
                         pTestValFsDhcp6RlyIfRemoteIdDUID->i4_Length))
            {
                /* The duid is already mapped to a different interface */
                CLI_SET_ERR (DHCP6_RLY_DUPLICATE_REMOTE_ID_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
	    i4NextIfIndex = (INT4) pNextInterface->u4IfIndex;
    	pNextInterface = D6RlIfGetNextNode (i4NextIfIndex);
    }
    while (pNextInterface);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RCheckIfUserDefinedasciiIsValid                                      */
/*                                                                           */
/* Description  : This function checks for the unique ascii value for interface     */
/*                                                                               */
/*                                                                           */
/* Input        : i4FsDhcp6RlyIfIndex - Interface index                         */
/*                  unique ascii Value to be set                                         */
/*                pu4ErrorCode - pointer to Error code.                      */
/*                                                                           */
/* Output       :                                                             */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
D6RCheckIfUserDefinedAsciiIsValid (tSNMP_OCTET_STRING_TYPE
                       * pTestValFsDhcp6RlyIfRemoteIdUserDefined, UINT4 *pu4ErrorCode)
{
    tDhcp6RlyIfInfo    *pFirstInterface = NULL;
    tDhcp6RlyIfInfo    *pNextInterface = NULL;
    tDhcp6RlyIfInfo    *pCurInterface = NULL;
    INT4                i4NextIfIndex = 0;
    INT4                i4FirstIfIndex = 0;

    if ((pFirstInterface = D6RlIfGetFirstNode ()) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC, "If Index not present\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    i4FirstIfIndex = (INT4) pFirstInterface->u4IfIndex;
    i4NextIfIndex = i4FirstIfIndex;

    if ((pCurInterface = D6RlIfGetNode (i4NextIfIndex)) == NULL)
        {
            DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                           DHCP6_RLY_FAILURE_TRC, "If Index not present\r\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return OSIX_FAILURE;
        }
    pNextInterface = pCurInterface;
    do
    {

        /* Test if this value have already been mapped to any other interface */
        if (STRLEN(pNextInterface->Dhcp6RemIdOpt.au1UserDefined) ==
            ((UINT4) pTestValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length))
        {
            if (!MEMCMP (pNextInterface->Dhcp6RemIdOpt.au1UserDefined,
                         pTestValFsDhcp6RlyIfRemoteIdUserDefined->pu1_OctetList,
                         pTestValFsDhcp6RlyIfRemoteIdUserDefined->i4_Length))
            {
                /* The value is already mapped to a different interface */
                CLI_SET_ERR (DHCP6_RLY_DUPLICATE_REMOTE_ID_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        i4NextIfIndex = (INT4) pNextInterface->u4IfIndex;
	    pNextInterface = D6RlIfGetNextNode (i4NextIfIndex);
    }
    while (pNextInterface);

    return OSIX_SUCCESS;
}



/***************************************************************************
 * FUNCTION NAME    : D6RlIfCreateNode 
 *
 * DESCRIPTION      : This function creates a If table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyIfInfo * - Pointer to the created Interface node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyIfInfo *
D6RlIfCreateNode (VOID)
{
    tDhcp6RlyIfInfo    *pInterface = NULL;

    pInterface = (tDhcp6RlyIfInfo *) MemAllocMemBlk (DHCP6_RLY_IF_TBL_POOL_ID);
    if (pInterface == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to allocate memory for Interface table\r\n");

        return NULL;
    }
    MEMSET (pInterface, 0x00, sizeof (tDhcp6RlyIfInfo));
    return pInterface;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfAddNode
 *
 * DESCRIPTION      : This function add a node to the Interface table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pInterface - pointer to Interface information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlIfAddNode (tDhcp6RlyIfInfo * pInterface)
{
    if (RBTreeAdd (DHCP6_RLY_IF_TABLE, pInterface) != RB_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to allocate memory for Interface table\r\n");
        return OSIX_FAILURE;
    }
    TMO_SLL_Init (&(pInterface->SrvAddrList));
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfDelNode
 *
 * DESCRIPTION      : This function delete a Interface node from the
 *                    Interface table and release the memory used by that
 *                    node to the memory pool. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pInterface - pointer to Interface information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlIfDelNode (tDhcp6RlyIfInfo * pInterface)
{
    RBTreeRem (DHCP6_RLY_IF_TABLE, pInterface);
    MemReleaseMemBlock (DHCP6_RLY_IF_TBL_POOL_ID, (UINT1 *) pInterface);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a Interface node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the I/face node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6RlIfRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    MemReleaseMemBlock (DHCP6_RLY_IF_TBL_POOL_ID, (UINT1 *) pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfDrainTable
 *
 * DESCRIPTION      : This function deletes all the Interface nodes
 *                    associated with the Interface table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6RlIfDrainTable (VOID)
{
    RBTreeDrain (DHCP6_RLY_IF_TABLE, D6RlIfRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfGetFirstNode
 *
 * DESCRIPTION      : This function returns the first Interface node
 *                    from the Interface table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyIfInfo * - pointer to the first Interface info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyIfInfo *
D6RlIfGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_RLY_IF_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfGetNextNode
 *
 * DESCRIPTION      : This function returns the next Interface info
 *                    from the Interface table.
 *
 * INPUT            : u4Index - Interface Index 
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyIfInfo * - Pointer to the next Interface info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyIfInfo *
D6RlIfGetNextNode (UINT4 u4Index)
{
    tDhcp6RlyIfInfo     Interface;
    MEMSET (&Interface, 0, sizeof (tDhcp6RlyIfInfo));
    Interface.u4IfIndex = u4Index;
    return RBTreeGetNext (DHCP6_RLY_IF_TABLE, &Interface, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6RlIfGetNode
 *
 * DESCRIPTION      : This function helps to find a if node from the if 
 *                    table.
 *
 * INPUT            : u4IfIndex - If Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6RlyIfInfo * - Pointer to the I/F info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6RlyIfInfo *
D6RlIfGetNode (UINT4 u4IfIndex)
{
    tDhcp6RlyIfInfo     Key;
    Key.u4IfIndex = u4IfIndex;
    return RBTreeGet (DHCP6_RLY_IF_TABLE, &Key);
}
PUBLIC VOID
D6RlIfServerListDelNode (tDhcp6RlySrvAddrInfo * pSrvInfo)
{
    TMO_SLL_Delete (&(pSrvInfo->pIf->SrvAddrList), &(pSrvInfo->InIfSllLink));

    return;
}
PUBLIC INT4
D6RlIfServerListAddNode (tDhcp6RlySrvAddrInfo * pSrvInfo)
{
    TMO_SLL_Init_Node (&(pSrvInfo->InIfSllLink));
    TMO_SLL_Add (&(pSrvInfo->pIf->SrvAddrList), &(pSrvInfo->InIfSllLink));
    return OSIX_SUCCESS;
}
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlIfServerListGetFirstNode (tDhcp6RlyIfInfo * pIfInfo)
{
    return (tDhcp6RlySrvAddrInfo *) TMO_SLL_First (&(pIfInfo->SrvAddrList));

}
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlIfServerListGetNextNode (tDhcp6RlyIfInfo * pIfInfo,
                             tDhcp6RlySrvAddrInfo * pSrvInfo)
{
    return (tDhcp6RlySrvAddrInfo *) TMO_SLL_Next (&(pIfInfo->SrvAddrList),
                                                  &(pSrvInfo->InIfSllLink));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file D6RlIf.c                      */
/*-----------------------------------------------------------------------*/
