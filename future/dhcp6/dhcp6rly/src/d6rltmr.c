/*******************************************************************************************
 *   Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * 
 *   $Id: d6rltmr.c,v 1.2 2014/07/17 12:29:02 siva Exp $
 *   
 *   DESC : This File contains Timer handling functions for  Prefix Delegation options.
 *********************************************************************************************/

#include "d6rlinc.h"

PRIVATE VOID D6RlTmrInitTmrDesc PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : D6RlTmrInit                                                *
*                                                                           *
* Description  : Creates and initializes the DHCP6 Relay Timer List               *                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
D6RlTmrInit (VOID)
{
    if (TmrCreateTimerList
        ((CONST UINT1 *) DHCP6_RLY_TASK_NAME, DHCP6_RLY_TIMER_EVENT, NULL,
         (tTimerListId *) & (gDhcp6RlyGblInfo.RlyTmrLst)) == TMR_FAILURE)
    {
        DHCP6_RLY_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "DHCP6 Relay Timer List Creation Failed\n");
        return OSIX_FAILURE;
    }

    D6RlTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : D6RlTmrDeInit                                   */
/*                                                                      */
/*  Description     : This function deletes the timer list created for  */
/*                    DHCPv6 Relay Task.                               */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : None                                              */
/************************************************************************/

PUBLIC INT4
D6RlTmrDeInit (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    if (gDhcp6RlyGblInfo.RlyTmrLst != 0)
    {
        /* Deleting the Timer List */
        if (TmrDeleteTimerList (gDhcp6RlyGblInfo.RlyTmrLst) == TMR_FAILURE)
        {
            DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC | DHCP6_RLY_FAILURE_TRC,
                           "D6RlTmrDeInit: "
                           "Timer list deleteion FAILED for DHCP6 Relay Task \r\n");
            i4RetVal = OSIX_FAILURE;
        }
        MEMSET (&(gDhcp6RlyGblInfo.Dhcp6RlyTmrDesc), 0, (sizeof (tTmrDesc)));

        gDhcp6RlyGblInfo.RlyTmrLst = 0;
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_CONTROL_PLANE_TRC,
                       "D6RlTmrDeInit: DHCP6 Relay Task Timers DeInitialized \r\n");
    }
    return i4RetVal;
}

/****************************************************************************
*                                                                           *
* Function     : D6RlTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize DHCP6 Relay Timer Descriptors                   *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PRIVATE VOID
D6RlTmrInitTmrDesc (VOID)
{

    gDhcp6RlyGblInfo.Dhcp6RlyTmrDesc.i2Offset =
        (INT2) FSAP_OFFSETOF (tDhcp6RlyPDEntry, Dhcp6RlyTmrBlk);

    gDhcp6RlyGblInfo.Dhcp6RlyTmrDesc.TmrExpFn = D6RlTmrValidTimeExpired;

}

/************************************************************************/
/*  Function Name   : D6RlTmrStart                                  */
/*                                                                      */
/*  Description     : This function is called to start timer for DHCPv6 */
/*                    Relay Task.                                      */
/*                                                                      */
/*  Input(s)        : pDhcp6RlyPDEntry Pointer to PD Node              */
/*                  : u4Duration  - Duration for which the timer needs  */
/*                                  to be started in units of sec.     */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC INT4
D6RlTmrStart (tTmrBlk * pDhcp6RlyTmr, UINT1 u1TmrId, UINT4 u4Duration,
              UINT4 u4MilliSecs)
{
    if (TmrStart
        (gDhcp6RlyGblInfo.RlyTmrLst, pDhcp6RlyTmr, u1TmrId, u4Duration,
         u4MilliSecs) == TMR_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                       "D6RlTmrStart: Failed to start Timer " "!! \r\n");
        return OSIX_FAILURE;
    }

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC | DHCP6_RLY_MGMT_TRC,
                   "D6RlTmrStart: PD Timer Start.\r\n");

    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : D6RlTmrStopTimer                                   */
/*                                                                      */
/*  Description     : This function is called to stop timer for DHCPv6  */
/*                    Relay Task.                                      */
/*                                                                      */
/*  Input(s)        : pDhcp6RlyPDEntry Pointer to PD Node             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC INT4
D6RlTmrStopTimer (tTmrBlk * pDhcp6RlyTmr)
{
    TmrStop (gDhcp6RlyGblInfo.RlyTmrLst, pDhcp6RlyTmr);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : D6RlTmrExpHandler                                 */
/*                                                                      */
/*  Description     : This function is called from DHCPv6 Relay Main    */
/*                    when Task receives the timer expiry event.        */
/*                                                                      */
/*  Input(s)        : None                                              */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS or OSIX_FAILURE                      */
/************************************************************************/
PUBLIC VOID
D6RlTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    INT2                i2Offset = 0;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gDhcp6RlyGblInfo.RlyTmrLst)) != NULL)
    {

        i2Offset = gDhcp6RlyGblInfo.Dhcp6RlyTmrDesc.i2Offset;

        /* Call the registered expired handler function */

        (*(gDhcp6RlyGblInfo.Dhcp6RlyTmrDesc.TmrExpFn))
            ((UINT1 *) pExpiredTimers - i2Offset);
    }

    return;
}

/******************************************************************************
 * Function Name      : D6RlTmrValidTimeExpired
 *
 * Description        : This routine is called when DHCP6 Relay PD timer running
 *                      for PD Route got expired.
 *
 * Input(s)           : pDhcp6RlyPDEntry Pointer to Prefix Delegation Entry
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

PUBLIC VOID
D6RlTmrValidTimeExpired (VOID *pArg)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    UINT1               u1CmdType = NETIPV6_DELETE_ROUTE;

    pDhcp6RlyPDEntry = (tDhcp6RlyPDEntry *) pArg;

    if (pDhcp6RlyPDEntry == NULL)
    {
        return;
    }

    if (D6RlLeakPDRoute (pDhcp6RlyPDEntry, u1CmdType) == OSIX_FAILURE)
    {
        return;
    }

    /*set valid time to zero,to indicate timer expired in current node */
    pDhcp6RlyPDEntry->u4ValidLifeTime = 0;

    Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                 &(pDhcp6RlyPDEntry->RbPrefixDelegationDbNode));
    Dhcp6RlyRedSyncDynInfo ();

    D6RlPDDelNode (pDhcp6RlyPDEntry);
    return;

}
