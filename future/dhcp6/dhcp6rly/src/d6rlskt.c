
/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains DHCP6 client task main loop and
 *              initialization   routines.
 *  $Id: d6rlskt.c,v 1.18 2015/03/14 11:49:39 siva Exp $
 * ************************************************************************/

#include "d6rlinc.h"
/**************************************************************************/
/*   Function Name   : Dhcp6RSockInit                                     */
/*   Description     : This function initializes the socket               */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE                   */
/**************************************************************************/

PUBLIC INT4
D6RlSktInit (VOID)
{

    struct sockaddr_in6 SockAddr;
    INT4                i4SockId = -1;
    INT4                i4Optval = 1;
#ifdef NPAPI_WANTED
    tHwCfaFilterInfo    HwCfaFilterInfo;
    tIp6Addr            McastAddr;
    tIp6Addr            RelayMcastAddr;
#endif

    MEMSET (&SockAddr, 0, sizeof (struct sockaddr_in6));
    SockAddr.sin6_family = AF_INET6;
    SockAddr.sin6_port = OSIX_HTONS (DHCP6_RLY_LISTEN_PORT);
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &SockAddr.sin6_addr.s6_addr);
    /* Create a UDP socket */
    i4SockId = socket (AF_INET6, SOCK_DGRAM, 0);

    if (i4SockId < 0)
    {
        return OSIX_FAILURE;
    }
    /* Bind the socket to the port */
    if (bind (i4SockId, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) < 0)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_DATA_PATH_TRC | DHCP6_RLY_FAILURE_TRC,
                       "Unable to bind to Dhcp relay Port\n");
        return OSIX_FAILURE;
    }
    if (setsockopt (i4SockId, IPPROTO_IPV6, IP_PKTINFO, (INT2 *) (VOID *)
                    &i4Optval, sizeof (i4Optval)) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    /* Make socket as non blocking */
    if (fcntl (i4SockId, F_SETFL, O_NONBLOCK) < 0)
    {
        close (i4SockId);
        DHCP6_RLY_TRC (DHCP6_RLY_DATA_PATH_TRC,
                       "Unable to make the socket non blocking\n");
        return OSIX_FAILURE;
    }
    /*Set the option not to receive multicast packet on the interface on which
     * it is sent*/
    i4Optval = FALSE;
    if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4Optval, sizeof (i4Optval)) < 0)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_DATA_PATH_TRC | DHCP6_RLY_FAILURE_TRC,
                       "setsockopt Failed for IPV6_MULTICAST_LOOP in DHCP6RLY. \n");
        return OSIX_FAILURE;
    }

    if (SelAddFd (i4SockId, D6RlSktDataRcvd) != OSIX_SUCCESS)
    {
        close (i4SockId);
        return OSIX_FAILURE;
    }

    if (DHCP6_RLY_SOCKET_FD >= 0)
    {
        SelRemoveFd (DHCP6_RLY_SOCKET_FD);
        close (DHCP6_RLY_SOCKET_FD);
    }
    DHCP6_RLY_SOCKET_FD = i4SockId;
#ifdef NPAPI_WANTED
    INET_ATON6 (DHCP6_ALL_RELAY_SERVER, &McastAddr);
    MEMSET (&HwCfaFilterInfo, 0, sizeof (tHwCfaFilterInfo));
    HwCfaFilterInfo.u4IfIndex = 0;
    HwCfaFilterInfo.u2Addr1Type = CFA_NP_IPV6_PROTO;
    MEMCPY (HwCfaFilterInfo.au1Addr1, &McastAddr.u1_addr, sizeof (tIp6Addr));
    if (FsCfaHwCreatePktFilter
        (&HwCfaFilterInfo, &gDhcp6RlyGblInfo.u4RlySrvFilterId) != FNP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    INET_ATON6 (DHCP6_ALL_SERVER, &RelayMcastAddr);
    MEMCPY (HwCfaFilterInfo.au1Addr1, &RelayMcastAddr.u1_addr,
            sizeof (tIp6Addr));
    if (FsCfaHwCreatePktFilter
        (&HwCfaFilterInfo, &gDhcp6RlyGblInfo.u4RlyFilterId) != FNP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

#endif

    return OSIX_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : Dhcp6RSockDeInit                                   */
/*   Description     : This function de-initializes the socket            */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
PUBLIC VOID
D6RlSktDeInit (VOID)
{
    if (DHCP6_RLY_SOCKET_FD != -1)
    {
        SelRemoveFd (DHCP6_RLY_SOCKET_FD);
        close (DHCP6_RLY_SOCKET_FD);
        DHCP6_RLY_SOCKET_FD = -1;
    }
#ifdef NPAPI_WANTED
    CfaFsCfaHwDeletePktFilter (gDhcp6RlyGblInfo.u4RlySrvFilterId);
    CfaFsCfaHwDeletePktFilter (gDhcp6RlyGblInfo.u4RlyFilterId);
    CfaFsCfaHwDeletePktFilter (gDhcp6RlyGblInfo.u4RlyClntBcastFilterId1);
    CfaFsCfaHwDeletePktFilter (gDhcp6RlyGblInfo.u4RlyClntBcastFilterId2);
#endif

    return;
}

/**************************************************************************/
/*   Function Name   : D6RlSktDataRcvd                                    */
/*   Description     : This function initializes the socket.              */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/

PUBLIC VOID
D6RlSktDataRcvd (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);
    OsixEvtSend (DHCP6_RLY_TASK_ID, DHCP6_RLY_PKT_RCVD_EVENT);
    return;
}
