/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlsz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _D6RLSZ_C
#include "d6rlinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
D6rlSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < D6RL_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsD6RLSizingParams[i4SizingId].u4StructSize,
                              FsD6RLSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(D6RLMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            D6rlSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
D6rlSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsD6RLSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, D6RLMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
D6rlSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < D6RL_MAX_SIZING_ID; i4SizingId++)
    {
        if (D6RLMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (D6RLMemPoolIds[i4SizingId]);
            D6RLMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
