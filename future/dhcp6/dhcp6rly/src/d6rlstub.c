/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlstub.c,v 1.1 2014/06/23 11:30:07 siva Exp $
 *
 * Description: This file contains DHCP6RLY Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __D6RLSTUB_C
#define __D6RLSTUB_C

#include "d6rlinc.h"
#include "d6rlred.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize Dhcp6rly dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Dhcp6Rly descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Dhcp6Rly  db node. */
/*                                                                           */
/*    Input(s)            : pDhcp6RlyPDInfo - pointer to Dhcp6Rly  Entry.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDbNodeInit (tDbTblNode * pDhcp6RlyDbTblNode)
{
    UNUSED_PARAM (pDhcp6RlyDbTblNode);
    return;
}


/************************************************************************
 *  Function Name   : Dhcp6RlyRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the DHCP6RLY module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
Dhcp6RlyRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the DHCP6RLY module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register DHCP6RLY with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
Dhcp6RlyRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : Dhcp6RlyRmDeRegisterProtocols                        
 *                                                                    
 *  Description     : This function is invoked by the DHCP6RLY module to   
 *                    de-register itself with the RM module.          
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
Dhcp6RlyRedRmDeRegisterProtocols (VOID)
{    
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to DHCP6RLY        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the DHCP6RLY module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the DHCP6RLY Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleRmEvents ()
{
    return;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the DHCP6RLY upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the DHCP6RLY upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleGoStandby (tDhcp6RlyRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleIdleToStandby (VOID)
{
    return;
}
/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);

    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);

    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the DHCP6RLY module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendBulkReqMsg                            */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedSendBulkReqMsg (VOID)
{
    return;
}
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pDhcp6RlyDataDesc - This is Dhcp6Rly sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pDhcp6RlyDbNode - This is db node defined in the DHCP6RLY*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDbUtilAddTblNode (tDbTblDescriptor * pDhcp6RlyDataDesc,
                         tDbTblNode * pDhcp6RlyDbNode)
{
    UNUSED_PARAM (pDhcp6RlyDataDesc);
    UNUSED_PARAM (pDhcp6RlyDbNode);
    return;
}
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate DHCP6RLY dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedSyncDynInfo (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedSendBulkUpdMsg (VOID)
{
    return;

}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the DHCP6RLY offers an IP address         */
/*                      to the DHCP6RLY client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pDhcp6RlyBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Dhcp6RlyRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Dhcp6RlyRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

#endif
