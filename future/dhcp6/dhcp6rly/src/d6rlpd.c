/*******************************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlpd.c,v 1.7 2015/03/14 11:49:39 siva Exp $
 *
 * DESC : This File contains processing of Prefix Delegation options from server or client 
 *******************************************************************************************/

#include "d6rlinc.h"

PRIVATE INT4        D6RlParsPrefixInfo
PROTO ((UINT1 *, UINT4, UINT4, UINT4, tIp6Addr * pNextHop));
PRIVATE INT4 D6RlPDRouteDeletionTxId PROTO ((UINT4));

/****************************************************************************
*                                                                           *
* Function     : D6RlParsPDInfo                                             * 
*
*                                                                           *
* Description  : This function to parse information of single/multiple      *  
*                IA_PD containing prefixes and other dhcpv6 options to be   *
*                parsed                                                     *
*                                                                           *
*                It is called by both message flow,                         *
*                 1. relay-reply from server to relay                       *
*                 2. decline/release message from client                    *  
*                 3. renew/rebind message from client                       *
* Input        : pu1ParsPDBuf - Buffer containing dhcp message              *
*                 u2Len -length of dhcp message in buffer                   *
*                 u4MsgType - message type of dhcpv6                        *
*                 u4TxId - transaction id used between dhcpv6 messages      * 
*                 u4RouteIndex - logical interface Index via route is added *
*                 pNextHop - nexthop for the route to be added.             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
D6RlParsPDInfo (UINT1 *pu1ParsPDBuf,
                UINT2 u2Len,
                UINT4 u4MsgType,
                UINT4 u4TxId, UINT4 u4RouteIndex, tIp6Addr * pNextHop)
{
    UINT4               u4Offset = 0;
    UINT2               u2OptionType = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2StatusMsg = 0;

    /* till packet length, check for IA_PD options */
    while (u4Offset < u2Len)
    {
        DHCP6_RLY_GET_2_BYTE (pu1ParsPDBuf, u4Offset, &u2OptionType);

        if (u2OptionType == DHCP6_RLY_IA_PD_OPTION)
        {
            DHCP6_RLY_GET_2_BYTE (pu1ParsPDBuf, u4Offset, &u2OptionLen);
            u4Offset = u4Offset - 2;
            if (OSIX_FAILURE ==
                D6RlParsPrefixInfo (pu1ParsPDBuf + u4Offset, u4MsgType, u4TxId,
                                    u4RouteIndex, pNextHop))
            {
                DHCP6_RLY_TRC_ARG2 (DHCP6_RLY_FAILURE_TRC,
                                    "D6RlParsPDInfo : D6RlParsPrefixInfo function failure for Msg :%u ,TxId : %u !!!\r\n",
                                    u4MsgType, u4TxId);

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "D6RlParsPDInfo : D6RlParsPrefixInfo function failure for Msg :%u ,TxId : %u !!!\r\n",
                                    u4MsgType, u4TxId));
                return OSIX_FAILURE;
            }
            /*this is 2 bytes included to get back to options */
            u4Offset = u4Offset + u2OptionLen + 2;
        }
        else if (u2OptionType == DHCP6_RLY_STATUS_MSG_OPTION)
        {
            DHCP6_RLY_GET_2_BYTE (pu1ParsPDBuf, u4Offset, &u2OptionLen);
            DHCP6_RLY_GET_2_BYTE (pu1ParsPDBuf, u4Offset, &u2StatusMsg);
            /* if NoBinding status is received in server reply msg for particular prefix,
               then those prefixes with given transaction id has to be removed from rbtree & rtm6 */
            if ((u2StatusMsg == DHCP6_RLY_NOBINDING)
                && (u4MsgType == DHCP6_SERVER_REPLY))
            {
                if (OSIX_FAILURE == D6RlPDRouteDeletionTxId (u4TxId))
                {
                    DHCP6_RLY_TRC_ARG2 (DHCP6_RLY_FAILURE_TRC,
                                        "D6RlParsPDInfo : D6RlPDRouteDeletionTxId function failure for Msg :%u , TxId : %u !!!\r\n",
                                        u4MsgType, u4TxId);

                    DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL,
                                        DHCP6_RLY_SYSLOG_ID,
                                        "D6RlParsPDInfo : D6RlPDRouteDeletionTxId function failure for Msg :%u , TxId : %u !!!\r\n",
                                        u4MsgType, u4TxId));
                    return OSIX_FAILURE;
                }
                /*since option length &status message is read */
                u2OptionLen = (UINT2) (u2OptionLen - 4);
                /*skip the rest of message in status msg,by moving the offset */
                u4Offset = u4Offset + u2OptionLen;
            }
        }
        else
        {
            /*get the option length (might be any dhcp option),skip those options by offset */
            DHCP6_RLY_GET_2_BYTE (pu1ParsPDBuf, u4Offset, &u2OptionLen);
            u4Offset = u4Offset + u2OptionLen;
        }

    }                            /*End of while loop */
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : D6RlParsPrefixInfo                                         *
*                                                                           *
* Description  : This function to parse information of prefixes in IA_PD,   *
*                which may contain single/multiple IA_PD prefix options.    *
*                                                                           *
*                It is called by both message flow,                         *
*                 1.relay-reply from server to relay                        *
*                 2. decline/release message from client                    *
*                  3. renew/rebind message from client                      *
* Input        : pu1ParsPrefixBuf - buffer containing prefix options        *
*                u4MsgType - message type of dhcpv6                         *
*                u4TxId - transaction id used between dhcpv6 messages       *
*                u4RouteIndex - logical interface Index via route is added  *
*                pNextHop - nexthop for the route to be added.              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
D6RlParsPrefixInfo (UINT1 *pu1ParsPrefixBuf,
                    UINT4 u4MsgType,
                    UINT4 u4TxId, UINT4 u4RouteIndex, tIp6Addr * pNextHop)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    tIp6Addr            Ipv6Prefix;
    UINT4               u4Offset = 0;
    UINT2               u2OptionType = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2IA_PD_OptionLen = 0;
    UINT2               u2IA_PD_Prefix_OptionLen = 0;
    UINT4               u4IA_ID = 0;
    UINT4               u4Time1 = 0;
    UINT4               u4Time2 = 0;
    UINT4               u4Preferredtime = 0;
    UINT4               u4Validtime = 0;
    UINT1               u1CommandType = 0;
    UINT1               u1PrefixLength = 0;
    UINT1               aTempPad[3];

    MEMSET (&Ipv6Prefix, 0, sizeof (tIp6Addr));
    MEMSET (aTempPad, 0, sizeof (aTempPad));

    /*get the IA_PD option length */
    DHCP6_RLY_GET_2_BYTE (pu1ParsPrefixBuf, u4Offset, &u2IA_PD_OptionLen);

    /*Obtain IA_ID */
    DHCP6_RLY_GET_4_BYTE (pu1ParsPrefixBuf, u4Offset, &u4IA_ID);

    /*Obtain T1 time */
    DHCP6_RLY_GET_4_BYTE (pu1ParsPrefixBuf, u4Offset, &u4Time1);

    /*Obtain T2 time */
    DHCP6_RLY_GET_4_BYTE (pu1ParsPrefixBuf, u4Offset, &u4Time2);

    /*To get IA_PD options,12 bytes(IA_ID, T1, T2) are truncated from IA_PD_OptionLen */
    u2IA_PD_OptionLen = (UINT2) (u2IA_PD_OptionLen - 12);

    while (u2IA_PD_Prefix_OptionLen < u2IA_PD_OptionLen)
    {
        /*get the IA_PD prefix option */
        DHCP6_RLY_GET_2_BYTE (pu1ParsPrefixBuf, u4Offset, &u2OptionType);
        if (u2OptionType != DHCP6_RLY_IA_PD_PREFIX_OPTION)
        {
            return OSIX_FAILURE;
        }
        /*get the IA_PD_Prefix Length */
        DHCP6_RLY_GET_2_BYTE (pu1ParsPrefixBuf, u4Offset, &u2OptionLen);

        /*since 4 bytes of IA Prefix option is parsed, prefix length +4 */
        u2IA_PD_Prefix_OptionLen =
            (UINT2) (u2IA_PD_Prefix_OptionLen + u2OptionLen + 4);

        /*get the preferred and valid lifetime */
        DHCP6_RLY_GET_4_BYTE (pu1ParsPrefixBuf, u4Offset, &u4Preferredtime);

        DHCP6_RLY_GET_4_BYTE (pu1ParsPrefixBuf, u4Offset, &u4Validtime);

        /* if preferred lifetime is greater than valid lifetime,it is invalid */
        if (u4Preferredtime > u4Validtime)
        {
            DHCP6_RLY_TRC_ARG2 (DHCP6_RLY_FAILURE_TRC,
                                "D6RlParsPrefixInfo: Preferred lifetime (%d) is greater than valid lifetime (%d),it is invalid !!! \r\n",
                                u4Preferredtime, u4Validtime);
            DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "D6RlParsPrefixInfo: Preferred lifetime (%d) is greater than valid lifetime (%d),it is invalid !!! \r\n",
                                u4Preferredtime, u4Validtime));
            return OSIX_FAILURE;
        }

        /* if valid time is greater than 7 days, it is invalid */
        if ( (u4Validtime > DHCP6_RLY_MAX_VALIDTIME) && (u4Validtime != DHCP6_RLY_INFINITY_VALIDTIME))
         {
             DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                            "D6RlParsPrefixInfo: Valid lifetime is greater than 7 days!!! \r\n");
             return OSIX_FAILURE;
         }

        /* get the prefix length */
        DHCP6_RLY_GET_1_BYTE (pu1ParsPrefixBuf, u4Offset, &u1PrefixLength);

        /* if prefix length is greater than 128,it is invalid */
        if (u1PrefixLength > DHCP6_RLY_MAX_PREFIX_LENGTH)
        {
            return OSIX_FAILURE;
        }

        /* get the delegated prefix */
        DHCP6_RLY_GET_N_BYTE (pu1ParsPrefixBuf, (UINT1 *) &(Ipv6Prefix),
                              u4Offset, DHCP6_RLY_IP6_ADDR_LEN);

        /*if the delegated prefix is multicast address or unspecified address,it is invalid */
        if ((IS_ADDR_MULTI (Ipv6Prefix) == TRUE)
            || (IS_ADDR_UNSPECIFIED (Ipv6Prefix) == TRUE))
        {
            return OSIX_FAILURE;
        }

        /* check if reply message */
        if (u4MsgType == DHCP6_SERVER_REPLY)
        {
            pDhcp6RlyPDEntry = D6RlPDGetNode (&Ipv6Prefix, u1PrefixLength);

            if (pDhcp6RlyPDEntry == NULL)
            {
                /*this check has been added to avoid  readding the PDentry
                   for Deleted Prefix in renew/rebind scenario lifetimes=zero */
                if (u4Validtime != 0 && u4Preferredtime != 0)
                {
                    /*At first,add the route to RBTree,
                       followed by addition to RTM6 and then start the timer */

                    pDhcp6RlyPDEntry = D6RlPDCreateNode ();

                    if (pDhcp6RlyPDEntry == NULL)
                    {
                        return OSIX_FAILURE;
                    }

                    MEMCPY (&(pDhcp6RlyPDEntry->Prefix), &(Ipv6Prefix),
                            sizeof (tIp6Addr));
                    MEMCPY (&(pDhcp6RlyPDEntry->NextHop), pNextHop,
                            sizeof (tIp6Addr));
                    MEMCPY (&(pDhcp6RlyPDEntry->au1Pad), aTempPad,
                            sizeof (aTempPad));
                    pDhcp6RlyPDEntry->u1PrefixLen = u1PrefixLength;
                    pDhcp6RlyPDEntry->u4RtIndex = u4RouteIndex;
                    pDhcp6RlyPDEntry->u4MsgType = DHCP6_SERVER_REPLY;
                    pDhcp6RlyPDEntry->u4TransactionId = u4TxId;
                    pDhcp6RlyPDEntry->u4T1Time = u4Time1;
                    pDhcp6RlyPDEntry->u4T2Time = u4Time1;
                    pDhcp6RlyPDEntry->u2Option = 0;
                    pDhcp6RlyPDEntry->u1Flag = 0;
                    pDhcp6RlyPDEntry->u4ValidLifeTime = u4Validtime;
                    pDhcp6RlyPDEntry->u1Version = DHCP6RLY_HA_VERSION;

                    if (D6RlPDAddNode (pDhcp6RlyPDEntry) != OSIX_SUCCESS)
                    {
                        D6RlPDDelNode (pDhcp6RlyPDEntry);
                        return OSIX_FAILURE;
                    }

                    u1CommandType = NETIPV6_ADD_ROUTE;

                    if (OSIX_FAILURE ==
                        D6RlLeakPDRoute (pDhcp6RlyPDEntry, u1CommandType))
                    {
                        DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                            "D6RlParsPrefixInfo: in prefix addition flow :Unable to add route for prefix : %s ,prefix length: %d ,nexthop : %s\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop));

                        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL,
                                            DHCP6_RLY_SYSLOG_ID,
                                            "D6RlParsPrefixInfo: in prefix addition flow :Unable to add route for prefix : %s ,prefix length: %d ,nexthop : %s\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop)));
                        return OSIX_FAILURE;
                    }

                    Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                                 &(pDhcp6RlyPDEntry->
                                                   RbPrefixDelegationDbNode));
                    Dhcp6RlyRedSyncDynInfo ();

                  if(u4Validtime != DHCP6_RLY_INFINITY_VALIDTIME)
                  {
                    if (OSIX_FAILURE ==
                        D6RlTmrStart (&(pDhcp6RlyPDEntry->Dhcp6RlyTmrBlk),
                                      DHCP6RLY_VALID_TMR, u4Validtime, 0))
                    {
                        DHCP6_RLY_TRC_ARG4 (DHCP6_RLY_FAILURE_TRC,
                                            "D6RlParsPrefixInfo: in prefix addition flow :Unable to start timer for prefix : %s ,prefix length : %u,nexthop : %s ValidTimervalue : %u\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop),
                                            u4Validtime);

                        DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL,
                                            DHCP6_RLY_SYSLOG_ID,
                                            "D6RlParsPrefixInfo: in prefix addition flow :Unable to start timer for prefix : %s ,prefix length : %u,nexthop : %s ValidTimervalue : %u\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop),
                                            u4Validtime));

                        return OSIX_FAILURE;
                    }
                }
             
             }

            }
            else
            {
                /*This check is added to test, reply message is in response to renew/rebind,
                   if any prefix with txid is available for renew/rebind, with IA_PD's T1=T2=0,
                   then route should be deleted, else prefix's valid timer should
                   updated with new assigned valid lifetime */

                if ((pDhcp6RlyPDEntry->u4TransactionId == u4TxId)
                    && ((pDhcp6RlyPDEntry->u4MsgType == DHCP6_CLIENT_REBIND)
                        || (pDhcp6RlyPDEntry->u4MsgType == DHCP6_CLIENT_RENEW))
                    && u4Time1 == 0 && u4Time2 == 0)
                {
                    /*  this deletion scenario requires only prefix and prefix Length */

                    Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                                 &(pDhcp6RlyPDEntry->
                                                   RbPrefixDelegationDbNode));
                    Dhcp6RlyRedSyncDynInfo ();

                    if (OSIX_FAILURE ==
                        D6RlPDRouteDeletionPrefix (&(Ipv6Prefix),
                                                   u1PrefixLength, u4TxId,
                                                   u4Time1, u4Time2, u4MsgType,
                                                   1))
                    {
                        DHCP6_RLY_TRC_ARG4 (DHCP6_RLY_FAILURE_TRC,
                                            "D6RlParsPrefixInfo in Renew/rebind T1=T2=0 : unable to delete prefix in rtm6 and Rbtree for Msg:  %u, prefix:  %s , prefix length: %u ,nexthop : %s \r\n",
                                            u4MsgType,
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop));

                        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL,
                                            DHCP6_RLY_SYSLOG_ID,
                                            "D6RlParsPrefixInfo in Renew/rebind T1=T2=0 : unable to delete prefix in rtm6 and Rbtree for Msg:  %u, prefix:  %s , prefix length: %u ,nexthop : %s \r\n",
                                            u4MsgType,
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop)));
                        return OSIX_FAILURE;
                    }

                }

                else if (((pDhcp6RlyPDEntry->u4MsgType == DHCP6_CLIENT_REBIND)
                          || (pDhcp6RlyPDEntry->u4MsgType ==
                              DHCP6_CLIENT_RENEW)) && u4Time1 != 0
                         && u4Time2 != 0 && u4Validtime != 0
                         && u4Preferredtime != 0)
                {                /* there is no need update prefix in rbtree or rtm6, 
                                   since prefix is present already */
                    if (OSIX_FAILURE ==
                        D6RlTmrStopTimer (&(pDhcp6RlyPDEntry->Dhcp6RlyTmrBlk)))
                    {
                        DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                            "D6RlParsPrefixInfo in renew/rebind T1=T1!=0 :Unable to stop timer for prefix: %s, prefix length: %d, nexthop %s\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop));

                        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL,
                                            DHCP6_RLY_SYSLOG_ID,
                                            "D6RlParsPrefixInfo in renew/rebind T1=T1!=0 :Unable to stop timer for prefix: %s, prefix length: %d, nexthop %s\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop)));
                        return OSIX_FAILURE;
                    }

                    if (OSIX_FAILURE ==
                        D6RlTmrStart (&(pDhcp6RlyPDEntry->Dhcp6RlyTmrBlk),
                                      DHCP6RLY_VALID_TMR, u4Validtime, 0))
                    {
                        DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                            "D6RlParsPrefixInfo in renew/rebind T1=T2!=0 :Unable to start timer for prefix: %s, prefix length: %d, nexthop %s\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop));

                        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL,
                                            DHCP6_RLY_SYSLOG_ID,
                                            "D6RlParsPrefixInfo in renew/rebind T1=T2!=0 :Unable to start timer for prefix: %s, prefix length: %d, nexthop %s\r\n",
                                            Ip6PrintAddr (&Ipv6Prefix),
                                            u1PrefixLength,
                                            Ip6PrintAddr (pNextHop)));
                        return OSIX_FAILURE;
                    }

                    Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                                 &(pDhcp6RlyPDEntry->
                                                   RbPrefixDelegationDbNode));
                    Dhcp6RlyRedSyncDynInfo ();

                }
            }
        }
        /* check if client's decline message or release message,
           then those prefixes has to be removed from RBtree as well as RTM6 */
        else if (u4MsgType == DHCP6_CLIENT_DECLINE
                 || u4MsgType == DHCP6_CLIENT_RELEASE)
        {
            if (OSIX_FAILURE ==
                D6RlPDRouteDeletionPrefix (&(Ipv6Prefix), u1PrefixLength,
                                           u4TxId, u4Time1, u4Time2, u4MsgType,
                                           0))
            {
                DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                    "D6RlParsPrefixInfo in decline/release flow: unable to delete prefix in rtm6 and Rbtee for prefix: %s, prefix length: %d, nexthop %s\r\n",
                                    Ip6PrintAddr (&Ipv6Prefix), u1PrefixLength,
                                    Ip6PrintAddr (pNextHop));
                return OSIX_FAILURE;
            }

        }
        else if ((u4MsgType == DHCP6_CLIENT_RENEW)
                 || (u4MsgType == DHCP6_CLIENT_REBIND))
        {
            /*fill the transaction id for prefix and prefix Length in RBTree,
               that has been parsed above */

            if (OSIX_FAILURE ==
                D6RlPDRouteFillTxId (&(Ipv6Prefix), u1PrefixLength, u4TxId,
                                     u4MsgType))
            {
                DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                    "D6RlParsPrefixInfo : unable to fill txid for prefix :%s , prefix length %d, nexthop: %s \r\n",
                                    Ip6PrintAddr (&Ipv6Prefix), u1PrefixLength,
                                    Ip6PrintAddr (pNextHop));

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "D6RlParsPrefixInfo : unable to fill txid for prefix :%s , prefix length %d, nexthop:%s \r\n",
                                    Ip6PrintAddr (&Ipv6Prefix), u1PrefixLength,
                                    Ip6PrintAddr (pNextHop)));
                return OSIX_FAILURE;

            }
        }
    }                            /* End of while loop */
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : D6RlLeakPDRoute                                            *
*                                                                           *
* Description  : This function Provisions addition/deletion of ipv6 route   *
*                based on delegated prefix and prefix length                *
*                                                                           *
* Input        : pPDEntry - pointer to PD structure                         *
*                u1CommandType - command to add/delete route
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
D6RlLeakPDRoute (tDhcp6RlyPDEntry * pPDEntry, UINT1 u1CommandType)
{
    tNetIpv6RtInfo      PDRouteInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    MEMSET (&PDRouteInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    MEMCPY (&(PDRouteInfo.Ip6Dst), &(pPDEntry->Prefix), sizeof (tIp6Addr));
    PDRouteInfo.u1Prefixlen = pPDEntry->u1PrefixLen;
    MEMCPY (&(PDRouteInfo.NextHop), &(pPDEntry->NextHop), sizeof (tIp6Addr));
    PDRouteInfo.u4Index = pPDEntry->u4RtIndex;
    PDRouteInfo.u1Preference = IP6_PREFERENCE_NETMGMT;
    PDRouteInfo.i1Type = IP6_ROUTE_TYPE_INDIRECT;
    PDRouteInfo.u4Metric = 0;    /*for static route->metric is zero */
    PDRouteInfo.i1Proto = IP6_NETMGMT_PROTOID;
    PDRouteInfo.i1DefRtrFlag = 0;
    PDRouteInfo.u2ChgBit = IP6_BIT_ALL;
    PDRouteInfo.u4ContextId = 0;
    PDRouteInfo.i1MetricType5 = 1;

    if (NetIpv6GetIfInfo (pPDEntry->u4RtIndex, &NetIpv6IfInfo) ==
        NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /*if the client connected interface is down w.r.t relay,then route should not be added */
    if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_DOWN)
    {
        return OSIX_FAILURE;
    }

    if (u1CommandType == NETIPV6_ADD_ROUTE)
    {
        PDRouteInfo.u4RowStatus = ACTIVE;
    }
    else
    {
        PDRouteInfo.u4RowStatus = DESTROY;
    }

    if (NetIpv6LeakRoute (u1CommandType, &PDRouteInfo) == NETIPV6_FAILURE)
    {
        DHCP6_RLY_TRC_ARG5 (DHCP6_RLY_FAILURE_TRC,
                            "PDLeakRoute failed for prefix : %s, prefix length :%u , nexthop : %s, routeindex: %u,CommandType: %u\r\n",
                            Ip6PrintAddr (&pPDEntry->Prefix),
                            pPDEntry->u1PrefixLen,
                            Ip6PrintAddr (&pPDEntry->NextHop),
                            pPDEntry->u4RtIndex, u1CommandType);

        DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "PDLeakRoute failed for prefix : %s, prefix length :%u , nexthop : %s, routeindex: %u,CommandType: %u\r\n",
                            Ip6PrintAddr (&pPDEntry->Prefix),
                            pPDEntry->u1PrefixLen,
                            Ip6PrintAddr (&pPDEntry->NextHop),
                            pPDEntry->u4RtIndex, u1CommandType));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
*                                                                           *
* Function     : D6RlPDRouteDeletionTxId                                    *
*                                                                           *
* Description  : This function is called to delete the stop the timer,      *
*                Delete the ipv6 route in rtm6 and delete rbnode based      *
*                On transaction id.                                         *
*                                                                           *
* Input        : u4TxId -Transaction id between dhcpv6 messages             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
D6RlPDRouteDeletionTxId (UINT4 u4TxId)
{

    tDhcp6RlyPDEntry   *pCurrDhcp6RlyPDEntry = NULL;
    tDhcp6RlyPDEntry   *pPrevDhcp6RlyPDEntry = NULL;
    UINT1               u1CommandType = NETIPV6_DELETE_ROUTE;
    UINT1               u1Set;    /*Used as flag to get prev and curr RbNode */

    pCurrDhcp6RlyPDEntry = D6RlPDGetFirstNode ();

    while (pCurrDhcp6RlyPDEntry != NULL)
    {
        u1Set = 1;                /*at initial of loop set to flag=0 */

        pCurrDhcp6RlyPDEntry->u2Option = DHCP6_RLY_NOBINDING;

        if (pCurrDhcp6RlyPDEntry->u4TransactionId == u4TxId)
        {
            pCurrDhcp6RlyPDEntry->u2Option = DHCP6_RLY_NOBINDING;

            u1Set = 0;
        }

        /*From the Rbnode, matching transaction is found so delete */
        if (u1Set == 0)
        {
            if (OSIX_FAILURE ==
                D6RlLeakPDRoute (pCurrDhcp6RlyPDEntry, u1CommandType))
            {
                DHCP6_RLY_TRC_ARG5 (DHCP6_RLY_FAILURE_TRC,
                                    "D6RlPDRouteDeletionTxId :Unable to delete route in rtm6 for TxID: %u, prefix : %s, prefix length :%u , nexthop : %s, routeindex: %u\r\n",
                                    u4TxId,
                                    Ip6PrintAddr (&pCurrDhcp6RlyPDEntry->
                                                  Prefix),
                                    pCurrDhcp6RlyPDEntry->u1PrefixLen,
                                    Ip6PrintAddr (&pCurrDhcp6RlyPDEntry->
                                                  NextHop),
                                    pCurrDhcp6RlyPDEntry->u4RtIndex);

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "D6RlPDRouteDeletionTxId :Unable to delete route in rtm6 for TxID: %u, prefix : %s, prefix length :%u , nexthop : %s, routeindex: %u\r\n",
                                    u4TxId,
                                    Ip6PrintAddr (&pCurrDhcp6RlyPDEntry->
                                                  Prefix),
                                    pCurrDhcp6RlyPDEntry->u1PrefixLen,
                                    Ip6PrintAddr (&pCurrDhcp6RlyPDEntry->
                                                  NextHop),
                                    pCurrDhcp6RlyPDEntry->u4RtIndex));
                return OSIX_FAILURE;
            }
            D6RlPDDelNode (pCurrDhcp6RlyPDEntry);

            Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                         &(pCurrDhcp6RlyPDEntry->
                                           RbPrefixDelegationDbNode));
            Dhcp6RlyRedSyncDynInfo ();
        }
        /*matching transaction id is not found, so assign the curr Rbnode to previous Rbnode */
        else
        {
            pPrevDhcp6RlyPDEntry = pCurrDhcp6RlyPDEntry;
        }
        /*Get the next node based on prev Rbnode,Whether it is null then getfirst should be done,and if it is not null,do getnext */
        if (pPrevDhcp6RlyPDEntry == NULL)
        {
            pCurrDhcp6RlyPDEntry = D6RlPDGetFirstNode ();
        }
        else
        {
            pCurrDhcp6RlyPDEntry =
                D6RlPDGetNextNode (&(pPrevDhcp6RlyPDEntry->Prefix),
                                   pPrevDhcp6RlyPDEntry->u1PrefixLen);
        }

    }

    return OSIX_SUCCESS;

}

/****************************************************************************
*                                                                           *
* Function     : D6RlPDRouteDeletionPrefix                                  *
*                                                                           *
* Description  : This function is called to stop the timer ,delete          *
*                 the ipv6 route  in rtm6 and delete the PDNode             *
*                 for given prefix and prefix lengh                         *
*                                                                           *
* Input        :   pIpv6Prefix - delegated prefix                            *
*                  u1Prefixlength - Prefix length                           *
*                  u4TxId -Transaction id between dhcpv6 messages           * 
*                  u4T1Time - T1 time in IA_PD option                       *
*                  u4T2Time - T2 time in IA_PD option                       *
*                  u4MessageType - message type of dhcpv6                   *
*                  u1Flag - flag set to indicate ,msg is received           *
*                           in response to rebind/renew & T1=T2=0           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
D6RlPDRouteDeletionPrefix (tIp6Addr * pIpv6Prefix,
                           UINT1 u1Prefixlength,
                           UINT4 u4TransactionId,
                           UINT4 u4T1Time,
                           UINT4 u4T2Time, UINT4 u4MessageType, UINT1 u1Flag)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    UINT1               u1CommandType = NETIPV6_DELETE_ROUTE;

    /*At first get the Rbtree node, stop the timer ,
       followed by deletion of route in rtm6 and prefix in RBtree */
    pDhcp6RlyPDEntry = D6RlPDGetNode (pIpv6Prefix, u1Prefixlength);

    if (pDhcp6RlyPDEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    else
    {
        /*set before deletion for redundancy checkup */
        pDhcp6RlyPDEntry->u4MsgType = u4MessageType;
        pDhcp6RlyPDEntry->u4TransactionId = u4TransactionId;
        pDhcp6RlyPDEntry->u4T1Time = u4T1Time;
        pDhcp6RlyPDEntry->u4T2Time = u4T2Time;
        pDhcp6RlyPDEntry->u1Flag = u1Flag;

        if (OSIX_FAILURE == D6RlLeakPDRoute (pDhcp6RlyPDEntry, u1CommandType))
        {
            DHCP6_RLY_TRC_ARG4 (DHCP6_RLY_FAILURE_TRC,
                                "D6RlPDRouteDeletionPrefix :Unable to delete route for prefix : %s, prefix length :%u , nexthop : %s, routeindex: %u\r\n",
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->Prefix),
                                pDhcp6RlyPDEntry->u1PrefixLen,
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->NextHop),
                                pDhcp6RlyPDEntry->u4RtIndex);
            return OSIX_FAILURE;
        }

        D6RlPDDelNode (pDhcp6RlyPDEntry);

        Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                     &(pDhcp6RlyPDEntry->
                                       RbPrefixDelegationDbNode));
        Dhcp6RlyRedSyncDynInfo ();

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : D6RlPDRouteFillTxId                                        *
*                                                                           *
* Description  : This function is called to fill the transaction id         *
*                for given prefix and prefix length                         *
*                                                                           *
* Input        :  pIpv6Prefix - delegated prefix                            *
*                  u1Prefixlength - Prefix length                           *
*                  u4TxId -Transaction id between dhcpv6 messages           *
*                  u4MessageType - message type of dhcpv6                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
D6RlPDRouteFillTxId (tIp6Addr * pIpv6Prefix,
                     UINT1 u1Prefixlength, UINT4 u4TxId, UINT4 u4MessageType)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;

    pDhcp6RlyPDEntry = D6RlPDGetNode (pIpv6Prefix, u1Prefixlength);

    if (pDhcp6RlyPDEntry == NULL)
    {
        DHCP6_RLY_TRC_ARG4 (DHCP6_RLY_FAILURE_TRC,
                            "D6RlPDRouteFillTxId :Unable to find node in RBTree for MsgType: %u, TxID :%u ,prefix: %s , prefix length %u \r\n",
                            u4MessageType, u4TxId, Ip6PrintAddr (pIpv6Prefix),
                            u1Prefixlength);
        return OSIX_FAILURE;
    }
    else
    {
        pDhcp6RlyPDEntry->u4TransactionId = u4TxId;
        pDhcp6RlyPDEntry->u4MsgType = u4MessageType;
    }

    Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                 &(pDhcp6RlyPDEntry->RbPrefixDelegationDbNode));

    Dhcp6RlyRedSyncDynInfo ();

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : D6RlPDNodeDeletion                                         *
*                                                                           *
* Description  : This function is called to delete the RBNode,              * 
*                it is called by function in red.c                          *
*                                                                           *
* Input        : pIpv6Prefix - delegated prefix                             *
*                u1Prefixlength - delegated prefix length                   * 
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
D6RlPDNodeDeletion (tIp6Addr * pIpv6Prefix, UINT1 u1Prefixlength)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    UINT1               u1CommandType = NETIPV6_DELETE_ROUTE;
    INT4                i4RetVal = 0;

    /*get the rbtree node to delete */
    pDhcp6RlyPDEntry = D6RlPDGetNode (pIpv6Prefix, u1Prefixlength);

    if (pDhcp6RlyPDEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    else
    {
        i4RetVal = D6RlLeakPDRoute (pDhcp6RlyPDEntry, u1CommandType);
        UNUSED_PARAM(i4RetVal);
        D6RlPDDelNode (pDhcp6RlyPDEntry);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : Dhcp6RlyRouteControlDisable                                *
*                                                                           *
* Description  : This function is called to delete the RBNode,              *
*                it is called by function in red.c                          *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
Dhcp6RlyRouteControlDisable (VOID)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    tDhcp6RlyPDEntry   *pTempDhcp6RlyPDEntry = NULL;
    UINT1               u1CommandType = NETIPV6_DELETE_ROUTE;

    pDhcp6RlyPDEntry = D6RlPDGetFirstNode ();

    while (pDhcp6RlyPDEntry != NULL)
    {

        if (OSIX_FAILURE == D6RlLeakPDRoute (pDhcp6RlyPDEntry, u1CommandType))
        {
            DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                "Dhcp6RlyRedHandleActiveToStandby :Unable to delete route in rtm6 for  prefix : %s ,prefix length: %d ,nexthop : %s\r\n",
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->Prefix),
                                pDhcp6RlyPDEntry->u1PrefixLen,
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->NextHop));

            DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyRedHandleActiveToStandby :Unable to delete route in rtm6 for  prefix : %s ,prefix length: %d ,nexthop : %s\r\n",
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->Prefix),
                                pDhcp6RlyPDEntry->u1PrefixLen,
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->NextHop)));
            break;
        }

        pTempDhcp6RlyPDEntry =
            D6RlPDGetNextNode (&(pDhcp6RlyPDEntry->Prefix),
                               pDhcp6RlyPDEntry->u1PrefixLen);
        D6RlPDDelNode (pDhcp6RlyPDEntry);

        pDhcp6RlyPDEntry = pTempDhcp6RlyPDEntry;
    }                            /*End of while loop */
    return;
}
