/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlmain.c,v 1.13 2014/07/10 12:23:57 siva Exp $
 *
 * Description:This file contains the Init, Memory allocation, Task
 *             creation, Queue creation for the DHCPv6 Relay Module
 *
 *******************************************************************/

#define _GLOBAL_MODULE
#include "d6rlinc.h"
/* Private Prototypes */

PRIVATE INT4 D6RlMainInit PROTO ((VOID));
PRIVATE VOID D6RlMainInitFailure PROTO ((VOID));
PRIVATE VOID D6RlMainHandleQMsg PROTO ((VOID));

/**************************************************************************/
/*   Function Name   : Dl6RlMainTaskMain                                  */
/*   Description     : This function awaits packet arrival to the relay   */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/

PUBLIC VOID
D6RlMainTask (INT1 *pi1Param)
{

    UINT4               u4EventReceived = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pi1Param);

    /* Creation of memory pools, timerlists, semaphores */
    /*Initialization of Globals */

    i4RetVal = D6RlMainInit ();

    /* Return failure if initialization of Dhcp6 relay resources fails */
    if (i4RetVal == OSIX_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC,
                       "Unable to Initialize for Beep Data Structures \n");
        D6RlMainInitFailure ();
        DHCP6_RLY_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (Dhcp6RlyRedInitGlobalInfo () == OSIX_FAILURE)
    {
        DHCP6_RLY_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    Dhcp6RlyRedDynDataDescInit ();

    Dhcp6RlyRedDescrTblInit ();

#ifdef SNMP_2_WANTED
    RegisterFSDH6R ();
#endif

    NetIpv6RegisterHigherLayerProtocol (IP6_DHCP6_RLY_PROTOID,
                                        NETIPV6_INTERFACE_PARAMETER_CHANGE,
                                        (VOID *) D6RlApiIp6IfStatusNotify);
    DHCP6_RLY_INIT_COMPLETE (OSIX_SUCCESS);
    DHCP6_RLY_TRC (DHCP6_RLY_DATA_PATH_TRC | DHCP6_RLY_CONTROL_PLANE_TRC,
                   "Initialization success for dhcpv6 relay \n");
    DHCP6_RLY_IS_INITIALIZED = OSIX_TRUE;
    while (OSIX_TRUE)
    {
        /* Receive the event */
        u4EventReceived = 0;
        if (OsixEvtRecv (DHCP6_RLY_TASK_ID, (DHCP6_RLY_PKT_RCVD_EVENT |
                                             DHCP6_RLY_TIMER_EVENT |
                                             DHCP6_RLY_RM_PKT_EVENT |
                                             DHCP6_RLY_QMSG_EVENT),
                         DHCP6_RLY_EVENT_WAIT_FLAG,
                         &u4EventReceived) != OSIX_SUCCESS)
        {
            continue;
        }
        DHCP6_RLY_LOCK ();
        if (u4EventReceived & DHCP6_RLY_PKT_RCVD_EVENT)
        {
            D6RlParsRcvAndProcessPkt ();
            SelAddFd (DHCP6_RLY_SOCKET_FD, D6RlSktDataRcvd);
        }
        if (u4EventReceived & DHCP6_RLY_QMSG_EVENT)
        {
            D6RlMainHandleQMsg ();
        }
        if (u4EventReceived & DHCP6_RLY_TIMER_EVENT)
        {
            D6RlTmrExpHandler ();
        }
        if (u4EventReceived & DHCP6_RLY_RM_PKT_EVENT)
        {
            Dhcp6RlyRedHandleRmEvents ();
        }
        DHCP6_RLY_UNLOCK ();
    }

}

/**************************************************************************/
/*   Function Name   : D6RlMainInit                                       */
/*   Description     : This function initializes system and memory        */
/*                     resources required for Dhcpv6 Relay.               */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/
PRIVATE INT4
D6RlMainInit (VOID)
{
    INT4                i4SysLogId = 0;
    UINT1               au1Temp[DHCP6_RLY_IP6_MAX_STRLEN];

    MEMSET (au1Temp, 0, DHCP6_RLY_IP6_MAX_STRLEN);

    /* Memset Globals */
    MEMSET (&gDhcp6RlyGblInfo, 0, sizeof (tDhcp6RlyGblInfo));

    /* Set the initialized flag as false */
    DHCP6_RLY_IS_INITIALIZED = OSIX_FALSE;

    /* Getting Task Id of Self and initializing the global Data structure */
    if (OsixTskIdSelf (&DHCP6_RLY_TASK_ID) == OSIX_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC,
                       "Exiting Function BeepCltTaskMain \n");

        /* Indicate the status of initialization to the main routine */

        return OSIX_FAILURE;
    }

    /* Create semaphore for mutual exclusion */
    if (OsixCreateSem (DHCP6_RLY_TASK_SEM, DHCP6_RLY_SEM_COUNT,
                       OSIX_DEFAULT_SEM_MODE,
                       &DHCP6_RLY_SEM_ID) != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "DHCP6R mutual exclusion semaphore"
                       "creation failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create a Queue for Dhcpv6 Relay */
    if (OsixQueCrt (DHCP6_RLY_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_D6RL_RLY_QMSGS, &DHCP6_RLY_QID) != OSIX_SUCCESS)
    {
        /* Delete the semaphore created if Queue creation failed */
        if (OsixDeleteSem (CLI_PTR_TO_U4 (DHCP6_RLY_SEM_ID), DHCP6_RLY_TASK_SEM)
            != OSIX_SUCCESS)
        {
            DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                           DHCP6_RLY_OS_RESOURCE_TRC |
                           DHCP6_RLY_FAILURE_TRC,
                           "error during deletion of DHCP6R semaphore\r\n");
        }

        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "DHCP6R queue creation failed\r\n");
        return OSIX_FAILURE;
    }

    /* Initialize memory resources */
    /* Memory pool Creation for Interface table */
    if (D6rlSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Memory Pools creation failure for DHCP6R \r\n");
        return OSIX_FAILURE;
    }

    if (D6RlIfCreateTable () != OSIX_SUCCESS)
    {

        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "RBTree creation failure for " "DHCP6R interface\r\n");
        return OSIX_FAILURE;
    }

    DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC,
                   "RBTree creation successful" "for DHCP6R interface table\n");

    /* Memory Pool Creation for Server Addr table */
    if (D6RlSrvCreateTable () != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "RBTree creation failure" "for DHCP6R Serv Addr\r\n");

        return OSIX_FAILURE;
    }

    /* Memory Pool Creation for Out I/f table */
    if (D6RlOuIfCreateTable () != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "RBTree creation failure for DHCP6R Out I/f\r\n");

        return OSIX_FAILURE;
    }

    /* Memory Pool Creation for Prefix Delegation table */
    if (D6RlPDCreateEntry () != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "RBTree creation failure for DHCP6R PD Entry\r\n");

        return OSIX_FAILURE;
    }

    /* Creation for Relay PD Timer */
    if (D6RlTmrInit () != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC | DHCP6_RLY_OS_RESOURCE_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "D6clTmrInit Return Failure !!! \r\n");
        return OSIX_FAILURE;
    }

    gDhcp6RlyGblInfo.b1PDRouteControl = DHCP6_RLY_PDROUTE_ENABLE;
    DHCP6_RLY_REMOTEID_STATUS = DHCP6_RLY_REMOTEID_DISABLE;
    DHCP6_RLY_SYS_LOG_OPTION = DHCP6_RLY_DISABLE;
    DHCP6_RLY_DEBUG_TRACE = DHCP6_RLY_DEBUG_TRACE | DHCP6_RLY_CRITICAL_TRC;
    DHCP6_RLY_TRAP_CONTROL = 0;
    DHCP6_RLY_LISTEN_PORT = DHCP6_RLY_RELAY_PORT_DEF;
    DHCP6_RLY_SRV_TRANSMIT_PORT = DHCP6_RLY_SERVER_PORT_DEF;
    DHCP6_RLY_CLNT_TRANSMIT_PORT = DHCP6_RLY_CLIENT_PORT_DEF;
    DHCP6_RLY_SOCKET_FD = -1;
#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "DHCP6_RLY", SYSLOG_CRITICAL_LEVEL);

    if (i4SysLogId < 0)
    {
        return OSIX_FAILURE;
    }
    DHCP6_RLY_SYSLOG_ID = i4SysLogId;
#else
    UNUSED_PARAM (i4SysLogId);
#endif
    return OSIX_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : D6RlMainInitFailure                                  */
/*   Description     : This function deinitializes system and memory      */
/*                     resources required for Dhcpv6 Relay.               */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
PRIVATE VOID
D6RlMainInitFailure (VOID)
{

    /* Delete the semaphore created if Queue creation failed */
    if (OsixDeleteSem (CLI_PTR_TO_U4 (DHCP6_RLY_SEM_ID), DHCP6_RLY_TASK_SEM)
        != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_INIT_SHUT_TRC |
                       DHCP6_RLY_OS_RESOURCE_TRC | DHCP6_RLY_FAILURE_TRC,
                       "error during deletion of DHCP6R semaphore\r\n");
    }

    /* Delete the Queue created of DHCPv6 Relay */
    OsixQueDel (DHCP6_RLY_QID);
    D6rlSizingMemDeleteMemPools ();
    D6RlIfDeleteTable ();
    D6RlSrvDeleteTable ();
    D6RlOuIfDeleteTable ();
    D6RlPDDeleteEntry ();
    return;
}

/**************************************************************************/
/*   Function Name   : D6RlMainLock                                       */
/*   Description     : This function is used to take the Dhcpv6Rly mutual */
/*                     exclusion SEMa4 to avoid simultaneous access to    */
/*                     protocol data structures by the protocol task and  */
/*                     configuration task/thread.                         */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : SNMP_SUCCESS or SNMP_FAILURE             */
/**************************************************************************/

PUBLIC INT4
D6RlMainLock (VOID)
{
    if (OsixSemTake (DHCP6_RLY_SEM_ID) != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_OS_RESOURCE_TRC,
                       "failed to take DHCP6R mutual exclusion semaphore\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : D6RlMainUnLock                                     */
/*   Description     : This function is used to give the Dhcpv6Rly mutual */
/*                     exclusion SEMa4 to avoid simultaneous access to    */
/*                     protocol data structures by the protocol task and  */
/*                     configuration task/thread.                         */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : SNMP_SUCCESS or SNMP_FAILURE             */
/**************************************************************************/

PUBLIC INT4
D6RlMainUnLock (VOID)
{
    OsixSemGive (DHCP6_RLY_SEM_ID);
    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : D6RlMainUnLock                                     */
/*   Description     : This function is used to handle configuration      */
/*                     queue messages.                                    */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
PRIVATE VOID
D6RlMainHandleQMsg (VOID)
{
    tDhcp6RlyQMsg      *pDhcp6QMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;
    while (OsixReceiveFromQ
           ((UINT4) 0, DHCP6_RLY_Q_NAME, OSIX_NO_WAIT, (UINT4) 0,
            &pOsixMsg) == OSIX_SUCCESS)
    {
        pDhcp6QMsg = (tDhcp6RlyQMsg *) pOsixMsg;
        if (pDhcp6QMsg->i4Command == DHCP6_RLY_INTF_DELETE)
        {
            D6RlUtlDeleteIfFromAll ((UINT4) pDhcp6QMsg->i4IfIndex);
        }
        MemReleaseMemBlock (DHCP6_RLY_Q_MSG_POOL_ID, (UINT1 *) pDhcp6QMsg);
    }
    return;
}
