/* $Id: d6rlcli.c,v 1.40 2016/06/29 11:11:57 siva Exp $   */

/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $: d6rlcli.c,v 1.14 2013/05/13 10:59:02 siva Exp $
 *
 * Description: This file contains the CLI SET/GET/TEST and GET NEXT
 *              routines for the MIB objects specified in fsd6r.mib
 *
 *
 * ************************************************************************/
#ifndef _DHCP6_RLY_CLI_C
#define _DHCP6_RLY_CLI_C
#include "d6rlinc.h"
#include "fsdh6rcli.h"

UINT1               gu1Temp[DHCP6_RLY_REM_ID_MAX_LEN];
PRIVATE VOID        D6RlCliDeleteDynamicIfEntries (VOID);
PRIVATE INT4        D6RlCliCheckOutIfPresent (UINT4);
/*******************************************************************************
 *    FUNCTION NAME    : cli_process_dhcp6r_cmd                                *
 *                                                                             *
 *    DESCRIPTION      : Protocol CLI Message Handler Function to handle       *
 *                       configuration messages only.                          *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Command - Command identifier                        *
 *                       ... - Variable command argument list                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
cli_process_dhcp6r_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tIp6Addr            SrvAddr;
    UINT1               au1RemIdDuid[DHCP6_RLY_REM_ID_MAX_LEN];
    UINT1               au1RemIdUserDefined[DHCP6_RLY_REM_ID_MAX_LEN];
    UINT1              *args[DHCP6_RLY_CLI_MAX_ARGS];
    UINT1              *pu1RemIdDuid;
    UINT1               u1RemIdLen = (UINT1) 0;
    INT1                i1argno = (INT1) 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Value = 0;
    UINT4               u4Index = 0;
    UINT4               u4InterfaceId = 0;
    INT4                i4RetVal = (INT4) CLI_FAILURE;

    MEMSET (au1RemIdDuid, 0, DHCP6_RLY_REM_ID_MAX_LEN);
    MEMSET (au1RemIdUserDefined, 0, DHCP6_RLY_REM_ID_MAX_LEN);

    if (DHCP6_RLY_IS_INITIALIZED != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r%% DHCP6 Relay module is not initialised\r\n");
        return CLI_FAILURE;

    }

    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);
    DHCP6_RLY_LOCK ();

    va_start (ap, u4Command);

    /*Third Argument is always the interface Index */

    u4InterfaceId = va_arg (ap, UINT4);

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == (INT1) DHCP6_RLY_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_DHCP6_RLY_IPV6_RELAY:

            u4Index = CLI_GET_IFINDEX ();
            if (args[0] != NULL)
            {
                MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[0], &SrvAddr);
                i4RetVal = D6RlCliSetIfEntry (CliHandle, u4Index,
                                              &SrvAddr,
                                              u4InterfaceId, &u4ErrorCode);
            }
            else
            {
                i4RetVal = D6RlCliSetIfEntry (CliHandle, u4Index,
                                              NULL,
                                              u4InterfaceId, &u4ErrorCode);
            }

            break;

        case CLI_DHCP6_RLY_NO_IPV6_RELAY:

            u4Index = CLI_GET_IFINDEX ();
            if (args[0] != NULL)
            {
                MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[0], &SrvAddr);
                i4RetVal = D6RlCliDelIfEntry (CliHandle, u4Index,
                                              &SrvAddr,
                                              u4InterfaceId, &u4ErrorCode);
            }
            else
            {
                i4RetVal = D6RlCliDelIfEntry (CliHandle, u4Index,
                                              NULL,
                                              u4InterfaceId, &u4ErrorCode);
            }

            break;

        case CLI_DHCP6_RLY_HOP_THRESHOLD:

            u4Index = CLI_GET_IFINDEX ();
            MEMCPY (&u4Value, args[0], sizeof (UINT4));
            i4RetVal = D6RlCliSetHopThreshold (CliHandle,
                                               u4Index, u4Value, &u4ErrorCode);

            break;

        case CLI_DHCP6_RLY_NO_HOP_THRESHOLD:

            u4Index = CLI_GET_IFINDEX ();

            i4RetVal = D6RlCliSetHopThreshold (CliHandle, u4Index,
                                               DHCP6_RLY_HOP_THRESH_DEF,
                                               &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_CLEAR_ALL_INTERFACE_COUNTERS:
            D6RlCliClearStatistics (CliHandle, 0, &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_CLEAR_INTERFACE_COUNTERS:
            i4RetVal = D6RlCliClearStatistics (CliHandle, u4InterfaceId,
                                               &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_REMOTEID_TYPE:
            u4Value = CLI_PTR_TO_U4 (args[0]);
            u4Index = CLI_GET_IFINDEX ();
            i4RetVal = D6RlCliSetRemoteIdType (CliHandle, u4Index,
                                               u4Value, &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_RID_DUID:

            /* args[0] contains duid value */
            pu1RemIdDuid = &au1RemIdDuid[0];
            u1RemIdLen = (UINT1) OctetLen ((UINT1 *) args[0]);
            CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) args[0], pu1RemIdDuid);

            u4Index = CLI_GET_IFINDEX ();
            i4RetVal = D6RlCliSetRemoteIdDuid (CliHandle,
                                               u4Index,
                                               pu1RemIdDuid,
                                               u1RemIdLen, &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_RID_USERDEFINED:
            u4Index = CLI_GET_IFINDEX ();
            u1RemIdLen = STRLEN ((UINT1 *) args[0]);
            MEMCPY (au1RemIdUserDefined, ((UINT1 *) args[0]), u1RemIdLen);
            i4RetVal = D6RlCliSetRemoteIdUserdefined (CliHandle,
                                                      u4Index,
                                                      (UINT1 *)
                                                      &au1RemIdUserDefined,
                                                      u1RemIdLen, &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_ENABLE_TRAP:
        case CLI_DHCP6_RLY_DISABLE_TRAP:
            i4RetVal =
                D6RlCliSetTraps (CliHandle, u4Command, CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_DHCP6_RLY_SYSLOG:
            i4RetVal = D6RlCliSetSysLogStatus (CliHandle, DHCP6_RLY_ENABLE,
                                               &u4ErrorCode);
            break;
        case CLI_DHCP6_RLY_NO_SYSLOG:
            i4RetVal = D6RlCliSetSysLogStatus (CliHandle, DHCP6_RLY_DISABLE,
                                               &u4ErrorCode);
            break;
        case CLI_DHCP6_RLY_REMOTEID_STATUS:
            u4Value = CLI_PTR_TO_U4 (args[0]);
            i4RetVal = D6RlCliSetRemoteIdStatus (CliHandle, (INT4) u4Value,
                                                 &u4ErrorCode);
            break;
        case CLI_DHCP6_RLY_PD_FORWARDING:
        case CLI_DHCP6_RLY_NO_PD_FORWARDING:
            u4Value = CLI_PTR_TO_U4 (args[0]);
            i4RetVal =
                D6RlCliSetPDRouteStatus (CliHandle, (INT4) u4Value,
                                         &u4ErrorCode);
            break;
        case CLI_DHCP6_RLY_DEBUG:
        case CLI_DHCP6_RLY_NO_DEBUG:
            i4RetVal = D6RlCliSetDebugs (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_DHCP6_RLY_SHOW_INTF:

            i4RetVal = D6RlCliShowIfRelayInfo (CliHandle, u4InterfaceId);
            break;

        case CLI_DHCP6_RLY_SHOW_STATISTICS:

            i4RetVal = D6RlCliShowStatistics (CliHandle, u4InterfaceId,
                                              &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_SHOW_ALL_STATISTICS:

            i4RetVal = D6RlCliShowStatistics (CliHandle, 0, &u4ErrorCode);
            break;

        case CLI_DHCP6_RLY_UDP_SOURCE_DEST_PORT:
            i4RetVal = D6RlCliSetClntUdpPorts
                (CliHandle, CLI_PTR_TO_U4 (args[0]), CLI_PTR_TO_U4 (args[1]));
            break;
        case CLI_DHCP6_RLY_SHOW_ALL_INTF:
            i4RetVal = D6RlCliShowRelayInfo (CliHandle);
            break;

        case CLI_DHCP6_RLY_SHOW_GLOB_INTF:
            i4RetVal = D6RlCliShowGlbRelayInfo (CliHandle);
            break;

        default:
            u4ErrorCode = DHCP6_RLY_INVALID_CMD;
            i4RetVal = CLI_FAILURE;
            break;
    }
    if ((i4RetVal == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < DHCP6_RLY_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", gaDhcp6RlyCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }

    DHCP6_RLY_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return i4RetVal;
}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliSetClntUdpPorts                                *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to set the UDP Ports             *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Type   -  Port Type                                 *
 *                       u4Value - Value                                       *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/

INT4
D6RlCliSetClntUdpPorts (tCliHandle CliHandle, UINT4 u4Type, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);
    if (u4Type == DHCP6_RLY_UDP_LISTEN_PORT_NUM)
    {
        if (nmhTestv2FsDhcp6RlyListenPort (&u4ErrCode, u4Value) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsDhcp6RlyListenPort (u4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (u4Type == DHCP6_RLY_UDP_CLNT_TRANS_PORT_NUM)
    {
        if (nmhTestv2FsDhcp6RlyClientTransmitPort (&u4ErrCode, u4Value) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        nmhSetFsDhcp6RlyClientTransmitPort (u4Value);
    }
    else if (u4Type == DHCP6_RLY_UDP_SRV_TRANS_PORT_NUM)
    {
        if (nmhTestv2FsDhcp6RlyServerTransmitPort (&u4ErrCode, u4Value) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        nmhSetFsDhcp6RlyServerTransmitPort (u4Value);
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliSetIfEntry                                     *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to set the interface routine     *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Index   - Command identifier                        *
 *                       pSrvAddr  - Server Address                            *
 *                       u4OutIf   - Outgoing Interface                        *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/
PUBLIC INT4
D6RlCliSetIfEntry (tCliHandle CliHandle, UINT4 u4Index, tIp6Addr * pSrvAddr,
                   UINT4 u4OutIf, UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4ContextId = 0;
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvAddr;
    UINT1               au1Ip6Addr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               u1IfEntrySet = OSIX_FALSE;
    UINT1               u1OutIfEntrySet = OSIX_FALSE;
    UINT1               u1SrvAddrEntrySet = OSIX_FALSE;

    UNUSED_PARAM (CliHandle);

    /* Check whther the server is given and Out Interface, 
     * Fetch the Out Interface with the IPV6 address */
    if((pSrvAddr != NULL) && (u4OutIf==0))
    {
        VcmGetContextIdFromCfaIfIndex (u4Index, &u4ContextId);
    }
    if (nmhValidateIndexInstanceFsDhcp6RlyIfTable (u4Index) == SNMP_FAILURE)
    {
        /* Create Interface Entry */
        if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode,
                                            u4Index,
                                            CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4RetVal = nmhSetFsDhcp6RlyIfRowStatus (u4Index, CREATE_AND_WAIT);

        if (i4RetVal == SNMP_FAILURE)
        {
            *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
            return CLI_FAILURE;
        }
        u1IfEntrySet = OSIX_TRUE;
        /* If Out != NULL do the above for OutIf */
    }
  
    /* Add Static Flag to the u4Index */
    if (nmhTestv2FsDhcp6RlyIfRelayState (pu4ErrorCode, (INT4) u4Index, DHCP6_RLY_STATIC) == SNMP_SUCCESS)
    {
        if (nmhSetFsDhcp6RlyIfRelayState((INT4) u4Index, DHCP6_RLY_STATIC) != SNMP_SUCCESS)
        {
           *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    /* Create a Row for the Out Interface */
    if(u4OutIf != 0)
    {
        if (nmhValidateIndexInstanceFsDhcp6RlyIfTable ((INT4) u4OutIf) == SNMP_FAILURE)
        {
            /* Create Interface Entry */
            if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode,
                                                (INT4) u4OutIf,
                                                CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            i4RetVal = nmhSetFsDhcp6RlyIfRowStatus (u4OutIf, CREATE_AND_WAIT);

            if (i4RetVal == SNMP_FAILURE)
            {
                *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
                return CLI_FAILURE;
            }
            u1OutIfEntrySet = OSIX_TRUE;
        }
        /* Add Dynamic flag to  the u4OutIf */
        if (nmhTestv2FsDhcp6RlyIfRelayState (pu4ErrorCode, (INT4) u4OutIf, DHCP6_RLY_DYNAMIC) == SNMP_SUCCESS)
        {
           if (nmhSetFsDhcp6RlyIfRelayState((INT4) u4OutIf, DHCP6_RLY_DYNAMIC) != SNMP_SUCCESS)
           {
              *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
               return CLI_FAILURE;
           }
        } 
	    else
	    {
	       return CLI_FAILURE;
        }
    }

    if (pSrvAddr != NULL)
    {
        MEMSET (au1Ip6Addr, 0, DHCP6_RLY_IP6_ADDR_LEN);
        MEMSET (&Dhcp6SrvAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        Dhcp6SrvAddr.pu1_OctetList = au1Ip6Addr;

        MEMCPY (Dhcp6SrvAddr.pu1_OctetList, (UINT1 *) pSrvAddr,
                DHCP6_RLY_IP6_ADDR_LEN);
        Dhcp6SrvAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;
        i4RetVal = nmhValidateIndexInstanceFsDhcp6RlySrvAddressTable
            ((INT4) u4Index, &Dhcp6SrvAddr);

        if (i4RetVal == SNMP_FAILURE)
        {
            i4RetVal =
                nmhTestv2FsDhcp6RlySrvAddressRowStatus (pu4ErrorCode,
                                                        (INT4) u4Index,
                                                        &Dhcp6SrvAddr,
                                                        CREATE_AND_WAIT);
            if (i4RetVal == SNMP_FAILURE)
            {
                if (u1IfEntrySet == OSIX_TRUE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (u4Index, DESTROY);
                }
                return CLI_FAILURE;
            }
            /* Create Server Addr Entry */
            i4RetVal = nmhSetFsDhcp6RlySrvAddressRowStatus ((INT4) u4Index,
                                                            &Dhcp6SrvAddr,
                                                            CREATE_AND_WAIT);
            if (i4RetVal == SNMP_FAILURE)
            {
                if (u1IfEntrySet == OSIX_TRUE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (u4Index, DESTROY);
                }
                *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
                return CLI_FAILURE;
            }

            if (u1IfEntrySet != OSIX_TRUE)
            {
                nmhSetFsDhcp6RlyIfRowStatus (u4Index, ACTIVE);
            }

            u1SrvAddrEntrySet = OSIX_TRUE;
        }
    }
    else
    {
        /*Make the Out Interface Row as Active */
        if (u1IfEntrySet == OSIX_TRUE)
        {
            nmhSetFsDhcp6RlyIfRowStatus (u4Index, ACTIVE);
        }
        return CLI_SUCCESS;
    }

    if (u4OutIf == 0)
    {
        if (u1IfEntrySet == OSIX_TRUE)
        {
            nmhSetFsDhcp6RlyIfRowStatus (u4Index, ACTIVE);
        }

        if (u1SrvAddrEntrySet == OSIX_TRUE)
        {
            nmhSetFsDhcp6RlySrvAddressRowStatus ((INT4) u4Index,
                                                 &Dhcp6SrvAddr, ACTIVE);
        }
        return CLI_SUCCESS;
    }
    else
    {
        if (u1OutIfEntrySet == OSIX_TRUE)
        {
            nmhSetFsDhcp6RlyIfRowStatus (u4OutIf, ACTIVE);
        }
    }
    i4RetVal = nmhValidateIndexInstanceFsDhcp6RlyOutIfTable (u4Index,
                                                             &Dhcp6SrvAddr,
                                                             (INT4) u4OutIf);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4RetVal = nmhTestv2FsDhcp6RlyOutIfRowStatus (pu4ErrorCode,
                                                      u4Index,
                                                      &Dhcp6SrvAddr,
                                                      (INT4) u4OutIf, CREATE_AND_WAIT);
        if (i4RetVal == SNMP_SUCCESS)
        {
            /* Create out If entry */
            i4RetVal = nmhSetFsDhcp6RlyOutIfRowStatus (u4Index,
                                                       &Dhcp6SrvAddr,
                                                       u4OutIf,
                                                       CREATE_AND_WAIT);
            if (i4RetVal == SNMP_FAILURE)
            {
                if (u1IfEntrySet == OSIX_TRUE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (u4Index, DESTROY);
                }
                if (u1SrvAddrEntrySet == OSIX_TRUE)
                {
                    nmhSetFsDhcp6RlySrvAddressRowStatus ((INT4) u4Index,
                                                         &Dhcp6SrvAddr,
                                                         DESTROY);
                }
                *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
                return CLI_FAILURE;
            }
            else
            {
                if (u1IfEntrySet == OSIX_TRUE)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (u4Index, ACTIVE);
                }

                if (u1SrvAddrEntrySet == OSIX_TRUE)
                {
                    nmhSetFsDhcp6RlySrvAddressRowStatus ((INT4) u4Index,
                                                         &Dhcp6SrvAddr, ACTIVE);
                }
                nmhSetFsDhcp6RlyOutIfRowStatus (u4Index,
                                                &Dhcp6SrvAddr, u4OutIf, ACTIVE);
            }
        }

    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliDelIfEntry                                     *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to del the interface conf        *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Index   - Command identifier                        *
 *                       pSrvAddr  - Server Address                            *
 *                       u4OutIf   - Outgoing Interface                        *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/
PUBLIC INT4
D6RlCliDelIfEntry (tCliHandle CliHandle, UINT4 u4Index, tIp6Addr * pSrvAddr,
                   UINT4 u4OutIf, UINT4 *pu4ErrorCode)
{
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvAddr;
    tSNMP_OCTET_STRING_TYPE Dhcp6SrvNextAddr;
    INT4                i4NextIndex = 0;
    INT4                i4NextOutIndex = 0;
    INT4                i4Flag = 0;
    UINT1               au1Ip6Addr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1Ip6NextAddr[DHCP6_RLY_IP6_ADDR_LEN];
    tIp6Addr            Ipv6DestAddr;
    tDhcp6RlyIfInfo    *pIfInfo = NULL;

    UNUSED_PARAM (CliHandle);
    MEMSET (&Dhcp6SrvAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1Ip6NextAddr, 0, DHCP6_RLY_IP6_ADDR_LEN);
    MEMSET (&Dhcp6SrvNextAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Ipv6DestAddr, 0, sizeof (tIp6Addr));
    Dhcp6SrvNextAddr.pu1_OctetList = au1Ip6NextAddr;
    if (pSrvAddr != NULL)
    {
        MEMSET (au1Ip6Addr, 0, DHCP6_RLY_IP6_ADDR_LEN);
        Dhcp6SrvAddr.pu1_OctetList = au1Ip6Addr;

        MEMCPY (Dhcp6SrvAddr.pu1_OctetList, (UINT1 *) pSrvAddr,
                DHCP6_RLY_IP6_ADDR_LEN);
        Dhcp6SrvAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;
    }

    if ((u4OutIf == 0) && (pSrvAddr == NULL) && (u4Index != 0))
    {
        if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode,
                                            (INT4) u4Index, DESTROY) == SNMP_SUCCESS)
        {
            /* Delete the Static flag to the u4Index */
            pIfInfo = D6RlIfGetNode (u4Index);
            if (NULL != pIfInfo)
            {
                nmhGetFsDhcp6RlyIfRelayState((INT4) u4Index, &i4Flag);
                pIfInfo->u1Flag = i4Flag & (~DHCP6_RLY_STATIC);
                if (pIfInfo->u1Flag == 0)
                {
                    nmhSetFsDhcp6RlyIfRowStatus (u4Index, DESTROY);
					/* Flush all the dynamically created entries */
					D6RlCliDeleteDynamicIfEntries ();
                }
            }
            return CLI_SUCCESS;
        }
        else
        {
            return CLI_FAILURE;
        }

    }
    if ((u4OutIf == 0) && (pSrvAddr != NULL) && (u4Index != 0))
    {
        while (nmhGetNextIndexFsDhcp6RlyOutIfTable (u4Index, &i4NextIndex,
                                                    &Dhcp6SrvAddr,
                                                    &Dhcp6SrvNextAddr,
                                                    (INT4) u4OutIf, &i4NextOutIndex)
               == SNMP_SUCCESS)
        {
            if ((u4Index == (UINT4) i4NextIndex)
                &&
                (MEMCMP (Dhcp6SrvNextAddr.pu1_OctetList,
                         Dhcp6SrvAddr.pu1_OctetList,
                         DHCP6_RLY_IP6_ADDR_LEN) == 0))
            {
                if (nmhTestv2FsDhcp6RlyOutIfRowStatus (pu4ErrorCode, u4Index,
                                                       &Dhcp6SrvNextAddr,
                                                       i4NextOutIndex,
                                                       DESTROY) == SNMP_SUCCESS)
                {
                    nmhSetFsDhcp6RlyOutIfRowStatus ((INT4) u4Index,
                                                    &Dhcp6SrvNextAddr,
                                                    i4NextOutIndex, DESTROY);
                }
                else
                {
                    *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
                    return CLI_FAILURE;
                }
                MEMCPY (Dhcp6SrvAddr.pu1_OctetList,
                        Dhcp6SrvNextAddr.pu1_OctetList, DHCP6_RLY_IP6_ADDR_LEN);
                u4OutIf = (UINT4) i4NextOutIndex;
            }
            else
            {
                break;
            }
        }
        if (nmhTestv2FsDhcp6RlySrvAddressRowStatus (pu4ErrorCode,
                                                    (INT4) u4Index,
                                                    &Dhcp6SrvAddr,
                                                    DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsDhcp6RlySrvAddressRowStatus ((INT4) u4Index,
                                                 &Dhcp6SrvAddr, DESTROY);
        }
        if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode,
                    (INT4)u4Index, DESTROY) == SNMP_SUCCESS)
        {
            /* Delete the Static flag to the u4Index */
            pIfInfo = D6RlIfGetNode (u4Index);
            if (NULL != pIfInfo)
            {
                nmhGetFsDhcp6RlyIfRelayState((INT4) u4Index, &i4Flag);
                pIfInfo->u1Flag = (UINT1)(i4Flag & (~DHCP6_RLY_STATIC));
                if( pIfInfo->u1Flag == 0)
                {
                    nmhSetFsDhcp6RlyIfRowStatus ((INT4) u4Index, DESTROY);
                    /* Flush all the dynamically created entries */
                    D6RlCliDeleteDynamicIfEntries ();

                }
            }
            return CLI_SUCCESS;
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    if ((u4Index != 0) && (pSrvAddr != NULL) && (u4OutIf != 0))
    {
        if (nmhTestv2FsDhcp6RlyOutIfRowStatus (pu4ErrorCode,
                                               u4Index,
                                               &Dhcp6SrvAddr,
                                               u4OutIf,
                                               DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsDhcp6RlyOutIfRowStatus ((INT4) u4Index,
                                            &Dhcp6SrvAddr, u4OutIf, DESTROY);
        }
        if (nmhTestv2FsDhcp6RlySrvAddressRowStatus (pu4ErrorCode,
                                                    (INT4) u4Index,
                                                    &Dhcp6SrvAddr,
                                                    DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsDhcp6RlySrvAddressRowStatus ((INT4) u4Index,
                                                 &Dhcp6SrvAddr, DESTROY);
        }
        if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode,
                    u4Index, DESTROY) == SNMP_SUCCESS)
        {
            /* Delete the Static flag to the u4Index */
            pIfInfo = D6RlIfGetNode (u4Index);
            if (NULL != pIfInfo)
            {
                nmhGetFsDhcp6RlyIfRelayState((INT4) u4Index, &i4Flag);
                pIfInfo->u1Flag = i4Flag & (~DHCP6_RLY_STATIC);
                if( pIfInfo->u1Flag == 0)
                {
                    nmhSetFsDhcp6RlyIfRowStatus ((INT4) u4Index, DESTROY);
                    /* Flush all the dynamically created entries */
                    D6RlCliDeleteDynamicIfEntries ();
                }
            }
            return CLI_SUCCESS;
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    return CLI_FAILURE;
}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliSetHopThreshold                                *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to set the hop threshold         *
 *                       for the given interface                               *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Index   - Command identifier                        *
 *                       pSrvAddr  - Server Address                            *
 *                       u4OutIf   - Outgoing Interface                        *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
D6RlCliSetHopThreshold (tCliHandle CliHandle, UINT4 u4Index,
                        UINT4 u4Value, UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = CLI_FAILURE;
    UNUSED_PARAM (CliHandle);

    i4RetVal = nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode, u4Index,
                                               NOT_IN_SERVICE);
    if (i4RetVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = DHCP6_RLY_NOT_ENABLED;
        return CLI_FAILURE;
    }

    nmhSetFsDhcp6RlyIfRowStatus (u4Index, NOT_IN_SERVICE);

    i4RetVal = nmhTestv2FsDhcp6RlyIfHopThreshold (pu4ErrorCode, u4Index,
                                                  u4Value);
    if (i4RetVal == SNMP_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }
    else
    {
        if (nmhSetFsDhcp6RlyIfHopThreshold (u4Index, u4Value) == SNMP_FAILURE)
        {
            *pu4ErrorCode = DHCP6_RLY_UNABLE_TO_SET;
            i4RetVal = CLI_FAILURE;
        }
        else
        {
            i4RetVal = CLI_SUCCESS;
        }
    }
    nmhSetFsDhcp6RlyIfRowStatus (u4Index, ACTIVE);
    return i4RetVal;
}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliSetHopThreshold                                *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to set the hop threshold         *
 *                       for the given interface                               *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Index   - Command identifier                        *
 *                       pSrvAddr  - Server Address                            *
 *                       u4OutIf   - Outgoing Interface                        *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
D6RlCliClearStatistics (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4NextIndex = 0;
    INT4                i4Index = u4IfIndex;
    UNUSED_PARAM (CliHandle);

    if (u4IfIndex != 0)
    {
        i4RetVal = nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode, i4Index,
                                                   NOT_IN_SERVICE);
        if (i4RetVal == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        nmhSetFsDhcp6RlyIfRowStatus (i4Index, NOT_IN_SERVICE);

        nmhSetFsDhcp6RlyIfCounterRest (i4Index, (INT4) OSIX_TRUE);

        nmhSetFsDhcp6RlyIfRowStatus (i4Index, ACTIVE);

        return CLI_SUCCESS;
    }

    if (SNMP_SUCCESS != nmhGetFirstIndexFsDhcp6RlyIfTable (&i4Index))
    {
        return CLI_FAILURE;
    }

    i4NextIndex = i4Index;

    do
    {
        i4Index = i4NextIndex;
        i4RetVal = nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode, i4Index,
                                                   NOT_IN_SERVICE);
        if (i4RetVal == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        nmhSetFsDhcp6RlyIfRowStatus (i4Index, NOT_IN_SERVICE);
        nmhSetFsDhcp6RlyIfCounterRest (i4Index, (INT4) OSIX_TRUE);
        nmhSetFsDhcp6RlyIfRowStatus (i4Index, ACTIVE);

    }
    while (nmhGetNextIndexFsDhcp6RlyIfTable (i4Index, &i4NextIndex)
           != SNMP_FAILURE);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6RlCliSetRemoteIdType                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the type of Remote-Id Option      */
/*                        on the specified interface For DHCP6c Relay.       */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        u4IfIndex - Interface Index                        */
/*                        u4RemoteIdType - Remote-Id Type                    */
/*                        pu4ErrorCode - Pointer to error code                */
/*                                                                             */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

PUBLIC INT4
D6RlCliSetRemoteIdType (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 u4RemoteIdType, UINT4 *pu4ErrorCode)
{
    INT4                i4RowStatus = 0;

    /* DHCPv6 Relay RowStatus verification */
    if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode, u4IfIndex,
                                        NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        *pu4ErrorCode = DHCP6_RLY_NOT_ENABLED;
        return CLI_FAILURE;
    }

    /* Get the current RowStatus */
    if (nmhGetFsDhcp6RlyIfRowStatus (u4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* To Set the value RowStatus should be Not-in-service state */
    if (nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Test the value before set */
    if (nmhTestv2FsDhcp6RlyIfRemoteIdOption (pu4ErrorCode, u4IfIndex,
                                             (INT4) u4RemoteIdType)
        == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        return CLI_FAILURE;
    }

    /* Set the Remote-Id option type */
    if (nmhSetFsDhcp6RlyIfRemoteIdOption (u4IfIndex,
                                          (INT4) u4RemoteIdType)
        == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Active the RowStatus */
    if (nmhTestv2FsDhcp6RlyIfRowStatus
        (pu4ErrorCode, u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6RlCliSetRemoteIdDuid                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Dhcp Unique id value which  */
/*                          is used as remote-id.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Context ID                           */
/*                        u4IfIndex - Interface index                          */
/*                        i4RemIdDuid - The DUID value                       */
/*                          u1RemIdLen - Length of Duid                     */
/*                          pu4ErrorCode - Pointer to error code                 */
/*                                                                           */
/*     OUTPUT           : NONE                                                 */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6RlCliSetRemoteIdDuid (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT1 *pu1RemIdDuid, UINT1 u1RemIdLen,
                        UINT4 *pu4ErrorCode)
{

    INT4                i4RowStatus = 0;
    UINT1               au1RemIdDuid[DHCP6_RLY_REM_ID_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE sOctetStrRemIdDuid;

    MEMSET (au1RemIdDuid, 0, DHCP6_RLY_REM_ID_MAX_LEN);
    MEMSET (&sOctetStrRemIdDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    sOctetStrRemIdDuid.pu1_OctetList = &au1RemIdDuid[0];
    sOctetStrRemIdDuid.i4_Length = u1RemIdLen;

    MEMCPY (sOctetStrRemIdDuid.pu1_OctetList, pu1RemIdDuid,
            sOctetStrRemIdDuid.i4_Length);

    /* Check the RowStatus for the Dhcp6RlyIfTable */
    if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode, u4IfIndex,
                                        NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        *pu4ErrorCode = DHCP6_RLY_NOT_ENABLED;
        return CLI_FAILURE;
    }

    /* Get the current value of RowStatus */
    if (nmhGetFsDhcp6RlyIfRowStatus (u4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Set the Remote-Id DUID value */
    if (nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6RlyIfRemoteIdDUID (pu4ErrorCode, u4IfIndex,
                                           &sOctetStrRemIdDuid) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6RlyIfRemoteIdDUID (u4IfIndex, &sOctetStrRemIdDuid)
        == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Activate the RowStatus */
    if (nmhTestv2FsDhcp6RlyIfRowStatus
        (pu4ErrorCode, u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6RlCliSetRemoteIdUserdefined                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Dhcp unique ascii value which  */
/*                          is used as remote-id.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Context ID                           */
/*                        u4IfIndex - Interface index                          */
/*                          pu4ErrorCode - Pointer to error code                 */
/*                                                                           */
/*     OUTPUT           : NONE                                                 */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
D6RlCliSetRemoteIdUserdefined (tCliHandle CliHandle, UINT4 u4IfIndex,
                               UINT1 *pu1RemIdUserDefined,
                               UINT1 u1RemIdUserDefinedLen, UINT4 *pu4ErrorCode)
{

    INT4                i4RowStatus = 0;
    UINT1               au1RemIdUserDefined[DHCP6_RLY_REM_ID_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE sOctetStrRemIdUserDefined;

    MEMSET (au1RemIdUserDefined, 0, DHCP6_RLY_REM_ID_MAX_LEN);
    MEMSET (&sOctetStrRemIdUserDefined, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    sOctetStrRemIdUserDefined.pu1_OctetList = &au1RemIdUserDefined[0];
    sOctetStrRemIdUserDefined.i4_Length = u1RemIdUserDefinedLen;

    MEMCPY (sOctetStrRemIdUserDefined.pu1_OctetList, pu1RemIdUserDefined,
            sOctetStrRemIdUserDefined.i4_Length);

    /* Check the RowStatus for the Dhcp6RlyIfTable */
    if (nmhTestv2FsDhcp6RlyIfRowStatus (pu4ErrorCode, u4IfIndex,
                                        NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        *pu4ErrorCode = DHCP6_RLY_NOT_ENABLED;
        return CLI_FAILURE;
    }

    /* Get the current value of RowStatus */
    if (nmhGetFsDhcp6RlyIfRowStatus (u4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Set the Remote-Id userdefined ascii value */
    if (nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined (pu4ErrorCode, u4IfIndex,
                                                  &sOctetStrRemIdUserDefined) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6RlyIfRemoteIdUserDefined
        (u4IfIndex, &sOctetStrRemIdUserDefined) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Activate the RowStatus */
    if (nmhTestv2FsDhcp6RlyIfRowStatus
        (pu4ErrorCode, u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6RlyIfRowStatus (u4IfIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliShowInterface                                  *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to show interface configuration  *
 *                       for the given interface or all interfaces             *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Index   - Command identifier                        *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE                               *
 *                                                                             *
 ******************************************************************************/
PUBLIC INT4
D6RlCliShowInterface (tCliHandle CliHandle, UINT4 u4Index)
{
    INT4                i4Index = u4Index;
    INT4                i4NextIndex = 0;
    INT4                i4HopThresh = 0;
    INT4                i4RemoteIdType = 0;
    INT4                i4RetVal = 0;
    INT4                i4OutIndex = 0;
    INT4                i4Value = 0;
    INT4                i4NextOutIndex = 0;
    UINT1               u1Count = 0;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               au1IpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1NextIpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1NextIfIpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1IfName[DHCP6_RLY_MAX_IF_NAME_LEN];
    UINT1               au1RemIdOptVal[DHCP6_RLY_CLI_STR_LEN];
    UINT1               u1Value = OSIX_FALSE;
    tSNMP_OCTET_STRING_TYPE ServAddr;
    tSNMP_OCTET_STRING_TYPE NextServAddr;
    tSNMP_OCTET_STRING_TYPE NextIfServAddr;
    tSNMP_OCTET_STRING_TYPE sOctetStrRemIdOptVal;
    tIp6Addr            AllSrvAddr;
    tIp6Addr            AllRlyAndSrvAddr;

    INT1                ai1IfName[DHCP6_RLY_MAX_IF_NAME_LEN];

    MEMSET (gu1Temp, 0, DHCP6_RLY_REM_ID_MAX_LEN);
    MEMSET (au1RemIdOptVal, 0, DHCP6_RLY_CLI_STR_LEN);
    MEMSET (ai1IfName, 0, DHCP6_RLY_MAX_IF_NAME_LEN);
    MEMSET (&ServAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextServAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextIfServAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddr, 0, DHCP6_RLY_IP6_ADDR_LEN);
    MEMSET (au1NextIpAddr, 0, DHCP6_RLY_IP6_ADDR_LEN);
    MEMSET (au1NextIfIpAddr, 0, DHCP6_RLY_IP6_ADDR_LEN);
    MEMSET (&AllSrvAddr, 0, sizeof (tIp6Addr));
    MEMSET (&AllRlyAndSrvAddr, 0, sizeof (tIp6Addr));
    MEMSET (&sOctetStrRemIdOptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    sOctetStrRemIdOptVal.pu1_OctetList = &gu1Temp[0];
    sOctetStrRemIdOptVal.i4_Length = DHCP6_RLY_REM_ID_MAX_LEN;
    ServAddr.pu1_OctetList = au1IpAddr;
    NextServAddr.pu1_OctetList = au1NextIpAddr;
    NextIfServAddr.pu1_OctetList = au1NextIfIpAddr;
    ServAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;
    NextServAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;
    NextIfServAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;

    if (nmhValidateIndexInstanceFsDhcp6RlyIfTable (i4Index) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    else
    {
        if (D6RlUtlCliConfGetIfName (u4Index, ai1IfName) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "%s is in relay mode\r\n", ai1IfName);
    }
    DHCP6_RLY_SET_ALL_SRV_ADDRESS (AllSrvAddr);
    DHCP6_RLY_SET_ALL_RLY_SRV_ADDRESS (AllRlyAndSrvAddr);
    nmhGetFsDhcp6RlyIfHopThreshold (u4Index, &i4HopThresh);
    nmhGetFsDhcp6RlyIfRemoteIdOption (u4Index, &i4RemoteIdType);
    nmhGetFsDhcp6RlyIfRemoteIdOptionValue (u4Index, &sOctetStrRemIdOptVal);

    CliPrintf (CliHandle, "  HopThreshold value : %d \r\n", i4HopThresh);

    if (i4RemoteIdType == DHCP6_RLY_REMOTEID_DUID)
    {
        CliOctetToStr (sOctetStrRemIdOptVal.pu1_OctetList,
                       sOctetStrRemIdOptVal.i4_Length, au1RemIdOptVal,
                       DHCP6_RLY_CLI_STR_LEN);
        CliPrintf (CliHandle, "  Remote-Id Option Type  : duid\r\n");

    }
    else if (i4RemoteIdType == DHCP6_RLY_REMOTEID_SW_NAME)
    {
        MEMCPY (au1RemIdOptVal, sOctetStrRemIdOptVal.pu1_OctetList,
                sOctetStrRemIdOptVal.i4_Length);
        CliPrintf (CliHandle, "  Remote-Id Option Type  : switch-name\r\n");
    }
    else if (i4RemoteIdType == DHCP6_RLY_REMOTEID_MGMT_IP)
    {
        MEMCPY (au1RemIdOptVal, sOctetStrRemIdOptVal.pu1_OctetList,
                sOctetStrRemIdOptVal.i4_Length);
        CliPrintf (CliHandle, "  Remote-Id Option Type  : mgmt-ip\r\n");
    }
    else if (i4RemoteIdType == DHCP6_RLY_REMOTEID_USERDEFINED)
    {
        MEMCPY (au1RemIdOptVal, sOctetStrRemIdOptVal.pu1_OctetList,
                sOctetStrRemIdOptVal.i4_Length);
        CliPrintf (CliHandle, "  Remote-Id Option Type  : userDefined\r\n");
    }

    CliPrintf (CliHandle, "  Remote-Id Option Value : %s \r\n", au1RemIdOptVal);
    CliPrintf (CliHandle, "  Server Address     : ");

    i4RetVal =
        nmhGetNextIndexFsDhcp6RlySrvAddressTable (i4Index, &i4NextIndex,
                                                  &ServAddr, &NextServAddr);

    if (i4RetVal == SNMP_SUCCESS)
    {
        if (MEMCMP (&AllSrvAddr, au1NextIpAddr, sizeof (tIp6Addr)) == 0)
        {
            CliPrintf (CliHandle, "multicast to servers only\r\n");
            u1Value = OSIX_TRUE;
        }
        else if (MEMCMP (&AllRlyAndSrvAddr, au1NextIpAddr
                         /*&ServAddr.pu1_OctetList */ , sizeof (tIp6Addr)) == 0)
        {
            CliPrintf (CliHandle, "multicast to relays and servers\r\n");
            u1Value = OSIX_TRUE;
        }
        else
        {
            CliPrintf (CliHandle, "unicast to configured servers only\r\n");
            u1Value = OSIX_TRUE;
        }			
		while ((i4RetVal == SNMP_SUCCESS) && i4Index == i4NextIndex)
		{
			CliPrintf (CliHandle, "    %s : ", Ip6PrintAddr ((tIp6Addr *)
						(VOID *)
						au1NextIpAddr));
			i4OutIndex = 0;
			i4Value = nmhGetNextIndexFsDhcp6RlyOutIfTable
				(i4Index, &i4NextIndex, &NextServAddr, &NextIfServAddr,
				 i4OutIndex, &i4NextOutIndex);
			u1Count = 0;
			u1Flag = OSIX_FALSE;
			while ((i4Value == SNMP_SUCCESS) && (i4Index == i4NextIndex) &&
					(MEMCMP
					 (au1NextIpAddr, au1NextIfIpAddr,
					  DHCP6_RLY_IP6_ADDR_LEN) == 0))
			{
				if (D6RlUtlGetIfName (i4NextOutIndex, au1IfName) ==
						OSIX_SUCCESS)
				{
					if (u1Count % 6 == 0)
					{
						CliPrintf (CliHandle, "\n     ");
						u1Flag = OSIX_FALSE;
					}
					if (u1Flag == OSIX_TRUE)
					{
						CliPrintf (CliHandle, ", ");
					}
					CliPrintf (CliHandle, "%s\t", au1IfName);
					u1Flag = OSIX_TRUE;
					u1Count = (UINT1) (u1Count + 1);
				}
				i4Index = i4NextIndex;
				MEMCPY
					(au1NextIpAddr, au1NextIfIpAddr,
					 DHCP6_RLY_IP6_ADDR_LEN);
				i4OutIndex = i4NextOutIndex;
				i4Value = nmhGetNextIndexFsDhcp6RlyOutIfTable
					(i4Index, &i4NextIndex, &NextServAddr, &NextIfServAddr,
					 i4OutIndex, &i4NextOutIndex);
			}
			CliPrintf (CliHandle, "\r\n");
			i4Index = i4NextIndex;
			MEMCPY (au1IpAddr, au1NextIpAddr, DHCP6_RLY_IP6_ADDR_LEN);
			i4RetVal = nmhGetNextIndexFsDhcp6RlySrvAddressTable
				(i4Index, &i4NextIndex, &ServAddr, &NextServAddr);

		}
    }
    if (u1Value == OSIX_FALSE)
    {
        CliPrintf (CliHandle, "-\r\n");
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *    FUNCTION NAME    : D6RlCliShowStatistics                                 *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to print relay stats of the      *
 *                       of the interface in relay mode.                       *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       pAddr   - Pointer to Address                          *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE/DHCP6_RLY_NO_ENTRY            *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
D6RlCliShowStatistics (tCliHandle CliHandle, UINT4 u4Index, UINT4 *pu4ErrorCode)
{
    INT4                i4NextIndex = 0;
    INT4                i4Index = u4Index;

    if (i4Index != 0)
    {
        if (D6RlCliPrintStatistics (CliHandle, i4Index, pu4ErrorCode) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if ((nmhGetFirstIndexFsDhcp6RlyIfTable (&i4Index)) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    i4NextIndex = i4Index;

    do
    {
        i4Index = i4NextIndex;

        if (D6RlCliPrintStatistics (CliHandle, (UINT4) i4Index, pu4ErrorCode)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    while (nmhGetNextIndexFsDhcp6RlyIfTable (i4Index, &i4NextIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*******************************************************************************
 *                                                                             *
 *    FUNCTION NAME    : D6RlCliPrintStatistics                                *
 *                                                                             *
 *    DESCRIPTION      : This routine is used to print relay stats of the      *
 *                       of the interface in relay mode.                       *
 *                                                                             *
 *    INPUT            : CliHandle - CLI Handle Value                          *
 *                       u4Index - Interface Id                                *
 *                       pu4ErrorCode - Pointer to error code                  *
 *                                                                             *
 *    OUTPUT           : None                                                  *
 *                                                                             *
 *    RETURNS          : CLI_SUCCESS/CLI_FAILURE/DHCP6_RLY_NO_ENTRY            *
 *                                                                             *
 ******************************************************************************/

PUBLIC INT4
D6RlCliPrintStatistics (tCliHandle CliHandle, UINT4 u4Index,
                        UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = 0;
    UINT4               u4Count = 0;
    INT1                ai1IfName[DHCP6_RLY_MAX_IF_NAME_LEN];
    tDhcp6RlyIfInfo    *pInterface = NULL;

    MEMSET (ai1IfName, 0, DHCP6_RLY_MAX_IF_NAME_LEN);

    if (D6RlUtlCliConfGetIfName (u4Index, ai1IfName) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\n %s \r\n", ai1IfName);
    i4RetVal = (INT4) nmhValidateIndexInstanceFsDhcp6RlyIfTable (u4Index);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Relay Mode disabled in the interface\r\n");
        return CLI_SUCCESS;
    }
    if ((pInterface = D6RlIfGetNode ((UINT4) u4Index)) == NULL)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, " Transmitted: \n");
    if (NULL != pInterface)
    {
        CliPrintf (CliHandle, "    Advertise           : %u\r\n",
                   pInterface->u4SrvAdv);
    }
    if (nmhGetFsDhcp6RlyIfRelayReplyOut (u4Index, &u4Count) == SNMP_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Unable to retrieve transmitted relay reply count!\r\n");
        *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "    Reply               : %u\r\n", u4Count);
    }
    if (NULL != pInterface)
    {
        CliPrintf (CliHandle, "    Reconfigure         : %u\r\n",
                   pInterface->u4Reconfig);
        CliPrintf (CliHandle, "    Relay Reply         : %u\r\n",
                   pInterface->u4RelayReply);
    }
    if (nmhGetFsDhcp6RlyIfRelayForwOut (u4Index, &u4Count) == SNMP_FAILURE)
    {
       DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                      DHCP6_RLY_FAILURE_TRC,
                      "Unable to retrieve transmitted relay forward count!\r\n");
       *pu4ErrorCode = DHCP6_RLY_UNEXPECTED; 
       return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "    Relay Forward       : %u\r\n", u4Count);
    }
    if (NULL != pInterface)
    {
        CliPrintf (CliHandle, " Received: \n");
        CliPrintf (CliHandle, "    Solicit             : %u\r\n",
                   pInterface->u4Solicit);
        CliPrintf (CliHandle, "    Request             : %u\r\n",
                   pInterface->u4Request);
        CliPrintf (CliHandle, "    Confirm             : %u\r\n",
                   pInterface->u4Confirm);
        CliPrintf (CliHandle, "    Renew               : %u\r\n",
                   pInterface->u4Renew);
        CliPrintf (CliHandle, "    Rebind              : %u\r\n",
                   pInterface->u4Rebind);
        CliPrintf (CliHandle, "    Release             : %u\r\n",
                   pInterface->u4Release);
        CliPrintf (CliHandle, "    Decline             : %u\r\n",
                   pInterface->u4Decline);
    }

    if (nmhGetFsDhcp6RlyIfInformIn (u4Index, &u4Count) == SNMP_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Cannot get Information req count!\r\n");
        *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "    Information-Request : %u\r\n", u4Count);
    }
    if (nmhGetFsDhcp6RlyIfRelayForwIn (u4Index, &u4Count) == SNMP_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Cannot get Relay Fwd In count!\r\n");
        *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "    Relay Forward       : %u\r\n", u4Count);
    }
    if (nmhGetFsDhcp6RlyIfRelayReplyIn ((INT4) u4Index, &u4Count) == SNMP_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                       DHCP6_RLY_FAILURE_TRC,
                       "Cannot get Relay Rpy In count!\r\n");
        *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "    Relay Reply         : %u\r\n", u4Count);
    }
    if (nmhGetFsDhcp6RlyIfInvalidPktIn ((INT4) u4Index, &u4Count) == SNMP_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC | DHCP6_RLY_FAILURE_TRC,
                       "Cannot get Invalid Pkt count!\r\n");
        *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "    Invalid             : %u\r\n", u4Count);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliSetDebugs                                      */
/*                                                                           */
/* Description      : This function is used to set the Debug for the Relay.  */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     pu1TraceInput- Input Trace Value                      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC INT4
D6RlCliSetDebugs (tCliHandle CliHandle, UINT1 *pu1TraceInput)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE TraceInput;
    UNUSED_PARAM (CliHandle);
    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);
    if (nmhTestv2FsDhcp6RlyDebugTrace (&u4ErrorCode, &TraceInput) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (DHCP6_RLY_INVALID_DEBUG_STRING);
        return CLI_FAILURE;
    }
    nmhSetFsDhcp6RlyDebugTrace (&TraceInput);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliSetSysLogStatus                                 */
/*                                                                           */
/* Description      : This function is used to set the Syslog for the Relay.  */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     i4Status - SysLogValue                                */
/*                     pu4ErrorCode - Pointer to error code                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
D6RlCliSetSysLogStatus (tCliHandle CliHandle, INT4 i4Status,
                        UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = 0;
    UNUSED_PARAM (CliHandle);
    i4RetVal = nmhTestv2FsDhcp6RlySysLogAdminStatus (pu4ErrorCode, i4Status);
    if (i4RetVal == SNMP_SUCCESS)
    {
        nmhSetFsDhcp6RlySysLogAdminStatus (i4Status);
        return CLI_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = DHCP6_RLY_UNEXPECTED;
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliSetTraps                                       */
/*                                                                           */
/* Description      : This function is used to set the Traps for the Relay.  */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     u4Command - Command Type                              */
/*                     u4TrapValue - Trap Value                              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC INT4
D6RlCliSetTraps (tCliHandle CliHandle, UINT4 u4Command, UINT4 u4TrapValue)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT4               u4ErrCode = 0;
    UINT1               u1Trap = 0;
    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    TrapOption.pu1_OctetList = &u1Trap;
    TrapOption.i4_Length = 1;
    if (nmhGetFsDhcp6RlyTrapAdminControl (&TrapOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    u1Trap = TrapOption.pu1_OctetList[0];
    if (u4TrapValue != 0 || (u4Command == CLI_DHCP6_RLY_DISABLE_TRAP))
    {
        if (u4Command == CLI_DHCP6_RLY_ENABLE_TRAP)
        {
            u1Trap = (UINT1) (u1Trap | u4TrapValue);
        }
        else
        {
            u1Trap = (UINT1) (u1Trap & u4TrapValue);
        }
        TrapOption.pu1_OctetList[0] = u1Trap;
        TrapOption.i4_Length = 1;
        if (nmhTestv2FsDhcp6RlyTrapAdminControl (&u4ErrCode, &TrapOption) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        nmhSetFsDhcp6RlyTrapAdminControl (&TrapOption);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliSetRemoteIdStatus                               */
/*                                                                           */
/* Description      : This function is used to set the RemoteId option       */
/*                    status.                                                 */
/*                                                                             */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     i4Status - RemoteIdValue                              */
/*                     pu4ErrorCode - Pointer to error code                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
D6RlCliSetRemoteIdStatus (tCliHandle CliHandle, INT4 i4Status,
                          UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = 0;
    UNUSED_PARAM (CliHandle);

    i4RetVal = nmhTestv2FsDhcp6RlyOption37Control (pu4ErrorCode, i4Status);
    if (i4RetVal == SNMP_SUCCESS)
    {
        nmhSetFsDhcp6RlyOption37Control (i4Status);
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliSetPDRouteStatus                                */
/*                                                                           */
/* Description      : This function is used to enable/disable the PD routes  */
/*               into routing table                                     */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     i4Status  - PDRouteValue                              */
/*                     pu4ErrorCode - Pointer to error code                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
D6RlCliSetPDRouteStatus (tCliHandle CliHandle, INT4 i4Status,
                         UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = 0;

    UNUSED_PARAM (CliHandle);

    i4RetVal = nmhTestv2FsDhcp6RlyPDRouteControl (pu4ErrorCode, i4Status);
    if (i4RetVal == SNMP_SUCCESS)
    {
        nmhSetFsDhcp6RlyPDRouteControl (i4Status);
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowRunningConfig                               */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP6 Relay.                                           */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     u4Module - module Value                               */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC INT4
D6RlCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UNUSED_PARAM (u4Module);
    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();

    if (D6RlCliShowRunningScalars (CliHandle) == CLI_SUCCESS)
    {
        D6RlCliShowRunningConfigInterface (CliHandle);
    }
    D6RlMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowRunningScalars                              */
/*                                                                           */
/* Description      : This function displays scalar objects in Dhcp6 Server  */
/*                    for show running configuration.                        */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
D6RlCliShowRunningScalars (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpTrapOption;
    UINT1               u1TrapOption = 0;
    UINT1               u1CurTrapOption = 0;
    INT4                i4SysLogStatus = 0;
    INT4                i4ReqListenPort = 0;
    INT4                i4ClientTransmitPort = 0;
    INT4                i4ServerTransmitPort = 0;
    INT4                i4RemoteIdStatus = 0;
    INT4                i4PDRouteStatus = 0;

    MEMSET (&DhcpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpTrapOption.pu1_OctetList = &u1TrapOption;
    DhcpTrapOption.i4_Length = 1;

    nmhGetFsDhcp6RlyTrapAdminControl (&DhcpTrapOption);

    u1CurTrapOption = DhcpTrapOption.pu1_OctetList[0];

    if (u1CurTrapOption != 0)
    {
        CliPrintf (CliHandle, " snmp-server enable traps ipv6 dhcp relay");

        if (DHCP6_RLY_RCVD_INVALID_MSG_TRAP_DISABLED () == OSIX_FALSE)
        {
            CliPrintf (CliHandle, " invalid-pkt");
        }
        if (DHCP6_RLY_HOP_THRESHOLD_TRAP_DISABLED () == OSIX_FALSE)
        {
            CliPrintf (CliHandle, " max-hop-count");
        }

        CliPrintf (CliHandle, "\r\n");
    }
    nmhGetFsDhcp6RlySysLogAdminStatus (&i4SysLogStatus);
    nmhGetFsDhcp6RlyListenPort (&i4ReqListenPort);
    nmhGetFsDhcp6RlyClientTransmitPort (&i4ClientTransmitPort);
    nmhGetFsDhcp6RlyServerTransmitPort (&i4ServerTransmitPort);
    nmhGetFsDhcp6RlyOption37Control (&i4RemoteIdStatus);
    nmhGetFsDhcp6RlyPDRouteControl (&i4PDRouteStatus);

    if (i4SysLogStatus == DHCP6_ENABLED)
    {
        CliPrintf (CliHandle, "ipv6 dhcp relay syslog enable\r\n");
    }
    if (i4ReqListenPort != DHCP6_RLY_RELAY_PORT_DEF)
    {
        CliPrintf (CliHandle, "ipv6 dhcp relay port listen %d\r\n",
                   i4ReqListenPort);
    }
    if (i4ClientTransmitPort != DHCP6_RLY_CLIENT_PORT_DEF)
    {
        CliPrintf (CliHandle,
                   "ipv6 dhcp relay port client transmit %d\r\n",
                   i4ClientTransmitPort);
    }
    if (i4ServerTransmitPort != DHCP6_RLY_SERVER_PORT_DEF)
    {
        CliPrintf (CliHandle, "ipv6 dhcp relay port server transmit %d\r\n",
                   i4ServerTransmitPort);
    }
    if (i4RemoteIdStatus == DHCP6_RLY_REMOTEID_ENABLE)
    {
        CliPrintf (CliHandle, "ipv6 dhcp relay remote-id enable\r\n");
    }

    if (i4PDRouteStatus == DHCP6_RLY_PDROUTE_DISABLE)
    {
        CliPrintf (CliHandle, "no ipv6 dhcp relay pd forwarding\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowRunningConfigInterface                  */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP6 relay Interface .                               */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/
VOID
D6RlCliShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT1                i1OutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1IfName[DHCP6_RLY_MAX_IF_NAME_LEN];
    MEMSET (ai1IfName, 0, DHCP6_RLY_MAX_IF_NAME_LEN);

    i1OutCome = nmhGetFirstIndexFsDhcp6RlyIfTable (&i4NextPort);

    while (i1OutCome != SNMP_FAILURE)
    {
        if (D6RlUtlCliConfGetIfName (i4NextPort, ai1IfName) == OSIX_FAILURE)
        {
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexFsDhcp6RlyIfTable (i4CurrentPort, &i4NextPort);
            continue;
        }

        D6RlMainUnLock ();
        CliUnRegisterLock (CliHandle);

        D6RlCliShowRunningConfigInterfaceDetails (CliHandle, i4NextPort);

        CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);
        D6RlMainLock ();

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexFsDhcp6RlyIfTable (i4CurrentPort, &i4NextPort);

    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : D6RlCliShowRunningConfigInterfaceDetails            */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in DHCP   */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
PUBLIC VOID
D6RlCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{

    tSNMP_OCTET_STRING_TYPE SrvAddr;
    tSNMP_OCTET_STRING_TYPE NextIfServAddr;
    tSNMP_OCTET_STRING_TYPE NextServAddr;
    tSNMP_OCTET_STRING_TYPE LastIfServAddr;
    tSNMP_OCTET_STRING_TYPE sOctetStrRemIdOptVal;
    INT4                i4RowStatus = 0;
    INT4                i4NextSrvAddrIfIndex = 0;
    INT4                i4RelVal = 0;
    INT4                i4OutIndex = 0;
    INT4                i4NextOutIndex = 0;
    INT4                i4RetValue = SNMP_SUCCESS;
    INT4                i4HopThreshold = 0;
    INT4                i4RemoteIdType = 0;
    UINT4               u4FlagValue = OSIX_FALSE;
    INT1                ai1IfName[DHCP6_RLY_MAX_IF_NAME_LEN];
    UINT1               au1NextIfIpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1LastIfIpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1NextIpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               u1Value = 0;
    UINT1               au1IpAddr[DHCP6_RLY_IP6_ADDR_LEN];
    UINT1               au1RemIdOptVal[DHCP6_RLY_CLI_STR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4InterfaceFlagValue = OSIX_FALSE;
    tDhcp6RlyIfInfo    *pIfInfo = NULL;

    MEMSET (ai1IfName, 0, DHCP6_RLY_MAX_IF_NAME_LEN);
    MEMSET (&NextIfServAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextServAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SrvAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LastIfServAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextIfIpAddr, 0, DHCP6_RLY_IP6_ADDR_LEN);
    MEMSET (au1LastIfIpAddr, 0, DHCP6_RLY_IP6_ADDR_LEN);
    SrvAddr.pu1_OctetList = au1IpAddr;

    MEMSET (gu1Temp, 0, DHCP6_RLY_REM_ID_MAX_LEN);
    MEMSET (au1RemIdOptVal, 0, DHCP6_RLY_CLI_STR_LEN);
    MEMSET (&sOctetStrRemIdOptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    sOctetStrRemIdOptVal.pu1_OctetList = &gu1Temp[0];
    sOctetStrRemIdOptVal.i4_Length = DHCP6_RLY_REM_ID_MAX_LEN;

    NextIfServAddr.pu1_OctetList = au1NextIfIpAddr;
    NextIfServAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;

    NextServAddr.pu1_OctetList = au1NextIpAddr;
    NextServAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;

    LastIfServAddr.pu1_OctetList = au1LastIfIpAddr;
    LastIfServAddr.i4_Length = DHCP6_RLY_IP6_ADDR_LEN;

    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);
    D6RlMainLock ();
    do
    {
        if (nmhValidateIndexInstanceFsDhcp6RlyIfTable (i4IfIndex) !=
            SNMP_SUCCESS)
        {
            break;
        }
        nmhGetFsDhcp6RlyIfRowStatus (i4IfIndex, &i4RowStatus);
        if (i4RowStatus != ACTIVE)
        {
            break;
        }
        CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1IfName);
        pIfInfo = D6RlIfGetNode ((UINT4) i4IfIndex);
        if (NULL != pIfInfo)
        {
            if ((pIfInfo->u1Flag & DHCP6_RLY_STATIC) != DHCP6_RLY_STATIC)
            {
                break;
            }
        }
        CliPrintf (CliHandle, "interface %s \r\n", au1IfName);
        u4InterfaceFlagValue = OSIX_TRUE;
        CliPrintf (CliHandle, " ipv6 dhcp relay\r\n");
        nmhGetFsDhcp6RlyIfHopThreshold (i4IfIndex, &i4HopThreshold);
        if (i4HopThreshold != DHCP6_RLY_HOP_THRESH_DEF)
        {
            CliPrintf (CliHandle, " ipv6 dhcp relay hop-threshold %d\r\n",
                       i4HopThreshold);
        }

        nmhGetFsDhcp6RlyIfRemoteIdOptionValue (i4IfIndex,
                                               &sOctetStrRemIdOptVal);
        nmhGetFsDhcp6RlyIfRemoteIdOption (i4IfIndex, &i4RemoteIdType);
        if (i4RemoteIdType != DHCP6_RLY_REMOTEID_SW_NAME)
        {
            if (i4RemoteIdType == DHCP6_RLY_REMOTEID_DUID)
            {
                CliOctetToStr (sOctetStrRemIdOptVal.pu1_OctetList,
                               sOctetStrRemIdOptVal.i4_Length,
                               au1RemIdOptVal, DHCP6_RLY_CLI_STR_LEN);
                CliPrintf (CliHandle,
                           " ipv6 dhcp relay remote-id type duid\r\n");

            }
            else if (i4RemoteIdType == DHCP6_RLY_REMOTEID_MGMT_IP)
            {
                MEMCPY (au1RemIdOptVal,
                        sOctetStrRemIdOptVal.pu1_OctetList,
                        sOctetStrRemIdOptVal.i4_Length);
                CliPrintf (CliHandle,
                           " ipv6 dhcp relay remote-id type mgmt-ip\r\n");
            }
            else if (i4RemoteIdType == DHCP6_RLY_REMOTEID_USERDEFINED)
            {
                MEMCPY (au1RemIdOptVal,
                        sOctetStrRemIdOptVal.pu1_OctetList,
                        sOctetStrRemIdOptVal.i4_Length);
                CliPrintf (CliHandle,
                           " ipv6 dhcp relay remote-id type userDefined\r\n");
            }

        }

        if (i4RemoteIdType == DHCP6_RLY_REMOTEID_DUID)
        {
            if (sOctetStrRemIdOptVal.i4_Length != DHCP6_RLY_DUID_DEF)
            {
                CliPrintf (CliHandle, " ipv6 dhcp relay remote-id duid %s \r\n",
                           au1RemIdOptVal);
            }
        }
        else if (i4RemoteIdType == DHCP6_RLY_REMOTEID_USERDEFINED)
        {
            CliPrintf (CliHandle,
                       " ipv6 dhcp relay remote-id userDefined %s \r\n",
                       au1RemIdOptVal);
        }

        i4NextSrvAddrIfIndex = i4IfIndex;
        MEMSET (SrvAddr.pu1_OctetList, 0x00, DHCP6_RLY_IP6_ADDR_LEN);
        SrvAddr.i4_Length = 0;
        i4RelVal =
            nmhGetNextIndexFsDhcp6RlySrvAddressTable (i4NextSrvAddrIfIndex,
                                                      &i4NextSrvAddrIfIndex,
                                                      &SrvAddr, &SrvAddr);
        if (i4RelVal == SNMP_SUCCESS)
        {
            CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1IfName);
        }
        while (i4RelVal == SNMP_SUCCESS)
        {
            if (i4NextSrvAddrIfIndex == i4IfIndex)
            {
                CliPrintf (CliHandle, " ipv6 dhcp relay");
                if (SrvAddr.i4_Length != 0)
                {
                    CliPrintf (CliHandle, " destination %s link-local",
                               Ip6PrintAddr ((tIp6Addr *) (VOID *) SrvAddr.
                                             pu1_OctetList));
                    u4FlagValue = OSIX_TRUE;
                    u1Value = OSIX_TRUE;
                    NextServAddr.i4_Length = SrvAddr.i4_Length;
                    MEMCPY (NextServAddr.pu1_OctetList, SrvAddr.pu1_OctetList,
                            DHCP6_RLY_IP6_ADDR_LEN);

                    i4OutIndex = 0;
                    i4RetValue = nmhGetNextIndexFsDhcp6RlyOutIfTable
                        (i4IfIndex, &i4NextSrvAddrIfIndex, &NextServAddr,
                         &NextIfServAddr, i4OutIndex, &i4NextOutIndex);
                    while ((i4RetValue == SNMP_SUCCESS)
                           && (i4IfIndex == i4NextSrvAddrIfIndex)
                           &&
                           (MEMCMP
                            (au1NextIpAddr, au1NextIfIpAddr,
                             DHCP6_RLY_IP6_ADDR_LEN) == 0))
                    {
                        if (D6RlUtlCliConfGetIfName ((UINT4) i4NextOutIndex, ai1IfName)
                            == OSIX_SUCCESS)
                        {
                            if (u4FlagValue == OSIX_TRUE)
                            {
                                CliPrintf (CliHandle, " interface %s",
                                           ai1IfName);
                            }
                            else
                            {
                                CliPrintf (CliHandle, "\n ipv6 dhcp relay "
                                           "destination %s link-local interface %s",
                                           Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                                         SrvAddr.pu1_OctetList),
                                           ai1IfName);
                            }
                        }
                        u4FlagValue = OSIX_FALSE;
                        MEMCPY
                            (au1NextIpAddr, au1NextIfIpAddr,
                             DHCP6_RLY_IP6_ADDR_LEN);
                        i4OutIndex = i4NextOutIndex;
                        i4RetValue = nmhGetNextIndexFsDhcp6RlyOutIfTable
                            (i4IfIndex, &i4NextSrvAddrIfIndex, &NextServAddr,
                             &NextIfServAddr, i4OutIndex, &i4NextOutIndex);
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
                if (u1Value == 0)
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            i4RelVal =
                nmhGetNextIndexFsDhcp6RlySrvAddressTable (i4NextSrvAddrIfIndex,
                                                          &i4NextSrvAddrIfIndex,
                                                          &SrvAddr, &SrvAddr);
        }
        if (u4InterfaceFlagValue != OSIX_FALSE)
        {
            CliPrintf (CliHandle, "!\r\n");
            u4InterfaceFlagValue = OSIX_FALSE;
        }
    }
    while (0);
    D6RlMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowIfRelayInfo                               */
/*                                                                           */
/* Description      : This function shows configuration of DHCP6 Relay.      */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC INT4
D6RlCliShowIfRelayInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{

    D6RlCliShowInterface (CliHandle, i4IfIndex);
    DHCP6_RLY_UNLOCK ();
    CliUnRegisterLock (CliHandle);
#ifdef DHCP6_CLNT_WANTED
    D6ClApiShowGlobalInfo (CliHandle);
#endif
#ifdef DHCP6_SRV_WANTED
    D6SrApiShowGlobalInfo (CliHandle);
#endif
    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);

    DHCP6_RLY_LOCK ();

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowGlbRelayInfo                               */
/*                                                                           */
/* Description      : This function shows configuration of DHCP6 Relay.      */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC INT4
D6RlCliShowGlbRelayInfo (tCliHandle CliHandle)
{

    D6RlCliShowGlobalInfo (CliHandle);
    DHCP6_RLY_UNLOCK ();
    CliUnRegisterLock (CliHandle);
#ifdef DHCP6_CLNT_WANTED
    D6ClApiShowGlobalInfo (CliHandle);
#endif
#ifdef DHCP6_SRV_WANTED
    D6SrApiShowGlobalInfo (CliHandle);
#endif
    CliRegisterLock (CliHandle, D6RlMainLock, D6RlMainUnLock);

    DHCP6_RLY_LOCK ();

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowRelayInfo                               */
/*                                                                           */
/* Description      : This function shows configuration of DHCP6 Relay.      */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC INT4
D6RlCliShowRelayInfo (tCliHandle CliHandle)
{
    UINT4               u4IfIndex = 0;

    for (u4IfIndex = 1; u4IfIndex <= MAX_D6RL_RLY_IF_ENTRIES; u4IfIndex++)
    {
        D6RlCliShowIfRelayInfo (CliHandle, (INT4) u4IfIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6RlCliShowGlobalInfo                                 */
/*                                                                           */
/* Description      : This function shows configuration of DHCP6 Relay.      */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

PUBLIC VOID
D6RlCliShowGlobalInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpTrapOption;
    INT4                i4SysLogStatus = 0;
    INT4                i4ListenPort = 0;
    INT4                i4RelplyTxPort = 0;
    INT4                i4RelayRepTxPort = 0;
    INT4                i4RemoteIdStatus = 0;
    UINT1               u1CurTrapOption = 0;
    UINT1               u1FlagValue = OSIX_FALSE;
    INT4                i4PDRouteStatus = 0;

    MEMSET (&DhcpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    DhcpTrapOption.pu1_OctetList = &u1CurTrapOption;
    DhcpTrapOption.i4_Length = 1;

    /*Show Global Information of Client */
    nmhGetFsDhcp6RlyTrapAdminControl (&DhcpTrapOption);
    u1CurTrapOption = DhcpTrapOption.pu1_OctetList[0];

    nmhGetFsDhcp6RlySysLogAdminStatus (&i4SysLogStatus);
    nmhGetFsDhcp6RlyListenPort (&i4ListenPort);
    nmhGetFsDhcp6RlyClientTransmitPort (&i4RelplyTxPort);
    nmhGetFsDhcp6RlyServerTransmitPort (&i4RelayRepTxPort);
    nmhGetFsDhcp6RlyOption37Control (&i4RemoteIdStatus);
    nmhGetFsDhcp6RlyPDRouteControl (&i4PDRouteStatus);

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "Relay information:\r\n");
    CliPrintf (CliHandle, "  Listen UDP port                       : %d\r\n",
               i4ListenPort);
    CliPrintf (CliHandle, "  Client Transmit UDP port              : %d\r\n",
               i4RelplyTxPort);
    CliPrintf (CliHandle, "  Server Transmit UDP port              : %d\r\n",
               i4RelayRepTxPort);
    CliPrintf (CliHandle, "  Sys log status                        : ");
    if (i4SysLogStatus == DHCP6_RLY_ENABLE)
    {
        CliPrintf (CliHandle, "enabled\t\n");
    }
    else
    {
        CliPrintf (CliHandle, "disabled\t\n");
    }

    CliPrintf (CliHandle, "  SNMP traps                            : ");
    if (u1CurTrapOption != 0)
    {
        if (DHCP6_RLY_RCVD_INVALID_MSG_TRAP_DISABLED () == OSIX_FALSE)
        {
            u1FlagValue = OSIX_TRUE;
            CliPrintf (CliHandle, "invalid-pkt");
        }
        if (DHCP6_RLY_HOP_THRESHOLD_TRAP_DISABLED () == OSIX_FALSE)
        {
            if (u1FlagValue == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ",");
            }
            CliPrintf (CliHandle, "max-hop-count");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "none\r\n");
    }
    CliPrintf (CliHandle, "  Remote-ID(Option 37)            : ");
    if (i4RemoteIdStatus == DHCP6_RLY_REMOTEID_ENABLE)
    {
        CliPrintf (CliHandle, "enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "disabled\t\n");
    }
    CliPrintf (CliHandle, "  PD Forwarding                   : ");
    if (i4PDRouteStatus == DHCP6_RLY_PDROUTE_ENABLE)
    {
        CliPrintf (CliHandle, "enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "disabled\r\n");
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IssDhcp6RelayShowDebugging                         */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DHCP6 relay debug level   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
VOID
IssDhcp6RelayShowDebugging (tCliHandle CliHandle)
{
    if (DHCP6_RLY_DEBUG_TRACE == DHCP6_RLY_INVALID_TRC)
    {
        return;
    }
    CliPrintf (CliHandle, "\rDHCP6 RELAY :");
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_ALL_TRC) == DHCP6_RLY_ALL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay all debugging is on");
        CliPrintf (CliHandle, "\r\n");
        return;
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_INIT_SHUT_TRC) ==
        DHCP6_RLY_INIT_SHUT_TRC)
    {
        CliPrintf (CliHandle,
                   "\r\n  DHCP6 relay init shutdown debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_MGMT_TRC) == DHCP6_RLY_MGMT_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay management debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_CONTROL_PLANE_TRC) ==
        DHCP6_RLY_CONTROL_PLANE_TRC)
    {
        CliPrintf (CliHandle,
                   "\r\n  DHCP6 relay control plane debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_DUMP_TRC) == DHCP6_RLY_DUMP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay packet dump debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_OS_RESOURCE_TRC) ==
        DHCP6_RLY_OS_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay os resource debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_FAILURE_TRC) ==
        DHCP6_RLY_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay all failure debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_BUFFER_TRC) == DHCP6_RLY_BUFFER_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay buffer debugging is on");
    }
    if ((DHCP6_RLY_DEBUG_TRACE & DHCP6_RLY_CRITICAL_TRC) ==
        DHCP6_RLY_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 relay critical debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6RelayShowTraps                                        */
/*                                                                           */
/* Description  : This function is for displaying the traps enabled in       */
/*                dhcp6 relay                                                */
/*                                                                           */
/* Input        : CliHandle - Handle to the cli context                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Dhcp6RelayShowTraps (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpRlyTrapOption;
    UINT1               u1RlyTrapOption = 0;
    UINT1               u1CurRlyTrapOption = 0;
    MEMSET (&DhcpRlyTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpRlyTrapOption.pu1_OctetList = &u1RlyTrapOption;
    DhcpRlyTrapOption.i4_Length = 1;
    nmhGetFsDhcp6RlyTrapAdminControl (&DhcpRlyTrapOption);
    u1CurRlyTrapOption = DhcpRlyTrapOption.pu1_OctetList[0];
    if (u1CurRlyTrapOption != 0)
    {
        CliPrintf (CliHandle, "\nipv6 dhcp relay");
        if (DHCP6_RLY_RCVD_INVALID_MSG_TRAP_DISABLED () == OSIX_FALSE)
        {
            CliPrintf (CliHandle, " invalid-pkt,");
        }
        if (DHCP6_RLY_HOP_THRESHOLD_TRAP_DISABLED () == OSIX_FALSE)
        {
            CliPrintf (CliHandle, " max-hop-count");
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlVlanCliDelIfEntry                                      */
/*                                                                           */
/* Description  : This function is for deleting the configuration of         */
/*                dhcp6 relay                                                */
/*                                                                           */
/* Input        : CliHandle - Handle to the cli context                      */
/*                u4Index - Interface Index                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6RlVlanCliDelIfEntry (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    /* Delete ipv6 dhcp relay configuration */
    UINT4               u4ErrorCode = 0;
    UINT4               u4InterfaceId = 0;
    tDhcp6RlyIfInfo    *pInterface = NULL;
    tDhcp6RlySrvAddrInfo *pDhcp6RlySrvAddrInfo = NULL;
    tIp6Addr            SrvAddr;

    pInterface = D6RlIfGetNode (u4IfIndex);
    if (pInterface != NULL)
    {
        pDhcp6RlySrvAddrInfo = D6RlSrvGetFirstNodeFromList (pInterface);
        if (pDhcp6RlySrvAddrInfo != NULL)
        {
            MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));
            INET_ATON6 (&pDhcp6RlySrvAddrInfo->Addr, &SrvAddr);
            D6RlCliDelIfEntry (CliHandle, u4IfIndex,
                               &SrvAddr, u4InterfaceId, &u4ErrorCode);
        }
        D6RlCliDelIfEntry (CliHandle, u4IfIndex,
                           NULL, u4InterfaceId, &u4ErrorCode);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlCliDeleteDynamicIfEntries                              */
/*                                                                           */
/* Description  : This function is for deleting the Dhcp Interface Entries   */
/*                which are created Dynamically and which are not present in */
/*                Out If Table                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6RlCliDeleteDynamicIfEntries (VOID)
{
    tDhcp6RlyIfInfo    *pIfInfo = NULL;

    /* Delete the Static flag to the u4Index */
    pIfInfo = D6RlIfGetFirstNode ();
    while (NULL != pIfInfo)
    {
        if (!(pIfInfo->u1Flag & DHCP6_RLY_STATIC))
        {
            if (D6RlCliCheckOutIfPresent (pIfInfo->u4IfIndex) == OSIX_FALSE)
            {
                /* If Interface is not present as Out interface and if 
                 * Interface is not created statically delete it */
                nmhSetFsDhcp6RlyIfRowStatus (pIfInfo->u4IfIndex, DESTROY);
                return;
            }
        }
        pIfInfo = D6RlIfGetNextNode (pIfInfo->u4IfIndex);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlCliCheckOutIfPresent                                   */
/*                                                                           */
/* Description  : This function is checks whether the Given Interface Index  */
/*                is present as Out Interface                                */
/*                                                                           */
/* Input        : u4Index - Interface Index                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
D6RlCliCheckOutIfPresent (UINT4 u4Index)
{
    tDhcp6RlyOutIfInfo *pIfInfo = NULL;

    /* Delete the Static flag to the u4Index */
    pIfInfo = D6RlOuIfGetFirstNode ();
    while (NULL != pIfInfo)
    {
        if (pIfInfo->u4OutIfIndex == u4Index)
        {
            return OSIX_TRUE;
        }
        pIfInfo = D6RlOuIfGetNextNode (pIfInfo->pSrvAddr->pIf->u4IfIndex,
                                       &(pIfInfo->pSrvAddr->Addr),
                                       pIfInfo->u4OutIfIndex);
    }
    return OSIX_FALSE;
}
#endif
