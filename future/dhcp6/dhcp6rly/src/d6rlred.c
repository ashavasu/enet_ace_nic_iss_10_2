/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlred.c,v 1.6 2015/02/20 12:05:45 siva Exp $
 *
 * Description: This file contains DHCP6RLY Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __D6RLRED_C
#define __D6RLRED_C

#include "d6rlinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize dhcp6Rly dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pDhcp6RlyDynDataDesc = NULL;

    pDhcp6RlyDynDataDesc = &(gaDhcp6RlyDynDataDescList[DHCP6RLY_DYN_INFO]);

    pDhcp6RlyDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pDhcp6RlyDynDataDesc->u4DbDataSize =
        sizeof (tDhcp6RlyPDEntry) - FSAP_OFFSETOF (tDhcp6RlyPDEntry, u1Version);

    pDhcp6RlyDynDataDesc->pDbDataOffsetTbl = gaDhcp6RlyOffsetTbl;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Dhcp6Rly descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDescrTblInit (VOID)
{
    tDbDescrParams      Dhcp6RlyDescrParams;
    MEMSET (&Dhcp6RlyDescrParams, 0, sizeof (tDbDescrParams));

    Dhcp6RlyDescrParams.u4ModuleId = RM_DHCP6RLY_APP_ID;

    Dhcp6RlyDescrParams.pDbDataDescList = gaDhcp6RlyDynDataDescList;

    DbUtilTblInit (&gDhcp6RlyDynInfoList, &Dhcp6RlyDescrParams);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Dhcp6Rly db node. */
/*                                                                           */
/*    Input(s)            : pDhcp6RlyDbTblNode - pointer to Dhcp6Rly Db Entry.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDbNodeInit (tDbTblNode * pDhcp6RlyDbTblNode)
{
    DbUtilNodeInit (pDhcp6RlyDbTblNode, DHCP6RLY_DYN_INFO);

    return;
}

/************************************************************************
 *  Function Name   : Dhcp6RlyRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the DHCP6RLY module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
Dhcp6RlyRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;
    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_DHCP6RLY_APP_ID;
    RmRegParams.pFnRcvPkt = Dhcp6RlyRedRmCallBack;

    /* Create the RM Packet arrival Q */

    if (OsixQueCrt (DHCP6_RLY_RM_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    DHCP6_RLY_RM_Q_DEPTH, &gDhcp6RlyRmPktQId) != OSIX_SUCCESS)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC, "INIT: Queue Creation Failed\n");
        return OSIX_FAILURE;
    }

    /* Registering DHCP6 RELAY with RM */
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedInitGlobalInfo: Registration with RM failed\r \n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedInitGlobalInfo: Registration with RM failed"));
        return OSIX_FAILURE;
    }
    DHCP6RLY_GET_NODE_STATUS () = RM_INIT;
    DHCP6RLY_NUM_STANDBY_NODES () = 0;
    gDhcp6RlyRedGblInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the dhcp6relay module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register dhcp6relay with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
Dhcp6RlyRedDeInitGlobalInfo (VOID)
{
    if (RmDeRegisterProtocols (RM_DHCP6RLY_APP_ID) == RM_FAILURE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedDeInitGlobalInfo: De-Registration with RM failed \r\n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedDeInitGlobalInfo: De-Registration with RM failed"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to dhcp6relay        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tDhcp6RlyRmMsg     *pMsg = NULL;
    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedRmCallBack:This event is not associated with RM \r\n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedRmCallBack:This event is not associated with RM"));
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedRmCallBack: Queue Message associated with the event "
                       "is not sent by RM \r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedRmCallBack: Queue Message associated with the "
                            "event is not sent by RM"));
        return OSIX_FAILURE;
    }

    pMsg =
        (tDhcp6RlyRmMsg *) MemAllocMemBlk ((tMemPoolId)
                                           DHCP6RLY_RM_MSG_MEMPOOL_ID);
    if (pMsg == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedRmCallBack: Queue message allocation failure\r \n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            " Dhcp6RlyRedRmCallBack: Queue message allocation failure"));
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            Dhcp6RlyRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tDhcp6RlyRmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    /* Send the message associated with the event to dhcp6relay module in a queue */
    if (OsixQueSend (DHCP6RLY_RM_PKT_Q_ID,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) DHCP6RLY_RM_MSG_MEMPOOL_ID,
                            (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            Dhcp6RlyRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedRmCallBack: Q send failure\r \n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedRmCallBack: Q send failure"));
        return OSIX_FAILURE;
    }

    /* Post a event to DHCP6RLY to process RM events */
    OsixEvtSend (DHCP6_RLY_TASK_ID, DHCP6_RLY_RM_PKT_EVENT);

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the DHCp module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the DHCP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleRmEvents ()
{
    tDhcp6RlyRmMsg     *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixQueRecv (DHCP6RLY_RM_PKT_Q_ID,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents:Received GO_ACTIVE event\r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents: Received GO_ACTIVE"
                                    " event"));
                Dhcp6RlyRedHandleGoActive ();
                break;
            case GO_STANDBY:
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents:Received GO_STANDBY event\r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents:Received GO_STANDBY event"));
                Dhcp6RlyRedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;
            case RM_STANDBY_UP:
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents: Received RM_STANDBY_UP"
                               " event \r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents: Received RM_STANDBY_UP"
                                    " event"));
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                DHCP6RLY_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Dhcp6RlyRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (DHCP6RLY_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    DHCP6RLY_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gDhcp6RlyRedGblInfo.u1BulkUpdStatus =
                        DHCP6RLY_HA_UPD_NOT_STARTED;
                    Dhcp6RlyRedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents: Received RM_STANDBY_DOWN "
                               "event \r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents: Received RM_STANDBY_DOWN "
                                    "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                DHCP6RLY_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Dhcp6RlyRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents:Received RM_MESSAGE event\r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents:Received RM_MESSAGE event"));
                ProtoAck.u4AppId = RM_DHCP6RLY_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gDhcp6RlyRedGblInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    Dhcp6RlyRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                       pMsg->
                                                       RmCtrlMsg.u2DataLen);
                }
                else if (gDhcp6RlyRedGblInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    Dhcp6RlyRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                        pMsg->
                                                        RmCtrlMsg.u2DataLen);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                                   "Dhcp6RlyRedHandleRmEvents: Sync-up message received"
                                   " at Idle Node!!!!\r\n");

                    DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                        "Dhcp6RlyRedHandleRmEvents: Sync-up message "
                                        "received at Idle Node"));
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the DHCP status is changed 
                 * from idle to standby */
                if (gDhcp6RlyRedGblInfo.u1NodeStatus == RM_INIT)
                {
                    if (DHCP6RLY_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        Dhcp6RlyRedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents: Received "
                               "L2_INITIATE_BULK_UPDATES \r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents: Received "
                                    "L2_INITIATE_BULK_UPDATES"));
                Dhcp6RlyRedSendBulkReqMsg ();
                break;
            default:
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Dhcp6RlyRedHandleRmEvents:Invalid RM event received\r\n");

                DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleRmEvents: Invalid RM event received"));
                break;

        }
        MemReleaseMemBlock (DHCP6RLY_RM_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
    }
    return;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the DHCP6RLY upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;
    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gDhcp6RlyRedGblInfo.u1BulkUpdStatus = DHCP6RLY_HA_UPD_NOT_STARTED;

    if (DHCP6RLY_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedHandleGoActive: GO_ACTIVE event reached when node "
                       "is already active \r\n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedHandleGoActive: GO_ACTIVE event reached when "
                            "node is already active"));
        return;
    }
    if (DHCP6RLY_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedHandleGoActive: Idle to Active transition...\r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedHandleGoActive: Idle to Active transition"));

        Dhcp6RlyRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (DHCP6RLY_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedHandleGoActive: Standby to Active transition...\r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedHandleGoActive: Standby to Active transition"));
        Dhcp6RlyRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (DHCP6RLY_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        DHCP6RLY_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gDhcp6RlyRedGblInfo.u1BulkUpdStatus = DHCP6RLY_HA_UPD_NOT_STARTED;
        Dhcp6RlyRedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    return;
}

/************************************************************************
 * Function Name      : Dhcp6RlyRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the DHCP6RLY upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleGoStandby (tDhcp6RlyRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;
    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    MemReleaseMemBlock (DHCP6RLY_RM_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
    gDhcp6RlyRedGblInfo.u1BulkUpdStatus = DHCP6RLY_HA_UPD_NOT_STARTED;

    if (DHCP6RLY_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedHandleGoStandby: GO_STANDBY event reached when "
                       "node is already in standby \r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedHandleGoStandby: GO_STANDBY event reached when "
                            "node is already in standby"));
        return;
    }
    if (DHCP6RLY_GET_NODE_STATUS () == RM_INIT)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedHandleGoStandby: GO_STANDBY event reached when node "
                       "is already idle \r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedHandleGoStandby: GO_STANDBY event reached when "
                            "node is already idle"));

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedHandleGoStandby: Active to Standby transition..\r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedHandleGoStandby: Active to Standby transition"));
        Dhcp6RlyRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleIdleToActive (VOID)
{
    /* Node status is set to active and the number of standby nodes 
     * are updated */
    DHCP6RLY_GET_NODE_STATUS () = RM_ACTIVE;
    DHCP6RLY_RM_GET_NUM_STANDBY_NODES_UP ();

    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleIdleToStandby (VOID)
{
    /* the node status is set to standby and no of standby nodes is set to 0 */
    DHCP6RLY_GET_NODE_STATUS () = RM_STANDBY;
    DHCP6RLY_NUM_STANDBY_NODES () = 0;

    DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                   "Dhcp6RlyRedHandleIdleToStandby: Node Status Idle to Standby\r\n");
    DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                        "Dhcp6RlyRedHandleIdleToStandby: Node Status Idle to Standby"));
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyAddRouteAndStartTimer                   */
/*                                                                      */
/* Description        : This is a will add route and start timer        */
/*                      for all the DHCP6RLY instances.                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : DHCP6RLY_OK/DHCP6RLY_NOT_OK                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyAddRouteAndStartTimer ()
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;

    if (DHCP6_RLY_PDROUTE_STATUS == DHCP6_RLY_PDROUTE_DISABLE)
    {
        return OSIX_SUCCESS;
    }

    pDhcp6RlyPDEntry = D6RlPDGetFirstNode ();
    do
    {
        if (pDhcp6RlyPDEntry == NULL)
        {
            return OSIX_FAILURE;
        }

        if (OSIX_FAILURE ==
            D6RlTmrStart (&(pDhcp6RlyPDEntry->Dhcp6RlyTmrBlk),
                          DHCP6RLY_VALID_TMR, pDhcp6RlyPDEntry->u4ValidLifeTime,
                          0))
        {
            DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                "Dhcp6RlyAddRouteAndStartTimer: Unable to start timer  prefix: %s, prefix length: %d, nexthop %s\r\n",
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->Prefix),
                                pDhcp6RlyPDEntry->u1PrefixLen,
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->NextHop));

            DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyAddRouteAndStartTimer: Unable to start timer  prefix: %s, prefix length: %d, nexthop %s\r\n",
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->Prefix),
                                pDhcp6RlyPDEntry->u1PrefixLen,
                                Ip6PrintAddr (&pDhcp6RlyPDEntry->NextHop)));
            return OSIX_FAILURE;
        }
        /*get the next node */
    }
    while ((pDhcp6RlyPDEntry =
            D6RlPDGetNextNode (&(pDhcp6RlyPDEntry->Prefix),
                               pDhcp6RlyPDEntry->u1PrefixLen)) != NULL);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleStandbyToActive (VOID)
{
    /*there is no need for Hardware audit, only if routes are added into rtm6 and rbtree,
       they are synced up */

    DHCP6RLY_GET_NODE_STATUS () = RM_ACTIVE;
    DHCP6RLY_RM_GET_NUM_STANDBY_NODES_UP ();

    SelAddFd (DHCP6_RLY_SOCKET_FD, D6RlSktDataRcvd);

    /*Routes to be added into RTM6 &  Timer to be started */

    Dhcp6RlyAddRouteAndStartTimer ();

    DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                   "Dhcp6RlyRedHandleStandbyToActive: Node Status Standby to "
                   "Active\r\n");
    DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                        "Dhcp6RlyRedHandleStandbyToActive: Node Status Standby to Active"));
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedHandleActiveToStandby (VOID)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    tDhcp6RlyPDEntry   *pTempDhcp6RlyPDEntry = NULL;

    /*update the statistics */
    DHCP6RLY_GET_NODE_STATUS () = RM_STANDBY;
    DHCP6RLY_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Stop the running timers in the active node. Check whether any timers are 
     * running, if running, stop those timers. If there are any messages in the 
     * queue, discard those messages. Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */

    if (DHCP6_RLY_PDROUTE_STATUS == DHCP6_RLY_PDROUTE_ENABLE)
    {
        pDhcp6RlyPDEntry = D6RlPDGetFirstNode ();

        while (pDhcp6RlyPDEntry != NULL)
        {
            if (OSIX_FAILURE ==
                D6RlTmrStopTimer (&(pDhcp6RlyPDEntry->Dhcp6RlyTmrBlk)))
            {
                DHCP6_RLY_TRC_ARG2 (DHCP6_RLY_FAILURE_TRC,
                                    "Dhcp6RlyRedHandleActiveToStandby :Unable to stop timer for prefix: %s, prefix length: %d\r\n",
                                    Ip6PrintAddr (&(pDhcp6RlyPDEntry->Prefix)),
                                    pDhcp6RlyPDEntry->u1PrefixLen);

                DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL,
                                    DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedHandleActiveToStandby :Unable to stop timer for prefix: %s, prefix length: %d\r\n",
                                    Ip6PrintAddr (&(pDhcp6RlyPDEntry->Prefix)),
                                    pDhcp6RlyPDEntry->u1PrefixLen));
                return;
            }

            pTempDhcp6RlyPDEntry =
                D6RlPDGetNextNode (&(pDhcp6RlyPDEntry->Prefix),
                                   pDhcp6RlyPDEntry->u1PrefixLen);

            pDhcp6RlyPDEntry = pTempDhcp6RlyPDEntry;
        }
    }
    DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                   "Dhcp6RlyRedHandleActiveToStandby: Node Status Active to "
                   "Standby\r\n");

    DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                        "Dhcp6RlyRedHandleActiveToStandby: Node Status Active to Standby"));
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;

    DHCP6RLY_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    DHCP6RLY_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if ((UINT2) u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != DHCP6RLY_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == DHCP6RLY_RED_BULK_REQ_MESSAGE)
    {
        gDhcp6RlyRedGblInfo.u1BulkUpdStatus = DHCP6RLY_HA_UPD_NOT_STARTED;
        if (!DHCP6RLY_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gDhcp6RlyRedGblInfo.bBulkReqRcvd = OSIX_TRUE;

            DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                           "Dhcp6RlyRedProcessPeerMsgAtActive:Bulk request message "
                           "before RM_STANDBY_UP\r \n");

            DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyRedProcessPeerMsgAtActive:Bulk request message "
                                "before RM_STANDBY_UP"));
            return;
        }
        Dhcp6RlyRedSendBulkUpdMsg ();
    }
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2ExtractMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;
    UINT2               u2OffSetPass = 0;

    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;
    u2MinLen = DHCP6RLY_RED_TYPE_FIELD_SIZE + DHCP6RLY_RED_LEN_FIELD_SIZE;

    while (u2OffSetPass < u2DataLen)
    {
        u2ExtractMsgLen = u2OffSetPass;
        u4OffSet = (UINT4) u2OffSetPass;
        DHCP6RLY_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
        DHCP6RLY_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u2OffSetPass = (UINT2) u4OffSet;
            u2OffSetPass = (UINT2) (u2OffSetPass + u2Length);
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen = (UINT2) (u2ExtractMsgLen + u2Length);

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given 
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            RmApiHandleProtocolEvent (&ProtoEvt);
            DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                           "Dhcp6RlyRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r \n");

            DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyRedProcessPeerMsgAtStandby:RM_PROCESS Failure"));
            return;
        }

        u2OffSetPass = (UINT2) u4OffSet;
        switch (u1MsgType)
        {
            case DHCP6RLY_RED_BULK_UPD_TAIL_MESSAGE:
                Dhcp6RlyRedProcessBulkTailMsg (pMsg, &u2OffSetPass);
                break;
            case DHCP6RLY_RED_DYN_CACHE_INFO:
                Dhcp6RlyRedProcessDynamicInfo (pMsg, &u2OffSetPass);
                break;
            default:
                break;
        }
    }
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the DHCP6RLY module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {

        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedRmReleaseMemoryForMsg:Failure in releasing allocated"
                       " memory\r \n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedRmReleaseMemoryForMsg:Failure in releasing "
                            "allocated memory"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*cleared*/
/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Dhcp6RlyRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;
    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_DHCP6RLY_APP_ID, RM_DHCP6RLY_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedSendMsgToRm:Freememory due to message send "
                       "failure\r \n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedSendMsgToRm:Freememory due to message send"
                            " failure"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2OffSetPass = 0;

    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    DHCP6 RELAY Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (DHCP6RLY_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedSendBulkReqMsg: RM Memory allocation failed\r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedSendBulkReqMsg: RM Memory allocation failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    DHCP6RLY_RM_PUT_1_BYTE (pMsg, &u4OffSet, DHCP6RLY_RED_BULK_REQ_MESSAGE);
    DHCP6RLY_RM_PUT_2_BYTE (pMsg, &u4OffSet, DHCP6RLY_RED_BULK_REQ_MSG_SIZE);

    u2OffSetPass = (UINT2) u4OffSet;

    if (Dhcp6RlyRedSendMsgToRm (pMsg, u2OffSetPass) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedSendBulkReqMsg: Send message to RM failed\r\n");

        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedSendBulkReqMsg: Send message to RM failed"));
        return;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pDhcp6RlyDataDesc - This is Dhcp6Rly sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pDhcp6RlyDbNode - This is db node defined in the DHCP6RLY*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedDbUtilAddTblNode (tDbTblDescriptor * pDhcp6RlyDataDesc,
                             tDbTblNode * pDhcp6RlyDbNode)
{
    if ((DHCP6RLY_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gDhcp6RlyRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pDhcp6RlyDataDesc, pDhcp6RlyDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate DHCP6RLY dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedSyncDynInfo (VOID)
{
    if ((DHCP6RLY_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gDhcp6RlyRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleNodes (&gDhcp6RlyDynInfoList);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Dhcp6RlyRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Dhcp6RlyRedAddAllNodeInDbTbl (VOID)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;

    /*calling the rbtree getfirst and rbtree getnext ,followed by UtilAddTblNode */
    pDhcp6RlyPDEntry = D6RlPDGetFirstNode ();
    while (pDhcp6RlyPDEntry != NULL)
    {
        Dhcp6RlyRedDbUtilAddTblNode (&(gDhcp6RlyDynInfoList),
                                     &
                                     (pDhcp6RlyPDEntry->
                                      RbPrefixDelegationDbNode));

        pDhcp6RlyPDEntry =
            D6RlPDGetNextNode (&(pDhcp6RlyPDEntry->Prefix),
                               pDhcp6RlyPDEntry->u1PrefixLen);
    }
    return;
}

/* cleared */
/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedSendBulkUpdMsg (VOID)
{
    if (DHCP6RLY_IS_STANDBY_UP () == OSIX_FALSE)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedSendBulkUpMsg:DHCP6RLY Standby up failure \r\n");
        if (DHCP6_RLY_SYS_LOG_OPTION == OSIX_TRUE)
        {
            DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyRedSendBulkUpMsg:DHCP6RLY stand by up failure"));
        }
        return;
    }

    if (gDhcp6RlyRedGblInfo.u1BulkUpdStatus == DHCP6RLY_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        Dhcp6RlyRedAddAllNodeInDbTbl ();

        Dhcp6RlyRedSyncDynInfo ();

        gDhcp6RlyRedGblInfo.u1BulkUpdStatus = DHCP6RLY_HA_UPD_COMPLETED;
        Dhcp6RlyRedSendBulkUpdTailMsg ();
    }

    return;

}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the DHCP6RLY offers an IP address         */
/*                      to the DHCP6RLY client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pDhcp6RlyBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Dhcp6RlyRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSetPass = 0;
    UINT4               u4OffSet = 0;
    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * DHCP6RLY_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (DHCP6RLY_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedSendBulkUpdTailMsg: RM Memory allocation"
                       " failed\r\n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedSendBulkUpdTailMsg: RM Memory allocation "
                            "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    DHCP6RLY_RM_PUT_1_BYTE (pMsg, &u4OffSet,
                            DHCP6RLY_RED_BULK_UPD_TAIL_MESSAGE);
    DHCP6RLY_RM_PUT_2_BYTE (pMsg, &u4OffSet,
                            DHCP6RLY_RED_BULK_UPD_TAIL_MSG_SIZE);

    u2OffSetPass = (UINT2) u4OffSet;

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (Dhcp6RlyRedSendMsgToRm (pMsg, u2OffSetPass) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "Dhcp6RlyRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        DHCP6_RLY_SYS_LOG ((SYSLOG_ERROR_LEVEL, DHCP6_RLY_SYSLOG_ID,
                            "Dhcp6RlyRedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Dhcp6RlyRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_DHCP6RLY_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                   "Dhcp6RlyRedProcessBulkTailMsg: Bulk Update Tail Message received"
                   " at Standby node.\r\n");
    DHCP6_RLY_SYS_LOG ((SYSLOG_INFO_LEVEL, DHCP6_RLY_SYSLOG_ID,
                        "Dhcp6RlyRedProcessBulkTailMsg: Bulk Update Tail Message"
                        " received at Standby node"));
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);
    return;
}

/************************************************************************/
/* Function Name      : Dhcp6RlyRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Dhcp6RlyRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tDhcp6RlyPDEntry   *pDhcp6RlyPDEntry = NULL;
    tIp6Addr            Ipv6Prefix;
    tIp6Addr            NextHop;
    UINT1               u1Version = 0;
    UINT4               u4TransactionId = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4RouteIndex = 0;
    UINT4               u4T1Time = 0;
    UINT4               u4T2Time = 0;
    UINT4               u4ValidTime = 0;
    UINT2               u2Option = 0;
    UINT1               u1PrefixLength = 0;
    UINT1               u1Flag = 0;
    UINT1               aTempPad[3];
    UINT4               u4OffSet = 0;
    UINT1               u1CommandType = 0;

    MEMSET (&Ipv6Prefix, 0, sizeof (tIp6Addr));
    MEMSET (&NextHop, 0, sizeof (tIp6Addr));
    MEMSET (aTempPad, 0, sizeof (aTempPad));

    /* First check the first byte. If first byte is 2, ACTIVE is
     * in sync with STANDBY. Else, ACTIVE is in older version.
     * Decoding should be done based on this. */
    RM_GET_DATA_1_BYTE (pMsg, *pu2OffSet, u1Version);

    u4OffSet = (*pu2OffSet) + (UINT4) 1;

    if (u1Version == DHCP6RLY_HA_VERSION)
    {
        DHCP6RLY_RM_GET_N_BYTE (pMsg, &u4OffSet, aTempPad, 3);
        DHCP6RLY_RM_GET_N_BYTE (pMsg, &u4OffSet, (UINT1 *) &(Ipv6Prefix),
                                DHCP6_RLY_IP6_ADDR_LEN);
        DHCP6RLY_RM_GET_N_BYTE (pMsg, &u4OffSet, (UINT1 *) &(NextHop),
                                DHCP6_RLY_IP6_ADDR_LEN);
        DHCP6RLY_RM_GET_4_BYTE (pMsg, &u4OffSet, u4TransactionId);
        DHCP6RLY_RM_GET_4_BYTE (pMsg, &u4OffSet, u4MsgType);
        DHCP6RLY_RM_GET_4_BYTE (pMsg, &u4OffSet, u4RouteIndex);
        DHCP6RLY_RM_GET_4_BYTE (pMsg, &u4OffSet, u4T1Time);
        DHCP6RLY_RM_GET_4_BYTE (pMsg, &u4OffSet, u4T2Time);
        DHCP6RLY_RM_GET_4_BYTE (pMsg, &u4OffSet, u4ValidTime);
        DHCP6RLY_RM_GET_1_BYTE (pMsg, &u4OffSet, u1PrefixLength);
        DHCP6RLY_RM_GET_1_BYTE (pMsg, &u4OffSet, u1Flag);
        DHCP6RLY_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Option);
    }

    /*At first,create the memblock for node,add the route to RBTree,
       followed by addition to RTM6 and then start the timer */

    /* check if reply message */
    if (u4MsgType == DHCP6_SERVER_REPLY)
    {
        pDhcp6RlyPDEntry = D6RlPDGetNode (&Ipv6Prefix, u1PrefixLength);

        if (pDhcp6RlyPDEntry == NULL)
        {
            if (u4ValidTime != 0)
            {
                pDhcp6RlyPDEntry = D6RlPDCreateNode ();

                if (pDhcp6RlyPDEntry == NULL)
                {
                    return;
                }

                MEMCPY (&(pDhcp6RlyPDEntry->Prefix), &(Ipv6Prefix),
                        sizeof (tIp6Addr));
                MEMCPY (&(pDhcp6RlyPDEntry->NextHop), &(NextHop),
                        sizeof (tIp6Addr));
                pDhcp6RlyPDEntry->u1PrefixLen = u1PrefixLength;
                pDhcp6RlyPDEntry->u4RtIndex = u4RouteIndex;
                pDhcp6RlyPDEntry->u4T1Time = u4T1Time;
                pDhcp6RlyPDEntry->u4T2Time = u4T2Time;
                pDhcp6RlyPDEntry->u4ValidLifeTime = u4ValidTime;
                pDhcp6RlyPDEntry->u4TransactionId = u4TransactionId;
                pDhcp6RlyPDEntry->u4MsgType = u4MsgType;
                pDhcp6RlyPDEntry->u2Option = u2Option;
                pDhcp6RlyPDEntry->u1Flag = u1Flag;
                pDhcp6RlyPDEntry->u1Version = u1Version;

                if (D6RlPDAddNode (pDhcp6RlyPDEntry) != OSIX_SUCCESS)
                {
                    D6RlPDDelNode (pDhcp6RlyPDEntry);
                    return;
                }

                u1CommandType = NETIPV6_ADD_ROUTE;

                if (OSIX_FAILURE ==
                    D6RlLeakPDRoute (pDhcp6RlyPDEntry, u1CommandType))
                {
                    DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                        "Dhcp6RlyRedProcessDynamicInfo: in route addition flow :Unable to add route for prefix : %s ,prefix length: %d ,nexthop : %s\r\n",
                                        Ip6PrintAddr (&Ipv6Prefix),
                                        u1PrefixLength,
                                        Ip6PrintAddr (&NextHop));
                    DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL,
                                        DHCP6_RLY_SYSLOG_ID,
                                        "Dhcp6RlyRedProcessDynamicInfo: in route addition flow :Unable to add route for prefix : %s ,prefix length: %d ,nexthop : %s\r\n",
                                        Ip6PrintAddr (&Ipv6Prefix),
                                        u1PrefixLength,
                                        Ip6PrintAddr (&NextHop)));

                    return;
                }
            }
        }

        /*This check is added to test, reply message is in response to renew/rebind,
           if any prefix with txid is available for renew/rebind, with IA_PD's T1=T2=0,
           then route should be deleted. */

        else if ((u4TransactionId != 0) && u4T1Time == 0 && u4T2Time == 0
                 && u1Flag == 1)
        {
            /*  this deletion scenario requires only prefix and prefix Length */

            if (OSIX_FAILURE ==
                D6RlPDNodeDeletion (&(Ipv6Prefix), u1PrefixLength))
            {
                DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                    "Dhcp6RlyRedProcessDynamicInfo:failed in Rebind in unable to delete prefix in Rbtee for TxID %u, prefix: %s, prefix length :%u\r\n",
                                    u4TransactionId, Ip6PrintAddr (&Ipv6Prefix),
                                    u1PrefixLength);

                DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                    "Dhcp6RlyRedProcessDynamicInfo:failed in Rebind in unable to delete prefix in Rbtee for TxID %u, prefix: %s, prefix length :%u\r\n",
                                    u4TransactionId, Ip6PrintAddr (&Ipv6Prefix),
                                    u1PrefixLength));

                return;
            }
        }
        /*if reply message is with status option for particular node, then it should be deleted */
        else if ((u4TransactionId != 0)
                 && (u2Option == DHCP6_RLY_STATUS_MSG_OPTION))
        {
            if (OSIX_FAILURE ==
                D6RlPDNodeDeletion (&(Ipv6Prefix), u1PrefixLength))
            {
                DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                    "Dhcp6RlyRedProcessDynamicInfo: failed in Nobind status: unable to delete prefix in Rbtee for TxId : %u, prefix %s, prefix length %u\r\n",
                                    u4TransactionId, Ip6PrintAddr (&Ipv6Prefix),
                                    u1PrefixLength);
                return;
            }

        }

        /* if Timer expired and route is removed in other node, sync up is done for that */
        else if (u4ValidTime == 0)
        {
            if (OSIX_FAILURE ==
                D6RlPDNodeDeletion (&(Ipv6Prefix), u1PrefixLength))
            {
                DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                    "Dhcp6RlyRedProcessDynamicInfo: failed validtime equals zero: unable to delete prefix in Rbtee for TxId : %u, prefix %s, prefix length %u\r\n",
                                    u4TransactionId, Ip6PrintAddr (&Ipv6Prefix),
                                    u1PrefixLength);
                return;
            }
        }
    }
    /* check if client's decline message or release message,
       then those prefixes has to be removed from RBtree  */
    else if (u4MsgType == DHCP6_CLIENT_DECLINE
             || u4MsgType == DHCP6_CLIENT_RELEASE)
    {
        if (OSIX_FAILURE == D6RlPDNodeDeletion (&(Ipv6Prefix), u1PrefixLength))
        {
            DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                "Dhcp6RlyRedProcessDynamicInfo:failed in Renew/rebind update txid: unable to add txid in RbtreeNode for prefix :%s ,prefix length :%d\r\n",
                                u4MsgType, Ip6PrintAddr (&Ipv6Prefix),
                                u1PrefixLength);

            DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyRedProcessDynamicInfo:failed in Renew/rebind update txid: unable to add txid in RbtreeNode for prefix :%s ,prefix length :%d\r\n",
                                u4MsgType, Ip6PrintAddr (&Ipv6Prefix),
                                u1PrefixLength));
            return;

        }
    }

    else if (((u4MsgType == DHCP6_CLIENT_RENEW)
              || (u4MsgType == DHCP6_CLIENT_REBIND)) && (u4TransactionId != 0))
    {
        /*fill the transaction id for prefix and prefix Length in RBTree,
           that has been parsed above */
        if (OSIX_FAILURE ==
            D6RlPDRouteFillTxId (&(Ipv6Prefix), u1PrefixLength, u4TransactionId,
                                 u4MsgType))
        {
            DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_FAILURE_TRC,
                                "Dhcp6RlyRedProcessDynamicInfo : unable to fill txid for MsgType: %u, prefix: %s ,prefix length: %u\r\n",
                                u4MsgType, Ip6PrintAddr (&Ipv6Prefix),
                                u1PrefixLength);

            DHCP6_RLY_SYS_LOG ((SYSLOG_CRITICAL_LEVEL, DHCP6_RLY_SYSLOG_ID,
                                "Dhcp6RlyRedProcessDynamicInfo : unable to fill txid for MsgType: %u, prefix: %s ,prefix length: %u\r\n",
                                u4MsgType, Ip6PrintAddr (&Ipv6Prefix),
                                u1PrefixLength));
            return;
        }
    }
    return;
}

#endif
