/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlpars.c,v 1.37 2017/09/08 12:22:45 siva Exp $
 *
 * DESC : Contains the Api routines called by external modules
 ***************************************************************************/

#include "d6rlinc.h"
 /* External function prototypes */
extern INT4         NetIpv4GetCfaIfIndexFromPort (UINT4 u4Port,
                                                  UINT4 *pu4IfIndex);
PUBLIC INT1         VlanGetPortsInPortList (UINT2 u2VlanId, UINT2 *u2PortCount,
                                            UINT2 *au2PortArray);
/* Private function prototypes */
PRIVATE INT4
    D6RlParsConstRlyFwdAndSend PROTO ((tDhcp6RlyUdp6Params * pUdp6Dhcp6RParams,
                                       UINT1 *pu1Dhcp6Buf, UINT2 u2Len,
                                       UINT1 u1Hops));
PRIVATE INT4
     
     
     
     D6RlParsProcessRlyRpy
PROTO ((tDhcp6RlyUdp6Params * pUdp6Dhcp6RParams, UINT1 *pu1Dhcp6Buf,
        UINT2 u2Len));
PRIVATE INT4        D6RlParsSendToDest
PROTO ((tDhcp6RlyDestInfo * pDhcp6RlyDest, UINT1 *pu1SendBuf, UINT2 u2Len));
PRIVATE INT4        D6RlParsSendToServer
PROTO ((UINT4 u4Index, UINT1 *pu1SendBuf, UINT4 u4Size));

PRIVATE VOID        D6RlParsSendAsMulti
PROTO ((UINT4 u4InIfIndex, tIp6Addr * pSrvAddr, UINT1 *pu1SendBuf,
        UINT4 u4Size));

/**************************************************************************/
/*   Function Name   : D6RlParsProcessPkt                                 */
/*   Description     : This function finds the incoming dhcpv6 pkt type   */
/*                     and calls appropriate routine to process.          */
/*                                                                        */
/*   Input(s)        : pUdp6Dhcp6Params - pointer to udp params struct    */
/*                     pu1Dhcp6Buf - The Dhcp6 pkt recvd on the interface */
/*                     u2Len       - Length of the Dhcpv6 pkt             */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/

PUBLIC INT4
D6RlParsProcessPkt (tDhcp6RlyUdp6Params * pUdp6Dhcp6RParams,
                    UINT1 *pu1Dhcp6Buf, UINT2 u2Len)
{
    tDhcp6RlyInvalidMsgRcvdTrap InvMsgTrap;
    tDhcp6RlyHopThresholdTrap MaxHopTrap;
    tDhcp6RlyIfInfo    *pInterface = NULL;
    tDhcp6RlySrvAddrInfo *pServerEntry = NULL;
    UINT1               u1Hops = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4Offset = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4TxId = 0;
    UINT4               u4ClientMsgOffset = 0;
    UINT1               u1Type = 0;
    UINT4               u4TempMsgOffset = 0;
    UINT2               u2TempLen = 0;
    UINT4               u4TempMsgType = 0;

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "DHCP6 packet processing starts \r\n");
    /*Obtain the Msgtype in UINT4 for stateful client messages - release,decline,renew,rebind */
    DHCP6_RLY_GET_4_BYTE (pu1Dhcp6Buf, u4ClientMsgOffset, &u4MsgType);

    /*since msg-type and transaction-id of 4 bytes is parsed ,so u2Len-4 */
    u4TxId = u4MsgType & DHCP6_RLY_MASK_TRANSACTION_ID;
    u4MsgType = u4MsgType & DHCP6_RLY_MASK_MSG_TYPE;

    /* Discard if the destination address is server's only
     */

    /* Check whther valid packet type is received 
     * Type 11, 12 and 13 are only valid at relay*/

    DHCP6_RLY_GET_1_BYTE (pu1Dhcp6Buf, u4Offset, &u1Type);
    DHCP6_RLY_GET_1_BYTE (pu1Dhcp6Buf, u4Offset, &u1Hops);

    if ((u1Type == DHCP6_RLY_RELAY_RPY) &&
        (Rtm6Ip6IsFreeRtTblEntriesAvailable () != IP6_SUCCESS))
    {
        return OSIX_FAILURE;
    }

    if ((u4MsgType != DHCP6_CLIENT_RELEASE)
        && (u4MsgType != DHCP6_CLIENT_DECLINE)
        && (u4MsgType != DHCP6_CLIENT_REBIND)
        && (u4MsgType != DHCP6_CLIENT_RENEW))
    {
        /* msg has relay header before client header, hence jump the message type (1), hopcount (1), link address (16),
         *  peer address (16) and extra 2 byte option and length 2 bytes */

        u4TempMsgOffset = DHCP6_CLIENT_RLY_SKIP_OFFSET;
        /* To get length of client header, which is last 2 bytes */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4TempMsgOffset, &u2TempLen);
        DHCP6_RLY_GET_4_BYTE (pu1Dhcp6Buf, u4TempMsgOffset, &u4TempMsgType);

        u4TempMsgType = u4TempMsgType & DHCP6_RLY_MASK_MSG_TYPE;
        if (((u4TempMsgType == DHCP6_CLIENT_RELEASE)
             || (u4TempMsgType == DHCP6_CLIENT_DECLINE)
             || (u4TempMsgType == DHCP6_CLIENT_RENEW)
             || (u4TempMsgType == DHCP6_CLIENT_REBIND)))
        {
            u4ClientMsgOffset = u4TempMsgOffset;
            u2Len = u2TempLen;
            u4MsgType = u4TempMsgType;
            u4TxId = u4MsgType & DHCP6_RLY_MASK_TRANSACTION_ID;

        }

    }
    pInterface = D6RlIfGetNode (pUdp6Dhcp6RParams->u4Index);

    if (pInterface != NULL)
    {
        switch (u1Type)
        {
            case DHCP6_RLY_CLIENT_REQ:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT INFORMATION REQ pkt received on relay\r\n");
                pInterface->u4InformIn++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT INFORMATION  REQ pkt received: %d\r\n",
                                    pInterface->u4InformIn);
                break;
            }
            case DHCP6_CLIENT_SOLICIT:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT SOLICIT pkt received on relay\r\n");
                pInterface->u4Solicit++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT SOLICIT  pkt received: %u\r\n",
                                    pInterface->u4Solicit);
                break;
            }
            case DHCP6_CLIENT_REQUEST:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT REQ pkt received on relay\r\n");
                pInterface->u4Request++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT REQ pkt received: %u\r\n",
                                    pInterface->u4Request);
                break;
            }
            case DHCP6_CLIENT_REBIND_SF:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT REBIND  pkt received on relay\r\n");
                pInterface->u4Rebind++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT REBIND pkt received: %u\r\n",
                                    pInterface->u4Rebind);
                break;
            }
            case DHCP6_CLIENT_RENEW_SF:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT RENEW  pkt received on relay\r\n");
                pInterface->u4Renew++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT RENEW pkt received: %u\r\n",
                                    pInterface->u4Rebind);
                break;
            }
            case DHCP6_CLIENT_RELEASE_SF:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT RELEASE  pkt received on relay\r\n");
                pInterface->u4Release++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT RELEASE pkt received: %u\r\n",
                                    pInterface->u4Release);
                break;
            }
            case DHCP6_CLIENT_DECLINE_SF:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT DECLINE pkt received on relay\r\n");
                pInterface->u4Decline++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT DECLINE pkt received: %u\r\n",
                                    pInterface->u4Decline);
                break;
            }
            case DHCP6_SERVER_RECONFIGURE_SF:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT RECONFIG pkt received on relay\r\n");
                pInterface->u4Reconfig++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT RECONIG pkt received: %u\r\n",
                                    pInterface->u4Reconfig);
                break;
            }
            case DHCP6_CLIENT_CONFIRM_SF:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 CLIENT CONFIRM pkt received on relay\r\n");
                pInterface->u4Confirm++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 CLIENT CONFIRM pkt received: %u\r\n",
                                    pInterface->u4Confirm);
                break;
            }

            case DHCP6_RLY_RELAY_FWD:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 RELAY FORWARD pkt received on relay\r\n");
                pInterface->u4RelayForwIn++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 RELAY FORWARD pkt received: %u\r\n",
                                    pInterface->u4RelayForwIn);
                break;
            }

            case DHCP6_RLY_RELAY_RPY:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "DHCP6 RELAY REPLY pkt received on relay\r\n");
                pInterface->u4RelayRepIn++;
                DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                    "No.of DHCP6 RELAY FORWARD pkt received: %u\r\n",
                                    pInterface->u4RelayRepIn);
                break;
            }

            default:
            {
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "Invalid DHCP6 packet " "received on relay\r\n");
                pInterface->u4InvalidPktIn++;
                MEMSET (&InvMsgTrap, 0, sizeof (tDhcp6RlyInvalidMsgRcvdTrap));
                InvMsgTrap.u4MsgType = (UINT4) u1Type;
                InvMsgTrap.u4IfIndex = pUdp6Dhcp6RParams->u4Index;
                D6RlTrapSnmpSend ((VOID *) &InvMsgTrap,
                                  DHCP6_RLY_RCVD_INVALID_MSG_TRAP_VAL);
                return OSIX_FAILURE;
                break;
            }
        }                        /* End of switch-case */
    }

    /* Check whether the interface supports the messsage type.
     * If relay is enabled on the interface, then accept type 11, 12 and 13.
     * Else accept only type 13 */

    if (pInterface == NULL)
    {
        if (u1Type != DHCP6_RLY_RELAY_RPY)
        {
            DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                           "Invalid DHCP6 packet received on relay\r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* If Server is not present then it is multicast, 
         * in this case no need to check the Hop count */
        pServerEntry = D6RlIfServerListGetFirstNode (pInterface);
        if (pServerEntry != NULL)
        {
            if ((u1Type == DHCP6_RLY_RELAY_FWD)
                || (u1Type == DHCP6_RLY_RELAY_RPY))
            {
                if (u1Hops >= pInterface->u1HopThreshold)
                {
                    MEMSET (&MaxHopTrap, 0, sizeof (tDhcp6RlyHopThresholdTrap));
                    MaxHopTrap.u4IfIndex = pUdp6Dhcp6RParams->u4Index;
                    D6RlTrapSnmpSend (&MaxHopTrap,
                                      DHCP6_RLY_HOP_THRESHOLD_TRAP_TRAP_VAL);
                    return OSIX_FAILURE;
                }
            }
        }
    }

    /* Conditional check added for PD Handling of Stateful DHCPv6 client  messages */
    if ((u1Type == DHCP6_RLY_CLIENT_REQ) || (u1Type == DHCP6_CLIENT_SOLICIT)
        || (u1Type == DHCP6_CLIENT_REQUEST)
        || (u4MsgType == DHCP6_CLIENT_RELEASE)
        || (u4MsgType == DHCP6_CLIENT_DECLINE)
        || (u4MsgType == DHCP6_CLIENT_REBIND)
        || (u4MsgType == DHCP6_CLIENT_RENEW))
    {
        /* this ParsPDInfo logic is not applicable to Information Request,
           and solicit may contain IA_PD,but not IA_PD prefix options */
        if ((DHCP6_RLY_PDROUTE_STATUS == DHCP6_RLY_PDROUTE_ENABLE)
            && ((u4MsgType == DHCP6_CLIENT_RELEASE)
                || (u4MsgType == DHCP6_CLIENT_DECLINE)
                || (u4MsgType == DHCP6_CLIENT_RENEW)
                || (u4MsgType == DHCP6_CLIENT_REBIND)))
        {
            /*since msg-type and transaction-id of 4 bytes is parsed ,so u2Len-4 */
            D6RlParsPDInfo (pu1Dhcp6Buf + u4ClientMsgOffset,
                            (UINT2) (u2Len - 4), u4MsgType, u4TxId, 0, NULL);
        }
        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "DHCP6 RLY FORWARD packet to be forwarded to the server\r\n");
        i4RetVal = D6RlParsConstRlyFwdAndSend (pUdp6Dhcp6RParams,
                                               pu1Dhcp6Buf, u2Len, (UINT1) 0);
    }
    else if (u1Type == DHCP6_RLY_RELAY_FWD)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "DHCP6 RLY FORWARD packet to be forwarded to the server\r\n");
        i4RetVal = D6RlParsConstRlyFwdAndSend (pUdp6Dhcp6RParams,
                                               pu1Dhcp6Buf, u2Len,
                                               (UINT1) (u1Hops + 1));
    }
    else if (u1Type == DHCP6_RLY_RELAY_RPY)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "DHCP6 RLY REPLY packet to be forwarded to the client\r\n");
        i4RetVal = D6RlParsProcessRlyRpy (pUdp6Dhcp6RParams,
                                          pu1Dhcp6Buf, u2Len);
    }

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "DHCP6 packet processing ends \r\n");
    return i4RetVal;
}

/**************************************************************************/
/*   Function Name   : D6RlParsConstRlyFwdAndSend                          */
/*   Description     : This function constructs the Rly Fwd Pkt           */
/*                     and forwards that to appropriate server.           */
/*                                                                        */
/*   Input(s)        : pUdp6Dhcp6Params - pointer to udp params struct    */
/*                     pu1Dhcp6Buf - The Dhcp6 pkt recvd on the interface */
/*                     u2Len       - Length of the Dhcpv6 pkt             */
/*                     u1Hops      - Relay hop count                      */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/

PRIVATE INT4
D6RlParsConstRlyFwdAndSend (tDhcp6RlyUdp6Params * pUdp6Dhcp6RParams,
                            UINT1 *pu1Dhcp6Buf, UINT2 u2Len, UINT1 u1Hops)
{

    tIp6Addr            ifGlobalAddr;
    UINT4               u4Offset = 0;
    UINT4               u4EntNumber = 0;
    UINT2               u2RemIdLen = 0;
    UINT2               u2RemIdOpLen = 0;
    UINT2               u2OptionType = 0;
    UINT2               u2OptionLen = 0;
    UINT1               u1Type = DHCP6_RLY_RELAY_FWD;
    UINT1               u1IfOptionReq = OSIX_FALSE;
    INT4                i4RetRemIdOptionType = 0;

    UINT4               u4IpAddr = 0;

    tDhcp6RlyRemIdOption Dh6RlyRemIdBuf;
    tDhcp6RlyRemIdOption *pDh6RlyRemIdBuf = NULL;

    MEMSET (&Dh6RlyRemIdBuf, 0, sizeof (tDhcp6RlyRemIdOption));
    MEMSET (&ifGlobalAddr, 0, sizeof (tIp6Addr));

    /* Initialize the send buffer */
    MEMSET (DHCP6_RLY_SEND_BUF, 0, DHCP6_MAX_MTU);

    pDh6RlyRemIdBuf = &Dh6RlyRemIdBuf;

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "DHCP6 RLY FORWARD packet construction starts \r\n");

    /* Initialize the Link Addr */

    /* Construct Relay Fwd 
       _________________________________________________________________________
       |        |       |              |              |        |                 |
       | Type12 | Hop   | Link Address | Peer Address | Option | Dhcpv6 Inf.Req  |
       | 1 Byte | 1 Byte| 16 Bytes     | 16 Bytes     | Var len|  (As Rcvd)      |
       |        |       |              |              |        |                 |
       ---------------------------------------------------------------------------

     */

    /* Set the Message type as Relay Forward */
    DHCP6_RLY_PUT_1_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u1Type);

    /* Set the hop count as zero */
    DHCP6_RLY_PUT_1_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u1Hops);

    DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                        "Hop count: %d is included in the packet\r\n", u1Hops);

    /* Set Interface Id for all the cases */
    u1IfOptionReq = OSIX_TRUE;

    /* Set the Link Address */
    if (!IS_ADDR_LLOCAL (pUdp6Dhcp6RParams->srcAddr))
    {
        /* Set the Link Addr */
        DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF,
                              &pUdp6Dhcp6RParams->srcAddr.ip6_addr_u.u1ByteAddr,
                              u4Offset, DHCP6_RLY_IP6_ADDR_LEN);
        DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                            "Link Address: %s is included in the packet\r\n",
                            Ip6PrintAddr (&pUdp6Dhcp6RParams->srcAddr));
    }
    else
    {
        /* Get the Address of the Interface */
        if (D6RlUtlGetGlobalAddr (pUdp6Dhcp6RParams->u4Index,
                                  NULL, &ifGlobalAddr) == OSIX_SUCCESS)
        {
            /* Set the Link Addr */
            DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF, &ifGlobalAddr,
                                  u4Offset, DHCP6_RLY_IP6_ADDR_LEN);
            DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                                "Link Address: %s is included in the packet\r\n",
                                Ip6PrintAddr (&ifGlobalAddr));
        }
        else
        {
            u4Offset += DHCP6_RLY_IP6_ADDR_LEN;
        }
    }
    /* Set the peer address */
    DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF,
                          &pUdp6Dhcp6RParams->srcAddr.ip6_addr_u.u1ByteAddr,
                          u4Offset, DHCP6_RLY_IP6_ADDR_LEN);

    DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                        "Peer Address: %s is included in the packet\r\n",
                        Ip6PrintAddr (&pUdp6Dhcp6RParams->srcAddr));

    /* Add the interface option if needed */
    if (u1IfOptionReq == OSIX_TRUE)
    {
        /* TYPE */
        u2OptionType = DHCP6_RLY_IF_ID_OPTION;
        DHCP6_RLY_PUT_2_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u2OptionType);

        /* LENGTH */
        u2OptionLen = DHCP6_RLY_IF_ID_LEN;
        DHCP6_RLY_PUT_2_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u2OptionLen);

        /* VALUE */
        DHCP6_RLY_PUT_4_BYTE (DHCP6_RLY_SEND_BUF, u4Offset,
                              &pUdp6Dhcp6RParams->u4Index);
        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "Interface option TLV is included in the packet \r\n");
    }

    /* Add the relay message option */
    /* TYPE */
    u2OptionType = DHCP6_RLY_RELAY_MSG_OPTION;
    DHCP6_RLY_PUT_2_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u2OptionType);

    /* LENGTH */
    DHCP6_RLY_PUT_2_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u2Len);

    /* VALUE */
    DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF, pu1Dhcp6Buf, u4Offset, u2Len);

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "Relay message option TLV is included in the packet \r\n");

    /* Add the Remote-Id option */
    if (D6RlParsRemIdOption (pUdp6Dhcp6RParams->u4Index,
                             &i4RetRemIdOptionType,
                             pDh6RlyRemIdBuf) == OSIX_SUCCESS)
    {

        /* TYPE */
        u2OptionType = DHCP6_RLY_REM_ID_OPTION;
        DHCP6_RLY_PUT_2_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u2OptionType);

        /* LENGTH */
        u2RemIdLen = pDh6RlyRemIdBuf->i4RemIdLen;
        u2RemIdOpLen = DHCP6_RLY_ENT_NUM_LEN + u2RemIdLen;
        DHCP6_RLY_PUT_2_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u2RemIdOpLen);

        /* Enterprise Number */
        u4EntNumber = DHCP6_RLY_ENTPRISE_NUMBER;
        DHCP6_RLY_PUT_4_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u4EntNumber);

        /* VALUE */
        if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_DUID)
        {
            DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF,
                                  pDh6RlyRemIdBuf->au1Duid,
                                  u4Offset, u2RemIdLen);
        }
        else if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_SW_NAME)
        {
            DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF,
                                  pDh6RlyRemIdBuf->au1SwName,
                                  u4Offset, u2RemIdLen);
        }
        else if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_MGMT_IP)
        {
            u4IpAddr = pDh6RlyRemIdBuf->u4MgmtIpAddr;
            DHCP6_RLY_PUT_4_BYTE (DHCP6_RLY_SEND_BUF, u4Offset, &u4IpAddr);
        }
        else if (i4RetRemIdOptionType == DHCP6_RLY_REMOTEID_USERDEFINED)
        {
            DHCP6_RLY_PUT_N_BYTE (DHCP6_RLY_SEND_BUF,
                                  pDh6RlyRemIdBuf->au1UserDefined,
                                  u4Offset, u2RemIdLen);
        }
        DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                            "Relay message option TLV is included in the packet"
                            "with Enterprise Number: %u \r\n", u4EntNumber);
    }

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "DHCP6 RLY FORWARD packet is constructed and "
                   "ready for sending to server\r\n");

    return D6RlParsSendToServer (pUdp6Dhcp6RParams->u4Index,
                                 DHCP6_RLY_SEND_BUF, u4Offset);
}

/**************************************************************************/
/*   Function Name   : D6RlParsProcessRlyRpy                               */
/*   Description     : This function Processes the Relay Rpy Packet       */
/*                     and forwards that to appropriate client.           */
/*                                                                        */
/*   Input(s)        : pUdp6Dhcp6Params - pointer to udp params struct    */
/*                     pu1Dhcp6Buf - The Dhcp6 pkt recvd on the interface */
/*                     u2Len       - Length of the Dhcpv6 pkt             */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/

PRIVATE INT4
D6RlParsProcessRlyRpy (tDhcp6RlyUdp6Params * pUdp6Dhcp6RParams,
                       UINT1 *pu1Dhcp6Buf, UINT2 u2Len)
{
    tDhcp6RlyDestInfo   Dhcp6RlyDestInfo;
    UINT4               u4Index = 0;
    UINT4               u4MsgOffset = 0;
    UINT4               u4Offset = 0;
    UINT2               u2OptionType = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OptionLen = 0;
    UINT1               u1HopCount = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4TxId = 0;
    UINT4               u4TempMsgOffset = 0;
    UINT2               u2TempLen = 0;

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "DHCP6 RLY REPLY packet processing starts\r\n");

    /* Initialize the send buffer */
    MEMSET (DHCP6_RLY_SEND_BUF, 0, DHCP6_RLY_MAX_MTU_SIZE);

    /* Initialize Relay reply header structure */
    MEMSET (&Dhcp6RlyDestInfo, 0, sizeof (tDhcp6RlyDestInfo));

    /* Strip Relay Reply header 
       _______________________________________________________  __________________
       |        |       |              |              |        > >                 |
       | Type13 | Hop   | Link Address | Peer Address | Option > > Dhcpv6 Inf.Req  |
       | 1 Byte | 1 Byte| 16 Bytes     | 16 Bytes     | Var len> >  (As Rcvd)      |
       |________|_______|______________|______________|________> >_________________|

     */

    /* Skip the message type since we know it */
    u4Offset += 1;

    /* Get the hop count */
    DHCP6_RLY_GET_1_BYTE (pu1Dhcp6Buf, u4Offset, &u1HopCount);

    /* Get the Link Address */
    DHCP6_RLY_GET_N_BYTE (pu1Dhcp6Buf, (UINT1 *) &Dhcp6RlyDestInfo.LinkAddr,
                          u4Offset, DHCP6_RLY_IP6_ADDR_LEN);

    /* Get the Peer Address */
    DHCP6_RLY_GET_N_BYTE (pu1Dhcp6Buf, (UINT1 *) &Dhcp6RlyDestInfo.PeerAddr,
                          u4Offset, DHCP6_RLY_IP6_ADDR_LEN);

    DHCP6_RLY_TRC_ARG3 (DHCP6_RLY_CONTROL_PLANE_TRC,
                        "REPLY packet is received with Hop count:  %d, LinkAddr:  %s , PeerAddr:%s \r\n",
                        u1HopCount,
                        Ip6PrintAddr (&Dhcp6RlyDestInfo.LinkAddr),
                        Ip6PrintAddr (&Dhcp6RlyDestInfo.PeerAddr));

    DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionType);

    if ((u2OptionType != DHCP6_RLY_IF_ID_OPTION) &&
        (u2OptionType != DHCP6_RLY_RELAY_MSG_OPTION))
    {
        return OSIX_FAILURE;
    }

    if (u2OptionType == DHCP6_RLY_IF_ID_OPTION)
    {
        /* TYPE is already got as DHCP6_RLY_IF_ID_OPTION */

        /* LENGTH */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionLen);
        if (u2OptionLen != DHCP6_RLY_IF_ID_LEN)
        {
            return OSIX_FAILURE;
        }

        /* VALUE */
        DHCP6_RLY_GET_4_BYTE (pu1Dhcp6Buf, u4Offset, &u4Index);
        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "Interface option TLV processed successfully \r\n");

        /* Check the relay message option */
        /* TYPE */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionType);
        if (u2OptionType != DHCP6_RLY_RELAY_MSG_OPTION)
        {
            return OSIX_FAILURE;
        }

        /* LENGTH */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionLen);
        if ((u4Offset + u2OptionLen) != u2Len)
        {
            return OSIX_FAILURE;
        }

        u2MsgLen = u2OptionLen;

        /* parse to check if reply message is received from server,
           which is encapsulated in relay-reply message */

        /* VALUE */
        DHCP6_RLY_GET_4_BYTE (pu1Dhcp6Buf, u4Offset, &u4MsgType);

        /* Message offset currently points to 4 bytes in Relay message header */
        u4MsgOffset = u4Offset;
    }
    else
    {
        /* Message Type comes first, so parse message type and
         * then check Interface option
         */

        /* TYPE is already read as Message type */

        /* LENGTH */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionLen);

        if (u2OptionLen > u2Len)
        {
            return OSIX_FAILURE;
        }

        u2MsgLen = u2OptionLen;

        /* parse to check if reply message is received from server,
           which is encapsulated in relay-reply message */
        /* VALUE */
        DHCP6_RLY_GET_4_BYTE (pu1Dhcp6Buf, u4Offset, &u4MsgType);

        /* Message offset currently points to 4 bytes in Relay message header */
        u4MsgOffset = u4Offset;

        u4Offset = u4Offset - 4;

        u4Offset = u4Offset + u2OptionLen;

        /* Now interface option details to be checked */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionType);

        if (u2OptionType != DHCP6_RLY_IF_ID_OPTION)
        {
            return OSIX_FAILURE;
        }

        /* LENGTH */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionLen);
        if (u2OptionLen != DHCP6_RLY_IF_ID_LEN)
        {
            return OSIX_FAILURE;
        }

        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "Interface option TLV processed successfully \r\n");

        /* VALUE */
        DHCP6_RLY_GET_4_BYTE (pu1Dhcp6Buf, u4Offset, &u4Index);
    }

    Dhcp6RlyDestInfo.u4OutIfIndex = u4Index;
    Dhcp6RlyDestInfo.u4InIfIndex = pUdp6Dhcp6RParams->u4Index;

    /* parse to check if reply message is received from server,
       which is encapsulated in relay-reply message */

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "Relay message option TLV processed successfully \r\n");

    u4TxId = u4MsgType & DHCP6_RLY_MASK_TRANSACTION_ID;
    u4MsgType = u4MsgType & DHCP6_RLY_MASK_MSG_TYPE;

    /* check has been made w.r.t routecontrol mib object if disabled,
       not to call this D6RlParsPDInfo function */
    if (u4MsgType == DHCP6_SERVER_REPLY)
    {
        if (DHCP6_RLY_PDROUTE_STATUS == DHCP6_RLY_PDROUTE_ENABLE)
        {
            /* here PeerAddr is passed to D6RlParsPDInfo,
               which is nexthop for the route to be added in rtm6 */
            /*since msgtype and transaction id are parsed u2OptionLen is reduced to 4 bytes */
            D6RlParsPDInfo ((pu1Dhcp6Buf + u4MsgOffset), (UINT2) (u2MsgLen - 4),
                            u4MsgType, u4TxId, u4Index,
                            &Dhcp6RlyDestInfo.PeerAddr);
        }

    }
    else
    {
        /* When the Relay Reply message is in relay message option. */
        if (DHCP6_RLY_PDROUTE_STATUS == DHCP6_RLY_PDROUTE_ENABLE)
        {
            u4MsgType = DHCP6_SERVER_REPLY;
            u4TxId = u4MsgType & DHCP6_RLY_MASK_TRANSACTION_ID;

            /* from the relay reply message, jump the message type (1), hopcount (1), link address (16),
             *  peer address (16) and extra 2 byte option */
            u4TempMsgOffset = DHCP6_RLY_SKIP_OFFSET;

            /* get the relay message length */

            DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4TempMsgOffset, &u2TempLen);
            if (D6RlParsPDInfo
                (pu1Dhcp6Buf + (u4TempMsgOffset + 4), (UINT2) (u2TempLen - 4),
                 u4MsgType, u4TxId, u4Index,
                 &Dhcp6RlyDestInfo.PeerAddr) == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    /* offset is setback to 4 bytes as before, then sent for D6RlParsSendToDest */
    u4MsgOffset = (UINT4) (u4MsgOffset - 4);
    pu1Dhcp6Buf += u4MsgOffset;

    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "DHCP6 RLY REPLY packet processing ends and "
                   "packet is ready for sending to client\r\n");

#ifndef LNXIP6_WANTED
    D6RlAddNdCache (u4Index, pu1Dhcp6Buf, u2MsgLen);
#endif

    return (D6RlParsSendToDest (&Dhcp6RlyDestInfo, pu1Dhcp6Buf, u2MsgLen));

}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlParsRcvAndProcessPkt                                   */
/*                                                                           */
/* Description  : The function process the packet                            */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6RlParsRcvAndProcessPkt (VOID)
{
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    tDhcp6RlyUdp6Params Udp6Dhcp6RParams;
    tIp6Addr            Ip6Addr;
    UINT4               u4IfIndex;
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4OptEnable = 1;
    struct iovec        Iov;
    UINT1               au1Cmsg[DHCP6_MAX_MTU];    /* For storing Auxillary Data -
                                                   IP6 Packet INFO. */

#endif
    struct cmsghdr      CmsgInfo;
#ifdef SLI_WANTED
    UINT4               u4BufLen = DHCP6_RLY_MAX_MTU_SIZE;
    INT4                i4AddrLen = sizeof (PeerAddr);
#endif
    INT4                i4RetVal = 0;
    INT4                i4RcvCount = 0;
    PRIVATE UINT1       au1Dhcp6RBuf[DHCP6_RLY_MAX_MTU_SIZE];

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Udp6Dhcp6RParams, 0, sizeof (tDhcp6RlyUdp6Params));
    MEMSET (au1Dhcp6RBuf, 0, DHCP6_RLY_MAX_MTU_SIZE);

    SET_ADDR_UNSPECIFIED (Ip6Addr);
    Ip6AddrCopy ((tIp6Addr *) (VOID *) PeerAddr.sin6_addr.s6_addr, &Ip6Addr);
    PeerAddr.sin6_port = 0;
#ifdef BSDCOMP_SLI_WANTED
    if (setsockopt (DHCP6_RLY_SOCKET_FD, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                    &(i4OptEnable), sizeof (INT4)) < 0)
    {
        return;
    }
#endif

#ifdef SLI_WANTED
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;
    Ip6MsgHdr.msg_controllen = DHCP6_MAX_MTU;
    /* Wait to receive data on the socket */
    i4RcvCount =
        recvfrom (DHCP6_RLY_SOCKET_FD, (char *) au1Dhcp6RBuf,
                  u4BufLen, 0, (struct sockaddr *) &PeerAddr, &i4AddrLen);

    if (i4RcvCount >= 0)
    {
        if (recvmsg (DHCP6_RLY_SOCKET_FD, &Ip6MsgHdr, 0) < 0)

        {
            DHCP6_RLY_TRC (DHCP6_RLY_DATA_PATH_TRC |
                           DHCP6_RLY_CONTROL_PLANE_TRC,
                           "Rcv Message Failed \n");
            return;
        }
    }
    /* store the required values from SOCKET LAYER to
       Params Structure */
    else
    {
        return;

    }                            /* rcv_count */
#else

    /* Wait to receive data on the socket */
    MEMSET (&CmsgInfo, 0, sizeof (CmsgInfo));
    MEMSET (au1Dhcp6RBuf, 0, DHCP6_RLY_MAX_MTU_SIZE);
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    Ip6MsgHdr.msg_name = (void *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (PeerAddr);
    Iov.iov_base = au1Dhcp6RBuf;
    Iov.iov_len = DHCP6_RLY_MAX_MTU_SIZE;
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (VOID *) &au1Cmsg;
    Ip6MsgHdr.msg_controllen = DHCP6_MAX_MTU;

    i4RcvCount = recvmsg (DHCP6_RLY_SOCKET_FD, &Ip6MsgHdr, 0);
    if (i4RcvCount < 0)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "D6RlParsRcvAndProcessPkt: Error in receiving message !\r\n");

        return;
    }
#endif
    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    /*
     * Store the required values from SOCKET LAYER to
     * Params Structure.
     */
#ifdef BSDCOMP_SLI_WANTED
    NetIpv4GetCfaIfIndexFromPort ((UINT4) pIn6Pktinfo->ipi6_ifindex,
                                  &u4IfIndex);
#else
    u4IfIndex = (UINT4) pIn6Pktinfo->ipi6_ifindex;
#endif
    Udp6Dhcp6RParams.u4Index = u4IfIndex;
    Udp6Dhcp6RParams.u2SrcPort = OSIX_NTOHS (PeerAddr.sin6_port);
    Udp6Dhcp6RParams.u2DstPort = (UINT2) DHCP6_RLY_RELAY_PORT_DEF;
    Udp6Dhcp6RParams.u4Len = i4RcvCount;

    i4RetVal = (INT4) sizeof (UINT1);

    if (getsockopt (DHCP6_RLY_SOCKET_FD, IPPROTO_IPV6,
                    IPV6_UNICAST_HOPS, &Udp6Dhcp6RParams.i4Hlim, &i4RetVal) < 0)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_BUFFER_TRC |
                       DHCP6_RLY_FAILURE_TRC, "DHCP6R : getsockopt failed\n");
        return;
    }

    Ip6AddrCopy (&Udp6Dhcp6RParams.srcAddr,
                 (tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr);
    Ip6AddrCopy (&Udp6Dhcp6RParams.dstAddr,
                 (tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr);

    D6RlParsProcessPkt (&Udp6Dhcp6RParams, au1Dhcp6RBuf, (UINT2) i4RcvCount);
    return;
}

/**************************************************************************/
/*   Function Name   : D6RlParsSendToClient                               */
/*   Description     : This function sends the packet to the client or    */
/*                     relay which can forwaard to the client.            */
/*                                                                        */
/*   Input(s)        : pDhcp6RlyRpyHdr - pointer to reply header struct   */
/*                     pu1SendBuf      - Pointer to the send Buffer       */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/
PRIVATE INT4
D6RlParsSendToDest (tDhcp6RlyDestInfo * pDhcp6RlyDest,
                    UINT1 *pu1SendBuf, UINT2 u2Len)
{
    tIp6Addr            SrcAddr;
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2DstPort = 0;
    UINT1               u1MsgType = 0;
#ifndef BSDCOMP_SLI_WANTED
    tDhcp6RlyIfInfo    *pInterface = NULL;
    INT4                i4HopLimit = 0;
    struct cmsghdr      CmsgInfo;
#else
    UINT1               CmsgInfo[112];
    INT4                i4ReturnValue = 0;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
#endif
    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));

    DHCP6_RLY_GET_1_BYTE (pu1SendBuf, u4Offset, &u1MsgType);

    if (u1MsgType == DHCP6_RLY_RELAY_RPY)
    {
        u2DstPort = (UINT2) DHCP6_RLY_SRV_TRANSMIT_PORT;
    }
    else if ((u1MsgType == DHCP6_RLY_RPY) ||
             (u1MsgType == DHCP6_SERVER_ADVERTISE))
    {
        u2DstPort = (UINT2) DHCP6_RLY_CLNT_TRANSMIT_PORT;
    }
    else if (u1MsgType == DHCP6_RLY_RELAY_FWD)
    {
        u2DstPort = (UINT2) DHCP6_RLY_SRV_TRANSMIT_PORT;
    }
    else
    {
        return OSIX_FAILURE;
    }

    if (pDhcp6RlyDest->u4OutIfIndex != 0)
    {
        u4IfIndex = pDhcp6RlyDest->u4OutIfIndex;
    }
    else
    {
        u4IfIndex = pDhcp6RlyDest->u4InIfIndex;
    }
    /* Set src addr as link local addr for this logical interface */
    if (D6RlUtlGetLinkAddr (u4IfIndex, (UINT1 *) &SrcAddr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (IS_ADDR_UNSPECIFIED (SrcAddr))
    {
        if (IS_ADDR_UNSPECIFIED (pDhcp6RlyDest->LinkAddr))
        {
            D6RlUtlGetGlobalAddr (u4IfIndex,
                                  &pDhcp6RlyDest->PeerAddr, &SrcAddr);
        }
        else
        {
            D6RlUtlGetGlobalAddr (u4IfIndex,
                                  &pDhcp6RlyDest->LinkAddr, &SrcAddr);
        }
    }
    PeerAddr.sin6_family = AF_INET6;

    PeerAddr.sin6_port = OSIX_HTONS (u2DstPort);
#ifdef BSDCOMP_SLI_WANTED

    i4ReturnValue =
        NetIpv6GetIfInfo (pDhcp6RlyDest->u4OutIfIndex, &NetIpv6IfInfo);
    if (i4ReturnValue == NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
    PeerAddr.sin6_scope_id = NetIpv6IfInfo.u4IpPort;
#else
    PeerAddr.sin6_scope_id = pDhcp6RlyDest->u4OutIfIndex;
#endif
    Ip6AddrCopy ((tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr,
                 &pDhcp6RlyDest->PeerAddr);
#ifndef BSDCOMP_SLI_WANTED
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        return OSIX_FAILURE;
    }
    if (!IS_ADDR_MULTI (pDhcp6RlyDest->PeerAddr))
    {
        if (pDhcp6RlyDest->u4OutIfIndex == 0)
        {
            if (D6RlUtlGetRoute (pDhcp6RlyDest->PeerAddr.u1_addr,
                                 &pDhcp6RlyDest->u4OutIfIndex) != OSIX_SUCCESS)
            {
                DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                               "D6RlParsSendToDest: Unable to get route \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    pInterface = D6RlIfGetNode (pDhcp6RlyDest->u4OutIfIndex);
    if (pInterface != NULL)
    {
        if (u1MsgType == DHCP6_RLY_RPY)
        {
            /* Increment Relay reply transmitted counter of
             * corresponding interface */
            pInterface->u4RelayReplyOut++;
        }
        else if (u1MsgType == DHCP6_SERVER_ADVERTISE)
        {
            /* Increment  server   adevertise transmitted counter of
             * corresponding interface */
            pInterface->u4SrvAdv++;
        }
        else if (u1MsgType == DHCP6_RLY_RELAY_RPY)
        {
            /* Increment Relay reply transmitted counter of
             * corresponding interface */
            pInterface->u4RelayReply++;
        }
        else if (u1MsgType == DHCP6_RLY_RELAY_FWD)
        {
            /* Increment Relay forward Transmitted counter of
             * corresponding interface */
            pInterface->u4RelayForwOut++;
        }
    }
    MEMCPY ((&pIn6Pktinfo->ipi6_ifindex), (&pDhcp6RlyDest->u4OutIfIndex),
            sizeof (UINT4));
    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 &SrcAddr);
    sendmsg (DHCP6_RLY_SOCKET_FD, &Ip6MsgHdr, 0);
    if (IS_ADDR_MULTI (pDhcp6RlyDest->PeerAddr))
    {
        if (setsockopt (DHCP6_RLY_SOCKET_FD, IPPROTO_IPV6, IPV6_MULTICAST_IF,
                        &pDhcp6RlyDest->u4OutIfIndex, sizeof (UINT4)) < 0)
        {
            return OSIX_FAILURE;
        }
        if (setsockopt (DHCP6_RLY_SOCKET_FD, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                        &i4HopLimit, sizeof (INT4)) < 0)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (setsockopt (DHCP6_RLY_SOCKET_FD, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                        &i4HopLimit, sizeof (INT4)) < 0)
        {
            return OSIX_FAILURE;
        }
    }
    if (sendto (DHCP6_RLY_SOCKET_FD, pu1SendBuf, u2Len, 0,
                (struct sockaddr *) &PeerAddr, sizeof (struct sockaddr)) < 0)
    {
        return OSIX_FAILURE;
    }
#else
    if (IS_ADDR_MULTI (pDhcp6RlyDest->PeerAddr))
    {
        if (setsockopt
            (DHCP6_RLY_SOCKET_FD, IPPROTO_IPV6, IPV6_MULTICAST_IF,
             (INT4 *) &(PeerAddr.sin6_scope_id), sizeof (INT4)) < 0)
        {
            return OSIX_FAILURE;
        }
    }
    pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;

    /* EVALRIPNG cc below :: changed from sizeof (Cmsginfo) */

    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    /* EVALRIPNG cc */
    Iov.iov_base = pu1SendBuf;
    Iov.iov_len = u2Len;

    Ip6MsgHdr.msg_name = (VOID *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    /* EVALRIPNG end */
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));
    pIn6Pktinfo->ipi6_ifindex = PeerAddr.sin6_scope_id;
    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 &SrcAddr);

    /* EVALRIPNG :: using a single call */
    if (sendmsg (DHCP6_RLY_SOCKET_FD, &Ip6MsgHdr, 0) < 0)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_FAILURE_TRC,
                       "D6RlParsSendToDest: Error in sending message !\r\n");
        return OSIX_FAILURE;
    }
#endif
    DHCP6_RLY_TRC_ARG1 (DHCP6_RLY_CONTROL_PLANE_TRC,
                        "Packet is forwarded to the destination. O/p interface :%u\r\n",
                        pDhcp6RlyDest->u4OutIfIndex);
    return OSIX_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : D6RlParsSendToServer                                 */
/*   Description     : This function sends the packet to the server or    */
/*                     relay which can forward to the server.             */
/*                                                                        */
/*   Input(s)        : u4Index - IfIndex on which the packet rcvd         */
/*                     pu1SendBuf      - Pointer to the send Buffer       */
/*                     u4Size          - Size of the packet to be sent    */
/*   Output(s)       : None                                               */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE             */
/**************************************************************************/
PRIVATE INT4
D6RlParsSendToServer (UINT4 u4Index, UINT1 *pu1SendBuf, UINT4 u4Size)
{
    tDhcp6RlyOutIfInfo *pOutIfEntry = NULL;
    tDhcp6RlySrvAddrInfo *pServerEntry = NULL;
    tDhcp6RlyIfInfo    *pIfEntry = NULL;

    tDhcp6RlyDestInfo   DstInfo;
    tIp6Addr            SrvAddr;
    UINT4               u4OffSet = 1;
    UINT2               u2OriginalHops = 0;
    UINT2               u2Hops = (UINT2) DHCP6_RLY_MAX_HOPS;
    UINT1               u1MaxHop = 0;

    MEMSET (&DstInfo, 0, sizeof (tDhcp6RlyDestInfo));
    MEMSET (&SrvAddr, 0, sizeof (tIp6Addr));

    INET_ATON6 (DHCP6_ALL_SERVER, &SrvAddr);

    pIfEntry = D6RlIfGetNode (u4Index);
    if (pIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    DstInfo.u4InIfIndex = u4Index;
    /* dont allow user to configure all relay-server as multicast address
     */
    /*1. If no server address is configured, send to all servers only
     *2. If server address is configured send to that address with the
     *   out interface if any
     */
    pServerEntry = D6RlIfServerListGetFirstNode (pIfEntry);

    /* No server address configured */
    if (pServerEntry == NULL)
    {
        DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                       "No Server is configured. Forward packet to all interfaces \r\n");
        u1MaxHop = (UINT1) DHCP6_RLY_MAX_HOPS;
        DHCP6_RLY_PUT_1_BYTE (pu1SendBuf, u4OffSet, &u1MaxHop);
        D6RlParsSendAsMulti (u4Index, &SrvAddr, pu1SendBuf, u4Size);
        return OSIX_SUCCESS;
    }
    /* Server address are configured */
    DHCP6_RLY_GET_2_BYTE (pu1SendBuf, u4OffSet, &u2OriginalHops);
    INET_ATON6 (DHCP6_ALL_SERVER, &SrvAddr);
    while (pServerEntry != NULL)
    {
        if (MEMCMP (&SrvAddr, &pServerEntry->Addr, sizeof (tIp6Addr)) == 0)
        {
            u4OffSet = 1;
            DHCP6_RLY_PUT_2_BYTE (pu1SendBuf, u4OffSet, &u2Hops);
        }
        else
        {
            u4OffSet = 1;
            DHCP6_RLY_PUT_2_BYTE (pu1SendBuf, u4OffSet, &u2OriginalHops);
        }
        pOutIfEntry = D6RlSrvOutIfListGetFirstNode (pServerEntry);
        if (pOutIfEntry == NULL)
        {
            /* No output interface is configured for this server address */
            MEMCPY (&DstInfo.PeerAddr, &pServerEntry->Addr, sizeof (tIp6Addr));
            DstInfo.u4OutIfIndex = 0;
            if (IS_ADDR_MULTI (pServerEntry->Addr))
            {
                DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                               "o/p interface is not configured."
                               " Forward packet to all interfaces \r\n");
                D6RlParsSendAsMulti (u4Index, &pServerEntry->Addr, pu1SendBuf,
                                     u4Size);
            }
            else
            {
                D6RlParsSendToDest (&DstInfo, pu1SendBuf, (UINT2) u4Size);
            }
            pServerEntry = D6RlIfServerListGetNextNode (pIfEntry, pServerEntry);
            continue;
        }
        while (pOutIfEntry != NULL)
        {
            MEMCPY (&DstInfo.PeerAddr, &pServerEntry->Addr, sizeof (tIp6Addr));
            DstInfo.u4OutIfIndex = pOutIfEntry->u4OutIfIndex;
            D6RlParsSendToDest (&DstInfo, pu1SendBuf, (UINT2) u4Size);
            pOutIfEntry =
                D6RlSrvOutIfListGetNextNode (pServerEntry, pOutIfEntry);
        }
        pServerEntry = D6RlIfServerListGetNextNode (pIfEntry, pServerEntry);
    }
    return OSIX_SUCCESS;
}
PRIVATE VOID
D6RlParsSendAsMulti (UINT4 u4InIfIndex, tIp6Addr * pSrvAddr, UINT1 *pu1SendBuf,
                     UINT4 u4Size)
{
    tDhcp6RlyDestInfo   DstInfo;
    tDhcp6RlyIfInfo    *pIfEntry = NULL;
    UINT4               u4TempIndex = 0;
    MEMCPY (&DstInfo.PeerAddr, pSrvAddr, sizeof (tIp6Addr));
    DstInfo.u4InIfIndex = u4InIfIndex;
    while ((pIfEntry = D6RlIfGetNextNode (u4TempIndex)) != NULL)
    {
        if (pIfEntry->u4IfIndex != u4InIfIndex)
        {
            DstInfo.u4OutIfIndex = pIfEntry->u4IfIndex;
            D6RlParsSendToDest (&DstInfo, pu1SendBuf, (UINT2) u4Size);
        }
        u4TempIndex = pIfEntry->u4IfIndex;
    }
    DHCP6_RLY_TRC (DHCP6_RLY_CONTROL_PLANE_TRC,
                   "Packet is forwarded to all interfaces\r\n");
    return;
}

/***************************************************************************/
/*   Function Name   : D6RlParsRemIdOption                                 */
/*   Description     : This function is used to fetch the remote-id option */
/*                     value which can be forwarded to the server.         */
/*                                                                         */
/*   Input(s)        : i4Dh6RlIfIndex - Inteface index                     */
/*                     pi4RetRemIdOptionType - Pointer                       */
/*                     pDh6RlyRemIdBuf - Remote-id buffer                   */
/*   Output(s)       : None                                                */
/*   Return Value    : OSIX_SUCCESS or OSIX_FAILURE                           */
/***************************************************************************/

INT2
D6RlParsRemIdOption (INT4 i4Dh6RlIfIndex,
                     INT4 *pi4RetRemIdOptionType,
                     tDhcp6RlyRemIdOption * pDh6RlyRemIdBuf)
{
    INT4                i4RetVal = 0;
    UINT1              *pu1SwitchName = NULL;
    UINT4               u4Len = 0;

    tDhcp6RlyIfInfo    *pInterface = NULL;

    i4RetVal = (INT4) DHCP6_RLY_REMOTEID_STATUS;
    if (i4RetVal == DHCP6_RLY_REMOTEID_ENABLE)
    {
        if ((pInterface = D6RlIfGetNode (i4Dh6RlIfIndex)) == NULL)
        {
            DHCP6_RLY_TRC (DHCP6_RLY_MGMT_TRC |
                           DHCP6_RLY_FAILURE_TRC, "Invalid If Index \r\n");
            return OSIX_FAILURE;
        }

        *pi4RetRemIdOptionType = pInterface->i4RemoteIdOption;

        if (*pi4RetRemIdOptionType == DHCP6_RLY_REMOTEID_DUID)
        {
            if (!(pInterface->Dhcp6RemIdOpt.i4RemIdLen))
            {
                pDh6RlyRemIdBuf->i4RemIdLen = 0;
                return OSIX_SUCCESS;
            }

            MEMCPY (pDh6RlyRemIdBuf->au1Duid,
                    pInterface->Dhcp6RemIdOpt.au1Duid,
                    pInterface->Dhcp6RemIdOpt.i4RemIdLen);
            pDh6RlyRemIdBuf->i4RemIdLen = pInterface->Dhcp6RemIdOpt.i4RemIdLen;
            return OSIX_SUCCESS;
        }
        else if (*pi4RetRemIdOptionType == DHCP6_RLY_REMOTEID_SW_NAME)
        {
            pu1SwitchName = IssSysGetSwitchName ();
            u4Len =
                ((STRLEN (pu1SwitchName)) <
                 (sizeof (pDh6RlyRemIdBuf->au1SwName) -
                  1)) ? STRLEN (pu1SwitchName) : (sizeof (pDh6RlyRemIdBuf->
                                                          au1SwName) - 1);
            STRNCPY (pDh6RlyRemIdBuf->au1SwName, pu1SwitchName, u4Len);
            pDh6RlyRemIdBuf->au1SwName[u4Len] = '\0';
            pDh6RlyRemIdBuf->i4RemIdLen = STRLEN (pu1SwitchName);
            return OSIX_SUCCESS;
        }
        else if (*pi4RetRemIdOptionType == DHCP6_RLY_REMOTEID_MGMT_IP)
        {
            pDh6RlyRemIdBuf->u4MgmtIpAddr = IssSysGetDefSwitchIp ();
            pDh6RlyRemIdBuf->i4RemIdLen = DHCP6_RLY_IP4_ADDR_LEN;
            return OSIX_SUCCESS;
        }
        else if (*pi4RetRemIdOptionType == DHCP6_RLY_REMOTEID_USERDEFINED)
        {
            if (!(STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined)))
            {
                pDh6RlyRemIdBuf->i4RemIdLen = 0;
                return OSIX_SUCCESS;
            }
            MEMCPY (pDh6RlyRemIdBuf->au1UserDefined,
                    pInterface->Dhcp6RemIdOpt.au1UserDefined,
                    STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined));
            pDh6RlyRemIdBuf->i4RemIdLen =
                STRLEN (pInterface->Dhcp6RemIdOpt.au1UserDefined);
            return OSIX_SUCCESS;

        }
    }

    return OSIX_FAILURE;

}

#ifndef LNXIP6_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : D6RlAddNdCache                                             */
/*                                                                           */
/* Description  : The function Check whether the reply packet is having the  */
/*                Mac address and IPV6 address of the client if so add a     */
/*                ND cache Entry.                                            */
/*                                                                           */
/* Input        : u4Index - Cfa Index through which client can be reached    */
/*                pu1Dhcp6Buf - DHCPv6 relay reply packet                    */
/*                u2Len - Length of the packet                               */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6RlAddNdCache (UINT4 u4Index, UINT1 *pu1Dhcp6Buf, UINT2 u2Len)
{
    tIp6Addr            LinkAddr;
    UINT1               au1MacAddr[MAC_ADDR_LEN];
    UINT4               u4Offset = 0;
    UINT4               u4TempOffset = 0;
    UINT2               u2OptionType = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2DuidType = 0;
    UINT1               u1IsMacPresent = OSIX_FALSE;
    UINT1               u1IsIpv6Present = OSIX_FALSE;

    MEMSET (au1MacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&LinkAddr, 0, sizeof (tIp6Addr));

    /* Skip the MessageType(1) + TransactionId(3) */
    u4Offset = (UINT4) (u4Offset + 4);

    while (u4Offset < u2Len)
    {
        /* TYPE */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionType);

        /* LENGTH */
        DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4Offset, &u2OptionLen);

        u4TempOffset = u4Offset;

        /* Calulating the offset of next option type */
        u4Offset = (UINT4) (u4Offset + u2OptionLen);

        /* Client Identifier is present */
        if (u2OptionType == DHCP6_RLY_CLIENT_DUID)
        {
            DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4TempOffset, &u2DuidType);

            /* Check the DUID Type to find Mac address is present */
            if (u2DuidType != DHCP6_RLY_DUID_TYPE_EN)
            {
                /* Point the offset to the MAC address */
                u4TempOffset = u4Offset - MAC_ADDR_LEN;

                /* Set the MAC present Flag and copy the MAC address */
                u1IsMacPresent = OSIX_TRUE;
                DHCP6_RLY_GET_N_BYTE (pu1Dhcp6Buf, au1MacAddr,
                                      u4TempOffset, MAC_ADDR_LEN);
            }
        }
        else if ((u2OptionType == DHCP6_RLY_IA_PD_OPTION) ||
                 (u2OptionType == DHCP6_RLY_IA_NTA_OPTION))
        {
            /* Skip the IAID(4) + T1(4) + T2(4) options */
            u4TempOffset = (UINT4) (u4TempOffset + 12);

            /* TYPE */
            DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4TempOffset, &u2OptionType);

            /* LENGTH */
            DHCP6_RLY_GET_2_BYTE (pu1Dhcp6Buf, u4TempOffset, &u2OptionLen);

            if (u2OptionType == DHCP6_RLY_IA_PD_PREFIX_OPTION)
            {
                /* Move the offset to the IPv6 Address
                 * Skip PreferredLifeTime(4) +  ValidLifeTime(4) + 
                 * PrifixLength(1)*/
                u4TempOffset = (UINT4) (u4TempOffset + 9);
            }
            else if (u2OptionType == DHCP6_RLY_IA_ADDRESS_OPTION)
            {
                /* In this case do nothing as the offset is pointing to 
                 * IPV6 address */
            }
            else
            {
                break;
            }

            /* Set the IPv6 present Flag and copy the IPv6 address */
            u1IsIpv6Present = OSIX_TRUE;
            DHCP6_RLY_GET_N_BYTE (pu1Dhcp6Buf, (UINT1 *) &LinkAddr,
                                  u4TempOffset, DHCP6_RLY_IP6_ADDR_LEN);
        }
    }

    /* If client Mac address and IPv6 address is present in the reply packet */
    if ((u1IsMacPresent == OSIX_TRUE) && (u1IsIpv6Present == OSIX_TRUE))
    {
        /* Add ND Cache Entry */
        NetIpv6AddNdCache (u4Index, (tIp6Addr *) (VOID *)
                           &LinkAddr,
                           au1MacAddr,
                           DHCP6_RLY_IP6_ADDR_LEN, ND6_CACHE_ENTRY_STALE);
    }

    return;
}
#endif
