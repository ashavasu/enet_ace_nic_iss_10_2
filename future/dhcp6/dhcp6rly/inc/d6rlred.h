/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: d6rlred.h,v 1.1 2014/06/23 11:30:07 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for DHCP6RLY Server module.
 *              
 *******************************************************************/
#ifndef __D6RLY_RED_H
#define __D6RLY_RED_H

enum{
    DHCP6RLY_DYN_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info  */
    DHCP6RLY_MAX_DYN_INFO_TYPE
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
};

/* Represents the message types encoded in the update messages */
typedef enum {
    DHCP6RLY_RED_DYN_CACHE_INFO  = DHCP6RLY_DYN_INFO,
    DHCP6RLY_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    DHCP6RLY_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG
}eDhcp6RlyRedRmMsgType;

typedef enum{
    DHCP6RLY_HA_UPD_NOT_STARTED = 1,/* 1 */
    DHCP6RLY_HA_UPD_IN_PROGRESS,    /* 2 */
    DHCP6RLY_HA_UPD_COMPLETED,      /* 3 */
    DHCP6RLY_HA_UPD_ABORTED,        /* 4 */
    DHCP6RLY_HA_MAX_BLK_UPD_STATUS
} eDhcp6RlyHaBulkUpdStatus;

/* Macro Definitions for DHCP6RLY Server Redundancy */


#define DHCP6RLY_RM_GET_NUM_STANDBY_NODES_UP() \
          gDhcp6RlyRedGblInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define DHCP6RLY_NUM_STANDBY_NODES() gDhcp6RlyRedGblInfo.u1NumPeersUp

#define DHCP6RLY_RM_BULK_REQ_RCVD() gDhcp6RlyRedGblInfo.bBulkReqRcvd

#define DHCP6RLY_IS_STANDBY_UP() \
          ((gDhcp6RlyRedGblInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */

#define DHCP6RLY_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define DHCP6RLY_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define DHCP6RLY_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define DHCP6RLY_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define DHCP6RLY_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define DHCP6RLY_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define DHCP6RLY_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define DHCP6RLY_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define DHCP6RLY_RED_MAX_MSG_SIZE        1500
#define DHCP6RLY_RED_TYPE_FIELD_SIZE     1
#define DHCP6RLY_RED_LEN_FIELD_SIZE      2
#define DHCP6RLY_RED_MIM_MSG_SIZE        (1 + 2 + 2)
#define DHCP6RLY_RED_DYN_INFO_SIZE       (6 + 4 + 2 + 2 + 1 + 1)

#define DHCP6RLY_ONE_BYTE                1
#define DHCP6RLY_TWO_BYTES               2
#define DHCP6RLY_FOUR_BYTES              4

#define DHCP6RLY_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define DHCP6RLY_RED_BULK_REQ_MSG_SIZE            3

#define DHCP6RLY_RED_BULQ_REQ_SIZE       3

#define DHCP6RLY_RED_ADD_CACHE            1
#define DHCP6RLY_RED_DEL_CACHE            2

#define DHCP6RLY_RED_CACHE_DEL         1
#define DHCP6RLY_RED_CACHE_DONT_DEL    0

/* Function prototypes for DHCP6RLY Redundancy */

#endif /* __D6RLY_RED_H */
