/* $Id: d6rlcons.h,v 1.18 2015/03/14 11:49:39 siva Exp $   */

#ifndef   _DHCP6_RLY_INC_H
#define   _DHCP6_RLY_INC_H

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "fssocket.h"
#include "iss.h"
#include "fssnmp.h"
#include "snmccons.h"
#include "snmputil.h"
#include "trace.h"
#include "ipv6.h"
#include "ip6util.h"
#include "fssyslog.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#include "cfanpwr.h"
#endif
#include "dhcp6.h"
#include "d6rlmacs.h"
#include "d6rlcons.h"
#include "d6rltdfs.h"
#include "d6rlextn.h"
#include "d6rlprot.h"
#include "d6rltrc.h"
#include "d6rltrap.h"
#include "d6rlcli.h"
#include "fsdh6rlw.h"
#include "fsdh6rwr.h"
#include "d6rlsz.h"
#endif

#ifndef __DHCP6_RLY_CONS_H
#define __DHCP6_RLY_CONS_H

#define DHCP6_RLY_TASK_NAME                 ((UINT1 *)"D6RL")

/* Compare macros */
#define DHCP6_RLY_EQUAL                   0
#define DHCP6_RLY_GREATER                 1
#define DHCP6_RLY_LESS                   -1

/* Semaphore for Dhcpv6 relay mutual exclusion lock */
#define DHCP6_RLY_TASK_SEM                 (const UINT1*)"d6rl"
#define DHCP6_RLY_SEM_COUNT                1

/* Events */

#define DHCP6_RLY_PKT_RCVD_EVENT           0x01
#define DHCP6_RLY_QMSG_EVENT               0x02
#define DHCP6_RLY_TIMER_EVENT              0x04
#define DHCP6_RLY_RM_PKT_EVENT             0x08

/* Trap  */
#define DHCP6_RLY_TRAP_CONTROL_MAX_VAL     0x03

/* Queue related macros */
#define DHCP6_RLY_Q_NAME                   (UINT1 *)"D6RQ"
#define DHCP6_RLY_RM_Q_NAME                (UINT1 *)"D6RMRQ"
#define DHCP6_RLY_RM_Q_DEPTH                64

/* Return values */
#define DHCP6_RLY_NO_ENTRY                 3 

/* Control values */
#define DHCP6_RLY_ENABLE                   1
#define DHCP6_RLY_DISABLE                  2

#define DHCP6_RLY_INTF_DELETE              1

/* Relay Socket */
#define DHCP6_RLY_RELAY_PORT_DEF           547
#define DHCP6_RLY_CLIENT_PORT_DEF          546
#define DHCP6_RLY_SERVER_PORT_DEF          (DHCP6_RLY_RELAY_PORT_DEF)
#define DHCP6_RLY_HOP_THRESH_DEF           4

/* Default DUID value */
#define DHCP6_RLY_DUID_DEF       0

/* Message Related Macros */
/* Types */
#define DHCP6_CLIENT_SOLICIT      1
#define DHCP6_SERVER_ADVERTISE    2
#define DHCP6_CLIENT_REQUEST      3
#define DHCP6_CLIENT_REBIND_SF    6
#define DHCP6_CLIENT_RENEW_SF     5
#define DHCP6_CLIENT_RELEASE_SF   8
#define DHCP6_CLIENT_DECLINE_SF   9
#define DHCP6_SERVER_RECONFIGURE_SF        10
#define DHCP6_CLIENT_CONFIRM_SF            4
/*used for DHCP6 Relay PD */
/* here DHCP6_SERVER_REPLY and DHCP6_RLY_RPY are same message with different byte size */
#define DHCP6_CLIENT_DECLINE               0x9000000
#define DHCP6_CLIENT_RELEASE               0x8000000
#define DHCP6_CLIENT_REBIND                0x6000000
#define DHCP6_CLIENT_RENEW                 0x5000000
#define DHCP6_SERVER_REPLY                 0x7000000
/************************/
#define DHCP6_RLY_CLIENT_REQ               11
#define DHCP6_RLY_RELAY_FWD                12
#define DHCP6_RLY_RELAY_RPY                13
#define DHCP6_RLY_RPY                      7

/*other Macros*/
#define DHCP6_RLY_MAX_PREFIX_LENGTH        (UINT1)128

/*STATUS CODE for DHCP6 Relay PD */
#define DHCP6_RLY_NOBINDING                3

/* Options */
#define DHCP6_RLY_RELAY_MSG_OPTION         (UINT2)9
#define DHCP6_RLY_IF_ID_OPTION             (UINT2)18
#define DHCP6_RLY_REM_ID_OPTION            (UINT2)37
#define DHCP6_RLY_CLIENT_DUID              (UINT2)1
#define DHCP6_RLY_SERVER_DUID              (UINT2)2
#define DHCP6_RLY_IA_PD_OPTION             (UINT2)25
#define DHCP6_RLY_IA_NTA_OPTION            (UINT2)3
#define DHCP6_RLY_IA_ADDRESS_OPTION        (UINT2)5
#define DHCP6_RLY_IA_PD_PREFIX_OPTION      (UINT2)26
#define DHCP6_RLY_STATUS_MSG_OPTION        (UINT2)13

/*DUID TYPE*/
#define DHCP6_RLY_DUID_TPYE_LLT            (UINT2)1
#define DHCP6_RLY_DUID_TYPE_EN             (UINT2)2
#define DHCP6_RLY_DUID_TYPE_LL             (UINT2)3

/* Length */
#define DHCP6_RLY_ENT_NUM_LEN      (UINT2)4
#define DHCP6_RLY_IF_ID_LEN                (UINT2)4
#define DHCP6_RLY_IP6_ADDR_LEN             16
#define DHCP6_RLY_IP4_ADDR_LEN             4
#define DHCP6_RLY_MAX_MTU_SIZE             4096
#define DHCP6_RLY_IP6_MAX_STRLEN           40
#define DHCP6_RLY_SKIP_OFFSET              74
#define DHCP6_CLIENT_RLY_SKIP_OFFSET       36

/* Maximum hop counts */
#define DHCP6_RLY_MAX_HOPS                 32  

#define DHCP6_RLY_UDP_PORT_MIN             1
#define DHCP6_RLY_UDP_PORT_MAX             65535 

/* Validtime is 7 days */
#define DHCP6_RLY_MAX_VALIDTIME            604800
#define DHCP6_RLY_INFINITY_VALIDTIME       0xffffffff


/* Enterprise number as registered with IANA */
#define DHCP6_RLY_ENTPRISE_NUMBER     2076

/* Mask to get msg type and transaction id */
# define DHCP6_RLY_MASK_TRANSACTION_ID     0x00ffffff
# define DHCP6_RLY_MASK_MSG_TYPE           0xff000000 

#define DHCP6_RLY_DYNAMIC      1
#define DHCP6_RLY_STATIC      2
#endif
