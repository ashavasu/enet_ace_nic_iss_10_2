/* $Id: d6rlglob.h,v 1.4 2014/07/10 12:23:57 siva Exp $   */ 
/* Description: This file contains the global structure used in DHCP6
 *              Relay module.
 *
 **************************************************************************/

#ifndef  _DHCP6_RL_GLOB_H
#define  _DHCP6_RL_GLOB_H
#ifdef   _GLOBAL_MODULE
tDhcp6RlyGblInfo       gDhcp6RlyGblInfo;
tOsixQId               gDhcp6RlyRmPktQId;
tDhcp6RlyRedGblInfo    gDhcp6RlyRedGblInfo;
tDbTblDescriptor       gDhcp6RlyDynInfoList;
tDbDataDescInfo        gaDhcp6RlyDynDataDescList[DHCP6RLY_MAX_DYN_INFO_TYPE];

   tDbOffsetTemplate gaDhcp6RlyOffsetTbl[] =
   {{(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u1Version) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, au1Pad) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 3},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, Prefix) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)),  sizeof (tIp6Addr)},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, NextHop) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)),  sizeof (tIp6Addr)},

     {(FSAP_OFFSETOF(tDhcp6RlyPDEntry,u4TransactionId) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u4MsgType) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u4RtIndex) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u4T1Time) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u4T2Time) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u4ValidLifeTime) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u1PrefixLen) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u1Flag) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tDhcp6RlyPDEntry, u2Option) -
      FSAP_OFFSETOF(tDhcp6RlyPDEntry, RbPrefixDelegationDbNode) - sizeof(tDbTblNode)), 2},

    {-1, 0}};

#else
extern tOsixQId               gDhcp6RlyRmPktQId;
extern tDhcp6RlyRedGblInfo    gDhcp6RlyRedGblInfo;
extern tDbTblDescriptor       gDhcp6RlyDynInfoList;
extern tDbOffsetTemplate      gaDhcp6RlyOffsetTbl[];
extern tDbDataDescInfo        gaDhcp6RlyDynDataDescList[DHCP6RLY_MAX_DYN_INFO_TYPE];
#endif
/*********************************/
#endif
