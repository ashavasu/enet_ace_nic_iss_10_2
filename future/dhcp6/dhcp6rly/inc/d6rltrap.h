/************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * Description: This file contains macros for snmp traps.
 *************************************************************************/


#ifndef  _DHCP6_RLY_TRAP_H
#define  _DHCP6_RLY_TRAP_H

PUBLIC VOID D6RlTrapSnmpSend PROTO((VOID *, UINT1));

typedef struct _Dhcp6RlyInvalidMsgRcvdTrap{
  UINT4   u4MsgType;
  UINT4   u4IfIndex;
}tDhcp6RlyInvalidMsgRcvdTrap;

typedef struct _Dhcp6RlyHopThresholdTrap{
  UINT4   u4IfIndex;
}tDhcp6RlyHopThresholdTrap;



#define DHCP6_RLY_TRAPS_OID                     "1.3.6.1.4.1.29601.2.41.0"
#define DHCP6_RLY_MIB_OBJ_INV_MSG_TYPE          "fsDhcp6RlyTrapInvalidMsgType"
#define DHCP6_RLY_MIB_OBJ_IF_INDEX              "fsDhcp6RlyTrapIfIndex"
#define DHCP6_RLY_MIB_OBJ_INV_MSG_TRAP          "fsDhcp6RlyRlyInvalidPacketTrap"
#define DHCP6_RLY_MIB_OBJ_MAX_HOP_TRAP          "fsDhcp6RlyIfHopThreshold"

#define DHCP6_RLY_VAL_10                                   10
#define DHCP6_RLY_4BIT_MAX                                 0xf
#define DHCP6_RLY_ARRAY_SIZE_257                           257
#define DHCP6_RLY_ARRAY_SIZE_256                           256
#define DHCP6_RLY_ARRAY_SIZE_68                            68

#define DHCP6_RLY_RCVD_INVALID_MSG_TRAP_VAL                1
#define DHCP6_RLY_HOP_THRESHOLD_TRAP_TRAP_VAL              2

#define DHCP6_RLY_RCVD_INVALID_MSG_TRAP_DISABLED()\
((DHCP6_RLY_TRAP_CONTROL_OPTION & 0x01)?(OSIX_FALSE):(OSIX_TRUE))

#define DHCP6_RLY_HOP_THRESHOLD_TRAP_DISABLED()\
((DHCP6_RLY_TRAP_CONTROL_OPTION & 0x02)?(OSIX_FALSE):(OSIX_TRUE))
#endif

