/* $Id: d6rltrc.h,v 1.4 2014/06/23 11:25:50 siva Exp $   */
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 * Description:Provides general purpose macros for tracing
 *
 *******************************************************************/
#include "d6rlinc.h"
#ifndef _DHCP6_RLY_TRACE_H_
#define _DHCP6_RLY_TRACE_H_

/* Module name */
#define DHCP6_RLY_NAME "D6RL"

/* syslog */
#define  DHCP6_RLY_SYS_LOG(args) \
{\
     if(DHCP6_RLY_SYS_LOG_OPTION == OSIX_TRUE)\
     {\
        SYS_LOG_MSG(args);\
     }\
}


/* Trace and debug flags */
#define  DHCP6_RLY_DBG_FLAG   gDhcp6RlyGblInfo.u4GblDbgTrc

/* Size */
#define  DHCP6_RLY_TRC_MIN_SIZE          1
#define  DHCP6_RLY_TRC_MAX_SIZE          255
#define  DHCP6_RLY_MAX_TRC_STR_COUNT     9     
#define  DHCP6_RLY_MAX_TRC_STR_LEN       15
#define  DHCP6_RLY_MAX_TRACE_TOKENS      12
#define  DHCP6_RLY_MAX_TRACE_TOKEN_SIZE  12

/* Delimiter */
#define DHCP6_RLY_TRACE_TOKEN_DELIMITER  ' ' /* Space Character */

/* Invalid Trace */
#define  DHCP6_RLY_INVALID_TRC          0x00000000
#define  DHCP6_RLY_CRITICAL_TRC         0x00000100

#define  DHCP6_RLY_INIT_SHUT_TRC      INIT_SHUT_TRC
#define  DHCP6_RLY_MGMT_TRC           MGMT_TRC
#define  DHCP6_RLY_DATA_PATH_TRC      DATA_PATH_TRC
#define  DHCP6_RLY_CONTROL_PLANE_TRC  CONTROL_PLANE_TRC
#define  DHCP6_RLY_DUMP_TRC           DUMP_TRC
#define  DHCP6_RLY_OS_RESOURCE_TRC    OS_RESOURCE_TRC
#define  DHCP6_RLY_FAILURE_TRC        ALL_FAILURE_TRC
#define  DHCP6_RLY_BUFFER_TRC         BUFFER_TRC

#define  DHCP6_RLY_ALL_TRC        (INIT_SHUT_TRC        |\
                                   MGMT_TRC             |\
                                   CONTROL_PLANE_TRC    |\
                                   DUMP_TRC             |\
                                   OS_RESOURCE_TRC      |\
                                   ALL_FAILURE_TRC      |\
                                   BUFFER_TRC           |\
                                   DHCP6_RLY_CRITICAL_TRC)
/* Trace definitions */
#ifdef TRACE_WANTED
#define DHCP6_RLY_TRC(TraceType,Str)\
        MOD_TRC(DHCP6_RLY_DBG_FLAG,TraceType,DHCP6_RLY_NAME,Str)
#define DHCP6_RLY_TRC_ARG1(TraceType,Str,Arg1)\
        MOD_TRC_ARG1(DHCP6_RLY_DBG_FLAG,TraceType,DHCP6_RLY_NAME,Str,Arg1)
#define DHCP6_RLY_TRC_ARG2(TraceType,Str,Arg1,Arg2)\
        MOD_TRC_ARG2(DHCP6_RLY_DBG_FLAG,TraceType,DHCP6_RLY_NAME,Str,Arg1,Arg2)
#define DHCP6_RLY_TRC_ARG3(TraceType,Str,Arg1,Arg2,Arg3)\
        MOD_TRC_ARG3(DHCP6_RLY_DBG_FLAG,TraceType,DHCP6_RLY_NAME,Str,Arg1,Arg2,Arg3)
#define DHCP6_RLY_TRC_ARG4(TraceType,Str,Arg1,Arg2,Arg3,Arg4)\
        MOD_TRC_ARG4(DHCP6_RLY_DBG_FLAG,TraceType,DHCP6_RLY_NAME,Str,Arg1,Arg2,Arg3,Arg4)
#define DHCP6_RLY_TRC_ARG5(TraceType,Str,Arg1,Arg2,Arg3,Arg4,Arg5)\
        MOD_TRC_ARG5(DHCP6_RLY_DBG_FLAG,TraceType,DHCP6_RLY_NAME,Str,Arg1,Arg2,Arg3,Arg4,Arg5)
#define DHCP6_RLY_PKT_DUMP(TraceType,pBuf,Length,Str)
#else
#define DHCP6_RLY_TRC(TraceType,Str)
#define DHCP6_RLY_TRC_ARG1(TraceType,Str,Arg1)
#define DHCP6_RLY_TRC_ARG2(TraceType,Str,Arg1,Arg2)
#define DHCP6_RLY_TRC_ARG3(TraceType,Str,Arg1,Arg2,Arg3)
#define DHCP6_RLY_TRC_ARG4(TraceType,Str,Arg1,Arg2,Arg3,Arg4)
#define DHCP6_RLY_TRC_ARG5(TraceType,Str,Arg1,Arg2,Arg3,Arg4,Arg5)
#define DHCP6_RLY_PKT_DUMP(TraceType,pBuf,Length,Str)
#endif
#endif /* _TRACE_H_ */

