/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6rdb.h,v 1.9 2015/06/17 10:46:53 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDH6RDB_H
#define _FSDH6RDB_H

UINT1 FsDhcp6RlyIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDhcp6RlySrvAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsDhcp6RlyOutIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsdh6r [] ={1,3,6,1,4,1,29601,2,41};
tSNMP_OID_TYPE fsdh6rOID = {9, fsdh6r};


UINT4 FsDhcp6RlyDebugTrace [ ] ={1,3,6,1,4,1,29601,2,41,1,1};
UINT4 FsDhcp6RlyTrapAdminControl [ ] ={1,3,6,1,4,1,29601,2,41,1,2};
UINT4 FsDhcp6RlySysLogAdminStatus [ ] ={1,3,6,1,4,1,29601,2,41,1,3};
UINT4 FsDhcp6RlyListenPort [ ] ={1,3,6,1,4,1,29601,2,41,1,4};
UINT4 FsDhcp6RlyClientTransmitPort [ ] ={1,3,6,1,4,1,29601,2,41,1,5};
UINT4 FsDhcp6RlyServerTransmitPort [ ] ={1,3,6,1,4,1,29601,2,41,1,6};
UINT4 FsDhcp6RlyOption37Control [ ] ={1,3,6,1,4,1,29601,2,41,1,7};
UINT4 FsDhcp6RlyPDRouteControl [ ] ={1,3,6,1,4,1,29601,2,41,1,8};
UINT4 FsDhcp6RlyIfIndex [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,1};
UINT4 FsDhcp6RlyIfHopThreshold [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,3};
UINT4 FsDhcp6RlyIfInformIn [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,4};
UINT4 FsDhcp6RlyIfRelayForwIn [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,5};
UINT4 FsDhcp6RlyIfRelayReplyIn [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,6};
UINT4 FsDhcp6RlyIfInvalidPktIn [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,10};
UINT4 FsDhcp6RlyIfCounterRest [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,11};
UINT4 FsDhcp6RlyIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,12};
UINT4 FsDhcp6RlyIfRemoteIdOption [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,13};
UINT4 FsDhcp6RlyIfRemoteIdDUID [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,14};
UINT4 FsDhcp6RlyIfRemoteIdOptionValue [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,15};
UINT4 FsDhcp6RlyIfRemoteIdUserDefined [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,16};
UINT4 FsDhcp6RlyIfRelayForwOut [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,17};
UINT4 FsDhcp6RlyIfRelayReplyOut [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,18};
UINT4 FsDhcp6RlyIfRelayState [ ] ={1,3,6,1,4,1,29601,2,41,2,1,1,19};
UINT4 FsDhcp6RlyInIfIndex [ ] ={1,3,6,1,4,1,29601,2,41,2,2,1,1};
UINT4 FsDhcp6RlySrvAddress [ ] ={1,3,6,1,4,1,29601,2,41,2,2,1,2};
UINT4 FsDhcp6RlySrvAddressRowStatus [ ] ={1,3,6,1,4,1,29601,2,41,2,2,1,3};
UINT4 FsDhcp6RlyOutIfIndex [ ] ={1,3,6,1,4,1,29601,2,41,2,3,1,1};
UINT4 FsDhcp6RlyOutIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,41,2,3,1,2};
UINT4 FsDhcp6RlyTrapIfIndex [ ] ={1,3,6,1,4,1,29601,2,41,3,1};
UINT4 FsDhcp6RlyTrapInvalidMsgType [ ] ={1,3,6,1,4,1,29601,2,41,3,2};




tMbDbEntry fsdh6rMibEntry[]= {

{{11,FsDhcp6RlyDebugTrace}, NULL, FsDhcp6RlyDebugTraceGet, FsDhcp6RlyDebugTraceSet, FsDhcp6RlyDebugTraceTest, FsDhcp6RlyDebugTraceDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "critical"},

{{11,FsDhcp6RlyTrapAdminControl}, NULL, FsDhcp6RlyTrapAdminControlGet, FsDhcp6RlyTrapAdminControlSet, FsDhcp6RlyTrapAdminControlTest, FsDhcp6RlyTrapAdminControlDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "\0"},

{{11,FsDhcp6RlySysLogAdminStatus}, NULL, FsDhcp6RlySysLogAdminStatusGet, FsDhcp6RlySysLogAdminStatusSet, FsDhcp6RlySysLogAdminStatusTest, FsDhcp6RlySysLogAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDhcp6RlyListenPort}, NULL, FsDhcp6RlyListenPortGet, FsDhcp6RlyListenPortSet, FsDhcp6RlyListenPortTest, FsDhcp6RlyListenPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "547"},

{{11,FsDhcp6RlyClientTransmitPort}, NULL, FsDhcp6RlyClientTransmitPortGet, FsDhcp6RlyClientTransmitPortSet, FsDhcp6RlyClientTransmitPortTest, FsDhcp6RlyClientTransmitPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "546"},

{{11,FsDhcp6RlyServerTransmitPort}, NULL, FsDhcp6RlyServerTransmitPortGet, FsDhcp6RlyServerTransmitPortSet, FsDhcp6RlyServerTransmitPortTest, FsDhcp6RlyServerTransmitPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "547"},

{{11,FsDhcp6RlyOption37Control}, NULL, FsDhcp6RlyOption37ControlGet, FsDhcp6RlyOption37ControlSet, FsDhcp6RlyOption37ControlTest, FsDhcp6RlyOption37ControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDhcp6RlyPDRouteControl}, NULL, FsDhcp6RlyPDRouteControlGet, FsDhcp6RlyPDRouteControlSet, FsDhcp6RlyPDRouteControlTest, FsDhcp6RlyPDRouteControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsDhcp6RlyIfIndex}, GetNextIndexFsDhcp6RlyIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfHopThreshold}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfHopThresholdGet, FsDhcp6RlyIfHopThresholdSet, FsDhcp6RlyIfHopThresholdTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 0, "4"},

{{13,FsDhcp6RlyIfInformIn}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfInformInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRelayForwIn}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRelayForwInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRelayReplyIn}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRelayReplyInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfInvalidPktIn}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfInvalidPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfCounterRest}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfCounterRestGet, FsDhcp6RlyIfCounterRestSet, FsDhcp6RlyIfCounterRestTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRowStatus}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRowStatusGet, FsDhcp6RlyIfRowStatusSet, FsDhcp6RlyIfRowStatusTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 1, NULL},

{{13,FsDhcp6RlyIfRemoteIdOption}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRemoteIdOptionGet, FsDhcp6RlyIfRemoteIdOptionSet, FsDhcp6RlyIfRemoteIdOptionTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 0, "2"},

{{13,FsDhcp6RlyIfRemoteIdDUID}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRemoteIdDUIDGet, FsDhcp6RlyIfRemoteIdDUIDSet, FsDhcp6RlyIfRemoteIdDUIDTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 0, "0"},

{{13,FsDhcp6RlyIfRemoteIdOptionValue}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRemoteIdOptionValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRemoteIdUserDefined}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRemoteIdUserDefinedGet, FsDhcp6RlyIfRemoteIdUserDefinedSet, FsDhcp6RlyIfRemoteIdUserDefinedTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRelayForwOut}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRelayForwOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRelayReplyOut}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRelayReplyOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyIfRelayState}, GetNextIndexFsDhcp6RlyIfTable, FsDhcp6RlyIfRelayStateGet, FsDhcp6RlyIfRelayStateSet, FsDhcp6RlyIfRelayStateTest, FsDhcp6RlyIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6RlyIfTableINDEX, 1, 0, 0, NULL},

{{13,FsDhcp6RlyInIfIndex}, GetNextIndexFsDhcp6RlySrvAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6RlySrvAddressTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6RlySrvAddress}, GetNextIndexFsDhcp6RlySrvAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsDhcp6RlySrvAddressTableINDEX, 2, 0, 0, NULL},

{{13,FsDhcp6RlySrvAddressRowStatus}, GetNextIndexFsDhcp6RlySrvAddressTable, FsDhcp6RlySrvAddressRowStatusGet, FsDhcp6RlySrvAddressRowStatusSet, FsDhcp6RlySrvAddressRowStatusTest, FsDhcp6RlySrvAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6RlySrvAddressTableINDEX, 2, 0, 1, NULL},

{{13,FsDhcp6RlyOutIfIndex}, GetNextIndexFsDhcp6RlyOutIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDhcp6RlyOutIfTableINDEX, 3, 0, 0, NULL},

{{13,FsDhcp6RlyOutIfRowStatus}, GetNextIndexFsDhcp6RlyOutIfTable, FsDhcp6RlyOutIfRowStatusGet, FsDhcp6RlyOutIfRowStatusSet, FsDhcp6RlyOutIfRowStatusTest, FsDhcp6RlyOutIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDhcp6RlyOutIfTableINDEX, 3, 0, 1, NULL},

{{11,FsDhcp6RlyTrapIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsDhcp6RlyTrapInvalidMsgType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsdh6rEntry = { 30, fsdh6rMibEntry };

#endif /* _FSDH6RDB_H */

