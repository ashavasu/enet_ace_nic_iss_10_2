/* $Id: d6rlmacs.h,v 1.9 2014/12/12 11:49:55 siva Exp $   */

#ifndef   _DHCP6_RLY_INC_H
#define   _DHCP6_RLY_INC_H

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "fssocket.h"
#include "iss.h"
#include "fssnmp.h"
#include "snmccons.h"
#include "snmputil.h"
#include "trace.h"
#include "ipv6.h"
#include "ip6util.h"
#include "fssyslog.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#include "cfanpwr.h"
#endif
#include "dhcp6.h"
#include "d6rlcons.h"
#include "d6rltdfs.h"
#include "d6rlextn.h"
#include "d6rlprot.h"
#include "d6rltrc.h"
#include "d6rltrap.h"
#include "d6rlcli.h"
#include "fsdh6rlw.h"
#include "fsdh6rwr.h"
#include "d6rlsz.h"
#endif
#ifndef _DHCP6_RLY_MACS_H
#define _DHCP6_RLY_MACS_H

/* Task Id for Dhcpv6 */
#define DHCP6_RLY_TASK_ID                 gDhcp6RlyGblInfo.TaskId

#define DHCP6_RLY_IS_INITIALIZED          gDhcp6RlyGblInfo.b1IsInitialized
#define DHCP6_RLY_LISTEN_PORT             gDhcp6RlyGblInfo.i4ListenPort
#define DHCP6_RLY_SRV_TRANSMIT_PORT       gDhcp6RlyGblInfo.i4SrvPort
#define DHCP6_RLY_CLNT_TRANSMIT_PORT      gDhcp6RlyGblInfo.i4ClntPort
#define DHCP6_RLY_TRAP_CONTROL            gDhcp6RlyGblInfo.u1TrapControl
#define DHCP6_RLY_DEBUG_TRACE             gDhcp6RlyGblInfo.u4GblDbgTrc
/* used for redundancy */
#define DHCP6RLY_RM_MSG_MEMPOOL_ID   D6RLMemPoolIds[MAX_D6RL_RM_QUE_DEPTH_SIZING_ID]
#define DHCP6RLY_RM_PKT_Q_ID gDhcp6RlyRmPktQId

#ifdef L2RED_WANTED
#define DHCP6RLY_GET_NODE_STATUS()  gDhcp6RlyRedGblInfo.u1NodeStatus
#else
#define DHCP6RLY_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define DHCP6RLY_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define DHCP6RLY_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#define DHCP6RLY_HA_VERSION                       1


/* Offset */
#define  DHCP6_RLY_OFFSET(x,y)            ((UINT4)(&(((x *)0)->y)))

/* Semaphore for Dhcpv6 relay mutual exclusion lock */
#define DHCP6_RLY_SEM_ID                   gDhcp6RlyGblInfo.SemId
#define DHCP6_RLY_LOCK                     D6RlMainLock
#define DHCP6_RLY_UNLOCK                   D6RlMainUnLock

#define DHCP6_RLY_QID                      gDhcp6RlyGblInfo.TaskQId
#define DHCP6_RLY_SOCKET_FD                gDhcp6RlyGblInfo.i4SockId

#define DHCP6_RLY_TRAP_CONTROL_OPTION      gDhcp6RlyGblInfo.u1TrapControl
/* Init complete indication */
#define DHCP6_RLY_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)

/* Wait event flags */
#define DHCP6_RLY_EVENT_NO_WAIT_FLAG       OSIX_NO_WAIT
#define DHCP6_RLY_EVENT_WAIT_FLAG          OSIX_WAIT

/* Syslog Id for DHCP6 RELAY */
#define DHCP6_RLY_SYSLOG_ID                gDhcp6RlyGblInfo.u4SyslogId
#define DHCP6_RLY_SYS_LOG_OPTION           gDhcp6RlyGblInfo.b1SysLogEnabled

/* Remote Id Option for DHCP6 Relay */
#define DHCP6_RLY_REMOTEID_STATUS   gDhcp6RlyGblInfo.b1RemoteIdOptionControl

/* PD Route control for DHCP6 Relay */
#define DHCP6_RLY_PDROUTE_STATUS gDhcp6RlyGblInfo.b1PDRouteControl

/*Send Buffer */
#define DHCP6_RLY_SEND_BUF                 gDhcp6RlyGblInfo.au1SendBuf
#define DHCP6_RLY_Q_MSG_BLK_SIZE           sizeof(tDhcp6RlyQMsg)
#define DHCP6_RLY_Q_MSG_POOL_ID            \
                 D6RLMemPoolIds[MAX_D6RL_RLY_QMSGS_SIZING_ID]
/* Interface table related */
#define DHCP6_RLY_IF_TBL_BLK_SIZE          sizeof(tDhcp6RlyIfInfo)
#define DHCP6_RLY_IF_TBL_POOL_ID           \
                 D6RLMemPoolIds[MAX_D6RL_RLY_IF_ENTRIES_SIZING_ID]
/* Server Address table related */
#define DHCP6_RLY_SRV_TBL_BLK_SIZE         sizeof(tDhcp6RlySrvAddrInfo)
#define DHCP6_RLY_SRV_TBL_POOL_ID          \
                 D6RLMemPoolIds[MAX_D6RL_RLY_ENTRIES_SIZING_ID]
/* Out If table related */
#define DHCP6_RLY_OUTIF_TBL_BLK_SIZE       sizeof(tDhcp6RlyOutIfInfo)
#define DHCP6_RLY_OUTIF_TBL_POOL_ID        \
                 D6RLMemPoolIds[MAX_D6RL_RLY_OUT_ENTRIES_SIZING_ID]
/* Prefix Delegation Route table related */
#define DHCP6_RLY_PD_TBL_BLK_SIZE       sizeof(tDhcp6RlyPDEntry)
#define DHCP6_RLY_PD_TBL_POOL_ID        \
                 D6RLMemPoolIds[MAX_D6RL_RLY_PD_ROUTES_SIZING_ID]

/* Table roots */
#define DHCP6_RLY_IF_TABLE                 gDhcp6RlyGblInfo.pIfTable
#define DHCP6_RLY_SRV_TABLE                gDhcp6RlyGblInfo.pSrvAddrTable
#define DHCP6_RLY_OUTIF_TABLE              gDhcp6RlyGblInfo.pOutIfTable
#define DHCP6_RLY_PD_TABLE                 gDhcp6RlyGblInfo.pPDTable

/* Length */
#define DHCP6_RLY_MAX_IF_NAME_LEN          IP6_MAX_IF_NAME_LEN

/* Dhcpv6 Packet construction and processing macros */
/* DHCPv6 PACKET CONSTRUCTION MACROS */
#define DHCP6_RLY_PUT_1_BYTE(pMsg, u4Offset, pu1MesgType) \
{\
    if (u4Offset < sizeof (DHCP6_RLY_SEND_BUF)) \
    {\
        MEMCPY (pMsg + u4Offset, pu1MesgType, 1); \
        u4Offset += 1; \
    }\
}

#define DHCP6_RLY_PUT_2_BYTE(pMsg, u4Offset, pu2MesgType) \
{\
    *pu2MesgType = OSIX_HTONS(*pu2MesgType); \
    if (u4Offset < sizeof (DHCP6_RLY_SEND_BUF)) \
    {\
        MEMCPY (pMsg + u4Offset, (UINT1 *) pu2MesgType, 2); \
        *pu2MesgType = OSIX_NTOHS (*pu2MesgType); \
        u4Offset += 2; \
    }\
}

#define DHCP6_RLY_PUT_4_BYTE(pMsg, u4Offset, pu4MesgType) \
{\
    *pu4MesgType =OSIX_HTONL(*pu4MesgType); \
    if (u4Offset < sizeof (DHCP6_RLY_SEND_BUF)) \
    {\
        MEMCPY (pMsg + u4Offset, (UINT1 *) pu4MesgType, 4); \
        *pu4MesgType = OSIX_NTOHL (*pu4MesgType); \
        u4Offset += 4; \
    }\
}

#define DHCP6_RLY_PUT_N_BYTE(pdest, psrc, u4Offset, u4Size) \
{\
    if (u4Offset < sizeof (DHCP6_RLY_SEND_BUF)) \
    {\
        MEMCPY (pdest + u4Offset, psrc, u4Size); \
        u4Offset +=u4Size; \
    }\
}

/* DATA RETRIEVAL FROM DHCPv6 PACKET */
#define DHCP6_RLY_GET_1_BYTE(pMsg, u4Offset, pu1MesgType) \
{\
    MEMCPY (pu1MesgType, pMsg + u4Offset, 1); \
    u4Offset += 1; \
}

#define DHCP6_RLY_GET_2_BYTE(pMsg, u4Offset, pu2MesgType) \
{\
    MEMCPY ((UINT1 *)pu2MesgType, pMsg + u4Offset, 2); \
    *pu2MesgType = OSIX_NTOHS (*pu2MesgType); \
    u4Offset += 2; \
}

#define DHCP6_RLY_GET_4_BYTE(pMsg, u4Offset, pu4MesgType) \
{\
    MEMCPY ((UINT1 *)pu4MesgType, pMsg + u4Offset, 4); \
    *pu4MesgType = OSIX_NTOHL (*pu4MesgType); \
    u4Offset += 4; \
}

#define DHCP6_RLY_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    MEMCPY (pdest, psrc + u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define DHCP6_RLY_SET_ALL_SRV_ADDRESS(a) {(a).u4_addr[0]=OSIX_HTONL (0xff050000);\
                                 (a).u4_addr[1]=0;\
                                 (a).u4_addr[2]=0;\
                                 (a).u4_addr[3]=OSIX_HTONL(0x00010003);}

#define DHCP6_RLY_SET_ALL_RLY_SRV_ADDRESS(a) {(a).u4_addr[0]=OSIX_HTONL(0xff020000);\
                                 (a).u4_addr[1]=0;\
                                 (a).u4_addr[2]=0;\
                                 (a).u4_addr[3]=OSIX_HTONL (0x00010002);}

#define DHCP6_RLY_INET_NTOA(ipaddr)          UtlInetNtoa(ipaddr)

#define DHCP6_RLY_CONVERT_IPADDR_TO_STR(pString, u4Value)\
{\
    tUtlInAddr          IpAddr;\
\
         IpAddr.u4Addr = OSIX_NTOHL (u4Value);\
\
         pString = (UINT1 *)DHCP6_RLY_INET_NTOA (IpAddr);\
\
}

#endif
