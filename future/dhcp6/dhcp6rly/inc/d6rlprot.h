/* $Id: d6rlprot.h,v 1.13 2015/07/18 11:22:15 siva Exp $   */

#include "d6rlinc.h"

#ifndef _DHCP6_RLY_PROT_H
#define _DHCP6_RLY_RROT_H

/* d6rlskt.c  */
PUBLIC VOID D6RlSktDataRcvd PROTO ((INT4 i4SockId));
PUBLIC INT4 D6RlSktInit PROTO ((VOID));
PUBLIC VOID D6RlSktDeInit PROTO ((VOID));

PUBLIC VOID D6RlParsRcvAndProcessPkt PROTO ((VOID));

/* D6RlPars.c */
PUBLIC INT4
D6RlParsProcessPkt PROTO ((tDhcp6RlyUdp6Params * pUdp6Dhcp6RParams,
                           UINT1 * pu1Dhcp6Buf, UINT2 u2Len));
PUBLIC INT2
D6RlParsRemIdOption PROTO ((INT4 i4Dh6RlIfIndex,
             INT4 *pi4RetRemIdOptionType,
             tDhcp6RlyRemIdOption *pDh6RlyRemIdBuf));
#ifndef LNXIP6_WANTED
PUBLIC VOID
D6RlAddNdCache PROTO ((UINT4 u4Index,
            UINT1 *pu1Dhcp6Buf, UINT2 u2Len));
#endif

/* d6rlutl.c */
PUBLIC INT4
D6RlUtlGetIfName PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfName));
PUBLIC INT4
D6RlUtlGetLinkAddr PROTO ((UINT4 u4IfIndex, UINT1 *pu1IpV6Addr));
PUBLIC INT4
D6RlUtlIp6AddrCompare PROTO ((tIp6Addr * pAddr1, tIp6Addr * pAddr2));
PUBLIC VOID
D6RlUtlDeleteIfFromAll PROTO ((UINT4 u4Index));
PUBLIC INT4
D6RlUtlGetGlobalAddr PROTO ((UINT4 u4IfIndex, tIp6Addr * pDstAddr,
                             tIp6Addr * pSrcAddr));
PUBLIC INT4 D6RlUtlGetRoute PROTO ((UINT1 *au1IpV6Addr, UINT4 *pu4IfIndex));
PUBLIC INT4 D6RlUtlCliConfGetIfName PROTO ((UINT4 , INT1 *));
PUBLIC INT4 D6RlUtlCfaValidateIfIndex PROTO ((UINT4 u4IfIndex));
/* d6rlif.c */
PUBLIC INT4 D6RlIfCreateTable PROTO ((VOID));
PUBLIC VOID D6RlIfDeleteTable PROTO ((VOID));
PUBLIC VOID D6RlIfServerListDelNode PROTO ((tDhcp6RlySrvAddrInfo * pSrvInfo));
PUBLIC INT4 D6RlIfServerListAddNode PROTO ((tDhcp6RlySrvAddrInfo * pSrvInfo));
PUBLIC tDhcp6RlySrvAddrInfo * D6RlIfServerListGetFirstNode PROTO ((tDhcp6RlyIfInfo * pIfInfo));
PUBLIC tDhcp6RlySrvAddrInfo * D6RlIfServerListGetNextNode PROTO ((tDhcp6RlyIfInfo * pIfInfo, tDhcp6RlySrvAddrInfo * pSrvInfo));
PUBLIC tDhcp6RlyIfInfo * D6RlIfGetNode PROTO ((UINT4 u4IfIndex));
PUBLIC tDhcp6RlyIfInfo * D6RlIfGetNextNode PROTO ((UINT4 u4Index));
PUBLIC tDhcp6RlyIfInfo * D6RlIfGetFirstNode PROTO ((VOID));
PUBLIC VOID D6RlIfDrainTable PROTO ((VOID));
PUBLIC INT4 D6RlIfRBFree PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC VOID D6RlIfDelNode PROTO ((tDhcp6RlyIfInfo * pInterface));
PUBLIC INT4 D6RlIfAddNode PROTO ((tDhcp6RlyIfInfo * pInterface));
PUBLIC tDhcp6RlyIfInfo * D6RlIfCreateNode PROTO ((VOID));
PUBLIC INT4 
D6RlIfCheckRowCanSet PROTO ((INT4 i4FsDhcp6RlyIfIndex, UINT4 *pu4ErrorCode));
PUBLIC INT4
D6RCheckIfDuidIsValid PROTO ((tSNMP_OCTET_STRING_TYPE 
         *pTestValFsDhcp6RlyIfRemoteIdDUID,
         UINT4 *pu4ErrorCode));
PUBLIC INT4
D6RCheckIfUserDefinedAsciiIsValid PROTO ((tSNMP_OCTET_STRING_TYPE
                       * pTestValFsDhcp6RlyIfRemoteIdUserDefined, UINT4 *pu4ErrorCode));
PUBLIC INT4 D6RlIfRBCmp PROTO ((tRBElem * e1, tRBElem * e2));

/* d6rlouif.c */
PUBLIC INT4 D6RlOuIfCreateTable PROTO ((VOID));
PUBLIC VOID D6RlOuIfDeleteTable PROTO ((VOID));

PUBLIC tDhcp6RlyOutIfInfo * 
D6RlOuIfGetNode PROTO (( UINT4 u4InIfIndex,
                         tIp6Addr *pServAddr, UINT4 u4Index ));
PUBLIC tDhcp6RlyOutIfInfo *
D6RlOuIfGetNextNode PROTO (( UINT4 u4InIfIndex,
                             tIp6Addr *pServAddr, UINT4 u4Index ));
PUBLIC tDhcp6RlyOutIfInfo * D6RlOuIfGetFirstNode PROTO ((VOID));
PUBLIC VOID D6RlOuIfDrainTable PROTO ((VOID));
PUBLIC INT4 D6RlOuIfRBFree PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC VOID D6RlOuIfDelNode PROTO ((tDhcp6RlyOutIfInfo * pOutIf));
PUBLIC INT4 D6RlOuIfAddNode PROTO ((tDhcp6RlyOutIfInfo * pOutIf));
PUBLIC tDhcp6RlyOutIfInfo * D6RlOuIfCreateNode PROTO ((VOID));
PUBLIC INT4 D6RlOuIfRBCmp PROTO ((tRBElem * e1, tRBElem * e2));

/* d6rlsrv.c */
PUBLIC INT4 D6RlSrvCreateTable PROTO ((VOID));
PUBLIC VOID D6RlSrvDeleteTable PROTO ((VOID));
PUBLIC VOID D6RlSrvOutIfListDelNode PROTO ((tDhcp6RlyOutIfInfo * pOutIfInfo));
PUBLIC INT4 D6RlSrvOutIfListAddNode PROTO ((tDhcp6RlyOutIfInfo * pOutIfInfo));
PUBLIC tDhcp6RlyOutIfInfo * D6RlSrvOutIfListGetFirstNode PROTO ((tDhcp6RlySrvAddrInfo * pSrvInfo));
PUBLIC tDhcp6RlyOutIfInfo * D6RlSrvOutIfListGetNextNode PROTO ((tDhcp6RlySrvAddrInfo * pSrvInfo, tDhcp6RlyOutIfInfo * pOutIfInfo));

PUBLIC INT4 D6RlSrvRBCmp PROTO ((tRBElem * e1, tRBElem * e2));
PUBLIC tDhcp6RlySrvAddrInfo * D6RlSrvCreateNode PROTO ((VOID));
PUBLIC INT4 D6RlSrvAddNode PROTO ((tDhcp6RlySrvAddrInfo * pSrvAddr));
PUBLIC tDhcp6RlySrvAddrInfo *
D6RlSrvGetNode PROTO ((UINT4 u4IfIndex, tIp6Addr *pSrvAddr ));
PUBLIC VOID D6RlSrvDelNode PROTO ((tDhcp6RlySrvAddrInfo * pSrvAddr));
PUBLIC INT4 D6RlSrvRBFree PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC VOID D6RlSrvDrainTable PROTO ((VOID));
PUBLIC tDhcp6RlySrvAddrInfo * D6RlSrvGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6RlySrvAddrInfo * 
D6RlSrvGetNextNode PROTO ((UINT4 u4Index, tIp6Addr *pSrvAddr));
PUBLIC tDhcp6RlySrvAddrInfo * D6RlSrvGetFirstNodeFromList (tDhcp6RlyIfInfo *pDhcp6RlyIfInfo);

/* d6rldb.c */
PUBLIC INT4 D6RlPDCreateEntry PROTO ((VOID));
PUBLIC VOID D6RlPDDeleteEntry PROTO ((VOID));
PUBLIC INT4 D6RlPDRBCmp PROTO ((tRBElem * e1, tRBElem * e2));
PUBLIC tDhcp6RlyPDEntry * D6RlPDCreateNode PROTO ((VOID));
PUBLIC INT4 D6RlPDAddNode PROTO ((tDhcp6RlyPDEntry * pPDEntry));
PUBLIC VOID D6RlPDDelNode PROTO ((tDhcp6RlyPDEntry * pPDEntry));
PUBLIC INT4 D6RlPDRBFree PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC tDhcp6RlyPDEntry * D6RlPDGetNode PROTO ((tIp6Addr * pIp6Prefix, UINT1 u1Prefixlength));
PUBLIC tDhcp6RlyPDEntry * D6RlPDGetFirstNode PROTO ((VOID));
PUBLIC tDhcp6RlyPDEntry * D6RlPDGetNextNode PROTO ((tIp6Addr * pIp6Prefix, UINT1 u1Prefixlength));

/* d6rlpd.c */
PUBLIC INT4 D6RlParsPDInfo PROTO ((UINT1 * pu1ParsPDBuf,UINT2 u2Len, UINT4 u4MsgType, UINT4 u4TxId, UINT4 u4RouteIndex, tIp6Addr * pNextHop));
PUBLIC INT4 D6RlLeakPDRoute PROTO ((tDhcp6RlyPDEntry * pPDEntry, UINT1 u1CommandType));
PUBLIC INT4 D6RlPDRouteDeletionPrefix PROTO ((tIp6Addr * pIpv6Prefix, UINT1 u1Prefixlength, UINT4 u4TransactionId,UINT4 u4T1Time,UINT4 u4T2Time, UINT4 u4MessageType,UINT1 u1Flag));
PUBLIC VOID Dhcp6RlyRouteControlDisable PROTO((VOID));
PUBLIC INT4 D6RlPDNodeDeletion PROTO ((tIp6Addr * pIpv6Prefix,UINT1 u1Prefixlength));
PUBLIC INT4 D6RlPDRouteFillTxId PROTO ((tIp6Addr * pIpv6Prefix, UINT1, UINT4, UINT4));

/* d6rltmr.c */
PUBLIC INT4 D6RlTmrInit PROTO ((VOID));
PUBLIC INT4 D6RlTmrDeInit PROTO ((VOID));
PUBLIC INT4 D6RlTmrStart PROTO ((tTmrBlk * pDhcp6RlyTmr, UINT1 u1TmrId, UINT4 u4Duration, UINT4 u4MilliSecs));
PUBLIC INT4 D6RlTmrStopTimer PROTO ((tTmrBlk * pDhcp6RlyTmr));
PUBLIC VOID D6RlTmrExpHandler PROTO ((VOID));
PUBLIC VOID D6RlTmrValidTimeExpired PROTO ((VOID *));

/* d6rlred.c */

PUBLIC INT4  Dhcp6RlyRedInitGlobalInfo PROTO ((VOID));
PUBLIC INT4  Dhcp6RlyRedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));
PUBLIC INT4  Dhcp6RlyRedRmDeRegisterProtocols PROTO ((VOID));
PUBLIC INT4  Dhcp6RlyRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
PUBLIC VOID  Dhcp6RlyRedHandleRmEvents PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedHandleGoActive PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedHandleGoStandby PROTO ((tDhcp6RlyRmMsg * pMsg));
PUBLIC VOID  Dhcp6RlyRedHandleStandbyToActive PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedHandleActiveToStandby PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedHandleIdleToActive PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedHandleIdleToStandby PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC VOID  Dhcp6RlyRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC UINT4 Dhcp6RlyRedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
PUBLIC INT4  Dhcp6RlyRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
PUBLIC INT4  Dhcp6RlyRedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));
PUBLIC VOID  Dhcp6RlyRedSendBulkReqMsg PROTO ((VOID));
PUBLIC INT4  Dhcp6RlyRedDeInitGlobalInfo PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedSendBulkUpdTailMsg PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
PUBLIC VOID  Dhcp6RlyRedSendBulkUpdMsg PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedProcessDynamicInfo PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
PUBLIC VOID  Dhcp6RlyRedDynDataDescInit PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedDescrTblInit PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedDbNodeInit PROTO ((tDbTblNode * pDhcp6RlyDbNode));
PUBLIC VOID  Dhcp6RlyRedDbUtilAddTblNode PROTO ((tDbTblDescriptor * pDhcp6RlyDataDesc,tDbTblNode * pDhcp6RlyDbNode));
PUBLIC VOID  Dhcp6RlyRedSyncDynInfo PROTO ((VOID));
PUBLIC INT4  Dhcp6RlyAddRouteAndStartTimer PROTO ((VOID));
PUBLIC VOID  Dhcp6RlyRedAddAllNodeInDbTbl PROTO ((VOID));

extern INT4    Rtm6Ip6IsFreeRtTblEntriesAvailable PROTO ((VOID));

/* d6rltrc.h */
PUBLIC INT4
D6RlTrcGetTraceOptionValue PROTO ((UINT1 *pu1TraceInput, INT4 i4Tracelen));
PUBLIC INT4
D6RlTrcGetTraceInputValue PROTO ((UINT1 *pu1TraceInput, UINT4 u4TraceOption));

#endif

