/* $Id: d6rlsz.h,v 1.2 2014/06/23 11:25:50 siva Exp $   */

enum {
    MAX_D6RL_RLY_ENTRIES_SIZING_ID,
    MAX_D6RL_RLY_IF_ENTRIES_SIZING_ID,
    MAX_D6RL_RLY_OUT_ENTRIES_SIZING_ID,
    MAX_D6RL_RLY_QMSGS_SIZING_ID,
    MAX_D6RL_RLY_PD_ROUTES_SIZING_ID,
    MAX_D6RL_RM_QUE_DEPTH_SIZING_ID,
    D6RL_MAX_SIZING_ID
};


#ifdef  _D6RLSZ_C
tMemPoolId D6RLMemPoolIds[ D6RL_MAX_SIZING_ID];
INT4  D6rlSizingMemCreateMemPools(VOID);
VOID  D6rlSizingMemDeleteMemPools(VOID);
INT4  D6rlSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _D6RLSZ_C  */
extern tMemPoolId D6RLMemPoolIds[ ];
extern INT4  D6rlSizingMemCreateMemPools(VOID);
extern VOID  D6rlSizingMemDeleteMemPools(VOID);
extern INT4  D6rlSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _D6RLSZ_C  */


#ifdef  _D6RLSZ_C
tFsModSizingParams FsD6RLSizingParams [] = {
{ "tDhcp6RlySrvAddrInfo", "MAX_D6RL_RLY_ENTRIES", sizeof(tDhcp6RlySrvAddrInfo),MAX_D6RL_RLY_ENTRIES, MAX_D6RL_RLY_ENTRIES,0 },
{ "tDhcp6RlyIfInfo", "MAX_D6RL_RLY_IF_ENTRIES", sizeof(tDhcp6RlyIfInfo),MAX_D6RL_RLY_IF_ENTRIES, MAX_D6RL_RLY_IF_ENTRIES,0 },
{ "tDhcp6RlyOutIfInfo", "MAX_D6RL_RLY_OUT_ENTRIES", sizeof(tDhcp6RlyOutIfInfo),MAX_D6RL_RLY_OUT_ENTRIES, MAX_D6RL_RLY_OUT_ENTRIES,0 },
{ "tDhcp6RlyQMsg", "MAX_D6RL_RLY_QMSGS", sizeof(tDhcp6RlyQMsg),MAX_D6RL_RLY_QMSGS, MAX_D6RL_RLY_QMSGS,0 },
{ "tDhcp6RlyPDEntry", "MAX_D6RL_RLY_PD_ROUTES", sizeof(tDhcp6RlyPDEntry),MAX_D6RL_RLY_PD_ROUTES, MAX_D6RL_RLY_PD_ROUTES,0 },
{"tDhcp6RlyRmMsg", "MAX_D6RL_RM_QUE_DEPTH", sizeof(tDhcp6RlyRmMsg),MAX_D6RL_RM_QUE_DEPTH,MAX_D6RL_RM_QUE_DEPTH,0},
{"\0","\0",0,0,0,0}
};
#else  /*  _D6RLSZ_C  */
extern tFsModSizingParams FsD6RLSizingParams [];
#endif /*  _D6RLSZ_C  */


