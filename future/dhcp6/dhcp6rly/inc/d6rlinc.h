/* $Id: d6rlinc.h,v 1.11 2014/12/19 10:18:13 siva Exp $   */

/*****************************************************************************
 *  FILE       : dhc6rinc.h
 *  DESCRIPTION: Contains the all the includes necessary for dhcpv6 relay
 *****************************************************************************/


#ifndef   _DHCP6_RLY_INC_H
#define   _DHCP6_RLY_INC_H

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "fssocket.h"
#include "iss.h"
#include "fssnmp.h"
#include "snmccons.h"
#include "snmputil.h"
#include "trace.h"
#include "ipv6.h"
#include "vcm.h"
#include "ip6util.h"
#include "fssyslog.h"
#include "dbutil.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#include "cfanpwr.h"
#endif
#include "dhcp6.h"
#include "d6rlmacs.h"
#include "d6rlcons.h"
#include "d6rltdfs.h"
#include "d6rlextn.h"
#include "d6rlprot.h"
#include "d6rltrc.h"
#include "d6rltrap.h"
#include "d6rlcli.h"
#include "fsdh6rlw.h"
#include "fsdh6rwr.h"
#include "d6rlsz.h"
#include "d6rlred.h"
#include "d6rlglob.h"
#endif
