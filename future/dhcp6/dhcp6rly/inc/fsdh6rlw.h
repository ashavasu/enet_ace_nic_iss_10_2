/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6rlw.h,v 1.8 2015/06/17 10:46:53 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6RlyDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6RlyTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6RlySysLogAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsDhcp6RlyOption37Control ARG_LIST((INT4 *));

INT1
nmhGetFsDhcp6RlyPDRouteControl ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsDhcp6RlyListenPort ARG_LIST((INT4 *));
INT1
nmhGetFsDhcp6RlyClientTransmitPort ARG_LIST((INT4 *));
INT1
nmhGetFsDhcp6RlyServerTransmitPort ARG_LIST((INT4 *));
INT1
nmhSetFsDhcp6RlyDebugTrace ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6RlyTrapAdminControl ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6RlySysLogAdminStatus ARG_LIST((INT4 ));
INT1
nmhSetFsDhcp6RlyListenPort ARG_LIST((INT4 ));
INT1
nmhSetFsDhcp6RlyClientTransmitPort ARG_LIST((INT4 ));
INT1
nmhSetFsDhcp6RlyServerTransmitPort ARG_LIST((INT4 ));

INT1
nmhSetFsDhcp6RlyOption37Control ARG_LIST((INT4 ));

INT1
nmhSetFsDhcp6RlyPDRouteControl ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6RlyDebugTrace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6RlyTrapAdminControl ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6RlySysLogAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyListenPort ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhTestv2FsDhcp6RlyClientTransmitPort ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhTestv2FsDhcp6RlyServerTransmitPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyOption37Control ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyPDRouteControl ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6RlyDebugTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6RlyTrapAdminControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6RlySysLogAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsDhcp6RlyListenPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsDhcp6RlyClientTransmitPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsDhcp6RlyServerTransmitPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6RlyOption37Control ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDhcp6RlyPDRouteControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsDhcp6RlyIfTable. */
INT1
nmhValidateIndexInstanceFsDhcp6RlyIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6RlyIfTable  */

INT1
nmhGetFirstIndexFsDhcp6RlyIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6RlyIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1

nmhGetFsDhcp6RlyIfHopThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6RlyIfInformIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6RlyIfRelayForwIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6RlyIfRelayReplyIn ARG_LIST((INT4 ,UINT4 *));

INT1



nmhGetFsDhcp6RlyIfInvalidPktIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6RlyIfCounterRest ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6RlyIfRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6RlyIfRemoteIdOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDhcp6RlyIfRemoteIdDUID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6RlyIfRemoteIdOptionValue ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6RlyIfRemoteIdUserDefined ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDhcp6RlyIfRelayForwOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6RlyIfRelayReplyOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDhcp6RlyIfRelayState ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1

nmhSetFsDhcp6RlyIfHopThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6RlyIfCounterRest ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6RlyIfRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6RlyIfRemoteIdOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDhcp6RlyIfRemoteIdDUID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6RlyIfRemoteIdUserDefined ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDhcp6RlyIfRelayState ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */


INT1
nmhTestv2FsDhcp6RlyIfHopThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyIfCounterRest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyIfRemoteIdOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDhcp6RlyIfRemoteIdDUID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6RlyIfRemoteIdUserDefined ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDhcp6RlyIfRelayState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6RlyIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6RlySrvAddressTable. */
INT1
nmhValidateIndexInstanceFsDhcp6RlySrvAddressTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6RlySrvAddressTable  */

INT1
nmhGetFirstIndexFsDhcp6RlySrvAddressTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6RlySrvAddressTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6RlySrvAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6RlySrvAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6RlySrvAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6RlySrvAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDhcp6RlyOutIfTable. */
INT1
nmhValidateIndexInstanceFsDhcp6RlyOutIfTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDhcp6RlyOutIfTable  */

INT1
nmhGetFirstIndexFsDhcp6RlyOutIfTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDhcp6RlyOutIfTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDhcp6RlyOutIfRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDhcp6RlyOutIfRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDhcp6RlyOutIfRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDhcp6RlyOutIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
