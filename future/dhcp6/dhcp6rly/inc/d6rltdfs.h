/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: d6rltdfs.h,v 1.18 2015/03/14 11:49:39 siva Exp $
 *  
 * Description:Header file contains data structures for Dhcpv6 Relay
 * 
 ********************************************************************/

#ifndef   _DHCP6_RLY_INC_H
#include "d6rlinc.h"
#endif
#ifndef _DHCP6_RLY_TDFS_H
#define _DHCP6_RLY_TDFS_H

/*******    GLOBAL DATA STRUCTURE *********/

typedef struct __Dhcp6RlyGblInfo
{
   tOsixTaskId          TaskId;
   tOsixQId             TaskQId;
   tOsixSemId           SemId;
   tRBTree              pIfTable;
   tRBTree              pSrvAddrTable;
   tRBTree              pOutIfTable;
   tRBTree              pPDTable;
   tTimerListId         RlyTmrLst;
   tTmrDesc             Dhcp6RlyTmrDesc;
   UINT4                u4GblDbgTrc;
   UINT4                u4SyslogId;
#ifdef NPAPI_WANTED
   UINT4         u4RlySrvFilterId;
   UINT4         u4RlyFilterId;
   UINT4         u4RlyClntBcastFilterId1;
   UINT4         u4RlyClntBcastFilterId2;
#endif
   INT4                 i4SockId;
   INT4                 i4ListenPort;
   INT4                 i4SrvPort;
   INT4                 i4ClntPort;
   UINT1                au1SendBuf [DHCP6_RLY_MAX_MTU_SIZE];
   UINT1                u1TrapControl;
   BOOL1                b1SysLogEnabled;
   BOOL1                b1IsInitialized;
   BOOL1                b1RemoteIdOptionControl;
   BOOL1                b1PDRouteControl;
   UINT1                au1pad[3];
}tDhcp6RlyGblInfo;

typedef struct __Dhcp6RlyRemIdOption
{
 INT4  i4RemIdLen;
 UINT4  u4MgmtIpAddr;
 UINT1  au1Duid[DHCP6_RLY_REM_ID_MAX_LEN];
 UINT1  au1UserDefined[DHCP6_RLY_REM_ID_MAX_LEN];
 UINT1  au1SwName[ISS_STR_LEN];
 UINT1  au1pad[2];
}tDhcp6RlyRemIdOption;

/**************    INTERFACE TABLE  ********************/
typedef struct __Dhcp6RlyIfInfo
{
   /* Indices
    * 1. IfIndex = u4IfIndex
    */
   tRBNodeEmbd   RbNode;
   tTMO_SLL      SrvAddrList;
   tDhcp6RlyRemIdOption Dhcp6RemIdOpt;
   INT4          i4RemoteIdOption;
   UINT4         u4IfIndex;
   UINT4         u4InformIn;
   UINT4         u4RelayForwIn;
   UINT4         u4RelayRepIn;
   UINT4         u4RelayReplyOut;
   UINT4         u4ReplyOut;
   UINT4         u4RelayForwOut;
   UINT4         u4InvalidPktIn;
   UINT4         u4Solicit;
   UINT4         u4Request;
   UINT4         u4Rebind;
   UINT4         u4Renew;
   UINT4         u4Release;
   UINT4         u4Decline;
   UINT4         u4Reconfig;
   UINT4         u4Confirm;
   UINT4         u4SrvAdv;
   UINT4         u4RelayReply;
   UINT1         u1HopThreshold;
   UINT1         u1CounterReset;
   UINT1         u1RowStatus;
   UINT1         u1Flag;
}tDhcp6RlyIfInfo;
/************    SERVER ADDRESS TABLE  *********************/
typedef struct __Dhcp6RlySrvAddrInfo
{
   /* Indices
    * 1. IfIndex         = pIf->u4IfIndex
    * 2. Server Address  = Addr
    */
   tTMO_SLL_NODE    InIfSllLink;
   tRBNodeEmbd      RbNode;
   tTMO_SLL         OutIfList;
   tDhcp6RlyIfInfo *pIf; 
   tIp6Addr         Addr;
   UINT1            u1RowStatus;
   UINT1            au1Pad[3];
}tDhcp6RlySrvAddrInfo;
/**************    OUT INTERFACE TABLE **********************/
typedef struct __Dhcp6RlyOutIfInfo
{
   /* Indices
    * 1. IfIndex         = pSrvAddr->pIf->u4IfIndex
    * 2. Server Address  = pSrvAddr->Addr
    * 3. Out IfIndex     = u4OutIfIndex 
    */
   tTMO_SLL_NODE         SrvAddrSllLink;
   tRBNodeEmbd           RbNode;
   tDhcp6RlySrvAddrInfo *pSrvAddr;
   UINT4                 u4OutIfIndex;
   UINT1                 u1RowStatus;
   UINT1                 au1Pad[3];
}tDhcp6RlyOutIfInfo;
/***************    UDP HEADER PARAMS   ********************/
typedef struct __Dhcp6RlyUdp6Params
{

   tIp6Addr    srcAddr;     /* Source address in the packet */
   tIp6Addr    dstAddr;     /* Destination address in the packet */
   UINT4       u4Index;     /* Index of the interface over which
                             * it is received */
   UINT4       u4Len;       /* Length of application data */
   UINT2       u2SrcPort;   /* Source port number over which packet
                             * came */
   UINT2       u2DstPort;   /* Destination port number over which
                             * packet is to be sent */
   INT4        i4Hlim;      /* Hop Limit of the packet */

}tDhcp6RlyUdp6Params;

/****************  RELAY REPLY HEADER   **********************/
typedef struct __Dhcp6RlyDest
{
   UINT4        u4InIfIndex;
   UINT4        u4OutIfIndex;
   tIp6Addr     LinkAddr;
   tIp6Addr     PeerAddr;
}tDhcp6RlyDestInfo;
typedef struct __Dhcp6RlyQMsg
{
   INT4        i4IfIndex;
   INT4        i4Command;
}tDhcp6RlyQMsg;

/*************** PREFIX DELEGATION TABLE *********************/

typedef struct __Dhcp6RlyPDEntry
{
    /*Indices
     * Prefix 
     * PrefixLen  */
 
    tTmrBlk       Dhcp6RlyTmrBlk;            /* Timer w.r.t ValidLifetime */
    tRBNodeEmbd   RbPDRouteNode;             /* Rbtree to maintain PD Details */
    tDbTblNode    RbPrefixDelegationDbNode;  /* Db Utility Node for redundancy offset */ 
    UINT1         u1Version;                 /* RM Dhcp6Rly Header version field*/
    UINT1         au1Pad[3];                 /* RM Dhcp6Rly Header Reserved field*/ 
    tIp6Addr      Prefix;                    /* Delegated prefix from server side */ 
    tIp6Addr      NextHop;                   /* Nexthop of the route to be added */
    UINT4         u4TransactionId;           /* Transaction id between msg-exchanges */ 
    UINT4         u4MsgType;                 /* used to store the messages in msg-exchange */
    UINT4         u4RtIndex;                 /* Route Index,on which nexthop is to be added */
    UINT4         u4T1Time;                  /* T1 Time in IA_PD */
    UINT4         u4T2Time;                  /* T2 Time in IA_PD */
    UINT4         u4ValidLifeTime;           /* ValidLifeTime of delegated prefix */
    UINT1         u1PrefixLen;               /* delegated prefix length */
    UINT1         u1Flag;                    /* Used for future purpose */
    UINT2         u2Option;                  /* inorder to validate status msg */

}tDhcp6RlyPDEntry;

/************** REDUNDANCY TABLE ********************************/

typedef struct _Dhcp6RlyRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tDhcp6RlyRmCtrlMsg;

typedef struct _Dhcp6RlyRmMsg
{
     tDhcp6RlyRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tDhcp6RlyRmMsg;

typedef struct _sDhcp6RlyRedGblInfo{
    UINT1 u1BulkUpdStatus; /* bulk update status
                            * DHCP6RLY_HA_UPD_NOT_STARTED 0
                            * DHCP6RLY_HA_UPD_COMPLETED 1
                            * DHCP6RLY_HA_UPD_IN_PROGRESS 2,
                            * DHCP6RLY_HA_UPD_ABORTED 3 */
    UINT1 u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1 u1NumPeersUp;     /* Indicates number of standby nodes
                               that are up. */
    UINT1 bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
}tDhcp6RlyRedGblInfo;

/* constants for timer types */
typedef enum {
    DHCP6RLY_VALID_TMR = 0,
    DHCP6RLY_MAX_TMRS
} enDhcp6RlyTmrId;

#endif

/*****************************************************************************/
