/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the common extern variables used in
 *              DHCP6 server module.
 *
 ***************************************************************************/

#ifndef  _DHCP6_RLY_EXTN_H
#define  _DHCP6_RLY_EXTN_H
extern  tDhcp6RlyGblInfo   gDhcp6RlyGblInfo;      /* Dhcp6 Relay Global*/
#endif
