Copyright (C) 2007 Aricent Inc . All Rights Reserved
===========================================


o   Introduction
    ============

Aricent Stateless Dynamic Host Configuration Protocol service for IPv6 
(SL-DHCPv6) is used by nodes to obtain configuration information, such 
as the addresses of DNS recursive name servers, that does not require 
the maintenance of any dynamic state for individual clients.

Aricent DHCPv6 is an implementation of IETF RFC 3736.

o   Contents:
    ========
The contents of this distribution are as follows:

  src/          - Header files for Stateless DHCPv6 Relay module.
  inc/          - Source files for Stateless DHCPv6 Relay module.
  Makefile      - The Aricent Stateless DHCPv6 Relay module level makefile
  make.h        - Contains compile flags and -I path used during make
  ../../mibs/fsdh6c.mib - Proprietary mib for Aricent Stateless DHCPv6 Relay  


The files in the following directories are needed for building the module.
o inc
o LR
o fsap2
o utils

Please refer the Aricent Stateless DHCPv6 Relay Porting Guide for instructions on building 
the module.


o   Important Documents
    ===================

Please refer to the following documents for further information-

- DHCP6CLNTpo.pdf     - Stateless DHCPv6 Relay Porting Guide
- DHCP6CLNTfs.pdf     - Stateless DHCPv6 Relay Feature Sheet
- DHCP6CLNTps.pdf     - Stateless DHCPv6 Relay Product Specification.
- DHCP6CLNTrn.pdf     - Stateless Release notes

END OF README
