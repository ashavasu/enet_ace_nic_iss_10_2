/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: buddy.h,v 1.8 2007/06/21 12:49:15 iss Exp $
 *
 * Description:This file contains definitions related to buddy
 *             management routines.
 *
 *******************************************************************/

#include <stdio.h>

#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "utlmacro.h"
#include "srmtmr.h"

typedef enum {BUDDY_INVALID=-1,BUDDY_FREE,BUDDY_ALLOCATED} tFlag;
typedef enum {BUDDY_FALSE=0,BUDDY_TRUE=1} tBudBoolean; 

typedef struct {
        INT4   i4Prev          ;     /* The links in the i_list */
        INT4   i4Next          ;
        UINT4  u4Multiples     ;     /* The size of the block as integral no */
        tFlag flag             ;
} tBlockInfo;   

typedef struct {
        UINT4           u4Id           ;
        tBudBoolean   active          ;
        UINT4           u4Size         ;          /* size of minimum unit */
        UINT4           u4NumUnits    ;
        UINT4           u4BufLen      ;
        UINT1           *pu1BufStart ;
	UINT4           u4AllocCount;
	UINT4           u4ReleaseCount;
	UINT4           u4FreeUnitsCount;
	UINT4           u4TypeOfMem;
        tBlockInfo    *block          ;
	UINT4           u4TimeStamp;
} tBuddyQueue;

#define MAX_QUEUES 3

extern tBuddyQueue buddyQueue[MAX_QUEUES];

INT4   BuddyCreate       (UINT4 u4Size, UINT4 u4NumUnits, UINT4 u4TypeOfMem);
INT4   BuddyDestroy      (UINT4 u4Id);
UINT4  BuddyAllocate     (UINT4 u4Size, UINT4 u4Id, UINT1 **ptr);
INT4   BuddyRelease      (UINT1* pu1Ptr, UINT4 u4Id);
INT4   BuddyDump         (UINT4 u4Id);

#define  BUDDY_DEBUG

#define BUDDY_THRESHOLD_VAL    0

/* Time interval in seconds between 2 Buddy debug message prints */
#define BUDDY_DEBUG_PRINT_TIME    60 

#define BUDDY_OK               0
#define BUDDY_ERR             -1
#define BLOCK_MALLOC_FAILURE  -2
#define BUFFER_MALLOC_FAILURE -3
#define NO_FREE_BLOCK         -4
#define INVALID_ID            -5
#define INVALID_SIZE          -6
#define NO_FIRST_FIT          -7
#define ILLEGAL_RELEASE       -8
#define STILL_ACTIVE          -9
#define CANT_CREATE           -10
#define ILLEGAL_SIZE_RELEASE  -11
#define INVALID_PARAMETER     -12  
