/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: buddy.c,v 1.11 2009/07/23 15:11:03 prabuc-iss Exp $
 *
 * Description: This file contains buddy management routines.
 *
 *******************************************************************/

#include "buddy.h"

tBuddyQueue         buddyQueue[MAX_QUEUES];

INT4                MapIdToIndex (UINT4 u4Id);
INT4                CreateBuddy (UINT4 size, UINT4 num_units, UINT4 u4Index);
void                InitBlockInfo (UINT4 numElements, UINT4 multiple,
                                   UINT4 u4Index);

UINT4               GetMultiple (UINT4 size, UINT4 u4Index);
UINT1              *GetBlock (UINT4 multiple, UINT4 u4Index);
INT4                FindExactFit (UINT4 multiple, UINT4 u4Index);
INT4                FindFirstFit (UINT4 multiple, UINT4 u4Index);
INT4                AdjustList (INT4 start, INT4 end, UINT4 total,
                                UINT4 u4Index);

/*******************************************************************************
* Function : BuddyCreate                                                      *
* Input    : u4Size       : Size of the memory block                          *
*            u4NumUnits  :number of memory blocks                            *
* Output   : None                                                              *
* Returns  : u4Index if successful  
              BUDDY_ERR on error                                               *
*******************************************************************************/
INT4
BuddyCreate (UINT4 u4Size, UINT4 u4NumUnits, UINT4 u4TypeOfMem)
{
    UINT4               u4Index;

    if ((u4Size == 0) || (u4NumUnits == 0))
        return (INVALID_PARAMETER);
    for (u4Index = 0; u4Index < MAX_QUEUES; u4Index++)
    {

        if (buddyQueue[u4Index].active == BUDDY_FALSE)
        {
            buddyQueue[u4Index].active = BUDDY_TRUE;
            buddyQueue[u4Index].u4TypeOfMem = u4TypeOfMem;
            buddyQueue[u4Index].u4TimeStamp = 0;
            if (CreateBuddy (u4Size, u4NumUnits, u4Index) == BUDDY_OK)
            {
                return (u4Index);
            }
            return (BUDDY_ERR);
        }
    }
    return (BUDDY_ERR);
}

/*******************************************************************************
* Function : BuddyDestroy                                                     *
* Input    : u4Id     : the ID of the buddy which has to be destroyed         *
* Output   : None 
* Returns  : BUDDY_OK if successful                                
             INVALID_ID on error                                              *
*******************************************************************************/
INT4
BuddyDestroy (u4Id)
     UINT4               u4Id;
{
    INT4                i4Index;
    i4Index = MapIdToIndex (u4Id);
    if (i4Index < 0)
        return (INVALID_ID);
    MEM_FREE (buddyQueue[i4Index].block);
    MEM_FREE (buddyQueue[i4Index].pu1BufStart);
    buddyQueue[i4Index].active = BUDDY_FALSE;
    buddyQueue[i4Index].u4BufLen = 0;
    buddyQueue[i4Index].u4Size = 0;
    buddyQueue[i4Index].u4NumUnits = 0;
    buddyQueue[i4Index].u4Id = 0;
    buddyQueue[i4Index].u4AllocCount = 0;
    buddyQueue[i4Index].u4ReleaseCount = 0;
    buddyQueue[i4Index].u4FreeUnitsCount = 0;
    buddyQueue[i4Index].u4TimeStamp = 0;

    return (BUDDY_OK);
}

/*******************************************************************************
* Function : MapIdToIndex                                                   *
* Input    : u4Id  : The ID which is mapped to index                          *
* Output   : None 
* Returns  : u4Id if successful 
             INVALID_ID otherwise                                              *
*******************************************************************************/
INT4
MapIdToIndex (u4Id)
     UINT4               u4Id;
{
    if ((u4Id < MAX_QUEUES) && (buddyQueue[u4Id].active))
    {
        return (u4Id);
    }
    else
    {
        return (INVALID_ID);
    }
}

/*******************************************************************************
* Function : CreateBuddy                                                      *
* Input    : u4Size                                                           *
*            u4NumUnits                                                      *
*            u4Index                                                          *
* Output   : None                                                              *
* Returns  : BLOCK_MALLOC_FAILURE - on calloc failure for block                *
*            BUFFER_MALLOC_FAILURE - on calloc failure for buf                 *
*            BUDDY_OK              - on success                                *
*******************************************************************************/
INT4
CreateBuddy (u4Size, u4NumUnits, u4Index)
     UINT4               u4Size;
     UINT4               u4NumUnits;
     UINT4               u4Index;
{
    UINT4               u4Temp = 0;

    buddyQueue[u4Index].u4Size = u4Size;
    buddyQueue[u4Index].u4Id = u4Index;
    buddyQueue[u4Index].u4NumUnits = u4NumUnits;
    buddyQueue[u4Index].u4AllocCount = 0;
    buddyQueue[u4Index].u4ReleaseCount = 0;
    buddyQueue[u4Index].u4FreeUnitsCount = u4NumUnits;

    if ((buddyQueue[u4Index].block = MEM_CALLOC (u4NumUnits,
                                                 sizeof
                                                 (tBlockInfo),
                                                 tBlockInfo)) == NULL)

    {
        return (BLOCK_MALLOC_FAILURE);
    }
    if (!(u4Index < MAX_QUEUES))
    {
        return BUDDY_ERR;
    }
    InitBlockInfo (u4NumUnits, 1, u4Index);
    u4Temp = u4Size * u4NumUnits;

    if ((buddyQueue[u4Index].pu1BufStart = MEM_CALLOC (1, u4Temp, UINT1))
        == NULL)
    {
        return (BUFFER_MALLOC_FAILURE);
    }
    else
    {
        buddyQueue[u4Index].u4BufLen = u4Temp;
    }
    return (BUDDY_OK);
}

/*******************************************************************************
* Function : InitBlockInfo                                                   *
* Input    : numElements                                                      *
*            multiple                                                          *
*            index                                                             *
* Output   : None                                                              *
* Returns  : void                                                              *
*******************************************************************************/
void
InitBlockInfo (numElements, multiple, u4Index)
     UINT4               numElements;
     UINT4               multiple;
     UINT4               u4Index;
{
    UINT4               i;

    for (i = 0; i < numElements; i++)
    {
        buddyQueue[u4Index].block[i].i4Prev = i - 1;
        buddyQueue[u4Index].block[i].i4Next = i + 1;
        buddyQueue[u4Index].block[i].flag = BUDDY_FREE;
        buddyQueue[u4Index].block[i].u4Multiples = multiple;
    }
    buddyQueue[u4Index].block[0].i4Prev = BUDDY_ERR;
    buddyQueue[u4Index].block[numElements - 1].i4Next = BUDDY_ERR;
}

/*******************************************************************************
* Function : BuddyAllocate                                                    *
* Input    : u4Size                                                           *
*            u4Id                                                             *
*            ppu1Block                                                        *
*                                                                             * 
* Output   : None                                                             *
* Returns  : MEM_SUCCESS       - when static memory allocation succeeds       *
*            MEM_FAILURE       - when static memory allocation fails          * 
*                                (if the memory type is default )             *
*                                when both static and heap memory fails       *
*                                (if the memory type is Heap )                *
*******************************************************************************/
UINT4
BuddyAllocate (UINT4 u4Size, UINT4 u4Id, UINT1 **pu1Block)
{
    INT4                i4Index;
    UINT4               u4Temp = 0;
#ifdef BUDDY_DEBUG
    UINT4               u4CurrentTime = 0;
#endif

    if (u4Size <= 0)
    {
        return MEM_FAILURE;
    }
    i4Index = MapIdToIndex (u4Id);
    if (i4Index < 0)
    {
        return MEM_FAILURE;
    }
#ifdef BUDDY_DEBUG
    if (buddyQueue[i4Index].u4FreeUnitsCount == BUDDY_THRESHOLD_VAL)
    {
        u4CurrentTime = OsixGetSysUpTime ();

        if ((u4CurrentTime - buddyQueue[i4Index].u4TimeStamp)
            >= BUDDY_DEBUG_PRINT_TIME)
        {
            PRINTF ("!!! Buddy Memory for LSA reached Threshold level !!!\n");
            buddyQueue[i4Index].u4TimeStamp = u4CurrentTime;
        }
    }
#endif
    if ((u4Size > (buddyQueue[i4Index].u4Size *
                   buddyQueue[i4Index].u4FreeUnitsCount)))
    {
        if (buddyQueue[i4Index].u4TypeOfMem == MEM_HEAP_MEMORY_TYPE)
        {
            *pu1Block = MEM_CALLOC (sizeof (UINT1), u4Size, UINT1);
            if (*pu1Block)
            {
                return MEM_SUCCESS;
            }
            else
            {
                return MEM_FAILURE;
            }
        }
        else
        {
            return MEM_FAILURE;
        }

    }

    u4Temp = GetMultiple (u4Size, i4Index);
    *pu1Block = GetBlock (u4Temp, i4Index);
    if (*pu1Block == NULL)
    {
        if (buddyQueue[i4Index].u4TypeOfMem == MEM_HEAP_MEMORY_TYPE)
        {
            *pu1Block = MEM_CALLOC (sizeof (UINT1), u4Size, UINT1);
            if (*pu1Block)
            {
                return MEM_SUCCESS;
            }
            else
            {
                return MEM_FAILURE;
            }
        }
        else
        {
            return MEM_FAILURE;
        }
    }

    buddyQueue[i4Index].u4FreeUnitsCount =
        (buddyQueue[i4Index].u4FreeUnitsCount - u4Temp);
    buddyQueue[i4Index].u4AllocCount =
        (buddyQueue[i4Index].u4AllocCount + u4Temp);

    /* For Buddy create draining out  */
    return MEM_SUCCESS;
}

/*******************************************************************************
* Function : BuddyRelease                                                     *
* Input    : pu1Ptr                                                           *
*            u4Id                                                             *
* Output   : None                                                             *
* Returns  : on failure - ILLEGAL_SIZE_RELEASE                                *
*                       - INVALID_PARAMETER                                   *
*                       - ILLEGAL_RELEASE                                     *
*                       - BUDDY_ERR                                           * 
*           on success  - BUDDY_OK                                            *
*******************************************************************************/
INT4
BuddyRelease (pu1Ptr, u4Id)
     UINT1              *pu1Ptr;
     UINT4               u4Id;
{
    INT4                i4Index;
    UINT4               u4Start, u4Rem;
    UINT4               i;
    UINT4               u4Multiple;
    INT4                i4Next, i4Prev;
    if (pu1Ptr == NULL)
        return (INVALID_PARAMETER);
    i4Index = MapIdToIndex (u4Id);
    if (i4Index < 0)
        return (i4Index);
    if ((pu1Ptr < buddyQueue[i4Index].pu1BufStart) ||
        (pu1Ptr >= (buddyQueue[i4Index].pu1BufStart +
                    buddyQueue[i4Index].u4BufLen)))
    {
        if (buddyQueue[i4Index].u4TypeOfMem == MEM_HEAP_MEMORY_TYPE)
        {
            MEM_FREE (pu1Ptr);
            return BUDDY_OK;
        }
        else
        {
            return (ILLEGAL_SIZE_RELEASE);
        }
    }
    else if ((u4Rem =
              ((pu1Ptr - buddyQueue[i4Index].pu1BufStart) %
               buddyQueue[i4Index].u4Size)))
    {
        return (ILLEGAL_RELEASE);
    }
    else
    {
        u4Start = (pu1Ptr - buddyQueue[i4Index].pu1BufStart) /
            buddyQueue[i4Index].u4Size;
        if (buddyQueue[i4Index].block[u4Start].flag != BUDDY_ALLOCATED)
        {
            return (BUDDY_ERR);
        }
        u4Multiple = buddyQueue[i4Index].block[u4Start].u4Multiples;
        i4Next = buddyQueue[i4Index].block[u4Start].i4Next;
        i4Prev = buddyQueue[i4Index].block[u4Start].i4Prev;
        for (i = u4Start; i < u4Start + u4Multiple; i++)
        {
            buddyQueue[i4Index].block[i].i4Next = i + 1;
            buddyQueue[i4Index].block[i].u4Multiples = 1;
            buddyQueue[i4Index].block[i].flag = BUDDY_FREE;
            buddyQueue[i4Index].block[i].i4Prev = i - 1;
        }

        buddyQueue[i4Index].block[u4Start + u4Multiple - 1].i4Next = i4Next;
        buddyQueue[i4Index].block[u4Start].i4Prev = i4Prev;
        if (i4Next != BUDDY_ERR)
        {
            buddyQueue[i4Index].block[i4Next].i4Prev = u4Start + u4Multiple - 1;
            buddyQueue[i4Index].u4FreeUnitsCount =
                (buddyQueue[i4Index].u4FreeUnitsCount + u4Multiple);
            buddyQueue[i4Index].u4ReleaseCount =
                (buddyQueue[i4Index].u4ReleaseCount + u4Multiple);
        }
        return (BUDDY_OK);
    }
}

/*******************************************************************************
* Function : GetMultiple                                                      *
* Input    : u4Size                                                           *
*            u4Index                                                          *
* Output   : None                                                              *
* Returns  : u4Temp - no: of units                                            *
*******************************************************************************/
UINT4
GetMultiple (u4Size, u4Index)
     UINT4               u4Size;
     UINT4               u4Index;
{
    UINT4               u4Temp;

    u4Temp = u4Size / buddyQueue[u4Index].u4Size;
    if ((u4Size % buddyQueue[u4Index].u4Size))
        return (u4Temp + 1);
    else
        return (u4Temp);
}

UINT1              *
GetBlock (u4Multiple, u4Index)
     UINT4               u4Multiple;
     UINT4               u4Index;
{
    INT4                i4Ret;
    if (!(u4Index < MAX_QUEUES))
    {
        return NULL;
    }
    if ((i4Ret = FindFirstFit (u4Multiple, u4Index)) != BUDDY_ERR)
    {

        return (buddyQueue[u4Index].pu1BufStart +
                buddyQueue[u4Index].u4Size * i4Ret);
    }

    return (NULL);
}

/*******************************************************************************
* Function : FindFirstFit                                                    *
* Input    : u4Multiple                                                       *
*            u4Index                                                          *
* Output   : None                                                              *
* Returns  : i4CurStart - on success                                         *
*            BUDDY_ERR    - on failure                                         *
*******************************************************************************/
INT4
FindFirstFit (u4Multiple, u4Index)
     UINT4               u4Multiple;
     UINT4               u4Index;
{
    UINT4               u4CurTotal = 0;
    INT4                i4CurStart = 0;
    INT4                i4Temp = 0;

    if (!(u4Index < MAX_QUEUES))
    {
        return BUDDY_ERR;
    }
    while (i4Temp != BUDDY_ERR)
    {
        if (buddyQueue[u4Index].block[i4Temp].flag == BUDDY_FREE)
        {
            u4CurTotal += buddyQueue[u4Index].block[i4Temp].u4Multiples;
            if (u4CurTotal >= u4Multiple)
            {
                AdjustList (i4CurStart, i4Temp, u4CurTotal, u4Index);
                return (i4CurStart);
            }
            i4Temp = buddyQueue[u4Index].block[i4Temp].i4Next;
        }
        else if (buddyQueue[u4Index].block[i4Temp].flag == BUDDY_ALLOCATED)
        {
            i4Temp = i4CurStart = buddyQueue[u4Index].block[i4Temp].i4Next;
            u4CurTotal = 0;
        }
        else
        {
            return (BUDDY_ERR);
        }
    }
    return (BUDDY_ERR);
}

/*******************************************************************************
* Function : FindExactFit                                                    *
* Input    : u4Multiple                                                       *
*            u4Index                                                          *
* Output   : None                                                              *
* Returns  : i4CurStart - on success                                         *
*            BUDDY_ERR    - on failure                                         *
*******************************************************************************/
INT4
FindExactFit (u4Multiple, u4Index)
     UINT4               u4Multiple;
     UINT4               u4Index;
{
    UINT4               u4CurTotal = 0;
    INT4                i4CurStart = 0;
    INT4                i4Temp = 0;
    INT4                i4Next;

    if (!(u4Index < MAX_QUEUES))
    {
        return BUDDY_ERR;
    }
    while (i4Temp != BUDDY_ERR)
    {
        if (buddyQueue[u4Index].block[i4Temp].flag == BUDDY_FREE)
        {
            u4CurTotal += buddyQueue[u4Index].block[i4Temp].u4Multiples;
            if (u4CurTotal == u4Multiple)
            {
                i4Next = buddyQueue[u4Index].block[i4Temp].i4Next;
                if ((i4Next == BUDDY_ERR) &&
                    ((buddyQueue[u4Index].block[i4CurStart].i4Prev ==
                      BUDDY_ERR)
                     ||
                     (buddyQueue[u4Index].block
                      [buddyQueue[u4Index].block[i4CurStart].
                       i4Prev].flag != BUDDY_FREE)))
                {
                    AdjustList (i4CurStart, i4Temp, u4CurTotal, u4Index);
                    return (i4CurStart);
                }
                else if ((buddyQueue[u4Index].block[i4Next].flag !=
                          BUDDY_FREE) &&
                         ((buddyQueue[u4Index].block[i4CurStart].i4Prev
                           == BUDDY_ERR) ||
                          (buddyQueue[u4Index].block
                           [buddyQueue[u4Index].block[i4CurStart].
                            i4Prev].flag != BUDDY_FREE)))
                {
                    AdjustList (i4CurStart, i4Temp, u4CurTotal, u4Index);
                    return (i4CurStart);

                }
                else
                {
                    i4Temp = i4CurStart =
                        buddyQueue[u4Index].block[i4Temp].i4Next;
                    u4CurTotal = 0;
                }
            }
            else
            {
                i4Temp = buddyQueue[u4Index].block[i4Temp].i4Next;
            }
        }
        else if (buddyQueue[u4Index].block[i4Temp].flag == BUDDY_ALLOCATED)
        {
            i4Temp = i4CurStart = buddyQueue[u4Index].block[i4Temp].i4Next;
            u4CurTotal = 0;
        }
        else
        {
            return (BUDDY_ERR);
        }
    }
    return (BUDDY_ERR);
}

/*******************************************************************************
* Function : AdjustList                                                       *
* Input    : i4Start                                                          *
*            i4End                                                            *
*            u4Total                                                          *
*            u4Index                                                          *
* Output   : None                                                              *
* Returns  : BUDDY_OK - on success                                             *
*            BUDDY_ERR    - on failure                                         *
*******************************************************************************/
INT4
AdjustList (i4Start, i4End, u4Total, u4Index)
     INT4                i4Start;
     INT4                i4End;
     UINT4               u4Total;
     UINT4               u4Index;
{
    UINT4               u4_check_total =
        buddyQueue[u4Index].block[i4Start].u4Multiples;
    INT4                i4Temp = buddyQueue[u4Index].block[i4Start].i4Next;
    INT4                i4EndNext = buddyQueue[u4Index].block[i4End].i4Next;
    UINT4               i4Store;

    while ((i4Temp <= i4End) && (i4Temp != BUDDY_ERR))
    {
        i4Store = buddyQueue[u4Index].block[i4Temp].i4Next;
        buddyQueue[u4Index].block[i4Temp].i4Prev =
            buddyQueue[u4Index].block[i4Temp].i4Next = BUDDY_ERR;
        buddyQueue[u4Index].block[i4Temp].flag = BUDDY_INVALID;
        u4_check_total += buddyQueue[u4Index].block[i4Temp].u4Multiples;
        buddyQueue[u4Index].block[i4Temp].u4Multiples = 0;
        i4Temp = i4Store;
    }
    if (u4_check_total != u4Total)
    {
        return (BUDDY_ERR);
    }
    else
    {
        buddyQueue[u4Index].block[i4Start].i4Next = i4EndNext;
        buddyQueue[u4Index].block[i4EndNext].i4Prev = i4Start;
        buddyQueue[u4Index].block[i4Start].flag = BUDDY_ALLOCATED;
        buddyQueue[u4Index].block[i4Start].u4Multiples = u4_check_total;
        return (BUDDY_OK);
    }
}

/*******************************************************************************
* Function : BuddyDump                                                        *
* Input    : u4Id                                                             *
* Output   : None                                                              *
* Returns  : BUDDY_OK - on success                                             *
*            BUDDY_ERR    - on failure                                         *
*******************************************************************************/
INT4
BuddyDump (u4Id)
     UINT4               u4Id;
{
    INT4                i4Index;
    INT4                i4Temp = 0;
    i4Index = MapIdToIndex (u4Id);
    if (i4Index < 0)
        return (BUDDY_ERR);
    while (i4Temp != BUDDY_ERR)
    {
        if (buddyQueue[i4Index].block[i4Temp].flag == BUDDY_ALLOCATED)
        {
        }
        i4Temp = buddyQueue[i4Index].block[i4Temp].i4Next;
    }
    return (BUDDY_OK);
}

/******************************************************************************
*                     End of file buddy.c                                     *
******************************************************************************/
