##############################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: Makefile.diab,v 1.7 2012/02/01 12:40:51 siva Exp $
#
# Description: Makefile for FutureOSPF.
##############################################################

#include $(PSS_CONFIG)/config.mk
.PHONY : clean

include ../LR/make.h
include ../LR/make.rule
include make.h

C_FLAGS = ${CC_FLAGS} ${GLOBAL_OPNS} ${GLOBAL_INCLUDES}
LDR    = ${LINKER} ${LINK_FLAGS}

ifeq (${OPQAPSIM}, YES)
SYSTEM_COMPILATION_SWITCHES  += -DOPQAPSIM_WANTED
endif

#Header file list
HDR_FILES = $(OSPF_INCD)/ospfcfg.h \
            $(OSPF_INCD)/osbufif.h \
            $(OSPF_INCD)/oscfgvar.h \
            $(OSPF_INCD)/osdefn.h\
            $(OSPF_INCD)/osglob.h \
            $(OSPF_INCD)/osinc.h \
            $(OSPF_INCD)/ostask.h \
            $(OSPF_INCD)/osipprot.h \
            $(OSPF_INCD)/osmacs.h \
            $(OSPF_INCD)/osmain.h \
            $(OSPF_INCD)/ososif.h \
            $(OSPF_INCD)/osprot.h \
            $(OSPF_INCD)/osfssnmp.h \
            $(OSPF_INCD)/ostdfs.h \
            $(OSPF_INCD)/ostmrif.h \
            $(OSPF_INCD)/ossock.h \
            $(OSPF_INCD)/osfsip.h \
            $(OSPF_INCD)/osappif.h \
            $(OSPF_INCD)/ososix.h \
            $(OSPF_INCD)/osport.h \
            $(OSPF_INCD)/osdebug.h 

#Object Module List
OSPF_OBJS  = $(OSPF_OBJD)/osism.o \
             $(OSPF_OBJD)/osnsm.o \
             $(OSPF_OBJD)/osnseln.o \
             $(OSPF_OBJD)/osnstrln.o \
             $(OSPF_OBJD)/osragadd.o \
             $(OSPF_OBJD)/osragdel.o \
             $(OSPF_OBJD)/osragext.o \
             $(OSPF_OBJD)/osragutl.o \
             $(OSPF_OBJD)/oslrq.o \
             $(OSPF_OBJD)/osddp.o \
             $(OSPF_OBJD)/osols.o \
             $(OSPF_OBJD)/oslsu.o \
             $(OSPF_OBJD)/oslak.o \
             $(OSPF_OBJD)/osagd.o \
             $(OSPF_OBJD)/osrtl.o \
             $(OSPF_OBJD)/osppp.o \
             $(OSPF_OBJD)/osrtc.o \
             $(OSPF_OBJD)/osrtr.o \
             $(OSPF_OBJD)/osarea.o \
             $(OSPF_OBJD)/osif.o \
             $(OSPF_OBJD)/osnbr.o \
             $(OSPF_OBJD)/osvif.o \
             $(OSPF_OBJD)/oshost.o \
             $(OSPF_OBJD)/osextrt.o \
             $(OSPF_OBJD)/ostask.o \
             $(OSPF_OBJD)/osfssnmp.o \
             $(OSPF_OBJD)/ostmrif.o \
             $(OSPF_OBJD)/osutil.o \
             $(OSPF_OBJD)/osmain.o \
             $(OSPF_OBJD)/osdbg.o \
             $(OSPF_OBJD)/osfetch.o \
             $(OSPF_OBJD)/osconf.o \
             $(OSPF_OBJD)/ostest.o \
             $(OSPF_OBJD)/osfsip.o \
             $(OSPF_OBJD)/oshp.o \
             $(OSPF_OBJD)/osrtm.o \
             $(OSPF_OBJD)/osappif.o \
             $(OSPF_OBJD)/osopqget.o \
             $(OSPF_OBJD)/osopqset.o \
             $(OSPF_OBJD)/osopqtst.o \
             $(OSPF_OBJD)/osopqlsa.o

ifeq (${OPQAPSIM}, YES)
OSPF_OBJS += $(APP_SIM_OBJ)/opqapsim.o\
             $(APP_SIM_OBJ)/fsosolow.o
endif

ifeq (${SNMP_2}, YES)
OSPF_OBJS += $(OSPF_OBJD)/stdospwr.o\
             $(OSPF_OBJD)/fsospfwr.o\
             $(OSPF_OBJD)/fsostewr.o\
             $(OSPF_OBJD)/stdostwr.o
    ifeq (${OPQAPSIM}, YES)
        OSPF_OBJS += $(APP_SIM_OBJ)/fsosoawr.o
    endif
endif

ifeq (${CLI}, YES)
OSPF_OBJS += $(OSPF_OBJD)/ospfcli.o
OSPF_OBJS += $(OSPF_OBJD)/osfscli.o
endif


#Final Object
OSPF_FINAL_OBJ = ${OSPF_OBJD}/FutureOSPF.o

##################################################
#### Compressed Object Module Dependency Rules ####
###################################################

${OSPF_FINAL_OBJ} : CREATE_OBJ_DIR ${OSPF_OBJS}  
	${LDR} ${OSPF_FINAL_OBJ} ${OSPF_OBJS} 

CREATE_OBJ_DIR:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) ${OSPF_OBJD}
    ifeq (${OPQAPSIM}, YES)
	    $(MKDIR) $(MKDIR_FLAGS) ${APP_SIM_OBJ}
    endif
endif
ifeq (${OPQAPSIM}, YES)
	${MAKE} -C ${OSPF_BASE_DIR}/opqapsim -f Makefile
endif


#############################################
#### OSPF Object Module Dependency Rules ####
#############################################

$(OSPF_OBJD)/osagd.o : $(OSPF_SRCD)/osagd.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osagd.c -o $@

$(OSPF_OBJD)/osarea.o : $(OSPF_SRCD)/osarea.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osarea.c -o $@

$(OSPF_OBJD)/osddp.o : $(OSPF_SRCD)/osddp.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osddp.c -o $@

$(OSPF_OBJD)/osdbg.o : $(OSPF_SRCD)/osdbg.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osdbg.c -o $@

$(OSPF_OBJD)/osextrt.o : $(OSPF_SRCD)/osextrt.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osextrt.c -o $@

$(OSPF_OBJD)/oshost.o : $(OSPF_SRCD)/oshost.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/oshost.c -o $@
    
$(OSPF_OBJD)/oshp.o : $(OSPF_SRCD)/oshp.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/oshp.c -o $@

$(OSPF_OBJD)/osif.o : $(OSPF_SRCD)/osif.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osif.c -o $@

$(OSPF_OBJD)/osism.o : $(OSPF_SRCD)/osism.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_INCD)/osism.h
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osism.c -o $@

$(OSPF_OBJD)/oslak.o : $(OSPF_SRCD)/oslak.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/oslak.c -o $@

$(OSPF_OBJD)/oslrq.o : $(OSPF_SRCD)/oslrq.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/oslrq.c -o $@

$(OSPF_OBJD)/oslsu.o : $(OSPF_SRCD)/oslsu.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/oslsu.c -o $@
    
$(OSPF_OBJD)/osnbr.o : $(OSPF_SRCD)/osnbr.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osnbr.c -o $@

$(OSPF_OBJD)/osnsm.o : $(OSPF_SRCD)/osnsm.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_INCD)/osnsm.h
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osnsm.c -o $@

$(OSPF_OBJD)/osnseln.o : $(OSPF_SRCD)/osnseln.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} 
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osnseln.c -o $@

$(OSPF_OBJD)/osnstrln.o : $(OSPF_SRCD)/osnstrln.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} 
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osnstrln.c -o $@


$(OSPF_OBJD)/osols.o : $(OSPF_SRCD)/osols.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osols.c -o $@

$(OSPF_OBJD)/osppp.o : $(OSPF_SRCD)/osppp.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osppp.c -o $@

$(OSPF_OBJD)/osragadd.o : $(OSPF_SRCD)/osragadd.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osragadd.c -o $@

$(OSPF_OBJD)/osragdel.o : $(OSPF_SRCD)/osragdel.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osragdel.c -o $@

$(OSPF_OBJD)/osragext.o : $(OSPF_SRCD)/osragext.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osragext.c -o $@

$(OSPF_OBJD)/osragutl.o : $(OSPF_SRCD)/osragutl.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osragutl.c -o $@

$(OSPF_OBJD)/osrtc.o : $(OSPF_SRCD)/osrtc.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osrtc.c -o $@

$(OSPF_OBJD)/osrtl.o : $(OSPF_SRCD)/osrtl.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osrtl.c -o $@

$(OSPF_OBJD)/osrtr.o : $(OSPF_SRCD)/osrtr.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osrtr.c -o $@

$(OSPF_OBJD)/osfssnmp.o : $(OSPF_SRCD)/osfssnmp.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_INCD)/ospf-mib.h
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osfssnmp.c -o $@

$(OSPF_OBJD)/ostmrif.o : $(OSPF_SRCD)/ostmrif.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/ostmrif.c -o $@

$(OSPF_OBJD)/osutil.o : $(OSPF_SRCD)/osutil.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osutil.c -o $@

$(OSPF_OBJD)/osfetch.o : $(OSPF_SRCD)/osfetch.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osfetch.c -o $@

$(OSPF_OBJD)/osconf.o : $(OSPF_SRCD)/osconf.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osconf.c -o $@

$(OSPF_OBJD)/ostest.o : $(OSPF_SRCD)/ostest.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/ostest.c -o $@

$(OSPF_OBJD)/osvif.o : $(OSPF_SRCD)/osvif.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osvif.c -o $@

$(OSPF_OBJD)/ostask.o : $(OSPF_SRCD)/ostask.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/ostask.c -o $@
    
$(OSPF_OBJD)/osmain.o : $(OSPF_SRCD)/osmain.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_INCD)/osmain.h
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osmain.c -o $@

$(OSPF_OBJD)/osfsip.o : $(OSPF_SRCD)/osfsip.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_INCD)/osfsip.h 
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osfsip.c -o $@

$(OSPF_OBJD)/osrtm.o : $(OSPF_SRCD)/osrtm.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osrtm.c -o $@

$(OSPF_OBJD)/osappif.o : $(OSPF_SRCD)/osappif.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osappif.c -o $@

$(OSPF_OBJD)/osopqget.o : $(OSPF_SRCD)/osopqget.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osopqget.c -o $@

$(OSPF_OBJD)/osopqset.o : $(OSPF_SRCD)/osopqset.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osopqset.c -o $@

$(OSPF_OBJD)/osopqtst.o : $(OSPF_SRCD)/osopqtst.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osopqtst.c -o $@

$(OSPF_OBJD)/osopqlsa.o : $(OSPF_SRCD)/osopqlsa.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/osopqlsa.c -o $@

$(OSPF_OBJD)/stdospwr.o : $(OSPF_SRCD)/stdospwr.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/stdospwr.c -o $@

$(OSPF_OBJD)/fsospfwr.o : $(OSPF_SRCD)/fsospfwr.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/fsospfwr.c -o $@

$(OSPF_OBJD)/fsostewr.o : $(OSPF_SRCD)/fsostewr.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/fsostewr.c -o $@

$(OSPF_OBJD)/stdostwr.o : $(OSPF_SRCD)/stdostwr.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF}
	$(CC) $(C_FLAGS) $(OSPF_SRCD)/stdostwr.c -o $@

$(OSPF_OBJD)/ospfcli.o : $(OSPF_SRCD)/ospfcli.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_CLI_INCLUDE_FILES) 
	$(CC) $(C_FLAGS) -I$(CLI_INCL_DIR) $(OSPF_SRCD)/ospfcli.c -o $@

$(OSPF_OBJD)/osfscli.o : $(OSPF_SRCD)/osfscli.c \
        $(HDR_FILES) \
        ${DEPENDENCIES_FOR_OSPF} \
        $(OSPF_CLI_INCLUDE_FILES) 
	$(CC) $(C_FLAGS) -I$(CLI_INCL_DIR) $(OSPF_SRCD)/osfscli.c -o $@

clean :
	${RM} ${OSPF_OBJS} ${OSPF_FINAL_OBJ} 
