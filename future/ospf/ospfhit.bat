# $Id: ospfhit.bat,v 1.3 2013/06/15 13:38:53 siva Exp $

@ECHO OFF
ECHO.
ECHO ************************************************
ECHO                   Building OSPF
ECHO ************************************************
ECHO.

set demo=%1
set endian=b
set general_option=LR_INTEGRATION,SVID_SOURCE,TRACE_WANTED,INCLUDE_IN_OSS,GNU_CC,PACK_REQUIRED
set local_option=A
set system_include=..\inc,..\inc\vlan,..\inc\snmp,D:\user\Epilogue\Nucleus\NuPlus,..\fsap2\cmn,..\fsap2\nucleus
set ospf_include=inc\,buddy\inc,opqapsim\inc\,..\util\trie,..\util\md5\inc, ..\util\hmac\h
set ospf_option=DEBUG_WANTED
set ospf_src=src
set ospf_opq_src=opqapsim\src
set ospf_buddy_src=buddy\src
set system_option=__STDC__,RRD_WANTED,FUTURE_SNMP_WANTED,CFA_WANTED,FFM_WANTED,IP_WANTED,SLI_WANTED,SNMP_WANTED,TCP_WANTED

if "%1"=="" set demo=demo
if "%1"=="f" set demo=flash
if "%1"=="F" set demo=flash
if "%1"==""  set output=-ou=o\plusdemo.abs
if "%1"=="f" set output=-ou=o\flash.mot -fo=stype
if "%1"=="F" set output=-ou=o\flash.mot -fo=stype

IF NOT EXIST "o\*.*" MKDIR "o"
CD ".\"

ECHO --osism--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osism.o %ospf_src%\osism.c

ECHO --osnsm--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osnsm.o %ospf_src%\osnsm.c

ECHO --oslrq--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\oslrq.o %ospf_src%\oslrq.c

ECHO --osddp--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osddp.o %ospf_src%\osddp.c

ECHO --osols--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osols.o %ospf_src%\osols.c

ECHO --oslsu--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\oslsu.o %ospf_src%\oslsu.c

ECHO --oslak--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\oslak.o %ospf_src%\oslak.c

ECHO --osagd--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osagd.o %ospf_src%\osagd.c

ECHO --osrtl--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osrtl.o %ospf_src%\osrtl.c

ECHO --osppp--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osppp.o %ospf_src%\osppp.c

ECHO --osrtc--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osrtc.o %ospf_src%\osrtc.c

ECHO --osrtr--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osrtr.o %ospf_src%\osrtr.c

ECHO --osarea--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osarea.o %ospf_src%\osarea.c

ECHO --osif--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osif.o %ospf_src%\osif.c

ECHO --osnbr--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osnbr.o %ospf_src%\osnbr.c

ECHO --osvif--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osvif.o %ospf_src%\osvif.c

ECHO --oshost--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\oshost.o %ospf_src%\oshost.c

ECHO --osextrt--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osextrt.o %ospf_src%\osextrt.c

ECHO --ostask--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\ostask.o %ospf_src%\ostask.c

ECHO --osfssnmp--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osfssnmp.o %ospf_src%\osfssnmp.c

ECHO --ostmrif--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\ostmrif.o %ospf_src%\ostmrif.c

ECHO --osutil--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osutil.o %ospf_src%\osutil.c

ECHO --osmain--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osmain.o %ospf_src%\osmain.c

ECHO --osdbg--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osdbg.o %ospf_src%\osdbg.c

ECHO --osfetch--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osfetch.o %ospf_src%\osfetch.c

ECHO --osconf--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osconf.o %ospf_src%\osconf.c

ECHO --ostest--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\ostest.o %ospf_src%\ostest.c

ECHO --osfsip--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osfsip.o %ospf_src%\osfsip.c

ECHO --oshp--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\oshp.o %ospf_src%\oshp.c

ECHO --osrtm--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osrtm.o %ospf_src%\osrtm.c

ECHO --osappif--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osappif.o %ospf_src%\osappif.c

ECHO --osopqget--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osopqget.o %ospf_src%\osopqget.c

ECHO --osopqset--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osopqset.o %ospf_src%\osopqset.c

ECHO --osopqtst--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osopqtst.o %ospf_src%\osopqtst.c

ECHO --osopqlsa--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\osopqlsa.o %ospf_src%\osopqlsa.c

ECHO --opqapsim--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\opqapsim.o %ospf_opq_src%\opqapsim.c

ECHO --oasmilow--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\oasmilow.o %ospf_opq_src%\oasmilow.c

ECHO --buddy--
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%general_option%,%system_option%,%local_option%,%ospf_option% -I=%system_include%,%ospf_include% -ob=o\buddy.o %ospf_buddy_src%\buddy.c

IF NOT EXIST "..\..\LIB\*.*" MKDIR "..\..\LIB"
IF EXIST "..\..\LIB\ospf.lib" DEL "..\..\LIB\ospf.lib"
ECHO Creating Library ospf.lib...
OPTLNK .\o\*.o -OU=..\..\lib\ospf.lib -FO=library

if "%1"=="all" goto all_bat
goto end_bat

:all_bat
cd ..\..\fsap2\nucleus\
fsap.bat all

:end_bat
