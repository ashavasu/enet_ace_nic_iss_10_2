##############################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.16 2012/02/01 12:40:51 siva Exp $
#
# Description: Specifies the options and modules to be
#              including for building the FutureOSPF product.
#
##############################################################

GLOBAL_OPNS = ${GENERAL_COMPILATION_SWITCHES}\
              ${SYSTEM_COMPILATION_SWITCHES}

############################################################################
#                         Directories                                      #
############################################################################

#Fill the base directory
OSPF_BASE_DIR       = ${BASE_DIR}/ospf
OSPF_SRCD = ${OSPF_BASE_DIR}/src
OSPF_INCD = ${OSPF_BASE_DIR}/inc
OSPF_OBJD = ${OSPF_BASE_DIR}/obj

DEPENDENCIES_FOR_OSPF = ${OSPF_BASE_DIR}/make.h \
                        ${OSPF_BASE_DIR}/Makefile \
                        ${COMMON_DEPENDENCIES}

ifeq (${CLI}, YES)
OSPF_CLI_INCLUDE_FILES =    \
    $(CLI_INCL_DIR)/ospfcli.h
endif

IP_BASE_DIR = ${BASE_DIR}/ip
RTM_BASE_DIR = ${IP_BASE_DIR}/rtm
IP_RTMINCD = ${RTM_BASE_DIR}/inc

APP_SIM_SRC = ${OSPF_BASE_DIR}/opqapsim/src
APP_SIM_INC = ${OSPF_BASE_DIR}/opqapsim/inc
APP_SIM_OBJ = ${OSPF_BASE_DIR}/opqapsim/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

OSPF_GLOBAL_INCLUDES  = -I${OSPF_INCD} -I${IP_RTMINCD} \
                        -I${APP_SIM_INC}

GLOBAL_INCLUDES  = ${OSPF_GLOBAL_INCLUDES} \
                   ${COMMON_INCLUDE_DIRS}

#############################################################################


##########################################################################
# Each line below indicates whether that module is to be included in the #
# OSPF build. A YES indicates inclusion.                                 #
##########################################################################
OPQAPSIM                     = NO
HIGH_PERF_RXMT_LST           = NO
RAWSOCK                      = NO
