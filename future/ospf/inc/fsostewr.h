
#ifndef _FSOSTEWRAP_H 
#define _FSOSTEWRAP_H 
VOID RegisterFSOSTE(VOID);
VOID UnRegisterFSOSTE(VOID);
INT4 FutOspfBRRouteIpAddrGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteIpAddrMaskGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteIpTosGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteIpNextHopGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteDestTypeGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteTypeGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteAreaIdGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteCostGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBRRouteInterfaceIndexGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteDestGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMaskGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteTOSGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMetricTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMetricSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMetricGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMetricTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMetricTypeSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteMetricTypeGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteTagTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteTagSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteTagGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteFwdAdrTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteFwdAdrSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteFwdAdrGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteIfIndexTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteIfIndexSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteNextHopTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteNextHopSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteNextHopGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteStatusSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfExtRouteStatusGet (tSnmpIndex *, tRetVal *);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexFutOspfExtRouteTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFutOspfBRRouteTable( tSnmpIndex *, tSnmpIndex *);

INT4 FutOspfExtRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FutOspfGrShutdownGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfGrShutdownSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfGrShutdownTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfGrShutdownDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

#endif
