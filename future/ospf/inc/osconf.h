/********************************************************************
 *Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: osconf.h,v 1.2 2015/01/21 10:33:26 siva Exp $
 *
 *Description:This file contains definitions for few of MIB
 *            database object.
 *            
 ********************************************************************/


#ifndef SNMP_2_WANTED

UINT4 FsMIOspfAreaAggregateExternalTag [ ] ={1,3,6,1,4,1,2076,145,10,1,5};
UINT4 FsMIOspfABRType [ ] ={1,3,6,1,4,1,2076,145,1,3,1,14};
UINT4 FsMIOspfAreaDfInfOriginate [ ] ={1,3,6,1,4,1,2076,145,2,1,9};
UINT4 FsMIOspfAreaNSSATranslatorRole [ ] ={1,3,6,1,4,1,2076,145,2,1,5};
UINT4 FsMIOspfAreaNSSATranslatorStabilityInterval [ ] ={1,3,6,1,4,1,2076,145,2,1,7};
UINT4 FsMIOspfAsExternalAggregationStatus [ ] ={1,3,6,1,4,1,2076,145,11,1,6};
UINT4 FsMIOspfAsExternalAggregationTranslation [ ] ={1,3,6,1,4,1,2076,145,11,1,5};
UINT4 FsMIOspfAsExternalAggregationEffect [ ] ={1,3,6,1,4,1,2076,145,11,1,4};
UINT4 FsMIOspfBfdStatus [ ] ={1,3,6,1,4,1,2076,145,1,3,1,33};
UINT4 FsMIOspfBfdAllIfState [ ] ={1,3,6,1,4,1,2076,145,1,3,1,34};
UINT4 FsMIOspfDefaultPassiveInterface [ ] ={1,3,6,1,4,1,2076,145,1,3,1,16};
UINT4 FsMIOspfExtRouteMetric [ ] ={1,3,6,1,4,1,2076,147,2,1,4};
UINT4 FsMIOspfExtRouteMetricType [ ] ={1,3,6,1,4,1,2076,147,2,1,5};
UINT4 FsMIOspfExtRouteTag [ ] ={1,3,6,1,4,1,2076,147,2,1,6};
UINT4 FsMIOspfExtRouteFwdAdr [ ] ={1,3,6,1,4,1,2076,147,2,1,7};
UINT4 FsMIOspfExtRouteIfIndex [ ] ={1,3,6,1,4,1,2076,147,2,1,8};
UINT4 FsMIOspfExtRouteNextHop [ ] ={1,3,6,1,4,1,2076,147,2,1,9};
UINT4 FsMIOspfExtRouteStatus [ ] ={1,3,6,1,4,1,2076,147,2,1,10};
UINT4 FsMIOspfExtTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,3,1,26};
UINT4 FsMIOspfGlobalTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,1};
UINT4 FsMIOspfGraceLsaRetransmitCount [ ] ={1,3,6,1,4,1,2076,145,1,3,1,29};
UINT4 FsMIOspfHelperGraceTimeLimit [ ] ={1,3,6,1,4,1,2076,145,1,3,1,27};
UINT4 FsMIOspfHelperSupport [ ] ={1,3,6,1,4,1,2076,145,1,3,1,25};
UINT4 FsMIOspfHostRouteIfIndex [ ] ={1,3,6,1,4,1,2076,145,3,1,3};
UINT4 FsMIOspfIfBfdState [ ] ={1,3,6,1,4,1,2076,145,4,1,22};
UINT4 FsMIOspfIfMD5AuthKey [ ] ={1,3,6,1,4,1,2076,145,5,1,4};
UINT4 FsMIOspfIfMD5AuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,145,5,1,5};
UINT4 FsMIOspfIfMD5AuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,145,5,1,6};
UINT4 FsMIOspfIfMD5AuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,145,5,1,7};
UINT4 FsMIOspfIfMD5AuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,145,5,1,8};
UINT4 FsMIOspfIfMD5AuthKeyStatus [ ] ={1,3,6,1,4,1,2076,145,5,1,9};
UINT4 FsMIOspfIfPassive [ ] ={1,3,6,1,4,1,2076,145,4,1,4};
UINT4 FsMIOspfMinLsaInterval [ ] ={1,3,6,1,4,1,2076,145,1,3,1,13};
UINT4 FsMIOspfNssaAsbrDefRtTrans [ ] ={1,3,6,1,4,1,2076,145,1,3,1,15};
UINT4 FsMIOspfOpaqueOption [ ] ={1,3,6,1,4,1,2076,145,12,1,1,1,1};
UINT4 FsMIOspfRFC1583Compatibility [ ] ={1,3,6,1,4,1,2076,145,1,3,1,5};
UINT4 FsMIOspfRRDRouteMapEnable [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,4};
UINT4 FsMIOspfRRDRouteDest [ ] ={1,3,6,1,4,1,2076,145,13,2,1,1};
UINT4 FsMIOspfRRDRouteMask [ ] ={1,3,6,1,4,1,2076,145,13,2,1,2};
UINT4 FsMIOspfRRDRouteMetric [ ] ={1,3,6,1,4,1,2076,145,13,2,1,3};
UINT4 FsMIOspfRRDRouteMetricType [ ] ={1,3,6,1,4,1,2076,145,13,2,1,4};
UINT4 FsMIOspfRRDRouteTagType [ ] ={1,3,6,1,4,1,2076,145,13,2,1,5};
UINT4 FsMIOspfRRDRouteTag [ ] ={1,3,6,1,4,1,2076,145,13,2,1,6};
UINT4 FsMIOspfRRDRouteStatus [ ] ={1,3,6,1,4,1,2076,145,13,2,1,7};
UINT4 FsMIOspfRRDProtocolId [ ] ={1,3,6,1,4,1,2076,145,13,3,1,1};
UINT4 FsMIOspfRRDMetricValue [ ] ={1,3,6,1,4,1,2076,145,13,3,1,2};
UINT4 FsMIOspfRRDMetricType [ ] ={1,3,6,1,4,1,2076,145,13,3,1,3};
UINT4 FsMIOspfRRDStatus [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,1};
UINT4 FsMIOspfRRDSrcProtoMaskEnable [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,2};
UINT4 FsMIOspfRRDSrcProtoMaskDisable [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,3};
UINT4 FsMIOspfRTStaggeringInterval [ ] ={1,3,6,1,4,1,2076,145,1,3,1,31};
UINT4 FsMIOspfRTStaggeringStatus [ ] ={1,3,6,1,4,1,2076,145,1,4};
UINT4 FsMIOspfRestartAckState [ ] ={1,3,6,1,4,1,2076,145,1,3,1,28};
UINT4 FsMIOspfRestartSupport [ ] ={1,3,6,1,4,1,2076,145,1,3,1,19};
UINT4 FsMIOspfRestartInterval [ ] ={1,3,6,1,4,1,2076,145,1,3,1,20};
UINT4 FsMIOspfRestartStrictLsaChecking [ ] ={1,3,6,1,4,1,2076,145,1,3,1,21};
UINT4 FsMIOspfRestartReason [ ] ={1,3,6,1,4,1,2076,145,1,3,1,30};
UINT4 FsMIOspfRouterIdPermanence [ ] ={1,3,6,1,4,1,2076,145,1,3,1,32};
UINT4 FsMIOspfSecIfStatus [ ] ={1,3,6,1,4,1,2076,145,9,1,5};
UINT4 FsMIOspfSpfHoldtime [ ] ={1,3,6,1,4,1,2076,145,1,3,1,17};
UINT4 FsMIOspfSpfDelay [ ] ={1,3,6,1,4,1,2076,145,1,3,1,18};
UINT4 FsMIOspfTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,3,1,12};
UINT4 FsMIOspfVirtIfMD5AuthKey [ ] ={1,3,6,1,4,1,2076,145,6,1,4};
UINT4 FsMIOspfVirtIfMD5AuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,145,6,1,5};
UINT4 FsMIOspfVirtIfMD5AuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,145,6,1,6};
UINT4 FsMIOspfVirtIfMD5AuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,145,6,1,7};
UINT4 FsMIOspfVirtIfMD5AuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,145,6,1,8};
UINT4 FsMIOspfVirtIfMD5AuthKeyStatus [ ] ={1,3,6,1,4,1,2076,145,6,1,9};
UINT4 FsMIOspfVrfSpfInterval [ ] ={1,3,6,1,4,1,2076,145,1,2};
UINT4 FsMIStdOspfASBdrRtrStatus [ ] ={1,3,6,1,4,1,2076,146,1,1,1,6};
UINT4 FsMIStdOspfAdminStat [ ] ={1,3,6,1,4,1,2076,146,1,1,1,3};
UINT4 FsMIStdOspfAreaAggregateStatus [ ] ={1,3,6,1,4,1,2076,146,13,1,5};
UINT4 FsMIStdOspfAreaAggregateEffect [ ] ={1,3,6,1,4,1,2076,146,13,1,6};
UINT4 FsMIStdOspfAreaSummary [ ] ={1,3,6,1,4,1,2076,146,2,1,9};
UINT4 FsMIStdOspfAreaStatus [ ] ={1,3,6,1,4,1,2076,146,2,1,10};
UINT4 FsMIStdOspfDemandExtensions [ ] ={1,3,6,1,4,1,2076,146,1,1,1,15};
UINT4 FsMIStdOspfExitOverflowInterval [ ] ={1,3,6,1,4,1,2076,146,1,1,1,14};
UINT4 FsMIStdOspfExtLsdbLimit [ ] ={1,3,6,1,4,1,2076,146,1,1,1,12};
UINT4 FsMIStdOspfHostMetric [ ] ={1,3,6,1,4,1,2076,146,5,1,3};
UINT4 FsMIStdOspfHostStatus [ ] ={1,3,6,1,4,1,2076,146,5,1,4};
UINT4 FsMIStdOspfIfAreaId [ ] ={1,3,6,1,4,1,2076,146,6,1,3};
UINT4 FsMIStdOspfIfType [ ] ={1,3,6,1,4,1,2076,146,6,1,4};
UINT4 FsMIStdOspfIfAdminStat [ ] ={1,3,6,1,4,1,2076,146,6,1,5};
UINT4 FsMIStdOspfIfRtrPriority [ ] ={1,3,6,1,4,1,2076,146,6,1,6};
UINT4 FsMIStdOspfIfTransitDelay [ ] ={1,3,6,1,4,1,2076,146,6,1,7};
UINT4 FsMIStdOspfIfRetransInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,8};
UINT4 FsMIStdOspfIfHelloInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,9};
UINT4 FsMIStdOspfIfRtrDeadInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,10};
UINT4 FsMIStdOspfIfPollInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,11};
UINT4 FsMIStdOspfIfAuthKey [ ] ={1,3,6,1,4,1,2076,146,6,1,16};
UINT4 FsMIStdOspfIfAuthType [ ] ={1,3,6,1,4,1,2076,146,6,1,20};
UINT4 FsMIStdOspfIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,146,6,1,21};
UINT4 FsMIStdOspfIfDemand [ ] ={1,3,6,1,4,1,2076,146,6,1,19};
UINT4 FsMIStdOspfIfMetricStatus [ ] ={1,3,6,1,4,1,2076,146,7,1,5};
UINT4 FsMIStdOspfIfMetricValue [ ] ={1,3,6,1,4,1,2076,146,7,1,4};
UINT4 FsMIStdOspfIfStatus [ ] ={1,3,6,1,4,1,2076,146,6,1,17};
UINT4 FsMIStdOspfImportAsExtern [ ] ={1,3,6,1,4,1,2076,146,2,1,3};
UINT4 FsMIStdOspfNbmaNbrStatus [ ] ={1,3,6,1,4,1,2076,146,9,1,9};
UINT4 FsMIStdOspfNbrPriority [ ] ={1,3,6,1,4,1,2076,146,9,1,5};
UINT4 FsMIStdOspfRouterId [ ] ={1,3,6,1,4,1,2076,146,1,1,1,2};
UINT4 FsMIStdOspfSetTrap [ ] ={1,3,6,1,4,1,2076,148,1,1,1,1};
UINT4 FsMIStdOspfStatus [ ] ={1,3,6,1,4,1,2076,146,1,1,1,16};
UINT4 FsMIStdOspfStubMetric [ ] ={1,3,6,1,4,1,2076,146,3,1,3};
UINT4 FsMIStdOspfStubStatus [ ] ={1,3,6,1,4,1,2076,146,3,1,4};
UINT4 FsMIStdOspfStubMetricType [ ] ={1,3,6,1,4,1,2076,146,3,1,5};
UINT4 FsMIStdOspfVirtIfAuthKey [ ] ={1,3,6,1,4,1,2076,146,8,1,9};
UINT4 FsMIStdOspfVirtIfStatus [ ] ={1,3,6,1,4,1,2076,146,8,1,10};
UINT4 FsMIStdOspfVirtIfAuthType [ ] ={1,3,6,1,4,1,2076,146,8,1,11};
UINT4 FsMIStdOspfVirtIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,146,8,1,12};
UINT4 FsMIStdOspfVirtIfTransitDelay [ ] ={1,3,6,1,4,1,2076,146,8,1,3};
UINT4 FsMIStdOspfVirtIfRetransInterval [ ] ={1,3,6,1,4,1,2076,146,8,1,4};
UINT4 FsMIStdOspfVirtIfHelloInterval [ ] ={1,3,6,1,4,1,2076,146,8,1,5};
UINT4 FsMIStdOspfVirtIfRtrDeadInterval [ ] ={1,3,6,1,4,1,2076,146,8,1,6};
UINT4 FsMIOspfGlobalExtTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,10};
#endif
