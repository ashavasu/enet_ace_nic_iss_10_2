/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsosmilw.h,v 1.5 2009/06/04 05:35:31 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIOspfBRRouteTable. */
INT1
nmhValidateIndexInstanceFsMIOspfBRRouteTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfBRRouteTable  */

INT1
nmhGetFirstIndexFsMIOspfBRRouteTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfBRRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfBRRouteType ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfBRRouteAreaId ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfBRRouteCost ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfBRRouteInterfaceIndex ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfExtRouteTable. */
INT1
nmhValidateIndexInstanceFsMIOspfExtRouteTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfExtRouteTable  */

INT1
nmhGetFirstIndexFsMIOspfExtRouteTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfExtRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfExtRouteMetric ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfExtRouteMetricType ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfExtRouteTag ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfExtRouteFwdAdr ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfExtRouteIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfExtRouteNextHop ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfExtRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfExtRouteMetric ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfExtRouteMetricType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfExtRouteTag ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfExtRouteFwdAdr ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMIOspfExtRouteIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfExtRouteNextHop ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMIOspfExtRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfExtRouteMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfExtRouteMetricType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfExtRouteTag ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfExtRouteFwdAdr ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMIOspfExtRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfExtRouteNextHop ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMIOspfExtRouteStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfExtRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfGrTable. */
INT1
nmhValidateIndexInstanceFsMIOspfGrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfGrTable  */

INT1
nmhGetFirstIndexFsMIOspfGrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfGrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfGrShutdown ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfGrShutdown ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfGrShutdown ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfGrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
