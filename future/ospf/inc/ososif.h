/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ososif.h,v 1.10 2012/06/25 12:52:29 siva Exp $
 *
 * Description:This file contains constants , typedefs & macros 
 *             related to OS interface.
 *
 *******************************************************************/

#ifndef _OSOSIF_H
#define _OSOSIF_H

#define  OS_MEM_CMP(m1, m2, n)    MEMCMP(m1, m2, n)     
#define  OS_MEM_CPY(dst, src, n)  MEMCPY(dst, src, n)   
#define  OS_MEM_SET(ptr, val, n)  MEMSET(ptr, val, n)   


#define     OSPF_TASK_LOCK() \
               OsixTakeSem ( (UINT4)0 ,(const UINT1 *) "RTSM" , OSIX_WAIT , (UINT4)0 )

#define     OSPF_TASK_UNLOCK() \
               OsixGiveSem ( (UINT4)0 , (const UINT1 *)"RTSM" )

#define     OSPF_CXT_LOCK() \
               OsixTakeSem ( (UINT4)0 ,(const UINT1 *) "CXTSM" , OSIX_WAIT , (UINT4)0 )

#define     OSPF_CXT_UNLOCK() \
               OsixGiveSem ( (UINT4)0 , (const UINT1 *)"CXTSM" )

#endif
