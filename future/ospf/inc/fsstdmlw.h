/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsstdmlw.h,v 1.4 2008/08/20 15:20:08 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIStdOspfTrapTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfTrapTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfTrapTable  */

INT1
nmhGetFirstIndexFsMIStdOspfTrapTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfTrapTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfSetTrap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfConfigErrorType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfPacketType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfPacketSrc ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfSetTrap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfSetTrap ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfTrapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
