#include "osinc.h"
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfRouterId ARG_LIST((UINT4 *));

INT1
nmhGetOspfAdminStat ARG_LIST((INT4 *));

INT1
nmhGetOspfVersionNumber ARG_LIST((INT4 *));

INT1
nmhGetOspfAreaBdrRtrStatus ARG_LIST((INT4 *));

INT1
nmhGetOspfASBdrRtrStatus ARG_LIST((INT4 *));

INT1
nmhGetOspfExternLsaCount ARG_LIST((UINT4 *));

INT1
nmhGetOspfExternLsaCksumSum ARG_LIST((INT4 *));

INT1
nmhGetOspfTOSSupport ARG_LIST((INT4 *));

INT1
nmhGetOspfOriginateNewLsas ARG_LIST((UINT4 *));

INT1
nmhGetOspfRxNewLsas ARG_LIST((UINT4 *));

INT1
nmhGetOspfExtLsdbLimit ARG_LIST((INT4 *));

INT1
nmhGetOspfMulticastExtensions ARG_LIST((INT4 *));

INT1
nmhGetOspfExitOverflowInterval ARG_LIST((INT4 *));

INT1
nmhGetOspfDemandExtensions ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfRouterId ARG_LIST((UINT4 ));

INT1
nmhSetOspfAdminStat ARG_LIST((INT4 ));

INT1
nmhSetOspfASBdrRtrStatus ARG_LIST((INT4 ));

INT1
nmhSetOspfTOSSupport ARG_LIST((INT4 ));

INT1
nmhSetOspfExtLsdbLimit ARG_LIST((INT4 ));

INT1
nmhSetOspfMulticastExtensions ARG_LIST((INT4 ));

INT1
nmhSetOspfExitOverflowInterval ARG_LIST((INT4 ));

INT1
nmhSetOspfDemandExtensions ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfRouterId ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2OspfAdminStat ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2OspfASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2OspfTOSSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2OspfExtLsdbLimit ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2OspfMulticastExtensions ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2OspfExitOverflowInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2OspfDemandExtensions ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfRouterId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfAdminStat ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfASBdrRtrStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfTOSSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfExtLsdbLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfMulticastExtensions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfExitOverflowInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2OspfDemandExtensions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfAreaTable. */
INT1
nmhValidateIndexInstanceOspfAreaTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfAreaTable  */

INT1
nmhGetFirstIndexOspfAreaTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfAreaTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfAuthType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfImportAsExtern ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfSpfRuns ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfAreaBdrRtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfAsBdrRtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfAreaLsaCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfAreaLsaCksumSum ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfAreaSummary ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfAreaStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfAuthType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfImportAsExtern ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfAreaSummary ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfAreaStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfAuthType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2OspfImportAsExtern ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2OspfAreaSummary ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2OspfAreaStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfAreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfStubAreaTable. */
INT1
nmhValidateIndexInstanceOspfStubAreaTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfStubAreaTable  */

INT1
nmhGetFirstIndexOspfStubAreaTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfStubAreaTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfStubMetric ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfStubStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfStubMetricType ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfStubMetric ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfStubStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfStubMetricType ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfStubMetric ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfStubStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfStubMetricType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfStubAreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfLsdbTable. */
INT1
nmhValidateIndexInstanceOspfLsdbTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfLsdbTable  */

INT1
nmhGetFirstIndexOspfLsdbTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfLsdbTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfLsdbSequence ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfLsdbAge ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfLsdbChecksum ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfLsdbAdvertisement ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for OspfAreaRangeTable. */
INT1
nmhValidateIndexInstanceOspfAreaRangeTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfAreaRangeTable  */

INT1
nmhGetFirstIndexOspfAreaRangeTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfAreaRangeTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfAreaRangeMask ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfAreaRangeStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfAreaRangeEffect ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfAreaRangeMask ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetOspfAreaRangeStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfAreaRangeEffect ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfAreaRangeMask ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2OspfAreaRangeStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfAreaRangeEffect ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfAreaRangeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfHostTable. */
INT1
nmhValidateIndexInstanceOspfHostTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfHostTable  */

INT1
nmhGetFirstIndexOspfHostTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfHostTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfHostMetric ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfHostStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfHostAreaID ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfHostMetric ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfHostStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfHostMetric ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfHostStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfHostTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfIfTable. */
INT1
nmhValidateIndexInstanceOspfIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfIfTable  */

INT1
nmhGetFirstIndexOspfIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfIfAreaId ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfIfType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfAdminStat ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfRtrPriority ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfTransitDelay ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfRetransInterval ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfHelloInterval ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfRtrDeadInterval ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfPollInterval ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfState ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfDesignatedRouter ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfIfBackupDesignatedRouter ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfIfEvents ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfIfAuthKey ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfIfStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfMulticastForwarding ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfDemand ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfAuthType ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfIfAreaId ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetOspfIfType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfAdminStat ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfRtrPriority ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfTransitDelay ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfRetransInterval ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfHelloInterval ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfRtrDeadInterval ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfPollInterval ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfAuthKey ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetOspfIfStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfMulticastForwarding ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfDemand ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfAuthType ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfIfAreaId ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2OspfIfType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfAdminStat ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfRtrPriority ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfTransitDelay ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfRetransInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfHelloInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfRtrDeadInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfPollInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfAuthKey ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2OspfIfStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfMulticastForwarding ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfDemand ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfAuthType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfIfMetricTable. */
INT1
nmhValidateIndexInstanceOspfIfMetricTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfIfMetricTable  */

INT1
nmhGetFirstIndexOspfIfMetricTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfIfMetricTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfIfMetricValue ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetOspfIfMetricStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfIfMetricValue ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetOspfIfMetricStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfIfMetricValue ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfIfMetricStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfIfMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfVirtIfTable. */
INT1
nmhValidateIndexInstanceOspfVirtIfTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfVirtIfTable  */

INT1
nmhGetFirstIndexOspfVirtIfTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfVirtIfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfVirtIfTransitDelay ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtIfRetransInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtIfHelloInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtIfRtrDeadInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtIfState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtIfEvents ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfVirtIfAuthKey ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfVirtIfStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtIfAuthType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfVirtIfTransitDelay ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfVirtIfRetransInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfVirtIfHelloInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfVirtIfRtrDeadInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfVirtIfAuthKey ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetOspfVirtIfStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfVirtIfAuthType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfVirtIfTransitDelay ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfVirtIfRetransInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfVirtIfHelloInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfVirtIfRtrDeadInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfVirtIfAuthKey ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2OspfVirtIfStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfVirtIfAuthType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfVirtIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfNbrTable. */
INT1
nmhValidateIndexInstanceOspfNbrTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfNbrTable  */

INT1
nmhGetFirstIndexOspfNbrTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfNbrTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfNbrRtrId ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfNbrOptions ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfNbrPriority ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfNbrState ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfNbrEvents ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfNbrLsRetransQLen ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetOspfNbmaNbrStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfNbmaNbrPermanence ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetOspfNbrHelloSuppressed ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfNbrPriority ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetOspfNbmaNbrStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfNbrPriority ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2OspfNbmaNbrStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfNbrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for OspfVirtNbrTable. */
INT1
nmhValidateIndexInstanceOspfVirtNbrTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfVirtNbrTable  */

INT1
nmhGetFirstIndexOspfVirtNbrTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfVirtNbrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfVirtNbrIpAddr ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfVirtNbrOptions ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtNbrState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfVirtNbrEvents ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfVirtNbrLsRetransQLen ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfVirtNbrHelloSuppressed ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for OspfExtLsdbTable. */
INT1
nmhValidateIndexInstanceOspfExtLsdbTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfExtLsdbTable  */

INT1
nmhGetFirstIndexOspfExtLsdbTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfExtLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfExtLsdbSequence ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfExtLsdbAge ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfExtLsdbChecksum ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfExtLsdbAdvertisement ARG_LIST((INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for OspfAreaAggregateTable. */
INT1
nmhValidateIndexInstanceOspfAreaAggregateTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for OspfAreaAggregateTable  */

INT1
nmhGetFirstIndexOspfAreaAggregateTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfAreaAggregateTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfAreaAggregateStatus ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfAreaAggregateEffect ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfAreaAggregateStatus ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfAreaAggregateEffect ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfAreaAggregateStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2OspfAreaAggregateEffect ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfAreaAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
