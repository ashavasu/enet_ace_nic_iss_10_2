/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osclipt.h,v 1.31 2017/12/28 10:40:15 siva Exp $
 *
 * Description: This has prototype definitions for OSPF CLI submodule
 *
 ***********************************************************************/
#ifndef __OSCLIPT_H__
#define __OSCLIPT_H__

/* Counters to store the LSA database informations w.r.t LSA Type
 * used in show command 
 */
typedef struct {
       UINT4 au4LsTypeCnt[MAX_LSA_TYPE];
       INT4  au4LsTypeMaxAgedCnt[MAX_LSA_TYPE];
} tLsdbCounter;

/* Prototype declarations for OSPF CLI */

UINT4 OspfCliGetIndicesFromIfIndex PROTO ((UINT4 u4Index, 
                                           UINT4 *pu4IfIpAddr,
                                           INT4 *pi4AddrlessIf));
INT4 OspfCliShowInterfaceInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                 INT4 i4AddrlessIf));
INT4 OspfCliShowNeighborInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                UINT4 u4IfIndex, UINT4 u4NbrId, 
                         INT4 i4ShowNbrOption, UINT1 u1PrintHdr));
INT4
OspfCliShowVlanInterfaceInCxt PROTO((tCliHandle CliHandle, UINT1 *pu1VrfName,
                               UINT1 *pu1IfName));
INT4 OspfCliShowRequestListInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                    UINT4 u4IfIndex, UINT4 u4NbrId, 
      INT4 i4ShowOption));
INT4 OspfCliShowRxmtListInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                UINT4 u4IfIndex, UINT4 u4NbrId, 
         INT4 i4ShowOption));
INT4 OspfCliShowVirtualIfInCxt PROTO ((tCliHandle CliHandle,UINT4 u4OspfCxtId));
INT4 OspfCliShowBorderRouterInCxt PROTO ((tCliHandle CliHandle,UINT4 u4OspfCxtId));
INT4 OspfCliShowSummaryAddressInCxt PROTO ((tCliHandle CliHandle, 
                       UINT4 u4OspfCxtId));
INT4 OspfCliShowExtSummaryAddressInCxt PROTO ((tCliHandle CliHandle,
                          UINT4 u4OspfCxtId));
INT4 OspfCliShowOspfInfoInCxt PROTO ((tCliHandle CliHandle, 
                 UINT4 u4OspfCxtId));
INT4 OspfCliShowOspfRedInfo (tCliHandle CliHandle);
#ifdef RM_WANTED
INT4 OspfCliShowOspfRedExtRouteInfo(tCliHandle CliHandle);
#endif

INT4 OspfCliShowRoutingTableInCxt PROTO ((tCliHandle CliHandle, 
                     UINT4 u4OspfCxtId));
INT4 OspfCliShowDatabaseInCxt PROTO ((tCliHandle CliHandle, tDatabase Database,
                        UINT4 u4OspfCxtId));
INT4 OspfCliSetTraceLevelInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                         INT4 i4CliTraceVal, INT4 i4CliGrTraceVal,
                                       UINT1 u1TraceFlag));
INT4 OspfCliSetHostMetric PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                  UINT4 u4HostIpAddr,INT4 i4HostTOS,INT4 i4HostMetric));
INT4 OspfCliDelHostMetric PROTO ((tCliHandle CliHandle, INT4 i4ContextId,UINT4 u4HostIpAddr));
INT4 OspfSetTraceValue PROTO ((INT4 i4TrapLevel,UINT1 u1LoggingCmd));
INT4 OspfCliSetAdminStatInCxt PROTO ((tCliHandle CliHandle, INT4 i4AdminStat, UINT4 u4OspfCxtId));
INT4 OspfCliShowOspfCounters  PROTO ((tCliHandle CliHandle));
INT4 OspfCliSetRouterId PROTO ((tCliHandle CliHandle, UINT4 u4RouterId));
INT4 OspfCliDelRouterId PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId));
INT4 OspfCliSetStabilityInterval PROTO ((tCliHandle CliHandle,
                                         UINT4 u4AreaId, INT4 i4NssaAttrib));
INT4 OspfCliSetTransRole PROTO ((tCliHandle CliHandle, 
                                 UINT4 u4AreaId, INT4 i4NssaAttrib));
INT4 OspfCliSetRFC1583Compatibility PROTO ((tCliHandle CliHandle, INT4 i4FutInt));
INT4 OspfCliSetBfdStatus PROTO ((tCliHandle CliHandle, INT4 i4FutInt));

INT4 OspfCliSetBfdAllIfState PROTO ((tCliHandle CliHandle, INT4 i4FutInt));

INT4 OspfCliSetBfdStatusSpecificInt PROTO ((tCliHandle CliHandle,
                                            UINT4 u4IfIndex, INT4 i4FutInt));
INT4 OspfCliSetABRType PROTO ((tCliHandle CliHandle, INT4 i4AbrType));
INT4 OspfCliSetNbrPriority PROTO ((tCliHandle CliHandle, 
                                   UINT4 u4IfIpAddr, INT4 i4NbrPriority, 
                                   INT4 i4AddrlessIf, UINT1 u1Flag));
INT4 OspfCliSetIfPollInterval PROTO ((tCliHandle CliHandle,
                                      UINT4 u4IfIpAddr, INT4 i4PollIntervali,
                                      INT4 i4AddrlessIf, UINT1 u1Flag));

INT4 OspfCliDelNbr PROTO ((tCliHandle CliHandle, UINT4 u4IfIpAddr, INT4 i4AddrlessIf));
INT4 OspfCliSetAreaAuthTypeInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                         UINT4 u4AreaId, INT4 i4AuthType));
INT4 OspfCliSetStubMetric PROTO ((tCliHandle CliHandle,
                                  UINT4 u4AreaId, INT4 i4MetricValue,
                                  INT4 i4MetricTOS, UINT1 u1NonTos0Delete));
INT4 OspfCliSetStubMetricType PROTO ((tCliHandle CliHandle, UINT4 u4AreaId, 
                                      INT4 i4Metric, INT4 i4StubMetricType, 
                                      INT4 i4MetricTOS, UINT1 u1NonTos0Delete));
INT4 OspfCliSetAreaStub PROTO ((tCliHandle CliHandle, UINT4 u4AreaId,
                                INT4 i4ImportAsExtern, INT4 i4AreaSummary));
INT4 OspfCliDelArea PROTO ((tCliHandle CliHandle, UINT4 u4AreaId));
INT4 OspfCliSetDefaultOriginate PROTO ((tCliHandle CliHandle, 
                                        INT4 i4Metric, INT4 i4MetricType,
                                        UINT4 u4FutOspfExtRouteDest, 
                                        UINT4 u4FutOspfExtRouteMask,
                                        UINT4 u4FutOspfExtRouteTOS,
     UINT1 u1flag));
INT4 OspfCliDelDefaultOriginate PROTO ((tCliHandle CliHandle,
                                        UINT4 u4FutOspfExtRouteDest,
                                        UINT4 u4FutOspfExtRouteMask,
                                        UINT4 u4FutOspfExtRouteTOS));
INT4 OspfCliSetVirtualLink PROTO ((tCliHandle CliHandle, tAreaStruct *pArea));
INT4 OspfCliDelVirtualLink PROTO ((tCliHandle CliHandle, tAreaStruct *pArea));
INT4 OspfCliSetDefVirtualLink PROTO ((tCliHandle CliHandle, tAreaStruct *pArea));
INT4 OspfCliSetASBRStatus PROTO ((tCliHandle CliHandle, INT4 i4FutInt));
INT4 OspfCliSetSummary PROTO ((tCliHandle CliHandle, UINT4 u4SummNet, 
                               UINT4 u4SummMask, UINT4 u4SummArea, 
                               INT4 i4SummEffect, INT4 i4SummLsaType,
                               INT4 i4SummTag, INT4 i4SetSummaryTag));
INT4 OspfCliDelSummAddr PROTO ((tCliHandle CliHandle, UINT4 u4SummNet,
                                UINT4 u4SummMask, UINT4 u4SummArea,INT4 i4SummLsaType)); 
INT4 OspfCliAddExtSummAddr PROTO ((tCliHandle CliHandle, UINT4 u4SummNet,
                                   UINT4 u4SummMask, UINT4 u4SummArea, 
                                   INT4 i4SummEffect, INT4 i4SummLsaType));
INT4 OspfCliDelExtSummAddr PROTO ((tCliHandle CliHandle, UINT4 u4SummNet,
                                   UINT4 u4SummMask, UINT4 u4SummArea));
INT4 OspfCliSetRedistribute PROTO ((tCliHandle CliHandle,
                                    UINT4 u4OspfCxtId, 
                                    INT4 i4RedistProtoMask, 
                                    UINT4 u4RedistStatus,
                                    UINT1 *pu1RouteMapName,
                                    INT4 i4MetricValue,
                                    INT4 i4MetricType));
INT4 OspfCliSetDistribute PROTO ((tCliHandle cliHandle, 
                                  UINT1 *pu1RMapName,
                                  UINT1 u1Status));
INT4 OspfCliSetRRDRoute PROTO ((tCliHandle CliHandle, 
                                UINT4 u4FutOspfRRDRouteDest,
                                UINT4 u4FutOspfRRDRouteMask,
                                INT4 i4Metric, INT4 i4MetricType, UINT4 u4TagValue));
INT4 OspfCliDelRRDRoute PROTO ((tCliHandle CliHandle,
                                UINT4 u4FutOspfRRDRouteDest,
                                UINT4 u4FutOspfRRDRouteMask));
INT4 OspfCliSetNetworkArea PROTO ((tCliHandle CliHandle,
                                   UINT4 u4OspfAddressLessIf,
                                   UINT4 u4IfIpAddr, UINT4 u4AreaId));
INT4 OspfCliDelNetworkArea PROTO ((tCliHandle CliHandle,
                                   UINT4 u4OspfAddressLessIf,
                                   UINT4 u4IfIpAddr, UINT4 u4AreaId));
INT4 OspfCliSetNssaAsbrDfRt PROTO ((tCliHandle CliHandle, INT4 i4FutInt));
INT4 OspfCliSetIfPassive PROTO ((tCliHandle CliHandle, 
                                 UINT4 u4IfIndex, INT4 i4AdminStat));
INT4 OspfCliSetDefaultPassive PROTO ((tCliHandle CliHandle, INT4 i4AdminStat));
INT4 OspfCliSetDemandCircuit PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                     INT4 i4DemandExtn, INT4 i4DemandCkt));
INT4 OspfCliSetIfRetransInterval PROTO ((tCliHandle CliHandle,
                                         UINT4 u4IfIndex, INT4 i4RetransInterval));
INT4 OspfCliSetIfTransitDelay PROTO ((tCliHandle CliHandle,
                                      UINT4 u4IfIndex, INT4 i4TransitDelay));
INT4 OspfCliSetIfRtrPriority PROTO ((tCliHandle CliHandle,
                                     UINT4 u4IfIndex, INT4 i4IfRtrPriority));
INT4 OspfCliSetIfHelloInterval PROTO ((tCliHandle CliHandle,
                                       UINT4 u4IfIndex, INT4 i4HelloInterval));
INT4 OspfCliSetIfDeadInterval PROTO ((tCliHandle CliHandle,
                                      UINT4 u4IfIndex, INT4 i4RtrDeadInterval));
INT4 OspfCliSetIfMetricValue PROTO ((tCliHandle CliHandle,
                                     UINT4 u4IfIndex, INT4 i4MetricValue,
                                     INT4 i4MetricTOS, UINT1 u1NonTos0Delete)); 
INT4 OspfCliSetIfType PROTO ((tCliHandle CliHandle,
                              UINT4 u4IfIndex, INT4 i4IfType));
INT4 OspfCliSetIfAuthKey PROTO ((tCliHandle CliHandle,
                                 UINT4 u4IfIndex, INT1 *pu1AuthKey));
INT4 OspfCliSetIfAuthType PROTO ((tCliHandle CliHandle,
                                  UINT4 u4IfIndex, INT4 i4AuthType, INT4 i4CryptoAuthType));
INT4 OspfCliSetIfMD5AuthKey PROTO ((tCliHandle CliHandle,
                                    UINT4 u4IfIndex, INT4 i4AuthKeyId,
                                    UINT1 *pu1AuthKey,UINT4 u4IfCryptoAuthType, INT4 i4KeyStatus));
INT4 OspfCliSetIfMD5AuthKeyStartAccept(tCliHandle CliHandle,INT4 KeyId,
          UINT1 *pu1StartAccepttime,UINT4 u4OspfCxtId,
          UINT4 u4IfIndex);
INT4 OspfCliSetIfMD5AuthKeyStartGenerate(tCliHandle CliHandle,INT4 KeyId,
                                      UINT1 *pu1StartGentime,UINT4 u4OspfCxtId,
                                      UINT4 u4IfIndex);
INT4 OspfCliSetIfMD5AuthKeyStopGenerate(tCliHandle CliHandle,INT4 KeyId,
                                      UINT1 *pu1StopGentime,UINT4 u4OspfCxtId,
                                      UINT4 u4IfIndex);
INT4 OspfCliSetIfMD5AuthKeyStopAccept(tCliHandle CliHandle,INT4 KeyId,
                                      UINT1 *pu1StopAccepttime,UINT4 u4OspfCxtId,
                                      UINT4 u4IfIndex);

INT4 OspfCliPrintLSAInfo PROTO ((tCliHandle CliHandle, tNeighbor * pNeighbor));
INT4 OspfCliDumpLsHeader PROTO ((tCliHandle CliHandle, UINT1 *pu1LsHeader));
INT4 OspfCliPrintRxmtLSAInfo PROTO ((tCliHandle CliHandle, tNeighbor * pNeighbor));
INT4 OspfCliPrintOspfInterfaceInfo PROTO ((tCliHandle CliHandle,
                                           tInterface * pInterface));
INT4 OspfCliShowBRInCxt PROTO ((tCliHandle CliHandle, UINT1 *pu1Buffer, 
                               UINT4 u4BufLen, tShowOspfBRCookie * pCookie,
          UINT4 u4ShowAllCxt));
INT4 OspfCliDisplayBR PROTO ((tCliHandle CliHandle,
                              tShowOspfBR * pOspfBR, INT4 i4HdrFlag));
INT4 OspfCliShowRTInCxt PROTO ((tCliHandle CliHandle, UINT1 *pu1Buffer,
                                UINT4 u4BufLen, tShowOspfRTCookie * pCookie,
           UINT4 u4ShowAllCxt));
INT4 OspfCliDisplayRT PROTO ((tCliHandle CliHandle, 
                              tShowOspfRT * pOspfRT, INT4 i4HdrFlag, UINT4 *PrevCxtId));
INT4 OspfCliSetAreaDataBaseInCxt PROTO ((tDatabase Database, 
                    tOspfCxt * pOspfCxt));
INT4 OspfCliFindAreaInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId, 
             UINT4 u4AreaId,INT4 i4LsaType, INT4 i4Option));
INT4 OspfCliShowAreaDataBaseSummaryInCxt PROTO ((tCliHandle CliHandle, 
                            UINT4 u4OspfCxtId,
                                                 UINT4 u4AreaId, 
       tLsdbCounter *LsdbCounter));
INT4 OspfCliDisplayLsa PROTO ((tCliHandle CliHandle, tShowOspfDb * pOspfDb,
                               INT4 i4ShowOption, UINT4 u4AreaId, 
          UINT4 u4AdvRouterId));
INT4 OspfCliShowLsdbDatabase PROTO ((tCliHandle CliHandle, UINT1 *pu1Buffer,
                                     UINT4 u4BufLen, tShowOspfDbCookie * pCookie));
INT4 OspfCliShowSAInCxt PROTO ((tCliHandle CliHandle, UINT1 *pu1Buffer, 
                                UINT4 u4BufLen, tShowOspfSACookie * pCookie, 
           UINT4 u4ShowAllCxt));
INT4 OspfCliDisplaySA PROTO ((tCliHandle CliHandle,
                              tShowOspfSA * pOspfSA, INT4 i4HdrFlag));
INT4 OspfCliFilterLsaInfo PROTO ((tCliHandle CliHandle, tLsaInfo * pLsaInfo,
                                  INT4 i4Option));
INT4 OspfCliPrintLsaInfo PROTO ((tCliHandle CliHandle, tLsaInfo * pLsaInfo));
INT4 OspfCliPrintLsaSummary PROTO ((tCliHandle CliHandle, tLsaInfo * pLsaInfo));
INT4 OspfCliFilterLsaSummaryInfo PROTO ((tCliHandle CliHandle,UINT4 u4OspfCxt, UINT4 u4AreaId,INT4 i4Option));

INT4 OspfCliFilterLsaSummary PROTO ((tCliHandle CliHandle, tLsaInfo *pLsaInfo, 
                        INT4 i4Option, INT4 *pi4HeaderFlag));
INT4 OspfCliPrintLsaSummaryHeader PROTO ((tCliHandle CliHandle, 
                     tLsaInfo *pLsaInfo, INT4 i4LsaType));
INT4 OspfCliShowVIInCxt PROTO ((tCliHandle CliHandle, UINT1 *pu1Buffer, 
                           UINT4 u4BufLen, tShowOspfVICookie * pCookie,
      UINT4 u4ShowAllCxt));
INT4 OspfCliDisplayVI PROTO ((tCliHandle CliHandle, tShowOspfVI * pOspfVI));
INT4 OspfCliShowExtSAInCxt PROTO ((tCliHandle CliHandle, UINT1 *pu1Buffer,
                                   UINT4 u4BufLen, 
       tShowOspfExtSACookie * pCookie, 
       UINT4 u4ShowAllCxt));
INT4 OspfCliDisplayExtSA PROTO ((tCliHandle CliHandle,
                                 tShowOspfExtSA * pOspfExtSA, INT4 i4HdrFlag));

INT4 OspfCliGetAsExternalLsaCntInCxt PROTO ((tOspfCxt * pOspfCxt, 
                        UINT4 *pu4LsaCnt, 
          UINT4 *pu4LsaAgedCnt));
INT4 OspfCliGetOpqAsLsaCntInCxt PROTO ((tOspfCxt * pOspfCxt, UINT4 *pu4LsaCnt, 
                   UINT4 *pu4LsaAgedCnt));
INT4 OspfCliShowExtDatabaseInCxt PROTO ((tCliHandle CliHandle, 
                    UINT4 u4OspfCxtId, INT4 i4Option));
INT4 OspfCliShowOpqAsDatabaseInCxt PROTO ((tCliHandle CliHandle, 
                      UINT4 u4OspfCxtId, INT4 i4Option));
VOID IssOspfShowDebuggingInCxt (tCliHandle, UINT4);
VOID IssOspfShowDebugging (tCliHandle, UINT4);



INT4
OspfCliSetIfVirtAuthKeyStartAccept (tCliHandle CliHandle,UINT4 u4AreaId, UINT4 u4NbrId, INT4 KeyId,
                                   UINT1 *pu1StartAcceptTime, UINT4 u4OspfCxtId);
INT4
OspfCliSetIfVirtAuthKeyStopGenerate (tCliHandle CliHandle,UINT4 u4AreaId, UINT4 u4NbrId, INT4 KeyId,
                                    UINT1 *pu1StopGenTime, UINT4 u4OspfCxtId);
INT4
OspfCliSetIfVirtAuthKeyStartGenerate (tCliHandle CliHandle,UINT4 u4AreaId, UINT4 u4NbrId,INT4 KeyId,
                                     UINT1 *pu1StartGenTime, UINT4 u4OspfCxtId);
INT4
OspfCliSetIfVirtAuthKeyStopAccept (tCliHandle CliHandle,UINT4 u4AreaId, UINT4 u4NbrId, INT4 KeyId,
                                    UINT1 *pu1StopAcceptTime, UINT4 u4OspfCxtId);
/*For Show Running Config*/
INT4  OspfShowRunningConfigInCxt(tCliHandle CliHandle,UINT4 u4Module, 
                   UINT4 u4OspfCxtId);
VOID OspfShowRunningConfigScalarsInCxt (tCliHandle CliHandle, 
                          UINT4 u4OspfCxtId);
VOID OspfShowRunningConfigTablesInCxt (tCliHandle CliHandle,
                         UINT4 u4OspfCxtId);
VOID FutOspfAreaTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                  UINT4 u4AreaId, UINT4 *pu4PagingStatus);
VOID OspfAreaTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
               UINT4 u4AreaId, UINT4 *pu4PagingStatus);
VOID OspfStubAreaTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                                 UINT4 u4AreaId,
                                 INT4 i4MetricTOS, UINT4 *pu4PagingStatus);
VOID OspfIfTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                           UINT4 u4IpAddr,
                           INT4 i4Index,
                           INT4 IntFlag,UINT4 *pu4PagingStatus);
VOID FutOspfIfTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                UINT4 u4IpAddr,INT4 i4Index, 
         UINT4 *pu4PagingStatus);
VOID OspfIfMetricTableInfoInCxt (tCliHandle CliHandle,
                   UINT4 u4OspfCxtId,
                                 UINT4 u4IpAddr,
                                 INT4 i4IfIndex,
                                 INT4 i4Tos);
VOID FutOspfIfMD5AuthTableInfoInCxt(tCliHandle CliHandle,
                      UINT4 u4OspfCxtId,
                                    UINT4 u4IpAddr,
                                    INT4 i4IfIndex,
                                    INT4 i4AuthKeyId,
        INT4 i4StartAccept,
        INT4 i4StartGenerate,
        INT4 i4StopGenerate,
        INT4 i4StopAccept,
        INT4 i4CryptoAuthType);
VOID OspfVirtIfTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                 UINT4 u4AreaId,UINT4 u4Neighbor,
                               UINT4 *pu4PagingStatus);
VOID OspfNbrTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
              UINT4 u4IpAddr,INT4 i4IfIndex, UINT4 *pu4PagingStatus);
VOID OspfAreaAggregateTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                 UINT4 u4SummArea,
                                 INT4 i4SummLsaType,
                                 UINT4 u4SummNet,
                                 UINT4 u4SummMask,UINT4 *pu4PagingStatus);
VOID FutOspfAsExternalAggregationTableInfoInCxt (tCliHandle CliHandle, 
                                   UINT4 u4OspfCxtId, 
                                                 UINT4 u4SummNet,
                                                 UINT4 u4SummMask,UINT4 u4SummArea,
                                                 UINT4 *pu4PagingStatus);
VOID FutOspfRRDRouteConfigTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                            UINT4 u4RouteDest, UINT4 u4RouteMask,
       UINT4 *pu4PagingStatus);
VOID FutOspfExtRouteTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, 
                      UINT4 u4RouteDest, UINT4 u4RouteMask,
                                    INT4 i4RouteTos,UINT4 *pu4PagingStatus);
INT4 OspfShowRunningConfigInterfaceInCxt (tCliHandle CliHandle, 
                            tOspfCxt * pOspfCxt);
INT4 OspfCliGetCxtIdFromCxtName PROTO ((tCliHandle CliContext, UINT1 * pu1OspfCxtName,
                          UINT4 * pu4OspfCxtId));

INT4 OspfCliGetCxtIdFromIfIndex PROTO ((tCliHandle CliContext, UINT4 u4IfIndex,
                          UINT4 * pu4OspfCxtId));

INT4 OspfCliGetCxtIdFromNeighborId PROTO ((tCliHandle CliHandle, UINT4 u4NbrId,
                             UINT4 * pu4OspfCxtId));

INT4 OspfCliGetContext PROTO ((tCliHandle CliHandle, UINT4 u4Command,
                 UINT4 * pu4OspfCxtId, UINT4 * pu4IfIndex,
          UINT2 * pu2ShowCmdFlag));
INT4 OspfCliSetSpfTimers PROTO ((tCliHandle CliHandle, INT4 i4OspfSpfHold,
      INT4 i4OspfSpfDelay));
INT4 OspfCliResetSpfTimers PROTO ((tCliHandle CliHandle ));

INT4 OspfCliSetOpaqueOption PROTO ((tCliHandle CliHandle,
                                    INT4 i4OpaqueOption));
VOID OspfIfAuthInfoInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                UINT4 u4IpAddr, INT4 i4AddrLessIfIndex));
/* CLI prototypes specific for graceful restart */
INT4 OspfCliSetRestartSupport PROTO ((tCliHandle CliHandle,
                                      INT4 i4RestartSupport));
INT4 OspfCliSetGracePeriod PROTO ((tCliHandle CliHandle, INT4 i4GracePeriod));
INT4 OspfCliSetHelperSupport PROTO ((tCliHandle CliHandle,
                                     INT4 i4HelperSupport,UINT1 u1HelperFlag));
INT4 OspfCliSetStrictLsaCheck PROTO ((tCliHandle CliHandle,
                                      INT4 i4StrictLsaCheck));
INT4 OspfCliSetHelperGraceLimitPeriod  PROTO ((tCliHandle CliHandle, 
                     INT4 i4GraceLimitPeriod));
INT4 OspfCliSetGrAckState PROTO ((tCliHandle CliHandle,
                                      INT4 i4GrAckState));

INT4 OspfCliSetGraceRetransCount PROTO ((tCliHandle CliHandle, INT4 i4GracePeriod));

INT4 OspfCliSetRestartReason  PROTO ((tCliHandle CliHandle, INT4 i4RestartReason));

INT4 OspfCliSetStaggeringStatus (tCliHandle , INT4);

INT4 OspfCliSetStaggeringInterval (tCliHandle, INT4);
INT4 OspfCliSetBfdAdmState (tCliHandle, INT4);

INT4 OspfCliSetBfdStatusAllInt (tCliHandle, INT4);
INT4 OspfSetRouteDistance PROTO ((tCliHandle CliHandle, INT4 i4Distance,
                                  UINT1 *pu1RMapName));

INT4 OspfSetNoRouteDistance PROTO ((tCliHandle CliHandle, UINT1 *pu1RMapName));
INT4
OspfCliShowReqLstForAllVlanInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                         UINT4 u4IfIndex, UINT4 u4NbrId, INT4 i4ShowOption,
                                         UINT1 *pu1IfName));
INT4
OspfCliShowRxmtLtForAllVlanInCxt PROTO ((tCliHandle CliHandle, UINT4 u4OspfCxtId,
                          UINT4 u4IfIndex, UINT4 u4NbrId, INT4 i4ShowOption,
                          UINT1 *pu1IfName));
#endif /* __OSCLIPT_H__ */
