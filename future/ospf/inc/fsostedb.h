/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsostedb.h,v 1.7 2009/06/04 05:35:31 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOSTEDB_H
#define _FSOSTEDB_H

UINT1 FutOspfBRRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfExtRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsoste [] ={1,3,6,1,4,1,2076,10,100};
tSNMP_OID_TYPE fsosteOID = {9, fsoste};


UINT4 FutOspfBRRouteIpAddr [ ] ={1,3,6,1,4,1,2076,10,100,1,1,1};
UINT4 FutOspfBRRouteIpAddrMask [ ] ={1,3,6,1,4,1,2076,10,100,1,1,2};
UINT4 FutOspfBRRouteIpTos [ ] ={1,3,6,1,4,1,2076,10,100,1,1,3};
UINT4 FutOspfBRRouteIpNextHop [ ] ={1,3,6,1,4,1,2076,10,100,1,1,4};
UINT4 FutOspfBRRouteDestType [ ] ={1,3,6,1,4,1,2076,10,100,1,1,5};
UINT4 FutOspfBRRouteType [ ] ={1,3,6,1,4,1,2076,10,100,1,1,6};
UINT4 FutOspfBRRouteAreaId [ ] ={1,3,6,1,4,1,2076,10,100,1,1,7};
UINT4 FutOspfBRRouteCost [ ] ={1,3,6,1,4,1,2076,10,100,1,1,8};
UINT4 FutOspfBRRouteInterfaceIndex [ ] ={1,3,6,1,4,1,2076,10,100,1,1,9};
UINT4 FutOspfExtRouteDest [ ] ={1,3,6,1,4,1,2076,10,100,2,1,1};
UINT4 FutOspfExtRouteMask [ ] ={1,3,6,1,4,1,2076,10,100,2,1,2};
UINT4 FutOspfExtRouteTOS [ ] ={1,3,6,1,4,1,2076,10,100,2,1,3};
UINT4 FutOspfExtRouteMetric [ ] ={1,3,6,1,4,1,2076,10,100,2,1,4};
UINT4 FutOspfExtRouteMetricType [ ] ={1,3,6,1,4,1,2076,10,100,2,1,5};
UINT4 FutOspfExtRouteTag [ ] ={1,3,6,1,4,1,2076,10,100,2,1,6};
UINT4 FutOspfExtRouteFwdAdr [ ] ={1,3,6,1,4,1,2076,10,100,2,1,7};
UINT4 FutOspfExtRouteIfIndex [ ] ={1,3,6,1,4,1,2076,10,100,2,1,8};
UINT4 FutOspfExtRouteNextHop [ ] ={1,3,6,1,4,1,2076,10,100,2,1,9};
UINT4 FutOspfExtRouteStatus [ ] ={1,3,6,1,4,1,2076,10,100,2,1,10};
UINT4 FutOspfGrShutdown [ ] ={1,3,6,1,4,1,2076,10,100,100,1};



tMbDbEntry fsosteMibEntry[]= {

{{12,FutOspfBRRouteIpAddr}, GetNextIndexFutOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteIpAddrMask}, GetNextIndexFutOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteIpTos}, GetNextIndexFutOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteIpNextHop}, GetNextIndexFutOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteDestType}, GetNextIndexFutOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteType}, GetNextIndexFutOspfBRRouteTable, FutOspfBRRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteAreaId}, GetNextIndexFutOspfBRRouteTable, FutOspfBRRouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteCost}, GetNextIndexFutOspfBRRouteTable, FutOspfBRRouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfBRRouteInterfaceIndex}, GetNextIndexFutOspfBRRouteTable, FutOspfBRRouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfBRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FutOspfExtRouteDest}, GetNextIndexFutOspfExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfExtRouteTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfExtRouteMask}, GetNextIndexFutOspfExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfExtRouteTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfExtRouteTOS}, GetNextIndexFutOspfExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfExtRouteTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfExtRouteMetric}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteMetricGet, FutOspfExtRouteMetricSet, FutOspfExtRouteMetricTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfExtRouteMetricType}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteMetricTypeGet, FutOspfExtRouteMetricTypeSet, FutOspfExtRouteMetricTypeTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 0, "1"},

{{12,FutOspfExtRouteTag}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteTagGet, FutOspfExtRouteTagSet, FutOspfExtRouteTagTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 0, "0"},

{{12,FutOspfExtRouteFwdAdr}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteFwdAdrGet, FutOspfExtRouteFwdAdrSet, FutOspfExtRouteFwdAdrTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 0, "0"},

{{12,FutOspfExtRouteIfIndex}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteIfIndexGet, FutOspfExtRouteIfIndexSet, FutOspfExtRouteIfIndexTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 0, "0"},

{{12,FutOspfExtRouteNextHop}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteNextHopGet, FutOspfExtRouteNextHopSet, FutOspfExtRouteNextHopTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 0, "0"},

{{12,FutOspfExtRouteStatus}, GetNextIndexFutOspfExtRouteTable, FutOspfExtRouteStatusGet, FutOspfExtRouteStatusSet, FutOspfExtRouteStatusTest, FutOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfExtRouteTableINDEX, 3, 0, 1, NULL},
{{11,FutOspfGrShutdown}, NULL, FutOspfGrShutdownGet, FutOspfGrShutdownSet, FutOspfGrShutdownTest, FutOspfGrShutdownDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData fsosteEntry = { 20, fsosteMibEntry };
#endif /* _FSOSTEDB_H */

