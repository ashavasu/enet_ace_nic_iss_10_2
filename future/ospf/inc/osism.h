/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osism.h,v 1.6 2007/02/01 15:00:38 iss Exp $
 *
 * Description:This file contains constants, typdefinitions
 *             and static varible declaration specific to osism.c
 *
 *******************************************************************/
#ifndef _OSISM_H
#define _OSISM_H


/* ISM Action (IA) routine numbers */
#define  IA0           0 
#define  IA1           1 
#define  IA2           2 
#define  IA3           3 
#define  IA4           4 
#define  IA5           5 
#define  MAX_ISM_FUNC  6 

/* the interface state machine transition table */

static UINT1  au1_ism_table[MAX_IF_EVENT][MAX_IF_STATE] = {

/*______________________________________________________________________________
               DOWN    LOOPBACK    WAITING   P-TO-P   DR   BACKUP   DROTHER  
________________________________________________________________________________
INTERFACE                                                                  
  UP */    {   IA1,     IA0,        IA0,      IA0,   IA0,    IA0,     IA0   },
/*______________________________________________________________________________
WAIT TIMER                                                                 
*/         {   IA0,     IA0,        IA2,      IA0,   IA0,    IA0,     IA0   },
/*______________________________________________________________________________
BACKUP                                                                     
SEEN */    {   IA0,     IA0,        IA2,      IA0,   IA0,    IA0,     IA0   },
/*______________________________________________________________________________
NBR CHANGE                                                                 
*/         {   IA0,     IA0,        IA0,      IA0,   IA2,    IA2,     IA2   },
/*______________________________________________________________________________
LOOP IND                                                                   
*/         {   IA5,     IA5,        IA5,      IA5,   IA5,    IA5,     IA5   },
/*______________________________________________________________________________
UNLOOP IND                                                                 
*/         {   IA0,     IA4,        IA0,      IA0,   IA0,    IA0,     IA0   },
/*______________________________________________________________________________
INTERFACE                                                                  
DOWN*/     {   IA3,     IA3,        IA3,      IA3,   IA3,    IA3,     IA3   }
/*______________________________________________________________________________
*/
};

/* type of the functions which perform the action in ISM */

typedef  void (*tIsmFunc)(tInterface *);

/* array of pointers to functions performing actions during ISM transitions */

tIsmFunc a_ism_func[MAX_ISM_FUNC] = {
               /* IA0 */          IsmInvalid,
               /* IA1 */          IsmIfUp,
               /* IA2 */          IsmElectDr,
               /* IA3 */          IsmIfReset,
               /* IA4 */          IsmIfDown,
               /* IA5 */          IsmIfLoop
               };
               
#endif


               
