/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsospfdb.h,v 1.20 2016/05/11 11:38:50 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOSPFDB_H
#define _FSOSPFDB_H

UINT1 FutOspfAreaTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfHostTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfIfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfIfMD5AuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfVirtIfMD5AuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfNbrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfRoutingTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfSecIfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfAreaAggregateTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfAsExternalAggregationTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfOpaqueInterfaceTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfType9LsdbTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfType11LsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfAppInfoDbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfRRDRouteConfigTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfVirtNbrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfDistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfIfAuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfVirtIfAuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfIfCryptoAuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfVirtIfCryptoAuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsospf [] ={1,3,6,1,4,1,2076,10};
tSNMP_OID_TYPE fsospfOID = {8, fsospf};


UINT4 FutOspfOverFlowState [ ] ={1,3,6,1,4,1,2076,10,1,1};
UINT4 FutOspfPktsRcvd [ ] ={1,3,6,1,4,1,2076,10,1,2};
UINT4 FutOspfPktsTxed [ ] ={1,3,6,1,4,1,2076,10,1,3};
UINT4 FutOspfPktsDisd [ ] ={1,3,6,1,4,1,2076,10,1,4};
UINT4 FutOspfRFC1583Compatibility [ ] ={1,3,6,1,4,1,2076,10,1,5};
UINT4 FutOspfMaxAreas [ ] ={1,3,6,1,4,1,2076,10,1,6};
UINT4 FutOspfMaxLSAperArea [ ] ={1,3,6,1,4,1,2076,10,1,7};
UINT4 FutOspfMaxExtLSAs [ ] ={1,3,6,1,4,1,2076,10,1,8};
UINT4 FutOspfMaxSelfOrgLSAs [ ] ={1,3,6,1,4,1,2076,10,1,9};
UINT4 FutOspfMaxRoutes [ ] ={1,3,6,1,4,1,2076,10,1,10};
UINT4 FutOspfMaxLsaSize [ ] ={1,3,6,1,4,1,2076,10,1,11};
UINT4 FutOspfTraceLevel [ ] ={1,3,6,1,4,1,2076,10,1,12};
UINT4 FutOspfMinLsaInterval [ ] ={1,3,6,1,4,1,2076,10,1,13};
UINT4 FutOspfABRType [ ] ={1,3,6,1,4,1,2076,10,1,14};
UINT4 FutOspfNssaAsbrDefRtTrans [ ] ={1,3,6,1,4,1,2076,10,1,15};
UINT4 FutOspfDefaultPassiveInterface [ ] ={1,3,6,1,4,1,2076,10,1,16};
UINT4 FutOspfSpfHoldtime [ ] ={1,3,6,1,4,1,2076,10,1,17};
UINT4 FutOspfSpfDelay [ ] ={1,3,6,1,4,1,2076,10,1,18};
UINT4 FutOspfRestartSupport [ ] ={1,3,6,1,4,1,2076,10,1,19};
UINT4 FutOspfRestartInterval [ ] ={1,3,6,1,4,1,2076,10,1,20};
UINT4 FutOspfRestartStrictLsaChecking [ ] ={1,3,6,1,4,1,2076,10,1,21};
UINT4 FutOspfRestartStatus [ ] ={1,3,6,1,4,1,2076,10,1,22};
UINT4 FutOspfRestartAge [ ] ={1,3,6,1,4,1,2076,10,1,23};
UINT4 FutOspfRestartExitReason [ ] ={1,3,6,1,4,1,2076,10,1,24};
UINT4 FutOspfHelperSupport [ ] ={1,3,6,1,4,1,2076,10,1,25};
UINT4 FutOspfHelperGraceTimeLimit [ ] ={1,3,6,1,4,1,2076,10,1,26};
UINT4 FutOspfRestartAckState [ ] ={1,3,6,1,4,1,2076,10,1,27};
UINT4 FutOspfGraceLsaRetransmitCount [ ] ={1,3,6,1,4,1,2076,10,1,28};
UINT4 FutOspfRestartReason [ ] ={1,3,6,1,4,1,2076,10,1,29};
UINT4 FutOspfRTStaggeringInterval [ ] ={1,3,6,1,4,1,2076,10,1,30};
UINT4 FutOspfRTStaggeringStatus [ ] ={1,3,6,1,4,1,2076,10,1,31};
UINT4 FutOspfHotStandbyAdminStatus [ ] ={1,3,6,1,4,1,2076,10,1,32};
UINT4 FutOspfHotStandbyState [ ] ={1,3,6,1,4,1,2076,10,1,33};
UINT4 FutOspfDynamicBulkUpdStatus [ ] ={1,3,6,1,4,1,2076,10,1,34};
UINT4 FutOspfStanbyHelloSyncCount [ ] ={1,3,6,1,4,1,2076,10,1,35};
UINT4 FutOspfStanbyLsaSyncCount [ ] ={1,3,6,1,4,1,2076,10,1,36};
UINT4 FutOspfExtTraceLevel [ ] ={1,3,6,1,4,1,2076,10,1,37};
UINT4 FutospfRouterIdPermanence [ ] ={1,3,6,1,4,1,2076,10,1,38};
UINT4 FutOspfBfdStatus [ ] ={1,3,6,1,4,1,2076,10,1,39};
UINT4 FutOspfBfdAllIfState [ ] ={1,3,6,1,4,1,2076,10,1,40};
UINT4 FutOspfAreaId [ ] ={1,3,6,1,4,1,2076,10,2,1,1};
UINT4 FutOspfAreaIfCount [ ] ={1,3,6,1,4,1,2076,10,2,1,2};
UINT4 FutOspfAreaNetCount [ ] ={1,3,6,1,4,1,2076,10,2,1,3};
UINT4 FutOspfAreaRtrCount [ ] ={1,3,6,1,4,1,2076,10,2,1,4};
UINT4 FutOspfAreaNSSATranslatorRole [ ] ={1,3,6,1,4,1,2076,10,2,1,5};
UINT4 FutOspfAreaNSSATranslatorState [ ] ={1,3,6,1,4,1,2076,10,2,1,6};
UINT4 FutOspfAreaNSSATranslatorStabilityInterval [ ] ={1,3,6,1,4,1,2076,10,2,1,7};
UINT4 FutOspfAreaNSSATranslatorEvents [ ] ={1,3,6,1,4,1,2076,10,2,1,8};
UINT4 FutOspfAreaDfInfOriginate [ ] ={1,3,6,1,4,1,2076,10,2,1,9};
UINT4 FutOspfHostIpAddress [ ] ={1,3,6,1,4,1,2076,10,3,1,1};
UINT4 FutOspfHostTOS [ ] ={1,3,6,1,4,1,2076,10,3,1,2};
UINT4 FutOspfHostRouteIfIndex [ ] ={1,3,6,1,4,1,2076,10,3,1,3};
UINT4 FutOspfIfIpAddress [ ] ={1,3,6,1,4,1,2076,10,4,1,1};
UINT4 FutOspfAddressLessIf [ ] ={1,3,6,1,4,1,2076,10,4,1,2};
UINT4 FutOspfIfOperState [ ] ={1,3,6,1,4,1,2076,10,4,1,3};
UINT4 FutOspfIfPassive [ ] ={1,3,6,1,4,1,2076,10,4,1,4};
UINT4 FutOspfIfNbrCount [ ] ={1,3,6,1,4,1,2076,10,4,1,5};
UINT4 FutOspfIfAdjCount [ ] ={1,3,6,1,4,1,2076,10,4,1,6};
UINT4 FutOspfIfHelloRcvd [ ] ={1,3,6,1,4,1,2076,10,4,1,7};
UINT4 FutOspfIfHelloTxed [ ] ={1,3,6,1,4,1,2076,10,4,1,8};
UINT4 FutOspfIfHelloDisd [ ] ={1,3,6,1,4,1,2076,10,4,1,9};
UINT4 FutOspfIfDdpRcvd [ ] ={1,3,6,1,4,1,2076,10,4,1,10};
UINT4 FutOspfIfDdpTxed [ ] ={1,3,6,1,4,1,2076,10,4,1,11};
UINT4 FutOspfIfDdpDisd [ ] ={1,3,6,1,4,1,2076,10,4,1,12};
UINT4 FutOspfIfLrqRcvd [ ] ={1,3,6,1,4,1,2076,10,4,1,13};
UINT4 FutOspfIfLrqTxed [ ] ={1,3,6,1,4,1,2076,10,4,1,14};
UINT4 FutOspfIfLrqDisd [ ] ={1,3,6,1,4,1,2076,10,4,1,15};
UINT4 FutOspfIfLsuRcvd [ ] ={1,3,6,1,4,1,2076,10,4,1,16};
UINT4 FutOspfIfLsuTxed [ ] ={1,3,6,1,4,1,2076,10,4,1,17};
UINT4 FutOspfIfLsuDisd [ ] ={1,3,6,1,4,1,2076,10,4,1,18};
UINT4 FutOspfIfLakRcvd [ ] ={1,3,6,1,4,1,2076,10,4,1,19};
UINT4 FutOspfIfLakTxed [ ] ={1,3,6,1,4,1,2076,10,4,1,20};
UINT4 FutOspfIfLakDisd [ ] ={1,3,6,1,4,1,2076,10,4,1,21};
UINT4 FutOspfIfBfdState [ ] ={1,3,6,1,4,1,2076,10,4,1,22};
UINT4 FutOspfIfMD5AuthIpAddress [ ] ={1,3,6,1,4,1,2076,10,5,1,1};
UINT4 FutOspfIfMD5AuthAddressLessIf [ ] ={1,3,6,1,4,1,2076,10,5,1,2};
UINT4 FutOspfIfMD5AuthKeyId [ ] ={1,3,6,1,4,1,2076,10,5,1,3};
UINT4 FutOspfIfMD5AuthKey [ ] ={1,3,6,1,4,1,2076,10,5,1,4};
UINT4 FutOspfIfMD5AuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,10,5,1,5};
UINT4 FutOspfIfMD5AuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,10,5,1,6};
UINT4 FutOspfIfMD5AuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,10,5,1,7};
UINT4 FutOspfIfMD5AuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,10,5,1,8};
UINT4 FutOspfIfMD5AuthKeyStatus [ ] ={1,3,6,1,4,1,2076,10,5,1,9};
UINT4 FutOspfVirtIfMD5AuthAreaId [ ] ={1,3,6,1,4,1,2076,10,6,1,1};
UINT4 FutOspfVirtIfMD5AuthNeighbor [ ] ={1,3,6,1,4,1,2076,10,6,1,2};
UINT4 FutOspfVirtIfMD5AuthKeyId [ ] ={1,3,6,1,4,1,2076,10,6,1,3};
UINT4 FutOspfVirtIfMD5AuthKey [ ] ={1,3,6,1,4,1,2076,10,6,1,4};
UINT4 FutOspfVirtIfMD5AuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,10,6,1,5};
UINT4 FutOspfVirtIfMD5AuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,10,6,1,6};
UINT4 FutOspfVirtIfMD5AuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,10,6,1,7};
UINT4 FutOspfVirtIfMD5AuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,10,6,1,8};
UINT4 FutOspfVirtIfMD5AuthKeyStatus [ ] ={1,3,6,1,4,1,2076,10,6,1,9};
UINT4 FutOspfNbrIpAddr [ ] ={1,3,6,1,4,1,2076,10,7,1,1};
UINT4 FutOspfNbrAddressLessIndex [ ] ={1,3,6,1,4,1,2076,10,7,1,2};
UINT4 FutOspfNbrDBSummaryQLen [ ] ={1,3,6,1,4,1,2076,10,7,1,3};
UINT4 FutOspfNbrLSReqQLen [ ] ={1,3,6,1,4,1,2076,10,7,1,4};
UINT4 FutOspfNbrRestartHelperStatus [ ] ={1,3,6,1,4,1,2076,10,7,1,5};
UINT4 FutOspfNbrRestartHelperAge [ ] ={1,3,6,1,4,1,2076,10,7,1,6};
UINT4 FutOspfNbrRestartHelperExitReason [ ] ={1,3,6,1,4,1,2076,10,7,1,7};
UINT4 FutOspfNbrBfdState [ ] ={1,3,6,1,4,1,2076,10,7,1,8};
UINT4 FutOspfRouteIpAddr [ ] ={1,3,6,1,4,1,2076,10,8,1,1};
UINT4 FutOspfRouteIpAddrMask [ ] ={1,3,6,1,4,1,2076,10,8,1,2};
UINT4 FutOspfRouteIpTos [ ] ={1,3,6,1,4,1,2076,10,8,1,3};
UINT4 FutOspfRouteIpNextHop [ ] ={1,3,6,1,4,1,2076,10,8,1,4};
UINT4 FutOspfRouteType [ ] ={1,3,6,1,4,1,2076,10,8,1,5};
UINT4 FutOspfRouteAreaId [ ] ={1,3,6,1,4,1,2076,10,8,1,6};
UINT4 FutOspfRouteCost [ ] ={1,3,6,1,4,1,2076,10,8,1,7};
UINT4 FutOspfRouteType2Cost [ ] ={1,3,6,1,4,1,2076,10,8,1,8};
UINT4 FutOspfRouteInterfaceIndex [ ] ={1,3,6,1,4,1,2076,10,8,1,9};
UINT4 FutOspfPrimIpAddr [ ] ={1,3,6,1,4,1,2076,10,9,1,1};
UINT4 FutOspfPrimAddresslessIf [ ] ={1,3,6,1,4,1,2076,10,9,1,2};
UINT4 FutOspfSecIpAddr [ ] ={1,3,6,1,4,1,2076,10,9,1,3};
UINT4 FutOspfSecIpAddrMask [ ] ={1,3,6,1,4,1,2076,10,9,1,4};
UINT4 FutOspfSecIfStatus [ ] ={1,3,6,1,4,1,2076,10,9,1,5};
UINT4 FutOspfAreaAggregateAreaID [ ] ={1,3,6,1,4,1,2076,10,10,1,1};
UINT4 FutOspfAreaAggregateLsdbType [ ] ={1,3,6,1,4,1,2076,10,10,1,2};
UINT4 FutOspfAreaAggregateNet [ ] ={1,3,6,1,4,1,2076,10,10,1,3};
UINT4 FutOspfAreaAggregateMask [ ] ={1,3,6,1,4,1,2076,10,10,1,4};
UINT4 FutOspfAreaAggregateExternalTag [ ] ={1,3,6,1,4,1,2076,10,10,1,5};
UINT4 FutOspfAsExternalAggregationNet [ ] ={1,3,6,1,4,1,2076,10,11,1,1};
UINT4 FutOspfAsExternalAggregationMask [ ] ={1,3,6,1,4,1,2076,10,11,1,2};
UINT4 FutOspfAsExternalAggregationAreaId [ ] ={1,3,6,1,4,1,2076,10,11,1,3};
UINT4 FutOspfAsExternalAggregationEffect [ ] ={1,3,6,1,4,1,2076,10,11,1,4};
UINT4 FutOspfAsExternalAggregationTranslation [ ] ={1,3,6,1,4,1,2076,10,11,1,5};
UINT4 FutOspfAsExternalAggregationStatus [ ] ={1,3,6,1,4,1,2076,10,11,1,6};
UINT4 FutOspfOpaqueOption [ ] ={1,3,6,1,4,1,2076,10,12,1,1};
UINT4 FutOspfType11LsaCount [ ] ={1,3,6,1,4,1,2076,10,12,1,2};
UINT4 FutOspfType11LsaCksumSum [ ] ={1,3,6,1,4,1,2076,10,12,1,3};
UINT4 FutOspfAreaIDValid [ ] ={1,3,6,1,4,1,2076,10,12,1,4};
UINT4 FutOspfOpaqueType9LsaCount [ ] ={1,3,6,1,4,1,2076,10,12,2,1,1};
UINT4 FutOspfOpaqueType9LsaCksumSum [ ] ={1,3,6,1,4,1,2076,10,12,2,1,2};
UINT4 FutOspfType9LsdbIfIpAddress [ ] ={1,3,6,1,4,1,2076,10,12,3,1,1};
UINT4 FutOspfType9LsdbOpaqueType [ ] ={1,3,6,1,4,1,2076,10,12,3,1,2};
UINT4 FutOspfType9LsdbLsid [ ] ={1,3,6,1,4,1,2076,10,12,3,1,3};
UINT4 FutOspfType9LsdbRouterId [ ] ={1,3,6,1,4,1,2076,10,12,3,1,4};
UINT4 FutOspfType9LsdbSequence [ ] ={1,3,6,1,4,1,2076,10,12,3,1,5};
UINT4 FutOspfType9LsdbAge [ ] ={1,3,6,1,4,1,2076,10,12,3,1,6};
UINT4 FutOspfType9LsdbChecksum [ ] ={1,3,6,1,4,1,2076,10,12,3,1,7};
UINT4 FutOspfType9LsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,10,12,3,1,8};
UINT4 FutOspfType11LsdbOpaqueType [ ] ={1,3,6,1,4,1,2076,10,12,4,1,1};
UINT4 FutOspfType11LsdbLsid [ ] ={1,3,6,1,4,1,2076,10,12,4,1,2};
UINT4 FutOspfType11LsdbRouterId [ ] ={1,3,6,1,4,1,2076,10,12,4,1,3};
UINT4 FutOspfType11LsdbSequence [ ] ={1,3,6,1,4,1,2076,10,12,4,1,4};
UINT4 FutOspfType11LsdbAge [ ] ={1,3,6,1,4,1,2076,10,12,4,1,5};
UINT4 FutOspfType11LsdbChecksum [ ] ={1,3,6,1,4,1,2076,10,12,4,1,6};
UINT4 FutOspfType11LsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,10,12,4,1,7};
UINT4 FutOspfAppInfoDbAppid [ ] ={1,3,6,1,4,1,2076,10,12,5,1,1};
UINT4 FutOspfAppInfoDbOpaqueType [ ] ={1,3,6,1,4,1,2076,10,12,5,1,2};
UINT4 FutOspfAppInfoDbLsaTypesSupported [ ] ={1,3,6,1,4,1,2076,10,12,5,1,3};
UINT4 FutOspfAppInfoDbType9Gen [ ] ={1,3,6,1,4,1,2076,10,12,5,1,4};
UINT4 FutOspfAppInfoDbType9Rcvd [ ] ={1,3,6,1,4,1,2076,10,12,5,1,5};
UINT4 FutOspfAppInfoDbType10Gen [ ] ={1,3,6,1,4,1,2076,10,12,5,1,6};
UINT4 FutOspfAppInfoDbType10Rcvd [ ] ={1,3,6,1,4,1,2076,10,12,5,1,7};
UINT4 FutOspfAppInfoDbType11Gen [ ] ={1,3,6,1,4,1,2076,10,12,5,1,8};
UINT4 FutOspfAppInfoDbType11Rcvd [ ] ={1,3,6,1,4,1,2076,10,12,5,1,9};
UINT4 FutOspfRRDStatus [ ] ={1,3,6,1,4,1,2076,10,13,1,1};
UINT4 FutOspfRRDSrcProtoMaskEnable [ ] ={1,3,6,1,4,1,2076,10,13,1,2};
UINT4 FutOspfRRDSrcProtoMaskDisable [ ] ={1,3,6,1,4,1,2076,10,13,1,3};
UINT4 FutOspfRRDRouteMapEnable [ ] ={1,3,6,1,4,1,2076,10,13,1,4};
UINT4 FutOspfRRDRouteDest [ ] ={1,3,6,1,4,1,2076,10,13,2,1,1};
UINT4 FutOspfRRDRouteMask [ ] ={1,3,6,1,4,1,2076,10,13,2,1,2};
UINT4 FutOspfRRDRouteMetric [ ] ={1,3,6,1,4,1,2076,10,13,2,1,3};
UINT4 FutOspfRRDRouteMetricType [ ] ={1,3,6,1,4,1,2076,10,13,2,1,4};
UINT4 FutOspfRRDRouteTagType [ ] ={1,3,6,1,4,1,2076,10,13,2,1,5};
UINT4 FutOspfRRDRouteTag [ ] ={1,3,6,1,4,1,2076,10,13,2,1,6};
UINT4 FutOspfRRDRouteStatus [ ] ={1,3,6,1,4,1,2076,10,13,2,1,7};
UINT4 FutOspfVirtNbrRestartHelperStatus [ ] ={1,3,6,1,4,1,2076,10,14,1,1};
UINT4 FutOspfVirtNbrRestartHelperAge [ ] ={1,3,6,1,4,1,2076,10,14,1,2};
UINT4 FutOspfVirtNbrRestartHelperExitReason [ ] ={1,3,6,1,4,1,2076,10,14,1,3};
UINT4 FutOspfDistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,10,15,1,1,1};
UINT4 FutOspfDistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,10,15,1,1,3};
UINT4 FutOspfDistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,10,15,1,1,4};
UINT4 FutOspfDistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,10,15,1,1,5};
UINT4 FutOspfPreferenceValue [ ] ={1,3,6,1,4,1,2076,10,16,1};
UINT4 FutOspfIfAuthIpAddress [ ] ={1,3,6,1,4,1,2076,10,17,1,1};
UINT4 FutOspfIfAuthAddressLessIf [ ] ={1,3,6,1,4,1,2076,10,17,1,2};
UINT4 FutOspfIfAuthKeyId [ ] ={1,3,6,1,4,1,2076,10,17,1,3};
UINT4 FutOspfIfAuthKey [ ] ={1,3,6,1,4,1,2076,10,17,1,4};
UINT4 FutOspfIfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,10,17,1,5};
UINT4 FutOspfIfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,10,17,1,6};
UINT4 FutOspfIfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,10,17,1,7};
UINT4 FutOspfIfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,10,17,1,8};
UINT4 FutOspfIfAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,10,17,1,9};
UINT4 FutOspfVirtIfAuthAreaId [ ] ={1,3,6,1,4,1,2076,10,18,1,1};
UINT4 FutOspfVirtIfAuthNeighbor [ ] ={1,3,6,1,4,1,2076,10,18,1,2};
UINT4 FutOspfVirtIfAuthKeyId [ ] ={1,3,6,1,4,1,2076,10,18,1,3};
UINT4 FutOspfVirtIfAuthKey [ ] ={1,3,6,1,4,1,2076,10,18,1,4};
UINT4 FutOspfVirtIfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,10,18,1,5};
UINT4 FutOspfVirtIfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,10,18,1,6};
UINT4 FutOspfVirtIfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,10,18,1,7};
UINT4 FutOspfVirtIfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,10,18,1,8};
UINT4 FutOspfVirtIfAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,10,18,1,9};
UINT4 FutOspfIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,10,19,1,1};
UINT4 FutOspfVirtIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,10,20,1,1};




tMbDbEntry fsospfMibEntry[]= {

{{10,FutOspfOverFlowState}, NULL, FutOspfOverFlowStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "2"},

{{10,FutOspfPktsRcvd}, NULL, FutOspfPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfPktsTxed}, NULL, FutOspfPktsTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfPktsDisd}, NULL, FutOspfPktsDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfRFC1583Compatibility}, NULL, FutOspfRFC1583CompatibilityGet, FutOspfRFC1583CompatibilitySet, FutOspfRFC1583CompatibilityTest, FutOspfRFC1583CompatibilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfMaxAreas}, NULL, FutOspfMaxAreasGet, FutOspfMaxAreasSet, FutOspfMaxAreasTest, FutOspfMaxAreasDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{10,FutOspfMaxLSAperArea}, NULL, FutOspfMaxLSAperAreaGet, FutOspfMaxLSAperAreaSet, FutOspfMaxLSAperAreaTest, FutOspfMaxLSAperAreaDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "128"},

{{10,FutOspfMaxExtLSAs}, NULL, FutOspfMaxExtLSAsGet, FutOspfMaxExtLSAsSet, FutOspfMaxExtLSAsTest, FutOspfMaxExtLSAsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "512"},

{{10,FutOspfMaxSelfOrgLSAs}, NULL, FutOspfMaxSelfOrgLSAsGet, FutOspfMaxSelfOrgLSAsSet, FutOspfMaxSelfOrgLSAsTest, FutOspfMaxSelfOrgLSAsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "128"},

{{10,FutOspfMaxRoutes}, NULL, FutOspfMaxRoutesGet, FutOspfMaxRoutesSet, FutOspfMaxRoutesTest, FutOspfMaxRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},

{{10,FutOspfMaxLsaSize}, NULL, FutOspfMaxLsaSizeGet, FutOspfMaxLsaSizeSet, FutOspfMaxLsaSizeTest, FutOspfMaxLsaSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "128"},

{{10,FutOspfTraceLevel}, NULL, FutOspfTraceLevelGet, FutOspfTraceLevelSet, FutOspfTraceLevelTest, FutOspfTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2048"},

{{10,FutOspfMinLsaInterval}, NULL, FutOspfMinLsaIntervalGet, FutOspfMinLsaIntervalSet, FutOspfMinLsaIntervalTest, FutOspfMinLsaIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FutOspfABRType}, NULL, FutOspfABRTypeGet, FutOspfABRTypeSet, FutOspfABRTypeTest, FutOspfABRTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfNssaAsbrDefRtTrans}, NULL, FutOspfNssaAsbrDefRtTransGet, FutOspfNssaAsbrDefRtTransSet, FutOspfNssaAsbrDefRtTransTest, FutOspfNssaAsbrDefRtTransDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfDefaultPassiveInterface}, NULL, FutOspfDefaultPassiveInterfaceGet, FutOspfDefaultPassiveInterfaceSet, FutOspfDefaultPassiveInterfaceTest, FutOspfDefaultPassiveInterfaceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfSpfHoldtime}, NULL, FutOspfSpfHoldtimeGet, FutOspfSpfHoldtimeSet, FutOspfSpfHoldtimeTest, FutOspfSpfHoldtimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{10,FutOspfSpfDelay}, NULL, FutOspfSpfDelayGet, FutOspfSpfDelaySet, FutOspfSpfDelayTest, FutOspfSpfDelayDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfRestartSupport}, NULL, FutOspfRestartSupportGet, FutOspfRestartSupportSet, FutOspfRestartSupportTest, FutOspfRestartSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfRestartInterval}, NULL, FutOspfRestartIntervalGet, FutOspfRestartIntervalSet, FutOspfRestartIntervalTest, FutOspfRestartIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "120"},

{{10,FutOspfRestartStrictLsaChecking}, NULL, FutOspfRestartStrictLsaCheckingGet, FutOspfRestartStrictLsaCheckingSet, FutOspfRestartStrictLsaCheckingTest, FutOspfRestartStrictLsaCheckingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfRestartStatus}, NULL, FutOspfRestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfRestartAge}, NULL, FutOspfRestartAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfRestartExitReason}, NULL, FutOspfRestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfHelperSupport}, NULL, FutOspfHelperSupportGet, FutOspfHelperSupportSet, FutOspfHelperSupportTest, FutOspfHelperSupportDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FutOspfHelperGraceTimeLimit}, NULL, FutOspfHelperGraceTimeLimitGet, FutOspfHelperGraceTimeLimitSet, FutOspfHelperGraceTimeLimitTest, FutOspfHelperGraceTimeLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FutOspfRestartAckState}, NULL, FutOspfRestartAckStateGet, FutOspfRestartAckStateSet, FutOspfRestartAckStateTest, FutOspfRestartAckStateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfGraceLsaRetransmitCount}, NULL, FutOspfGraceLsaRetransmitCountGet, FutOspfGraceLsaRetransmitCountSet, FutOspfGraceLsaRetransmitCountTest, FutOspfGraceLsaRetransmitCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfRestartReason}, NULL, FutOspfRestartReasonGet, FutOspfRestartReasonSet, FutOspfRestartReasonTest, FutOspfRestartReasonDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FutOspfRTStaggeringInterval}, NULL, FutOspfRTStaggeringIntervalGet, FutOspfRTStaggeringIntervalSet, FutOspfRTStaggeringIntervalTest, FutOspfRTStaggeringIntervalDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, NULL, 0, 0, 0, "10000"},

{{10,FutOspfRTStaggeringStatus}, NULL, FutOspfRTStaggeringStatusGet, FutOspfRTStaggeringStatusSet, FutOspfRTStaggeringStatusTest, FutOspfRTStaggeringStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfHotStandbyAdminStatus}, NULL, FutOspfHotStandbyAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfHotStandbyState}, NULL, FutOspfHotStandbyStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfDynamicBulkUpdStatus}, NULL, FutOspfDynamicBulkUpdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfStanbyHelloSyncCount}, NULL, FutOspfStanbyHelloSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfStanbyLsaSyncCount}, NULL, FutOspfStanbyLsaSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfExtTraceLevel}, NULL, FutOspfExtTraceLevelGet, FutOspfExtTraceLevelSet, FutOspfExtTraceLevelTest, FutOspfExtTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FutospfRouterIdPermanence}, NULL, FutospfRouterIdPermanenceGet, FutospfRouterIdPermanenceSet, FutospfRouterIdPermanenceTest, FutospfRouterIdPermanenceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfBfdStatus}, NULL, FutOspfBfdStatusGet, FutOspfBfdStatusSet, FutOspfBfdStatusTest, FutOspfBfdStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfBfdAllIfState}, NULL, FutOspfBfdAllIfStateGet, FutOspfBfdAllIfStateSet, FutOspfBfdAllIfStateTest, FutOspfBfdAllIfStateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FutOspfAreaId}, GetNextIndexFutOspfAreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfAreaIfCount}, GetNextIndexFutOspfAreaTable, FutOspfAreaIfCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfAreaNetCount}, GetNextIndexFutOspfAreaTable, FutOspfAreaNetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfAreaRtrCount}, GetNextIndexFutOspfAreaTable, FutOspfAreaRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfAreaNSSATranslatorRole}, GetNextIndexFutOspfAreaTable, FutOspfAreaNSSATranslatorRoleGet, FutOspfAreaNSSATranslatorRoleSet, FutOspfAreaNSSATranslatorRoleTest, FutOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfAreaTableINDEX, 1, 0, 0, "2"},

{{11,FutOspfAreaNSSATranslatorState}, GetNextIndexFutOspfAreaTable, FutOspfAreaNSSATranslatorStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfAreaTableINDEX, 1, 0, 0, "3"},

{{11,FutOspfAreaNSSATranslatorStabilityInterval}, GetNextIndexFutOspfAreaTable, FutOspfAreaNSSATranslatorStabilityIntervalGet, FutOspfAreaNSSATranslatorStabilityIntervalSet, FutOspfAreaNSSATranslatorStabilityIntervalTest, FutOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfAreaTableINDEX, 1, 0, 0, "40"},

{{11,FutOspfAreaNSSATranslatorEvents}, GetNextIndexFutOspfAreaTable, FutOspfAreaNSSATranslatorEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfAreaDfInfOriginate}, GetNextIndexFutOspfAreaTable, FutOspfAreaDfInfOriginateGet, FutOspfAreaDfInfOriginateSet, FutOspfAreaDfInfOriginateTest, FutOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfAreaTableINDEX, 1, 0, 0, "2"},

{{11,FutOspfHostIpAddress}, GetNextIndexFutOspfHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfHostTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfHostTOS}, GetNextIndexFutOspfHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfHostTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfHostRouteIfIndex}, GetNextIndexFutOspfHostTable, FutOspfHostRouteIfIndexGet, FutOspfHostRouteIfIndexSet, FutOspfHostRouteIfIndexTest, FutOspfHostTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfHostTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfIpAddress}, GetNextIndexFutOspfIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfAddressLessIf}, GetNextIndexFutOspfIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfOperState}, GetNextIndexFutOspfIfTable, FutOspfIfOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfPassive}, GetNextIndexFutOspfIfTable, FutOspfIfPassiveGet, FutOspfIfPassiveSet, FutOspfIfPassiveTest, FutOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfNbrCount}, GetNextIndexFutOspfIfTable, FutOspfIfNbrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfAdjCount}, GetNextIndexFutOspfIfTable, FutOspfIfAdjCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfHelloRcvd}, GetNextIndexFutOspfIfTable, FutOspfIfHelloRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfHelloTxed}, GetNextIndexFutOspfIfTable, FutOspfIfHelloTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfHelloDisd}, GetNextIndexFutOspfIfTable, FutOspfIfHelloDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfDdpRcvd}, GetNextIndexFutOspfIfTable, FutOspfIfDdpRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfDdpTxed}, GetNextIndexFutOspfIfTable, FutOspfIfDdpTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfDdpDisd}, GetNextIndexFutOspfIfTable, FutOspfIfDdpDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLrqRcvd}, GetNextIndexFutOspfIfTable, FutOspfIfLrqRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLrqTxed}, GetNextIndexFutOspfIfTable, FutOspfIfLrqTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLrqDisd}, GetNextIndexFutOspfIfTable, FutOspfIfLrqDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLsuRcvd}, GetNextIndexFutOspfIfTable, FutOspfIfLsuRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLsuTxed}, GetNextIndexFutOspfIfTable, FutOspfIfLsuTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLsuDisd}, GetNextIndexFutOspfIfTable, FutOspfIfLsuDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLakRcvd}, GetNextIndexFutOspfIfTable, FutOspfIfLakRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLakTxed}, GetNextIndexFutOspfIfTable, FutOspfIfLakTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfLakDisd}, GetNextIndexFutOspfIfTable, FutOspfIfLakDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfBfdState}, GetNextIndexFutOspfIfTable, FutOspfIfBfdStateGet, FutOspfIfBfdStateSet, FutOspfIfBfdStateTest, FutOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfIfMD5AuthIpAddress}, GetNextIndexFutOspfIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfMD5AuthAddressLessIf}, GetNextIndexFutOspfIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfMD5AuthKeyId}, GetNextIndexFutOspfIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfMD5AuthKey}, GetNextIndexFutOspfIfMD5AuthTable, FutOspfIfMD5AuthKeyGet, FutOspfIfMD5AuthKeySet, FutOspfIfMD5AuthKeyTest, FutOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfMD5AuthKeyStartAccept}, GetNextIndexFutOspfIfMD5AuthTable, FutOspfIfMD5AuthKeyStartAcceptGet, FutOspfIfMD5AuthKeyStartAcceptSet, FutOspfIfMD5AuthKeyStartAcceptTest, FutOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfMD5AuthKeyStartGenerate}, GetNextIndexFutOspfIfMD5AuthTable, FutOspfIfMD5AuthKeyStartGenerateGet, FutOspfIfMD5AuthKeyStartGenerateSet, FutOspfIfMD5AuthKeyStartGenerateTest, FutOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfMD5AuthKeyStopGenerate}, GetNextIndexFutOspfIfMD5AuthTable, FutOspfIfMD5AuthKeyStopGenerateGet, FutOspfIfMD5AuthKeyStopGenerateSet, FutOspfIfMD5AuthKeyStopGenerateTest, FutOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, "-1"},

{{11,FutOspfIfMD5AuthKeyStopAccept}, GetNextIndexFutOspfIfMD5AuthTable, FutOspfIfMD5AuthKeyStopAcceptGet, FutOspfIfMD5AuthKeyStopAcceptSet, FutOspfIfMD5AuthKeyStopAcceptTest, FutOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, "-1"},

{{11,FutOspfIfMD5AuthKeyStatus}, GetNextIndexFutOspfIfMD5AuthTable, FutOspfIfMD5AuthKeyStatusGet, FutOspfIfMD5AuthKeyStatusSet, FutOspfIfMD5AuthKeyStatusTest, FutOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthAreaId}, GetNextIndexFutOspfVirtIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthNeighbor}, GetNextIndexFutOspfVirtIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthKeyId}, GetNextIndexFutOspfVirtIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthKey}, GetNextIndexFutOspfVirtIfMD5AuthTable, FutOspfVirtIfMD5AuthKeyGet, FutOspfVirtIfMD5AuthKeySet, FutOspfVirtIfMD5AuthKeyTest, FutOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthKeyStartAccept}, GetNextIndexFutOspfVirtIfMD5AuthTable, FutOspfVirtIfMD5AuthKeyStartAcceptGet, FutOspfVirtIfMD5AuthKeyStartAcceptSet, FutOspfVirtIfMD5AuthKeyStartAcceptTest, FutOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthKeyStartGenerate}, GetNextIndexFutOspfVirtIfMD5AuthTable, FutOspfVirtIfMD5AuthKeyStartGenerateGet, FutOspfVirtIfMD5AuthKeyStartGenerateSet, FutOspfVirtIfMD5AuthKeyStartGenerateTest, FutOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfMD5AuthKeyStopGenerate}, GetNextIndexFutOspfVirtIfMD5AuthTable, FutOspfVirtIfMD5AuthKeyStopGenerateGet, FutOspfVirtIfMD5AuthKeyStopGenerateSet, FutOspfVirtIfMD5AuthKeyStopGenerateTest, FutOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, "-1"},

{{11,FutOspfVirtIfMD5AuthKeyStopAccept}, GetNextIndexFutOspfVirtIfMD5AuthTable, FutOspfVirtIfMD5AuthKeyStopAcceptGet, FutOspfVirtIfMD5AuthKeyStopAcceptSet, FutOspfVirtIfMD5AuthKeyStopAcceptTest, FutOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, "-1"},

{{11,FutOspfVirtIfMD5AuthKeyStatus}, GetNextIndexFutOspfVirtIfMD5AuthTable, FutOspfVirtIfMD5AuthKeyStatusGet, FutOspfVirtIfMD5AuthKeyStatusSet, FutOspfVirtIfMD5AuthKeyStatusTest, FutOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfVirtIfMD5AuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfNbrIpAddr}, GetNextIndexFutOspfNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrAddressLessIndex}, GetNextIndexFutOspfNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrDBSummaryQLen}, GetNextIndexFutOspfNbrTable, FutOspfNbrDBSummaryQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrLSReqQLen}, GetNextIndexFutOspfNbrTable, FutOspfNbrLSReqQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrRestartHelperStatus}, GetNextIndexFutOspfNbrTable, FutOspfNbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrRestartHelperAge}, GetNextIndexFutOspfNbrTable, FutOspfNbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrRestartHelperExitReason}, GetNextIndexFutOspfNbrTable, FutOspfNbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfNbrBfdState}, GetNextIndexFutOspfNbrTable, FutOspfNbrBfdStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfRouteIpAddr}, GetNextIndexFutOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteIpAddrMask}, GetNextIndexFutOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteIpTos}, GetNextIndexFutOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteIpNextHop}, GetNextIndexFutOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteType}, GetNextIndexFutOspfRoutingTable, FutOspfRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteAreaId}, GetNextIndexFutOspfRoutingTable, FutOspfRouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteCost}, GetNextIndexFutOspfRoutingTable, FutOspfRouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteType2Cost}, GetNextIndexFutOspfRoutingTable, FutOspfRouteType2CostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfRouteInterfaceIndex}, GetNextIndexFutOspfRoutingTable, FutOspfRouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfRoutingTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfPrimIpAddr}, GetNextIndexFutOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfSecIfTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfPrimAddresslessIf}, GetNextIndexFutOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfSecIfTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfSecIpAddr}, GetNextIndexFutOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfSecIfTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfSecIpAddrMask}, GetNextIndexFutOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfSecIfTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfSecIfStatus}, GetNextIndexFutOspfSecIfTable, FutOspfSecIfStatusGet, FutOspfSecIfStatusSet, FutOspfSecIfStatusTest, FutOspfSecIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfSecIfTableINDEX, 4, 0, 1, NULL},

{{11,FutOspfAreaAggregateAreaID}, GetNextIndexFutOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfAreaAggregateLsdbType}, GetNextIndexFutOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfAreaAggregateNet}, GetNextIndexFutOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfAreaAggregateMask}, GetNextIndexFutOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfAreaAggregateExternalTag}, GetNextIndexFutOspfAreaAggregateTable, FutOspfAreaAggregateExternalTagGet, FutOspfAreaAggregateExternalTagSet, FutOspfAreaAggregateExternalTagTest, FutOspfAreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfAsExternalAggregationNet}, GetNextIndexFutOspfAsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAsExternalAggregationTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfAsExternalAggregationMask}, GetNextIndexFutOspfAsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAsExternalAggregationTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfAsExternalAggregationAreaId}, GetNextIndexFutOspfAsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfAsExternalAggregationTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfAsExternalAggregationEffect}, GetNextIndexFutOspfAsExternalAggregationTable, FutOspfAsExternalAggregationEffectGet, FutOspfAsExternalAggregationEffectSet, FutOspfAsExternalAggregationEffectTest, FutOspfAsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfAsExternalAggregationTableINDEX, 3, 0, 0, "1"},

{{11,FutOspfAsExternalAggregationTranslation}, GetNextIndexFutOspfAsExternalAggregationTable, FutOspfAsExternalAggregationTranslationGet, FutOspfAsExternalAggregationTranslationSet, FutOspfAsExternalAggregationTranslationTest, FutOspfAsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfAsExternalAggregationTableINDEX, 3, 0, 0, "1"},

{{11,FutOspfAsExternalAggregationStatus}, GetNextIndexFutOspfAsExternalAggregationTable, FutOspfAsExternalAggregationStatusGet, FutOspfAsExternalAggregationStatusSet, FutOspfAsExternalAggregationStatusTest, FutOspfAsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfAsExternalAggregationTableINDEX, 3, 0, 1, NULL},

{{11,FutOspfOpaqueOption}, NULL, FutOspfOpaqueOptionGet, FutOspfOpaqueOptionSet, FutOspfOpaqueOptionTest, FutOspfOpaqueOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FutOspfType11LsaCount}, NULL, FutOspfType11LsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FutOspfType11LsaCksumSum}, NULL, FutOspfType11LsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FutOspfAreaIDValid}, NULL, FutOspfAreaIDValidGet, FutOspfAreaIDValidSet, FutOspfAreaIDValidTest, FutOspfAreaIDValidDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FutOspfOpaqueType9LsaCount}, GetNextIndexFutOspfOpaqueInterfaceTable, FutOspfOpaqueType9LsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfOpaqueInterfaceTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfOpaqueType9LsaCksumSum}, GetNextIndexFutOspfOpaqueInterfaceTable, FutOspfOpaqueType9LsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfOpaqueInterfaceTableINDEX, 2, 0, 0, "0"},

{{12,FutOspfType9LsdbIfIpAddress}, GetNextIndexFutOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbOpaqueType}, GetNextIndexFutOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbLsid}, GetNextIndexFutOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbRouterId}, GetNextIndexFutOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbSequence}, GetNextIndexFutOspfType9LsdbTable, FutOspfType9LsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbAge}, GetNextIndexFutOspfType9LsdbTable, FutOspfType9LsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbChecksum}, GetNextIndexFutOspfType9LsdbTable, FutOspfType9LsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType9LsdbAdvertisement}, GetNextIndexFutOspfType9LsdbTable, FutOspfType9LsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FutOspfType9LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FutOspfType11LsdbOpaqueType}, GetNextIndexFutOspfType11LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfType11LsdbLsid}, GetNextIndexFutOspfType11LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfType11LsdbRouterId}, GetNextIndexFutOspfType11LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfType11LsdbSequence}, GetNextIndexFutOspfType11LsdbTable, FutOspfType11LsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfType11LsdbAge}, GetNextIndexFutOspfType11LsdbTable, FutOspfType11LsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfType11LsdbChecksum}, GetNextIndexFutOspfType11LsdbTable, FutOspfType11LsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfType11LsdbAdvertisement}, GetNextIndexFutOspfType11LsdbTable, FutOspfType11LsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FutOspfType11LsdbTableINDEX, 3, 0, 0, NULL},

{{12,FutOspfAppInfoDbAppid}, GetNextIndexFutOspfAppInfoDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbOpaqueType}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbOpaqueTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbLsaTypesSupported}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbLsaTypesSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbType9Gen}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbType9GenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbType9Rcvd}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbType9RcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbType10Gen}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbType10GenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbType10Rcvd}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbType10RcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbType11Gen}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbType11GenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{12,FutOspfAppInfoDbType11Rcvd}, GetNextIndexFutOspfAppInfoDbTable, FutOspfAppInfoDbType11RcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfAppInfoDbTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfRRDStatus}, NULL, FutOspfRRDStatusGet, FutOspfRRDStatusSet, FutOspfRRDStatusTest, FutOspfRRDStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FutOspfRRDSrcProtoMaskEnable}, NULL, FutOspfRRDSrcProtoMaskEnableGet, FutOspfRRDSrcProtoMaskEnableSet, FutOspfRRDSrcProtoMaskEnableTest, FutOspfRRDSrcProtoMaskEnableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FutOspfRRDSrcProtoMaskDisable}, NULL, FutOspfRRDSrcProtoMaskDisableGet, FutOspfRRDSrcProtoMaskDisableSet, FutOspfRRDSrcProtoMaskDisableTest, FutOspfRRDSrcProtoMaskDisableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "57478"},

{{11,FutOspfRRDRouteMapEnable}, NULL, FutOspfRRDRouteMapEnableGet, FutOspfRRDRouteMapEnableSet, FutOspfRRDRouteMapEnableTest, FutOspfRRDRouteMapEnableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FutOspfRRDRouteDest}, GetNextIndexFutOspfRRDRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfRRDRouteConfigTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfRRDRouteMask}, GetNextIndexFutOspfRRDRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfRRDRouteConfigTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfRRDRouteMetric}, GetNextIndexFutOspfRRDRouteConfigTable, FutOspfRRDRouteMetricGet, FutOspfRRDRouteMetricSet, FutOspfRRDRouteMetricTest, FutOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfRRDRouteConfigTableINDEX, 2, 0, 0, "10"},

{{12,FutOspfRRDRouteMetricType}, GetNextIndexFutOspfRRDRouteConfigTable, FutOspfRRDRouteMetricTypeGet, FutOspfRRDRouteMetricTypeSet, FutOspfRRDRouteMetricTypeTest, FutOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfRRDRouteConfigTableINDEX, 2, 0, 0, "2"},

{{12,FutOspfRRDRouteTagType}, GetNextIndexFutOspfRRDRouteConfigTable, FutOspfRRDRouteTagTypeGet, FutOspfRRDRouteTagTypeSet, FutOspfRRDRouteTagTypeTest, FutOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfRRDRouteConfigTableINDEX, 2, 0, 0, "1"},

{{12,FutOspfRRDRouteTag}, GetNextIndexFutOspfRRDRouteConfigTable, FutOspfRRDRouteTagGet, FutOspfRRDRouteTagSet, FutOspfRRDRouteTagTest, FutOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutOspfRRDRouteConfigTableINDEX, 2, 0, 0, "0"},

{{12,FutOspfRRDRouteStatus}, GetNextIndexFutOspfRRDRouteConfigTable, FutOspfRRDRouteStatusGet, FutOspfRRDRouteStatusSet, FutOspfRRDRouteStatusTest, FutOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfRRDRouteConfigTableINDEX, 2, 0, 1, NULL},

{{11,FutOspfVirtNbrRestartHelperStatus}, GetNextIndexFutOspfVirtNbrTable, FutOspfVirtNbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfVirtNbrRestartHelperAge}, GetNextIndexFutOspfVirtNbrTable, FutOspfVirtNbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FutOspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfVirtNbrRestartHelperExitReason}, GetNextIndexFutOspfVirtNbrTable, FutOspfVirtNbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfDistInOutRouteMapName}, GetNextIndexFutOspfDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfDistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfDistInOutRouteMapType}, GetNextIndexFutOspfDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfDistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfDistInOutRouteMapValue}, GetNextIndexFutOspfDistInOutRouteMapTable, FutOspfDistInOutRouteMapValueGet, FutOspfDistInOutRouteMapValueSet, FutOspfDistInOutRouteMapValueTest, FutOspfDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfDistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfDistInOutRouteMapRowStatus}, GetNextIndexFutOspfDistInOutRouteMapTable, FutOspfDistInOutRouteMapRowStatusGet, FutOspfDistInOutRouteMapRowStatusSet, FutOspfDistInOutRouteMapRowStatusTest, FutOspfDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfDistInOutRouteMapTableINDEX, 2, 0, 1, NULL},

{{10,FutOspfPreferenceValue}, NULL, FutOspfPreferenceValueGet, FutOspfPreferenceValueSet, FutOspfPreferenceValueTest, FutOspfPreferenceValueDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FutOspfIfAuthIpAddress}, GetNextIndexFutOspfIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthAddressLessIf}, GetNextIndexFutOspfIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKeyId}, GetNextIndexFutOspfIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKey}, GetNextIndexFutOspfIfAuthTable, FutOspfIfAuthKeyGet, FutOspfIfAuthKeySet, FutOspfIfAuthKeyTest, FutOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKeyStartAccept}, GetNextIndexFutOspfIfAuthTable, FutOspfIfAuthKeyStartAcceptGet, FutOspfIfAuthKeyStartAcceptSet, FutOspfIfAuthKeyStartAcceptTest, FutOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKeyStartGenerate}, GetNextIndexFutOspfIfAuthTable, FutOspfIfAuthKeyStartGenerateGet, FutOspfIfAuthKeyStartGenerateSet, FutOspfIfAuthKeyStartGenerateTest, FutOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKeyStopGenerate}, GetNextIndexFutOspfIfAuthTable, FutOspfIfAuthKeyStopGenerateGet, FutOspfIfAuthKeyStopGenerateSet, FutOspfIfAuthKeyStopGenerateTest, FutOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKeyStopAccept}, GetNextIndexFutOspfIfAuthTable, FutOspfIfAuthKeyStopAcceptGet, FutOspfIfAuthKeyStopAcceptSet, FutOspfIfAuthKeyStopAcceptTest, FutOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfAuthKeyStatus}, GetNextIndexFutOspfIfAuthTable, FutOspfIfAuthKeyStatusGet, FutOspfIfAuthKeyStatusSet, FutOspfIfAuthKeyStatusTest, FutOspfIfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthAreaId}, GetNextIndexFutOspfVirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthNeighbor}, GetNextIndexFutOspfVirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKeyId}, GetNextIndexFutOspfVirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKey}, GetNextIndexFutOspfVirtIfAuthTable, FutOspfVirtIfAuthKeyGet, FutOspfVirtIfAuthKeySet, FutOspfVirtIfAuthKeyTest, FutOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKeyStartAccept}, GetNextIndexFutOspfVirtIfAuthTable, FutOspfVirtIfAuthKeyStartAcceptGet, FutOspfVirtIfAuthKeyStartAcceptSet, FutOspfVirtIfAuthKeyStartAcceptTest, FutOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKeyStartGenerate}, GetNextIndexFutOspfVirtIfAuthTable, FutOspfVirtIfAuthKeyStartGenerateGet, FutOspfVirtIfAuthKeyStartGenerateSet, FutOspfVirtIfAuthKeyStartGenerateTest, FutOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKeyStopGenerate}, GetNextIndexFutOspfVirtIfAuthTable, FutOspfVirtIfAuthKeyStopGenerateGet, FutOspfVirtIfAuthKeyStopGenerateSet, FutOspfVirtIfAuthKeyStopGenerateTest, FutOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKeyStopAccept}, GetNextIndexFutOspfVirtIfAuthTable, FutOspfVirtIfAuthKeyStopAcceptGet, FutOspfVirtIfAuthKeyStopAcceptSet, FutOspfVirtIfAuthKeyStopAcceptTest, FutOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfVirtIfAuthKeyStatus}, GetNextIndexFutOspfVirtIfAuthTable, FutOspfVirtIfAuthKeyStatusGet, FutOspfVirtIfAuthKeyStatusSet, FutOspfVirtIfAuthKeyStatusTest, FutOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfVirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfIfCryptoAuthType}, GetNextIndexFutOspfIfCryptoAuthTable, FutOspfIfCryptoAuthTypeGet, FutOspfIfCryptoAuthTypeSet, FutOspfIfCryptoAuthTypeTest, FutOspfIfCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfIfCryptoAuthTableINDEX, 2, 0, 0, NULL},
                                                                                                                                                                 {{11,FutOspfVirtIfCryptoAuthType}, GetNextIndexFutOspfVirtIfCryptoAuthTable, FutOspfVirtIfCryptoAuthTypeGet, FutOspfVirtIfCryptoAuthTypeSet, FutOspfVirtIfCryptoAuthTypeTest, FutOspfVirtIfCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfVirtIfCryptoAuthTableINDEX, 2, 0, 0, NULL},
                                                                                                                                                             };

tMibData fsospfEntry = { 194, fsospfMibEntry };

#endif /* _FSOSPFDB_H */

