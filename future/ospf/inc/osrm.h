/************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved     */
/* $Id: osrm.h,v 1.8 2015/07/09 13:14:02 siva Exp $       */
/*                                                          */
/*  FILE NAME             : osrm.h                         */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : OSPF                            */
/*  LANGUAGE              : C                               */
/*  DESCRIPTION           : This file contains definitions  */
/*                          related to High Availability    */ 
/************************************************************/
/*                                                          */
/*  Change History                                          */
/*  Version               :                                 */
/*  Date(DD/MM/YYYY)      :                                 */
/*  Modified by           :                                 */
/*  Description of change :                                 */
/************************************************************/

#ifndef _OSRED_H
#define _OSRED_H

/* Following are the macros defined for High Avalability */
enum {
       OSPF_RED_BULK_UPDT_REQ_MSG = RM_BULK_UPDT_REQ_MSG,
       
       OSPF_RED_BULK_UPDATE_MSG = RM_BULK_UPDATE_MSG,
       
       OSPF_RED_BULK_UPDT_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
                                                                           
                                        
       OSPF_RED_SYNC_IFSTATE_MSG = 4,   /*  This is the value used to 
                             *  identify the If state for 
                                     *  sync up  in RM  Message */ 
       OSPF_RED_SYNC_HELLO_PDU_MSG = 5,   /*  This is the value used to 
                             *  identify the Hello PDU for 
                                     *  sync up  in RM  Message */ 

       OSPF_RED_SYNC_NBR_STATE_MSG  = 6, /*This is the value used to                           
                                     *  identify the Nbr state for 
                                       *  sync up  in RM Message */ 

       OSPF_RED_SYNC_NBR_STATE_CHNG_MSG = 7, /* This is the value used 
                                              * to identify the neighbor 
                                              * state change event in 
                                              * RM message */

       OSPF_RED_SYNC_LSU_MSG = 8,          /* This is the value used to 
                                            * identify the LSU syncup in 
                                            * RM message */

       OSPF_RED_SYNC_LSACK_MSG = 9,        /* This is the value used to 
                                           * identify the LSAck syncup 
                                           * in RM message */
       OSPF_RED_SYNC_EXT_RT_MSG = 10,        /* This is the added to sync 
                                           * the redistributed routes */
       OSPF_RED_SYNC_DBOVERFLOW_TMR_MSG = 11,  /* This is the added to sync 
                                             * the database overflow timer value */
       OSPF_RED_SYNC_DEF_CXT_MSG = 12,       /* If the default context is deleted 
                                                in active node, this message is sent to 
                                                standby as part of bulk update processing.
                                                On receiving the new message in standby, context deletion 
                                                for default context is invoked*/

       
      OSPF_RED_SYNC_MD5_SEQ_NO_MSG = 13       /*This is added to sync the md5 seqno to standby */
      
};

enum {

         OSPF_RED_SYNC_NBR_STATE_TWO_WAY_RECVD = 1,/* This is the MACRO used 
                                                    * to identify the neighbor 
                                                    * state change from init to 
                                                    * two-way */

         OSPF_RED_SYNC_NBR_STATE_RESTART_ADJ = 2,  /* This is the macro used to 
                                                    * identify the neighbor 
                                                    * state full to extstart
                                                    * state */

         OSPF_RED_SYNC_NBR_STATE_FULL = 3,          /* This is the macro used to 
                                                    * identify neighbor state 
                                                    * is FULL */

         OSPF_RED_SYNC_NBR_STATE_DOWN = 4,         /* This is the macro used to 
                                                    * identify neighbor state
                                                    * down */

         OSPF_RED_SYNC_NBR_STATE_LL_DOWN = 5,       /* This is the macro used to 
                                                    * identify neighbor state
                                                    * down.When lower layer is
                                                    * down */

         OSPF_RED_SYNC_NBR_STATE_HELPER = 6,        /* This is the macro used to 
                                                    * identify the neighbor is 
                                                    * in helper mode*/
         OSPF_RED_SYNC_MAX_STATE = 7    
};


/* Maximum UINT4 value */
#define OSPF_MAX_TIME                   0xFFFFFFFF

#define OSPF_RED_SYN_HELLO_HDR_SIZE     11+( MAX_IP_ADDR_LEN * 3) 
                                        /* MAX_IPV4_ADDR_LEN(NbrId) + 
                                         * MAX_IP_ADDR_LEN (Src IP) + 
                                         * 4 (Addressless If) + 
                                         * MAX_IP_ADDR_LEN (If IP) + 
                                         * 4 (Context Id) + 
                                         * 1 (Wait Tmr Expiry) +
                                         * 2 (Hello Pkt Length)
                                         */
#define OSPF_RED_BULK_SYN_NBR_FIXED_SIZE  21 
                                        /* 1 byte Sub bulk type as nbr info +
                                         * 1 byte (neighbor state) +
                                         * 4 byte (NbrIpAddr) + 
                                         * 4 byte (IfAddrLessIf) + 
                                         * 4 byte(Context Id) + 
                                         * 4 byte (Interface IpAddr)+
                                         * 1(Neighbor helper status) +
                                         * 1 Network Type +
                                         * 1 nbr options 
                                         */
#define OSPF_RED_SYN_IF_SIZE  28 /* 4 (NetIpIfInfo.u4ContextId) + 
                                            * 4 (NetIpIfInfo.u4Addr) + 
                                            * 4 (NetIpIfInfo.u4NetMask) + 
                                            * 4 (NetIpIfInfo.u4IfIndex) + 
                                            * 4 (NetIpIfInfo.u4Oper) + 
                                            * 4 (NetIpIfInfo.u4Mtu)  +  
                                            * 4 (u4BitMap) */ 
#define OSPF_RED_SYN_NBR_STATE_SIZE      11 + (MAX_IP_ADDR_LEN * 2) 
                                         /* 4(Context Id) + 1 (event) +
                                          * 4 (IfAddreslessIf) + 
                                          * MAX_IP_ADDR_LEN (neighbor routerId)
                                          * + MAX_IP_ADDR_LEN(Interface IpAddr)
                                          * + 1 Network Type 
                                          * + 1 byte for neigbor options*/
#define OSPF_RED_SYNC_DB_TMR_MSG_SIZE  9 /*  1 (Message Type) +
                                                4 (ContextId)    +
                                                4 (Overflow tmer value) */
#define OSPF_RED_DYN_SYN_HDR_SIZE        3 /*  1 (Message Type) +
                                            2 (Message Length) */
#define OSPF_RED_SYN_IF_STATE_SIZE   46 /* 4 (ContextId) +
                                           1 (Network Type) +
                                           4 virtual link transit AreaId +
                                           4 virtual link end point IP  +
                                           4 (Interface IP Address)+
                                           4 (AddresslessIfIndex) +
                                           4 (Interface IP Address mask)+
                                           4 (oper status) +
                                           4 (BitMap indication from IP) +
                                           1 (Interface state) +
                                           4 (DR Address) +
                                           4 (BDR Address)
                                           4 (Interface MTU Size)
                                           */

#define OSPF_RED_BLK_SYN_IF_SIZE        16 /* 4(Context Id) + 4(Ipaddr)
                                            * + 4 (IfIndex) + 4 (flag )*/

#define OSPF_RED_DYN_SYN_SEQNO_SIZE     12 + (MAX_IP_ADDR_LEN)  /* 4(Context Id) + 4(u4AddressIf)
                                            * + MAX_IP_ADDR_LEN + 4 (u4CryptoSeqNo)*/
#define OSPF_RED_NBR_CNT_SIZE     4    
#define OSPF_RED_IFACE_CNT_SIZE     4    

#define OSPF_RED_BULK_SYNC           1 /* Indicates bulk sync */ 
#define OSPF_RED_DYN_SYNC           2 /* Indicates dynamic sync */ 

#define OSPF_IS_BULKUPDATE_INPROGRESS() \
 ((gOsRtr.osRedInfo.u4DynBulkUpdatStatus == OSPF_RED_BLKUPDT_INPROGRESS) ?\
             OSPF_TRUE : OSPF_FALSE)
            
#define OSPF_IS_STANDBY_UP() \
          ((gOsRtr.osRedInfo.u4OsRmState == OSPF_RED_ACTIVE_STANDBY_UP) \
           ? OSPF_TRUE : OSPF_FALSE)

#define OSPF_IS_NODE_ACTIVE()\
             ((gOsRtr.osRedInfo.u4OsRmState == OSPF_RED_ACTIVE_STANDBY_UP || \
               gOsRtr.osRedInfo.u4OsRmState == OSPF_RED_ACTIVE_STANDBY_DOWN) \
               ? OSPF_TRUE : OSPF_FALSE)

#define OSPF_TMR_RUNNING    0x1
#define OSPF_TMR_EXPD           0x2
            
#define  OSPF_RED_HELLO_MOD           1 
#define  OSPF_RED_NBR_MOD             2 
#define  OSPF_RED_VIRT_NBR_MOD        3 
#define  OSPF_RED_LSDB_MOD            4
#define  OSPF_RED_TMR_MOD             5
#define  OSPF_RED_MOD_COMPLETED       6 /*Set this value when all the modules
                                          bulk update is completed */  
#define  OSPF_RED_DEF_CXT_MOD         7

#define  OSPF_RED_BULK_UPD_REQ_MSG_LEN  3 /* Msg Type(1) + Msg Length */
#define  OSPF_RED_BULK_UPD_MSG_LEN      3 /* Msg Type(1) + Msg Length */
#define  OSPF_RED_BULK_TAIL_MSG_LEN     3 /* Msg Type(1) + Msg Length */
#define  OSPF_RED_MSG_HDR_SIZE          3 /* Msg Type(1) + Msg Length */
#define  OSPF_RED_BULK_LEN_OFFSET       1 /* Offset in the RM Message to 
                                             fill the RM Message length */

#define  OSPF_BULK_UPDATE_RELINQUISH_TIME 1 /* Time interval (in seconds) after which the bulk 
                                               update process will be relinquished */ 
#define  OSPF_RED_IFACE_MTU  RM_MAX_SYNC_PKT_LEN - (2 * OSPF_RED_MESSAGE_LENGTH_OFFSET) - RM_HDR_LENGTH
#define  OSPF_RED_MAX_LSA_PER_BULK_EVENT     15
#define  OSPF_RED_MAX_HELLO_PER_BULK_EVENT   10 /* used to set the max number 
                     * of hellos can be sent
                                                 * at a time */
#define OSPF_RED_MAX_IF_PER_BULK_EVENT       10 /* used to set the max number 
                     * of Interfaces can be sent 
                                          * at a time */

#define  OSPF_RED_MAX_NBR_PER_BULK_EVENT   20 /* used to set the max number of
                                                 * Nbr */
#define OSPF_RED_LSACK_RCVD            1
#define OSPF_RED_LSACK_NOT_RCVD        2

#define  OSPF_RED_MESSAGE_LENGTH_OFFSET    1

#define  OSPF_RED_DYN_LSU_FIXED_MSG_SIZE   OSPF_RED_MSG_HDR_SIZE +\
                                           sizeof(UINT4) + sizeof(tIPADDR) +\
                                           sizeof (UINT4) + sizeof (UINT4) 
                                             /* Header + Context Id + Nbr addr +
                                                Nbr AddrLess IfIndex +
                                                lsa length */
                                          

#define  OSPF_RED_DYN_ACK_MSG_SIZE         OSPF_RED_MSG_HDR_SIZE +\
                                           sizeof(UINT4) + sizeof (tLsaId) +\
                                           sizeof(tIPADDR) + sizeof (UINT4)      
                                          /* Header + Context Id + LSA ID + 
                                             LSA Scope.
                                          */       
#define  OSPF_RED_PROCESSED       1       
#define  OSPF_RED_DISCARDED       2                
#define  OSPF_RED_CONTINUE        3

#define OSPF_RED_MSG_NOT_CONSTRUCTED_NOT_SENT  0
#define OSPF_RED_MSG_CONSTRUCTED_NOT_SENT      1
#define OSPF_RED_MSG_CONSTRUCTED_AND_SENT      2

#define OSPF_RED_PUT_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define OSPF_RED_PUT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define OSPF_RED_PUT_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4Value); \
    u4Offset += 4; \
}

#define OSPF_RED_PUT_N_BYTE(pdest, psrc, u4Offset, u4Size) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define OSPF_RED_GET_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define OSPF_RED_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define OSPF_RED_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4Value); \
    u4Offset += 4; \
}

#define OSPF_RED_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
} 

#define OSPF_RED_BUF_MOVE_VALID_OFFSET(pBuf, Len) \
{ \
    CRU_BUF_Move_ValidOffset (pBuf, Len);\
}

#endif /* _OSRED_H */
