/********************************************************************
 *Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: fsstdmwr.h,v 1.5 2014/03/01 11:40:41 siva Exp $
 *
 *Description: This file has the handle routines for cli SET/GET
 *              destined for OSPF  Module
 *
 *
 ***********************************************************************/

#ifndef _FSSTDMWR_H
#define _FSSTDMWR_H
INT4 GetNextIndexFsMIStdOspfTrapTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSSTDM(VOID);
VOID UnRegisterFSSTDM(VOID);
INT4 FsMIStdOspfSetTrapGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfConfigErrorTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfPacketTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfPacketSrcGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfSetTrapSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfSetTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfTrapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSSTDMWR_H */
