/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ostmrif.h,v 1.11 2012/07/10 06:03:27 siva Exp $
 *
 * Description:This file contains definitions related to OSPF
 *             timers and timer action routines.
 *
 *******************************************************************/

#ifndef _OSTMRIF_H
#define _OSTMRIF_H


/* constants for timer types */
#define  HELLO_TIMER                  0  
#define  POLL_TIMER                   1  
#define  WAIT_TIMER                   2  
#define  INACTIVITY_TIMER             3  
#define  DD_INIT_RXMT_TIMER           4  
#define  DD_RXMT_TIMER                5  
#define  DD_LAST_PKT_LIFE_TIME_TIMER  6  
#define  LSA_REQ_RXMT_TIMER           7  
 
#define  LSA_NORMAL_AGING_TIMER       8  
#define  LSA_PREMATURE_AGING_TIMER    9  
#define  MIN_LSA_INTERVAL_TIMER       10 
#define  LSA_RXMT_TIMER               11 
#define  DEL_ACK_TIMER                12 

#define  RUN_RT_TIMER                 13 

#ifdef DEBUG_WANTED
#define  DUMP_TIMER                   14 
#endif
#define  EXIT_OVERFLOW_TIMER          15 
#define  DNA_LSA_SPL_AGING_TIMER      16 
#define  TRAP_LIMIT_TIMER             17        
#define NSSA_STBLTY_INTERVAL_TIMER 18 /* Nssa Stability Timer  */
#define VRF_SPF_TIMER                 19      /* VRF Spf timer */
/* Graceful restart related timers */
#define RESTART_GRACE_TIMER           20      /* Grace timer used to indicate
                                               * the restarting router that
                                               * grace period has been
                                               * expired */ 
#define HELPER_GRACE_TIMER            21
/* Distance timer is used to update distance 
 * value for all existing routes */
#define DISTANCE_TIMER                22      
#define MAX_TIMERS                    23

#ifdef RAWSOCK_WANTED
/* Sets the socket receive in non blocking mode */
#define OSPF_RAWSOCK_NON_BLOCK        16
#endif

#define  INVALID_TIMER     MAX_TIMERS 

/* This is to indicate the starting & stopping of dna_spl_aging timer */ 
#define  START  1 
#define  STOP   0 

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

/* constant specifying the percentage of jitter */
#define  OSPF_JITTER           5         
#define  OSPF_MAX_JITTER       25         
#define  OSPF_DIV_FACTOR       180         
#define  OSPF_MIN_TIMER_VAL    3600         
#define  OSPF_DISTANCE_TIME_INTERVAL  1
 
#define  OSPF_TIMER_EVENT  0x00000100 

/* type definitions */
typedef struct _TimerDesc {
    VOID                (*pTimerExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
} tTimerDesc;

/* The timer structure ; If required, this structure can be modified */

typedef struct _OspfTimer {
       tTmrAppTimer    timerNode;
       INT2            i2Status;
       UINT1           u1TimerId;
       UINT1           u1Rsvd;
} tOspfTimer;

#endif
