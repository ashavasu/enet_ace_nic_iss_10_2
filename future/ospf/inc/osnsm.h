/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osnsm.h,v 1.6 2007/02/01 15:00:38 iss Exp $
 *
 * Description:This file contains constants, macros, typdefinitions
 *             and static varible declaration specific to osnsm.c
 *
 *******************************************************************/

#ifndef _OSNSM_H
#define _OSNSM_H


/* NSM Action (NA) routine numbers */
#define  NA0           0  
#define  NA1           1  
#define  NA2           2  
#define  NA3           3  
#define  NA4           4  
#define  NA5           5  
#define  NA6           6  
#define  NA7           7  
#define  NA8           8  
#define  NA9           9  
#define  NA10          10 
#define  NA11          11 
#define  NA12          12 
#define  NA13          13 
#define  NA14          14 
#define  NA15          15 
#define  MAX_NSM_FUNC  16 

/* the neighbor state machine transition table */

static UINT1  au1_nsm_table[MAX_NBR_EVENT][MAX_NBR_STATE]  = { 
/*______________________________________________________________________________

             DOWN   ATTEMPT   INIT   2WAY   EXSTRT   EXCHNG   LOADING    FULL
________________________________________________________________________________
HELLO RCVD
*/         { NA10 ,  NA12   , NA11 , NA11 ,  NA11  ,  NA11  ,  NA11    , NA11 },
/*______________________________________________________________________________
START
*/         {  NA9 ,   NA0   ,  NA0 ,  NA0 ,   NA0  ,   NA0  ,   NA0    ,  NA0 },
/*______________________________________________________________________________
2WAY RCVD
*/         {  NA0 ,   NA0  ,   NA8  ,NA14  , NA14  ,  NA14  ,  NA14    , NA14 },
/*______________________________________________________________________________
NEG DONE
*/         {  NA0 ,   NA0  ,   NA0  , NA0  ,  NA7  ,   NA0  ,   NA0    ,  NA0 },
/*______________________________________________________________________________
EXCHANGE
DONE */    {  NA0 ,   NA0  ,   NA0  , NA0  ,  NA0  ,   NA6  ,   NA0    ,  NA0 },
/*______________________________________________________________________________
BAD LSREQ
*/         {  NA0 ,   NA0  ,   NA0  , NA0  ,  NA0  ,   NA3  ,   NA3    ,  NA3 },
/*______________________________________________________________________________
LDNG DONE
*/         {  NA0 ,   NA0  ,   NA0  , NA0  ,  NA0  ,   NA0  ,  NA13    ,  NA0 },
/*______________________________________________________________________________
ADJ OK?
*/         {  NA0 ,   NA0  ,   NA0  , NA5  ,  NA4  ,   NA4  ,   NA4    ,  NA4 },
/*______________________________________________________________________________
SEQ NUM
MISMATCH */{  NA0 ,   NA0  ,   NA0  , NA0  ,  NA0  ,   NA3  ,   NA3    , NA3  },
/*______________________________________________________________________________
1-WAY
*/         {  NA0 ,   NA0  ,  NA14  , NA2  ,  NA2  ,   NA2  ,   NA2    ,  NA2 },
/*______________________________________________________________________________
KILL NBR
*/         {  NA1 ,   NA1  ,   NA1  , NA1  ,  NA1  ,   NA1  ,   NA1    ,  NA1 },
/*______________________________________________________________________________
INACT
TIMER */   {  NA1 ,   NA1  ,   NA1  , NA1  ,  NA1  ,   NA1  ,   NA1    ,  NA1 },
/*______________________________________________________________________________
LLDOWN
*/         { NA15 ,  NA15  ,  NA15  , NA15 , NA15  ,   NA15  ,  NA15    , NA15 }
/*______________________________________________________________________________
*/
                            } ;

/* type of the functions which perform the action in NBRSM */
typedef  void (*tNsmFunc)(tNeighbor *);

/* array of pointers to functions performing actions during NBRSM transitions */

tNsmFunc a_nsm_func[MAX_NSM_FUNC] = {
                             /* NA0 */           NsmInvalid,
                             /* NA1 */           NsmDown,
                             /* NA2 */           Nsm1wayRcvd,
                             /* NA3 */           NsmRestartAdj,
                             /* NA4 */           NsmCheckAdj,
                             /* NA5 */           NsmProcessAdj,
                             /* NA6 */           NsmExchgDone,
                             /* NA7 */           NsmNegDone,
                             /* NA8 */           Nsm2wayRcvd,
                             /* NA9 */           NsmStart,
                             /* NA10*/           NsmStartInactTimer,
                             /* NA11*/           NsmRestartInactTimer,
                             /* NA12*/           NsmAttHelloRcvd,    
                             /* NA13*/           NsmLdngDone,
                             /* NA14*/           NsmNoAction,
                             /* NA15*/           NsmLlDown
                                            };

#endif 
