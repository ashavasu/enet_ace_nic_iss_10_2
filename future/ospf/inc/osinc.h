/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osinc.h,v 1.31 2015/12/30 13:42:52 siva Exp $
 *
 * Description:This file contains the list of include files
 *
 *******************************************************************/
#ifndef _OSINC_H
#define _OSINC_H

#include "ospfcfg.h" 
#include "lr.h"
#include "ip.h"
#include "vcm.h"
#include "redblack.h"
#include "trie.h"
#include "arMD5_inc.h"
#include "arMD5_api.h"
#include "ospf.h"
#include "ososix.h"
#include "osport.h"
#include "ossock.h"
#include "cli.h"
#include "fssyslog.h"
#include "bfd.h"

/* OSPF include files */
#include "osdefn.h"
#include "oscfgvar.h"

#include "ostmrif.h"
#include "osbufif.h"

#include "rmap.h"
#include "rmgr.h"
#include "osrm.h"
#include "ostdfs.h"
#include "osmacs.h"

#include "ososif.h"
#include "ostask.h"
#include "osfssnmp.h"
#include "osextn.h"

#include "osappif.h"

#include "osprot.h"
#include "osipprot.h"
#include "osdebug.h"
#include "osfsip.h"
#include "oscfgvar.h"
#include "osglob.h"
#include "rtm.h"
#include "icch.h"

#ifdef FUTURE_SNMP_WANTED
#include "snmcport.h"
#include "snmctdfs.h"
#endif

#ifndef FUTURE_SNMP_WANTED
#include "fssnmp.h"
#endif
#include "rmgr.h"
#include "snmccons.h"
#include "oslow.h"
#include "fsmioslw.h"
#include "fsmistlw.h"
#include "fsosmilw.h"
#include "fsstdmlw.h"
#include "iss.h"
#include "csr.h"
#include "osrtstg.h"

#include "fsosplow.h"
#include "fsospfwr.h"
#include "fsostewr.h"
#include "stdospwr.h"
#include "stdostwr.h"

#include "fsmioswr.h"
#include "fsmistwr.h"
#include "fsosmiwr.h"
#include "fsstdmwr.h"
#include "ossz.h"
#include "arHmac_api.h"
#ifdef NPAPI_WANTED
#include "ospfnpwr.h"
#endif /* NPAPI_WANTED */

#endif
