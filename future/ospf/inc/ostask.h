/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ostask.h,v 1.10 2012/08/09 08:57:07 siva Exp $
 *
 * Description:This file contains definitions for constants
 *             and types related to interface with IP.
 *
 *******************************************************************/
#ifndef _OSIPIF_H
#define _OSIPIF_H


/*
 * Bit Mask for circuit attributes
 * which will be passed by Ip to OSPF
 * These are in synchronous with those defined 
 * at IP level.
 * This has to be taken care when
 * interfacing with a third party IP
 */

#ifndef OPER_STATE_MASK
#define  OPER_STATE_MASK  0x00000001 
#endif

#ifndef IFACE_DELETED_MASK
#define  IFACE_DELETED_MASK  0x00000020 
#endif

#ifndef IFACE_MTU_MASK
#define  IFACE_MTU_MASK  0x00000040 
#endif
 
#ifndef IFACE_SPEED_MASK
#define  IFACE_SPEED_MASK  0x00000080 
#endif
#endif /* for IP_WANTED */


#define OSPF_IP_PKT_MOVE_TO_DATA(pBuf, u1HLen) \
       { \
        CRU_BUF_Move_ValidOffset (pBuf, u1HLen);\
       }
#define  OSPF_IP_PKT_GET_SRC(pBuf,u4_Src)\
         OSPF_CRU_BMC_GET_4_BYTE (pBuf,IP_PKT_OFF_SRC,u4_Src)


/* 
 * Include the IP header files provided by the IP module 
 */

/* IP header precedence field should be set to Internetwork Control for OSPF 
   packets - RFC 791
   Precedence bits in Type of Service field in IP Header should be set to 110
   which indicates Internetwork Control packet.
   Remaining bits should be set to 0.
   So the Hex value of the Type of Servive for OSPF packet will become 0xc0 */
#define  OSPF_PKT_TOS  0xc0 

/* Segmentation of OSPF packets should be avoided */
#define  SEG_NOT_ALLOWED  0x00 

/* The protocol field in IP header should be set to 89 */
#define  FS_OSPF_PROTO  89 

/* OSPF packets which are not sent over virtual links travel only one hop */
#define  TTL_ONE  1 

/* OSPF packets which are sent over virtual links travel more than one hop */
#define  VIRTUAL_LINK_TTL  5                          

#define  IP_OSPF_IF_EVENT  TMO1_TASK_QUE_00_ENQ_EVENT 

#define  IP_RT_DIRECT      3                          
#define  IP_RT_INDIRECT    4

#define OSPF_TASK_NAME     ((const UINT1 *)"OSPF")       /* OSPF Task Name */
