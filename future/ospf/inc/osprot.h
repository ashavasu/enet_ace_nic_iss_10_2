/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osprot.h,v 1.73 2017/09/21 13:48:45 siva Exp $
 *
 * Description: This file contains function prototypes.
 *
 *******************************************************************/

#ifndef _OSPROT_H
#define _OSPROT_H

#include  "rtm.h"
#include  "osrm.h"

PUBLIC VOID CpuRelinquishHello (VOID);
 
PUBLIC INT4 FlashRemoveFile PROTO ((CONST CHR1 *));
PUBLIC  INT4  FlashFileExists PROTO ((CONST CHR1 * FileName));
/************************** osagd.c ***************************************/

PUBLIC VOID AgdFlushOut       PROTO((tLsaInfo *pLsaInfo));
PUBLIC INT4 AgdCheckChksum    PROTO((tLsaInfo *pLsaInfo));

/************************** osarea.c ***************************************/
PUBLIC tArea* AreaCreateInCxt      PROTO((tOspfCxt * pOspfCxt,
                                          tAreaId *pAreaId));
PUBLIC tArea* AreaCreateBackBoneAreaInCxt
                                PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID AreaParamChange   PROTO((tArea *pArea));
PUBLIC INT4 AreaDelete         PROTO((tArea *pArea));
PUBLIC INT4 AreaSendSummaryStatusChange PROTO((tArea *pArea));
PUBLIC UINT1 AreaDeleteNetSummaryLsas PROTO((tArea *pArea));
PUBLIC VOID AreaDeleteAllInCxt  PROTO((tOspfCxt * OspfCxt));
PUBLIC INT1 AreaCreateAddrRange PROTO((tArea *pArea, tIPADDR *pIpAddr, 
                                       tIPADDR *pIpAddrMask, UINT1 u1LsdbType,
                                       tROWSTATUS bStatus, INT1 *pReturnIndex));
PUBLIC INT4  AreaActiveAddrRange PROTO((tArea * pArea, UINT1 u1Index));
PUBLIC INT4  AreaSetAggrEffect PROTO ((tArea * pArea, UINT1 u1Index, 
              UINT1 u1AggrEffect));
PUBLIC INT4 AreaDeleteAddrRange 
                                PROTO((tArea* pArea, UINT1 u1Index));
PUBLIC INT4 AreaSetAddrRangeTag
                                PROTO((tArea* pArea, UINT1 u1Index));

PUBLIC INT4 AreaAddToAreasLst
                                PROTO((tArea *pArea));
PUBLIC INT4 AreaChangeAreaType PROTO ((tArea * pArea, INT4 i4AreaType));
PUBLIC VOID AreaReInitialize PROTO ((tArea * pArea));
PUBLIC VOID AreaSetaggFlag PROTO ((tArea * pArea));
PUBLIC VOID AreaGenerateAggLsa PROTO ((tArea * pArea));
PUBLIC VOID AreaResetAggFlag PROTO ((tArea * pArea, UINT1 u1Flag));
PUBLIC VOID AreaFlushLsaFallingInAddrRange 
                                     PROTO ((tArea * pArea, UINT1 u1Index));

PUBLIC VOID TransAreaFlushLsaFallingInAddrRange   PROTO ((tArea * pLstArea));

PUBLIC VOID AreaFlushAggLsaInTransitArea PROTO ((tArea * pLstArea));
PUBLIC VOID AreaAsExtRtAggrTrieDelete PROTO ((tArea * pArea));
                                
PUBLIC UINT1 OspfIsValidIpAddress PROTO ((UINT4 u4IpAddress));
PUBLIC UINT4 UtilOspfGetIfIndex PROTO ((UINT4 u4OspfCxtId, UINT4 u4Address,
     INT4 i4AddresslessIf, UINT4 *pu4IfIndex));

PUBLIC VOID AreaResetAggrEntries (tArea * pArea);

#ifdef DEBUG_WANTED
/************************** osdbg.c ***************************************/

PUBLIC VOID DbgPrintTimer     PROTO((tTmrAppTimer *pAppTimer));
PUBLIC VOID DbgPrintAreasInCxt PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgPrintAllIfsInCxt   PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgPrintIfsInAreaInCxt      
                                PROTO((tOspfCxt * pOspfCxt, tAreaId areaId));
PUBLIC VOID DbgDisplayIfsInArea    
                                PROTO((tArea *pArea));
PUBLIC UINT4 DbgCountAdj      PROTO((tInterface *pInterface));
PUBLIC VOID DbgPrintAllNbrsInCxt  PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgPrintIfInCxt        PROTO((tOspfCxt * pOspfCxt,
                                          tIPADDR pIfIpAddr, 
                                       UINT4 u4AddrlessIf));
PUBLIC VOID DbgPrintNbrInCxt       PROTO((tOspfCxt * pOspfCxt,
                                          tIPADDR *pNbrIpAddr, 
                                       UINT4 u4AddrlessIf));
PUBLIC VOID DbgPrintNbrsInIfInCxt      
                                PROTO((tOspfCxt * pOspfCxt,
                                       tIPADDR pIfIpAddr,
                                       UINT4 u4AddrlessIf));
PUBLIC VOID DbgPrintAllLsasInCxt  PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgPrintLsasInAreaInCxt     
                                PROTO((tOspfCxt * pOspfCxt,
                                       tAreaId *pAreaId));
PUBLIC VOID DbgPrintLsaInCxt       PROTO((tOspfCxt * pOspfCxt,
                                          tAreaId pAreaId, UINT1 u1LsaType, 
                                        tLINKSTATEID pLinkStateId, 
                                        tRouterId pAdvRtrId));
PUBLIC VOID DbgPrintOspfRtInCxt   PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgDisplayLsasInArea   
                                PROTO((tArea  *pArea));
PUBLIC VOID DbgPrintRtEntry  PROTO((tRtEntry *pRtEntry));
PUBLIC VOID DbgDumpOspfRtInCxt    PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgDumpRtEntry   PROTO((tRtEntry *pRtEntry));
PUBLIC VOID DbgPrintLsaInfoInCxt  PROTO((tOspfCxt * pOspfCxt,
                                         tAreaId pAreaId, UINT1 u1LsaType, 
                                        tLINKSTATEID pLinkStateId, 
                                        tRouterId pAdvRtrId));
PUBLIC VOID DbgDisplayLsaInfo       
                                PROTO((tLsaInfo *pLsaInfo));
PUBLIC VOID DbgPrintDbSummaryLstInCxt   
                                PROTO((tOspfCxt * pOspfCxt,
                                       tIPADDR *pNbrIpAddr, 
                                       UINT4 u4AddrlessIf));
PUBLIC VOID DbgPrintLsaReqLstInCxt      
                                PROTO((tOspfCxt * pOspfCxt,
                                       tIPADDR *pNbrIpAddr, 
                                       UINT4 u4AddrlessIf));
PUBLIC VOID DbgPrintLsaRxmtLstInCxt     
                                PROTO((tOspfCxt * pOspfCxt,
                                       tIPADDR *pNbrIpAddr, 
                                       UINT4 u4AddrlessIf));
PUBLIC VOID DbgDumpBuf        PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       UINT2 u2Len,
                                       UINT1 u1Type, UINT1 u1Flag));

PUBLIC VOID DbgDumpLsa        PROTO((UINT1 *pLsa, UINT2 u2Len));
PUBLIC VOID DbgDumpLsHeader  PROTO((UINT1 *pLsHeader));
PUBLIC VOID DbgPrintAddrRangeInCxt       
                                PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgShowRouteInCxt      PROTO((tOspfCxt * pOspfCxt,
                                          tIPADDR *pDestIpAddr ));
PUBLIC VOID DbgDumpAllInCxt        PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID DbgNotifyTrap     PROTO((UINT1 u1TrapId, void *p_trap_info));
PUBLIC VOID DbgPrntTestCaseNum 
                                PROTO((UINT1 au1TstCasNum[50],
                                       UINT1 *begEndInd));
PUBLIC VOID DbgDeleteLsaInCxt  PROTO(( tOspfCxt * pOspfCxt,
                                       tAreaId areaId, UINT1 u1LsaType,
                                        tLINKSTATEID linkStateId,
                                        tRouterId advRtrId));
PUBLIC VOID DbgConfDeleteAllExtRoutesInCxt  PROTO((tOspfCxt * pOspfCxt));


/*-- Packet Dumping Specific definations  --*/

PUBLIC VOID DbgOspfDumpPktData  PROTO((UINT4 u4Trace, UINT4 u4TrcType, 
                        UINT1 Direction, tCRU_BUF_CHAIN_HEADER *pBuf));
PUBLIC VOID DbgOspfPktAnalyser  PROTO((UINT1 Direction, UINT1 DumpLevel,
                                tCRU_BUF_CHAIN_HEADER *pBuf));
PUBLIC VOID DbgOspfHelloDump    PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT1 DumpLevel));
PUBLIC VOID DbgOspfDdpDump  PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT1 DumpLevel));
PUBLIC VOID DbgOspfLSRequestDump  PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT1 DumpLevel));
PUBLIC VOID DbgOspfLSUpdateDump PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT1 DumpLevel));
PUBLIC VOID DbgOspfLSAckDump    PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT1 DumpLevel));
PUBLIC VOID DbgOspfLSAdvtDump   PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT1 u1Hdr));
PUBLIC UINT1 * DbgOspfOptionsDump  PROTO((UINT1 u1Options, 
                        CONST CHR1 *pu1Pad, UINT1 *pu1String));
PUBLIC UINT1 * DbgOspfLSTypeString PROTO((UINT4 u4LStype, UINT1 *pu1String));
PUBLIC UINT2 DbgOspfRouterLinkTuplesDump PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                        CHR1 *pu1LogBuf, INT4 i4DataLen, UINT2 u2OffSet));
PUBLIC UINT1 * DbgOspfRouterLinkTypeDump   PROTO((UINT1 u1Lnktype,
                                                    UINT1 *pu1String));
PUBLIC UINT1 * DbgOspfFormatTime PROTO((UINT4 u4TimeSec, UINT1 *pu1String));
PUBLIC VOID DbgOspfUtlTrcPrint PROTO((CONST CHR1 *pu1Msg));
PUBLIC VOID DbgPrintOspfRtTable PROTO ((tOspfRt *pOspfRt));

#endif
PUBLIC UINT1 * OspfTOSToString  PROTO((UINT1 u1Tos, UINT1 *pu1String));
/************************** osddp.c ***************************************/

PUBLIC VOID DdpRcvDdp         PROTO((tCRU_BUF_CHAIN_HEADER *pDdPkt,
                                       UINT2 u2Len, tNeighbor *pNbr));
PUBLIC UINT1 DdpBuildSummary  PROTO((tNeighbor *pNbr));
PUBLIC VOID DdpClearSummary   PROTO((VOID *pAgr));
PUBLIC VOID DdpRxmtDdp        PROTO((tNeighbor *pNbr));
PUBLIC VOID DdpSendDdp        PROTO((tNeighbor *pNbr ));


/************************** osextrt.c ***************************************/
PUBLIC tExtRoute *  ExtrtAddInCxt PROTO ((tOspfCxt * pOspfCxt,
                                     tIPADDR ipNetNum,
                                     tIPADDRMASK ipAddrMask,
                                     INT1 i1Tos,
                                     UINT4 u4Metric,
                                     UINT1 u1MetricType,
                                     UINT4 u4ExtTag,
                                     tIPADDR fwdAddr,
                                     UINT2 u4FwdIfIndex,
                                     tIPADDR fwdNextHop, UINT2 u2SrcProto, 
         UINT4 u4RowStatus, UINT1 u1Level));
PUBLIC VOID RrdConfRtInfoDeleteAllInCxt     PROTO ((tOspfCxt * pOspfCxt));

PUBLIC INT4 ExtrtActivateInCxt      PROTO((tOspfCxt * pOspfCxt,
                                           tExtRoute* pExtRoute));
PUBLIC UINT1 ExtrtInactivateInCxt    PROTO((tOspfCxt * pOspfCxt, tExtRoute* pExtRoute));
PUBLIC VOID ExtrtParamChangeInCxt  PROTO((tOspfCxt * pOspfCxt, tExtRoute* pExtRoute));
PUBLIC INT4 ExtrtDeleteInCxt   PROTO((tOspfCxt * pOspfCxt,
                                      tExtRoute* pExtRoute));
PUBLIC VOID ExtrtDeleteFromTrieInCxt PROTO((tOspfCxt * pOspfCxt,
                                            tExtRoute * pExtRoute));
PUBLIC VOID ExtrtDeleteAllInCxt    PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID ExtrtAddStaticAllInCxt PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID ExtrtForceDeleteAllInCxt PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID ExtrtLsaPolicyHandlerInCxt PROTO((tOspfCxt *pOspfCxt));

PUBLIC INT4 ExtRouteTblInitInCxt PROTO((tOspfCxt * pOspfCxt));

/************************** oshost.c ***************************************/
PUBLIC tHost* HostAddInCxt PROTO ((tOspfCxt * pOspfCxt, tIPADDR * pHostAddr,
                                   UINT4 u4FwdIfIndex));
PUBLIC VOID HostIfmtrcPrmChange   
                                PROTO((tArea* pArea));
PUBLIC INT4 HostDelete         PROTO((tHost* pHost));
PUBLIC VOID HostDeleteAllInCxt     PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT1 HostCheckHostStatus     
                                PROTO((tHost* pHost));


/*****************************  oshp.c  *************************************/

PUBLIC VOID HpSendHello       PROTO(( tInterface *pInterface,
                                        tNeighbor *pNbr, 
                                        UINT1 u1TimerId ));
PUBLIC tNeighbor *HpSearchNbrLst   
                                PROTO(( tIPADDR *pIpAddr, 
                                        tInterface *pInterface));
PUBLIC VOID HpElectDesgRtr   PROTO(( tInterface *pInterface));
PUBLIC INT4 HpRcvHello        PROTO(( tCRU_BUF_CHAIN_HEADER *pHelloPkt, 
                                        UINT2 pktLen, 
                                        tInterface *pInterface, 
                                        tIPADDR *pSrcIpAddr));

/************************** osif.c ****************************************/
PUBLIC tInterface *IfCreateInCxt  PROTO((tOspfCxt * pOspfCxt,
                                        tIPADDR pIpAddr, 
                                        tIPADDRMASK pIpAddrMask, 
                                        UINT4 u4AddrlessIf, 
                                        UINT4 u4IfIndex,
                                        UINT4 u4MtuSize, 
                                        UINT4 u4IfSpeed,
                                        UINT1 u1IfType,
                                        UINT1 u1IfOperStat));
PUBLIC VOID IfSetDefaultValues PROTO(( tInterface *pInterface,
                                        tROWSTATUS rowStatus));
PUBLIC INT4 IfActivate         PROTO(( tInterface *pInterface));
PUBLIC INT4 IfInactivate       PROTO(( tInterface *pInterface));
PUBLIC INT4 IfDelete           PROTO(( tInterface *pInterface));
PUBLIC VOID IfDisable          PROTO(( tInterface *pInterface));
PUBLIC VOID OspfIfUp           PROTO(( tInterface *pInterface));
PUBLIC VOID OspfIfDown         PROTO(( tInterface *pInterface));
PUBLIC VOID IfUpdateState      PROTO(( tInterface *pInterface, UINT1 state));
PUBLIC tInterface *IfGetMinCostIfInCxt  PROTO(( tOspfCxt * pOspfCxt, INT1 i1Tos, tAreaId *pAreaId));
PUBLIC tInterface *IfGetMinCostIfInArea PROTO(( INT1 i1Tos, tArea *pArea));
PUBLIC VOID IfCleanup          PROTO((tInterface *pInterface, 
                                       UINT1  u1CleanupFlag));
PUBLIC INT4 IfDeleteIfMetric PROTO((tArea *pArea));
PUBLIC INT4 IfSetAreaId      PROTO((tInterface *pInterface,
                                       tAreaId *pAreaId));
PUBLIC INT4 IfSetAdmnStat    PROTO((tInterface *pInterface,
                                       UINT1 u1Status));
PUBLIC INT4 IfSetOperStat    PROTO((tInterface *pInterface,
                                       UINT1 u1Status));
PUBLIC INT1 OSPFSetIfMtuSizeInCxt PROTO ((tInterface * pInterface,UINT4 u4MtuSize));

PUBLIC INT1 OSPFSetIfSpeedInCxt PROTO ((tOspfCxt * pOspfCxt, UINT4 u4IfSpeed,
                                        tInterface *pInterface));

PUBLIC VOID IfSendConfErrTrap  PROTO((tInterface*    pInterface,
                                       UINT1           u1TrapId,
                                       UINT1           u1ErrType,
                                       UINT1           u1PktType,
                                       tIPADDR*   pSrcIpAddr));
PUBLIC VOID IfSendBadPktTrap   PROTO((tInterface* pInterface, 
                                       UINT1 u1PktType,
                                       tIPADDR*   pSrcIpAddr));
PUBLIC VOID IfSendRxmtTrap   PROTO((tNeighbor*     pNbr,
                                       tLsHeader*    pLsHeader,
                                       UINT1           u1PktType));

PUBLIC INT4  IfSetType PROTO ((tInterface * pInterface, UINT1 u1Type));
PUBLIC INT4 IfSetDemand PROTO ((tInterface * pInterface, UINT1 u1DemandValue));

PUBLIC INT4 IfSetPassive PROTO ((tInterface * pInterface));
PUBLIC VOID IfSendDefaultSumToStubNssaArea PROTO ((tArea * pArea));

PUBLIC UINT4 IfFindRouterIdFromIfAddr (tIPADDR  pIfIpAddr, tInterface * pInterface);
PUBLIC INT4  IfAllPassive PROTO ((tOspfCxt * pOspfCxt));

#ifdef BFD_WANTED
PUBLIC INT1 OspfHandleNbrPathStatusChange PROTO ((UINT4 u4ContextId,
                                          tBfdClientNbrIpPathInfo * pNbrPathInfo));
#endif
PUBLIC INT4 IfOspfBfdRegister PROTO ((tNeighbor*     pNbr));
PUBLIC INT4 IfOspfBfdDeRegister PROTO ((tNeighbor*     pNbr, BOOL1 bIsDynDis));
PUBLIC VOID OspfSecIpNetworkDel PROTO ((tNetIpv4IfInfo * pSecIpIfInfo));
/************************** osism.c ***************************************/

PUBLIC VOID IsmRunIsm         PROTO((tInterface *pInterface,
                                        UINT1 u1IfEvent));
PUBLIC VOID IsmResetVariables PROTO((tInterface *pInterface));
PUBLIC VOID IsmDisableIfTimers      
                                PROTO((tInterface *pInterface));
PUBLIC VOID IsmInitSchedQueue       
                                PROTO((VOID));
PUBLIC VOID IsmProcessSchedQueue    
                                PROTO((VOID));

PUBLIC VOID IsmSchedule        PROTO((tInterface *pInterface,
                                       UINT1 u1Event));
PUBLIC VOID IsmInvalid         PROTO((tInterface *pInterface));
PUBLIC VOID IsmIfUp           PROTO((tInterface *pInterface));
PUBLIC VOID IsmElectDr        PROTO((tInterface *pInterface));
PUBLIC VOID IsmIfReset        PROTO((tInterface *pInterface));
PUBLIC VOID IsmIfDown         PROTO((tInterface *pInterface));
PUBLIC VOID IsmIfLoop         PROTO((tInterface *pInterface));
PUBLIC VOID
IsmInitSchedQueueInCxt        PROTO ((tOspfCxt *pOspfCxt, 
              tInterface *pInterface));


/************************** oslak.c ***************************************/

PUBLIC VOID LakRcvLsAck         PROTO((tCRU_BUF_CHAIN_HEADER *pLsAckPkt,
                                       UINT2 u2Len, tNeighbor *pNbr));
PUBLIC VOID LakSendDirect       PROTO((tNeighbor *pNbr, UINT1 *pLsa));
PUBLIC VOID LakSendDelayedAck   PROTO((VOID *pArg));
PUBLIC VOID LakAddToDelayedAck     
                                PROTO((tInterface *pInterface, UINT1 *pLsa));
PUBLIC VOID LakClearDelayedAck      
                                PROTO(( tInterface *pInterface));

/************************** oslrq.c ***************************************/

PUBLIC VOID LrqRcvLsaReq     PROTO((tCRU_BUF_CHAIN_HEADER *pLsaReqPkt,
                                        UINT2 u2Len,tNeighbor *pNbr));
PUBLIC VOID LrqSendLsaReq    PROTO((tNeighbor *pNbr));
PUBLIC VOID LrqRxmtLsaReq    PROTO((VOID *pArg));
PUBLIC UINT1 LrqAddToLsaReqLst   
                                PROTO((tNeighbor *pNbr,
                                       tLsHeader *pLsHeader ));
PUBLIC VOID LrqDeleteFromLsaReqLst
                                PROTO((tNeighbor *pNbr,
                                       tLsaReqNode *pLsaReqNode));
PUBLIC tLsaReqNode *LrqSearchLsaReqLst
                                PROTO(( tNeighbor *pNbr,
                                        tLsHeader *pLsHeader));
PUBLIC VOID LrqClearLsaReqLst      
                                PROTO((tNeighbor *pNbr));
PUBLIC INT1 LrqIsEmptyLsaReqLst   
                                PROTO(( tNeighbor *pNbr));


/************************** oslsu.c ***************************************/

PUBLIC VOID LsuStartorStopSplAgingTmrInCxt PROTO ((tOspfCxt * pOspfCxt,
                                              tRtEntry * pRtEntry, UINT1 u1Action));
PUBLIC VOID LsuRcvLsUpdate   PROTO((tCRU_BUF_CHAIN_HEADER *pLsUpdate, 
                                       tNeighbor *pNbr)); 
PUBLIC VOID LsuSendAllFloodUpdatesInCxt  PROTO((tOspfCxt * pOspfCxt));

 
PUBLIC VOID LsuClearLsaDesc PROTO ((tLsaDesc * pLsaDesc));

PUBLIC UINT1 LsuFloodOut
                               PROTO((UINT1 *pLsa, UINT2 u2Len,
                                       tNeighbor *pRcvNbr,
                                       tArea *pArea,
                                       UINT1 u1LsaChngdFlag,
                                       tInterface *pIface));

PUBLIC VOID
LsuSendLsaInfoInfoToOpqApp (tLsaInfo * pLsaInfo, UINT1 u1LsaStatus);
PUBLIC VOID
NbrSendNbrInfoInfoToOpqApp (tNeighbor * pNbr);

PUBLIC VOID LsuSplAgeTmrAction     
                                PROTO((tTMO_SLL *pLsaLst, 
                                       tRouterId routerId, 
                                       UINT1 u1Action));
PUBLIC INT4 LsuInstallLsa     PROTO((UINT1 *pLsa, 
                                       tLsaInfo *pLsaInfo));
PUBLIC tLsaInfo *LsuSearchDatabase  PROTO((UINT1 lsType,
                                       tLINKSTATEID *pLinkStateId,
                                       tRouterId *pAdvRtrId,
                                       UINT1  *pPtr1, UINT1 *pPtr2));

PUBLIC tLsaInfo    *
LsuDatabaseLookUp PROTO ((UINT1 u1LsaType, tLINKSTATEID * pLinkStateId, 
                                          tIPADDR *pLinkMask, tRouterId * pAdvRtrId,
                                          UINT1 *pPtr1, UINT1 *pPtr2));

PUBLIC UINT1 LsuCompLsaInstance     
                                PROTO((tNeighbor *pNbr,tLsaInfo *pLsaInfo, 
                                       tLsHeader *pRcvdLsHeader));
PUBLIC VOID LsuProcessDeletedLsa    
                                PROTO((tArea *pArea));
PUBLIC VOID LsuFlushoutSelfOrgExtLsaInCxt
                                PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT1 LsuAddToLsu      PROTO((tNeighbor  *pNbr, 
                                       tLsaInfo  *pLsaInfo, 
                                       tLsUpdate *pUpdate, 
                                       UINT1 u1TxFlag));
PUBLIC VOID LsuSendLsu        PROTO((tNeighbor *pNbr, 
                                       tLsUpdate *pUpdate));
PUBLIC VOID LsuClearUpdate    PROTO((tLsUpdate *pUpdate));
PUBLIC VOID LsuFlushLsaAfterRemovingFromDescLst 
                                PROTO((tLsaInfo *pLsaInfo));
PUBLIC UINT1 LsuDeleteLsaFromDatabase 
                                PROTO((tLsaInfo *pLsaInfo, 
                                       UINT1 u1ForcedRelFlag));
PUBLIC VOID LsuDeleteAllLsasInCxt PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID LsuFlushAllSelfOrgLsasInCxt PROTO((tOspfCxt * pOspfCxt));

PUBLIC VOID LsuDeleteAreaLsaDatabase
                                PROTO((tArea *pArea));
PUBLIC VOID LsuFlushSelfOrgAreaLsaDatabase  PROTO((tArea * pArea));
PUBLIC tLsaInfo* LsuAddToLsdb
                              PROTO((tArea*         pArea,
                                    UINT1            u1LsaType,
                                    tLINKSTATEID* pLinkStateId,
                                    tRouterId*     pAdvRtrId,
                                    tInterface*     pIface,
                                    UINT1            u1LSAStatus,
                                    UINT1          *pLsa));
PUBLIC VOID LsuFlushAllNonDefaultSelfOrgAseLsaInCxt
                                PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID LsuGenerateNonDefaultAsExtLsasInCxt
                                PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT1 LsuProcessLsa     PROTO((UINT1 *pLsa, UINT2 u2Len,
                                       tNeighbor *pNbr));
PUBLIC VOID LsuDeleteAllExtLsasInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID LsuFlushAllSelfOrgExtLsasInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID LsuDeleteAllType11LsasInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID LsuDeleteAllType9LsasInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID  LsuFloodOutLsaToArea PROTO ((tArea * pFloodArea,
                      tLsaInfo *pLsaInfo,UINT1 u1RxtFlag));
PUBLIC VOID LsuClearLsaInfo (tLsaInfo * pLsaInfo);

PUBLIC INT4
LsuCheckActualChange      PROTO ((UINT1 *pDbLsa, UINT1 *pRcvdLsa));

PUBLIC UINT4
LsuPrefixLsaHashFunc (tLINKSTATEID LinkStateId,tIPADDRMASK ipAddrMask);
PUBLIC UINT4 LsuGetExtLsaCountInCxt (tOspfCxt * pOspfCxt);
PUBLIC INT4 LsuGetTranslatedLsaCount (tOspfCxt * pOspfCxt);

PUBLIC VOID LsuProcessAckFlag
PROTO ((tNeighbor * pNbr, UINT1 *pLsa, UINT1 u1AckFlag));
PUBLIC UINT1 LsuIsSelfOriginatedLsaInCxt
PROTO ((tOspfCxt * pOspfCxt, tLsHeader * pLsHeader));
PUBLIC VOID        LsuPutTxTime
PROTO ((tCRU_BUF_CHAIN_HEADER * pLsuPkt, tInterface * pInterface));
PUBLIC VOID        LsuProcessRcvdSelfOrgLsa
PROTO ((tLsaInfo * pLsaInfo, tLSASEQNUM lsaSeqNum));
/************************** osmain.c ***************************************/

PUBLIC INT4 OspfTaskInit      PROTO((VOID));
PUBLIC INT4 OspfSnmpIfHandler  PROTO((tOspfSnmpIfParam   *pOspfIfParam));
PUBLIC VOID  OspfProcessQMsg   PROTO((const UINT1 au1QName[4]));
PUBLIC VOID OspfProcessLowPriQMsg   PROTO((const UINT1 au1QName[4]));
PUBLIC VOID OspfProcessIPPacket PROTO((tOspfLowPriPktInfo *pOspfLowPriPktInfo));
PUBLIC VOID OspfSendMsgToLowPriorityQ PROTO ((tOspfQMsg *pOspfQMsg));
PUBLIC INT4 OspfRtmCallbackFn  PROTO((tRtmRespInfo * pRespInfo,
                                      tRtmMsgHdr * pRtmHdr));
PUBLIC VOID OspfMemClear PROTO ((VOID));
PUBLIC INT1 OspfRtmRouteUptAckMsg PROTO((tRtmRespInfo * pRespInfo));

#ifdef ROUTEMAP_WANTED
PUBLIC INT4 OspfSendRouteMapUpdateMsg PROTO ((UINT1 *pu1RMapName, 
                                              UINT4 u4Status));
PUBLIC tFilteringRMap * OspfGetMinFilterRMap
PROTO ((tFilteringRMap * pFilterRMap1, INT1 i1Type1,
        tFilteringRMap * pFilterRMap2, INT1 i1Type2));
PUBLIC INT4
OspfCmpFilterRMapName (tFilteringRMap * pFilterRMap,
                       tSNMP_OCTET_STRING_TYPE * pRMapName);
#endif

/************************** osnbr.c ***************************************/

PUBLIC tNeighbor *NbrCreate    PROTO((tIPADDR *pNbrIpAddr,
                                        tRouterId *pNbrId,
                                        UINT4 nbrAddrlessIf,
                                        tInterface *pInterface,
                                        UINT1 configStatus));
PUBLIC INT4 NbrActivate        PROTO((tNeighbor *pNbr));
PUBLIC INT4 NbrInactivate      PROTO((tNeighbor *pNbr));
PUBLIC INT4 NbrDelete          PROTO((tNeighbor *pNbr));
PUBLIC VOID NbrUpdateState     PROTO((tNeighbor *pNbr, UINT1 state));
PUBLIC INT4 NbrProcessPriorityChange PROTO((tNeighbor *pNbr,
                                UINT1 u1NewPriority));
PUBLIC VOID NbrSetArea         PROTO((tNeighbor *pNbr ,tArea *pOldArea ,
                                       tArea *pNewArea));
PUBLIC VOID NbrCreateNbrsTable PROTO((tOspfCxt *pOspfCxt));
PUBLIC VOID NbrUpdateNbrTable PROTO((tNeighbor * pNbr,
                                  UINT4 u4PrevIfIndex, UINT4 u4CurrIfIndex));

/************************** osnseln.c ***************************************/

PUBLIC VOID NssaUpdtRngForLsa PROTO ((tArea * pArea, UINT4 u4Indice));
PUBLIC VOID        NssaUpdtRngCostType
PROTO ((tLsaInfo * pLsaInfo, tAddrRange * pAddrRng));
PUBLIC VOID NssaFsmTrgrTrnsltrElnInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID NssaABRStatLostInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID NssaType7To5TranslatorInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID NssaTrnsltrStatLost PROTO ((tArea *pArea));
PUBLIC INT4 NssaFsmTrnsltrRlChng PROTO ((tArea * pArea));
PUBLIC VOID NssaFsmUpdtTrnsltrState
                 PROTO ((tArea *pArea, UINT1 u1PrvState, UINT1 u1NewState));
PUBLIC VOID NssaFsmFindTranslator PROTO ((tArea * pArea));


/************************** osnsm.c ***************************************/

PUBLIC INT4 NsmRunNsm            PROTO((tNeighbor *pNbr , UINT1 nbr_event ));
PUBLIC VOID NsmInvalid           PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmLlDown            PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmDown              PROTO((tNeighbor *pNbr));
PUBLIC VOID Nsm1wayRcvd          PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmRestartAdj        PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmCheckAdj          PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmProcessAdj        PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmExchgDone         PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmNegDone           PROTO((tNeighbor *pNbr));
PUBLIC VOID Nsm2wayRcvd          PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmStart             PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmStartInactTimer   PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmRestartInactTimer PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmAttHelloRcvd      PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmLdngDone          PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmNoAction          PROTO((tNeighbor *pNbr));
PUBLIC VOID NsmResetVariables    PROTO((tNeighbor* pNbr));
PUBLIC INT1 NsmToBecomeAdj PROTO ((tNeighbor * pNbr));

/************************** osnstrln.c ***************************************/

PUBLIC INT4 NssaType7AddrRangeAgg PROTO ((tArea * pArea, tLsaInfo * pLsaInfo));
PUBLIC VOID        NssaUpdtType7AggRange
PROTO ((tArea * pArea, tLsaInfo * pLsaInfo, UINT4 u4Indice));
PUBLIC UINT1 NssaChkSelfOrgType5 PROTO ((tLsaInfo * pLsaInfo));
PUBLIC VOID NssaFlshType5TrnsltdInCxt PROTO ((tOspfCxt * pOspfCxt, tAddrRange * pAddrRng));
PUBLIC VOID NssaTrnsltType7NotInRng PROTO ((tLsaInfo * pLsaInfo));
PUBLIC VOID NssaType7LsaTranslation 
                         PROTO ((tArea *pArea, tLsaInfo *pLsaInfo));
PUBLIC VOID NssaProcAggRangeInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID NssaAdvertiseType7Rngs PROTO ((tArea *pArea));


/************************** osols.c ***************************************/

PUBLIC VOID OlsStartTimerForLsaRegen 
                                PROTO((tLsaInfo *pLsaInfo));
PUBLIC VOID OlsGenerateAsExtLsasInCxt
                                PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID OlsGenerateIndicationLsa
                                PROTO((tArea *pArea));
PUBLIC INT4 OlsSignalLsaRegenInCxt
                                PROTO((tOspfCxt * pOspfCxt,
                                       UINT1 u1Type, UINT1 *pPtr));
                                
PUBLIC INT4 OlsBuildSummaryParam
                                PROTO((tArea *pArea,
                                       tRtEntry *pRtEntry,
                                       tSummaryParam *pSummaryParam)); 

PUBLIC VOID OlsGenerateLsa   PROTO((tArea *pArea,
                                       UINT1 u1InternalType,
                                       tLINKSTATEID *pLsId,
                                       UINT1 *pPtr));
PUBLIC INT4 OlsifParamChangeInCxt (tOspfCxt * pOspfCxt,
                                   UINT1 *pStruct,
                                  INT1  i1LsaType);
PUBLIC VOID OlsGenerateSummaryToArea PROTO ((tArea * pNewArea));
PUBLIC VOID OlsGenerateASSummaryToArea PROTO ((tArea * pNewArea));

PUBLIC VOID OlsResetType7FwdAddr PROTO (( tInterface *pInterface));
PUBLIC VOID OlsGenerateNssaDfLsa 
       PROTO (( tArea *pArea, UINT1 u1InternalType, tLINKSTATEID * pLsId ));
PUBLIC VOID OlsHandleFuncEqvType7 PROTO (( UINT1* pLsa, tNeighbor * pNbr));
PUBLIC INT4 OlsModifyNssaAsbrDefRtTransInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4 OlsIsEligibleToGenAseLsaInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                  tExtRoute * pExtRt));
PUBLIC VOID OlsChkAndGenerateSumToNewAreasInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID        OlsGenerateSummary PROTO ((tArea * pArea, 
                         tRtEntry * pRtEntry));
PUBLIC VOID OlsModifyLsId PROTO ((tArea * pArea, UINT1 u1InternalType,
                                   tLINKSTATEID * pLsId, UINT1 *pPtr));
PUBLIC tLsaDesc   *OlsGetLsaDescInCxt
PROTO ((tOspfCxt * pOspfCxt, UINT1 u1LsaType,
        tLINKSTATEID * pLinkStateId, UINT1 *pPtr, UINT1 *pu1NewDescFlag));


/************************** osppp.c ***************************************/

PUBLIC VOID PppSendPkt        PROTO(( tCRU_BUF_CHAIN_HEADER *pPkt,
                                        UINT2 pktLen,tInterface *pInterface,
                                        tIPADDR *pDestIpAddr,
                                        UINT1 u1ReleaseFlag));
PUBLIC VOID PppRcvPkt         PROTO((tCRU_BUF_CHAIN_HEADER *pPkt,
                                       UINT2 u2Len,
                                       tInterface *pInterface,
                                       tIPADDR *pSrcIpAddr,
                                       tIPADDR *pDestIpAddr));
PUBLIC tInterface *PppAssociateWithVirtualIf
                              PROTO ((tInterface * pInterface,
                                      tRouterId * pRtrId));

/************************** osragadd.c ***************************************/
PUBLIC VOID RagAddNewAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                            tAsExtAddrRange * pAsExtRange));
PUBLIC INT4 RagAddNewBkBoneAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                   tAsExtAddrRange * pAsExtRange));

PUBLIC VOID RagAddNewNssaAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                tAsExtAddrRange * pAsExtRange));

PUBLIC VOID        RagInsertRouteInBkboneAggAddrRangeInCxt
PROTO ((tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange, tExtRoute * pExtRoute));

PUBLIC VOID RagProcessBkBoneRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                               tAsExtAddrRange * pAsExtRange));

PUBLIC VOID RagProcessNssaRange PROTO ((tAsExtAddrRange * pAsExtRange,
                                         tArea * pArea));

PUBLIC VOID RagProcBboneRngForNssa PROTO ((tAsExtAddrRange * pAsExtRange,
                                            tArea * pArea));
/************************** osragdel.c ***************************************/
PUBLIC VOID RagDelAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                         tAsExtAddrRange * pAsExtRange,
                                     UINT1 u1Flag));
PUBLIC VOID RagDelBkBoneAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                               tAsExtAddrRange * pAsExtRange));
PUBLIC VOID RagDelNssaAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                             tAsExtAddrRange * pAsExtRange));

PUBLIC VOID RagFlushBkBoneRangeInCxt PROTO ((tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange,
                                         tExtRoute * pExtRoute));

PUBLIC VOID RagFlushNssaRange PROTO ((tAsExtAddrRange * pAsExtRange,
                                       tExtRoute * pExtRoute, tArea * pArea));

PUBLIC VOID        RagFlshNssaRngExtDel
PROTO ((tAsExtAddrRange * pAsExtRange, tArea * pArea, tExtRoute *pExtRoute));

PUBLIC VOID RagFlshNssaRngDel PROTO ((tAsExtAddrRange * pAsExtRange,
                                       tArea * pArea));


/************************** osragext.c ***************************************/
PUBLIC VOID RagAddRouteInAggAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                   tExtRoute *pExtRoute));

PUBLIC VOID RagDelRouteFromAggAddrRangeInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                     tExtRoute *pExtRoute));
PUBLIC INT4 RagHandleAsExtAggInCxt PROTO ((tOspfCxt * pOspfCxt,
                                           tAsExtAddrRange *pAsExtRange, UINT1 u1Type));
PUBLIC UINT1 RagAddRangeInRtLstInCxt PROTO ((tOspfCxt * pOspfCxt,
                                             tAsExtAddrRange * pAsExtRange));

PUBLIC tAsExtAddrRange * RagAsExtAddDefaultValues PROTO ((UINT4 u4AsExtNet, 
                                                          UINT4 u4AsExtMask, 
                                                          UINT4 u4AreaId));
PUBLIC VOID        RagOriginateNonAggLsaInCxt
 PROTO ((tOspfCxt * pOspfCxt, tExtRoute * pExtRoute, tArea * pArea));
PUBLIC VOID RagAddrRangeAttrChngInCxt PROTO ((tOspfCxt * pOspfCxt,
                                         tAsExtAddrRange * pAsExtRange));

PUBLIC INT1
RagInsertRngInAreaLst PROTO ((tAsExtAddrRange * pAsExtRange, tArea *pArea));


/************************** osragutl.c ***************************************/

PUBLIC UINT1 RagCompAddrRng PROTO ((tAsExtAddrRange * pAsExtRange1,
                                     tAsExtAddrRange * pAsExtRange2));
PUBLIC VOID RagUpdtBboneRngFrmExtLstInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                  tAsExtAddrRange * pAsExtRange));

PUBLIC VOID
RagUpdtBboneRngForNssa PROTO ((tArea *pArea, tAsExtAddrRange * pAsExtRange));

PUBLIC UINT1 RagIsExtRtFallInRange PROTO ((tExtRoute * pExtRoute,
                                            tAsExtAddrRange * pAsExtRange));

PUBLIC UINT1 RagChkLsaFallInRng PROTO ((tAsExtAddrRange * pAsExtRange,
                                         tLsaInfo * pLsaInfo));
PUBLIC UINT1 RagCompRngAndLsa PROTO ((tAsExtAddrRange * pAsExtRange,
                                       tLsaInfo * pLsaInfo));
PUBLIC UINT1 RagChkLsaFallInPrvRng PROTO ((tAsExtAddrRange * pAsExtRange,
                                            tLsaInfo * pLsaInfo));
PUBLIC UINT1 RagChkLsaFallInBackboneRng PROTO ((tOspfCxt *pOspfCxt,
                                                tAsExtAddrRange * pAsExtRange,
                                                tLsaInfo * pLsaInfo));

PUBLIC UINT1
RagChkLsaFallInNssaRng PROTO ((tArea *pArea,
                               tAsExtAddrRange * pAsExtRange,
                               tLsaInfo * pLsaInfo));

PUBLIC UINT1 RagChkExtRtFallInPrvRngInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                  tAsExtAddrRange * pAsExtRange,
                                              tExtRoute * pExtRoute));
PUBLIC UINT1 RagChkExtRtFallInBbRngInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                 tExtRoute * pExtRoute));

PUBLIC UINT1 RagUpdatCostTypeForRange PROTO ((tAsExtAddrRange * pAsExtRange,
                                               tExtRoute * pExtRoute));
PUBLIC VOID RagUpdtRtLstBtwRangesInCxt PROTO ((tOspfCxt * pOspfCxt,
                                               tAsExtAddrRange * pAsExtRange,
                                           tAsExtAddrRange * pSpecLessRange,
                                           UINT1 u1Flag));
PUBLIC VOID RagUpdtRtLstFromExtRtTableInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                    tAsExtAddrRange * pAsExtRange));

PUBLIC UINT1 RagChkValidMetricInRng PROTO ((tAsExtAddrRange * pAsExtRange));

PUBLIC UINT1 RagIsChangeInMetric PROTO ((tMetric *pMetric1, tMetric *pMetric2));

PUBLIC UINT1 RagGenExtLsaInCxt PROTO ((tOspfCxt * pOspfCxt, UINT1 u1Type,
                                       UINT1 *pPtr));

PUBLIC VOID        RagScanRtTableForAddrRangesInCxt
PROTO ((tOspfCxt * pOspfCxt, tAsExtAddrRange * pMoreSpecRange,
        tAsExtAddrRange * pLessSpecRange, tArea *pArea, UINT4 u4AreaType));
PUBLIC 
tAsExtAddrRange *RagFindMatChngBkBoneRangeInCxt PROTO ((tOspfCxt *pOspfCxt, 
                                                  tExtRoute * pExtRoute));

PUBLIC tAsExtAddrRange *
RagFindAggAddrRangeInBkboneInCxt PROTO ((tOspfCxt *pOspfCxt, tExtRoute * pExtRoute));

PUBLIC tAsExtAddrRange *RagFindAggAddrRangeInNssaForRt
PROTO ((tExtRoute * pExtRoute, tArea * pArea));


/************************** osrtc.c ***************************************/

PUBLIC INT1 RtcInitRt         PROTO((tOspfRt* pOspfRt, UINT1 u1AppId, 
                                       UINT4 u4Type));
PUBLIC INT1 RtcDeInitRt         PROTO((VOID * pRtRoot, INT1 i1AppId));

PUBLIC VOID  OspfTrieCbDelete PROTO ((VOID *pInput));

PUBLIC VOID RtcCalculateRtInCxt    PROTO((tOspfCxt * pOspfCxt));
      
PUBLIC INT1 RtcGetExtLsaLink       
                                PROTO((UINT1 *pLsa, UINT2 u2LsaLen,
                                       INT1 i1Tos,
                                       tExtLsaLink *pExtLsaLink));
PUBLIC UINT4 RtcGetCostFromSummaryLsa  PROTO((UINT1 *pLsa, UINT2 u2LsaLen,
                           INT1 i1Tos));
PUBLIC VOID RtcIncUpdtRtSmmryLinkLsa
                                PROTO((tOspfRt *pCurrRt,
                                       tLsaInfo *pLsaInfo));
PUBLIC VOID RtcIncUpdateAllAsExtRouteInCxt  PROTO((tOspfCxt * pOspfCxt,
                                              tOspfRt * pCurrRt));
PUBLIC VOID RtcClearAndDeleteRtFromIpInCxt
                          PROTO((tOspfCxt *pOspfCxt, tOspfRt * pOspfRt));

PUBLIC tRtEntry *RtcFindRtEntry   
                                PROTO((tOspfRt *pCurrRt,
                                       tIPADDR *pDest,
                                       tIPADDRMASK *pDestMask,
                                       UINT1 u1DestType));

PUBLIC tPath *RtcFindPath    PROTO((tOspfRt *pCurrRt, tIPADDR *pDest,
                                       UINT1 u1DestType, INT1 i1Tos,
                                       tArea *pArea));

PUBLIC VOID RtcClearSpfTree   PROTO ((tSpf * pSpfTree));

PUBLIC VOID RtcDeleteExtRoutes PROTO((tOspfRt* pOspfRt));
PUBLIC UINT1 RtcBldSpfTreeTransAreaInCxt  PROTO((tOspfCxt * pOspfCxt,
                                                 tAreaId* transitAreaId,
                                                 tRouterId* destRtrId,
                                                 tIPADDR *pIpAddr));


PUBLIC VOID RtcFlushOutSummaryLsa  
                                PROTO((tArea *pArea, UINT1 u1LsaType,
                                       tLINKSTATEID *pLinkStateId,
           UINT4 u4IpAddrMask));
PUBLIC VOID RtcCalculateAllExtRoutesInCxt
                                PROTO((tOspfCxt * pOspfCxt, tOspfRt *pCurrRt, INT1 i1Tos));
PUBLIC VOID RtcProcessRtChangesInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID RtcIncUpdateExtRoute 
                      PROTO ((tOspfRt * pCurrRt, tLsaInfo *pLsaInfo));
PUBLIC tPath      *RtcGetPathInArea PROTO ((tRtEntry *, tAreaId, INT1));

PUBLIC INT4  OspfTrieAddRtEntry PROTO
                 ((VOID *pInputParams, VOID *pOutputParams,
                 VOID **ppAppSpecInfo, VOID * pNewAppSpecInfo));

PUBLIC INT4 OspfTrieDeleteRtEntry PROTO ((VOID *pInputParams,
                      VOID **ppAppSpecInfo,
               VOID *pOutputParams,
        VOID * NextHop,
        tKey Key));

PUBLIC UINT1 * OspfTrieDelAllRtEntry PROTO((tInputParams *pInputParams,
                                VOID *dummy, tOutputParams *pOutputParams));

PUBLIC INT4 OspfTrieSearchRtEntry PROTO ((tInputParams *pInputParams,
            tOutputParams *pOutputParams, VOID * pAppSpecInfo));

PUBLIC INT4 OspfTrieLookUpEntry PROTO ((tInputParams * pInputParams,
                                        tOutputParams *pOutputParams,
            VOID *pAppSpecInfo,
            UINT2 u2KeySize, tKey key));

PUBLIC INT4 OspfBestMatch PROTO 
  ((UINT2  u2KeySize, tInputParams * pInputParams,
                  VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key));

PUBLIC INT4
OspfTrieGetNextRtEntry PROTO
               ((tInputParams *pInputParams, VOID *pOutputParams,
                VOID *ppAppSpecInfo, tKey Key));

PUBLIC VOID RtcSetRtTimer PROTO ((tOspfCxt *pOspfCxt));

PUBLIC tPath * RtcGetRtTosPath PROTO ((tRtEntry *pRtEntry,
                                       INT1 i1Tos, UINT1 u1Flag));

PUBLIC VOID   RtcDeleteNodesFromSpf PROTO ((tSpf * pSpfTree));
PUBLIC VOID RtcFreeRtEntry PROTO ((tRtEntry * pRtEntry));
PUBLIC VOID RtcGenerateAggLsaInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT1 RtcGetPathType PROTO ((tRtEntry *pRtEntry, INT1 i1Tos));
PUBLIC VOID RtcCheckTranslationInCxt PROTO ((tOspfCxt * pOspfCxt));

/************************** osrtl.c ***************************************/

PUBLIC tPath *RtlRtLookupInCxt    PROTO((tOspfCxt * pOspfCxt, tOspfRt *pCurrRt,
                                       tIPADDR *pIpAddr, INT1 i1Tos
                                       ));

PUBLIC tRtEntry  *RtlNssaRtLookup 
          PROTO((tOspfRt * pCurrRt, tIPADDR * pDestIpAddr, INT1 i1Tos));


/************************** osrtr.c ***************************************/
PUBLIC INT4 RtrSetProtocolStatusInCxt  PROTO((UINT4 u4OspfCxtId, UINT1 u1AdmnStatus));
#ifdef TOS_SUPPORT
PUBLIC INT4 RtrSetTosSupportInCxt PROTO((tOspfCxt *pOspfCxt, UINT1 u1TosCapability));
#endif
PUBLIC INT4 RtrSetAsbrStatusInCxt PROTO((tOspfCxt *pOspfCxt, UINT1 u1AsBdrRtrStatus));
PUBLIC VOID RtrEnterOverflowStateInCxt PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrEnableInCxt           PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrDisableInCxt          PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrShutdownInCxt         PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrRebootInCxt           PROTO((UINT4 u4OspfCxtId));
PUBLIC INT4 RtrSetRouterIdInCxt      PROTO((tOspfCxt * pOspfCxt, tRouterId RtrId));
PUBLIC INT4 RtrSetExtLsdbLimitInCxt  PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrSetExitOverflowIntervalInCxt  PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID RtrHandleABRStatChngInCxt PROTO((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrSetABRTypeInCxt PROTO ((tOspfCxt * pOspfCxt, UINT4 u4ABRType));
PUBLIC INT1 RtrCheckABRStatInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4 RtrCreateCxt             PROTO((UINT4 u4OspfCxtId, 
                              UINT1 u1OspfAdminStat));
PUBLIC INT4 RtrDeleteCxt             PROTO((tOspfCxt * pOspfCxt));

/************************** ossnmpif.c ***************************************/
PUBLIC VOID SnmpifSendTrapInCxt    PROTO((tOspfCxt *pOspfCxt, UINT1 u1TrapId, VOID *pTrapInfo));
PUBLIC tOspfQMsg * SnmpOspfIfMsgAlloc    PROTO((VOID));
PUBLIC INT4 SnmpOspfIfMsgSend      PROTO((tOspfQMsg  *pOspfQMsg));
PUBLIC VOID SnmpSendStatusChgTrapInCxt
PROTO ((tOspfCxt * pOspfCxt, tNeighbor * pNbr, UINT4 u4Trap));

/************************** ostmrif.c ***************************************/

PUBLIC VOID TmrInitTimerDesc PROTO((VOID));
PUBLIC INT4 TmrSetTimer       PROTO((tOspfTimer* pTimer, UINT1 u1TimerId,
                                       INT4 i4NrTicks));
PUBLIC VOID TmrRestartTimer   PROTO((tOspfTimer* pTimer, UINT1 u1TimerId,
                                       INT4 i4NrTicks));
PUBLIC VOID TmrDeleteTimer    PROTO((tOspfTimer* pTimer));
PUBLIC VOID TmrHelloDeleteTimer    PROTO((tOspfTimer* pTimer));
PUBLIC VOID TmrHandleExpiry   PROTO((VOID));
PUBLIC VOID TmrHandleHelloExpiry   PROTO((VOID));
PUBLIC VOID TmrHelloTimer PROTO ((VOID *pArg));
PUBLIC VOID TmrPollTimer PROTO ((VOID *pArg));


/************************** oscxtutil.c **************************************/

INT4 OspfConvertTime (UINT1 *pu1TimeStr, tUtlTm * tm);
INT4 OspfConvertTimeForSnmp (UINT1 *pu1TimeStr, tUtlTm * tm);
VOID OspfPrintKeyTime(INT4 u4Secs,UINT1 *pOspfKeyTime);
PUBLIC UINT1 *OspfPrintIpAddr PROTO((UINT1 *pu1IpAddr));
PUBLIC UINT1 UtilFindMaskLen PROTO(( UINT4 u4Mask ));

PUBLIC VOID OsUtilCalCxtRBTree PROTO ((UINT4 u4Base, UINT4 u4OspfCxtId, 
           UINT1 au1SemName[]));
PUBLIC VOID UtilConstructHdr  PROTO((tInterface *pInterface, 
                                       tCRU_BUF_CHAIN_HEADER *au1Buf, 
                                       UINT1 u1Type, 
                                       UINT2 u2Len));
PUBLIC tCRU_BUF_CHAIN_HEADER *UtilOsMsgAlloc 
                                PROTO((UINT4 u4Size));
PUBLIC VOID UtilOsMsgFree    PROTO((tCRU_BUF_CHAIN_HEADER *msg, 
                                       UINT1 u1ReleaseFlag ));
PUBLIC UINT1 UtilIpAddrComp  PROTO((tIPADDR addr1, tIPADDR addr2));
PUBLIC UINT1 UtilIpAddrMaskComp  
                                PROTO((tIPADDR addr1, tIPADDR addr2, 
                                       tIPADDRMASK mask));
PUBLIC VOID UtilIpAddrMaskCopy     
                                PROTO((tIPADDR dest, tIPADDR src, 
                                       tIPADDR mask));
PUBLIC UINT1 UtilIpAddrIndComp     
                                PROTO(( tIPADDR  ifIpAddr1, 
                                        UINT4 u4AddrlessIf1, 
                                        tIPADDR ifIpAddr2, 
                                        UINT4 u4AddrlessIf2));
PUBLIC UINT1 UtilLsaIdComp   PROTO(( UINT1 u1LsaType1, 
                                        tLINKSTATEID linkStateId1, 
                                        tRouterId advRtrId1, 
                                        UINT1 u1LsaType2, 
                                        tLINKSTATEID linkStateId2, 
                                        tRouterId advRtrId2));
PUBLIC tAddrRange  *UtilFindAssoAreaAddrRangeInCxt 
                                PROTO((tOspfCxt * pOspfCxt, tIPADDR *pAddr, tAreaId *pAreaId));
PUBLIC tAddrRange  *UtilFindAssoAllAreaAddrRangeInCxt 
                                PROTO((tOspfCxt * pOspfCxt, tIPADDR *pAddr));
PUBLIC tArea *UtilFindAssoAreaInCxt
                                PROTO((tOspfCxt * pOspfCxt, tIPADDR *pAddr));
PUBLIC UINT4   UtilHashGetValue     
                                PROTO((UINT4 u4HashSize, UINT1 *pKey, 
                                       UINT1 u1KeyLen));
PUBLIC UINT1   UtilVirtIfIndComp   
                                PROTO((tAreaId  areaId1, 
                                       tRouterId nbrId1, 
                                       tAreaId areaId2, 
                                       tRouterId nbrId2));
PUBLIC tInterface *UtilFindIfInCxt       PROTO((tOspfCxt * pOspfCxt,
                                                UINT4   u4IfIndex));
PUBLIC VOID UtilExtractLsHeaderFromPkt 
                                PROTO((tCRU_BUF_CHAIN_HEADER *pPkt, 
                                       tLsHeader *pLsHeader, 
                                       INT4 i4Offset,INT4 i4PktType));
PUBLIC VOID UtilExtractLsHeaderFromLbuf 
                                PROTO((UINT1 *pLsa, 
                                       tLsHeader *pLsHeader));
PUBLIC UINT1 UtilIpAddrNComp       
                                PROTO((tIPADDR *pIpAddr1, 
                                       tIPADDR *pIpAddr2,
                                       UINT1 u1Len));
PUBLIC UINT2 UtilCalculateChksum     
                                PROTO((tCRU_BUF_CHAIN_HEADER* pBuf,
                                       UINT2 u2Len));
PUBLIC VOID UtilComputeLsaFletChksum 
                                PROTO((UINT1*  pLsa ,UINT2   u2Len));
PUBLIC INT1 UtilVerifyLsaFletChksum
                                PROTO((UINT1 *pLsa, UINT2 u2Len));
PUBLIC INT1 UtilFindIsTransitArea PROTO ((tArea *pArea));

PUBLIC UINT4 UtilJitter        PROTO((UINT4 u4Value, UINT4 u4JitterPcent));

PUBLIC UINT1 UtilAddrMaskCmpe       PROTO((UINT4 addr1, UINT4 mask1, 
                                              UINT4 addr2, UINT4 mask2));

PUBLIC INT4 UtilSelectOspfRouterId PROTO((UINT4 u4OspfCxtId,tRouterId *pRtrId));

PUBLIC INT4 UtilRBFreeRouterLinks PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
PUBLIC INT4 UtilRBFreeNwLinks PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
/* Low level Util Routines */
PUBLIC tHost      *SetFindHostInCxt PROTO ((tOspfCxt * pOspfCxt,
                                           tIPADDR * pHostAddr));

PUBLIC tMd5AuthkeyInfo *Md5AuthKeyInfoCreate PROTO ((VOID));

PUBLIC VOID AuthKeyCopy PROTO ((UINT1 *pu1Dst, UINT1 *pu1Src, INT4 i4Len));

PUBLIC VOID SortInsertMd5AuthKeyInfo PROTO ((tTMO_SLL * pMd5authkeyLst,
                                              tMd5AuthkeyInfo * pAuthkeyInfo));
PUBLIC tExtRoute  *ExtrtRtAddDefaultValuesInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                    tIPADDR extRouteDest,
                                                    tIPADDRMASK extRouteMask,
                                                    INT4 i4ExtRouteTOS));


PUBLIC tRedistrConfigRouteInfo *RrdConfRtInfoCreateInCxt
    PROTO((tOspfCxt * pOspfCxt,
           tIPADDR * pRrdDestIPAddr, tIPADDRMASK * pRrdDestAddrMask));
PUBLIC INT4
OsUtilGetCryptoLen(tInterface * pInterface);


PUBLIC INT4 RrdConfRtInfoDeleteInCxt PROTO ((tOspfCxt * pOspfCxt,
                                         tIPADDR * pRrdDestIPAddr,
                                         tIPADDRMASK * pRrdDestAddrMask));

PUBLIC tOspfCxt * UtilOspfGetCxt PROTO  ((UINT4 u4OspfCxtId));
PUBLIC INT4       UtilOspfReleaseContext PROTO  ((VOID));
PUBLIC INT4       UtilOspfGetFirstCxtId PROTO  ((UINT4 * pu4OspfCxtId));
PUBLIC INT4       UtilOspfGetNextCxtId  PROTO  ((UINT4 u4OspfCxtId, UINT4 *
                                                 pu4NextOspfCxtId));
PUBLIC INT4       UtilOspfIsValidCxtId PROTO ((UINT4 u4OspfCxtId));
PUBLIC UINT4      UtilOspfGetTraceFlag PROTO ((UINT4 u4OspfCxtId));
PUBLIC INT4       UtilOspfVcmIsVcExist PROTO ((UINT4 u4OspfCxtId));
PUBLIC INT4       UtilOspfIsVcmSwitchExist PROTO ((UINT1 *pu1Alias,
                                      UINT4 *pu4VcNum));
PUBLIC INT4       UtilOspfGetVcmAliasName PROTO ((UINT4 u4ContextId,
                                    UINT1 *pu1Alias));
PUBLIC INT4       UtilOspfGetVcmSystemMode PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4       UtilOspfGetVcmSystemModeExt PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4       UtilOspfIsLowPriorityMessage PROTO ((UINT4 u4MsgType));
PUBLIC INT4       UtilOspfIsLowPriorityOspfPkt PROTO ((UINT1 u1OspfPktType));
PUBLIC VOID       UtilOspfBitGet PROTO ((UINT1 * pu1RxmtInfo,
                                         UINT4 u4DllNodeSegment, UINT4 * pu4Value));
PUBLIC VOID       UtilOspfBitSet PROTO ((UINT1 *pu1RxmtInfo,
                                         UINT4 u4DllNodeSegment, UINT4 u4value));
PUBLIC UINT4      UtilOspfGetExtTraceFlag PROTO ((UINT4 u4OspfCxtId));
PUBLIC VOID       UtilConstRtrLsaRBTree PROTO ((UINT1 * pLsa));
PUBLIC tArea *    UtilOspfGetNextAreaInCxt PROTO ((tOspfCxt *, tAreaId));
PUBLIC tInterface * UtilOspfGetNextInterfaceInCxt PROTO ((tOspfCxt *,
                                                         tIPADDR , UINT4));
PUBLIC tOspfCxt * UtilOspfGetFirstCxt PROTO((VOID));
PUBLIC tOspfCxt * UtilOspfGetNextCxt PROTO((UINT4 u4OspfCxtId));

PUBLIC INT4
GetfsMIStdOspfIfAuthKey PROTO (( UINT4 u4OspfCxtId,UINT4 u4OspfIfIpAddress,UINT4 u4AddrlessIf,tSNMP_OCTET_STRING_TYPE *pOctetAuthKeyValue));
PUBLIC VOID UtilOspfApplyRMapRule PROTO ((tOspfCxt *pOspfCxt));
PUBLIC UINT4 UtilOspfDwordFromPdu PROTO ((UINT1 *pu1PduAddr));
PUBLIC VOID UtilOspfDwordToPdu PROTO ((UINT1 *pu1PduAddr, UINT4 u4Value));
PUBLIC UINT1 * RouterLsaAlloc PROTO ((UINT2 length));
PUBLIC VOID RouterLsaFree PROTO ((UINT1 *pLsaBlk));
PUBLIC VOID UtilOspfClearAreaDb PROTO ((tArea  *pArea));
PUBLIC VOID UtilGetShaDigest ( tCRU_BUF_CHAIN_HEADER *pBuf,  UINT2 u2Len,tMd5AuthkeyInfo * pAuthkeyInfo,
                                                                  UINT4 whichSha, UINT1 * pdigest);

/************************** osvif.c **************************************/
PUBLIC tInterface* VifCreateInCxt  PROTO((tOspfCxt * pOspfCxt,
                                       tAreaId *pTransitAreaId, 
                                       tRouterId *pNbrId, 
                                       tROWSTATUS rowStatus));
PUBLIC VOID VifSetVifValues  PROTO((tInterface *pVirtIf, 
                                       tRtEntry *pRtEntry));
PUBLIC INT4 VifSetVLValues PROTO ((tInterface * pVirtIf, 
              tCandteNode *pNewVertex));
PUBLIC VOID VifSendConfErrTrap     
                                PROTO((tInterface*    pInterface,
                                       UINT1           u1TrapId,
                                       UINT1           u1ErrType,
                                       UINT1           u1PktType));
PUBLIC VOID VifSendBadPktTrap      
                                PROTO((tInterface *pInterface, 
                                       UINT1 u1PktType));
PUBLIC VOID VifSendRxmtTrap  PROTO((tNeighbor *pNbr, 
                                       tLsHeader *pLsHeader, 
                                       UINT1 u1PktType));

/************************** osfetch.c **************************************/
PUBLIC tArea *GetFindAreaInCxt    PROTO((tOspfCxt * pOspfCxt, tAreaId *pAreaId));

PUBLIC tHost *GetFindHostInCxt    PROTO((tOspfCxt * pOspfCxt,
                                         tIPADDR *pHostIpAddr));

PUBLIC tExtRoute *GetFindExtRouteInCxt 
                                PROTO((tOspfCxt * pOspfCxt,
                                       tIPADDR *pExtRouteDest,
                                       tIPADDR *pExtRouteMask));
PUBLIC tMd5AuthkeyInfo  *GetFindIfAuthkeyInfoInCxt 
                    PROTO((tOspfCxt * pOspfCxt,
                           tIPADDR pIfIpAddr, 
                           UINT4 u4AddrlessIf,
                           UINT1 u1AuthkeyId));
PUBLIC tInterface *GetFindIfInCxt PROTO((tOspfCxt * pOspfCxt,
                                         tIPADDR pIfIpAddr, 
                                       UINT4 u4AddrlessIf));

PUBLIC tMd5AuthkeyInfo *GetFindVirtIfAuthkeyInfoInCxt
                                PROTO((tOspfCxt * pOspfCxt,
                                       tAreaId *pTransitAreaId, 
                                       tRouterId *pNbrId,
                                       UINT1 u1AuthkeyId));
PUBLIC tInterface *GetFindVirtIfInCxt
                                PROTO((tOspfCxt * pOspfCxt,
                                       tAreaId *pTransitAreaId, 
                                       tRouterId *pNbrId));
PUBLIC tNeighbor *GetFindNbrInCxt PROTO((tOspfCxt * pOspfCxt,
                                    tIPADDR *pNbrIpAddr, 
                                       UINT4 u4NbrAddrlessIf));
PUBLIC INT1 GetFindAreaAggrIndex PROTO ((tArea *pArea,
                     tIPADDR *pIpAddr, tIPADDR *pIpAddrMask, UINT1 u1LsdbType,
       INT1 *pReturnIndex));


PUBLIC tNeighbor *HpAssociateWithNbr
                                PROTO(( tIPADDR *pSrcIpAddr,
                                        tRouterId *pHeaderRtrId,
                                        tInterface *pInterface));


PUBLIC tRedistrConfigRouteInfo *GetFindRrdConfRtInfoInCxt
                               PROTO((tOspfCxt * pOspfCxt,
                                      tIPADDR  *pRrdDestIPAddr,
                                      tIPADDRMASK *pRrdDestAddrMask));

PUBLIC tRedistrConfigRouteInfo *FindBestRrdConfRtInfoInCxt
                               PROTO((tOspfCxt * pOspfCxt,
                                      tIPADDR *pDestId,
                                      tIPADDRMASK *pDestMask));
PUBLIC tRedistrConfigRouteInfo * FindNextBestRrdConfRtInfoInCxt
                       PROTO ((tOspfCxt * pOspfCxt,
                   tIPADDR * pDestId, tIPADDRMASK * pDestMask,
             tRedistrConfigRouteInfo *pRrdConfRtInfo));

/************************** osrtm.c **************************************/

PUBLIC INT4 OspfRtmInitInCxt              PROTO((tOspfCxt * pOspfCxt));
PUBLIC VOID
OspfUpdateDistanceForExitRoutes PROTO ((tOspfCxt   *pOspfCxt));
PUBLIC VOID  OspfProcessRtmRts       PROTO ((VOID));

PUBLIC VOID RtmMsgHandler            PROTO((tOsixMsg * pRtmMsg));

PUBLIC INT4 RtmTxRtUpdate            PROTO((tOspfCxt * pOspfCxt,
                                            tRtEntry *pRtEntry));

PUBLIC INT4 RtmTxRtChngNtf           PROTO((tOspfCxt * pOspfCxt,
                                            tRtEntry *pRtEntry,
                                            UINT1       u1BitMask));
PUBLIC INT4 RtmTxNextHopUpdate        PROTO ((tOspfCxt *pOspfCxt, 
                                              tRtEntry * pRtEntry , 
                                              tPath *pCurrPath,
                                               UINT1 u1HopIndex ));

PUBLIC INT4 RtmTxRtChngNextHopNtf PROTO ((tOspfCxt *pOspfCxt,
                                          tRtEntry * pRtEntry,
                                          UINT1 u1HopIndex,
                                          tPath *pCurrPath, UINT1 u1BitMask));

PUBLIC INT4 RtmDeRegisterInCxt       PROTO((tOspfCxt * pOspfCxt));

PUBLIC INT4 RtmTxRedistributeEnableInCxt  PROTO((tOspfCxt * pOspfCxt, 
                                                 UINT4 u4SrcProtoBitMask,
                                                 UINT1 *pu1RMapName));
PUBLIC INT4 RtmSrcProtoDisableInCxt       PROTO((tOspfCxt * pOspfCxt,
                                                 UINT4 u4SrcProtoBitMask));
PUBLIC INT4 RtmSetRRDConfigRecordInCxt PROTO ((tOspfCxt * pOspfCxt,
                                               tIPADDR *pRrdDestIPAddr,
                 tIPADDRMASK *pRrdDestAddrMask));

PUBLIC INT4 RtmDelRRDConfigRecordInCxt PROTO ((tOspfCxt * pOspfCxt,
                                               tIPADDR *pRrdDestIPAddr,
               tIPADDRMASK *pRrdDestAddrMask));
PUBLIC INT4 RtmGrNotifInd PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4
RtmTxRedistributeDisableInCxt (tOspfCxt * pOspfCxt, UINT4 u4SrcProtoBitMask,
                               UINT1 *pu1RMapName);

PUBLIC VOID RtmProcessRtUpdateInCxt PROTO ((tOspfCxt * pOspfCxt,
                                             tRtmRtUpdateMsg * pRtmRtUpdate));

/****************************osfsip.c *****************************************/
PUBLIC UINT4 OspfGetIfIndexFromPort (UINT4 u4Port);


PUBLIC UINT1 OspfFilterRouteSourceInCxt PROTO ((tOspfCxt *pOspfCxt,
                                  tNetIpv4RtInfo * pNetIpRtInfo));


PUBLIC  INT1  
     OspfNbrLlDownInCxt  PROTO ((tOspfCxt * pOspfCxt,
                                 UINT4 u4OspfNbrIpAddr, UINT4 u4AddrlessIf));
/************************** osopqlsa.c ****************************************/
PUBLIC tLsaInfo* olsGetType9OpqLsaInCxt
                       PROTO((tOspfCxt * pOspfCxt,
                              UINT4 u4FutOspfType9LsdbIfIpAddress,
                              UINT4 u4FutOspfType9LsdbIfAddressLessIf,
                              UINT4 u4FutOspfType9LsdbLsid,
                              UINT4 u4FutOspfType9LsdbRouterId));

PUBLIC tLsaInfo* olsGetType11OpqLsaInCxt
                      PROTO((tOspfCxt * pOspfCxt,
                                INT4 i4FutOspfType11LsdbOpaqueType,
                                UINT4 u4FutOspfType11LsdbLsid,
                                UINT4 u4FutOspfType11LsdbRouterId));
PUBLIC INT1 GetIsNextType11LsaTableIndex
                             PROTO(( tLINKSTATEID *pCurrLinkStateId,
                                  UINT1           u1LsIdLen,
                                  tRouterId     *pCurrAdvRtrId,
                                  UINT1           u1AdvRtrIdLen,
                                  tLsaInfo      *pLsaInfo));

/*************************** osappif.c ****************************************/

PUBLIC VOID OspfOpqLSAFree     PROTO ((tOpqLSAInfo* pOpqLSAInfo));
PUBLIC INT4 OpqChkAndChangeStatusInCxt      PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4 OpqAppDeRegMsgFrmQInCxt PROTO ((tOspfCxt * pOspfCxt,
                                            UINT1 u1AppOpqtype ));
PUBLIC INT4 OspfDeRegMsgToOpqApp PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4 OpqAppRegMsgFrmQInCxt   PROTO ((tOspfCxt * pOspfCxt,
                                            UINT1 u1AppOpqtype ));

PUBLIC tOspfQMsg * OpqOspfIfMsgAlloc    PROTO((VOID));
PUBLIC INT4 OpqOspfIfMsgSend      PROTO((tOspfQMsg  *pOspfQMsg));
PUBLIC VOID AppOspfIfHandler   PROTO((tAppOspfIfParam   *pAppOspfIfParam));
PUBLIC INT4 MsgFrmAPPQSndToOpqMod PROTO (( tOpqLSAInfo    * pOpqLSAInfo));
PUBLIC VOID opqStatusChngInCxt (tOspfCxt * pOspfCxt);
PUBLIC VOID OspfGiveType9LsaToAppInCxt PROTO((tOspfCxt * pOspfCxt,
                                              UINT1 u1AppOpqType));
PUBLIC VOID OspfGiveType10LsaToAppInCxt PROTO((tOspfCxt * pOspfCxt,
                                               UINT1 u1AppOpqType));
PUBLIC VOID OspfGiveType11LsaToAppInCxt PROTO((tOspfCxt * pOspfCxt,
                                               UINT1 u1AppOpqType));
PUBLIC VOID OspfFlushType9LsaInCxt PROTO((tOspfCxt * pOspfCxt,
                                          UINT1 u1AppOpqType));
PUBLIC VOID OspfFlushType10LsaInCxt PROTO((tOspfCxt * pOspfCxt,
                                           UINT1 u1AppOpqType));
PUBLIC VOID OspfFlushType11LsaInCxt PROTO((tOspfCxt * pOspfCxt,
                                           UINT1 u1AppOpqType));
PUBLIC VOID OspfDeleteType9Lsas PROTO ((tInterface * pInterface));

PUBLIC INT1 IsOpqApplicationRegisteredInCxt (tOspfCxt * pOspfCxt);

PUBLIC VOID AsbrSendAsbrInfoToOpqApp (tOspfCxt * pOspfCxt, UINT4 u4AsbrStatus);
/**************************************************************************/

/************************** osrbutil.c **************************************/
PUBLIC INT4 RbCompareLsa PROTO ((tRBElem * e1, tRBElem * e2));
PUBLIC INT4 RbCompareNbr PROTO ((tRBElem * e1, tRBElem * e2));
INT4 RbWalkHandleLsuSplAgeTmrAction PROTO ((tRBElem * e, eRBVisit visit, UINT4 level, void *arg, void *out));
INT4 RbWalkIndicationLsa PROTO ((tRBElem * e, eRBVisit visit, UINT4 level, void *arg, void *out));
INT4 RbWalkAndRestartSummaryDnaLsa PROTO ((tRBElem * e, eRBVisit visit, UINT4 level, void *arg, void *out));
INT4 RbWalkAndRestartIndicationLsa PROTO ((tRBElem * e, eRBVisit visit, UINT4 level, void *arg, void *out));
/**************************************************************************/
/************************** osnrlsu.c ***************************************/
PUBLIC VOID
NrLsuAddToRxmtLst PROTO ((tNeighbor * pNbr, tLsaInfo * pLsaInfo));
PUBLIC VOID
NrLsuRxmtLsu PROTO ((tNeighbor * pNbr));
PUBLIC VOID
NrLsuDeleteFromRxmtLst PROTO ((tNeighbor * pNbr, tLsaInfo * pLsaInfo));
PUBLIC VOID
NrLsuDeleteNodeFromAllRxmtLst PROTO ((tLsaInfo * pLsaInfo));
PUBLIC VOID
NrLsuClearRxmtLst PROTO ((tNeighbor * pNbr));
PUBLIC VOID
NrLsuFindAndProcessAckFlag PROTO ((tNeighbor *pNbr, tLsaInfo *pLsaInfo,
                                   UINT1 *pLsa, tLsHeader * pRcvdLsHeader));
PUBLIC VOID
NrLsuDeleteFromAllRxmtLst PROTO ((tLsaInfo * pLsaInfo));
PUBLIC  VOID
NrLsuCheckAndDelLsaFromRxmtLst PROTO((tNeighbor * pNbr, tLsaInfo *pLsaInfo));
PUBLIC  INT4
NrLsuCheckInRxmtLst PROTO((tNeighbor * pNbr, tLsaInfo *pLsaInfo));
PUBLIC VOID
NrLsuInitialiseLsaRxmtInfo PROTO ((tLsaInfo *pLsaInfo));
/**************************************************************************/

/************************** oshrlsu.c ***************************************/
#ifdef HIGH_PERF_RXMT_LST_WANTED
PUBLIC INT4 RbCompareRxmtInfo PROTO ((tRBElem * e1, tRBElem * e2));
PUBLIC VOID
HrLsuAddToRxmtLst PROTO ((tNeighbor * pNbr, tLsaInfo * pLsaInfo));
PUBLIC VOID
HrLsuRxmtLsu PROTO ((tNeighbor * pNbr));
PUBLIC VOID
HrLsuDeleteFromRxmtLst PROTO ((tNeighbor * pNbr, tLsaInfo * pLsaInfo));
PUBLIC VOID
HrLsuDeleteNodeFromAllRxmtLst PROTO ((tLsaInfo * pLsaInfo));
PUBLIC VOID
HrLsuClearRxmtLst PROTO ((tNeighbor * pNbr));
PUBLIC VOID
HrLsuDeleteFromAllRxmtLst PROTO ((tLsaInfo * pLsaInfo));
PUBLIC VOID
HrLsuFindAndProcessAckFlag PROTO ((tNeighbor *pNbr, tLsaInfo *pLsaInfo,
                                   UINT1 *pLsa, tLsHeader * pRcvdLsHeader));
PUBLIC  VOID
HrLsuCheckAndDelLsaFromRxmtLst PROTO((tNeighbor * pNbr, tLsaInfo *pLsaInfo));
PUBLIC INT4
HrLsuCheckInRxmtLst PROTO((tNeighbor * pNbr, tLsaInfo *pLsaInfo));
PUBLIC VOID
HrLsuInitialiseLsaRxmtInfo PROTO ((tLsaInfo *pLsaInfo));

PUBLIC VOID
HrLsuLsaRxmtIndexDllDel PROTO ((tLsaInfo * pLsaInfo, tRxmtNode * pRxmtNode));
PUBLIC VOID
HrLsuNbrRxmtIndexDllDel PROTO ((tNeighbor * pNbr, tRxmtNode * pRxmtNode));
PUBLIC VOID HrLsuRxmtNodeMemFree PROTO ((tRxmtNode * pRxmtNode));
#endif

/**************************************************************************/

/************************** osgr.c ****************************************/
UINT4       GrGetSecondsSinceBase (tUtlTm utlTm);
PUBLIC INT4 GrSetRestartSupport
PROTO ((tOspfCxt * pOspfCxt, UINT1 u1RestartSupport));
PUBLIC INT4 GrSetGraceperiod
PROTO ((tOspfCxt * pOspfCxt, UINT4 u4GracePeriod));
PUBLIC INT4 GrSetGraceAckState
PROTO ((tOspfCxt * pOspfCxt, UINT4 u4GraceAckState));
PUBLIC INT4 GrSetGraceLsaTxCount
PROTO ((tOspfCxt * pOspfCxt, UINT1 u1GrLsaMaxTxCount));
PUBLIC INT4 GrSetGrRestartReason
PROTO ((tOspfCxt * pOspfCxt, UINT1 u1RestartReason));
PUBLIC INT4 GrSetHelperGrTimeLimit
PROTO ((tOspfCxt * pOspfCxt, UINT4 u4HelperGrTimeLimit));
PUBLIC INT4 GrInitiateRestart
PROTO ((UINT1 u1RestartType, UINT1 u1RestartReason));
PUBLIC INT4 GrCheckRxmtLst
PROTO ((tOspfCxt * pOspfCxt));
PUBLIC INT4 GrDeleteRxmtLst
PROTO ((tNeighbor * pNbr, tLsaInfo * pGrLsaInfo));
PUBLIC VOID GrCheckAndShutdownProcess PROTO ((VOID));
PUBLIC INT4 GrDisableInCxt PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID GrRestoreGlobalRestartInfo PROTO ((VOID));
PUBLIC VOID GrRestoreCxtRestartInfo PROTO ((tOspfCxt * pOspfCxt));
PUBLIC VOID GrRestoreIntfRestartInfo PROTO ((tInterface * pInterface,
                                             tLSASEQNUM * seqNum));
PUBLIC INT4 GrExitGracefultRestart PROTO ((tOspfCxt * pOspfCxt,
                                           UINT1 u1ExitReason));
PUBLIC INT4 GrLsuCheckLsaConsistency
PROTO ((tOspfCxt * pOspfCxt, tArea * pArea, tLsHeader * pLsHeader,
        UINT1 * pLsa));
PUBLIC INT1 GrLsuProcessRcvdSelfOrgLsa
PROTO ((tLsHeader *pLsHeader, tLsaInfo *pLsaInfo,
        UINT1 *pLsa, UINT2 u2Len, tNeighbor *pNbr));
PUBLIC VOID GrDeleteLsaFromDatabase PROTO ((tLsaInfo * pLsaInfo));
PUBLIC VOID
PROTO (GrCheckAndExitGracefulRestart(tOspfCxt *pOspfCxt));
PUBLIC VOID GrShutdownProcess (VOID);
PUBLIC INT4 GrUtilConstructGraceLSA (tOspfCxt * pOspfCxt,
                                     tInterface * pInterface,
                                     tOpqLSAInfo * pOpqLSAInfo,
                                     UINT1 u1LsaStatus);
PUBLIC VOID  GrDeleteCxt (tOspfCxt * pOspfCxt);
/**************************************************************************/
/**********************oshelper.c******************************************/
PUBLIC INT4  GrExitHelper(UINT1 u1ExitReason, tNeighbor *pNbr);
PUBLIC INT4 
 GrHelperProcessGraceLSA(tNeighbor *pNbr, tLsHeader *pHeader, UINT1 *pLsa);
PUBLIC INT4 GrFindTopologyChangeLSA(tLsaInfo *pLsa1, UINT1  *pLsa2);
PUBLIC INT4 CheckRtLSAForTopologyChg(tLsaInfo *pLsa1, UINT1  *pLsa2);
PUBLIC INT4 CheckNwLSAForTopologyChg(tLsaInfo *pLsa1, UINT1  *pLsa2);
PUBLIC INT4 CheckSumLSAForTopologyChg(tLsaInfo *pLsa1, UINT1  *pLsa2);
PUBLIC INT4 CheckExtLSAForTopologyChg(tLsaInfo *pLsa1, UINT1  *pLsa2);
PUBLIC INT4 GrCompareRouterLsaEntry(tRBElem * e1, tRBElem * e2);
PUBLIC INT4 GrCompareNetworkLsaEntry(tRBElem * e1, tRBElem * e2);
PUBLIC INT4 GrSetHelperSupport
PROTO ((tOspfCxt * pOspfCxt, UINT1 u1HelperSupport));
PUBLIC INT4 GrSetStrictLsaCheck
PROTO ((tOspfCxt * pOspfCxt, UINT1 u1StrictLsaCheck));

/******************************************************************/
PUBLIC VOID  OspfRmProcessRmMsg (tOsRmMsg  *ospfRmIfParam);
PUBLIC VOID  OspfRmProcessBulkReq (VOID);
PUBLIC VOID  OspfRmInit(VOID);
PUBLIC VOID  OspfRmSnmpSendNodeStatusChgTrap (INT4);
PUBLIC INT4  OsRmBulkUpdtRelinquish PROTO ((VOID));

PUBLIC INT4   OspfRmRegisterWithRM (VOID);
PUBLIC INT4   OspfRmDeRegisterWithRM (VOID);
PUBLIC INT4   OspfRmRelRmMsgMem (UINT1 *pu1Block);
PUBLIC tRmMsg * OspfRmAllocForRmMsg (UINT2 u2Size);
PUBLIC UINT1  OspfRmGetStandbyNodeCount (VOID);
PUBLIC INT4   OspfRmSendMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen);
PUBLIC INT4   OspfRmSendEventToRm (tRmProtoEvt * pEvt);
PUBLIC VOID   OspfRmSetBulkUpdateStatus (VOID);
PUBLIC VOID   OspfRmApiSendProtoAckToRM (UINT4);
PUBLIC UINT4  OspfRmGetRmNodeState(VOID);
PUBLIC VOID   OspfRmSendBulkAbort(UINT4);
PUBLIC INT4   OspfRmRtrEnableInCxt (tOspfCxt *pOspfCxt);

PUBLIC INT4   OspfRmProcessHello (tRmMsg *, UINT4 *pu4Offset);
PUBLIC INT4   OspfRmProcessBulkIfState (tRmMsg *);
PUBLIC INT4   OspfRmProcessBulkNbrState(tRmMsg *, UINT4 *pu4Offset);

PUBLIC VOID   OspfRmSendBulkIfState (VOID);
PUBLIC VOID   OspfRmSendBulkVirtNbrInfo PROTO ((VOID));
PUBLIC VOID   OspfRmSendBulkNbrInfo PROTO((VOID));
PUBLIC INT4   OspfRmSendBulkUpdate PROTO((tRmMsg *pBuf, UINT4 u4PktLen));
PUBLIC INT4   OspfRmAllocateBulkUpdatePkt PROTO((tRmMsg **ppBuf,
                                                 UINT4 *pu4Offset));
PUBLIC INT4   OspfRmSendNbrInfo PROTO((tInterface * pInterface, 
                                       tRmMsg **pRmBuf,  UINT4 *pu4Offset));
PUBLIC VOID   OspfRmProcessNbrInfo PROTO ((tRmMsg * pMsg));
PUBLIC VOID   OspfRmProcessBulkLsuInfo (tRmMsg *, UINT2 );
PUBLIC VOID   OspfRmSendBulkLsaInfo(VOID);
PUBLIC VOID   OspfRmAddToRxmtLst (tLsaInfo *, tNeighbor *);

PUBLIC INT4  OspfRmProcessIfEvent (tRmMsg *);
PUBLIC VOID  OspfRmProcessNbrStateChange (tRmMsg *);
PUBLIC VOID  OspfRmProcessMd5SeqNo (tRmMsg * pMsg);
PUBLIC VOID  OspfRmSendHello (tCRU_BUF_CHAIN_HEADER *, UINT2, tInterface *,
                              tNeighbor *, tIPADDR *);
PUBLIC VOID  OspfRmSendIfStateChangeEvent (tOspfIpIfParam *);
PUBLIC VOID  OspfRmSendNbrStateChange (UINT4, tNeighbor *, UINT1, UINT4);
PUBLIC VOID   OspfRmSendMd5SeqNo (tInterface * pInterface);
PUBLIC INT4  OspfRmSendLsuInfo (UINT1 *, tNeighbor *);
PUBLIC VOID  OspfRmProcessLsuInfo (tRmMsg *, UINT2);
PUBLIC VOID  OspfRmSendLsAckInfo (tLsaInfo *);
PUBLIC VOID  OspfRmProcessLsAckInfo (tRmMsg *);
PUBLIC VOID  OspfRmInitNbrBulkUpdateFlags (VOID);
PUBLIC VOID  OspfRmInitVirtNbrBulkUpdateFlags (VOID);
PUBLIC VOID  OspfRmInitLsdbBulkUpdateFlags (VOID);
PUBLIC VOID  OspfRmSendIfState(tInterface * pInterface, UINT4 u4BitMask);
PUBLIC INT4  OspfRmProcessIfState (tRmMsg * pRmMesg, UINT4 *pu4Offset);
PUBLIC VOID  OspfRmConstructIfState (tRmMsg * pMsg, UINT4 *pu4Offset, 
                                     tInterface * pInterface, UINT4 u4BitMask);
PUBLIC VOID  OspfRmConstructHello (tRmMsg * pMsg, UINT4 *pu4Offset,
                                   tNeighbor * pNbr);
PUBLIC INT4  OspfRmSendIfInfo (tInterface * pInterface, 
                               tRmMsg **pRmBuf,  UINT4 *pu4Offset);
PUBLIC INT4  OsRmDynLsSendSelfOrigLsa (tLsaDesc * pLsaDesc);
PUBLIC INT4  OspfRmProcessLSA (tRmMsg * pRmMsg, UINT4 * pu4Offset);
PUBLIC BOOL1 OspfRmConstructBulkLsaMsg (tLsaInfo *, tRmMsg **, UINT2 *);
PUBLIC INT4  OsRmDynLsSendLsa (tLsaInfo * pLsaInfo);
PUBLIC BOOL1 OspfRmConstructLsaMsg (tLsaInfo * pLsaInfo, tRmMsg ** pPMsg,
                                    UINT2 *pu2MsgLen);
PUBLIC VOID  OspfRmSendBulkExtRouteInfo (VOID);
PUBLIC INT4  OspfRmSendExtRtInfo (tOspfCxt *pOspfCxt, tExtRoute * pExtRoute,
                                  tRmMsg **ppRmMsgBuf,  UINT4 *pu4Offset);
PUBLIC VOID  OspfRmConstructExtRt (tOspfCxt *pOspfCxt, tRmMsg * pMsg,
                                   UINT4 *pu4Offset, tExtRoute *pExtRoute);
PUBLIC VOID  OspfRmProcessBulkExtRouteInfo (tRmMsg * pMsg);
tOpqLSAInfo *
OspfRmConstOpqLsaAssocPrimitive (tOspfCxt *pOspfCxt, tRmMsg *pRmMsg,
                                 UINT4 *pu4OffSet, UINT1 *pLsa,
                                 tLsHeader * pLsHeader);
PUBLIC VOID  OspfRmSendBulkTmrInfo (VOID);
PUBLIC VOID  OspfRmSendBulkDefCxtInfo (VOID);

PUBLIC VOID  OspfRmProcessTmrInfo (tRmMsg * pMsg);
tMd5AuthkeyInfo *
GetAuthkeyTouse (tInterface * pInterface);

INT4
OspfRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));
INT4
OspfGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum);
INT4
OspfCliGetShowCmdOutputToFile (UINT1 *);
INT4
OspfCliCalcSwAudCheckSum (UINT1 *, UINT2 *);
INT1
OspfCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);
VOID OspfRmHandleDynSyncAudit (VOID);
VOID OspfExecuteCmdAndCalculateChkSum  (VOID);

/**************************************************************************/

/**************************************************************************/

#ifdef RAWSOCK_WANTED
UINT2 OspfIpCalcCheckSum (UINT1 *pBuf, UINT4 u4Size);
VOID OspfPacketOnSocket (INT4 i4SockFd);
#endif
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
VOID OspfPacketOnNLHelloSocket (INT4 i4SockFd);
#endif
#endif
PUBLIC VOID OspfVcmCallbackFn PROTO ((UINT4 u4IpIfIndex, 
                                      UINT4 u4VcmCxtId, UINT1 u1BitMap));
PUBLIC VOID VcmMsgHandler PROTO ((tOspfVcmInfo  * pOspfVcmInfo));
tLsaInfo* GetFirstLsaBytype PROTO ((UINT4 u4OspfCxtId, UINT4 u4AreaId,
                                    INT4 i4Lsatype));

tLsaInfo* GetNextLsaBytype PROTO ((UINT4 u4OspfCxtId , UINT4 u4AreaId,tLsaInfo *pFirstLsaInfo, INT4 i4Lsatype));
extern tIssBool MsrGetRestorationStatus PROTO ((VOID));
extern tIssBool MsrGetSaveStatus PROTO ((VOID));
#endif /* _OSPROT_H */ 
