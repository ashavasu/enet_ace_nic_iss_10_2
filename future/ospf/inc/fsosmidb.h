/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsosmidb.h,v 1.5 2009/06/04 05:35:31 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOSMIDB_H
#define _FSOSMIDB_H

UINT1 FsMIOspfBRRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfExtRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfGrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsosmi [] ={1,3,6,1,4,1,2076,147};
tSNMP_OID_TYPE fsosmiOID = {8, fsosmi};


UINT4 FsMIOspfBRRouteIpAddr [ ] ={1,3,6,1,4,1,2076,147,1,1,1};
UINT4 FsMIOspfBRRouteIpAddrMask [ ] ={1,3,6,1,4,1,2076,147,1,1,2};
UINT4 FsMIOspfBRRouteIpTos [ ] ={1,3,6,1,4,1,2076,147,1,1,3};
UINT4 FsMIOspfBRRouteIpNextHop [ ] ={1,3,6,1,4,1,2076,147,1,1,4};
UINT4 FsMIOspfBRRouteDestType [ ] ={1,3,6,1,4,1,2076,147,1,1,5};
UINT4 FsMIOspfBRRouteType [ ] ={1,3,6,1,4,1,2076,147,1,1,6};
UINT4 FsMIOspfBRRouteAreaId [ ] ={1,3,6,1,4,1,2076,147,1,1,7};
UINT4 FsMIOspfBRRouteCost [ ] ={1,3,6,1,4,1,2076,147,1,1,8};
UINT4 FsMIOspfBRRouteInterfaceIndex [ ] ={1,3,6,1,4,1,2076,147,1,1,9};
UINT4 FsMIOspfExtRouteDest [ ] ={1,3,6,1,4,1,2076,147,2,1,1};
UINT4 FsMIOspfExtRouteMask [ ] ={1,3,6,1,4,1,2076,147,2,1,2};
UINT4 FsMIOspfExtRouteTOS [ ] ={1,3,6,1,4,1,2076,147,2,1,3};
UINT4 FsMIOspfExtRouteMetric [ ] ={1,3,6,1,4,1,2076,147,2,1,4};
UINT4 FsMIOspfExtRouteMetricType [ ] ={1,3,6,1,4,1,2076,147,2,1,5};
UINT4 FsMIOspfExtRouteTag [ ] ={1,3,6,1,4,1,2076,147,2,1,6};
UINT4 FsMIOspfExtRouteFwdAdr [ ] ={1,3,6,1,4,1,2076,147,2,1,7};
UINT4 FsMIOspfExtRouteIfIndex [ ] ={1,3,6,1,4,1,2076,147,2,1,8};
UINT4 FsMIOspfExtRouteNextHop [ ] ={1,3,6,1,4,1,2076,147,2,1,9};
UINT4 FsMIOspfExtRouteStatus [ ] ={1,3,6,1,4,1,2076,147,2,1,10};
UINT4 FsMIOspfGrShutdown [ ] ={1,3,6,1,4,1,2076,147,100,2,1,1};


tMbDbEntry fsosmiMibEntry[]= {

{{11,FsMIOspfBRRouteIpAddr}, GetNextIndexFsMIOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteIpAddrMask}, GetNextIndexFsMIOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteIpTos}, GetNextIndexFsMIOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteIpNextHop}, GetNextIndexFsMIOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteDestType}, GetNextIndexFsMIOspfBRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteType}, GetNextIndexFsMIOspfBRRouteTable, FsMIOspfBRRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteAreaId}, GetNextIndexFsMIOspfBRRouteTable, FsMIOspfBRRouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteCost}, GetNextIndexFsMIOspfBRRouteTable, FsMIOspfBRRouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfBRRouteInterfaceIndex}, GetNextIndexFsMIOspfBRRouteTable, FsMIOspfBRRouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfBRRouteTableINDEX, 6, 0, 0, NULL},

{{11,FsMIOspfExtRouteDest}, GetNextIndexFsMIOspfExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfExtRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfExtRouteMask}, GetNextIndexFsMIOspfExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfExtRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfExtRouteTOS}, GetNextIndexFsMIOspfExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfExtRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfExtRouteMetric}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteMetricGet, FsMIOspfExtRouteMetricSet, FsMIOspfExtRouteMetricTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfExtRouteMetricType}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteMetricTypeGet, FsMIOspfExtRouteMetricTypeSet, FsMIOspfExtRouteMetricTypeTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 0, "1"},

{{11,FsMIOspfExtRouteTag}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteTagGet, FsMIOspfExtRouteTagSet, FsMIOspfExtRouteTagTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsMIOspfExtRouteFwdAdr}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteFwdAdrGet, FsMIOspfExtRouteFwdAdrSet, FsMIOspfExtRouteFwdAdrTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsMIOspfExtRouteIfIndex}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteIfIndexGet, FsMIOspfExtRouteIfIndexSet, FsMIOspfExtRouteIfIndexTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsMIOspfExtRouteNextHop}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteNextHopGet, FsMIOspfExtRouteNextHopSet, FsMIOspfExtRouteNextHopTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsMIOspfExtRouteStatus}, GetNextIndexFsMIOspfExtRouteTable, FsMIOspfExtRouteStatusGet, FsMIOspfExtRouteStatusSet, FsMIOspfExtRouteStatusTest, FsMIOspfExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfExtRouteTableINDEX, 4, 0, 1, NULL},
};
tMibData fsosmiEntry = { 19, fsosmiMibEntry };
#endif /* _FSOSMIDB_H */

