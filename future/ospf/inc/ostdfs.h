/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ostdfs.h,v 1.57 2015/12/15 10:38:32 siva Exp $
 *
 * Description:This file contains type definitions relating$
 *             to protocol.
 *
 *******************************************************************/

#ifndef _OSTDFS_H
#define _OSTDFS_H


#include "osrm.h"
/****************************************************************************
 * BASIC TYPE DEFINITIONS *
 ****************************************************************************/

typedef UINT1           tIPADDRMASK[MAX_IP_ADDR_LEN];
typedef tIPADDR         tAreaId;
typedef UINT1           tOPTIONS;
typedef UINT1           tAUTHENTICATION[AUTH_KEY_SIZE + 1];
                            /* 64 bit authentication key */

/****************************************************************************
 * LSA Related DEFINITIONS *
 ****************************************************************************/

typedef INT4            tLSASEQNUM;
typedef UINT4           tRXMTFLAG;
                            /* Used as bit map for handling LSA
                             * retransmission to the Neighbors
                             */
typedef tCRU_INTERFACE  tInterfaceId;

/****************************************************************************
 * MIB Related DEFINITIONS *
 ****************************************************************************/

typedef UINT4           tTRUTHVALUE;
                            /* Structure to hold a Boolean value
                             * '1' - If true
                             * '2' - If false
                             */
typedef UINT4           tOSPFSTATUS;
                            /* Structure to hold the general status
                             * '1' - If enabled
                             * '2' - If disabled
                             */
#define  OSPF_SUCCESS           0
#define  OSPF_FAILURE          -1
#define  ALLOC_FAILURE         -1
#define  OSPF_CHKSUM_FAILURE   -2

typedef UINT1           tVALIDATION;
                            /* Structure to hold a Boolean value indicating
                             * whether a route metric is VALID/INVALID
                             */
typedef UINT1           tROWSTATUS;
                            /* Structure to hold the different row status
                             * value as defined in RFC 1443
                             */
typedef UINT1           tROWMASK;
                            /* Structure to indicate the mask for tables
                             * having critical variables
                             */
typedef UINT2           COUNTER16;

/* ------------------------------------------------------- */
/* OSPFV2 prefix LSA Hash node structure                   */
/* ------------------------------------------------------- */
typedef struct _OsDbNode {
    tTMO_HASH_NODE    NextDbNode;   
                         /* Points to next DB hash node   */
    tTMO_SLL          lsaLst;  
                         /* List holding the LSA's of Same Prefix */
} tOsDbNode;

/****************************************************************************
 * RT-Calculation Related DEFINITIONS *
 ****************************************************************************/

typedef tIPADDR         tDestId;
typedef tRouterId       tVertexId;
                            /* Structure to hold an IP address of a vertex in
                             * the spanning tree
                             */
typedef tTMO_HASH_TABLE tSpf;
                            /* Spanning Tree structure. This has been mapped
                             * to the TMO hash table in this implementation
                             */
typedef tTMO_HASH_TABLE tCandteLst;
                            /* List of candidates during RT calculation. This
                             * has been mapped to the TMO hash table in this
                             * implementation
                             */


/****************************************************************************/
/* Metric related.                                                          */
/****************************************************************************/

/* type definitions related to metrics */
typedef struct _Metric {
    UINT4               u4Value;
                            /* Indicates the metric cost
                             */
    UINT4               u4MetricType;                             
    tVALIDATION         bStatus;
                            /* Validity of the metric cost
                             */
    UINT1               au1Rsvd[3];
                           /* Included for 4-byte Alignment
                            */
} tMetric;

typedef struct _MibMetric {
    UINT4               u4Value;
    UINT4               u4MetricType;    
    tROWSTATUS          rowStatus;
                            /* Row status of the metric entry
                             */
    tROWMASK            rowMask;
    UINT1               au1Rsvd[2];
                           /* Included for 4-byte Alignment
                            */
} tMibMetric;


/****************************************************************************/
/* Buffer QID Related.                                                      */
/****************************************************************************/

/* structure which holds the queue ids for the various buffer pools */

typedef struct _BufQid {
    INT4                i4LsaQid;
                            /* LSA structures
                             */
} tBufQid;

/****************************************************************************/
/* Related to Configuaration variables.                                     */
/****************************************************************************/

typedef struct _CnfgInfoRec {
    UINT4               u4MaxAppReg;
                            /* Maximum number of Opaque LSA triggering
                             * applications  that can be registered to OSPF
                             */
    UINT4               u4MaxOpqLSAs;
                            /* Maximum number of Opaque LSAs
                             */
    UINT4               u4MaxIfsPerArea;
                            /* Maximum number of interfaces that can be
                             * associated with an area
                             */
    UINT4               u4MaxIfs;
                            /* Maximum number of interfaces that can be
                             * handled by the Router
                             */
    UINT4               u4MaxLsas;
                            /* Maximum number of LSAs that can be handled by
                             * the route
                             */
    UINT4               u4MaxPaths;
                            /* Maximum number of paths that can be associated
                             * with a route
                             */
    UINT2               u2MaxLsasPerArea;
                            /* Maximum number of LSAs that can be associated
                             * with an area
                             */
    UINT2               u2MaxNbrs;
                            /* Maximum number of neighbors that can be
                             * handled by the Router
                             */
    UINT2               u2MaxNbrsPerIface;
                            /* Maximum number of neighbors that can be
                             * associated with an interface
                             */
    UINT2               u2MaxExtLsas;
                            /* Maximum number of external LSAs that can be
                             * handled by the router
                             */
    UINT2               u2MaxSelfOrgLsas;
                            /* Maximum number of Self originated LSAs that
                             * can be handled by the router
                             */
    UINT2               u2MaxSelfOrgSumLsas;
                            /* Maximum number of Self originated summary LSAs
                             * that can be handled by the router
                             */
    UINT2               u2MaxRoutes;
                            /* Maximum number of routes that can be handled
                             * by the router
                             */
    UINT2               u2MaxNextHops;
                            /* Maximum number of next hops that can be had
                             * for a route
                             */
    UINT2               u2MaxExtRoutes;
                            /* Maximum number of external routes that can be
                             * handled by the router
                             */
    UINT1               u1MaxAddrRngsPerArea;
                            /* Maximum number of address ranges that can be
                             * associated with an area
                             */
    UINT1               u1MaxAreas;
                            /* Maximum number of areas that can be handled by
                             * the Router
                             */
    UINT1               u1MaxHosts;
                            /* Maximum number of hosts that can be handled by
                             * the Router
                             */
    UINT1               u1MaxVirtualIfs;
                            /* Maximum number of Virtual interfaces that can
                             * be handled by the Router
                             */
    UINT1               au1Reserved[2];
} tCnfgInfoRec;

/****************************************************************************/
/* OSPF PACKETS                                                             */
/****************************************************************************/

/* format of ospf packet header */

typedef struct _OsHeader {
    UINT1               u1Version;
                            /* OSPF version number
                             */
    UINT1               u1Type;
                            /* OSPF packet type
                             */
    UINT2               u2Len;
                            /* length of packet in bytes
                             */
    tRouterId           rtrId;
                            /* router id of the packet's source
                             */
    tAreaId             areaId;
                            /* area id the packet belongs toi
                             */
    UINT2               u2Chksum;
                            /* standard IP checksum of the entire contents of
                             * the packet excluding 64-bit authentication
                             * field
                             */
    UINT2               u2AuthType;
                            /* identifies the authentication scheme used
                             */
    tAUTHENTICATION     authentication;
                            /* 64 bit authentication field
                             */
    UINT1               au1Reserved[3]; 
}  tOsHeader;

/* format of hello packet */

typedef struct _HelloStaticPortion {
    tIPADDRMASK         networkMask;
                            /* network mask associated with this interface
                             */
    UINT2               u2HelloInterval;
                            /* the interval between this router's hello packets
                             */
    tOPTIONS            options;
                            /* optional capabilities supported by this router
                             */
    UINT1               u1RtrPriority;
                            /* this router's priority used in DR election
                             */
    INT4                i4RtrDeadInterval;
                            /* the number of seconds before declaring a
                             * silent rtr dead
                             */
    tIPADDR             desgRtr;
                            /* the designated router in view of the
                             * advertising router
                             */
    tIPADDR             backupDesgRtr;
                            /* the backup designated router in advertising
                             * router's view
                             */
}  tHelloStaticPortion;

/* format of link state advertisement header */

typedef struct _LsHeader {
    UINT2               u2LsaAge;
                            /* the time in seconds since the link state advt
                             * was originated
                             */
    tOPTIONS           lsaOptions;
                            /* optional capabilities supported by the
                             * described portion of the routing domain
                             */
    UINT1               u1LsaType;
                            /* the type of the link state advt.
                             */
    tLINKSTATEID        linkStateId;
                            /* this field identifies the portion of the
                             * internet being described by this advertisement
                             */
    tRouterId           advRtrId;
                            /* the router ID of the router that originated
                             * the advt.
                             */
    tLSASEQNUM          lsaSeqNum;
                            /* identifies the instance of the advertisement
                             */
    UINT2               u2LsaChksum;
                            /* the checksum of the complete contents of the
                             * link state advt
                             */
    UINT2               u2LsaLen;
                            /* length in bytes of the advt.
                             * including the 20 -byte header
                             */
}  tLsHeader;


/****************************************************************************/
/* Area Address Range (Aggregate)                                           */
/****************************************************************************/

/* address ranges structure which define areas */

typedef struct _AddrRange {
    tIPADDR             ipAddr;
                            /* From RFC - ip address
                             */
    tIPADDRMASK         ipAddrMask;
                            /* From RFC -
                             * ip address and mask together
                             * specify a range of addresses
                             */
    struct _LsaInfo     *pLsaInfo;       
    UINT4    u4LsaCount;     
    UINT4    u4RngUpdt;      
    UINT4  u4ExtRtTag; 
    
    tROWSTATUS          areaAggStatus;
                             /* From MIB -
                             * SNMP row status
                             */
  
    UINT1               u1AdvertiseStatus;
                            /* From RFC -
                             * Flag which indicates whether this address range
                             * is to be summarized or not, in LSAs.
                             * Value - 1, the address range is summarized
                             * Value - 2, the address range is not summarized
                             */
    UINT1               u1Active;
                            /* Implementation -
                             * flag which indicated whether
                             * this addr range is active or not
                             */
    UINT1               u1CalActive;
                            /* Implementation -
                             * flag which indicated whether this addr range
                             * is active or not. It is set during cal,
                             * and then copied to u1Active
                             */
    UINT1               u1LsdbType;
                            /* From MIB -
                             * This field specifes the LsdbType that
                             * area agg. applies
                             */
    tROWSTATUS          aggFlag;  
    UINT1               u1AddrRangeIndex;
                            /* Addr range index in the respective area structure
                             * This is to find the associated Area
                             */
    UINT1               u1Rsvd;
                           /* Included for 4-byte Alignment
                            */
    tMetric             aMetric[OSPF_MAX_METRIC];
                            /* Specifies the cost to be advertised during
                             * summary LSA construction. MAX_METRIC value is
                             * '1', as TOS option is deleted in 2178 and 2328.
                             */
} tAddrRange;


/****************************************************************************/
/* AREA                                                                     */
/****************************************************************************/

/* area data structure */

typedef struct _Area {
    tTMO_SLL_NODE       nextArea;
                            /* Points to the next area structure in the
                             * linked list
                             */
    struct _OspfCxt    *pOspfCxt;
                            /* Points to the current context ospf information */
    tRBTree            pSummaryLsaRoot;
                            /* From RFC -
                             * RBTree to maintain all summary LSAs generated
                             * by area border routers. They describe routes to
                             * destinations that are internal to the AS, but
                             * external to this area. The Key of RBTree is    
                             * based on LS Type, LS ID and advertising Router's ID
                             */
    tAreaId             areaId;
                            /* A 32-bit number that identifies the area.
                             * `0.0.0.0' is reserved for the backbone area
                             */
    tAddrRange          aAddrRange[MAX_ADDR_RANGES_PER_AREA];
                            /* From RFC -
                             * Each area is defined by a list of address
                             * ranges. Each address range is specified
                             * by an [address, mask] pair.
                             */
    tTMO_SLL            ifsInArea;
                            /* From RFC -
                             * A list of interface structures corresponding to
                             * the router's interfaces connecting to this area.
                             */
    tTMO_SLL            rtrLsaLst;
                            /* From RFC -
                             * A singly linked list of router LSAs generated by
                             * routers in this area. The elements are ordered
                             * based on LS ID and advertising Router's ID
                             */
    tTMO_SLL            networkLsaLst;
                            /* From RFC -
                             * A singly linked list of network LSAs belonging
                             * to this area. The elements are ordered based
                             * on LS ID and advertising Router's ID
                             */
    tTMO_SLL            Type10OpqLSALst;
                            /* Implementation -
                             * A singly linked list of type 10 Opaque LSAs.
                             * The elements are ordered based on LS ID and
                             * advertising Router's ID.
                             */
    tTMO_SLL            nssaLSALst;   

    void               *pAsExtAddrRangeTrie;
                        /* Implementation -
    * The External address range entries will be stored as 
    * Patricia Tree. Pointer to the root of the Trie instance.
    */

    tRBTree             pNbrTbl;
                            /* Implementation -
                             * Neighbors in this area are stored in the RBTree.
                             * The key for RBtree comaprison function is
                             * nbr_ipAddr on multi-access networks and
                             * nbr_rtrId on point-to-point & virtual interfaces,
        * and the interface index over which this neighbor 
        * was learned
                             */
    tTMO_HASH_TABLE    *pLsaHashTable;
                            /* Implementation -
                             * Pointer to the hash table that contains the LSAs
                             * in this area's link state database. The key is
                             * derived from the LS ID, LS type and the source
                             * router's ID.
                             */
    tTRUTHVALUE         bTransitCapability;
                            /* From RFC -
                             * indicates whether any active virtual links,
                             * in this area
                             * OSPF_TRUE when the area supports a virtual link
                             * otherwise OSPF_FALSE.
                             */
                             
    tTRUTHVALUE         bPreviousTransitCapability;
    UINT4              u4AreaType; 
    UINT4              u4AbrStatFlg; 
    
    tMibMetric          aStubDefaultCost[OSPF_MAX_METRIC];
                            /* From RFC -
                             * If this area is configured as a stub area and
                             * this router is itself an area border router, then
                             * this field specifies the cost to be advertised in
                             * the default summary link advertisement.
                             */
    UINT4               u4XchgOrLoadNbrCount;
                            /* Implementation -
                             * The number of neighboring routers in this area,
                             * which are in state exchange or loading. This is
                             * used while flushing MAX_AGE advertisements from
                             * the database. Such advertisements can be flushed
                             * only when this count is '0'
                             */
    UINT4               u4AreaBdrRtrCount;
                            /* From MIB -
                             * The number of area border routers reachable
                             * within this area. This is initially '0', and is
                             * calculated in each SPF pass.
                             */
    UINT4               u4AsBdrRtrCount;
                            /* From MIB -
                             * The number of AS boundary routers reachable
                             * within this area. This is initially '0', and is
                             * calculated in each SPF pass
                             */
    UINT4               u4AreaLsaChksumSum;
                            /* From MIB -
                             * The sum of the checksums of the LSAs in this
                             * area's database
                             */
    UINT4               u4Type10OpqLSAChksumSum;
                            /* Implementation -
                             * The sum of the checksums of the type10 opaque
                             * LSAs in the area's database
                             */
    UINT4               u4ActIntCount;
                            /* Active Interfaces count in the Area
                             */
    UINT4               u4FullNbrCount;
                            /* Number of Neighbors in the Area Which
                             * are in FULL state
                             */
    UINT4               u4FullVirtNbrCount;
                            /* Number of Virtual Neighbors in the
                             * Area which are in FULL state
                             */
    UINT4               u4NssaTrnsltrEvents;  
    
    UINT4               u4DfInfOriginate;  

    UINT4               u4DcBitResetLsaCount;
                            /* denotes the no. of DC bit reset lsas in the
                             * area's database including the indication lsa
                             */
    UINT4               u4IndicationLsaCount;
                            /* denotes the no. of indication lsas in the
                             * area's database
                             */
    tTRUTHVALUE         bIndicationLsaPresence;
                            /* indicates whether the router originated any
                             * indication lsa into this area or not
                             */
    UINT4               u4NssaTrnsltrStbltyInterval;                             
    
                           
    UINT1               u1SummaryFunctionality;
                            /* From MIB -
                             * Indicates whether the summarization will be done
                             * for the stub area or not.
                             */
    tROWSTATUS          areaStatus;
                            /* From MIB -
                             * This field indicates the row status
                             */
    UINT1               u1NssaTrnsltrRole;  
    UINT1               u1NssaTrnsltrState;  
                             
    tOspfTimer      nssaStbltyIntrvlTmr;                              
    tSpf                *pSpf;
                            /* Pointer to the SPF tree Hash Table structure
                             * resulted out of the RT calculation.
                             */
                           
    tTRUTHVALUE         bNewlyAttached; 
                            /* This flag will be set if the area is newly 
                  * created and will be reset after the route 
                  * calculation and summary LSAs generation.
                             */
    UINT2               u2SpfRuns;
                            /* From MIB -
                             * The number of times the intra-area routing table
                             * has been calculated using this area's database
                             */
    tOPTIONS            areaOptions;                     
   
    UINT1               au1Rsvd[1];                             
} tArea;

/****************************************************************************/
/* HOST                                                                     */
/****************************************************************************/

typedef struct _Host {
    tTMO_SLL_NODE       nextHost;
                            /* Pointer to the next host in the linked list.
                             */
    struct _OspfCxt    *pOspfCxt;
                            /* Points to the current context ospf information */
    tIPADDR             hostIpAddr;
                            /* Indicates the Host IP address.
                             */
    tMibMetric          aHostCost[OSPF_MAX_METRIC];
                            /* An array of cost associated with the host
                             * corresponding to the various TOS values.
                             */
    tArea              *pArea;
                            /* From MIB -
                             * Pointer to the area to which the host belongs.
                             */
    UINT4               u4FwdIfIndex;
                            /* Implementation -
                             * Interface index through which the host can be
                             * reached.
                             */
} tHost;

/****************************************************************************/
/* INTERFACE                                                                */
/****************************************************************************/

/* structure for sending LAK packets on the interface */

typedef struct _DelLsAck {
    tCRU_BUF_CHAIN_HEADER  *pAckPkt;
                               /* Pointer to the delayed Ack packet
                                */
    tOspfTimer              delAckTimer;
                               /* An interval timer whose expiry results in the
                                * transmission of delayed acknowledgement
                                */
    UINT2                   u2CurrentLen;
                               /* current length of the pkt
                                */
    UINT1                   au1Rsvd[2];
                               /* Included for 4-byte Alignment
                                */
} tDelLsAck;

/* structure for sending LSU packets on the interface */

typedef struct _LsUpdate {
    tCRU_BUF_CHAIN_HEADER  *pLsuPkt;
                               /* Pointer to the LSU packet
                                */
    UINT4                   u4LsaCount;
                               /* current count of lsas in the lsu pkt
                                */
    UINT2                   u2CurrentLen;
                               /* current length of the pkt
                                */
    UINT1                   au1Rsvd[2];
                               /* Included for 4-byte Alignment
                                */
} tLsUpdate;

/* This structure holds information associated
 * with a particular secondary IP address
 */
typedef struct _SecIp {
    tIPADDR             ifSecIpAddr;
                            /* Secondary IP address associated with the
                             * interface
                             */
    tIPADDRMASK         ifSecIpAddrMask;
                            /* Secondary IP address mask associated with the
                             * interface
                             */
    UINT1               u1Status;
                            /* Row Status of the Secondary IP Address
                             */
    UINT1               au1Rsvd[3];
                           /* Included for 4-byte Alignment
                            */
} tSecIp;

typedef struct _SecondaryIp {
    tSecIp              ifSecIp[MAX_SEC_INTERFACES];
                            /* This field stores all the Secondary IP
                             * addresses configured for the interface
                             */
    INT1                i1SecIpCount;
                            /* Number of secondary IP addresses configured
                             * for the interface
                             */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                             */
} tSecondaryIp;

/* MD5 Authentication information data structure */

typedef struct _Md5authkeyInfo {
    tTMO_SLL_NODE       nextSortKey;
                            /* next key for the interface sorted by keyid
                             */
    UINT4               u4KeyStartAccept;
                            /* The time that the router will start
                             * accepting pkt with this key
                             */
    UINT4               u4KeyStartGenerate;
                            /* The time that the router will start
                             * using this key for pkt generation
                             */
    UINT4               u4KeyStopGenerate;
                            /* The time that the router will stop
                             * using this key for pkt generation
                             */
    UINT4               u4KeyStopAccept;
                            /* The time that the router will stop
                             * accepting pkt with this key
                             */
    UINT1               authKey[MAX_AUTHKEY_LEN + 1];
                            /* The authentication key
                             */
    UINT1               u1KeyLen; 
                            /* Length of the Cryptographic authentication 
                               key
                            */
    UINT1               u1AuthkeyId;
                            /* authentication key identifier
                             */
    UINT1               u1AuthkeyStatus;
                            /* indicates status of this entry valid or invalid
                             */
                            /* Included for 4-byte Alignment
                             */
} tMd5AuthkeyInfo;


/*This enumeration is derived from the values in future/util/sha2/inc/sha2.h
 * Currently this enum is used for SHA 2 only
 */ 

typedef enum _OspfSHAversion {
    OSPF_AR_SHA1_ALGO,
    OSPF_AR_SHA224_ALGO,
    OSPF_AR_SHA256_ALGO,
    OSPF_AR_SHA384_ALGO,
    OSPF_AR_SHA512_ALGO
} tShaVersion;



/* interface data structure */

typedef struct _Interface {
    tTMO_HASH_NODE      nextIfNode;
                            /* Pointer to the next and the previous interface
                             * elements hashed on to the same hash index value
                             */
    tTMO_SLL_NODE       nextIfInArea;
                            /* Pointer to the next element in the list of
                             * interfaces belonging to this area
                             * (Maintained as a singly linked list)
                             */
    tTMO_SLL_NODE       nextSortIf;
                            /* Pointer to the next element in the sort list
                             */
    tTMO_SLL            nbrsInIf;
                            /* Pointer to linked list of neighbors associated
                             * with this interface
                             */
    tTMO_SLL            Type9OpqLSALst;
                            /* A singly linked list of type 9 Opaque LSAs. The
                             * elements are ordered based on LS ID and
                             * advertising Router's ID
                             */
    tTMO_SLL            sortMd5authkeyLst;
                            /* MD5 / SHA  Auth -
                             * List of md5 and sha authentication key information
                             * sorted by the key id
                             */
    tIPADDR             ifIpAddr;
                            /* From RFC -
                             * IP address of the interface
                             */
    tIPADDR             desgRtr;
                            /* the designated router
                             * selected for attatched network
                             */
    tIPADDR             backupDesgRtr;
                            /* the backup designated router
                             * selected for attatched network
                             */
    tIPADDRMASK         ifIpAddrMask;
                            /* From RFC -
                             * portion of the IP address that identifies
                             * the attatched network
                             */
    tAreaId             transitAreaId;
                            /* The Area ID of the area through which packets
                             * over this link are transmitted. (Valid for
                             * virtual links alone).
                             */
    tRouterId           destRtrId;
                            /* The router id at the other end point of the
                             * links. (Valid for virtual links alone).
                             */
    tDelLsAck           delLsAck;
                            /* Implementation -
                             * The Acknowledgement packet used to send delayed
                             * acknowledgements
                             */
    tLsUpdate           lsUpdate;
                            /* Implementation -
                             * The LSU pkt that is to be sent on
                             * this interface
                             */
    tSecondaryIp        secondaryIP;
                            /* secondary IPs for this interface
                             */
    tOspfTimer          helloTimer;
                            /* an interval timer which fires
                             * every hello_interval seconds
                             */
    tOspfTimer          waitTimer;
                            /* a single shot timer which causes
                             * interface to exit waiting state
                             */
    tOspfTimer          pollTimer;
                            /* NBMA and DC ptop nbrs -
                             * timer whose firing causes hellos
                             * to be sent to inactive nbrs
                             * An interval timer which fires every poll
                             * interval seconds
                             */
    tMibMetric          aIfOpCost[OSPF_MAX_METRIC];
                            /* Metric associated with this interface
                             */
    tAUTHENTICATION     authKey;
                            /* Authentication associated information
                             */
    UINT1               au1Reserved[3];    
    tMd5AuthkeyInfo    *pLastAuthkey;
                            /* MD5 auth - The AuthInfo last used
                             */
    tArea              *pArea;
                            /* Pointer to the area to which the interface is
                             * associated
                             */
    tOSPFSTATUS         admnStatus;
                            /* From MIB -
                             * Administrative status of the interface
                             */
    tOSPFSTATUS         operStatus;
                            /* Implementation - operational state of interface
                             */

    /* Number of Packets in this Interface */
    /* the following fields are configured for virtual links */
    UINT4               u4HelloRcvdCount;
                            /* hello packets received
                             */
    UINT4               u4HelloTxedCount;
                            /* hello packets transmitted
                             */
    UINT4               u4HelloDisdCount;
                            /* hello packets discarded
                             */

     /* the following fields are counters for statistics */

    UINT4               u4DdpRcvdCount;
                            /* DDP packets received
                             */
    UINT4               u4DdpTxedCount;
                            /* DDP packets transmitted
                             */
    UINT4               u4DdpDisdCount;
                            /* DDP packets discarded
                             */
    UINT4               u4LsaReqRcvdCount;
                            /* LSA request packets received
                             */
    UINT4               u4LsaReqTxedCount;
                            /* LSA request packets transmitted
                             */
    UINT4               u4LsaReqDisdCount;
                            /* LSA request packets discarded
                             */
    UINT4               u4LsaUpdateRcvdCount;
                            /* LSA update packets received
                             */
    UINT4               u4LsaUpdateTxedCount;
                            /* LSA update packets transmitted
                             */
    UINT4               u4LsaUpdateDisdCount;
                            /* LSA update packets discarded
                             */
    UINT4               u4LsaAckRcvdCount;
                            /* LSA ack packets received
                             */
    UINT4               u4LsaAckTxedCount;
                            /* LSA ack packets transmitted
                             */
    UINT4               u4LsaAckDisdCount;
                            /* LSA ack packets discarded
                             */
    INT4                i4PollInterval;
                            /* Poll interval value
                             */
    INT4                i4RtrDeadInterval;
                            /* Router dead interval timer value
                             */
    UINT4               u4CryptSeqNum;
                            /* MD5 auth -
                             * cryptographic sequence number
                             */
    UINT4               u4AddrlessIf;
                            /* From MIB -
                             * used to identify addressless interfaces
                             */
    UINT4               u4MtuSize;
                            /* Implementation -
                             * The maximum size of a packet that
                             * can be txed on this interface
                             */
    UINT4               u4Type9OpqLSAChksumSum;
                            /* Fletcher Checksum of Type 9 OpaqueLSAs
                             * The sum of the checksums of the type 9 Opaque
                             * LSAs in the interface data structure
                             */
    UINT4               VlReachability;
    UINT4               u4NbrFullCount;
                            /* This will indicate the Number of 
                             * Neighbors in FULL state on this Interface
                             */
    UINT4               u4IfIndex;
                            /* Implementation -
                             * MIB II index of the interface
                             */
    COUNTER16           aIfEvents;
                            /* From MIB -
                             * the no. of times this interface has changed
                             * state,or an error has occurred
                             */
    UINT2               u2HelloInterval;
                            /* Hello interval timer value
                             */
    UINT2               u2AuthType;
                            /* Type of authentication used.
                             * Namely, 'None' or 'simple password'
                             */
    UINT2               u2RxmtInterval;
                            /* Retransmit interval value
                             */
    UINT2               u2IfTransDelay;
                            /* time taken to transmit an lsa
                             * over this interface
                             */
    tROWMASK            rowMask;
                            /* From MIB - SNMP if table row mask
                             */
    tROWSTATUS          ifStatus;
                            /* From MIB - SNMP if table row status
                             */
    UINT1               u1HelloIntervalsElapsed;
                            /* Number of hello intervals elapsed, used in
                             * controlling the number of traps generated
                             */
    UINT1               u1NetworkType;
                            /* From RFC -
                             * Type of attached network
                             */
    UINT1               u1IsmState;
                            /* From RFC -
                             * functional level of interface
                             */
    UINT1               u1RtrPriority;
                            /* Priority of the router (defines the eligibility
                             * to become DR or BDR)
                             */
    tTRUTHVALUE         bDcEndpt;
                            /* whether the interface connects to a demand
                             * circuit or not
                             */
    UINT1               u1ConfStatus;
                            /* Indicates whether the interface has been
                             * configured as a DC end point or it is a
                             * discovered DC end point
                             */
    tOPTIONS        ifOptions; 
    UINT1               u1GRSupportFlag;  
                             /* GR router present in this interface, 
                              * this flag is required in helper mode 
                              * to know atlease on GR router present 
                              * in this interface as a neighbor */
    UINT1               u1GrLsaAckRcvd;
    tTRUTHVALUE         bPassive;
                            /* Flag to indicate whether the interface 
        * is passive or not. */
    UINT4               u4CryptoAuthType;
                        /* Indicates which Cryptographic algorithm is
                           used
                        */
    UINT1               u1BfdIfStatus;
                         /* Used to check whether
                          *specifi interface is BFD  enabled or not */
    UINT1               au1Rsvd[3];

}  tInterface;


/* queue structure which contains ism_scheduled ism events to be processed */
typedef struct _IsmSchedNode {
    tInterface         *pInterface;
    UINT1               u1Event;
    UINT1               u1IsNodeValid;
    UINT1               au1Rsvd[2];
} tIsmSchedNode;

typedef struct _IsmSchedQueue {
    UINT1               u1FreeEntry;
    UINT1               au1Rsvd[3];
    tIsmSchedNode       aIfEvents[MAX_ISM_SCHED_QUEUE_SIZE];

} tIsmSchedQueue;


/****************************************************************************/
/* NEIGHBOR                                                                 */
/****************************************************************************/

/* structure for sending DDP packets to the neighbor */

typedef struct _DbSummaryDesc {
    UINT1                   *dbSummaryLst;
                                /* Pointer to the first LSA Header to be added 
     * in the DDP Packet */
    tOspfTimer               ddTimer;
                                /* ddp rxmt timer
                                 * The usage of the timer depends on the
                                 * neighbor state.
                                 *  _________________________________________
                                 * | Neighbor | Master/ | Usage of ddTimer   |
                                 * | State    | Slave   |                    |
                                 * |          | status  |                    |
                                 * |__________|_________|____________________|
                                 * | EXSTART  | BOTH    | retransmission of  |
                                 * |          |         | DDP init packets   |
                                 * |          |         |                    |
                                 * | EXCHANGE | MASTER  | retransmission of  |
                                 * |          |         | unacknowledged DDP |
                                 * | LOADING/ |         |                    |
                                 * | FULL     | SLAVE   | maintaining the    |
                                 * |          |         | last transmitted   |
                                 * |          |         | DDP for            |
                                 * |          |         | rtr_dead_interval  |
                                 * |          |         | seconds in order to|
                                 * |          |         | respond to         |
                                 * |          |         | duplicates received|
                                 * |          |         | from the master.   |
                                 * |__________|_________|____________________|
                                 */
    tLSASEQNUM               seqNum;
                                /* This sequence number is used to identify
                                 * whether a received DDP is a duplicate or the
                                 * next in sequence
                                 */
    tTRUTHVALUE              bMaster;
                                /* Indicates the status of this router during
                                 * the database exchange process with the
                                 * neighbor. It is set to TRUE if this router is
                                 * the master during exchange process
                                 */
    tCRU_BUF_CHAIN_HEADER   *dbSummaryPkt;
                                /* The DD packet that was transmitted the last
                                 * time. This is used for retransmission.
                                 */

    struct _LsaInfo         *pLastLsaTxed; /* Pointer to the Last LSA 
                                              Transmitted */
    UINT2                    u2Len;
                                /* length of the summary lst
                                 */
    UINT2                    u2TopDbSummaryLst;
                                /* Offset in the database summary list from
                                 * where the last transmitted DDP started
                                 */
    UINT2                    u2LastTxedSummaryLen;
                                /* Length of the last transmitted summary list
                                 */

    UINT2                   u2TxedLsaType; /* Type Of the LSA transmitted
                                              Earlier */
} tDbSummaryDesc;

/* structure for lsa rxmission to the neighbor */

typedef struct _LsaRxmDesc {
    UINT4               u4LsaRxmtCount;
                            /* no. of lsas to be rxmtted
                             */
    tOspfTimer          lsaRxmtTimer;
                            /* Timer started for LSA retransmission timeout
                             * period
                             */
} tLsaRxmtDesc;

/* structures for lsa request to the neighbor */

typedef struct _LsaReqNode {

   tTMO_SLL_NODE        nextLsaReqNode;
                            /* The next node in the lsaReqLst
                             */
   tLsHeader            lsHeader;
                            /* LS header of the lsa to be requested
                             */
} tLsaReqNode;

typedef struct _LsaReqDesc {
   tTMO_SLL             lsaReqLst;
                            /* The singly linked list of LS headers. This is
                             * maintained as a singly linked list
                             */
   tOspfTimer           lsaReqRxmtTimer;
                            /* Unsatisfied requests are retransmitted every
                             * rxmt_interval seconds. This timer is used for
                             * that purpose
                             */
   tTRUTHVALUE          bLsaReqOutstanding;
                            /* Indicates whether there is any LSA request that
                             * is yet to be acknowledged
                             * OSPF_TRUE when lrq pkt is sent and waiting
                             * for response. OSPF_FALSE otherwise
                             */
   tTMO_SLL_NODE       *startNextReqPkt;
                            /* The start of the next LS req packet
                             */
} tLsaReqDesc;

typedef struct _LastRxDdp {
    INT4                i4Ddseqno;
                            /* Contains the DD sequence number
                             */
    UINT1               u1Options;
                            /* Indicates the optional capabilities supported
                             * by the router
                             */
    UINT1               u1Flags;
                            /* The flag holding the values of Init, More and
                             * MS bits
                             */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
}tLastRxDdp;

typedef struct _LsaId {
    tLINKSTATEID        linkStateId;
                            /* Identifies the portion of the internet being
                             * described by this advertisement
                             */
    tRouterId           advRtrId;
                            /* Router ID of the router that originated the
                             * Advertisement
                             */
    UINT1               u1LsaType;
                            /* Type of LSA
                             */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                             */
} tLsaId;

/* This structure holds RM-related global information */
typedef struct  _osRedInfo  {
  tLsaId          lsaId;
                  /* The LSA identifier of the last synced LSA */
  UINT4           u4OsRmState; 
                  /* The RM state of the router 
                   *  it can be INIT or ACTIVE or STANDBY */
  UINT4           u4PeerCount; 
                  /* number of syanby nodes up */
  UINT4           u4DynBulkUpdatStatus; 
                  /* 1.-indicates the bulkupdate is not started
             * 2 -indicates the bulkupdate is in progress
                * 3 -indicates the bulkupdate is completed
             * 4 -indicates the bulkupdate is aborted */
  UINT4           u4HelloSyncCount;
                  /* Total number of hello packets 
                   * synced to standby node */
  UINT4           u4LsaSyncCount; 
                  /* Total number of LSAs synced 
                   * to standby node */
  UINT4           u4CurrentBulkStartTime;
                  /* Start time of the Bulk Update process.
                   * Used to relinquish the bulk update process after 
                   * OSPF_BULK_UPDATE_RELINQUISH_TIME
                   */
  UINT4           u4LastSynCxtId;
                  /* The last context information 
                   * sent to standby during 
                   * bulk update */
  tAreaId         lastSynAreaId; 
                  /* The last Area information sent 
                   * to standby during bulk update */
  tIPADDR         lastSynIfIpAddr;
  UINT4           u4LastSynIfAddrlessIf;
                  /* The last interface information 
                   * sent to standby during 
                   * bulk update */

  tIPADDR         lastSynVirtRtrId;
                  /*The last RtrId information on Virtual Link */ 
  tAreaId         lastSynVirtTransitAreaId; 
                  /*The last transit AreaId on Virtual Link */ 
  UINT1           u1AdminState;

  UINT1           u1BulkUpdModuleStatus; 
                  /* This indicates the bulk update 
                   * of sub modules status, it carries 
                   * the following value
                   * 1 - Indicates  bulk update is in progress for Adjacency module 
                   *   2. - Indicates  bulk update is in progress for LSDB module
                   */
 BOOL1            b1IsBulkReqRcvd; /* This flag will be set to
                                    * OSPF_TRUE if bulk request
                                    * is received before getting
                                    * the RM_PEER_UP event.
                                   */
 UINT1            u1Rsvd;
}tosRedInfo;

/* This structure holds First/Last received hello 
   header information in active node */

typedef struct  _OsLastRcvdHelloHdr {
  tIPADDR    SrcIpAddr;
             /* The source IP address 
              * of the Hello Packet */
  tIPADDR    IfIpAddr;
             /* The IP address of the If
              * on which the Hello is received */

  UINT4      u4AddrlessIf;
             /* The addresslessIf of the If 
              * on which the Hello is received */
  UINT4      u4ContextId;
             /* The Context Id 
              * on which the Hello is received */

  UINT2      u2HelloPktLen;
             /* Length of the Hello Packet */

  UINT1      Pad[2];           
}tOsLastRcvdHelloHdr;


typedef struct _OsLastRcvdHello{
  UINT1               au1Pkt[MAX_HELLO_SIZE];
                     /* The hello packet received 
                      * from neighbor */
  tOsLastRcvdHelloHdr  helloPktHdr;
                     /* The Hello packet header information 
                      * identify the interface on which the 
                      * hello is received */
}tOsLastRcvdHello;

typedef struct _OsRmMsg
{
    tRmMsg        *pFrame;     /* Message given by RM module. */
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved; /* Added for alignment purpose */
}tOsRmMsg;

/* neighbor data structure */

typedef struct _Neighbor {

    tTMO_SLL_NODE       nextNbrInIf;
                            /* The next element in the list of neighbors
                             * belonging to the same interface
                             */
    tTMO_SLL_NODE       nextSortNbr;
                            /* The next element in the list of neighbors
                             * maintained for the entire router
                             */
    tRBNodeEmbd         RbNode;
    tOspfTimer          inactivityTimer;
                            /* From RFC -
                             * timer which indicates that no hello has been
                             * received from the neighbor recently.
                             * While acting as helper, since the neighbor is
                             * not considered as down, this timer acts
                             * as grace timer and is started with the grace
                             * period specified in the grace LSA packet
                             */
    tLastRxDdp          lastRxDdp;
                            /* From RFC -
                             * Indicates the last received data base description
                             * packet used to determine duplicate packets
                             */
    tLsaRxmtDesc        lsaRxmtDesc;
                            /* From RFC -
                             * list of lsas to be retransmitted
                             * to this neighbor
                             */
    tLsaReqDesc         lsaReqDesc;
                            /* From RFC -
                             * List of LS headers to be sent in the LS request
                             * packets to the neighbor. This is maintained as a
                             * linked list
                             */
    tDbSummaryDesc      dbSummary;
                            /* From RFC -
                             * Complete list of LSAs in the database to be sent
                             * in DDPs during the database exchange process.
                             * This is maintained as an array
                             */
    tIPADDR             nbrIpAddr;
                            /* From RFC -
                             * IP address of the neighboring router's interface
                             */
    tIPADDR             desgRtr;
                            /* From RFC -
                             * Neighbor's idea of the designated router of the
                             * attached network
                             */
    tIPADDR             backupDesgRtr;
                            /* From RFC -
                             * Neighbor's idea of the backup designated router
                             * of the attached network.
                             */
    tRouterId           nbrId;
                            /* From RFC -
                             * router id of the neighbor
                             */
   tOsLastRcvdHello     lastRcvHello;
                            /* Structure to store first/last received hello 
                             * from this neighbor this is used in Active Node 
                             * to sync up the hello in standby Node */
    tInterface         *pInterface;
                            /* Implementation -
                             * Pointer to the interface to which this neighbor
                             * belongs
                             */
    UINT4               u4NbrCryptSeqNum;
                            /* From RFC -
                             * Nbr's crypt seq num
                             */
    UINT4               u4NbrAddrlessIf;
                            /* From MIB -
                             * Contains a non-zero value for unnumbered
                             * interfaces. This value equals the IfIndex value
                             * of the interface
                             */
    COUNTER16           u4NbrEvents;
                            /* From MIB -
                             * the no. of times the neighbor relation has
                             * changed state or an error had occurred
                             */
    tOPTIONS            nbrOptions;
                            /* From RFC -
                             * optional capabilities of the neighbor
                             */
    UINT1               u1NsmState;
                            /* From RFC -
                             * functional level of the neighbor conversation
                             */
    UINT1               u1NbrRtrPriority;
                            /* From RFC -
                             * router priority of the neighboring router
                             */
    UINT1               u1ConfigStatus;
                            /* From MIB -
                             * flag which indicates whether this neighbor
                             * was dynamically discovered or configured
                             */
    tROWSTATUS          nbrStatus;
                            /* From MIB -
                             * SNMP row status for nbr table
                             */
    UINT1               u1GraceLsaTxCount;
                           /* No. of grace LSA transmitted to this neighbor */

    tTRUTHVALUE         bHelloSuppression;
                            /* Indicates whether hello had been suppressed for
                             * this nbr relation or not
                             */

    UINT4               u4HelloSuppressPtoMp;

#ifdef HIGH_PERF_RXMT_LST_WANTED
    UINT1               au1RxmtInfo[MAX_NBR_DLL_HEAD_SIZE];
                             /* Added for maintaining List of Lsa waiting for
                              * ack from this nbr
                              */
    UINT1               au1Rsvd[1];
#else
    INT2                i2NbrsTableIndex;
                            /* The index to be used in the array of Nbr
                             * pointers
                             */
#endif
    UINT1               u1NbrHelperStatus; 
                           /* Helper Status */
    UINT1               u1NbrHelperExitReason;
                           /* Exit reason for last occurred Helper operation */
    tOspfTimer          helperGraceTimer;
    UINT1               u1NbrBfdState;
                           /* To Check the Neighbor BFD status */
    BOOL1               bIsBfdDisable;
                           /* To check if BFD session to be disabled */
    UINT1               au1Pad[2];
} tNeighbor;


/* structure which contains the array of pointers to neighbor structures.
 * The index into this array is used in maintaining the retransmission list
 * as a set of flags in the lsa_info structure.
 */
typedef struct _NbrsTable {
    tNeighbor          *apNbr[MAX_OSPF_NBRS_LIMIT];
                            /* Pointer to the first neighbor structure. This
                             * is the first element in the array
                             */
    INT2                i2NextFreeElement;
                            /* Contains the value of the next free neighbor
                             * index
                             */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
} tNbrsTable;

/****************************************************************************/
/* LSAs                                                                     */
/****************************************************************************/

typedef struct _LsaDesc {
    tTMO_DLL_NODE       lsaDescNode;
                            /* The self-originated LSAs are maintained as a
                             * doubly linked list
                             */
    struct _LsaInfo  *pLsaInfo;
                            /* Pointer to the lsa_info field of this LSA
                             */
    tOspfTimer          minLsaIntervalTimer;
                            /* Only when this timer fires, newer instance of
                             * the advertisement can be originated
                             */
    UINT1              *pAssoPrimitive;
                            /* Pointer to     - Type of LSA
                             * -----------------------------
                             * area           -     1
                             * interface      -     2
                             * summary param  -     3
                             * summary param  -     4
                             * external route -     5
                             * address range  -     6
                             * area           -     7
                             * summary param  -     8
                             */
    UINT4               u4LsaGenTime;
                            /* The time at which the LSA was last generated.
                             * This is used to avoid unnecessary
                             * regeneration of the same advertisement during
                             * processing of routing table changes
                             */
    UINT1               u1InternalLsaType;
                            /* Used to differentiate between network summary
                             * LSA and default summary LSA
                             */
    UINT1               u1MinLsaIntervalExpired;
                            /* Flag which indicates whether minLsaInterval
                             * Timer has fired or not. If this flag is set then
                             * newer instances can be immediately originated
                             */
    UINT1               u1LsaChanged;
                            /* This flag is checked when minLsaIntervalTimer
                             * fires. If at that time this flag is set, then a
                             * newer instance of the advertisement is
                             * originated
                             */
    UINT1               u1SeqNumWrapAround;
                            /* Indicates that the LSA has been flushed out due
                             * to sequence number wrap around. If this flag is
                             * set when the LSA is actually removed from
                             * database, a new instance is originated
                             */
} tLsaDesc;


typedef struct _LsaInfo {
    tTMO_HASH_NODE      nextLsaInfoNode;
                            /* The node used for linking in the hash table
                             */
    tTMO_SLL_NODE       nextLsaInfo;
                            /* The next LSA of the same LS type. In each area
                             * the LSAs of the same type are maintained in a
                             * singly linked list
                             */
#ifndef HIGH_PERF_RXMT_LST_WANTED
    tTMO_DLL_NODE       rxmtNode;
                            /* The retransmission list is maintained as a
                             * doubly linked list
                             */
#endif
    tRBNodeEmbd         RbNode;
    tLsaDesc           *pLsaDesc;
                            /* Contains information regarding self-
                             * originated LSAs
                             */
    struct _OspfCxt    *pOspfCxt;
    tArea              *pArea;
                            /* Pointer to the area structure to whose
                             * database this LSA belongs
                             */
    tLsaId              lsaId;
                            /* Contains the lsa_type, linkStateId and the
                             * advRtrId fields used to identify an LSA in the
                             * database
                             */
    tLSASEQNUM          lsaSeqNum;
                            /* The sequence number of the advertisement.
                             * Whenever a router generates a new instance of
                             * an advertisement its sequence number is
                             * incremented. This field is useful in
                             * differentiating  between  various  instances
                             * of  an advertisement.
                             */
    tOspfTimer          lsaAgingTimer;
                            /* A single shot timer whose firing indicates the
                             * LSA has reached MAX_AGE.
                             * In context of DEMAND_ CIRCUIT_SUPPORT
                             * it has a different Meaning
                             */
    tInterface         *pInterface;
                            /* Pointer to the interface on which the LSA is
                             * received. This information is passed onto the
                             * application along with the Opaque LSA
                             */
    UINT1              *pLsa;
                            /* The pointer to actual LSA
                             * the lsa is stored in a linear buffer with the
                             * integers stored in network byte order
                             */
    UINT4               u4LsaArrivalTime;
                            /* The time of arrival of an advertisement. This
                             * field is used in aging the advertisement. This
                             * field is also used to discard advertisements
                             * received in less than min_lsa_interval seconds
                             * after installation
                             */
    UINT4               u4RxmtCount;
                            /* The number of neighbors in whose retransmit
                             * list this LSA is currently present
                             */
    UINT2               u2LsaAge;
                            /* The age in seconds of the advertisement. An
                             * advertisement is flushed out of the routing
                             * domain when its age reaches MAX_AGE
                             */
    UINT2               u2LsaChksum;
                            /* The checksum of the complete contents of the
                             * LSA except the age field 'u2LsaAge'. This
                             * allows age field to be updated without
                             * updating the checksum field
                             */
    UINT2               u2LsaLen;
                            /* The length of the LSA
                             */
    tOPTIONS            lsaOptions;
                            /* The optional capabilities associated with the
                             * LSA
                             */
    UINT1               u1LsaFlushed;
                            /* Flag indicating whether an LSA has been flushed
                             * out
                             */
    UINT1               u1LSAStatus;
                            /* BitMask used to identify if the Opaque LSA is a
                             * new one, new instance of a previously generated
                             * Opaque LSA or an Invalid LSA (MAXAgeLSA)
                             */
    UINT1               u1FloodFlag;
                            /* Whenever a MAX_AGE Lsa is received, this flag is
                             * set to TRUE. This is checked when Lsa's are
                             * flushed
                             */

    UINT1               u1TrnsltType5; 

    UINT1               u1LsaRefresh;
                            /* Used to distinguish between modified LSA and
                               periodic refresh LSA */

    tOspfTimer          dnaLsaSplAgingTimer;
                            /* Timer to identify and flush the stale DNA LSAs
                             * whose origination is unreachable for MAX_AGE
                             * seconds
                             */

    UINT4               u4LsaTxTime;

#ifdef HIGH_PERF_RXMT_LST_WANTED
    UINT1               au1RxmtInfo[MAX_LSA_DLL_HEAD_SIZE];
                            /* added newly for maintaining list of nbr from
                             * which lsa ack needs to be received for this lsa
                             */
    UINT1               au1Reserved[3];
#else

    tRXMTFLAG           aRxmtBitMapFlag[MAX_OSPF_NBRS_LIMIT/8];
                            /* Bit map used to identify the list of neighbors
                             * to which the retransmission is to be done
                             */
#endif
} tLsaInfo;

#ifdef HIGH_PERF_RXMT_LST_WANTED
typedef struct _rxmtInfo {
    tRBNodeEmbd         RbNode;
    tNeighbor           *pNbr; 
                             /* Neighbor associated with this
                              * retransmission info
                              */
    
    tLsaInfo            *pLsaInfo;
                              /* pointer to LSA associated with
                               * this  retransmission info */
    UINT4               u4LastSentTime;
                              /* Time at which this pLsaInfo was sent to pNbr */ 
    UINT1               au1RxmtInfo[MAX_RXMT_DLL_NODE_SIZE];
    UINT1               au1Rsvd[3];
 
} tRxmtNode;
#endif

/* Structure which is passed as a parameter to the procedure constructing
 * summary lsa for an area.
 */

typedef struct _SummaryParam {
    tMetric             aMetric[OSPF_MAX_METRIC];
    tIPADDR             ipAddr;
    tIPADDR             ipAddrMask;
    tOPTIONS            options;
    UINT1               au1Rsvd[3];
} tSummaryParam;


/****************************************************************************/
/* ROUTING TABLE                                                            */
/****************************************************************************/

typedef struct _RouteNextHop {

    /* fields taken from RFC */

    tIPADDR             nbrIpAddr;
                            /* IP address of the neighbor, if the type of
                             * next hop is a neighbor
                             */
    tRouterId           advRtrId;
                            /* Advertising Router ID associated with the
                             * routes next hop
                             */
    tInterface          *pInterface;
    UINT4               u4IfIndex;
                            /* Interface index value of the interface if the
                             * next hop is an interface
                             */
    UINT1               u1Status;
    UINT1               au1Rsvd[3];

} tRouteNextHop;

typedef struct _Path {

    tTMO_SLL_NODE       nextPath;
                            /* The next path for the same TOS
                             */
    tAreaId             areaId;
                            /* From RFC
                             * The ID of the area whose Link State Database has
                             * led to this path
                             */
    tLsaInfo           *pLsaInfo;
                            /* From RFC
                             * Pointer to the LSA that directly references the
                             * destination. This field is valid only for intra
                             * area paths
                             */
    UINT4               u4Cost;
                            /* From RFC
                             * The cost associated with this path. For paths
                             * other than type-2 external, this field indicates
                             * the entire path's cost. For type-2 external paths
                             * this indicates the cost of the path internal to
                             * AS
                             */
    UINT4               u4Type2Cost;
                            /* From RFC
                             * Defined for type-2 external paths alone. This
                             * field indicates the cost of the path's external
                             * portion
                             */
    UINT1               u1PathType;
                            /* From RFC
                             * The type of the path specified by this entry.
                             * This can be intra-area, inter-area, type-1
                             * external or type-2 external
                             */
    UINT1               u1HopCount;
                            /* Implementation
                             * The number of nextHops in this path
                             */
    UINT1               u1NextHopIndex;
                            /* Implementation
                             * The index into the array of next-hops to be used
                             * for the next lookup. This field is incremented
                             * for each lookup to see that all next hops are
                             * used
                             */
    UINT1               u1Flag;
                            /* Used for distinguising the route path in Route calculation
                             * Macro              Value    Description
                             * --------------------------------------
                             * OSPF_OLD_PATH        0     Old path needs to be deleted
                             *                            at the end of route
                             *                            calculation
                             * OSPF_NEW_PATH        1     Valid Path in the route
                             */
    tRouteNextHop       aNextHops[MAX_NEXT_HOPS];
                            /* Implementation
                             * Array of next hops with MAX_NEXT_HOPS been
                             * currently defined as '10'
                             */
} tPath;

typedef struct _RtEntry {

    /* Do not alter the arrangement or alignment of this Data Structure as
     * this reflects the 'tTrieApp' Data Structure of FutureTrie.
     */

    tTMO_SLL_NODE       nextRtEntryNode;
                            /* Points to the next Routing Table entry
                             */
    VOID               *pNextApp;
                            /* Link to the next Application information.
                             * Used by Trie Library only
                             */
    VOID               *pNextAlternatePath;
                            /* Link to the next Alternate Path information.
                             * V.N
                             */
    UINT1               au1Flag[OSPF_MAX_METRIC];
                            /* Used for distinguising the routes in Route calculation
                             * Macro              Value    Description
                             * --------------------------------------
                             * OSPF_ROUTE_UNUSED    1     Created in previous
                             *                            route calculation but
                             *                            unused in present
                             *                            calculation
                             * OSPF_ROUTE_USED      2     No change in this
                             *                            route during route
                             *                            calculation
                             * OSPF_ROUTE_ADDED     3     New route added
                             * OSPF_ROUTE_MODIFIED  4     Router Modified from
                             *                            previous caculation
                             */
#ifndef TOS_SUPPORT
   UINT1  au1Pad[3];

#endif
    tDestId             destId;
                            /* From RFC -
                             * For destination type as network it is their IP
                             * address, for others it is their router ID
                             */
    UINT4               u4IpAddrMask;
                            /* From RFC -
                             * The mask associated with the network. The mask
                             * together with associated IP address defines a
                             * range of IP addresses
                             */
    UINT4               u4Tos;
                            /* From RFC -
                             * Type of service of this Route Entry.
                             * Normally it is TOS0
                             */
    UINT4               u4NxtHop;
                            /* From RFC -
                             * First Next Hop for this destination
                             */
    UINT4               u4RtIfIndx;
                            /* From RFC -
                             * The Interface Index for the first next hop
                             */
    UINT2               u2RtType;
                            /* From RFC -
                             * The Dest Type is assigned to this for Trie use
                             */
    UINT2               u2RtProto;
                            /* From RFC -
                             * Protocol Id. This value is 13 for OSPF.
                             */
    UINT4               u4RtAge;
                            /* From RFC -
                             * The number of seconds since this route was last
                             * updated
                             */
    UINT4               u4RtNxtHopAs;
                            /* From RFC -
                             * The autonomous system number of next hop
                             */
    UINT4               u4Metric;
                            /* From RFC -
                             * The reachability cost for the destination
                             */
    UINT4               u4RowStatus;
                            /* From RFC -
                             * Status of the row
                             */
    UINT4               u4UpdateTime;
                            /* From MIB -
                             * The time of the last update of the Routing Table
                             * entry
                             */
    tTMO_SLL            aTosPath[OSPF_MAX_METRIC];
                            /* Implementation -
                             * Array of paths corresponding to various TOS
                             * values. Each TOS can have a list of paths
                             */
#ifndef TOS_SUPPORT
   UINT1  au1Pad2[3];

#endif
    tOPTIONS            options;
                            /* From RFC -
                             * If the destination is a router this field
                             * specifies the optional capabilities of the
                             * router
                             */
    UINT1               u1DestType;
                            /* From RFC -
                             * Destination type of the route entry. This can be
                             * network type or Area Border Router or As
                             * Boundary Router
                             */
    UINT1              u1IsRmapAffected;
                            /* Mark whether the route entry is dropped by
                             * Routemap rules */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
} tRtEntry ;

typedef struct      _OspfRt {
    VOID               *pOspfRoot;
                            /* Points to the root of the Trie Instance
                             */
    tTMO_SLL            routesList;
                            /* A singly linked list of routing table entries
                             */
    tTMO_SLL            oldAbrAsbrRtList;
                            /* A singly linked list of Old Abr Asbr routing table entries
                             */
    INT1                i1OspfId;
                            /* Application Identifier
                             */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                             */
} tOspfRt;

/**********************************************************************/
/* OPAQUE MODULE DATA STRUCTURES */
/***********************************************************************/

/* Structure to maintain application related information */

typedef  struct _AppInfo {

    VOID               *OpqAppCallBackFn;
                            /* The call-back function provided by the
                             * application to receive OpaqueLSAs meant for it
                             */
    UINT4               u4AppId;
                            /*The Application ID
                             */
    COUNTER16           Type9LSAGen;
                            /* Number of Type9 LSAs generated by the
                             * application.
                             */
    COUNTER16           Type9LSARcvd;
                            /* Number of Type9 LSAs received for this
                             * application.
                             */
    COUNTER16           Type10LSAGen;
                            /* Number of Type10 LSAs generated by the
                             * application.
                             */
    COUNTER16           Type10LSARcvd;
                            /* Number of Type10 LSAs received for this
                             * application.
                             */
    COUNTER16           Type11LSAGen;
                            /* Number of Type11 LSAs generated by the
                             * application.
                             */
    COUNTER16           Type11LSARcvd;
                            /* Number of Type11 LSAs received for this
                             * application.
                             */
    UINT4               u4InfoFromOSPF;
                  /*  00000   ==> No information is expected from OSPF.
                      00001   ==> Opaque LSA information is needed.
                      00010   ==> Self Originated Router LSA needed.
                      00100   ==> Network LSAs are needed.
                      01000   ==> Neighbor information is needed.
                      10000   ==> Passive interface information is needed.
                   */
    UINT1               u1OpqType;
                            /* The Type of the application
                             */
    UINT1               u1LSATypesRegistered;
                            /* The type of Opaque LSAs the application can
                             * generate
                             */
    UINT1               u1Status;
                            /* Indicates the status of the Opaque Application
                             * entry.
                             */
    UINT1               u1Rsvd;
                            /* Included for 4-byte Alignment
                             */
} tAppInfo;

/****************************************************************************/
/*                Route Redistribution Related type definitions             */
/****************************************************************************/

/*
 * Structure describing the configuration of Metric Cost and Route Type
 * information to be applied to the routes learnt from the RTM
 */
typedef struct _RedistrConfigRouteInfo {
    tTMO_SLL_NODE       nextRrdConfigRoute;
                            /* next node
                             */
    tIPADDR             rrdDestIPAddr;
                            /* IP address of the interface
                             */
    tIPADDRMASK         rrdDestAddrMask;
                            /* Net mask
                             */
    UINT4               u4RrdRouteMetricValue;
                            /* configured metric value
                             */
    UINT4               u4ManualTagValue;
                            /* Configured Tag Value
                             */
    tROWSTATUS          rrdRtInfoStatus;
                            /* This field indicates the row status
                             */
    UINT1               u1RrdRouteMetricType;
                            /* route type. By defualt it is AS External type2
                             */
    UINT1               u1RedistrTagType;
                            /* Tag Type.by default it is Manual Tag type
                             */
    UINT1               u1Rsvd;
} tRedistrConfigRouteInfo;


/* Structure used to store the registration parameters */
typedef struct _RedistrRegInfo {
   UINT1            au1QueueName[4];
                            /* queue to which RTM enqueues it's messages when
                             * it has to communicate with OSPF
                             */
   UINT1            au1TaskName[4];
                            /* OSPF Task Name
                             */
   UINT4            u4RPEvent;
                            /* event that is sent by RTM to OSPF after
                             * enqueuing a message to the above Q
                             */
   UINT4            u4MaxQMsgLength;
                            /* max message size that OSPF can send to the RTM
                             */
} tRedistrRegInfo;




/****************************************************************************/
/* Ospf Context Specific Info                                                   */
/****************************************************************************/

typedef struct _OspfCxt {
    UINT4               u4OspfCxtId;        /* Ospf Context id */
    tRouterId           rtrId;
                            /* From RFC -
                             * A 32-bit number that uniquely identifies the
                             * router
                             */
    tTMO_SLL            areasLst;
                            /* From RFC -
                             * Contains the list of attached area structures,
                             * which is maintained as a singly linked list. This
                             * list is maintained in the increasing order of
                             * area id. This list contains the backbone
                             * structure also if the router is attached to the
                             * backbone
                             */
    VOID               *pExtRtRoot;
                            /* From RFC -
                             *  All routes to destinations external to the AS 
        *  are maintained in Trie Data Structure. 
        *  Pointer to the root of the Trie 
        *  InstancePointer */

    tTMO_SLL            asExtAddrRangeLstInRtr;
   
    tTMO_HASH_TABLE    *pExtLsaHashTable;
                            /* Implementation -
                             * This hash table contains database node as an 
        * element. Each database node will have a list
        * of LSAs which describes the same network.
        * (That is Link state Id + Network Mask). 
        * The purpose of this kind of data structure 
        * is to help the incremental route calculation 
        * of Type 5 LSAs. */

    tRBTree             pAsExtLsaRBRoot;
                           /* Pointer to RB tree Root. RB tree maintain all 
                            * Type 5 LSAs  */

    tTMO_SLL            sortIfLst;
                            /* Implementation -
                             * List of all interfaces in the order of the IP
                             * address and the addressless index
                             */
    tTMO_SLL            virtIfLst;
                            /* Implementation -
                             * List of all configured virtual interfaces, which
                             * is maintained in the order of Area ID and
                             * Neighbor ID.
                             */
    tTMO_SLL            sortNbrLst;
                            /* Implementation -
                             * List of all neighbors of this router. This list
                             * is maintained in the order of neighbor IP address
                             * and the addressless index.
                             */
    tTMO_SLL            hostLst;
                            /* Implementation -
                             * Singly linked list of hosts directly attached to
                             * this router
                             */
#ifndef HIGH_PERF_RXMT_LST_WANTED
    tTMO_DLL            rxmtLst;
                            /* Implementation -
                             * list of lsas to be retransmitted
                             */
    tNbrsTable         nbrsTable;
                            /* Implementation -
                             * Points to the array of pointers to neighbor data
                             * structure
                             */
#else
   UINT4                u4LastRxmtFailEventTime;
                            /* Last Ospf LSA Retransmission Node Allocation 
                             * Failure Event Handling Time 
                             */
#endif /* HIGH_PERF_RXMT_LST_WANTED */
    tTMO_DLL            origLsaDescLst;
                            /* Implementation -
                             * List of LSA descriptors corresponding to self
                             * originated LSAs.
                             * corresponding to self-originated lsas
                             */
    tTMO_SLL            Type11OpqLSALst;
                            /* A singly linked list of type11 Opaque LSAs. The
                             * elements are ordered based on LS ID and
                             * advertising Router's ID.
                             */
    tOSPFSTATUS         rfc1583Compatibility;
                            /* From RFC -
                             * Controls the preference rules when choosing
                             * among multiple AS ext LSA advertising same
                             * external destination.
                             */
    tOSPFSTATUS         admnStat;
                            /* From MIB -
                             * The administrative status of the router. If this
                             * is set to ENABLED then the router takes part in
                             * OSPF routing, otherwise this field is set to
                             * DISABLED
                             */
    tTRUTHVALUE         bTosCapability;
    tTRUTHVALUE         bAreaBdrRtr;
                            /* From MIB -
                             * If this field is TRUE then this router is an area
                             * border router. This field is FALSE otherwise
                             */
    tTRUTHVALUE         bAsBdrRtr;
                            /* From MIB -
                             * If this field is TRUE then this router is an AS
                             * boundary router. This field is FALSE otherwise
                             */
    tTRUTHVALUE         bLsdbApproachingOvflTrapGen;
                             /* Boolean flag indicating whether the trap
                              * indicating lsdb_approaching_overflow state has
                              * been generated or not.
                              */
    tTRUTHVALUE  bGenType5Flag;         
    tTRUTHVALUE  bNssaAsbrDefRtTrans; 
                              
    tOspfTimer          runRtTimer;
                            /* Whenever this timer fires the flags
                             * 'rtr_network_lsa_changed' and
                             * 'as_ext_lsa_change' are checked. If any of
                             * these flags are set, the routing table has to
                             * be recalculated
                             */
    tOspfTimer          trapLimitTimer;
                            /* Timer interval to restrict the number of traps
                             * generated in the sliding window fashion.
                             */
    tIPADDR             pktSrcAddr;
                            /* Ip address of the pkt
                             */
    tOspfRt            *pOspfRt;
                            /* From RFC -
                             * Pointer to the OSPF routing table of this router
                             */
    tArea              *pBackbone;
                            /* From RFC -
                             * Pointer to the backbone area structure
                             */
    tSpf               *pSpfTree;
                            /* The hash table structure, which is used to
                             * maintain the SPF tree during the routing table
                             * calculation.
                             */
    tCandteLst         *pCandteLst;
                            /* The hash table structure that is used to
                             * maintain the candidate list structure during
                             * the routing table calculation.
                             */
    tCandteLst         *pCandteLst2;
    tAppInfo           *pAppInfo[MAX_APP_REG];
                            /* Pointer to tAppInfo structure.
                             */
     /* last list accessed by GET_NEXT LSDB */
    tTMO_SLL           *pLastLsdbLst;

    /* last node accessed by GET_NEXT LSDB */
    tTMO_SLL_NODE      *pLastLsdbNode;

    /* last node accessed by GET_NEXT ext aggr */
    tTMO_SLL_NODE      *pLastExtAgg;

    /* last node accessed by GET_NEXT OSPF IfTable */
    tTMO_SLL_NODE      *pLastOspfIf;

    /* last node accessed by GET_NEXT OSPF NbrTable */
    tTMO_SLL_NODE      *pLastNbr;

    /* last node accessed by GET_NEXT OSPF RouteConfigTable */
    tRedistrConfigRouteInfo *pLastConfigRouteInfo;

    tFilteringRMap     *pDistributeInFilterRMap;  
                                                     /* Config for Distribute in filtering feature */
    tFilteringRMap     *pDistanceFilterRMap;    /* Config for Distance filtering feature */

    UINT4               u4OriginateNewLsa;
                            /* From MIB -
                             * The number of LSAs originated by this router
                             */
    UINT4               u4RcvNewLsa;
                            /* From MIB -
                             * The number of LSAs received that have been
                             * determined to be newer originations
                             */
    UINT4               u4NssaAreaCount;                             
    UINT4               u4ExtLsaChksumSum;
                            /* From MIB -
                             * Sum of the checksums of the external LSAs
                             */
    UINT4               u4LsaGenTime;
                            /* used for process a_rt chg->summary lsa
                             * gen/flush logic
                             * Last modification time of the RT. Used in
                             * generation of summary LSA.
                             */
    UINT4               u4OspfPktsDisd;
                            /* Total number of packets discarded
                             */
    UINT4               u4TrapControl;
                            /* Bit map used to selectively enable the required
                             * TRAPS. When a particular bit is set, then the
                             * corresponding Trap is enabled.
                             */
    UINT4               u4Type11OpqLSAChksumSum;
                            /* Fletcher checksum of the Type11 LSAs
                             * The sum of the checksums of the type 11 Opaque
                             * LSAs in the router's data structure.
                             */

    UINT4               u4ExtRtCount; /* No of External Routes */
    
#ifdef DEBUG_WANTED
    tOspfTimer          dumpTimer;
                            /* Valid only when the compilation flag
                             * DEBUG_WANTED is enabled. When this timer fires,
                             * all data structure information of OSPF will be
                             * dumped.
                             */
#endif

    UINT4               u4OspfTrace;

    UINT4               u4OspfExtTrace;

    tOspfTimer          exitOverflowTimer;
                            /* The timer interval for which the router stays
                             * in the overflow state.
                             */
    tTRUTHVALUE         bOverflowState;
                            /* Indicates whether the router is in overflow
                             * state or not.
                             */
    tTRUTHVALUE         bDefaultAseLsaPresenc;
                            /* Indicates whether the router has default ASE
                             * LSA or not.
                             */
    INT4                i4ExtLsdbLimit;
                            /* Indicates the max no. of ASE lsas that can
                             * reside in the database
                             */
    UINT4               u4ExitOverflowInterval;
                            /* The timer interval for which the
                             * exitOverflowTimer is run.
                             */

    tTRUTHVALUE         bDemandExtension;
                            /* Indicates whether the router supports Demand
                             * Circuit extensions or not. OSPF_TRUE when
                             * support is provided, otherwise OSPF_FALSE.
                             * Default value is OSPF_TRUE.
                             */
   UINT4                u4ABRType;
                            /* Type of the ABR supported by Router.
           * 1. Standard ABR.
           * 2, Cisco Type ABR.
           * 3. IBM Type ABR.
                    */

   tOSPFSTATUS          redistrAdmnStatus;
                            /* Indicates whether route distribution in OSPF
                             * is enabled or not.
                             */

   UINT1                au1RMapName[RMAP_MAX_NAME_LEN + 4];
                           /* Indicates the Route Map Name using which
                            * Route Redistribution will occur */
   
   tTMO_SLL             RedistrRouteConfigLst;
                            /* the SLL that specifies the OSPF metric &
                             * metric type for the redistributed routes
                             */
   tRedistrRegInfo     *pRedistribute;
                            /* registration info that is passed to RTM while
                             * registration
                             */
    UINT4               u4RrdSrcProtoBitMask;
                            /* Bit Mask of source protocols from which
                             * redistribution is enabled. Used when  sending
                             * redistribution enable/disable message to RTM
                             */

    UINT4               u4ActiveAreaCount;
                        /* Varibale to indicate number of configured
                         * Areas. */
    tTRUTHVALUE         bDefaultPassiveInterface; 
                        /* When set to true all OSPF interfaces created
                         * after this setting will be passive interfaces.
                         * If set to false OSPF interfaces created after
                         * this setting will not be passive. */
   
    UINT4               u4MaxMtuSize;
                            /* Indicates the Max MTU of all the interfaces
                             */
    UINT4               u4SpfHoldTimeInterval;
                           /* Minimum time between two consecutive 
                              SPF calculations */
    UINT4               u4SpfInterval;
                            /* Delay at which routing calculation is carried 
                             *  after a topology change. */
    UINT4               u4LastCalTime;
                            /* Time (in STUPS) at which Last SPf Calculation done. */
    UINT4               u4MaxCost; /* Max ospf Cost of a route in Last SPf Calculation */
    UINT4               u4MinCost; /* Min ospf Cost of a route in Last SPf Calculation */
    UINT4               u4CurrSpfMaxCost;
                                  /* Max ospf Cost of a route in the current SPf Calculation */
    UINT4               u4CurrSpfMinCost;
                                  /* Max ospf Cost of a route in the current SPf Calculation */

    tTMO_DLL_NODE       vrfSpfNode;
    /* The Vrf spf related route calculation entries are 
       maintained in a doubly linked list */
    tOspfTimer          graceTimer;
                           /* Grace Timer */
    UINT4               u4GracePeriod;
                           /* Maximum restart interval */
    UINT4               u4GraceAckState;
                          /* For Grace Restart Ack is required from helper or not*/

    UINT4               u4HelperGrTimeLimit;
                        /*Grace Time limit supported by helper*/

    UINT4               u4StaggeringDelta;
                        /* This will denote the time when current route calculation 
                         * should be relinquished */
    UINT4               u4RTStaggeringInterval; /* Relinquish Interval */
    UINT4               au4MetricValue[MAX_PROTO_REDISTRUTE_SIZE];
    UINT4               u4AsbrRtChange;
                            /* This variable will be used to track the ASBR
                             * route calculation, for triggering NSSA
                             * Translator election.*/

    UINT2               u2MinLsaInterval;
                            /* From MIB - minimum time between the LSAs
                             */

    UINT2               u2ASNo;
                            /* AS number that is provided by RTM once
                             * registration is successful
                             */

    UINT1               u1OpqCapableRtr;
                            /* This indicates the Opaque capability of the
                             * router
                             */
    UINT1               u1CnfgErrType;
                            /* Types of configuration conflicts used
                             * by ospf traps
                             */
    UINT1               u1PktType;
                            /* ospf_packet_types
                             */
    UINT1               u1TrapCount;
                            /* Number of traps generated so far in the current
                             * sliding window period.
                             */
    UINT1               u1SwPeriodsElapsed;
                            /* Number of sliding window periods elapsed. This
                             * is used to limit the number of traps generated.
                             */
    UINT1               u1RtrNetworkLsaChanged;
                            /* Whenever a router LSA or network LSA in the
                             * database changes, this flag is set to indicate
                             * that the routing table has to be recalculated
                             */
    UINT1               u1AsbSummaryLsaChanged;
                            /* Whenever an ASBR LSA in the database changes,
                             * this flag is set to indicate that all the
                             * external routes in the routing table has to be
                             * recalculated
                             */

    UINT1               u1VrfSpfTmrStatus;
                            /* when set to OSPF_TRUE indicates Route calc
                             * is pending for this ospf context
                             */ 
       /* following variables are added for graceful restart feature */
    UINT1               u1RestartSupport;
                               /* Graceful restart support for this context */
    UINT1               u1RestartStatus;
                              /* Current status of Graceful restart */
    UINT1               u1HelperSupport;
                             /* Helper Support for this context */
    UINT1               u1RestartExitReason;
                            /* Exit reason for last graceful restart process */
    UINT1               u1GraceLsaTxCount;
                            /* Number of grace LSA sent.  */
    UINT1               u1StrictLsaCheck;
                            /* Strict LSA check option */
    UINT1               u1OspfRestartState;
                           /* Indicates whether restart process is started or not 
                            * Flag takes the following values  
                            * OSPF_GR_SHUTDOWN(1) - Ospf is undergoing planned GR shutdown 
                            * OSPF_GR_RESTART(2) - Ospf is GR -restart phase 
                            * OSPF_GR_NONE(0) - Ospf is not in GR state
                            */
    UINT1               u1PrevOspfRestartState;
                           /* Indicates the previous
                            * state of variable u1OspfRestartState */ 
    UINT1               u1GrLsaMaxTxCount;
    UINT1               u1RestartReason;
                            /* Reason for Graceful restart */
    UINT1               u1GrLsaAckRcvd;
    UINT1               u1Distance; /* Distance value for routes*/
    tOPTIONS      rtrOptions;                             
    tROWSTATUS          contextStatus; /* Row Status */
    BOOL1               bIsOspfEnabled;
                          /* Used to check whether OSPF is enabled or not
                           * through MSR restoration during standby boot-up
                           */
    UINT1               u1HelperStatus;

    INT4                i4RouterIdStatus;
    /* Global Spacing timer for update distance 
     * value for existing routes */
    tOspfTimer          distanceTimer;
     /* last update route for get next valid route 
      * to update distance value */
    tRtEntry           *pLastUpdateRtLst;
    UINT2               u2AreaZeroInterfaceCnt;/* The count of interfaces attached to area 0 */
    UINT1               u1AsbrFlag; /* Flag used to save the detail whether
                                     * asbr is set user or not.*/
    UINT1               u1BfdAdminStatus; /*To check BFD status globally */
    UINT1               u1BfdStatusInAllIf; /*To check BFD status in all interfaces */
    UINT1               au1MetricType[MAX_PROTO_REDISTRUTE_SIZE];
    UINT1               au1Rsvd[2];


} tOspfCxt;

typedef struct _OsRtr {
    tIsmSchedQueue      ismSchedQueue;
                            /* Implementation -
                             * The queue that contains the events for which the
                             * ISM is scheduled
                             */
    tOspfCxt            *apOspfCxt[SYS_DEF_MAX_NUM_CONTEXTS];
    tosRedInfo          osRedInfo; 
    tOspfTimer          vrfSpfTimer;
    /* Global Spacing timer for route calculation delay */
    tTMO_SLL            RtmRtLst;
                            /* SLL that contains the Routes from RTM till
                             * OSPF Process them */
    tTMO_DLL         vrfSpfRtcList;
    /* Implementation -
     * List of vrf spf route calulation entries.  
     */

#ifdef HIGH_PERF_RXMT_LST_WANTED
    tRxmtNode           aRxmtNode[MAX_RXMT_NODES];
    tRBTree             pRxmtLstRBRoot;
                           /* Pointer to RB tree Root. RB tree maintain all 
                            * retransmission LSAs  */
    tTMO_DLL            rxmtNodeFreeMemLst;
                           /* List of Free Memory for Rxmt node */
#endif
    tRBTree             pRtrLsaCheck;
                            /* Temporarily maintains router LSA link information */
    tRBTree             pNwLsaCheck;
                           /* Temporarily maintains network LSA link information */
    tOspfCxt           *pOspfCxt;
                           /* Points to Current Context info */
    tOsixSemId           RtmLstSemId;
    tOsixSemId           OspfRtSemId;
    tOsixSemId           OspfCxtSemId;

    tTRUTHVALUE         bIgnoreExpiredTimers;
                        /* When set to true, expired timers will be
            * processed, but no call back function will 
            * be called in TmrHandleExpiry.
            */

    tTMO_HASH_TABLE    *pIfHashTable;
    /* Implementation -
     * The interface structures are maintained in a hash
     * table.  The key for this hash table is
     * tInterfaceId
     */


    UINT4               u4MaxMtuSize;
    UINT4               u4OspfGblTrace;
    UINT4               u4OspfGblExtTrace;
    UINT4               u4ActiveCxtCount; /* No of active contexts */
    UINT4               u4OspfCxtId;
                           /* Current Context ID */
    UINT4               u4VrfSpfInterval;
    UINT4               u4AsExtRouteAggrCnt;

    UINT4               u4RTstaggeredCtxId; 
                         /* ID of OSPF Context which is in the route 
                          * calculation staggered state*/

    UINT4               u4RTStaggeringStatus; 
                         /*Route table calculation 
                          * staggering is enable or not */
#ifdef RAWSOCK_WANTED
    INT4                 ai4SockId[SYS_DEF_MAX_NUM_CONTEXTS];
                            /* Raw socket Descriptor
                             */
#endif
    UINT1               u1RouteCalcCompleted;
                          /* Flag to indicate Ospf Route calculation 
                           * is completed or suspended 
                           */
    BOOL1               b1GrCsrStatus; 
                            /* Default value is FALSE.
                               This will be set to TRUE only during the GR config restoration time.
                               After restoration is completed, the value will be reverted to FALLSE.
                            */
    UINT1               au1Rsv[2];
    UINT4               u4AckOrUpdatePktSentCtr;
    /* Counter for Ack Or Update Pkts.
     * Range is 0 to MAX_OSPF_MSG_PROCESSED
     */
    UINT4               u4RcvPktCtr;
    /* Counter for received packets in LINUX platform.
     * Range is 0 to MAX_OSPF_MSG_PROCESSED
     */
    #ifdef RAWSOCK_HELLO_PKT_WANTED
    #ifdef LNXIP4_WANTED
        INT4                i4NLHelloSockId; 
    #endif
    #endif

} tOsRtr;


/****************************************************************************/
/* ROUTING TABLE CALCULATION                                                */
/****************************************************************************/

typedef struct _CandteNextHop {
    tInterface          *pInterface;
    tIPADDR             nbrIpAddr;

    UINT1               u1Status;
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                             */
} tCandteNextHop;

typedef struct _CandteNode {

    tTMO_HASH_NODE      candteHashNode;
                            /* Node associated with the candidate
                             */
    tTMO_HASH_NODE      candteHashNode2;
                            /* Node associated with the candidate
                             */

    tVertexId           vertexId;
                            /* Vertex id associated with the candidate
                             */
    tCandteNextHop      aNextHops[MAX_NEXT_HOPS];
                            /* Array of next hops associated with the candidate
                             */
    tTMO_SLL            stubLinks;
                            /* list of stub links to be considered in the 2nd
                             * phase of intra-area route calculation
                             */
    tLsaInfo           *pLsaInfo;
                            /* Pointer to the LSA associated with the candidate
                             */
    UINT4               u4Cost;
                            /* Cost derived from the Router LSA
                             */
    UINT1               u1VertType;
                            /* Vertex Type  - Value
                             * --------------------
                             * Router       -  1
                             * Network      -  0
                             */
    UINT1               u1RtrEncnted;
                            /* 1 - if intervening router encountered
                             * 0, otherwise
                             */
    UINT1               u1HopCount;
                            /* The number of next hops in this path
                             */
    UINT1               u1SpfFlag;
                            /* Used for distinguising the nodes in SPF tree
                             * Macro             Value    Description
                             * --------------------------------------
                             * OSPF_SPF_UNUSED   1        Created in previous
                             *                            route calculation but
                             *                            unsed in present calculation
                             * OSPF_SPF_ADDED    2        Added during route calculation
                             * OSPF_SPF_MODIFIED 3        Modified from previous caculation
                             */
} tCandteNode;

/* structure used to store links obtained from router or network lsa
 * during calculation of intra area routes
 */

typedef struct _LinkNode {
    tIPADDR             linkId;
                            /* Identifies the object that the router connects
                             * to another router/network.  Its value depends
                             * upon u1LinkType
                             */
    tIPADDR             linkData;
                            /* Provides the IP address of the next hop router
                             */
    UINT2               u2LinkCost;
                            /* Indicates the cost of using this outbound
                             * router link for traffic of the specified TOS
                             */
    UINT1               u1LinkType;
                            /* Indicates the type of link by which the router
                             * is connected to another router/network
                             */
    UINT1               au1Rsvd;
                            /* Included for 4-byte Alignment
                             */
} tLinkNode;

/* structure used to store links obtained from AS external lsa
 * during calculation of ASE route
 */

typedef struct _ExtLsaLink {
    tIPADDR             fwdAddr;
                            /* Indicates the address to which the data
                             * destined for the advertised destination will
                             * be forwarded
                             */
    UINT4               u4ExtRouteTag;
                            /* This 32-bit field has the value attached to
                             * each external route
                             */
    UINT4               u4Cost;
                            /* Indicates the cost associated with this route.
                             * It depends upon the u1MetricType
                             */
    UINT1               u1MetricType;
                            /* Indicates the type of the external metric.
                             * When the 'E' Bit is  set to
                             * '0' it indicates Type 1 external metric
                             * '1' it indicates Type 2 external metric
                             */
    INT1                i1Tos;
                            /* Indicates the type of service
                             */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
} tExtLsaLink;

/****************************************************************************/
/* EXTERNAL ROUTES configured                                               */
/****************************************************************************/

typedef struct _ExtrtParam {
    tIPADDR             fwdAddr;
                            /* specifies address of the router to which the
                             * packet is to be sent
                             */
    tIPADDR             fwdNextHop;
                            /* Specifies the next hop to send the packet.
                             */
    UINT4               u4ExtTag;
                            /* the following two fields will have valid only if
                             * this rtr is to forward the pkts
                             * This 32-bit field is attached to each external
                             * route.
                             */
    UINT4               u4FwdIfIndex;
                            /* Indicates the index of the interface to reach the
                             * destination.
                             */
    UINT1               u1MetricType;
                            /* Indicates the type of external metric.
                             */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                             */
} tExtrtParam;

typedef struct _AsextaddrRange  {
    tTMO_SLL_NODE       nextAsExtAddrRngInRtr;        
    tTMO_SLL         extRtLst;
    tIPADDR  ipAddr;
    tIPADDRMASK  ipAddrMask;
    tAreaId  areaId;
    UINT1  u1AggTranslation;
    UINT1  u1RangeEffect;
    UINT1  u1AttrMask;
    tROWSTATUS  rangeStatus;
    tMetric             aMetric[OSPF_MAX_METRIC];
} tAsExtAddrRange;


typedef struct _ExtRoute  {
    tTMO_SLL_NODE       nextExtRoute;
                            /* Points to the next external route in the
                             * singly linked list.
                             */
    tTMO_SLL_NODE       nextExtRouteInRange;

    tAsExtAddrRange *pAsExtAddrRange;
                             
    tIPADDR             ipNetNum;
                            /* From RFC -
                             * Indicates the IP address of the network.
                             */
    tIPADDR             ipAddrMask;
                            /* Indicates the address mask of the network.
                             */
    tMibMetric          aMetric[OSPF_MAX_METRIC];
                            /* An array of metrics corresponding to the
                             * various TOS values.
                             */
    tExtrtParam         extrtParam[OSPF_MAX_METRIC];
                            /* An array of exterior route parameters
                             * corresponding to the various TOS values.
                             */

    UINT2               u2SrcProto;
                            /* source of the route. can be
                             * local (2),
                             * static (3),
                             * RIP (8) or
                             * BGP (14)
                             */
    UINT1               u1ExtRtDelFlag;
                            /* Flag to indicate that this external route to
                             * be deleted or not.
                             */
    UINT1               u1ExtRtIsStatic;
                            /* Flag to indicate if a route is statically configured
                               or dynamically learnt from RTM.
                               For Static, u1ExtRtIsStatic = OSPF_TRUE
                               For Dynamic, u1ExtRtIsStatic = OSPF_FALSE
                             */
    UINT1                u1Level;
    UINT1                au1pad[3];

} tExtRoute;

typedef struct _IpHeader {
    UINT1               u1VerHdrlen;
    UINT1               u1Tos;
    UINT2               u2Totlen;
                            /* Total length  IP header + DATA
                             */
    UINT2               u2Id;
                            /* Identification
                             */
    UINT2               u2FlOffs;
                            /* Flags + fragment offset
                             */
    UINT1               u1Ttl;
                            /* Time to live
                             */
    UINT1               u1Proto;
                            /* Protocol
                             */
    UINT2               u2Cksum;
                            /* Checksum value
                             */
    UINT4               u4Src;
                            /* Source address
                             */
    UINT4               u4Dest;
                            /* Destination address
                             */
    UINT1               u1Options[4];
                            /* Options field
                             */
} tIpHeader;


/* the following structures are used comparing contents of two instances of a LSA */
/* The following structure holds the TOS and the corresponding metric in LSA */

typedef struct _LsaTosMetric {
    UINT4               u4Metric; /* cost */
    UINT1               u1Type; /* Link Type */
    UINT1               u1Tos; /* TOS field */
    UINT1               u1Rsvd[2];
} tLsaTosMetric;

/* The following structure holds the router LSA related route information */

typedef struct _RtrLsaRtInfo {
    tRBNodeEmbd          RbNode;
    UINT4                u4LinkId;
    UINT4                u4LinkData;
    UINT4                u4Metric [OSPF_MAX_METRIC];
    UINT1                u1Type;
    UINT1                u1Rsvd[3];
} tRtrLsaRtInfo;

/* The following structure holds the network LSA related route information. */

typedef struct _NtLsaRtInfo {
    tRBNodeEmbd         RbNode;
    UINT4               u4AttachedRtrId;
} tNtLsaRtInfo;

/* The following structure holds the external LSA related route information. */
typedef struct _ExtLsaRtInfo {
     UINT4               u4ForwardingAddress;
     UINT4               u4ExtRtTag;
     tLsaTosMetric       aExtLinkMetric;
} tExtLsaRtInfo;



#include "osfssnmp.h"
#include "osappif.h"

/* (OSPF PACKET + IP Information) for Packet received from IP */
typedef struct _OspfIpPktInfo {
    tIPADDR                srcIpAddr;
    tIPADDR                dstIpAddr;
    UINT4                  u4IfIndex;
    UINT4                  u4ContextId;
    UINT4                  u4PktLen;
    UINT4                  u4Hlen;
    tCRU_BUF_CHAIN_HEADER  *pPkt;
} tOspfIpPktInfo;

/* Message for Interface Param Changes from IP */
typedef struct _OspfIpIfParam {
    tNetIpv4IfInfo OspfNetIpIfInfo;
    UINT4          u4BitMap;        /* Interface Param for which the 
                                       Control Message Received */
}tOspfIpIfParam;

typedef struct _OspfVcmInfo {
    UINT4                   u4IpIfIndex;
    UINT4                   u4VcmCxtId;
    UINT1                   u1BitMap;
    UINT1                   au1Rsvd[3];
} tOspfVcmInfo;

typedef struct _OspfQMsg {
    UINT4    u4MsgType;
    union {
        tOspfSnmpIfParam  ospfSnmpIfParam;
 tOspfIpPktInfo    ospfPktInfo;
        tOspfIpIfParam    ospfIpIfParam;
 tAppOspfIfParam   appOspfIfParam;
 tOsixMsg          *pOspfRtmIfParam;
        tOsixMsg          *pOspfRMapParam;
        tOspfVcmInfo      ospfVcmInfo; 
        tOsRmMsg          ospfRmIfParam; 
#ifdef BFD_WANTED
        tBfdClientNbrIpPathInfo BfdClientNbrIpPathInfo;
#endif
#ifdef MBSM_WANTED
        struct MbsmIndication {
            tMbsmProtoMsg mbsmProtoMsg;
        }MbsmCardUpdate;
#endif
    }unOspfMsgIfParam;
    UINT4                 u4OspfCxtId;
#ifdef BFD_WANTED
#define OspfBfdMsgInfo unOspfMsgIfParam.BfdClientNbrIpPathInfo
#endif
} tOspfQMsg;

typedef struct _OspfLowPriPktInfo {
 UINT4             u4IfIndex;
 UINT4             u4OspfCxtId;
 tIPADDR           NbrIp;
        tCRU_BUF_CHAIN_HEADER  *pPkt;
 UINT2             u2PktLen;
 UINT1             u1PktType;
 UINT1             u1NetworkType; /* when interface network type is
                                          * IF_VIRTUAL then associate this
                                          * interface with a virtual link */
}tOspfLowPriPktInfo;

typedef struct _OspfSockFdParam {
    INT4                    i4SockFd;
} tOspfVrfSock;

typedef struct _OspfLowPriQMsg {
    UINT4                   u4MsgType;
    union {
        tOspfIpIfParam      ospfLowPriQIpIfParam;
 tOspfLowPriPktInfo  ospfLowPriQPktInfo;
        tOspfVcmInfo        ospfLowPriQVcmInfo; 
    }unOspfLowPriQParam;
}tOspfLowPriQMsg;

typedef struct {
    UINT1         au1ExtLsa[MAX_EXT_LSA_SIZE];
} tExtLsa;

typedef struct {
    UINT1         au1Lsa[MAX_LSA_SIZE];
} tLsa;
typedef struct {
    UINT1         au1LsaSz1[MAX_LSA_SIZE256];
} tLsablkSz256;
typedef struct {
    UINT1         au1LsaSz2[MAX_LSA_SIZE512];
} tLsablkSz512;
typedef struct {
    UINT1         au1LsaSz4[MAX_LSA_SIZE2048];
} tLsablkSz2048;
typedef struct {
    UINT1         au1LsaSz5[MAX_LSA_SIZE4096];
} tLsablkSz4096;

#define ospfLowPriQIpIfParam   unOspfLowPriQParam.ospfLowPriQIpIfParam
#define ospfLowPriQPktInfo     unOspfLowPriQParam.ospfLowPriQPktInfo
#define ospfLowPriQVcmInfo     unOspfLowPriQParam.ospfLowPriQVcmInfo

#define  ospfSnmpIfParam   unOspfMsgIfParam.ospfSnmpIfParam
#define  ospfIpIfParam     unOspfMsgIfParam.ospfIpIfParam 
#define  appOspfIfParam    unOspfMsgIfParam.appOspfIfParam 
#define  pOspfRtmIfParam   unOspfMsgIfParam.pOspfRtmIfParam
#define  pOspfRMapParam   unOspfMsgIfParam.pOspfRMapParam
#define  ospfPktInfo       unOspfMsgIfParam.ospfPktInfo
#define  ospfVcmInfo       unOspfMsgIfParam.ospfVcmInfo
#endif
