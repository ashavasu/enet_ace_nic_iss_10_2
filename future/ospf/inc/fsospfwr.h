/********************************************************************
 *  $Id: fsospfwr.h,v 1.14 2013/09/05 15:09:40 siva Exp $
 ********************************************************************/
#ifndef _FSOSPFWR_H
#define _FSOSPFWR_H

VOID RegisterFSOSPF(VOID);
VOID UnRegisterFSOSPF(VOID);
INT4 FutOspfOverFlowStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfPktsTxedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfPktsDisdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRFC1583CompatibilityGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxAreasGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxLSAperAreaGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxExtLSAsGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxSelfOrgLSAsGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxLsaSizeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMinLsaIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfABRTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNssaAsbrDefRtTransGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDefaultPassiveInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfSpfHoldtimeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfSpfDelayGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRTStaggeringIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRTStaggeringStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHotStandbyAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHotStandbyStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDynamicBulkUpdStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfStanbyHelloSyncCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfStanbyLsaSyncCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartSupportGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartStrictLsaCheckingGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartAgeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartExitReasonGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHelperSupportGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfExtTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FutospfRouterIdPermanenceGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfBfdStatusGet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBfdAllIfStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHelperGraceTimeLimitGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartAckStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfGraceLsaRetransmitCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartReasonGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRFC1583CompatibilitySet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxAreasSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxLSAperAreaSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxExtLSAsSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxSelfOrgLSAsSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxLsaSizeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfMinLsaIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfABRTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNssaAsbrDefRtTransSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDefaultPassiveInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfSpfHoldtimeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfSpfDelaySet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRTStaggeringIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRTStaggeringStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartSupportSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartStrictLsaCheckingSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHelperSupportSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfExtTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FutospfRouterIdPermanenceSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfBfdStatusSet (tSnmpIndex *, tRetVal *);
INT4 FutOspfBfdAllIfStateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHelperGraceTimeLimitSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartAckStateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfGraceLsaRetransmitCountSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartReasonSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRFC1583CompatibilityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxAreasTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxLSAperAreaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxExtLSAsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxSelfOrgLSAsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMaxLsaSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfMinLsaIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfABRTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfNssaAsbrDefRtTransTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfDefaultPassiveInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfSpfHoldtimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfSpfDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRTStaggeringIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRTStaggeringStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartSupportTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartStrictLsaCheckingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfHelperSupportTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfExtTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutospfRouterIdPermanenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfBfdStatusTest (UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfBfdAllIfStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfHelperGraceTimeLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartAckStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfGraceLsaRetransmitCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRestartReasonTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRFC1583CompatibilityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMaxAreasDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMaxLSAperAreaDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMaxExtLSAsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMaxSelfOrgLSAsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMaxRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMaxLsaSizeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfMinLsaIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfABRTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfNssaAsbrDefRtTransDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfDefaultPassiveInterfaceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfSpfHoldtimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfSpfDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRTStaggeringIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRTStaggeringStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRestartSupportDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRestartIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRestartStrictLsaCheckingDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfHelperSupportDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfExtTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutospfRouterIdPermanenceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfBfdStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfBfdAllIfStateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfHelperGraceTimeLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRestartAckStateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfGraceLsaRetransmitCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRestartReasonDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

















INT4 GetNextIndexFutOspfAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfAreaIfCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNetCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaRtrCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorRoleGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorStabilityIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorEventsGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaDfInfOriginateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorRoleSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorStabilityIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaDfInfOriginateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaNSSATranslatorStabilityIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaDfInfOriginateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFutOspfHostTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfHostRouteIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHostRouteIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfHostRouteIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfHostTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFutOspfIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfIfOperStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfPassiveGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfNbrCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAdjCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfHelloRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfHelloTxedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfHelloDisdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfDdpRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfDdpTxedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfDdpDisdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLrqRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLrqTxedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLrqDisdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLsuRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLsuTxedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLsuDisdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLakRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLakTxedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfLakDisdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfBfdStateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfBfdStateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfBfdStateTest(UINT4 *,tSnmpIndex *, tRetVal *);
INT4 FutOspfIfPassiveSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfPassiveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFutOspfIfMD5AuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfIfMD5AuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfMD5AuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFutOspfVirtIfMD5AuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfVirtIfMD5AuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfMD5AuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFutOspfNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfNbrDBSummaryQLenGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNbrLSReqQLenGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNbrRestartHelperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNbrRestartHelperAgeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNbrRestartHelperExitReasonGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfNbrBfdStateGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfRoutingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfRouteTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRouteAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRouteCostGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRouteType2CostGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRouteInterfaceIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfSecIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfSecIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfSecIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfSecIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfSecIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFutOspfAreaAggregateTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfAreaAggregateExternalTagGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaAggregateExternalTagSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaAggregateExternalTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaAggregateTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFutOspfAsExternalAggregationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfAsExternalAggregationEffectGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationTranslationGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationEffectSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationTranslationSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationEffectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationTranslationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAsExternalAggregationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 FutOspfOpaqueOptionGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType11LsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType11LsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaIDValidGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfOpaqueOptionSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaIDValidSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfOpaqueOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfAreaIDValidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfOpaqueOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfAreaIDValidDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexFutOspfOpaqueInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfOpaqueType9LsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfOpaqueType9LsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfType9LsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfType9LsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType9LsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType9LsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType9LsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfType11LsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfType11LsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType11LsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType11LsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfType11LsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfAppInfoDbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfAppInfoDbOpaqueTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbLsaTypesSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbType9GenGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbType9RcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbType10GenGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbType10RcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbType11GenGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfAppInfoDbType11RcvdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDSrcProtoMaskEnableGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDSrcProtoMaskDisableGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMapEnableGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDSrcProtoMaskEnableSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDSrcProtoMaskDisableSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMapEnableSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDSrcProtoMaskEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDSrcProtoMaskDisableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMapEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRRDSrcProtoMaskEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRRDSrcProtoMaskDisableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfRRDRouteMapEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);




INT4 GetNextIndexFutOspfRRDRouteConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfRRDRouteMetricGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteTagTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteTagGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMetricSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteTagTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteTagSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteTagTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfRRDRouteConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFutOspfVirtNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfVirtNbrRestartHelperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtNbrRestartHelperAgeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtNbrRestartHelperExitReasonGet(tSnmpIndex *, tRetVal *);

INT4 GetNextIndexFutOspfDistInOutRouteMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfDistInOutRouteMapValueGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDistInOutRouteMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDistInOutRouteMapValueSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDistInOutRouteMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfDistInOutRouteMapValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfDistInOutRouteMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfDistInOutRouteMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 FutOspfPreferenceValueGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfPreferenceValueSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfPreferenceValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfPreferenceValueDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFutOspfIfAuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfAuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFutOspfVirtIfAuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfVirtIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfAuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFutOspfIfCryptoAuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfIfCryptoAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfCryptoAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfIfCryptoAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfIfCryptoAuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFutOspfVirtIfCryptoAuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfVirtIfCryptoAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfCryptoAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfCryptoAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfVirtIfCryptoAuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSOSPFWR_H */
