/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ososix.h,v 1.22 2014/10/21 11:15:57 siva Exp $
 *
 * Description:This file contains constants , typedefs & macros 
 *             related to OS interface.
 *
 *******************************************************************/

/****** This file contains #define's common to OSPF & other Protocols eg: IP
 **/
#ifndef  _OSOSIX_H
#define  _OSOSIX_H

/********* Definitions for FSAP2 - OSPF - IP Interaction *********/

#define  CRU_IP_ROUTING_OSPF_MODULE     0x0A 
#define  CRU_IP_FORWARD_MODULE          0x01 
#define  CRU_VLAN_INTERFACE_TYPE        0x88
#define  CRU_LAGG_INTERFACE_TYPE        0xA1
 




/********* Definitions for FSAP2 - Task Priorities *********/
#define  OSPF_ROUTING_TASK_PRIORITY      100 

/********* Definitions for FSAP2 - Event Bit Positions *****/
#define  TMO1_TIMER_01_EXP_EVENT     (0x00001000) 
#define  TMO1_TASK_QUE_00_ENQ_EVENT  (0x00002000) 
#define  TMO1_TASK_QUE_05_ENQ_EVENT  (0x00004000) 
#define  TMO1_TASK_QUE_15_ENQ_EVENT  (0x00008000) 
#define  OSPF_RTM_RTMSG_EVENT        (0x00020000)
#define  OSPF_PKT_ARRIVAL_EVENT      (0x00040000)
#define  OSPF_RED_SUB_BULK_UPD_EVENT (0x00080000)  
#define  HELLO_TIMER_EXP_EVENT       (0x00200000)  
#define  OSPF_NLH_PKT_ARRIVAL_EVENT  (0x00400000)


#define TMO1_TASK_QUE_16_ENQ_EVENT     (0x00020000)
#define OSPF_APP_IF_EVENT    TMO1_TASK_QUE_16_ENQ_EVENT

/*  FT UP/DOWN event */
#define OSPF_SNMP_FT_UP_EVENT      0x00100000
#define OSPF_SNMP_FT_DN_EVENT    0x01000000

#endif /*  _OSOSIX_H  */

