
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfSetTrap ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfConfigErrorType ARG_LIST((INT4 *));

INT1
nmhGetOspfPacketType ARG_LIST((INT4 *));

INT1
nmhGetOspfPacketSrc ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfSetTrap ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfSetTrap ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2OspfSetTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
