#ifndef _STDOSPWR_H
#define _STDOSPWR_H

VOID RegisterSTDOSP(VOID);

VOID UnRegisterSTDOSP(VOID);
INT4 OspfRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfAdminStatGet(tSnmpIndex *, tRetVal *);
INT4 OspfVersionNumberGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaBdrRtrStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfASBdrRtrStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfExternLsaCountGet(tSnmpIndex *, tRetVal *);
INT4 OspfExternLsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 OspfTOSSupportGet(tSnmpIndex *, tRetVal *);
INT4 OspfOriginateNewLsasGet(tSnmpIndex *, tRetVal *);
INT4 OspfRxNewLsasGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbLimitGet(tSnmpIndex *, tRetVal *);
INT4 OspfMulticastExtensionsGet(tSnmpIndex *, tRetVal *);
INT4 OspfExitOverflowIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfDemandExtensionsGet(tSnmpIndex *, tRetVal *);
INT4 OspfRouterIdSet(tSnmpIndex *, tRetVal *);
INT4 OspfAdminStatSet(tSnmpIndex *, tRetVal *);
INT4 OspfASBdrRtrStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfTOSSupportSet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbLimitSet(tSnmpIndex *, tRetVal *);
INT4 OspfMulticastExtensionsSet(tSnmpIndex *, tRetVal *);
INT4 OspfExitOverflowIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfDemandExtensionsSet(tSnmpIndex *, tRetVal *);
INT4 OspfRouterIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAdminStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfASBdrRtrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfTOSSupportTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfMulticastExtensionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfExitOverflowIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfDemandExtensionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfRouterIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfAdminStatDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfASBdrRtrStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfTOSSupportDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfExtLsdbLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfMulticastExtensionsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfExitOverflowIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 OspfDemandExtensionsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);








INT4 GetNextIndexOspfAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfImportAsExternGet(tSnmpIndex *, tRetVal *);
INT4 OspfSpfRunsGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaBdrRtrCountGet(tSnmpIndex *, tRetVal *);
INT4 OspfAsBdrRtrCountGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaLsaCountGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaLsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaSummaryGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 OspfImportAsExternSet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaSummarySet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfImportAsExternTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaSummaryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexOspfStubAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfStubAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfStubTOSGet(tSnmpIndex *, tRetVal *);
INT4 OspfStubMetricGet(tSnmpIndex *, tRetVal *);
INT4 OspfStubStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfStubMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfStubMetricSet(tSnmpIndex *, tRetVal *);
INT4 OspfStubStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfStubMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 OspfStubMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfStubStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfStubMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfStubAreaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexOspfLsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfLsdbAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbLsidGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 OspfLsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexOspfAreaRangeTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfAreaRangeAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeNetGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeMaskGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeEffectGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeMaskSet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeEffectSet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeEffectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaRangeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexOspfHostTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfHostIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 OspfHostTOSGet(tSnmpIndex *, tRetVal *);
INT4 OspfHostMetricGet(tSnmpIndex *, tRetVal *);
INT4 OspfHostStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfHostAreaIDGet(tSnmpIndex *, tRetVal *);
INT4 OspfHostMetricSet(tSnmpIndex *, tRetVal *);
INT4 OspfHostStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfHostMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfHostStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfHostTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexOspfIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfIfIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 OspfAddressLessIfGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAdminStatGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfRtrPriorityGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfTransitDelayGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfRetransIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfHelloIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfRtrDeadIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfPollIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfStateGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfDesignatedRouterGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfBackupDesignatedRouterGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfEventsGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMulticastForwardingGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfDemandGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAreaIdSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfTypeSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAdminStatSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfRtrPrioritySet(tSnmpIndex *, tRetVal *);
INT4 OspfIfTransitDelaySet(tSnmpIndex *, tRetVal *);
INT4 OspfIfRetransIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfHelloIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfRtrDeadIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfPollIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 OspfIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMulticastForwardingSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfDemandSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfAreaIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfAdminStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfRtrPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfTransitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfRetransIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfHelloIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfRtrDeadIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfPollIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfMulticastForwardingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfDemandTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);














INT4 GetNextIndexOspfIfMetricTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfIfMetricIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricAddressLessIfGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricTOSGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricValueGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricValueSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfIfMetricTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexOspfVirtIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfVirtIfAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfNeighborGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfTransitDelayGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfRetransIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfHelloIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfRtrDeadIntervalGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfStateGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfEventsGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfTransitDelaySet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfRetransIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfHelloIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfRtrDeadIntervalSet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfTransitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfRetransIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfHelloIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfRtrDeadIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfVirtIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexOspfNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfNbrIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrAddressLessIndexGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrRtrIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrOptionsGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrPriorityGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrStateGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrEventsGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrLsRetransQLenGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbmaNbrStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbmaNbrPermanenceGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrHelloSuppressedGet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrPrioritySet(tSnmpIndex *, tRetVal *);
INT4 OspfNbmaNbrStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfNbrPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfNbmaNbrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfNbrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexOspfVirtNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfVirtNbrAreaGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrRtrIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrOptionsGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrStateGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrEventsGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrLsRetransQLenGet(tSnmpIndex *, tRetVal *);
INT4 OspfVirtNbrHelloSuppressedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexOspfExtLsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfExtLsdbTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbLsidGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 OspfExtLsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexOspfAreaAggregateTable(tSnmpIndex *, tSnmpIndex *);
INT4 OspfAreaAggregateAreaIDGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateLsdbTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateNetGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateMaskGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateStatusGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateEffectGet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateStatusSet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateEffectSet(tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateEffectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfAreaAggregateTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _STDOSPWR_H */
