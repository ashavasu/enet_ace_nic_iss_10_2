#include "osinc.h"
/* Proto Validate Index Instance for FutOspfBRRouteTable. */
INT1
nmhValidateIndexInstanceFutOspfBRRouteTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfBRRouteTable  */

INT1
nmhGetFirstIndexFutOspfBRRouteTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfBRRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfBRRouteType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfBRRouteAreaId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfBRRouteCost ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfBRRouteInterfaceIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfExtRouteTable. */
INT1
nmhValidateIndexInstanceFutOspfExtRouteTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfExtRouteTable  */

INT1
nmhGetFirstIndexFutOspfExtRouteTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfExtRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfExtRouteMetric ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfExtRouteMetricType ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfExtRouteTag ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfExtRouteFwdAdr ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfExtRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfExtRouteNextHop ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfExtRouteStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfExtRouteMetric ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfExtRouteMetricType ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfExtRouteTag ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfExtRouteFwdAdr ARG_LIST((UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFutOspfExtRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfExtRouteNextHop ARG_LIST((UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFutOspfExtRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfExtRouteMetric ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfExtRouteMetricType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfExtRouteTag ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfExtRouteFwdAdr ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FutOspfExtRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfExtRouteNextHop ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FutOspfExtRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfExtRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfGrShutdown ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfGrShutdown ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfGrShutdown ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfGrShutdown ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
