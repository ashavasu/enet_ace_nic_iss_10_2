/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osmain.h,v 1.23 2013/12/18 12:51:09 siva Exp $
 *
 * Description:This file contains all global variable declarations
 *
 *******************************************************************/

#ifndef _OSMAIN_H
#define _OSMAIN_H

tIPADDR           gAllSpfRtrs       = {224,0,0,5};
tIPADDR           gAllDRtrs         = {224,0,0,6};
tIPADDR           gNullIpAddr       = {0,0,0,0};
tIPADDR           gDefaultDest      = {0,0,0,0};
tIPADDRMASK       gHostRouteMask    = {0xff,0xff,0xff,0xff};
tAreaId           gBackboneAreaId   = {0,0,0,0};
UINT1             gu1DefCostCiscoCompat = OSPF_FALSE; /* cisco compatability is enabled by default */
                    
UINT4 gu4CsrFlag;
UINT4 gu4RestoreFlag;

tOsixMsg *gOsRmapMsg[MAX_OSPF_RMAP_MSGS];

tOsRtr            gOsRtr;
#ifdef BFD_WANTED
tBfdReqParams     gBfdInParams;
#endif
tOsixSemId    gOspfSemId;

UINT4               gOspf1sTimer;

INT4                gi4OspfSysLogId;
/* tmrif specific global varibale */
tTimerListId gTimerLst;
tTimerListId gOnesecTimerLst;
tTimerListId gHelloTimerLst;
tTmrAppTimer gOnesecTimerNode;
                                        
/* bufif specific global varibale */

tBufQid           gBufQid;        /* 
                                       * Structure having qids of various 
                                       * CRU buffer pools 
                                       */

INT1                gGaugeOpValue; /* Added mainly to Remove Warning */
 
FILE                *gp_ospf_output_file;


/* Global variables which are used for debugging & Trace log */

/* The Various Traps that are Generated */

UINT1  au1DbgTrapId[18][40] =
                        { "",
                          "VIRT_IF_STATE_CHANGE_TRAP",
                          "NBR_STATE_CHANGE_TRAP",
                          "VIRT_NBR_STATE_CHANGE_TRAP",   
                          "IF_CONFIG_ERROR_TRAP",
                          "VIRT_IF_CONFIG_ERROR_TRAP",
                          "IF_AUTH_FAILURE_TRAP",
                          "VIRT_IF_AUTH_FAILURE_TRAP",
                          "IF_RX_BAD_PACKET_TRAP",
                          "VIRT_IF_RX_BAD_PACKET_TRAP",
                          "IF_TX_RETRANSMIT_TRAP",
                          "VIRT_IF_TX_RETRANSMIT_TRAP",
                          "ORIGINATE_LSA_TRAP",
                          "MAX_AGE_LSA_TRAP",
                          "LSDB_OVERFLOW_TRAP",
                          "LSDB_APPROACHING_OVERFLOW_TRAP",
                          "IF_STATE_CHANGE_TRAP"};
 
UINT1  au1DbgIfType[6][30] = 
                          { "",
                            "BROADCAST",
                            "NBMA",
                            "PTOP",
                            "",
       "PTOM"};

/* interface states */

UINT1  au1DbgIfState[MAX_IF_STATE][12] =
         { "DOWN",
           "LOOP_BACK",
           "WAITING",
           "PTOP",
           "DR",
           "BACKUP",
           "DR_OTHER" };

/* interface events */

UINT1  au1DbgIfEvent[MAX_IF_EVENT][30] =
       { "IFE_UP",
         "IFE_WAIT_TIMER",
         "IFE_BACKUP_SEEN",
         "IFE_NBR_CHANGE",
         "IFE_LOOP_IND",
         "IFE_UNLOOP_IND",
         "IFE_DOWN" };

/*  neighbor states */

UINT1 au1DbgNbrState[MAX_NBR_STATE][12] =
                { "DOWN",
                  "ATTEMPT",
                  "INIT",
                  "2WAY",
                  "EXSTART",
                  "EXCHANGE",
                  "LOADING",
                  "FULL" };

/* events causing neighbor state transitions */

UINT1 au1DbgNbrEvent[MAX_NBR_EVENT][30] =
       { "NBRE_HELLO_RCVD",
         "NBRE_START",
         "NBRE_2WAY_RCVD",
         "NBRE_NEG_DONE",
         "NBRE_EXCHANGE_DONE",
         "NBRE_BAD_LS_REQ",
         "NBRE_LOADING_DONE",
         "NBRE_ADJ_OK",
         "NBRE_SEQ_NUM_MISMATCH",
         "NBRE_1WAY_RCVD",
         "NBRE_KILL_NBR",
         "NBRE_INACTIVITY_TIMER",
         "NBRE_LL_DOWN" };


/* Types of destinations in routing table. */

UINT1 au1DbgRtDestType[4][30] = 
        { "",
          "NETWORK",
          "AREA_BORDER",
          "AS_BOUNDARY" };

/* Types of paths in routing table */

UINT1 au1DbgRtPathType[5][30] = 
        { "",
          "INTRA_AREA",
          "INTER_AREA",
          "TYPE_1_EXT",
          "TYPE_2_EXT" };

/* Types of timers used */

UINT1 au1DbgTimerId[MAX_TIMERS][30] = 
    { "HELLO_TIMER",
      "POLL_TIMER",
      "WAIT_TIMER",
      "INACTIVITY_TIMER",
      "DD_INIT_RXMT_TIMER",
      "DD_RXMT_TIMER",
      "DD_LAST_PKT_LIFE_TIME_TIMER",
      "LSA_REQ_RXMT_TIMER",
      "LSA_NORMAL_AGING_TIMER",
      "LSA_PREMATURE_AGING_TIMER",
      "MIN_LSA_INTERVAL_TIMER",
      "LSA_RXMT_TIMER",
      "DEL_ACK_TIMER",
      "RUN_RT_TIMER",
      "DUMP_TIMER",
      "EXIT_OVERFLOW_TIMER",
      "DNA_LSA_SPL_AGING_TIMER",
      "TRAP_LIMIT_TIMER"};

UINT1 au1DbgNbrCfgStatus[3][30] = 
    { "",
      "CONFIGURED",
      "DISCOVERED" };

UINT1 au1DbgLsaType[12][30] = 
        { "",
      "ROUTER_LSA",
      "NETWORK_LSA",
      "NETWORK_SUM_LSA",
      "ASBR_SUM_LSA",
      "AS_EXT_LSA",
      "MOSPF_LSA",
      "NSSA_LSA",
      "ATTRIBUTE_LSA",
      "TYPE9_OPQ_LSA",
      "TYPE10_OPQ_LSA",
      "TYPE11_OPQ_LSA"
        };

UINT1 au1DbgPktType[6][30] = 
       { "",
         "HELLO_PKT",
         "DD_PKT",
         "LSA_REQ_PKT",
         "LS_UPDATE_PKT",
         "LSA_ACK_PKT"  };

UINT1 au1DbgAbrType[4][30] = 
       { "",
         "Standard ABR",
         "Cisco ABR",
         "IBM ABR"  };

struct TrieAppFns  OspfTrieLibFuncs = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) OspfTrieAddRtEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pNxtHop, tKey Key)) OspfTrieDeleteRtEntry,

    (INT4 (*) (tInputParams *pInputParams, VOID *pOutputParams,
                   VOID *pAppPtr)) OspfTrieSearchRtEntry,

    (INT4 (*) (tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)) OspfTrieLookUpEntry,

    (VOID *(*) (tInputParams *, void (*) (VOID *),VOID *)) NULL,

    (VOID (*)  (VOID *)) NULL,

    (INT4 (*) (VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*) (VOID *, VOID *, tKey Key)) NULL,
    
    (INT4 (*) (tInputParams *, VOID *, VOID **, void *, UINT4)) NULL,

    (VOID *(*)(tInputParams *pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) OspfTrieDelAllRtEntry,

    (VOID (*)(VOID *)) OspfTrieCbDelete,

    (INT4 (*)(VOID *, VOID **ppAppPtr, tKey key)) NULL,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*) (UINT2  u2KeySize, tInputParams * pInputParams,
               VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)) OspfBestMatch,

    (INT4 (*)(tInputParams *pInputParams, VOID *pOutputParams,
              VOID * ppAppSpecInfo, tKey Key)) OspfTrieGetNextRtEntry,

    (INT4 (*)(tInputParams * , VOID *, VOID *, UINT2 , tKey )) NULL
        
};

#endif
