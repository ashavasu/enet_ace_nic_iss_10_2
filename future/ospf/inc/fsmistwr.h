
/********************************************************************
 $Id: fsmistwr.h,v 1.6 2014/03/01 11:40:41 siva Exp $
 *******************************************************************/
#ifndef _FSMIOSSTWR_H
#define _FSMIOSSTWR_H
INT4 GetNextIndexFsMIStdOspfTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMIST(VOID);
VOID UnRegisterFSMIST(VOID);
INT4 FsMIStdOspfRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAdminStatGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVersionNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaBdrRtrStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfASBdrRtrStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExternLsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExternLsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfTOSSupportGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfOriginateNewLsasGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfRxNewLsasGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExtLsdbLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfMulticastExtensionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExitOverflowIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfDemandExtensionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfRouterIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAdminStatSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfASBdrRtrStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfTOSSupportSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExtLsdbLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfMulticastExtensionsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExitOverflowIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfDemandExtensionsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfRouterIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAdminStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfASBdrRtrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfTOSSupportTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExtLsdbLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfMulticastExtensionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExitOverflowIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfDemandExtensionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);









INT4 GetNextIndexFsMIStdOspfAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfImportAsExternGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfSpfRunsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaBdrRtrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAsBdrRtrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaLsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaLsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaSummaryGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfImportAsExternSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaSummarySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfImportAsExternTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaSummaryTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsMIStdOspfStubAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfStubMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfStubAreaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsMIStdOspfLsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfLsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfLsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfLsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfLsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdOspfHostTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfHostMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostAreaIDGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfHostTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIStdOspfIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfIfAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAdminStatGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRtrPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTransitDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRetransIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfHelloIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRtrDeadIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfPollIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfDesignatedRouterGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfBackupDesignatedRouterGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfEventsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMulticastForwardingGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfDemandGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfCryptoAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAreaIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAdminStatSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRtrPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTransitDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRetransIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfHelloIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRtrDeadIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfPollIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMulticastForwardingSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfDemandSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfCryptoAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAreaIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAdminStatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRtrPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTransitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRetransIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfHelloIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfRtrDeadIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfPollIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMulticastForwardingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfDemandTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfCryptoAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);














INT4 GetNextIndexFsMIStdOspfIfMetricTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfIfMetricValueGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMetricStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMetricValueSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMetricStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMetricValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMetricStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfIfMetricTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIStdOspfVirtIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfVirtIfTransitDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfRetransIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfHelloIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfRtrDeadIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfEventsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfCryptoAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfTransitDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfRetransIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfHelloIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfRtrDeadIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfCryptoAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfTransitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfRetransIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfHelloIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfRtrDeadIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfCryptoAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexFsMIStdOspfNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfNbrRtrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrOptionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrEventsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrLsRetransQLenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbmaNbrStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbmaNbrPermanenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrHelloSuppressedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbmaNbrStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbmaNbrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfNbrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIStdOspfVirtNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfVirtNbrIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtNbrOptionsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtNbrStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtNbrEventsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtNbrLsRetransQLenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfVirtNbrHelloSuppressedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdOspfExtLsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfExtLsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExtLsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExtLsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfExtLsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdOspfAreaAggregateTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdOspfAreaAggregateStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaAggregateEffectGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaAggregateStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaAggregateEffectSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaAggregateStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaAggregateEffectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdOspfAreaAggregateTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _FSMISTWR_H */
