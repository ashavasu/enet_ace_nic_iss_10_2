/********************************************************************
 *  *Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *    *$Id: stdostwr.h,v 1.6 2014/03/01 11:40:41 siva Exp $
 *
 *      *Description: This file has the handle routines for cli SET/GET
 *              destined for OSPF  Module
 *
 *
 ************************************************************************/

#ifndef _STDOSTWR_H
#define _STDOSTWR_H

VOID RegisterSTDOST(VOID);

VOID UnRegisterSTDOST(VOID);
INT4 OspfSetTrapGet(tSnmpIndex *, tRetVal *);
INT4 OspfConfigErrorTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfPacketTypeGet(tSnmpIndex *, tRetVal *);
INT4 OspfPacketSrcGet(tSnmpIndex *, tRetVal *);
INT4 OspfSetTrapSet(tSnmpIndex *, tRetVal *);
INT4 OspfSetTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 OspfSetTrapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

#endif /* _STDOSTWR_H */
