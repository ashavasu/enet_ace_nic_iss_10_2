/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmioslw.h,v 1.13 2013/09/05 15:09:40 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfGlobalTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfVrfSpfInterval ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfGlobalTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMIOspfVrfSpfInterval ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfGlobalTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIOspfVrfSpfInterval ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfGlobalTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIOspfVrfSpfInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfTable. */
INT1
nmhValidateIndexInstanceFsMIOspfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfTable  */

INT1
nmhGetFirstIndexFsMIOspfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfOverFlowState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfPktsRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfPktsTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfPktsDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfRFC1583Compatibility ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfTraceLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfMinLsaInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfABRType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfNssaAsbrDefRtTrans ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfDefaultPassiveInterface ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfSpfHoldtime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfSpfDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRTStaggeringInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfRouterIdPermanence ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfBfdStatus ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetFsMIOspfBfdAllIfState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartStrictLsaChecking ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartAge ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfRestartExitReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfHelperSupport ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));


INT1
nmhGetFsMIOspfExtTraceLevel ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsMIOspfHelperGraceTimeLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartAckState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfGraceLsaRetransmitCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRestartReason ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfRFC1583Compatibility ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfTraceLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfMinLsaInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfABRType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfNssaAsbrDefRtTrans ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfDefaultPassiveInterface ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfSpfHoldtime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfSpfDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRTStaggeringInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIOspfRouterIdPermanence ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfBfdStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfBfdAllIfState ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsMIOspfRestartSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRestartInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRestartStrictLsaChecking ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfHelperSupport ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfExtTraceLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfHelperGraceTimeLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRestartAckState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfGraceLsaRetransmitCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRestartReason ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfRFC1583Compatibility ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfMinLsaInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfABRType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfNssaAsbrDefRtTrans ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfDefaultPassiveInterface ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfSpfHoldtime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfSpfDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRTStaggeringInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIOspfRouterIdPermanence ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfBfdStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfBfdAllIfState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FsMIOspfRestartSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRestartInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfHelperSupport ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfExtTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfHelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRestartAckState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfGraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRestartReason ARG_LIST((UINT4 *  ,INT4  ,INT4 ));



/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfRTStaggeringStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfHotStandbyAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfHotStandbyState ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfDynamicBulkUpdStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfStanbyHelloSyncCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMIOspfStanbyLsaSyncCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMIOspfGlobalExtTraceLevel ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfRTStaggeringStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMIOspfGlobalExtTraceLevel ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfRTStaggeringStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIOspfGlobalExtTraceLevel ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfRTStaggeringStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIOspfGlobalExtTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfAreaTable. */
INT1
nmhValidateIndexInstanceFsMIOspfAreaTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfAreaTable  */

INT1
nmhGetFirstIndexFsMIOspfAreaTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfAreaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfAreaIfCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAreaNetCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAreaRtrCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAreaNSSATranslatorRole ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfAreaNSSATranslatorState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfAreaNSSATranslatorStabilityInterval ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfAreaNSSATranslatorEvents ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAreaDfInfOriginate ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfAreaNSSATranslatorRole ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfAreaNSSATranslatorStabilityInterval ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfAreaDfInfOriginate ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfAreaNSSATranslatorRole ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfAreaDfInfOriginate ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfAreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfHostTable. */
INT1
nmhValidateIndexInstanceFsMIOspfHostTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfHostTable  */

INT1
nmhGetFirstIndexFsMIOspfHostTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfHostTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfHostRouteIfIndex ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfHostRouteIfIndex ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfHostRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfHostTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfIfTable. */
INT1
nmhValidateIndexInstanceFsMIOspfIfTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfIfTable  */

INT1
nmhGetFirstIndexFsMIOspfIfTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfIfOperState ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfIfPassive ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfIfNbrCount ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfAdjCount ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfHelloRcvd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfHelloTxed ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfHelloDisd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfDdpRcvd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfDdpTxed ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfDdpDisd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLrqRcvd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLrqTxed ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLrqDisd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLsuRcvd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLsuTxed ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLsuDisd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLakRcvd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLakTxed ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfIfLakDisd ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));
INT1
nmhGetFsMIOspfIfBfdState ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfIfPassive ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
INT1
nmhSetFsMIOspfIfBfdState ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsMIOspfIfBfdState ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
INT1
nmhTestv2FsMIOspfIfPassive ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfIfMD5AuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable ARG_LIST((INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfIfMD5AuthTable  */

INT1
nmhGetFirstIndexFsMIOspfIfMD5AuthTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfIfMD5AuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfIfMD5AuthKey ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfIfMD5AuthKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfIfMD5AuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfIfMD5AuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfIfMD5AuthKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfIfMD5AuthKeyStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfIfMD5AuthKey ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfIfMD5AuthKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfIfMD5AuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfIfMD5AuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfIfMD5AuthKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfIfMD5AuthKeyStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfIfMD5AuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfIfMD5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfVirtIfMD5AuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfVirtIfMD5AuthTable  */

INT1
nmhGetFirstIndexFsMIOspfVirtIfMD5AuthTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfVirtIfMD5AuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfVirtIfMD5AuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfVirtIfMD5AuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfVirtIfMD5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfNbrTable. */
INT1
nmhValidateIndexInstanceFsMIOspfNbrTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfNbrTable  */

INT1
nmhGetFirstIndexFsMIOspfNbrTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfNbrDBSummaryQLen ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfNbrLSReqQLen ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfNbrRestartHelperStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfNbrRestartHelperAge ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfNbrRestartHelperExitReason ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfNbrBfdState ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfRoutingTable. */
INT1
nmhValidateIndexInstanceFsMIOspfRoutingTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfRoutingTable  */

INT1
nmhGetFirstIndexFsMIOspfRoutingTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfRoutingTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfRouteType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfRouteAreaId ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfRouteCost ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfRouteType2Cost ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfRouteInterfaceIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfSecIfTable. */
INT1
nmhValidateIndexInstanceFsMIOspfSecIfTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfSecIfTable  */

INT1
nmhGetFirstIndexFsMIOspfSecIfTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfSecIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfSecIfStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfSecIfStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfSecIfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfSecIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfAreaAggregateTable. */
INT1
nmhValidateIndexInstanceFsMIOspfAreaAggregateTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfAreaAggregateTable  */

INT1
nmhGetFirstIndexFsMIOspfAreaAggregateTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfAreaAggregateTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfAreaAggregateExternalTag ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfAreaAggregateExternalTag ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfAreaAggregateExternalTag ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfAreaAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfAsExternalAggregationTable. */
INT1
nmhValidateIndexInstanceFsMIOspfAsExternalAggregationTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfAsExternalAggregationTable  */

INT1
nmhGetFirstIndexFsMIOspfAsExternalAggregationTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfAsExternalAggregationTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfAsExternalAggregationEffect ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfAsExternalAggregationTranslation ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfAsExternalAggregationStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfAsExternalAggregationEffect ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfAsExternalAggregationTranslation ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfAsExternalAggregationStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfAsExternalAggregationEffect ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfAsExternalAggregationTranslation ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfAsExternalAggregationStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfAsExternalAggregationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfOpaqueTable. */
INT1
nmhValidateIndexInstanceFsMIOspfOpaqueTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfOpaqueTable  */

INT1
nmhGetFirstIndexFsMIOspfOpaqueTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfOpaqueTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfOpaqueOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfType11LsaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfType11LsaCksumSum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfAreaIDValid ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfOpaqueOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfAreaIDValid ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfOpaqueOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfAreaIDValid ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfOpaqueTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfOpaqueInterfaceTable. */
INT1
nmhValidateIndexInstanceFsMIOspfOpaqueInterfaceTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfOpaqueInterfaceTable  */

INT1
nmhGetFirstIndexFsMIOspfOpaqueInterfaceTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfOpaqueInterfaceTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfOpaqueType9LsaCount ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfOpaqueType9LsaCksumSum ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfType9LsdbTable. */
INT1
nmhValidateIndexInstanceFsMIOspfType9LsdbTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfType9LsdbTable  */

INT1
nmhGetFirstIndexFsMIOspfType9LsdbTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfType9LsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfType9LsdbSequence ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfType9LsdbAge ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfType9LsdbChecksum ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfType9LsdbAdvertisement ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIOspfType11LsdbTable. */
INT1
nmhValidateIndexInstanceFsMIOspfType11LsdbTable ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfType11LsdbTable  */

INT1
nmhGetFirstIndexFsMIOspfType11LsdbTable ARG_LIST((INT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfType11LsdbTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfType11LsdbSequence ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfType11LsdbAge ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfType11LsdbChecksum ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfType11LsdbAdvertisement ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIOspfAppInfoDbTable. */
INT1
nmhValidateIndexInstanceFsMIOspfAppInfoDbTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfAppInfoDbTable  */

INT1
nmhGetFirstIndexFsMIOspfAppInfoDbTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfAppInfoDbTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfAppInfoDbOpaqueType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfAppInfoDbLsaTypesSupported ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfAppInfoDbType9Gen ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAppInfoDbType9Rcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAppInfoDbType10Gen ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAppInfoDbType10Rcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAppInfoDbType11Gen ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfAppInfoDbType11Rcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIOspfRRDRouteTable. */
INT1
nmhValidateIndexInstanceFsMIOspfRRDRouteTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfRRDRouteTable  */

INT1
nmhGetFirstIndexFsMIOspfRRDRouteTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfRRDRouteTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfRRDStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRRDSrcProtoMaskEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRRDSrcProtoMaskDisable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfRRDRouteMapEnable ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfRRDStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRRDSrcProtoMaskEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRRDSrcProtoMaskDisable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfRRDRouteMapEnable ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfRRDStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRRDSrcProtoMaskEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRRDSrcProtoMaskDisable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRRDRouteMapEnable ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfRRDRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfRRDRouteConfigTable. */
INT1
nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfRRDRouteConfigTable  */

INT1
nmhGetFirstIndexFsMIOspfRRDRouteConfigTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfRRDRouteConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfRRDRouteMetric ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfRRDRouteMetricType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfRRDRouteTagType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfRRDRouteTag ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfRRDRouteStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfRRDRouteMetric ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfRRDRouteMetricType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfRRDRouteTagType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfRRDRouteTag ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIOspfRRDRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfRRDRouteMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRRDRouteMetricType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRRDRouteTagType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfRRDRouteTag ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIOspfRRDRouteStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfRRDRouteConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfVirtNbrTable. */
INT1
nmhValidateIndexInstanceFsMIOspfVirtNbrTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfVirtNbrTable  */

INT1
nmhGetFirstIndexFsMIOspfVirtNbrTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfVirtNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfVirtNbrRestartHelperStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfVirtNbrRestartHelperAge ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIOspfVirtNbrRestartHelperExitReason ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfDistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsMIOspfDistInOutRouteMapTable ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfDistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsMIOspfDistInOutRouteMapTable ARG_LIST((
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfDistInOutRouteMapTable ARG_LIST((
    INT4    , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4    , 
    INT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfDistInOutRouteMapValue ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ,
    INT4    *));

INT1
nmhGetFsMIOspfDistInOutRouteMapRowStatus ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfDistInOutRouteMapValue ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

INT1
nmhSetFsMIOspfDistInOutRouteMapRowStatus ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfDistInOutRouteMapValue ARG_LIST((UINT4 *  ,
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

INT1
nmhTestv2FsMIOspfDistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfDistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfPreferenceTable. */
INT1
nmhValidateIndexInstanceFsMIOspfPreferenceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfPreferenceTable  */

INT1
nmhGetFirstIndexFsMIOspfPreferenceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfPreferenceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfPreferenceValue ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfPreferenceValue ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfPreferenceValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfPreferenceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFirstIndexFsMIOspfRRDMetricTable ARG_LIST((INT4 * , INT4 * ));

INT1
nmhGetNextIndexFsMIOspfRRDMetricTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

INT1
nmhGetFsMIOspfRRDMetricValue ARG_LIST((INT4  , INT4 , INT4 *));

INT1
nmhGetFsMIOspfRRDMetricType ARG_LIST((INT4  , INT4 , INT4 *));

INT1
nmhTestv2FsMIOspfRRDMetricValue ARG_LIST((UINT4 *  ,INT4 , INT4  , INT4  ));

INT1
nmhTestv2FsMIOspfRRDMetricType ARG_LIST((UINT4 *  ,INT4 , INT4  , INT4  ));


INT1
nmhSetFsMIOspfRRDMetricValue ARG_LIST((INT4  ,INT4 ,INT4 ));

INT1
nmhSetFsMIOspfRRDMetricType ARG_LIST((INT4  ,INT4 ,INT4 ));

INT1
nmhValidateIndexInstanceFsMIOspfRRDMetricTable ARG_LIST((INT4 , INT4 ));

INT1
nmhDepv2FsMIOspfRRDMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



/* Proto Validate Index Instance for FsMIOspfIfAuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfIfAuthTable ARG_LIST((INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfIfAuthTable  */

INT1
nmhGetFirstIndexFsMIOspfIfAuthTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfIfAuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfIfAuthKey ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsMIOspfIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsMIOspfIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsMIOspfIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsMIOspfIfAuthKeyStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfIfAuthKey ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsMIOspfIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsMIOspfIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfIfAuthKeyStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIOspfIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIOspfIfAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfIfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfVirtIfAuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfVirtIfAuthTable  */

INT1
nmhGetFirstIndexFsMIOspfVirtIfAuthTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfVirtIfAuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfVirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfVirtIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE  *));

INT1
nmhGetFsMIOspfVirtIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsMIOspfVirtIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsMIOspfVirtIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE  *));

INT1
nmhGetFsMIOspfVirtIfAuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfVirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfVirtIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsMIOspfVirtIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsMIOspfVirtIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsMIOspfVirtIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsMIOspfVirtIfAuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfVirtIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfVirtIfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

