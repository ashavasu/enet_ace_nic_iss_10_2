/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osappif.h,v 1.9 2007/04/25 15:04:37 iss Exp $
 *                                                                  
 * Description :This File contains macros,constants and type definitions         *              related to network management
**********************************************************************/
#ifndef _OSAPPIF_H
#define _OSAPPIF_H

#define  OSPF_APP_IF_MSG_ALLOC()    OpqOspfIfMsgAlloc()

#define  OSPF_APP_IF_MSG_FREE(pMsg)   QMSG_FREE (pMsg);

#define  OSPF_APP_IF_MSG_GET_PARAMS_PTR(pMsg) \
                      &(pMsg->appOspfIfParam)

#define  OSPF_APP_IF_MSG_SEND(pMsg) \
                           OpqOspfIfMsgSend(pMsg) 
#define  OSPF_APP_IF_EVENT_SEND() \
         OsixSendEvent (SELF, (const UINT1 *)"OSPF" , OSPF_MSGQ_IF_EVENT )

  

#define  SND_OPQ_LSA_FROM_APP_TO_OSPF   1
#define  OPQ_APP_DEREG                  3
#define  OPQ_APP_REG                        4


#define  TYPE9_BIT_MASK                1    /* 00000001 */
#define  TYPE10_BIT_MASK               2    /* 00000010 */
#define  TYPE11_BIT_MASK               4    /* 00000100 */


/*
 * This structure contains the Opcode & Parameters that have to be passed while
 * posing an event to the OSPF from application context and vice-versa.
 */
typedef VOID (*tAppRelFuncPtr)(UINT1 *);
typedef VOID (*tOspfOpqRelFuncPtr)(UINT1*);

typedef struct _AppOspfIfParam {
   UINT1      u1AppOpCode;        /* Application operation code */
   UINT1      au1Rsvd[3];
   UINT4      u4OspfCxtId;
   union {
      struct {
          tOpqLSAInfo    *pOpqLSAInfo;
      } AppTxOpqLsaParam;
      struct {
          UINT1        u1AppOpqtype;
          UINT1        au1Rsvd[3];
      } AppDeRegParm;
      struct {
          UINT1        u1AppOpqType;
          UINT1        au1Rsvd[3];
      } AppRegParm;
   } AppParam;
} tAppOspfIfParam;

#endif /* _OSAPPIF_H */
