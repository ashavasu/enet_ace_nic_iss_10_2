/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osmacs.h,v 1.31 2017/05/12 13:35:38 siva Exp $
 *
 * Description:This file contains macros relating to protocol.
 *
 *******************************************************************/

#ifndef _OSMACS_H
#define _OSMACS_H

 /****************************************************************************/
/* mathematical                                                             */
/****************************************************************************/

#define  OSPF_MIN(a,b)  ((a < b) ? a : b ) 

#define  OSPF_MAX(a,b)  ((a > b) ? a : b ) 

#define  DIFF(a,b)  ((a < b) ? (b-a) : (a-b)) 

#define  ROUND_OFF(x,y)  x = (UINT2) (x - (x % y))    /* converts x to a multiple of y */

#define  OSPF_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#define COUNTER_OP(var, value) \
      (var) += (value);

/* convert into OSPF TOS form */
#define  ENCODE_TOS(x)  (x * 2) 

/* convert from OSPF TOS form */

#define  DECODE_TOS(x)  (x / 2) 


/****************************************************************************/
/* macros for operation on statiscal counters and gauges                    */
/****************************************************************************/

#define INC_DISCARD_HELLO_CNT(p_iface) \
        COUNTER_OP(p_iface->u4HelloDisdCount, 1); 
#define INC_DISCARD_DDP_CNT(p_iface) \
        COUNTER_OP(p_iface->u4DdpDisdCount, 1);
#define INC_DISCARD_LRQ_CNT(p_iface) \
        COUNTER_OP(p_iface->u4LsaReqDisdCount, 1);
#define INC_DISCARD_LSU_CNT(p_iface) \
        COUNTER_OP(p_iface->u4LsaUpdateDisdCount, 1);
#define INC_DISCARD_LAK_CNT(p_iface) \
        COUNTER_OP(p_iface->u4LsaAckDisdCount, 1);


/****************************************************************************/
/* state machines                                                           */
/****************************************************************************/
 
#define  GENERATE_IF_EVENT(pInterface, event)  IsmRunIsm(pInterface, event); 

#define  GENERATE_NBR_EVENT(pNbr, event)       NsmRunNsm(pNbr, event);       


/****************************************************************************/
/* assignment macros                                                        */
/****************************************************************************/
 
#define  IP_ADDR_COPY(dst,src)   OS_MEM_CPY(dst, src, MAX_IP_ADDR_LEN) 

#define  SET_NULL_IP_ADDR(addr)  OSPF_CRU_BMC_DWTOPDU((addr), 0)       


#define CPY_TO_SNMP(pSnmpStruct, pu1Strg, u1Len) \
               MEMCPY((pSnmpStruct)->pu1_OctetList, (pu1Strg), (u1Len)) ; \
               (pSnmpStruct)->i4_Length = (u1Len);
#define OSPF_DYNM_SLL_SCAN(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))))

/****************************************************************************/
/* macros for size of various structures                                    */
/****************************************************************************/


#define NETWORK_LSA_FIXED_PORTION_SIZE (LS_HEADER_SIZE + sizeof(tIPADDRMASK))

#define MAX_SUMMARY_LSA_SIZE    (LS_HEADER_SIZE + sizeof(tIPADDRMASK) + \
                                 (OSPF_MAX_METRIC * 4))

#define MAX_TYPE7_LSA_SIZE  (LS_HEADER_SIZE + sizeof(tIPADDRMASK) + \
                           (OSPF_MAX_METRIC * NSSA_LSA_LINK_SIZE))

#ifndef HIGH_PERF_RXMT_LST_WANTED
#define LSA_INFO_SIZE (sizeof(tLsaInfo) +  \
        ((MAX_OSPF_NBRS/BIT_MAP_FLAG_UNIT_SIZE) * sizeof(tRXMTFLAG)))
#else
#define LSA_INFO_SIZE (sizeof(tLsaInfo)) 
#endif

#define LSA_HASH_KEY_SIZE (sizeof(UINT1) + sizeof(tLINKSTATEID) + \
                           sizeof(tRouterId))

#define CANDTE_NODE_SIZE (sizeof(tCandteNode)) 
#define PATH_SIZE (sizeof(tPath))


/****************************************************************************/
/* macros for accessing global variables                                    */
/****************************************************************************/

#define  ADDRLESS_IF  1                   /* Check */

/* macro for accessing Opaque Application Information */
#define APP_INFO_IN_CXT(pOspfCxt, value)         (pOspfCxt->pAppInfo[value])

/****************************************************************************/
/* macros for get functions                                                 */
/****************************************************************************/

#define GET_IF_AREA_ID(x)       (((x)->u1NetworkType == IF_VIRTUAL) ? \
                                    gBackboneAreaId : (x)->pArea->areaId )

#define  GET_IF_COST(if_speed)  (UINT4)( 100000000 / if_speed) 

#define GET_IF_TOS_0_COST(p_if) p_if->aIfOpCost[TOS_0].u4Value

#define  GET_DR(x)           ((x)->desgRtr)                               

#define  GET_BDR(x)          ((x)->backupDesgRtr)           

#define  GET_NBR_ID(x)       ( ((x) == 0) ?  gNullIpAddr : (x)->nbrId) 

#define  GET_DR_IP_ADDR(x)   GET_DR(x)                                     

#define  GET_BDR_IP_ADDR(x)  GET_BDR(x)                                    

    
#define  GET_ASE_LSA_COUNT_IN_CXT(pOspfCxt)  (INT4)LsuGetExtLsaCountInCxt(pOspfCxt)


#define GET_TYPE9_LSA_COUNT(pInterface) \
         TMO_SLL_Count(&pInterface->Type9OpqLSALst)

#define GET_TYPE10_LSA_COUNT(pArea) \
         TMO_SLL_Count(&pArea->Type10OpqLSALst)

#define GET_LSA_OPTIONS(pLsa)      *((UINT1 *)((pLsa) + \
                                             OPTIONS_OFFSET_IN_LS_HEADER))

#define  GET_LSA_MASK(pLsa)  *((UINT4 *)(pLsa + LS_HEADER_SIZE)) 

#define Type7LSA_FWD_ADDR_OFFSET(tos) \
            ((LS_HEADER_SIZE+MAX_IP_ADDR_LEN)+(tos)*NSSA_LSA_LINK_SIZE+4)

/* New macro introduced to enable the list scan
 * and deletion of node from the list simultaneously.
 */ 
#define TMO_SLL_Scan_Ospf(pList, pNode, pTempNode, TypeCast)\
for(((pNode) = (TypeCast)(TMO_SLL_First((pList)))),\
     ((pTempNode) = ((pNode == NULL) ? NULL : \
                         ((TypeCast) \
                         (TMO_SLL_Next((pList), \
                                ((tTMO_SLL_NODE *)(pNode)))))));\
    (pNode) != NULL;\
    (pNode) = (pTempNode), \
    ((pTempNode) = ((pNode == NULL) ? NULL :\
                      ((TypeCast) \
                       (TMO_SLL_Next((pList), \
                              ((tTMO_SLL_NODE *)(pNode))))))))


#define  GET_Type7LSA_FwdAddr(pLsa, offset)  *((UINT4 *)(VOID *)(pLsa + offset)) 

#define IS_DNA_LSA(lsa_info) \
                ((lsa_info->u2LsaAge) & DO_NOT_AGE)

/* This macro gets the path type of the rt entry for the specified TOS */
/* Macro is modified for removing the klocwork warnings */
#define GET_PATH_TYPE(pRtEntry, tos)  RtcGetPathType(pRtEntry, tos)

/* This macro gets the area id of the TOS 0 entry of the rt entry */
#define GET_PATH_AREA_ID(pRtEntry) \
   ((tPath *)TMO_SLL_First( &(pRtEntry->aTosPath[TOS_0])))->areaId

/* This macro gets the first of the list of paths corresponding to a specified
 * TOS in the routing table entry.
 */
#define GET_TOS_PATH(pRtEntry, tos) \
   (tPath *)TMO_SLL_First( &(pRtEntry->aTosPath[(INT2)tos]))

#define EXTLSA_FWD_ADDR_OFFSET(tos) \
            ((LS_HEADER_SIZE+MAX_IP_ADDR_LEN)+(tos)*EXT_LSA_LINK_SIZE+4)
#define OS_GET_FWD_ADDR(lsa,tos,addr)  \
            OS_MEM_CPY(addr,(lsa+EXTLSA_FWD_ADDR_OFFSET(tos)),MAX_IP_ADDR_LEN)

/****************************************************************************/
/* getting the appropriate pointers from the SLLs                           */
/****************************************************************************/
 
#define GET_NBR_PTR_FROM_NBR_LST(x) (tNeighbor *)(VOID *)((UINT1*)(x) \
                                   - OSPF_OFFSET(tNeighbor, nextNbrInIf))

#define GET_NBR_PTR_FROM_SORT_LST(x) (tNeighbor *)(VOID *)((UINT1*)(x) \
                                   - OSPF_OFFSET(tNeighbor, nextSortNbr))

#define GET_LSA_INFO_PTR_FROM_LST(x) (tLsaInfo *)(VOID *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tLsaInfo, nextLsaInfo))

#define GET_LSA_INFO_PTR_FROM_RXMT_LST(x) (tLsaInfo *)(VOID *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tLsaInfo, rxmtNode))

#define GET_IF_PTR_FROM_SORT_LST(x) ((tInterface *)(VOID *)(((UINT1 *)(x)) \
                                   - OSPF_OFFSET(tInterface, nextSortIf)))

#define GET_IF_PTR_FROM_LST(x) (tInterface *)(VOID *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tInterface, nextIfInArea))
                                   
/*  Getting the base pointers */
#define OSPF_GET_BASE_PTR(type, memberName, pMember) \
       (type *) ((FS_ULONG)(pMember) - (FS_ULONG)(&((type *)0)->memberName))
       
#define GET_ASEXTRNG_FROM_RTRLST(x) (tAsExtAddrRange*)(VOID *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tAsExtAddrRange, nextAsExtAddrRngInRtr))

#define GET_EXTRT_FROM_ASEXTLST(x) (tExtRoute*)(VOID *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tExtRoute, nextExtRouteInRange))

#define GET_LSADESC_FROM_LSADESCLST(x) (tLsaDesc*)(VOID *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tLsaDesc, lsaDescNode))



/****************************************************************************/
/* macros  for conditions                                                   */
/****************************************************************************/

#define IS_NULL_IP_ADDR(addr)   (OSPF_CRU_BMC_DWFROMPDU((addr)) == 0)

#define IS_EQUAL_ADDR(addr1, addr2) \
                                (UtilIpAddrComp(addr1, addr2) == OSPF_EQUAL)

#define IS_NTBIT_SET_RTR_LSA(pLsa)  (*((UINT1 *)(pLsa + LS_HEADER_SIZE)) \
                                         & NT_BIT_MASK)


#define IS_P_BIT_SET(pLsaInfo)  (pLsaInfo->lsaOptions & P_BIT_MASK)

/* Aggregate route information */
#define IS_THIS_PART_OF_OSPF_AGGR_ROUTE(u4LsaAddr, u4RangeAddr, u4RangeMask) \
    ((u4LsaAddr & u4RangeMask) == u4RangeAddr)

#define  IS_STUB_AREA(pArea)  (pArea->u4AreaType == STUB_AREA) 
#define IS_SELF_ORIGINATED_LSA_IN_CXT(pLsaInfo) \
                        (UtilIpAddrComp(pLsaInfo->lsaId.advRtrId,\
                        pLsaInfo->pArea->pOspfCxt->rtrId) == OSPF_EQUAL)

#define IS_AREA_TRANSIT_CAPABLE(pArea) \
                                (pArea->bTransitCapability == OSPF_TRUE)

#define IS_V_BIT_SET(pLsa) \
                         (*(pLsa + LS_HEADER_SIZE) & V_BIT_MASK)

#define IS_SEND_AREA_SUMMARY(pArea)    \
                         (pArea->u1SummaryFunctionality == SEND_AREA_SUMMARY)

#define IS_NOT_ADVERTISABLE_ADDR_RANGE(pAddrRange)    \
                (pAddrRange->u1AdvertiseStatus == DO_NOT_ADVERTISE_MATCHING)

#define IS_VIRTUAL_IFACE(p_iface)       \
                                (p_iface->u1NetworkType == IF_VIRTUAL)

#define IS_PTOP_IFACE(p_iface)          \
                                (p_iface->u1NetworkType == IF_PTOP)
#define IS_PTOMP_IFACE(p_iface)          \
                                (p_iface->u1NetworkType == IF_PTOMP)
 
#define IS_NUMBERED_PTOP_IFACE(p_iface) \
                                (p_iface->u1NetworkType == IF_PTOP && \
                                 p_iface->u4AddrlessIf == 0)

#define IS_UNNUMBERED_PTOP_IFACE(p_iface)   \
                                (p_iface->u1NetworkType == IF_PTOP && \
                                 p_iface->u4AddrlessIf != 0)                                 
#define IS_DR(p_iface)           (p_iface->u1IsmState == IFS_DR)
#define IS_BDR(p_iface)          (p_iface->u1IsmState == IFS_BACKUP)

#define IS_VALID_IF_COST(p_iface, tos)      \
               (p_iface->aIfOpCost[(INT2)tos].rowStatus == ACTIVE && \
                p_iface->aIfOpCost[(INT2)tos].u4Value < LS_INFINITY_16BIT)

#define IS_VALID_STUB_AREA_COST(pArea, tos)      \
         (pArea->aStubDefaultCost[(INT2)tos].rowStatus == ACTIVE && \
          pArea->aStubDefaultCost[(INT2)tos].u4Value < LS_INFINITY_16BIT)

#define IS_VALID_ADDR_RANGE_COST(p_range, tos)      \
                   ((p_range->aMetric[(INT2)tos].bStatus == OSPF_VALID) && \
                    (p_range->aMetric[(INT2)tos].u4Value < LS_INFINITY_24BIT)&&\
                    (p_range->aMetric[(INT2)tos].u4Value > 0))

#define IS_VALID_SUM_PARAM_COST(p_param, tos)      \
                   (p_param->aMetric[(INT2)tos].bStatus == OSPF_VALID && \
                    p_param->aMetric[(INT2)tos].u4Value < LS_INFINITY_24BIT)

#define  IS_NBR_FULL(pNbr)      (pNbr->u1NsmState == NBRS_FULL)        



#define  IS_DEST_NETWORK(p_rte)  (p_rte->u1DestType == DEST_NETWORK)
#define  IS_DEST_ABR(p_rte)      (p_rte->u1DestType == DEST_AREA_BORDER)
#define  IS_DEST_ASBR(p_rte)     (p_rte->u1DestType == DEST_AS_BOUNDARY)

#define IS_INTRA_AREA_PATH(p_rte)       \
                            (GET_PATH_TYPE(p_rte, TOS_0) == INTRA_AREA)

#define IS_INTER_AREA_PATH(p_rte)       \
                            (GET_PATH_TYPE(p_rte, TOS_0) == INTER_AREA)

#define IS_TYPE_1_EXT_PATH(p_rte)       \
                            (GET_PATH_TYPE(p_rte, TOS_0) == TYPE_1_EXT)

#define IS_TYPE_2_EXT_PATH(p_rte)       \
                            (GET_PATH_TYPE(p_rte, TOS_0) == TYPE_2_EXT)


#define  IS_VALID_LS_TYPE(type)  ((type >= 1) && (type <= MAX_LSA_TYPE)) 

#define IS_AREA_BORDER_RTR_LSA(pLsa)  (*((UINT1 *)(pLsa + LS_HEADER_SIZE)) \
                                         & ABR_MASK)
#define IS_AS_BOUNDARY_RTR_LSA(pLsa)  (*((UINT1 *)(pLsa + LS_HEADER_SIZE)) \
                                         & ASBR_MASK)

#define IS_NON_DEFAULT_ASE_LSA(lsa)  \
                          ((lsa.u1LsaType == AS_EXT_LSA) && \
                          (!IS_NULL_IP_ADDR(lsa.linkStateId)))

#define IS_NBMA_IFACE(pInterface) \
                          (pInterface->u1NetworkType == IF_NBMA)

#define E_BIT_OPTION_MISMATCH(pInterface,options) \
        ((pInterface->pArea->u4AreaType == NORMAL_AREA) \
        && !(E_BIT_MASK & options)) \
        ||(((pInterface->pArea->u4AreaType == NSSA_AREA) \
        ||(pInterface->pArea->u4AreaType == STUB_AREA)) \
        &&(E_BIT_MASK & options))

/* To check "N" bit option matching for NSSA */
#define N_BIT_OPTION_MISMATCH(pInterface,options) \
  (((pInterface->pArea->u4AreaType == NSSA_AREA) \
  && !(N_BIT_MASK & options)) \
  || (((pInterface->pArea->u4AreaType == NORMAL_AREA) \
           || (pInterface->pArea->u4AreaType == STUB_AREA)) \
           && (N_BIT_MASK & options)))

#define NBR_LOSE_OR_GAIN_DR_OR_BDR(pNbr,dr_bdr_rtr) \
        ((UtilIpAddrComp(pNbr->nbrIpAddr, pNbr->backupDesgRtr) != OSPF_EQUAL) \
         &&(UtilIpAddrComp(pNbr->nbrIpAddr, dr_bdr_rtr) == OSPF_EQUAL)) \
      ||((UtilIpAddrComp(pNbr->nbrIpAddr, pNbr->backupDesgRtr) == OSPF_EQUAL) \
         &&(UtilIpAddrComp(pNbr->nbrIpAddr, dr_bdr_rtr) != OSPF_EQUAL))

#define BACKBONE_ID(areaId) IS_NULL_IP_ADDR(areaId)
#define IS_NBR_OPQ_CAPABLE(pNbr)     (pNbr->nbrOptions & OPQ_BIT_MASK)
#define IS_OPQ_LSA(u1LsaType)    \
                      ((u1LsaType == TYPE9_OPQ_LSA ) ||\
                       (u1LsaType == TYPE10_OPQ_LSA) ||\
                       (u1LsaType == TYPE11_OPQ_LSA))
#define IS_TYPE9(u1Type)  (u1Type & TYPE9_BIT_MASK)
#define IS_TYPE10(u1Type) (u1Type & TYPE10_BIT_MASK)
#define IS_TYPE11(u1Type) (u1Type & TYPE11_BIT_MASK)
#define IS_VALID_OPQ_LSA_TYPE_SUPPORT(u1OpqLSATypesSupported) \
        ((u1OpqLSATypesSupported & 7) == 0)

#define IS_CONFIGURED_AREA(pArea) \
                              (TMO_SLL_Count (&(pArea->ifsInArea)) > 0)

#define IS_CONFIGURED_BACKBONE_IN_CXT(pOspfCxt) \
                      (((TMO_SLL_Count (&(pOspfCxt->pBackbone->ifsInArea))) -  \
                        ((TMO_SLL_Count (&(pOspfCxt->virtIfLst))))) > 0)

#define IS_ACTIVELY_ATTACHED_AREA(pArea) \
                              (pArea->u4ActIntCount > 0)


/* Macros for NSSA */
#define IS_TRNST_AREA(pArea)  (pArea->u4AreaType == NORMAL_AREA)
#define IS_TRNSLTR_ALWAYS(pArea) \
  (pArea->u1NssaTrnsltrRole == TRNSLTR_ROLE_ALWAYS)



 /* macros for retransmission logic */

#define  SET_BIT_POSITION(index ,bit_pos_mask,p_byte) \
         { \
            INT2 i2ArrayIndex; \
            i2ArrayIndex = (INT2) (index/BIT_MAP_FLAG_UNIT_SIZE); \
            bit_pos_mask = bit_pos_mask << (index % BIT_MAP_FLAG_UNIT_SIZE);\
            p_byte[i2ArrayIndex] = p_byte[i2ArrayIndex] | bit_pos_mask; \
         }

#define  RESET_BIT_POSITION(index ,bit_pos_mask,p_byte) \
         { \
            INT2 i2ArrayIndex; \
            i2ArrayIndex = (INT2) (index/BIT_MAP_FLAG_UNIT_SIZE); \
            bit_pos_mask = ~(bit_pos_mask << (index % BIT_MAP_FLAG_UNIT_SIZE));\
            p_byte[i2ArrayIndex] = p_byte[i2ArrayIndex] & bit_pos_mask;\
         }
            
#define  IS_RXMT_BIT_SET(index, lsa_info) \
         ((lsa_info->aRxmtBitMapFlag[index/BIT_MAP_FLAG_UNIT_SIZE] >> \
         (index % BIT_MAP_FLAG_UNIT_SIZE)) & FIRST_BIT_SET_MASK)


#define IS_DEFAULT_ASE_LSA_PRESENT_IN_CXT(pOspfCxt)  \
             (pOspfCxt->bDefaultAseLsaPresenc == OSPF_TRUE)

#define GET_NON_DEF_ASE_LSA_COUNT_IN_CXT(pOspfCxt)  \
     (GET_ASE_LSA_COUNT_IN_CXT(pOspfCxt) - \
         (IS_DEFAULT_ASE_LSA_PRESENT_IN_CXT(pOspfCxt) ? 1 : 0))
 

#define IS_MAX_AGE(age) ((age >= MAX_AGE && age < DO_NOT_AGE) || \
                     (age >= MAX_AGE + DO_NOT_AGE))

#define INC_LSA_AGE(lsa_age, inc_value)  \
    ((((lsa_age)+(inc_value)) > (MAX_AGE + DO_NOT_AGE)) ? \
    (MAX_AGE + DO_NOT_AGE) : ((lsa_age)+(inc_value)))


#define IS_INDICATION_LSA(lsa_info) \
            ((!(IS_DC_BIT_SET_LSA(lsa_info))) && \
                     (lsa_info->lsaId.u1LsaType == ASBR_SUM_LSA) && \
             (THREE_BYTE_FROM_PDU(lsa_info->pLsa + SUMMARY_LSA_METRIC_OFFSET) \
               == LS_INFINITY_24BIT))

#define IS_INDICATION_LSA_PRESENCE(area) \
                    (area->bIndicationLsaPresence == OSPF_TRUE)

#define IS_DNA_LSA_PROCESS_CAPABLE(area) \
                        (area->u4DcBitResetLsaCount == 0)

#define IS_DC_BIT_SET_LSA(lsa_info) \
                    ((GET_LSA_OPTIONS(lsa_info->pLsa)) & DC_BIT_MASK)

#define IS_DC_EXT_APPLICABLE_IF(interface) \
                    (interface->bDcEndpt == OSPF_TRUE)

#define IS_DC_EXT_APPLICABLE_PTOP_IF(interface) \
        (interface->bDcEndpt == OSPF_TRUE &&  \
         interface->u1NetworkType == IF_PTOP)

#define IS_DC_EXT_APPLICABLE_PTOMP_IF(interface) \
        (interface->bDcEndpt == OSPF_TRUE &&  \
         interface->u1NetworkType == IF_PTOMP)

#define IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_IF(interface) \
       (interface->bDcEndpt == OSPF_TRUE && \
        (interface->u1NetworkType == IF_PTOP ||  \
         interface->u1NetworkType == IF_VIRTUAL))


#define IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF(interface) \
       (interface->bDcEndpt == OSPF_TRUE && \
        (interface->u1NetworkType == IF_PTOP ||  \
         interface->u1NetworkType == IF_VIRTUAL || \
        interface->u1NetworkType == IF_PTOMP))

#define IS_DISCOVERED_DC_ENDPOINT_IFACE(interface) \
       (interface->u1ConfStatus == DISCOVERED_ENDPOINT) 

#define IS_HELLO_SUPPRESSED_NBR(nbr) \
       (nbr->bHelloSuppression == OSPF_TRUE)


#define GET_LSA_AGE(pLsaInfo,pu2_lsa_age) { \
  if ((IS_MAX_AGE (pLsaInfo->u2LsaAge)) && (!IS_DNA_LSA (pLsaInfo)))\
      *(pu2_lsa_age) = pLsaInfo->u2LsaAge;\
  else {\
  UINT4 u4RemainingTime; \
  TmrGetRemainingTime ( gTimerLst , (tTmrAppTimer *)&(pLsaInfo->lsaAgingTimer.timerNode) ,  (UINT4 *)&(u4RemainingTime) );\
  *(pu2_lsa_age) = (UINT2) ((IS_DNA_LSA(pLsaInfo)) ? (pLsaInfo->u2LsaAge) : (pLsaInfo->u2LsaAge + ((CHECK_AGE - (pLsaInfo->u2LsaAge % CHECK_AGE)) - (UINT2)(u4RemainingTime/NO_OF_TICKS_PER_SEC))));}\
}



#define IS_TRAP_ENABLED_IN_CXT(trap_id, pOspfCxt) (pOspfCxt->u4TrapControl & \
                                           (1 << (trap_id - 1)))

#define IS_INITIAL_IFACE_ACTIVITY_TRAP(interface) \
           (interface->u1HelloIntervalsElapsed < \
            (2 * interface->i4RtrDeadInterval)/interface->u2HelloInterval)
#define IS_INITIAL_RTR_ACTIVITY_TRAP_IN_CXT(pOspfCxt) \
           (pOspfCxt->u1SwPeriodsElapsed < 16)
            /* 16 = 2 * 80(default router dead interval) / 10 (SWP) */

/****************************************************************************/
/* functional macros                                                        */
/****************************************************************************/

#define OLS_INTERCHANGE_METRICS(x, y) \
    {   \
        tMetric    tmp[OSPF_MAX_METRIC];    \
        OS_MEM_CPY(tmp, x, (sizeof(tMetric)*OSPF_MAX_METRIC));    \
        OS_MEM_CPY(x, y, (sizeof(tMetric)*OSPF_MAX_METRIC));    \
        OS_MEM_CPY(y, tmp, (sizeof(tMetric)*OSPF_MAX_METRIC));    \
    }
 
#define OLS_RTR_LSA_ADD_LINK(currPtr,linkId,linkData,link_type,link_count) \
                      LADDSTR(currPtr, linkId, MAX_IP_ADDR_LEN); \
                      LADDSTR(currPtr, linkData, MAX_IP_ADDR_LEN);\
                      LADD1BYTE(currPtr, link_type); \
                      link_count++;

#define OLS_ADD_UN_NUM_PTP_LNK(currPtr,linkId,linkData,link_type,link_count)\
                      LADDSTR(currPtr, linkId, MAX_IP_ADDR_LEN); \
                      LADD4BYTE(currPtr, linkData);\
                      LADD1BYTE(currPtr, link_type); \
                      link_count++; 
                     
#define GET_IP_NET_NUM(netnum, addr, mask) \
    { \
        UINT4 u4Tmp; \
        u4Tmp = (OSPF_CRU_BMC_DWFROMPDU(addr)) & (OSPF_CRU_BMC_DWFROMPDU(mask)); \
        OSPF_CRU_BMC_DWTOPDU((UINT1 *)netNum, u4Tmp); \
    }

#define LSU_SEARCH_RXMT_LST(pNbr, pLsaInfo) \
              pLsaInfo->aRxmtBitMapFlag[pNbr->i2NbrsTableIndex]

#define IFACE_MTU(p_iface)      (p_iface->u4MtuSize - IP_HEADER_SIZE)

#define IF_HASH_FN(u4IfIndex) UtilHashGetValue(IF_HASH_TABLE_SIZE, \
                                    (UINT1 *)(&(u4IfIndex)), sizeof(UINT4))

/****************************************************************************/
/* macros for lsa construction in linear buffers                            */
/****************************************************************************/
 
#define  LADD1BYTE(addr, val)     *((UINT1 *)addr) = val; addr++;             
#define  LADD2BYTE(addr, val)     OSPF_CRU_BMC_WTOPDU(addr, val); addr += 2;  
#define  LADD3BYTE(addr, val)     THREE_BYTE_TO_PDU(addr, (val)); addr += 3;  
#define  LADD4BYTE(addr, val)     OSPF_CRU_BMC_DWTOPDU(addr, val); addr += 4; 
#define  LADDSTR(addr, src, len)  OS_MEM_CPY(addr, src, len); addr += len;    

/* macros extracting info from linear buffer */
#define LGETSTR(addr, dest, len) \
                               OS_MEM_CPY((UINT1 *)dest, (UINT1 *)addr, len);\
                               addr += len;

/* the following macros have certain constraints
 * 1. used only in the rhs of assignment operator
 * 2. addr should be a single variable(not an expression)
 * 3. the macro itself should not be a part of any expression
 */

#define  LGET1BYTE(addr)  *((UINT1 *)addr); addr += 1;             
#define  LGET2BYTE(addr)  OSPF_CRU_BMC_WFROMPDU(addr); addr += 2;  
#define  LGET3BYTE(addr)  THREE_BYTE_FROM_PDU(addr); addr += 3;    
#define  LGET4BYTE(addr)  OSPF_CRU_BMC_DWFROMPDU(addr); addr += 4; 

#define THREE_BYTE_TO_PDU(addr, val) { \
                 *((UINT1 *)(addr)   ) =(UINT1) ((val >> 16)&0xff); \
                 *((UINT1 *)(addr) +1) =(UINT1) ((val >> 8)&0xff); \
                 *((UINT1 *)(addr) +2) =(UINT1) (val & 0xff);}

#define THREE_BYTE_FROM_PDU(addr) \
       ( ((*((UINT1 *)(addr)   ) << 16) & 0x00ff0000) |\
         ((*((UINT1 *)(addr) +1) << 8 ) & 0x0000ff00) |\
         ((*((UINT1 *)(addr) +2)      ) & 0x000000ff) )

#define  IS_VALID_AS_NO(x)   ((x > 0) && (x <= 0xffff)) 

#define  IS_VALID_RTR_ID(x)  ((!IS_NULL_IP_ADDR(x)))    

#define   OSPF_RTM_IF_GET_MSG_HDR_PTR(pMsg)   \
                              IP_GET_MODULE_DATA_PTR(pMsg)

#define OSFT_GET_AREA_ID(u4AreaId, pArea) \
{ \
    tAreaId TmpAreaId; \
    *((UINT4 *)TmpAreaId) = OSIX_NTOHL (u4AreaId); \
    pArea = GetFindArea (&TmpAreaId); \
}

/* Process only 5 % of the messages in low priority Q */
#define OSPF_GET_MSG_COUNT(u4LowPriMessageCount)\
                  (((u4LowPriMessageCount * 100)/100) ? ((u4LowPriMessageCount * 100)/100):1)

#ifdef HIGH_PERF_RXMT_LST_WANTED
#define  NBR_HEAD_NEXT_DLL_INDEX  1
#define  NBR_HEAD_PREV_DLL_INDEX  2
#define  LSA_HEAD_NEXT_DLL_INDEX  1
#define  LSA_HEAD_PREV_DLL_INDEX  2
#define  NBR_NEXT_DLL_INDEX       1
#define  NBR_PREV_DLL_INDEX       2
#define  LSA_NEXT_DLL_INDEX       3
#define  LSA_PREV_DLL_INDEX       4
#define  OSPF_INVALID_RXMT_INDEX  MAX_RXMT_NODES

#define OSPF_SET_DLL_INDEX_IN_RXMT_NODE(au1RxmtInfo, bit_position, value) \
    UtilOspfBitSet (au1RxmtInfo, bit_position, value)

#define OSPF_GET_DLL_INDEX_IN_RXMT_NODE(au1RxmtInfo, bit_position, value) \
    UtilOspfBitGet (au1RxmtInfo, bit_position, &value)

#define GET_RXMT_NODE_INDEX(pRxmtNode) \
          (((UINT1*) pRxmtNode-(UINT1*)(&gOsRtr.aRxmtNode)) ? \
      (((UINT1*) pRxmtNode-(UINT1*)(&gOsRtr.aRxmtNode))/sizeof(tRxmtNode)):0)

#endif

/* Macros for function calls common for high and normal performance retransmission */

#ifdef HIGH_PERF_RXMT_LST_WANTED 
#define LsuClearRxmtLst(pNbr)   HrLsuClearRxmtLst (pNbr) 
#define LsuDeleteFromRxmtLst(pNbr, pLsaInfo)  HrLsuDeleteFromRxmtLst (pNbr, pLsaInfo)
#define LsuRxmtLsu(pNbr) HrLsuRxmtLsu (pNbr)
#define LsuAddToRxmtLst(pNbr, pLsaInfo) HrLsuAddToRxmtLst (pNbr, pLsaInfo)
#define LsuFindAndProcessAckFlag(pNbr, pLsaInfo, pLsa, lsHeader) \
        HrLsuFindAndProcessAckFlag (pNbr, pLsaInfo, pLsa, lsHeader)
#define LsuDeleteFromAllRxmtLst(pLsaInfo) HrLsuDeleteFromAllRxmtLst (pLsaInfo)
#define LsuDeleteNodeFromAllRxmtLst(pLsaInfo) HrLsuDeleteNodeFromAllRxmtLst (pLsaInfo)
#define LsuCheckInRxmtLst(pNbr, pLsaInfo) HrLsuCheckInRxmtLst (pNbr, pLsaInfo)
#define LsuCheckAndDelLsaFromRxmtLst(pNbr, pLsaInfo) HrLsuCheckAndDelLsaFromRxmtLst (pNbr, pLsaInfo)
#define LsuInitialiseLsaRxmtInfo(pLsaInfo) HrLsuInitialiseLsaRxmtInfo(pLsaInfo)
#else
#define LsuClearRxmtLst(pNbr)   NrLsuClearRxmtLst (pNbr) 
#define LsuDeleteFromRxmtLst(pNbr, pLsaInfo)  NrLsuDeleteFromRxmtLst (pNbr, pLsaInfo) 
#define LsuRxmtLsu(pNbr) NrLsuRxmtLsu (pNbr)
#define LsuAddToRxmtLst(pNbr, pLsaInfo) NrLsuAddToRxmtLst (pNbr, pLsaInfo)
#define LsuFindAndProcessAckFlag(pNbr, pLsaInfo, pLsa, lsHeader) \
        NrLsuFindAndProcessAckFlag (pNbr, pLsaInfo, pLsa, lsHeader)
#define LsuDeleteFromAllRxmtLst(pLsaInfo) NrLsuDeleteFromAllRxmtLst (pLsaInfo)
#define LsuDeleteNodeFromAllRxmtLst(pLsaInfo) NrLsuDeleteNodeFromAllRxmtLst (pLsaInfo)
#define LsuCheckAndDelLsaFromRxmtLst(pNbr, pLsaInfo) NrLsuCheckAndDelLsaFromRxmtLst (pNbr, pLsaInfo)
#define LsuCheckInRxmtLst(pNbr, pLsaInfo) NrLsuCheckInRxmtLst (pNbr, pLsaInfo)
#define LsuInitialiseLsaRxmtInfo(pLsaInfo) NrLsuInitialiseLsaRxmtInfo(pLsaInfo)
#endif

#define IS_TRANSIT_NETWORK(p_iface) \
        ((p_iface->u1NetworkType == IF_BROADCAST) || (p_iface->u1NetworkType == IF_NBMA))

/* Macros to set/reset the bit in LsaRefesh flag in LsaInfo structure */
/* p_lsainfo is the LSA pointer and bit_position is the exact bit to be set */
#define LSA_REFRESH_BIT_SET(p_lsainfo,bit_position) \
        p_lsainfo->u1LsaRefresh = (p_lsainfo->u1LsaRefresh | (1 << (bit_position-1)))

#define LSA_REFRESH_BIT_RESET(p_lsainfo,bit_position) \
        p_lsainfo->u1LsaRefresh = (UINT1)(p_lsainfo->u1LsaRefresh & (~(1 << (bit_position-1))))

#define IS_LSA_REFRESH_BIT_SET(p_lsainfo,bit_position) \
        (p_lsainfo->u1LsaRefresh & (1 << (bit_position-1)))

#define OSPF_CONVERT_BPS_TO_KBPS(f4Val) \
    (UINT4) ((f4Val * 8) / 1000)
#define OSPF_MAX_PRIORITY_LVL   8
#define OSPF_TYPE10_LINK_TYPE   1
#define OSPF_TYPE10_LINK_ID     2
#define OSPF_TYPE10_LOCAL_IP    3
#define OSPF_TYPE10_REMOTE_IP   4
#define OSPF_TYPE10_TE_METRIC   5
#define OSPF_TYPE10_MAX_BW      6
#define OSPF_TYPE10_MAX_RES_BW  7
#define OSPF_TYPE10_UNRES_BW    8
#define OSPF_TYPE10_RES_COLOR   9
#define OSPF_TYPE10_LCL_RMT_ID 11
#define OSPF_TYPE10_LINK_PROTECTION 14
#define OSPF_TYPE10_IF_DESC    15
#define OSPF_TYPE10_SRLG       16
#define OSPF_TYPE10_LINK_P2P    1
#define OSPF_TYPE10_LINK_MULTI  2
#define OSPF_TYPE10_PSC1        1
#define OSPF_TYPE10_PSC2        2
#define OSPF_TYPE10_PSC3        3
#define OSPF_TYPE10_PSC4        4
#define OSPF_TYPE10_L2SC       51
#define OSPF_TYPE10_TDM       100
#define OSPF_TYPE10_LSC       150
#define OSPF_TYPE10_FSC       250
#define DEFAULT_HOST_METRIC     0

#endif
