/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osdebug.h,v 1.19 2017/09/21 13:48:45 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *
 *******************************************************************/


#ifndef _OSDEBUG_H
#define _OSDEBUG_H

#define OSPF_SENT       1
#define  OSPF_RCVD      2                    

#ifdef TRACE_WANTED
#define  OSPF_TRC_FLAG(CxtId)  UtilOspfGetTraceFlag (CxtId)
#define  OSPF_EXT_TRC_FLAG(CxtId)  UtilOspfGetExtTraceFlag (CxtId)

#define  CONTEXT_INFO(x, CxtId) "Context-%d:" x, CxtId
 
#define  OSPF_NAME      "OSPF"               

#define OSPF_PKT_DUMP(pBuf, Length, fmt) \
            MOD_PKT_DUMP(OSPF_TRC_FLAG, DUMP_TRC, OSPF_NAME, pBuf, Length, fmt)

#define OSPF_TRC(Value, CxtId,  Fmt)\
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId));\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId));\
}
#define OSPF_GBL_TRC(Value, CxtId,  Fmt)\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt);\
}

#define OSPF_TRC1(Value, CxtId,  Fmt, Arg)\
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg);\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg);\
}
#define OSPF_GBL_TRC1(Value, CxtId,  Fmt, Arg)\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg);\
}

#define OSPF_TRC2(Value, CxtId,  Fmt, Arg1, Arg2)\
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2);\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                   Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2);\
}
#define OSPF_GBL_TRC2(Value, CxtId,  Fmt, Arg1, Arg2)\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg1, Arg2);\
}

#define OSPF_TRC3(Value, CxtId,  Fmt, Arg1, Arg2, Arg3)\
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3);\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, \
        CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3);\
}

#define OSPF_GBL_TRC3(Value, CxtId,  Fmt, Arg1, Arg2, Arg3)\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg1, Arg2, Arg3);\
}

#define OSPF_TRC4(Value, CxtId,  Fmt, Arg1, Arg2, Arg3, Arg4)\
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4);\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, \
        CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4);\
}

#define OSPF_GBL_TRC4(Value, CxtId,  Fmt, Arg1, Arg2, Arg3, Arg4)\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg1, Arg2, Arg3, Arg4);\
}

#define OSPF_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4, Arg5);\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, \
                   CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4, Arg5);\
}

#define OSPF_GBL_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
}

#define OSPF_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
if ((Value) & OSPF_CRITICAL_TRC )\
{\
    UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, OSPF_TRC_FLAG (CxtId), Value, OSPF_NAME, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
}\
else\
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                    Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId),\
                    Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
}
#define OSPF_GBL_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
{\
    UtlTrcLog(OSPF_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, \
        Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
}
#define OSPF_EXT_TRC(Value, CxtId,  Fmt)\
{\
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId));\
}
#define OSPF_EXT_GBL_TRC(Value, CxtId,  Fmt)\
   { \
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt);\
}

#define OSPF_EXT_TRC1(Value, CxtId,  Fmt, Arg)\
{\
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg);\
}
#define OSPF_EXT_GBL_TRC1(Value, CxtId,  Fmt, Arg)\
{ \
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg);\
}

#define OSPF_EXT_TRC2(Value, CxtId,  Fmt, Arg1, Arg2)\
{\
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2);\
}
#define OSPF_EXT_GBL_TRC2(Value, CxtId,  Fmt, Arg1, Arg2)\
{ \
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, Fmt, Arg1, Arg2);\
}

#define OSPF_EXT_TRC3(Value, CxtId,  Fmt, Arg1, Arg2, Arg3)\
{\
   UtlTrcLog(OSPF_EXT_TRC_FLAG (CxtId), \
                      Value, OSPF_NAME, CONTEXT_INFO (Fmt, CxtId), Arg1, Arg2, Arg3);\
}

#define OSPF_NBR_TRC(Value,pNbr,CxtId,Fmt)\
{\
 if(pNbr != NULL)\
 {\
  OSPF_TRC(Value, CxtId,  Fmt)\
 }\
}

#define OSPF_NBR_TRC1(Value, pNbr, CxtId, Fmt, Arg)\
{\
 if(pNbr != NULL)\
 {\
  OSPF_TRC1(Value, CxtId, Fmt, Arg)\
 }\
}

#define OSPF_NBR_TRC2(Value, pNbr, CxtId, Fmt, Arg1, Arg2)\
{\
 if(pNbr != NULL)\
 {\
  OSPF_TRC2(Value, CxtId, Fmt, Arg1, Arg2)\
 }\
}

#define OSPF_NBR_TRC3(Value, pNbr, CxtId, Fmt, Arg1, Arg2, Arg3)\
{\
        if(pNbr != NULL)\
        {\
                OSPF_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)\
        }\
}

#define OSPF_NBR_TRC4(Value, pNbr, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)\
{\
        if(pNbr != NULL)\
        {\
                OSPF_TRC4(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)\
        }\
}

#define OSPF_NBR_TRC5(Value, pNbr, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)\
{\
        if(pNbr != NULL)\
        {\
                OSPF_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)\
        }\
}

#else /* TRACE_WANTED */

#define  OSPF_TRC_FLAG
#define  OSPF_EXT_TRC_FLAG
#define  OSPF_EXT_TRC1(Value, CxtId, Fmt, Arg)                                  
#define  OSPF_EXT_TRC2(Value, CxtId, Fmt, Arg1, Arg2)                           
#define  OSPF_NAME
#define  OSPF_PKT_DUMP(pBuf, Length, fmt)                            
#define  OSPF_TRC(Value, CxtId, Fmt)                                        
#define  OSPF_TRC1(Value, CxtId, Fmt, Arg)                                  
#define  OSPF_TRC2(Value, CxtId, Fmt, Arg1, Arg2)                           
#define  OSPF_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)                     
#define  OSPF_TRC4(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)               
#define  OSPF_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)         
#define  OSPF_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)   
#define  OSPF_EXT_TRC(Value, CxtId,  Fmt)
#define  OSPF_EXT_TRC1(Value, CxtId, Fmt, Arg)
#define  OSPF_EXT_TRC2(Value, CxtId, Fmt, Arg1, Arg2)
#define  OSPF_EXT_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)

#define OSPF_EXT_GBL_TRC1(Value, CxtId,  Fmt, Arg)
#define OSPF_TRC(Value, CxtId,  Fmt)
#define OSPF_GBL_TRC(Value, CxtId,  Fmt)
#define OSPF_GBL_TRC1(Value, CxtId,  Fmt, Arg)
#define OSPF_GBL_TRC2(Value, CxtId,  Fmt, Arg1, Arg2)
#define OSPF_GBL_TRC3(Value, CxtId,  Fmt, Arg1, Arg2, Arg3)
#define OSPF_GBL_TRC4(Value, CxtId,  Fmt, Arg1, Arg2, Arg3, Arg4)
#define OSPF_GBL_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    
#define OSPF_GBL_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)    
#define OSPF_EXT_GBL_TRC(Value, CxtId,  Fmt)
#define OSPF_NBR_TRC(Value,pNbr,CxtId,  Fmt)
#define OSPF_NBR_TRC1(Value, pNbr, CxtId, Fmt, Arg)
#define OSPF_NBR_TRC2(Value, pNbr, CxtId, Fmt, Arg1, Arg2)
#define OSPF_NBR_TRC3(Value, pNbr, CxtId, Fmt, Arg1, Arg2, Arg3)
#define OSPF_NBR_TRC4(Value, pNbr, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)
#define OSPF_NBR_TRC5(Value, pNbr, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#endif /* TRACE_WANTED */

/* OSPF Specific Trace Categories */
#define  OSPF_HP_TRC   0x00010000 
#define  OSPF_DDP_TRC  0x00020000 
#define  OSPF_LRQ_TRC  0x00040000 
#define  OSPF_LSU_TRC  0x00080000 
#define  OSPF_LAK_TRC  0x00100000 
#define  OSPF_ISM_TRC  0x00200000 
#define  OSPF_NSM_TRC  0x00400000 
#define  OSPF_RT_TRC   0x00800000 

#define  OSPF_NSSA_TRC   0x04000000
#define  OSPF_RAG_TRC    0x08000000

/* OSPF MODULE Specific Trace Categories */
#define  OSPF_CONFIGURATION_TRC  0x10000000 
#define  OSPF_ADJACENCY_TRC      0x20000000 
#define  OSPF_LSDB_TRC           0x40000000 
#define  OSPF_PPP_TRC            0x80000000
#define  OSPF_RTMODULE_TRC       0x01000000 
#define  OSPF_INTERFACE_TRC      0x02000000 

/* OSPF FUNCTION Specific Trace Categories */
#define  OSPF_FN_ENTRY  0x00001000 
#define  OSPF_FN_EXIT   0x00002000 

/* OSPF MEMORY RESOURCE Specific Trace Categories */
#define  OSPF_MEM_SUCC  0x00004000 
#define  OSPF_MEM_FAIL  0x00008000 

/*-- New Packet Dumping Specific definations --*/

/* OSPF Packet Dumping Trace Categories */
#define  OSPF_DUMP_HGH_TRC  0x00000100  /* Packet HighLevel dump */
#define  OSPF_DUMP_LOW_TRC  0x00000200  /* Packet LowLevel dump */
#define  OSPF_DUMP_HEX_TRC  0x00000400  /* Packet Hex dump */
#define  OSPF_CRITICAL_TRC  0x00000800  /* Critical Trace */

/* OSPF Extended Trace Categories */
#define  OSPF_RESTART_TRC   0x00000100  /* Restarting module */
#define  OSPF_HELPER_TRC    0x00000200  /* Helper module */
#define  OSPF_REDUNDANCY_TRC 0x00000400  /* Redundancy module */

/*OSPF MIN and MAX Trace levels*/
#define OSPF_DBG_MIN_TRC  OSPF_DUMP_HGH_TRC
#define OSPF_DBG_MAX_TRC  OSPF_PPP_TRC
#define OSPF_MAX_EXT_TRC OSPF_REDUNDANCY_TRC
#if defined (DEBUG_WANTED) && (TRACE_WANTED)

/* Incoming Packet Dumping */
#define OSPF_INC_PKT_DUMP(CxtId, pBuf)                                    \
        DbgOspfDumpPktData (OSPF_TRC_FLAG (CxtId),                           \
            (OSPF_DUMP_HGH_TRC|OSPF_DUMP_LOW_TRC|OSPF_DUMP_HEX_TRC),  \
            OSPF_DIR_IN, pBuf)

/* Outgoing Packet Dumping */
#define OSPF_OUT_PKT_DUMP(CxtId, pBuf)                                    \
        DbgOspfDumpPktData (OSPF_TRC_FLAG (CxtId),                            \
            (OSPF_DUMP_HGH_TRC|OSPF_DUMP_LOW_TRC|OSPF_DUMP_HEX_TRC),  \
            OSPF_DIR_OUT, pBuf)

#else /* Packet Dump is not included */
#define OSPF_INC_PKT_DUMP(CxtId, pBuf)
#define OSPF_OUT_PKT_DUMP(CxtId, pBuf)
#endif /* DEBUG_WANTED */

#define OSPF_HIGH_DUMP      0x01    /* High level dumping (One liner output)*/
#define OSPF_LOW_DUMP       0x02    /* Low level dumping (Detailed output) */
#define OSPF_HEX_DUMP       0x03    /* Hex level dumping (Hex bytes) */

#define OSPF_DIR_IN         0x01    /* Incoming pakcet */
#define OSPF_DIR_OUT        0x02    /* Outgoing pakcet */

/* the maximum size for packet decodes is 40K bytes of string */
#define OSPF_MAX_LOG_STR_LEN     256
#define OSPF_MAX_DUMP_BUF   (2 * 1024U)
#define OSPF_BIT(x, n)      (((x) >> n) & 0x01)
#define OSPFBUF(BufPtr)     BufPtr+STRLEN(BufPtr)

#define OSPF_HDR_LEN                    24
#define OSPF_HELLO_LEN                  20
#define OSPF_DDP_LEN                    8

#define OSPF_LSA_HDR_LEN                20      /* LSA Header length */
#define OSPF_TOS_METRIC_LEN             4

#define OSPF_ROUTER_LSA_HDR_LEN         4
#define OSPF_NETWORK_LSA_HDR_LEN        4
#define OSPF_SUMMARY_LSA_HDR_LEN        4
#define OSPF_AS_EXTERN_LSA_HDR_LEN      4

#define OSPF_LS_REQ_LEN                 12
#define OSPF_LS_UPDATE_LEN              4

#define OSPF_TOS_ROUTE_LEN              12
#define OSPF_ROUTER_LSA_TUPLE_HDR_LEN   12

#define OSPF_LSA_HDR    TRUE
#define OSPF_LSA_ALL    FALSE

#define OSPF_DDP_LSA_OFFSET     32      /* Data Base Desr */
#define OSPF_LSU_LSA_OFFSET     28      /* Link State Update */
#define OSPF_LSA_LSA_OFFSET     24      /* Link State Ack */

/* Link State Advt header offsets */
#define OSPF_LSA_HDR_AGE_OFFSET         0   /* LS age */
#define OSPF_LSA_HDR_OPT_OFFSET         2   /* Options */
#define OSPF_LSA_HDR_LSTYPE_OFFSET      3   /* LS type */
#define OSPF_LSA_HDR_LNK_ID_OFFSET      4   /* Link state ID */
#define OSPF_LSA_HDR_ADV_RTR_OFFSET     8   /* Advertising Router */
#define OSPF_LSA_HDR_SEQ_NO_OFFSET      12  /* LS sequence number */
#define OSPF_LSA_HDR_CHECKSUM_OFFSET    16  /* LS checksum */
#define OSPF_LSA_HDR_LENGTH_OFFSET      18  /* LSA length */
#define OSPF_LSA_HDR_GRACEPERIOD_OFFSET      24  /* LSA length */
#define OSPF_LSA_HDR_RESTARTREASON_OFFSET 4 /* Restart Reason*/
/* Router LSA header offsets */
#define OSPF_RTR_LSA_FLAG_OFFSET        0   /* Flag */
#define OSPF_RTR_LSA_RESERV_OFFSET      1   /* Reserved */
#define OSPF_RTR_LSA_NO_LINK_OFFSET     2   /* No of Links */

/* Router LSA data offsets */
#define OSPF_RTR_LSA_LINK_ID_OFFSET         0   /* Link ID */
#define OSPF_RTR_LSA_LINK_DATA_OFFSET       4   /* Link Data */
#define OSPF_RTR_LSA_LINK_TYPE_OFFSET       8   /* Link Tyep */
#define OSPF_RTR_LSA_NUM_OF_TOS_OFFSET      9   /* # TOS */
#define OSPF_RTR_LSA_ZERO_METRIC_OFFSET     10  /* Metric 0 */

#define OSPF_RTR_LSA_TOS_OFFSET             0   /* TOS */
#define OSPF_RTR_LSA_TOS_METRIC_OFFSET      2   /* TOS metric */

/* Router LSA link types */
#define OSPF_ROUTER_LINK_TYPE_UNKNOWN           0
#define OSPF_ROUTER_LINK_TYPE_PNT_TO_PNT        1
#define OSPF_ROUTER_LINK_TYPE_TRANSIT           2
#define OSPF_ROUTER_LINK_TYPE_STUB              3
#define OSPF_ROUTER_LINK_TYPE_VIRTUAL           4

/* AS-external - LSA */
#define OSPF_AS_EXT_LSA_TOS_MASK    0x7F    /* TOS Mask in AS external LSA */

/* OSPF Architectural Constants */
#define OSPF_MAX_AGE 3600

#define OSPF_SECS_IN_HOUR 3600      /* No of seconds in a hour */
#define OSPF_SECS_IN_MIN  60        /* No of seconds in a minute */

#endif /* _OSDEBUG_H */
