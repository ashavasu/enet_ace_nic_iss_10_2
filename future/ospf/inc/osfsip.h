/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osfsip.h,v 1.7 2007/02/01 15:00:38 iss Exp $
 *
 * Description:This file contains definitions for constants
 *             and types related to interface with IP.
 *
 *******************************************************************/
#ifndef _OSFSIP_H
#define _OSFSIP_H

#define  OSPF_OPCODE_DATA        0 
#define  OSPF_OPCODE_STATE_CHNG  1 
 
PUBLIC INT1
OspfCliGetInterfaceAddress PROTO ((UINT4 *pu4IfIpAddr, INT4 *pi4AddrlessIf,
			                            UINT1 *au1IfName));
#endif /* _OSFSIP_H */
