/********************************************************************
 $Id: fsmioswr.h,v 1.12 2014/03/01 11:40:41 siva Exp $
 *******************************************************************/

#ifndef _FSMIOSWR_H
#define _FSMIOSWR_H


VOID RegisterFSMIOS(VOID);
VOID UnRegisterFSMIOS(VOID);
INT4 FsMIOspfGlobalTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVrfSpfIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGlobalTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVrfSpfIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGlobalTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVrfSpfIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGlobalTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIOspfVrfSpfIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexFsMIOspfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfOverFlowStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfPktsRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfPktsTxedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfPktsDisdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRFC1583CompatibilityGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfMinLsaIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfABRTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNssaAsbrDefRtTransGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDefaultPassiveInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSpfHoldtimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSpfDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRTStaggeringIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouterIdPermanenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBfdStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBfdAllIfStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartSupportGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartStrictLsaCheckingGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartExitReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHelperSupportGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHelperGraceTimeLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartAckStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGraceLsaRetransmitCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRFC1583CompatibilitySet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfMinLsaIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfABRTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNssaAsbrDefRtTransSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDefaultPassiveInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSpfHoldtimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSpfDelaySet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRTStaggeringIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouterIdPermanenceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBfdStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBfdAllIfStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartSupportSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartStrictLsaCheckingSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHelperSupportSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHelperGraceTimeLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartAckStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGraceLsaRetransmitCountSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartReasonSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRFC1583CompatibilityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfMinLsaIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfABRTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNssaAsbrDefRtTransTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDefaultPassiveInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSpfHoldtimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSpfDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRTStaggeringIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouterIdPermanenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBfdStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBfdAllIfStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartSupportTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartStrictLsaCheckingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHelperSupportTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHelperGraceTimeLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartAckStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGraceLsaRetransmitCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRestartReasonTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIOspfRTStaggeringStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHotStandbyAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHotStandbyStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDynamicBulkUpdStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfStanbyHelloSyncCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfStanbyLsaSyncCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRTStaggeringStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRTStaggeringStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRTStaggeringStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIOspfGlobalExtTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGlobalExtTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGlobalExtTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGlobalExtTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsMIOspfAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfAreaIfCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNetCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaRtrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorStabilityIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorEventsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaDfInfOriginateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorStabilityIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaDfInfOriginateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaNSSATranslatorStabilityIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaDfInfOriginateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIOspfHostTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfHostRouteIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHostRouteIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHostRouteIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfHostTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIOspfIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfIfOperStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfPassiveGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfNbrCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAdjCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfHelloRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfHelloTxedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfHelloDisdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfDdpRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfDdpTxedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfDdpDisdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLrqRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLrqTxedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLrqDisdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLsuRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLsuTxedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLsuDisdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLakRcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLakTxedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfLakDisdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfBfdStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfBfdStateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfBfdStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfPassiveSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfPassiveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIOspfIfMD5AuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfIfMD5AuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfMD5AuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsMIOspfVirtIfMD5AuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfVirtIfMD5AuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfMD5AuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsMIOspfNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfNbrDBSummaryQLenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNbrLSReqQLenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNbrRestartHelperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNbrRestartHelperAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNbrRestartHelperExitReasonGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfNbrBfdStateGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfRoutingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfRouteTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouteAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouteCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouteType2CostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRouteInterfaceIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfSecIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfSecIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSecIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSecIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfSecIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIOspfAreaAggregateTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfAreaAggregateExternalTagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaAggregateExternalTagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaAggregateExternalTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaAggregateTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIOspfAsExternalAggregationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfAsExternalAggregationEffectGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationTranslationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationEffectSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationTranslationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationEffectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationTranslationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAsExternalAggregationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsMIOspfOpaqueTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfOpaqueOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType11LsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType11LsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaIDValidGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfOpaqueOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaIDValidSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfOpaqueOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAreaIDValidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfOpaqueTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIOspfOpaqueInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfOpaqueType9LsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfOpaqueType9LsaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfType9LsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfType9LsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType9LsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType9LsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType9LsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfType11LsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfType11LsdbSequenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType11LsdbAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType11LsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfType11LsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfAppInfoDbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfAppInfoDbOpaqueTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbLsaTypesSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbType9GenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbType9RcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbType10GenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbType10RcvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbType11GenGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfAppInfoDbType11RcvdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfRRDRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfRRDStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDSrcProtoMaskEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDSrcProtoMaskDisableGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMapEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDSrcProtoMaskEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDSrcProtoMaskDisableSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMapEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDSrcProtoMaskEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDSrcProtoMaskDisableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMapEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsMIOspfRRDRouteConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfRRDRouteMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTagTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTagTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTagTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDRouteConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIOspfVirtNbrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfVirtNbrRestartHelperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtNbrRestartHelperAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtNbrRestartHelperExitReasonGet(tSnmpIndex *, tRetVal *);

INT4 GetNextIndexFsMIOspfRRDMetricTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfRRDMetricValueGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDMetricValueSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDMetricValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfRRDMetricTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIOspfDistInOutRouteMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfDistInOutRouteMapValueGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDistInOutRouteMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDistInOutRouteMapValueSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDistInOutRouteMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDistInOutRouteMapValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDistInOutRouteMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfDistInOutRouteMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIOspfPreferenceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfPreferenceValueGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfPreferenceValueSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfPreferenceValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfPreferenceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIOspfIfAuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfIfAuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIOspfVirtIfAuthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfVirtIfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStartAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStartGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStopGenerateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStopAcceptGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStartAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStartGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStopGenerateSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStopAcceptSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStartAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStartGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStopGenerateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStopAcceptTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthKeyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfVirtIfAuthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _FSMIOSWR_H */
