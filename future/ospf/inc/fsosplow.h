/*$Id: fsosplow.h,v 1.18 2014/03/01 11:40:41 siva Exp $*/
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfOverFlowState ARG_LIST((INT4 *));

INT1
nmhGetFutOspfPktsRcvd ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfPktsTxed ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfPktsDisd ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfRFC1583Compatibility ARG_LIST((INT4 *));

INT1
nmhGetFutOspfMaxAreas ARG_LIST((INT4 *));

INT1
nmhGetFutOspfMaxLSAperArea ARG_LIST((INT4 *));


INT1
nmhGetFutOspfMaxSelfOrgLSAs ARG_LIST((INT4 *));

INT1
nmhGetFutOspfMaxRoutes ARG_LIST((INT4 *));

INT1
nmhGetFutOspfMaxLsaSize ARG_LIST((INT4 *));

INT1
nmhGetFutOspfTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFutOspfMinLsaInterval ARG_LIST((INT4 *));

INT1
nmhGetFutOspfABRType ARG_LIST((INT4 *));

INT1
nmhGetFutOspfNssaAsbrDefRtTrans ARG_LIST((INT4 *));

INT1
nmhGetFutOspfDefaultPassiveInterface ARG_LIST((INT4 *));

INT1
nmhGetFutOspfSpfHoldtime ARG_LIST((INT4 *));

INT1
nmhGetFutOspfSpfDelay ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRTStaggeringInterval ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfRTStaggeringStatus ARG_LIST((INT4 *));


INT1
nmhGetFutOspfRestartSupport ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRestartInterval ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRestartStrictLsaChecking ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRestartStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRestartAge ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfRestartExitReason ARG_LIST((INT4 *));

INT1
nmhGetFutOspfHelperSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfHotStandbyAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfHotStandbyState ARG_LIST((INT4 *));

INT1
nmhGetFutOspfDynamicBulkUpdStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfStanbyHelloSyncCount ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfStanbyLsaSyncCount ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfHelperGraceTimeLimit ARG_LIST((INT4 *));

INT1
nmhGetFutOspfExtTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRestartAckState ARG_LIST((INT4 *));

INT1
nmhGetFutOspfGraceLsaRetransmitCount ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRestartReason ARG_LIST((INT4 *));

INT1
nmhGetFutospfRouterIdPermanence ARG_LIST((INT4 *));

INT1 
nmhGetFutOspfBfdStatus ARG_LIST ((INT4 *));

INT1
nmhGetFutOspfBfdAllIfState ARG_LIST ((INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfRFC1583Compatibility ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMaxAreas ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMaxLSAperArea ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMaxExtLSAs ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMaxSelfOrgLSAs ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMaxRoutes ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMaxLsaSize ARG_LIST((INT4 ));

INT1
nmhSetFutOspfTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFutOspfMinLsaInterval ARG_LIST((INT4 ));

INT1
nmhSetFutOspfABRType ARG_LIST((INT4 ));

INT1
nmhSetFutOspfNssaAsbrDefRtTrans ARG_LIST((INT4 ));

INT1
nmhSetFutOspfDefaultPassiveInterface ARG_LIST((INT4 ));

INT1
nmhSetFutOspfSpfHoldtime ARG_LIST((INT4 ));

INT1
nmhSetFutOspfSpfDelay ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRTStaggeringInterval ARG_LIST((UINT4 ));

INT1
nmhSetFutOspfRTStaggeringStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRestartSupport ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRestartInterval ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRestartStrictLsaChecking ARG_LIST((INT4 ));

INT1
nmhSetFutOspfHelperSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfExtTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFutOspfHelperGraceTimeLimit ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRestartAckState ARG_LIST((INT4 ));

INT1
nmhSetFutOspfGraceLsaRetransmitCount ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRestartReason ARG_LIST((INT4 ));

INT1
nmhSetFutospfRouterIdPermanence ARG_LIST((INT4 ));

INT1
nmhSetFutOspfBfdStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfBfdAllIfState ARG_LIST((INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfRFC1583Compatibility ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMaxAreas ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMaxLSAperArea ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMaxExtLSAs ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMaxSelfOrgLSAs ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMaxRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMaxLsaSize ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfMinLsaInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfABRType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfNssaAsbrDefRtTrans ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfDefaultPassiveInterface ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfSpfHoldtime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfSpfDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRTStaggeringInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FutOspfRTStaggeringStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRestartSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRestartInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfHelperSupport ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfExtTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfHelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRestartAckState ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfGraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRestartReason ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutospfRouterIdPermanence ARG_LIST((UINT4 *  ,INT4 ));

INT1 
nmhTestv2FutOspfBfdStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1 
nmhTestv2FutOspfBfdAllIfState ARG_LIST((UINT4 *  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfRFC1583Compatibility ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMaxAreas ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMaxLSAperArea ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMaxExtLSAs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMaxSelfOrgLSAs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMaxRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMaxLsaSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfMinLsaInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfABRType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfNssaAsbrDefRtTrans ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfDefaultPassiveInterface ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfSpfHoldtime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfSpfDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRTStaggeringInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRTStaggeringStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRestartSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRestartInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRestartStrictLsaChecking ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfHelperSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhDepv2FutOspfExtTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfHelperGraceTimeLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRestartAckState ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfGraceLsaRetransmitCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRestartReason ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutospfRouterIdPermanence ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfBfdStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfBfdAllIfState ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for FutOspfAreaTable. */
INT1
nmhValidateIndexInstanceFutOspfAreaTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfAreaTable  */

INT1
nmhGetFirstIndexFutOspfAreaTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfAreaTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfAreaIfCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFutOspfAreaNetCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFutOspfAreaRtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFutOspfAreaNSSATranslatorRole ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFutOspfAreaNSSATranslatorState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFutOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFutOspfAreaNSSATranslatorEvents ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFutOspfAreaDfInfOriginate ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfAreaNSSATranslatorRole ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFutOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFutOspfAreaDfInfOriginate ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfAreaNSSATranslatorRole ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfAreaDfInfOriginate ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfAreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfHostTable. */
INT1
nmhValidateIndexInstanceFutOspfHostTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfHostTable  */

INT1
nmhGetFirstIndexFutOspfHostTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfHostTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfHostRouteIfIndex ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfHostRouteIfIndex ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfHostRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfHostTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfIfTable. */
INT1
nmhValidateIndexInstanceFutOspfIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfIfTable  */

INT1
nmhGetFirstIndexFutOspfIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfIfOperState ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfIfPassive ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfIfBfdState ARG_LIST((UINT4  , INT4 ,INT4 *));
INT1
nmhGetFutOspfIfNbrCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfAdjCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfHelloRcvd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfHelloTxed ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfHelloDisd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfDdpRcvd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfDdpTxed ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfDdpDisd ARG_LIST((UINT4  , INT4 ,UINT4 *));



INT1
nmhGetFutOspfIfLakRcvd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLakTxed ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLakDisd ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfIfPassive ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfIfBfdState ARG_LIST((UINT4  , INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfIfPassive ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfIfBfdState ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfIfMD5AuthTable. */
INT1
nmhValidateIndexInstanceFutOspfIfMD5AuthTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfIfMD5AuthTable  */

INT1
nmhGetFirstIndexFutOspfIfMD5AuthTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfIfMD5AuthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfIfMD5AuthKey ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfIfMD5AuthKeyStartAccept ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfIfMD5AuthKeyStartGenerate ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfIfMD5AuthKeyStopGenerate ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfIfMD5AuthKeyStopAccept ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfIfMD5AuthKeyStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfIfMD5AuthKey ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfIfMD5AuthKeyStartAccept ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfIfMD5AuthKeyStartGenerate ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfIfMD5AuthKeyStopGenerate ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfIfMD5AuthKeyStopAccept ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfIfMD5AuthKeyStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfIfMD5AuthKey ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfIfMD5AuthKeyStartAccept ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfIfMD5AuthKeyStartGenerate ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfIfMD5AuthKeyStopGenerate ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfIfMD5AuthKeyStopAccept ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfIfMD5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfVirtIfMD5AuthTable. */
INT1
nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfVirtIfMD5AuthTable  */

INT1
nmhGetFirstIndexFutOspfVirtIfMD5AuthTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfVirtIfMD5AuthTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfVirtIfMD5AuthKey ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfVirtIfMD5AuthKey ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFutOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfVirtIfMD5AuthKey ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfVirtIfMD5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfNbrTable. */
INT1
nmhValidateIndexInstanceFutOspfNbrTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfNbrTable  */

INT1
nmhGetFirstIndexFutOspfNbrTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfNbrTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfNbrDBSummaryQLen ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfNbrLSReqQLen ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfNbrRestartHelperStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfNbrRestartHelperAge ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfNbrRestartHelperExitReason ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfNbrBfdState ARG_LIST ((UINT4 , INT4 ,INT4 *));
/* Proto Validate Index Instance for FutOspfRoutingTable. */
INT1
nmhValidateIndexInstanceFutOspfRoutingTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfRoutingTable  */

INT1
nmhGetFirstIndexFutOspfRoutingTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfRoutingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfRouteAreaId ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutOspfRouteCost ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfRouteType2Cost ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfRouteInterfaceIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfSecIfTable. */
INT1
nmhValidateIndexInstanceFutOspfSecIfTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfSecIfTable  */

INT1
nmhGetFirstIndexFutOspfSecIfTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfSecIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfSecIfStatus ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfSecIfStatus ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfSecIfStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfSecIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfAreaAggregateTable. */
INT1
nmhValidateIndexInstanceFutOspfAreaAggregateTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfAreaAggregateTable  */

INT1
nmhGetFirstIndexFutOspfAreaAggregateTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfAreaAggregateTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfAreaAggregateExternalTag ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfAreaAggregateExternalTag ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfAreaAggregateExternalTag ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfAreaAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfAsExternalAggregationTable. */
INT1
nmhValidateIndexInstanceFutOspfAsExternalAggregationTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfAsExternalAggregationTable  */

INT1
nmhGetFirstIndexFutOspfAsExternalAggregationTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfAsExternalAggregationTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfAsExternalAggregationEffect ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfAsExternalAggregationTranslation ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfAsExternalAggregationStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfAsExternalAggregationEffect ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfAsExternalAggregationTranslation ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfAsExternalAggregationStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfAsExternalAggregationEffect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfAsExternalAggregationTranslation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfAsExternalAggregationStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfAsExternalAggregationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfOpaqueOption ARG_LIST((INT4 *));

INT1
nmhGetFutOspfType11LsaCount ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfType11LsaCksumSum ARG_LIST((INT4 *));

INT1
nmhGetFutOspfAreaIDValid ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfOpaqueOption ARG_LIST((INT4 ));

INT1
nmhSetFutOspfAreaIDValid ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfOpaqueOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfAreaIDValid ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfOpaqueOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfAreaIDValid ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfOpaqueInterfaceTable. */
INT1
nmhValidateIndexInstanceFutOspfOpaqueInterfaceTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfOpaqueInterfaceTable  */

INT1
nmhGetFirstIndexFutOspfOpaqueInterfaceTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfOpaqueInterfaceTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfOpaqueType9LsaCount ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfOpaqueType9LsaCksumSum ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfType9LsdbTable. */
INT1
nmhValidateIndexInstanceFutOspfType9LsdbTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfType9LsdbTable  */

INT1
nmhGetFirstIndexFutOspfType9LsdbTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfType9LsdbTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfType9LsdbSequence ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfType9LsdbAge ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfType9LsdbChecksum ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfType9LsdbAdvertisement ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FutOspfType11LsdbTable. */
INT1
nmhValidateIndexInstanceFutOspfType11LsdbTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfType11LsdbTable  */

INT1
nmhGetFirstIndexFutOspfType11LsdbTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfType11LsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfType11LsdbSequence ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfType11LsdbAge ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfType11LsdbChecksum ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfType11LsdbAdvertisement ARG_LIST((INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FutOspfAppInfoDbTable. */
INT1
nmhValidateIndexInstanceFutOspfAppInfoDbTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfAppInfoDbTable  */

INT1
nmhGetFirstIndexFutOspfAppInfoDbTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfAppInfoDbTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfAppInfoDbOpaqueType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfAppInfoDbLsaTypesSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfAppInfoDbType9Gen ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfAppInfoDbType9Rcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfAppInfoDbType10Gen ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfAppInfoDbType10Rcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfAppInfoDbType11Gen ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfAppInfoDbType11Rcvd ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfRRDStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRRDSrcProtoMaskEnable ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRRDSrcProtoMaskDisable ARG_LIST((INT4 *));

INT1
nmhGetFutOspfRRDRouteMapEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfRRDStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRRDSrcProtoMaskEnable ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRRDSrcProtoMaskDisable ARG_LIST((INT4 ));

INT1
nmhSetFutOspfRRDRouteMapEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfRRDStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRRDSrcProtoMaskEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRRDSrcProtoMaskDisable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfRRDRouteMapEnable ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfRRDStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRRDSrcProtoMaskEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRRDSrcProtoMaskDisable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfRRDRouteMapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfRRDRouteConfigTable. */
INT1
nmhValidateIndexInstanceFutOspfRRDRouteConfigTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfRRDRouteConfigTable  */

INT1
nmhGetFirstIndexFutOspfRRDRouteConfigTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfRRDRouteConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfRRDRouteMetric ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfRRDRouteMetricType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfRRDRouteTagType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfRRDRouteTag ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutOspfRRDRouteStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfRRDRouteMetric ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfRRDRouteMetricType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfRRDRouteTagType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfRRDRouteTag ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFutOspfRRDRouteStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfRRDRouteMetric ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfRRDRouteMetricType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfRRDRouteTagType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfRRDRouteTag ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FutOspfRRDRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfRRDRouteConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfVirtNbrTable. */
INT1
nmhValidateIndexInstanceFutOspfVirtNbrTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfVirtNbrTable  */

INT1
nmhGetFirstIndexFutOspfVirtNbrTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfVirtNbrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfVirtNbrRestartHelperStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfVirtNbrRestartHelperAge ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutOspfVirtNbrRestartHelperExitReason ARG_LIST((UINT4  , UINT4 ,INT4 *));


/* Proto Validate Index Instance for FutOspfDistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFutOspfDistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfDistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFutOspfDistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfDistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfDistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFutOspfDistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfDistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFutOspfDistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfDistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfDistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfDistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FutOspfIfAuthTable. */
INT1
nmhValidateIndexInstanceFutOspfIfAuthTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfIfAuthTable  */

INT1
nmhGetFirstIndexFutOspfIfAuthTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfIfAuthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfIfAuthKey ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfIfAuthKeyStartAccept ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfIfAuthKeyStartGenerate ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfIfAuthKeyStopGenerate ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfIfAuthKeyStopAccept ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfIfAuthKeyStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfIfAuthKey ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfIfAuthKeyStartAccept ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfIfAuthKeyStartGenerate ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *) );

INT1
nmhSetFutOspfIfAuthKeyStopGenerate ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfIfAuthKeyStopAccept ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfIfAuthKeyStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfIfAuthKey ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfIfAuthKeyStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfIfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FutOspfVirtIfAuthTable. */
INT1
nmhValidateIndexInstanceFutOspfVirtIfAuthTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfVirtIfAuthTable  */

INT1
nmhGetFirstIndexFutOspfVirtIfAuthTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfVirtIfAuthTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfVirtIfAuthKey ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfVirtIfAuthKeyStartAccept ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfVirtIfAuthKeyStartGenerate ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfVirtIfAuthKeyStopGenerate ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfVirtIfAuthKeyStopAccept ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFutOspfVirtIfAuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfVirtIfAuthKey ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfVirtIfAuthKeyStartAccept ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfVirtIfAuthKeyStartGenerate ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfVirtIfAuthKeyStopGenerate ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfVirtIfAuthKeyStopAccept ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFutOspfVirtIfAuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfVirtIfAuthKey ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfVirtIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FutOspfVirtIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FutOspfVirtIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FutOspfVirtIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FutOspfVirtIfAuthKeyStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfVirtIfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhGetFirstIndexFutOspfIfCryptoAuthTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfIfCryptoAuthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));


INT1
nmhValidateIndexInstanceFutOspfIfCryptoAuthTable ARG_LIST((UINT4  , INT4 ));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfIfCryptoAuthType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhGetFutOspfIfCryptoAuthType ARG_LIST((UINT4  , UINT4  ,INT4 * ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfIfCryptoAuthType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfIfCryptoAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FutOspfVirtIfCryptoAuthTable. */
INT1
nmhValidateIndexInstanceFutOspfVirtIfCryptoAuthTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfVirtIfCryptoAuthTable  */

INT1
nmhGetFirstIndexFutOspfVirtIfCryptoAuthTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfVirtIfCryptoAuthTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfVirtIfCryptoAuthType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfVirtIfCryptoAuthType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2OspfVirtIfCryptoAuthType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfVirtIfCryptoAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
