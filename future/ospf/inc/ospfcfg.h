/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ospfcfg.h,v 1.8 2011/03/24 12:56:44 siva Exp $
 *
 * Description:This file contains compile-time switches for OSPF.
 *
 *******************************************************************/
/* Each #define in this file enables a specific capability         */
/* in FutureOspf. To enable the capability, let the #define        */
/* stay; to disable the capability, place the #define statement    */
/* in comments.                                                    */
/* By default, all the switches are disabled here since a makefile */
/* is used to define these. This file is for use in environments   */
/* where use of a makefile for these switches is not possible      */

/**********************************************************************
 * Compile-time switches that need to defined / undefined pertaining to OSPF
 * 
 * TOS_SUPPORT                -     TO ENABLE SUPPORT FOR MULTIPLE TOSs
                                    (TYPES OF SERVICE). IF NOT ENABLED,
                                    ONLY TOS 0 IS SUPPORTED.
 * RAWSOCK_WANTED             -     TO ENABLE RAW SOCKET SUPPORT. IF NOT
                                    ENABLED, A FUNCTIONAL INTERFACE WITH
                                    THE IP STACK IS ASSUMED.
 * DEBUG_WANTED               -     TO ENABLE DEBUGGING ROUTINES 
 * In VxWorks RAWSOCK_WANTED is must.
 **********************************************************************/

#ifndef __OSPFCFG_H
#define __OSPFCFG_H

/* #define TOS_SUPPORT */
#define DEBUG_WANTED            

#endif/* !__OSPFCFG_H */
