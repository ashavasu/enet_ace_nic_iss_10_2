/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdospdb.h,v 1.6 2008/08/20 15:20:08 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDOSPDB_H
#define _STDOSPDB_H

UINT1 OspfAreaTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 OspfStubAreaTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 OspfLsdbTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 OspfAreaRangeTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 OspfHostTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 OspfIfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 OspfIfMetricTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 OspfVirtIfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 OspfNbrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 OspfVirtNbrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 OspfExtLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 OspfAreaAggregateTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 stdosp [] ={1,3,6,1,2,1,14};
tSNMP_OID_TYPE stdospOID = {7, stdosp};


UINT4 OspfRouterId [ ] ={1,3,6,1,2,1,14,1,1};
UINT4 OspfAdminStat [ ] ={1,3,6,1,2,1,14,1,2};
UINT4 OspfVersionNumber [ ] ={1,3,6,1,2,1,14,1,3};
UINT4 OspfAreaBdrRtrStatus [ ] ={1,3,6,1,2,1,14,1,4};
UINT4 OspfASBdrRtrStatus [ ] ={1,3,6,1,2,1,14,1,5};
UINT4 OspfExternLsaCount [ ] ={1,3,6,1,2,1,14,1,6};
UINT4 OspfExternLsaCksumSum [ ] ={1,3,6,1,2,1,14,1,7};
UINT4 OspfTOSSupport [ ] ={1,3,6,1,2,1,14,1,8};
UINT4 OspfOriginateNewLsas [ ] ={1,3,6,1,2,1,14,1,9};
UINT4 OspfRxNewLsas [ ] ={1,3,6,1,2,1,14,1,10};
UINT4 OspfExtLsdbLimit [ ] ={1,3,6,1,2,1,14,1,11};
UINT4 OspfMulticastExtensions [ ] ={1,3,6,1,2,1,14,1,12};
UINT4 OspfExitOverflowInterval [ ] ={1,3,6,1,2,1,14,1,13};
UINT4 OspfDemandExtensions [ ] ={1,3,6,1,2,1,14,1,14};
UINT4 OspfAreaId [ ] ={1,3,6,1,2,1,14,2,1,1};
UINT4 OspfAuthType [ ] ={1,3,6,1,2,1,14,2,1,2};
UINT4 OspfImportAsExtern [ ] ={1,3,6,1,2,1,14,2,1,3};
UINT4 OspfSpfRuns [ ] ={1,3,6,1,2,1,14,2,1,4};
UINT4 OspfAreaBdrRtrCount [ ] ={1,3,6,1,2,1,14,2,1,5};
UINT4 OspfAsBdrRtrCount [ ] ={1,3,6,1,2,1,14,2,1,6};
UINT4 OspfAreaLsaCount [ ] ={1,3,6,1,2,1,14,2,1,7};
UINT4 OspfAreaLsaCksumSum [ ] ={1,3,6,1,2,1,14,2,1,8};
UINT4 OspfAreaSummary [ ] ={1,3,6,1,2,1,14,2,1,9};
UINT4 OspfAreaStatus [ ] ={1,3,6,1,2,1,14,2,1,10};
UINT4 OspfStubAreaId [ ] ={1,3,6,1,2,1,14,3,1,1};
UINT4 OspfStubTOS [ ] ={1,3,6,1,2,1,14,3,1,2};
UINT4 OspfStubMetric [ ] ={1,3,6,1,2,1,14,3,1,3};
UINT4 OspfStubStatus [ ] ={1,3,6,1,2,1,14,3,1,4};
UINT4 OspfStubMetricType [ ] ={1,3,6,1,2,1,14,3,1,5};
UINT4 OspfLsdbAreaId [ ] ={1,3,6,1,2,1,14,4,1,1};
UINT4 OspfLsdbType [ ] ={1,3,6,1,2,1,14,4,1,2};
UINT4 OspfLsdbLsid [ ] ={1,3,6,1,2,1,14,4,1,3};
UINT4 OspfLsdbRouterId [ ] ={1,3,6,1,2,1,14,4,1,4};
UINT4 OspfLsdbSequence [ ] ={1,3,6,1,2,1,14,4,1,5};
UINT4 OspfLsdbAge [ ] ={1,3,6,1,2,1,14,4,1,6};
UINT4 OspfLsdbChecksum [ ] ={1,3,6,1,2,1,14,4,1,7};
UINT4 OspfLsdbAdvertisement [ ] ={1,3,6,1,2,1,14,4,1,8};
UINT4 OspfAreaRangeAreaId [ ] ={1,3,6,1,2,1,14,5,1,1};
UINT4 OspfAreaRangeNet [ ] ={1,3,6,1,2,1,14,5,1,2};
UINT4 OspfAreaRangeMask [ ] ={1,3,6,1,2,1,14,5,1,3};
UINT4 OspfAreaRangeStatus [ ] ={1,3,6,1,2,1,14,5,1,4};
UINT4 OspfAreaRangeEffect [ ] ={1,3,6,1,2,1,14,5,1,5};
UINT4 OspfHostIpAddress [ ] ={1,3,6,1,2,1,14,6,1,1};
UINT4 OspfHostTOS [ ] ={1,3,6,1,2,1,14,6,1,2};
UINT4 OspfHostMetric [ ] ={1,3,6,1,2,1,14,6,1,3};
UINT4 OspfHostStatus [ ] ={1,3,6,1,2,1,14,6,1,4};
UINT4 OspfHostAreaID [ ] ={1,3,6,1,2,1,14,6,1,5};
UINT4 OspfIfIpAddress [ ] ={1,3,6,1,2,1,14,7,1,1};
UINT4 OspfAddressLessIf [ ] ={1,3,6,1,2,1,14,7,1,2};
UINT4 OspfIfAreaId [ ] ={1,3,6,1,2,1,14,7,1,3};
UINT4 OspfIfType [ ] ={1,3,6,1,2,1,14,7,1,4};
UINT4 OspfIfAdminStat [ ] ={1,3,6,1,2,1,14,7,1,5};
UINT4 OspfIfRtrPriority [ ] ={1,3,6,1,2,1,14,7,1,6};
UINT4 OspfIfTransitDelay [ ] ={1,3,6,1,2,1,14,7,1,7};
UINT4 OspfIfRetransInterval [ ] ={1,3,6,1,2,1,14,7,1,8};
UINT4 OspfIfHelloInterval [ ] ={1,3,6,1,2,1,14,7,1,9};
UINT4 OspfIfRtrDeadInterval [ ] ={1,3,6,1,2,1,14,7,1,10};
UINT4 OspfIfPollInterval [ ] ={1,3,6,1,2,1,14,7,1,11};
UINT4 OspfIfState [ ] ={1,3,6,1,2,1,14,7,1,12};
UINT4 OspfIfDesignatedRouter [ ] ={1,3,6,1,2,1,14,7,1,13};
UINT4 OspfIfBackupDesignatedRouter [ ] ={1,3,6,1,2,1,14,7,1,14};
UINT4 OspfIfEvents [ ] ={1,3,6,1,2,1,14,7,1,15};
UINT4 OspfIfAuthKey [ ] ={1,3,6,1,2,1,14,7,1,16};
UINT4 OspfIfStatus [ ] ={1,3,6,1,2,1,14,7,1,17};
UINT4 OspfIfMulticastForwarding [ ] ={1,3,6,1,2,1,14,7,1,18};
UINT4 OspfIfDemand [ ] ={1,3,6,1,2,1,14,7,1,19};
UINT4 OspfIfAuthType [ ] ={1,3,6,1,2,1,14,7,1,20};
UINT4 OspfIfMetricIpAddress [ ] ={1,3,6,1,2,1,14,8,1,1};
UINT4 OspfIfMetricAddressLessIf [ ] ={1,3,6,1,2,1,14,8,1,2};
UINT4 OspfIfMetricTOS [ ] ={1,3,6,1,2,1,14,8,1,3};
UINT4 OspfIfMetricValue [ ] ={1,3,6,1,2,1,14,8,1,4};
UINT4 OspfIfMetricStatus [ ] ={1,3,6,1,2,1,14,8,1,5};
UINT4 OspfVirtIfAreaId [ ] ={1,3,6,1,2,1,14,9,1,1};
UINT4 OspfVirtIfNeighbor [ ] ={1,3,6,1,2,1,14,9,1,2};
UINT4 OspfVirtIfTransitDelay [ ] ={1,3,6,1,2,1,14,9,1,3};
UINT4 OspfVirtIfRetransInterval [ ] ={1,3,6,1,2,1,14,9,1,4};
UINT4 OspfVirtIfHelloInterval [ ] ={1,3,6,1,2,1,14,9,1,5};
UINT4 OspfVirtIfRtrDeadInterval [ ] ={1,3,6,1,2,1,14,9,1,6};
UINT4 OspfVirtIfState [ ] ={1,3,6,1,2,1,14,9,1,7};
UINT4 OspfVirtIfEvents [ ] ={1,3,6,1,2,1,14,9,1,8};
UINT4 OspfVirtIfAuthKey [ ] ={1,3,6,1,2,1,14,9,1,9};
UINT4 OspfVirtIfStatus [ ] ={1,3,6,1,2,1,14,9,1,10};
UINT4 OspfVirtIfAuthType [ ] ={1,3,6,1,2,1,14,9,1,11};
UINT4 OspfNbrIpAddr [ ] ={1,3,6,1,2,1,14,10,1,1};
UINT4 OspfNbrAddressLessIndex [ ] ={1,3,6,1,2,1,14,10,1,2};
UINT4 OspfNbrRtrId [ ] ={1,3,6,1,2,1,14,10,1,3};
UINT4 OspfNbrOptions [ ] ={1,3,6,1,2,1,14,10,1,4};
UINT4 OspfNbrPriority [ ] ={1,3,6,1,2,1,14,10,1,5};
UINT4 OspfNbrState [ ] ={1,3,6,1,2,1,14,10,1,6};
UINT4 OspfNbrEvents [ ] ={1,3,6,1,2,1,14,10,1,7};
UINT4 OspfNbrLsRetransQLen [ ] ={1,3,6,1,2,1,14,10,1,8};
UINT4 OspfNbmaNbrStatus [ ] ={1,3,6,1,2,1,14,10,1,9};
UINT4 OspfNbmaNbrPermanence [ ] ={1,3,6,1,2,1,14,10,1,10};
UINT4 OspfNbrHelloSuppressed [ ] ={1,3,6,1,2,1,14,10,1,11};
UINT4 OspfVirtNbrArea [ ] ={1,3,6,1,2,1,14,11,1,1};
UINT4 OspfVirtNbrRtrId [ ] ={1,3,6,1,2,1,14,11,1,2};
UINT4 OspfVirtNbrIpAddr [ ] ={1,3,6,1,2,1,14,11,1,3};
UINT4 OspfVirtNbrOptions [ ] ={1,3,6,1,2,1,14,11,1,4};
UINT4 OspfVirtNbrState [ ] ={1,3,6,1,2,1,14,11,1,5};
UINT4 OspfVirtNbrEvents [ ] ={1,3,6,1,2,1,14,11,1,6};
UINT4 OspfVirtNbrLsRetransQLen [ ] ={1,3,6,1,2,1,14,11,1,7};
UINT4 OspfVirtNbrHelloSuppressed [ ] ={1,3,6,1,2,1,14,11,1,8};
UINT4 OspfExtLsdbType [ ] ={1,3,6,1,2,1,14,12,1,1};
UINT4 OspfExtLsdbLsid [ ] ={1,3,6,1,2,1,14,12,1,2};
UINT4 OspfExtLsdbRouterId [ ] ={1,3,6,1,2,1,14,12,1,3};
UINT4 OspfExtLsdbSequence [ ] ={1,3,6,1,2,1,14,12,1,4};
UINT4 OspfExtLsdbAge [ ] ={1,3,6,1,2,1,14,12,1,5};
UINT4 OspfExtLsdbChecksum [ ] ={1,3,6,1,2,1,14,12,1,6};
UINT4 OspfExtLsdbAdvertisement [ ] ={1,3,6,1,2,1,14,12,1,7};
UINT4 OspfAreaAggregateAreaID [ ] ={1,3,6,1,2,1,14,14,1,1};
UINT4 OspfAreaAggregateLsdbType [ ] ={1,3,6,1,2,1,14,14,1,2};
UINT4 OspfAreaAggregateNet [ ] ={1,3,6,1,2,1,14,14,1,3};
UINT4 OspfAreaAggregateMask [ ] ={1,3,6,1,2,1,14,14,1,4};
UINT4 OspfAreaAggregateStatus [ ] ={1,3,6,1,2,1,14,14,1,5};
UINT4 OspfAreaAggregateEffect [ ] ={1,3,6,1,2,1,14,14,1,6};


tMbDbEntry stdospMibEntry[]= {

{{9,OspfRouterId}, NULL, OspfRouterIdGet, OspfRouterIdSet, OspfRouterIdTest, OspfRouterIdDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,OspfAdminStat}, NULL, OspfAdminStatGet, OspfAdminStatSet, OspfAdminStatTest, OspfAdminStatDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,OspfVersionNumber}, NULL, OspfVersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,OspfAreaBdrRtrStatus}, NULL, OspfAreaBdrRtrStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,OspfASBdrRtrStatus}, NULL, OspfASBdrRtrStatusGet, OspfASBdrRtrStatusSet, OspfASBdrRtrStatusTest, OspfASBdrRtrStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,OspfExternLsaCount}, NULL, OspfExternLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,OspfExternLsaCksumSum}, NULL, OspfExternLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,OspfTOSSupport}, NULL, OspfTOSSupportGet, OspfTOSSupportSet, OspfTOSSupportTest, OspfTOSSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,OspfOriginateNewLsas}, NULL, OspfOriginateNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,OspfRxNewLsas}, NULL, OspfRxNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,OspfExtLsdbLimit}, NULL, OspfExtLsdbLimitGet, OspfExtLsdbLimitSet, OspfExtLsdbLimitTest, OspfExtLsdbLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "-1"},

{{9,OspfMulticastExtensions}, NULL, OspfMulticastExtensionsGet, OspfMulticastExtensionsSet, OspfMulticastExtensionsTest, OspfMulticastExtensionsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{9,OspfExitOverflowInterval}, NULL, OspfExitOverflowIntervalGet, OspfExitOverflowIntervalSet, OspfExitOverflowIntervalTest, OspfExitOverflowIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{9,OspfDemandExtensions}, NULL, OspfDemandExtensionsGet, OspfDemandExtensionsSet, OspfDemandExtensionsTest, OspfDemandExtensionsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,OspfAreaId}, GetNextIndexOspfAreaTable, OspfAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfAreaTableINDEX, 1, 0, 0, NULL},

{{10,OspfAuthType}, GetNextIndexOspfAreaTable, OspfAuthTypeGet, OspfAuthTypeSet, OspfAuthTypeTest, OspfAreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfAreaTableINDEX, 1, 0, 0, "0"},

{{10,OspfImportAsExtern}, GetNextIndexOspfAreaTable, OspfImportAsExternGet, OspfImportAsExternSet, OspfImportAsExternTest, OspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaTableINDEX, 1, 0, 0, "1"},

{{10,OspfSpfRuns}, GetNextIndexOspfAreaTable, OspfSpfRunsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, OspfAreaTableINDEX, 1, 0, 0, NULL},

{{10,OspfAreaBdrRtrCount}, GetNextIndexOspfAreaTable, OspfAreaBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, OspfAreaTableINDEX, 1, 0, 0, NULL},

{{10,OspfAsBdrRtrCount}, GetNextIndexOspfAreaTable, OspfAsBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, OspfAreaTableINDEX, 1, 0, 0, NULL},

{{10,OspfAreaLsaCount}, GetNextIndexOspfAreaTable, OspfAreaLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, OspfAreaTableINDEX, 1, 0, 0, NULL},

{{10,OspfAreaLsaCksumSum}, GetNextIndexOspfAreaTable, OspfAreaLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfAreaTableINDEX, 1, 0, 0, "0"},

{{10,OspfAreaSummary}, GetNextIndexOspfAreaTable, OspfAreaSummaryGet, OspfAreaSummarySet, OspfAreaSummaryTest, OspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaTableINDEX, 1, 0, 0, "1"},

{{10,OspfAreaStatus}, GetNextIndexOspfAreaTable, OspfAreaStatusGet, OspfAreaStatusSet, OspfAreaStatusTest, OspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaTableINDEX, 1, 0, 1, NULL},

{{10,OspfStubAreaId}, GetNextIndexOspfStubAreaTable, OspfStubAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfStubAreaTableINDEX, 2, 0, 0, NULL},

{{10,OspfStubTOS}, GetNextIndexOspfStubAreaTable, OspfStubTOSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfStubAreaTableINDEX, 2, 0, 0, NULL},

{{10,OspfStubMetric}, GetNextIndexOspfStubAreaTable, OspfStubMetricGet, OspfStubMetricSet, OspfStubMetricTest, OspfStubAreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfStubAreaTableINDEX, 2, 0, 0, NULL},

{{10,OspfStubStatus}, GetNextIndexOspfStubAreaTable, OspfStubStatusGet, OspfStubStatusSet, OspfStubStatusTest, OspfStubAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfStubAreaTableINDEX, 2, 0, 1, NULL},

{{10,OspfStubMetricType}, GetNextIndexOspfStubAreaTable, OspfStubMetricTypeGet, OspfStubMetricTypeSet, OspfStubMetricTypeTest, OspfStubAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfStubAreaTableINDEX, 2, 0, 0, "1"},

{{10,OspfLsdbAreaId}, GetNextIndexOspfLsdbTable, OspfLsdbAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbType}, GetNextIndexOspfLsdbTable, OspfLsdbTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbLsid}, GetNextIndexOspfLsdbTable, OspfLsdbLsidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbRouterId}, GetNextIndexOspfLsdbTable, OspfLsdbRouterIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbSequence}, GetNextIndexOspfLsdbTable, OspfLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbAge}, GetNextIndexOspfLsdbTable, OspfLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbChecksum}, GetNextIndexOspfLsdbTable, OspfLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfLsdbAdvertisement}, GetNextIndexOspfLsdbTable, OspfLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, OspfLsdbTableINDEX, 4, 0, 0, NULL},

{{10,OspfAreaRangeAreaId}, GetNextIndexOspfAreaRangeTable, OspfAreaRangeAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfAreaRangeTableINDEX, 2, 0, 0, NULL},

{{10,OspfAreaRangeNet}, GetNextIndexOspfAreaRangeTable, OspfAreaRangeNetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfAreaRangeTableINDEX, 2, 0, 0, NULL},

{{10,OspfAreaRangeMask}, GetNextIndexOspfAreaRangeTable, OspfAreaRangeMaskGet, OspfAreaRangeMaskSet, OspfAreaRangeMaskTest, OspfAreaRangeTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, OspfAreaRangeTableINDEX, 2, 0, 0, NULL},

{{10,OspfAreaRangeStatus}, GetNextIndexOspfAreaRangeTable, OspfAreaRangeStatusGet, OspfAreaRangeStatusSet, OspfAreaRangeStatusTest, OspfAreaRangeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaRangeTableINDEX, 2, 0, 1, NULL},

{{10,OspfAreaRangeEffect}, GetNextIndexOspfAreaRangeTable, OspfAreaRangeEffectGet, OspfAreaRangeEffectSet, OspfAreaRangeEffectTest, OspfAreaRangeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaRangeTableINDEX, 2, 0, 0, "1"},

{{10,OspfHostIpAddress}, GetNextIndexOspfHostTable, OspfHostIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfHostTableINDEX, 2, 0, 0, NULL},

{{10,OspfHostTOS}, GetNextIndexOspfHostTable, OspfHostTOSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfHostTableINDEX, 2, 0, 0, NULL},

{{10,OspfHostMetric}, GetNextIndexOspfHostTable, OspfHostMetricGet, OspfHostMetricSet, OspfHostMetricTest, OspfHostTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfHostTableINDEX, 2, 0, 0, NULL},

{{10,OspfHostStatus}, GetNextIndexOspfHostTable, OspfHostStatusGet, OspfHostStatusSet, OspfHostStatusTest, OspfHostTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfHostTableINDEX, 2, 0, 1, NULL},

{{10,OspfHostAreaID}, GetNextIndexOspfHostTable, OspfHostAreaIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfHostTableINDEX, 2, 0, 0, NULL},

{{10,OspfIfIpAddress}, GetNextIndexOspfIfTable, OspfIfIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfAddressLessIf}, GetNextIndexOspfIfTable, OspfAddressLessIfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfIfAreaId}, GetNextIndexOspfIfTable, OspfIfAreaIdGet, OspfIfAreaIdSet, OspfIfAreaIdTest, OspfIfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfIfType}, GetNextIndexOspfIfTable, OspfIfTypeGet, OspfIfTypeSet, OspfIfTypeTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfIfAdminStat}, GetNextIndexOspfIfTable, OspfIfAdminStatGet, OspfIfAdminStatSet, OspfIfAdminStatTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfIfRtrPriority}, GetNextIndexOspfIfTable, OspfIfRtrPriorityGet, OspfIfRtrPrioritySet, OspfIfRtrPriorityTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfIfTransitDelay}, GetNextIndexOspfIfTable, OspfIfTransitDelayGet, OspfIfTransitDelaySet, OspfIfTransitDelayTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfIfRetransInterval}, GetNextIndexOspfIfTable, OspfIfRetransIntervalGet, OspfIfRetransIntervalSet, OspfIfRetransIntervalTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "5"},

{{10,OspfIfHelloInterval}, GetNextIndexOspfIfTable, OspfIfHelloIntervalGet, OspfIfHelloIntervalSet, OspfIfHelloIntervalTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "10"},

{{10,OspfIfRtrDeadInterval}, GetNextIndexOspfIfTable, OspfIfRtrDeadIntervalGet, OspfIfRtrDeadIntervalSet, OspfIfRtrDeadIntervalTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "40"},

{{10,OspfIfPollInterval}, GetNextIndexOspfIfTable, OspfIfPollIntervalGet, OspfIfPollIntervalSet, OspfIfPollIntervalTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "120"},

{{10,OspfIfState}, GetNextIndexOspfIfTable, OspfIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfIfDesignatedRouter}, GetNextIndexOspfIfTable, OspfIfDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfIfBackupDesignatedRouter}, GetNextIndexOspfIfTable, OspfIfBackupDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfIfEvents}, GetNextIndexOspfIfTable, OspfIfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, OspfIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfIfAuthKey}, GetNextIndexOspfIfTable, OspfIfAuthKeyGet, OspfIfAuthKeySet, OspfIfAuthKeyTest, OspfIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfIfStatus}, GetNextIndexOspfIfTable, OspfIfStatusGet, OspfIfStatusSet, OspfIfStatusTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 1, NULL},

{{10,OspfIfMulticastForwarding}, GetNextIndexOspfIfTable, OspfIfMulticastForwardingGet, OspfIfMulticastForwardingSet, OspfIfMulticastForwardingTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfIfDemand}, GetNextIndexOspfIfTable, OspfIfDemandGet, OspfIfDemandSet, OspfIfDemandTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "2"},

{{10,OspfIfAuthType}, GetNextIndexOspfIfTable, OspfIfAuthTypeGet, OspfIfAuthTypeSet, OspfIfAuthTypeTest, OspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfIfMetricIpAddress}, GetNextIndexOspfIfMetricTable, OspfIfMetricIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfIfMetricTableINDEX, 3, 0, 0, NULL},

{{10,OspfIfMetricAddressLessIf}, GetNextIndexOspfIfMetricTable, OspfIfMetricAddressLessIfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfIfMetricTableINDEX, 3, 0, 0, NULL},

{{10,OspfIfMetricTOS}, GetNextIndexOspfIfMetricTable, OspfIfMetricTOSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfIfMetricTableINDEX, 3, 0, 0, NULL},

{{10,OspfIfMetricValue}, GetNextIndexOspfIfMetricTable, OspfIfMetricValueGet, OspfIfMetricValueSet, OspfIfMetricValueTest, OspfIfMetricTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfIfMetricTableINDEX, 3, 0, 0, NULL},

{{10,OspfIfMetricStatus}, GetNextIndexOspfIfMetricTable, OspfIfMetricStatusGet, OspfIfMetricStatusSet, OspfIfMetricStatusTest, OspfIfMetricTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfIfMetricTableINDEX, 3, 0, 1, NULL},

{{10,OspfVirtIfAreaId}, GetNextIndexOspfVirtIfTable, OspfVirtIfAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfVirtIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtIfNeighbor}, GetNextIndexOspfVirtIfTable, OspfVirtIfNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfVirtIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtIfTransitDelay}, GetNextIndexOspfVirtIfTable, OspfVirtIfTransitDelayGet, OspfVirtIfTransitDelaySet, OspfVirtIfTransitDelayTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfVirtIfRetransInterval}, GetNextIndexOspfVirtIfTable, OspfVirtIfRetransIntervalGet, OspfVirtIfRetransIntervalSet, OspfVirtIfRetransIntervalTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 0, "5"},

{{10,OspfVirtIfHelloInterval}, GetNextIndexOspfVirtIfTable, OspfVirtIfHelloIntervalGet, OspfVirtIfHelloIntervalSet, OspfVirtIfHelloIntervalTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 0, "10"},

{{10,OspfVirtIfRtrDeadInterval}, GetNextIndexOspfVirtIfTable, OspfVirtIfRtrDeadIntervalGet, OspfVirtIfRtrDeadIntervalSet, OspfVirtIfRtrDeadIntervalTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 0, "60"},

{{10,OspfVirtIfState}, GetNextIndexOspfVirtIfTable, OspfVirtIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfVirtIfTableINDEX, 2, 0, 0, "1"},

{{10,OspfVirtIfEvents}, GetNextIndexOspfVirtIfTable, OspfVirtIfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, OspfVirtIfTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtIfAuthKey}, GetNextIndexOspfVirtIfTable, OspfVirtIfAuthKeyGet, OspfVirtIfAuthKeySet, OspfVirtIfAuthKeyTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfVirtIfStatus}, GetNextIndexOspfVirtIfTable, OspfVirtIfStatusGet, OspfVirtIfStatusSet, OspfVirtIfStatusTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 1, NULL},

{{10,OspfVirtIfAuthType}, GetNextIndexOspfVirtIfTable, OspfVirtIfAuthTypeGet, OspfVirtIfAuthTypeSet, OspfVirtIfAuthTypeTest, OspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfVirtIfTableINDEX, 2, 0, 0, "0"},

{{10,OspfNbrIpAddr}, GetNextIndexOspfNbrTable, OspfNbrIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfNbrAddressLessIndex}, GetNextIndexOspfNbrTable, OspfNbrAddressLessIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfNbrRtrId}, GetNextIndexOspfNbrTable, OspfNbrRtrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, "0"},

{{10,OspfNbrOptions}, GetNextIndexOspfNbrTable, OspfNbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, "0"},

{{10,OspfNbrPriority}, GetNextIndexOspfNbrTable, OspfNbrPriorityGet, OspfNbrPrioritySet, OspfNbrPriorityTest, OspfNbrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, OspfNbrTableINDEX, 2, 0, 0, "1"},

{{10,OspfNbrState}, GetNextIndexOspfNbrTable, OspfNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, "1"},

{{10,OspfNbrEvents}, GetNextIndexOspfNbrTable, OspfNbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfNbrLsRetransQLen}, GetNextIndexOspfNbrTable, OspfNbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfNbmaNbrStatus}, GetNextIndexOspfNbrTable, OspfNbmaNbrStatusGet, OspfNbmaNbrStatusSet, OspfNbmaNbrStatusTest, OspfNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfNbrTableINDEX, 2, 0, 1, NULL},

{{10,OspfNbmaNbrPermanence}, GetNextIndexOspfNbrTable, OspfNbmaNbrPermanenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, "2"},

{{10,OspfNbrHelloSuppressed}, GetNextIndexOspfNbrTable, OspfNbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrArea}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrAreaGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrRtrId}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrRtrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrIpAddr}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrOptions}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrState}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrEvents}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrLsRetransQLen}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfVirtNbrHelloSuppressed}, GetNextIndexOspfVirtNbrTable, OspfVirtNbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfVirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,OspfExtLsdbType}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfExtLsdbLsid}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbLsidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfExtLsdbRouterId}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbRouterIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfExtLsdbSequence}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfExtLsdbAge}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfExtLsdbChecksum}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfExtLsdbAdvertisement}, GetNextIndexOspfExtLsdbTable, OspfExtLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, OspfExtLsdbTableINDEX, 3, 0, 0, NULL},

{{10,OspfAreaAggregateAreaID}, GetNextIndexOspfAreaAggregateTable, OspfAreaAggregateAreaIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{10,OspfAreaAggregateLsdbType}, GetNextIndexOspfAreaAggregateTable, OspfAreaAggregateLsdbTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, OspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{10,OspfAreaAggregateNet}, GetNextIndexOspfAreaAggregateTable, OspfAreaAggregateNetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{10,OspfAreaAggregateMask}, GetNextIndexOspfAreaAggregateTable, OspfAreaAggregateMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, OspfAreaAggregateTableINDEX, 4, 0, 0, NULL},

{{10,OspfAreaAggregateStatus}, GetNextIndexOspfAreaAggregateTable, OspfAreaAggregateStatusGet, OspfAreaAggregateStatusSet, OspfAreaAggregateStatusTest, OspfAreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaAggregateTableINDEX, 4, 0, 1, NULL},

{{10,OspfAreaAggregateEffect}, GetNextIndexOspfAreaAggregateTable, OspfAreaAggregateEffectGet, OspfAreaAggregateEffectSet, OspfAreaAggregateEffectTest, OspfAreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, OspfAreaAggregateTableINDEX, 4, 0, 0, "1"},
};
tMibData stdospEntry = { 115, stdospMibEntry };
#endif /* _STDOSPDB_H */

