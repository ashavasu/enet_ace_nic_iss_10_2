/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osfssnmp.h,v 1.27 2017/03/10 12:45:34 siva Exp $
 *
 * Description:This file contains macros, constants and
 *             type definitions relating to network management.
 *
 *******************************************************************/

#ifndef _OSSNMPIF_H
#define _OSSNMPIF_H


/* The following are opcodes for identifying the procedures which provide
 * interface with the OSPF task.
 */

#define   OSPF_SNMP_IF_MSG      1
#define   OSPF_IP_IF_MSG        2
#define   OSPF_APP_IF_MSG       3
#define   OSPF_RTM_IF_MSG       4
#define   OSPF_IP_PKT_RCVD      5
#define   OSPF_VCM_MSG_RCVD     6
#define   OSPF_RXMT_NODE_ALLOC_FAIL_MSG 7
#define   OSPF_ROUTEMAP_UPDATE_MSG      8
#define   OSPF_RED_IF_MSG       9

#define OSPF_SET_PROTOCOL_STATUS     1
#define OSPF_SET_AS_BDR_RTR_STATUS   2
#define OSPF_DELETE_AREA             3
#define OSPF_DELETE_HOST             4
#define OSPF_ACTIVE_AREA_ADDR_RANGE  5
#define OSPF_SIGNAL_LSA_REGEN        6
#define OSPF_REGENERATE_LSA          7
#define OSPF_EXTRT_INACTIVATE        8
#define OSPF_EXTRT_DELETE            9
#define OSPF_IF_ACTIVATE             10
#define OSPF_IF_INACTIVATE           11
#define OSPF_DELETE_IF               12
#define OSPF_SET_ADMN_STATUS         13
#define OSPF_SET_IF_AREA_ID          15
#define OSPF_DELETE_IF_METRIC        16
#define OSPF_CREATE_VIRT_IF          17
#define OSPF_ACTIVATE_NBR            18
#define OSPF_INACTIVATE_NBR          19
#define OSPF_DELETE_NBR              20
#define OSPF_ACTIVATE_VIRT_IF        21
#define OSPF_INACTIVATE_VIRT_IF      22
#define OSPF_CHNG_OPQ_STATUS         23
#define OSPF_CHNG_ROUTERID           24
#define OSPF_CHANGE_AREA_SUMMARY     25
#define OSPF_SET_IF_DEMAND           26
#define OSPF_SET_IF_TYPE             27
#define OSPF_NBR_LL_DOWN             28
#define OSPF_DELETE_AREA_ADDR_RANGE  29
#define SET_AREA_AGGR_EFFECT         30
#define OSPF_SET_IF_PASSIVE          31
#define OSPF_SET_ASBR_TRANS_STATUS   32
#define OSPF_AS_EXT_AGG       33
#define OSPF_EXTRT_ADD       34
#define SET_AREA_AGGR_TAG      35
#define OSPF_CHNG_AREA_TYPE      36
#define OSPF_TRNSLTR_ROLE_CHANGE     37
#define OSPF_SET_EXT_LSDB_LIMIT      38
#define OSPF_SET_EXIT_OVERFLOW_INT   39
#define OSPF_SET_ABR_TYPE            40
#ifdef TOS_SUPPORT
#define OSPF_SET_TOS_SUPPORT         41
#endif /* TOS_SUPPORT */
#define OSPF_DISABLE_SRC_PROTO       42
#define SET_RRD_CONFIG_RECORD        43
#define DELETE_RRD_CONFIG_RECORD     44
/* Graceful restart related macros */
/* Restarting router related macros */
#define OSPF_GR_SET_RESTART_SUPPORT  45 /* Sets the restart support 
                                         * (none/planned/planned and unplanned) */
#define OSPF_GR_SET_GRACE_PERIOD     46 /* Sets the grace period */
#define OSPF_GR_PERFORM_RESTART      47 /* Perfoms the graceful
                                         * restart process */

#define  OSPF_GR_SET_HELPER_SUPPORT  48
#define OSPF_GR_SET_STRICT_LSA_CHECK 49
#define OSPF_GR_SET_ACK_STATE 50
#define OSPF_GR_SET_GRACELSA_MAXCOUNT 51
#define OSPF_GR_SET_GR_RESTARTREASON 52
#define OSPF_GR_SET_HELPER_GRTIMELIMIT  53
#define OSPF_SET_ALL_IF_PASSIVE 54

/* BFD Event for neighbor down */
#define OSPF_BFD_NBR_DOWN 55

/* Mib related */
#define INVALID              0
#define NOT_IMPLEMENTED      -1

#define IMPORT_EXTERNAL      1
#define IMPORT_NO_EXTERNAL   2

#define ADV_MATCHING         1
#define DO_NOT_ADV_MATCHING  2

#define OSPF_METRIC          1
#define TYPE1EXT_METRIC 2
#define TYPE2EXT_METRIC 3


/* Note : All those variables in a table having read-create access
 * except rowstatus variable, not having default values are considered
 * as critical variables (CV)
 */
#define STUB_METRIC_MASK            0x01     /* ospfStubAreaTable CV */
#define STUB_METRIC_TYPE_MASK      0x02
#define  STUB_AREA_TABLE_STD_MASK   0x03 

#define HOST_METRIC_MASK            0x01     /* ospfHostTable CV */
#define HOST_IF_INDEX_MASK          0x02
#define HOST_TABLE_STD_MASK         0x03

#define IF_TYPE_MASK                0x01     /* ospfIfTable CV */
#define IF_TABLE_STD_MASK           0x01

#define IF_METRIC_VALUE_MASK        0x01     /* ospfExtLsdbTable CV */
#define IF_METRIC_TABLE_STD_MASK    0x01

#define EXT_ROUTE_METRIC_MASK       0x01
#define EXT_ROUTE_METRIC_TYPE_MASK  0x02

/* This mask is used if Forwarding address is Non Zero. */
#define EXT_ROUTE_STD_MASK1         0x03

#define EXT_ROUTE_NEXT_HOP_MASK     0x04
#define EXT_ROUTE_IF_INDEX_MASK     0x08

/* This mask is used if Forwarding address is Zero. */
#define EXT_ROUTE_STD_MASK2         0x0f

/* Size of SNMPv2 trap id */
#define SNMP_V2_TRAP_OID_LEN            11

/* Traps defined in 1850 MIB */
#define VIRT_IF_STATE_CHANGE_TRAP       1
#define NBR_STATE_CHANGE_TRAP           2
#define VIRT_NBR_STATE_CHANGE_TRAP      3
#define IF_CONFIG_ERROR_TRAP            4
#define VIRT_IF_CONFIG_ERROR_TRAP       5
#define IF_AUTH_FAILURE_TRAP            6
#define VIRT_IF_AUTH_FAILURE_TRAP       7
#define IF_RX_BAD_PACKET_TRAP           8
#define VIRT_IF_RX_BAD_PACKET_TRAP      9
#define IF_TX_RETRANSMIT_TRAP           10
#define VIRT_IF_TX_RETRANSMIT_TRAP      11
#define ORIGINATE_LSA_TRAP              12
#define MAX_AGE_LSA_TRAP                13
#define LSDB_OVERFLOW_TRAP              14
#define LSDB_APPROACHING_OVERFLOW_TRAP  15
#define IF_STATE_CHANGE_TRAP            16
#define RST_STATUS_CHANGE_TRAP          17
#define NBR_RST_STATUS_CHANGE_TRAP      18
#define VIRT_NBR_RST_STATUS_CHANGE_TRAP 19
#define OSPF_RED_STATE_CHANGE_TRAP      20

/* Configuration Error Types of IF_CONFIG_ERROR_TRAP */
#define OSPF_BAD_VERSION                1
#define AREA_MISMATCH                   2
#define UNKNOWN_NBMA_NBR                3
#define UNKNOWN_VIRTUAL_NBR             4
#define AUTH_TYPE_MISMATCH              5
#define OSPF_AUTH_FAILURE               6
#define NETMASK_MISMATCH                7
#define HELLO_INTERVAL_MISMATCH         8
#define DEAD_INTERVAL_MISMATCH          9
#define OPTIONS_MISMATCH                10

 /* Operation Types of ROW_STATUS_FAILURE_TRAPs */
#define OSPF_CREATE   1
#define OSPF_DELETE   2

#define ROW_CREATION  1
#define ROW_DELETION  2

#define ALLOWABLE_TRAP_COUNT_IN_SWP  7

#define  OSPF_MSGQ_IF_EVENT           TMO1_TASK_QUE_15_ENQ_EVENT 

#define OSPF_SNMP_IF_MSG_ALLOC()      SnmpOspfIfMsgAlloc() 
#define OSPF_SNMP_IF_MSG_FREE(pMsg)   QMSG_FREE (pMsg); 
#define OSPF_SNMP_IF_MSG_GET_PARAMS_PTR(pMsg) \
                               &(pMsg->ospfSnmpIfParam)
#define OSPF_SNMP_IF_MSG_SEND(pMsg)    SnmpOspfIfMsgSend(pMsg)

#define OSPF_SNMP_IF_EVENT_SEND() \
{     OsixSendEvent ((UINT4) NULL, (const UINT1 *)"OSPF" , \
                            OSPF_MSGQ_IF_EVENT ); \
      OsixTakeSem(SELF, CFG_SYNC_SEM_NAME, OSIX_WAIT, 0); \
}

#define ENQUEUE_REQUEST  1
#define PROCESS_REQUEST  2


/* Macros for validation of various data types. */
#define IS_VALID_STATUS_VALUE(x)    ((x == OSPF_ENABLED) || \
                                     (x == OSPF_DISABLED)) 
#define IS_VALID_TOS_VALUE(x)        (((x & 1) == 0) && (x >= 0) && \
                                      (x < ENCODE_TOS(OSPF_MAX_METRIC)))
#define IS_VALID_TRUTH_VALUE(x)      ((x == OSPF_TRUE) || (x == OSPF_FALSE))
#define IS_VALID_VALIDATION_VALUE(x) ((x == OSPF_VALID) || (x == OSPF_INVALID))

#define IS_VALID_METRIC(x)       ((x >= 0) && (x <= 0xffff))

#define IS_VALID_BIG_METRIC(x)   ((x >= 0) && (x <= 0xffffff))
#define IS_VALID_PRIORITY(x)     ((x >= 0) && (x <= 0xff))
#define IS_VALID_POS_INTEGER(x)  (x >=0)
#define IS_VALID_POSITIVE_INT(x) ((x >= 1) && (x <= 0xffffffff))
#define IS_VALID_HELLO_RANGE(x)  ((x >= 1) && (x <= 0xffff))
#define IS_VALID_MIN_LSA_TIME(x)  ((x > 0) && (x <= 0xffff))
#define IS_VALID_AGE(x)          ((x > 0) && (x <= MAX_AGE))

#define IS_VALID_ROUTER_ID(x)  (UtilIpAddrComp (x, gNullIpAddr) != OSPF_EQUAL) 

#define IS_VALID_IP_ADDR(x)    OspfIsValidIpAddress(x)

#define IS_INTERNAL_RTR_IN_STUB_AREA_IN_CXT(pOspfCxt) \
        ((TMO_SLL_Count(&(pOspfCxt->areasLst)) == 1) && \
        ((((tArea*)TMO_SLL_First(&pOspfCxt->areasLst))->u4AreaType)  \
         == STUB_AREA ))

#define IS_VALID_EXT_LSDB_LIMIT(x)       (x >= -1)
#define IS_VALID_EXIT_OVERFLOW_INT(x)    (x >= 0)
#define IS_VALID_ROWSTATUS_VALUE(x) \
        ((x == ACTIVE) || (x == NOT_READY) || (x == NOT_IN_SERVICE))
#define IS_VALID_METRIC_TYPE(x)           (x == OSPF_METRIC)
#define IS_VALID_IF_TYPE_VALUE(x)        ( (x > 0) && (x < 6) && (x != 4))
#define IS_VALID_AGG_SFFECT(x) \
        ((x == ADV_MATCHING) || (x == DO_NOT_ADV_MATCHING))
#define IS_VALID_IF_INDEX(x)             ((x >= 0) && (x <= 0xffff))
#define IS_VALID_EXT_METRIC_TYPE(x)      ((x == TYPE_1_METRIC) || \
                                               (x == TYPE_2_METRIC))
#define IS_VALID_ABR_TYPE(x)    ((x == STANDARD_ABR) || \
                                 (x == CISCO_ABR)    || \
                                 (x == IBM_ABR))
#define IS_VALID_RRD_STATUS_VALUE(x) \
                             ( ((x) == OSPF_ENABLED) || (x == OSPF_DISABLED) )

#define IS_VALID_RRD_TAG_TYPE_VALUE(x) \
                             ( ((x) == MANUAL_TAG) || (x == AUTOMATIC_TAG) )

#define IS_VALID_METRIC_VALUE(x) \
                               ( (x > 0) && (x <= 0xffffff))

/*** IP Address Validation ***/
#define THIS_HOST_ADDR        0x0            /* This host address */
#define LTD_B_CAST_ADDR       0xffffffff     /* Limited BroadCast address */

#define LOOP_BACK_ADDR_MASK   0xff000000     /* Loop back address
                                                127.x.x.x range */
#define LOOP_BACK_ADDRESES    0x7f000000

#define CLASS_A_HOST_MASK     0x00ffffff      /* Mask for Directed
                                                broadcast of Class A*/
#define CLASS_B_HOST_MASK     0x0000ffff      /* Mask for Directed
                                                broadcast of Class B*/
#define CLASS_C_HOST_MASK     0x000000ff      /* Mask for Directed
                                                broadcast of Class C*/

#define IS_CLASS_A_ADDR(u4Addr) \
                              ((u4Addr & 0x80000000) == 0)

#define IS_CLASS_B_ADDR(u4Addr) \
                              ((u4Addr & 0xc0000000) == 0x80000000)

#define IS_CLASS_C_ADDR(u4Addr) \
                              ((u4Addr & 0xe0000000) == 0xc0000000)

#define IS_MCAST_D_ADDR(u4Addr) \
                              (u4Addr >= 0xe0000000 && u4Addr <= 0xe00000ff)


/*
 * This structure contains the Opcode & Parameters that have to be passed while
 * posing an event to the OSPF from SNMP context.
 */
typedef struct _OspfSnmpIfParam {

    UINT1                   u1OpCode;
    UINT1           au1Pad[3];
    union {

        struct admnStatus {
            tTRUTHVALUE   bOspfStatus;
        } admnStatus;

        struct asbr_status {
            tTRUTHVALUE   bStatus;
        } asbr_status;

#ifdef TOS_SUPPORT
        struct tos_support {
            tTRUTHVALUE   bTosStatus;
        } tos_support;
#endif  /*  TOS_SUPPORT */

        struct {
            tArea         *pArea;
            tIPADDR       rangeNet;
            tIPADDRMASK   rangeMask;
            UINT1         u1AggregateIndex;
            UINT1         u1AggregateEffect;
            UINT1         au1Rsvd[2];
        } area_param;

        struct {
            tHost         *pHost;
        } host_param;

        struct {
            UINT1         *pStruct;
            UINT1         u1Type;
                            /* for LSA regeneration it holds LSA type &
                             * for LSA origination it holds the Signal type
                             */
            UINT1         au1Rsvd[3];
        } sig_param;

        struct {
            tInterface    *pInterface;
            UINT4         u4AreaId;
            UINT1         u1Type;
            UINT1         u1Demand;
            UINT1         au1Rsvd[2];
        } if_param;

        struct {
            tInterface    *pInterface;
            UINT4         u4IfIndex;
            UINT1         u1Status;
            UINT1         au1Rsvd[3];
        } admn_param;

        struct {
            tAreaId       areaId;
            tRouterId     nbrId;
            tROWSTATUS    rowStatus;
        } vif_param;

        struct {
            tInterface    *pInterface;
            tNeighbor     *pNbr;
        } nbr_param;
        struct {
            UINT4         u4SrcProtoDisMask;
        } srcProtoDisParam;
 struct {
            tIPADDR       rrdDestIPAddr;
            tIPADDRMASK   rrdDestAddrMask;
 } RRDConfigRecParam;

        struct {
            tRouterId     rtrId;
        } rtrId_param;
        struct {
            UINT4 u4ABRType;
        } abrtype_param;
        /* Graceful restart related structures */
        struct {
            UINT1         u1RestartSupport; /* None/planned/both */
        } restartSupportParam;
        struct {
            UINT4         u4GracePeriod;
        } gracePeriodParam;
        struct {
            UINT4         u4GraceAckState;
            UINT1         u1RestartType;
            UINT1         u1RestartReason;
            UINT1         u1GrLsaMaxTxCount;
            UINT1         u1Pad;
        } restart_param;
        struct {
            UINT4         u4HelperGrTimeLimit;
            UINT1         u1HelperSupport;
            UINT1         u1StrictLsaCheck;
            UINT2         u2Pad;
        } helper_param;
    } param;
    tOspfCxt               *pOspfCxt;
    UINT4                 u4OspfCxtId;
} tOspfSnmpIfParam;


/*
 * Following are the structures which are used for passing various informations
 * regarding the traps that are generated.
 */

typedef struct _IfAuthfailTrpInfo {
    tRouterId  rtrId;
    tIPADDR    ifIpAddr;
    UINT4      u4AddrlessIf;
    tIPADDR    pktSrcAddr;
    UINT1      u1ErrType;
    UINT1      u1PktType;
    UINT1      au1Rsvd[2];
} tIfAuthfailTrpInfo;

typedef struct _VirtifAuthfailTrpInfo {
    tRouterId  rtrId;
    tAreaId    areaId;
    tRouterId  nbrId;
    UINT1      u1ErrType;
    UINT1      u1PktType;
    UINT1      au1Rsvd[2];
} tVirtifAuthfailTrpInfo;

typedef struct _IfStChgTrapInfo {
    tRouterId  rtrId;
    tIPADDR    ifIpAddr;
    UINT4      u4AddrlessIf;
    UINT1      u1State;
    UINT1      au1Rsvd[3];
} tIfStChgTrapInfo;

typedef struct _VifOrVnbrStChgTrapInfo {
    tRouterId  rtrId;
    tAreaId    tranAreaId;
    tIPADDR    virtNbr;
    UINT1      u1State;
    UINT1      au1Rsvd[3];
} tVifOrVnbrStChgTrapInfo;

typedef struct _NbrStChgTrapInfo {
    tRouterId  rtrId;
    tIPADDR    nbrIpAddr;
    UINT4      u4NbrAddrlessIf;
    tRouterId  nbrId;
    UINT1      u1State;
    UINT1      au1Rsvd[3];
} tNbrStChgTrapInfo;

typedef struct _IfConfErrTrapInfo {
    tRouterId  rtrId;
    tIPADDR    ifIpAddr;
    UINT4      u4AddrlessIf;
    tIPADDR    packetSrcAdr;
    UINT1      u1ErrType;
    UINT1      u1PktType;
    UINT1      au1Rsvd[2];
} tIfConfErrTrapInfo;

typedef struct _VifConfErrTrapInfo {
    tRouterId  rtrId;
    tAreaId    tranAreaId;
    tIPADDR    virtNbr;
    UINT1      u1ErrType;
    UINT1      u1PktType;
    UINT1      au1Rsvd[2];
} tVifConfErrTrapInfo;

typedef struct _IfRxBadPktTrapInfo {
    tRouterId  rtrId;
    tIPADDR    ifIpAddr;
    UINT4      u4AddrlessIf;
    tIPADDR    packetSrcAdr;
    UINT1      u1PktType;
    UINT1      au1Rsvd[3];
} tIfRxBadPktTrapInfo;

typedef struct _VifRxBadPktTrapInfo {
    tRouterId  rtrId;
    tAreaId    tranAreaId;
    tIPADDR    virtNbr;
    UINT1      u1PktType;
    UINT1      au1Rsvd[3];
} tVifRxBadPktTrapInfo;

typedef struct _IfRxmtTrapInfo {
    tRouterId  rtrId;
    tIPADDR    ifIpAddr;
    UINT4      u4AddrlessIf;
    tIPADDR    nbrId;
    UINT1      u1PktType;
    UINT1      u1LsdbType;
    UINT1      au1Rsvd[2];
    tIPADDR    lsdbLsid;
    tIPADDR    lsdbRtrId;
} tIfRxmtTrapInfo;

typedef struct _VifRxmtTrapInfo {
    tRouterId  rtrId;
    tAreaId    tranAreaId;
    tIPADDR    virtNbr;
    UINT1      u1PktType;
    UINT1      u1LsdbType;
    UINT1      au1Rsvd[2];
    tIPADDR    lsdbLsid;
    tIPADDR    lsdbRtrId;
} tVifRxmtTrapInfo;

typedef struct _LsaTrapInfo {
    tRouterId  rtrId;
    tAreaId    lsdbAreaId;
    UINT1      u1LsdbType;
    UINT1      au1Rsvd[3];
    tIPADDR    lsdbLsid;
    tIPADDR    lsdbRtrId;
} tLsaTrapInfo;

typedef struct _ExtLsdbOverflowTrapInfo {
    tRouterId  rtrId;
    INT4       i4ExtLsdbLimit;
} tExtLsdbOverflowTrapInfo;

typedef struct _RstStatChgTrapInfo {
    tRouterId  rtrId;
    UINT4      u4GracePeriod;
    UINT1      u1RestartStatus;
    UINT1      u1RestartExitReason;
 UINT1      au1Rsvd[2];
}tRstStatChgTrapInfo;

typedef struct _NbrRstStatChgTrapInfo {
    tRouterId  rtrId;
    tRouterId  nbrId;
    tIPADDR    nbrIpAddr;
    UINT4      u4NbrAddrlessIf;
    UINT4      u4NbrHelperAge;
    UINT1      u1NbrHelperStatus;
    UINT1      u1NbrHelperExitReason;
 UINT1      au1Rsvd[2];
}tNbrRstStatChgTrapInfo;

typedef struct _VifRstStatChgTrapInfo {
    tRouterId  rtrId;
    tRouterId  VifNbrId;
    tAreaId    VifNbrAreaId;
    UINT4      u4VifNbrHelperAge;
    UINT1      u1VifNbrHelperStatus;
    UINT1      u1VifNbrHelperExitReason;
 UINT1      au1Rsvd[2];
}tVifRstStatChgTrapInfo;


typedef struct _OsRedStatChgTrapInfo{
    tRouterId  RtrId;
    INT4       i4HotStandbyState;
    INT4       i4DynamicBulkUpdStatus;
}tOsRedStatChgTrapInfo;


#define OSPF_TRAPS_OID_LEN  9
#define SNMP_VERSION        1

#endif
