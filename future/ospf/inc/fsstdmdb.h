/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsstdmdb.h,v 1.4 2008/08/20 15:20:08 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSTDMDB_H
#define _FSSTDMDB_H

UINT1 FsMIStdOspfTrapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsstdm [] ={1,3,6,1,4,1,2076,148};
tSNMP_OID_TYPE fsstdmOID = {8, fsstdm};


UINT4 FsMIStdOspfSetTrap [ ] ={1,3,6,1,4,1,2076,148,1,1,1,1};
UINT4 FsMIStdOspfConfigErrorType [ ] ={1,3,6,1,4,1,2076,148,1,1,1,2};
UINT4 FsMIStdOspfPacketType [ ] ={1,3,6,1,4,1,2076,148,1,1,1,3};
UINT4 FsMIStdOspfPacketSrc [ ] ={1,3,6,1,4,1,2076,148,1,1,1,4};


tMbDbEntry fsstdmMibEntry[]= {

{{12,FsMIStdOspfSetTrap}, GetNextIndexFsMIStdOspfTrapTable, FsMIStdOspfSetTrapGet, FsMIStdOspfSetTrapSet, FsMIStdOspfSetTrapTest, FsMIStdOspfTrapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdOspfTrapTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfConfigErrorType}, GetNextIndexFsMIStdOspfTrapTable, FsMIStdOspfConfigErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfTrapTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfPacketType}, GetNextIndexFsMIStdOspfTrapTable, FsMIStdOspfPacketTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfTrapTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfPacketSrc}, GetNextIndexFsMIStdOspfTrapTable, FsMIStdOspfPacketSrcGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfTrapTableINDEX, 1, 0, 0, NULL},
};
tMibData fsstdmEntry = { 4, fsstdmMibEntry };
#endif /* _FSSTDMDB_H */

