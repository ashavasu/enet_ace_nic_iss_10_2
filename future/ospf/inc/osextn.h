/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osextn.h,v 1.16 2012/04/03 13:08:30 siva Exp $
 *
 * Description:This file contains extern definitions in ospf
 *
 *******************************************************************/
 
#ifndef _OSEXTN_H
#define _OSEXTN_H

extern tTimerDesc      aTimerDesc[MAX_TIMERS];
extern DBL8 UtilCeil  (DBL8);

#endif /* _OSEXTN_H */
