/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osbufif.h,v 1.29 2017/09/21 13:48:45 siva Exp $
 *
 * Description:This file contains constants , typedefs & macros 
 *             related to Buffer interface.
 *
 *******************************************************************/
#ifndef _OSBUFIF_H
#define _OSBUFIF_H 

/*This is required for tuning the OSPF max LSA size.
 * Example to support 1048 OSPF max LSA size as
 * MAX_LSA_SIZE              1048*/
#define  MAX_LSA_SIZE              1024
#define  MAX_LSA_SIZE256           256
#define  MAX_LSA_SIZE512           512
#define  MAX_LSA_SIZE1024          1024
#define  MAX_LSA_SIZE2048          2048
#define  MAX_LSA_SIZE4096          4096
#define  MAX_ISM_SCHED_QUEUE_SIZE  16                  
#define  MAX_LSA_REQ_NODES         MAX_LSAS
/*This is required for tuning the maximum number of spf nodes*/
#define  MAX_SPF_NODES             128 * MAX_AREAS * 4 
#define  OSPF_HELLO_FIXED_SIZE     44 
/* 1. The maximum size of the hello packet
 * 2. Hello packet is per interface and the size would be varying 
 * based on number of neigbors connected on an interface
 * 3. For example on broadcast network, the number of neighbors 
 * could be varying based on topology. 
 * In case too many neighbors are there, 
 * then this parameter may required to be tuned
 * 4. Already in the implementation, the equation is maintained*/
#define  MAX_HELLO_SIZE            (OSPF_HELLO_FIXED_SIZE + (4 * MAX_OSPF_NBRS_LIMIT))
/* This is required for tuning the maximum number of OSPF low priority messages
 * Example to support 150 OSPF low priority messages as 
 * MAX_OSPF_LP_MSGS          150*/
#define  MAX_OSPF_LP_MSGS          100
#define  MAX_OSPF_LRQ_TX           10
/* This is required for tuning the maximum number of OSPF high priority messages
 * Example to support 60 OSPF low priority messages as
 * MAX_OSPF_HP_MSGS          60*/
#define  MAX_OSPF_HP_MSGS          40
#define  MAX_OSPF_MSG_PROCESSED    10
#define  MAX_OSPF_TX_RXMT_LS_UPDATES 16 /* This macro is used for controlling the number of 
                                        * LS update messages to be sent when the RXMT timer 
                                        * expires for a neighbor. 
                                        * Theoritically we have to send LS update messages for 
                                        * all the LSAs in the RXMT list 
                                        * Setting MAX_OSPF_TX_RXMT_LS_UPDATES to -1, makes the 
                                        * to happen, means is useful to send all the LSAs in update
                                        */
/* 1. This is the maximum number count after which 
 * we will re-linquishing the process of timer of expiry events
 * 2. The default value kept as 20
 * 3. It is possible when running on different processors and 
 * mainly scheduling effects on different OS, this parameter is expected to be tuned*/
#define  MAX_OSPF_TIMER_COUNTS     20
#define  MAX_EXT_LSA_SIZE        (LS_HEADER_SIZE + sizeof(tIPADDRMASK) + \
                                 (OSPF_MAX_METRIC * EXT_LSA_LINK_SIZE))
/* 
 * no. of hash buckets = 
 * total no. of objects stored / no. of objects per hash bucket;
 * no. of objects per hash bucket is chosen to be 16
 * it is subject to change
 */
#define  BUCKET_SIZE  16 
 
#define  IF_HASH_TABLE_SIZE       ((FsOSPFSizingParams[MAX_OSPF_INTERFACES_SIZING_ID].u4PreAllocatedUnits /                                             BUCKET_SIZE) + 1)   
#define  NBR_HASH_TABLE_SIZE      ((MAX_OSPF_NBRS_LIMIT / BUCKET_SIZE) + 1)         
#define  LSA_HASH_TABLE_SIZE      ((MAX_LSAS_PER_AREA) + 1)
#define  EXT_LSA_HASH_TABLE_SIZE  ((MAX_EXT_LSAS / BUCKET_SIZE) + 1)      
#define  RT_HASH_TABLE_SIZE       ((MAX_ROUTES / BUCKET_SIZE) + 1)        
#define  SPF_HASH_TABLE_SIZE      ((MAX_SPF_NODES / BUCKET_SIZE) + 1)    
#define  CANDTE_HASH_TABLE_SIZE   SPF_HASH_TABLE_SIZE              
#define  CANDTE_COST_HASH_TABLE_SIZE   100

#define  MAX_EXT_LSA_DB_NODE      BUCKET_SIZE * EXT_LSA_HASH_TABLE_SIZE

#define  SZMAX_LEN                (UINT1) 32   
#define  SZ4096_LEN               (UINT1) 16   
#define  SZ2048_LEN               (UINT1) 8    
#define  SZ1024_LEN               (UINT1) 4    
#define  SZ512_LEN                (UINT1) 2    
#define  SZ256_LEN                (UINT1) 1    

/* Macros for defining respective QIDs */
#define  CONTEXT_QID              OSPFMemPoolIds[MAX_OSPF_CONTEXTS_SIZING_ID] 
#define  AREA_QID                 OSPFMemPoolIds[MAX_OSPF_AREAS_SIZING_ID] 
#define  HOST_QID                 OSPFMemPoolIds[MAX_OSPF_HOSTS_SIZING_ID]   
#define  IF_QID                   OSPFMemPoolIds[MAX_OSPF_INTERFACES_SIZING_ID]
#define  QMSG_QID                 OSPFMemPoolIds[MAX_OSPF_QUE_MSGS_SIZING_ID] 
#define  PRIORITY_QMSG_QID        OSPFMemPoolIds[MAX_OSPF_LOWPRIO_QUE_MSGS_SIZING_ID] 
#define  APP_QID                  OSPFMemPoolIds[MAX_OSPF_OPQ_APPS_SIZING_ID]
#define  NBR_QID                  OSPFMemPoolIds[MAX_OSPF_NBRS_SIZING_ID]     
#define  LSA_DESC_QID             OSPFMemPoolIds[MAX_OSPF_LSA_DESCRIPTORS_SIZING_ID]      
#define  LSA_INFO_QID             OSPFMemPoolIds[MAX_OSPF_LSAINFOS_SIZING_ID]     
#define  SUM_PARAM_QID            OSPFMemPoolIds[MAX_OSPF_SUMMARY_PARAMS_SIZING_ID]    
#define  AS_EXT_AGG_QID           OSPFMemPoolIds[MAX_OSPF_EXT_ADDR_RANGES_SIZING_ID]    
#define  EXT_ROUTE_QID            OSPFMemPoolIds[MAX_OSPF_EXT_ROUTES_SIZING_ID]    
#define  RT_ENTRY_QID             OSPFMemPoolIds[MAX_OSPF_RT_ENTRIES_SIZING_ID]     
#define  PATH_QID                 OSPFMemPoolIds[MAX_OSPF_ROUTE_PATHS_SIZING_ID]         
#define  SPF_QID                  OSPFMemPoolIds[MAX_OSPF_CANDT_NODES_SIZING_ID]          
#define  LSA_REQ_QID              OSPFMemPoolIds[MAX_OSPF_REQ_NODES_SIZING_ID]      
#define  MD5AUTH_QID              OSPFMemPoolIds[MAX_OSPF_MD5_AUTH_KEYS_SIZING_ID]  
#define  DBMSG_QID                OSPFMemPoolIds[MAX_OSPF_EXT_LSA_DB_NODES_SIZING_ID]  
#define  REDISTR_CONFIG_INFO_QID  OSPFMemPoolIds[MAX_OSPF_REDISTRIBUTE_ROUTES_SIZING_ID] 
#define  RTM_ROUTE_QID            OSPFMemPoolIds[MAX_OSPF_RTM_ROUTE_CACHE_SIZING_ID] 
#define  OPQ_LSA_INFO_QID         OSPFMemPoolIds[MAX_OSPF_OPQ_LSAS_SIZING_ID]  
#define  RTR_LSA_LINK_QID         OSPFMemPoolIds[MAX_OSPF_RTR_LSA_INFO_SIZING_ID] 
#define  NW_LSA_LINK_QID          OSPFMemPoolIds[MAX_OSPF_NW_LSA_INFO_SIZING_ID]
#define  RMAP_FILTER_QID          OSPFMemPoolIds[MAX_OSPF_RMAP_FILTER_SIZING_ID]
#define  REDIST_REG_QID           OSPFMemPoolIds[MAX_OSPF_REDIST_REG_SIZING_ID]
#define  ROUTING_TABLE_QID        OSPFMemPoolIds[MAX_OSPF_ROUTING_TABLE_INSTANCE_SIZING_ID]
#define  EXT_LSA_QID              OSPFMemPoolIds[MAX_OSPF_EXT_LSAS_SIZING_ID]
#define  LSA_QID                  OSPFMemPoolIds[MAX_OSPF_LSAS_SZ1024_SIZING_ID]
#define  LSA_QID1                 OSPFMemPoolIds[MAX_OSPF_LSAS_SZ256_SIZING_ID]
#define  LSA_QID2                 OSPFMemPoolIds[MAX_OSPF_LSAS_SZ512_SIZING_ID]
#define  LSA_QID4                 OSPFMemPoolIds[MAX_OSPF_LSAS_SZ2048_SIZING_ID]
#define  LSA_QID5                 OSPFMemPoolIds[MAX_OSPF_LSAS_SZ4096_SIZING_ID]
#define  VRF_SOCK_FD              OSPFMemPoolIds[MAX_OSPF_VRF_SOCK_SIZING_ID]

/* FSAP2 MemPoolAllocation Macros <--> Routines  */
#define CRU_BUF_CREATE_FREE_OBJ_POOL(u4BlockSize,u4NumOfBlocks,u4TypeOfMem,pPoolId)  \
        MemCreateMemPool( u4BlockSize , u4NumOfBlocks , u4TypeOfMem , (tMemPoolId *)pPoolId)

#define CRU_BUF_DELETE_FREE_OBJ_POOL(PoolId)  \
        MemDeleteMemPool( (tMemPoolId)PoolId )

/* Macros for memory allocation using cru buffers */
#define RTR_LSA_LINK_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(RTR_LSA_LINK_QID,pu1Block,tRtrLsaRtInfo)
#define NW_LSA_LINK_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(NW_LSA_LINK_QID,pu1Block,tNtLsaRtInfo)
#define CONTEXT_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(CONTEXT_QID,pu1Block,tOspfCxt)
#define AREA_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(AREA_QID,pu1Block,tArea)
#define HOST_ALLOC(pu1Block)   \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(HOST_QID,pu1Block,tHost)
#define IF_ALLOC(pu1Block)      \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(IF_QID,pu1Block,tInterface)
#define APP_ALLOC(pu1Block)      \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(APP_QID,pu1Block,tAppInfo)
#define MD5AUTH_ALLOC(pu1Block)   \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(MD5AUTH_QID,pu1Block,tMd5AuthkeyInfo)
#define NBR_ALLOC(pu1Block)  \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(NBR_QID,pu1Block,tNeighbor)
#define LSA_INFO_ALLOC(pu1Block)    \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(LSA_INFO_QID,pu1Block,tLsaInfo)
#define LSA_DESC_ALLOC(pu1Block)    \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(LSA_DESC_QID,pu1Block,tLsaDesc)
#define SUM_PARAM_ALLOC(pu1Block)   \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(SUM_PARAM_QID,pu1Block,UINT1)
#define EXT_ROUTE_ALLOC(pu1Block)   \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(EXT_ROUTE_QID,pu1Block,tExtRoute)
#define AS_EXT_AGG_ALLOC(pu1Block)   \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(AS_EXT_AGG_QID,pu1Block,tAsExtAddrRange)
#define RT_ENTRY_ALLOC(pu1Block)    \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(RT_ENTRY_QID,pu1Block,tRtEntry)
#define PATH_ALLOC(pu1Block)   \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(PATH_QID,pu1Block,tPath)
#define SPF_ALLOC(pu1Block)     \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(SPF_QID,pu1Block,tCandteNode)
#define LSA_REQ_ALLOC(pu1Block)     \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(LSA_REQ_QID,pu1Block,tLsaReqNode)
#define OPQ_LSA_INFO_ALLOC(pu1Block)  \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(OPQ_LSA_INFO_QID,pu1Block,tOpqLSAInfo)
#define QMSG_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(QMSG_QID,pu1Block,tOspfQMsg)
#define PRIORITY_QMSG_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(PRIORITY_QMSG_QID,pu1Block,tOspfLowPriQMsg)
#define OSPF_DB_NODE_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(DBMSG_QID,pu1Block,tOsDbNode)
#define REDISTR_CONFIG_INFO_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(REDISTR_CONFIG_INFO_QID,pu1Block,tRedistrConfigRouteInfo)
#define RTM_ROUTE_ALLOC(pu1Block) \
        OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(RTM_ROUTE_QID,pu1Block,tExpRtNode)
#define RMAP_FILTER_ALLOC(pu1Block) \
 OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(RMAP_FILTER_QID,pu1Block,tFilteringRMap)
#define REDIST_REG_ALLOC(pu1Block) \
 OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(REDIST_REG_QID,pu1Block,tRedistrRegInfo)
#define ROUTING_TABLE_ALLOC(pu1Block) \
 OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(ROUTING_TABLE_QID,pu1Block,tOspfRt)


#define  RTR_LSA_LINK_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(RTR_LSA_LINK_QID, (UINT1 *)ptr) 
#define  NW_LSA_LINK_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(NW_LSA_LINK_QID, (UINT1 *)ptr) 
#define  CONTEXT_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(CONTEXT_QID, (UINT1 *)ptr) 
#define  AREA_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(AREA_QID, (UINT1 *)ptr) 
#define  HOST_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(HOST_QID, (UINT1 *)ptr) 
#define  IF_FREE(ptr)    OSPF_CRU_BUF_RELEASE_FREE_OBJ(IF_QID, (UINT1 *)ptr)   
#define  APP_FREE(ptr)   OSPF_CRU_BUF_RELEASE_FREE_OBJ(APP_QID, (UINT1 *)ptr)   
#define MD5AUTH_FREE(ptr) \
                     OSPF_CRU_BUF_RELEASE_FREE_OBJ(MD5AUTH_QID, (UINT1 *)ptr)
#define NBR_FREE(ptr)       OSPF_CRU_BUF_RELEASE_FREE_OBJ(NBR_QID, (UINT1 *)ptr)
#define LSA_INFO_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(LSA_INFO_QID, (UINT1 *)ptr)
#define LSA_DESC_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(LSA_DESC_QID, (UINT1 *)ptr)
#define SUM_PARAM_FREE(ptr) \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ(SUM_PARAM_QID, (UINT1 *)ptr)
#define EXT_ROUTE_FREE(ptr) \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ(EXT_ROUTE_QID, (UINT1 *)ptr)
#define AS_EXT_AGG_FREE(ptr) \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ(AS_EXT_AGG_QID, (UINT1 *)ptr)
#define RT_ENTRY_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(RT_ENTRY_QID, (UINT1 *)ptr)
#define PATH_FREE(ptr)      OSPF_CRU_BUF_RELEASE_FREE_OBJ(PATH_QID, (UINT1 *)ptr)
#define SPF_FREE(ptr)       OSPF_CRU_BUF_RELEASE_FREE_OBJ(SPF_QID, (UINT1 *)ptr)
#define LSA_REQ_FREE(ptr)   OSPF_CRU_BUF_RELEASE_FREE_OBJ(LSA_REQ_QID, (UINT1 *)ptr)
#define QMSG_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(QMSG_QID, (UINT1 *)ptr) 
#define PRIORITY_QMSG_FREE(ptr)  OSPF_CRU_BUF_RELEASE_FREE_OBJ(PRIORITY_QMSG_QID, (UINT1 *)ptr)
#define OPQ_LSA_INFO_FREE(ptr)  \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ(OPQ_LSA_INFO_QID, (UINT1 *)ptr)
#define OSPF_DB_NODE_FREE(ptr)  \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ(DBMSG_QID, (UINT1 *)ptr)
#define REDISTR_CONFIG_INFO_FREE(ptr) \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ (REDISTR_CONFIG_INFO_QID, (UINT1 *)ptr)
#define RTM_ROUTE_FREE(ptr) \
        OSPF_CRU_BUF_RELEASE_FREE_OBJ (RTM_ROUTE_QID, (UINT1 *)ptr)

#define RMAP_FILTER_FREE(ptr) \
 OSPF_CRU_BUF_RELEASE_FREE_OBJ (RMAP_FILTER_QID, (UINT1 *)ptr)
#define REDIST_REG_FREE(ptr) \
 OSPF_CRU_BUF_RELEASE_FREE_OBJ (REDIST_REG_QID, (UINT1 *)ptr)
#define ROUTING_TABLE_FREE(ptr) \
 OSPF_CRU_BUF_RELEASE_FREE_OBJ (ROUTING_TABLE_QID, (UINT1 *)ptr)


#define CANDTE_ALLOC(pu1Block)      SPF_ALLOC(pu1Block)
#define CANDTE_FREE(ptr)    SPF_FREE(ptr)

/* Macros for memory allocation using buddy queues */
#define LSA_ALLOC(type,ptr,length) \
        if ( type == AS_EXT_LSA || type == NSSA_LSA )\
            ptr = (UINT1 *) MemAllocMemBlk (EXT_LSA_QID);\
    else if (type == ROUTER_LSA )\
        ptr = (UINT1 *) RouterLsaAlloc(length);\
    else\
        ptr = (UINT1 *) MemAllocMemBlk (LSA_QID);

#define  LSA_FREE(type ,ptr)\
        if ( type == AS_EXT_LSA || type == NSSA_LSA )\
    OSPF_CRU_BUF_RELEASE_FREE_OBJ( EXT_LSA_QID, (UINT1 *)ptr);\
    else if (type == ROUTER_LSA )\
        RouterLsaFree (ptr);\
    else\
    OSPF_CRU_BUF_RELEASE_FREE_OBJ( LSA_QID, (UINT1 *)ptr);
#define OPQ_LSA_MSG_FREE(ptr)\
 if (ptr->u1AppOpCode == SND_OPQ_LSA_FROM_APP_TO_OSPF)\
 OspfOpqLSAFree(ptr->AppParam.AppTxOpqLsaParam.pOpqLSAInfo)
#endif
