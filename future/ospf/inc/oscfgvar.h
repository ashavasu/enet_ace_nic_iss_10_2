/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oscfgvar.h,v 1.28 2013/05/30 13:06:30 siva Exp $
 *
 * Description:This file contains the Configurable constants.
 *
 *******************************************************************/

#ifndef _OSCFGVAR_H
#define _OSCFGVAR_H 

/****************************************************************************/
 /* Configurtion constants; porting sensitive                                */
 /****************************************************************************/

/* Ospf Sizing */
#define  MAX_AREAS                 MAX_OSPF_AREAS
#define OSPF_DEF_MAX_SELF_ORG_LSAS MAX_OSPF_LSA_DESCRIPTORS
/* Ospf Sizing */

#define  MAX_LSAS_PER_AREA  OSPF_DEF_MAX_LSA_PER_AREA                                             
#define  MAX_EXT_LSAS       OSPF_DEF_MAX_EXT_LSAS
#define  MAX_SELF_ORG_LSAS  OSPF_DEF_MAX_SELF_ORG_LSAS                                           
#define  MAX_ROUTES         OSPF_DEF_MAX_ROUTES 
#define  MAX_ADDR_RANGES_PER_AREA  3    
#define  MIN_ADDR_RANGES_PER_AREA  0
#define  MAX_HOSTS                 8                                          
 

#define  MAX_VIRTUAL_IFS         8                                            
#define  MAX_MD5AUTHKEYS_PER_IF  5                                            

#define  MAX_SEC_INTERFACES      10                                           
 
#define  MAX_NBRS_PER_IFACE      8                                            
#define  MAX_RXMT_NODES_PER_NBR  500 
                                 /*  MAX_RXMT_NODES_PER_NBR macro needs to changed to a higher
                                     value if the number of nighbors is reduced below 256 
                                     Assumption :
                                     (For MAX_OSPF_NBRS = 256 & MAX_RXMT_NODES_PER_NBR = 500
                                     If 1000 pps could be handled by HW,
                                     then it would make 2 min (256 * 500/1000) to process 
                                     256 * 500 Ack messages. If Rxmt list is not freed within 2 min
                                     this means that something is wrong in the network */

/* Start of HIGH_PERF_RXMT_LST_WANTED related macros */
#define  MAX_RXMT_NODES          256 * MAX_RXMT_NODES_PER_NBR
#define  MAX_RXMT_NODE_SIZE      18 /* No of bits required to store MAX_RXMT_NODES */
#define  MAX_NBR_DLL_HEAD_SIZE   5 /* No of bytes required to store (2 * MAX_RXMT_NODE_SIZE) bits */
#define  MAX_LSA_DLL_HEAD_SIZE   5 /* No of bytes required to store (2 * MAX_RXMT_NODE_SIZE) bits */
#define  MAX_RXMT_DLL_NODE_SIZE  9 /* No of bytes required to store (4 * MAX_RXMT_NODE_SIZE) bits */
/* End of HIGH_PERF_RXMT_LST_WANTED related macros */


#define  MAX_LSAS                MAX_LSAS_PER_AREA * MAX_AREAS + MAX_EXT_LSAS 
#define  MAX_SELF_ORG_SUM_LSAS   64                                           

#define  MAX_ECMP_PATHS          64     /* Extra paths for ECMP routes
                                           in addition to normal Routes */
#define  MAX_PATHS               (MAX_ROUTES + MAX_ECMP_PATHS) * 2
#define  MAX_NEXT_HOPS           10                                           

#define  MAX_EXT_ROUTES          MAX_EXT_LSAS                               

#define  MAX_OPQ_LSAS              512
#define  MAX_APP_REG               255

#define  MIN_METRIC_VALUE               1
#define  MAX_MERTIC_VALUE               16777214

/* 
 * The Frequency at which the Routing table calculation is done if  
 * 'u1RtrNetworkLsaChanged' flags is set 
 */
 
#define  MAX_REDISTR_ROUTES  64                       
#define MAX_GRACE_LSA_SENT   180 /*Maximum number of grace LSA can be sent out*/ 

#define  MAX_RTR_LSA_INFO     ((MAX_LSA_SIZE - (LS_HEADER_SIZE + OPTIONS_OR_MASK_LEN)) / \
                                       (sizeof (tRtrLsaRtInfo) - sizeof (tRBNodeEmbd)))
#define  MAX_NW_LSA_INFO      ((MAX_LSA_SIZE - (LS_HEADER_SIZE + OPTIONS_OR_MASK_LEN)) / \
                                      (sizeof (tNtLsaRtInfo) - sizeof (tRBNodeEmbd)))

/* Route map message array size which will preserve rmap messages during 
 * staggered state
 */
#define MAX_OSPF_RMAP_MSGS      10
#define MAX_OSPF_PKT_LEN      9216
#endif
