/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistdb.h,v 1.7 2017/03/10 12:45:34 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIOSSTDB_H
#define _FSMIOSSTDB_H

UINT1 FsMIStdOspfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfAreaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfStubAreaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfHostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfIfMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfVirtIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfVirtNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfExtLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfAreaAggregateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsmist [] ={1,3,6,1,4,1,2076,146};
tSNMP_OID_TYPE fsmistOID = {8, fsmist};


UINT4 FsMIStdOspfContextId [ ] ={1,3,6,1,4,1,2076,146,1,1,1,1};
UINT4 FsMIStdOspfRouterId [ ] ={1,3,6,1,4,1,2076,146,1,1,1,2};
UINT4 FsMIStdOspfAdminStat [ ] ={1,3,6,1,4,1,2076,146,1,1,1,3};
UINT4 FsMIStdOspfVersionNumber [ ] ={1,3,6,1,4,1,2076,146,1,1,1,4};
UINT4 FsMIStdOspfAreaBdrRtrStatus [ ] ={1,3,6,1,4,1,2076,146,1,1,1,5};
UINT4 FsMIStdOspfASBdrRtrStatus [ ] ={1,3,6,1,4,1,2076,146,1,1,1,6};
UINT4 FsMIStdOspfExternLsaCount [ ] ={1,3,6,1,4,1,2076,146,1,1,1,7};
UINT4 FsMIStdOspfExternLsaCksumSum [ ] ={1,3,6,1,4,1,2076,146,1,1,1,8};
UINT4 FsMIStdOspfTOSSupport [ ] ={1,3,6,1,4,1,2076,146,1,1,1,9};
UINT4 FsMIStdOspfOriginateNewLsas [ ] ={1,3,6,1,4,1,2076,146,1,1,1,10};
UINT4 FsMIStdOspfRxNewLsas [ ] ={1,3,6,1,4,1,2076,146,1,1,1,11};
UINT4 FsMIStdOspfExtLsdbLimit [ ] ={1,3,6,1,4,1,2076,146,1,1,1,12};
UINT4 FsMIStdOspfMulticastExtensions [ ] ={1,3,6,1,4,1,2076,146,1,1,1,13};
UINT4 FsMIStdOspfExitOverflowInterval [ ] ={1,3,6,1,4,1,2076,146,1,1,1,14};
UINT4 FsMIStdOspfDemandExtensions [ ] ={1,3,6,1,4,1,2076,146,1,1,1,15};
UINT4 FsMIStdOspfStatus [ ] ={1,3,6,1,4,1,2076,146,1,1,1,16};
UINT4 FsMIStdOspfAreaId [ ] ={1,3,6,1,4,1,2076,146,2,1,1};
UINT4 FsMIStdOspfImportAsExtern [ ] ={1,3,6,1,4,1,2076,146,2,1,3};
UINT4 FsMIStdOspfSpfRuns [ ] ={1,3,6,1,4,1,2076,146,2,1,4};
UINT4 FsMIStdOspfAreaBdrRtrCount [ ] ={1,3,6,1,4,1,2076,146,2,1,5};
UINT4 FsMIStdOspfAsBdrRtrCount [ ] ={1,3,6,1,4,1,2076,146,2,1,6};
UINT4 FsMIStdOspfAreaLsaCount [ ] ={1,3,6,1,4,1,2076,146,2,1,7};
UINT4 FsMIStdOspfAreaLsaCksumSum [ ] ={1,3,6,1,4,1,2076,146,2,1,8};
UINT4 FsMIStdOspfAreaSummary [ ] ={1,3,6,1,4,1,2076,146,2,1,9};
UINT4 FsMIStdOspfAreaStatus [ ] ={1,3,6,1,4,1,2076,146,2,1,10};
UINT4 FsMIStdOspfStubAreaId [ ] ={1,3,6,1,4,1,2076,146,3,1,1};
UINT4 FsMIStdOspfStubTOS [ ] ={1,3,6,1,4,1,2076,146,3,1,2};
UINT4 FsMIStdOspfStubMetric [ ] ={1,3,6,1,4,1,2076,146,3,1,3};
UINT4 FsMIStdOspfStubStatus [ ] ={1,3,6,1,4,1,2076,146,3,1,4};
UINT4 FsMIStdOspfStubMetricType [ ] ={1,3,6,1,4,1,2076,146,3,1,5};
UINT4 FsMIStdOspfLsdbAreaId [ ] ={1,3,6,1,4,1,2076,146,4,1,1};
UINT4 FsMIStdOspfLsdbType [ ] ={1,3,6,1,4,1,2076,146,4,1,2};
UINT4 FsMIStdOspfLsdbLsid [ ] ={1,3,6,1,4,1,2076,146,4,1,3};
UINT4 FsMIStdOspfLsdbRouterId [ ] ={1,3,6,1,4,1,2076,146,4,1,4};
UINT4 FsMIStdOspfLsdbSequence [ ] ={1,3,6,1,4,1,2076,146,4,1,5};
UINT4 FsMIStdOspfLsdbAge [ ] ={1,3,6,1,4,1,2076,146,4,1,6};
UINT4 FsMIStdOspfLsdbChecksum [ ] ={1,3,6,1,4,1,2076,146,4,1,7};
UINT4 FsMIStdOspfLsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,146,4,1,8};
UINT4 FsMIStdOspfHostIpAddress [ ] ={1,3,6,1,4,1,2076,146,5,1,1};
UINT4 FsMIStdOspfHostTOS [ ] ={1,3,6,1,4,1,2076,146,5,1,2};
UINT4 FsMIStdOspfHostMetric [ ] ={1,3,6,1,4,1,2076,146,5,1,3};
UINT4 FsMIStdOspfHostStatus [ ] ={1,3,6,1,4,1,2076,146,5,1,4};
UINT4 FsMIStdOspfHostAreaID [ ] ={1,3,6,1,4,1,2076,146,5,1,5};
UINT4 FsMIStdOspfIfIpAddress [ ] ={1,3,6,1,4,1,2076,146,6,1,1};
UINT4 FsMIStdOspfAddressLessIf [ ] ={1,3,6,1,4,1,2076,146,6,1,2};
UINT4 FsMIStdOspfIfAreaId [ ] ={1,3,6,1,4,1,2076,146,6,1,3};
UINT4 FsMIStdOspfIfType [ ] ={1,3,6,1,4,1,2076,146,6,1,4};
UINT4 FsMIStdOspfIfAdminStat [ ] ={1,3,6,1,4,1,2076,146,6,1,5};
UINT4 FsMIStdOspfIfRtrPriority [ ] ={1,3,6,1,4,1,2076,146,6,1,6};
UINT4 FsMIStdOspfIfTransitDelay [ ] ={1,3,6,1,4,1,2076,146,6,1,7};
UINT4 FsMIStdOspfIfRetransInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,8};
UINT4 FsMIStdOspfIfHelloInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,9};
UINT4 FsMIStdOspfIfRtrDeadInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,10};
UINT4 FsMIStdOspfIfPollInterval [ ] ={1,3,6,1,4,1,2076,146,6,1,11};
UINT4 FsMIStdOspfIfState [ ] ={1,3,6,1,4,1,2076,146,6,1,12};
UINT4 FsMIStdOspfIfDesignatedRouter [ ] ={1,3,6,1,4,1,2076,146,6,1,13};
UINT4 FsMIStdOspfIfBackupDesignatedRouter [ ] ={1,3,6,1,4,1,2076,146,6,1,14};
UINT4 FsMIStdOspfIfEvents [ ] ={1,3,6,1,4,1,2076,146,6,1,15};
UINT4 FsMIStdOspfIfAuthKey [ ] ={1,3,6,1,4,1,2076,146,6,1,16};
UINT4 FsMIStdOspfIfStatus [ ] ={1,3,6,1,4,1,2076,146,6,1,17};
UINT4 FsMIStdOspfIfMulticastForwarding [ ] ={1,3,6,1,4,1,2076,146,6,1,18};
UINT4 FsMIStdOspfIfDemand [ ] ={1,3,6,1,4,1,2076,146,6,1,19};
UINT4 FsMIStdOspfIfAuthType [ ] ={1,3,6,1,4,1,2076,146,6,1,20};
UINT4 FsMIStdOspfIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,146,6,1,21};
UINT4 FsMIStdOspfIfMetricIpAddress [ ] ={1,3,6,1,4,1,2076,146,7,1,1};
UINT4 FsMIStdOspfIfMetricAddressLessIf [ ] ={1,3,6,1,4,1,2076,146,7,1,2};
UINT4 FsMIStdOspfIfMetricTOS [ ] ={1,3,6,1,4,1,2076,146,7,1,3};
UINT4 FsMIStdOspfIfMetricValue [ ] ={1,3,6,1,4,1,2076,146,7,1,4};
UINT4 FsMIStdOspfIfMetricStatus [ ] ={1,3,6,1,4,1,2076,146,7,1,5};
UINT4 FsMIStdOspfVirtIfAreaId [ ] ={1,3,6,1,4,1,2076,146,8,1,1};
UINT4 FsMIStdOspfVirtIfNeighbor [ ] ={1,3,6,1,4,1,2076,146,8,1,2};
UINT4 FsMIStdOspfVirtIfTransitDelay [ ] ={1,3,6,1,4,1,2076,146,8,1,3};
UINT4 FsMIStdOspfVirtIfRetransInterval [ ] ={1,3,6,1,4,1,2076,146,8,1,4};
UINT4 FsMIStdOspfVirtIfHelloInterval [ ] ={1,3,6,1,4,1,2076,146,8,1,5};
UINT4 FsMIStdOspfVirtIfRtrDeadInterval [ ] ={1,3,6,1,4,1,2076,146,8,1,6};
UINT4 FsMIStdOspfVirtIfState [ ] ={1,3,6,1,4,1,2076,146,8,1,7};
UINT4 FsMIStdOspfVirtIfEvents [ ] ={1,3,6,1,4,1,2076,146,8,1,8};
UINT4 FsMIStdOspfVirtIfAuthKey [ ] ={1,3,6,1,4,1,2076,146,8,1,9};
UINT4 FsMIStdOspfVirtIfStatus [ ] ={1,3,6,1,4,1,2076,146,8,1,10};
UINT4 FsMIStdOspfVirtIfAuthType [ ] ={1,3,6,1,4,1,2076,146,8,1,11};
UINT4 FsMIStdOspfVirtIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,146,8,1,12};
UINT4 FsMIStdOspfNbrIpAddr [ ] ={1,3,6,1,4,1,2076,146,9,1,1};
UINT4 FsMIStdOspfNbrAddressLessIndex [ ] ={1,3,6,1,4,1,2076,146,9,1,2};
UINT4 FsMIStdOspfNbrRtrId [ ] ={1,3,6,1,4,1,2076,146,9,1,3};
UINT4 FsMIStdOspfNbrOptions [ ] ={1,3,6,1,4,1,2076,146,9,1,4};
UINT4 FsMIStdOspfNbrPriority [ ] ={1,3,6,1,4,1,2076,146,9,1,5};
UINT4 FsMIStdOspfNbrState [ ] ={1,3,6,1,4,1,2076,146,9,1,6};
UINT4 FsMIStdOspfNbrEvents [ ] ={1,3,6,1,4,1,2076,146,9,1,7};
UINT4 FsMIStdOspfNbrLsRetransQLen [ ] ={1,3,6,1,4,1,2076,146,9,1,8};
UINT4 FsMIStdOspfNbmaNbrStatus [ ] ={1,3,6,1,4,1,2076,146,9,1,9};
UINT4 FsMIStdOspfNbmaNbrPermanence [ ] ={1,3,6,1,4,1,2076,146,9,1,10};
UINT4 FsMIStdOspfNbrHelloSuppressed [ ] ={1,3,6,1,4,1,2076,146,9,1,11};
UINT4 FsMIStdOspfVirtNbrArea [ ] ={1,3,6,1,4,1,2076,146,10,1,1};
UINT4 FsMIStdOspfVirtNbrRtrId [ ] ={1,3,6,1,4,1,2076,146,10,1,2};
UINT4 FsMIStdOspfVirtNbrIpAddr [ ] ={1,3,6,1,4,1,2076,146,10,1,3};
UINT4 FsMIStdOspfVirtNbrOptions [ ] ={1,3,6,1,4,1,2076,146,10,1,4};
UINT4 FsMIStdOspfVirtNbrState [ ] ={1,3,6,1,4,1,2076,146,10,1,5};
UINT4 FsMIStdOspfVirtNbrEvents [ ] ={1,3,6,1,4,1,2076,146,10,1,6};
UINT4 FsMIStdOspfVirtNbrLsRetransQLen [ ] ={1,3,6,1,4,1,2076,146,10,1,7};
UINT4 FsMIStdOspfVirtNbrHelloSuppressed [ ] ={1,3,6,1,4,1,2076,146,10,1,8};
UINT4 FsMIStdOspfExtLsdbType [ ] ={1,3,6,1,4,1,2076,146,11,1,1};
UINT4 FsMIStdOspfExtLsdbLsid [ ] ={1,3,6,1,4,1,2076,146,11,1,2};
UINT4 FsMIStdOspfExtLsdbRouterId [ ] ={1,3,6,1,4,1,2076,146,11,1,3};
UINT4 FsMIStdOspfExtLsdbSequence [ ] ={1,3,6,1,4,1,2076,146,11,1,4};
UINT4 FsMIStdOspfExtLsdbAge [ ] ={1,3,6,1,4,1,2076,146,11,1,5};
UINT4 FsMIStdOspfExtLsdbChecksum [ ] ={1,3,6,1,4,1,2076,146,11,1,6};
UINT4 FsMIStdOspfExtLsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,146,11,1,7};
UINT4 FsMIStdOspfAreaAggregateAreaID [ ] ={1,3,6,1,4,1,2076,146,13,1,1};
UINT4 FsMIStdOspfAreaAggregateLsdbType [ ] ={1,3,6,1,4,1,2076,146,13,1,2};
UINT4 FsMIStdOspfAreaAggregateNet [ ] ={1,3,6,1,4,1,2076,146,13,1,3};
UINT4 FsMIStdOspfAreaAggregateMask [ ] ={1,3,6,1,4,1,2076,146,13,1,4};
UINT4 FsMIStdOspfAreaAggregateStatus [ ] ={1,3,6,1,4,1,2076,146,13,1,5};
UINT4 FsMIStdOspfAreaAggregateEffect [ ] ={1,3,6,1,4,1,2076,146,13,1,6};


tMbDbEntry fsmistMibEntry[]= {

{{12,FsMIStdOspfContextId}, GetNextIndexFsMIStdOspfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfRouterId}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfRouterIdGet, FsMIStdOspfRouterIdSet, FsMIStdOspfRouterIdTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, "0"},

{{12,FsMIStdOspfAdminStat}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfAdminStatGet, FsMIStdOspfAdminStatSet, FsMIStdOspfAdminStatTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfVersionNumber}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfVersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfAreaBdrRtrStatus}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfAreaBdrRtrStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfASBdrRtrStatus}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfASBdrRtrStatusGet, FsMIStdOspfASBdrRtrStatusSet, FsMIStdOspfASBdrRtrStatusTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfExternLsaCount}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfExternLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfExternLsaCksumSum}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfExternLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfTOSSupport}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfTOSSupportGet, FsMIStdOspfTOSSupportSet, FsMIStdOspfTOSSupportTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfOriginateNewLsas}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfOriginateNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfRxNewLsas}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfRxNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfExtLsdbLimit}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfExtLsdbLimitGet, FsMIStdOspfExtLsdbLimitSet, FsMIStdOspfExtLsdbLimitTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, "-1"},

{{12,FsMIStdOspfMulticastExtensions}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfMulticastExtensionsGet, FsMIStdOspfMulticastExtensionsSet, FsMIStdOspfMulticastExtensionsTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, "0"},

{{12,FsMIStdOspfExitOverflowInterval}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfExitOverflowIntervalGet, FsMIStdOspfExitOverflowIntervalSet, FsMIStdOspfExitOverflowIntervalTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, "0"},

{{12,FsMIStdOspfDemandExtensions}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfDemandExtensionsGet, FsMIStdOspfDemandExtensionsSet, FsMIStdOspfDemandExtensionsTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdOspfStatus}, GetNextIndexFsMIStdOspfTable, FsMIStdOspfStatusGet, FsMIStdOspfStatusSet, FsMIStdOspfStatusTest, FsMIStdOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfTableINDEX, 1, 0, 1, NULL},

{{11,FsMIStdOspfAreaId}, GetNextIndexFsMIStdOspfAreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdOspfImportAsExtern}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfImportAsExternGet, FsMIStdOspfImportAsExternSet, FsMIStdOspfImportAsExternTest, FsMIStdOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfAreaTableINDEX, 2, 0, 0, "1"},

{{11,FsMIStdOspfSpfRuns}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfSpfRunsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdOspfAreaBdrRtrCount}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfAreaBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdOspfAsBdrRtrCount}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfAsBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdOspfAreaLsaCount}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfAreaLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdOspfAreaLsaCksumSum}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfAreaLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfAreaTableINDEX, 2, 0, 0, "0"},

{{11,FsMIStdOspfAreaSummary}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfAreaSummaryGet, FsMIStdOspfAreaSummarySet, FsMIStdOspfAreaSummaryTest, FsMIStdOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfAreaTableINDEX, 2, 0, 0, "1"},

{{11,FsMIStdOspfAreaStatus}, GetNextIndexFsMIStdOspfAreaTable, FsMIStdOspfAreaStatusGet, FsMIStdOspfAreaStatusSet, FsMIStdOspfAreaStatusTest, FsMIStdOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfAreaTableINDEX, 2, 0, 1, NULL},

{{11,FsMIStdOspfStubAreaId}, GetNextIndexFsMIStdOspfStubAreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfStubAreaTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfStubTOS}, GetNextIndexFsMIStdOspfStubAreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfStubAreaTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfStubMetric}, GetNextIndexFsMIStdOspfStubAreaTable, FsMIStdOspfStubMetricGet, FsMIStdOspfStubMetricSet, FsMIStdOspfStubMetricTest, FsMIStdOspfStubAreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfStubAreaTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfStubStatus}, GetNextIndexFsMIStdOspfStubAreaTable, FsMIStdOspfStubStatusGet, FsMIStdOspfStubStatusSet, FsMIStdOspfStubStatusTest, FsMIStdOspfStubAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfStubAreaTableINDEX, 3, 0, 1, NULL},

{{11,FsMIStdOspfStubMetricType}, GetNextIndexFsMIStdOspfStubAreaTable, FsMIStdOspfStubMetricTypeGet, FsMIStdOspfStubMetricTypeSet, FsMIStdOspfStubMetricTypeTest, FsMIStdOspfStubAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfStubAreaTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfLsdbAreaId}, GetNextIndexFsMIStdOspfLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbType}, GetNextIndexFsMIStdOspfLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbLsid}, GetNextIndexFsMIStdOspfLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbRouterId}, GetNextIndexFsMIStdOspfLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbSequence}, GetNextIndexFsMIStdOspfLsdbTable, FsMIStdOspfLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbAge}, GetNextIndexFsMIStdOspfLsdbTable, FsMIStdOspfLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbChecksum}, GetNextIndexFsMIStdOspfLsdbTable, FsMIStdOspfLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfLsdbAdvertisement}, GetNextIndexFsMIStdOspfLsdbTable, FsMIStdOspfLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfLsdbTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfHostIpAddress}, GetNextIndexFsMIStdOspfHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfHostTOS}, GetNextIndexFsMIStdOspfHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfHostMetric}, GetNextIndexFsMIStdOspfHostTable, FsMIStdOspfHostMetricGet, FsMIStdOspfHostMetricSet, FsMIStdOspfHostMetricTest, FsMIStdOspfHostTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfHostStatus}, GetNextIndexFsMIStdOspfHostTable, FsMIStdOspfHostStatusGet, FsMIStdOspfHostStatusSet, FsMIStdOspfHostStatusTest, FsMIStdOspfHostTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfHostTableINDEX, 3, 0, 1, NULL},

{{11,FsMIStdOspfHostAreaID}, GetNextIndexFsMIStdOspfHostTable, FsMIStdOspfHostAreaIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfIfIpAddress}, GetNextIndexFsMIStdOspfIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfAddressLessIf}, GetNextIndexFsMIStdOspfIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfIfAreaId}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfAreaIdGet, FsMIStdOspfIfAreaIdSet, FsMIStdOspfIfAreaIdTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfIfType}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfTypeGet, FsMIStdOspfIfTypeSet, FsMIStdOspfIfTypeTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfIfAdminStat}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfAdminStatGet, FsMIStdOspfIfAdminStatSet, FsMIStdOspfIfAdminStatTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfIfRtrPriority}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfRtrPriorityGet, FsMIStdOspfIfRtrPrioritySet, FsMIStdOspfIfRtrPriorityTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfIfTransitDelay}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfTransitDelayGet, FsMIStdOspfIfTransitDelaySet, FsMIStdOspfIfTransitDelayTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfIfRetransInterval}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfRetransIntervalGet, FsMIStdOspfIfRetransIntervalSet, FsMIStdOspfIfRetransIntervalTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "5"},

{{11,FsMIStdOspfIfHelloInterval}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfHelloIntervalGet, FsMIStdOspfIfHelloIntervalSet, FsMIStdOspfIfHelloIntervalTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "10"},

{{11,FsMIStdOspfIfRtrDeadInterval}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfRtrDeadIntervalGet, FsMIStdOspfIfRtrDeadIntervalSet, FsMIStdOspfIfRtrDeadIntervalTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "40"},

{{11,FsMIStdOspfIfPollInterval}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfPollIntervalGet, FsMIStdOspfIfPollIntervalSet, FsMIStdOspfIfPollIntervalTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "120"},

{{11,FsMIStdOspfIfState}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfIfDesignatedRouter}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfIfTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfIfBackupDesignatedRouter}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfBackupDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfIfTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfIfEvents}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfIfAuthKey}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfAuthKeyGet, FsMIStdOspfIfAuthKeySet, FsMIStdOspfIfAuthKeyTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfIfStatus}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfStatusGet, FsMIStdOspfIfStatusSet, FsMIStdOspfIfStatusTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 1, NULL},

{{11,FsMIStdOspfIfMulticastForwarding}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfMulticastForwardingGet, FsMIStdOspfIfMulticastForwardingSet, FsMIStdOspfIfMulticastForwardingTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfIfDemand}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfDemandGet, FsMIStdOspfIfDemandSet, FsMIStdOspfIfDemandTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "2"},

{{11,FsMIStdOspfIfAuthType}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfAuthTypeGet, FsMIStdOspfIfAuthTypeSet, FsMIStdOspfIfAuthTypeTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfIfCryptoAuthType}, GetNextIndexFsMIStdOspfIfTable, FsMIStdOspfIfCryptoAuthTypeGet, FsMIStdOspfIfCryptoAuthTypeSet, FsMIStdOspfIfCryptoAuthTypeTest, FsMIStdOspfIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfIfMetricIpAddress}, GetNextIndexFsMIStdOspfIfMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfIfMetricTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfIfMetricAddressLessIf}, GetNextIndexFsMIStdOspfIfMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfIfMetricTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfIfMetricTOS}, GetNextIndexFsMIStdOspfIfMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfIfMetricTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfIfMetricValue}, GetNextIndexFsMIStdOspfIfMetricTable, FsMIStdOspfIfMetricValueGet, FsMIStdOspfIfMetricValueSet, FsMIStdOspfIfMetricValueTest, FsMIStdOspfIfMetricTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfIfMetricTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfIfMetricStatus}, GetNextIndexFsMIStdOspfIfMetricTable, FsMIStdOspfIfMetricStatusGet, FsMIStdOspfIfMetricStatusSet, FsMIStdOspfIfMetricStatusTest, FsMIStdOspfIfMetricTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfIfMetricTableINDEX, 4, 0, 1, NULL},

{{11,FsMIStdOspfVirtIfAreaId}, GetNextIndexFsMIStdOspfVirtIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtIfNeighbor}, GetNextIndexFsMIStdOspfVirtIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtIfTransitDelay}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfTransitDelayGet, FsMIStdOspfVirtIfTransitDelaySet, FsMIStdOspfVirtIfTransitDelayTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfVirtIfRetransInterval}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfRetransIntervalGet, FsMIStdOspfVirtIfRetransIntervalSet, FsMIStdOspfVirtIfRetransIntervalTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "5"},

{{11,FsMIStdOspfVirtIfHelloInterval}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfHelloIntervalGet, FsMIStdOspfVirtIfHelloIntervalSet, FsMIStdOspfVirtIfHelloIntervalTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "10"},

{{11,FsMIStdOspfVirtIfRtrDeadInterval}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfRtrDeadIntervalGet, FsMIStdOspfVirtIfRtrDeadIntervalSet, FsMIStdOspfVirtIfRtrDeadIntervalTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "60"},

{{11,FsMIStdOspfVirtIfState}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfVirtIfEvents}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtIfAuthKey}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfAuthKeyGet, FsMIStdOspfVirtIfAuthKeySet, FsMIStdOspfVirtIfAuthKeyTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfVirtIfStatus}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfStatusGet, FsMIStdOspfVirtIfStatusSet, FsMIStdOspfVirtIfStatusTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 1, NULL},

{{11,FsMIStdOspfVirtIfAuthType}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfAuthTypeGet, FsMIStdOspfVirtIfAuthTypeSet, FsMIStdOspfVirtIfAuthTypeTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "0"},


{{11,FsMIStdOspfVirtIfCryptoAuthType}, GetNextIndexFsMIStdOspfVirtIfTable, FsMIStdOspfVirtIfCryptoAuthTypeGet, FsMIStdOspfVirtIfCryptoAuthTypeSet, FsMIStdOspfVirtIfCryptoAuthTypeTest, FsMIStdOspfVirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfVirtIfTableINDEX, 3, 0, 0, "1"},


{{11,FsMIStdOspfNbrIpAddr}, GetNextIndexFsMIStdOspfNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfNbrAddressLessIndex}, GetNextIndexFsMIStdOspfNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfNbrRtrId}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrRtrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfNbrOptions}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, "0"},

{{11,FsMIStdOspfNbrPriority}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrPriorityGet, FsMIStdOspfNbrPrioritySet, FsMIStdOspfNbrPriorityTest, FsMIStdOspfNbrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfNbrTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfNbrState}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, "1"},

{{11,FsMIStdOspfNbrEvents}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfNbrLsRetransQLen}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfNbmaNbrStatus}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbmaNbrStatusGet, FsMIStdOspfNbmaNbrStatusSet, FsMIStdOspfNbmaNbrStatusTest, FsMIStdOspfNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfNbrTableINDEX, 3, 0, 1, NULL},

{{11,FsMIStdOspfNbmaNbrPermanence}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbmaNbrPermanenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, "2"},

{{11,FsMIStdOspfNbrHelloSuppressed}, GetNextIndexFsMIStdOspfNbrTable, FsMIStdOspfNbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrArea}, GetNextIndexFsMIStdOspfVirtNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrRtrId}, GetNextIndexFsMIStdOspfVirtNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrIpAddr}, GetNextIndexFsMIStdOspfVirtNbrTable, FsMIStdOspfVirtNbrIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrOptions}, GetNextIndexFsMIStdOspfVirtNbrTable, FsMIStdOspfVirtNbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrState}, GetNextIndexFsMIStdOspfVirtNbrTable, FsMIStdOspfVirtNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrEvents}, GetNextIndexFsMIStdOspfVirtNbrTable, FsMIStdOspfVirtNbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrLsRetransQLen}, GetNextIndexFsMIStdOspfVirtNbrTable, FsMIStdOspfVirtNbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfVirtNbrHelloSuppressed}, GetNextIndexFsMIStdOspfVirtNbrTable, FsMIStdOspfVirtNbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbType}, GetNextIndexFsMIStdOspfExtLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbLsid}, GetNextIndexFsMIStdOspfExtLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbRouterId}, GetNextIndexFsMIStdOspfExtLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbSequence}, GetNextIndexFsMIStdOspfExtLsdbTable, FsMIStdOspfExtLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbAge}, GetNextIndexFsMIStdOspfExtLsdbTable, FsMIStdOspfExtLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbChecksum}, GetNextIndexFsMIStdOspfExtLsdbTable, FsMIStdOspfExtLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfExtLsdbAdvertisement}, GetNextIndexFsMIStdOspfExtLsdbTable, FsMIStdOspfExtLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfExtLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FsMIStdOspfAreaAggregateAreaID}, GetNextIndexFsMIStdOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfAreaAggregateLsdbType}, GetNextIndexFsMIStdOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfAreaAggregateNet}, GetNextIndexFsMIStdOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfAreaAggregateMask}, GetNextIndexFsMIStdOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIStdOspfAreaAggregateStatus}, GetNextIndexFsMIStdOspfAreaAggregateTable, FsMIStdOspfAreaAggregateStatusGet, FsMIStdOspfAreaAggregateStatusSet, FsMIStdOspfAreaAggregateStatusTest, FsMIStdOspfAreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfAreaAggregateTableINDEX, 5, 0, 1, NULL},

{{11,FsMIStdOspfAreaAggregateEffect}, GetNextIndexFsMIStdOspfAreaAggregateTable, FsMIStdOspfAreaAggregateEffectGet, FsMIStdOspfAreaAggregateEffectSet, FsMIStdOspfAreaAggregateEffectTest, FsMIStdOspfAreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfAreaAggregateTableINDEX, 5, 0, 0, "1"},
};
tMibData fsmistEntry = { 113, fsmistMibEntry };
#endif /* _FSMIOSSTDB_H */

