/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiosdb.h,v 1.18 2016/05/11 11:38:50 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIOSDB_H
#define _FSMIOSDB_H

UINT1 FsMIOspfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfAreaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfHostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfIfMD5AuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfVirtIfMD5AuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfRoutingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfSecIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfAreaAggregateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfAsExternalAggregationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfOpaqueTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfOpaqueInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfType9LsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfType11LsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfAppInfoDbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfRRDRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfRRDRouteConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfRRDMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfVirtNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfDistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfPreferenceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfIfAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfVirtIfAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};


UINT4 fsmios [] ={1,3,6,1,4,1,2076,145};
tSNMP_OID_TYPE fsmiosOID = {8, fsmios};


UINT4 FsMIOspfGlobalTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,1};
UINT4 FsMIOspfVrfSpfInterval [ ] ={1,3,6,1,4,1,2076,145,1,2};
UINT4 FsMIOspfOverFlowState [ ] ={1,3,6,1,4,1,2076,145,1,3,1,1};
UINT4 FsMIOspfPktsRcvd [ ] ={1,3,6,1,4,1,2076,145,1,3,1,2};
UINT4 FsMIOspfPktsTxed [ ] ={1,3,6,1,4,1,2076,145,1,3,1,3};
UINT4 FsMIOspfPktsDisd [ ] ={1,3,6,1,4,1,2076,145,1,3,1,4};
UINT4 FsMIOspfRFC1583Compatibility [ ] ={1,3,6,1,4,1,2076,145,1,3,1,5};
UINT4 FsMIOspfTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,3,1,12};
UINT4 FsMIOspfMinLsaInterval [ ] ={1,3,6,1,4,1,2076,145,1,3,1,13};
UINT4 FsMIOspfABRType [ ] ={1,3,6,1,4,1,2076,145,1,3,1,14};
UINT4 FsMIOspfNssaAsbrDefRtTrans [ ] ={1,3,6,1,4,1,2076,145,1,3,1,15};
UINT4 FsMIOspfDefaultPassiveInterface [ ] ={1,3,6,1,4,1,2076,145,1,3,1,16};
UINT4 FsMIOspfSpfHoldtime [ ] ={1,3,6,1,4,1,2076,145,1,3,1,17};
UINT4 FsMIOspfSpfDelay [ ] ={1,3,6,1,4,1,2076,145,1,3,1,18};
UINT4 FsMIOspfRestartSupport [ ] ={1,3,6,1,4,1,2076,145,1,3,1,19};
UINT4 FsMIOspfRestartInterval [ ] ={1,3,6,1,4,1,2076,145,1,3,1,20};
UINT4 FsMIOspfRestartStrictLsaChecking [ ] ={1,3,6,1,4,1,2076,145,1,3,1,21};
UINT4 FsMIOspfRestartStatus [ ] ={1,3,6,1,4,1,2076,145,1,3,1,22};
UINT4 FsMIOspfRestartAge [ ] ={1,3,6,1,4,1,2076,145,1,3,1,23};
UINT4 FsMIOspfRestartExitReason [ ] ={1,3,6,1,4,1,2076,145,1,3,1,24};
UINT4 FsMIOspfHelperSupport [ ] ={1,3,6,1,4,1,2076,145,1,3,1,25};
UINT4 FsMIOspfExtTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,3,1,26};
UINT4 FsMIOspfHelperGraceTimeLimit [ ] ={1,3,6,1,4,1,2076,145,1,3,1,27};
UINT4 FsMIOspfRestartAckState [ ] ={1,3,6,1,4,1,2076,145,1,3,1,28};
UINT4 FsMIOspfGraceLsaRetransmitCount [ ] ={1,3,6,1,4,1,2076,145,1,3,1,29};
UINT4 FsMIOspfRestartReason [ ] ={1,3,6,1,4,1,2076,145,1,3,1,30};
UINT4 FsMIOspfRTStaggeringInterval [ ] ={1,3,6,1,4,1,2076,145,1,3,1,31};
UINT4 FsMIOspfRouterIdPermanence [ ] ={1,3,6,1,4,1,2076,145,1,3,1,32};
UINT4 FsMIOspfBfdStatus [ ] ={1,3,6,1,4,1,2076,145,1,3,1,33};
UINT4 FsMIOspfBfdAllIfState [ ] ={1,3,6,1,4,1,2076,145,1,3,1,34};
UINT4 FsMIOspfRTStaggeringStatus [ ] ={1,3,6,1,4,1,2076,145,1,4};
UINT4 FsMIOspfHotStandbyAdminStatus [ ] ={1,3,6,1,4,1,2076,145,1,5};
UINT4 FsMIOspfHotStandbyState [ ] ={1,3,6,1,4,1,2076,145,1,6};
UINT4 FsMIOspfDynamicBulkUpdStatus [ ] ={1,3,6,1,4,1,2076,145,1,7};
UINT4 FsMIOspfStanbyHelloSyncCount [ ] ={1,3,6,1,4,1,2076,145,1,8};
UINT4 FsMIOspfStanbyLsaSyncCount [ ] ={1,3,6,1,4,1,2076,145,1,9};
UINT4 FsMIOspfGlobalExtTraceLevel [ ] ={1,3,6,1,4,1,2076,145,1,10};
UINT4 FsMIOspfAreaId [ ] ={1,3,6,1,4,1,2076,145,2,1,1};
UINT4 FsMIOspfAreaIfCount [ ] ={1,3,6,1,4,1,2076,145,2,1,2};
UINT4 FsMIOspfAreaNetCount [ ] ={1,3,6,1,4,1,2076,145,2,1,3};
UINT4 FsMIOspfAreaRtrCount [ ] ={1,3,6,1,4,1,2076,145,2,1,4};
UINT4 FsMIOspfAreaNSSATranslatorRole [ ] ={1,3,6,1,4,1,2076,145,2,1,5};
UINT4 FsMIOspfAreaNSSATranslatorState [ ] ={1,3,6,1,4,1,2076,145,2,1,6};
UINT4 FsMIOspfAreaNSSATranslatorStabilityInterval [ ] ={1,3,6,1,4,1,2076,145,2,1,7};
UINT4 FsMIOspfAreaNSSATranslatorEvents [ ] ={1,3,6,1,4,1,2076,145,2,1,8};
UINT4 FsMIOspfAreaDfInfOriginate [ ] ={1,3,6,1,4,1,2076,145,2,1,9};
UINT4 FsMIOspfHostIpAddress [ ] ={1,3,6,1,4,1,2076,145,3,1,1};
UINT4 FsMIOspfHostTOS [ ] ={1,3,6,1,4,1,2076,145,3,1,2};
UINT4 FsMIOspfHostRouteIfIndex [ ] ={1,3,6,1,4,1,2076,145,3,1,3};
UINT4 FsMIOspfIfIpAddress [ ] ={1,3,6,1,4,1,2076,145,4,1,1};
UINT4 FsMIOspfAddressLessIf [ ] ={1,3,6,1,4,1,2076,145,4,1,2};
UINT4 FsMIOspfIfOperState [ ] ={1,3,6,1,4,1,2076,145,4,1,3};
UINT4 FsMIOspfIfPassive [ ] ={1,3,6,1,4,1,2076,145,4,1,4};
UINT4 FsMIOspfIfNbrCount [ ] ={1,3,6,1,4,1,2076,145,4,1,5};
UINT4 FsMIOspfIfAdjCount [ ] ={1,3,6,1,4,1,2076,145,4,1,6};
UINT4 FsMIOspfIfHelloRcvd [ ] ={1,3,6,1,4,1,2076,145,4,1,7};
UINT4 FsMIOspfIfHelloTxed [ ] ={1,3,6,1,4,1,2076,145,4,1,8};
UINT4 FsMIOspfIfHelloDisd [ ] ={1,3,6,1,4,1,2076,145,4,1,9};
UINT4 FsMIOspfIfDdpRcvd [ ] ={1,3,6,1,4,1,2076,145,4,1,10};
UINT4 FsMIOspfIfDdpTxed [ ] ={1,3,6,1,4,1,2076,145,4,1,11};
UINT4 FsMIOspfIfDdpDisd [ ] ={1,3,6,1,4,1,2076,145,4,1,12};
UINT4 FsMIOspfIfLrqRcvd [ ] ={1,3,6,1,4,1,2076,145,4,1,13};
UINT4 FsMIOspfIfLrqTxed [ ] ={1,3,6,1,4,1,2076,145,4,1,14};
UINT4 FsMIOspfIfLrqDisd [ ] ={1,3,6,1,4,1,2076,145,4,1,15};
UINT4 FsMIOspfIfLsuRcvd [ ] ={1,3,6,1,4,1,2076,145,4,1,16};
UINT4 FsMIOspfIfLsuTxed [ ] ={1,3,6,1,4,1,2076,145,4,1,17};
UINT4 FsMIOspfIfLsuDisd [ ] ={1,3,6,1,4,1,2076,145,4,1,18};
UINT4 FsMIOspfIfLakRcvd [ ] ={1,3,6,1,4,1,2076,145,4,1,19};
UINT4 FsMIOspfIfLakTxed [ ] ={1,3,6,1,4,1,2076,145,4,1,20};
UINT4 FsMIOspfIfLakDisd [ ] ={1,3,6,1,4,1,2076,145,4,1,21};
UINT4 FsMIOspfIfBfdState [ ] ={1,3,6,1,4,1,2076,145,4,1,22};
UINT4 FsMIOspfIfMD5AuthIpAddress [ ] ={1,3,6,1,4,1,2076,145,5,1,1};
UINT4 FsMIOspfIfMD5AuthAddressLessIf [ ] ={1,3,6,1,4,1,2076,145,5,1,2};
UINT4 FsMIOspfIfMD5AuthKeyId [ ] ={1,3,6,1,4,1,2076,145,5,1,3};
UINT4 FsMIOspfIfMD5AuthKey [ ] ={1,3,6,1,4,1,2076,145,5,1,4};
UINT4 FsMIOspfIfMD5AuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,145,5,1,5};
UINT4 FsMIOspfIfMD5AuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,145,5,1,6};
UINT4 FsMIOspfIfMD5AuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,145,5,1,7};
UINT4 FsMIOspfIfMD5AuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,145,5,1,8};
UINT4 FsMIOspfIfMD5AuthKeyStatus [ ] ={1,3,6,1,4,1,2076,145,5,1,9};
UINT4 FsMIOspfVirtIfMD5AuthAreaId [ ] ={1,3,6,1,4,1,2076,145,6,1,1};
UINT4 FsMIOspfVirtIfMD5AuthNeighbor [ ] ={1,3,6,1,4,1,2076,145,6,1,2};
UINT4 FsMIOspfVirtIfMD5AuthKeyId [ ] ={1,3,6,1,4,1,2076,145,6,1,3};
UINT4 FsMIOspfVirtIfMD5AuthKey [ ] ={1,3,6,1,4,1,2076,145,6,1,4};
UINT4 FsMIOspfVirtIfMD5AuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,145,6,1,5};
UINT4 FsMIOspfVirtIfMD5AuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,145,6,1,6};
UINT4 FsMIOspfVirtIfMD5AuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,145,6,1,7};
UINT4 FsMIOspfVirtIfMD5AuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,145,6,1,8};
UINT4 FsMIOspfVirtIfMD5AuthKeyStatus [ ] ={1,3,6,1,4,1,2076,145,6,1,9};
UINT4 FsMIOspfNbrIpAddr [ ] ={1,3,6,1,4,1,2076,145,7,1,1};
UINT4 FsMIOspfNbrAddressLessIndex [ ] ={1,3,6,1,4,1,2076,145,7,1,2};
UINT4 FsMIOspfNbrDBSummaryQLen [ ] ={1,3,6,1,4,1,2076,145,7,1,3};
UINT4 FsMIOspfNbrLSReqQLen [ ] ={1,3,6,1,4,1,2076,145,7,1,4};
UINT4 FsMIOspfNbrRestartHelperStatus [ ] ={1,3,6,1,4,1,2076,145,7,1,5};
UINT4 FsMIOspfNbrRestartHelperAge [ ] ={1,3,6,1,4,1,2076,145,7,1,6};
UINT4 FsMIOspfNbrRestartHelperExitReason [ ] ={1,3,6,1,4,1,2076,145,7,1,7};
UINT4 FsMIOspfNbrBfdState [ ] ={1,3,6,1,4,1,2076,145,7,1,8};
UINT4 FsMIOspfRouteIpAddr [ ] ={1,3,6,1,4,1,2076,145,8,1,1};
UINT4 FsMIOspfRouteIpAddrMask [ ] ={1,3,6,1,4,1,2076,145,8,1,2};
UINT4 FsMIOspfRouteIpTos [ ] ={1,3,6,1,4,1,2076,145,8,1,3};
UINT4 FsMIOspfRouteIpNextHop [ ] ={1,3,6,1,4,1,2076,145,8,1,4};
UINT4 FsMIOspfRouteType [ ] ={1,3,6,1,4,1,2076,145,8,1,5};
UINT4 FsMIOspfRouteAreaId [ ] ={1,3,6,1,4,1,2076,145,8,1,6};
UINT4 FsMIOspfRouteCost [ ] ={1,3,6,1,4,1,2076,145,8,1,7};
UINT4 FsMIOspfRouteType2Cost [ ] ={1,3,6,1,4,1,2076,145,8,1,8};
UINT4 FsMIOspfRouteInterfaceIndex [ ] ={1,3,6,1,4,1,2076,145,8,1,9};
UINT4 FsMIOspfPrimIpAddr [ ] ={1,3,6,1,4,1,2076,145,9,1,1};
UINT4 FsMIOspfPrimAddresslessIf [ ] ={1,3,6,1,4,1,2076,145,9,1,2};
UINT4 FsMIOspfSecIpAddr [ ] ={1,3,6,1,4,1,2076,145,9,1,3};
UINT4 FsMIOspfSecIpAddrMask [ ] ={1,3,6,1,4,1,2076,145,9,1,4};
UINT4 FsMIOspfSecIfStatus [ ] ={1,3,6,1,4,1,2076,145,9,1,5};
UINT4 FsMIOspfAreaAggregateAreaID [ ] ={1,3,6,1,4,1,2076,145,10,1,1};
UINT4 FsMIOspfAreaAggregateLsdbType [ ] ={1,3,6,1,4,1,2076,145,10,1,2};
UINT4 FsMIOspfAreaAggregateNet [ ] ={1,3,6,1,4,1,2076,145,10,1,3};
UINT4 FsMIOspfAreaAggregateMask [ ] ={1,3,6,1,4,1,2076,145,10,1,4};
UINT4 FsMIOspfAreaAggregateExternalTag [ ] ={1,3,6,1,4,1,2076,145,10,1,5};
UINT4 FsMIOspfAsExternalAggregationNet [ ] ={1,3,6,1,4,1,2076,145,11,1,1};
UINT4 FsMIOspfAsExternalAggregationMask [ ] ={1,3,6,1,4,1,2076,145,11,1,2};
UINT4 FsMIOspfAsExternalAggregationAreaId [ ] ={1,3,6,1,4,1,2076,145,11,1,3};
UINT4 FsMIOspfAsExternalAggregationEffect [ ] ={1,3,6,1,4,1,2076,145,11,1,4};
UINT4 FsMIOspfAsExternalAggregationTranslation [ ] ={1,3,6,1,4,1,2076,145,11,1,5};
UINT4 FsMIOspfAsExternalAggregationStatus [ ] ={1,3,6,1,4,1,2076,145,11,1,6};
UINT4 FsMIOspfOpaqueOption [ ] ={1,3,6,1,4,1,2076,145,12,1,1,1,1};
UINT4 FsMIOspfType11LsaCount [ ] ={1,3,6,1,4,1,2076,145,12,1,1,1,2};
UINT4 FsMIOspfType11LsaCksumSum [ ] ={1,3,6,1,4,1,2076,145,12,1,1,1,3};
UINT4 FsMIOspfAreaIDValid [ ] ={1,3,6,1,4,1,2076,145,12,1,1,1,4};
UINT4 FsMIOspfOpaqueType9LsaCount [ ] ={1,3,6,1,4,1,2076,145,12,2,1,1};
UINT4 FsMIOspfOpaqueType9LsaCksumSum [ ] ={1,3,6,1,4,1,2076,145,12,2,1,2};
UINT4 FsMIOspfType9LsdbIfIpAddress [ ] ={1,3,6,1,4,1,2076,145,12,3,1,1};
UINT4 FsMIOspfType9LsdbOpaqueType [ ] ={1,3,6,1,4,1,2076,145,12,3,1,2};
UINT4 FsMIOspfType9LsdbLsid [ ] ={1,3,6,1,4,1,2076,145,12,3,1,3};
UINT4 FsMIOspfType9LsdbRouterId [ ] ={1,3,6,1,4,1,2076,145,12,3,1,4};
UINT4 FsMIOspfType9LsdbSequence [ ] ={1,3,6,1,4,1,2076,145,12,3,1,5};
UINT4 FsMIOspfType9LsdbAge [ ] ={1,3,6,1,4,1,2076,145,12,3,1,6};
UINT4 FsMIOspfType9LsdbChecksum [ ] ={1,3,6,1,4,1,2076,145,12,3,1,7};
UINT4 FsMIOspfType9LsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,145,12,3,1,8};
UINT4 FsMIOspfType11LsdbOpaqueType [ ] ={1,3,6,1,4,1,2076,145,12,4,1,1};
UINT4 FsMIOspfType11LsdbLsid [ ] ={1,3,6,1,4,1,2076,145,12,4,1,2};
UINT4 FsMIOspfType11LsdbRouterId [ ] ={1,3,6,1,4,1,2076,145,12,4,1,3};
UINT4 FsMIOspfType11LsdbSequence [ ] ={1,3,6,1,4,1,2076,145,12,4,1,4};
UINT4 FsMIOspfType11LsdbAge [ ] ={1,3,6,1,4,1,2076,145,12,4,1,5};
UINT4 FsMIOspfType11LsdbChecksum [ ] ={1,3,6,1,4,1,2076,145,12,4,1,6};
UINT4 FsMIOspfType11LsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,145,12,4,1,7};
UINT4 FsMIOspfAppInfoDbAppid [ ] ={1,3,6,1,4,1,2076,145,12,5,1,1};
UINT4 FsMIOspfAppInfoDbOpaqueType [ ] ={1,3,6,1,4,1,2076,145,12,5,1,2};
UINT4 FsMIOspfAppInfoDbLsaTypesSupported [ ] ={1,3,6,1,4,1,2076,145,12,5,1,3};
UINT4 FsMIOspfAppInfoDbType9Gen [ ] ={1,3,6,1,4,1,2076,145,12,5,1,4};
UINT4 FsMIOspfAppInfoDbType9Rcvd [ ] ={1,3,6,1,4,1,2076,145,12,5,1,5};
UINT4 FsMIOspfAppInfoDbType10Gen [ ] ={1,3,6,1,4,1,2076,145,12,5,1,6};
UINT4 FsMIOspfAppInfoDbType10Rcvd [ ] ={1,3,6,1,4,1,2076,145,12,5,1,7};
UINT4 FsMIOspfAppInfoDbType11Gen [ ] ={1,3,6,1,4,1,2076,145,12,5,1,8};
UINT4 FsMIOspfAppInfoDbType11Rcvd [ ] ={1,3,6,1,4,1,2076,145,12,5,1,9};
UINT4 FsMIOspfRRDStatus [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,1};
UINT4 FsMIOspfRRDSrcProtoMaskEnable [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,2};
UINT4 FsMIOspfRRDSrcProtoMaskDisable [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,3};
UINT4 FsMIOspfRRDRouteMapEnable [ ] ={1,3,6,1,4,1,2076,145,13,1,1,1,4};
UINT4 FsMIOspfRRDRouteDest [ ] ={1,3,6,1,4,1,2076,145,13,2,1,1};
UINT4 FsMIOspfRRDRouteMask [ ] ={1,3,6,1,4,1,2076,145,13,2,1,2};
UINT4 FsMIOspfRRDRouteMetric [ ] ={1,3,6,1,4,1,2076,145,13,2,1,3};
UINT4 FsMIOspfRRDRouteMetricType [ ] ={1,3,6,1,4,1,2076,145,13,2,1,4};
UINT4 FsMIOspfRRDRouteTagType [ ] ={1,3,6,1,4,1,2076,145,13,2,1,5};
UINT4 FsMIOspfRRDRouteTag [ ] ={1,3,6,1,4,1,2076,145,13,2,1,6};
UINT4 FsMIOspfRRDRouteStatus [ ] ={1,3,6,1,4,1,2076,145,13,2,1,7};
UINT4 FsMIOspfRRDProtocolId [ ] ={1,3,6,1,4,1,2076,145,13,3,1,1};
UINT4 FsMIOspfRRDMetricValue [ ] ={1,3,6,1,4,1,2076,145,13,3,1,2};
UINT4 FsMIOspfRRDMetricType [ ] ={1,3,6,1,4,1,2076,145,13,3,1,3};
UINT4 FsMIOspfVirtNbrRestartHelperStatus [ ] ={1,3,6,1,4,1,2076,145,14,1,1};
UINT4 FsMIOspfVirtNbrRestartHelperAge [ ] ={1,3,6,1,4,1,2076,145,14,1,2};
UINT4 FsMIOspfVirtNbrRestartHelperExitReason [ ] ={1,3,6,1,4,1,2076,145,14,1,3};
UINT4 FsMIOspfDistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,145,15,1,1,1};
UINT4 FsMIOspfDistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,145,15,1,1,3};
UINT4 FsMIOspfDistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,145,15,1,1,4};
UINT4 FsMIOspfDistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,145,15,1,1,5};
UINT4 FsMIOspfPreferenceValue [ ] ={1,3,6,1,4,1,2076,145,16,1,1,1};
UINT4 FsMIOspfIfAuthIpAddress [ ] ={1,3,6,1,4,1,2076,145,17,1,1};
UINT4 FsMIOspfIfAuthAddressLessIf [ ] ={1,3,6,1,4,1,2076,145,17,1,2};
UINT4 FsMIOspfIfAuthKeyId [ ] ={1,3,6,1,4,1,2076,145,17,1,3};
UINT4 FsMIOspfIfAuthKey [ ] ={1,3,6,1,4,1,2076,145,17,1,4};
UINT4 FsMIOspfIfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,145,17,1,5};
UINT4 FsMIOspfIfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,145,17,1,6};
UINT4 FsMIOspfIfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,145,17,1,7};
UINT4 FsMIOspfIfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,145,17,1,8};
UINT4 FsMIOspfIfAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,145,17,1,9};
UINT4 FsMIOspfVirtIfAuthAreaId [ ] ={1,3,6,1,4,1,2076,145,18,1,1};
UINT4 FsMIOspfVirtIfAuthNeighbor [ ] ={1,3,6,1,4,1,2076,145,18,1,2};
UINT4 FsMIOspfVirtIfAuthKeyId [ ] ={1,3,6,1,4,1,2076,145,18,1,3};
UINT4 FsMIOspfVirtIfAuthKey [ ] ={1,3,6,1,4,1,2076,145,18,1,4};
UINT4 FsMIOspfVirtIfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,145,18,1,5};
UINT4 FsMIOspfVirtIfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,145,18,1,6};
UINT4 FsMIOspfVirtIfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,145,18,1,7};
UINT4 FsMIOspfVirtIfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,145,18,1,8};
UINT4 FsMIOspfVirtIfAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,145,18,1,9};

tMbDbEntry fsmiosMibEntry[]= {

{{10,FsMIOspfGlobalTraceLevel}, NULL, FsMIOspfGlobalTraceLevelGet, FsMIOspfGlobalTraceLevelSet, FsMIOspfGlobalTraceLevelTest, FsMIOspfGlobalTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIOspfVrfSpfInterval}, NULL, FsMIOspfVrfSpfIntervalGet, FsMIOspfVrfSpfIntervalSet, FsMIOspfVrfSpfIntervalTest, FsMIOspfVrfSpfIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{12,FsMIOspfOverFlowState}, GetNextIndexFsMIOspfTable, FsMIOspfOverFlowStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfPktsRcvd}, GetNextIndexFsMIOspfTable, FsMIOspfPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfPktsTxed}, GetNextIndexFsMIOspfTable, FsMIOspfPktsTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfPktsDisd}, GetNextIndexFsMIOspfTable, FsMIOspfPktsDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfRFC1583Compatibility}, GetNextIndexFsMIOspfTable, FsMIOspfRFC1583CompatibilityGet, FsMIOspfRFC1583CompatibilitySet, FsMIOspfRFC1583CompatibilityTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfTraceLevel}, GetNextIndexFsMIOspfTable, FsMIOspfTraceLevelGet, FsMIOspfTraceLevelSet, FsMIOspfTraceLevelTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2048"},

{{12,FsMIOspfMinLsaInterval}, GetNextIndexFsMIOspfTable, FsMIOspfMinLsaIntervalGet, FsMIOspfMinLsaIntervalSet, FsMIOspfMinLsaIntervalTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "5"},

{{12,FsMIOspfABRType}, GetNextIndexFsMIOspfTable, FsMIOspfABRTypeGet, FsMIOspfABRTypeSet, FsMIOspfABRTypeTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfNssaAsbrDefRtTrans}, GetNextIndexFsMIOspfTable, FsMIOspfNssaAsbrDefRtTransGet, FsMIOspfNssaAsbrDefRtTransSet, FsMIOspfNssaAsbrDefRtTransTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfDefaultPassiveInterface}, GetNextIndexFsMIOspfTable, FsMIOspfDefaultPassiveInterfaceGet, FsMIOspfDefaultPassiveInterfaceSet, FsMIOspfDefaultPassiveInterfaceTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfSpfHoldtime}, GetNextIndexFsMIOspfTable, FsMIOspfSpfHoldtimeGet, FsMIOspfSpfHoldtimeSet, FsMIOspfSpfHoldtimeTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "10"},

{{12,FsMIOspfSpfDelay}, GetNextIndexFsMIOspfTable, FsMIOspfSpfDelayGet, FsMIOspfSpfDelaySet, FsMIOspfSpfDelayTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfRestartSupport}, GetNextIndexFsMIOspfTable, FsMIOspfRestartSupportGet, FsMIOspfRestartSupportSet, FsMIOspfRestartSupportTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfRestartInterval}, GetNextIndexFsMIOspfTable, FsMIOspfRestartIntervalGet, FsMIOspfRestartIntervalSet, FsMIOspfRestartIntervalTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "120"},

{{12,FsMIOspfRestartStrictLsaChecking}, GetNextIndexFsMIOspfTable, FsMIOspfRestartStrictLsaCheckingGet, FsMIOspfRestartStrictLsaCheckingSet, FsMIOspfRestartStrictLsaCheckingTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfRestartStatus}, GetNextIndexFsMIOspfTable, FsMIOspfRestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfRestartAge}, GetNextIndexFsMIOspfTable, FsMIOspfRestartAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfRestartExitReason}, GetNextIndexFsMIOspfTable, FsMIOspfRestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfHelperSupport}, GetNextIndexFsMIOspfTable, FsMIOspfHelperSupportGet, FsMIOspfHelperSupportSet, FsMIOspfHelperSupportTest, FsMIOspfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfExtTraceLevel}, GetNextIndexFsMIOspfTable, FsMIOspfExtTraceLevelGet, FsMIOspfExtTraceLevelSet, FsMIOspfExtTraceLevelTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfHelperGraceTimeLimit}, GetNextIndexFsMIOspfTable, FsMIOspfHelperGraceTimeLimitGet, FsMIOspfHelperGraceTimeLimitSet, FsMIOspfHelperGraceTimeLimitTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "0"},

{{12,FsMIOspfRestartAckState}, GetNextIndexFsMIOspfTable, FsMIOspfRestartAckStateGet, FsMIOspfRestartAckStateSet, FsMIOspfRestartAckStateTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfGraceLsaRetransmitCount}, GetNextIndexFsMIOspfTable, FsMIOspfGraceLsaRetransmitCountGet, FsMIOspfGraceLsaRetransmitCountSet, FsMIOspfGraceLsaRetransmitCountTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfRestartReason}, GetNextIndexFsMIOspfTable, FsMIOspfRestartReasonGet, FsMIOspfRestartReasonSet, FsMIOspfRestartReasonTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "0"},

{{12,FsMIOspfRTStaggeringInterval}, GetNextIndexFsMIOspfTable, FsMIOspfRTStaggeringIntervalGet, FsMIOspfRTStaggeringIntervalSet, FsMIOspfRTStaggeringIntervalTest, FsMIOspfTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "10000"},

{{12,FsMIOspfRouterIdPermanence}, GetNextIndexFsMIOspfTable, FsMIOspfRouterIdPermanenceGet, FsMIOspfRouterIdPermanenceSet, FsMIOspfRouterIdPermanenceTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfBfdStatus}, GetNextIndexFsMIOspfTable, FsMIOspfBfdStatusGet, FsMIOspfBfdStatusSet, FsMIOspfBfdStatusTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfBfdAllIfState}, GetNextIndexFsMIOspfTable, FsMIOspfBfdAllIfStateGet, FsMIOspfBfdAllIfStateSet, FsMIOspfBfdAllIfStateTest, FsMIOspfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfTableINDEX, 1, 0, 0, "2"},

{{10,FsMIOspfRTStaggeringStatus}, NULL, FsMIOspfRTStaggeringStatusGet, FsMIOspfRTStaggeringStatusSet, FsMIOspfRTStaggeringStatusTest, FsMIOspfRTStaggeringStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsMIOspfHotStandbyAdminStatus}, NULL, FsMIOspfHotStandbyAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIOspfHotStandbyState}, NULL, FsMIOspfHotStandbyStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIOspfDynamicBulkUpdStatus}, NULL, FsMIOspfDynamicBulkUpdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIOspfStanbyHelloSyncCount}, NULL, FsMIOspfStanbyHelloSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIOspfStanbyLsaSyncCount}, NULL, FsMIOspfStanbyLsaSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIOspfGlobalExtTraceLevel}, NULL, FsMIOspfGlobalExtTraceLevelGet, FsMIOspfGlobalExtTraceLevelSet, FsMIOspfGlobalExtTraceLevelTest, FsMIOspfGlobalExtTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMIOspfAreaId}, GetNextIndexFsMIOspfAreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIOspfAreaIfCount}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaIfCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIOspfAreaNetCount}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaNetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIOspfAreaRtrCount}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIOspfAreaNSSATranslatorRole}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaNSSATranslatorRoleGet, FsMIOspfAreaNSSATranslatorRoleSet, FsMIOspfAreaNSSATranslatorRoleTest, FsMIOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfAreaTableINDEX, 2, 0, 0, "2"},

{{11,FsMIOspfAreaNSSATranslatorState}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaNSSATranslatorStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfAreaTableINDEX, 2, 0, 0, "3"},

{{11,FsMIOspfAreaNSSATranslatorStabilityInterval}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaNSSATranslatorStabilityIntervalGet, FsMIOspfAreaNSSATranslatorStabilityIntervalSet, FsMIOspfAreaNSSATranslatorStabilityIntervalTest, FsMIOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfAreaTableINDEX, 2, 0, 0, "40"},

{{11,FsMIOspfAreaNSSATranslatorEvents}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaNSSATranslatorEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAreaTableINDEX, 2, 0, 0, NULL},

{{11,FsMIOspfAreaDfInfOriginate}, GetNextIndexFsMIOspfAreaTable, FsMIOspfAreaDfInfOriginateGet, FsMIOspfAreaDfInfOriginateSet, FsMIOspfAreaDfInfOriginateTest, FsMIOspfAreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfAreaTableINDEX, 2, 0, 0, "2"},

{{11,FsMIOspfHostIpAddress}, GetNextIndexFsMIOspfHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfHostTOS}, GetNextIndexFsMIOspfHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfHostRouteIfIndex}, GetNextIndexFsMIOspfHostTable, FsMIOspfHostRouteIfIndexGet, FsMIOspfHostRouteIfIndexSet, FsMIOspfHostRouteIfIndexTest, FsMIOspfHostTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfHostTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfIpAddress}, GetNextIndexFsMIOspfIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfAddressLessIf}, GetNextIndexFsMIOspfIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfOperState}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfPassive}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfPassiveGet, FsMIOspfIfPassiveSet, FsMIOspfIfPassiveTest, FsMIOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfNbrCount}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfNbrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfAdjCount}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfAdjCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfHelloRcvd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfHelloRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfHelloTxed}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfHelloTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfHelloDisd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfHelloDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfDdpRcvd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfDdpRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfDdpTxed}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfDdpTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfDdpDisd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfDdpDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLrqRcvd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLrqRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLrqTxed}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLrqTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLrqDisd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLrqDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLsuRcvd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLsuRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLsuTxed}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLsuTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLsuDisd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLsuDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLakRcvd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLakRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLakTxed}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLakTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfLakDisd}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfLakDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfIfTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfIfBfdState}, GetNextIndexFsMIOspfIfTable, FsMIOspfIfBfdStateGet, FsMIOspfIfBfdStateSet, FsMIOspfIfBfdStateTest, FsMIOspfIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfIfTableINDEX, 3, 0, 0, "2"},

{{11,FsMIOspfIfMD5AuthIpAddress}, GetNextIndexFsMIOspfIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfMD5AuthAddressLessIf}, GetNextIndexFsMIOspfIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfMD5AuthKeyId}, GetNextIndexFsMIOspfIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfMD5AuthKey}, GetNextIndexFsMIOspfIfMD5AuthTable, FsMIOspfIfMD5AuthKeyGet, FsMIOspfIfMD5AuthKeySet, FsMIOspfIfMD5AuthKeyTest, FsMIOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfMD5AuthKeyStartAccept}, GetNextIndexFsMIOspfIfMD5AuthTable, FsMIOspfIfMD5AuthKeyStartAcceptGet, FsMIOspfIfMD5AuthKeyStartAcceptSet, FsMIOspfIfMD5AuthKeyStartAcceptTest, FsMIOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfMD5AuthKeyStartGenerate}, GetNextIndexFsMIOspfIfMD5AuthTable, FsMIOspfIfMD5AuthKeyStartGenerateGet, FsMIOspfIfMD5AuthKeyStartGenerateSet, FsMIOspfIfMD5AuthKeyStartGenerateTest, FsMIOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfMD5AuthKeyStopGenerate}, GetNextIndexFsMIOspfIfMD5AuthTable, FsMIOspfIfMD5AuthKeyStopGenerateGet, FsMIOspfIfMD5AuthKeyStopGenerateSet, FsMIOspfIfMD5AuthKeyStopGenerateTest, FsMIOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, "-1"},

{{11,FsMIOspfIfMD5AuthKeyStopAccept}, GetNextIndexFsMIOspfIfMD5AuthTable, FsMIOspfIfMD5AuthKeyStopAcceptGet, FsMIOspfIfMD5AuthKeyStopAcceptSet, FsMIOspfIfMD5AuthKeyStopAcceptTest, FsMIOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, "-1"},

{{11,FsMIOspfIfMD5AuthKeyStatus}, GetNextIndexFsMIOspfIfMD5AuthTable, FsMIOspfIfMD5AuthKeyStatusGet, FsMIOspfIfMD5AuthKeyStatusSet, FsMIOspfIfMD5AuthKeyStatusTest, FsMIOspfIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthAreaId}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthNeighbor}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthKeyId}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthKey}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, FsMIOspfVirtIfMD5AuthKeyGet, FsMIOspfVirtIfMD5AuthKeySet, FsMIOspfVirtIfMD5AuthKeyTest, FsMIOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthKeyStartAccept}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, FsMIOspfVirtIfMD5AuthKeyStartAcceptGet, FsMIOspfVirtIfMD5AuthKeyStartAcceptSet, FsMIOspfVirtIfMD5AuthKeyStartAcceptTest, FsMIOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthKeyStartGenerate}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, FsMIOspfVirtIfMD5AuthKeyStartGenerateGet, FsMIOspfVirtIfMD5AuthKeyStartGenerateSet, FsMIOspfVirtIfMD5AuthKeyStartGenerateTest, FsMIOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfMD5AuthKeyStopGenerate}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, FsMIOspfVirtIfMD5AuthKeyStopGenerateGet, FsMIOspfVirtIfMD5AuthKeyStopGenerateSet, FsMIOspfVirtIfMD5AuthKeyStopGenerateTest, FsMIOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, "-1"},

{{11,FsMIOspfVirtIfMD5AuthKeyStopAccept}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, FsMIOspfVirtIfMD5AuthKeyStopAcceptGet, FsMIOspfVirtIfMD5AuthKeyStopAcceptSet, FsMIOspfVirtIfMD5AuthKeyStopAcceptTest, FsMIOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, "-1"},

{{11,FsMIOspfVirtIfMD5AuthKeyStatus}, GetNextIndexFsMIOspfVirtIfMD5AuthTable, FsMIOspfVirtIfMD5AuthKeyStatusGet, FsMIOspfVirtIfMD5AuthKeyStatusSet, FsMIOspfVirtIfMD5AuthKeyStatusTest, FsMIOspfVirtIfMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfVirtIfMD5AuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfNbrIpAddr}, GetNextIndexFsMIOspfNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrAddressLessIndex}, GetNextIndexFsMIOspfNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrDBSummaryQLen}, GetNextIndexFsMIOspfNbrTable, FsMIOspfNbrDBSummaryQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrLSReqQLen}, GetNextIndexFsMIOspfNbrTable, FsMIOspfNbrLSReqQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrRestartHelperStatus}, GetNextIndexFsMIOspfNbrTable, FsMIOspfNbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrRestartHelperAge}, GetNextIndexFsMIOspfNbrTable, FsMIOspfNbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrRestartHelperExitReason}, GetNextIndexFsMIOspfNbrTable, FsMIOspfNbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfNbrBfdState}, GetNextIndexFsMIOspfNbrTable, FsMIOspfNbrBfdStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfRouteIpAddr}, GetNextIndexFsMIOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteIpAddrMask}, GetNextIndexFsMIOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteIpTos}, GetNextIndexFsMIOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteIpNextHop}, GetNextIndexFsMIOspfRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteType}, GetNextIndexFsMIOspfRoutingTable, FsMIOspfRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteAreaId}, GetNextIndexFsMIOspfRoutingTable, FsMIOspfRouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteCost}, GetNextIndexFsMIOspfRoutingTable, FsMIOspfRouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteType2Cost}, GetNextIndexFsMIOspfRoutingTable, FsMIOspfRouteType2CostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfRouteInterfaceIndex}, GetNextIndexFsMIOspfRoutingTable, FsMIOspfRouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfRoutingTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfPrimIpAddr}, GetNextIndexFsMIOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfSecIfTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfPrimAddresslessIf}, GetNextIndexFsMIOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfSecIfTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfSecIpAddr}, GetNextIndexFsMIOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfSecIfTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfSecIpAddrMask}, GetNextIndexFsMIOspfSecIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfSecIfTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfSecIfStatus}, GetNextIndexFsMIOspfSecIfTable, FsMIOspfSecIfStatusGet, FsMIOspfSecIfStatusSet, FsMIOspfSecIfStatusTest, FsMIOspfSecIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfSecIfTableINDEX, 5, 0, 1, NULL},

{{11,FsMIOspfAreaAggregateAreaID}, GetNextIndexFsMIOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfAreaAggregateLsdbType}, GetNextIndexFsMIOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfAreaAggregateNet}, GetNextIndexFsMIOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfAreaAggregateMask}, GetNextIndexFsMIOspfAreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfAreaAggregateExternalTag}, GetNextIndexFsMIOspfAreaAggregateTable, FsMIOspfAreaAggregateExternalTagGet, FsMIOspfAreaAggregateExternalTagSet, FsMIOspfAreaAggregateExternalTagTest, FsMIOspfAreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfAreaAggregateTableINDEX, 5, 0, 0, NULL},

{{11,FsMIOspfAsExternalAggregationNet}, GetNextIndexFsMIOspfAsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfAsExternalAggregationMask}, GetNextIndexFsMIOspfAsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfAsExternalAggregationAreaId}, GetNextIndexFsMIOspfAsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfAsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfAsExternalAggregationEffect}, GetNextIndexFsMIOspfAsExternalAggregationTable, FsMIOspfAsExternalAggregationEffectGet, FsMIOspfAsExternalAggregationEffectSet, FsMIOspfAsExternalAggregationEffectTest, FsMIOspfAsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfAsExternalAggregationTableINDEX, 4, 0, 0, "1"},

{{11,FsMIOspfAsExternalAggregationTranslation}, GetNextIndexFsMIOspfAsExternalAggregationTable, FsMIOspfAsExternalAggregationTranslationGet, FsMIOspfAsExternalAggregationTranslationSet, FsMIOspfAsExternalAggregationTranslationTest, FsMIOspfAsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfAsExternalAggregationTableINDEX, 4, 0, 0, "1"},

{{11,FsMIOspfAsExternalAggregationStatus}, GetNextIndexFsMIOspfAsExternalAggregationTable, FsMIOspfAsExternalAggregationStatusGet, FsMIOspfAsExternalAggregationStatusSet, FsMIOspfAsExternalAggregationStatusTest, FsMIOspfAsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfAsExternalAggregationTableINDEX, 4, 0, 1, NULL},

{{13,FsMIOspfOpaqueOption}, GetNextIndexFsMIOspfOpaqueTable, FsMIOspfOpaqueOptionGet, FsMIOspfOpaqueOptionSet, FsMIOspfOpaqueOptionTest, FsMIOspfOpaqueTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfOpaqueTableINDEX, 1, 0, 0, "2"},

{{13,FsMIOspfType11LsaCount}, GetNextIndexFsMIOspfOpaqueTable, FsMIOspfType11LsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfOpaqueTableINDEX, 1, 0, 0, NULL},

{{13,FsMIOspfType11LsaCksumSum}, GetNextIndexFsMIOspfOpaqueTable, FsMIOspfType11LsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfOpaqueTableINDEX, 1, 0, 0, NULL},

{{13,FsMIOspfAreaIDValid}, GetNextIndexFsMIOspfOpaqueTable, FsMIOspfAreaIDValidGet, FsMIOspfAreaIDValidSet, FsMIOspfAreaIDValidTest, FsMIOspfOpaqueTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfOpaqueTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfOpaqueType9LsaCount}, GetNextIndexFsMIOspfOpaqueInterfaceTable, FsMIOspfOpaqueType9LsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfOpaqueInterfaceTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfOpaqueType9LsaCksumSum}, GetNextIndexFsMIOspfOpaqueInterfaceTable, FsMIOspfOpaqueType9LsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfOpaqueInterfaceTableINDEX, 3, 0, 0, "0"},

{{12,FsMIOspfType9LsdbIfIpAddress}, GetNextIndexFsMIOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbOpaqueType}, GetNextIndexFsMIOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbLsid}, GetNextIndexFsMIOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbRouterId}, GetNextIndexFsMIOspfType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbSequence}, GetNextIndexFsMIOspfType9LsdbTable, FsMIOspfType9LsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbAge}, GetNextIndexFsMIOspfType9LsdbTable, FsMIOspfType9LsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbChecksum}, GetNextIndexFsMIOspfType9LsdbTable, FsMIOspfType9LsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType9LsdbAdvertisement}, GetNextIndexFsMIOspfType9LsdbTable, FsMIOspfType9LsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIOspfType9LsdbTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfType11LsdbOpaqueType}, GetNextIndexFsMIOspfType11LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfType11LsdbLsid}, GetNextIndexFsMIOspfType11LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfType11LsdbRouterId}, GetNextIndexFsMIOspfType11LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfType11LsdbSequence}, GetNextIndexFsMIOspfType11LsdbTable, FsMIOspfType11LsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfType11LsdbAge}, GetNextIndexFsMIOspfType11LsdbTable, FsMIOspfType11LsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfType11LsdbChecksum}, GetNextIndexFsMIOspfType11LsdbTable, FsMIOspfType11LsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfType11LsdbAdvertisement}, GetNextIndexFsMIOspfType11LsdbTable, FsMIOspfType11LsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIOspfType11LsdbTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbAppid}, GetNextIndexFsMIOspfAppInfoDbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbOpaqueType}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbOpaqueTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbLsaTypesSupported}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbLsaTypesSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbType9Gen}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbType9GenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbType9Rcvd}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbType9RcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbType10Gen}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbType10GenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbType10Rcvd}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbType10RcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbType11Gen}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbType11GenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfAppInfoDbType11Rcvd}, GetNextIndexFsMIOspfAppInfoDbTable, FsMIOspfAppInfoDbType11RcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfAppInfoDbTableINDEX, 2, 0, 0, NULL},

{{13,FsMIOspfRRDStatus}, GetNextIndexFsMIOspfRRDRouteTable, FsMIOspfRRDStatusGet, FsMIOspfRRDStatusSet, FsMIOspfRRDStatusTest, FsMIOspfRRDRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfRRDRouteTableINDEX, 1, 0, 0, NULL},

{{13,FsMIOspfRRDSrcProtoMaskEnable}, GetNextIndexFsMIOspfRRDRouteTable, FsMIOspfRRDSrcProtoMaskEnableGet, FsMIOspfRRDSrcProtoMaskEnableSet, FsMIOspfRRDSrcProtoMaskEnableTest, FsMIOspfRRDRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfRRDRouteTableINDEX, 1, 0, 0, "0"},

{{13,FsMIOspfRRDSrcProtoMaskDisable}, GetNextIndexFsMIOspfRRDRouteTable, FsMIOspfRRDSrcProtoMaskDisableGet, FsMIOspfRRDSrcProtoMaskDisableSet, FsMIOspfRRDSrcProtoMaskDisableTest, FsMIOspfRRDRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfRRDRouteTableINDEX, 1, 0, 0, "57478"},

{{13,FsMIOspfRRDRouteMapEnable}, GetNextIndexFsMIOspfRRDRouteTable, FsMIOspfRRDRouteMapEnableGet, FsMIOspfRRDRouteMapEnableSet, FsMIOspfRRDRouteMapEnableTest, FsMIOspfRRDRouteTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfRRDRouteTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfRRDRouteDest}, GetNextIndexFsMIOspfRRDRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfRRDRouteMask}, GetNextIndexFsMIOspfRRDRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfRRDRouteMetric}, GetNextIndexFsMIOspfRRDRouteConfigTable, FsMIOspfRRDRouteMetricGet, FsMIOspfRRDRouteMetricSet, FsMIOspfRRDRouteMetricTest, FsMIOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 0, "10"},

{{12,FsMIOspfRRDRouteMetricType}, GetNextIndexFsMIOspfRRDRouteConfigTable, FsMIOspfRRDRouteMetricTypeGet, FsMIOspfRRDRouteMetricTypeSet, FsMIOspfRRDRouteMetricTypeTest, FsMIOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 0, "2"},

{{12,FsMIOspfRRDRouteTagType}, GetNextIndexFsMIOspfRRDRouteConfigTable, FsMIOspfRRDRouteTagTypeGet, FsMIOspfRRDRouteTagTypeSet, FsMIOspfRRDRouteTagTypeTest, FsMIOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 0, "1"},

{{12,FsMIOspfRRDRouteTag}, GetNextIndexFsMIOspfRRDRouteConfigTable, FsMIOspfRRDRouteTagGet, FsMIOspfRRDRouteTagSet, FsMIOspfRRDRouteTagTest, FsMIOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 0, "0"},

{{12,FsMIOspfRRDRouteStatus}, GetNextIndexFsMIOspfRRDRouteConfigTable, FsMIOspfRRDRouteStatusGet, FsMIOspfRRDRouteStatusSet, FsMIOspfRRDRouteStatusTest, FsMIOspfRRDRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfRRDRouteConfigTableINDEX, 3, 0, 1, NULL},

{{12,FsMIOspfRRDProtocolId}, GetNextIndexFsMIOspfRRDMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfRRDMetricTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfRRDMetricValue}, GetNextIndexFsMIOspfRRDMetricTable, FsMIOspfRRDMetricValueGet, FsMIOspfRRDMetricValueSet, FsMIOspfRRDMetricValueTest, FsMIOspfRRDMetricTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfRRDMetricTableINDEX, 2, 0, 0, NULL},

{{12,FsMIOspfRRDMetricType}, GetNextIndexFsMIOspfRRDMetricTable, FsMIOspfRRDMetricTypeGet, FsMIOspfRRDMetricTypeSet, FsMIOspfRRDMetricTypeTest, FsMIOspfRRDMetricTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfRRDMetricTableINDEX, 2, 0, 0, NULL},

{{11,FsMIOspfVirtNbrRestartHelperStatus}, GetNextIndexFsMIOspfVirtNbrTable, FsMIOspfVirtNbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfVirtNbrRestartHelperAge}, GetNextIndexFsMIOspfVirtNbrTable, FsMIOspfVirtNbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{11,FsMIOspfVirtNbrRestartHelperExitReason}, GetNextIndexFsMIOspfVirtNbrTable, FsMIOspfVirtNbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfVirtNbrTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfDistInOutRouteMapName}, GetNextIndexFsMIOspfDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfDistInOutRouteMapType}, GetNextIndexFsMIOspfDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfDistInOutRouteMapValue}, GetNextIndexFsMIOspfDistInOutRouteMapTable, FsMIOspfDistInOutRouteMapValueGet, FsMIOspfDistInOutRouteMapValueSet, FsMIOspfDistInOutRouteMapValueTest, FsMIOspfDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfDistInOutRouteMapRowStatus}, GetNextIndexFsMIOspfDistInOutRouteMapTable, FsMIOspfDistInOutRouteMapRowStatusGet, FsMIOspfDistInOutRouteMapRowStatusSet, FsMIOspfDistInOutRouteMapRowStatusTest, FsMIOspfDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfDistInOutRouteMapTableINDEX, 3, 0, 1, NULL},

{{12,FsMIOspfPreferenceValue}, GetNextIndexFsMIOspfPreferenceTable, FsMIOspfPreferenceValueGet, FsMIOspfPreferenceValueSet, FsMIOspfPreferenceValueTest, FsMIOspfPreferenceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfPreferenceTableINDEX, 1, 0, 0, "0"},

{{11,FsMIOspfIfAuthIpAddress}, GetNextIndexFsMIOspfIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthAddressLessIf}, GetNextIndexFsMIOspfIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKeyId}, GetNextIndexFsMIOspfIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKey}, GetNextIndexFsMIOspfIfAuthTable, FsMIOspfIfAuthKeyGet, FsMIOspfIfAuthKeySet, FsMIOspfIfAuthKeyTest, FsMIOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKeyStartAccept}, GetNextIndexFsMIOspfIfAuthTable, FsMIOspfIfAuthKeyStartAcceptGet, FsMIOspfIfAuthKeyStartAcceptSet, FsMIOspfIfAuthKeyStartAcceptTest, FsMIOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKeyStartGenerate}, GetNextIndexFsMIOspfIfAuthTable, FsMIOspfIfAuthKeyStartGenerateGet, FsMIOspfIfAuthKeyStartGenerateSet, FsMIOspfIfAuthKeyStartGenerateTest, FsMIOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKeyStopGenerate}, GetNextIndexFsMIOspfIfAuthTable, FsMIOspfIfAuthKeyStopGenerateGet, FsMIOspfIfAuthKeyStopGenerateSet, FsMIOspfIfAuthKeyStopGenerateTest, FsMIOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKeyStopAccept}, GetNextIndexFsMIOspfIfAuthTable, FsMIOspfIfAuthKeyStopAcceptGet, FsMIOspfIfAuthKeyStopAcceptSet, FsMIOspfIfAuthKeyStopAcceptTest, FsMIOspfIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfIfAuthKeyStatus}, GetNextIndexFsMIOspfIfAuthTable, FsMIOspfIfAuthKeyStatusGet, FsMIOspfIfAuthKeyStatusSet, FsMIOspfIfAuthKeyStatusTest, FsMIOspfIfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfAuthAreaId}, GetNextIndexFsMIOspfVirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfAuthNeighbor}, GetNextIndexFsMIOspfVirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfAuthKeyId}, GetNextIndexFsMIOspfVirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfAuthKey}, GetNextIndexFsMIOspfVirtIfAuthTable, FsMIOspfVirtIfAuthKeyGet, FsMIOspfVirtIfAuthKeySet, FsMIOspfVirtIfAuthKeyTest, FsMIOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{11,FsMIOspfVirtIfAuthKeyStartAccept}, GetNextIndexFsMIOspfVirtIfAuthTable, FsMIOspfVirtIfAuthKeyStartAcceptGet, FsMIOspfVirtIfAuthKeyStartAcceptSet, FsMIOspfVirtIfAuthKeyStartAcceptTest, FsMIOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, "0"},

{{11,FsMIOspfVirtIfAuthKeyStartGenerate}, GetNextIndexFsMIOspfVirtIfAuthTable, FsMIOspfVirtIfAuthKeyStartGenerateGet, FsMIOspfVirtIfAuthKeyStartGenerateSet, FsMIOspfVirtIfAuthKeyStartGenerateTest, FsMIOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, "0"},

{{11,FsMIOspfVirtIfAuthKeyStopGenerate}, GetNextIndexFsMIOspfVirtIfAuthTable, FsMIOspfVirtIfAuthKeyStopGenerateGet, FsMIOspfVirtIfAuthKeyStopGenerateSet, FsMIOspfVirtIfAuthKeyStopGenerateTest, FsMIOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, "-1"},

{{11,FsMIOspfVirtIfAuthKeyStopAccept}, GetNextIndexFsMIOspfVirtIfAuthTable, FsMIOspfVirtIfAuthKeyStopAcceptGet, FsMIOspfVirtIfAuthKeyStopAcceptSet, FsMIOspfVirtIfAuthKeyStopAcceptTest, FsMIOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, "-1"},

{{11,FsMIOspfVirtIfAuthKeyStatus}, GetNextIndexFsMIOspfVirtIfAuthTable, FsMIOspfVirtIfAuthKeyStatusGet, FsMIOspfVirtIfAuthKeyStatusSet, FsMIOspfVirtIfAuthKeyStatusTest, FsMIOspfVirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfVirtIfAuthTableINDEX, 4, 0, 0, NULL},
};
tMibData fsmiosEntry = { 192, fsmiosMibEntry };

#endif /* _FSMIOSDB_H */

