/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osdefn.h,v 1.50 2017/09/21 13:48:45 siva Exp $
 *
 * Description:This file contains all constants relating to
 *             protocol and the implementation of it.
 *
 *******************************************************************/
#ifndef _OSDEFN_H
#define _OSDEFN_H

/****************************************************************************/
/* General                                                                  */
/****************************************************************************/

#define  MAX_IP_ADDR_LEN          4                   
#define  MAX_IP_ADDR_LEN_IN_BITS  MAX_IP_ADDR_LEN * 8 
#define  OPTIONS_OR_MASK_LEN      4

/* MD5 Authentication KEY related defns   */
#define  MAX_AUTHKEY_LEN           16                  
#define  MD5_MSG_DIGEST_LEN        16
#define  OSPF_SHA1_DIGEST_LEN    20                 
#define  OSPF_SHA2_224_DIGEST_LEN 28
#define  OSPF_SHA2_256_DIGEST_LEN 32
#define  OSPF_SHA2_384_DIGEST_LEN 48
#define  OSPF_SHA2_512_DIGEST_LEN 64 
#define  DIGEST_LEN_IF_CRYPT(p_if)    ( ((p_if)->u2AuthType ==      \
                           CRYPT_AUTHENTICATION ) ? OsUtilGetCryptoLen((p_if))/*MD5_MSG_DIGEST_LEN*/ : 0)

#define  OSPF_MAX_AUTHKEY_VALUE  255
#define  OSPF_MIN_AUTHKEY_VALUE  0
#define  DEF_KEY_START_ACCEPT    0                             
#define  DEF_KEY_START_GENERATE  0                             
#define  DEF_KEY_STOP_GENERATE   (UINT4)-1                     
#define  DEF_KEY_STOP_ACCEPT     (UINT4)-1                     
#define  AUTHKEY_STATUS_VALID    1                             
#define  AUTHKEY_STATUS_INVALID  2                             
#define  AUTHKEY_STATUS_DELETE   3                             
#define  OSPF_MAX_CRYPTOTYPE_VALUE 6
#define  OSPF_MIN_CRYPTOTYPE_VALUE 0
/* the values of TRUE and FALSE as defined in MIB */
#define  OSPF_TRUE               1                             
#define  OSPF_FALSE              2                            
#define REDISTRIBUTED_TYPE5 3

#define OSPF_ADD                 1

#define OSPF_ONE          1

/*CLI constants need for dynamic sync audit implementation*/
#define OSPF_AUDIT_FILE_ACTIVE        "/tmp/ospf_output_file_active"
#define OSPF_AUDIT_FILE_STDBY         "/tmp/ospf_output_file_stdby"
#define OSPF_DYN_MAX_CMDS             14
#define OSPF_CLI_EOF                  2
#define OSPF_CLI_NO_EOF               1
#define OSPF_CLI_RDONLY               OSIX_FILE_RO
#define OSPF_CLI_WRONLY               OSIX_FILE_WO
#define OSPF_CLI_MAX_GROUPS_LINE_LEN  200

/* constans used to indicate route calculation is compelted or suspended */
#define OSPF_ROUTE_CALC_COMPLETED    1
#define OSPF_ROUTE_CALC_SUSPENDED    0


/* Macro used for RBTree Comparisions */
#define OSPF_RB_EQUAL             0
#define OSPF_RB_GREATER           1
#define OSPF_RB_LESS             -1


/* constants used for interface state change indications */
#define  OSPF_OPERUP             1                             
#define  OSPF_OPERDOWN           2                             
#define  OSPF_LOOPBACK_IND       3                             
#define  OSPF_UNLOOP_IND         4                             

/* row creation/deletion values as defined in MIB */
#define  OSPF_VALID              1                             
#define  OSPF_INVALID            2                             

/* authentication types as per MIB */
#define  NO_AUTHENTICATION       0                             
#define  SIMPLE_PASSWORD         1                             
#define  CRYPT_AUTHENTICATION    2                             

/* Crypto Authentication type */
#define OSPF_AUTH_MD5           1  
#define OSPF_AUTH_SHA1  2  
#define OSPF_AUTH_SHA2_224  3  
#define OSPF_AUTH_SHA2_256 4  
#define OSPF_AUTH_SHA2_384 5  
#define OSPF_AUTH_SHA2_512 6  

/* Different Types of ABR Types supported */
#define  STANDARD_ABR            1 
#define  CISCO_ABR               2
#define  IBM_ABR                 3
 
 /****************************************************************************/
 /* Constant defining the Number of Metrics                                  */
 /****************************************************************************/

#ifdef TOS_SUPPORT
#define  OSPF_MAX_METRIC              16                            
#else
#define  OSPF_MAX_METRIC              1                             
#endif /* TOS_SUPPORT */

#ifdef TOS_SUPPORT
#define OSPF_TOS_LOOP(u1Tos)     for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
#else
#define OSPF_TOS_LOOP(u1Tos)
#endif

#define  MAX_METRIC_NUM          0xffffff                            
#define  OSPF_TOS_IN_EXT_LSA           0x7f 
 
/****************************************************************************/
/* Architectural Constants                                                  */
/****************************************************************************/
#define  LSA_REFRESH_INTERVAL    (NO_OF_TICKS_PER_SEC * 30*60)    /* 30 min  */

#define  MIN_LSA_ARRIVAL         (NO_OF_TICKS_PER_SEC * 1)         /*  1 sec  */
#define  MAX_AGE                 3600                              /*  1 hour */
#define  CHECK_AGE               (5*60)                            /*  5 min  */
#define  MAX_AGE_DIFF            (15*60)                           /* 15 min  */

#define  LS_INFINITY_16BIT       0xffff                /* used in router LSA  */
#define  LS_INFINITY_24BIT       0xffffff   /* used in summary and AS ext LSA */

#define  DO_NOT_AGE              0x8000                        

/****************************************************************************/
/* OSPF packet related constants                                            */
/****************************************************************************/

#define  OSPF_VERSION_NO_2              2                                  

/* OSPF packet types */
#define  HELLO_PKT                      1                                  
#define  DD_PKT                         2                                  
#define  LSA_REQ_PKT                    3                                  
#define  LS_UPDATE_PKT                  4                                  
#define  LSA_ACK_PKT                    5                                  
#define  MAX_OSPF_PKT_TYPE              5                                  

/* LSA types */
#define  ROUTER_LSA                     1                                  
#define  NETWORK_LSA                    2                                  
#define  NETWORK_SUM_LSA                3                                  
#define  ASBR_SUM_LSA                   4                                  
#define  AS_EXT_LSA                     5
#define  COND_NETWORK_SUM_LSA           6
#define  DEFAULT_NETWORK_SUM_LSA        20 /* Changed to support  NSSA */
#define NSSA_LSA    7
#define DEFAULT_NSSA_LSA  12
#define AS_TRNSLTD_EXT_LSA  13                 /* Translated Type 5 LSA - Assoc Primitive 
               is tRtEntry        */
#define AS_TRNSLTD_EXT_RNG_LSA 14     /* Translated Type 5 LSA - Assoc Primitive 
                is tAddrRange       */

#define COND_AS_EXT_LSA  15   /* Specifies aggregated Type 5 LSA - Assoc
                primitive is tAsExtAddrRange    */
                
#define COND_NSSA_LSA   16   /* Specifies aggregated Type 7 LSA  */


#define OSPF_NEXT_HOP_VALID           1
#define OSPF_NEXT_HOP_INVALID         0

#define  INDICATION_LSA                 8
#define    MOSPF_LSA                     6
#define    ATTRIBUTE_LSA                 8

#define    TYPE9_OPQ_LSA                9
#define    TYPE10_OPQ_LSA               10
#define    TYPE11_OPQ_LSA               11
#define    MAX_LSA_TYPE                 11
#define    MAX_APP_INFO_SIZE            512
#define    NEW_LSA                      1  /*00000001*/
#define    NEW_LSA_INSTANCE             2   /*00000010*/
#define    FLUSHED_LSA                  4   /*00000100*/

/* constants related to packet formats and sizes */
#define  IP_HEADER_SIZE                 20                                 
#define  OS_HEADER_SIZE                 24                                 
#define  ETH_HEADER_SIZE                14                                 
#define  AUTH_KEY_SIZE                  8                                  
#define  MD5_AUTH_DATA_LEN              16                                 

#define SET_FLAG                      2
#define UNSET_FLAG                    1

/* Offset for OSPF Packet Header fields */ 
#define  VERSION_NO_OFFSET_IN_HEADER    0                                  
#define  TYPE_OFFSET_IN_HEADER          1                                  
#define  RTR_ID_OFFSET_IN_HEADER        4                                  
#define  AREA_ID_OFFSET_IN_HEADER       8                                  
#define  CHKSUM_OFFSET_IN_HEADER        12                                 
#define  AUTH_TYPE_OFFSET_IN_HEADER     14                                 
#define  AUTH_KEY_OFFSET_IN_HEADER      16                                 
#define  AUTH_KEYID_OFFSET_IN_HEADER    18                                 
#define  AUTH_DATALEN_OFFSET_IN_HEADER  19                                 
#define  CSN_OFFSET_IN_HEADER           20          /* crypt seq num offset */

#define  LS_HEADER_SIZE         (UINT2) 20                                 
#define  LSA_SIZE                       12 

/* Offset for Link state Header fields */ 
#define  AGE_SIZE_IN_LS_HEADER          2                                  
#define  AGE_OFFSET_IN_LS_HEADER        0                                  
#define  OPTIONS_OFFSET_IN_LS_HEADER    2                                  
#define  TYPE_OFFSET_IN_LS_HEADER       3                                  
#define  CHKSUM_OFFSET_IN_LS_HEADER     16                                 
#define  LENGTH_OFFSET_IN_LS_HEADER     18                                 

/* Offset for Summary LSA fields */ 
#define  SUMMARY_LSA_METRIC_OFFSET      25

/* Offset for AS External LSA fields */ 
#define  FWD_ADDR_OFFSET_IN_EXT_LSA     8
#define  METRIC_TYPE_SIZE_IN_EXT_LSA    1
#define  METRIC_SIZE_IN_EXT_LSA         3

#define  HELLO_FIXED_PORTION_SIZE       20                                 

#define  DDP_FIXED_PORTION_SIZE         8                                  

#define  OPTIONS_OFFSET_IN_DDP          26                                 
#define  FLAGS_OFFSET_IN_DDP            27                                 

/* Default Values for External Routes  - osconf.c */
#define  MIN_METRIC                     1
#define  DEFAULT_METRIC_TYPE            TYPE_2_METRIC
#define  DEFAULT_EXTRT_TAG              0
#define  DEFAULT_FWD_IF_INDEX           0

/* masks for flags in DDP */
#define  I_BIT_MASK             (UINT1) 4    /* 00000100 */
#define  M_BIT_MASK             (UINT1) 2    /* 00000010 */
#define  MS_BIT_MASK            (UINT1) 1    /* 00000001 */

#define  LSU_FIXED_PORTION_SIZE         4                                  
#define  LSA_REQ_SIZE                   12                                 

/* constants used for setting options */
#define  E_BIT_MASK             (UINT1) 2    /* 00000010 */
#define  T_BIT_MASK             (UINT1) 1    /* 00000001 */
#define N_BIT_MASK       (UINT1) 8   /*   00001000  */

#define  DC_BIT_MASK            (UINT1) 32   /* 00100000 */

#define    OPQ_BIT_MASK         (UINT1) 64   /* 01000000 */

/* constants used for setting the VEB bits in router LSAs */
#define  ABR_MASK               (UINT1) 1    /* 0000 0001 */
#define  ASBR_MASK              (UINT1) 2    /* 0000 0010 */
#define  V_BIT_MASK             (UINT1) 4    /* 0000 0100 */

#define NT_BIT_MASK    (UINT1) 16 /* 00010000 */
#define P_BIT_MASK    (UINT1) 8 /* 00001000 */

                                            
#define  RTR_LSA_LINK_ID_SIZE           4                                  
#define  RTR_LSA_LINK_DATA_SIZE         4  
#define  RTR_LSA_LINK_SIZE                (RTR_LSA_LINK_ID_SIZE + \
                                           RTR_LSA_LINK_DATA_SIZE + \
                                           (OSPF_MAX_METRIC * 4))                                
#define  EXT_LSA_LINK_SIZE              12                                 
#define  EXT_LSA_TOS_MASK               0x7f                               
#define  EXT_LSA_METRIC_TYPE_MASK       0x80                  

#define NSSA_LSA_LINK_SIZE   12

/****************************************************************************/
/* Area related constants */
/****************************************************************************/

#define  NO_AREA_SUMMARY                1                                  
#define  SEND_AREA_SUMMARY              2                                  

#define  SUMMARY_LINK                   3                                  
#define  NSSA_EXTERNAL_LINK             7                                  

#define  ADVERTISE_MATCHING             1                                  
#define  DO_NOT_ADVERTISE_MATCHING      2                                  

#define  FLUSH                          1                                  
#define  DO_NOT_FLUSH                   2                                  

/* Constants specifying different OSPF area types */
#define NORMAL_AREA  1
#define STUB_AREA    2
#define NSSA_AREA    3
#define DEFAULT_SUMM_COST               10
#define STUB_NSSA_DEFAULT_COST          1                                                                        
/****************************************************************************/
/* Interface related constants                                              */
/****************************************************************************/

/* interface types */
#define  IF_BROADCAST                   1                                  
#define  IF_NBMA                        2                                  
#define  IF_PTOP                        3                                  
#define  IF_VIRTUAL                     4                                  

#define  IF_PTOMP                       5                                  
#define  IF_LOOPBACK                    6                                  
#define  MAX_IF_TYPE                    6                                  

/* interface states */
#define  IFS_DOWN                       0                                  
#define  IFS_LOOP_BACK                  1                                  
#define  IFS_WAITING                    2                                  
#define  IFS_PTOP                       3                                  
#define  IFS_DR                         4                                  
#define  IFS_BACKUP                     5                                  
#define  IFS_DR_OTHER                   6                                  
#define  IFS_LAST_STATE                 IFS_DR_OTHER                       
#define  MAX_IF_STATE                   (IFS_LAST_STATE+1)                 

/* interface events */
#define  IFE_UP                         0                                  
#define  IFE_WAIT_TIMER                 1                                  
#define  IFE_BACKUP_SEEN                2                                  
#define  IFE_NBR_CHANGE                 3                                  
#define  IFE_LOOP_IND                   4                                  
#define  IFE_UNLOOP_IND                 5                                  
#define  IFE_DOWN                       6                                  
#define  IFE_LAST_EVENT                 IFE_DOWN                           
#define  MAX_IF_EVENT                   (IFE_LAST_EVENT + 1)               
#define  VIF_COST_CHANGED               (MAX_IF_EVENT + 1)                 

 
/* interface cleanup operations */
#define  IF_CLEANUP_DOWN                0                                  
#define  IF_CLEANUP_DISABLE             1                                  
#define  IF_CLEANUP_DELETE              2                                  

#define  DISCOVERED_ENDPOINT            1                                  
#define  CONFIGURED_ENDPOINT            2                                  

/****************************************************************************/
/* Neighbor related constants                                               */
/****************************************************************************/

/*  neighbor states */
#define  NBRS_DOWN                      0                                  
#define  NBRS_ATTEMPT                   1                                  
#define  NBRS_INIT                      2                                  
#define  NBRS_2WAY                      3                                  
#define  NBRS_EXSTART                   4                                  
#define  NBRS_EXCHANGE                  5                                  
#define  NBRS_LOADING                   6                                  
#define  NBRS_FULL                      7                                  
#define  NBRS_RSTADJEXSTART             8
#define  NBRS_LAST_STATE                NBRS_FULL                          
#define  MAX_NBR_STATE                  (NBRS_LAST_STATE +1)               

/* neighbor events */         
#define  NBRE_HELLO_RCVD                0                                  
#define  NBRE_START                     1                                  
#define  NBRE_2WAY_RCVD                 2                                  
#define  NBRE_NEG_DONE                  3                                  
#define  NBRE_EXCHANGE_DONE             4                                  
#define  NBRE_BAD_LS_REQ                5                                  
#define  NBRE_LOADING_DONE              6                                  
#define  NBRE_ADJ_OK                    7                                  
#define  NBRE_SEQ_NUM_MISMATCH          8                                  
#define  NBRE_1WAY_RCVD                 9                                  
#define  NBRE_KILL_NBR                  10                                 
#define  NBRE_INACTIVITY_TIMER          11                                 
#define  NBRE_LL_DOWN                   12                                 
#define  NBRE_LAST_EVENT                NBRE_LL_DOWN                       
#define  MAX_NBR_EVENT                  (NBRE_LAST_EVENT+1)                

#define  MAX_NBR_CFG_STATUS             3
#define  MAX_RT_DST_TYPE             4
#define  MAX_RT_PATH_TYPE            5
#define  MAX_DBG_LSA_TYPE           12
#define  MAX_PKT_TYPE                6
#define  MAX_ABR_TYPE                4

/* configuration status of the neighbor */
#define  DISCOVERED_NBR                 1    /* dynamic */
#define  CONFIGURED_NBR                 2    /* permanent */

/* NSSA Translator States */

#define TRNSLTR_STATE_ENAELCTD   0
#define TRNSLTR_STATE_DISABLED   1

/* NSSA Translator Role */
#define TRNSLTR_ROLE_ALWAYS    1
#define TRNSLTR_ROLE_CANDIDATE   2

/* Default Infomation Originate */
#define DEFAULT_INFO_ORIGINATE    1
#define NO_DEFAULT_INFO_ORIGINATE   2

/****************************************************************************/
/* LSA related constants                                                    */
/****************************************************************************/

#define  RESERVED_SEQ_NUM               0x80000000                         
#define  MIN_SEQ_NUM                    0x80000001                         
#define  MAX_SEQ_NUM                    0x7fffffff                         

/* link types to be used in router lsa */
#define  PTOP_LINK                      1                                  
#define  TRANSIT_LINK                   2                                  
#define  STUB_LINK                      3                                  
#define  VIRTUAL_LINK                   4                                  

/* constants used for retransmission logic */
#define  FIRST_BIT_SET_MASK             0X00000001                         
#define  BIT_MAP_FLAG_UNIT_SIZE         32                                 

/* Constants used to indicate the type of event to lsa origination procedure */
#define  SIG_LS_REFRESH                 0                                  
#define  SIG_IF_STATE_CHANGE            1                                  
#define  SIG_DR_CHANGE                  2                                  
#define  SIG_NBR_STATE_CHANGE           3                                  
#define  SIG_INTRA_AREA_CHANGE          4                                  
#define  SIG_INTER_AREA_CHANGE          5                                  
#define  SIG_NEW_AREA_ATTACHED          6                                  
#define  SIG_EXT_ROUTE_ADDED            7                                  
#define  SIG_HOST_ATTACHED              8                                  
#define  SIG_HOST_DETACHED              9                                  
#define  SIG_DEFAULT_SUMMARY            10                                 
#define  SIG_VIRT_LINK_COST_CHANGE      11                                 
#define  SIG_NEXT_INSTANCE              12                                 
#define  SIG_IF_METRIC_PARAM_CHANGE     13                                 

#define  SIG_INDICATION                 14                                 

#define    SIG_OPQ_LSA_GEN              15
#define    SIG_OPQ_LSA_NEXT_INSTANCE    16
#define    SIG_DEFAULT_NSSA             17

/****************************************************************************/
/* Routing Table related constants                                          */
/****************************************************************************/
                                    
/* Types of destinations in routing table. */
#define  DEST_NETWORK                   1                                  
#define  DEST_AREA_BORDER               2
#define  DEST_AS_BOUNDARY               3                                  
#define  MAX_DEST_TYPE                  3  

/* Types of paths in routing table */
#define  INTRA_AREA                     1                                  
#define  INTER_AREA                     2                                  
#define  TYPE_1_EXT                     3                                  
#define  TYPE_2_EXT                     4                                 
#define  INVALID_ROUTE                  5
#define  MAX_PATH_TYPE                  5 

/* constants for vertex types in candidate list */
#define  VERT_NETWORK                   0                                  
#define  VERT_ROUTER                    1                                  


#define  RT_CAL_INTER_AREA_ROUTE        1                                  
#define  EXMN_TRNST_SMMRY_LNKS          2                                  
#define  MAX_RT_FOR_UPDATE_DISTANCE     500
/****************************************************************************/
/* Miscellaneous                                                            */
/****************************************************************************/

#define  VIRTUAL_IF_RXMT_INTERVAL       5  

/* constant to specify default tos */
#define  TOS_0                          0                                  

/* constant to specify the router is ineligible for DR election */
#define  INELIGIBLE_RTR_PRIORITY        0                                  

#define  GAUGE_MAX                      4294967295UL                       

                                    
#define  TEN_POWER_EIGHT                100000000                          

#define  NO_LIMIT                       -1                                 

#define  DONT_LEAVE_OVERFLOW_STATE       0                                  

/* flags used in packet transmission */
#define  NO_RELEASE                     0                                  
#define  RELEASE                        1                                  

/* flags used when received packets are freed,after processing*/
#define  NORMAL_RELEASE                 0                                  
#define  FORCED_RELEASE                 1                                  

/* router-id selection process flags */
#define  ROUTERID_DYNAMIC               1                                  
#define  ROUTERID_PERMANENT             2                                  
/* Constants are used in manipulating the number of days in a year */
enum {
OSPF_MAX_MONTH_DAYS = 31,
OSPF_MIN_MONTH_DAYS = 30,
OSPF_MAX_LEAP_FEB_DAYS = 29,
OSPF_MIN_LEAP_FEB_DAYS = 28,
OSPF_MAX_MAR_DAYS = 62,
OSPF_MAX_APR_DAYS = 92,
OSPF_MAX_MAY_DAYS = 123,
OSPF_MAX_JUN_DAYS = 153,
OSPF_MAX_JUL_DAYS = 184,
OSPF_MAX_AUG_DAYS = 215,
OSPF_MAX_SEP_DAYS = 245,
OSPF_MAX_OCT_DAYS = 276,
OSPF_MAX_NOV_DAYS = 306,
OSPF_MAX_MIN = 59,
OSPF_MAX_HOUR = 23
};

#define OSPF_IS_MULTICAST_ADDR(u4FutOspfRRDRouteDest) ((u4FutOspfRRDRouteDest >= 0xe0000000))
#define OSPF_IS_HOST_ADDR(u4FutOspfRRDRouteDest) ((u4FutOspfRRDRouteDest) >> 24 == 0) 
/* constants used in comparison functions */
enum {
OSPF_EQUAL = 0,
OSPF_GREATER,
OSPF_LESS,
NOT_EQUAL,
NOT_VALID
};

#define  RCVD_LSA                       1                                  
#define  DB_LSA                         2                                  

#define  TYPE_1_METRIC                  1                                  
#define  TYPE_2_METRIC                  2                                  

/* Constants used in Ext RT Calc */ 
#define OSPF_ZERO                            0
#define OSPF_NEW_RT_ENTRY                    1
#define OSPF_NEW_PATH                        1  
#define OSPF_REPLACE_PATH                    2
#define OSPF_ADD_TO_LIST_OF_PATH             3 
#define OSPF_NO_NEW_PATH                     4  
#define OSPF_OLD_PATH                        0
#define OSPF_ROUTE_UNUSED                    1
#define OSPF_ROUTE_USED                      2
#define OSPF_ROUTE_ADDED                     3
#define OSPF_ROUTE_MODIFIED                  4
#define OSPF_PRINT_BUF_SIZE                  16
/* Flags used to distinguish between the nodes in SPF Tree */
#define OSPF_SPF_UNUSED    1
#define OSPF_SPF_ADDED     2
#define OSPF_SPF_MODIFIED  3
 
/* flags passed to LsuAddToLsu() */
#define  TX_ONCE                        1                                  
#define  TX_MANY                        2                                  

#define  TRAP_LIMIT_TIMER_INTERVAL      (NO_OF_TICKS_PER_SEC * 10)         
                                            /* SWP - sliding window period */
#define  ALLOWABLE_TRAP_COUNT_PER_SWP   7                                  
#define  OVERFLOW_APPROACHING_LIMIT_IN_CXT(pOspfCxt) \
                           ((9 * pOspfCxt->i4ExtLsdbLimit) / 10)
                                            /* ninety percent of the limit */

/* Values used in Trie */
#define  OSPF_RT_TYPE                   2                                  
#define  OSPF_CAL_RT_TYPE               3                                  
#define  OSPF_EXT_RT_TYPE               4
  
#define OSPF_RT_TYPE_IN_CXT(pOspfCxt)\
                  (OSPF_RT_TYPE | (pOspfCxt->u4OspfCxtId << 16))

#define OSPF_CAL_RT_TYPE_IN_CXT(pOspfCxt)\
                  (OSPF_CAL_RT_TYPE | (pOspfCxt->u4OspfCxtId << 16))

  
#define OSPF_EXT_RT_TYPE_IN_CXT(pOspfCxt)\
                  (OSPF_EXT_RT_TYPE | (pOspfCxt->u4OspfCxtId << 16))
/* value returned by lsa_process_lsa() */
#define  DISCARDED                      1                                  

/* values returned by LsuAddToLsu */
#define  LSU_TXED                       1                                  
#define  LSA_ADDED                      2                                  

/* constants used to identify the type of ack to be sent
   REFerence : RFC-1247 Section 13.5                    */
#define  UNUSED_ACK_FLAG                0                                  
#define  IMPLIED_ACK                    1                                  
#define  NO_IMPLIED_ACK                 2                                  
#define  FLOOD_BACK                     3                                  
#define  NO_FLOOD_BACK                  4                                  

/* values used in HpRcvHello() */
#define  ISM_SCHEDULED                  1                                  
#define  ISM_NOT_SCHEDULED              0                                  

/* constants used in osif.c */
#define  OSPF_DEF_HELLO_INTERVAL        10                                
#define  DEF_VIRT_IF_RTR_DEAD_INTERVAL  60     

#define  DEF_RTR_DEAD_INTERVAL          40                                 
#define  DEF_IF_TRANS_DELAY             1                                  
#define  DEF_RTR_PRIORITY               1                                  
#define  DEF_RXMT_INTERVAL              5                                  
#define  DEF_POLL_INTERVAL              120                                

#define NSSA_TRNSLTR_STBLTY_INTRVL  40  

/* Constants used for route calculation staggering */
#define OSPF_STAGGERING_ENABLED  1
#define OSPF_STAGGERING_DISABLED 2

#define OSPF_MIN_STAGGERING_INTERVAL 1000 /* milliseconds */
#define OSPF_MAX_STAGGERING_INTERVAL 2147483647 /* milliseconds */
#define OSPF_DEF_STAGGERING_INTERVAL 10000 /*milliseconds*/

/* constants used in setting flags during DR election */
#define  ALL_RTRS                       0                                  
#define  ONLY_BDRS                      1                                  

/* constants used in oslsu.c */
#define  LSA_EQUAL                      0                                  
#define  LSA1                           1                                  
#define  LSA2                           2                                  

/* constants used in osnbr.c */
#define  DEF_NBR_PRIORITY               1                                  

#define  STATIC_RT                      3                                  
#define  DELETE_EXT_ROUTE               1                                 
#define  DONT_DELETE_EXT_ROUTE          2                                  
#define  AS_EXT_DEF_METRIC              10                                 

/* Masks for route aggregation*/
#define RAG_TRANS_MASK    2  /* 00000010   */
#define RAG_ADV_MASK    4  /* 00000100   */
#define RAG_DONOTADV_MASK   8  /* 00001000  */
#define RAG_ALLOW_MASK   16  /* 00010000  */
#define RAG_DENY_MASK    32     /* 00100000   */
#define  RAG_TRANS_RESET    253      /* 11111101    */
#define RAG_EFFECT_FND    60  /* 00111100    */

#define RAG_ADVERTISE    1
#define RAG_DO_NOT_ADVERTISE  2
#define RAG_ALLOW_ALL    3
#define RAG_DENY_ALL    4
#define RAG_NO_CHNG     0

#define OSPF_MUT_EXCL_SEM_NAME  ((UINT1 *)"OMES")
#define OSPF_RTMRT_LIST_SEM     ((UINT1 *)"ORTMLST")

/* constants and macros related to OSPF-RTM interaction */


#define  MANUAL_TAG                     1                                  
#define  AUTOMATIC_TAG                  2                                  

#define  MANUAL_TAG_MASK                0xffffffff                         

/* Bit Mask values for Route Change Notification Message */
#define  RT_ROWSTATUS_MASK              RTM_ROW_STATUS_MASK
#define  RT_METRIC_MASK                 RTM_METRIC_MASK
#define  RT_IFINDX_MASK                 RTM_IFINDEX_MASK                    

#define  LOCAL_RT                       2                                  
#define  RIP_RT                         8                                  
#define  BGP_RT                         14                                 
#define  ISIS_RT                        9                                 

#define  LOCAL_RT_MASK                  0x00000002     /* Bit 1 */
#define  STATIC_RT_MASK                 0x00000004     /* Bit 2 */
#define  RIP_RT_MASK                    0x00000080     /* Bit 7 */
#define  BGP_RT_MASK                    0x00002000     /* Bit 13 */
#define  ISISL1_RT_MASK                0x00004000     /* Bit 16 */
#define  ISISL2_RT_MASK                0x00008000     /* Bit 13 */
#define  ISIS_RT_MASK                   0x0000c000     /* Bit 13 */


#define  MAX_PROTO_REDISTRUTE_SIZE               5
#define  REDISTRUTE_ARR_BGP         1
#define  REDISTRUTE_ARR_RIP         2
#define  REDISTRUTE_ARR_CONNECTED         3
#define  REDISTRUTE_ARR_STATIC         4
#define  REDISTRUTE_ARR_ISIS         5


#define   RRD_SRC_PROTO_BIT_MASK \
                 ( LOCAL_RT_MASK | STATIC_RT_MASK | RIP_RT_MASK | BGP_RT_MASK  | ISIS_RT_MASK)

#define   IS_VALID_RRD_SRC_PROTO_ENABLE_MASK_VALUE(x) \
                          ( (x == 0) || !((x) & ~(RRD_SRC_PROTO_BIT_MASK)) )

#define   IS_VALID_RRD_SRC_PROTO_DISABLE_MASK_VALUE(x) \
                          ( (x != 0) && !((x) & ~(RRD_SRC_PROTO_BIT_MASK)) )




#define  DONT_REDISTRIBUTE          0x02 
#define  ACK_REQD_FROM_RTM          0x80 


#define DIRECT_ROUTE     3
#define INDIRECT_ROUTE   4

#define OSPF_ROUTE_RTTYPE     1      
#define OSPF_ROUTE_AREAID     2      
#define OSPF_ROUTE_COST       3      
#define OSPF_ROUTE_TYPE2COST  4      
#define OSPF_ROUTE_IFACEINDEX 5     
#define OSPF_DEFAULT_COST     1

/* Dead interval (1 - 65535) */
#define OSPF_MAX_DEAD_INTERVAL 65535
#define OSPF_MIN_DEAD_INTERVAL 1


/* SPF_HOLDTIME and SPF_DELAY in milliseconds */
#define OSPF_DEF_SPF_HOLDTIME   10 /* 10 milli seconds */
#define OSPF_MAX_SPF_HOLDTIME   65535
#define OSPF_MIN_SPF_HOLDTIME   0
#define OSPF_MAX_SPF_DELAY      65535
#define OSPF_MIN_SPF_DELAY      0
#define OSPF_DEF_SPF_DELAY      1  /* 1 milli seconds */
#define OSPF_NO_SPF_DELAY      0
#define OSPF_DEF_VRF_SPF_INTERVAL     10 /* 10 milli seconds */

/* Minimum VRF SPF Interval Time in milli seconds */
#define OSPF_MIN_VRF_SPF_INTERVAL  0     
/* Maximum VRF SPF Interval Time in milli seconds */
#define OSPF_MAX_VRF_SPF_INTERVAL  1000  
/* (Per Context )Timer Interval in seconds for sending
 *  OSPF Rxmt Node Allocation failure event to OSPF Task
 */
#define OSPF_RXMT_FAIL_INTERVAL   10 /* 10 seconds */

/*converting milliseconds to STUPS */ 
#define OSPF_TIME_IN_STUPS(i4MilliSeconds) ((i4MilliSeconds * NO_OF_TICKS_PER_SEC) / 1000)

/* Macros used by restarting router during graceful restart */
/* Graceful restart support */
#define OSPF_RESTART_NONE       1     /* Not supported */
#define OSPF_GR_UNPLANNED_SHUTDOWN 2  /* OSPF graceful unplanned shutdown*/
#define OSPF_RESTART_PLANNED    2     /* Supports only planned restart */
#define OSPF_RESTART_BOTH       3     /* Supports both planned and unplanned */

/* Last Graceful restart operation status */
/* OSPF_RESTART_NONE is used to indicate that no restart process has
 * been attempted */
#define OSPF_RESTART_INPROGRESS 2 /* The router is undergoing
                                   * graceful restart */
#define OSPF_RESTART_COMPLETED  3 /* Last restart attempt is successful */
#define OSPF_RESTART_TIMEDOUT   4 /* Last restart process has been timed
                                   * out */
#define OSPF_RESTART_TOP_CHG    5 /* Last restart process has been exited
                                   * abnormally due to topology change */
#define OSPF_RESTART_TERM_BFD_DOWN  OSPF_RESTART_TOP_CHG 
                                   /* OSPF Graceful restart is aborted due to BFD down message
                                    * hence terminating graceful restart by sending
                                    * topology change notification.
                                    */

/* Current graceful restart operation */
/* OSPF_RESTART_NONE and OSPF_RESTART_PLANNED are used to indicate
 * that currently no graceful restart process is initiated and 
 * planned restart is in progress respectively */
#define OSPF_RESTART_UNPLANNED  3 /* Unplanned restart is in progress */

/* OSPF restart state */
#define OSPF_GR_NONE              0 /* Normal OSPF operation */
#define OSPF_GR_SHUTDOWN          1 /* OSPF graceful shutdown */
#define OSPF_GR_RESTART           2 /* OSPF graceful restart */

/* Graceful restart reason */
#define OSPF_GR_UNKNOWN           0
#define OSPF_GR_SW_RESTART        1
#define OSPF_GR_SW_UPGRADE        2
#define OSPF_GR_SW_RED            3
#define OSPF_DEFAULT_REASON       OSPF_GR_UNKNOWN


/* Grace interval */
#define OSPF_GR_MIN_INTERVAL      1
#define OSPF_GR_MAX_INTERVAL      1800
#define OSPF_GR_DEF_INTERVAL      120
#define OSPF_DEF_HELPER_GR_TIME_LIMIT   0

/* Size of TLV in Grace LSA */
#define GR_TLV_SIZE               8

/* TLV value used in Grace LSA */
/* Grace period TLV */
#define GR_PERIOD_TLV_TYPE        (UINT2)1
#define GR_PERIOD_TLV_LENGTH      (UINT2)4
/* Restart reason TLV */
#define GR_REASON_TLV_TYPE        (UINT2)2
#define GR_REASON_TLV_LENGTH      (UINT2)1
/* IP address TLV */
#define GR_ADDRESS_TLV_TYPE       (UINT2)3
#define GR_ADDRESS_TLV_LENGTH     (UINT2)(MAX_IP_ADDR_LEN)

/* Opaque application ID for TE */
#define OSPF_TE_OPQ_TYPE            1
/* Opaque type for grace LSA */
#define  GRACE_LSA_OPQ_TYPE       3    /* RFC 3623 Appendix A */

/* Link state ID for Grace LSA. Opaque Type = 3 and Opaque Id = 0 */
#define OSPF_GR_LSID              0x03000000
 
/* GR related configuration file */
#define OSPF_GR_CONF              (const UINT1 *)"ospfgr.conf"

/* Helper related macros */
#define OSPF_GR_NOT_HELPING       1
#define OSPF_GR_HELPING           2

/* Helper Exit Reason */
#define OSPF_HELPER_NONE              1
#define OSPF_HELPER_INPROGRESS        2
#define OSPF_HELPER_COMPLETED         3
#define OSPF_HELPER_GRACE_TIMEDOUT    4
#define OSPF_HELPER_TOP_CHG           5

#define RB_EQUAL 0
#define RB_LESSER -1
#define RB_GREATER 1


#define OSPF_NO_TOPOLOGY_CHANGE      1
#define OSPF_TOPOLOGY_CHANGE         0



/* Bit position in LSA refresh flag */
#define OSPF_GR_LSA_BIT           1

#define OSPF_GRACE_NO_HELPER                      0x00
#define OSPF_GRACE_UNKNOWN                        0x01
#define OSPF_GRACE_SW_RESTART                     0x02
#define OSPF_GRACE_SW_RELOADUPGRADE               0x04
#define OSPF_GRACE_SW_REDUNDANT                   0x08
#define OSPF_GRACE_HELPER_ALL                     0xF


#define OSPF_GR_HELPER_MIN_INTERVAL      0
#define OSPF_GR_HELPER_MAX_INTERVAL      1800

#define OSPF_SI_MODE   VCM_SI_MODE
#define OSPF_MI_MODE   VCM_MI_MODE

#define DEF_GRACE_LSA_SENT   2  /*Default grace LSA can be sent out*/ 

/* Following macros are used for redundancy support.
   If the redundacy is not defined some of the values are 
   used for return the redundancy MIB objects values.
*/
enum {

         OSPF_RED_INIT = 1,           /* The OSPF instance is up */

         OSPF_RED_ACTIVE_STANDBY_UP, /* The current OSPF instance is up and
                                        functioning as active node and its peer
                                        is functioning as the standby */

         OSPF_RED_ACTIVE_STANDBY_DOWN, /* The current OSPF instance is up
                                          and functioning as active node but
                                          its peer is down */

         OSPF_RED_STANDBY              /* The node is in standby state */
};


#define OSPF_RED_BLKUPDT_NOT_STARTED 1 /* Bulk Update is not started*/ 
#define OSPF_RED_BLKUPDT_INPROGRESS 2 /* Bulk Update is in progress*/
#define OSPF_RED_BLKUPDT_COMPLETED 3 /* Bulk Update is not completed */
#define OSPF_RED_BLKUPDT_ABORTED 4 /* Bulk update is failed */

#define OSPF_RED_ADMIN_ENABLE  1 /* Redundancy is enabled */ 
#define OSPF_RED_ADMIN_DISABLE  2 /* REdundancy is disable */

/* CMSG Data Buffer related macros */
#define OSPF_CMSG_DATA_LEN          24

/* Time Buffer related macros */
#define OSPF_TMP_NUM_STR            7
#define OSPF_INVALID_HOUR   8
#define OSPF_INVALID_MIN    7
#define OSPF_INVALID_SEC    6
#define OSPF_INVALID_DATE   5
#define OSPF_INVALID_MONTH  4
#define OSPF_INVALID_YEAR   3
#define OSPF_ONE            1
#define OSPF_BASE_YEAR       2000
#endif /* _OSDEFN_H */
#ifdef OSMAIN_C
UINT4 gu4UtilOspfIsLowPriorityMessageFail  =  0;  
UINT4 gu4OspfMemReleaseMemBlockFail  =  0;   
UINT4 gu4UtilOspfGetCxtFail  =  0;    
UINT4 gu4UtilSelectOspfRouterIdFail  =  0;   
UINT4 gu4NetIpv4GetCfaIfIndexFromPortFail  =  0;   
UINT4 gu4VcmIsL3VcExistFail  =  0;    
UINT4 gu4UtilOspfVcmIsVcExistFail  =  0;    
UINT4 gu4IfSetOperStatFail  =  0;    
UINT4 gu4OSPFSetIfMtuSizeInCxtFail  =  0;   
UINT4 gu4OspfSnmpIfMsgAllocFail  =  0;    
UINT4 gu4OspfSnmpIfMsgSendFail  =  0;    
UINT4 gu4OSPFSetIfSpeedInCxtFail  =  0;   
UINT4 gu4RtrDisableInCxtFail  =  0;    
UINT4 gu4FsNpOspfCreateAndDeleteFilterFail  =  0;  
UINT4 gu4GetFindRrdConfRtInfoInCxtFail  =  0;   
UINT4 gu4IfacesDelFail  =  0;     
UINT4 gu4OspfCreateSocketFail  =  0;    
UINT4 gu4AppAllocFail  =  0;     
UINT4 gu4AppOpqtypeFail  =  0;    
UINT4 gu4AppInfoInCxtFail  =  0;    
UINT4 gu4pOspfCxtFail  =  0;     
UINT4 gu4RtmTxRedistributeDisableInCxtFail  =  0;   
UINT4 gu4CruBufAllocMsgBufChainFail  =  0;   
UINT4 gu4IpCopyToBufFail  =  0;    
UINT4 gu4RpsEnqueuePktToRtmFail  =  0;   
UINT4 gu4RtrShutdownInCxtFail  =  0;    
UINT4 gu4OspfCxtIdFail  =  0;     
UINT4 gu4RtrRebootInCxtFail  =  0;    
UINT4 gu4RmApiHandleProtocolEventFail  =  0;   
UINT4 gu4RmReleaseMemoryForMsgFail  =  0;   
UINT4 gu4RmEnqMsgToRmFromApplFail  =  0;   
UINT4 gu4UtilOspfGetFirstCxtIdFail  =  0;    
UINT4 gu4UtilOspfGetNextCxtIdFail  =  0;   
UINT4 gu4UtilOsMsgAllocFail  =  0;    
UINT4 gu4OspfRmSendBulkUpdateFail  =  0;   
UINT4 gu4OspfRmAllocateBulkUpdatePktFail  =  0;   
UINT4 gu4OspfRmSendMsgToRmFail  =  0;   
UINT4 gu4OsRmBulkUpdtRelinquishFail  =  0;   
#else
 extern UINT4 gu4UtilOspfIsLowPriorityMessageFail;  
 extern UINT4 gu4OspfMemReleaseMemBlockFail;   
 extern UINT4 gu4UtilOspfGetCxtFail;    
 extern UINT4 gu4UtilSelectOspfRouterIdFail;   
 extern UINT4 gu4NetIpv4GetCfaIfIndexFromPortFail;   
 extern UINT4 gu4VcmIsL3VcExistFail;    
 extern UINT4 gu4UtilOspfVcmIsVcExistFail;    
 extern UINT4 gu4IfSetOperStatFail;    
 extern UINT4 gu4OSPFSetIfMtuSizeInCxtFail;   
 extern UINT4 gu4OspfSnmpIfMsgAllocFail;    
 extern UINT4 gu4OspfSnmpIfMsgSendFail;    
 extern UINT4 gu4OSPFSetIfSpeedInCxtFail;   
 extern UINT4 gu4RtrDisableInCxtFail;    
 extern UINT4 gu4FsNpOspfCreateAndDeleteFilterFail;  
 extern UINT4 gu4GetFindRrdConfRtInfoInCxtFail;   
 extern UINT4 gu4IfacesDelFail;     
 extern UINT4 gu4OspfCreateSocketFail;    
 extern UINT4 gu4AppAllocFail;     
 extern UINT4 gu4AppOpqtypeFail;    
 extern UINT4 gu4AppInfoInCxtFail;    
 extern UINT4 gu4pOspfCxtFail;     
 extern UINT4 gu4RtmTxRedistributeDisableInCxtFail;   
 extern UINT4 gu4CruBufAllocMsgBufChainFail;   
 extern UINT4 gu4IpCopyToBufFail;    
 extern UINT4 gu4RpsEnqueuePktToRtmFail;   
 extern UINT4 gu4RtrShutdownInCxtFail;    
 extern UINT4 gu4OspfCxtIdFail;     
 extern UINT4 gu4RtrRebootInCxtFail;    
 extern UINT4 gu4RmApiHandleProtocolEventFail;   
 extern UINT4 gu4RmReleaseMemoryForMsgFail;   
 extern UINT4 gu4RmEnqMsgToRmFromApplFail;   
 extern UINT4 gu4UtilOspfGetFirstCxtIdFail;    
 extern UINT4 gu4UtilOspfGetNextCxtIdFail;   
 extern UINT4 gu4UtilOsMsgAllocFail;    
 extern UINT4 gu4OspfRmSendBulkUpdateFail;   
 extern UINT4 gu4OspfRmAllocateBulkUpdatePktFail;   
 extern UINT4 gu4OspfRmSendMsgToRmFail;     
 extern UINT4 gu4OsRmBulkUpdtRelinquishFail;   
#endif
