/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osglob.h,v 1.20 2013/12/18 12:51:09 siva Exp $
 *
 * Description:This file contains all global variable  PUBLICal 
 *             declarations
 *
 *******************************************************************/
#ifndef _OSGLOB_H
#define _OSGLOB_H


PUBLIC tIPADDR           gAllSpfRtrs;
PUBLIC tIPADDR           gAllDRtrs;
PUBLIC tIPADDR           gNullIpAddr;
PUBLIC tIPADDR           gDefaultDest;
PUBLIC tIPADDRMASK      gHostRouteMask;
PUBLIC tAreaId           gBackboneAreaId;
PUBLIC UINT1             gu1DefCostCiscoCompat ;  /* This Flag will indicate the cost of nssa/stub to be align
                                                   with cisco / standard */                  
PUBLIC tOsRtr            gOsRtr;
#ifdef BFD_WANTED
PUBLIC tBfdReqParams     gBfdInParams;
#endif
PUBLIC UINT4  gOspf1sTimer;
PUBLIC tOsixSemId    gOspfSemId;
PUBLIC INT4  gi4OspfSysLogId;
/*GR Global Varibales*/

PUBLIC UINT4 gu4CsrFlag;
PUBLIC UINT4 gu4RestoreFlag;

/* RMap global variable */
PUBLIC tOsixMsg *gOsRmapMsg[MAX_OSPF_RMAP_MSGS];

/* tmrif specific global varibale */
PUBLIC tTimerListId gTimerLst;
PUBLIC tTimerListId gOnesecTimerLst;
PUBLIC tTimerListId gHelloTimerLst;
PUBLIC tTmrAppTimer gOnesecTimerNode;

/* bufif specific global varibale */

PUBLIC tBufQid           gBufQid;    /* qids of various CRU buffer pools */

PUBLIC INT1                gGaugeOpValue;
 
PUBLIC FILE                *gp_ospf_output_file;

/* This variable is for the global Trace log file & defined in cru_trc_log.c */
PUBLIC FILE                *pTRC_LogFile;

/* Global variables which are used for debugging & Trace log */
PUBLIC UINT1  au1DbgTrapId[18][40];
PUBLIC UINT1 au1DbgIfType[MAX_IF_TYPE][30];
PUBLIC UINT1 au1DbgIfState[MAX_IF_STATE][12];
PUBLIC UINT1 au1DbgIfEvent[MAX_IF_EVENT][30];
PUBLIC UINT1 au1DbgNbrState[MAX_NBR_STATE][12];
PUBLIC UINT1 au1DbgNbrEvent[MAX_NBR_EVENT][30];
PUBLIC UINT1 au1DbgRtDestType[MAX_RT_DST_TYPE][30];
PUBLIC UINT1 au1DbgRtPathType[MAX_RT_PATH_TYPE][30];
PUBLIC UINT1 au1DbgTimerId[MAX_TIMERS][30];
PUBLIC UINT1 au1DbgNbrCfgStatus[MAX_NBR_CFG_STATUS][30];
PUBLIC UINT1 au1DbgLsaType[MAX_DBG_LSA_TYPE][30];
PUBLIC UINT1 au1DbgPktType[MAX_PKT_TYPE][30];
PUBLIC UINT1 au1DbgAbrType[MAX_ABR_TYPE][30];

PUBLIC struct TrieAppFns  OspfTrieLibFuncs;

#endif
