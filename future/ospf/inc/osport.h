/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osport.h,v 1.15 2016/03/03 10:54:56 siva Exp $
 *
 * Description:Contains the global definitions 
 *             and prototypes to be used by the
 *             external modules.               
 *
 *******************************************************************/
#ifndef  _BUFWRAP_H

#define  _BUFWRAP_H

/*******************************************************
*** MACROs For Host To Network Byte Order Conversion ***
********************************************************/
#define OSPF_CRU_BMC_WTOPDU(pu1PduAddr,u2Value) \
        *((UINT2 *)(VOID *)(pu1PduAddr)) = OSIX_HTONS(u2Value);

#define OSPF_CRU_BMC_DWTOPDU(pu1PduAddr,u4Value) \
         UtilOspfDwordToPdu (pu1PduAddr,u4Value)

/*******************************************************
*** MACROs For Network To Host Byte Order Conversion ***
********************************************************/
#define OSPF_CRU_BMC_WFROMPDU(pu1PduAddr) \
        OSIX_NTOHS(*((UINT2 *)(VOID *)(pu1PduAddr)))

#define OSPF_CRU_BMC_DWFROMPDU(pu1PduAddr) \
            UtilOspfDwordFromPdu (pu1PduAddr)

#define OSPF_CRU_BMC_GET_1_BYTE(pMsg, u4Offset, u1Value) \
   CRU_BUF_Copy_FromBufChain(pMsg, &u1Value, u4Offset, 1)

#define OSPF_CRU_BMC_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define OSPF_CRU_BMC_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define OSPF_CRU_BMC_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,u4_StringLength) \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1_StringMemArea, u4Offset, u4_StringLength);\
}

#define OSPF_CRU_BMC_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
   UINT1  LinearBuf = (UINT1) u1Value;\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 1);\
}

#define OSPF_CRU_BMC_ASSIGN_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   UINT2  LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 2);\
}

#define OSPF_CRU_BMC_ASSIGN_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   UINT4  LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 4);\
}

#define OSPF_CRU_BMC_ASSIGN_STRING(pBufChain,pu1_StringMemArea,u4Offset,u4_StringLength) \
{\
   CRU_BUF_Copy_OverBufChain(pBufChain, pu1_StringMemArea, u4Offset, u4_StringLength);\
}

#define OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU(pBufChain,u4Offset,u1Value)  \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, &u1Value, u4Offset, 1);\
}

#define OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU(pBufChain,u4Offset,u2Value)  \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU(pBufChain,u4Offset,u4Value)  \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU(pBufChain,pu1StringMemArea,u4Offset,u4StringLength) \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1StringMemArea, u4Offset, u4StringLength);\
} 

#define OSPF_CRU_BUF_DELETE_CHAIN_ENDING_DATA   CRU_BUF_Delete_BufChainAtEnd

#define OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN(pChainHdr,u1Char,u4Offset,u4RepCount) \
{ \
   UINT1 u1LoopCountVar = 0; \
   UINT1 u1CharValue = u1Char; \
   UINT4 u4OffsetCopy = u4Offset; \
   for(u1LoopCountVar = 0; u1LoopCountVar < u4RepCount; u1LoopCountVar++){ \
      CRU_BUF_Copy_OverBufChain((tCRU_BUF_CHAIN_HEADER *)pChainHdr,(UINT1 *)&(u1CharValue),u4OffsetCopy,1); \
      u4OffsetCopy++; \
   } \
}

#define OSPF_CRU_BMC_APPEND_STRING_IN_CRU(pBufChain,pu1StringMemArea,u4Offset,u4StringLength) \
        CRU_BUF_Copy_OverBufChain(pBufChain, pu1StringMemArea, u4Offset, u4StringLength); 

#define OSPF_CRU_BMC_Set_InterfaceId(pMsg,tInterface) \
        CRU_BUF_Set_InterfaceId(pMsg,tInterface)

#define OSPF_CRU_BMC_Set_SourceModuleId(pBufChain,u1ModuleId) \
        CRU_BUF_Set_SourceModuleId(pBufChain,u1ModuleId)

#define OSPF_CRU_BMC_Set_DestinModuleId(pBufChain,u1ModuleId) \
        CRU_BUF_Set_DestinModuleId(pBufChain,u1ModuleId)

#define OSPF_CRU_BUF_ALLOCATE_FREE_OBJ(PoolId,pu1Block,type) \
        (pu1Block = (type *)(MemAllocMemBlk ((tMemPoolId)PoolId)))

#define OSPF_CRU_BUF_RELEASE_FREE_OBJ(PoolId,ppu1Block) \
        OspfMemReleaseMemBlock((tMemPoolId)PoolId,(UINT1 *)ppu1Block)

#define OSPF_CRU_BMC_Get_InterfaceId(pPkt)  \
        CRU_BUF_Get_InterfaceId(pPkt)

/* extract the two bytes from the linear buffer */
#define OSPF_BUFFER_EXTRACT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   MEMCPY(((UINT1 *) &u2Value), (pMsg + u4Offset), 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define OSPF_INIT_COMPLETE(u4Status)       lrInitComplete(u4Status)

#define  VERSION_NO_OFFSET  0  
#define  TYPE_OFFSET        1  
#define  PKT_LENGTH_OFFSET  2  
#define  RTR_ID_OFFSET      4  
#define  AREA_ID_OFFSET     8  
#define  CHKSUM_OFFSET      12 
#define  AUTH_TYPE_OFFSET   14 
#define  AUTH_1_OFFSET      16 
#define  AUTH_2_OFFSET      18 
#define  AUTH_3_OFFSET      19 
#define  AUTH_4_OFFSET      20 

#define  DDP_OPTIONS_OFFSET           26 
#define  DDP_MASK_OFFSET              27 
#define  DDP_SEQ_NUM_OFFSET           28 
#define  DDP_SUMMARY_LST_OFFSET       32 

#define  HP_HELLO_INTERVAL_OFFSET     28 
#define  HP_OPTIONS_OFFSET            30 
#define  HP_RTR_PRI_OFFSET            31 
#define  HP_RTR_DEAD_INTERVAL_OFFSET  32 
#define  HP_DSG_RTR_OFFSET            36 
#define  HP_BK_DSG_RTR_OFFSET         40 
#define  HP_NBR_ID_OFFSET             44 

#define  LRQ_TYPE_OFFSET            24 
#define  LRQ_STATE_ID_OFFSET        28 
#define  LRQ_ADV_RTRID_OFFSET       32 

#define  DDP_LSA_AGE_OFFSET         32 
#define  DDP_LSA_OPTIONS_OFFSET     34 
#define  DDP_LSA_TYPE_OFFSET        35 
#define  DDP_LSA_ID_OFFSET          36 
#define  DDP_LSA_ADV_RTR_ID_OFFSET  40 
#define  DDP_LSA_SEQ_NUM_OFFSET     44 
#define  DDP_LSA_CHKSUM_OFFSET      48 
#define  DDP_LSA_LEN_OFFSET         50 

#define  LSA_AGE_OFFSET             24 
#define  LSA_OPTIONS_OFFSET         26 
#define  LSA_TYPE_OFFSET            27 
#define  LSA_ID_OFFSET              28 
#define  LSA_ADV_RTR_ID_OFFSET      32 
#define  LSA_SEQ_NUM_OFFSET         36 
#define  LSA_CHKSUM_OFFSET          40 
#define  LSA_LEN_OFFSET             42 

#define  OSPF_MEM_POOL_BLOCK_SIZE    64  
#define  OSPF_MEM_POOL_NO_OF_BLOCKS  512 

/* for Testing on Dmond  */
#define  DMOND_PRINTF  dmond_console_print 

/* Macros for RAW sockets */
#define OSPF_SEND_TO           sendto 
#define OSPF_SEND_MSG          sendmsg
#define OSPF_SET_SOCK_OPT      setsockopt 
#define OSPF_RECV_FROM         recvfrom 
#define OSPF_RECV_MSG          recvmsg
#define OSPF_CLOSE             close 
#define OSPF_SOCKET            socket 
#define OSPF_SELECT            select 
#define OSPF_FCNTL             fcntl 

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#define   OSPF_VRF_IP_PKT_ARRIVAL_Q_NAME  "OSPFLNXVRF"
#define   OSPF_IP_PKT_ARRIVAL_Q_DEPTH     5000
#endif
        
#endif  /*  _BUFWRAP_H  */

