/********************************************************************
 *Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: fsosmiwr.h,v 1.5 2014/03/01 11:40:41 siva Exp $
 *
 *Description: This file has the handle routines for cli SET/GET
 *              destined for OSPF  Module
 *
 *
 **********************************************************************/

#ifndef _FSOSMIWR_H
#define _FSOSMIWR_H

INT4 GetNextIndexFsMIOspfBRRouteTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSOSMI(VOID);
VOID UnRegisterFSOSMI(VOID);
INT4 FsMIOspfBRRouteTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBRRouteAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBRRouteCostGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfBRRouteInterfaceIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIOspfExtRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfExtRouteMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteTagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteFwdAdrGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteNextHopGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteTagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteFwdAdrSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteNextHopSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteFwdAdrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteNextHopTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfExtRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIOspfGrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfGrShutdownGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGrShutdownSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfGrShutdownTest(UINT4 *, tSnmpIndex *, tRetVal *);

#endif /* _FSOSMIWR_H */
