/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdostdb.h,v 1.6 2008/08/20 15:20:08 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDOSTDB_H
#define _STDOSTDB_H


UINT4 stdost [] ={1,3,6,1,2,1,14,16};
tSNMP_OID_TYPE stdostOID = {8, stdost};


UINT4 OspfSetTrap [ ] ={1,3,6,1,2,1,14,16,1,1};
UINT4 OspfConfigErrorType [ ] ={1,3,6,1,2,1,14,16,1,2};
UINT4 OspfPacketType [ ] ={1,3,6,1,2,1,14,16,1,3};
UINT4 OspfPacketSrc [ ] ={1,3,6,1,2,1,14,16,1,4};


tMbDbEntry stdostMibEntry[]= {

{{10,OspfSetTrap}, NULL, OspfSetTrapGet, OspfSetTrapSet, OspfSetTrapTest, OspfSetTrapDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,OspfConfigErrorType}, NULL, OspfConfigErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,OspfPacketType}, NULL, OspfPacketTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,OspfPacketSrc}, NULL, OspfPacketSrcGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stdostEntry = { 4, stdostMibEntry };
#endif /* _STDOSTDB_H */

