/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistlw.h,v 1.5 2013/06/15 13:38:54 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIStdOspfTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfTable  */

INT1
nmhGetFirstIndexFsMIStdOspfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfRouterId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfAdminStat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVersionNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfAreaBdrRtrStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfASBdrRtrStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfExternLsaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfExternLsaCksumSum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfTOSSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfOriginateNewLsas ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfRxNewLsas ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfExtLsdbLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfMulticastExtensions ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfExitOverflowInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfDemandExtensions ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfRouterId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfAdminStat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfASBdrRtrStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfTOSSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfExtLsdbLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfMulticastExtensions ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfExitOverflowInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfDemandExtensions ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfRouterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfAdminStat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfTOSSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfExtLsdbLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfMulticastExtensions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfExitOverflowInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfDemandExtensions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfAreaTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfAreaTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfAreaTable  */

INT1
nmhGetFirstIndexFsMIStdOspfAreaTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfAreaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfImportAsExtern ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfSpfRuns ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfAreaBdrRtrCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfAsBdrRtrCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfAreaLsaCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfAreaLsaCksumSum ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfAreaSummary ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfAreaStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfImportAsExtern ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfAreaSummary ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfAreaStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfImportAsExtern ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfAreaSummary ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfAreaStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfAreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfStubAreaTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfStubAreaTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfStubAreaTable  */

INT1
nmhGetFirstIndexFsMIStdOspfStubAreaTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfStubAreaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfStubMetric ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfStubStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfStubMetricType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfStubMetric ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfStubStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfStubMetricType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfStubMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfStubStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfStubMetricType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfStubAreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfLsdbTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfLsdbTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfLsdbTable  */

INT1
nmhGetFirstIndexFsMIStdOspfLsdbTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfLsdbSequence ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfLsdbAge ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfLsdbChecksum ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfLsdbAdvertisement ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIStdOspfHostTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfHostTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfHostTable  */

INT1
nmhGetFirstIndexFsMIStdOspfHostTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfHostTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfHostMetric ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfHostStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfHostAreaID ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfHostMetric ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfHostStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfHostMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfHostStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfHostTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfIfTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfIfTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfIfTable  */

INT1
nmhGetFirstIndexFsMIStdOspfIfTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfIfAreaId ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfIfType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfAdminStat ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfRtrPriority ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfTransitDelay ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfRetransInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfHelloInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfPollInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfState ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfDesignatedRouter ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfIfBackupDesignatedRouter ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfIfEvents ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfIfAuthKey ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfIfStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfMulticastForwarding ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfDemand ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfAuthType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));


INT1
nmhGetFsMIStdOspfIfCryptoAuthType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfIfAreaId ARG_LIST((INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfIfType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfAdminStat ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfRtrPriority ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfTransitDelay ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfRetransInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfHelloInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfPollInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfAuthKey ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdOspfIfStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfMulticastForwarding ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfDemand ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfAuthType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfCryptoAuthType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfIfAreaId ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfIfType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfAdminStat ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfRtrPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfTransitDelay ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfRetransInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfHelloInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfPollInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdOspfIfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfMulticastForwarding ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfDemand ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfIfMetricTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfIfMetricTable ARG_LIST((INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfIfMetricTable  */

INT1
nmhGetFirstIndexFsMIStdOspfIfMetricTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfIfMetricTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfIfMetricValue ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfIfMetricStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfIfMetricValue ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfIfMetricStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfIfMetricValue ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfIfMetricStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfIfMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfVirtIfTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfVirtIfTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfVirtIfTable  */

INT1
nmhGetFirstIndexFsMIStdOspfVirtIfTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfVirtIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfVirtIfTransitDelay ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfRetransInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfHelloInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfState ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfEvents ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfVirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfVirtIfStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfAuthType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtIfCryptoAuthType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfVirtIfTransitDelay ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfVirtIfRetransInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfVirtIfHelloInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfVirtIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfVirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdOspfVirtIfStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfVirtIfAuthType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));


INT1
nmhSetFsMIStdOspfVirtIfCryptoAuthType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfVirtIfTransitDelay ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfVirtIfRetransInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfVirtIfHelloInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfVirtIfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfVirtIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdOspfVirtIfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfVirtIfAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfVirtIfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfVirtIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfNbrTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfNbrTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfNbrTable  */

INT1
nmhGetFirstIndexFsMIStdOspfNbrTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfNbrRtrId ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfNbrOptions ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfNbrPriority ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfNbrState ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfNbrEvents ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfNbrLsRetransQLen ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfNbmaNbrStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfNbmaNbrPermanence ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfNbrHelloSuppressed ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfNbrPriority ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfNbmaNbrStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfNbrPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfNbmaNbrStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfNbrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfVirtNbrTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfVirtNbrTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfVirtNbrTable  */

INT1
nmhGetFirstIndexFsMIStdOspfVirtNbrTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfVirtNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfVirtNbrIpAddr ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfVirtNbrOptions ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtNbrState ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfVirtNbrEvents ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfVirtNbrLsRetransQLen ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfVirtNbrHelloSuppressed ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdOspfExtLsdbTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfExtLsdbTable ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfExtLsdbTable  */

INT1
nmhGetFirstIndexFsMIStdOspfExtLsdbTable ARG_LIST((INT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfExtLsdbTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfExtLsdbSequence ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfExtLsdbAge ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfExtLsdbChecksum ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfExtLsdbAdvertisement ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIStdOspfAreaAggregateTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfAreaAggregateTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfAreaAggregateTable  */

INT1
nmhGetFirstIndexFsMIStdOspfAreaAggregateTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfAreaAggregateTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfAreaAggregateStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfAreaAggregateEffect ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfAreaAggregateStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfAreaAggregateEffect ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfAreaAggregateStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfAreaAggregateEffect ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfAreaAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
