/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osopqextrn.h,v 1.7 2007/02/01 15:00:38 iss Exp $
 *
 * Description:This file contains typedefintions and API
 *             prototypes defined by FutureOSPF to enable
 *             the Opaque Applicaitons interact with the
 *             FutureOSPF.
 *
 *******************************************************************/

#ifndef _OSOPQEXTRN_H
#define _OSOPQEXTRN_H

#define  MAX_IPV4_ADDR_LEN      4

#define  TYPE9_BIT_MASK                1    /* 00000001 */
#define  TYPE10_BIT_MASK               2    /* 00000010 */
#define  TYPE11_BIT_MASK               4    /* 00000100 */

#endif /*_OSOPQEXTRN_H */
/*---------------------------------------------------------------------------*/
/*                        End of file osopqextrn.h                           */
/*---------------------------------------------------------------------------*/

