/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oslow.h,v 1.16 2011/10/13 10:19:53 siva Exp $
 *
 * Description:This file contains snmp low level function
 *             prototypes.
 *
 *******************************************************************/
/*
 *  ProtoTypes for the Allocate and Freeing
 *  of Octet String and Object Identifiers.
 */
tSNMP_OID_TYPE * AllocOid ARG_LIST ((INT4));

tSNMP_OCTET_STRING_TYPE * AllocmemOctetstring ARG_LIST ((INT4));


void FreeOctetstring ARG_LIST ((tSNMP_OCTET_STRING_TYPE *));

void FreeOid ARG_LIST ((tSNMP_OID_TYPE *));

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfMaxExtLSAs ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfIfLrqRcvd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLrqTxed ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLrqDisd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLsuRcvd ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLsuTxed ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfIfLsuDisd ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfIfOperState ARG_LIST((UINT4 *, UINT4  , INT4  ,INT4 ));


/* Change in func name */


tAsExtAddrRange * GetFindAsExtRngInCxt 
        ARG_LIST ((tOspfCxt * pOspfCxt, tIPADDR asExtNet, tIPADDR asExtMask, tAreaId areaId));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOasAppRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOasAppDeRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOasAppOpqLsaSndFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOasAppRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOasAppDeRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOasAppOpqLsaSndFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOasAppRegFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOasAppDeRegFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOasAppOpqLsaSndFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));


/* Low level SET routines for graceful restart */

INT1
nmhGetFutOspfPreferenceValue ARG_LIST((INT4 *));
INT1
nmhSetFutOspfPreferenceValue ARG_LIST((INT4 ));
INT1
nmhTestv2FutOspfPreferenceValue ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhDepv2FutOspfPreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#ifdef __cplusplus
    }
#endif /* __cplusplus */
