/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osipprot.h,v 1.17 2016/03/03 10:54:56 siva Exp $
 *
 * Description:This file contains function prototypes of 
 *             IP interface functions.
 *
 *******************************************************************/
#ifndef _OSIPPROT_H
#define _OSIPPROT_H

/**************************************************************************/
/* The prototypes of the Functions which are provided by OSPF to IP */
 

PUBLIC INT4 IpifSendPktInCxt       PROTO((UINT4 u4ContextId,
                                       tCRU_BUF_CHAIN_HEADER* pPkt,
                                       UINT4 u4Port,
                                       tIPADDR*             pSrcIpAddr,   
                                       tIPADDR*             pDestIpAddr,  
                                       UINT2                  u2Len,
                                       UINT1                  u1TimeToLive));
#ifdef RAWSOCK_WANTED
PUBLIC VOID IpifGetPkt        PROTO((INT4 i4SockId));
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
PUBLIC INT4 OspfCreateNLHelloSocket          PROTO((VOID));
PUBLIC VOID IpifGetNLHelloPkt                PROTO((VOID));
PUBLIC INT4 OspfSetNLHelloSockOptions        PROTO((VOID));
PUBLIC VOID OspfCloseNLHelloSocket           PROTO((VOID));
#endif
#endif
PUBLIC INT4 OspfSetRawSockOptions     PROTO((UINT4 u4OspfCxtId));
PUBLIC INT4 OspfCreateSocket          PROTO((UINT4 u4OspfCxtId));
PUBLIC VOID OspfCloseSocket           PROTO((UINT4 u4OspfCxtId));
PUBLIC INT4 OspfSetIfSockOptions
                               PROTO((tInterface * pInterface));
#else
PUBLIC VOID IpifDeqPkt PROTO ((tOspfIpPktInfo *pOspfIpIfParam));
#endif

PUBLIC VOID OspfProcessIfStateChg (tOspfIpIfParam *pOspfIfParams);

PUBLIC VOID IpifRcvPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                              UINT2 u2Len, UINT4 u4IfIndex,
                              tCRU_INTERFACE IfaceId, UINT1 u1Flag));

PUBLIC INT4 IpifGetIfParamsInCxt  PROTO((UINT4 u4OspfCxtId,
                                       tIPADDR pIpAddr,          
                                       UINT4  u4AddrlessIf,      
                                       UINT4* pIfIndex,
                                       tIPADDRMASK pIfIpAddrMask,  
                                       UINT4* pIfMtuSize,      
                                       UINT4* pIfSpeed,
                                       UINT1* pu1IfType,
                                       UINT1 *pu1OperStat));

PUBLIC INT4 ValidIpInterfaceInCxt PROTO((UINT4 u4OspfCxtId, tIPADDR pIpAddr,
                              UINT4 u4AddrlessIf));

PUBLIC VOID IpifJoinMcastGroup      
                                PROTO((tIPADDR * pMcastGroupAddr,
                                       tInterface * pInterface));
PUBLIC VOID IpifLeaveMcastGroup     
                                PROTO((tIPADDR *pMcastGroupAddr,
                                       tInterface * pInterface));

PUBLIC VOID OspfIfStateChgHdlr
           PROTO ((tNetIpv4IfInfo *pNetIpIfInfo, UINT4 u4BitMap));

#ifdef DEBUG_WANTED
PUBLIC INT1 OSPFGetRoute PROTO ( (UINT4           u4DestIpAddr,
                                     UINT1           u1Tos,
                                     tInterfaceId* pNextHopIf,
                                     UINT4*          pNextHopIpAddr) );
#endif /* DEBUG_WANTED */

PUBLIC INT4 OspfGetSystemMode (UINT2 u2ProtocolId);

/* CFA util functions called in CLI */
PUBLIC UINT4 OspfGetIpAddr PROTO ((UINT4 u4IfIndex, UINT4 *pu4IpAddr));

PUBLIC UINT4 OspfMemReleaseMemBlock PROTO ((tMemPoolId PoolId, UINT1 *pu1Block));

/**************************************************************************/
#endif
