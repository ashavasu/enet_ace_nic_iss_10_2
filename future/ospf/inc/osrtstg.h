#ifndef __OSRTSTG_H
#define __OSRTSTG_H

#define OSPF_RT_STAGGERED      1
#define OSPF_RT_NOT_STAGGERED  2

#define OSPF_INVALID_STAGGERING_DELTA 0xffffffff

/* Function prototypes */
VOID OSPFRtcRelinquish (tOspfCxt *);

VOID OSPFRtRelinquishProcessEvents (VOID);

#endif
