
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: opqapsim.c,v 1.16 2011/05/04 13:41:19 siva Exp $
 *
 * Description:This file contains functions that are
 *             associated with Opaque LSA Application simulator.
 *
 *  The Opaque Application Simulator (OAS) is created to test Opaque LSA 
 *  handling capability of FutureOSPF. The OAS runs as a single task. It waits
 *  either an SNMP event or for a TIMER event. When the events happen the 
 *  associated actions are executed. 
 *
 *  In case of SNMP event, (which is of only one type) an input file name
 *  is expected via SNMP. The OAS then reads the file contents and carries out
 *  appropriate actions. One of the following actions will be triggered based
 *  on the file contents.
 *  1) Registration of a new application with FutureOSPF
 *     - the new application can be registered for non polling mode
 *     - the new application can be registered for polling mode.
 *  2) DeRegistration of an application with FutureOSPF
 *  3) Indication of a new Opaque LSA to FutureOSPF
 *  4) Indication of an earlier generated LSA being no more valid to 
 *     FutureOSPF. 
 *******************************************************************/

#include "oasdefs.h"
#include "oasinc.h"
#include "oasgbl.h"
#include "osport.h"
PUBLIC VOID UtilOspfDwordToPdu PROTO ((UINT1 *pu1PduAddr, UINT4 u4Value));
/*--------------------------------------------------------------------------*/
/* 
 * Function    : oasInit
 * Description : This function should be called by the root application to 
 *               to stat the Opaque Application Simulator (OAS).
 *               This routine creates a task that will perform the actions
 *               of the OAS.
 * Inputs      : None. 
 * Outputs     : None.
 * Returns     : OAS_SUCCESS on successful task creation and needed actions.
 *               Returns OAS_FAILURE otherwise.
 */
/*---------------------------------------------------------------------------*/
PUBLIC INT1
oasInit (VOID)
{
    UINT4               u4OasTaskId;
    UINT4               u4RetVal;
    u4RetVal =
        OsixCreateTask ((const UINT1 *) "OAST", TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE, oasTaskMain, NULL,
                        OSIX_TASK_USER_MODE, &u4OasTaskId);
    if (u4RetVal != OSIX_SUCCESS)

    {
        OAS_DBG (OAS_MEM, "Task Creation failure \n");
        return OAS_FAILURE;

        /* exit(1); */
    }
    OAS_DBG1 (OAS_MEM, "Task Crtn success - Tsk Id = %d \n", u4OasTaskId);
    return OAS_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasTaskMain
 * Description : This is the main function of the Opaque Application Simulator 
 *               (OAS). Here the task waits for two events SNMP event and Timer
 *               event. Based on the event, the appropriate action routines are
 *               invoked.
 * Inputs      : None. 
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasTaskMain (INT1 *pTaskId)
{
    UINT4               u4RcvdEvent;
    UINT4               u4DeQSate;
    FS_ULONG            aulTaskEnqMsgInfo[4];
    tOsixQId            OASQID;

    /* tTmrAppTimer       *pNewTimer;     Dummy Variable */
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSimOpqAppInfo     *pSimOpqAppInfoTab;
    UINT4               u4AppIndx;
    UNUSED_PARAM (pTaskId);

    /* Create Q for  OAS Task  */
    /* 
     * The queue is created with the name OASQ and the queue is created
     * to hold 16 messages. This is the second parameter to the function
     * and is known asthe queue depth.
     */
    if (OsixCreateQ ((const UINT1 *) "OASQ", 16, OSIX_LOCAL, &OASQID) !=
        OSIX_SUCCESS)

    {
        OAS_DBG (OAS_MEM, "Task Queue creation failure. OAS Tsk Exiting. \n");
        OsixDeleteTask (SELF, (const UINT1 *) "OAST");
    }

    /* Allocate memory for Application Info Table */
    APP_INFO_TAB (gOasInfo) =
        MEM_MALLOC ((sizeof (tSimOpqAppInfo) * MAX_SIM_APPLN_SUPRTD),
                    tSimOpqAppInfo);
    if (APP_INFO_TAB (gOasInfo) == NULL)

    {
        OAS_DBG (OAS_MEM, "Mem appln tbl allcn failure. OAS Tsk Exiting.\n");
        OsixDeleteQ (SELF, (const UINT1 *) "OASQ");
        OsixDeleteTask (SELF, (const UINT1 *) "OAST");
    }

    /* Initialising the SockInfo Table */
    MEMSET (APP_INFO_TAB (gOasInfo), 0,
            (sizeof (tSimOpqAppInfo) * MAX_SIM_APPLN_SUPRTD));
    pSimOpqAppInfoTab = APP_INFO_TAB (gOasInfo);
    for (u4AppIndx = 0; u4AppIndx < MAX_SIM_APPLN_SUPRTD;
         u4AppIndx++, pSimOpqAppInfoTab++)

    {
        pSimOpqAppInfoTab->u1Status = OAS_INVALID;
    }

    /* This is the main while loop which handles all events */
    while (OAS_TRUE)

    {
        if (OsixReceiveEvent ((OAS_TSK_ENQ_EVENT), OSIX_WAIT, 0, &u4RcvdEvent)
            != OSIX_SUCCESS)

        {

            /* Osix Receive event Failure - This is not an error cndn */
            continue;
        }

        /* Processing the messages that are EnQ */
        if (u4RcvdEvent)

        {
            while ((u4DeQSate =
                    OsixReceiveFromQ (SELF, (const UINT1 *) "OASQ",
                                      OSIX_NO_WAIT, 0,
                                      &pBuf)) != OSIX_ERR_NO_MSG)

            {
                if (u4DeQSate != OSIX_SUCCESS)

                {

                    /*  
                     * Failed to Dequeue the mse from OASQ - Err Cndn can be 
                     * ignored.
                     */
                    break;
                }
                if (CRU_BUF_Copy_FromBufChain
                    (pBuf, (UINT1 *) aulTaskEnqMsgInfo, 0,
                     sizeof (aulTaskEnqMsgInfo)) != sizeof (aulTaskEnqMsgInfo))

                {
                    OAS_DBG (OAS_MISC, "Error cpying data from buffer.\n");
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                switch (aulTaskEnqMsgInfo[0])

                {
                    case OAS_SNMP_REQ_EVENT:

                        /* Case snmp input received */
                        oasSnmpReqHandler (aulTaskEnqMsgInfo);
                        break;
                    default:
                        break;
                }
            }                    /* End of while within the else if cndn */
        }                        /* else if */
    }                            /* while  TRUE */
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasSnmpReqHandler
 * Description : This fuction acts when an SNMP event is received. This function
 *               obtains the file name provided via the SNMP and then invokes 
 *               appropriate actions based on the file contents.
 * Inputs      : aulTaskEnqMsgInfo - info about events and file name    
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasSnmpReqHandler (FS_ULONG aulTaskEnqMsgInfo[])
{
    switch (aulTaskEnqMsgInfo[1])

    {
        case OAS_APP_REGISTER:
            oasHandleAppReg (aulTaskEnqMsgInfo);
            break;
        case OAS_APP_DEREGISTER:
            oasHandleAppDeReg (aulTaskEnqMsgInfo);
            break;
        case OAS_APP_SEND_OPQLSA:
            oasHandleAppSendOpqLsa (aulTaskEnqMsgInfo);
            break;
        default:
            break;
    }
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasHandleAppReg
 * Description : This fuction reads information of the application that needs
 *               to registered with the OSPF, from a file. The file name is 
 *               passed in the fourth UINT4 field of the input variable.
 *               The applicaiton related information is obtained from the file
 *               and applicaiton registration is triggered.
 * Inputs      : aulTaskEnqMsgInfo - info containing file name    
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasHandleAppReg (FS_ULONG aulTaskEnqMsgInfo[])
{
    FILE               *pfp;
    UINT1              *pu1filename;
    INT4                i4AppOpqtype;
    INT4                i4OpqLSATypesSupported;
    INT4                i4RetVal = OAS_FAILURE;
    UINT4               u4Indx;
    UINT4               u4ContextId;
    UINT4               u4InfoFromOSPF = 0;
    tSimOpqAppInfo     *pSimOpqAppInfoTab;
    pu1filename = (UINT1 *) (aulTaskEnqMsgInfo[3]);
    if ((pfp = FOPEN ((const char *) pu1filename, (const char *) "r")) != NULL)

    {
        if (fscanf
            (pfp, "%u %d %d ", &u4ContextId, &i4AppOpqtype,
             &i4OpqLSATypesSupported) == 3)

        {
            u4InfoFromOSPF = (u4InfoFromOSPF | OSPF_OPQ_LSA_TO_OPQ_APP |
                              OSPF_SELF_RTR_LSA_TO_OPQ_APP |
                              OSPF_NET_LSA_TO_OPQ_APP |
                              OSPF_NBR_INFO_TO_OPQ_APP);
            if ((i4OpqLSATypesSupported & OAS_TYPE_11_LSA_SPRTD) ==
                OAS_TYPE_11_LSA_SPRTD)
            {
                u4InfoFromOSPF = u4InfoFromOSPF | OSPF_ASBR_INFO_TO_OPQ_APP;
            }

            i4RetVal = OpqAppRegisterInCxt (u4ContextId,
                                            (UINT1) i4AppOpqtype,
                                            (UINT1) i4OpqLSATypesSupported,
                                            u4InfoFromOSPF,
                                            (void *) oasOpqAppCallBackFn);

            if (i4RetVal == OAS_SUCCESS)

            {
                OAS_DBG (OAS_MISC, "Appln Sccfly regd\n");

                /* Updating Application Database */
                pSimOpqAppInfoTab = APP_INFO_TAB (gOasInfo);
                for (u4Indx = 0; u4Indx < MAX_SIM_APPLN_SUPRTD;
                     u4Indx++, pSimOpqAppInfoTab++)

                {
                    if (pSimOpqAppInfoTab->u1Status == OAS_VALID)

                    {
                        continue;
                    }

                    else

                    {
                        pSimOpqAppInfoTab->u1LSATypesSupported =
                            (UINT1) i4OpqLSATypesSupported;
                        pSimOpqAppInfoTab->u1Status = OAS_VALID;
                        pSimOpqAppInfoTab->u1OpqType = (UINT1) i4AppOpqtype;
                        pSimOpqAppInfoTab->Type9LSAGen = 0;
                        pSimOpqAppInfoTab->Type9LSARcvd = 0;
                        pSimOpqAppInfoTab->Type10LSAGen = 0;
                        pSimOpqAppInfoTab->Type10LSARcvd = 0;
                        pSimOpqAppInfoTab->Type11LSAGen = 0;
                        pSimOpqAppInfoTab->Type11LSARcvd = 0;
                        break;
                    }            /* else */
                }                /* for */
            }

            else

            {
                OAS_DBG (OAS_MISC, "Appln regstrn failed.");
            }
        }
        fclose (pfp);
    }

    else

    {
        OAS_DBG1 (OAS_MISC, "Error couldn't read File = %s \n", pu1filename);
    }

    /* 
     * Memory for this would have been allocated in the low level routine.
     * and hence freed here after the processing.
     */
    MEM_FREE (pu1filename);
    pu1filename = NULL;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasHandleAppDeReg
 * Description : This fuction reads information of the application that needs
 *               to deregistered with the OSPF, from a file. The file name is 
 *               passed in the fourth UINT4 field of the input variable.
 *               The appliction id value obtained from the file and applicaiton
 *               deregistration is triggered.
 * Inputs      : aulTaskEnqMsgInfo - info containing file name    
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasHandleAppDeReg (FS_ULONG aulTaskEnqMsgInfo[])
{
    FILE               *pfp;
    UINT1              *pu1filename;
    INT4                i4AppOpqtype;
    INT4                i4RetVal;
    UINT4               u4Indx;
    UINT4               u4ContextId;
    tSimOpqAppInfo     *pSimOpqAppInfoTab;
    pu1filename = (UINT1 *) (aulTaskEnqMsgInfo[3]);
    if ((pfp = FOPEN ((const char *) pu1filename, (const char *) "r")) != NULL)

    {
        if (fscanf (pfp, "%u %d", &u4ContextId, &i4AppOpqtype) == 2)

        {
            i4RetVal = OpqAppDeRegisterInCxt (u4ContextId,
                                              (UINT1) i4AppOpqtype);
            if (i4RetVal == OAS_SUCCESS)

            {
                OAS_DBG (OAS_MISC, "Appln Drgstrn Sccfly trgrd.");

                /*Updating the Application Table */
                pSimOpqAppInfoTab = APP_INFO_TAB (gOasInfo);
                for (u4Indx = 0; u4Indx < MAX_SIM_APPLN_SUPRTD;
                     u4Indx++, pSimOpqAppInfoTab++)

                {
                    if (pSimOpqAppInfoTab->u1OpqType == (UINT1) i4AppOpqtype)

                    {
                        pSimOpqAppInfoTab->u1Status = OAS_INVALID;
                        break;
                    }
                }
            }

            else

            {
                OAS_DBG (OAS_MISC, "Appln Drgstrn trgr failed.");
            }
        }
        fclose (pfp);
    }

    else

    {
        OAS_DBG1 (OAS_MISC, "Error couldn't read File = %s \n", pu1filename);
    }

    /* 
     * Memory for this would have been allocated in the low level routine.
     * and hence freed here after the processing.
     */
    MEM_FREE (pu1filename);
    pu1filename = NULL;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasHandleAppSendOpqLsa
 * Description : This fuction reads information of the Opaque LSA that an 
 *               application that needs intimate the OSPF.
 *               The file name is passed in the fourth
 *               UINT4 field of the input variable. The Opaque LSA realted 
 *               information is obtained from the file and the LSA is passed
 *               to the OSPF.
 * Inputs      : aulTaskEnqMsgInfo - info containing file name    
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasHandleAppSendOpqLsa (FS_ULONG aulTaskEnqMsgInfo[])
{
    FILE               *pfp;
    UINT4               u4OpqAppID;
    UINT4               u4AddrlessIf;
    UINT4               u4OpqLSAType;
    INT4                i4LSAStatus;
    UINT4               u4LsId;
    INT4                i4LSALen;
    UINT4               u4IfaceId;
    UINT4               u4AreaId;
    INT4                i4RetVal;
    INT4                i4Count;
    UINT1              *pu1TmpLsa;
    UINT1              *pu1filename;
    tOpqLSAInfo        *pOpqLSAInfo;
    UINT4               u4Indx;
    tSimOpqAppInfo     *pSimOpqAppInfoTab;
    UINT4               u4ReadLsa;
    UINT4               u4ContextId;
    UINT1               u1Flag = OAS_FALSE;
    pu1filename = (UINT1 *) (aulTaskEnqMsgInfo[3]);
    if ((pfp = FOPEN ((const BOOL1 *) pu1filename, (const BOOL1 *) "r"))
        != NULL)

    {
        if (fscanf
            (pfp, "%u %u %u %d %x %d %x %u %x", &u4ContextId,
             &u4OpqAppID, &u4OpqLSAType,
             &i4LSAStatus, &u4LsId, &i4LSALen, &u4IfaceId, &u4AddrlessIf,
             &u4AreaId) == 9)

        {
            pOpqLSAInfo = MEM_MALLOC (sizeof (tOpqLSAInfo), tOpqLSAInfo);
            if (pOpqLSAInfo != NULL)

            {
                MEMSET ((VOID *) pOpqLSAInfo, 0, sizeof (tOpqLSAInfo));
                pOpqLSAInfo->pu1LSA = MEM_MALLOC (i4LSALen, UINT1);
                if (pOpqLSAInfo->pu1LSA != NULL)

                {
                    MEMSET ((VOID *) pOpqLSAInfo->pu1LSA, 0, i4LSALen);
                    pu1TmpLsa = pOpqLSAInfo->pu1LSA;
                    for (i4Count = 0; i4Count < i4LSALen;
                         i4Count++, pu1TmpLsa++)

                    {
                        if (!(feof (pfp)))

                        {
                            fscanf (pfp, "%x ", &u4ReadLsa);
                            *pu1TmpLsa = (UINT1) u4ReadLsa;
                        }

                        else

                        {
                            u1Flag = OAS_TRUE;
                            OAS_DBG (OAS_MISC,
                                     "LSA info read from file  failed.");
                            oasAppPvdOpqLSAFree (pOpqLSAInfo);
                            break;
                        }
                    }
                    if (u1Flag == OAS_FALSE)

                    {

                        /* Required information obtained */
                        pOpqLSAInfo->u1OpqLSAType = (UINT1) u4OpqLSAType;
                        pOpqLSAInfo->u1LSAStatus = (UINT1) i4LSAStatus;
                        OSPF_CRU_BMC_DWTOPDU (pOpqLSAInfo->LsId, u4LsId);
                        pOpqLSAInfo->u2LSALen = (UINT2) i4LSALen;
                        pOpqLSAInfo->u4IfaceID = u4IfaceId;
                        pOpqLSAInfo->u4AreaID = u4AreaId;
                        pOpqLSAInfo->u4AddrlessIf = u4AddrlessIf;
                        i4RetVal = OpqAppToOpqModuleSendInCxt (u4ContextId,
                                                               (void *)
                                                               pOpqLSAInfo);
                        if (i4RetVal == OAS_SUCCESS)

                        {
                            OAS_DBG (OAS_MISC, "Opq LSA Sccfly Sent to OSPF.");
                            pSimOpqAppInfoTab = APP_INFO_TAB (gOasInfo);
                            for (u4Indx = 0; u4Indx < MAX_SIM_APPLN_SUPRTD;
                                 u4Indx++, pSimOpqAppInfoTab++)

                            {
                                if (pSimOpqAppInfoTab->u4Appid ==
                                    (UINT4) u4OpqAppID)

                                {
                                    switch (u4OpqLSAType)

                                    {
                                        case OAS_TYPE9_OPQ_LSA:
                                            pSimOpqAppInfoTab->Type9LSAGen += 1;
                                            break;
                                        case OAS_TYPE10_OPQ_LSA:
                                            pSimOpqAppInfoTab->Type10LSAGen +=
                                                1;
                                            break;
                                        case OAS_TYPE11_OPQ_LSA:
                                            pSimOpqAppInfoTab->Type11LSAGen +=
                                                1;
                                            break;
                                    }
                                    break;
                                }    /*if */
                            }    /*for */
                        }

                        else

                        {
                            OAS_DBG (OAS_MISC, "Opq LSA Sent to OSPF Failed.");
                            oasAppPvdOpqLSAFree (pOpqLSAInfo);
                        }
                    }
                }

                else

                {
                    OAS_DBG (OAS_MISC,
                             "Mem alloc for pOpqLSAInfo->pu1Lsa failed.");
                    MEM_FREE (pOpqLSAInfo);
                }
            }

            else

            {
                OAS_DBG (OAS_MISC, "Mem alloc for pOpqLSAInfo failed.");
            }
        }
        fclose (pfp);
    }

    else

    {
        OAS_DBG1 (OAS_MISC, "Error couldn't read File = %s \n", pu1filename);
    }

    /* 
     * Memory for this would have been allocated in the low level routine.
     * and hence freed here after the processing.
     */
    MEM_FREE (pu1filename);
    pu1filename = NULL;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasAppPvdOpqLSAFree
 * Description : This function is provided as call back function to free the
 *               OpqLsaInfo provided to the OSPF.
 * Inputs      : pOpqLSAInfo - info containing Opaque LSA information 
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasAppPvdOpqLSAFree (tOpqLSAInfo * pOpqLSAInfo)
{
    if (pOpqLSAInfo->pu1LSA != NULL)

    {
        MEM_FREE (pOpqLSAInfo->pu1LSA);
    }
    MEM_FREE (pOpqLSAInfo);
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : oasOpqAppCallBackFn
 * Description : This function is provided as call back function to free the
 *               OpqLsaInfo provided to the OSPF.
 * Inputs      : pOpqLSAInfo - info containing Opaque LSA information 
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
oasOpqAppCallBackFn (tOsToOpqApp * pOpqMsg)
{
    FS_ULONG            aulMsgInfo[4];
    UINT4               u4Indx;
    UINT4               u4AsbrStatus = OAS_FALSE;
    UINT1              *pu1Filename = NULL;
    UINT1               au1NewInstLsaFileName[64] =
        "switch3_txt/r3t11s1as0ls1.txt";
    UINT1               au1MaxAgeLsaFileName[64] =
        "switch3_txt/r3t11s4as0ls1.txt";
    tSimOpqAppInfo     *pSimOpqAppInfoTab;

    pSimOpqAppInfoTab = APP_INFO_TAB (gOasInfo);

    aulMsgInfo[0] = OAS_SNMP_REQ_EVENT /* 0x00000001 */ ;
    aulMsgInfo[1] = OAS_APP_REGISTER /*  1  */ ;
    aulMsgInfo[2] = 0;

    for (u4Indx = 0; u4Indx < MAX_SIM_APPLN_SUPRTD;
         u4Indx++, pSimOpqAppInfoTab++)

    {
        if (pSimOpqAppInfoTab->u1Status == OAS_VALID)
        {
            if (pOpqMsg->u4MsgSubType == OSPF_TE_ASBR_STATUS_INFO)
            {
                if ((pSimOpqAppInfoTab->
                     u1LSATypesSupported & OAS_TYPE_11_LSA_SPRTD) ==
                    OAS_TYPE_11_LSA_SPRTD)
                {
                    u4AsbrStatus = pOpqMsg->asbrInfo.u4ASBdrRtrStatus;

                    pu1Filename = MEM_MALLOC (64, UINT1);
                    if (pu1Filename == NULL)
                        return;

                    MEMSET (pu1Filename, 0, 64);

                    if (u4AsbrStatus == OAS_ASBR_FALSE)
                    {

                        MEMCPY ((pu1Filename), au1MaxAgeLsaFileName, 64);
                        aulMsgInfo[3] = (FS_ULONG) pu1Filename;
                        oasHandleAppSendOpqLsa (aulMsgInfo);
                    }
                    else
                    {
                        MEMCPY ((pu1Filename), au1NewInstLsaFileName, 64);
                        aulMsgInfo[3] = (FS_ULONG) pu1Filename;
                        oasHandleAppSendOpqLsa (aulMsgInfo);
                    }
                }

            }                    /*if */
        }                        /*for */
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : oasDeInit                                                  */
/*                                                                           */
/* Description  : This routine clears all the memory and deletes the task    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
oasDeInit (VOID)
{
    MEM_FREE (APP_INFO_TAB (gOasInfo));
    OsixDeleteQ (SELF, (const UINT1 *) "OASQ");
    OsixDeleteTask (SELF, (const UINT1 *) "OAST");
}

/*---------------------------------------------------------------------------*/
/*                    End of the file opqapsim.c                             */
/*---------------------------------------------------------------------------*/
