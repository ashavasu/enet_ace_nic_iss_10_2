# include  "osinc.h"
# include  "oasdefs.h"
# include  "fsosolow.h"
# include  "fsosoacli.h"

/* Low Level GET Routine for All Objects  */
/* Low Level Routines have been written for the sake of completion */
/****************************************************************************
 Function    :  nmhGetFutOasAppRegFileName
 Input       :  The Indices

                The Object 
                retValFutOasAppRegFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOasAppRegFileName (tSNMP_OCTET_STRING_TYPE *
                            pRetValFutOasAppRegFileName)
{
    CPY_TO_SNMP (pRetValFutOasAppRegFileName, "", 0);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOasAppDeRegFileName
 Input       :  The Indices

                The Object 
                retValFutOasAppDeRegFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOasAppDeRegFileName (tSNMP_OCTET_STRING_TYPE *
                              pRetValFutOasAppDeRegFileName)
{
    CPY_TO_SNMP (pRetValFutOasAppDeRegFileName, "", 0);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOasAppOpqLsaSndFileName
 Input       :  The Indices

                The Object 
                retValFutOasAppOpqLsaSndFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOasAppOpqLsaSndFileName (tSNMP_OCTET_STRING_TYPE *
                                  pRetValFutOasAppOpqLsaSndFileName)
{
    CPY_TO_SNMP (pRetValFutOasAppOpqLsaSndFileName, "", 0);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOasAppRegFileName
 Input       :  The Indices

                The Object 
                setValFutOasAppRegFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOasAppRegFileName (tSNMP_OCTET_STRING_TYPE *
                            pSetValFutOasAppRegFileName)
{
    FS_ULONG            aulTaskEnqMsgInfo[4];
    UINT1              *pu1Filename;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    /*  
     * Assuming that the file name will be less than or equal to 64 characters
     * in length.
     */
    pu1Filename = MEM_MALLOC (64, UINT1);

    if (pu1Filename == NULL)
        return SNMP_FAILURE;

    MEMSET (pu1Filename, 0, 64);

    aulTaskEnqMsgInfo[0] = OAS_SNMP_REQ_EVENT /* 0x00000001 */ ;
    aulTaskEnqMsgInfo[1] = OAS_APP_REGISTER /*  1  */ ;
    aulTaskEnqMsgInfo[2] = 0;

    /* The file name provided via the Manager is copied here */
    MEMCPY ((pu1Filename), pSetValFutOasAppRegFileName->pu1_OctetList,
            pSetValFutOasAppRegFileName->i4_Length);
    aulTaskEnqMsgInfo[3] = (FS_ULONG) pu1Filename;

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (aulTaskEnqMsgInfo), 0))
        == NULL)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Memory allocation for OAS SNMP Msg Info failed\n");
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) aulTaskEnqMsgInfo, 0,
                                   sizeof (aulTaskEnqMsgInfo)) != CRU_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Message copy failed\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (OsixSendToQ (SELF, (const UINT1 *) "OASQ", pBuf, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to EnQ the Msg to OASQ\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OAST",
                       OAS_TSK_ENQ_EVENT /* 0x00000010 */ )
        != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to send Msg EnQd event to OAS Task\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FutOasAppRegFileName;
    SnmpNotifyInfo.u4OidLen = sizeof (FutOasAppRegFileName) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFutOasAppRegFileName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOasAppDeRegFileName
 Input       :  The Indices

                The Object 
                setValFutOasAppDeRegFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOasAppDeRegFileName (tSNMP_OCTET_STRING_TYPE *
                              pSetValFutOasAppDeRegFileName)
{
    FS_ULONG            aulTaskEnqMsgInfo[4];
    UINT1              *pu1Filename;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    /*  
     * Assuming that the file name will be less than or equal to 64 characters
     * in length.
     */
    pu1Filename = MEM_MALLOC (64, UINT1);

    if (pu1Filename == NULL)
        return SNMP_FAILURE;

    MEMSET (pu1Filename, 0, 64);

    aulTaskEnqMsgInfo[0] = OAS_SNMP_REQ_EVENT /* 0x00000001 */ ;
    aulTaskEnqMsgInfo[1] = OAS_APP_DEREGISTER /* 2 */ ;
    aulTaskEnqMsgInfo[2] = 0;

    /* The file name provided via the Manager is copied here */
    MEMCPY ((pu1Filename), pSetValFutOasAppDeRegFileName->pu1_OctetList,
            pSetValFutOasAppDeRegFileName->i4_Length);
    aulTaskEnqMsgInfo[3] = (FS_ULONG) pu1Filename;

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (aulTaskEnqMsgInfo), 0))
        == NULL)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Memory allocation for OAS SNMP Msg Info failed\n");
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) aulTaskEnqMsgInfo, 0,
                                   sizeof (aulTaskEnqMsgInfo)) != CRU_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Message copy failed\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (OsixSendToQ (SELF, (const UINT1 *) "OASQ", pBuf, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to EnQ the Msg to OASQ\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OAST",
                       OAS_TSK_ENQ_EVENT /* 0x00000010 */ )
        != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to send Msg EnQd event to OAS Task\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FutOasAppDeRegFileName;
    SnmpNotifyInfo.u4OidLen = sizeof (FutOasAppDeRegFileName) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFutOasAppDeRegFileName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOasAppOpqLsaSndFileName
 Input       :  The Indices

                The Object 
                setValFutOasAppOpqLsaSndFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOasAppOpqLsaSndFileName (tSNMP_OCTET_STRING_TYPE *
                                  pSetValFutOasAppOpqLsaSndFileName)
{
    FS_ULONG            aulTaskEnqMsgInfo[4];
    UINT1              *pu1Filename;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    /*  
     * Assuming that the file name will be less than or equal to 64 characters
     * in length.
     */
    pu1Filename = MEM_MALLOC (64, UINT1);

    if (pu1Filename == NULL)
        return SNMP_FAILURE;

    MEMSET (pu1Filename, 0, 64);

    aulTaskEnqMsgInfo[0] = OAS_SNMP_REQ_EVENT /* 0x00000001 */ ;
    aulTaskEnqMsgInfo[1] = OAS_APP_SEND_OPQLSA /* 3 */ ;
    aulTaskEnqMsgInfo[2] = 0;

    /* The file name provided via the Manager is copied here */
    MEMCPY ((pu1Filename), pSetValFutOasAppOpqLsaSndFileName->pu1_OctetList,
            pSetValFutOasAppOpqLsaSndFileName->i4_Length);
    aulTaskEnqMsgInfo[3] = (FS_ULONG) pu1Filename;

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (aulTaskEnqMsgInfo), 0))
        == NULL)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Memory allocation for OAS SNMP Msg Info failed\n");
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) aulTaskEnqMsgInfo, 0,
                                   sizeof (aulTaskEnqMsgInfo)) != CRU_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Message copy failed\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (OsixSendToQ (SELF, (const UINT1 *) "OASQ", pBuf, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to EnQ the Msg to OASQ\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OAST",
                       OAS_TSK_ENQ_EVENT /* 0x00000010 */ )
        != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (ALL_FAILURE_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to send Msg EnQd event to OAS Task\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1Filename);
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FutOasAppOpqLsaSndFileName;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FutOasAppOpqLsaSndFileName) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFutOasAppOpqLsaSndFileName));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOasAppRegFileName
 Input       :  The Indices

                The Object 
                testValFutOasAppRegFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOasAppRegFileName (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFutOasAppRegFileName)
{
    UNUSED_PARAM (pTestValFutOasAppRegFileName);
    *pu4ErrorCode = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOasAppDeRegFileName
 Input       :  The Indices

                The Object 
                testValFutOasAppDeRegFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOasAppDeRegFileName (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFutOasAppDeRegFileName)
{
    UNUSED_PARAM (pTestValFutOasAppDeRegFileName);
    *pu4ErrorCode = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOasAppOpqLsaSndFileName
 Input       :  The Indices

                The Object 
                testValFutOasAppOpqLsaSndFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOasAppOpqLsaSndFileName (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFutOasAppOpqLsaSndFileName)
{
    UNUSED_PARAM (pTestValFutOasAppOpqLsaSndFileName);
    *pu4ErrorCode = 0;
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOasAppRegFileName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOasAppRegFileName (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOasAppDeRegFileName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOasAppDeRegFileName (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOasAppOpqLsaSndFileName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOasAppOpqLsaSndFileName (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
