#include "lr.h"
#include "fssnmp.h"
#include "fsosoawr.h"
#include "fsosolow.h"
#include "fsosoadb.h"

VOID
RegisterFSOSOA ()
{
    SNMPRegisterMib (&fsosoaOID, &fsosoaEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsosoaOID, (const UINT1 *) "futOspfOasGroup");
}

VOID
UnRegisterFSOSOA ()
{
    SNMPUnRegisterMib (&fsosoaOID, &fsosoaEntry);
    SNMPDelSysorEntry (&fsosoaOID, (const UINT1 *) "futOspfOasGroup");
}

INT4
FutOasAppRegFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOasAppRegFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FutOasAppRegFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOasAppRegFileName (pMultiData->pOctetStrValue));
}

INT4
FutOasAppRegFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOasAppRegFileName (pMultiData->pOctetStrValue));
}

INT4
FutOasAppDeRegFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOasAppDeRegFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FutOasAppDeRegFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOasAppDeRegFileName (pMultiData->pOctetStrValue));
}

INT4
FutOasAppDeRegFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOasAppDeRegFileName (pMultiData->pOctetStrValue));
}

INT4
FutOasAppOpqLsaSndFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOasAppOpqLsaSndFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FutOasAppOpqLsaSndFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOasAppOpqLsaSndFileName (pMultiData->pOctetStrValue));
}

INT4
FutOasAppOpqLsaSndFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOasAppOpqLsaSndFileName (pMultiData->pOctetStrValue));
}

INT4
FutOasAppRegFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOasAppRegFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOasAppDeRegFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOasAppDeRegFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOasAppOpqLsaSndFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOasAppOpqLsaSndFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
