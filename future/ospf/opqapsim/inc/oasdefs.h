/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oasdefs.h,v 1.8 2011/03/23 06:00:26 siva Exp $
 *
 * Description:This file contains constant definitions, global 
 *             declarations that are associated with the
 *             Opaque Application Simulator (OAS). 
 *******************************************************************/

#ifndef _OASDEFS_H
#define _OASDEFS_H

#define OAS_SUCCESS            0
#define OAS_FAILURE           -1
#define OAS_TRUE               1
#define OAS_FALSE              0
#define OAS_ASBR_TRUE          1
#define OAS_ASBR_FALSE         2  
#define OAS_VALID              1
#define OAS_INVALID            2  

#define OAS_NON_POLL_MODE      1
#define OAS_POLL_MODE          2

#define TASK_PRIORITY          100

/* LSA Type definitions */
#define OAS_TYPE9_OPQ_LSA      9
#define OAS_TYPE10_OPQ_LSA     10
#define OAS_TYPE11_OPQ_LSA     11

/* LSA Instance definitions */
#define OAS_NEW_LSA            1   /*00000001*/
#define OAS_NEW_LSA_INSTANCE   2   /*00000010*/
#define OAS_FLUSHED_LSA        4   /*00000100*/

/* Applicaiton action types */
#define OAS_APP_REGISTER       1
#define OAS_APP_DEREGISTER     2
#define OAS_APP_SEND_OPQLSA    3

/* Opaque LSA Type supported  */
#define OAS_TYPE_9_LSA_SPRTD   1
#define OAS_TYPE_10_LSA_SPRTD  2
#define OAS_TYPE_11_LSA_SPRTD  4

#define OAS_POLL_REQ_TIME         10 /* Polling done after every 10 secs */
#define OAS_NUM_TICKS_PER_SEC     1  /* Assuming granularity of 1 tick persec */

#define OAS_TIMER(x)              gOasInfo.pOasTimer
#define OAS_TIMER_LISTID(x)       gOasInfo.oasTimerListId
#define APP_INFO_TAB(gOasInfo)    gOasInfo.pSimOpqAppInfoTab

/* Memory related constants */
#define MAX_SIM_APPLN_SUPRTD 1000

/* Task events definition */
#define OAS_POLL_REQ_EVENT 0x00000001
#define OAS_TSK_ENQ_EVENT  0x00000010

/* OAS Task Main events */
#define OAS_SNMP_REQ_EVENT 0x00000001

/* Definitions for Debug options */
/* Macros used in OAS Debugging */
#define    OAS_DBG_FLAG      gOasInfo.u4OasDbgFlag
#define    OAS_MEM           0x00000001
#define    OAS_MISC          0x00000002

#define OAS_DBG(u4Value, pu1Format)\
          UtlTrcLog  (OAS_DBG_FLAG, u4Value, "OAS", pu1Format)

#define OAS_DBG1(u4Value, pu1Format, Arg1)\
          UtlTrcLog  (OAS_DBG_FLAG, u4Value, "OAS", pu1Format, Arg1 )

#define OAS_DBG2(u4Value, pu1Format, Arg1, Arg2)\
          UtlTrcLog  (OAS_DBG_FLAG, u4Value, "OAS", pu1Format, Arg1, Arg2)

#endif /*_OASDEFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file oasdefs.h                              */
/*---------------------------------------------------------------------------*/
