/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOasAppRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOasAppDeRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOasAppOpqLsaSndFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOasAppRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOasAppDeRegFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOasAppOpqLsaSndFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOasAppRegFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOasAppDeRegFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOasAppOpqLsaSndFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOasAppRegFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOasAppDeRegFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOasAppOpqLsaSndFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
