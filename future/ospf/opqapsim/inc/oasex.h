
/********************************************************************
 *                                                                  *
 * $RCSfile: oasex.h,v $                                          
 *                                                                  *
 * $Date: 2007/02/01 15:00:40 $
 *                                                                  *
 * $Revision: 1.7 $                                             
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : oasex.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OSPF - Utility - Opaque Application simulator
 *    MODULE NAME            : 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Red Hat Linux  6.1 (Portable)                  
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains external definitions that
 *                             will be required by other applications, which
 *                             will make use of the Opaque Application Simulator
 *                             (OAS)
 * 
 *-----------------------------------------------------------------------------
 * CHANGE RECORD :
 * -------------
 *-----------------------------------------------------------------------------
 * VERSION |    AUTHOR/   |    DESCRIPTION OF CHANGE
 * OLD/NEW |    DATE      |            RFC NO
 *---------|--------------|----------------------------------------------------
 * 1.0     | 08/02/2001   | First creation
 *         | S.Manikantan |
 *-----------------------------------------------------------------------------
 */

#ifndef _OASEX_H
#define _OASEX_H

#endif /* _OASEX_H */

/*---------------------------------------------------------------------------*/
/*                        End of file oasex.h                                */
/*---------------------------------------------------------------------------*/
