/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oashdr.h,v 1.10 2011/03/23 06:00:26 siva Exp $
 *
 * Description:This file contains typedefintions that are 
 *             associated with the Opaque Application Simulator (OAS). 
 *******************************************************************/

#ifndef _OASHDR_H
#define _OASHDR_H

typedef struct _simOpqAppInfo 
{
   UINT4       u4Appid;

   UINT2       Type9LSAGen;                /*Number of Type9 LSAs generated*/
   UINT2       Type9LSARcvd;               /*Number of Type9 LSAs received*/

   UINT2       Type10LSAGen;               /*Number of Type10 LSAs generated*/
   UINT2       Type10LSARcvd;              /*Number of Type10 LSAs received*/
   UINT2       Type11LSAGen;               /*Number of Type11 LSAs generated*/
   UINT2       Type11LSARcvd;              /*Number of Type11 LSAs received*/

   UINT1       u1LSATypesSupported;
   UINT1       u1PurgeIntimate;            /*Flag to intimate the if the
                                             application needs a intimation when
                                             an Invalid LSA is flushed.*/
   UINT1       u1Status;                   /*Indicates the status of the entry*/
   UINT1       u1OpqType;                  /*The Application Opaque Type  */ 
   UINT1       u1PollMode;                 /*Pollmode/NonPollMode */
   UINT1       au1Rsvd[3];                 /*Included for 4-Byte Alignment*/
}tSimOpqAppInfo;

typedef struct _oasInfo 
{
   UINT4           u4OasDbgFlag;
   tTmrAppTimer*   pOasTimer;
   tTimerListId    oasTimerListId;
   tSimOpqAppInfo* pSimOpqAppInfoTab;
}tOasInfo;


/* Function prototypes */
PUBLIC INT1  oasInit (VOID);
VOID oasProcessTmrPollEvent (VOID);
VOID oasSnmpReqHandler ARG_LIST((FS_ULONG aulTaskEnqMsgInfo[]));
VOID oasProcessTmrPollEvent ARG_LIST((VOID));
VOID oasHandleAppReg ARG_LIST ((FS_ULONG aulTaskEnqMsgInfo[]));
VOID oasHandleAppDeReg ARG_LIST((FS_ULONG aulTaskEnqMsgInfo[]));
VOID oasHandleAppSendOpqLsa ARG_LIST((FS_ULONG aulTaskEnqMsgInfo[]));
VOID oasAppPvdOpqLSAFree ARG_LIST((tOpqLSAInfo* pOpqLSAInfo));
VOID oasOpqAppCallBackFn ARG_LIST((tOsToOpqApp* pOspfMsg));
PUBLIC VOID  oasDeInit (VOID);

#endif /*_OASHDR_H */
/*---------------------------------------------------------------------------*/
/*                        End of file oashdr.h                               */
/*---------------------------------------------------------------------------*/
