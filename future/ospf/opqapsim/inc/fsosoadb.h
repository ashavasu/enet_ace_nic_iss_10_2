#ifndef __FUTOSPFOASGROUPMBDB_H__
#define __FUTOSPFOASGROUPMBDB_H__


/*  MIB information  */
UINT4 fsosoa [ ] ={1,3,6,1,4,1,2076,10,102};
tSNMP_OID_TYPE fsosoaOID = {9, fsosoa};


/*  OID Description*/
UINT4 FutOasAppRegFileName [ ] ={1,3,6,1,4,1,2076,10,102,1,1};
UINT4 FutOasAppDeRegFileName [ ] ={1,3,6,1,4,1,2076,10,102,1,2};
UINT4 FutOasAppOpqLsaSndFileName [ ] ={1,3,6,1,4,1,2076,10,102,1,3};


tMbDbEntry fsosoaMibEntry[]= {

{{11,FutOasAppRegFileName}, NULL, FutOasAppRegFileNameGet, FutOasAppRegFileNameSet, FutOasAppRegFileNameTest, FutOasAppRegFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FutOasAppDeRegFileName}, NULL, FutOasAppDeRegFileNameGet, FutOasAppDeRegFileNameSet, FutOasAppDeRegFileNameTest, FutOasAppDeRegFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FutOasAppOpqLsaSndFileName}, NULL, FutOasAppOpqLsaSndFileNameGet, FutOasAppOpqLsaSndFileNameSet, FutOasAppOpqLsaSndFileNameTest, FutOasAppOpqLsaSndFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsosoaEntry = { 3, fsosoaMibEntry };
#endif /* __MBDB_H__ */
