
/********************************************************************
 *                                                                  *
 * $RCSfile: oasgbl.h,v $                                          
 *                                                                  *
 * $Date: 2007/02/01 15:00:40 $
 *                                                                  *
 * $Revision: 1.7 $                                             
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : oasgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OSPF - Utility - Opaque Application simulator
 *    MODULE NAME            : 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Red Hat Linux  6.1 (Portable)                  
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global declarations that are
 *                             associated with Opaque Application Simulator 
 *                             (OAS)
 *-----------------------------------------------------------------------------
 * CHANGE RECORD :
 * -------------
 *-----------------------------------------------------------------------------
 * VERSION |    AUTHOR/   |    DESCRIPTION OF CHANGE
 * OLD/NEW |    DATE      |            RFC NO
 *---------|--------------|----------------------------------------------------
 * 1.0     | 08/02/2001   | First creation
 *         | S.Manikantan |
 *-----------------------------------------------------------------------------
 */

#ifndef _OASGBL_H
#define _OASGBL_H

tOasInfo   gOasInfo;


#endif /*_OASGBL_H */
/*---------------------------------------------------------------------------*/
/*                        End of file labelgbl.h                             */
/*---------------------------------------------------------------------------*/
