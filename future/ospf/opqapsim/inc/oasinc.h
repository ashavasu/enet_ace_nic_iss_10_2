
/********************************************************************
 *                                                                  *
 * $RCSfile: oasinc.h,v $                                          
 *                                                                  *
 * $Date: 2007/02/01 15:00:40 $
 *                                                                  *
 * $Revision: 1.7 $                                             
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : oasinc.h  
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OSPF - Utility - Opaque Application simulator
 *    MODULE NAME            :                  
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Red Hat Linux  6.1 (Portable)                  
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains list of FSAP and other header
 *                             files utilised by the Opaque Application 
 *                             Simulator (OAS).
 *-----------------------------------------------------------------------------
 * CHANGE RECORD :
 * -------------
 *-----------------------------------------------------------------------------
 * VERSION |    AUTHOR/   |    DESCRIPTION OF CHANGE
 * OLD/NEW |    DATE      |            RFC NO
 *---------|--------------|----------------------------------------------------
 * 1.0     | 08/02/2001   | First creation
 *         | S.Manikantan |
 *-----------------------------------------------------------------------------
 */

#ifndef _OASINC_H
#define _OASINC_H

#include "lr.h"

#include "ospf.h"

/* Inclusion of oas header related files */
#include "oashdr.h"
#include "oasdefs.h"

#endif /*_OASINC_H */
/*---------------------------------------------------------------------------*/
/*                        End of file oasinc.h                               */
/*---------------------------------------------------------------------------*/
