
#ifndef _FSOSOAWRAP_H 
#define _FSOSOAWRAP_H 
VOID RegisterFSOSOA(VOID);
VOID UnRegisterFSOSOA(VOID);
INT4 FutOasAppRegFileNameTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOasAppRegFileNameSet (tSnmpIndex *, tRetVal *);
INT4 FutOasAppRegFileNameGet (tSnmpIndex *, tRetVal *);
INT4 FutOasAppDeRegFileNameTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOasAppDeRegFileNameSet (tSnmpIndex *, tRetVal *);
INT4 FutOasAppDeRegFileNameGet (tSnmpIndex *, tRetVal *);
INT4 FutOasAppOpqLsaSndFileNameTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOasAppOpqLsaSndFileNameSet (tSnmpIndex *, tRetVal *);
INT4 FutOasAppOpqLsaSndFileNameGet (tSnmpIndex *, tRetVal *);
INT4 FutOasAppRegFileNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOasAppDeRegFileNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOasAppOpqLsaSndFileNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
 #endif
