/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: osrmport.c,v 1.11 2017/09/21 13:48:47 siva Exp $
 *
 * Description:This file contains the ospf high availability handling
 *             routines.
 *
 *******************************************************************/
#ifndef _OSRED_C_
#define _OSRED_C_

#include "osinc.h"

#ifdef RM_WANTED
PRIVATE INT4        OspfRmRcvPktFromRm (UINT1, tRmMsg *, UINT2);
#endif

#ifdef RM_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmRcvPktFromRm                                         */
/*                                                                           */
/* Description  : This API constructs a message containing the               */
/*                given RM event and RM message. And posts it to the         */
/*                OSPF queue                                                 */
/*                                                                           */
/* Input        : u1Event   - Event type given by RM module                  */
/*              : pData     - RM Message to enqueue                          */
/*              : u2DataLen - Length of the message                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UINT4               u4RetValue = OSIX_FAILURE;
    tOspfQMsg          *pOspfQMsg = NULL;

    /* If the received event is not any of the following, then just return
     * without processing the event
     */
    if ((u1Event != RM_MESSAGE) && (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) && (u1Event != RM_PEER_UP) &&
        (u1Event != RM_PEER_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT) && (u1Event != RM_INIT_HW_AUDIT))
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Invalid event received from RM.\r\n");
        return OSIX_FAILURE;
    }

    /* pData message pointer is valid only if the event is
     * RM_MESSAGE/RM_PEER_UP/RM_PEER_DOWN. If the recevied
     * event is any of the above mentioned event then validate the
     * message pointer and message length
     */

    if ((u1Event == RM_MESSAGE) ||
        (u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
    {
        if (pData == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "NULL pointer message is received \r\n");
            return OSIX_FAILURE;
        }

        if (u1Event == RM_MESSAGE)
        {
            if (u2DataLen < OSPF_RED_MSG_HDR_SIZE)
            {
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                                  "Received invalid RM Message \r\n");
                RM_FREE (pData);
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                OspfRmRelRmMsgMem ((UINT1 *) pData);
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                                  "Received invalid standby node information \r\n");
                return OSIX_FAILURE;
            }
        }
    }

    QMSG_ALLOC (pOspfQMsg);

    if (pOspfQMsg == NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
        {
            OspfRmRelRmMsgMem ((UINT1 *) pData);
        }

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Memory allocation failed for message to post "
                          "OSPF queue \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pOspfQMsg, 0, sizeof (tOspfQMsg));

    pOspfQMsg->u4MsgType = OSPF_RED_IF_MSG;

    pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.u1Event = u1Event;
    pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.pFrame = pData;
    pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.u2Length = u2DataLen;

    u4RetValue = OsixSendToQ ((UINT4) 0, (const UINT1 *) "OSPQ",
                              (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL);

    if (u4RetValue == OSIX_SUCCESS)
    {
        u4RetValue = OsixSendEvent (SELF, (const UINT1 *) "OSPF",
                                    OSPF_MSGQ_IF_EVENT);

        if (u4RetValue == OSIX_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
    }

    QMSG_FREE (pOspfQMsg);

    if (u1Event == RM_MESSAGE)
    {
        RM_FREE (pData);
    }
    else if ((u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
    {
        OspfRmRelRmMsgMem ((UINT1 *) pData);
    }

    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                      "Sending RM message to OSPF queue failed \r\n");
    return OSIX_FAILURE;
}
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmRegisterWithRM                                       */
/*                                                                           */
/* Description  : This function calls the Redundancy Module API to register  */
/*                with the redundancy module.                                */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSPF_SUCCESS on Successfull registration.                  */
/*                else returns OSPF_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmRegisterWithRM (VOID)
{
#ifdef RM_WANTED
    UINT4               u4RetVal = RM_FAILURE;
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_OSPF_APP_ID;
    RmRegParams.pFnRcvPkt = OspfRmRcvPktFromRm;

    u4RetVal = RmRegisterProtocols (&RmRegParams);

    if (u4RetVal != RM_SUCCESS)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "OSPF registration with RM failed \r\n");
        return OSPF_FAILURE;
    }

    gOsRtr.osRedInfo.u1AdminState = OSPF_ENABLED;
#else
    gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_DOWN;
    gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_MOD_COMPLETED;
#endif

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmDeRegisterWithRM                                     */
/*                                                                           */
/* Description  : This function calls the Redundancy Module API for          */
/*                De-registration.                                           */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSPF_SUCCESS on Successfull De-registration.               */
/*                else returns OSPF_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmDeRegisterWithRM (VOID)
{
#ifdef RM_WANTED
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmDeRegisterProtocols (RM_OSPF_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "OSPF de-registration with RM failed \r\n");
        return OSPF_FAILURE;
    }
#endif

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmRelRmMsgMem                                          */
/*                                                                           */
/* Description  : This function calls the RM API to release the memory       */
/*                allocated for RM message.                                  */
/*                                                                           */
/* Input        : pu1Block - Memory block to be released                     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSPF_SUCCESS on Successfull release        .               */
/*                else returns OSPF_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT4
OspfRmRelRmMsgMem (UINT1 *pu1Block)
{
#ifdef RM_WANTED
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmReleaseMemoryForMsg (pu1Block);

    if (u4RetVal != RM_SUCCESS)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Releasing the RM message memory failed \r\n");
        gu4RmReleaseMemoryForMsgFail++;
        return OSPF_FAILURE;
    }
#else
    UNUSED_PARAM (pu1Block);
#endif

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmAllocForRmMsg                                        */
/*                                                                           */
/* Description  : This function allocates memory for the RM messages.        */
/*                If allocation fails the send the event to RM.              */
/*                                                                           */
/* Input        : u2Size - required memory size in bytes.                    */
/*                                                                           */
/* Output       : pRmMsg - pointer to the allocated memory.                  */
/*                                                                           */
/* Returns      : OSPF_SUCCESS on Successfull release.                       */
/*                else returns OSPF_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
tRmMsg             *
OspfRmAllocForRmMsg (UINT2 u2Size)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pRmMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

#ifdef L2RED_WANTED
    pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2Size);
#else
    UNUSED_PARAM (u2Size);
#endif

    if (pRmMsg == NULL)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Memory allocation failed \r\n");
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;

        /* Send bulk update failure event to RM */
        OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
    }

    return pRmMsg;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmGetStandbyNodeCount                                  */
/*                                                                           */
/* Description  : This function calls the RM API to get the stan by node     */
/*                count.                                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                None                                                       */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Standby node count                                         */
/*                                                                           */
/*****************************************************************************/
UINT1
OspfRmGetStandbyNodeCount ()
{
    UINT1               u1StandbyCount = 0;

#ifdef RM_WANTED
    u1StandbyCount = RmGetStandbyNodeCount ();
#endif

    return u1StandbyCount;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmGetRmNodeState                                       */
/*                                                                           */
/* Description  : This function calls the RM API to get the node state.      */
/*                                                                           */
/* Input        : None                                                       */
/*                None                                                       */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Node state.                                                */
/*                                                                           */
/*****************************************************************************/
UINT4
OspfRmGetRmNodeState ()
{
    UINT4               u4RmState = RM_ACTIVE;

#ifdef RM_WANTED
    u4RmState = RmGetNodeState ();
#endif

    return u4RmState;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendMsgToRm                                          */
/*                                                                           */
/* Description  : This function calls the RM API to en-queue the             */
/*                message from OSPF to RM task.                              */
/*                                                                           */
/* Input        : pRmMsg - Message from OSPF.                                */
/*                u2DataLen - Length of message                              */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSPF_SUCCESS on Successfull en-queue                       */
/*                else returns OSPF_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmSendMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
#ifdef RM_WANTED
    UINT4               u4SrcEntId = (UINT4) RM_OSPF_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_OSPF_APP_ID;
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                                     u4SrcEntId, u4DestEntId);
    if (u4RetVal != RM_SUCCESS)
    {
        /* Memory allocated for pRmMsg is freed here, only in failure case.
         * In success case RM will free the memory
         */
        RM_FREE (pRmMsg);
        gu4RmEnqMsgToRmFromApplFail++;
        return OSPF_FAILURE;
    }
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
#endif

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : OspfRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSPF_SUCCESS/OSPF_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return OSPF_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendEventToRm                                       */
/*                                                                           */
/* Description  : This function calls the RM API to post the event           */
/* Input        : u1Event - Event to be sent to RM. And the event can be     */
/*                any of the following                                       */
/*                RM_PROTOCOL_BULK_UPDT_COMPLETION                           */
/*                RM_BULK_UPDT_ABORT                                         */
/*                RM_STANDBY_TO_ACTIVE_EVT_PROCESSED                         */
/*                RM_IDLE_TO_ACTIVE_EVT_PROCESSED                            */
/*                RM_STANDBY_EVT_PROCESSED                                   */
/*                                                                           */
/*                u1Error - In case the event is RM_BULK_UPDATE_ABORT,       */
/*                the reason for the failure is send in the error. And       */
/*                the error can be any of the following,                     */
/*                RM_MEMALLOC_FAIL                                           */
/*                RM_SENTO_FAIL                                              */
/*                RM_PROCESS_FAIL                                            */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSPF_SUCCESS on Success                                    */
/*                else returns OSPF_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmSendEventToRm (tRmProtoEvt * pEvt)
{
#ifdef RM_WANTED
    INT4                i4RetVal = RM_FAILURE;

    i4RetVal = RmApiHandleProtocolEvent (pEvt);

    if (i4RetVal != RM_SUCCESS)
    {
        gu4RmApiHandleProtocolEventFail++;
        return OSPF_FAILURE;
    }
#else
    UNUSED_PARAM (pEvt);
#endif
    return OSPF_SUCCESS;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : OspfRmSetBulkUpdateStatus
 *
 * DESCRIPTION        : This function sets OSPF bulk update status in RM
 *
 * INPUT              : NONE
 *
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE
 *
 ****************************************************************************/
PUBLIC VOID
OspfRmSetBulkUpdateStatus (VOID)
{
#ifdef RM_WANTED
    RmSetBulkUpdatesStatus (RM_OSPF_APP_ID);
#endif
}

/******************************************************************************
 * Function           : OspfRmApiSendProtoAckToRM           
 * Input(s)           : u4SeqNum - Sequence number of the RM message for
 *                                  which this ACK is generated.
 * Output(s)          : None.
 * Returns            : NONE
 * Action             : This is the function used by OSPF task  
 *                      to send acknowledgement to RM after processing the
 *                      sync-up message.
 ******************************************************************************/
PUBLIC VOID
OspfRmApiSendProtoAckToRM (UINT4 u4SeqNum)
{
#ifdef RM_WANTED
    tRmProtoAck         ProtoAck;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    ProtoAck.u4AppId = RM_OSPF_APP_ID;
    ProtoAck.u4SeqNumber = u4SeqNum;
    RmApiSendProtoAckToRM (&ProtoAck);
#else
    UNUSED_PARAM (u4SeqNum);
#endif
}
#endif
