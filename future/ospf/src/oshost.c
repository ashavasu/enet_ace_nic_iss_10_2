/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oshost.c,v 1.14 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures for configuration 
 *             of host routes
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function        :  HostAddToHostsLst                                  */
/*                                                                           */
/* Description     :  Adds the specified host to the list of hosts maintained*/
/*                    in the OS Router structure. The specified host is      */
/*                    inserted in such a way as to maintain the sorted order.*/
/*                                                                           */
/* Input           :  pHost          :   ptr to host                        */
/*                                                                           */
/* Output          :  None                                                   */
/*                                                                           */
/* Returns         :  VOID                                                   */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HostAddToHostsLst (tHost * pHost)
{
    tHost              *pPrevHost;
    tHost              *pLstHost;

    pPrevHost = (tHost *) NULL;
    TMO_SLL_Scan (&(pHost->pOspfCxt->hostLst), pLstHost, tHost *)
    {
        if (UtilIpAddrComp (pLstHost->hostIpAddr, pHost->hostIpAddr)
            == OSPF_GREATER)
        {
            break;
        }
        pPrevHost = pLstHost;
    }
    TMO_SLL_Insert (&(pHost->pOspfCxt->hostLst), &pPrevHost->nextHost,
                    &pHost->nextHost);
}

/*****************************************************************************/
/*                                                                           */
/* Function        : HostAdd                                                 */
/*                                                                           */
/* Description     : This procedure creates the host entry, initializes      */
/*                   and adds the specified host route to the router         */
/*                   structure.                                              */
/*                                                                           */
/* Input           : pHostAddr     :   ptr to host IP address                */
/*                   u2FwdIfIndex :   interface to reach the host            */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : SUCCESS/FAILURE                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tHost       *
HostAddInCxt (tOspfCxt * pOspfCxt, tIPADDR * pHostAddr, UINT4 u4FwdIfIndex)
{
    tHost              *pHost;
    INT1                i1Tos;

    OSPF_TRC2 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "FUNC:HostAdd\n host addr = %08x, if index = %d\n",
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pHostAddr)), u4FwdIfIndex);
    if (HOST_ALLOC (pHost) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Host Alloc Failed\n");
        return NULL;
    }

    TMO_SLL_Init_Node (&(pHost->nextHost));
    pHost->pOspfCxt = pOspfCxt;
    IP_ADDR_COPY (pHost->hostIpAddr, *pHostAddr);
    pHost->u4FwdIfIndex = u4FwdIfIndex;
    pHost->pArea = pOspfCxt->pBackbone;

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
        pHost->aHostCost[(INT2) i1Tos].u4Value = 0;
        pHost->aHostCost[(INT2) i1Tos].rowStatus = INVALID;
        pHost->aHostCost[(INT2) i1Tos].rowMask = 0;
    }

    HostAddToHostsLst (pHost);

    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId, "Host Added\n");
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT HostAdd\n");
    return pHost;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : HostIfmtrcPrmChange                                  */
/*                                                                           */
/* Description     : This routine is invoked to start the timer for rtr lsa  */
/*                   generation when any host or if_metric param changes     */
/*                                                                           */
/* Input           : pArea          :   ptr to area to which the host or    */
/*                                       Interface belongs to                */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HostIfmtrcPrmChange (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tNeighbor          *pNbr = NULL;

    /* If the interface is down, If this router is helper for any neighbor
     * Stop the helper process */
    if ((pArea->pOspfCxt->u1HelperStatus == OSPF_GR_HELPING) &&
        (pArea->pOspfCxt->u1StrictLsaCheck == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pArea->pOspfCxt->sortNbrLst), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_SORT_LST (pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                GrExitHelper (OSPF_HELPER_TOP_CHG, pNbr);
            }
        }
    }
    /* concerned lsa is to be regenerated */

    if ((pLsaInfo = LsuSearchDatabase (ROUTER_LSA, &(pArea->pOspfCxt->rtrId),
                                       &(pArea->pOspfCxt->rtrId),
                                       (UINT1 *) NULL,
                                       (UINT1 *) pArea)) != NULL)
    {

        OlsStartTimerForLsaRegen (pLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function        : HostDelete                                              */
/*                                                                           */
/* Description     : deletes the specified host from the router list         */
/*                   and frees the memory                                    */
/*                                                                           */
/* Input           : pHost          :   ptr to host                          */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
HostDelete (tHost * pHost)
{
    TMO_SLL_Delete (&(pHost->pOspfCxt->hostLst), &(pHost->nextHost));
    HOST_FREE (pHost);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :   HostDeleteAll                                         */
/*                                                                           */
/* Description     :   This routine deletes all the host routes              */
/*                                                                           */
/* Input           :   None                                                  */
/*                                                                           */
/* Output          :   None                                                  */
/*                                                                           */
/* Returns         :   VOID                                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
HostDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tHost              *pHost;
    while ((pHost = (tHost *) TMO_SLL_First (&(pOspfCxt->hostLst))) != NULL)
    {
        HostDelete (pHost);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HostCheckHostStatus                                     */
/*                                                                           */
/* Description  : This routine checks the bStatus of various tos entries in */
/*                the host. If all the tos entries are invalid then the host */
/*                route is deleted.                                          */
/*                                                                           */
/* Input        : pHost         : pointer to the host                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_VALID, if atleast one tos value is VALID              */
/*                OSPF_INVALID, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
HostCheckHostStatus (tHost * pHost)
{
    INT1                i1Tos;

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {

        if (pHost->aHostCost[(INT2) i1Tos].rowStatus != INVALID)
        {
            return OSPF_VALID;
        }
    }
    return OSPF_INVALID;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  oshost.c                       */
/*-----------------------------------------------------------------------*/
