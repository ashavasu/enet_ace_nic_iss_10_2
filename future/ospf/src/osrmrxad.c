/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: osrmrxad.c,v 1.12 2017/09/21 13:48:47 siva Exp $
 *
 * Description: This file contains definitions related to bulk
 *              update for High Availability
 *******************************************************************/
#ifndef _OSREDBLK_
#define _OSREDBLK_

#include "osinc.h"
PRIVATE UINT1       gau1Pkt[MAX_HELLO_SIZE];

/***************************************************************/
/*  Function Name   :OspfRmProcessHello                       */
/*  Description     :This function is invoked at standby node  */
/*                   when it gets the Hello PDU from active    */
/*                   through RM. This function is invoked from */
/*                   OspfRmHandleBulkUpdateMsg or from        */
/*                   OspfRmHandleRMMessage. This function gets*/
/*                   the hello, source address and interface   */
/*                   information from pRmMesg and do the       */
/*                   following steps                           */
/*                         If bulk update is in progress check */
/*                           the interface state if interface  */
/*                           state is down then it calls       */
/*                           ospfIfUp to make the interface    */
/*                           status up then                    */
/*                         It calls the HpRcvHello to process  */
/*                           the hello packet.                 */
/*                                                             */
/*                   The pRmMesg may have more than one hello  */
/*                   so this function will get all the hello   */
/*                   packets and do the above process till     */
/*                   the number of hellos in the message       */
/*                   reaches zero.                             */
/*                                                             */
/*  Input(s)        :pRmMesg - The message received from RM    */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :OSPF_SUCCESS/OSPF_FAILURE                 */
/***************************************************************/
INT4
OspfRmProcessHello (tRmMsg * pRmMesg, UINT4 *pu4Offset)
{
    tIPADDR             SrcIpAddr;
    tIPADDR             IfIpAddr;
    tInterface         *pInterface = NULL;
    tRouterId           nbrId;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tOspfCxt           *pContext = NULL;
    UINT4               u4AddrlessIf = 0;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4ContextId = OSPF_INVALID_CXT_ID;
    UINT2               u2HelloPktLen = 0;
    tRouterId           headerRtrId;
    tAreaId             areaId;
    tInterface         *pAssoIface;
    UINT1               u1Version;
    UINT4               u4CryptSeqNum = 0;

    if (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Received Bulk Hello Sync At active  \n");
        return OSPF_FAILURE;
    }
    MEMSET (&gau1Pkt, 0, MAX_HELLO_SIZE);
    gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_HELLO_MOD;

    /* Get the hello packet header from each hello neighbor */
    OSPF_CRU_BMC_GET_STRING (pRmMesg, nbrId, u4Offset, MAX_IP_ADDR_LEN);
    u4Offset += MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_GET_STRING (pRmMesg, SrcIpAddr, u4Offset, MAX_IP_ADDR_LEN);
    u4Offset += MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4AddrlessIf);
    u4Offset += sizeof (UINT4);

    OSPF_CRU_BMC_GET_STRING (pRmMesg, IfIpAddr, u4Offset, MAX_IP_ADDR_LEN);
    u4Offset += MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4ContextId);
    u4Offset += sizeof (UINT4);

    OSPF_CRU_BMC_GET_2_BYTE (pRmMesg, u4Offset, u2HelloPktLen);
    u4Offset += sizeof (UINT2);

    /* Get the hello packet from each neighbor */
    OSPF_CRU_BMC_GET_STRING (pRmMesg, gau1Pkt, u4Offset, u2HelloPktLen);
    u4Offset += u2HelloPktLen;
    if ((pMsg = UtilOsMsgAlloc (u2HelloPktLen)) == NULL)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Hello Syn Alloc Failed \n");
        if (OSPF_IS_BULKUPDATE_INPROGRESS () == OSPF_TRUE)
        {
            OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
        }
        return OSPF_FAILURE;
    }
    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, gau1Pkt, 0, u2HelloPktLen);

    if ((pContext = UtilOspfGetCxt (u4ContextId)) == NULL)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Context is not existing \n");
        UtilOsMsgFree (pMsg, NORMAL_RELEASE);
        return OSPF_FAILURE;
    }
    if ((pInterface = GetFindIfInCxt (pContext, IfIpAddr,
                                      u4AddrlessIf)) == NULL)
    {

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Processing Hello From Active: "
                          "dropping unwanted packets\n ");
        UtilOsMsgFree (pMsg, NORMAL_RELEASE);
        return OSPF_FAILURE;
    }

    if ((pInterface->u2AuthType == CRYPT_AUTHENTICATION)
        && (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5))
    {
        /*Get the hello packet seqno */
        OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4CryptSeqNum);
        u4Offset += sizeof (UINT4);
        pInterface->u4CryptSeqNum = u4CryptSeqNum;
    }
    /* Increment the Hello Count and Call the 
     * HpRcvHello to process the hello */
    COUNTER_OP (pInterface->u4HelloRcvdCount, OSPF_ONE);
    OSPF_CRU_BMC_GET_1_BYTE (pMsg, VERSION_NO_OFFSET_IN_HEADER, u1Version);
    OSPF_CRU_BMC_GET_STRING (pMsg, headerRtrId, RTR_ID_OFFSET_IN_HEADER,
                             MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_GET_STRING (pMsg, areaId, AREA_ID_OFFSET_IN_HEADER,
                             MAX_IP_ADDR_LEN);

    if ((UtilIpAddrComp (areaId, pInterface->pArea->areaId) != OSPF_EQUAL)
        && (UtilIpAddrComp (areaId, gBackboneAreaId) == OSPF_EQUAL))
    {
        /* associate with virtual interface */
        if ((pAssoIface = PppAssociateWithVirtualIf (pInterface,
                                                     &headerRtrId)) == NULL)
        {
            if (OSPF_IS_BULKUPDATE_INPROGRESS () == OSPF_TRUE)
            {
                OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
            }
            /* Free the duplicate buffer */
            UtilOsMsgFree (pMsg, NORMAL_RELEASE);
            *pu4Offset = u4Offset;
            return OSPF_SUCCESS;
        }
        pInterface = pAssoIface;
    }
    HpRcvHello (pMsg, u2HelloPktLen, pInterface, &SrcIpAddr);
    /* Free the duplicate buffer */
    UtilOsMsgFree (pMsg, NORMAL_RELEASE);

    gOsRtr.osRedInfo.u4HelloSyncCount++;
    *pu4Offset = u4Offset;
    return OSPF_SUCCESS;
}

/***************************************************************/
/*  Function Name   :OspfRmProcessBulkNbrState                */
/*  Description     :This function is invoked at standby node  */
/*                   when it gets the Nbr State change from    */
/*                   active                                    */
/*                                                             */
/*  Input(s)        :pRmMesg - The message received from RM    */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :OSPF_SUCCESS/OSPF_FAILURE                 */
/***************************************************************/
INT4
OspfRmProcessBulkNbrState (tRmMsg * pRmMesg, UINT4 *pu4Offset)
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4IfAddlessIf = 0;
    tNeighbor          *pNeighbor = NULL;
    tInterface         *pInterface = NULL;
    UINT4               u4Offset = *pu4Offset;
    UINT1               u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
    UINT4               u4RemGracePeriod = 0;
    UINT1               u1NbrNsmState = NBRS_DOWN;
    tIPADDR             nbrId;
    tIPADDR             IfIpAddr;
    tAreaId             transitAreaId;
    tOPTIONS            nbrOptions;
    UINT1               u1NetworkType = 0;

    if (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Received Bulk Nbr State Sync At active  \n");
        return OSPF_FAILURE;
    }
    gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_NBR_MOD;

    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4OspfCxtId);
    u4Offset += sizeof (UINT4);
    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4IfAddlessIf);
    u4Offset += sizeof (UINT4);
    OSPF_CRU_BMC_GET_STRING (pRmMesg, IfIpAddr, u4Offset, MAX_IP_ADDR_LEN);
    u4Offset += MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, nbrOptions);
    u4Offset += sizeof (UINT1);

    OSPF_CRU_BMC_GET_STRING (pRmMesg, nbrId, u4Offset, MAX_IP_ADDR_LEN);
    u4Offset += MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, u1NetworkType);
    u4Offset += sizeof (UINT1);

    if (u1NetworkType == IF_VIRTUAL)
    {
        OSPF_CRU_BMC_GET_STRING (pRmMesg, transitAreaId,
                                 u4Offset, MAX_IP_ADDR_LEN);
        u4Offset += MAX_IP_ADDR_LEN;
    }

    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, u1NbrNsmState);
    u4Offset += sizeof (UINT1);

    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, u1NbrHelperStatus);
    u4Offset += sizeof (UINT1);

    if (u1NbrHelperStatus == OSPF_GR_HELPING)
    {
        OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4RemGracePeriod);
        u4Offset += sizeof (UINT4);
    }
    /* Get the context pointer corresponding to the 
     * context structure */
    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt == NULL)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Context is not existing \n");
        return OSPF_FAILURE;
    }
    if (u1NetworkType == IF_VIRTUAL)
    {
        pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId);
        if (pInterface == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Interface Not Found \n");

            return OSPF_FAILURE;
        }
    }
    else
    {

        if ((pInterface = GetFindIfInCxt (pOspfCxt, IfIpAddr,
                                          u4IfAddlessIf)) == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Interface Not Found \n");
            return OSPF_FAILURE;

        }
    }

    pNeighbor = HpSearchNbrLst (&nbrId, pInterface);

    if (pNeighbor != NULL)
    {
        /* update the neighbor options */
        pNeighbor->nbrOptions = nbrOptions;
        if (OSPF_IS_BULKUPDATE_INPROGRESS () == OSPF_TRUE)
        {
            switch (u1NbrNsmState)
            {
                case NBRS_2WAY:
                    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC | OSPF_CRITICAL_TRC,
                                      OSPF_INVALID_CXT_ID,
                                      "Two way state sync is received");
                    OSPF_TRC1 (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                               "NBRE: Two way state sync is recieved\n"
                               "for Neighbour with Address = %x\n",
                               OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrIpAddr));
                    GENERATE_NBR_EVENT (pNeighbor, NBRE_2WAY_RCVD);
                    break;

                case NBRS_RSTADJEXSTART:
                    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                                      OSPF_INVALID_CXT_ID,
                                      "Restart Adjacency state sync is received");
                    NsmRestartAdj (pNeighbor);
                    break;

                case NBRS_FULL:
                    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                                      OSPF_INVALID_CXT_ID,
                                      "Full state sync is received");
                    NbrUpdateState (pNeighbor, NBRS_FULL);
                    break;

                case NBRS_DOWN:
                    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                                      OSPF_INVALID_CXT_ID,
                                      "Down state sync is received ");
                    NsmLlDown (pNeighbor);
                    break;

                default:
                    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                                      OSPF_INVALID_CXT_ID,
                                      "None of the State is matching ");
                    break;
            }

            if (u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                pNeighbor->u1NbrHelperStatus = OSPF_GR_HELPING;
                /* Update u1GRSupportFlag in pInterface, this 
                 * flag is required in helper mode to know 
                 * atlease on GR router present in this 
                 * interface as a neighbor */
                pNeighbor->pInterface->u1GRSupportFlag = OSPF_TRUE;

                /* Start the Grace Timer for GR Neighbor */
                /* Helper will support upto the configured 
                 * Grace time limit only */
                TmrSetTimer (&(pNeighbor->helperGraceTimer),
                             HELPER_GRACE_TIMER,
                             (NO_OF_TICKS_PER_SEC * u4RemGracePeriod));
            }
        }
    }
    *pu4Offset = u4Offset;
    return OSPF_SUCCESS;
}

/***************************************************************/
/*  Function Name   :OspfRmProcessIfState                     */
/*  Description     :This function is invoked at standby node  */
/*                   when it gets interface state change event */
/*                   from active through RM. This function is  */
/*                   invoked from OspfRmHandleRMMessage.      */
/*                   This function will do the following       */
/*                       It gets the pOspfIfParams from pRmMesg*/
/*                       call OspfProcessIfStateChg to handle  */
/*                       the interface state change            */
/*                                                             */
/*  Input(s)        :pRmMesg - The message received from RM    */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :OSPF_SUCCESS/OSPF_FAILURE                 */
/***************************************************************/
INT4
OspfRmProcessIfState (tRmMsg * pRmMesg, UINT4 *pu4Offset)
{
    tInterface          Interface;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4ContextId;
    UINT1               u1IsmState = 0;
    UINT4               u4BitMap = ZERO;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4AddrlessIf = 0;

    if (gOsRtr.osRedInfo.u4OsRmState != OSPF_RED_STANDBY)
    {
        /* Only Standby node can process sync messages */
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is active");
        return OSPF_SUCCESS;
    }

    MEMSET (&Interface, 0, sizeof (tInterface));

    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4ContextId);
    u4Offset += sizeof (UINT4);
    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, Interface.u1NetworkType);
    u4Offset = u4Offset + sizeof (UINT1);
    if (Interface.u1NetworkType == IF_VIRTUAL)
    {
        OSPF_CRU_BMC_GET_STRING (pRmMesg, Interface.transitAreaId, u4Offset,
                                 MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;
        OSPF_CRU_BMC_GET_STRING (pRmMesg, Interface.destRtrId, u4Offset,
                                 MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    }
    OSPF_CRU_BMC_GET_STRING (pRmMesg, Interface.ifIpAddr, u4Offset,
                             MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, Interface.u4AddrlessIf);
    u4Offset = u4Offset + sizeof (UINT4);
    OSPF_CRU_BMC_GET_STRING (pRmMesg, Interface.ifIpAddrMask, u4Offset,
                             MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, Interface.operStatus);
    u4Offset += sizeof (tOSPFSTATUS);
    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4BitMap);
    u4Offset += sizeof (UINT4);

    if ((pOspfCxt = UtilOspfGetCxt (u4ContextId)) == NULL)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Context is not existing, interface state update failed \n");
        return OSPF_FAILURE;
    }
    if (Interface.u1NetworkType == IF_VIRTUAL)
    {
        pInterface = GetFindVirtIfInCxt (pOspfCxt,
                                         &(Interface.transitAreaId),
                                         &(Interface.destRtrId));
        if (pInterface == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Interface Not Found \n");

            return OSPF_FAILURE;
        }
    }
    else
    {
        if ((pInterface = GetFindIfInCxt (pOspfCxt, Interface.ifIpAddr,
                                          u4AddrlessIf)) == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Interface Not Found, Interface state update failed \n");
            return OSPF_FAILURE;

        }
    }

    /* Interface OperState will be processed in IfSetOperStat function */
    if (pInterface->operStatus == Interface.operStatus)
    {
        OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, u1IsmState);
        IfUpdateState (pInterface, u1IsmState);
    }
    u4Offset = u4Offset + sizeof (UINT1);

    OSPF_CRU_BMC_GET_STRING (pRmMesg, pInterface->desgRtr, u4Offset,
                             MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_GET_STRING (pRmMesg, pInterface->backupDesgRtr, u4Offset,
                             MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, pInterface->u4MtuSize);
    u4Offset += sizeof (UINT4);
    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset,
                             pInterface->aIfOpCost[TOS_0].u4Value);
    u4Offset += sizeof (UINT4);
    *pu4Offset = u4Offset;

    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        return OSPF_SUCCESS;
    }
    if (OS_MEM_CMP (Interface.ifIpAddr, pInterface->ifIpAddr, sizeof (tIPADDR))
        != 0)
    {
        /* IP Address has changed so delete the OSPF Interface */
        OSPF_TRC (CONTROL_PLANE_TRC, u4ContextId,
                  "Iface Address Chg Notification\n");
        OSPF_TRC (CONTROL_PLANE_TRC, u4ContextId,
                  "Delete the OSPF Interface\n");
        IfDelete (pInterface);
        return OSPF_SUCCESS;
    }
    if (u4BitMap == IFACE_DELETED)
    {
        OSPF_TRC (CONTROL_PLANE_TRC, u4ContextId,
                  "Iface Deleted Chg Notification\n");
        IfDelete (pInterface);
        return OSPF_SUCCESS;
    }
    if ((OS_MEM_CMP (Interface.ifIpAddrMask, pInterface->ifIpAddrMask,
                     sizeof (tIPADDRMASK)) != 0))
    {
        IfSetOperStat (pInterface, OSPF_DISABLED);
        OS_MEM_CPY (&(pInterface->ifIpAddrMask), &(Interface.ifIpAddrMask),
                    sizeof (tIPADDRMASK));
        IfSetOperStat (pInterface, OSPF_ENABLED);
        /* IP Address has changed so delete the OSPF Interface */
        OSPF_TRC (CONTROL_PLANE_TRC, u4ContextId,
                  "Iface Address mask Chg Notification\n");
    }
    if (pInterface->operStatus != Interface.operStatus)
    {
        OSPF_TRC (CONTROL_PLANE_TRC, u4ContextId,
                  "OperState Chg Notification\n");
        IfSetOperStat (pInterface, ((UINT1) Interface.operStatus));
    }
    return OSPF_SUCCESS;
}

/***************************************************************/
/*  Function Name   :OspfRmProcessBulkExtRouteInfo             */
/*  Description     :This function is invoked at standby node  */
/*                   when it gets external route information   */
/*                   from active through RM.                   */
/*                                                             */
/*  Input(s)        :pMsg - The message received from RM       */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :OSPF_SUCCESS/OSPF_FAILURE                 */
/***************************************************************/
PUBLIC VOID
OspfRmProcessBulkExtRouteInfo (tRmMsg * pMsg)
{
    tExtRoute           extRoute;
    tInputParams        inParams;
    tOutputParams       outParams;
    tExtRoute          *pExtRoute = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               au4Index[2];
    UINT4               u4Offset = OSPF_ONE;
    UINT4               u4OspfCxtId = OSPF_DEFAULT_CXT_ID;
    UINT2               u2PktLen = OSPF_ZERO;
    UINT1               u1SubMsgType = OSPF_ZERO;
    UINT1               u1Tos = OSPF_ZERO;

    /* Extract Total Message length */
    OSPF_RED_GET_2_BYTE (pMsg, u4Offset, u2PktLen);

    while (u4Offset < u2PktLen)
    {
        /* Read the SubMessageType */
        OSPF_RED_GET_1_BYTE (pMsg, u4Offset, u1SubMsgType);

        MEMSET (&extRoute, 0, sizeof (tExtRoute));
        OSPF_CRU_BMC_GET_4_BYTE (pMsg, u4Offset, u4OspfCxtId);
        u4Offset = u4Offset + sizeof (UINT4);

        OSPF_CRU_BMC_GET_STRING (pMsg, extRoute.ipNetNum, u4Offset,
                                 MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;

        OSPF_CRU_BMC_GET_STRING (pMsg, extRoute.ipAddrMask, u4Offset,
                                 MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;

        OSPF_CRU_BMC_GET_STRING (pMsg, (UINT1 *) &(extRoute.aMetric), u4Offset,
                                 (sizeof (tMibMetric) * OSPF_MAX_METRIC));
        u4Offset = u4Offset + (sizeof (tMibMetric) * OSPF_MAX_METRIC);

        OSPF_CRU_BMC_GET_STRING (pMsg, (UINT1 *) &(extRoute.extrtParam),
                                 u4Offset,
                                 (sizeof (tExtrtParam) * OSPF_MAX_METRIC));
        u4Offset = u4Offset + (sizeof (tExtrtParam) * OSPF_MAX_METRIC);

        OSPF_CRU_BMC_GET_2_BYTE (pMsg, u4Offset, extRoute.u2SrcProto);
        u4Offset = u4Offset + sizeof (UINT2);

        OSPF_CRU_BMC_GET_1_BYTE (pMsg, u4Offset, extRoute.u1Level);
        u4Offset = u4Offset + sizeof (UINT1);
        if ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL)
        {
            continue;
        }
        pExtRoute =
            GetFindExtRouteInCxt (pOspfCxt, &(extRoute.ipNetNum),
                                  &(extRoute.ipAddrMask));
        if (pExtRoute == NULL)
        {
            if (EXT_ROUTE_ALLOC (pExtRoute) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId, "EXT ROUTE Alloc Failed\n");

                continue;
            }
            pExtRoute->pAsExtAddrRange = NULL;
            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                pExtRoute->aMetric[u1Tos].rowStatus = INVALID;
            }

            TMO_SLL_Init_Node (&(pExtRoute->nextExtRoute));
            TMO_SLL_Init_Node (&(pExtRoute->nextExtRouteInRange));

            inParams.pLeafNode = NULL;
            inParams.u1PrefixLen = UtilFindMaskLen (OSPF_CRU_BMC_DWFROMPDU
                                                    (extRoute.ipAddrMask));
            inParams.pRoot = pOspfCxt->pExtRtRoot;
            inParams.i1AppId = OSPF_EXT_RT_ID;
            au4Index[0] =
                OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (extRoute.ipNetNum));
            au4Index[1] =
                OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (extRoute.ipAddrMask));

            inParams.Key.pKey = (UINT1 *) au4Index;

            outParams.pAppSpecInfo = NULL;
            outParams.Key.pKey = NULL;

            if (TrieAdd (&inParams, (VOID *) pExtRoute, &outParams) !=
                TRIE_SUCCESS)
            {
                OSPF_TRC (OSPF_RT_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Failed to add the route entry to TRIE\n");
                EXT_ROUTE_FREE (pExtRoute);
                return;

            }

            pOspfCxt->u4ExtRtCount++;
        }
        pExtRoute->u2SrcProto = extRoute.u2SrcProto;
        pExtRoute->u1Level = extRoute.u1Level;
        pExtRoute->u1ExtRtDelFlag = DONT_DELETE_EXT_ROUTE;

        IP_ADDR_COPY (pExtRoute->ipNetNum, extRoute.ipNetNum);
        IP_ADDR_COPY (pExtRoute->ipAddrMask, extRoute.ipAddrMask);
        OSPF_TRC2 (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                   "Dest Ip Addr = %x, Addr Mask = %x\n",
                   pExtRoute->ipNetNum, pExtRoute->ipAddrMask);
        MEMCPY (pExtRoute->aMetric, &(extRoute.aMetric),
                (sizeof (tMibMetric) * OSPF_MAX_METRIC));

        MEMCPY (pExtRoute->extrtParam, &(extRoute.extrtParam),
                (sizeof (tExtrtParam) * OSPF_MAX_METRIC));

        ExtrtActivateInCxt (pOspfCxt, pExtRoute);

    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessTmrInfo                                       */
/*                                                                           */
/* Description  : This function processes the Timer info update from the     */
/*                active node.                                               */
/*                                                                           */
/* Input        : pRmMsg - RM bulk update message                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfRmProcessTmrInfo (tRmMsg * pMsg)
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4Offset = OSPF_ONE;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4RemainingTime = OSPF_ZERO;
    UINT1               u1SubMsgType;
    UINT2               u2PktLen = OSPF_ZERO;
    INT4                i4RetVal = OSPF_SUCCESS;

    /* Extract Total Messge length */
    OSPF_RED_GET_2_BYTE (pMsg, u4Offset, u2PktLen);

    while ((u4Offset < u2PktLen) && (i4RetVal == OSPF_SUCCESS))
    {
        /* Read the SubMessageType */
        OSPF_RED_GET_1_BYTE (pMsg, u4Offset, u1SubMsgType);
        if (u1SubMsgType == OSPF_RED_SYNC_DBOVERFLOW_TMR_MSG)
        {
            if (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE)
            {
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                                  "Received Bulk DB Timer Sync At active  \n");
                i4RetVal = OSPF_FAILURE;
            }
            gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_TMR_MOD;
            OSPF_CRU_BMC_GET_4_BYTE (pMsg, u4Offset, u4OspfCxtId);
            u4Offset += sizeof (UINT4);
            OSPF_CRU_BMC_GET_4_BYTE (pMsg, u4Offset, u4RemainingTime);
            u4Offset += sizeof (UINT4);

            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
            if (pOspfCxt != NULL)
            {
                pOspfCxt->bOverflowState = OSPF_TRUE;
                TmrSetTimer (&(pOspfCxt->exitOverflowTimer),
                             EXIT_OVERFLOW_TIMER, UtilJitter (u4RemainingTime,
                                                              OSPF_JITTER));
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                                  "StandBy: Exit Over flow interval started\n");
            }
        }
        else
        {
            /* Error in getting sub message type */
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Error in fetching sub message type for Timer Info Msg\n");
            i4RetVal = OSPF_FAILURE;
        }
    }
    return;
}
#endif /* _OSREDBLK_ */
