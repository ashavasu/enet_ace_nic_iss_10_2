/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osarea.c,v 1.40 2017/09/21 13:48:45 siva Exp $
 *
 * Description: This file contains procedures for configuration 
 *
 *******************************************************************/

#include "osinc.h"
#include "ospfcli.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID AreaSetDefaultValues PROTO ((tArea * pArea));
PRIVATE INT4 AreaAsExtRtAggrTrieCreate PROTO ((tArea * pArea));

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaCreate                                                */
/*                                                                           */
/* Description  : Creates an AREA.                                           */
/*                                                                           */
/* Input        : pAreaId         : Area Identifier                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the AREA created on success                     */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tArea       *
AreaCreateInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId)
{
    tArea              *pArea;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: AreaCreate\n");
    OSPF_TRC1 (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Area ID = %x", OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAreaId));

    if (AREA_ALLOC (pArea) == NULL)
    {

        OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                   pOspfCxt->u4OspfCxtId, "AreaId %x Alloc Failure\n",
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAreaId));
        CLI_SET_ERR (CLI_OSPF_AREA_ALLOC_FAILED);
        return NULL;
    }

    AreaSetDefaultValues (pArea);

    /* Trie Created for storing External Route Aggregation entries. */
    if (AreaAsExtRtAggrTrieCreate (pArea) == OSPF_FAILURE)
    {
        OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                   "AreaId %x AS External Route Trie Creation Failure\n",
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAreaId));

        /* Free RB Trees and hash table created in AreaSetDefaultValues */
        if (pArea->pNbrTbl != NULL)
        {
            RBTreeDelete (pArea->pNbrTbl);
        }
        if (pArea->pLsaHashTable != NULL)
        {
            TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
        }
        if (pArea->pSummaryLsaRoot != NULL)
        {
            RBTreeDelete (pArea->pSummaryLsaRoot);
        }

        AREA_FREE (pArea);
        CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
        return NULL;
    }

    IP_ADDR_COPY (pArea->areaId, *pAreaId);

    if ((pArea->pSpf =
         TMO_HASH_Create_Table (SPF_HASH_TABLE_SIZE, NULL, FALSE)) == NULL)
    {
        OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                   pOspfCxt->u4OspfCxtId,
                   "SPF Tree Hash Table Failed for AreaId %x\n",
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAreaId));
        AreaAsExtRtAggrTrieDelete (pArea);

        /* Free RB Trees and hash table created in AreaSetDefaultValues */
        if (pArea->pNbrTbl != NULL)
        {
            RBTreeDelete (pArea->pNbrTbl);
        }
        if (pArea->pLsaHashTable != NULL)
        {
            TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
        }
        if (pArea->pSummaryLsaRoot != NULL)
        {
            RBTreeDelete (pArea->pSummaryLsaRoot);
        }

        AREA_FREE (pArea);
        CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
        return NULL;
    }

    pArea->pOspfCxt = pOspfCxt;

    if ((AreaAddToAreasLst (pArea)) == OSPF_FAILURE)
    {
        OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                   pOspfCxt->u4OspfCxtId,
                   "Failed to add AreaId %x into OSPF context list\n",
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAreaId));
        AreaAsExtRtAggrTrieDelete (pArea);

        /* Free RB Tree and hash table created in AreaSetDefaultValues */
        if (pArea->pNbrTbl != NULL)
        {
            RBTreeDelete (pArea->pNbrTbl);
        }
        if (pArea->pLsaHashTable != NULL)
        {
            TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
        }
        if (pArea->pSummaryLsaRoot != NULL)
        {
            RBTreeDelete (pArea->pSummaryLsaRoot);
        }

        TMO_HASH_Delete_Table (pArea->pSpf, NULL);
        AREA_FREE (pArea);
        return NULL;
    }

    pArea->areaStatus = INVALID;

    OSPF_TRC1 (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId, "AreaId %x Created\n",
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAreaId));
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: AreaCreate\n");

    return pArea;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaCreateBackBoneArea                                 */
/*                                                                           */
/* Description  : Creates an Backbone area.                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the Backbone AREA created on success            */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tArea       *
AreaCreateBackBoneAreaInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: AreaCreateBackBoneArea\n");

    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
              "BB Area Being Created\n");

    if (AREA_ALLOC (pArea) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "BB Area Alloc Failed\n");
        CLI_SET_ERR (CLI_OSPF_AREA_ALLOC_FAILED);
        return NULL;
    }
    AreaSetDefaultValues (pArea);

    /* Trie Created for storing External Route Aggregation entries. */
    if (AreaAsExtRtAggrTrieCreate (pArea) == OSPF_FAILURE)
    {
        OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                   "AreaId %x AS External Route Trie Creation Failure\n",
                   OSPF_CRU_BMC_DWFROMPDU (gBackboneAreaId));

        /* Free RB Trees and hash tables created in AreaSetDefaultValues */
        if (pArea->pNbrTbl != NULL)
        {
            RBTreeDelete (pArea->pNbrTbl);
        }
        if (pArea->pLsaHashTable != NULL)
        {
            TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
        }
        if (pArea->pSummaryLsaRoot != NULL)
        {
            RBTreeDelete (pArea->pSummaryLsaRoot);
        }

        AREA_FREE (pArea);
        return NULL;
    }
    IP_ADDR_COPY (pArea->areaId, gBackboneAreaId);

    if ((pArea->pSpf =
         TMO_HASH_Create_Table (SPF_HASH_TABLE_SIZE, NULL, FALSE)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "SPF Tree Hash Table Failed for backbone area %x\n");
        AreaAsExtRtAggrTrieDelete (pArea);

        /* Free RB Trees and hash table created in AreaSetDefaultValues */
        if (pArea->pNbrTbl != NULL)
        {
            RBTreeDelete (pArea->pNbrTbl);
        }
        if (pArea->pLsaHashTable != NULL)
        {
            TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
        }
        if (pArea->pSummaryLsaRoot != NULL)
        {
            RBTreeDelete (pArea->pSummaryLsaRoot);
        }

        AREA_FREE (pArea);
        return NULL;
    }

    pArea->pOspfCxt = pOspfCxt;

    pArea->areaStatus = ACTIVE;

    if ((AreaAddToAreasLst (pArea)) == OSPF_FAILURE)
    {
        OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                   "Failed to add backbone area AreaId %x to context list\n",
                   OSPF_CRU_BMC_DWFROMPDU (gBackboneAreaId));

        AreaAsExtRtAggrTrieDelete (pArea);

        /* Free RB Tree and hash table created in AreaSetDefaultValues */
        if (pArea->pNbrTbl != NULL)
        {
            RBTreeDelete (pArea->pNbrTbl);
        }
        if (pArea->pLsaHashTable != NULL)
        {
            TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
        }
        if (pArea->pSummaryLsaRoot != NULL)
        {
            RBTreeDelete (pArea->pSummaryLsaRoot);
        }

        TMO_HASH_Delete_Table (pArea->pSpf, NULL);
        AREA_FREE (pArea);
        return NULL;
    }

    OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId, "BB Area Created\n");
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: AreaCreateBackBoneArea\n");
    return pArea;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : AreaParamChange                                         */
/*                                                                           */
/* Description     : This routine is invoked to start the timer for rtr lsa  */
/*                   generation when any area param changes                  */
/*                                                                           */
/* Input           : pArea         :  pointer to area.                       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
AreaParamChange (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tNeighbor          *pNbr = NULL;

    if ((pArea->pOspfCxt->u1HelperStatus == OSPF_GR_HELPING) &&
        (pArea->pOspfCxt->u1StrictLsaCheck == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pArea->pOspfCxt->sortNbrLst), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_SORT_LST (pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                GrExitHelper (OSPF_HELPER_TOP_CHG, pNbr);
            }
        }
    }
    /* concerned lsa is to be regenerated */
    if (LsuSearchDatabase (NETWORK_SUM_LSA,
                           &gDefaultDest, &(pArea->pOspfCxt->rtrId),
                           (UINT1 *) NULL, (UINT1 *) pArea) != NULL)
    {
        OlsSignalLsaRegenInCxt (pArea->pOspfCxt,
                                SIG_DEFAULT_SUMMARY, (UINT1 *) pArea);
    }

    if (pArea->u1SummaryFunctionality == SEND_AREA_SUMMARY)
    {
        if ((pLsaInfo = LsuSearchDatabase (NSSA_LSA,
                                           &gDefaultDest,
                                           &(pArea->pOspfCxt->rtrId),
                                           (UINT1 *) NULL,
                                           (UINT1 *) pArea)) != NULL)
        {
            OlsStartTimerForLsaRegen (pLsaInfo);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaDelete                                                */
/*                                                                           */
/* Description  : Deletes an AREA if no interfaces are attached to the AREA. */
/*                                                                           */
/* Input        : pArea     :    pointer to the AREA to be deleted          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
AreaDelete (tArea * pArea)
{
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pArea->pOspfCxt->u4OspfCxtId;
    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: AreaDelete\n");

    OSPF_TRC1 (CONTROL_PLANE_TRC, u4OspfCxtId,
               "AreaID %x Being Deleted\n",
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));

    if (TMO_SLL_Count (&(pArea->ifsInArea)) != 0)
    {

        OSPF_TRC1 (CONTROL_PLANE_TRC, u4OspfCxtId,
                   "Ifaces Attached to AreaID %x. Cannot Be Deleted\n",
                   OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));

        gu4IfacesDelFail++;
        return OSPF_FAILURE;
    }

    if (pArea->u4AreaType == NSSA_AREA)
        pArea->pOspfCxt->u4NssaAreaCount--;

    LsuFlushSelfOrgAreaLsaDatabase (pArea);
    LsuDeleteAreaLsaDatabase (pArea);
    if (pArea->pNbrTbl != NULL)
    {
        RBTreeDestroy (pArea->pNbrTbl, NULL, 0);
    }
    TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
    TMO_SLL_Delete (&(pArea->pOspfCxt->areasLst), &(pArea->nextArea));

    /* This is done while deleting the back_bone area during 
     * shut down operation.
     */
    if (UtilIpAddrComp (pArea->areaId, gBackboneAreaId) == OSPF_EQUAL)
    {
        pArea->pOspfCxt->pBackbone = NULL;
    }

    RtcClearSpfTree (pArea->pSpf);

    if (pArea->pSummaryLsaRoot != NULL)
    {
        RBTreeDelete (pArea->pSummaryLsaRoot);
    }
    TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);

    /* Deleting External Route Aggregation Trie */
    AreaAsExtRtAggrTrieDelete (pArea);

    if ((pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART) &&
        (pArea->pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        GrExitGracefultRestart (pArea->pOspfCxt, OSPF_RESTART_TOP_CHG);
    }
    OSPF_TRC1 (CONTROL_PLANE_TRC, u4OspfCxtId,
               "AreaID %x.Deleted\n", OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));
    AREA_FREE (pArea);
    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: AreaDelete\n");

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaDeleteAll                                            */
/*                                                                           */
/* description  : this routine deletes all the areas.                        */
/*                called only once when the protocol is brought down.        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
AreaDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;

    while ((pArea = (tArea *) TMO_SLL_First (&(pOspfCxt->areasLst))) != NULL)
    {
        AreaDelete (pArea);
    }
    /* To delete the back_bone area if the router is internal router */
    if (pOspfCxt->pBackbone != NULL)
    {
        AreaDelete (pOspfCxt->pBackbone);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaSetDefaultValues                                    */
/*                                                                           */
/* Description  : Initializes all the initial state parameters of specified  */
/*                AREA with default values.                                  */
/*                                                                           */
/* Input        : pArea      :    pointer to the AREA to be initialized     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
AreaSetDefaultValues (tArea * pArea)
{

    UINT1               u1Index;
    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: AreaSetDefaultValues \n");

    OS_MEM_SET ((UINT1 *) pArea, 0, sizeof (tArea));

    TMO_SLL_Init_Node (&(pArea->nextArea));
    TMO_SLL_Init (&(pArea->ifsInArea));
    TMO_SLL_Init (&(pArea->rtrLsaLst));
    TMO_SLL_Init (&(pArea->networkLsaLst));
    pArea->pSummaryLsaRoot = RBTreeCreateEmbedded (OSPF_OFFSET (tLsaInfo,
                                                                RbNode),
                                                   RbCompareLsa);
    TMO_SLL_Init (&(pArea->Type10OpqLSALst));

    pArea->pNbrTbl = RBTreeCreateEmbedded (OSPF_OFFSET (tNeighbor, RbNode),
                                           RbCompareNbr);
    pArea->pLsaHashTable = TMO_HASH_Create_Table (LSA_HASH_TABLE_SIZE, NULL,
                                                  FALSE);

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        pArea->aAddrRange[u1Index].areaAggStatus = INVALID;
        pArea->aAddrRange[u1Index].u1Active = OSPF_FALSE;
        pArea->aAddrRange[u1Index].u1CalActive = OSPF_FALSE;
    }

    pArea->u4AreaType = NORMAL_AREA;
    pArea->areaOptions = E_BIT_MASK;
    pArea->u1SummaryFunctionality = NO_AREA_SUMMARY;

    for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
    {
        pArea->aStubDefaultCost[u1Index].rowStatus = INVALID;
        pArea->aStubDefaultCost[u1Index].u4Value = LS_INFINITY_24BIT;
        pArea->aStubDefaultCost[u1Index].u4MetricType = OSPF_METRIC;
    }

    pArea->bTransitCapability = OSPF_FALSE;
    pArea->bPreviousTransitCapability = OSPF_FALSE;
    TMO_SLL_Init (&(pArea->nssaLSALst));
    pArea->u1NssaTrnsltrState = TRNSLTR_STATE_DISABLED;
    pArea->u4DfInfOriginate = NO_DEFAULT_INFO_ORIGINATE;
    pArea->u1NssaTrnsltrRole = TRNSLTR_ROLE_CANDIDATE;
    pArea->u4NssaTrnsltrStbltyInterval = NSSA_TRNSLTR_STBLTY_INTRVL;
    pArea->u4NssaTrnsltrEvents = 0;
    pArea->bNewlyAttached = OSPF_FALSE;

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: AreaSetDefaultValues\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaAddToAreasLst                                      */
/*                                                                           */
/* Description  : Adds the specified area to the list of areas maintained in */
/*                the OS Router structure. The specified area is inserted    */
/*                in such a way as to maintain the sorted order.             */
/*                                                                           */
/* Input        : pArea      :    pointer to the AREA to be initialized     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
AreaAddToAreasLst (tArea * pArea)
{

    tArea              *pLstArea;
    tArea              *pPrevArea;

    pPrevArea = NULL;
    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "ENTRY: AreaAddToAreasLst\n");

    TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pLstArea, tArea *)
    {
        if (UtilIpAddrComp (pArea->areaId, pLstArea->areaId) == OSPF_EQUAL)
        {
            return OSPF_FAILURE;
        }
        if (UtilIpAddrComp (pArea->areaId, pLstArea->areaId) == OSPF_GREATER)
        {
            break;
        }
        pPrevArea = pLstArea;
    }
    TMO_SLL_Insert (&(pArea->pOspfCxt->areasLst), &pPrevArea->nextArea,
                    &pArea->nextArea);

    pArea->u4ActIntCount = 0;
    pArea->u4FullNbrCount = 0;
    pArea->u4FullVirtNbrCount = 0;

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaAddToAreasLst \n");

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaDeleteAddrRange                                     */
/*                                                                           */
/* Description  : Invalidates the address range entry & packs the array.     */
/*                                                                           */
/* Input        : pArea          :     pointer to the AREA                  */
/*                u1Index        :     Index of the address range entry     */
/*                                      to be invalidated.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
AreaDeleteAddrRange (tArea * pArea, UINT1 u1Index)
{
    tIPADDR             pIpAddr;
    tIPADDR             pIpAddrMask;
    tAddrRange         *pAddrRange;
    tArea              *pLstArea;
    tRtEntry           *pRtEntry;
    tLsaInfo           *pLsaInfo;
    UINT1               u1PassedIndex;
    UINT1               u1FlushFlag = OSPF_FALSE;
    tExtRoute          *pExtRoute = NULL;
    UINT4               u4Count = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaDeleteAddrRange\n");
    OSPF_TRC2 (OSPF_CONFIGURATION_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "AREA:%x ,INDEX:%d \n", pArea->areaId, u1Index);
    if ((pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART) &&
        (pArea->pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        GrExitGracefultRestart (pArea->pOspfCxt, OSPF_RESTART_TOP_CHG);
    }
    IP_ADDR_COPY (pIpAddr, pArea->aAddrRange[u1Index].ipAddr);
    IP_ADDR_COPY (pIpAddrMask, pArea->aAddrRange[u1Index].ipAddrMask);

    /* invalidate specified entry */

    pArea->aAddrRange[u1Index].areaAggStatus = INVALID;

    u1PassedIndex = u1Index;
    /* entries in the array are sorted on ip addr                     */
    /* when an entry is deleted pack the valid entries at the top of  */
    /* the array                                                      */

    /* Incremented to check the next element onwards */

    u1Index++;

    while (u1Index < MAX_ADDR_RANGES_PER_AREA)
    {

        if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
            break;

        OS_MEM_CPY (&(pArea->aAddrRange[u1Index - 1]),
                    &(pArea->aAddrRange[u1Index]), sizeof (tAddrRange));
        pArea->aAddrRange[u1Index - 1].u1AddrRangeIndex = (UINT1) (u1Index - 1);
        pArea->aAddrRange[u1Index].areaAggStatus = INVALID;
        u1Index++;
    }

    if (pArea->pOspfCxt->bAreaBdrRtr != OSPF_TRUE)
    {
        return OSPF_SUCCESS;
    }

    if (pArea->aAddrRange[u1PassedIndex].u1LsdbType == NSSA_EXTERNAL_LINK)
    {
        NssaProcAggRangeInCxt (pArea->pOspfCxt);
        pExtRoute = GetFindExtRouteInCxt (pArea->pOspfCxt,
                                          &(pArea->aAddrRange[u1PassedIndex].
                                            ipAddr),
                                          &(pArea->aAddrRange[u1PassedIndex].
                                            ipAddrMask));
        if (pExtRoute != NULL)
            RagAddRouteInAggAddrRangeInCxt (pArea->pOspfCxt, pExtRoute);
        return OSPF_SUCCESS;
    }

    /* Scan the areas table and delete the cond. network summary lsa */

    TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pLstArea, tArea *)
    {

        if ((UtilIpAddrComp (pArea->areaId,
                             pLstArea->areaId) != OSPF_EQUAL) &&
            ((pLstArea->u4AreaType == NORMAL_AREA) ||
             ((pLstArea->u4AreaType != NORMAL_AREA) &&
              (pLstArea->u1SummaryFunctionality == SEND_AREA_SUMMARY))))
        {
            if ((pLsaInfo = LsuDatabaseLookUp (NETWORK_SUM_LSA,
                                               &pIpAddr, &pIpAddrMask,
                                               &(pArea->pOspfCxt->rtrId),
                                               (UINT1 *) NULL,
                                               (UINT1 *) pLstArea)) != NULL)

            {

                if (RtcFindRtEntry (pArea->pOspfCxt->pOspfRt, &pIpAddr,
                                    &pIpAddrMask, DEST_NETWORK) == NULL)
                {
                    u1FlushFlag = OSPF_TRUE;
                }
                else
                {
                    /*Scanning the other Area Range */
                    for (u4Count = 0; u4Count < MAX_ADDR_RANGES_PER_AREA;
                         u4Count++)
                    {
                        pAddrRange = &(pArea->aAddrRange[u4Count]);
                        if (pAddrRange->areaAggStatus == INVALID)
                        {
                            break;
                        }
                        if (pAddrRange->u1AdvertiseStatus ==
                            DO_NOT_ADV_MATCHING)
                        {
                            continue;
                        }
                        if ((pIpAddr != pAddrRange->ipAddr) &&
                            (UtilIpAddrMaskComp (pIpAddr,
                                                 pAddrRange->ipAddr,
                                                 pAddrRange->ipAddrMask) ==
                             OSPF_EQUAL))
                        {
                            u1FlushFlag = OSPF_TRUE;
                            break;
                        }
                    }
                }
                if (u1FlushFlag == OSPF_TRUE)
                {
                    AgdFlushOut (pLsaInfo);
                }
                else
                {

                    /* This scenario occurs if the configured address range is
                     * equal to a destination network in the routing table and
                     * this destination network does not belong to any other
                     * address range. Since the link state id, advertising
                     * router and LSA type is same for both the LSA, the
                     * pointer to summary param is set to NULL.
                     * This LSA will be re-orginated as a normal network LSA
                     * with the next sequence number */
                    pLsaInfo->pLsaDesc->pAssoPrimitive = NULL;

                }
            }
        }

    }

    /* Scan the routing table */

    TMO_SLL_Scan (&(pArea->pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        if (!(IS_DEST_NETWORK (pRtEntry)))
            continue;

        if (UtilIpAddrMaskComp (pRtEntry->destId, pIpAddr,
                                pIpAddrMask) == OSPF_EQUAL)
        {

            TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pLstArea, tArea *)
            {

                if ((UtilIpAddrComp (pArea->areaId,
                                     pLstArea->areaId) != OSPF_EQUAL) &&
                    ((pLstArea->u4AreaType == NORMAL_AREA) ||
                     ((pLstArea->u4AreaType != NORMAL_AREA) &&
                      (pLstArea->u1SummaryFunctionality == SEND_AREA_SUMMARY))))
                {
                    OlsGenerateSummary (pLstArea, pRtEntry);
                }
            }                    /* TMO_SLL_Scan */
        }
    }                            /* end for TMO_SLL_Scan of pRtEntry */
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaDeleteAddrRange \n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaSendSummaryStatusChange                                */
/*                                                                           */
/* Description  : Send Summary into a stub Area if Area summary is set else  */
/*                 Delete the summary generated into stub Area         .     */
/*                                                                           */
/* Input        : pArea     :    pointer to the AREA                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
AreaSendSummaryStatusChange (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    UINT1               u1Index;
    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaSendSummaryStatusChange\n");

    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* Either GR shutdown or restart is in progress 
         * LSA's can reorginated/Flushed only on exiting GR */
        return OSPF_SUCCESS;
    }

    if (IS_STUB_AREA (pArea))
    {
        if (IS_SEND_AREA_SUMMARY (pArea))
        {
            OlsGenerateSummaryToArea (pArea);
        }

        else
        {
            AreaDeleteNetSummaryLsas (pArea);
        }
    }
    else if (pArea->u4AreaType == NSSA_AREA)
    {
        if (IS_SEND_AREA_SUMMARY (pArea))
        {
            pLsaInfo = LsuSearchDatabase (NETWORK_SUM_LSA,
                                          &(gDefaultDest),
                                          &(pArea->pOspfCxt->rtrId), NULL,
                                          (UINT1 *) pArea);
            if (pLsaInfo != NULL)
                AgdFlushOut (pLsaInfo);

            for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
            {
                if (pArea->aStubDefaultCost[u1Index].u4MetricType
                    == OSPF_METRIC)
                {
                    pArea->aStubDefaultCost[u1Index].u4MetricType
                        = TYPE1EXT_METRIC;
                }
            }
            OlsGenerateSummaryToArea (pArea);
            if (pArea->u4DfInfOriginate == DEFAULT_INFO_ORIGINATE)
            {
                OlsGenerateNssaDfLsa (pArea, DEFAULT_NSSA_LSA, &gDefaultDest);
            }
        }
        else
        {
            if (pArea->u4DfInfOriginate != DEFAULT_INFO_ORIGINATE)
            {
                AreaDeleteNetSummaryLsas (pArea);
                pLsaInfo = LsuSearchDatabase (NSSA_LSA,
                                              &(gDefaultDest),
                                              &(pArea->pOspfCxt->rtrId), NULL,
                                              (UINT1 *) pArea);
                if (pLsaInfo != NULL)
                    AgdFlushOut (pLsaInfo);

                for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
                {
                    if (pArea->aStubDefaultCost[u1Index].u4MetricType
                        != OSPF_METRIC)
                    {
                        pArea->aStubDefaultCost[u1Index].u4MetricType =
                            OSPF_METRIC;
                    }
                }
                OlsSignalLsaRegenInCxt (pArea->pOspfCxt,
                                        SIG_DEFAULT_SUMMARY, (UINT1 *) pArea);
            }
            else
            {
                for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
                {
                    if (pArea->aStubDefaultCost[u1Index].u4MetricType
                        == OSPF_METRIC)
                    {
                        pArea->aStubDefaultCost[u1Index].u4MetricType
                            = TYPE1EXT_METRIC;
                    }
                }
                OlsGenerateNssaDfLsa (pArea, DEFAULT_NSSA_LSA, &gDefaultDest);
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaSendSummaryStatusChange \n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaDeleteNetSummaryLsas                                   */
/*                                                                           */
/* Description  : Delete the network summary LSA's in the area,except default*/
/*                Summary LSA's, returns OSPF_TRUE, if default LSA present.  */
/*                                                                           */
/* Input        : pArea     :    pointer to the AREA                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1DefSumFlag                                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
AreaDeleteNetSummaryLsas (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    tLsaInfo           *pNextLsaInfo;
    UINT1               u1DefSumFlag = OSPF_FALSE;
    tLsaInfo           *pTempLsaInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaDeleteSummaryLsas \n");
    pNextLsaInfo = (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
    while (pNextLsaInfo != NULL)
    {
        pLsaInfo = pNextLsaInfo;
        pNextLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot,
                                        (tRBElem *) pNextLsaInfo, NULL);

        if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, pArea->pOspfCxt->rtrId) ==
            OSPF_EQUAL)
        {
            if (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId,
                                gDefaultDest) == OSPF_EQUAL)
            {
                u1DefSumFlag = OSPF_TRUE;
                continue;
            }
            else
            {
                AgdFlushOut (pLsaInfo);
            }
        }
        else if (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId,
                                 gDefaultDest) == OSPF_EQUAL)
        {
            continue;
        }
        else
        {
            pTempLsaInfo = pLsaInfo;
        }

        if (pTempLsaInfo != NULL)
        {
            LsuDeleteLsaFromDatabase (pTempLsaInfo, OSPF_TRUE);
            pTempLsaInfo = NULL;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaDeleteSummaryLsas \n");

    return u1DefSumFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaCreateAddrRange                                        */
/*                                                                           */
/* Description  : Create an IP address Range and add to the address          */
/*                range list.                                                */
/*                                                                           */
/* Input        : pArea        :   pointer to the AREA.                      */
/*                pIpAddr      :   IP address of the Address range.          */
/*                pIpAddrMask  :   IP address mask of the Address range.     */
/*                bStatus      :   Status of the Address range               */
/*                                                                           */
/* Output       : pReturnIndex                                              */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                              */
/*                OSPF_FAILURE                                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
AreaCreateAddrRange (tArea * pArea,
                     tIPADDR * pIpAddr, tIPADDR * pIpAddrMask, UINT1 u1LsdbType,
                     tROWSTATUS bStatus, INT1 *pReturnIndex)
{

    UINT1               u1Index;
    tAddrRange          tempAddrRange;
    tAddrRange          newAddrRange;
    tTRUTHVALUE         addrRangeSet = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaCreateAddrRange\n");
    OSPF_TRC4 (OSPF_CONFIGURATION_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "AREA:%x, IP_ADDR:%x, MASK:%x, STATUS:%d \n",
               pArea->areaId, *pIpAddr, *pIpAddrMask, bStatus);

    if (pArea->aAddrRange[MAX_ADDR_RANGES_PER_AREA - 1].areaAggStatus
        != INVALID)
    {
        return OSPF_FAILURE;
    }
    MEMSET (&tempAddrRange, 0, sizeof (tAddrRange));
    MEMSET (&newAddrRange, 0, sizeof (tAddrRange));

    newAddrRange.areaAggStatus = bStatus;
    newAddrRange.u1LsdbType = u1LsdbType;
    IP_ADDR_COPY (newAddrRange.ipAddr, *pIpAddr);
    IP_ADDR_COPY (newAddrRange.ipAddrMask, *pIpAddrMask);
    newAddrRange.u1Active = OSPF_FALSE;
    newAddrRange.u1AdvertiseStatus = ADVERTISE_MATCHING;
    newAddrRange.u4LsaCount = 0;
    newAddrRange.pLsaInfo = NULL;
    newAddrRange.u4RngUpdt = OSPF_FALSE;
    newAddrRange.aggFlag = OSPF_FALSE;

    for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
    {
        newAddrRange.aMetric[u1Index].bStatus = INVALID;
        newAddrRange.aMetric[u1Index].u4Value = LS_INFINITY_24BIT;
        if (newAddrRange.u1LsdbType == NSSA_EXTERNAL_LINK)
            newAddrRange.aMetric[u1Index].u4MetricType = TYPE_1_METRIC;
    }

    /* Insert entry in the array at the appropriate position */
    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        if (addrRangeSet == OSPF_TRUE)
        {
            break;
        }

        if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
        {
            /* Free entry available */
            OS_MEM_CPY (&(pArea->aAddrRange[u1Index]), &(newAddrRange),
                        sizeof (tAddrRange));
            pArea->aAddrRange[u1Index].u1AddrRangeIndex = u1Index;
            *pReturnIndex = (INT1) u1Index;
            addrRangeSet = OSPF_TRUE;
        }
        else if ((UtilIpAddrComp (*pIpAddr,
                                  pArea->aAddrRange[u1Index].ipAddr)
                  == OSPF_GREATER) ||
                 ((UtilIpAddrComp (*pIpAddr,
                                   pArea->aAddrRange[u1Index].ipAddr)
                   == OSPF_EQUAL) &&
                  ((UtilIpAddrComp (*pIpAddrMask,
                                    pArea->aAddrRange[u1Index].ipAddrMask)
                    == OSPF_GREATER) ||
                   (u1LsdbType > pArea->aAddrRange[u1Index].u1LsdbType))))

        {
            /* New entry has to be inserted here */
            while (u1Index < MAX_ADDR_RANGES_PER_AREA)
            {
                /* Move current to temp */
                OS_MEM_CPY (&(tempAddrRange),
                            &(pArea->aAddrRange[u1Index]), sizeof (tAddrRange));

                /* Move new entry to current entry */
                OS_MEM_CPY (&(pArea->aAddrRange[u1Index]),
                            &(newAddrRange), sizeof (tAddrRange));
                pArea->aAddrRange[u1Index].u1AddrRangeIndex = u1Index;

                /* Move temp to new entry */
                OS_MEM_CPY (&(newAddrRange), &(tempAddrRange),
                            sizeof (tAddrRange));

                u1Index++;
                if (u1Index == MAX_ADDR_RANGES_PER_AREA)
                {
                    break;
                }
                if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
                {
                    OS_MEM_CPY (&(pArea->aAddrRange[u1Index]),
                                &(newAddrRange), sizeof (tAddrRange));
                    pArea->aAddrRange[u1Index].u1AddrRangeIndex = u1Index;
                    addrRangeSet = OSPF_TRUE;
                    *pReturnIndex = (INT1) u1Index;
                    break;
                }
            }
        }
    }
    if (addrRangeSet == OSPF_TRUE)
    {
        OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: AreaCreateAddrRange\n");
        return OSPF_SUCCESS;
    }
    else
    {
        OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: AreaCreateAddrRange\n");
        return OSPF_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaActiveAddrRange                                        */
/*                                                                           */
/* Description  : Activate the configured address range and                  */
/*                do the aggregation of summary Lsas.                        */
/*                                                                           */
/* Input        : pArea        :  Pointer to the AREA.                       */
/*                u1Index      :  Address Range Index.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
AreaActiveAddrRange (tArea * pArea, UINT1 u1Index)
{

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaActiveAddrRange\n");

    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* Either GR shutdown or restart is in progress 
         * LSA's can reorginated/Flushed only on exiting GR */
        return OSPF_SUCCESS;
    }
    if (pArea->pOspfCxt->bAreaBdrRtr != OSPF_TRUE)
    {
        return OSPF_SUCCESS;
    }

    if (!(u1Index < MAX_ADDR_RANGES_PER_AREA))
    {
        return OSPF_FAILURE;
    }
    if (pArea->aAddrRange[u1Index].u1LsdbType == NSSA_EXTERNAL_LINK)
    {
        NssaProcAggRangeInCxt (pArea->pOspfCxt);
        return OSPF_SUCCESS;
    }

    if (pArea->aAddrRange[u1Index].u1AdvertiseStatus == DO_NOT_ADV_MATCHING)
    {
        AreaFlushLsaFallingInAddrRange (pArea, u1Index);
        return OSPF_SUCCESS;
    }

    /* Scan the areas table and transmit the cond. network summary lsa
     * to all the areas 
     */

    AreaSetaggFlag (pArea);

    AreaGenerateAggLsa (pArea);
    AreaFlushLsaFallingInAddrRange (pArea, u1Index);

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaActiveAddrRange\n");
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : AreaSetAggrEffect                                          */
/*                                                                           */
/* Description  : Set the aggregation Effect of the address range and do     */
/*                the necessary operations  as specified in                  */
/*                RFC - 2378 APPENDIX c-2.                                   */
/*                                                                           */
/* Input        : pArea        :  pointer to the AREA.                       */
/*                u1Index      :  Address Range Index.                       */
/*                u1AggrEffect :  Aggregate Effect value.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
AreaSetAggrEffect (tArea * pArea, UINT1 u1Index, UINT1 u1AggrEffect)
{
    tArea              *pLstArea;
    tIPADDR            *pIpAddr;
    tIPADDR            *pIpAddrMask;
    tLsaInfo           *pLsaInfo;
    tExtRoute          *pExtRoute = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaSetAggrEffect\n");

    pArea->aAddrRange[u1Index].u1AdvertiseStatus = u1AggrEffect;

    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* Either GR shutdown or restart is in progress 
         * LSA's can reorginated/Flushed only on exiting GR */
        return OSPF_SUCCESS;
    }

    if ((pArea->pOspfCxt->bAreaBdrRtr != OSPF_TRUE) ||
        pArea->aAddrRange[u1Index].areaAggStatus != ACTIVE)
    {
        return OSPF_SUCCESS;
    }

    if (pArea->aAddrRange[u1Index].u1LsdbType == NSSA_EXTERNAL_LINK)
    {
        NssaProcAggRangeInCxt (pArea->pOspfCxt);
        if (pArea->aAddrRange[u1Index].u1AdvertiseStatus
            == DO_NOT_ADVERTISE_MATCHING)
        {

            pExtRoute = GetFindExtRouteInCxt (pArea->pOspfCxt,
                                              &(pArea->aAddrRange[u1Index].
                                                ipAddr),
                                              &(pArea->aAddrRange[u1Index].
                                                ipAddrMask));
            if (pExtRoute != NULL)
                RagAddRouteInAggAddrRangeInCxt (pArea->pOspfCxt, pExtRoute);
        }
        return OSPF_SUCCESS;
    }

    pIpAddr = &(pArea->aAddrRange[u1Index].ipAddr);
    pIpAddrMask = &(pArea->aAddrRange[u1Index].ipAddrMask);

    if (u1AggrEffect == DO_NOT_ADV_MATCHING)
    {
        /* Flushing The Condensed summary Lsa if it was advertised. */
        TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pLstArea, tArea *)
        {
            if (UtilIpAddrComp (pArea->areaId, pLstArea->areaId) != OSPF_EQUAL)
            {
                if ((pLsaInfo = LsuDatabaseLookUp (NETWORK_SUM_LSA,
                                                   pIpAddr, pIpAddrMask,
                                                   &(pArea->pOspfCxt->rtrId),
                                                   (UINT1 *) NULL,
                                                   (UINT1 *) pLstArea)) != NULL)

                {
                    AgdFlushOut (pLsaInfo);
                }
            }
        }
    }
    else
    {
        AreaSetaggFlag (pArea);

        AreaGenerateAggLsa (pArea);

    }
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaSetaggFlag\n");
    return OSPF_SUCCESS;

}

/****************************************************************/
/*                                  */
/* Function      : AreaChangeAreaType                */
/*                                      */
/* Description    : This routine habdle conversion between     */
/*           different area types                 */
/*                                  */
/* Input           : pArea - Pointer to the AREA.          */
/*              : i4AreaType - New Area Type              */
/*                                           */
/* Output       : None                          */
/*                                     */
/* Returns      : Void                          */
/*                                    */
/****************************************************************/
PUBLIC INT4
AreaChangeAreaType (tArea * pArea, INT4 i4AreaType)
{
/* Pointers Initialized to NULL*/

    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Index;
    tArea              *pScanArea = NULL;

    if (pArea == NULL)
        return OSPF_FAILURE;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "AreaChangeAreaType\n");

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pLstNode);
        OspfIfDown (pInterface);
    }
    AreaReInitialize (pArea);

    if (((pArea->u4AreaType == NORMAL_AREA) || (pArea->u4AreaType == NSSA_AREA)
         || (pArea->u4AreaType == STUB_AREA)) && ((i4AreaType == NSSA_AREA)
                                                  || (i4AreaType == STUB_AREA)))
    {
        if (gu1DefCostCiscoCompat == OSPF_FALSE)
        {
            if (((pInterface = IfGetMinCostIfInArea (DECODE_TOS (TOS_0),
                                                     pArea))) == NULL)
            {
                pArea->aStubDefaultCost[DECODE_TOS (TOS_0)].u4Value =
                    LS_INFINITY_24BIT;
                pArea->aStubDefaultCost[DECODE_TOS (TOS_0)].rowStatus =
                    NOT_READY;
            }
            else
            {
                pArea->aStubDefaultCost[DECODE_TOS (TOS_0)].u4Value =
                    pInterface->aIfOpCost[DECODE_TOS (TOS_0)].u4Value;
                pArea->aStubDefaultCost[DECODE_TOS (TOS_0)].rowStatus = ACTIVE;
            }
        }
        else
        {
            pArea->aStubDefaultCost[DECODE_TOS (TOS_0)].u4Value =
                STUB_NSSA_DEFAULT_COST;
            pArea->aStubDefaultCost[DECODE_TOS (TOS_0)].rowStatus = ACTIVE;

        }
    }
    if ((i4AreaType != NSSA_AREA) && (pArea->u4AreaType == NSSA_AREA))

        pArea->pOspfCxt->u4NssaAreaCount--;
    else if (i4AreaType == NSSA_AREA)
        pArea->pOspfCxt->u4NssaAreaCount++;

    pArea->u4AreaType = i4AreaType;

    for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            pArea->aStubDefaultCost[u1Index].u4MetricType = OSPF_METRIC;
        }
        else
        {
            if (pArea->u1SummaryFunctionality == NO_AREA_SUMMARY)
            {
                pArea->aStubDefaultCost[u1Index].u4MetricType = OSPF_METRIC;
            }
            else
            {
                pArea->aStubDefaultCost[u1Index].u4MetricType = TYPE1EXT_METRIC;
            }
        }
    }

    if (pArea->u4AreaType == NSSA_AREA)
        pArea->areaOptions = N_BIT_MASK;
    else if (pArea->u4AreaType == NORMAL_AREA)
        pArea->areaOptions = E_BIT_MASK;
    else
        pArea->areaOptions = RAG_NO_CHNG;

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pLstNode);
        OspfIfUp (pInterface);
    }
    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* LSA's cannot be generated in graceful restart mode */
        return OSPF_SUCCESS;
    }
    TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pScanArea, tArea *)
    {
        if (TMO_SLL_Count (&(pScanArea->ifsInArea)) != 0)
        {
            OlsGenerateLsa (pScanArea, ROUTER_LSA, &pArea->pOspfCxt->rtrId,
                            NULL);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "AreaChangeAreaType\n");
    return OSPF_SUCCESS;
}

/****************************************************************/
/*                                                              */
/* Function      : AreaReInitialize                   */
/*                                       */
/* Description    : This routine reinitializes the area         */
/*                                   */
/* Input           : pArea - Pointer to the AREA.          */
/*                                  */
/* Output       : None                          */
/*                                     */
/* Returns      : Void                         */
/*                                     */
/****************************************************************/
PUBLIC VOID
AreaReInitialize (tArea * pArea)
{
    tLsaInfo           *pLsaInfo = NULL;
    UINT4               u4HashKey;
    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "AreaChangeAreaType\n");

    TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashKey)
    {
        while ((pLsaInfo =
                (tLsaInfo *)
                TMO_HASH_Get_First_Bucket_Node (pArea->pLsaHashTable,
                                                u4HashKey)) != NULL)
        {
            LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
        }
    }

    pArea->bTransitCapability = OSPF_FALSE;
    pArea->u4ActIntCount = 0;
    pArea->u4FullNbrCount = 0;
    pArea->u4FullVirtNbrCount = 0;
    pArea->u4XchgOrLoadNbrCount = 0;
    pArea->u4DfInfOriginate = NO_DEFAULT_INFO_ORIGINATE;
    pArea->u1NssaTrnsltrRole = TRNSLTR_ROLE_CANDIDATE;
    pArea->u4NssaTrnsltrStbltyInterval = NSSA_TRNSLTR_STBLTY_INTRVL;
    pArea->u4AreaBdrRtrCount = 0;
    pArea->u4AsBdrRtrCount = 0;
    /* Set these default values also when area is created the first time */
    pArea->u4NssaTrnsltrEvents = 0;
    pArea->bNewlyAttached = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "AreaChangeAreaType\n");
}

/************************************************************************/
/*                                                                      */
/* Function     : AreaSetAddrRangeTag                   */
/*                                            */
/* Description  : Set the aggregation entry tag              */
 /*                                         */
/* Input        : pArea        :  pointer to the AREA.            */
/*                u1Index      :  Address Range Index.          */
/*                                      */
/* Output       : None                                */
/*                                         */
/* Returns     : None                                */
/*                                        */
/************************************************************************/
PUBLIC INT4
AreaSetAddrRangeTag (tArea * pArea, UINT1 u1Index)
{

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: AreaSetAddrRangeTag\n");
    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* Either GR shutdown or restart is in progress 
         * LSA's can reorginated/Flushed only on exiting GR */
        return OSPF_SUCCESS;
    }
    if ((pArea->pOspfCxt->bAreaBdrRtr != OSPF_TRUE) ||
        pArea->aAddrRange[u1Index].areaAggStatus != ACTIVE)
    {
        return OSPF_SUCCESS;
    }

    if (pArea->aAddrRange[u1Index].u1LsdbType == NSSA_EXTERNAL_LINK)
    {
        NssaProcAggRangeInCxt (pArea->pOspfCxt);
        return OSPF_SUCCESS;
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: AreaSetAddrRangeTag\n");
    return OSPF_SUCCESS;
}

/************************************************************************/
/*                                                                      */
/* Function     : AreaFlushLsaFallingInAddrRange                        */
/*                                                                      */
/* Description  : This Function flushes the indvidual Network Entry     */
 /*               if they falls in Configured Address range             */
/*                                                                      */
/* Input        : pArea        :  pointer to the AREA.                  */
/*                u1Index      :  Address Range Index.                  */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns     : None                                                   */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
AreaFlushLsaFallingInAddrRange (tArea * pArea, UINT1 u1Index)
{

    tArea              *pLstArea;
    tRtEntry           *pRtEntry;
    tIPADDR            *pIpAddr;
    tIPADDR            *pIpAddrMask;

    pIpAddr = &(pArea->aAddrRange[u1Index].ipAddr);
    pIpAddrMask = &(pArea->aAddrRange[u1Index].ipAddrMask);

    /* Scan the Routing table */
    TMO_SLL_Scan (&(pArea->pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        if (!(IS_DEST_NETWORK (pRtEntry)))
            continue;

        TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pLstArea, tArea *)
        {

            if ((UtilIpAddrComp (pArea->areaId,
                                 pLstArea->areaId) != OSPF_EQUAL) &&
                (UtilIpAddrMaskComp (pRtEntry->destId,
                                     *pIpAddr, *pIpAddrMask) == OSPF_EQUAL))
            {
                if ((pLstArea->u4AreaType == NORMAL_AREA) ||
                    ((pLstArea->u4AreaType != NORMAL_AREA) &&
                     (pLstArea->u1SummaryFunctionality == SEND_AREA_SUMMARY)))
                {
                    if (IS_AREA_TRANSIT_CAPABLE (pLstArea) &&
                        IS_NULL_IP_ADDR (pArea->areaId))
                    {
                        /* Backbone Network Entry is not summarized in transit
                           area */
                        continue;
                    }
                    /* As per RFC 2328 Section 11.1, An address range is 
                     * considered active if it contains an intra-area route to
                     * networks within the address range. So, inter-area routes
                     * need not be flushed out as a summary route will not be 
                     * generated for them. */
                    if (GET_PATH_TYPE (pRtEntry, TOS_0) != INTRA_AREA)
                    {
                        continue;
                    }
                    RtcFlushOutSummaryLsa (pLstArea, NETWORK_SUM_LSA,
                                           &(pRtEntry->destId),
                                           pRtEntry->u4IpAddrMask);
                }
            }
        }                        /* End TMO_SLL_Scan of pArea */
    }                            /* End TMO_SLL_Scan of pRtEntry */

}

/************************************************************************/
/*                                                                      */
/* Function     : TransAreaFlushLsaFallingInAddrRange                   */
/*                                                                      */
/* Description  : This Function flushes the indvidual Network Entry     */
 /*               if they falls in Configured Address range             */
/*                                                                      */
/* Input        : pArea        :  pointer to the AREA.                  */
/*                u1Index      :  Address Range Index.                  */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns     : None                                                   */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
TransAreaFlushLsaFallingInAddrRange (tArea * pLstArea)
{

    tArea              *pArea;
    tRtEntry           *pRtEntry;
    tIPADDR            *pIpAddr;
    tIPADDR            *pIpAddrMask;
    UINT1               u1Index;

    pArea = pLstArea->pOspfCxt->pBackbone;

    /* Scan the Routing table */
    TMO_SLL_Scan (&(pLstArea->pOspfCxt->pOspfRt->routesList), pRtEntry,
                  tRtEntry *)
    {
        if (!(IS_DEST_NETWORK (pRtEntry)))
            continue;

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {
            pIpAddr = &(pArea->aAddrRange[u1Index].ipAddr);
            pIpAddrMask = &(pArea->aAddrRange[u1Index].ipAddrMask);

            if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
            {
                break;
            }
            if ((UtilIpAddrComp (pArea->areaId,
                                 pLstArea->areaId) != OSPF_EQUAL) &&
                (UtilIpAddrMaskComp (pRtEntry->destId,
                                     *pIpAddr, *pIpAddrMask) == OSPF_EQUAL))
            {
                if ((pLstArea->u4AreaType == NORMAL_AREA) ||
                    ((pLstArea->u4AreaType != NORMAL_AREA) &&
                     (pLstArea->u1SummaryFunctionality == SEND_AREA_SUMMARY)))
                {
                    RtcFlushOutSummaryLsa (pLstArea, NETWORK_SUM_LSA,
                                           &(pRtEntry->destId),
                                           pRtEntry->u4IpAddrMask);
                }
            }
        }
    }                            /* End TMO_SLL_Scan of pRtEntry */

}

PUBLIC VOID
AreaFlushAggLsaInTransitArea (tArea * pLstArea)
{

    tArea              *pArea;
    tRtEntry           *pRtEntry;
    tIPADDR            *pIpAddr;
    tIPADDR            *pIpAddrMask;
    UINT1               u1Index;
    tLsaInfo           *pLsaInfo;

    pArea = pLstArea->pOspfCxt->pBackbone;

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        pIpAddr = &(pArea->aAddrRange[u1Index].ipAddr);
        pIpAddrMask = &(pArea->aAddrRange[u1Index].ipAddrMask);
        if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
        {
            break;
        }

        if ((pLsaInfo = LsuDatabaseLookUp (NETWORK_SUM_LSA,
                                           pIpAddr, pIpAddrMask,
                                           &(pLstArea->pOspfCxt->rtrId),
                                           (UINT1 *) NULL,
                                           (UINT1 *) pLstArea)) != NULL)

        {
            AgdFlushOut (pLsaInfo);
        }
    }

    /* Scan the Routing table */
    TMO_SLL_Scan (&(pLstArea->pOspfCxt->pOspfRt->routesList), pRtEntry,
                  tRtEntry *)
    {
        if (!(IS_DEST_NETWORK (pRtEntry)))
            continue;

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {
            pIpAddr = &(pArea->aAddrRange[u1Index].ipAddr);
            pIpAddrMask = &(pArea->aAddrRange[u1Index].ipAddrMask);
            if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
            {
                break;
            }
            if ((UtilIpAddrComp (pArea->areaId,
                                 pLstArea->areaId) != OSPF_EQUAL) &&
                (UtilIpAddrMaskComp (pRtEntry->destId,
                                     *pIpAddr, *pIpAddrMask) == OSPF_EQUAL))
            {
                if ((pLstArea->u4AreaType == NORMAL_AREA) ||
                    ((pLstArea->u4AreaType != NORMAL_AREA) &&
                     (pLstArea->u1SummaryFunctionality == SEND_AREA_SUMMARY)))
                {
                    OlsGenerateSummary (pLstArea, pRtEntry);
                }
            }
        }
    }                            /* End TMO_SLL_Scan of pRtEntry */

}

/************************************************************************/
/*                                                                      */
/* Function     : AreaSetaggFlag                                       */
/*                                                                      */
/* Description  : This Function Scan the Routing table and if any      */
 /*               route entry falls in Address range then set the flag */
/*                this flag will be used for generation of agg lsa     */
/*                                                                     */
/* Input        : pArea        :  pointer to the AREA.                 */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns     : None                                                  */
/*                                                                     */
/************************************************************************/
PUBLIC VOID
AreaSetaggFlag (tArea * pArea)
{

    tRtEntry           *pRtEntry;
    tAddrRange         *pAddrRange;
    tPath              *pPath;
    UINT1               i1Tos;
    UINT1               u1Index;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* LSA cannot be generated/flushed in standby node */
        return;
    }

    /* Scan Routing Table */
    TMO_SLL_Scan (&(pArea->pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        if ((IS_DEST_NETWORK (pRtEntry))
            && (GET_PATH_TYPE (pRtEntry, TOS_0) == INTRA_AREA))
        {
            for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
            {
                pAddrRange = &(pArea->aAddrRange[u1Index]);
                if (pAddrRange->areaAggStatus == INVALID)
                {
                    break;
                }
                if (pAddrRange->u1AdvertiseStatus == DO_NOT_ADV_MATCHING)
                {
                    continue;
                }
                if (UtilIpAddrMaskComp (pRtEntry->destId,
                                        pAddrRange->ipAddr,
                                        pAddrRange->ipAddrMask) == OSPF_EQUAL)
                {
                    /* update cost for each TOS */
                    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                    {
                        pPath = GET_TOS_PATH (pRtEntry, i1Tos);
                        if ((pPath == NULL) ||
                            (pPath->u1PathType != INTRA_AREA) ||
                            (UtilIpAddrComp (pPath->areaId, pArea->areaId)
                             != OSPF_EQUAL))
                        {
                            continue;
                        }
                        if (pAddrRange->aMetric[i1Tos].bStatus == OSPF_VALID)
                        {
                            if (pArea->pOspfCxt->rfc1583Compatibility ==
                                OSPF_DISABLED)
                            {
                                pAddrRange->aMetric[i1Tos].u4Value
                                    = OSPF_MAX (pPath->u4Cost,
                                                pAddrRange->aMetric[i1Tos].
                                                u4Value);
                            }
                            else
                            {
                                pAddrRange->aMetric[i1Tos].u4Value
                                    = OSPF_MIN (pPath->u4Cost,
                                                pAddrRange->aMetric[i1Tos].
                                                u4Value);
                            }
                        }
                        else
                        {
                            pAddrRange->aMetric[i1Tos].u4Value = pPath->u4Cost;
                            pAddrRange->aMetric[i1Tos].bStatus = OSPF_VALID;
                        }
                    }
                    if (pAddrRange->aMetric[TOS_0].u4Value != LS_INFINITY_24BIT)
                    {
                        pAddrRange->aggFlag = OSPF_TRUE;
                        break;
                    }
                }
            }

        }                        /* if */
    }                            /* TMO_SLL_Scan */

}

/************************************************************************/
/*                                                                      */
/* Function     : AreaGenerateAggLsa                                    */
/*                                                                      */
/* Description  : This Function Scan the Area List and generate Agg      */
 /*               Lsa if Flag is set else flush the Agg Lsa if          */
/*                flag is not set and Agg lsa is present in the database */
/*                                                                     */
/* Input        : pArea        :  pointer to the AREA.                 */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns     : None                                                  */
/*                                                                     */
/************************************************************************/
PUBLIC VOID
AreaGenerateAggLsa (tArea * pArea)
{
    tArea              *pLstArea;
    UINT1               u1Index;
    UINT1               u1Tos;
    UINT1               u1Flag = OSPF_FALSE;
    UINT4               u4Cost;
    tAddrRange         *pAddrRange;
    tLsaInfo           *pLsaInfo;
    tIPADDR             lsId;
    UINT4               u4IpAddrMask;
    UINT4               u4CurrentTime = 0;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* LSA cannot be generated/flushed in standby node */
        return;
    }

    TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pLstArea, tArea *)
    {
        /* Relinquish Needed? */
        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                && (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta))
            {
                gOsRtr.u4RTstaggeredCtxId = pArea->pOspfCxt->u4OspfCxtId;
                OSPFRtcRelinquish (pArea->pOspfCxt);
            }
        }

        if ((UtilIpAddrComp (pArea->areaId,
                             pLstArea->areaId) != OSPF_EQUAL) &&
            ((pLstArea->u4AreaType == NORMAL_AREA) ||
             ((pLstArea->u4AreaType != NORMAL_AREA) &&
              (pLstArea->u1SummaryFunctionality == SEND_AREA_SUMMARY))))
        {
            if (IS_NULL_IP_ADDR (pArea->areaId))
            {
                if ((pLstArea->bTransitCapability == OSPF_TRUE) &&
                    (pLstArea->bPreviousTransitCapability == OSPF_FALSE))
                {
                    AreaFlushAggLsaInTransitArea (pLstArea);
                }
                else if ((pLstArea->bTransitCapability == OSPF_FALSE) &&
                         (pLstArea->bPreviousTransitCapability == OSPF_TRUE))
                {
                    TransAreaFlushLsaFallingInAddrRange (pLstArea);
                }
            }

            if (IS_AREA_TRANSIT_CAPABLE (pLstArea) &&
                IS_NULL_IP_ADDR (pArea->areaId))
            {
                /* Backbone Network should not be condensed in Transit Area */
                continue;
            }
            for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
            {
                pAddrRange = &(pArea->aAddrRange[u1Index]);

                if (pAddrRange->areaAggStatus == INVALID)
                {
                    break;
                }
                pLsaInfo = LsuDatabaseLookUp (NETWORK_SUM_LSA,
                                              &(pAddrRange->ipAddr),
                                              &(pAddrRange->ipAddrMask),
                                              &(pArea->pOspfCxt->rtrId),
                                              (UINT1 *) NULL,
                                              (UINT1 *) pLstArea);

                if (pLsaInfo != NULL)
                {
                    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                    {
                        u4Cost = RtcGetCostFromSummaryLsa
                            (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, u1Tos);
                        if (u4Cost != pAddrRange->aMetric[u1Tos].u4Value)
                        {
                            u1Flag = OSPF_TRUE;
                        }
                    }
                }
                else
                {
                    u1Flag = OSPF_TRUE;
                }

                if ((pAddrRange->aggFlag == OSPF_TRUE) && (u1Flag == OSPF_TRUE))
                {
                    IP_ADDR_COPY (lsId, pAddrRange->ipAddr);
                    OlsModifyLsId (pLstArea, COND_NETWORK_SUM_LSA, &(lsId),
                                   (UINT1 *) pAddrRange);

                    OlsGenerateLsa (pLstArea, COND_NETWORK_SUM_LSA,
                                    &(lsId), (UINT1 *) pAddrRange);

                }
                else if ((pAddrRange->aggFlag == OSPF_FALSE) &&
                         (pLsaInfo != NULL))
                {
                    u4IpAddrMask =
                        OSPF_CRU_BMC_DWFROMPDU (pAddrRange->ipAddrMask);
                    RtcFlushOutSummaryLsa (pLstArea, COND_NETWORK_SUM_LSA,
                                           &(pAddrRange->ipAddr), u4IpAddrMask);
                }
            }
        }
    }
    AreaResetAggFlag (pArea, OSPF_FALSE);

}

/************************************************************************/
/*                                                                      */
/* Function     : AreaResetAggFlag                                    */
/*                                                                      */
/* Description  : This Function reset the Agg Flag                    */
/*                                                                     */
/* Input        : pArea        :  pointer to the AREA.                 */
/*              : u1Flag       :  OSPF_TRUE or OSPF_FALSE              */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns     : None                                                  */
/*                                                                     */
/************************************************************************/
PUBLIC VOID
AreaResetAggFlag (tArea * pArea, UINT1 u1Flag)
{
    UINT1               u1Index;
    UINT1               u1Count;

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        pArea->aAddrRange[u1Index].aggFlag = OSPF_FALSE;
        if (u1Flag == OSPF_TRUE)
        {
            for (u1Count = 0; u1Count < OSPF_MAX_METRIC; u1Count++)
            {
                pArea->aAddrRange[u1Index].aMetric[u1Count].bStatus =
                    OSPF_INVALID;
            }
        }
    }
}

/************************************************************************/
/*                                                                     */
/* Function     : AreaAsExtRtAggrTrieCreate                            */
/*                                                                     */
/* Description  : This Function Creates a Trie Tree for storing        */
/*                 As Ext Route aggr entries                           */
/*                                                                     */
/* Input        : pArea        :  pointer to the AREA.                 */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns      : OSPF_SUCCESS or OSPF_FAILURE                         */
/*                                                                     */
/************************************************************************/
PRIVATE INT4
AreaAsExtRtAggrTrieCreate (tArea * pArea)
{
    tTrieCrtParams      ospfTrieCrtParams;
    UINT4               u4MaxSize = 0;

    ospfTrieCrtParams.u2KeySize = 2 * sizeof (UINT4);
    ospfTrieCrtParams.AppFns = &(OspfTrieLibFuncs);
    u4MaxSize = OSPF_MIN (MAX_OSPF_EXT_ROUTES,
                          FsOSPFSizingParams[MAX_OSPF_EXT_ROUTES_SIZING_ID].
                          u4PreAllocatedUnits) + 1;
    ospfTrieCrtParams.u4NumRadixNodes = u4MaxSize;
    ospfTrieCrtParams.u4NumLeafNodes = u4MaxSize;
    ospfTrieCrtParams.u4NoofRoutes = u4MaxSize;
    ospfTrieCrtParams.u1AppId = OSPF_EXT_RT_AGGR_ID;
    ospfTrieCrtParams.bValidateType = OSIX_FALSE;
    ospfTrieCrtParams.bPoolPerInst = OSIX_FALSE;
    ospfTrieCrtParams.bSemPerInst = OSIX_FALSE;
    pArea->pAsExtAddrRangeTrie = TrieCreateInstance (&ospfTrieCrtParams);

    if (pArea->pAsExtAddrRangeTrie == NULL)
    {
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

/************************************************************************/
/*                                                                     */
/* Function     : AreaAsExtRtAggrTrieDelete                            */
/*                                                                     */
/* Description  : This Function delete the External Rt Aggr Trie       */
/*                                                                     */
/* Input        : pArea        :  pointer to the AREA.                 */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns      : OSPF_SUCCESS or OSPF_FAILURE                         */
/*                                                                     */
/************************************************************************/
PUBLIC VOID
AreaAsExtRtAggrTrieDelete (tArea * pArea)
{
    UINT4               au4Key[2];
    UINT4               au4AsExtRangeIndex[2];
    VOID               *pTemp = NULL;
    tAsExtAddrRange    *pAsExtRange;
    tInputParams        inParams;
    tOutputParams       outParams;
    tDeleteOutParams    DelParams;
    VOID               *pAppSpecPtr = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    /* reset pointer to the cached node */
    pArea->pOspfCxt->pLastExtAgg = NULL;

    while (TrieGetFirstNode (&inParams,
                             &pAppSpecPtr,
                             (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        pAsExtRange = (tAsExtAddrRange *) pAppSpecPtr;
        /* rmeove the Aggr entry from the global (per context Aggr List */
        TMO_SLL_Delete (&(pArea->pOspfCxt->asExtAddrRangeLstInRtr),
                        &(pAsExtRange->nextAsExtAddrRngInRtr));

        inParams.pRoot = pArea->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4AsExtRangeIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pAsExtRange->ipAddr));
        au4AsExtRangeIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4AsExtRangeIndex;

        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;

        OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "IP Addr = %x, Addr Mask = %x\n", pAsExtRange->ipAddr,
                   pAsExtRange->ipAddrMask);
        /* rmeove the Aggr entry from the Trie */
        if (TrieRemove (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
        {
            return;
        }
        gOsRtr.u4AsExtRouteAggrCnt--;

        AS_EXT_AGG_FREE (pAsExtRange);
        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pArea->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
    }
    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.i1AppId = OSPF_EXT_RT_AGGR_ID;
    /* Delete the Ext Rt Aggr Trie */
    if (TrieDelInstance (&inParams, OspfTrieCbDelete, (VOID *) &DelParams) !=
        TRIE_SUCCESS)
    {
        return;
    }
}

/************************************************************************/
/*                                                                     */
/* Function     : AreaResetAggrEntries                                  */
/*                                                                     */
/* Description  : This Function resets the aggr entries                */
/*                                                                     */
/* Input        : pArea        :  pointer to the AREA.                 */
/*                                                                     */
/* Output       : None                                                 */
/*                                                                     */
/* Returns      : None                                                 */
/*                                                                     */
/************************************************************************/
PUBLIC VOID
AreaResetAggrEntries (tArea * pArea)
{
    tAddrRange         *pCurrRange = NULL;
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        pCurrRange = &(pArea->aAddrRange[u1Index]);
        pCurrRange->u4LsaCount = RAG_NO_CHNG;
        pCurrRange->pLsaInfo = NULL;
        pCurrRange->u4RngUpdt = OSPF_FALSE;
        pCurrRange->aggFlag = OSPF_FALSE;
    }
    return;

}

/*---------------------------------------------------------------------*/
/*                     End of the file  osarea.c                       */
/*---------------------------------------------------------------------*/
