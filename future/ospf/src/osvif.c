/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osvif.c,v 1.17 2017/09/21 13:48:47 siva Exp $
 *
 * Description:This file contains procedures for configuration 
 *             of virtual interfaces
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID VifAddToSortVifLst PROTO ((tInterface * pInterface));

/*****************************************************************************/
/*                                                                           */
/* Function     : VifCreate                                                  */
/*                                                                           */
/* Description  : Creates a virtual interface and initializes it.            */
/*                                                                           */
/* Input        : pTransitAreaId    : Area identifier                        */
/*                pNbrId             : Router Id                             */
/*                rowStatus           :                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the created virtual interface, on successful    */
/*                creation                                                   */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
VifCreateInCxt (tOspfCxt * pOspfCxt, tAreaId * pTransitAreaId,
                tRouterId * pNbrId, tROWSTATUS rowStatus)
{
    tInterface         *pInterface;
    tNeighbor          *pNbr;
    tRtEntry           *pRtEntry;
    tIPADDR             ipAddr;
    UINT4               u4PrevIfIndex;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC : VifCreate\n");
    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC,
               pOspfCxt->u4OspfCxtId,
               "Virt If Creation TransAreaId %x NbrId %x\n",
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pTransitAreaId),
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pNbrId));
    MEMSET (ipAddr, 0, sizeof (tIPADDR));

    if (GetFindAreaInCxt (pOspfCxt, pTransitAreaId) == NULL)
    {
        return NULL;
    }

    /* allocate buffer for interface data structure */
    if (IF_ALLOC (pInterface) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "If Alloc Failure\n");
        return NULL;
    }

    /* initialize variables */

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : IfSetDefaultValues \n");

    IfSetDefaultValues (pInterface, INVALID);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : IfSetDefaultValues \n");

    pInterface->bDcEndpt = OSPF_TRUE;
    pInterface->ifOptions = DC_BIT_MASK;
    pInterface->i4RtrDeadInterval = DEF_VIRT_IF_RTR_DEAD_INTERVAL;
    pInterface->u1NetworkType = IF_VIRTUAL;
    pInterface->ifStatus = rowStatus;
    pInterface->pArea = pOspfCxt->pBackbone;

    TMO_SLL_Add (&(pOspfCxt->pBackbone->ifsInArea),
                 &(pInterface->nextIfInArea));

    IP_ADDR_COPY (pInterface->transitAreaId, *pTransitAreaId);
    IP_ADDR_COPY (pInterface->destRtrId, *pNbrId);

    VifAddToSortVifLst (pInterface);

    if ((pNbr = NbrCreate (&gNullIpAddr, pNbrId, 0, pInterface,
                           CONFIGURED_NBR)) == NULL)
    {

        IfDelete (pInterface);

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC |
                  OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Virt Nbr Creation Failure\n");

        return NULL;
    }

    if (pOspfCxt->u4ABRType != STANDARD_ABR)
    {
        if (pOspfCxt->u1RtrNetworkLsaChanged == OSPF_TRUE)
        {
            TmrStopTimer (gTimerLst, &(pOspfCxt->runRtTimer.timerNode));
        }
        pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
        RtcSetRtTimer (pOspfCxt);
        return pInterface;
    }

    /* 
     * search routing table to find cost and interface to be associated with
     * the virtual link 
     */
    if ((pRtEntry = RtcFindRtEntry (pOspfCxt->pOspfRt, pNbrId,
                                    &gNullIpAddr, DEST_AREA_BORDER)) != NULL)
    {

        if ((RtcBldSpfTreeTransAreaInCxt (pInterface->pArea->pOspfCxt,
                                          &(pInterface->transitAreaId),
                                          &(pInterface->destRtrId),
                                          &ipAddr)) == OSPF_TRUE)
        {

            pInterface->operStatus = OSPF_ENABLED;
            pInterface->u2RxmtInterval = VIRTUAL_IF_RXMT_INTERVAL;
            u4PrevIfIndex = pInterface->u4IfIndex;
            VifSetVifValues (pInterface, pRtEntry);

            if (pInterface->u4IfIndex != u4PrevIfIndex)
            {
                NbrUpdateNbrTable (pNbr, u4PrevIfIndex, pInterface->u4IfIndex);
            }

            if (pInterface->u4AddrlessIf != 0)
            {
                IP_ADDR_COPY (pNbr->nbrIpAddr, pNbrId);
            }
            else
            {
                IP_ADDR_COPY (pNbr->nbrIpAddr, ipAddr);
            }
            OspfIfUp (pInterface);
        }

    }
    else
    {
        pInterface->operStatus = OSPF_DISABLED;
    }

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Virt If Created\n");
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT : VifCreate\n");

    return pInterface;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VifAddToSortVifLst                                    */
/*                                                                           */
/* Description  : The specified virtual interface is added to the list of    */
/*                virtual interfaces in the appropriate position which is    */
/*                found based on the transit area id and the destination     */
/*                router id.                                                 */
/*                                                                           */
/* Input        : pInterface        : Interface to be added                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
VifAddToSortVifLst (tInterface * pInterface)
{

    tInterface         *pLstInterface;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pPrevNode;

    pPrevNode = NULL;

    TMO_SLL_Scan (&(pInterface->pArea->pOspfCxt->virtIfLst), pLstNode,
                  tTMO_SLL_NODE *)
    {

        pLstInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        if (UtilVirtIfIndComp (pLstInterface->transitAreaId,
                               pLstInterface->destRtrId,
                               pInterface->transitAreaId,
                               pInterface->destRtrId) == OSPF_GREATER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&(pInterface->pArea->pOspfCxt->virtIfLst), pPrevNode,
                    &(pInterface->nextSortIf));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VifSetVifValues                                            */
/*                                                                           */
/* Description  : Initializes the virtual interface.                         */
/*                                                                           */
/* Input        : pVirtIf           : The virtual interface whose fields     */
/*                                      are to be set                        */
/*                pRtEntry          : The RT entry to the other end point    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
VifSetVifValues (tInterface * pVirtIf, tRtEntry * pRtEntry)
{

    tInterface         *pInterface = (tInterface *) NULL;
    /* The corresponding non-virtual interface  */
    tPath              *pPath;
    INT1                i1Tos;

    OSPF_TRC (OSPF_FN_ENTRY, pVirtIf->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC:VifSetVifValues\n");

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
        TMO_SLL_Scan (&(pRtEntry->aTosPath[i1Tos]), pPath, tPath *)
        {
            if (pPath->u1Flag != OSPF_NEW_PATH)
            {
                /* This path is a old path get the next valid path */
                continue;
            }

            if (UtilIpAddrComp (pVirtIf->transitAreaId,
                                pPath->areaId) == OSPF_EQUAL)
            {
                if (i1Tos == TOS_0)
                {
                    if (pPath->aNextHops[0].u1Status == OSPF_NEXT_HOP_VALID)
                    {
                        pInterface = pPath->aNextHops[0].pInterface;
                    }
                    if (pInterface == NULL)
                    {
                        return;
                    }
                    pVirtIf->admnStatus = OSPF_ENABLED;

                    IP_ADDR_COPY (pVirtIf->ifIpAddr, pInterface->ifIpAddr);

                    IP_ADDR_COPY (pVirtIf->ifIpAddrMask,
                                  pInterface->ifIpAddrMask);
                    pVirtIf->u4AddrlessIf = pInterface->u4AddrlessIf;
                    pVirtIf->u4IfIndex = pInterface->u4IfIndex;
                    /* MTU Size of the Virtual interface is set to the Next hop 
                     * Interfaces MTU Size.
                     */
                    pVirtIf->u4MtuSize = pInterface->u4MtuSize;

                    /* The virtual interface assigned end pt status as the
                       associated interface */

                    pVirtIf->bDcEndpt = OSPF_TRUE;
                    pVirtIf->ifOptions = DC_BIT_MASK;
                }
                pVirtIf->aIfOpCost[(INT2) i1Tos].u4Value = pPath->u4Cost;
                pVirtIf->aIfOpCost[(INT2) i1Tos].rowStatus = ACTIVE;
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pVirtIf->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT:VifSetVifValues\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VifSetVLValues                                             */
/*                                                                           */
/* Description  : Initializes the virtual interface.                         */
/*                                                                           */
/* Input        : pVirtIf           : The virtual interface whose fields     */
/*                                      are to be set                        */
/*                pRtEntry          : The RT entry to the other end point    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
VifSetVLValues (tInterface * pVirtIf, tCandteNode * pNewVertex)
{
    tInterface         *pInterface = (tInterface *) NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pVirtIf->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC:VifSetVLValues\n");

    if (pNewVertex->aNextHops[0].u1Status == OSPF_NEXT_HOP_VALID)
    {
        pInterface = pNewVertex->aNextHops[0].pInterface;
    }
    else
    {
        return OSPF_FAILURE;
    }

    pVirtIf->admnStatus = OSPF_ENABLED;
    IP_ADDR_COPY (pVirtIf->ifIpAddr, pInterface->ifIpAddr);
    IP_ADDR_COPY (pVirtIf->ifIpAddrMask, pInterface->ifIpAddrMask);
    pVirtIf->u4AddrlessIf = 0;
    pVirtIf->u4IfIndex = pInterface->u4IfIndex;
    /* MTU Size of the Virtual interface is set to the Next hop 
     * Interfaces MTU Size.
     */
    pVirtIf->u4MtuSize = pInterface->u4MtuSize;
    /* The virtual interface assigned end pt status as the
       associated interface */
    pVirtIf->bDcEndpt = OSPF_TRUE;
    OSPF_TRC (OSPF_FN_EXIT, pVirtIf->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT:VifSetVLValues\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VifSendConfErrTrap                                     */
/*                                                                           */
/* Description  : This routine is used to send trap for configuration errors.*/
/*                                                                           */
/* Input        : pInterface      : pointer to interface.                   */
/*                u1TrapId       : trap identifier.                        */
/*                u1PktType      : type of packet.                         */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
VifSendConfErrTrap (tInterface * pInterface,
                    UINT1 u1TrapId, UINT1 u1ErrType, UINT1 u1PktType)
{
    tVifConfErrTrapInfo trapVifConfErr;

    if (IS_TRAP_ENABLED_IN_CXT (u1TrapId, pInterface->pArea->pOspfCxt))
    {
        IP_ADDR_COPY (trapVifConfErr.tranAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (trapVifConfErr.virtNbr, pInterface->destRtrId);
        trapVifConfErr.u1ErrType = u1ErrType;
        trapVifConfErr.u1PktType = u1PktType;
        IP_ADDR_COPY (trapVifConfErr.rtrId, pInterface->pArea->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pInterface->pArea->pOspfCxt, u1TrapId,
                             &trapVifConfErr);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : VifSendBadPktTrap                                      */
/*                                                                           */
/* Description  : This routine is used to send trap for reception of bad     */
/*                packets over virtual interface.                            */
/*                                                                           */
/* Input        : pInterface      : pointer to interface.                   */
/*                u1PktType      : type of the packet.                     */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
VifSendBadPktTrap (tInterface * pInterface, UINT1 u1PktType)
{
    tVifRxBadPktTrapInfo trapVifRxBadPkt;

    if (IS_TRAP_ENABLED_IN_CXT (VIRT_IF_RX_BAD_PACKET_TRAP,
                                pInterface->pArea->pOspfCxt))
    {
        IP_ADDR_COPY (trapVifRxBadPkt.tranAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (trapVifRxBadPkt.virtNbr, pInterface->destRtrId);
        trapVifRxBadPkt.u1PktType = u1PktType;
        IP_ADDR_COPY (trapVifRxBadPkt.rtrId,
                      pInterface->pArea->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pInterface->pArea->pOspfCxt,
                             VIRT_IF_RX_BAD_PACKET_TRAP, &trapVifRxBadPkt);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : VifSendRxmtTrap                                         */
/*                                                                           */
/* Description  : This routine is used to send traps for retransmissions     */
/*                over this interface.                                       */
/*                                                                           */
/* Input        : pNbr            : pointer to neighbour.                   */
/*                pLsHeader      : pointer to Ls header.                   */
/*                u1PktType      : type of the packet.                     */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
VifSendRxmtTrap (tNeighbor * pNbr, tLsHeader * pLsHeader, UINT1 u1PktType)
{
    tVifRxmtTrapInfo    trapVifTxRxmt;

    if (!IS_INITIAL_IFACE_ACTIVITY_TRAP (pNbr->pInterface) &&
        IS_TRAP_ENABLED_IN_CXT (VIRT_IF_TX_RETRANSMIT_TRAP,
                                pNbr->pInterface->pArea->pOspfCxt))
    {
        IP_ADDR_COPY (trapVifTxRxmt.tranAreaId,
                      pNbr->pInterface->transitAreaId);
        IP_ADDR_COPY (trapVifTxRxmt.virtNbr, pNbr->pInterface->destRtrId);
        trapVifTxRxmt.u1PktType = u1PktType;
        trapVifTxRxmt.u1LsdbType = pLsHeader->u1LsaType;
        IP_ADDR_COPY (trapVifTxRxmt.lsdbLsid, pLsHeader->linkStateId);
        IP_ADDR_COPY (trapVifTxRxmt.lsdbRtrId, pLsHeader->advRtrId);
        IP_ADDR_COPY (trapVifTxRxmt.rtrId,
                      pNbr->pInterface->pArea->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pNbr->pInterface->pArea->pOspfCxt,
                             VIRT_IF_TX_RETRANSMIT_TRAP, &trapVifTxRxmt);
    }
}

/*-----------------------------------------------------------------------*/
/*                   End of the file   osvif.c                           */
/*-----------------------------------------------------------------------*/
