/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osgr.c,v 1.42 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains the restarting router related
 *             modules during Graceful restart process.
 *
 *******************************************************************/
#include "osinc.h"
#ifdef OPQAPSIM_WANTED
#include "oashdr.h"
#endif
#ifdef L3_SWITCHING_WANTED
#include "ipnp.h"
#endif

#define MAX_COLUMN_LENGTH 80

PRIVATE VOID        GrStoreRestartInfo (VOID);
PRIVATE VOID        GrExtRouteDeleteAllInCxt (tOspfCxt * pOspfCxt);
PRIVATE VOID        GrLsaDeleteAllInCxt (tOspfCxt * pOspfCxt);
PRIVATE VOID        GrRtDeleteAllInCxt (tOspfRt * pOspfRt);
PRIVATE VOID        GrInterfaceDeleteAllInCxt (tOspfCxt * pOspfCxt);
PRIVATE VOID        GrInterfaceDisable (tInterface * pInterface);
PRIVATE VOID        GrInterfaceDelete (tInterface * pInterface);
PRIVATE VOID        GrAreaDeleteAllInCxt (tOspfCxt * pOspfCxt);
PRIVATE VOID        GrAreaDeleteInCxt (tArea * pArea);
PRIVATE VOID        GrTaskDeInitialise (VOID);
PRIVATE VOID        GrFlushUnwantedSelfLsaInCxt (tOspfCxt * pOspfCxt);
PRIVATE VOID        GrFlushGraceLsaInCxt (tOspfCxt * pOspfCxt);
PRIVATE INT4        GrLsuCheckSelfOrigLsaInCxt (tOspfCxt * pOspfCxt,
                                                tArea * pArea,
                                                tLsHeader * pLsHeader,
                                                UINT1 *pLsa);
PRIVATE INT4        GrLsuSearchRtrLsa (tLsaInfo * pLsaInfo,
                                       tRtrLsaRtInfo * pRtrLsaRtInfo);
PRIVATE INT4        GrLsuIsNbrOrigLsaInCxt (tOspfCxt * pOspfCxt,
                                            tLsHeader * pLsHeader);
PRIVATE INT4        GrLsuCheckNbrLsaInCxt (tOspfCxt * pOspfCxt, tArea * pArea,
                                           tLsHeader * pLsHeader, UINT1 *pLsa);
PRIVATE VOID        GrLsuGetLinksFromLsa (UINT1 *pu1CurrPtr,
                                          tRtrLsaRtInfo * pRtrLsaRtInfo);
PRIVATE INT4        GrLsuSearchRtrLsaRBTree (tOspfCxt * pOspfCxt,
                                             tLsHeader * pLsHeader);
PRIVATE VOID        GrLsuDelLinkInfoFromRtrRBTree (tOspfCxt * pOspfCxt,
                                                   tRtrLsaRtInfo
                                                   nbrRtrLsaRtInfo,
                                                   tLsHeader * pLsHeader);
PRIVATE tLsaDesc   *GrOlsConstructLsaDescRcvdSelfOrgLsa
PROTO ((tLsHeader * pLsHeader, tNeighbor * pNbr, UINT1 *pu1NewDescFlag));

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetRestartSupport                                        */
/*                                                                           */
/* Description  : Routine to set/reset the graceful restart support. The     */
/*                routine registers/de-registers from the opaque module      */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                u1RestartSupport - Restart support                         */
/*                                   OSPF_RESTART_NONE    - Not supported    */
/*                                   OSPF_RESTART_PLANNED - supports planned */
/*                                   OSPF_RESTART_BOTH    - supports both    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetRestartSupport (tOspfCxt * pOspfCxt, UINT1 u1RestartSupport)
{
    tAppInfo           *pAppInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetRestartSupport\r\n");

    /* Graceful restart is supported only if opaque option is enabled
     * in this router */

    if (pOspfCxt->u1OpqCapableRtr == OSPF_FALSE)
    {
        OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                      "Opaque support not enabled\r\n");
        return OSPF_FAILURE;
    }

    if (u1RestartSupport == OSPF_RESTART_NONE)
    {
        /* De-register from the opaque module. Instead of posting a message
         * to queue, De-registration function is directly called */
        if ((OpqAppDeRegMsgFrmQInCxt (pOspfCxt,
                                      GRACE_LSA_OPQ_TYPE)) == OSPF_FAILURE)
        {
            /* Failed to de-register from the opaque module */
            OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                          "Opaque de-registration failed\r\n");
            return OSPF_FAILURE;
        }
    }
    else
    {
        /* Restart support is either planned or both. Send a registration
           request to the opaque module */
        /* Grace LSA registration request.
         * Opauqe Type = 3
         * Opaque LSA type = 9 (Link local opaque LSA) */

        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, GRACE_LSA_OPQ_TYPE);
        if (pAppInfo == NULL)
        {
            if (APP_ALLOC (pAppInfo) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "GrSetRestartSupport: APP Alloc Failure\n");
                return OSPF_FAILURE;
            }
            pOspfCxt->pAppInfo[GRACE_LSA_OPQ_TYPE] = pAppInfo;

            pAppInfo->u4AppId = GRACE_LSA_OPQ_TYPE;
            pAppInfo->u1OpqType = GRACE_LSA_OPQ_TYPE;
            pAppInfo->u1LSATypesRegistered = TYPE9_BIT_MASK;
            pAppInfo->u4InfoFromOSPF = 0;
            pAppInfo->OpqAppCallBackFn = NULL;
            pAppInfo->u1Status = OSPF_VALID;

            /* Register with the opaque module. Instead of posting a message
             * to queue, Registration function is directly called */
            OpqAppRegMsgFrmQInCxt (pOspfCxt, GRACE_LSA_OPQ_TYPE);
        }
    }

    pOspfCxt->u1RestartSupport = u1RestartSupport;
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetRestartSupport\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetGraceperiod                                           */
/*                                                                           */
/* Description  : Routine to set the grace period for graceful restart       */
/*                Default value is 120                                       */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                u4GracePeriod    - Grace period                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetGraceperiod (tOspfCxt * pOspfCxt, UINT4 u4GracePeriod)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetGraceperiod\r\n");

    pOspfCxt->u4GracePeriod = u4GracePeriod;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetGraceperiod\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetGraceAckState                                         */
/*                                                                           */
/* Description  : Routine to set the whether Ack is required from Helper or  */
/*                not for the sent Grace LSA                                 */
/*                                                                           */
/* Input        : pOspfCxt           - pointer to OSPF context               */
/*                u4GraceAckState    - Grace period                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetGraceAckState (tOspfCxt * pOspfCxt, UINT4 u4GraceAckState)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetGraceAckState\r\n");

    pOspfCxt->u4GraceAckState = u4GraceAckState;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetGraceAckState\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetGraceLsaTxCount                                       */
/*                                                                           */
/* Description  : Routine to set the Maximum Grace LSA Count                 */
/*                                                                           */
/* Input        : pOspfCxt           - pointer to OSPF context               */
/*                u1GrLsaMaxTxCount    - Grace LSA MAX Count                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetGraceLsaTxCount (tOspfCxt * pOspfCxt, UINT1 u1GrLsaMaxTxCount)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetGraceLsaTxCount\r\n");

    pOspfCxt->u1GrLsaMaxTxCount = u1GrLsaMaxTxCount;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetGraceLsaTxCount\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetGrRestartReason                                       */
/*                                                                           */
/* Description  : Routine to set the Graceful Restart ERason                 */
/*                                                                           */
/* Input        : pOspfCxt           - pointer to OSPF context               */
/*                u1GrRestartReason    - Graceful Restart Reason             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetGrRestartReason (tOspfCxt * pOspfCxt, UINT1 u1RestartReason)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetGrRestartReason\r\n");

    pOspfCxt->u1RestartReason = u1RestartReason;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetGrRestartReason\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrInitiateRestart                                          */
/*                                                                           */
/* Description  : This routine triggers the graceful shutdown of OSPF        */
/*                process. If the application is registered, grace LSA is    */
/*                constructed and sent out through all the interfaces        */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                u1RestartType    - planned or unplanned                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrInitiateRestart (UINT1 u1RestartType, UINT1 u1RestartReason)
{
    tAppInfo           *pAppInfo = NULL;    /* Used to find whether the
                                             * application is registered
                                             * or not */
    tOpqLSAInfo         opqLSAInfo;    /* used to store the grace LSA */
    tInterface         *pInterface = NULL;    /* Interface through which the
                                             * LSA is to be transmitted */
    tOspfCxt           *pOspfCxt = NULL;    /* pointer to context */
    tTMO_SLL_NODE      *pLstNode = NULL;    /* Interface SLL node */
    tArea              *pArea = NULL;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4OspfPrevCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4NbrCount = 0;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrInitiateRestart\r\n");

    /* Scan all the context */
    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        /* No context available */
        return OSPF_FAILURE;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
        if (pOspfCxt == NULL)
        {
            return OSPF_FAILURE;
        }
        /* Initialise the Grace LSA transmission count */
        pOspfCxt->u1GraceLsaTxCount = 0;

        /* graceful restart process should not be triggered
         * on the following cases.
         * a. Application is not registered.
         * b. Graceful restart support is not configured.
         */
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, GRACE_LSA_OPQ_TYPE);

        if ((pAppInfo == NULL) ||
            (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS) ||
            (pOspfCxt->u1RestartSupport == OSPF_RESTART_NONE) ||
            ((pOspfCxt->u1RestartSupport != OSPF_RESTART_BOTH) &&
             (u1RestartType == OSPF_RESTART_UNPLANNED)))
        {
            /* Scan the next context */
            continue;
        }

        /* Get the neighbor count in this context */
        if (pOspfCxt->pBackbone != NULL)
        {
            u4NbrCount =
                (pOspfCxt->pBackbone->u4FullNbrCount +
                 pOspfCxt->pBackbone->u4FullVirtNbrCount);
        }
        if (u4NbrCount == 0)
        {
            pArea = NULL;
            while ((pArea = (tArea *) TMO_SLL_Next (&(pOspfCxt->areasLst),
                                                    (tTMO_SLL_NODE *) pArea)) !=
                   NULL)
            {
                u4NbrCount += pArea->u4FullNbrCount;
                u4NbrCount += pArea->u4FullVirtNbrCount;
                if (u4NbrCount > 0)
                {
                    break;
                }
            }
        }
        if (u4NbrCount == 0)
        {
            /* If there is no neighbors in this context,
             * move to the next context */
            continue;
        }

        /* In case of unplanned restart, restart reason should be unknown or
         * switch to redundant control processor (RFC 3623. Section 5) */
        if ((u1RestartType == OSPF_RESTART_UNPLANNED) &&
            ((u1RestartReason == OSPF_GR_SW_RESTART) ||
             (u1RestartReason == OSPF_GR_SW_UPGRADE)))
        {
            /* Scan the next context */
            continue;
        }
        pOspfCxt->u1RestartReason = u1RestartReason;

        /* Delete the route related timers and trigger the route calculation
         * to ensure that the forwarding table is up-to date.
         * RFC 3623 Section 2.1 Step 1 */
        TmrDeleteTimer (&(pOspfCxt->runRtTimer));
        if (pOspfCxt->u1VrfSpfTmrStatus == OSPF_TRUE)
        {
            TMO_DLL_Delete (&(gOsRtr.vrfSpfRtcList), &(pOspfCxt->vrfSpfNode));
            pOspfCxt->u1VrfSpfTmrStatus = OSPF_FALSE;
        }

        /* Trigger the route calculation if the flag is set */
        if (pOspfCxt->u1RtrNetworkLsaChanged == OSPF_TRUE)
        {
            pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;
            RtcCalculateRtInCxt (pOspfCxt);
        }

        /* Scan all the interfaces attached to the context and construct the
         * grace LSA for the each interface. Call OpqAppToOpqModuleSendInCxt to
         * transmit the LSA through that interface */
        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            /* Clear the tOpqLSAInfo structure */
            MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));

            /* Construct the Grace LSA packet */
            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSAInfo,
                                          NEW_LSA)) == OSPF_FAILURE)
            {
                /* Contruct GRace LSA failed. Scan the next interface */
                continue;
            }

            /* Transmit the Grace LSA  and set the Grace LSA sent flag
             * to true */
            OpqAppToOpqModuleSendInCxt (pOspfCxt->u4OspfCxtId, &opqLSAInfo);
            /* Clear LSA allocated in GrUtilConstructGraceLSA */
            LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
        }

        /* Transmit the Grace LSA through all the attached virtual links */
        TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));

            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSAInfo,
                                          NEW_LSA)) == OSPF_FAILURE)
            {
                continue;
            }

            OpqAppToOpqModuleSendInCxt (pOspfCxt->u4OspfCxtId, &opqLSAInfo);
            LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
        }

        /* Grace LSA is transmitted through all the interfaces in this
         * instance */
        pOspfCxt->u1GraceLsaTxCount++;

        /* Change the restart exit reason to in progress and
         * restart state to shutdown */
        pOspfCxt->u1RestartExitReason = OSPF_RESTART_INPROGRESS;
        pOspfCxt->u1PrevOspfRestartState = OSPF_GR_NONE;
        pOspfCxt->u1RestartStatus = u1RestartType;

        /* Send Trap notification regarding restart status cheange */
        SnmpSendStatusChgTrapInCxt (pOspfCxt, NULL, RST_STATUS_CHANGE_TRAP);

        /* Indicate the RTM regarding Graceful shutdown */
        RtmGrNotifInd (pOspfCxt);

        /* Start the grace timer */
        TmrSetTimer (&(pOspfCxt->graceTimer), RESTART_GRACE_TIMER,
                     NO_OF_TICKS_PER_SEC * pOspfCxt->u4GracePeriod);
    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    /* Delete the global VRF SPF timer */
    TmrDeleteTimer (&(gOsRtr.vrfSpfTimer));

    /* if the total number of neighbors in all contexts is zero,
     * then grace LSA will not be sent. Shutdown the ospf task */
    if (u4NbrCount == 0)
    {
        GrShutdownProcess ();
    }
    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrInitiateRestart\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrUtilConstructGraceLSA                                    */
/*                                                                           */
/* Description  : This routine contructs the grace LSA packets               */
/*                The TLVs related to the grace LSA are filled               */
/*                RFC 3623 Appendix A                                        */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                pInterface       - Interface through which the pkt         */
/*                                   is to be transmitted                    */
/*                pOpqLSAInfo      - pointer to the opaque LSA structure     */
/*                u1LsaStatus      - Status of LSA                           */
/*                                   NEW_LSA           New LSA               */
/*                                   NEW_LSA_INSTANCE  New instance of LSA   */
/*                                   FLUSHED_LSA       Flushed LSA           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrUtilConstructGraceLSA (tOspfCxt * pOspfCxt,
                         tInterface * pInterface,
                         tOpqLSAInfo * pOpqLSAInfo, UINT1 u1LsaStatus)
{
    UINT1              *pu1CurPtr = NULL;    /* LSA pointer */

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrUtilConstructGraceLSA\r\n");

    /* Size of contents od Grace LSA
     * PTMP, Broadcast, NBMA - Grace period TLV + Restart reason TLV +
     *                         IP address TLV
     * PTOP, virtual         - Grace period TLV + Restart reason TLV
     */

    if ((pInterface->u1NetworkType == IF_PTOP) ||
        (pInterface->u1NetworkType == IF_VIRTUAL))
    {
        pOpqLSAInfo->u2LSALen = (UINT2) (2 * GR_TLV_SIZE);
    }
    else
    {
        pOpqLSAInfo->u2LSALen = (UINT2) (3 * GR_TLV_SIZE);
    }

    /* Allocate the memory for the pointer to LSA TLV */

    if (pOpqLSAInfo->u2LSALen > MAX_LSA_SIZE)
    {
        OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                      "GR LSA alloc failed\r\n");
        return OSPF_FAILURE;
    }

    LSA_ALLOC (TYPE10_OPQ_LSA, (pOpqLSAInfo->pu1LSA), pOpqLSAInfo->u2LSALen);
    if (pOpqLSAInfo->pu1LSA == NULL)
    {
        OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                      "GR LSA alloc failed\r\n");
        return OSPF_FAILURE;
    }

    if (pOpqLSAInfo->pu1LSA == NULL)
    {
        OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                      "Grace LSA alloc failed\r\n");
        return OSPF_FAILURE;
    }

    MEMSET ((VOID *) (pOpqLSAInfo->pu1LSA), 0, pOpqLSAInfo->u2LSALen);
    pu1CurPtr = pOpqLSAInfo->pu1LSA;

    /* Add the Grace period TLV */
    LADD2BYTE (pu1CurPtr, GR_PERIOD_TLV_TYPE);
    LADD2BYTE (pu1CurPtr, GR_PERIOD_TLV_LENGTH);
    LADD4BYTE (pu1CurPtr, pOspfCxt->u4GracePeriod);

    /* Add the Restart reason TLV */
    LADD2BYTE (pu1CurPtr, GR_REASON_TLV_TYPE);
    LADD2BYTE (pu1CurPtr, GR_REASON_TLV_LENGTH);
    LADD1BYTE (pu1CurPtr, pOspfCxt->u1RestartReason);
    LADD3BYTE (pu1CurPtr, 0);    /* Padding */

    /* Add the IP address TLV in case of broadcast, NBMA
     * and point-to-multipoint networks. Router-id is used to
     * identify the neighbor for PTOP and virtual links. Since
     * Router-id can be identified using advertising router-id,
     * no need for IP address TLV */
    if ((pInterface->u1NetworkType == IF_PTOMP) ||
        (pInterface->u1NetworkType == IF_NBMA) ||
        (pInterface->u1NetworkType == IF_BROADCAST))
    {
        LADD2BYTE (pu1CurPtr, GR_ADDRESS_TLV_TYPE);
        if (pInterface->u4AddrlessIf == 0)
        {
            LADD2BYTE (pu1CurPtr, GR_ADDRESS_TLV_LENGTH);
            LADDSTR (pu1CurPtr, pInterface->ifIpAddr, MAX_IP_ADDR_LEN);
        }
        else
        {
            LADD2BYTE (pu1CurPtr, sizeof (UINT4));
            LADD4BYTE (pu1CurPtr, pInterface->u4AddrlessIf);
        }
    }

    /* Add the LS header related information in tOpqLSAInfo structure */
    pOpqLSAInfo->u1OpqLSAType = TYPE9_OPQ_LSA;
    OSPF_CRU_BMC_DWTOPDU (pOpqLSAInfo->LsId, OSPF_GR_LSID);

    /* Add the area, interface and LSA instance related information in
     * tOpqLSAInfo structure */
    pOpqLSAInfo->u1LSAStatus = u1LsaStatus;
    pOpqLSAInfo->u4IfaceID = OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
    pOpqLSAInfo->u4AddrlessIf = pInterface->u4AddrlessIf;
    pOpqLSAInfo->u4AreaID = OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId);
    pOpqLSAInfo->pOspfCxt = pOspfCxt;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrUtilConstructGraceLSA\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrDeleteRxmtLst                                            */
/*                                                                           */
/* Description  : This routine deletes the given grace LSA from              */
/*                the re-transmission list. If the grace LSA's               */
/*                re-transmission count is 0, then check the re-transmission */
/*                count for all grace LSA to shutdown the OSPF process       */
/*                                                                           */
/* Input        : pNbr             - The neighbor to which grace LSA         */
/*                                   transmission has reached the threshold  */
/*                pGrLsaInfo       - pointer to Grace LSA info structure     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrDeleteRxmtLst (tNeighbor * pNbr, tLsaInfo * pGrLsaInfo)
{
#ifndef HIGH_PERF_RXMT_LST_WANTED
    UINT4               u4BitPosMask = 0;
#endif /* HIGH_PERF_RXMT_LST_WANTED */
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: GrDeleteRxmtLst\r\n");

#ifdef HIGH_PERF_RXMT_LST_WANTED
    tRxmtNode          *pRxmtNode = NULL;

    pRxmtNode->pNbr = pNbr;
    pRxmtNode->pLsaInfo = pGrLsaInfo;

    /* Delete hte node from the re-transmission list */
    HrLsuLsaRxmtIndexDllDel (pGrLsaInfo, pRxmtNode);
    HrLsuNbrRxmtIndexDllDel (pNbr, pRxmtNode);
    RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);
    pNbr->lsaRxmtDesc.u4LsaRxmtCount--;
    pGrLsaInfo->u4RxmtCount--;
    HrLsuRxmtNodeMemFree (pRxmtNode);
#else

    /* Reset the LSA bit from the neighbor's re-trasnmission list */
    u4BitPosMask = FIRST_BIT_SET_MASK;
    RESET_BIT_POSITION (pNbr->i2NbrsTableIndex, u4BitPosMask,
                        pGrLsaInfo->aRxmtBitMapFlag);
    pNbr->lsaRxmtDesc.u4LsaRxmtCount--;
    pGrLsaInfo->u4RxmtCount--;
    if (pGrLsaInfo->u4RxmtCount == 0)
    {
        TMO_DLL_Delete (&(pGrLsaInfo->pOspfCxt->rxmtLst),
                        &(pGrLsaInfo->rxmtNode));
    }
#endif
    /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
    }

    if (pGrLsaInfo->u4RxmtCount == 0)
    {
        GrCheckRxmtLst (pNbr->pInterface->pArea->pOspfCxt);
    }

    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: GrDeleteRxmtLst\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrCheckRxmtLst                                             */
/*                                                                           */
/* Description  : This routine checks the re-transmission count              */
/*                for all grace LSA in all interfaces. If all the grace LSA  */
/*                are transmitted, OSPF process is shutdown                  */
/*                                                                           */
/* Input        : pNbr             - The neighbor to which grace LSA         */
/*                                   transmission has reached the threshold  */
/*                pGrLsaInfo       - pointer to Grace LSA info structure     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
GrCheckRxmtLst (tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfLstNode = NULL;
    UINT1               u1TxFlag = OSPF_FALSE;
    UINT1               u1GrNoAckFlag = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "EXIT: GrCheckRxmtLst\r\n");

    /* If the Grace LSA re-transmission count is 0, then
     * a. The rest of the neighbors have acknowledged the Grace LSA
     * b. The re-transmission count has reached the threshold for the
     *    neighbors.
     * Since the Grace LSAs are trasnmissted , the router enters restart mode
     */

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfLstNode);

        /* Scan all type 9 opaque LSA */
        TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pLstNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
            if (((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
                 (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
                && (pLsaInfo->u4RxmtCount != 0))
            {
                u1TxFlag = OSPF_TRUE;
                break;
            }
        }
    }
    /* Grace LSA has been transmitted to all the neighbors in this
     * instance. Increment the grace LSA transmission count */
    if (u1TxFlag == OSPF_FALSE)
    {
        /* Grace LSA is not present in any of the re-transmission
         * list in this context */
        pOspfCxt->u1GraceLsaTxCount++;
    }

    /* Scan all the interfaces attached to the context and check 
     * grace LSA ack is received */
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pInterface->u1GrLsaAckRcvd == OSPF_TRUE)
        {
            continue;
        }
        else if (pInterface->u4NbrFullCount == 0)
        {
            continue;
        }
        u1GrNoAckFlag = OSPF_TRUE;
    }

    /* Scan all the virtual interfaces  attached to the context and check 
     * grace LSA ack is received */
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pInterface->u1GrLsaAckRcvd == OSPF_TRUE)
        {
            continue;
        }
        else if (pInterface->u4NbrFullCount == 0)
        {
            continue;
        }
        u1GrNoAckFlag = OSPF_TRUE;
    }

    /* Grace LSA is received from  all the interfaces in this
     * instance */
    if (u1GrNoAckFlag == OSPF_FALSE)
    {
        pOspfCxt->u1GraceLsaTxCount = pOspfCxt->u1GrLsaMaxTxCount;
        pOspfCxt->u1GrLsaAckRcvd = OSPF_TRUE;
    }

    if ((pOspfCxt->u1RestartStatus == OSPF_RESTART_PLANNED) &&
        (u1TxFlag == OSPF_FALSE))
    {
        if ((pOspfCxt->u4GraceAckState == OSPF_ENABLED)
            && (pOspfCxt->u1GrLsaAckRcvd == OSPF_FALSE))
        {
            pOspfCxt->u1OspfRestartState = OSPF_GR_NONE;
            pOspfCxt->u1PrevOspfRestartState = OSPF_GR_RESTART;
            pOspfCxt->u1RestartStatus = OSPF_RESTART_NONE;
        }
        pOspfCxt->u1OspfRestartState = OSPF_GR_SHUTDOWN;
        GrCheckAndShutdownProcess ();
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: GrCheckRxmtLst\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrCheckAndShutdownProcess                                  */
/*                                                                           */
/* Description  : This routine checks the Grace LSA transmission             */
/*                count in all the context. If the transmission count has    */
/*                reached the threshold in all the context or ack received   */
/*                the routing process undergoes graceful shutdown.           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
GrCheckAndShutdownProcess (VOID)
{
    tOspfCxt           *pOspfCxt = NULL;    /* pointer to context */
    tArea              *pArea = NULL;    /* pointer to Area */
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    /* Current context id */
    UINT4               u4OspfPrevCxtId = OSPF_INVALID_CXT_ID;
    /* Current context id */
    UINT4               u4NbrCount = 0;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrCheckAndShutdownProcess\r\n");

    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        return;
    }
    else
    {
        do
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
            if (pOspfCxt == NULL)
            {
                return;
            }
            if (pOspfCxt->pBackbone != NULL)
            {
                u4NbrCount =
                    (pOspfCxt->pBackbone->u4FullNbrCount +
                     pOspfCxt->pBackbone->u4FullVirtNbrCount);
            }
            if (u4NbrCount == 0)
            {
                pArea = NULL;
                while ((pArea = (tArea *) TMO_SLL_Next (&(pOspfCxt->areasLst),
                                                        (tTMO_SLL_NODE *)
                                                        pArea)) != NULL)
                {
                    u4NbrCount += pArea->u4FullNbrCount;
                    u4NbrCount += pArea->u4FullVirtNbrCount;
                    if (u4NbrCount > 0)
                    {
                        break;
                    }
                }
            }
            if (u4NbrCount == 0)
            {
                /* If there is no neighbors in this context,
                 * move to the next context */
                continue;
            }

            /* If the re-transmission count has reached the threshold,
             * scan the next context */
            if (pOspfCxt->u1GraceLsaTxCount < pOspfCxt->u1GrLsaMaxTxCount)
            {
                return;
            }

        }
        while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
               != OSPF_FAILURE);
    }

    OSPF_EXT_TRC (OSPF_RESTART_TRC, OSPF_INVALID_CXT_ID,
                  "Router is shutting down gracefully\r\n");

    /* Store all the GR specific information in non-volatile storage */
    GrStoreRestartInfo ();
    GrShutdownProcess ();
    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrCheckAndShutdownProcess\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrShutdownProcess                                          */
/*                                                                           */
/* Description  : This routine does all the actions specific to              */
/*                Graceful shutdown and stores all the information           */
/*                necessary during restart. CSR is used to store the         */
/*                graceful restart related information.                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
GrShutdownProcess (VOID)
{
    UINT4               u4OspfCxtId = OSPF_DEFAULT_CXT_ID;

    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrShutdownProcess\r\n");

    OspfUnLock ();                /* the lock would be taken by OSPF Show Running Cfg */
#ifdef CLI_WANTED
    IssCsrSaveCli ((UINT1 *) "OSPF");
#endif
    OspfLock ();

    /* Delete all context */
    for (u4OspfCxtId = OSPF_DEFAULT_CXT_ID; u4OspfCxtId < u4MaxCxt;
         u4OspfCxtId++)
    {
        GrDeleteCxt (gOsRtr.apOspfCxt[u4OspfCxtId]);
    }
    OspfRmDeRegisterWithRM ();
    /* Deregister with vcm module */
    VcmDeRegisterHLProtocol (OSPF_PROTOCOL_ID);

    /* Delete all memory and delete the task */
    GrTaskDeInitialise ();

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrShutdownProcess\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrStoreRestartInfo                                         */
/*                                                                           */
/* Description  : This routine stores all GR specific configurations         */
/*                in non-volatile storage.                                   */
/*                                                                           */
/*                  1. GR shutdown time                                      */
/*                  2. 1s timer value                                        */
/*                                                                           */
/*                  1. Context name                                          */
/*                  2. GR status                                             */
/*                  3. Restart reason                                        */
/*                  4. Last attempted GR exit reason                         */
/*                  5. Absolute time at which GR terminates                  */
/*                                                                           */
/*                  1. IP address of each interface                          */
/*                  3. Address If Index of each interface                    */
/*                  3. Cryptographic sequence number of each interface       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrStoreRestartInfo (VOID)
{
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tUtlTm              utlTm;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4OspfPrevCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4SysTime = 0;    /* Time since 2000 */
    UINT4               u4Hindex;
    UINT4               u4RemGracePeriod = 0;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    CHR1                ai1Buf[MAX_COLUMN_LENGTH + OSPF_ONE];
    INT4                i4FileFd = OSPF_ZERO;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrStoreRestartInfo\r\n");

    i4FileFd = FileOpen (OSPF_GR_CONF, OSIX_FILE_CR | OSIX_FILE_WO |
                         OSIX_FILE_AP);

    if (i4FileFd < OSPF_ZERO)
    {
        /* File Creation failed */
        return;
    }

    /* Store the restart status and absolute time at which GR terminates
     * for all context */

    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        /* No context available */
        FileClose (i4FileFd);
        return;
    }

    /* Store the 1s timer and current system time */
    MEMSET (&utlTm, 0, sizeof (tUtlTm));
    UtlGetTime (&utlTm);
    /* Get the seconds since the base year (2000) */
    u4SysTime = GrGetSecondsSinceBase (utlTm);

    SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "SHUT_SYS_TIME   = %d\n", u4SysTime);
    FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

    SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "1s_TIMER_VALUE  = %d\n",
              gOspf1sTimer);
    FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
        if (pOspfCxt == NULL)
        {
            FileClose (i4FileFd);
            return;
        }

        MEMSET (au1OspfCxtName, 0, OSPF_MAX_CONTEXT_STR_LEN);
        if (UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)
            == OSPF_FAILURE)
        {
            /* Scan the next context */
            continue;
        }

        if (pOspfCxt->u1RestartStatus == OSPF_RESTART_PLANNED)
        {
            if (TmrGetRemainingTime (gTimerLst,
                                     (&(pOspfCxt->graceTimer.timerNode)),
                                     &u4RemGracePeriod) != TMR_SUCCESS)
            {
                u4RemGracePeriod = 0;
            }
            else
            {
                u4RemGracePeriod = u4RemGracePeriod / NO_OF_TICKS_PER_SEC;
            }
        }
        else
        {
            u4RemGracePeriod = pOspfCxt->u4GracePeriod;
        }

        TmrStopTimer (gTimerLst, &(pOspfCxt->graceTimer.timerNode));

        SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "\n");
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "CONTEXT = %s\n", au1OspfCxtName);
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "STATUS = ");
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        if ((u4RemGracePeriod == 0) ||
            (pOspfCxt->u1RestartStatus == OSPF_RESTART_NONE))
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "none\n");
        }
        else if (pOspfCxt->u1RestartStatus == OSPF_RESTART_PLANNED)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "planned\n");
        }
        else if (pOspfCxt->u1RestartStatus == OSPF_RESTART_UNPLANNED)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "unplanned\n");
        }
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "GR_REASON = ");
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        if (pOspfCxt->u1RestartReason == OSPF_GR_UNKNOWN)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "unknown\n");
        }

        else if (pOspfCxt->u1RestartReason == OSPF_GR_SW_RESTART)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "restart\n");
        }

        else if (pOspfCxt->u1RestartReason == OSPF_GR_SW_UPGRADE)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "upgrade\n");
        }

        else if (pOspfCxt->u1RestartReason == OSPF_GR_SW_RED)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "redundancy\n");
        }
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "GR_EXIT_REASON  = ");
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_NONE)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "none\n");
        }

        else if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "inprogress\n");
        }

        else if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_COMPLETED)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "completed\n");
        }

        else if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_TIMEDOUT)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "timedout\n");
        }

        else if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_TOP_CHG)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "topchg\n");
        }
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

        /* Store the 1s timer and current system time */
        MEMSET (&utlTm, 0, sizeof (tUtlTm));
        UtlGetTime (&utlTm);
        /* Get the seconds since the base year (2000) */
        u4SysTime = GrGetSecondsSinceBase (utlTm);

        SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "GR_END_TIME     = %d\n",
                  (u4SysTime + u4RemGracePeriod));
        FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));
        /* Since router-id should not change after gracefull resart,
         * so we are saving router-id and routerid-status only when 
         * gracefull restart mode is enabled and router-id selected 
         * as dynamically.*/
        if (pOspfCxt->i4RouterIdStatus != ROUTERID_PERMANENT)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "ROUTERID = %d.%d.%d.%d\n",
                      pOspfCxt->rtrId[0], pOspfCxt->rtrId[1],
                      pOspfCxt->rtrId[2], pOspfCxt->rtrId[3]);
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "ROUTERID_STATUS     = %d\n",
                      pOspfCxt->i4RouterIdStatus);
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));
        }
    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    TMO_HASH_Scan_Table (gOsRtr.pIfHashTable, u4Hindex)
    {
        TMO_HASH_Scan_Bucket (gOsRtr.pIfHashTable, u4Hindex,
                              pInterface, tInterface *)
        {
            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "\n");
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "INTERFACE_IP = %d.%d.%d.%d\n",
                      pInterface->ifIpAddr[0], pInterface->ifIpAddr[1],
                      pInterface->ifIpAddr[2], pInterface->ifIpAddr[3]);
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "INTERFACE_UNNUM = %d\n",
                      pInterface->u4AddrlessIf);
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

            SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "CRYPTO_SEQ_NUM  = %d\n",
                      pInterface->u4CryptSeqNum);
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

            /* Scan the opaque LSA and get the seq num of grace LSA */
            /* Scan all type 9 opaque LSA */
            TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pLstNode,
                          tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
                if (((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
                     (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
                    && (pLsaInfo->u4RxmtCount != 0))
                {
                    break;
                }
            }

            if (pLstNode == NULL)
            {
                SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "GRACE_SEQ_NUM  = 0\n");
            }
            else
            {
                SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "GRACE_SEQ_NUM  = %d\n",
                          pLsaInfo->lsaSeqNum);
            }
            FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));
        }
    }

    FileClose (i4FileFd);
    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrStoreRestartInfo\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrDeleteCxt                                                */
/*                                                                           */
/* Description  : This routine deletes all the LSDB, neighbors,              */
/*                interfaces, routes and timers in this context.             */
/*                The module does not flush the LSA throughout the domain,   */
/*                does not de-register from RTM and does not send any        */
/*                indication to RTM during route deletion.                   */
/*                                                                           */
/* Input        : pOspfCxt - pointer to OSPF context                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
GrDeleteCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;

    if (pOspfCxt == NULL)
    {
        return;
    }
    u4OspfCxtId = pOspfCxt->u4OspfCxtId;

    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: GrDeleteCxt\r\n");

    /* Disable the context */
    GrDisableInCxt (pOspfCxt);

    /* Delete all the interfaces (includeing virtual interfaces.
     * This in turn deletes all the neighbors */
    GrInterfaceDeleteAllInCxt (pOspfCxt);

    /* Delete the RRD config table */
    RrdConfRtInfoDeleteAllInCxt (pOspfCxt);

    /* Delete all the areas attached to the instance */
    GrAreaDeleteAllInCxt (pOspfCxt);

    /* Delete all the hosts attached to the instance */
    HostDeleteAllInCxt (pOspfCxt);

    /* De-initialise the routing table trie instance */
    RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
    RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);

    /* Delete the external LSA staructure */
    RBTreeDelete (pOspfCxt->pAsExtLsaRBRoot);
    pOspfCxt->pAsExtLsaRBRoot = NULL;
    TMO_HASH_Delete_Table (pOspfCxt->pExtLsaHashTable, NULL);
    pOspfCxt->pExtLsaHashTable = NULL;

    /* Free the memory */
    REDIST_REG_FREE (pOspfCxt->pRedistribute);
    ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
    TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
    gOsRtr.apOspfCxt[pOspfCxt->u4OspfCxtId] = NULL;

    TmrDeleteTimer (&(pOspfCxt->graceTimer));

    CONTEXT_FREE (pOspfCxt);

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: GrDeleteCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrDisableInCxt                                             */
/*                                                                           */
/* Description  : This routine disbale the particular context.               */
/*                This function is similar to RtrDisableInCxt                */
/*                but does not flush the LSA from routing domain and         */
/*                does not indicate the RTM regarding route deletion         */
/*                                                                           */
/* Input        : pOspfCxt - pointer to OSPF context                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrDisableInCxt (tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface = NULL;    /* pointer to interface */
    tTMO_SLL_NODE      *pLstNode = NULL;    /* pointer to interface SLL node */
    tArea              *pArea = NULL;    /* pointer ro Area */

    if (pOspfCxt == NULL)
    {
        gu4pOspfCxtFail++;
        return OSPF_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: GrDisableInCxt\r\n");
#ifdef L3_SWITCHING_WANTED
    /* Do the hardware initialisation to receive OSPF packets */
    OspfFsNpOspfDeInit ();
#endif /* L3_SWITCHING_WANTED */
    /* Since the process is undergoing graceful shutdown, self originated
     * LSA should not be flushed */

    /* Delete all external routes redistributed from RTM */
    GrExtRouteDeleteAllInCxt (pOspfCxt);

    /* Disable the admin status */
    pOspfCxt->admnStat = OSPF_DISABLED;

    /* Make all the interfaces down */
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        GrInterfaceDisable (pInterface);
    }
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        GrInterfaceDisable (pInterface);
    }

    /* Delete all the LSA from LSDB */
    GrLsaDeleteAllInCxt (pOspfCxt);

    NetIpv4DeRegisterHigherLayerProtocolInCxt (pOspfCxt->u4OspfCxtId,
                                               FS_OSPF_PROTO);

    /* Initialise the ISM schedule queue */
    IsmInitSchedQueueInCxt (pOspfCxt, NULL);

    /* Delete all routes from the OSPF routing table */
    GrRtDeleteAllInCxt (pOspfCxt->pOspfRt);

    /* Delete all the nodes from SPF tree in all areas */
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        RtcDeleteNodesFromSpf (pArea->pSpf);
    }

    /* Delete the route calculation timer. If this function is called
     * during graceful shutdown, this woud have been deleted before the
     * transmission of grace LSA */
    TmrDeleteTimer (&(pOspfCxt->runRtTimer));
#ifdef DEBUG_WANTED
    /* NOTE: Uncomment for debugging */
    /* TmrDeleteTimer (&(pOspfCxt->dumpTimer)); */
#endif
    /* Delete the trap limit timer */
    TmrDeleteTimer (&(pOspfCxt->trapLimitTimer));

    /* Delete the distance timer */
    TmrDeleteTimer (&(pOspfCxt->distanceTimer));

    pOspfCxt->u4ExtLsaChksumSum = 0;
    pOspfCxt->u4OriginateNewLsa = 0;
    pOspfCxt->u4RcvNewLsa = 0;
    pOspfCxt->u4ExtRtCount = 0;
    pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;
    pOspfCxt->u1AsbSummaryLsaChanged = OSPF_FALSE;
    pOspfCxt->bOverflowState = OSPF_FALSE;

    /* Delete the exit overflow timer */
    TmrDeleteTimer (&pOspfCxt->exitOverflowTimer);

    /* Delete the VRF SPF timer node. When GrDisableInCxt is called during
     * graceful shutdown process, this would have been deleted before
     * generating the grace LSA and status would have been set to OSPF_FALSE */
    if (pOspfCxt->u1VrfSpfTmrStatus == OSPF_TRUE)
    {
        TMO_DLL_Delete (&(gOsRtr.vrfSpfRtcList), &(pOspfCxt->vrfSpfNode));
        pOspfCxt->u1VrfSpfTmrStatus = OSPF_FALSE;
    }

    /* Disbale the re-distribution status and send an event
     * to RTM */
    if (pOspfCxt->u4RrdSrcProtoBitMask & RRD_SRC_PROTO_BIT_MASK)
    {
        if (RtmTxRedistributeDisableInCxt (pOspfCxt,
                                           pOspfCxt->u4RrdSrcProtoBitMask,
                                           pOspfCxt->au1RMapName) ==
            OSPF_FAILURE)

        {
            /* Re-distribution disable failed */
            OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                          "Re-distribution disable failed\r\n");
            gu4RtmTxRedistributeDisableInCxtFail++;
            return OSPF_FAILURE;
        }
    }
    pOspfCxt->u4RrdSrcProtoBitMask = 0;

    pOspfCxt->pRedistribute->u4MaxQMsgLength = 0;

    gOsRtr.u4ActiveCxtCount--;
#ifdef RAWSOCK_WANTED
    if (gOsRtr.u4ActiveCxtCount == 0)
    {
        OspfCloseSocket (pOspfCxt->u4OspfCxtId);
    }
#endif

    /* Graceful process should not de-register from RTM */
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: GrDisableInCxt\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrExtRouteDeleteAllInCxt                                   */
/*                                                                           */
/* Description  : This routine deletes all the external routes               */
/*                re-distributed from the external trie data structure       */
/*                                                                           */
/* Input        : pOspfCxt - pointer to OSPF context                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrExtRouteDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tExtRoute          *pExtRoute = NULL;    /* pointer to external route */
    tExpRtNode         *pRtmRouteMsg = NULL;    /* RTM route message node */
    tExpRtNode         *pRtmNextRouteMsg = NULL;    /* Next RTM route
                                                     * message node */
    tInputParams        inParams;    /* Trie input parameters */
    VOID               *pAppSpecPtr = NULL;    /* Trie output */
    UINT4               au4Key[2];    /* Key to be used by trie */

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrExtRouteDeleteAllInCxt\r\n");

    MEMSET (&inParams, 0, sizeof (tInputParams));
    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    while (TrieGetFirstNode (&inParams, &pAppSpecPtr,
                             (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        pExtRoute = (tExtRoute *) pAppSpecPtr;
        /* Delete the external route from the trie */
        ExtrtDeleteFromTrieInCxt (pOspfCxt, pExtRoute);
        /* Clear the memory */
        EXT_ROUTE_FREE (pExtRoute);
        pExtRoute = NULL;
    }

    /* Delete the unprocesses RTM routes */
    OsixSemTake (gOsRtr.RtmLstSemId);
    if ((pRtmRouteMsg = (tExpRtNode *)
         TMO_SLL_First (&gOsRtr.RtmRtLst)) == NULL)
    {
        OsixSemGive (gOsRtr.RtmLstSemId);
        return;
    }
    do
    {
        pRtmNextRouteMsg = (tExpRtNode *) TMO_SLL_Next ((&gOsRtr.RtmRtLst),
                                                        &(pRtmRouteMsg->
                                                          NextRt));
        if (pRtmRouteMsg->RtInfo.u4ContextId == pOspfCxt->u4OspfCxtId)
        {
            TMO_SLL_Delete (&gOsRtr.RtmRtLst, &(pRtmRouteMsg->NextRt));
            RTM_ROUTE_FREE (pRtmRouteMsg);
        }
        pRtmRouteMsg = pRtmNextRouteMsg;

    }
    while (pRtmRouteMsg != NULL);
    /* Release the DataLock for accessing RTM Route List */
    OsixSemGive (gOsRtr.RtmLstSemId);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrExtRouteDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsaDeleteAllInCxt                                        */
/*                                                                           */
/* Description  : This routine deletes all the LSA present in this           */
/*                instance. This routine does not flush the LSA from         */
/*                the routing domain                                         */
/*                                                                           */
/* Input        : pOspfCxt - pointer to OSPF context                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrLsaDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;    /* SLL node for opaque LSA */
    tTMO_SLL_NODE      *pIntNode = NULL;    /* SLL node for Interface */
    UINT4               u4HashKey = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsaDeleteAllInCxt\r\n");

    /* Delete all the LSA present in all the attached area's database */
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashKey)
        {
            while ((pLsaInfo = (tLsaInfo *)
                    TMO_HASH_Get_First_Bucket_Node (pArea->pLsaHashTable,
                                                    u4HashKey)) != NULL)
            {
                /* Delete the LSA from the databse */
                GrDeleteLsaFromDatabase (pLsaInfo);
            }
        }
    }

    /* If the backbone area is not present in the area List,
     * delete the LSA from backbone area */
    if (((tArea *) TMO_SLL_Last (&(pOspfCxt->areasLst))) != pOspfCxt->pBackbone)
    {
        pArea = pOspfCxt->pBackbone;
        u4HashKey = 0;
        TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashKey)
        {
            while ((pLsaInfo = (tLsaInfo *)
                    TMO_HASH_Get_First_Bucket_Node (pArea->pLsaHashTable,
                                                    u4HashKey)) != NULL)
            {
                /* Delete the LSA from the databse */
                GrDeleteLsaFromDatabase (pLsaInfo);
            }
        }
    }
    /* Delete all external LSA */
    while ((pLsaInfo = (tLsaInfo *)
            RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot)) != NULL)
    {
        GrDeleteLsaFromDatabase (pLsaInfo);
    }

    /* Delete all Type 11 LSA */
    while ((pLstNode = TMO_SLL_First (&(pOspfCxt->Type11OpqLSALst))) != NULL)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
        GrDeleteLsaFromDatabase (pLsaInfo);
    }

    /* Delete all Type 9 LSA */
    pLstNode = NULL;
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIntNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIntNode);
        while ((pLstNode =
                TMO_SLL_First (&(pInterface->Type9OpqLSALst))) != NULL)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
            GrDeleteLsaFromDatabase (pLsaInfo);
        }
    }
    pLstNode = NULL;
    pIntNode = NULL;
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pIntNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIntNode);
        while ((pLstNode =
                TMO_SLL_First (&(pInterface->Type9OpqLSALst))) != NULL)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
            GrDeleteLsaFromDatabase (pLsaInfo);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrLsaDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrDeleteLsaFromDatabase                                    */
/*                                                                           */
/* Description  : This routine deletes LSA from the LSDB                     */
/*                The routine deletes the LSA from all rxmt list, deletes    */
/*                all LSA specific timers and clears the memory              */
/*                                                                           */
/* Input        : pLsaInfo - pointer to LSA info structure                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
GrDeleteLsaFromDatabase (tLsaInfo * pLsaInfo)
{
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;
    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: GrDeleteLsaFromDatabase\r\n");

    /* Delete the LSA from all the re-trasnmission list.
     * After deleting all the LSA from the rxmt list,
     * LSA rxmt timer in neighbor will be deleted */
    LsuDeleteNodeFromAllRxmtLst (pLsaInfo);

    /* Modify the Area check sum */
    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case COND_NETWORK_SUM_LSA:
        case DEFAULT_NETWORK_SUM_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case AS_EXT_LSA:
            pLsaInfo->pOspfCxt->u4ExtLsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case TYPE11_OPQ_LSA:
            pLsaInfo->pOspfCxt->u4Type11OpqLSAChksumSum
                -= pLsaInfo->u2LsaChksum;
            break;
        case TYPE10_OPQ_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
            pLsaInfo->pArea->u4Type10OpqLSAChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case TYPE9_OPQ_LSA:
            pLsaInfo->pInterface->u4Type9OpqLSAChksumSum -=
                pLsaInfo->u2LsaChksum;
            break;
    }

    /* if the lsa to be removed is dc_bit_reset lsa */
    if (!IS_DC_BIT_SET_LSA (pLsaInfo) &&
        pLsaInfo->lsaId.u1LsaType != AS_EXT_LSA &&
        pLsaInfo->lsaId.u1LsaType != TYPE9_OPQ_LSA &&
        pLsaInfo->lsaId.u1LsaType != TYPE11_OPQ_LSA)
    {

        if (pLsaInfo->pArea->u4DcBitResetLsaCount != 0)
        {
            pLsaInfo->pArea->u4DcBitResetLsaCount--;
        }
        if (IS_INDICATION_LSA (pLsaInfo))
        {
            pLsaInfo->pArea->u4IndicationLsaCount--;
        }
    }

    /* Clear the LSA Descriptor.
     * This will delete the minimum LSA interval timer. */
    if (pLsaInfo->pLsaDesc != NULL)
    {
        LsuClearLsaDesc (pLsaInfo->pLsaDesc);
        pLsaInfo->pLsaDesc = NULL;
    }
    /* Clear the LSA. This will delete
     * lsaAgingTimer and dnaLsaSplAgingTimer */
    LsuClearLsaInfo (pLsaInfo);
    pLsaInfo = NULL;

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: GrDeleteLsaFromDatabase\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrRtDeleteAllInCxt                                         */
/*                                                                           */
/* Description  : This routine deletes all the routes from OSPF              */
/*                trie and SLL. It does not give an indication to RTM        */
/*                regarding the route deletion                               */
/*                                                                           */
/* Input        : pOspfRt - pointer to Routing table                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrRtDeleteAllInCxt (tOspfRt * pOspfRt)
{
    tInputParams        inParams;    /* Trie input parameters */
    tOutputParams       outParams;    /* Trie output parameters */
    tRtEntry           *pRtEntry = NULL;    /* pointer to Route entry */
    UINT4               au4Indx[2];    /* Index for Trie search */
    UINT4               u4NextHop = 0;    /* Next hop */

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrRtDeleteAllInCxt\r\n");

    MEMSET (&inParams, 0, sizeof (tInputParams));
    MEMSET (&outParams, 0, sizeof (tOutputParams));

    while ((pRtEntry = (tRtEntry *) TMO_SLL_First (&(pOspfRt->routesList)))
           != NULL)
    {
        if (IS_DEST_NETWORK (pRtEntry))
        {
            /* Remove from trie and SLL */
            au4Indx[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId));
            au4Indx[1] = CRU_HTONL (pRtEntry->u4IpAddrMask);
            inParams.i1AppId = pOspfRt->i1OspfId;
            inParams.pRoot = pOspfRt->pOspfRoot;
            inParams.Key.pKey = (UINT1 *) au4Indx;
            inParams.u1Reserved1 = (UINT1) pRtEntry->u2RtType;

            outParams.pAppSpecInfo = NULL;
            outParams.Key.pKey = NULL;

            u4NextHop = pRtEntry->u4NxtHop;
            /* Delete the instance from the trie */
            if (TrieRemove (&inParams, &outParams, &u4NextHop) == TRIE_SUCCESS)
            {
                OSPF_TRC3 (CONTROL_PLANE_TRC, (UINT4) pOspfRt->i1OspfId,
                           "Dest Ip Addr = %x, AddrMask = %x, Next Hop = %d\n",
                           pRtEntry->destId, pRtEntry->u4IpAddrMask, u4NextHop);
                /* Delete the instance from the SLL */
                TMO_SLL_Delete (&(pOspfRt->routesList),
                                &(pRtEntry->nextRtEntryNode));

                /* Delete the path and clear the memory */
                RtcFreeRtEntry (pRtEntry);
                pRtEntry = NULL;
            }
        }
        else
        {
            /* Delete only from SLL */
            TMO_SLL_Delete (&(pOspfRt->routesList),
                            &(pRtEntry->nextRtEntryNode));
            RtcFreeRtEntry (pRtEntry);
            pRtEntry = NULL;
        }
    }
    TMO_SLL_Init (&(pOspfRt->routesList));

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrRtDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrInterfaceDeleteAllInCxt                                  */
/*                                                                           */
/* Description  : This routine deletes all the interfaces attached           */
/*                to this instance. The deletion of each interface in turn   */
/*                deletes all the neighbors attached                         */
/*                                                                           */
/* Input        : pOspfCxt - pointer to OSPF context                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrInterfaceDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tTMO_SLL_NODE      *pLstNode = NULL;    /* SLL node for interface */
    tInterface         *pInterface = NULL;    /* pointer to interface */

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrInterfaceDeleteAllInCxt\r\n");

    while ((pLstNode = TMO_SLL_First (&(pOspfCxt->sortIfLst))) != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        GrInterfaceDelete (pInterface);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrInterfaceDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrInterfaceDisable                                         */
/*                                                                           */
/* Description  : This routine disables the interface, all the neighbors     */
/*                attached to this interface                                 */
/*                                                                           */
/* Input        : pInterface  -  pointer to the interface                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrInterfaceDisable (tInterface * pInterface)
{
    tNeighbor          *pNbr = NULL;    /* pointer to neighbor */
    tTMO_SLL_NODE      *pNbrNode = NULL;    /* Neighbor SLL node */
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pInterface->pArea->pOspfCxt->u4OspfCxtId;
    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: GrInterfaceDisable\r\n");

    if ((pInterface->u1IsmState == IFS_DR) ||
        (pInterface->u1IsmState == IFS_BACKUP))
    {
        IpifLeaveMcastGroup (&gAllDRtrs, pInterface);
    }

    if (pInterface->u1IsmState != IFS_DOWN)
    {
        IpifLeaveMcastGroup (&gAllSpfRtrs, pInterface);
    }
    pInterface->u1IsmState = IFS_DOWN;

    /* Delete the hello, poll and wait timers */
    IsmDisableIfTimers (pInterface);
    /* Reset the variables. Clear ack packets and delete ack timers.
     * Clear update packets */
    IsmResetVariables (pInterface);

    /* Scan and delete all the neighbors */
    while ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        /* Delete the neighbor and clear the memory. This deletes
         * the inactivity timer, clears the request list and deletes
         * the request timer, clears the update packets and deletes
         * the rxmt timer, clears the ddp packets and deletes the
         *  */
        pNbr->bIsBfdDisable = OSIX_TRUE;
        NbrDelete (pNbr);
        pNbr = NULL;
    }

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "FUNC: GrInterfaceDisable\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrInterfaceDelete                                          */
/*                                                                           */
/* Description  : This routine deletes the interface, all the neighbors      */
/*                attached to this interface and clears the memory           */
/*                                                                           */
/* Input        : pInterface  -  pointer to the interface                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrInterfaceDelete (tInterface * pInterface)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;    /* MD5 auth key node */
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pInterface->pArea->pOspfCxt->u4OspfCxtId;
    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: GrInterfaceDelete\r\n");

    /* Disable the interface */
    GrInterfaceDisable (pInterface);

    /* Delete the interface from all the data strucutres */
    TMO_SLL_Delete (&(pInterface->pArea->ifsInArea),
                    &(pInterface->nextIfInArea));

    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        TMO_SLL_Delete (&(pInterface->pArea->pOspfCxt->virtIfLst),
                        &(pInterface->nextSortIf));
    }
    else
    {
        TMO_HASH_Delete_Node (gOsRtr.pIfHashTable,
                              &(pInterface->nextIfNode),
                              IF_HASH_FN (pInterface->u4IfIndex));
        TMO_SLL_Delete (&(pInterface->pArea->pOspfCxt->sortIfLst),
                        &(pInterface->nextSortIf));
    }

    while ((pAuthkeyInfo = (tMd5AuthkeyInfo *) TMO_SLL_First
            (&(pInterface->sortMd5authkeyLst))) != NULL)
    {
        TMO_SLL_Delete (&(pInterface->sortMd5authkeyLst),
                        &(pAuthkeyInfo->nextSortKey));
        MD5AUTH_FREE (pAuthkeyInfo);
    }

    IF_FREE (pInterface);
    pInterface = NULL;

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: GrInterfaceDelete\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrAreaDeleteAllInCxt                                       */
/*                                                                           */
/* Description  : This routine deletes all the areas attached to             */
/*                this instance                                              */
/*                                                                           */
/* Input        : pOspfCxt - pointer to OSPF context                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrAreaDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;    /* pointer to Area */

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrAreaDeleteAllInCxt\r\n");

    while ((pArea = (tArea *) TMO_SLL_First (&(pOspfCxt->areasLst))) != NULL)
    {
        /* Delete the area */
        GrAreaDeleteInCxt (pArea);
    }

    /* To delete the back_bone area if the router is internal router */
    if (pOspfCxt->pBackbone != NULL)
    {
        GrAreaDeleteInCxt (pOspfCxt->pBackbone);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrAreaDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrAreaDeleteInCxt                                          */
/*                                                                           */
/* Description  : This routine deletes the particular area                   */
/*                                                                           */
/* Input        : pArea    - pointer to OSPF area                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrAreaDeleteInCxt (tArea * pArea)
{
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pArea->pOspfCxt->u4OspfCxtId;
    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: GrAreaDeleteInCxt\r\n");

    /* All the interfaces and LSA are already deleted from the database */
    /* Delete the neighbor table RB Tree */
    RBTreeDestroy (pArea->pNbrTbl, NULL, 0);
    /* Delete the LSA hash Table */
    TMO_HASH_Delete_Table (pArea->pLsaHashTable, NULL);
    /* Delete the are from the SLL */
    TMO_SLL_Delete (&(pArea->pOspfCxt->areasLst), &(pArea->nextArea));
    /* If the area is backbone, make the backbone pointer as NULL */
    if (UtilIpAddrComp (pArea->areaId, gBackboneAreaId) == OSPF_EQUAL)
    {
        pArea->pOspfCxt->pBackbone = NULL;
    }
    /* Clear the SPF Tree */
    RtcClearSpfTree (pArea->pSpf);
    /* Delete the NSSA stability interval */
    TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);
    /* Deleting External Route Aggregation Trie */
    AreaAsExtRtAggrTrieDelete (pArea);
    /* Clear the area memory */
    AREA_FREE (pArea);

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: GrAreaDeleteInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrTaskDeInitialise                                         */
/*                                                                           */
/* Description  : This routine clears all the memory                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrTaskDeInitialise (VOID)
{
    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrTaskDeInitialise\r\n");

#ifdef OPQAPSIM_WANTED
    oasDeInit ();
#endif

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
    UnRegisterOSPFMibs ();
#endif

    /* Initialize the temporary RTM Route List */
    TMO_SLL_Init (&(gOsRtr.RtmRtLst));

    /* Initialise the RTM route message */
    TMO_SLL_Init (&(gOsRtr.RtmRtLst));

#ifdef HIGH_PERF_RXMT_LST_WANTED
    RBTreeDelete (gOsRtr.pRxmtLstRBRoot);
    gOsRtr.pRxmtLstRBRoot = NULL;
#endif

    /* Delete all the queues */
    OsixDeleteQ (SELF, (const UINT1 *) "OSPQ");
    OsixDeleteQ (SELF, (const UINT1 *) "OLPQ");
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    OsixDeleteQ (SELF, (const UINT1 *) "OSPFLNXVRF");
#endif

    /* Stop all the global timers and delete the timer list */
    TmrStopTimer (gOnesecTimerLst, &(gOnesecTimerNode));
    TmrDeleteTimerList (gTimerLst);
    TmrDeleteTimerList (gOnesecTimerLst);
    TmrDeleteTimerList (gHelloTimerLst);

    /* Delete the interface hash table */
    TMO_HASH_Delete_Table (gOsRtr.pIfHashTable, NULL);

    /* Clear all the memory */
    OspfMemClear ();

    OspfUnLock ();
    /* Delete the semaphores */
    OsixSemDel (gOspfSemId);
    OsixSemDel (gOsRtr.RtmLstSemId);
    OsixDeleteSem (OSIX_TRUE, (const UINT1 *) "RTSM");
    OsixDeleteSem (OSIX_TRUE, (const UINT1 *) "CXTSM");

    /* Clear the global structures */
    MEMSET (&gOsRtr, 0, sizeof (tOsRtr));
    MEMSET (&gBufQid, 0, sizeof (tBufQid));
    /* Delete the OSPF Task */
    OsixDeleteTask (SELF, (const UINT1 *) "OSPF");
    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrTaskDeInitialise\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrReStoreGlobalRestartInfo                                 */
/*                                                                           */
/* Description  : This routine restores all Global GR specific configuration */
/*                from non-volatile storage.                                 */
/*                                                                           */
/*                  1. GR shutdown time                                      */
/*                  2. 1s timer value                                        */
/*                                                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
GrRestoreGlobalRestartInfo (VOID)
{
#define MAX_COLUMN_LENGTH 80
    tUtlTm              utlTm;    /* Used to get the time */
    UINT4               u4SysTime = 0;    /* Current system time */
    UINT4               u4GrDowntime = 0;    /* System time during GR shut */
    UINT4               u4GrDown1sTimer = 0;    /* 1s value during GR shut */
    INT1                ai1Buf[MAX_COLUMN_LENGTH + OSPF_ONE];
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_COLUMN_LENGTH + 1];
    /* Stores line identification */
    UINT1               au1EqualStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the string "=" */
    UINT1               au1ValueStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the value for each
     * identification */
    INT4                i4FileFd = OSPF_ZERO;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrReStoreGlobalRestartInfo\r\n");

    MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    i4FileFd = FileOpen (OSPF_GR_CONF, OSIX_FILE_RO);

    if (i4FileFd < OSPF_ZERO)
    {
        /* File Creation failed */
        return;
    }

    while (FsUtlReadLine (i4FileFd, ai1Buf) == OSIX_SUCCESS)
    {
        MEMSET (au1NameStr, 0, sizeof (au1NameStr));
        MEMSET (au1EqualStr, 0, sizeof (au1EqualStr));
        MEMSET (au1ValueStr, 0, sizeof (au1ValueStr));

        if ((STRCMP (ai1Buf, "")) != 0)
        {
            SSCANF ((char *) ai1Buf, "%80s%80s%80s", au1NameStr, au1EqualStr,
                    au1ValueStr);

            au1ValueStr[MAX_COLUMN_LENGTH] = '\0';
            if (STRLEN (au1ValueStr) == 0)
            {
                continue;
            }
            /* System shutdown time and 1s value needs to be restored
             * during task creation. */
            if (STRCMP (au1NameStr, "SHUT_SYS_TIME") == 0)
            {
                u4GrDowntime = (UINT4) ATOI (au1ValueStr);
            }
            if (STRCMP (au1NameStr, "1s_TIMER_VALUE") == 0)
            {
                u4GrDown1sTimer = (UINT4) ATOI (au1ValueStr);

                /* Store the 1s timer and current system time */
                MEMSET (&utlTm, 0, sizeof (tUtlTm));
                UtlGetTime (&utlTm);
                /* Get the seconds since the base year (2000) */
                u4SysTime = GrGetSecondsSinceBase (utlTm);

                /* u4GrDowntime contains the system time during
                 * GR shutdown
                 * u4GrDown1sTimer contains the value of gOspf1sTimer
                 * during GR shutdown
                 * u4SysTime contains the current system time
                 * Current gOspf1sTimer is obtained by adding the
                 * difference between the two system time with the old
                 * gOspf1sTimer value */
                if (u4GrDowntime != 0)
                {
                    gOspf1sTimer = u4GrDown1sTimer + (u4SysTime - u4GrDowntime);
                }
                break;
            }
        }
        MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    }

    FileClose (i4FileFd);
    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT: GrReStoreGlobalRestartInfo\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrReStoreCxtRestartInfo                                    */
/*                                                                           */
/* Description  : This routine restores all Context specific GR              */
/*                configurations from non-volatile storage.                  */
/*                                                                           */
/*                  1. Context name                                          */
/*                  2. GR status                                             */
/*                  3. Restart reason                                        */
/*                  4. Last attempted GR exit reason                         */
/*                  5. Absolute time at which GR terminates                  */
/*                                                                           */
/*                                                                           */
/* Input        : pOspfCxt     -    pointer to OSPF context                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
GrRestoreCxtRestartInfo (tOspfCxt * pOspfCxt)
{
#define MAX_COLUMN_LENGTH 80
    tUtlTm              utlTm;    /* Used to get the time */
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4SysTime = 0;    /* Current system time */
    UINT4               u4GraceEndTime = 0;    /* Absolute time at which GR
                                             * exits */
    INT1                ai1Buf[MAX_COLUMN_LENGTH + OSPF_ONE];
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_COLUMN_LENGTH + 1];
    /* Stores line identification */
    UINT1               au1EqualStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the string "=" */
    UINT1               au1ValueStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the value for each
     * identification */
    INT4                i4FileFd = OSPF_ZERO;
    UINT4               u4IpAddr = OSPF_ZERO;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrReStoreCxtRestartInfo\r\n");

    MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    i4FileFd = FileOpen (OSPF_GR_CONF, OSIX_FILE_RO);

    if (i4FileFd < OSPF_ZERO)
    {
        /* File Creation failed */
        return;
    }

    while (FsUtlReadLine (i4FileFd, ai1Buf) == OSIX_SUCCESS)
    {
        MEMSET (au1NameStr, 0, sizeof (au1NameStr));
        MEMSET (au1EqualStr, 0, sizeof (au1EqualStr));
        MEMSET (au1ValueStr, 0, sizeof (au1ValueStr));

        if ((STRCMP (ai1Buf, "")) != 0)
        {
            SSCANF ((char *) ai1Buf, "%80s%80s%80s", au1NameStr, au1EqualStr,
                    au1ValueStr);

            au1ValueStr[MAX_COLUMN_LENGTH] = '\0';
            if (STRLEN (au1ValueStr) == 0)
            {
                continue;
            }

            if (STRCMP (au1NameStr, "INTERFACE_IP") == 0)
            {
                /* All context information has been scanned */
                break;
            }

            if (STRCMP (au1NameStr, "CONTEXT") == 0)
            {
                u4OspfCxtId = OSPF_INVALID_CXT_ID;
                UtilOspfIsVcmSwitchExist (au1ValueStr, &u4OspfCxtId);
            }
            if (STRCMP (au1NameStr, "STATUS") == 0)
            {
                if (u4OspfCxtId == pOspfCxt->u4OspfCxtId)
                {
                    if (STRCMP (au1ValueStr, "planned") == 0)
                    {
                        pOspfCxt->u1RestartStatus = OSPF_RESTART_PLANNED;
                        pOspfCxt->u1RestartExitReason = OSPF_RESTART_INPROGRESS;
                        pOspfCxt->u1OspfRestartState = OSPF_GR_RESTART;
                        pOspfCxt->u1PrevOspfRestartState = OSPF_GR_SHUTDOWN;
                    }
                    if (STRCMP (au1ValueStr, "unplanned") == 0)
                    {
                        pOspfCxt->u1RestartStatus = OSPF_RESTART_UNPLANNED;
                        pOspfCxt->u1RestartExitReason = OSPF_RESTART_INPROGRESS;
                        pOspfCxt->u1OspfRestartState = OSPF_GR_RESTART;
                        pOspfCxt->u1PrevOspfRestartState = OSPF_GR_SHUTDOWN;
                    }
                    if (STRCMP (au1ValueStr, "none") == 0)
                    {
                        pOspfCxt->u1RestartStatus = OSPF_RESTART_NONE;
                        pOspfCxt->u1OspfRestartState = OSPF_GR_NONE;
                        pOspfCxt->u1PrevOspfRestartState = OSPF_GR_SHUTDOWN;
                    }
                }
            }
            if (STRCMP (au1NameStr, "GR_REASON") == 0)
            {
                if (u4OspfCxtId == pOspfCxt->u4OspfCxtId)
                {
                    if (STRCMP (au1ValueStr, "unknown") == 0)
                    {
                        pOspfCxt->u1RestartReason = OSPF_GR_UNKNOWN;
                    }
                    if (STRCMP (au1ValueStr, "restart") == 0)
                    {
                        pOspfCxt->u1RestartReason = OSPF_GR_SW_RESTART;
                    }
                    if (STRCMP (au1ValueStr, "upgrade") == 0)
                    {
                        pOspfCxt->u1RestartReason = OSPF_GR_SW_UPGRADE;
                    }
                    if (STRCMP (au1ValueStr, "redundancy") == 0)
                    {
                        pOspfCxt->u1RestartReason = OSPF_GR_SW_RED;
                    }
                }
            }
            if (STRCMP (au1NameStr, "GR_EXIT_REASON") == 0)
            {
                if ((u4OspfCxtId == pOspfCxt->u4OspfCxtId) &&
                    (pOspfCxt->u1RestartExitReason != OSPF_RESTART_INPROGRESS))
                {
                    if (STRCMP (au1ValueStr, "none") == 0)
                    {
                        pOspfCxt->u1RestartExitReason = OSPF_RESTART_NONE;
                    }
                    if (STRCMP (au1ValueStr, "completed") == 0)
                    {
                        pOspfCxt->u1RestartExitReason = OSPF_RESTART_COMPLETED;
                    }
                    if (STRCMP (au1ValueStr, "timedout") == 0)
                    {
                        pOspfCxt->u1RestartExitReason = OSPF_RESTART_TIMEDOUT;
                    }
                    if (STRCMP (au1ValueStr, "topchg") == 0)
                    {
                        pOspfCxt->u1RestartExitReason = OSPF_RESTART_TOP_CHG;
                    }
                }
            }
            if (STRCMP (au1NameStr, "GR_END_TIME") == 0)
            {
                if (u4OspfCxtId == pOspfCxt->u4OspfCxtId)
                {
                    u4GraceEndTime = ATOI (au1ValueStr);

                    /* Store the 1s timer and current system time */
                    MEMSET (&utlTm, 0, sizeof (tUtlTm));
                    UtlGetTime (&utlTm);
                    /* Get the seconds since the base year (2000) */
                    u4SysTime = GrGetSecondsSinceBase (utlTm);

                    /* u4SysTime contains current system time
                     * u4GraceEndTime contains the graceful restart end
                     * time
                     * Start the grace timer with the remaining time */
                    if (u4GraceEndTime <= u4SysTime)
                    {
                        GrExitGracefultRestart (pOspfCxt,
                                                OSPF_RESTART_TIMEDOUT);
                        continue;
                    }
                    else if ((u4GraceEndTime > u4SysTime) &&
                             (pOspfCxt->u1RestartExitReason ==
                              OSPF_RESTART_INPROGRESS))
                    {
                        TmrSetTimer (&(pOspfCxt->graceTimer),
                                     RESTART_GRACE_TIMER,
                                     NO_OF_TICKS_PER_SEC *
                                     (u4GraceEndTime - u4SysTime));

                    }
                }
            }
            if (STRCMP (au1NameStr, "ROUTERID") == 0)
            {
                if (u4OspfCxtId == pOspfCxt->u4OspfCxtId)
                {
                    MEMSET (pOspfCxt->rtrId, 0, MAX_IP_ADDR_LEN);
                    u4IpAddr = 0;

                    u4IpAddr = OSIX_NTOHL (INET_ADDR (au1ValueStr));
                    OSPF_CRU_BMC_DWTOPDU (pOspfCxt->rtrId, u4IpAddr);
                }
            }
            if (STRCMP (au1NameStr, "ROUTERID_STATUS") == 0)
            {
                if ((u4OspfCxtId == pOspfCxt->u4OspfCxtId) &&
                    (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
                {
                    pOspfCxt->i4RouterIdStatus = ATOI (au1ValueStr);
                }
            }
        }
        MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    }

    FileClose (i4FileFd);
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrReStoreCxtRestartInfo\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrReStoreIntfRestartInfo                                   */
/*                                                                           */
/* Description  : This routine restores all Interface specifu GR             */
/*                configurations from non-volatile storage.                  */
/*                                                                           */
/*                  1. IP address of each interface                          */
/*                  3. Address If Index of each interface                    */
/*                  3. Cryptographic sequence number of each interface       */
/*                                                                           */
/* Input        : pInterface   -    pointer to Interface                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
GrRestoreIntfRestartInfo (tInterface * pInterface, tLSASEQNUM * seqNum)
{
    tIPADDR             ipaddr;    /* ip address stored in file */
    UINT4               u4IpAddr = 0;    /* UINT4 form of ipaddr */
    UINT4               u4AddresslessIf = 0;    /* addressless if stored
                                                 * in file */
    UINT4               u4CryptSeqNum = 0;    /* Crpto seq num store
                                             * in file */
    INT1                ai1Buf[MAX_COLUMN_LENGTH + OSPF_ONE];
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_COLUMN_LENGTH + 1];
    /* Stores line identification */
    UINT1               au1EqualStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the string "=" */
    UINT1               au1ValueStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the value for each
     * identification */
    INT4                i4FileFd = OSPF_ZERO;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: GrReStoreIntfRestartInfo\r\n");

    MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    i4FileFd = FileOpen (OSPF_GR_CONF, OSIX_FILE_RO);

    if (i4FileFd < OSPF_ZERO)
    {
        /* File Creation failed */
        return;
    }

    while (FsUtlReadLine (i4FileFd, ai1Buf) == OSIX_SUCCESS)
    {
        MEMSET (au1NameStr, 0, sizeof (au1NameStr));
        MEMSET (au1EqualStr, 0, sizeof (au1EqualStr));
        MEMSET (au1ValueStr, 0, sizeof (au1ValueStr));

        if ((STRCMP (ai1Buf, "")) != 0)
        {
            SSCANF ((char *) ai1Buf, "%80s%80s%80s", au1NameStr, au1EqualStr,
                    au1ValueStr);

            au1ValueStr[MAX_COLUMN_LENGTH] = '\0';
            if (STRLEN (au1ValueStr) == 0)
            {
                continue;
            }

            if (STRCMP (au1NameStr, "INTERFACE_IP") == 0)
            {
                MEMSET (ipaddr, 0, MAX_IP_ADDR_LEN);
                u4IpAddr = 0;

                u4IpAddr = OSIX_NTOHL (INET_ADDR (au1ValueStr));
                OSPF_CRU_BMC_DWTOPDU (ipaddr, u4IpAddr);
            }
            if (STRCMP (au1NameStr, "INTERFACE_UNNUM") == 0)
            {
                u4AddresslessIf = 0;
                u4AddresslessIf = ATOI (au1ValueStr);
            }
            if (STRCMP (au1NameStr, "CRYPTO_SEQ_NUM") == 0)
            {
                u4CryptSeqNum = 0;
                u4CryptSeqNum = ATOI (au1ValueStr);

                if ((UtilIpAddrComp (ipaddr, pInterface->ifIpAddr)
                     == OSPF_EQUAL) &&
                    (u4AddresslessIf == pInterface->u4AddrlessIf))
                {
                    pInterface->u4CryptSeqNum = u4CryptSeqNum;
                }
            }
            if (STRCMP (au1NameStr, "GRACE_SEQ_NUM") == 0)
            {
                u4CryptSeqNum = 0;
                u4CryptSeqNum = ATOI (au1ValueStr);

                if ((UtilIpAddrComp (ipaddr, pInterface->ifIpAddr)
                     == OSPF_EQUAL) &&
                    (u4AddresslessIf == pInterface->u4AddrlessIf))
                {
                    *seqNum = u4CryptSeqNum;
                }
            }
        }
        MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    }

    FileClose (i4FileFd);
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: GrReStoreIntfRestartInfo\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrGetSecondsSinceBase                                      */
/*                                                                           */
/* Description  : This routine calculates the seconds from the obtained      */
/*                time. The seconds is calculated from the BASE year (2000)  */
/*                                                                           */
/* Input        : tUtlTm   -   Time format structure                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : The calculated time in seconds                             */
/*                                                                           */
/*****************************************************************************/

UINT4
GrGetSecondsSinceBase (tUtlTm utlTm)
{
    UINT4               u4year = utlTm.tm_year;
    UINT4               u4Secs = 0;

    --u4year;
    while (u4year >= TM_BASE_YEAR)
    {
        u4Secs += SECS_IN_YEAR (u4year);
        --u4year;
    }
    u4Secs += (utlTm.tm_yday * 86400);
    u4Secs += (utlTm.tm_hour * 3600);
    u4Secs += (utlTm.tm_min * 60);
    u4Secs += (utlTm.tm_sec);

    return u4Secs;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrExitGracefultRestart                                     */
/*                                                                           */
/* Description  : This routine does the actions required on exiting          */
/*                Graceful restart                                           */
/*                RFC 3623 Section 2.3                                       */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to OSPF context                     */
/*                u1ExitReason - Reasong for Graceful restart exit           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrExitGracefultRestart (tOspfCxt * pOspfCxt, UINT1 u1ExitReason)
{
    tArea              *pArea = NULL;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tRtmRegnId          RegnId;
    UINT1               u1AbrFlag;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrExitGracefultRestart\r\n");

    /* Stop the grace timer */
    TmrStopTimer (gTimerLst, &(pOspfCxt->graceTimer.timerNode));
    /* Copy the registration id */
    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
    RegnId.u2ProtoId = OSPF_ID;
    RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;

    pOspfCxt->u1OspfRestartState = OSPF_GR_NONE;
    pOspfCxt->u1PrevOspfRestartState = OSPF_GR_RESTART;
    pOspfCxt->u1RestartStatus = OSPF_RESTART_NONE;

    /* Send trap notification reagrding the status change.
     * Nbr parameter is required for Neighbor status change
     * when the router is acting as helper */
    SnmpSendStatusChgTrapInCxt (pOspfCxt, NULL, RST_STATUS_CHANGE_TRAP);

    u1AbrFlag = RtrCheckABRStatInCxt (pOspfCxt);
    pOspfCxt->bAreaBdrRtr = u1AbrFlag;

    /* Step 1 */
    /* Re-originate router LSA for all attached areas */
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pLstNode, tTMO_SLL_NODE *)
    {
        pArea = (tArea *) pLstNode;
        /* Generate router LSA for this area */
        OlsGenerateLsa (pArea, ROUTER_LSA, &(pOspfCxt->rtrId), (UINT1 *) pArea);
    }

    /* Step 2 */
    /* Re-originate network LSA */
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        /* Generate the network LSA if the interface is DR */
        if (IS_DR (pInterface))
        {
            OlsGenerateLsa (pInterface->pArea, NETWORK_LSA,
                            &(pInterface->ifIpAddr), (UINT1 *) pInterface);
        }
    }

    TmrDeleteTimer (&(pOspfCxt->runRtTimer));
    /* Step 3 */
    /* Delete all the routes from OSPF */
    GrRtDeleteAllInCxt (pOspfCxt->pOspfRt);

    /* Schedule route calculation */
    pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;
    pOspfCxt->u1AsbSummaryLsaChanged = OSPF_FALSE;
    RtcCalculateRtInCxt (pOspfCxt);
    OsixGetSysTime (&(pOspfCxt->u4LastCalTime));

    /* Generate Type 5 and Type 7 LSA */
    ExtrtLsaPolicyHandlerInCxt (pOspfCxt);

    /* Step 4 */
    /* Delete all the invalid routes in RTM */
    RtmProcessGRRouteCleanUp (&RegnId, IP_SUCCESS);
    pOspfCxt->u1RestartExitReason = u1ExitReason;

    /* Step 5 */
    /* Flush all unwanted self originated LSA */
    GrFlushUnwantedSelfLsaInCxt (pOspfCxt);

    /* Step 6 */
    /* Flush the Grace LSA */
    GrFlushGraceLsaInCxt (pOspfCxt);

    /*Remove the GR conf file */
    FileDelete (OSPF_GR_CONF);

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "FUNC: GrExitGracefultRestart\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrFlushUnwantedSelfLsaInCxt                                */
/*                                                                           */
/* Description  : This routine flushes all the unwanted self                  */
/*                originated LSA in context                                  */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrFlushUnwantedSelfLsaInCxt (tOspfCxt * pOspfCxt)
{
    tLsaDesc           *pLsaDesc = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrFlushUnwantedSelfLsaInCxt\r\n");

    /* The GR LSA bit in each of the self-orignated LSA will be set
     * if the LSA is not re-originated. Remove those LSA and flush them from
     * the routing domain */
    TMO_DLL_Scan (&(pOspfCxt->origLsaDescLst), pLstNode, tTMO_DLL_NODE *)
    {
        pLsaDesc = GET_LSADESC_FROM_LSADESCLST (pLstNode);
        if (pLsaDesc->pLsaInfo != NULL)
        {
            if (IS_LSA_REFRESH_BIT_SET (pLsaDesc->pLsaInfo, OSPF_GR_LSA_BIT))
            {
                AgdFlushOut (pLsaDesc->pLsaInfo);
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrFlushUnwantedSelfLsaInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrFlushGraceLsaInCxt                                       */
/*                                                                           */
/* Description  : This routine flushes the grace LSA                          */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrFlushGraceLsaInCxt (tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOpqLSAInfo         opqLSAInfo;
    tOpqLSAInfo         opqLSANewInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrFlushGraceLsaInCxt\r\n");

    MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));
    MEMSET (&opqLSANewInfo, 0, sizeof (tOpqLSAInfo));

    /* Scan all the interfaces attached. Get the Grace opaque LSA
     * and call OpqAppToOpqModuleSendInCxt by setting status as FLUSHED_LSA
     * to flush the LSA through that interface */
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        /* Clear the tOpqLSAInfo structure */
        MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));
        MEMSET (&opqLSANewInfo, 0, sizeof (tOpqLSAInfo));

        if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_COMPLETED)
        {
            /* Construct the Grace LSA packet */
            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSAInfo,
                                          FLUSHED_LSA)) == OSPF_FAILURE)
            {
                /* Contruct GRace LSA failed. Scan the next interface */
                continue;
            }
        }
        else
        {
            /* Construct the Grace LSA packet */
            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSAInfo,
                                          NEW_LSA)) == OSPF_FAILURE)
            {
                /* Contruct GRace LSA failed. Scan the next interface */
                continue;
            }
            /* Construct the Grace LSA packet */
            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSANewInfo,
                                          FLUSHED_LSA)) == OSPF_FAILURE)
            {
                /* Contruct GRace LSA failed. Scan the next interface */
                /* Clear LSA allocated in GrUtilConstructGraceLSA */
                LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
                opqLSAInfo.pu1LSA = NULL;
                continue;
            }

        }

        /* Transmit the Grace LSA  and set the Grace LSA sent flag
         * to true */
        OpqAppToOpqModuleSendInCxt (pOspfCxt->u4OspfCxtId, &opqLSAInfo);
        /* Clear LSA allocated in GrUtilConstructGraceLSA */
        LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
        opqLSAInfo.pu1LSA = NULL;

        if (pOspfCxt->u1RestartExitReason != OSPF_RESTART_COMPLETED)
        {
            /* Transmit the Grace LSA  and set the Grace LSA sent flag
             * to true */
            OpqAppToOpqModuleSendInCxt (pOspfCxt->u4OspfCxtId, &opqLSANewInfo);
            /* Clear LSA allocated in GrUtilConstructGraceLSA */
            LSA_FREE (TYPE10_OPQ_LSA, opqLSANewInfo.pu1LSA);
            opqLSANewInfo.pu1LSA = NULL;

        }
    }

    /* Flush the Grace LSA through all the attached virtual links */
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));

        if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_COMPLETED)
        {
            /* Construct the Grace LSA packet */
            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSAInfo,
                                          FLUSHED_LSA)) == OSPF_FAILURE)
            {
                /* Contruct GRace LSA failed. Scan the next interface */
                continue;
            }
        }
        else
        {
            /* Construct the Grace LSA packet */
            if ((GrUtilConstructGraceLSA (pOspfCxt, pInterface, &opqLSAInfo,
                                          NEW_LSA)) == OSPF_FAILURE)
            {
                /* Contruct GRace LSA failed. Scan the next interface */
                continue;
            }

        }
        OpqAppToOpqModuleSendInCxt (pOspfCxt->u4OspfCxtId, &opqLSAInfo);
        LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
        opqLSAInfo.pu1LSA = NULL;
    }
    if (opqLSAInfo.pu1LSA != NULL)
    {
        LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrFlushGraceLsaInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuCheckLsaConsistency                                   */
/*                                                                           */
/* Description  : This module is called when the restarting router receives  */
/*                a LSA from the neighor. The module checks whether the      */
/*                neighbor.s LSA is consistent with restarting router's      */
/*                pre-restart LSA. When inconsistency is detected, the       */
/*                router exits restart mode.                                 */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                pArea        - Pointer to Area                             */
/*                pLsHeader    - LSA header                                  */
/*                pLsa         - pointer to LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE     -     if LSA is inconsistent                 */
/*                OSPF_FALSE    -     if LSA is not inconsistent             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrLsuCheckLsaConsistency (tOspfCxt * pOspfCxt, tArea * pArea,
                          tLsHeader * pLsHeader, UINT1 *pLsa)
{
    INT4                i4LsaInconsistent = OSPF_FALSE;
    UINT1               u1LsaType = pLsHeader->u1LsaType;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuCheckLsaConsistency\r\n");

    if ((u1LsaType != ROUTER_LSA) && (u1LsaType != NETWORK_LSA))
    {
        /* Only router and network LSA are considered for LSA
         * inconsistency check */
        return OSPF_FALSE;
    }

    /* Only self-originated and neighbor originated LSAs are considered */
    if (LsuIsSelfOriginatedLsaInCxt (pOspfCxt, pLsHeader) == OSPF_TRUE)
    {
        i4LsaInconsistent = GrLsuCheckSelfOrigLsaInCxt (pOspfCxt, pArea,
                                                        pLsHeader, pLsa);
    }
    else if (GrLsuIsNbrOrigLsaInCxt (pOspfCxt, pLsHeader) == OSPF_TRUE)
    {
        i4LsaInconsistent = GrLsuCheckNbrLsaInCxt (pOspfCxt, pArea, pLsHeader,
                                                   pLsa);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuCheckLsaConsistency\r\n");
    return i4LsaInconsistent;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuCheckSelfOrigLsaInCxt                                 */
/*                                                                           */
/* Description  : This routine checks for LSA inconsistency if the           */
/*                received LSA is self-originated LSA                        */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                pArea        - Pointer to Area                             */
/*                pLsHeader    - LSA header                                  */
/*                pLsa         - pointer to LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE     -     if LSA is inconsistent                 */
/*                OSPF_FALSE    -     if LSA is not inconsistent             */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
GrLsuCheckSelfOrigLsaInCxt (tOspfCxt * pOspfCxt, tArea * pArea,
                            tLsHeader * pLsHeader, UINT1 *pLsa)
{
    tRtrLsaRtInfo       rtrLsaRtInfo;    /* Contains the particular link
                                         * information from received
                                         * self-originated router LSA */
    tRtrLsaRtInfo       rtrLsaSearchRtrInfo;    /* Contains the link info to be
                                                 * searched from Nbr router LSA */
    tInterface         *pInterface = NULL;    /* pointer to interface */
    tNeighbor          *pNeighbor = NULL;    /* pointer to neighbor */
    tLsaInfo           *pLsaInfo = NULL;    /* pointer to Nbr router LSA */
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tIPADDR             attachedRtrId;    /* Nbr id from network LSA */
    tRouterId           NetDRRtrId;
    tRouterId           NetBDRRtrId;
    UINT2               u2LinkCount = 0;    /* Router LSA link count */
    UINT2               u2CurrLink = 0;    /* Current router LSA link */
    UINT1              *pu1CurrPtr = NULL;    /* pointer to scan LSA */
    UINT1               u1LsaType = pLsHeader->u1LsaType;
    UINT1               u1DRFound = OSPF_FALSE;
    UINT1               u1BDRFound = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuCheckSelfOrigLsaInCxt\r\n");

    if (u1LsaType == ROUTER_LSA)
    {
        /* If self-originated router LSA is received, then router LSA
         * originated by each neighbor in that interface should have a back
         * link (Transit or p2p) */

        /* Skip the LSA header, options and unused fields */
        pu1CurrPtr = pLsa + LS_HEADER_SIZE + 2;

        /* Get the numner of links and scan all the links */
        u2LinkCount = LGET2BYTE (pu1CurrPtr);
        for (u2CurrLink = 0; u2CurrLink < u2LinkCount; u2CurrLink++)
        {
            MEMSET (&rtrLsaRtInfo, 0, sizeof (tRtrLsaRtInfo));
            GrLsuGetLinksFromLsa (pu1CurrPtr, &rtrLsaRtInfo);
            /*Increment the Pointer for taking next link */
            pu1CurrPtr = pu1CurrPtr + LSA_SIZE;

            if (rtrLsaRtInfo.u1Type == STUB_LINK)
            {
                /* Stub links need not be considered for inconsistency
                 * check */
                continue;
            }

            if (rtrLsaRtInfo.u1Type == TRANSIT_LINK)
            {
                TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
                {
                    pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

                    if (IS_TRANSIT_NETWORK (pInterface) &&
                        (OSPF_CRU_BMC_DWFROMPDU ((pInterface->ifIpAddr))
                         == rtrLsaRtInfo.u4LinkData))
                    {
                        break;
                    }
                }

                if (pLstNode == NULL)
                {
                    /* Interface not available. Inconsistent LSA */
                    OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                                   "Inconsistent LSA"
                                   " Interface 0x%x not found in context\r\n",
                                   rtrLsaRtInfo.u4LinkData);
                    return OSPF_TRUE;
                }

                TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                    /* Search the database for the router LSA of neighbor */
                    pLsaInfo =
                        LsuSearchDatabase (ROUTER_LSA, &(pNeighbor->nbrId),
                                           &(pNeighbor->nbrId), NULL,
                                           (UINT1 *) pArea);
                    if (pLsaInfo == NULL)
                    {
                        continue;
                    }

                    /* A link with type = transit, link id = DR ip address and
                     * link data = nbr ip address should exist. Else LSA is
                     * inconsistent */
                    MEMSET (&rtrLsaSearchRtrInfo, 0, sizeof (tRtrLsaRtInfo));
                    rtrLsaSearchRtrInfo.u4LinkId =
                        OSPF_CRU_BMC_DWFROMPDU ((pInterface->desgRtr));
                    rtrLsaSearchRtrInfo.u4LinkData =
                        OSPF_CRU_BMC_DWFROMPDU ((pNeighbor->nbrIpAddr));
                    rtrLsaSearchRtrInfo.u1Type = rtrLsaRtInfo.u1Type;

                    if (GrLsuSearchRtrLsa (pLsaInfo, &rtrLsaSearchRtrInfo)
                        == OSPF_FALSE)
                    {
                        return OSPF_TRUE;
                    }
                }
            }
            else
            {
                /* p2p or vitual link */
                /* In this case, the neighbor should also have a p2p or virtual
                 * link to the restarting router, else LSA is inconsistent */
                TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
                {
                    pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

                    if (!IS_TRANSIT_NETWORK (pInterface) &&
                        (OSPF_CRU_BMC_DWFROMPDU ((pInterface->ifIpAddr))
                         == rtrLsaRtInfo.u4LinkData))
                    {
                        break;
                    }
                }

                if (pLstNode == NULL)
                {
                    /* Interface not available. Inconsistent LSA */
                    if (pOspfCxt->u4OspfCxtId != 0)
                    {
                        OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                                       "Inconsistent LSA"
                                       " Interface 0x%x not found in context\r\n",
                                       rtrLsaRtInfo.u4LinkData);
                    }
                    return OSPF_TRUE;
                }

                if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf)))
                    == NULL)
                {
                    /* Neighbor not found */
                    return OSPF_FALSE;
                }

                pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                /* Search the database for the router LSA of neighbor */
                pLsaInfo = LsuSearchDatabase (ROUTER_LSA, &(pNeighbor->nbrId),
                                              &(pNeighbor->nbrId), NULL,
                                              (UINT1 *) pArea);
                if (pLsaInfo == NULL)
                {
                    continue;
                }

                /* A link with type = p2p/virtual, link id = restarting router-id
                 * and link data = nbr ip address should exist. Else LSA is
                 * inconsistent */
                MEMSET (&rtrLsaSearchRtrInfo, 0, sizeof (tRtrLsaRtInfo));
                rtrLsaSearchRtrInfo.u4LinkId =
                    OSPF_CRU_BMC_DWFROMPDU ((pOspfCxt->rtrId));
                rtrLsaSearchRtrInfo.u4LinkData =
                    OSPF_CRU_BMC_DWFROMPDU ((pNeighbor->nbrIpAddr));
                rtrLsaSearchRtrInfo.u1Type = rtrLsaRtInfo.u1Type;

                if (GrLsuSearchRtrLsa (pLsaInfo, &rtrLsaSearchRtrInfo)
                    == OSPF_FALSE)
                {
                    /* Inconsistent */
                    OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                                   "Inconsistent LSA "
                                   "Nbr 0x%x does not have p2p link to router\r\n",
                                   OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrId));
                    return OSPF_TRUE;
                }
            }
        }
    }
    else
    {
        /* If self-originated network LSA is received, then router LSA
         * originated by each router-id in the LSA should have a transit link
         * to the same network */

        /* Skip the LSA header and Mask field */
        pu1CurrPtr = pLsa + LS_HEADER_SIZE + MAX_IP_ADDR_LEN;

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            MEMSET (&NetDRRtrId, 0, sizeof (NetDRRtrId));
            MEMSET (&NetBDRRtrId, 0, sizeof (NetBDRRtrId));
            u1DRFound = OSPF_FALSE;
            u1BDRFound = OSPF_FALSE;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                if (UtilIpAddrComp (pNeighbor->nbrIpAddr, pInterface->desgRtr)
                    == OSPF_EQUAL)
                {
                    MEMCPY (&NetDRRtrId, pNeighbor->nbrId, sizeof (NetDRRtrId));
                    u1DRFound = OSPF_TRUE;
                }
                if (UtilIpAddrComp
                    (pNeighbor->nbrIpAddr,
                     pInterface->backupDesgRtr) == OSPF_EQUAL)
                {
                    MEMCPY (&NetBDRRtrId, pNeighbor->nbrId,
                            sizeof (NetBDRRtrId));
                    u1BDRFound = OSPF_TRUE;
                }
                if (u1DRFound == OSPF_TRUE && u1BDRFound == OSPF_TRUE)
                {
                    break;
                }
            }
            if ((UtilIpAddrComp (NetDRRtrId,
                                 pLsHeader->advRtrId) == OSPF_EQUAL) ||
                (UtilIpAddrComp (NetBDRRtrId,
                                 pLsHeader->advRtrId) == OSPF_EQUAL))
            {
                break;
            }
            else if (IS_TRANSIT_NETWORK (pInterface) &&
                     (UtilIpAddrMaskComp (pInterface->ifIpAddr,
                                          pLsHeader->linkStateId,
                                          pInterface->ifIpAddrMask) ==
                      OSPF_EQUAL))
            {
                break;
            }
        }

        if (pLstNode == NULL)
        {
            /* Advertised network LSA does not belong to any of
             * the Nbr. Hence LSA has to be discarded */
            OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                           "Discarded LSA "
                           "Advertised n/w LSA with network 0x%x has not come"
                           " from any Neighbor\r\n",
                           OSPF_CRU_BMC_DWFROMPDU (pLsHeader->linkStateId));
            return OSPF_FALSE;
        }

        while (pu1CurrPtr < (pLsa + pLsHeader->u2LsaLen))
        {
            MEMSET (&attachedRtrId, 0, MAX_IP_ADDR_LEN);
            LGETSTR (pu1CurrPtr, attachedRtrId, MAX_IP_ADDR_LEN);

            /* Scan all the neighbors in this interface to match the neighbor
             * id and get the corresponding neighbor ip address */
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                if (UtilIpAddrComp (pNeighbor->nbrId, attachedRtrId)
                    == OSPF_EQUAL)
                {
                    break;
                }
            }
            if (pNbrNode == NULL)
            {
                /* Neighbor is not found. The helper must have gone down
                 * during restart process. or hello packet is not yet received
                 * from that neighbor. So skip this inconsistancy check */
                OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                               "Inconsistent LSA "
                               "Attached neighbor id 0x%x not found in interface",
                               OSPF_CRU_BMC_DWFROMPDU (attachedRtrId));
                return OSPF_FALSE;
            }
            /* Search the database for the router LSA of attached router-id */
            pLsaInfo = LsuSearchDatabase (ROUTER_LSA, &(attachedRtrId),
                                          &(attachedRtrId), NULL,
                                          (UINT1 *) pArea);

            if (pLsaInfo == NULL)
            {
                continue;
            }

            MEMSET (&rtrLsaSearchRtrInfo, 0, sizeof (tRtrLsaRtInfo));
            rtrLsaSearchRtrInfo.u4LinkId =
                OSPF_CRU_BMC_DWFROMPDU ((pInterface->ifIpAddr));
            rtrLsaSearchRtrInfo.u4LinkData =
                OSPF_CRU_BMC_DWFROMPDU ((pNeighbor->nbrIpAddr));
            rtrLsaSearchRtrInfo.u1Type = TRANSIT_LINK;

            if (GrLsuSearchRtrLsa (pLsaInfo, &rtrLsaSearchRtrInfo)
                == OSPF_FALSE)
            {
                /* Inconsistent */
                OSPF_EXT_TRC2 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                               "Inconsistent LSA "
                               "Attached neighbor id 0x%x does not have a transit link"
                               " for DR 0x%x\r\n",
                               OSPF_CRU_BMC_DWFROMPDU (attachedRtrId),
                               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));
                return OSPF_TRUE;
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuCheckSelfOrigLsaInCxt\r\n");
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuSearchRtrLsa                                          */
/*                                                                           */
/* Description  : This routine scans the router LSA and searches for the     */
/*                given node                                                 */
/*                                                                           */
/* Input        : pLsaInfo      - pointer to LSA info structure              */
/*                pRtrLsaRtInfo - pointer to a link                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE/OSPF_FALSE                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
GrLsuSearchRtrLsa (tLsaInfo * pLsaInfo, tRtrLsaRtInfo * pRtrLsaRtInfo)
{
    tRtrLsaRtInfo       nbrRtrLsaRtInfo;
    UINT2               u2LinkCount = 0;
    UINT2               u2CurrLink = 0;
    UINT1              *pu1NbrLsaPtr = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuSearchRtrLsa\r\n");

    /* Skip the LSA header, options and unused fields */
    pu1NbrLsaPtr = pLsaInfo->pLsa + LS_HEADER_SIZE + 2;

    /* Get the numner of links and scan all the links */
    u2LinkCount = LGET2BYTE (pu1NbrLsaPtr);
    for (u2CurrLink = 0; u2CurrLink < u2LinkCount; u2CurrLink++)
    {
        MEMSET (&nbrRtrLsaRtInfo, 0, sizeof (tRtrLsaRtInfo));
        GrLsuGetLinksFromLsa (pu1NbrLsaPtr, &nbrRtrLsaRtInfo);
        pu1NbrLsaPtr = pu1NbrLsaPtr + LSA_SIZE;

        /* In this case, link type = transit, link id = interface ip
         * address (since this interface is DR, link data = Nbr ip
         * address */
        if ((nbrRtrLsaRtInfo.u1Type == pRtrLsaRtInfo->u1Type) &&
            (nbrRtrLsaRtInfo.u4LinkId == pRtrLsaRtInfo->u4LinkId) &&
            (nbrRtrLsaRtInfo.u4LinkData == pRtrLsaRtInfo->u4LinkData))
        {
            return OSPF_TRUE;
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuSearchRtrLsa\r\n");
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuIsNbrOrigLsaInCxt                                     */
/*                                                                           */
/* Description  : This routine checks whether the LSA is originated by       */
/*                one of the attached neighbors                              */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                pLsHeader    - LSA header                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE     -     if LSA is nbr-originated               */
/*                OSPF_FALSE    -     if LSA is not nbr-originated           */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
GrLsuIsNbrOrigLsaInCxt (tOspfCxt * pOspfCxt, tLsHeader * pLsHeader)
{
    tNeighbor          *pNeighbor = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuIsNbrOrigLsaInCxt\r\n");

    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

        if (UtilIpAddrComp (pNeighbor->nbrId, pLsHeader->advRtrId)
            == OSPF_EQUAL)
        {
            return OSPF_TRUE;
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuIsNbrOrigLsaInCxt\r\n");
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuCheckNbrLsaInCxt                                      */
/*                                                                           */
/* Description  : This routine checks for LSA inconsistency if the           */
/*                received LSA is nbr-originated LSA                         */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                pArea        - Pointer to Area                             */
/*                pLsHeader    - LSA header                                  */
/*                pLsa         - pointer to LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE     -     if LSA is inconsistent                 */
/*                OSPF_FALSE    -     if LSA is not inconsistent             */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
GrLsuCheckNbrLsaInCxt (tOspfCxt * pOspfCxt, tArea * pArea,
                       tLsHeader * pLsHeader, UINT1 *pLsa)
{
    tLsaInfo           *pLsaInfo = NULL;    /* pointer to LsaInfo */
    tInterface         *pInterface = NULL;    /* pointer to interface */
    tRtrLsaRtInfo       nbrRtrLsaRtInfo;    /* Structure to store
                                             * the LSA links */
    tRtrLsaRtInfo       selfRtrLsaRtInfo;    /* Used for RBTree search for links
                                             * from self router LSA */
    tTMO_SLL_NODE      *pLstNode = NULL;
    tIPADDR             linkId;
    tIPADDR             linkData;
    tIPADDR             networkMask;
    tRouterId           attachedRtrId;
    tRBElem            *pRBElem = NULL;
    INT4                i4RetVal = OSPF_FALSE;
    UINT2               u2LinkCount = 0;    /* No of links in router LSA */
    UINT2               u2CurrLink = 0;    /* Current router LSA link */
    UINT1              *pu1CurrPtr = NULL;    /* Current LSA pointer */
    UINT1               u1LsaType = pLsHeader->u1LsaType;
    UINT1               u1NetworkFlag = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuCheckNbrLsaInCxt\r\n");

    if (u1LsaType == ROUTER_LSA)
    {
        /* Neighbor originated router LSA */
        /* Inconsistency occurs in the following scenarios
         * In both these cases, Nbr has declared the restarting router as down
         *
         * 1. In broadcast/nbma/p2mp network, restarting router has a link
         *    to transit network but neighbor has a link to stub network
         * 2. In other networks, neighbor has advertised only a stub link
         *    but restarting router has advertised a stub link and a p2p
         *    link.
         */
        pLsaInfo = LsuSearchDatabase (ROUTER_LSA, &(pOspfCxt->rtrId),
                                      &(pOspfCxt->rtrId), NULL,
                                      (UINT1 *) pArea);

        if (pLsaInfo == NULL)
        {
            /* Restarting router has not received the self-originated LSA */
            return OSPF_FALSE;
        }
        /* Scan the LSA and store the links in pRtrLsaCheck RBTree */
        UtilConstRtrLsaRBTree (pLsaInfo->pLsa);

        /* Scan the LSA received from the neighbor */
        /* Skip the LSA header, options and unused fields */
        pu1CurrPtr = pLsa + LS_HEADER_SIZE + 2;

        /* Get the numner of links and scan all the links */
        u2LinkCount = LGET2BYTE (pu1CurrPtr);
        for (u2CurrLink = 0; u2CurrLink < u2LinkCount; u2CurrLink++)
        {
            MEMSET (&nbrRtrLsaRtInfo, 0, sizeof (tRtrLsaRtInfo));
            GrLsuGetLinksFromLsa (pu1CurrPtr, &nbrRtrLsaRtInfo);

            /* if the link type is transit, no need to check for
             * inconsistency */
            if (nbrRtrLsaRtInfo.u1Type == TRANSIT_LINK)
            {
                continue;
            }

            /* If link type is p2p or virtual, then delete the corresponding
             * link advertised by the restarting router from RBTree. After 
             * scanning the LSA, if any p2p or virtual link advertised by
             * restarting router to the advertised router-id is available,
             * then the LSA is inconsistent */
            if ((nbrRtrLsaRtInfo.u1Type == PTOP_LINK) ||
                (nbrRtrLsaRtInfo.u1Type == VIRTUAL_LINK))
            {
                GrLsuDelLinkInfoFromRtrRBTree (pOspfCxt, nbrRtrLsaRtInfo,
                                               pLsHeader);
                continue;
            }

            MEMSET (&linkId, 0, MAX_IP_ADDR_LEN);
            MEMSET (&linkData, 0, MAX_IP_ADDR_LEN);
            OSPF_CRU_BMC_DWTOPDU (linkId, nbrRtrLsaRtInfo.u4LinkId);
            OSPF_CRU_BMC_DWTOPDU (linkData, nbrRtrLsaRtInfo.u4LinkData);

            /* Scan all the interfaces in this context and check if
             * network/mask advertised matches */
            TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
            {
                pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

                /* If the stub link belongs to any of the attached interface
                 * and the interface belongs to a transit network, Scan the
                 * RBTree to match a trasnsit link. If a transit link is
                 * present, the LSA is declared inconsistent */
                if ((UtilIpAddrMaskComp (pInterface->ifIpAddr, linkId,
                                         pInterface->ifIpAddrMask)
                     == OSPF_EQUAL) &&
                    (UtilIpAddrComp (pInterface->ifIpAddrMask, linkData)
                     == OSPF_EQUAL) && IS_TRANSIT_NETWORK (pInterface))
                {
                    MEMSET (&selfRtrLsaRtInfo, 0, sizeof (tRtrLsaRtInfo));
                    selfRtrLsaRtInfo.u1Type = TRANSIT_LINK;
                    selfRtrLsaRtInfo.u4LinkId =
                        OSPF_CRU_BMC_DWFROMPDU ((pInterface->desgRtr));
                    selfRtrLsaRtInfo.u4LinkData =
                        OSPF_CRU_BMC_DWFROMPDU ((pInterface->ifIpAddr));

                    pRBElem = RBTreeRem (gOsRtr.pRtrLsaCheck,
                                         (tRBElem *) & selfRtrLsaRtInfo);
                    if (pRBElem != NULL)
                    {
                        /* Neighbor has originated a stub link but restarting
                         * router originated a transit link. This LSA is
                         * inconsistent */
                        RBTreeDrain (gOsRtr.pRtrLsaCheck,
                                     UtilRBFreeRouterLinks, 0);
                        OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                                       "Inconsistent LSA "
                                       "Restarting router has a transit link"
                                       " for DR 0x%x\r\n",
                                       OSPF_CRU_BMC_DWFROMPDU (pInterface->
                                                               desgRtr));
                        return OSPF_TRUE;
                    }
                }
            }
        }
        /* Scan the RB Tree and check if any p2p or virtual link is there
         * to advertising router. If there is any link, then the LSA is
         * is inconsistent */
        i4RetVal = GrLsuSearchRtrLsaRBTree (pOspfCxt, pLsHeader);
        RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);

        if (i4RetVal == OSPF_TRUE)
        {
            OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                          "Inconsistent LSA "
                          "p2p/virtual link is not advertised by neighbor");
            return OSPF_TRUE;
        }
    }
    else if (u1LsaType == NETWORK_LSA)
    {
        /* Neighbor has originated a network LSA. Network LSA needs to be
         * considered in the following scenario
         *
         * If there are more than 2 routers in a transit network and restarting
         * router is not DR, then by quitting helper mode, helper will not send
         * a stub link in router LSA. LSA inconsistency can be identified if
         * the restarting router-id is not seen in the received network LSA */
        /* Skip LSA header */
        pu1CurrPtr = pLsa + LS_HEADER_SIZE;

        /* Get the network mask and check whether the network LSA is
         * for the network attached to this instance */
        MEMSET (&networkMask, 0, MAX_IP_ADDR_LEN);
        LGETSTR (pu1CurrPtr, networkMask, MAX_IP_ADDR_LEN);

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if (UtilIpAddrMaskComp
                (pInterface->ifIpAddr, pLsHeader->linkStateId,
                 networkMask) == OSPF_EQUAL)
            {
                break;
            }
        }

        if (pLstNode == NULL)
        {
            /* This network LSA is not advertised for any of the networks
             * to which the instance attached to this instance */
            return OSPF_FALSE;
        }

        while (pu1CurrPtr < (pLsa + pLsHeader->u2LsaLen))
        {
            MEMSET (&attachedRtrId, 0, MAX_IP_ADDR_LEN);

            LGETSTR (pu1CurrPtr, attachedRtrId, MAX_IP_ADDR_LEN);
            if (UtilIpAddrComp (attachedRtrId, pOspfCxt->rtrId) == OSPF_EQUAL)
            {
                u1NetworkFlag = OSPF_TRUE;
                break;
            }
        }
        if ((u1NetworkFlag == OSPF_FALSE)
            && (pu1CurrPtr >= (pLsa + pLsHeader->u2LsaLen)))
        {
            /* Restarting router-id is not found in network LSA */
            OSPF_EXT_TRC1 (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                           "Inconsistent LSA "
                           "Restarting router id is not found in network"
                           " LSA originated by neighbor %x\r\n",
                           OSPF_CRU_BMC_DWFROMPDU (pLsHeader->advRtrId));
            return OSPF_TRUE;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuCheckNbrLsaInCxt\r\n");
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuGetLinksFromLsa                                       */
/*                                                                           */
/* Description  : This routine scans the router LSA from current pointer     */
/*                and updates the pRtrLsaRtInfo with next link               */
/*                                                                           */
/* Input        : pu1CurrPtr    - Current LSA pointer                        */
/*                                                                           */
/* Output       : pRtrLsaRtInfo - pointer to a link                          */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GrLsuGetLinksFromLsa (UINT1 *pu1CurrPtr, tRtrLsaRtInfo * pRtrLsaRtInfo)
{
    tIPADDR             linkId;
    tIPADDR             linkData;
    INT1                i1TosCount = 0;
    INT1                i1Tos = 0;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: GrLsuGetLinksFromLsa\r\n");

    MEMSET (&linkId, 0, MAX_IP_ADDR_LEN);
    MEMSET (&linkData, 0, MAX_IP_ADDR_LEN);

    LGETSTR (pu1CurrPtr, &linkId, MAX_IP_ADDR_LEN);
    LGETSTR (pu1CurrPtr, &linkData, MAX_IP_ADDR_LEN);
    pRtrLsaRtInfo->u4LinkId = OSPF_CRU_BMC_DWFROMPDU (linkId);
    pRtrLsaRtInfo->u4LinkData = OSPF_CRU_BMC_DWFROMPDU (linkData);

    pRtrLsaRtInfo->u1Type = LGET1BYTE (pu1CurrPtr);
    i1TosCount = LGET1BYTE (pu1CurrPtr);
    pRtrLsaRtInfo->u4Metric[TOS_0] = LGET2BYTE (pu1CurrPtr);

    while (i1TosCount--)
    {
        i1Tos = LGET1BYTE (pu1CurrPtr);
        /* Skip one unused byte and get the TOS metric */
        pu1CurrPtr += 1;
        pRtrLsaRtInfo->u4Metric[DECODE_TOS (i1Tos)] = LGET2BYTE (pu1CurrPtr);
    }

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "FUNC: GrLsuGetLinksFromLsa\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuDelLinkInfoFromRtrRBTree                              */
/*                                                                           */
/* Description  : This routine deletes the link info from router LSA         */
/*                Yhis is called only for p2p and virtual link               */
/*                                                                           */
/* Input        : pOspfCxt        - pointer to OSPF instance                 */
/*                nbrRtrLsaRtInfo - Router LSA link info                     */
/*                pLsHeader       - pointer to LS Header                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
GrLsuDelLinkInfoFromRtrRBTree (tOspfCxt * pOspfCxt,
                               tRtrLsaRtInfo nbrRtrLsaRtInfo,
                               tLsHeader * pLsHeader)
{
    tRtrLsaRtInfo       delRtrLsaLinkInfo;
    tInterface         *pInterface = NULL;
    tNeighbor          *pNeighbor = NULL;
    tTMO_SLL           *pIfList = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tIPADDR             linkId;
    tIPADDR             linkData;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuDelLinkInfoFromRtrRBTree\r\n");

    MEMSET (&delRtrLsaLinkInfo, 0, sizeof (tRtrLsaRtInfo));
    MEMSET (&linkId, 0, MAX_IP_ADDR_LEN);
    MEMSET (&linkData, 0, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_DWTOPDU (linkId, nbrRtrLsaRtInfo.u4LinkId);
    OSPF_CRU_BMC_DWTOPDU (linkData, nbrRtrLsaRtInfo.u4LinkData);

    /* If the link does not belong to the restarting router, return */
    if (UtilIpAddrComp (pOspfCxt->rtrId, linkId) != OSPF_EQUAL)
    {
        return;
    }

    if (nbrRtrLsaRtInfo.u1Type == VIRTUAL_LINK)
    {
        pIfList = &(pOspfCxt->virtIfLst);
    }
    else
    {
        pIfList = &(pOspfCxt->sortIfLst);
    }

    TMO_SLL_Scan (pIfList, pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        if (IS_PTOP_IFACE (pInterface) || IS_VIRTUAL_IFACE (pInterface))
        {
            if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
            {
                pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if (UtilIpAddrComp (pNeighbor->nbrId, pLsHeader->advRtrId)
                    != OSPF_EQUAL)
                {
                    continue;
                }
            }
            delRtrLsaLinkInfo.u1Type = nbrRtrLsaRtInfo.u1Type;
            delRtrLsaLinkInfo.u4LinkId
                = OSPF_CRU_BMC_DWFROMPDU (pLsHeader->advRtrId);
            delRtrLsaLinkInfo.u4LinkData
                = OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
            RBTreeRem (gOsRtr.pRtrLsaCheck, (tRBElem *) & delRtrLsaLinkInfo);
            break;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuDelLinkInfoFromRtrRBTree\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuSearchRtrLsaRBTree                                    */
/*                                                                           */
/* Description  : This routine scand the router LSA link info RBTree         */
/*                During nbr router LSA scan, all the p2p/virtual links      */
/*                should have been deleted from RB Tree. If there is         */
/*                any p2p/virtual link available to the advertising          */
/*                router, then LSA is inconcistent                           */
/*                                                                           */
/* Input        : pOspfCxt        - pointer to OSPF instance                 */
/*                pLsHeader       - pointer to LS Header                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE/OSPF_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
GrLsuSearchRtrLsaRBTree (tOspfCxt * pOspfCxt, tLsHeader * pLsHeader)
{
    tRtrLsaRtInfo      *pRtrLsaCheck = NULL;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pOspfCxt);

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuSearchRtrLsaRBTree\r\n");

    if (gOsRtr.pRtrLsaCheck != NULL)
    {
        pRtrLsaCheck = (tRtrLsaRtInfo *) RBTreeGetFirst (gOsRtr.pRtrLsaCheck);

        while (pRtrLsaCheck != NULL)
        {
            /* Check whether it is the re-transmission node for Grace LSA */
            if (((pRtrLsaCheck->u1Type == PTOP_LINK) ||
                 (pRtrLsaCheck->u1Type == VIRTUAL_LINK))
                && (pRtrLsaCheck->u4LinkId ==
                    OSPF_CRU_BMC_DWFROMPDU (pLsHeader->advRtrId)))
            {
                return OSPF_TRUE;
            }
            pRtrLsaCheck = (tRtrLsaRtInfo *) RBTreeGetNext
                (gOsRtr.pRtrLsaCheck, (tRBElem *) pRtrLsaCheck, NULL);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrLsuSearchRtrLsaRBTree\r\n");
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrLsuProcessRcvdSelfOrgLsa                                 */
/*                                                                           */
/* Description  : This routine process the self orginated LSA received       */
/*                from the nbr during graceful restart mode                  */
/*                                                                           */
/* Input        : pLsaInfo        - pointer to  Self Orginated Lsa           */
/*                pLsHeader       - pointer to LS Header                     */
/*                pLsa            - pointer to Lsa pkt buffer                */
/*                u2Len           -  Lsa pkt length                          */
/*                pNbr           -  pointer to the neighbor                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS/DISCARDED                                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
GrLsuProcessRcvdSelfOrgLsa (tLsHeader * pLsHeader, tLsaInfo * pLsaInfo,
                            UINT1 *pLsa, UINT2 u2Len, tNeighbor * pNbr)
{
    UINT1               u1LsaChngdFlag = OSPF_FALSE;
    tLsaDesc           *pLsaDesc = NULL;
    tLsaInfo           *pNewLsaInfo = NULL;
    UINT1               ackFlag = UNUSED_ACK_FLAG;
    UINT1               u1NewDescFlag = OSPF_FALSE;
    tExtLsdbOverflowTrapInfo trapElsdbOverflow;

    if (pLsHeader->lsaSeqNum == MAX_SEQ_NUM)
    {
        return DISCARDED;
    }

    if (pLsaInfo != (tLsaInfo *) NULL)
    {
        /* Set the u1LsaChngdFlag appropriately */
        u1LsaChngdFlag = (UINT1) LsuCheckActualChange (pLsaInfo->pLsa, pLsa);
    }

    /* If the lsa is received for first time, Add the New lsa to the
     * Data base */
    if (pLsaInfo == NULL)
    {
        pLsaDesc = GrOlsConstructLsaDescRcvdSelfOrgLsa
            (pLsHeader, pNbr, &u1NewDescFlag);
        if (pLsaDesc == NULL)
        {
            return DISCARDED;
        }
        if ((pNewLsaInfo = LsuAddToLsdb (pNbr->pInterface->pArea,
                                         pLsHeader->u1LsaType,
                                         &(pLsHeader->linkStateId),
                                         &(pLsHeader->advRtrId),
                                         pNbr->pInterface,
                                         NEW_LSA, pLsa)) == NULL)
        {
            if (u1NewDescFlag == OSPF_TRUE)
            {
                LSA_DESC_FREE (pLsaDesc);
            }
            return DISCARDED;
        }

        pNewLsaInfo->pLsaDesc = pLsaDesc;
        pLsaDesc->pLsaInfo = pNewLsaInfo;
        TMO_DLL_Add (&(pNewLsaInfo->pOspfCxt->origLsaDescLst),
                     &(pLsaDesc->lsaDescNode));
        /* Set the bit in LSA to indicate that the self-originated LSA
         * is in restarting router */
        LSA_REFRESH_BIT_SET (pNewLsaInfo, OSPF_GR_LSA_BIT);

        /* Install the New Lsa in the Link state database */
        if (LsuInstallLsa (pLsa, pNewLsaInfo) == OSPF_FAILURE)
        {
            return DISCARDED;
        }

    }
    else
    {
        if (LsuCompLsaInstance (NULL, pLsaInfo, pLsHeader) == OSPF_EQUAL)
        {
            LsuFindAndProcessAckFlag (pNbr, pLsaInfo, pLsa, pLsHeader);
            return DISCARDED;
        }
        /* remove the old instance of lsa from all rxmt lists */
        LsuDeleteFromAllRxmtLst (pLsaInfo);
        /* Update the Old Lsa_info in the Data base  with new one */
        if (LsuInstallLsa (pLsa, pLsaInfo) == OSPF_FAILURE)
        {
            return DISCARDED;
        }
        /*
         * The flood procedure returns FLOOD_BACK or NO_FLOOD_BACK based on
         * whether the lsa has been flooded out on the receiving interface
         * or not
         */

        ackFlag = LsuFloodOut (pLsa, u2Len, pNbr,
                               pNbr->pInterface->pArea,
                               u1LsaChngdFlag, pNbr->pInterface);
        LsuProcessAckFlag (pNbr, pLsa, ackFlag);

    }

    COUNTER_OP (pNbr->pInterface->pArea->pOspfCxt->u4RcvNewLsa, 1);

    if (!(pNbr->pInterface->pArea->pOspfCxt->bOverflowState == OSPF_TRUE) &&
        (pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
        (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT
         (pNbr->pInterface->pArea->pOspfCxt) ==
         pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit))
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ADJACENCY_TRC,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Rtr Enters OvrFlw State\n");

        RtrEnterOverflowStateInCxt (pNbr->pInterface->pArea->pOspfCxt);
    }

    if (IS_TRAP_ENABLED_IN_CXT (LSDB_APPROACHING_OVERFLOW_TRAP,
                                pNbr->pInterface->pArea->pOspfCxt) &&
        (pNbr->pInterface->pArea->pOspfCxt->bLsdbApproachingOvflTrapGen
         == OSPF_FALSE) &&
        (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT
         (pNbr->pInterface->pArea->pOspfCxt) >=
         OVERFLOW_APPROACHING_LIMIT_IN_CXT
         (pNbr->pInterface->pArea->pOspfCxt)) &&
        (pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit != NO_LIMIT))
    {
        trapElsdbOverflow.i4ExtLsdbLimit =
            pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit;
        IP_ADDR_COPY (trapElsdbOverflow.rtrId,
                      pNbr->pInterface->pArea->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pNbr->pInterface->pArea->pOspfCxt,
                             LSDB_APPROACHING_OVERFLOW_TRAP,
                             &trapElsdbOverflow);
        pNbr->pInterface->pArea->pOspfCxt->bLsdbApproachingOvflTrapGen =
            OSPF_TRUE;
    }

    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrOlsConstructLsaDescRcvdSelfOrgLsa                        */
/*                                                                           */
/* Description  : This routine constructs the Lsa desc for the self org LSA  */
/*                received from the nbr during graceful restart mode         */
/*                                                                           */
/* Input        : pLsHeader       - pointer to LS Header                     */
/*                pNbr           -  pointer to the neighbor                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to Lsa Desc                                        */
/*                                                                           */
/*****************************************************************************/
PRIVATE tLsaDesc   *
GrOlsConstructLsaDescRcvdSelfOrgLsa (tLsHeader * pLsHeader,
                                     tNeighbor * pNbr, UINT1 *pu1NewDescFlag)
{
    UINT1              *pTmpPtr = NULL;
    UINT1               u1LsaType;
    tLsaDesc           *pLsaDesc;

    u1LsaType = pLsHeader->u1LsaType;
    if ((u1LsaType == TYPE11_OPQ_LSA) || (u1LsaType == AS_EXT_LSA))
    {
        pTmpPtr = NULL;
    }
    else if (u1LsaType == TYPE9_OPQ_LSA)
    {
        pTmpPtr = (UINT1 *) pNbr->pInterface;
    }
    else
    {
        pTmpPtr = (UINT1 *) pNbr->pInterface->pArea;
    }

    pLsaDesc = OlsGetLsaDescInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                   u1LsaType, &(pLsHeader->linkStateId),
                                   pTmpPtr, pu1NewDescFlag);
    return pLsaDesc;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrCheckAndExitGracefulRestart                              */
/*                                                                           */
/* Description  : This routine checks and exits the graceful restart after   */
/*                restoring the pre-restart adjaceny with the neighbors      */
/*                                                                           */
/* Input        : pOspfCxt        - pointer to ospf context                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
GrCheckAndExitGracefulRestart (tOspfCxt * pOspfCxt)
{
    tNeighbor          *pNbr = NULL;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    UINT1               u1AdjFlag = OSPF_FALSE;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            u1AdjFlag = OSPF_FALSE;
            if (NsmToBecomeAdj (pNbr) == OSPF_TRUE)
            {
                /* If neighbor state is not full then GR is not yet completed
                   so return */
                if (pNbr->u1NsmState == NBRS_FULL)
                {
                    u1AdjFlag = OSPF_TRUE;
                }
            }
            else
            {
                /* If neighbor state is not two-way then GR is not yet completed
                   so return */
                if ((pNbr->u1NsmState == NBRS_2WAY)
                    && (pInterface->u1IsmState == IFS_DR_OTHER))
                {
                    u1AdjFlag = OSPF_TRUE;
                }
            }
            if (u1AdjFlag == OSPF_FALSE)
            {
                return;
            }
        }                        /* neighbor scan */
    }                            /* Interface scan */
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
        {
            u1AdjFlag = OSPF_FALSE;
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            /* If neighbor state is not full then GR is not yet completed
             * so return */
            if (pNbr->u1NsmState == NBRS_FULL)
            {
                u1AdjFlag = OSPF_TRUE;
            }
            if (u1AdjFlag == OSPF_FALSE)
            {
                return;
            }
        }
    }

    GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_COMPLETED);
    return;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osgr.c                         */
/*-----------------------------------------------------------------------*/
