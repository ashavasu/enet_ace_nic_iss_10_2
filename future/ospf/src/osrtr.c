/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osrtr.c,v 1.56 2017/10/04 13:41:03 siva Exp $
 *
 * Description:This file contains procedures for configuration 
 *             of routers
 *
 *******************************************************************/

#include "osinc.h"
#ifdef L3_SWITCHING_WANTED
#include "ipnp.h"
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrSetProtocolStatus                                       */
/*                                                                           */
/* Description  : Enables the OSPF protocol.                                 */
/*                                                                           */
/* Input        : u1AdmnStatus  : admin status to be set                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtrSetProtocolStatusInCxt (UINT4 u4OspfCxtId, UINT1 u1AdmnStatus)
{
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4RetStat = OSPF_FAILURE;

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    switch (u1AdmnStatus)
    {
        case OSPF_ENABLED:
            if (pOspfCxt != NULL)
            {
                if (gOsRtr.osRedInfo.u1AdminState == OSPF_ENABLED)
                {
                    i4RetStat = OspfRmRtrEnableInCxt (pOspfCxt);
                }
                else
                {
                    i4RetStat = RtrEnableInCxt (pOspfCxt);
                }
            }
            break;

        case OSPF_DISABLED:
            if ((pOspfCxt != NULL) && (pOspfCxt->admnStat != OSPF_DISABLED))
            {
                if (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
                {
                    i4RetStat = RtrDisableInCxt (pOspfCxt);
                }
                else if (pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
                {
                    i4RetStat = GrDisableInCxt (pOspfCxt);
                }
            }
            break;

        case OSPF_SHUTDOWN:
            if (pOspfCxt != NULL)
            {
                i4RetStat = RtrShutdownInCxt (pOspfCxt);
            }
            break;

        case OSPF_REBOOT:
            i4RetStat = RtrRebootInCxt (u4OspfCxtId);
            break;

        default:
            break;
    }

    return i4RetStat;
}

#ifdef TOS_SUPPORT
/****************************************************************************/
/*                                                                          */
/* Function     : RtrSetTosSupport                                          */
/*                                                                          */
/* Description  : sets the tos capability of OSPF router                    */
/*                                                                          */
/* Input        : u1TosCapability  : OSPF_TRUE  - Enables the TOS support   */
/*                                     OSPF_FALSE - Disables the TOS        */
/*                                                  support                 */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
RtrSetTosSupportInCxt (tOspfCxt * pOspfCxt, UINT1 u1TosCapability)
{

    if (pOspfCxt->admnStat == OSPF_ENABLED)
    {
        if (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            if (RtrDisableInCxt (pOspfCxt) == OSPF_FAILURE)
            {
                return OSPF_FAILURE;
            }
            pOspfCxt->bTosCapability = u1TosCapability;
            if (RtrEnableInCxt (pOspfCxt) == OSPF_FAILURE)
            {
                return OSPF_FAILURE;
            }
        }
        else if (pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
        {

            if (GrDisableInCxt (pOspfCxt) == OSPF_FAILURE)
            {
                return OSPF_FAILURE;
            }
            pOspfCxt->bTosCapability = u1TosCapability;
            if (RtrEnableInCxt (pOspfCxt) == OSPF_FAILURE)
            {
                return OSPF_FAILURE;
            }
        }
    }
    else
    {
        pOspfCxt->bTosCapability = u1TosCapability;
    }
    return OSPF_SUCCESS;
}
#endif /*  TOS_SUPPORT */
/*****************************************************************************/
/*                                                                           */
/* Function     : RtrSetAsbrStatus                                        */
/*                                                                           */
/* Description  : sets the ASBR status of OSPF router                        */
/*                                                                           */
/* Input        : u1AsBdrRtrStatus : OSPF_TRUE  - Makes router as ASBR   */
/*                                       OSPF_FALSE - Non ASBR router        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtrSetAsbrStatusInCxt (tOspfCxt * pOspfCxt, UINT1 u1AsBdrRtrStatus)
{
    tArea              *pArea;

    if (u1AsBdrRtrStatus == OSPF_TRUE)
    {
        /* newly configured as AS boundary router */
        pOspfCxt->bAsBdrRtr = OSPF_TRUE;
        if ((pOspfCxt->admnStat == OSPF_ENABLED) &&
            (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE))
        {
            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                if (pArea->u4ActIntCount > 0)
                {
                    OlsGenerateLsa (pArea, ROUTER_LSA, &pOspfCxt->rtrId, NULL);
                }
            }
            OlsGenerateAsExtLsasInCxt (pOspfCxt);

            /* If Opaque Capability is enabled, give the ASBR Status Change 
             * Info to Opq Application */
            if (pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)
            {
                AsbrSendAsbrInfoToOpqApp (pOspfCxt, OSPF_TRUE);
            }
        }

    }
    else if (u1AsBdrRtrStatus == OSPF_FALSE)
    {

        /* 
         * This router is no longer AS boundary router.
         * Self originated AS ext LSAs should be flushed from
         * the routing domain.
         */

        pOspfCxt->bAsBdrRtr = OSPF_FALSE;
        if ((pOspfCxt->admnStat == OSPF_ENABLED) &&
            (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE))
        {

            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                OlsGenerateLsa (pArea, ROUTER_LSA, &pOspfCxt->rtrId, NULL);
            }
            LsuFlushoutSelfOrgExtLsaInCxt (pOspfCxt);

            /* If Opaque Capability is enabled, give the ASBR Status Change 
             * Info to Opq Application */
            if (pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)
            {
                AsbrSendAsbrInfoToOpqApp (pOspfCxt, OSPF_FALSE);
            }
        }
    }
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtrEnterOverflowState                                */
/*                                                                           */
/* Description     : actions taken when the router enters overflow state     */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtrEnterOverflowStateInCxt (tOspfCxt * pOspfCxt)
{
    tExtLsdbOverflowTrapInfo trapElsdbOverflow;

    pOspfCxt->bOverflowState = OSPF_TRUE;
    if (pOspfCxt->u4ExitOverflowInterval != DONT_LEAVE_OVERFLOW_STATE)
    {
        TmrSetTimer (&(pOspfCxt->exitOverflowTimer), EXIT_OVERFLOW_TIMER,
                     UtilJitter (NO_OF_TICKS_PER_SEC *
                                 (pOspfCxt->u4ExitOverflowInterval),
                                 OSPF_JITTER));
    }
    if (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        LsuFlushAllNonDefaultSelfOrgAseLsaInCxt (pOspfCxt);
    }
    else
    {
        /* router is trying to enter the overflow state in graceful restart mode */
        /* Exit GR with exit reason as TOPOLOGY change */
        GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_TOP_CHG);
    }

    if (IS_TRAP_ENABLED_IN_CXT (LSDB_OVERFLOW_TRAP, pOspfCxt))
    {
        trapElsdbOverflow.i4ExtLsdbLimit = pOspfCxt->i4ExtLsdbLimit;
        IP_ADDR_COPY (trapElsdbOverflow.rtrId, &(pOspfCxt->rtrId));
        SnmpifSendTrapInCxt (pOspfCxt, LSDB_OVERFLOW_TRAP, &trapElsdbOverflow);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrCreateCxt                                               */
/*                                                                           */
/* Description  : Creates ospf context                                       */
/*                                                                           */
/* Input        : u4OspfCxtId,                                               */
/*                u4OspfAdminStat  - ospf admin status Enabled/disabled      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtrCreateCxt (UINT4 u4OspfCxtId, UINT1 u1OspfAdminStat)
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               u1ArrayIndx;
    UINT4               u4CsrRestoreFlag = OSPF_FALSE;
    INT4                i4FileFd = OSPF_ZERO;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];
    UINT1               u1ProtoIndex;

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, u4OspfCxtId,
              "OSPF Context to be created and ospf enabled\n");

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt != NULL)
    {
        /* reset pointers used to cache list nodes */
        pOspfCxt->pLastLsdbLst = NULL;
        pOspfCxt->pLastLsdbNode = NULL;
        pOspfCxt->pLastExtAgg = NULL;
        pOspfCxt->pLastOspfIf = NULL;
        pOspfCxt->pLastNbr = NULL;
        pOspfCxt->pLastConfigRouteInfo = NULL;

        return OSPF_SUCCESS;
    }

    if (UtilOspfVcmIsVcExist (u4OspfCxtId) == OSPF_FAILURE)
    {
        /* u4OspfCxtId is not created  in VCM */
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, u4OspfCxtId,
                  "Context Creation Failed\n");
        return OSPF_FAILURE;
    }

    if (CONTEXT_ALLOC (pOspfCxt) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC, u4OspfCxtId,
                  "Ospf Context Alloc Failure\n");

        return OSPF_FAILURE;
    }

    OS_MEM_SET (pOspfCxt, 0, sizeof (tOspfCxt));

    IP_ADDR_COPY (&(pOspfCxt->rtrId), gNullIpAddr);
    pOspfCxt->i4RouterIdStatus = ROUTERID_DYNAMIC;
    pOspfCxt->u4OspfCxtId = u4OspfCxtId;
    pOspfCxt->admnStat = OSPF_DISABLED;

    TMO_SLL_Init (&(pOspfCxt->areasLst));

    if (ExtRouteTblInitInCxt (pOspfCxt) == OSPF_FAILURE)
    {
        OSPF_TRC (OSPF_CRITICAL_TRC, u4OspfCxtId,
                  "OSPF External Route Trie Creation Failed\n");
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }
    TMO_SLL_Init (&(pOspfCxt->asExtAddrRangeLstInRtr));
    TMO_SLL_Init (&(pOspfCxt->sortIfLst));
    TMO_SLL_Init (&(pOspfCxt->virtIfLst));
    TMO_SLL_Init (&(pOspfCxt->sortNbrLst));
    TMO_SLL_Init (&(pOspfCxt->hostLst));
    TMO_SLL_Init (&(pOspfCxt->Type11OpqLSALst));
    TMO_DLL_Init (&(pOspfCxt->origLsaDescLst));

#ifdef TOS_SUPPORT
    pOspfCxt->bTosCapability = OSPF_TRUE;
    pOspfCxt->rtrOptions = T_BIT_MASK;
#else /* TOS_SUPPORT */
    pOspfCxt->bTosCapability = OSPF_FALSE;
    pOspfCxt->rtrOptions = RAG_NO_CHNG;
#endif /*  TOS_SUPPORT */
    pOspfCxt->u1OpqCapableRtr = OSPF_FALSE;
    pOspfCxt->bAreaBdrRtr = OSPF_FALSE;
    pOspfCxt->bNssaAsbrDefRtTrans = OSPF_DISABLED;
    pOspfCxt->bAsBdrRtr = OSPF_FALSE;
    pOspfCxt->u4OspfPktsDisd = 0;
    pOspfCxt->rfc1583Compatibility = OSPF_ENABLED;
    pOspfCxt->u2MinLsaInterval = 5;
    pOspfCxt->bGenType5Flag = OSPF_FALSE;
    pOspfCxt->u4SpfHoldTimeInterval = OSPF_DEF_SPF_HOLDTIME;
    pOspfCxt->u4SpfInterval = OSPF_DEF_SPF_DELAY;
    pOspfCxt->u4LastCalTime = 0;

    pOspfCxt->u4NssaAreaCount = 0;

#ifdef TRACE_WANTED
    pOspfCxt->u4OspfTrace = 0;
#endif
    pOspfCxt->u4StaggeringDelta = OSPF_INVALID_STAGGERING_DELTA;
    pOspfCxt->u4RTStaggeringInterval = OSPF_DEF_STAGGERING_INTERVAL;

    pOspfCxt->i4ExtLsdbLimit = NO_LIMIT;
    pOspfCxt->u4ExitOverflowInterval = DONT_LEAVE_OVERFLOW_STATE;
    pOspfCxt->bOverflowState = OSPF_FALSE;

    pOspfCxt->bLsdbApproachingOvflTrapGen = OSPF_FALSE;
    pOspfCxt->u4TrapControl = 0;
    pOspfCxt->u1PktType = 0;
    pOspfCxt->u1CnfgErrType = 0;
    IP_ADDR_COPY (pOspfCxt->pktSrcAddr, gNullIpAddr);

    /* By Default ABR Type supported is STANDARD_ABR */
    pOspfCxt->u4ABRType = STANDARD_ABR;

    for (u1ArrayIndx = 0; u1ArrayIndx < MAX_APP_REG; u1ArrayIndx++)
    {
        pOspfCxt->pAppInfo[u1ArrayIndx] = NULL;
    }

    pOspfCxt->bDemandExtension = OSPF_TRUE;

    pOspfCxt->bDefaultPassiveInterface = OSPF_FALSE;

    /*Graceful Restart Default Values */
    pOspfCxt->u1RestartSupport = OSPF_RESTART_NONE;
    pOspfCxt->u1RestartStatus = OSPF_RESTART_NONE;
    pOspfCxt->u4GracePeriod = OSPF_GR_DEF_INTERVAL;
    pOspfCxt->u1StrictLsaCheck = OSPF_FALSE;
    pOspfCxt->u1HelperSupport = OSPF_GRACE_HELPER_ALL;
    pOspfCxt->u1HelperStatus = OSPF_GR_NOT_HELPING;
    pOspfCxt->u4HelperGrTimeLimit = OSPF_ZERO;
    pOspfCxt->u4GraceAckState = OSPF_ENABLED;
    pOspfCxt->u1GrLsaMaxTxCount = DEF_GRACE_LSA_SENT;
    pOspfCxt->u1RestartReason = OSPF_DEFAULT_REASON;
    pOspfCxt->u1RestartExitReason = OSPF_RESTART_NONE;

    if (FlashFileExists (CSR_CONFIG_FILE) == ISS_SUCCESS)
    {
        gu4CsrFlag = OSPF_TRUE;
    }
    if ((i4FileFd = FileOpen (OSPF_GR_CONF, OSIX_FILE_RO)) >= OSPF_ZERO)
    {
        FileClose (i4FileFd);
        gu4RestoreFlag = OSPF_TRUE;
    }
    u4CsrRestoreFlag = IssGetCsrRestoreFlag ();
    if ((gu4CsrFlag == OSPF_TRUE) && (gu4RestoreFlag != OSPF_TRUE) &&
        (u4CsrRestoreFlag == OSPF_TRUE))
    {
        pOspfCxt->u1RestartStatus = OSPF_RESTART_UNPLANNED;
        pOspfCxt->u1OspfRestartState = OSPF_GR_RESTART;
        pOspfCxt->u1RestartReason = OSPF_GR_SW_RED;
        pOspfCxt->u1RestartExitReason = OSPF_RESTART_INPROGRESS;
    }

    /* initialize RTM related parameters */
    if (OspfRtmInitInCxt (pOspfCxt) == OSPF_FAILURE)
    {
        OSPF_TRC (OSPF_CRITICAL_TRC, u4OspfCxtId,
                  "Failure in Initialization of RTM related parameters\n");

        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }

    ROUTING_TABLE_ALLOC (pOspfCxt->pOspfRt);
    if (pOspfCxt->pOspfRt == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC, u4OspfCxtId, "OSPF RT Alloc Failed\n");
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }
    if (RtcInitRt (pOspfCxt->pOspfRt, OSPF_RT_ID,
                   OSPF_RT_TYPE_IN_CXT (pOspfCxt)) == OSPF_FAILURE)
    {
        ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }

    if ((pOspfCxt->pCandteLst = TMO_HASH_Create_Table (CANDTE_HASH_TABLE_SIZE,
                                                       NULL, FALSE)) == NULL)
    {
        RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);
        ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }

    if ((pOspfCxt->pCandteLst2 =
         UTL_HASH_Create_Table (CANDTE_COST_HASH_TABLE_SIZE, NULL, FALSE,
                                FSAP_OFFSETOF (tCandteNode,
                                               candteHashNode2))) == NULL)
    {
        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
        RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);
        ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }

    pOspfCxt->pExtLsaHashTable =
        TMO_HASH_Create_Table (EXT_LSA_HASH_TABLE_SIZE, NULL, FALSE);
    if (pOspfCxt->pExtLsaHashTable == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, u4OspfCxtId,
                  "Ext LSA hash Table Creation Failed\n");

        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst2, NULL);
        RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);
        ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }

    /* Create RBTree for Type5 External  LSAs */
    MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
    OsUtilCalCxtRBTree (OSPF_ONE, (u4OspfCxtId + 1), au1SemName);

    pOspfCxt->pAsExtLsaRBRoot =
        RBTreeCreateEmbeddedExtended (OSPF_OFFSET (tLsaInfo, RbNode),
                                      RbCompareLsa, ((UINT1 *) au1SemName));

    if (pOspfCxt->pAsExtLsaRBRoot == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, u4OspfCxtId,
                  "AS Scope LSA RBTree  Creation Failed\r\n");
        TMO_HASH_Delete_Table (pOspfCxt->pExtLsaHashTable, NULL);
        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst2, NULL);
        RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);
        ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        return OSPF_FAILURE;
    }

#ifndef HIGH_PERF_RXMT_LST_WANTED
    NbrCreateNbrsTable (pOspfCxt);
#endif
    gOsRtr.apOspfCxt[u4OspfCxtId] = pOspfCxt;
/* Restore the context specific graceful restart configurations */
    GrRestoreCxtRestartInfo (pOspfCxt);

    /* As the router is made up by default the back-bone area is created. */
    if ((pOspfCxt->pBackbone = AreaCreateBackBoneAreaInCxt (pOspfCxt)) == NULL)
    {
        RBTreeDelete (pOspfCxt->pAsExtLsaRBRoot);
        TMO_HASH_Delete_Table (pOspfCxt->pExtLsaHashTable, NULL);
        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
        TMO_HASH_Delete_Table (pOspfCxt->pCandteLst2, NULL);
        RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);
        ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
        REDIST_REG_FREE (pOspfCxt->pRedistribute);
        RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
        CONTEXT_FREE (pOspfCxt);
        gOsRtr.apOspfCxt[u4OspfCxtId] = NULL;
        return OSPF_FAILURE;
    }

    if (u1OspfAdminStat == OSPF_ENABLED)
    {
        if (RtrEnableInCxt (pOspfCxt) != OSPF_SUCCESS)
        {
            RBTreeDelete (pOspfCxt->pAsExtLsaRBRoot);
            TMO_HASH_Delete_Table (pOspfCxt->pExtLsaHashTable, NULL);
            TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
            TMO_HASH_Delete_Table (pOspfCxt->pCandteLst2, NULL);
            AreaDelete (pOspfCxt->pBackbone);
            RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);
            ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
            REDIST_REG_FREE (pOspfCxt->pRedistribute);
            RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
            CONTEXT_FREE (pOspfCxt);
            gOsRtr.apOspfCxt[u4OspfCxtId] = NULL;
            return OSPF_FAILURE;
        }
    }

    pOspfCxt->pDistributeInFilterRMap = NULL;
    pOspfCxt->pDistanceFilterRMap = NULL;
    pOspfCxt->u1Distance = OSPF_DEFAULT_PREFERENCE;
    pOspfCxt->bIsOspfEnabled = OSPF_FALSE;
    pOspfCxt->u1BfdAdminStatus = OSPF_BFD_DISABLED;
    for (u1ProtoIndex = 0; u1ProtoIndex < MAX_PROTO_REDISTRUTE_SIZE;
         u1ProtoIndex++)
    {
        pOspfCxt->au4MetricValue[u1ProtoIndex] = AS_EXT_DEF_METRIC;
        pOspfCxt->au1MetricType[u1ProtoIndex] = TYPE_2_METRIC;
    }
    KW_FALSEPOSITIVE_FIX (pOspfCxt->pAsExtLsaRBRoot->SemId);
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrEnableInCxt                                             */
/*                                                                           */
/* Description  : Enables ospf in the given context                          */
/*                                                                           */
/* Input        : pOspfCxt   -pointer to the ospf context                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FALSE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtrEnableInCxt (tOspfCxt * pOspfCxt)
{
    tRtmRegnId          RegnId;
    tNetIpRegInfo       RegInfo;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tAppInfo           *pAppInfo = NULL;

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "OSPF to be enabled\n");

    TMO_DLL_Init (&(pOspfCxt->origLsaDescLst));
#ifndef HIGH_PERF_RXMT_LST_WANTED
    TMO_DLL_Init (&(pOspfCxt->rxmtLst));
#endif
    TMO_DLL_Init_Node (&(pOspfCxt->vrfSpfNode));
    pOspfCxt->u1VrfSpfTmrStatus = OSPF_FALSE;

    pOspfCxt->u4ExtLsaChksumSum = 0;
    pOspfCxt->u4OriginateNewLsa = 0;
    pOspfCxt->u4RcvNewLsa = 0;
    pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;
    pOspfCxt->u1AsbSummaryLsaChanged = OSPF_FALSE;

    pOspfCxt->bDefaultAseLsaPresenc = OSPF_FALSE;

    pOspfCxt->u1PktType = 0;
    pOspfCxt->u1CnfgErrType = 0;
    IP_ADDR_COPY (pOspfCxt->pktSrcAddr, gNullIpAddr);
    pOspfCxt->u1TrapCount = 0;
    pOspfCxt->u1SwPeriodsElapsed = 0;
    pOspfCxt->bLsdbApproachingOvflTrapGen = OSPF_FALSE;
    pOspfCxt->u4MaxMtuSize = 0;

    /* The following routine initialises the ISM schedule queue structure */

    pOspfCxt->u4ActiveAreaCount = 0;

    IsmInitSchedQueue ();
    IsmInitSchedQueueInCxt (pOspfCxt, NULL);

    TmrSetTimer (&(pOspfCxt->trapLimitTimer), TRAP_LIMIT_TIMER,
                 TRAP_LIMIT_TIMER_INTERVAL);
#ifdef RAWSOCK_WANTED
    if (gOsRtr.u4ActiveCxtCount < SYS_DEF_MAX_NUM_CONTEXTS)
    {
        /* Create the Sockets on each interface when
         *  the Router Admin Status is Enabled */
        if (OspfCreateSocket (pOspfCxt->u4OspfCxtId) == OSPF_FAILURE)
        {
            return OSPF_FAILURE;
        }
    }
#endif /* End of RAWSOCK_WANTED */

#ifdef DEBUG_WANTED
    /* NOTE: Uncomment for debugging */
    /* TmrSetTimer (&(pOspfCxt->dumpTimer), DUMP_TIMER,
       NO_OF_TICKS_PER_SEC * 10 * 60); */
#endif

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "Enable All Interfaces\n");

    MEMSET ((UINT1 *) &RegInfo, 0, sizeof (tNetIpRegInfo));
#ifdef RAWSOCK_WANTED
    /* This Registration is done to get the information regarding interface
       Oper status changes */
    RegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    RegInfo.pIfStChng = OspfIfStateChgHdlr;
    RegInfo.pRtChng = NULL;
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.u1ProtoId = FS_OSPF_PROTO;
    RegInfo.u4ContextId = pOspfCxt->u4OspfCxtId;
    NetIpv4RegisterHigherLayerProtocol (&RegInfo);

#else
    /*
     * This routine is used to Register OSPF with IP.
     * And it is a call back function from IP to OSPF.
     * When interfacing with third party IP
     * this routine has to be taken care.
     */
    RegInfo.u2InfoMask |= (NETIPV4_IFCHG_REQ | NETIPV4_PROTO_PKT_REQ);
    RegInfo.pIfStChng = OspfIfStateChgHdlr;
    RegInfo.pRtChng = NULL;
    RegInfo.pProtoPktRecv = IpifRcvPkt;
    RegInfo.u1ProtoId = FS_OSPF_PROTO;
    RegInfo.u4ContextId = pOspfCxt->u4OspfCxtId;
    NetIpv4RegisterHigherLayerProtocol (&RegInfo);
#endif /* End of ifndef RAWSOCK_WANTED switch */

    if (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        RegnId.u2ProtoId = OSPF_ID;
        RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;
        RtmRegister (&RegnId, ACK_REQD_FROM_RTM, OspfRtmCallbackFn);

        if ((pOspfCxt->redistrAdmnStatus == OSPF_ENABLED) &&
            (pOspfCxt->u4RrdSrcProtoBitMask != 0))
        {
            /* EnQ redis enable message to RTM */
            RtmTxRedistributeEnableInCxt
                (pOspfCxt, pOspfCxt->u4RrdSrcProtoBitMask,
                 pOspfCxt->au1RMapName);
        }
    }

    if ((pOspfCxt->u1OpqCapableRtr == OSPF_TRUE) &&
        (pOspfCxt->u1RestartSupport != OSPF_RESTART_NONE))
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, GRACE_LSA_OPQ_TYPE);
        if (pAppInfo == NULL)
        {
            if (APP_ALLOC (pAppInfo) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC, pOspfCxt->u4OspfCxtId,
                          "GrSetRestartSupport: APP Alloc Failure\n");
                return OSPF_FAILURE;
            }
            pOspfCxt->pAppInfo[GRACE_LSA_OPQ_TYPE] = pAppInfo;

            pAppInfo->u4AppId = GRACE_LSA_OPQ_TYPE;
            pAppInfo->u1OpqType = GRACE_LSA_OPQ_TYPE;
            pAppInfo->u1LSATypesRegistered = TYPE9_BIT_MASK;
            pAppInfo->u4InfoFromOSPF = 0;
            pAppInfo->OpqAppCallBackFn = NULL;
            pAppInfo->u1Status = OSPF_VALID;

            /* Register with the opaque module. Instead of posting a message
             * to queue, Registration function is directly called */
            OpqAppRegMsgFrmQInCxt (pOspfCxt, GRACE_LSA_OPQ_TYPE);
        }

    }
    pOspfCxt->admnStat = OSPF_ENABLED;

    if (pOspfCxt->pBackbone == NULL)
    {
        pOspfCxt->pBackbone = AreaCreateBackBoneAreaInCxt (pOspfCxt);
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        OspfIfUp (pInterface);
    }

    pOspfCxt->u4MaxCost = 0;
    pOspfCxt->u4MinCost = 0;
    pOspfCxt->u4CurrSpfMaxCost = 0;
    pOspfCxt->u4CurrSpfMinCost = 0;
    ExtrtAddStaticAllInCxt (pOspfCxt);
#ifdef L3_SWITCHING_WANTED
    /* Do the hardware initialisation to receive OSPF packets */
    OspfFsNpOspfInit ();
#endif /* L3_SWITCHING_WANTED */

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "OSPF Enabled\n");
    gOsRtr.u4ActiveCxtCount++;
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrDeleteCxt                                               */
/*                                                                           */
/* Description  : Deletes ospf context                                       */
/*                                                                           */
/* Input        : pOspfCxt   -pointer to the ospf context                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FALSE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtrDeleteCxt (tOspfCxt * pOspfCxt)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;
    INT4                i4RetVal = OSPF_FAILURE;
#if defined (LNXIP4_WANTED) && defined (L3_SWITCHING_WANTED)
    UINT4               u4CfaIndex = OSPF_ZERO;
#endif

    if (RtrDisableInCxt (pOspfCxt) != OSPF_SUCCESS)
    {
        i4RetVal = OSPF_FAILURE;
    }

    while ((pLstNode = TMO_SLL_First (&(pOspfCxt->sortIfLst))) != NULL)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

/* Need to delete the KNET filters created w.r.t the interfaces created. */
#if defined (LNXIP4_WANTED) && defined (L3_SWITCHING_WANTED)
        if (ISS_HW_SUPPORTED ==
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
            u4CfaIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
            if (FsNpOspfCreateAndDeleteFilter (u4CfaIndex, DESTROY) ==
                FNP_FAILURE)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "OSPF Filter Deletion Failed\n");
                return SNMP_FAILURE;
            }
        }
#endif

        IfDelete (pInterface);
    }
    while ((pLstNode = TMO_SLL_First (&(pOspfCxt->virtIfLst))) != NULL)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IfDelete (pInterface);
    }
    RrdConfRtInfoDeleteAllInCxt (pOspfCxt);
    AreaDeleteAllInCxt (pOspfCxt);
    HostDeleteAllInCxt (pOspfCxt);
    ExtrtForceDeleteAllInCxt (pOspfCxt);
    RtcDeInitRt (pOspfCxt->pExtRtRoot, OSPF_INVALID_RT_ID);
    RtcDeInitRt (pOspfCxt->pOspfRt->pOspfRoot, OSPF_INVALID_RT_ID);

    /* Delete the RBTree for Type5 External LSAs */
    RBTreeDelete (pOspfCxt->pAsExtLsaRBRoot);
    pOspfCxt->pAsExtLsaRBRoot = NULL;

    /* Delete the HashList for Type5  LSAs */
    TMO_HASH_Delete_Table (pOspfCxt->pExtLsaHashTable, NULL);
    TMO_HASH_Delete_Table (pOspfCxt->pCandteLst2, NULL);
    pOspfCxt->pExtLsaHashTable = NULL;
    REDIST_REG_FREE (pOspfCxt->pRedistribute);
    ROUTING_TABLE_FREE (pOspfCxt->pOspfRt);
    TMO_HASH_Delete_Table (pOspfCxt->pCandteLst, NULL);
    gOsRtr.apOspfCxt[pOspfCxt->u4OspfCxtId] = NULL;
    if (pOspfCxt->pDistributeInFilterRMap != NULL)
    {
        RMAP_FILTER_FREE (pOspfCxt->pDistributeInFilterRMap);
        pOspfCxt->pDistributeInFilterRMap = NULL;
    }
    if (pOspfCxt->pDistanceFilterRMap != NULL)
    {
        RMAP_FILTER_FREE (pOspfCxt->pDistanceFilterRMap);
        pOspfCxt->pDistanceFilterRMap = NULL;
    }
    CONTEXT_FREE (pOspfCxt);
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrDisableInCxt                                            */
/*                                                                           */
/* Description  : Disables the OSPF protocol in the given  context           */
/*                                                                           */
/* Input        : pOspfCxt   -pointer to the ospf context                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FALSE                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtrDisableInCxt (tOspfCxt * pOspfCxt)
{

    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;
    tArea              *pArea;

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "OSPF To Be Disabled\n");

#ifdef L3_SWITCHING_WANTED
    /* Do the hardware initialisation to receive OSPF packets */
    OspfFsNpOspfDeInit ();
#endif /* L3_SWITCHING_WANTED */

    if ((pOspfCxt->contextStatus != DESTROY) &&
        ((pOspfCxt->u1OspfRestartState == OSPF_GR_SHUTDOWN) ||
         (pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)))
    {
        /* This router is already under going GR shutdown process
           so no need to disable this context */
        return OSPF_SUCCESS;
    }
    if (IsOpqApplicationRegisteredInCxt (pOspfCxt) == OSPF_SUCCESS)
    {
        OpqAppDeRegMsgFrmQInCxt (pOspfCxt, GRACE_LSA_OPQ_TYPE);
        OspfDeRegMsgToOpqApp (pOspfCxt);
    }

    LsuFlushAllSelfOrgLsasInCxt (pOspfCxt);
    ExtrtDeleteAllInCxt (pOspfCxt);

    pOspfCxt->admnStat = OSPF_DISABLED;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        AreaResetAggrEntries (pArea);
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IfDisable (pInterface);
    }
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IfDisable (pInterface);
    }

    LsuDeleteAllLsasInCxt (pOspfCxt);

    NetIpv4DeRegisterHigherLayerProtocolInCxt (pOspfCxt->u4OspfCxtId,
                                               FS_OSPF_PROTO);

    IsmInitSchedQueueInCxt (pOspfCxt, NULL);

    RtcClearAndDeleteRtFromIpInCxt (pOspfCxt, pOspfCxt->pOspfRt);

    /* Delete all the nodes from SPF tree in all areas */
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        RtcDeleteNodesFromSpf (pArea->pSpf);
    }

    TmrDeleteTimer (&(pOspfCxt->runRtTimer));

#ifdef DEBUG_WANTED
    /* NOTE: Uncomment for debugging */
    /* TmrDeleteTimer (&(pOspfCxt->dumpTimer)); */
#endif

    TmrDeleteTimer (&(pOspfCxt->trapLimitTimer));

    /* Delete the distance timer */
    TmrDeleteTimer (&(pOspfCxt->distanceTimer));

    TmrDeleteTimer (&pOspfCxt->exitOverflowTimer);

    pOspfCxt->u4ExtLsaChksumSum = 0;
    pOspfCxt->u4OriginateNewLsa = 0;
    pOspfCxt->u4RcvNewLsa = 0;
    pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;

    pOspfCxt->u1AsbSummaryLsaChanged = OSPF_FALSE;
    pOspfCxt->bOverflowState = OSPF_FALSE;

    if (pOspfCxt->u1VrfSpfTmrStatus == OSPF_TRUE)
    {
        /* when u1VrfSpfTmrStatus equal to OSPF_TRUE, then this ospf context
         * is added to the global vrfSpfRtcList, Route Calc is pending for this context. 
         * When disabling ospfcontext delete the node from the VrfSpfRtcList
         * and reset the u1VrfSpfTmrStatus flag to OSPF_FALSE
         */
        TMO_DLL_Delete (&(gOsRtr.vrfSpfRtcList), &(pOspfCxt->vrfSpfNode));
        pOspfCxt->u1VrfSpfTmrStatus = OSPF_FALSE;
    }

    if (RtmDeRegisterInCxt (pOspfCxt) == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }
    pOspfCxt->pRedistribute->u4MaxQMsgLength = 0;

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "OSPF Disabled\n");

    gOsRtr.u4ActiveCxtCount--;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    OspfCloseSocket (pOspfCxt->u4OspfCxtId);
#else
#ifdef RAWSOCK_WANTED
    if (gOsRtr.u4ActiveCxtCount == 0)
    {
        OspfCloseSocket (pOspfCxt->u4OspfCxtId);
    }
#endif
#endif
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrShutdownInCxt                                           */
/*                                                                           */
/* Description  : Shutdowns the OSPF protocol                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtrShutdownInCxt (tOspfCxt * pOspfCxt)
{
    if (RtrDeleteCxt (pOspfCxt) == OSPF_SUCCESS)
    {
        return OSPF_SUCCESS;
    }
    return OSPF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrRebootInCxt                                             */
/*                                                                           */
/* Description  : Restarts the OSPF protocol.                                */
/*                This function is to be used to restart the OSPF once it has*/
/*                been shut down using RtrShutdown.                         */
/*                NOTE: This routine is created to simulate the shutdown and */
/*                rebooting the OSPF in the development environment.         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtrRebootInCxt (UINT4 u4OspfCxtId)
{
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if (u4OspfCxtId >= u4MaxCxt)
    {
        return OSPF_FAILURE;
    }

    if (RtrCreateCxt (u4OspfCxtId, OSPF_ENABLED) == OSPF_SUCCESS)
    {
        return OSPF_SUCCESS;
    }
    return OSPF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrSetRouterId                                             */
/*                                                                           */
/* Description  : Disables the OSPF protocol. Updates the RouterId and then  */
/*                Enables the OSPF protocol.                                 */
/*                                                                           */
/* Input        : rtrId  : RouterID to be updated                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS/FAILURE                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtrSetRouterIdInCxt (tOspfCxt * pOspfCxt, tRouterId RtrId)
{
    if ((UtilIpAddrComp (pOspfCxt->rtrId, gNullIpAddr) != OSPF_EQUAL) &&
        (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE))
    {
        /* routerid is changed during graceful restart mode */
        /* so exit GR with exit reason as TOPOLOGY change */
        IP_ADDR_COPY (&(pOspfCxt->rtrId), RtrId);
        GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_TOP_CHG);
        return OSPF_SUCCESS;
    }

    if ((UtilIpAddrComp (pOspfCxt->rtrId, gNullIpAddr) == OSPF_EQUAL) &&
        (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE))
    {
        IP_ADDR_COPY (&(pOspfCxt->rtrId), RtrId);
        return OSPF_SUCCESS;
    }

    if (RtrDisableInCxt (pOspfCxt) == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }
    IP_ADDR_COPY (&(pOspfCxt->rtrId), RtrId);
    if (RtrEnableInCxt (pOspfCxt) == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrSetExtLsdbLimit                                       */
/*                                                                           */
/* Description  : Set Ext Lsdb Limit if router contains more ext lsa       */
/*                whatever limit is configured */
/*                                                                           */
/* Input        : None                                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtrSetExtLsdbLimitInCxt (tOspfCxt * pOspfCxt)
{
    if (!(pOspfCxt->bOverflowState == OSPF_TRUE) &&
        (pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
        (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT (pOspfCxt)
         >= pOspfCxt->i4ExtLsdbLimit))
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
                  "Rtr Enters OvrFlw State\n");
        RtrEnterOverflowStateInCxt (pOspfCxt);
    }
    else if ((pOspfCxt->i4ExtLsdbLimit == NO_LIMIT) &&
             (pOspfCxt->bOverflowState == OSPF_TRUE))
    {
        /* Lsdb Limit is reset to -1. So, stop exit overflow timer and
         * start generating non-default LSA's */
        pOspfCxt->bOverflowState = OSPF_FALSE;

        TmrDeleteTimer (&(pOspfCxt->exitOverflowTimer));

        LsuGenerateNonDefaultAsExtLsasInCxt (pOspfCxt);

        OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                  "Rtr leaves overflow state as lsdb limit is NO_LIMIT\n");
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrSetExitOverflowInterval                             */
/*                                                                           */
/* Description  : Set Exit overflow interval                        */
/*                                                                           */
/* Input        : None                                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtrSetExitOverflowIntervalInCxt (tOspfCxt * pOspfCxt)
{
    if (pOspfCxt->bOverflowState == OSPF_TRUE)
    {
        if (pOspfCxt->u4ExitOverflowInterval != DONT_LEAVE_OVERFLOW_STATE)
        {
            TmrSetTimer (&(pOspfCxt->exitOverflowTimer), EXIT_OVERFLOW_TIMER,
                         UtilJitter (NO_OF_TICKS_PER_SEC *
                                     (pOspfCxt->u4ExitOverflowInterval),
                                     OSPF_JITTER));
        }
    }
    return OSPF_SUCCESS;
}

/************************************************************************/
/* Function    : RtrHandleABRStatChng                                   */
/*                                                                      */
/* Description : Handles Change in ABR status.                          */
/*                                                                      */
/* Input       : u1AbrFlag.                                             */
/*                                                                      */
/* Output      : None                                                   */
/*                                                                      */
/* Returns     : VOID                                                   */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RtrHandleABRStatChngInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    UINT1               u1AbrFlag;

    u1AbrFlag = RtrCheckABRStatInCxt (pOspfCxt);

    /* There is no change in the ABR state of the Router */
    if (pOspfCxt->bAreaBdrRtr == u1AbrFlag)
    {
        return;
    }

    pOspfCxt->bAreaBdrRtr = u1AbrFlag;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        /* Generating Router LSA into attached areas
         * to indicate ABR Status change */
        if (pArea->u4ActIntCount != 0)
        {
            OlsGenerateLsa (pArea, ROUTER_LSA, &(pOspfCxt->rtrId), NULL);
        }
    }
    if (pOspfCxt->u4NssaAreaCount > RAG_NO_CHNG)
    {
        if (u1AbrFlag == OSPF_TRUE)
        {
            /* Rtr has become ABR, so Trigger NSSA
             * Translator Election
             */
            NssaFsmTrgrTrnsltrElnInCxt (pOspfCxt);
        }
        else
        {
            /* Rtr has lost ABR Status, invoke */
            NssaABRStatLostInCxt (pOspfCxt);
        }
    }
}

/************************************************************************/
/* Function    : RtrSetABRType                                          */
/*                                                                      */
/* Description : This function will set the ABR Type and takes          */
/*               necessary action.                                      */
/*                                                                      */
/* Input       : u4ABRType : Type of the ABR to be set.                 */
/*                                                                      */
/* Output      : None                                                   */
/*                                                                      */
/* Returns     : VOID                                                   */
/*                                                                      */
/************************************************************************/
PUBLIC INT4
RtrSetABRTypeInCxt (tOspfCxt * pOspfCxt, UINT4 u4ABRType)
{

    pOspfCxt->u4ABRType = u4ABRType;
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        return OSPF_SUCCESS;
    }
    RtrHandleABRStatChngInCxt (pOspfCxt);

    /* Calculate the Entire Routing Table, because 
     * inter area route calculation will differ for different 
     * types of ABR's */
    if (pOspfCxt->u1RtrNetworkLsaChanged == OSPF_TRUE)
    {
        TmrStopTimer (gTimerLst, &(pOspfCxt->runRtTimer.timerNode));
    }
    pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
    RtcSetRtTimer (pOspfCxt);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtrCheckABRStat                                            */
/*                                                                           */
/* Description  : Reference : RFC 2328 and RFC 3509                          */
/*                This function checks whether the router is ABR or not.     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if ABR                                          */
/*                OSPF_FALSE, if not ABR                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
RtrCheckABRStatInCxt (tOspfCxt * pOspfCxt)
{
    switch (pOspfCxt->u4ABRType)
    {
        case STANDARD_ABR:
            /* Ref: RFC 2328 -- Section 3.3 */
            if (pOspfCxt->u4ActiveAreaCount > 1)
            {
                return OSPF_TRUE;
            }
            break;

        case CISCO_ABR:
            /* Ref: RFC 3509 -- Section 2.1 */
            if ((pOspfCxt->u4ActiveAreaCount > 1) &&
                (IS_ACTIVELY_ATTACHED_AREA (pOspfCxt->pBackbone)))
            {
                return OSPF_TRUE;
            }
            break;

        case IBM_ABR:
            /* Ref: RFC 3509 -- Section 2.1 */
            if ((pOspfCxt->u4ActiveAreaCount > 1) &&
                (IS_CONFIGURED_BACKBONE_IN_CXT (pOspfCxt)))
            {
                return OSPF_TRUE;
            }
            break;

        default:
            return OSPF_FALSE;
    }
    return OSPF_FALSE;
}

/*----------------------------------------------------------------------------*/
/*                           End of file osrtr.c                              */
/*----------------------------------------------------------------------------*/
