/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osextrt.c,v 1.26 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures for configuration 
 *             of AS external routes
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtRouteTblInit                                         */
/*                                                                           */
/* Description     : This routine creates the Trie DataStructure for storing */
/*                   External Routes                                         */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/
INT4
ExtRouteTblInitInCxt (tOspfCxt * pOspfCxt)
{
    tTrieCrtParams      ospfTrieCrtParams;
    UINT4               u4MaxRts = 0;

    ospfTrieCrtParams.u2KeySize = 2 * sizeof (UINT4);
    ospfTrieCrtParams.u4Type = OSPF_EXT_RT_TYPE_IN_CXT (pOspfCxt);
    ospfTrieCrtParams.AppFns = &(OspfTrieLibFuncs);
    ospfTrieCrtParams.u1AppId = OSPF_EXT_RT_ID;
    u4MaxRts = OSPF_MIN (MAX_OSPF_EXT_ROUTES,
                         FsOSPFSizingParams[MAX_OSPF_EXT_ROUTES_SIZING_ID].
                         u4PreAllocatedUnits) + 1;
    ospfTrieCrtParams.u4NumRadixNodes = u4MaxRts + 1;
    ospfTrieCrtParams.u4NumLeafNodes = u4MaxRts;
    ospfTrieCrtParams.u4NoofRoutes = u4MaxRts;
    ospfTrieCrtParams.bValidateType = OSIX_FALSE;
    ospfTrieCrtParams.bPoolPerInst = OSIX_FALSE;
    ospfTrieCrtParams.bSemPerInst = OSIX_FALSE;
    pOspfCxt->pExtRtRoot = TrieCreateInstance (&ospfTrieCrtParams);

    if (pOspfCxt->pExtRtRoot == NULL)
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtAdd                                                */
/*                                                                           */
/* Description     : This procedure creates the AS external route entry,     */
/*                   initializes it and adds the specified AS external route */
/*                   to the router structure                                 */
/*                                                                           */
/* Input           : ipNetNum       : IP Address of the network              */
/*                   ipAddrMask     : Address Mask of the network            */
/*                   i1Tos          : Type of service                        */
/*                   u4Metric       : Cost of the route                      */
/*                   u1MetricType   : External type 1 or type 2 metric type  */
/*                   u4ExtTag       : External Route Tag info.               */
/*                   fwdAddr        : Forwarding IP Address                  */
/*                   u4FwdIfIndex   : Interface Index of the forwarding      */
/*                                    Address                                */
/*                   fwdNextHop     : Next Hop IP Address                    */
/*                   u2SrcProto     : Source ProtocolId. This field is       */
/*                                    used for the routes learned from RTM.  */
/*                   rowStatus      : Row Status. This field is used for     */
/*                                    the routes learned from RTM.           */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to Added External Route Entry.                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC tExtRoute   *
ExtrtAddInCxt (tOspfCxt * pOspfCxt,
               tIPADDR ipNetNum,
               tIPADDRMASK ipAddrMask,
               INT1 i1Tos,
               UINT4 u4Metric,
               UINT1 u1MetricType,
               UINT4 u4ExtTag,
               tIPADDR fwdAddr,
               UINT2 u4FwdIfIndex,
               tIPADDR fwdNextHop, UINT2 u2SrcProto, UINT4 u4RowStatus,
               UINT1 u1Level)
{
    tExtRoute          *pExtRoute = NULL;
    tExtRoute          *pExtRouteOld = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    UINT1               u1Tos;
    UINT4               au4Index[2];
    tInputParams        inParams;
    tOutputParams       outParams;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: ExtrtAdd\n");
    OSPF_TRC3 (OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
               pOspfCxt->u4OspfCxtId,
               "Add route Dest IP addr = %s, Addr Mask = %s, fwdAddr = %s \n",
               OspfPrintIpAddr (ipNetNum), OspfPrintIpAddr (ipAddrMask),
               OspfPrintIpAddr (fwdAddr));

    if (i1Tos >= OSPF_MAX_METRIC)
    {
        return NULL;
    }

    pExtRouteOld =
        GetFindExtRouteInCxt (pOspfCxt, (tIPADDR *) ipNetNum,
                              (tIPADDR *) ipAddrMask);

    if (pExtRouteOld == NULL)
    {
        if (EXT_ROUTE_ALLOC (pExtRoute) == NULL)
        {

            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId, "EXT ROUTE Alloc Failed\n");

            return NULL;
        }

        pExtRoute->pAsExtAddrRange = NULL;
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            pExtRoute->aMetric[u1Tos].rowStatus = INVALID;
        }

        TMO_SLL_Init_Node (&(pExtRoute->nextExtRoute));
        TMO_SLL_Init_Node (&(pExtRoute->nextExtRouteInRange));

        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = UtilFindMaskLen (OSPF_CRU_BMC_DWFROMPDU
                                                (ipAddrMask));
        inParams.pRoot = pOspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPF_EXT_RT_ID;
        au4Index[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (ipNetNum));
        au4Index[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (ipAddrMask));

        inParams.Key.pKey = (UINT1 *) au4Index;

        outParams.pAppSpecInfo = NULL;
        outParams.Key.pKey = NULL;

        if (TrieAdd (&inParams, (VOID *) pExtRoute, &outParams) != TRIE_SUCCESS)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "Failed to add the route entry to TRIE\n");
            EXT_ROUTE_FREE (pExtRoute);
            return NULL;
        }
        pOspfCxt->u4ExtRtCount++;
    }
    else
    {
        pExtRoute = pExtRouteOld;
    }

    pExtRoute->u2SrcProto = u2SrcProto;
    pExtRoute->u1ExtRtDelFlag = DONT_DELETE_EXT_ROUTE;

    IP_ADDR_COPY (pExtRoute->ipNetNum, ipNetNum);
    IP_ADDR_COPY (pExtRoute->ipAddrMask, ipAddrMask);

    pExtRoute->aMetric[(INT2) i1Tos].u4Value = u4Metric;
    pExtRoute->aMetric[(INT2) i1Tos].rowStatus = (UINT1) u4RowStatus;

    pExtRoute->extrtParam[(INT2) i1Tos].u1MetricType = u1MetricType;
    pExtRoute->extrtParam[(INT2) i1Tos].u4ExtTag = u4ExtTag;
    IP_ADDR_COPY (pExtRoute->extrtParam[(INT2) i1Tos].fwdAddr, fwdAddr);
    pExtRoute->extrtParam[(INT2) i1Tos].u4FwdIfIndex = u4FwdIfIndex;
    IP_ADDR_COPY (pExtRoute->extrtParam[(INT2) i1Tos].fwdNextHop, fwdNextHop);
    pExtRoute->u1Level = u1Level;

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Ext Route Added during GR\n");
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "External LSA can generated only after exiting GR\n");

        OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: ExtrtAdd\n");

        return pExtRoute;
    }

    if (((pLsaInfo = LsuDatabaseLookUp (AS_EXT_LSA,
                                        &(pExtRoute->ipNetNum),
                                        &(pExtRoute->ipAddrMask),
                                        &(pOspfCxt->rtrId),
                                        NULL,
                                        (UINT1 *) pOspfCxt->pBackbone)) != NULL)
        && (pLsaInfo->u1LsaFlushed == OSPF_TRUE))
    {
        if (pLsaInfo->pLsaDesc != NULL)
        {
            pLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pExtRoute;
            pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
            LsuDeleteNodeFromAllRxmtLst (pLsaInfo);
        }
        pLsaInfo->u1LsaFlushed = OSPF_FALSE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId, "Ext Route Added\n");

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: ExtrtAdd\n");

    return pExtRoute;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtActivate                                          */
/*                                                                           */
/* Description     : This routine activates the particular exterior route    */
/*                                                                           */
/* Input           : pExtRoute :   ptr to the external route structure     */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ExtrtActivateInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{
    /* originate new ASE lsa */
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* new ASE lsa cannot be orginated in GR Mode */
        return OSPF_SUCCESS;
    }
    RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtInactivate                                         */
/*                                                                           */
/* Description     : This routine inactivates the particular exterior route  */
/*                                                                           */
/* Input           : pExtRoute :   ptr to the external route structure       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Return DO_NOT_FLUSH or FLUSH                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
ExtrtInactivateInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{
    UINT1               u1TosIndex = 0;
    UINT1               u1Flag = FLUSH;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: ExtrtInactivate\n");

    if ((pExtRoute->aMetric[u1TosIndex].rowStatus) != INVALID)
    {
        RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);
        u1Flag = DO_NOT_FLUSH;
    }
    else
    {
        u1Flag = FLUSH;
    }

    if (u1Flag == FLUSH)
    {
        RagDelRouteFromAggAddrRangeInCxt (pOspfCxt, pExtRoute);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: ExtrtInactivate\n");

    return u1Flag;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtParamChange                                      */
/*                                                                           */
/* Description     : This routine generates an LSA if any of the exterior    */
/*                   route parameters change.                                */
/*                                                                           */
/* Input           : pExtRoute :   ptr to the external route structure     */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ExtrtParamChangeInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{

    /* concerned lsa is to be regenerated */
    RagDelRouteFromAggAddrRangeInCxt (pOspfCxt, pExtRoute);

    RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtDelete                                             */
/*                                                                           */
/* Description     : This routine deletes the exterior route.                */
/*                                                                           */
/* Input           : pExtRoute :   ptr to the external route structure       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ExtrtDeleteInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{
    UINT1               u1Flag;
    tArea              *pArea = NULL;
    tIPADDR             ipNetNum;
    tIPADDR             ipAddrMask;

    IP_ADDR_COPY (ipNetNum, pExtRoute->ipNetNum);
    IP_ADDR_COPY (ipAddrMask, pExtRoute->ipAddrMask);

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
               "Delete route Dest Ip Addr = %s, Addr Mask = %s\n",
               OspfPrintIpAddr (pExtRoute->ipNetNum),
               OspfPrintIpAddr (pExtRoute->ipAddrMask));
    if (pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
    {
        /* Delete the route entry from Trie during GR restart process */
        ExtrtDeleteFromTrieInCxt (pOspfCxt, pExtRoute);
        EXT_ROUTE_FREE (pExtRoute);
        return OSPF_SUCCESS;
    }
    else if (pOspfCxt->u1OspfRestartState == OSPF_GR_SHUTDOWN)
    {
        /* Route entry deletion will be taken care during GR shutdown process */
        return OSPF_SUCCESS;
    }

    u1Flag = ExtrtInactivateInCxt (pOspfCxt, pExtRoute);

    if (u1Flag == FLUSH)
    {
        /* Delete the route entry from Trie */
        ExtrtDeleteFromTrieInCxt (pOspfCxt, pExtRoute);
        EXT_ROUTE_FREE (pExtRoute);
    }

    /* case occurs when the external route is deleted is default route 
     * and the router is ABR, it has to generate default
     * type-7 LSA to NSSA */
    if ((UtilIpAddrComp (ipNetNum, gNullIpAddr) == OSPF_EQUAL)
        && (UtilIpAddrComp (ipAddrMask, gNullIpAddr) == OSPF_EQUAL)
        && (pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if (pArea->u4AreaType == NSSA_AREA)
            {
                OlsGenerateLsa (pArea, DEFAULT_NSSA_LSA, &gDefaultDest,
                                (UINT1 *) pArea);
            }
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtDeleteAll                                          */
/*                                                                           */
/* Description     : This routine deletes all the AS external routes         */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ExtrtDeleteAllInCxt (tOspfCxt * pOspfCxt)
{

    tExtRoute          *pExtRoute = NULL;
    tExpRtNode         *pRtmRouteMsg = NULL;
    tExpRtNode         *pRtmNextRouteMsg = NULL;
    UINT4               au4Key[2];
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    VOID               *pLeafNode = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {

        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            if (pExtRoute->u1ExtRtIsStatic == OSIX_TRUE)
            {
                pExtRoute->aMetric[DECODE_TOS ((INT1) TOS_0)].rowStatus =
                    INVALID;
                ExtrtInactivateInCxt (pOspfCxt, pExtRoute);
                pLeafNode = inParams.pLeafNode;
                pExtRoute = NULL;
                continue;
            }
            pExtRoute->aMetric[DECODE_TOS ((INT1) TOS_0)].rowStatus = INVALID;
            ExtrtDeleteInCxt (pOspfCxt, pExtRoute);
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }
    /* If any unprocessed Routes from RTM,Delete them */
    /* Take the DataLock for accessing RTM Route List */
    OsixSemTake (gOsRtr.RtmLstSemId);
    if ((pRtmRouteMsg = (tExpRtNode *)
         TMO_SLL_First (&gOsRtr.RtmRtLst)) == NULL)
    {
        OsixSemGive (gOsRtr.RtmLstSemId);
        return;
    }
    do
    {
        pRtmNextRouteMsg = (tExpRtNode *) TMO_SLL_Next ((&gOsRtr.RtmRtLst),
                                                        &(pRtmRouteMsg->
                                                          NextRt));
        if (pRtmRouteMsg->RtInfo.u4ContextId == pOspfCxt->u4OspfCxtId)
        {
            TMO_SLL_Delete (&gOsRtr.RtmRtLst, &(pRtmRouteMsg->NextRt));
            RTM_ROUTE_FREE (pRtmRouteMsg);
        }
        pRtmRouteMsg = pRtmNextRouteMsg;

    }
    while (pRtmRouteMsg != NULL);
    /* Release the DataLock for accessing RTM Route List */
    OsixSemGive (gOsRtr.RtmLstSemId);
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtAddStaticAllInCxt                                  */
/*                                                                           */
/* Description     : This routine adds all the static AS external routes     */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ExtrtAddStaticAllInCxt (tOspfCxt * pOspfCxt)
{

    tExtRoute          *pExtRoute = NULL;
    UINT4               au4Key[2];
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    VOID               *pLeafNode = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            if (pExtRoute->u1ExtRtIsStatic == OSIX_TRUE)
            {
                pExtRoute->aMetric[DECODE_TOS ((INT1) TOS_0)].rowStatus =
                    ACTIVE;
                ExtrtActivateInCxt (pOspfCxt, pExtRoute);
            }
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtForceDeleteAllInCxt                                */
/*                                                                           */
/* Description     : This routine deletes all the AS external routes         */
/*                   including static routes                                 */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
ExtrtForceDeleteAllInCxt (tOspfCxt * pOspfCxt)
{

    tExtRoute          *pExtRoute = NULL;
    tExpRtNode         *pRtmRouteMsg = NULL;
    tExpRtNode         *pRtmNextRouteMsg = NULL;
    UINT4               au4Key[2];
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    VOID               *pLeafNode = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {

        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            pExtRoute->aMetric[DECODE_TOS ((INT1) TOS_0)].rowStatus = INVALID;
            ExtrtDeleteInCxt (pOspfCxt, pExtRoute);
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }
    /* If any unprocessed Routes from RTM,Delete them */
    /* Take the DataLock for accessing RTM Route List */
    OsixSemTake (gOsRtr.RtmLstSemId);
    if ((pRtmRouteMsg = (tExpRtNode *)
         TMO_SLL_First (&gOsRtr.RtmRtLst)) == NULL)
    {
        OsixSemGive (gOsRtr.RtmLstSemId);
        return;
    }
    do
    {
        pRtmNextRouteMsg = (tExpRtNode *) TMO_SLL_Next ((&gOsRtr.RtmRtLst),
                                                        &(pRtmRouteMsg->
                                                          NextRt));
        if (pRtmRouteMsg->RtInfo.u4ContextId == pOspfCxt->u4OspfCxtId)
        {
            TMO_SLL_Delete (&gOsRtr.RtmRtLst, &(pRtmRouteMsg->NextRt));
            RTM_ROUTE_FREE (pRtmRouteMsg);
        }
        pRtmRouteMsg = pRtmNextRouteMsg;

    }
    while (pRtmRouteMsg != NULL);
    /* Release the DataLock for accessing RTM Route List */
    OsixSemGive (gOsRtr.RtmLstSemId);
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtRtAddDefaultValues                                 */
/*                                                                           */
/* Description     : This procedure creates the AS external route entry,     */
/*                   with Default Values and adds the specified              */
/*                   AS external route                                       */
/*                                                                           */
/* Input           : extRouteDestn : IP Address of the Destn network         */
/*                   extRouteMask    : Address Mask of the Destn             */
/*                   i4ExtRouteTos   : Type of service                       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS/OSPF_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC tExtRoute   *
ExtrtRtAddDefaultValuesInCxt (tOspfCxt * pOspfCxt, tIPADDR extRouteDest,
                              tIPADDRMASK extRouteMask, INT4 i4ExtRouteTOS)
{

    tExtRoute          *pExtRoute = NULL;
    UINT4               u4MinVal = 0;
    tInterface         *pTmpInterface = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: ExtrtRtAddDefaultEntry\n");
    OSPF_TRC2 (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Dest IP addr = %x, Addr Mask = %x\n",
               OSPF_CRU_BMC_DWFROMPDU (extRouteDest),
               OSPF_CRU_BMC_DWFROMPDU (extRouteMask));

    if ((pTmpInterface =
         IfGetMinCostIfInCxt (pOspfCxt, (INT1) i4ExtRouteTOS, NULL)) != NULL)
    {
        u4MinVal = pTmpInterface->aIfOpCost[(INT2) i4ExtRouteTOS].u4Value;
    }

    if (pTmpInterface == NULL)
    {
        return NULL;
    }
    if ((pExtRoute = ExtrtAddInCxt (pOspfCxt, extRouteDest, extRouteMask,
                                    (INT1) i4ExtRouteTOS, u4MinVal,
                                    DEFAULT_METRIC_TYPE, DEFAULT_EXTRT_TAG,
                                    gNullIpAddr, DEFAULT_FWD_IF_INDEX,
                                    gNullIpAddr, STATIC_RT, INVALID,
                                    INVALID)) == NULL)
    {
        return NULL;
    }
    pExtRoute->aMetric[((INT1) i4ExtRouteTOS)].rowStatus = ACTIVE;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: ExtrtRtAddDefaultEntry\n");
    return (pExtRoute);
}

/************************************************************************/
/*                                        */
/* Function        :   ExtrtLsaPolicyHandler               */
/*                                         */
/* Description         : This routine decides whether Type 5 LSAs need */
/*                   to be generated/deleted        */
/* Input               : None                           */
/*                                        */
/* Output              : None                           */
/*                                       */
/* Returns             : VOID                         */
/*                                         */
/************************************************************************/
PUBLIC VOID
ExtrtLsaPolicyHandlerInCxt (tOspfCxt * pOspfCxt)
{

    UINT4               u4FlagVal;
    UINT4               u4HashKey = 0;
    tArea              *pArea;
    tInterface         *pLstIf;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pTmpLstNode;
    tLsaInfo           *pLsaInfo;
    tExtRoute          *pExtRoute;
    tLsaInfo           *pType7LsaInfo;
    tOsDbNode          *pOsDbNode = NULL;
    tLINKSTATEID        linkStateId;
    tIPADDRMASK         destIpAddrMask;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    tOsDbNode          *pOsNextDbNode = NULL;

    u4FlagVal = pOspfCxt->bGenType5Flag;
    pOspfCxt->bGenType5Flag = OSPF_FALSE;
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLstIf = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((pLstIf->pArea->u4AreaType == NORMAL_AREA) &&
            (pLstIf->ifStatus == ACTIVE))
        {
            pOspfCxt->bGenType5Flag = OSPF_TRUE;
            break;
        }
    }

    if ((u4FlagVal == OSPF_FALSE) && (pOspfCxt->bGenType5Flag == OSPF_TRUE))
    {
        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pOspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPF_EXT_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;

        if (TrieGetFirstNode (&inParams,
                              &pAppSpecPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return;
        }

        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;

            RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);

            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipNetNum));
            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipAddrMask));
            inParams.Key.pKey = (UINT1 *) au4Key;
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }
    else if ((u4FlagVal == OSPF_TRUE)
             && (pOspfCxt->bGenType5Flag == OSPF_FALSE))
    {
        TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
        {
            TMO_HASH_DYN_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                                      pOsDbNode, pOsNextDbNode, tOsDbNode *)
            {
                OSPF_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode,
                                    pTmpLstNode, tTMO_SLL_NODE *)
                {
                    pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo,
                                                  nextLsaInfo, pLstNode);
                    OS_MEM_CPY (destIpAddrMask,
                                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE),
                                MAX_IP_ADDR_LEN);
                    LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
                    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
                    {
                        if (pArea->u4AreaType == NSSA_AREA)
                        {
                            pType7LsaInfo =
                                LsuSearchDatabase (NSSA_LSA, &(linkStateId),
                                                   &(pOspfCxt->rtrId), NULL,
                                                   (UINT1 *) pArea);
                            if (pType7LsaInfo != NULL)
                            {
                                pType7LsaInfo->pLsaDesc->
                                    u1MinLsaIntervalExpired = OSPF_TRUE;
                                OlsSignalLsaRegenInCxt (pOspfCxt,
                                                        SIG_NEXT_INSTANCE,
                                                        (UINT1 *)
                                                        pType7LsaInfo->
                                                        pLsaDesc);
                            }
                        }
                    }
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function        : ExtrtDeleteFromTrie                                     */
/*                                                                           */
/* Description     : This routine deletes the exterior route from the Trie  */
/*                                                                           */
/* Input           : pExtRoute :   ptr to the external route structure       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ExtrtDeleteFromTrieInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4Index[2];

    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = UtilFindMaskLen (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipAddrMask));
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_RT_ID;
    au4Index[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
    au4Index[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));
    inParams.Key.pKey = (UINT1 *) au4Index;

    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;
    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
               "Delete route Dest Ip Addr = %s, Addr Mask = %s\n",
               OspfPrintIpAddr (pExtRoute->ipNetNum),
               OspfPrintIpAddr (pExtRoute->ipAddrMask));
    if (TrieRemove (&inParams, &outParams, pTemp) != TRIE_SUCCESS)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Failed to delete the route entry" "from trie\n");
    }
    pOspfCxt->u4ExtRtCount--;
}

/*----------------------------------------------------------------------*/
/*                     End of the file  osextrt.c                       */
/*----------------------------------------------------------------------*/
