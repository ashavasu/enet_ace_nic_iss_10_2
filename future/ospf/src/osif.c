/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osif.c,v 1.54 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures for creating and
 *             deleting interface structures and handling interface
 *             up/down/loopback/unloop indications from lower 
 *             levels.
 *
 *******************************************************************/

#include "osinc.h"
#include "stdoslow.h"
#include "fsmioscli.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID        IfAddToSortIfLstInCxt
PROTO ((tOspfCxt * pOspfCxt, tInterface * pInterface));
PRIVATE VOID        IfFindAndAddToAreaInCxt
PROTO ((tOspfCxt * pOspfCxt, tInterface * pInterface));
PRIVATE VOID IfAssoIfWithHost PROTO ((tInterface * pInterface));
PRIVATE VOID IfLoopback PROTO ((tInterface * pInterface));
PRIVATE VOID IfUnloop PROTO ((tInterface * pInterface));

PRIVATE VOID IfSendStateChgTrap PROTO ((tInterface * pInterface));

/*****************************************************************************/
/*                                                                           */
/* Function     : IfCreateInCxt                                              */
/*                                                                           */
/* Description  : Creates a new interface structure with the fields set to   */
/*                the value specified. This procedure also adds the new      */
/*                interface structure to the interface hash table,           */
/*                sort_if_list and ifsInArea list.                           */
/*                                                                           */
/* Input        : pOspfCxt            : Ospf Context Pointer                 */
/*                ipAddr              : IP address                           */
/*                ipAddrMask          : IP address mask                      */
/*                u4AddrlessIf        : index to identify address less       */
/*                                        interfaces                         */
/*                u4IfIndex           : used to calculate cost               */
/*                u4MtuSize           : maximum size of packet that can be   */
/*                                        transmitted on the specified       */
/*                                        interface                          */
/*                u4IfSpeed           : type of network attached to the      */
/*                                        specified interface                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the created interface, if successfully created  */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
IfCreateInCxt (tOspfCxt * pOspfCxt, tIPADDR ipAddr,
               tIPADDRMASK ipAddrMask,
               UINT4 u4AddrlessIf,
               UINT4 u4IfIndex,
               UINT4 u4MtuSize, UINT4 u4IfSpeed, UINT1 u1IfType,
               UINT1 u1IfOperStat)
{

    tInterface         *pInterface;
    tOpqLSAInfo         opqLSAInfo;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaDesc           *pLsaDesc = NULL;
    tLSASEQNUM          grSeqNum = 0;
    UINT4               u4RemGracePeriod = 0;
    UINT1              *pu1CurrPtr = NULL;
    UINT1               u1NewDescFlag = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC : IfCreateInCxt \n");

    /* Allocate buffer for interface data structure */
    if (IF_ALLOC (pInterface) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "If Alloc Failure\n");
        return NULL;
    }

    /* Initialize variables */
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : IfSetDefaultValues \n");

    IfSetDefaultValues (pInterface, INVALID);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : IfSetDefaultValues \n");

    if (u4AddrlessIf == 0)
    {
        IP_ADDR_COPY (pInterface->ifIpAddr, ipAddr);
        IP_ADDR_COPY (pInterface->ifIpAddrMask, ipAddrMask);
    }

    /*
     * The Interface oper status which is to be got from lower layers,
     * is obtained as one of the parameter which is in turn obtained from IP. 
     */
    pInterface->operStatus = u1IfOperStat;

    pInterface->u4AddrlessIf = u4AddrlessIf;
    pInterface->u4MtuSize = u4MtuSize;
    pInterface->u1NetworkType = u1IfType;
    pInterface->rowMask = IF_TYPE_MASK;
    if (u4IfSpeed == 0)
        pInterface->aIfOpCost[TOS_0].u4Value = 0xffff;
    else if (u4IfSpeed > TEN_POWER_EIGHT)
        pInterface->aIfOpCost[TOS_0].u4Value = 1;
    else
        pInterface->aIfOpCost[TOS_0].u4Value = TEN_POWER_EIGHT / u4IfSpeed;

    pInterface->aIfOpCost[TOS_0].rowStatus = ACTIVE;
    pInterface->u4IfIndex = u4IfIndex;

    /* Add this interface to hash table & sorted if list */
    TMO_HASH_Add_Node (gOsRtr.pIfHashTable, &(pInterface->nextIfNode),
                       IF_HASH_FN (pInterface->u4IfIndex), NULL);
    IfAddToSortIfLstInCxt (pOspfCxt, pInterface);
    IfFindAndAddToAreaInCxt (pOspfCxt, pInterface);
    /*   pOspfCxt can be back referenced from pArea in the pInterface */
    IfAssoIfWithHost (pInterface);

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC, pOspfCxt->u4OspfCxtId,
               "If %x.%d Created\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    if (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        /* This invokes Type 7 & router LSA orgination/flushing   
         * This should not be called when router is performing GR */

        /* The Router may become ABR if the ABR Type supported
         * by Router is IBM Type ABR */
        if (pOspfCxt->u4ABRType == IBM_ABR)
        {
            RtrHandleABRStatChngInCxt (pOspfCxt);
        }
    }
    if (pInterface->u4MtuSize > pOspfCxt->u4MaxMtuSize)
    {
        pOspfCxt->u4MaxMtuSize = pInterface->u4MtuSize;
    }
    if (pOspfCxt->u4MaxMtuSize > gOsRtr.u4MaxMtuSize)
    {
        gOsRtr.u4MaxMtuSize = pOspfCxt->u4MaxMtuSize;
    }

    if ((pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART) &&
        (pOspfCxt->u1RestartStatus == OSPF_RESTART_PLANNED))
    {
        /* Restore the cryptographic sequence number from
         * non-volatile storage */
        GrRestoreIntfRestartInfo (pInterface, &grSeqNum);

        if (grSeqNum != 0)
        {
            /* Construct the grace LSA and add the LSDB */
            MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));
            GrUtilConstructGraceLSA (pInterface->pArea->pOspfCxt,
                                     pInterface, &opqLSAInfo, NEW_LSA_INSTANCE);
            /* Create the LSA descriptor and add to the SLL */
            pLsaDesc = OlsGetLsaDescInCxt (pInterface->pArea->pOspfCxt,
                                           TYPE9_OPQ_LSA, &(opqLSAInfo.LsId),
                                           (UINT1 *) pInterface,
                                           &u1NewDescFlag);

            if (pLsaDesc == NULL)
            {
                LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
                return pInterface;
            }
            /* Add the LSA to LSDB */
            pLsaInfo = LsuAddToLsdb (pInterface->pArea, TYPE9_OPQ_LSA,
                                     &(opqLSAInfo.LsId),
                                     &(pInterface->pArea->pOspfCxt->rtrId),
                                     pInterface, NEW_LSA_INSTANCE,
                                     opqLSAInfo.pu1LSA);
            if (pLsaInfo == NULL)
            {
                LSA_DESC_FREE (pLsaDesc);
                LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
                return pInterface;
            }

            pLsaInfo->pLsaDesc = pLsaDesc;
            pLsaDesc->pLsaInfo = pLsaInfo;
            TMO_DLL_Add (&(pInterface->pArea->pOspfCxt->origLsaDescLst),
                         &(pLsaDesc->lsaDescNode));

            /* Add the sequence number and Age in LSA packet and
             * tLsaInfo structure */
            if (TmrGetRemainingTime
                (gTimerLst,
                 (tTmrAppTimer *)
                 (&(pInterface->pArea->pOspfCxt->graceTimer.timerNode)),
                 &u4RemGracePeriod) != TMR_SUCCESS)
            {
                u4RemGracePeriod = 0;
            }
            pu1CurrPtr = opqLSAInfo.pu1LSA;
            pLsaInfo->u2LsaAge =
                (UINT2) (pInterface->pArea->pOspfCxt->u4GracePeriod -
                         u4RemGracePeriod);
            LADD2BYTE (pu1CurrPtr, pLsaInfo->u2LsaAge);
            /* Skip options, type, link state id and advertising router id */
            pu1CurrPtr = pu1CurrPtr + 1 + 1 + MAX_IP_ADDR_LEN + MAX_IP_ADDR_LEN;
            /* Add the sequence number */
            pLsaInfo->lsaSeqNum = grSeqNum + 1;
            LADD4BYTE (pu1CurrPtr, pLsaInfo->lsaSeqNum);
            pLsaInfo->pLsa = opqLSAInfo.pu1LSA;
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT : IfCreateInCxt \n");

    return pInterface;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetDefaultValues                                         */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure initialises the default values for the      */
/*                various fields in the interface data structure. This       */
/*                procedure is called during creation of an interface.       */
/*                                                                           */
/* Input        : pInterface    :    interface to be initialized             */
/*                rowStatus     :    row status value.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfSetDefaultValues (tInterface * pInterface, tROWSTATUS rowStatus)
{
    INT1                i1Tos;
    INT2                i2Index;

    OS_MEM_SET (pInterface, 0, sizeof (tInterface));
    pInterface->admnStatus = OSPF_ENABLED;
    pInterface->u1IsmState = IFS_DOWN;

    pInterface->u2HelloInterval = OSPF_DEF_HELLO_INTERVAL;
    pInterface->i4RtrDeadInterval = DEF_RTR_DEAD_INTERVAL;
    pInterface->u2IfTransDelay = DEF_IF_TRANS_DELAY;
    pInterface->u1RtrPriority = DEF_RTR_PRIORITY;
    pInterface->u2RxmtInterval = DEF_RXMT_INTERVAL;
    pInterface->i4PollInterval = DEF_POLL_INTERVAL;
    pInterface->ifStatus = INVALID;

    pInterface->bDcEndpt = OSPF_FALSE;
    pInterface->ifOptions = RAG_NO_CHNG;
    pInterface->bPassive = OSPF_FALSE;
    pInterface->u1HelloIntervalsElapsed = 0;

    TMO_SLL_Init (&(pInterface->nbrsInIf));

    TMO_HASH_INIT_NODE (&(pInterface->nextIfNode));
    TMO_SLL_Init_Node (&(pInterface->nextIfInArea));
    TMO_SLL_Init_Node (&(pInterface->nextSortIf));

    TMO_SLL_Init (&(pInterface->Type9OpqLSALst));
    TMO_SLL_Init (&(pInterface->sortMd5authkeyLst));
    pInterface->pLastAuthkey = NULL;

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
        pInterface->aIfOpCost[(INT2) i1Tos].rowStatus = rowStatus;
    }

    /* Set all secondary IP as disabled */
    for (i2Index = 0; i2Index < MAX_SEC_INTERFACES; i2Index++)
    {
        pInterface->secondaryIP.ifSecIp[i2Index].u1Status = OSPF_DISABLED;
    }
    pInterface->secondaryIP.i1SecIpCount = 0;
    /*Initializing no. of secondary ip networks to be zero */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfAddToSortIfLstInCxt                                           */
/*                                                                           */
/* Description  : Adds the given interface structure to the sort_if_list.    */
/*                                                                           */
/* Input        : pOspfCxt          : Ospf Context Pointer                   */
/*              : pInterface        : the new interface to be added to the   */
/*                                     list                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
IfAddToSortIfLstInCxt (tOspfCxt * pOspfCxt, tInterface * pInterface)
{

    tInterface         *pLstIf;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pPrevLstNode;

    pPrevLstNode = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : IfAddToSortIfLstInCxt \n");

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pLstIf = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (UtilIpAddrIndComp (pInterface->ifIpAddr,
                               pInterface->u4AddrlessIf,
                               pLstIf->ifIpAddr,
                               pLstIf->u4AddrlessIf) == OSPF_LESS)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pOspfCxt->sortIfLst), pPrevLstNode,
                    &(pInterface->nextSortIf));

    pOspfCxt->pLastOspfIf = NULL;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : IfAddToSortIfLstInCxt \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfFindAndAddToArea                                         */
/*                                                                           */
/* Description  : Finds the area to which the interface belongs to and       */
/*                adds the given interface structure to the area's if_list   */
/*                                                                           */
/* Input        : pInterface        : the new interface to be added to the   */
/*                                     list                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
IfFindAndAddToAreaInCxt (tOspfCxt * pOspfCxt, tInterface * pInterface)
{
    tArea              *pArea;

    if ((pArea =
         UtilFindAssoAreaInCxt (pOspfCxt, &(pInterface->ifIpAddr))) == NULL)
    {
        pArea = pOspfCxt->pBackbone;
    }
    /* Add the associated area's pointer in the interface structure. */
    TMO_SLL_Add (&(pArea->ifsInArea), &(pInterface->nextIfInArea));
    pInterface->pArea = pArea;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfAssoIfWithHost                                           */
/*                                                                           */
/* Description  : Associates the interface with the host.                    */
/*                                                                           */
/* Input        : pInterface        : the new interface to be associated     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
IfAssoIfWithHost (tInterface * pInterface)
{
    tHost              *pLstHost;
    TMO_SLL_Scan (&(pInterface->pArea->pOspfCxt->hostLst), pLstHost, tHost *)
    {
        if (pLstHost->u4FwdIfIndex == pInterface->u4IfIndex)
        {
            pLstHost->pArea = pInterface->pArea;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfActivate                                                 */
/*                                                                           */
/* Description  : Generates the Event OspfIfUp                               */
/*                                                                           */
/* Input        : pInterface        : the interface to be inactivated        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfActivate (tInterface * pInterface)
{
    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d To be Activated\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);

    /* If Default Passive Interface is set to true, then make
     * this interface as passive by default */
    if (pInterface->pArea->pOspfCxt->bDefaultPassiveInterface == OSPF_TRUE)
    {
        pInterface->bPassive = OSPF_TRUE;
    }

    if ((pInterface->operStatus != OSPF_ENABLED) ||
        (pInterface->admnStatus != OSPF_ENABLED) ||
        (pInterface->ifStatus != ACTIVE)
        || (pInterface->pArea->pOspfCxt->admnStat != OSPF_ENABLED))
    {
        return OSPF_SUCCESS;
    }
    else
    {
        OspfIfUp (pInterface);
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfInactivate                                               */
/*                                                                           */
/* Description  : Flushes the network lsa , if Interface was DR.             */
/*                Disables the interface.                                    */
/*                                                                           */
/* Input        : pInterface        : the interface to be inactivated        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfInactivate (tInterface * pInterface)
{

    tLsaInfo           *pLsaInfo;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d To be Inactivated\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    /* OSPF is disabled on this interface.
     * Generate AS external LSA if the router is ASBR and redistribution is enabled
     */
    if (pInterface->u1IsmState != IFS_DOWN)
    {
        if (pInterface->pArea->pOspfCxt->u4RrdSrcProtoBitMask & LOCAL_RT_MASK)
        {

            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

            RtQuery.u4DestinationIpAddress =
                OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
            RtQuery.u4DestinationSubnetMask =
                OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask);
            RtQuery.u2AppIds = RTM_DIRECT_MASK;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
            RtQuery.u4ContextId = pInterface->pArea->pOspfCxt->u4OspfCxtId;
            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
            {
                if (NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID)
                {
                    RtmProcessRtUpdateInCxt (pInterface->pArea->pOspfCxt,
                                             &NetIpRtInfo);
                }
            }
        }
    }

    /* 
     * if interface is in state Designated Router then the network lsa should 
     * be flushed out. 
     */

    if ((pInterface->u1IsmState == IFS_DR) &&
        (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE))
    {

        if ((pLsaInfo = LsuSearchDatabase (NETWORK_LSA,
                                           &(pInterface->ifIpAddr),
                                           &(pInterface->pArea->pOspfCxt->
                                             rtrId), (UINT1 *) NULL,
                                           (UINT1 *) pInterface->pArea)) !=
            NULL)
        {
            /* The Network lsa has to be flushed out of the routing domain */
            AgdFlushOut (pLsaInfo);
        }
    }
    IfDisable (pInterface);
    ExtrtLsaPolicyHandlerInCxt (pInterface->pArea->pOspfCxt);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfDelete                                                   */
/*                                                                           */
/* Description  : Deletes the specified interface structure from the hash    */
/*                table and other lists. The associated timers are disabled. */
/*                Also all neighbors on this interface are deleted. The      */
/*                memory allocated for this interface structure is freed.    */
/*                                                                           */
/* Input        : pInterface        : the interface to be deleted            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfDelete (tInterface * pInterface)
{
    tInterface         *pLstIf;
    tTMO_SLL_NODE      *pLstNode;
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfCxtId;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tNeighbor          *pNbr = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    u4OspfCxtId = pInterface->pArea->pOspfCxt->u4OspfCxtId;
    pOspfCxt = pInterface->pArea->pOspfCxt;

    OSPF_TRC2 (OSPF_FN_ENTRY, u4OspfCxtId,
               "FUNC :IfDelete : If %x.%d To be Deleted\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    /* OSPF is disabled on this interface.
     * Generate AS external LSA if the router is ASBR and redistribution is eanbled
     */
    if (pInterface->u1IsmState != IFS_DOWN)
    {
        if (pInterface->pArea->pOspfCxt->u4RrdSrcProtoBitMask & LOCAL_RT_MASK)
        {

            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

            RtQuery.u4DestinationIpAddress =
                OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
            RtQuery.u4DestinationSubnetMask =
                OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask);
            RtQuery.u2AppIds = RTM_DIRECT_MASK;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
            RtQuery.u4ContextId = pInterface->pArea->pOspfCxt->u4OspfCxtId;
            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
            {
                if (NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID)
                {
                    RtmProcessRtUpdateInCxt (pInterface->pArea->pOspfCxt,
                                             &NetIpRtInfo);
                }
            }
        }
    }
    IfUpdateState (pInterface, IFS_DOWN);

    IfCleanup (pInterface, IF_CLEANUP_DELETE);

    TMO_SLL_Delete (&(pInterface->pArea->ifsInArea),
                    &(pInterface->nextIfInArea));

    pOspfCxt = pInterface->pArea->pOspfCxt;
    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        TMO_SLL_Delete (&(pOspfCxt->virtIfLst), &(pInterface->nextSortIf));
    }
    else
    {
        TMO_HASH_Delete_Node (gOsRtr.pIfHashTable,
                              &(pInterface->nextIfNode),
                              IF_HASH_FN (pInterface->u4IfIndex));
        TMO_SLL_Delete (&(pOspfCxt->sortIfLst), &(pInterface->nextSortIf));
        pOspfCxt->pLastOspfIf = NULL;

        RtrHandleABRStatChngInCxt (pOspfCxt);
    }

    while ((pAuthkeyInfo = (tMd5AuthkeyInfo *) TMO_SLL_First
            (&(pInterface->sortMd5authkeyLst))) != NULL)
    {
        TMO_SLL_Delete (&(pInterface->sortMd5authkeyLst),
                        &(pAuthkeyInfo->nextSortKey));
        MD5AUTH_FREE (pAuthkeyInfo);
    }

    OspfDeleteType9Lsas (pInterface);

    if (pInterface->u4MtuSize == pOspfCxt->u4MaxMtuSize)
    {
        pOspfCxt->u4MaxMtuSize = 0;
        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pLstIf = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if (pLstIf->u4MtuSize > pOspfCxt->u4MaxMtuSize)
            {
                pOspfCxt->u4MaxMtuSize = pLstIf->u4MtuSize;
            }
        }
        if (pOspfCxt->u4MaxMtuSize > gOsRtr.u4MaxMtuSize)
        {
            gOsRtr.u4MaxMtuSize = pOspfCxt->u4MaxMtuSize;
        }
    }

    IsmInitSchedQueueInCxt (NULL, pInterface);

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
               u4OspfCxtId,
               "If %x.%d Deleted\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    if (pInterface->u1NetworkType != IF_VIRTUAL)
    {
        UtilOspfClearAreaDb (pInterface->pArea);
    }

    if (pInterface->pArea != pOspfCxt->pBackbone)
    {
        if (TMO_SLL_Count (&(pInterface->pArea->ifsInArea)) == OSPF_ZERO)
        {
            AreaDelete (pInterface->pArea);
        }
    }

    IF_FREE (pInterface);

    ExtrtLsaPolicyHandlerInCxt (pOspfCxt);

    if ((pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART) &&
        (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_TOP_CHG);
    }

    /* If the interface is down, If this router is helper for any neighbor
       Stop the helper process */
    if ((pOspfCxt->u1HelperSupport & OSPF_GRACE_HELPER_ALL) &&
        (pOspfCxt->u1HelperStatus == OSPF_GR_HELPING) &&
        (pOspfCxt->u1StrictLsaCheck == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_SORT_LST (pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                GrExitHelper (OSPF_HELPER_TOP_CHG, pNbr);
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT : IfDelete\n");
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfDisable                                                  */
/*                                                                           */
/* Description  : Disables the specified interface structure from the hash   */
/*                table and other lists. The associated timers are disabled. */
/*                Also dynamically discovered neighbors on this interface are*/
/*                deleted.                                                   */
/*                                                                           */
/* Input        : pInterface : the interface to be disabled                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfDisable (tInterface * pInterface)
{
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tNeighbor          *pNbr = NULL;

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Interface %s To Be Disabled\n",
               OspfPrintIpAddr (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);

    /* If the interface is disabled, and if this router is helper for any neighbor
     *Stop the helper process */

    if ((pInterface->pArea->pOspfCxt->u1HelperSupport & OSPF_GRACE_HELPER_ALL)
        && (pInterface->pArea->pOspfCxt->u1HelperStatus == OSPF_GR_HELPING)
        && (pInterface->pArea->pOspfCxt->u1StrictLsaCheck == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pInterface->pArea->pOspfCxt->sortNbrLst),
                      pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_SORT_LST (pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                GrExitHelper (OSPF_HELPER_TOP_CHG, pNbr);
            }
        }
    }

    GENERATE_IF_EVENT (pInterface, IFE_DOWN);
    IfCleanup (pInterface, IF_CLEANUP_DISABLE);

    /* Check whether the router is still an ABR
     * after disabling this interface
     */
    if (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        RtrHandleABRStatChngInCxt (pInterface->pArea->pOspfCxt);
    }

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d Disabled\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfIfUp                                                   */
/*                                                                           */
/* Description  : This function is invoked when we receive indication from   */
/*                the lower levels that this interface is operational.       */
/*                                                                           */
/* Input        : pInterface        : interface which has come up            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OspfIfUp (tInterface * pInterface)
{
    UINT1               u1PrvAbrStat;
    UINT4               au4Key[2] = { 0, 0 };
    UINT1               u1Tos = 0;
    tInputParams        inParams;
    tOutputParams       outParams;
    VOID               *pTemp = NULL;
    tExtRoute          *pExtRoute = NULL;
    tIPADDR             maskedAddr;

    OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_ISM_TRC | OSPF_FN_ENTRY |
               OSPF_CRITICAL_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Ospf interface up for %s\n",
               OspfPrintIpAddr (pInterface->ifIpAddr));

    if ((pInterface->operStatus == OSPF_ENABLED) &&
        (pInterface->admnStatus == OSPF_ENABLED) &&
        (pInterface->ifStatus == ACTIVE)
        && (pInterface->pArea->pOspfCxt->admnStat == OSPF_ENABLED))
    {

        /* Add the backbone area to areas list if the new iface coming up is
         * associated with backbone.
         */
        if (pInterface->pArea == pInterface->pArea->pOspfCxt->pBackbone)
        {
            AreaAddToAreasLst (pInterface->pArea->pOspfCxt->pBackbone);
        }

        /* Check whether the router has become ABR after adding this 
           interface. */
        if (pInterface->u1NetworkType == IF_LOOPBACK)
        {
            GENERATE_IF_EVENT (pInterface, IFE_LOOP_IND);
        }
        else
        {
            GENERATE_IF_EVENT (pInterface, IFE_UP);
        }
        u1PrvAbrStat = (UINT1) pInterface->pArea->pOspfCxt->bAreaBdrRtr;
        if (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            /* This invokes Type 7 & router LSA orgination/flushing   
             * This should not be called when router is performing GR */
            RtrHandleABRStatChngInCxt (pInterface->pArea->pOspfCxt);
            if ((u1PrvAbrStat == pInterface->pArea->pOspfCxt->bAreaBdrRtr) &&
                (pInterface->pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
            {
                /* Implies New interface has come up
                 * in Rtr whose ABR state has not 
                 * changed
                 */
                if (pInterface->pArea->u4AreaType == NSSA_AREA)
                    NssaFsmFindTranslator (pInterface->pArea);
            }
        }
        if (pInterface->u4MtuSize > pInterface->pArea->pOspfCxt->u4MaxMtuSize)
        {
            pInterface->pArea->pOspfCxt->u4MaxMtuSize = pInterface->u4MtuSize;
        }

        if (pInterface->pArea->pOspfCxt->u4MaxMtuSize > gOsRtr.u4MaxMtuSize)
        {
            gOsRtr.u4MaxMtuSize = pInterface->pArea->pOspfCxt->u4MaxMtuSize;
        }

        OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "If %s UP\n", OspfPrintIpAddr (pInterface->ifIpAddr));
    }
    else
    {
        OSPF_TRC1 (OSPF_FN_EXIT | OSPF_CRITICAL_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "EXIT : Ospf interface %s UP Failed\n",
                   OspfPrintIpAddr (pInterface->ifIpAddr));
    }
    if (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        /* Type 5 LSA will be orginated/Flushed only if the router is not in GR state */
        ExtrtLsaPolicyHandlerInCxt (pInterface->pArea->pOspfCxt);
    }
    /* Delete self connected route in External route list */
    UtilIpAddrMaskCopy (maskedAddr, pInterface->ifIpAddr,
                        pInterface->ifIpAddrMask);
    au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (maskedAddr));
    au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask));
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pInterface->pArea->pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;

    if ((TrieSearch (&inParams, &outParams, (VOID **) &pTemp)) == SUCCESS)
    {
        pExtRoute = ((tExtRoute *) outParams.pAppSpecInfo);
        if (UtilIpAddrComp (((tExtRoute *) outParams.pAppSpecInfo)->ipNetNum,
                            maskedAddr) == OSPF_EQUAL)
        {
            if (pExtRoute->u2SrcProto == LOCAL_RT_MASK)
            {
                for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                {
                    pExtRoute->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                }
                ExtrtDeleteInCxt (pInterface->pArea->pOspfCxt, pExtRoute);
            }
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfIfDown                                                 */
/*                                                                           */
/* Description  : This function is invoked when we receive indication from   */
/*                the lower levels that this interface is not operational.   */
/*                                                                           */
/* Input        : pInterface        : interface to be brought down           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OspfIfDown (tInterface * pInterface)
{
    tInterface         *pLstIf;
    tTMO_SLL_NODE      *pLstNode;

    OSPF_TRC2 (OSPF_FN_ENTRY,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "FUNC :OspfIfDown: If %x.%d To Be Brought DN\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);

    IfDisable (pInterface);

    if (pInterface->u4MtuSize == pInterface->pArea->pOspfCxt->u4MaxMtuSize)
    {
        pInterface->pArea->pOspfCxt->u4MaxMtuSize = 0;
        TMO_SLL_Scan (&(pInterface->pArea->pOspfCxt->sortIfLst), pLstNode,
                      tTMO_SLL_NODE *)
        {
            pLstIf = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if (pLstIf->u4MtuSize > pInterface->pArea->pOspfCxt->u4MaxMtuSize)
            {
                pInterface->pArea->pOspfCxt->u4MaxMtuSize = pLstIf->u4MtuSize;
            }
            if (pInterface->pArea->pOspfCxt->u4MaxMtuSize > gOsRtr.u4MaxMtuSize)
            {
                gOsRtr.u4MaxMtuSize = pInterface->pArea->pOspfCxt->u4MaxMtuSize;
            }
        }
    }

    /* Check if this interface was Fwd Address for  */
    /* any Type 7 LSA originated by the router      */
    /* re-orignate that LSA                         */
    if (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        OlsResetType7FwdAddr (pInterface);
    }

    OSPF_TRC2 (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "EXIT : OspfIfDown :If %x.%d Brought DN\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfLoopback                                                 */
/*                                                                           */
/* Description  : This function is invoked when we receive indication either */
/*                from lower levels or the network management that the       */
/*                interface should loopback.                                 */
/*                                                                           */
/* Input        : pInterface        : interface to be looped back            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
IfLoopback (tInterface * pInterface)
{
    GENERATE_IF_EVENT (pInterface, IFE_LOOP_IND);

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d Looped Back\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfUnloop                                                   */
/*                                                                           */
/* Description  : This function is invoked when we receive indication either */
/*                from lower levels or the network management  that the      */
/*                interface should come out of the loopback state.           */
/*                                                                           */
/* Input        : pInterface       : interface to be unlooped                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
IfUnloop (tInterface * pInterface)
{
    GENERATE_IF_EVENT (pInterface, IFE_UNLOOP_IND);

    OSPF_TRC2 (CONTROL_PLANE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d UnLooped\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfUpdateState                                              */
/*                                                                           */
/* Description  : This function is invoked if a state transition during the  */
/*                action functions of the interface state machine. This      */
/*                function takes care of the steps to be taken during        */
/*                specific state transitions.                                */
/*                                                                           */
/* Input        : pInterface       : the interface whose state is updated    */
/*                u1State          : the new state of the interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfUpdateState (tInterface * pInterface, UINT1 u1State)
{
    UINT1               u1PrevState;
    tLsaInfo           *pLsaInfo;
    tExtRoute          *pExtRoute = NULL;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    tIPADDR             lnkStId;
    tIPADDR             newRngNet;
    tIPADDR             tmpRngNet;
    tAsExtAddrRange    *pBkBoneRng = NULL;
    UINT1               u1AdvStatus = OSPF_FALSE;
    tInterface         *pLstIf;
    tTMO_SLL_NODE      *pLstNode;

    OSPF_TRC3 (OSPF_FN_ENTRY,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "FUNC :IfUpdateState: If %x.%d To Be Updated New State s\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf, au1DbgIfState[u1State]);

    if (pInterface->u1IsmState != u1State)
    {
        if ((pInterface->u1NetworkType == IF_BROADCAST) ||
            (pInterface->u1NetworkType == IF_PTOP))
        {
            if ((u1State != IFS_DOWN) && (pInterface->u1IsmState == IFS_DOWN))
            {
                IpifJoinMcastGroup (&gAllSpfRtrs, pInterface);
            }
        }

        /* 
         * If interface is in state Designated Router then the network lsa 
         * should be flushed out. 
         */
        if (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            /* lsa can be flushed only if router is not in GR state */
            if (pInterface->u1IsmState == IFS_DR)
            {
                if ((pLsaInfo = LsuSearchDatabase (NETWORK_LSA,
                                                   &(pInterface->ifIpAddr),
                                                   &(pInterface->pArea->
                                                     pOspfCxt->rtrId),
                                                   (UINT1 *) NULL,
                                                   (UINT1 *) pInterface->
                                                   pArea)) != NULL)
                {
                    /* Network lsa has to be flushed out of the routing domain */
                    AgdFlushOut (pLsaInfo);
                }
            }
        }
        u1PrevState = pInterface->u1IsmState;
        pInterface->u1IsmState = u1State;
        if ((u1PrevState == IFS_DOWN) && (!IS_VIRTUAL_IFACE (pInterface)))
        {
            pInterface->pArea->u4ActIntCount++;
            /* The Area is becoming actively attached area */
            if (pInterface->pArea->u4ActIntCount == 1)
            {
                pInterface->pArea->pOspfCxt->u4ActiveAreaCount++;
                pInterface->pArea->bNewlyAttached = OSPF_TRUE;

                if (pInterface->pArea->pOspfCxt->u1OspfRestartState ==
                    OSPF_GR_NONE)
                {
                    /* bGenType5Flag must be true to generate external summary LSAs. */
                    TMO_SLL_Scan (&(pInterface->pArea->pOspfCxt->sortIfLst),
                                  pLstNode, tTMO_SLL_NODE *)
                    {
                        pLstIf = GET_IF_PTR_FROM_SORT_LST (pLstNode);
                        if (pLstIf->pArea->u4AreaType == NORMAL_AREA)
                        {
                            pInterface->pArea->pOspfCxt->bGenType5Flag =
                                OSPF_TRUE;
                            break;
                        }
                    }

                    /* LSA can be flushed only if router is not in GR state
                       RagAddRouteInAggAddrRangeInCxt - triggers type 7
                       Lsa reorgination/flushing */

                    au4Key[0] = 0;
                    au4Key[1] = 0;
                    inParams.Key.pKey = (UINT1 *) au4Key;
                    inParams.pRoot = pInterface->pArea->pOspfCxt->pExtRtRoot;
                    inParams.i1AppId = OSPF_EXT_RT_ID;
                    inParams.pLeafNode = NULL;
                    inParams.u1PrefixLen = 0;
                    MEMSET (tmpRngNet, 0, sizeof (tIPADDR));
                    if (TrieGetFirstNode (&inParams,
                                          &pAppSpecPtr,
                                          (VOID **) &(inParams.pLeafNode)) !=
                        TRIE_FAILURE)
                    {
                        do
                        {
                            pExtRoute = (tExtRoute *) pAppSpecPtr;
                            RagAddRouteInAggAddrRangeInCxt (pInterface->pArea->
                                                            pOspfCxt,
                                                            pExtRoute);

                            /* External summarised LSAs must be generated when
                               first interface comes up in the normal area  */
                            if (pExtRoute == NULL)
                            {
                                break;
                            }
                            else if ((pExtRoute != NULL) &&
                                     pInterface->pArea->pOspfCxt->
                                     bGenType5Flag == OSPF_TRUE)
                            {
                                if ((pExtRoute->pAsExtAddrRange != NULL)
                                    && (pExtRoute->pAsExtAddrRange->
                                        rangeStatus == ACTIVE))
                                {
                                    if ((pInterface->pArea->pOspfCxt->
                                         bAsBdrRtr == OSPF_TRUE)
                                        &&
                                        (UtilIpAddrComp
                                         (pInterface->pArea->areaId,
                                          gNullIpAddr) == OSPF_EQUAL))
                                    {
                                        pBkBoneRng = pExtRoute->pAsExtAddrRange;

                                        UtilIpAddrMaskCopy (newRngNet,
                                                            pExtRoute->ipNetNum,
                                                            pBkBoneRng->
                                                            ipAddrMask);

                                        if (UtilIpAddrComp
                                            (newRngNet,
                                             tmpRngNet) != OSPF_EQUAL)
                                        {
                                            u1AdvStatus = OSPF_FALSE;
                                        }
                                        if ((RagIsExtRtFallInRange
                                             (pExtRoute,
                                              pBkBoneRng) == OSPF_TRUE)
                                            && (u1AdvStatus == OSPF_FALSE))
                                        {
                                            UtilIpAddrMaskCopy (lnkStId,
                                                                pBkBoneRng->
                                                                ipAddr,
                                                                pBkBoneRng->
                                                                ipAddrMask);
                                            OlsGenerateLsa (pInterface->pArea,
                                                            COND_AS_EXT_LSA,
                                                            &(lnkStId),
                                                            (UINT1 *)
                                                            pBkBoneRng);
                                            UtilIpAddrMaskCopy (tmpRngNet,
                                                                pExtRoute->
                                                                ipNetNum,
                                                                pBkBoneRng->
                                                                ipAddrMask);
                                            u1AdvStatus = OSPF_TRUE;
                                        }
                                    }
                                }
                            }
                            au4Key[0] =
                                OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipNetNum));
                            au4Key[1] =
                                OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipAddrMask));

                            inParams.Key.pKey = (UINT1 *) au4Key;
                            pLeafNode = inParams.pLeafNode;
                            pExtRoute = NULL;

                        }
                        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                                &pAppSpecPtr,
                                                (VOID **) &(inParams.
                                                            pLeafNode)) !=
                               TRIE_FAILURE);
                    }
                }
                if ((pInterface->pArea->u4AreaType != NORMAL_AREA) &&
                    (pInterface->pArea->aStubDefaultCost[TOS_0].rowStatus
                     != ACTIVE))
                {
                    pInterface->pArea->aStubDefaultCost[TOS_0].rowStatus =
                        ACTIVE;
                    pInterface->pArea->aStubDefaultCost[TOS_0].u4Value =
                        DEFAULT_SUMM_COST;
                }
                if ((pInterface->pArea->u4AreaType != NSSA_AREA) &&
                    (pInterface->pArea->aStubDefaultCost[TOS_0].rowStatus
                     != ACTIVE))
                {
                    pInterface->pArea->aStubDefaultCost[TOS_0].u4MetricType =
                        OSPF_METRIC;
                }
                else
                {
                    if (pInterface->pArea->aStubDefaultCost[TOS_0].rowStatus
                        != ACTIVE)
                    {

                        if (pInterface->pArea->u1SummaryFunctionality ==
                            NO_AREA_SUMMARY)
                        {
                            pInterface->pArea->aStubDefaultCost[TOS_0].
                                u4MetricType = OSPF_METRIC;
                        }
                        else
                        {
                            pInterface->pArea->aStubDefaultCost[TOS_0].
                                u4MetricType = TYPE1EXT_METRIC;
                        }
                    }
                }
            }
        }
        else if ((u1State == IFS_DOWN) && (!IS_VIRTUAL_IFACE (pInterface)))
        {
            pInterface->pArea->u4ActIntCount--;
            /* The Area is becoming configured area from
             *              * actively attached area */
            if (pInterface->pArea->u4ActIntCount == 0)
            {
                pInterface->pArea->pOspfCxt->u4ActiveAreaCount--;
                if (UtilIpAddrComp (pInterface->pArea->areaId, gNullIpAddr) ==
                    OSPF_EQUAL)
                {

                    au4Key[0] = 0;
                    au4Key[1] = 0;
                    inParams.Key.pKey = (UINT1 *) au4Key;
                    inParams.pRoot = pInterface->pArea->pOspfCxt->pExtRtRoot;
                    inParams.i1AppId = OSPF_EXT_RT_ID;
                    inParams.pLeafNode = NULL;
                    inParams.u1PrefixLen = 0;

                    if (TrieGetFirstNode (&inParams,
                                          &pAppSpecPtr,
                                          (VOID **) &(inParams.pLeafNode)) !=
                        TRIE_FAILURE)
                    {
                        do
                        {
                            pExtRoute = (tExtRoute *) pAppSpecPtr;
                            /* Flush Summarized LSAs(sub summed connected routes) */
                            if (pExtRoute == NULL)
                            {
                                break;
                            }
                            else if ((pExtRoute != NULL)
                                     && (pExtRoute->pAsExtAddrRange != NULL)
                                     && (pExtRoute->pAsExtAddrRange->
                                         rangeStatus == ACTIVE)
                                     && (pExtRoute->u2SrcProto == LOCAL_ID))
                            {
                                RagDelRouteFromAggAddrRangeInCxt (pInterface->
                                                                  pArea->
                                                                  pOspfCxt,
                                                                  pExtRoute);
                            }
                            au4Key[0] =
                                OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipNetNum));
                            au4Key[1] =
                                OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipAddrMask));
                            inParams.Key.pKey = (UINT1 *) au4Key;
                            pLeafNode = inParams.pLeafNode;
                            pExtRoute = NULL;
                        }
                        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                                &pAppSpecPtr,
                                                (VOID **) &(inParams.
                                                            pLeafNode)) !=
                               TRIE_FAILURE);
                    }
                }
            }

        }
        if ((u1PrevState == IFS_DOWN) && (IS_VIRTUAL_IFACE (pInterface))
            && (pInterface->pArea->pOspfCxt->pBackbone->u4ActIntCount == 0))

        {
            pInterface->pArea->bNewlyAttached = OSPF_TRUE;
        }
        if (((pInterface->u1IsmState == IFS_DR) ||
             (pInterface->u1IsmState == IFS_BACKUP)) &&
            ((u1PrevState != IFS_DR) && (u1PrevState != IFS_BACKUP)))
        {
            IpifJoinMcastGroup (&gAllDRtrs, pInterface);
        }

        if (((u1PrevState == IFS_DR) ||
             (u1PrevState == IFS_BACKUP)) &&
            ((pInterface->u1IsmState != IFS_DR) &&
             (pInterface->u1IsmState != IFS_BACKUP)))
        {
            IpifLeaveMcastGroup (&gAllDRtrs, pInterface);
        }

        COUNTER_OP (pInterface->aIfEvents, 1);
        if (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            /* lsa can be regenerated only if router is not in GR state */

            if ((pInterface->pArea->pOspfCxt->admnStat == OSPF_ENABLED) &&
                ((u1PrevState == IFS_DOWN) || (u1State == IFS_DOWN)
                 || (u1State == IFS_LOOP_BACK)))
            {
                if ((u1PrevState != IFS_DOWN) && (u1State == IFS_DOWN)
                    && (pInterface->pArea->u4ActIntCount == 0))
                {
                    pLsaInfo = NULL;
                    if ((pLsaInfo = LsuSearchDatabase (ROUTER_LSA,
                                                       &(pInterface->pArea->
                                                         pOspfCxt->rtrId),
                                                       &(pInterface->pArea->
                                                         pOspfCxt->rtrId),
                                                       (UINT1 *) NULL,
                                                       (UINT1 *) pInterface->
                                                       pArea)) != NULL)
                    {
                        AgdFlushOut (pLsaInfo);
                    }
                }
                else
                {
                    OlsSignalLsaRegenInCxt (pInterface->pArea->pOspfCxt,
                                            SIG_IF_STATE_CHANGE,
                                            (UINT1 *) pInterface);
                }
            }
        }

        IfSendStateChgTrap (pInterface);
        if (pInterface->u1IsmState == IFS_DOWN ||
            (u1PrevState == IFS_DOWN && pInterface->u1IsmState > IFS_DOWN))
        {
            if (pInterface->pArea->pOspfCxt->u1RtrNetworkLsaChanged ==
                OSPF_TRUE)
            {
                TmrStopTimer (gTimerLst,
                              &(pInterface->pArea->pOspfCxt->runRtTimer.
                                timerNode));
            }
            pInterface->pArea->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
            RtcSetRtTimer (pInterface->pArea->pOspfCxt);
        }
        if ((pInterface->u1NetworkType == IF_BROADCAST) ||
            (pInterface->u1NetworkType == IF_PTOP))
        {
            if ((pInterface->u1IsmState == IFS_DOWN)
                && (u1PrevState != IFS_DOWN))
            {
                IpifLeaveMcastGroup (&gAllSpfRtrs, pInterface);
            }
        }
        OspfRmSendIfState (pInterface, OSPF_ZERO);
    }
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT : IfUpdateState\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfGetMinCostIfInCxt                                        */
/*                                                                           */
/* Description  : The following procedure is used to find default cost for   */
/*                setting in stub area table and also in advertising router  */
/*                as host route in router lsa generation.                    */
/*                                                                           */
/* Input        : pOspfCxt      : Ospf Context Pointer                       */
/*                i1Tos         : type of service                            */
/*                pAreaId         : AREA Id pointer                          */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to interface with minimum cost                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
IfGetMinCostIfInCxt (tOspfCxt * pOspfCxt, INT1 i1Tos, tAreaId * pAreaId)
{
    tInterface         *pInterface;
    tInterface         *pRetIf;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4Cost = 0xffffffff;

    pRetIf = NULL;
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((pAreaId != NULL) &&
            (UtilIpAddrComp (*pAreaId,
                             pInterface->pArea->areaId) == OSPF_EQUAL))
        {
            /*
             * If area id is specified then only interfaces to other areas are
             * considred.
             */
            continue;
        }
        if ((pInterface->aIfOpCost[(INT2) i1Tos].rowStatus != INVALID) &&
            (pInterface->aIfOpCost[(INT2) i1Tos].u4Value < u4Cost))
        {

            u4Cost = pInterface->aIfOpCost[(INT2) i1Tos].u4Value;
            pRetIf = pInterface;
        }
    }
    return pRetIf;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfGetMinCostIfInArea                                       */
/*                                                                           */
/* Description  : The following procedure is used to find default cost for   */
/*                setting in stub area table and also in advertising router  */
/*                as host route in router lsa generation.                    */
/*                                                                           */
/* Input        : i1Tos         : type of service                            */
/*                pArea         : AREA pointer                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to interface with minimum cost                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
IfGetMinCostIfInArea (INT1 i1Tos, tArea * pArea)
{
    tInterface         *pInterface;
    tInterface         *pRetIf;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4Cost = 0xffffffff;

    pRetIf = NULL;

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pLstNode);

        if ((pInterface->aIfOpCost[(INT2) i1Tos].rowStatus != INVALID) &&
            (pInterface->aIfOpCost[(INT2) i1Tos].u4Value < u4Cost))
        {
            u4Cost = pInterface->aIfOpCost[(INT2) i1Tos].u4Value;
            pRetIf = pInterface;
        }
    }
    return pRetIf;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfCleanup                                                  */
/*                                                                           */
/* Description  : Cleans up the specified interface.                         */
/*                                                                           */
/* Input        : pInterface       :  interface to be cleaned                */
/*                u1CleanupFlag   :  flag                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfCleanup (tInterface * pInterface, UINT1 u1CleanupFlag)
{
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    tTMO_SLL_NODE      *pPrevNode;

    IsmDisableIfTimers (pInterface);
    IsmResetVariables (pInterface);
    pPrevNode = NULL;
    while ((pNbrNode = TMO_SLL_Next (&(pInterface->nbrsInIf), pPrevNode))
           != NULL)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        pNbr->bIsBfdDisable = OSIX_FALSE;
        if (u1CleanupFlag == IF_CLEANUP_DOWN)
        {
            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Link down Recived for Neighbour with Address = %s\n",
                       OspfPrintIpAddr (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_LL_DOWN);
            pPrevNode = pNbrNode;
        }
        else if (u1CleanupFlag == IF_CLEANUP_DISABLE)
        {
            if (pNbr->u1ConfigStatus == DISCOVERED_NBR)
            {
                /* NbrUpdateState is called to take care of the Full Nbr 
                 * count in Area Structure. Router LSA generation is based on
                 * this Count only. */
                NbrUpdateState (pNbr, NBRS_DOWN);
                NbrDelete (pNbr);
            }
            else
            {
                OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                           pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "NBRE: Link down Recived for Neighbour with Address = %s\n",
                           OspfPrintIpAddr (pNbr->nbrIpAddr));
                GENERATE_NBR_EVENT (pNbr, NBRE_LL_DOWN);
                pPrevNode = pNbrNode;
            }
        }
        else if (u1CleanupFlag == IF_CLEANUP_DELETE)
        {
            /* NbrUpdateState is called to take care of the Full Nbr 
             * count in Area Structure. Router LSA generation is based on
             * this Count only. */
            NbrUpdateState (pNbr, NBRS_DOWN);
            NbrDelete (pNbr);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfDeleteIfMetric                                           */
/*                                                                           */
/* Description  : Starts the regeneration timer for router LSA               */
/*                                                                           */
/* Input        : pArea        : pointer to the AREA                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfDeleteIfMetric (tArea * pArea)
{
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tNeighbor          *pNbr = NULL;
    if ((pArea->pOspfCxt->u1HelperSupport & OSPF_GRACE_HELPER_ALL) &&
        (pArea->pOspfCxt->u1HelperStatus == OSPF_GR_HELPING) &&
        (pArea->pOspfCxt->u1StrictLsaCheck == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pArea->pOspfCxt->sortNbrLst), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_SORT_LST (pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                GrExitHelper (OSPF_HELPER_TOP_CHG, pNbr);
            }
        }
    }
    if ((pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART) &&
        (pArea->pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        GrExitGracefultRestart (pArea->pOspfCxt, OSPF_RESTART_TOP_CHG);
    }

    if (pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        OlsGenerateLsa (pArea, ROUTER_LSA, &(pArea->pOspfCxt->rtrId), NULL);
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetAreaId                                                */
/*                                                                           */
/* Description  : This routine is invoked to associate the interface to      */
/*                a area.                                                    */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface which has to      */
/*                                 associated.                               */
/*                pAreaId      : pointer to areaId to which the interface    */
/*                                 has to associated.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfSetAreaId (tInterface * pInterface, tAreaId * pAreaId)
{
    tNeighbor          *pNbr;
    tArea              *pNewArea;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               u1UpFlag;

    if ((pNewArea = GetFindAreaInCxt (pInterface->pArea->pOspfCxt, pAreaId))
        == NULL)
    {
        return OSPF_FAILURE;
    }

    /* Bring interface down */
    if (pInterface->u1IsmState != IFS_DOWN)
    {
        OspfIfDown (pInterface);
        u1UpFlag = OSPF_TRUE;
    }
    else
    {
        u1UpFlag = OSPF_FALSE;
    }

    /* Remove neighbors from the old area ap table and nbr hash table
     * and add them to the new area.
     */

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);
        NbrSetArea (pNbr, pInterface->pArea, pNewArea);
    }

    /* Remove interface from current area
     * add interface to new area
     */
    TMO_SLL_Delete (&(pInterface->pArea->ifsInArea),
                    &(pInterface->nextIfInArea));
    TMO_SLL_Add (&(pNewArea->ifsInArea), &(pInterface->nextIfInArea));
    pInterface->pArea = pNewArea;

    /* Bring interface up */
    if (u1UpFlag == OSPF_TRUE)
    {
        OspfIfUp (pInterface);
    }
    else
    {
        /* In the case of IBM Type ABR, the interface in DOWN state in the 
         * backbone area will have effect on the ABR status. 
         * So if the interface area-id is changing from backbone to non 
         * backbone area or from non backbone to backbone area, in IBM Type 
         * ABR we need to call RtrHandleABRStatChng function. */
        if (pNewArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            if (pNewArea->pOspfCxt->u4ABRType == IBM_ABR)
            {
                RtrHandleABRStatChngInCxt (pNewArea->pOspfCxt);
            }
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetAdmnStat                                              */
/*                                                                           */
/* Description  : This routine is used to enable/disable the interface.      */
/*                                                                           */
/* Input        : pInterface    : pointer to interface.                      */
/*                u1Status      : adminstatus value.                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfSetAdmnStat (tInterface * pInterface, UINT1 u1Status)
{
    switch (u1Status)
    {
        case OSPF_ENABLED:
            if (pInterface->admnStatus != OSPF_ENABLED)
            {
                pInterface->admnStatus = OSPF_ENABLED;
                OspfIfUp (pInterface);
            }
            break;

        case OSPF_DISABLED:
            if (pInterface->admnStatus == OSPF_ENABLED)
            {
                pInterface->admnStatus = OSPF_DISABLED;
                OspfIfDown (pInterface);
            }
            break;

        default:
            return OSPF_FAILURE;
            /* end of switch */
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetOperStat                                              */
/*                                                                           */
/* Description  : This routine is invoked to set the operational status of   */
/*                Interface.                                                 */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface.                  */
/*                u1Status      : operational status value.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfSetOperStat (tInterface * pInterface, UINT1 u1Status)
{

    switch (u1Status)
    {
        case OSPF_ENABLED:
            pInterface->operStatus = u1Status;
            OspfIfUp (pInterface);
            break;

        case OSPF_DISABLED:
            pInterface->operStatus = u1Status;
            OspfIfDown (pInterface);
            break;

        case OSPF_LOOPBACK_IND:
            pInterface->operStatus = u1Status;
            IfLoopback (pInterface);
            break;

        case OSPF_UNLOOP_IND:
            pInterface->operStatus = u1Status;
            IfUnloop (pInterface);
            break;

        default:
            gu4IfSetOperStatFail++;
            return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSendStateChgTrap                                         */
/*                                                                           */
/* Description  : This routine is used to send a trap when interface state   */
/*                changes.                                                   */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
IfSendStateChgTrap (tInterface * pInterface)
{
    tIfStChgTrapInfo    trapIfStChg;
    tVifOrVnbrStChgTrapInfo trapVifStChg;

    if (!IS_INITIAL_IFACE_ACTIVITY_TRAP (pInterface) &&
        (pInterface->u1IsmState != IFS_WAITING &&
         pInterface->u1IsmState != IFS_LOOP_BACK))
    {

        if (!IS_VIRTUAL_IFACE (pInterface) &&
            IS_TRAP_ENABLED_IN_CXT (IF_STATE_CHANGE_TRAP,
                                    pInterface->pArea->pOspfCxt))
        {
            IP_ADDR_COPY (trapIfStChg.ifIpAddr, pInterface->ifIpAddr);
            trapIfStChg.u4AddrlessIf = pInterface->u4AddrlessIf;
            trapIfStChg.u1State = pInterface->u1IsmState;
            IP_ADDR_COPY (trapIfStChg.rtrId,
                          pInterface->pArea->pOspfCxt->rtrId);
            SnmpifSendTrapInCxt (pInterface->pArea->pOspfCxt,
                                 IF_STATE_CHANGE_TRAP, &trapIfStChg);
        }
        else
        {
            if (IS_VIRTUAL_IFACE (pInterface) &&
                IS_TRAP_ENABLED_IN_CXT (VIRT_IF_STATE_CHANGE_TRAP,
                                        pInterface->pArea->pOspfCxt))
            {
                IP_ADDR_COPY (trapVifStChg.tranAreaId,
                              pInterface->transitAreaId);
                IP_ADDR_COPY (trapVifStChg.virtNbr, pInterface->destRtrId);
                trapVifStChg.u1State = pInterface->u1IsmState;
                IP_ADDR_COPY (trapVifStChg.rtrId,
                              pInterface->pArea->pOspfCxt->rtrId);
                SnmpifSendTrapInCxt (pInterface->pArea->pOspfCxt,
                                     VIRT_IF_STATE_CHANGE_TRAP, &trapVifStChg);
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSendConfErrTrap                                          */
/*                                                                           */
/* Description  : This routine is used to send a configuration error trap.   */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface.                  */
/*                u1TrapId     : trap identifier.                            */
/*                u1ErrType    : configuration error type.                   */
/*                u1PktType    : packet type.                                */
/*                pSrcIpAddr  : Source ip address.                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfSendConfErrTrap (tInterface * pInterface,
                   UINT1 u1TrapId,
                   UINT1 u1ErrType, UINT1 u1PktType, tIPADDR * pSrcIpAddr)
{
    tIfConfErrTrapInfo  trapIfConfErr;

    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        VifSendConfErrTrap (pInterface, u1TrapId, u1ErrType, u1PktType);
    }
    else
    {
        if (IS_TRAP_ENABLED_IN_CXT (u1TrapId, pInterface->pArea->pOspfCxt))
        {
            IP_ADDR_COPY (trapIfConfErr.ifIpAddr, pInterface->ifIpAddr);
            trapIfConfErr.u4AddrlessIf = pInterface->u4AddrlessIf;
            IP_ADDR_COPY (trapIfConfErr.packetSrcAdr, *pSrcIpAddr);
            trapIfConfErr.u1ErrType = u1ErrType;
            trapIfConfErr.u1PktType = u1PktType;
            IP_ADDR_COPY (trapIfConfErr.rtrId,
                          pInterface->pArea->pOspfCxt->rtrId);
            SnmpifSendTrapInCxt (pInterface->pArea->pOspfCxt,
                                 u1TrapId, &trapIfConfErr);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSendBadPktTrap                                           */
/*                                                                           */
/* Description  : This routine is used to send traps regarding the bad pkts. */
/*                                                                           */
/* Input        : pInterface    : pointer to interface.                      */
/*                u1PktType    : type of the packet.                         */
/*                pSrcIpAddr  : source ip_address.                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfSendBadPktTrap (tInterface * pInterface,
                  UINT1 u1PktType, tIPADDR * pSrcIpAddr)
{
    tIfRxBadPktTrapInfo trapIfRxBadPkt;

    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        VifSendBadPktTrap (pInterface, u1PktType);
    }
    else
    {
        if (IS_TRAP_ENABLED_IN_CXT (IF_RX_BAD_PACKET_TRAP,
                                    pInterface->pArea->pOspfCxt))
        {
            IP_ADDR_COPY (trapIfRxBadPkt.ifIpAddr, pInterface->ifIpAddr);
            trapIfRxBadPkt.u4AddrlessIf = pInterface->u4AddrlessIf;
            IP_ADDR_COPY (trapIfRxBadPkt.packetSrcAdr, *pSrcIpAddr);
            trapIfRxBadPkt.u1PktType = u1PktType;
            IP_ADDR_COPY (trapIfRxBadPkt.rtrId,
                          pInterface->pArea->pOspfCxt->rtrId);
            SnmpifSendTrapInCxt (pInterface->pArea->pOspfCxt,
                                 IF_RX_BAD_PACKET_TRAP, &trapIfRxBadPkt);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSendRxmtTrap                                             */
/*                                                                           */
/* Description  : This routine is used to send traps due to retransmissions. */
/*                                                                           */
/* Input        : pNbr          : pointer to neighbour.                      */
/*                pLsHeader    : pointer to link state headre.               */
/*                u1PktType    : type of the packet.                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IfSendRxmtTrap (tNeighbor * pNbr, tLsHeader * pLsHeader, UINT1 u1PktType)
{
    tIfRxmtTrapInfo     trapIfTxRxmt;

    if (!IS_INITIAL_IFACE_ACTIVITY_TRAP (pNbr->pInterface) &&
        IS_TRAP_ENABLED_IN_CXT (IF_TX_RETRANSMIT_TRAP,
                                pNbr->pInterface->pArea->pOspfCxt))
    {
        IP_ADDR_COPY (trapIfTxRxmt.ifIpAddr, pNbr->pInterface->ifIpAddr);
        trapIfTxRxmt.u4AddrlessIf = pNbr->pInterface->u4AddrlessIf;
        IP_ADDR_COPY (trapIfTxRxmt.nbrId, pNbr->nbrId);
        trapIfTxRxmt.u1PktType = u1PktType;
        trapIfTxRxmt.u1LsdbType = pLsHeader->u1LsaType;
        IP_ADDR_COPY (trapIfTxRxmt.lsdbLsid, pLsHeader->linkStateId);
        IP_ADDR_COPY (trapIfTxRxmt.lsdbRtrId, pLsHeader->advRtrId);
        IP_ADDR_COPY (trapIfTxRxmt.rtrId,
                      pNbr->pInterface->pArea->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pNbr->pInterface->pArea->pOspfCxt,
                             IF_TX_RETRANSMIT_TRAP, &trapIfTxRxmt);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSendDefaultSumToStubArea                                 */
/*                                                                           */
/* Description  : This routine is used to send default summary LSA into      */
/*                stub/Nssa area.                                            */
/*                                                                           */
/* Input        : pArea                 : pointer to Stub Area.              */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IfSendDefaultSumToStubNssaArea (tArea * pArea)
{
    tInterface         *pMinCstIface;
    UINT1               u1Tos;

    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (gu1DefCostCiscoCompat == OSPF_FALSE)
        {
            pMinCstIface =
                IfGetMinCostIfInArea ((UINT1) DECODE_TOS (u1Tos), pArea);
            if ((pMinCstIface != NULL)
                && (pArea->aStubDefaultCost[DECODE_TOS (u1Tos)].rowStatus ==
                    NOT_READY))
            {
                pArea->aStubDefaultCost[DECODE_TOS (u1Tos)].
                    u4Value =
                    pMinCstIface->aIfOpCost[DECODE_TOS (u1Tos)].u4Value;
                pArea->aStubDefaultCost[DECODE_TOS (u1Tos)].rowStatus = ACTIVE;
            }
        }
        else
        {
            pArea->aStubDefaultCost[DECODE_TOS (u1Tos)].u4Value =
                STUB_NSSA_DEFAULT_COST;
            pArea->aStubDefaultCost[DECODE_TOS (u1Tos)].rowStatus = ACTIVE;

        }
    }
    if (pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        if (pArea->u4AreaType == STUB_AREA)
        {
            OlsSignalLsaRegenInCxt (pArea->pOspfCxt,
                                    SIG_DEFAULT_SUMMARY, (UINT1 *) pArea);
        }
        else if (pArea->u4AreaType == NSSA_AREA)
        {
            if (pArea->u1SummaryFunctionality == SEND_AREA_SUMMARY)
            {
                if (pArea->u4DfInfOriginate == DEFAULT_INFO_ORIGINATE)
                {
                    OlsGenerateNssaDfLsa (pArea, DEFAULT_NSSA_LSA,
                                          &gDefaultDest);
                }
            }
            else
            {
                OlsSignalLsaRegenInCxt (pArea->pOspfCxt,
                                        SIG_DEFAULT_SUMMARY, (UINT1 *) pArea);
            }
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetType                                                  */
/*                                                                           */
/* Description  : This routine is invoked to associate the Network type  to  */
/*                a Interface.                                               */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface                   */
/*                u1Type        : Network type                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IfSetType (tInterface * pInterface, UINT1 u1Type)
{
    UINT1               u1UpFlag = OSPF_FALSE;
    /* Bring the interface down */
    if (pInterface->u1IsmState != IFS_DOWN)
    {
        OspfIfDown (pInterface);
        u1UpFlag = OSPF_TRUE;
    }

    pInterface->u1NetworkType = u1Type;

    /* Bring interface up */
    if (u1UpFlag == OSPF_TRUE)
    {
        OspfIfUp (pInterface);
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetDemand                                                */
/*                                                                           */
/* Description  : This routine is invoked to associate the interface to      */
/*                a area.                                                    */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface which has to      */
/*                                 associated.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IfSetDemand (tInterface * pInterface, UINT1 u1DemandValue)
{

    /* Bring the interface down */
    if (pInterface->u1IsmState != IFS_DOWN)
    {
        OspfIfDown (pInterface);
    }

    pInterface->bDcEndpt = u1DemandValue;

    if (pInterface->bDcEndpt == OSPF_TRUE)
    {
        pInterface->ifOptions = DC_BIT_MASK;
    }
    else
    {
        pInterface->ifOptions = RAG_NO_CHNG;
    }

    /* Bring interface up */
    OspfIfUp (pInterface);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfSetPassive                                               */
/*                                                                           */
/* Description    This function makes an interface as Passive.               */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface which has to      */
/*                                be made passive                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IfSetPassive (tInterface * pInterface)
{
    UINT2               u2HelloInterval;
    INT4                i4RetStat = OSPF_SUCCESS;

    if (pInterface->bPassive == OSPF_TRUE)
    {
        TmrHelloDeleteTimer (&pInterface->helloTimer);
        if (pInterface->u1NetworkType == IF_NBMA)
        {
            TmrDeleteTimer (&pInterface->pollTimer);
        }
    }
    else
    {
        u2HelloInterval = pInterface->u2HelloInterval;
        if (pInterface->u1NetworkType == IF_BROADCAST)
        {
            u2HelloInterval = (UINT2) UtilJitter (u2HelloInterval, OSPF_JITTER);
        }
        else if (pInterface->u1NetworkType == IF_NBMA)
        {
            if (TmrSetTimer (&(pInterface->pollTimer), POLL_TIMER,
                             NO_OF_TICKS_PER_SEC * (pInterface->i4PollInterval))
                == (INT4) TMR_FAILURE)
            {
                i4RetStat = OSPF_FAILURE;
            }
        }
        if (TmrSetTimer (&(pInterface->helloTimer), HELLO_TIMER,
                         NO_OF_TICKS_PER_SEC * (u2HelloInterval))
            == (INT4) TMR_FAILURE)
        {
            i4RetStat = OSPF_FAILURE;
        }
    }
    return i4RetStat;
}

/******************************************************************************
Function : IfFindRouterIdFromIfAddr

Input    : pIfIpAddr
           pInterface

Output   : None

Returns  : Router id
******************************************************************************/
PUBLIC UINT4
IfFindRouterIdFromIfAddr (tIPADDR pIfIpAddr, tInterface * pInterface)
{
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    UINT4               u4RtrId = 0;

    if (UtilIpAddrComp (pIfIpAddr, pInterface->ifIpAddr) == OSPF_EQUAL)
    {
        u4RtrId = OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->pOspfCxt->rtrId);
    }
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (UtilIpAddrComp (pIfIpAddr, pNbr->nbrIpAddr) == OSPF_EQUAL)
        {
            u4RtrId = OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId);
        }
    }
    return u4RtrId;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IfAllPassive                                               */
/*                                                                           */
/* Description    This function scan all the interface and                   */
/*                make the pInterface->bPassive as true or false        */
/*                depend upon the incoming value.                            */
/*                                                                           */
/* Input        : pOspfCxt      : pointer to the context which has to        */
/*                                be made passive                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IfAllPassive (tOspfCxt * pOspfCxt)
{
    UINT1               u1UpFlag = OSPF_FALSE;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode;
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        /* Bring the interface down */
        if (pInterface->u1IsmState != IFS_DOWN)
        {
            OspfIfDown (pInterface);
            u1UpFlag = OSPF_TRUE;
        }

        pInterface->bPassive = pOspfCxt->bDefaultPassiveInterface;

        /* Bring interface up */
        if (u1UpFlag == OSPF_TRUE)
        {
            OspfIfUp (pInterface);
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssHealthChkClearCtrOspf                                   */
/*                                                                           */
/* Description    This function clears the counters.                         */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfHealthChkClearCtr ()
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    UINT4               u4MaxCxt = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4OspfAreaId = 0;
    UINT4               u4OspfPrevAreaId = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4ContextId = 0; u4ContextId < u4MaxCxt; u4ContextId++)
    {
        OspfLock ();
        if (gOsRtr.apOspfCxt[u4ContextId] == NULL)
        {
            /* Get the next context id */
            OspfUnLock ();
            continue;
        }
        pOspfCxt = gOsRtr.apOspfCxt[u4ContextId];
        pOspfCxt->u4OriginateNewLsa = 0;
        pOspfCxt->u4RcvNewLsa = 0;
        pOspfCxt->u4OspfPktsDisd = 0;
        if (nmhGetFirstIndexOspfAreaTable (&u4OspfAreaId) != SNMP_FAILURE)
        {
            gOsRtr.pOspfCxt = pOspfCxt;
            do
            {

                OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
                if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
                {
                    pArea->u2SpfRuns = 0;
                    pArea->u4AreaBdrRtrCount = 0;
                    pArea->u4AsBdrRtrCount = 0;
                    pArea->u4NssaTrnsltrEvents = 0;
                }
                u4OspfPrevAreaId = u4OspfAreaId;
            }
            while (nmhGetNextIndexOspfAreaTable
                   (u4OspfPrevAreaId, &u4OspfAreaId) != SNMP_FAILURE);
        }

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            pInterface->aIfEvents = 0;
            pInterface->u4HelloRcvdCount = 0;
            pInterface->u4DdpRcvdCount = 0;
            pInterface->u4LsaReqRcvdCount = 0;
            pInterface->u4LsaUpdateRcvdCount = 0;
            pInterface->u4LsaAckRcvdCount = 0;
            pInterface->u4HelloTxedCount = 0;
            pInterface->u4DdpTxedCount = 0;
            pInterface->u4LsaReqTxedCount = 0;
            pInterface->u4LsaUpdateTxedCount = 0;
            pInterface->u4LsaAckTxedCount = 0;
            pInterface->u4HelloDisdCount = 0;
            pInterface->u4DdpDisdCount = 0;
            pInterface->u4LsaReqDisdCount = 0;
            pInterface->u4LsaUpdateDisdCount = 0;
            pInterface->u4LsaAckDisdCount = 0;

        }

        pLstNode = NULL;

        TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            pInterface->aIfEvents = 0;
            pInterface->aIfEvents = 0;
            pInterface->u4HelloRcvdCount = 0;
            pInterface->u4DdpRcvdCount = 0;
            pInterface->u4LsaReqRcvdCount = 0;
            pInterface->u4LsaUpdateRcvdCount = 0;
            pInterface->u4LsaAckRcvdCount = 0;
            pInterface->u4HelloTxedCount = 0;
            pInterface->u4DdpTxedCount = 0;
            pInterface->u4LsaReqTxedCount = 0;
            pInterface->u4LsaUpdateTxedCount = 0;
            pInterface->u4LsaAckTxedCount = 0;
            pInterface->u4HelloDisdCount = 0;
            pInterface->u4DdpDisdCount = 0;
            pInterface->u4LsaReqDisdCount = 0;
            pInterface->u4LsaUpdateDisdCount = 0;
            pInterface->u4LsaAckDisdCount = 0;

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                pNbr->u4NbrEvents = 0;
            }

        }

        pLstNode = NULL;

        TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
            pNbr->u4NbrEvents = 0;

        }

        OspfUnLock ();
    }

    return;
}

/*******************************************
 Function :  IfOspfBfdRegister

 Input    :  pNbr

 Output   :  None

 Returns  :  OSPF_SUCCESS/OSPF_FAILURE
*******************************************/
PUBLIC INT4
IfOspfBfdRegister (tNeighbor * pNbr)
{
#ifdef BFD_WANTED
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1EventMask[1] = { 0 };

    MEMSET (au1EventMask, 0, sizeof (au1EventMask));

    if (pNbr == NULL)
    {
        return OSPF_FAILURE;
    }

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return OSPF_FAILURE;
    }
#ifdef RM_WANTED
    if (RmGetNodeState () == RM_STANDBY)
    {
        return OSPF_SUCCESS;
    }
#endif
    /* Checking for BFD OSPF status Globally and
     * BFD status on All interfaces or specific interface*/
    if ((pOspfCxt->u1BfdAdminStatus == OSPF_BFD_ENABLED) &&
        ((pOspfCxt->u1BfdStatusInAllIf == OSPF_BFD_ENABLED) ||
         (pNbr->pInterface->u1BfdIfStatus == OSPF_BFD_ENABLED)))

    {
        /* Filling Ospf info for Registering */
        gBfdInParams.u4ReqType = BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING;
        gBfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_OSPF;
        gBfdInParams.u4ContextId = pOspfCxt->u4OspfCxtId;

        MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                &pNbr->nbrIpAddr, MAX_IPV4_ADDR_LEN);
        MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                &pNbr->pInterface->ifIpAddr, MAX_IPV4_ADDR_LEN);

        gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
            IPVX_ADDR_FMLY_IPV4;
        gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex =
            OspfGetIfIndexFromPort (pNbr->pInterface->u4IfIndex);
        gBfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;

        /* The Desired detection timer is not configurable. hence making
         * this value as default detection multipler as BFD 3Sec*/
        gBfdInParams.BfdClntInfoParams.u4DesiredDetectionTime =
            BFD_DEFAULT_TIME_OUT;

        /* Informing status down notification to OSPF Que.
         * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
         * Hence the bit + 1 used to set/check the bit */
        OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);
        MEMCPY (&(gBfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

        gBfdInParams.BfdClntInfoParams.pBfdNotifyPathStatusChange =
            OspfHandleNbrPathStatusChange;

        /* BFD API call for registering OSPF Nbr path monitroing */
        if (BfdApiHandleExtRequest (pOspfCxt->u4OspfCxtId, &gBfdInParams,
                                    NULL) != OSIX_SUCCESS)
        {
            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId, "Bfd Registration failed\n");
            return OSPF_FAILURE;
        }
        else
        {
            pNbr->u1NbrBfdState = OSPF_BFD_ENABLED;
        }
    }
#else
    UNUSED_PARAM (pNbr);
#endif
    return OSPF_SUCCESS;
}

/*******************************************
 Function :  IfOspfBfdDeRegister

 Input    :  pNbr

 Output   :  None

 Returns  :  OSPF_SUCCESS/OSPF_FAILURE
*******************************************/
PUBLIC INT4
IfOspfBfdDeRegister (tNeighbor * pNbr, BOOL1 bIsDynDis)
{
#ifdef BFD_WANTED
    tOspfCxt           *pOspfCxt = NULL;

    if (pNbr == NULL)
    {
        return OSPF_FAILURE;
    }

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return OSPF_FAILURE;
    }

#ifdef RM_WANTED
    if (RmGetNodeState () == RM_STANDBY)
    {
        return OSPF_SUCCESS;
    }
#endif

    if (bIsDynDis)
    {
        gBfdInParams.u4ReqType = BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING;
    }
    else
    {
        gBfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING;
    }

    gBfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_OSPF;
    gBfdInParams.u4ContextId = pOspfCxt->u4OspfCxtId;
    gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex =
        OspfGetIfIndexFromPort (pNbr->pInterface->u4IfIndex);

    MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
            &pNbr->nbrIpAddr, MAX_IPV4_ADDR_LEN);
    MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
            &pNbr->pInterface->ifIpAddr, MAX_IPV4_ADDR_LEN);

    gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
        IPVX_ADDR_FMLY_IPV4;
    gBfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;

    /* API call to Deregister Nbr path monitroing by BFD */
    if (BfdApiHandleExtRequest (pOspfCxt->u4OspfCxtId, &gBfdInParams,
                                NULL) != OSIX_SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Bfd DeRegistration failed\n");
        return OSPF_FAILURE;
    }
    else
    {
        pNbr->u1NbrBfdState = OSPF_BFD_DISABLED;
    }
#else
    UNUSED_PARAM (pNbr);
    UNUSED_PARAM (bIsDynDis);
#endif /* BFD_WANTED */
    return OSPF_SUCCESS;
}

/********************************
 Function :  OspfHandleNbrPathStatusChange

 Input    :  u4ContextId
             pNbrPathInfo 

 Output   :  None

 Returns  :  OSIX_SUCCESS/OSIX_FAILURE
*******************************************/
#ifdef BFD_WANTED
INT1
OspfHandleNbrPathStatusChange (UINT4 u4ContextId,
                               tBfdClientNbrIpPathInfo * pNbrPathInfo)
{
    tOspfQMsg          *pOspfQMsg;
    tOspfCxt           *pOspfCxt = NULL;

    pOspfCxt = UtilOspfGetCxt (u4ContextId);
    if (pOspfCxt == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pNbrPathInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        if (BfdApiCheckControlPlaneDependent
            (pOspfCxt->u4OspfCxtId, pNbrPathInfo) == OSIX_SUCCESS)
        {
            GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_TERM_BFD_DOWN);
            return OSIX_SUCCESS;
        }
    }
    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                      OSPF_INVALID_CXT_ID, " Alloc Failure\n");
        return OSIX_FAILURE;
    }
    pOspfQMsg->u4MsgType = OSPF_BFD_NBR_DOWN;

    MEMCPY (&(pOspfQMsg->OspfBfdMsgInfo), pNbrPathInfo,
            sizeof (tBfdClientNbrIpPathInfo));

    pOspfQMsg->u4OspfCxtId = pOspfCxt->u4OspfCxtId;
    if (OsixSendToQ
        ((UINT4) 0, (const UINT1 *) "OSPQ",
         (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        QMSG_FREE (pOspfQMsg);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfQMsg->u4OspfCxtId,
                  "Enqueue To OSPF Failed\n");
        return OSIX_FAILURE;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, pOspfQMsg->u4OspfCxtId,
                  "Send Evt Failed\n");
    }
    OSPF_TRC (CONTROL_PLANE_TRC, pOspfQMsg->u4OspfCxtId,
              "Pkt Enqueued To OSPF\n");
    return OSIX_SUCCESS;

}
#endif

/********************************************************
 Function :  OspfSecIpNetworkDel

 Description: Deletes the given secondary IP ospf network

 Input    :  pSecIpIfInfo 

 Output   :  None

 Returns  :  OSIX_SUCCESS/OSIX_FAILURE
********************************************************/
PUBLIC VOID
OspfSecIpNetworkDel (tNetIpv4IfInfo * pSecIpIfInfo)
{
    tNetIpv4IfInfo      PriIpIfInfo;
    tOspfCxt           *pOspfCxt = NULL;
    tIPADDR             primIpAddr;
    UINT4               u4Index = 0;
    tInterface         *pInterface = NULL;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    INT2                i2SecCount = 0;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    MEMSET (&PriIpIfInfo, 0, sizeof (PriIpIfInfo));
#ifdef SNMP_2_WANTED
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif
    MEMSET (&primIpAddr, 0, sizeof (tIPADDR));

    /* In Standby Node , the LSA Regeneration will take happen thro 
     * nmhSetFutOspfSecIfStatus function - DESTROY Case*/

#ifdef RM_WANTED
    if (RmGetNodeState () == RM_STANDBY)
    {
        return;
    }
#endif

    if (NetIpv4GetPortFromIfIndex (pSecIpIfInfo->u4IfIndex, &u4Index)
        == NETIPV4_FAILURE)
    {
        return;
    }

    if (NetIpv4GetIfInfo (u4Index, &PriIpIfInfo) == NETIPV4_FAILURE)
    {
        return;
    }

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, PriIpIfInfo.u4Addr);

    pOspfCxt = UtilOspfGetCxt (pSecIpIfInfo->u4ContextId);
    if (pOspfCxt == NULL)
    {
        return;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfSecIfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfSecIfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 5;
#endif
/* Get the corresponding primary OSPF network - pInterface and remove the matching 
 * secondary IP in the ifSecIP array (also update the secondary count). And then 
 * move/shift the arrays elements below the deleted entry - up, so that there is 
 * no empty row in between*/

    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr, 0)) != NULL)
    {
        while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
        {
            if (UtilAddrMaskCmpe
                (OSPF_CRU_BMC_DWFROMPDU
                 (pInterface->secondaryIP.ifSecIp[i2SecCount].
                  ifSecIpAddr),
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->secondaryIP.
                                         ifSecIp[i2SecCount].
                                         ifSecIpAddrMask),
                 pSecIpIfInfo->u4Addr, pSecIpIfInfo->u4NetMask) == OSPF_EQUAL)
            {

                /* shifting the entries following the one found above one
                   way up */
                while (i2SecCount < (pInterface->secondaryIP.i1SecIpCount - 1))
                {
                    if (i2SecCount < (MAX_SEC_INTERFACES - 1))
                    {
                        IP_ADDR_COPY (pInterface->secondaryIP.ifSecIp
                                      [i2SecCount].ifSecIpAddr,
                                      pInterface->secondaryIP.
                                      ifSecIp[i2SecCount + 1].ifSecIpAddr);
                        IP_ADDR_COPY (pInterface->secondaryIP.
                                      ifSecIp[i2SecCount].
                                      ifSecIpAddrMask,
                                      pInterface->secondaryIP.
                                      ifSecIp[i2SecCount + 1].ifSecIpAddrMask);

                        pInterface->secondaryIP.ifSecIp[i2SecCount].
                            u1Status =
                            pInterface->secondaryIP.ifSecIp[i2SecCount +
                                                            1].u1Status;
                    }
                    i2SecCount++;
                }                /* end while */
                pInterface->secondaryIP.i1SecIpCount--;

                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC,
                              pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                    break;
                }

                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);

                pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                pOspfIfParam->param.sig_param.pStruct =
                    (UINT1 *) pInterface->pArea;
                pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
                pOspfIfParam->pOspfCxt = pOspfCxt;

                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC,
                              pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
                    break;
                }

                break;
            }
            i2SecCount++;
        }                        /* End of While */

    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                      pSecIpIfInfo->u4ContextId, PriIpIfInfo.u4Addr, OSPF_ZERO,
                      pSecIpIfInfo->u4Addr, pSecIpIfInfo->u4NetMask, DESTROY));
#endif
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file osif.c                          */
/*-----------------------------------------------------------------------*/
