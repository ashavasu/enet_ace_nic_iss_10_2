/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osrtl.c,v 1.12 2014/10/29 12:51:00 siva Exp $
 *
 * Description:This file contains procedures concerned with 
 *             routing table lookup.
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : RtlRtLookup                                                */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 11.1.                         */
/*                This procedure scans the entire routing table and finds the*/
/*                most appropriate route for the given destination addr. Used*/
/*                during forwarding.                                         */
/*                                                                           */
/* Input        : pOspfCxt       : ospf context pointer                      */
/*                pCurrRt        : routing table                             */
/*                pDestIpAddr   : IP address                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : path, if found                                             */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPath       *
RtlRtLookupInCxt (tOspfCxt * pOspfCxt,
                  tOspfRt * pCurrRt, tIPADDR * pDestIpAddr, INT1 i1Tos)
{

    tRtEntry           *pRtEntry;
    tPath              *pCurrPath;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4Key[2];
    tArea              *pArea;
    UINT1               u1Index;
    VOID               *pTemp = NULL;

    /* 
     * This procedure initialises current best path to NULL and updates it
     * as each matching entry is encountered.
     */

    pRtEntry = (tRtEntry *) NULL;
    pCurrPath = (tPath *) NULL;

    au4Key[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (*pDestIpAddr));
    au4Key[1] = 0xFFFFFFFF;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pCurrRt->pOspfRoot;
    inParams.i1AppId = pCurrRt->i1OspfId;
    inParams.pLeafNode = NULL;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams, (VOID **) &pTemp) != SUCCESS)
    {
        return NULL;
    }
    pRtEntry = (tRtEntry *) (VOID *) ((UINT1 *) (outParams.pAppSpecInfo));

    if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED)
    {
        return NULL;
    }

    if ((!(IS_DEST_NETWORK (pRtEntry))) && (!(IS_DEST_ASBR (pRtEntry))))
    {
        return NULL;
    }

    if ((pCurrPath = RtcGetRtTosPath (pRtEntry, i1Tos, OSPF_NEW_PATH)) != NULL)
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {

            for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
            {

                if ((pArea->aAddrRange[u1Index].areaAggStatus != INVALID)
                    &&
                    (UtilIpAddrMaskComp
                     (pRtEntry->destId,
                      pArea->aAddrRange[u1Index].ipAddr,
                      pArea->aAddrRange[u1Index].ipAddrMask) ==
                     OSPF_EQUAL) && (pCurrPath->u1PathType == INTER_AREA))
                {
                    return NULL;

                }
            }
        }
        return pCurrPath;
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtlNssaRtLookup                                            */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 11.1.                         */
/*                This procedure scans the entire routing table and finds the*/
/*                most appropriate route for the given destination addr. Used*/
/*                during forwarding.                                         */
/*                                                                           */
/* Input        : pCurrRt        : routing table                             */
/*                pDestIpAddr   : IP address                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Route Entry, if found                                      */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tRtEntry    *
RtlNssaRtLookup (tOspfRt * pCurrRt, tIPADDR * pDestIpAddr, INT1 i1Tos)
{

    tRtEntry           *pRtEntry;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4Key[2];
    VOID               *pTemp = NULL;

    /* 
     * This procedure initialises current best path to NULL and updates it
     * as each matching entry is encountered.
     */

    pRtEntry = (tRtEntry *) NULL;

    UNUSED_PARAM (i1Tos);

    au4Key[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (*pDestIpAddr));
    au4Key[1] = 0xFFFFFFFF;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pCurrRt->pOspfRoot;
    inParams.i1AppId = pCurrRt->i1OspfId;
    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams, (VOID **) &pTemp) != SUCCESS)
    {
        return NULL;
    }
    pRtEntry = (tRtEntry *) (VOID *) ((UINT1 *) (outParams.pAppSpecInfo));

    if (!(IS_DEST_NETWORK (pRtEntry)))
    {
        return NULL;
    }
    return pRtEntry;
}

/*-----------------------------------------------------------------------*/
/*                   End of the file   osrtl.c                           */
/*-----------------------------------------------------------------------*/
