/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osnstrln.c,v 1.13 2014/10/29 12:50:59 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             NSSA.
 *
 *******************************************************************/
#include "osinc.h"

/************************************************************************/
/*                                           */
/* Function     : NssaType7LsaTranslation                     */
/*                                      */
/* Description    : This routine performs Type 7 to Type 5  translation    */
/*                                         */
/* Input          : pArea - Pointer to area                */
/*              pLsaInfo - Pointer to tLsaInfo         */
/*                                         */
/* Output       : None                          */
/*                                       */
/* Returns      : VOID                          */
/*                                      */
/************************************************************************/
PUBLIC VOID
NssaType7LsaTranslation (tArea * pArea, tLsaInfo * pLsaInfo)
{
    tIPADDRMASK         destIpAddrMask;
    tRtEntry           *pDestRtEntry = NULL;
    tPath              *pPath = NULL;
    tIPADDR             fwdAddress;
    UINT1               u1Offset;
    UINT4               u4FwdAddr;

/*
    u1TrnsltType5 can take three values     
    REDISTRIBUTED_TYPE5  - 3     
    For Type 5 implies route imported from RTM            
    OSPF_TRUE             - 1     
    For Type 5 it implies it is translated Type 5    
    OSPF_FALSE    - 2    
    For Type 5 it implies unapproved Trnsltd Type 5        
    At the end of translation any unapproved Type 5         
    needs to be FLUSHED    
*/

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaType7LsaTranslation\n");

    if ((!(pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)) ||
        (pArea->u1NssaTrnsltrState == TRNSLTR_STATE_DISABLED) ||
        (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        return;
    }

    if (IS_P_BIT_SET (pLsaInfo) == RAG_NO_CHNG)
    {

        OSPF_TRC2 (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                   "Pbit Clear, LSA FRM Rtr %x, for net %x not trnsltd\n",
                   pLsaInfo->lsaId.advRtrId, pLsaInfo->lsaId.linkStateId);
        return;
    }
    u1Offset = Type7LSA_FWD_ADDR_OFFSET (TOS_0);
    u4FwdAddr = GET_Type7LSA_FwdAddr (pLsaInfo->pLsa, u1Offset);
    OSPF_CRU_BMC_DWTOPDU (fwdAddress, u4FwdAddr);
    if (UtilIpAddrComp (fwdAddress, gNullIpAddr) == OSPF_EQUAL)
    {
        OSPF_TRC2 (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                   "Fwd Addr 0.0.0.0, LSA FRM Rtr %x, for net %x not trnsltd\n",
                   pLsaInfo->lsaId.advRtrId, pLsaInfo->lsaId.linkStateId);
        return;
    }

    OS_MEM_CPY (destIpAddrMask,
                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

    pDestRtEntry =
        RtcFindRtEntry (pArea->pOspfCxt->pOspfRt,
                        &(pLsaInfo->lsaId.linkStateId),
                        &(destIpAddrMask), DEST_NETWORK);
    if (pDestRtEntry != NULL)
    {
        pPath = GET_TOS_PATH (pDestRtEntry, TOS_0);

        if (pPath != NULL)
        {
            if ((pPath->u1PathType == INTRA_AREA) ||
                (pPath->u1PathType == INTER_AREA) ||
                (pPath->pLsaInfo != pLsaInfo))
            {
                return;
            }
        }
    }
    if (NssaType7AddrRangeAgg (pArea, pLsaInfo) == OSPF_TRUE)
    {
        OSPF_TRC2 (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                   "LSA FRM Rtr %x, for net %x either"
                   "aggregated or DoNotAdvertise\n",
                   pLsaInfo->lsaId.advRtrId, pLsaInfo->lsaId.linkStateId);
        return;
    }

    NssaTrnsltType7NotInRng (pLsaInfo);

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: NssaType7LsaTranslation\n");
}

/************************************************************************/
/*                                           */
/* Function     : NssaAdvertiseType7Rngs                     */
/*                                      */
/* Description    : This routine adv Type 7 Ranges    */
/*                                         */
/* Input          : pArea - Pointer to area                */
/*                                         */
/* Output       : None                          */
/*                                       */
/* Returns      : VOID                          */
/*                                      */
/************************************************************************/
PUBLIC VOID
NssaAdvertiseType7Rngs (tArea * pArea)
{
    tAddrRange         *pCurrRange = NULL;
    UINT4               u4Index;
    tIPADDR             rngIpAddr;
    tLsaInfo           *pSelfLsdbLsaInfo = NULL;
    UINT1               u1GenType5FrmRng;
    UINT1               u1ReOrgFrmRng;
    UINT1               u1Tos;
    tExtLsaLink         lsdbExtLsaLink;
    tLsaInfo           *pRngLsaInfo = NULL;
    tIPADDR             lsaIpAddrMask;
    tExtRoute          *pExtRoute = NULL;

    if ((pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        /* LSA's cannot be originated during graceful restart 
         * or in standby node */
        return;
    }

    for (u4Index = MIN_ADDR_RANGES_PER_AREA;
         u4Index < MAX_ADDR_RANGES_PER_AREA; u4Index++)
    {
        pCurrRange = &(pArea->aAddrRange[u4Index]);
        if ((pCurrRange->areaAggStatus != ACTIVE) &&
            (pCurrRange->areaAggStatus != NOT_IN_SERVICE))
        {
            /* Address Range is maintained in sorted order. First empty  */
            /* Row indicates no more valid address ranges            */
            break;
        }

        if ((pCurrRange->areaAggStatus != ACTIVE) ||
            (pCurrRange->u1LsdbType != NSSA_EXTERNAL_LINK))
        {
            continue;
        }

        UtilIpAddrMaskCopy (rngIpAddr, pCurrRange->ipAddr,
                            pCurrRange->ipAddrMask);

        /* Single LSA is falling in this range. So translate that LSA without
         * Aggregating */
        if (pCurrRange->u4LsaCount == OSPF_TRUE)
        {
            OS_MEM_CPY (lsaIpAddrMask,
                        (UINT1 *) (pCurrRange->pLsaInfo->pLsa + LS_HEADER_SIZE),
                        MAX_IP_ADDR_LEN);

            if ((UtilIpAddrComp (pCurrRange->pLsaInfo->lsaId.linkStateId,
                                 rngIpAddr) == OSPF_EQUAL) &&
                (UtilIpAddrComp (lsaIpAddrMask,
                                 pCurrRange->ipAddrMask) == OSPF_EQUAL))
            {
                pRngLsaInfo =
                    LsuDatabaseLookUp (AS_EXT_LSA, &(rngIpAddr),
                                       &(pCurrRange->ipAddrMask),
                                       &(pArea->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea->pOspfCxt->pBackbone);

                if (pRngLsaInfo != NULL)
                {
                    if (pRngLsaInfo->pLsaDesc->u1InternalLsaType !=
                        AS_TRNSLTD_EXT_RNG_LSA)
                    {
                        pRngLsaInfo->u1TrnsltType5 = OSPF_TRUE;
                        pCurrRange->u4RngUpdt = OSPF_FALSE;
                        pCurrRange->u4LsaCount = RAG_NO_CHNG;
                    }
                    continue;
                }
            }
        }
        else if (pCurrRange->u4LsaCount == RAG_NO_CHNG)
        {
            pCurrRange->u4RngUpdt = OSPF_FALSE;
            continue;
        }
        /* Range has more than 1 LSA falling in. Need to aggregate.
         * Check the validity */

        if ((pCurrRange->u1AdvertiseStatus == DO_NOT_ADVERTISE_MATCHING) ||
            (pCurrRange->u4RngUpdt == OSPF_FALSE))
        {

            continue;
        }

        pCurrRange->u4RngUpdt = OSPF_FALSE;

        NssaFlshType5TrnsltdInCxt (pArea->pOspfCxt, pCurrRange);
        pSelfLsdbLsaInfo =
            LsuDatabaseLookUp (AS_EXT_LSA, &(rngIpAddr),
                               &(pCurrRange->ipAddrMask),
                               &(pArea->pOspfCxt->rtrId), NULL,
                               (UINT1 *) pArea->pOspfCxt->pBackbone);

        u1GenType5FrmRng = OSPF_FALSE;
        u1ReOrgFrmRng = OSPF_FALSE;
        if (pSelfLsdbLsaInfo != NULL)
        {
            pSelfLsdbLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pCurrRange;

            /* TosSupport */
            for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                RtcGetExtLsaLink (pSelfLsdbLsaInfo->pLsa,
                                  pSelfLsdbLsaInfo->u2LsaLen,
                                  u1Tos, &lsdbExtLsaLink);

                if ((lsdbExtLsaLink.u4Cost !=
                     pCurrRange->aMetric[u1Tos].u4Value) ||
                    (lsdbExtLsaLink.u1MetricType !=
                     pCurrRange->aMetric[u1Tos].u4MetricType) ||
                    (UtilIpAddrComp (lsdbExtLsaLink.fwdAddr,
                                     gNullIpAddr) != OSPF_EQUAL) ||
                    (lsdbExtLsaLink.u4ExtRouteTag != pCurrRange->u4ExtRtTag))
                {
                    if (pSelfLsdbLsaInfo->pLsaDesc->u1InternalLsaType
                        == AS_TRNSLTD_EXT_RNG_LSA)
                    {
                        u1ReOrgFrmRng = OSPF_TRUE;
                        pExtRoute =
                            GetFindExtRouteInCxt (pArea->pOspfCxt,
                                                  &(pCurrRange->ipAddr),
                                                  &(pCurrRange->ipAddrMask));

                        if (pExtRoute != NULL)
                        {
                            if (pExtRoute->extrtParam[u1Tos].u1MetricType !=
                                pCurrRange->aMetric[u1Tos].u4MetricType)
                            {
                                if (pExtRoute->extrtParam[u1Tos].u1MetricType ==
                                    TYPE_2_METRIC)
                                {
                                    pCurrRange->aMetric[u1Tos].u4Value =
                                        pExtRoute->aMetric[u1Tos].u4Value;
                                    pCurrRange->aMetric[u1Tos].u4MetricType =
                                        pExtRoute->extrtParam[u1Tos].
                                        u1MetricType;
                                }
                            }
                        }
                        break;
                    }
                    else
                    {
                        u1GenType5FrmRng = OSPF_TRUE;
                    }
                    if (lsdbExtLsaLink.u1MetricType == TYPE_2_METRIC)
                    {

                        if (pCurrRange->aMetric[u1Tos].u4MetricType ==
                            TYPE_2_METRIC)
                        {
                            if (lsdbExtLsaLink.u4Cost >
                                pCurrRange->aMetric[u1Tos].u4Value)
                            {
                                pCurrRange->aMetric[u1Tos].u4Value =
                                    lsdbExtLsaLink.u4Cost;
                            }
                        }
                        else
                        {
                            pCurrRange->aMetric[u1Tos].u4Value =
                                lsdbExtLsaLink.u4Cost;
                        }

                        pCurrRange->aMetric[u1Tos].u4MetricType =
                            lsdbExtLsaLink.u1MetricType;
                    }

                    if ((pCurrRange->aMetric[u1Tos].u4MetricType ==
                         TYPE_1_METRIC) &&
                        (lsdbExtLsaLink.u1MetricType == TYPE_1_METRIC))
                    {
                        if (lsdbExtLsaLink.u4Cost >
                            pCurrRange->aMetric[u1Tos].u4Value)
                        {
                            pCurrRange->aMetric[u1Tos].u4Value =
                                lsdbExtLsaLink.u4Cost;
                        }
                    }
                }
            }
            if ((u1ReOrgFrmRng == OSPF_FALSE) &&
                (u1GenType5FrmRng == OSPF_FALSE))
            {
                if (pSelfLsdbLsaInfo->u1TrnsltType5 == OSPF_FALSE)
                {
                    pSelfLsdbLsaInfo->u1TrnsltType5 = OSPF_TRUE;
                }
            }
        }
        else
        {
            u1GenType5FrmRng = OSPF_TRUE;
        }

        if (u1GenType5FrmRng == OSPF_TRUE)
        {
            /* Backbone Area is passed to access the context pointer 
               in OlsModifyLsId */
            OlsModifyLsId (pArea->pOspfCxt->pBackbone, AS_TRNSLTD_EXT_RNG_LSA,
                           &(rngIpAddr), (UINT1 *) pCurrRange);

            OlsGenerateLsa (pArea->pOspfCxt->pBackbone, AS_TRNSLTD_EXT_RNG_LSA,
                            &(rngIpAddr), (UINT1 *) pCurrRange);
        }
        else if (u1ReOrgFrmRng == OSPF_TRUE)
        {
            OlsSignalLsaRegenInCxt (pArea->pOspfCxt, SIG_NEXT_INSTANCE,
                                    (UINT1 *) pSelfLsdbLsaInfo->pLsaDesc);
        }
    }
}

/****************************************************************/
/*                                     */
/* Function     : NssaFlshType5TrnsltdInCxt                 */
/*                                       */
/* Description    : Flush translated Type 5 falling */
/*                       in the range to be advertised */
/*                            */
/*                                     */
/* Input          : pAddrRng - Address Range    */
/*                                    */
/* Output       : None                       */
/*                                      */
/* Returns      : VOID                        */
/*                                     */
/****************************************************************/
PUBLIC VOID
NssaFlshType5TrnsltdInCxt (tOspfCxt * pOspfCxt, tAddrRange * pAddrRng)
{

    tTMO_SLL_NODE      *pLsaNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tAddrRange         *pLsaAddrRng = NULL;    /* TC 34 */
    tOsDbNode          *pOsDbNode = NULL;
    UINT4               u4RangeAddr;
    UINT4               u4RangeMask;
    UINT4               u4LsaAddr;
    UINT4               u4LsaMask;    /* TC 34 */
    UINT4               u4HashKey = 0;

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable,
                              u4HashKey, pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLsaNode);
                if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                                    pOspfCxt->rtrId) != OSPF_EQUAL)
                {
                    continue;
                }
                if (pLsaInfo->u1TrnsltType5 != OSPF_TRUE)
                {
                    continue;
                }

                u4RangeAddr = OSPF_CRU_BMC_DWFROMPDU (pAddrRng->ipAddr);
                u4RangeMask = OSPF_CRU_BMC_DWFROMPDU (pAddrRng->ipAddrMask);
                u4LsaAddr = OSPF_CRU_BMC_DWFROMPDU
                    (pLsaInfo->lsaId.linkStateId);

                if (u4LsaAddr == (u4RangeAddr & u4RangeMask))
                {
                    continue;
                }

                if ((u4LsaAddr & u4RangeMask) == u4RangeAddr)
                {
                    if (pLsaInfo->pLsaDesc->u1InternalLsaType
                        == AS_TRNSLTD_EXT_RNG_LSA)
                    {
                        pLsaAddrRng = (tAddrRange *) (VOID *)
                            pLsaInfo->pLsaDesc->pAssoPrimitive;
                        u4LsaMask = OSPF_CRU_BMC_DWFROMPDU
                            (pLsaAddrRng->ipAddrMask);

                        if (u4LsaMask > u4RangeMask)
                        {
                            OSPF_TRC (OSPF_NSSA_TRC, pOspfCxt->u4OspfCxtId,
                                      "LSA is frm More Spec Rng. Shud not flsh \n");
                        }
                        else
                        {
                            AgdFlushOut (pLsaInfo);
                        }
                    }
                    else
                    {
                        AgdFlushOut (pLsaInfo);
                    }
                }
            }
        }
    }
}

/****************************************************************/
/*                                     */
/* Function     : NssaType7AddrRangeAgg                 */
/*                                       */
/* Description    : This routine aaggregates Type 7 LSAs */
/*                       based on Type 7 address ranges*/
/*                            */
/*                                     */
/* Input          : pArea - Pointer to the area            */
/*          pLsaInfo - Pointer to tLsaInfo for Type 7 LSA */
/*                                    */
/* Output       : None                       */
/*                                      */
/* Returns      : VOID                        */
/*                                     */
/****************************************************************/
PUBLIC INT4
NssaType7AddrRangeAgg (tArea * pArea, tLsaInfo * pLsaInfo)
{

    UINT4               u4RangeAddr;
    UINT4               u4RangeMask;
    UINT4               u4LsaAddr;
    UINT4               u4LsaMask;
    UINT4               u4MtchdRangeMask = RAG_NO_CHNG;
    UINT4               u4Index, u4Indice = RAG_NO_CHNG;
    tAddrRange          currRange;
    UINT1               u1MtchRng = OSPF_FALSE;

    for (u4Index = MIN_ADDR_RANGES_PER_AREA;
         u4Index < MAX_ADDR_RANGES_PER_AREA; u4Index++)
    {
        currRange = pArea->aAddrRange[u4Index];
        if ((currRange.areaAggStatus != ACTIVE) &&
            (currRange.areaAggStatus != NOT_IN_SERVICE))
        {
            /* Address Range is maintained in sorted order. First empty  */
            /* Row indicates no more valid address ranges            */
            break;
        }

        if (currRange.u1LsdbType != NSSA_EXTERNAL_LINK)
        {
            continue;
        }
        u4RangeAddr = OSPF_CRU_BMC_DWFROMPDU (currRange.ipAddr);
        u4RangeMask = OSPF_CRU_BMC_DWFROMPDU (currRange.ipAddrMask);
        u4LsaAddr = OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        u4LsaMask = OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pLsa + LS_HEADER_SIZE);

        if (((u4LsaAddr & u4RangeMask) == u4RangeAddr) &&
            (u4LsaMask >= u4RangeMask) && (currRange.areaAggStatus == ACTIVE))
        {
            /* There is a range which has a greater mask matching the earlier 
               matched range. The new range is the one to be checked */
            if (u4MtchdRangeMask < u4RangeMask)
            {
                u4MtchdRangeMask = u4RangeMask;
                u4Indice = u4Index;
                u1MtchRng = OSPF_TRUE;
                /* We continue further to check for higher range if available */
            }
        }
    }

    /* If no matching range is found */
    if (u1MtchRng == OSPF_FALSE)
    {
        return OSPF_FALSE;
    }

    /* Updates Metric/Type for range */
    if (u4Indice < MAX_ADDR_RANGES_PER_AREA)
    {
        NssaUpdtType7AggRange (pArea, pLsaInfo, u4Indice);
    }
    return OSPF_TRUE;
}

/****************************************************************/
/*                                   */
/* Function     : NssaUpdtType7AggRange                */
/*                                   */
/* Description    : This routine updates Type 7 address   */
/*                    range cost/path type based on cost/ */
/*                      path type advertised in LSA            */
/*                                     */
/* Input          : pArea    - Pointer to the area             */
/*          pLsaInfo - Pointer to tLsaInfo for Type 7 LSA */
/*          u4Indice - Index of Rng best mtchng this LSA     */
/*                                     */
/* Output       : None                       */
/*                                    */
/* Returns      : VOID                        */
/*                                     */
/****************************************************************/
PUBLIC VOID
NssaUpdtType7AggRange (tArea * pArea, tLsaInfo * pLsaInfo, UINT4 u4Indice)
{

    tAddrRange         *pType7Range = NULL;
    tLsaInfo           *pFlshLsaInfo = NULL;
    tIPADDR             rngIpAddr;

    pType7Range = &(pArea->aAddrRange[u4Indice]);
    UtilIpAddrMaskCopy (rngIpAddr, pType7Range->ipAddr,
                        pType7Range->ipAddrMask);

    /* To handle receiving of MAX AGE Type 7 LSA    */
    /* previously aggregated                */
    if (IS_MAX_AGE (pLsaInfo->u2LsaAge) ||
        (pLsaInfo->u1TrnsltType5 == FLUSHED_LSA))
    {
        NssaUpdtRngForLsa (pArea, u4Indice);
        if (pType7Range->u4LsaCount == RAG_NO_CHNG)
        {
            pFlshLsaInfo =
                LsuDatabaseLookUp (AS_EXT_LSA, &(rngIpAddr),
                                   &(pType7Range->ipAddrMask),
                                   &(pArea->pOspfCxt->rtrId),
                                   NULL, (UINT1 *) pArea->pOspfCxt->pBackbone);

            if ((pFlshLsaInfo != NULL) &&
                (pFlshLsaInfo->pLsaDesc->u1InternalLsaType ==
                 AS_TRNSLTD_EXT_RNG_LSA))
            {
                AgdFlushOut (pFlshLsaInfo);
            }
        }
        return;
    }

    if (pType7Range->u4LsaCount == OSPF_TRUE)
    {
        pType7Range->u4RngUpdt = OSPF_TRUE;
    }

    pType7Range->u4LsaCount++;

    if (pType7Range->u4LsaCount == OSPF_TRUE)
    {
        pType7Range->pLsaInfo = pLsaInfo;
    }
    else
    {
        pType7Range->pLsaInfo = NULL;
    }

    NssaUpdtRngCostType (pLsaInfo, pType7Range);
}

/****************************************************************/
/*                                   */
/* Function     : NssaChkSelfOrgType5                */
/*                                   */
/* Description    : This routine checks that if there    */
/*                   is  any self -originated Type 5 LSA   */
/*                  having same link state id as the LSA to be */
/*            translated */
/*                                     */
/* Input          : pLsaInfo - Pointer to tLsaInfo for Type 7 LSA*/
/*                                     */
/* Output       : None                       */
/*                                    */
/* Returns      : OSPF_TRUE - Type 7 is not Trnslted        */
/*                    OSPF_FALSE - Type 7 is Trnslted    */
/****************************************************************/
PUBLIC UINT1
NssaChkSelfOrgType5 (tLsaInfo * pLsaInfo)
{

    tLsaInfo           *pSelfLsdbLsaInfo = NULL;
    tExtLsaLink         lsdbExtLsaLink;
    tExtLsaLink         extLsaLink;
    tRtEntry           *pFwdAddrEntry = NULL;
    tRtEntry           *pLsdbFwdAddrEntry = NULL;
    UINT4               u4FwdAddrCost, u4lsdbFwdAddrCost;
    UINT1               u1DoNotTrnslt = OSPF_FALSE;

    pSelfLsdbLsaInfo =
        LsuSearchDatabase (AS_EXT_LSA,
                           &(pLsaInfo->lsaId.linkStateId),
                           &(pLsaInfo->pOspfCxt->rtrId), NULL,
                           (UINT1 *) pLsaInfo->pOspfCxt->pBackbone);

    if (pSelfLsdbLsaInfo != NULL)
    {
        if (pSelfLsdbLsaInfo->u1TrnsltType5 == REDISTRIBUTED_TYPE5)
        {
            RtcGetExtLsaLink (pSelfLsdbLsaInfo->pLsa,
                              pSelfLsdbLsaInfo->u2LsaLen,
                              TOS_0, &lsdbExtLsaLink);
            RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                              TOS_0, &extLsaLink);

            if (UtilIpAddrComp (extLsaLink.fwdAddr, gNullIpAddr) != OSPF_EQUAL)
            {
                pFwdAddrEntry =
                    RtlNssaRtLookup (pLsaInfo->pOspfCxt->pOspfRt,
                                     &(extLsaLink.fwdAddr), TOS_0);

                if (pFwdAddrEntry != NULL)
                {
                    u4FwdAddrCost = pFwdAddrEntry->u4Metric;
                }
                else
                {
                    u4FwdAddrCost = LS_INFINITY_24BIT;
                }
            }
            else
            {
                u4FwdAddrCost = RAG_NO_CHNG;
            }
            if (UtilIpAddrComp (lsdbExtLsaLink.fwdAddr,
                                gNullIpAddr) != OSPF_EQUAL)
            {
                pLsdbFwdAddrEntry =
                    RtlNssaRtLookup (pLsaInfo->pOspfCxt->pOspfRt,
                                     &(lsdbExtLsaLink.fwdAddr), TOS_0);

                if (pLsdbFwdAddrEntry != NULL)
                {
                    u4lsdbFwdAddrCost = pLsdbFwdAddrEntry->u4Metric;
                }
                else
                {
                    u4lsdbFwdAddrCost = LS_INFINITY_24BIT;
                }
            }
            else
            {
                u4lsdbFwdAddrCost = RAG_NO_CHNG;
            }

            if ((lsdbExtLsaLink.u1MetricType == TYPE_1_METRIC) &&
                (extLsaLink.u1MetricType == TYPE_2_METRIC))
            {
                u1DoNotTrnslt = OSPF_TRUE;
            }

            if ((lsdbExtLsaLink.u1MetricType == TYPE_2_METRIC) &&
                (extLsaLink.u1MetricType == TYPE_2_METRIC))
            {
                if (lsdbExtLsaLink.u4Cost < extLsaLink.u4Cost)
                {
                    u1DoNotTrnslt = OSPF_TRUE;
                }
                else if (lsdbExtLsaLink.u4Cost == extLsaLink.u4Cost)
                {
                    /* Compare distance to fwd address  and decide */
                    if (u4lsdbFwdAddrCost < u4FwdAddrCost)
                    {
                        u1DoNotTrnslt = OSPF_TRUE;
                    }
                }
            }

            if ((lsdbExtLsaLink.u1MetricType == TYPE_1_METRIC) &&
                (extLsaLink.u1MetricType == TYPE_1_METRIC))
            {
                /* Compare (cost+dis to fwd addr) and decide */
                if ((lsdbExtLsaLink.u4Cost + u4lsdbFwdAddrCost) <
                    (extLsaLink.u4Cost + u4FwdAddrCost))
                {
                    u1DoNotTrnslt = OSPF_TRUE;
                }
            }
        }
        else if (pSelfLsdbLsaInfo->pLsaDesc != NULL)
        {
            pSelfLsdbLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
        }
    }
    return u1DoNotTrnslt;
}

/****************************************************************/
/*                                   */
/* Function     : NssaTrnsltType7NotInRng                */
/*                                   */
/* Description    : This routine translates Type 7    */
/*                    which does not fall in any range  */
/*                                     */
/* Input          : pLsaInfo - Pointer to tLsaInfo for Type 7 LSA*/
/*                                     */
/* Output       : None                       */
/*                                    */
/* Returns      : Void        */
/*                     */
/****************************************************************/
PUBLIC VOID
NssaTrnsltType7NotInRng (tLsaInfo * pLsaInfo)
{

    UINT1               u1DoNotTrnslt;
    tLsaInfo           *pTempLsaInfo = NULL;
    tRtEntry           *pDestRtEntry = NULL;
    tIPADDRMASK         destIpAddrMask;
    tPath              *pPath = NULL;
    tIPADDR             lsId;
    tIPADDR             modLsId;
    UINT4               newLsId;
    UINT4               netMask1;

    /* Handle the case :
       Presence of functionally equivalent Type 5 LSA
       generated by some other router 
     */

    OS_MEM_CPY (destIpAddrMask,
                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

    pDestRtEntry =
        RtcFindRtEntry (pLsaInfo->pOspfCxt->pOspfRt,
                        &(pLsaInfo->lsaId.linkStateId),
                        &(destIpAddrMask), DEST_NETWORK);

    if (pDestRtEntry != NULL)
    {
        pPath = GET_TOS_PATH (pDestRtEntry, TOS_0);
        if (pPath != NULL)
        {
            if ((pPath->u1PathType == INTRA_AREA) ||
                (pPath->u1PathType == INTER_AREA))
            {
                return;
            }
        }
    }
    else
    {
        return;
    }

    /* Check if there is any self -originated Type 5 LSA
       having same link state id as the LSA to be 
       translated
     */

    u1DoNotTrnslt = NssaChkSelfOrgType5 (pLsaInfo);

    if (u1DoNotTrnslt == OSPF_TRUE)
    {
        OSPF_TRC (OSPF_NSSA_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "LSDB Type 5 LSA is same or preferred \n");
        return;
    }

    pLsaInfo->u1TrnsltType5 = OSPF_TRUE;

    /* Check whether Type-5 Lsas are already present with LSID as 
       DestID in which host bits are set */

    netMask1 = pDestRtEntry->u4IpAddrMask;
    newLsId = OSPF_CRU_BMC_DWFROMPDU (pDestRtEntry->destId);
    newLsId |= (~netMask1);
    OSPF_CRU_BMC_DWTOPDU (modLsId, newLsId);

    pTempLsaInfo =
        LsuSearchDatabase (AS_EXT_LSA, &(modLsId), &(pLsaInfo->pOspfCxt->rtrId),
                           NULL, (UINT1 *) pLsaInfo->pOspfCxt->pBackbone);

    if (pTempLsaInfo == NULL || pTempLsaInfo->pLsaDesc == NULL)
    {
        IP_ADDR_COPY (lsId, pDestRtEntry->destId);
        OlsModifyLsId (pLsaInfo->pOspfCxt->pBackbone, AS_TRNSLTD_EXT_LSA,
                       &(lsId), (UINT1 *) pDestRtEntry);
    }
    else
    {
        IP_ADDR_COPY (lsId, modLsId);
    }

    OlsGenerateLsa (pLsaInfo->pOspfCxt->pBackbone, AS_TRNSLTD_EXT_LSA, &(lsId),
                    (UINT1 *) pDestRtEntry);

}

/************************************************************************/
/*                                                                      */
/* Function     : NssaProcAggRange                                      */
/*                                                                      */
/* Description    : This routine performs Type 7 to Type 5  translation */
/*                                                                      */
/* Input          : None                                                */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
NssaProcAggRangeInCxt (tOspfCxt * pOspfCxt)
{
    tLsaInfo           *pLsaInfo = NULL;
    tOsDbNode          *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4HashKey = 0;

    NssaType7To5TranslatorInCxt (pOspfCxt);

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLstNode);
                if ((pLsaInfo->u1TrnsltType5 == OSPF_FALSE) &&
                    (UtilIpAddrComp (pOspfCxt->rtrId, pLsaInfo->lsaId.advRtrId)
                     == OSPF_EQUAL))
                {
                    /* While flushing check what all clean up is required */
                    AgdFlushOut (pLsaInfo);

                }
            }
        }

    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osnstrln.c
-----------------------------------------------------------------------*/
