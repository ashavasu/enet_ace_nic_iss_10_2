/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osrtc.c,v 1.62 2017/05/12 13:35:39 siva Exp $
 *
 * Description:This file contains procedures used in routing
 *             table calculation.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */
PRIVATE tInterface *RtcFindIfId PROTO ((tCandteNode * pVertex,
                                        UINT1 u1Type, tArea * pArea,
                                        INT1 i1Tos));
PRIVATE VOID RtcCheckAddrRangeFlagInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                tLINKSTATEID * pLinkStateId,
                                                tAreaId * pNewAreaId,
                                                tAreaId * pOldAreaId));
PRIVATE INT4        RtcIsUnresolvedVirtNextHopInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry));
PRIVATE VOID        RtcProcessRtEntryChangeInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry, tRtEntry * pOldRtEntry));
PRIVATE VOID        RtcProcessNextHopChangesInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pCurrRtEntry, tPath * pRtPath,
        tRtEntry * pOldRtEntry, tPath * pOldRtPath));
PRIVATE VOID        RtcProcessVirtualLinkInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry, UINT1 u1Event, UINT1 u1Flag));
PRIVATE UINT1      *RtcGetLinksFromLsa
PROTO ((tLsaInfo * pLsaInfo, INT1 i1Tos,
        UINT1 *pCurrPtr, tLinkNode * pLinkNode));
PRIVATE INT4        RtcCalculateIntraAreaRoute
PROTO ((tOspfRt * pCurrRt, tArea * pArea, INT1 i1Tos));
PRIVATE VOID        RtcUpdateRtIntraAreaRoute
PROTO ((tOspfRt * pCurrRt,
        tCandteNode * pNewVertex, UINT1 u1DestType, tArea * pArea, INT1 i1Tos));
PRIVATE INT4        RtcUpdateCandteLst
PROTO ((tCandteNode * spRoot, tSpf * pSpfTree,
        tCandteNode * pNewVertex,
        tLinkNode * pLinkNode, tArea * pArea, INT1 i1Tos));
PRIVATE VOID        RtcAddStubLinkToVertex
PROTO ((tCandteNode * pNewVertex,
        tLinkNode * pLinkNode, tArea * pArea, INT1 i1Tos));
PRIVATE VOID        RtcProcessStubLinks
PROTO ((tOspfRt * pCurrRt, tArea * pArea, INT1 i1Tos));
PRIVATE tCandteNode *RtcGetCandteNode
PROTO ((tIPADDR * pVertexId, UINT1 u1VertType));
PRIVATE VOID        RtcSetStubNextHops
PROTO ((tPath * pPath, tCandteNode * pParent, tIPADDR * pDest, tArea * pArea));
PRIVATE VOID RtcSetNextHops PROTO ((tCandteNode * spRoot, tCandteNode * pParent,
                                    tCandteNode * pVertex, INT1 i1Tos));
PRIVATE VOID        RtcUpdateNextHopInCxt
PROTO ((tArea * pArea, tRouteNextHop * pRtNextHop, tIPADDR dest));
PRIVATE tInterface *RtcFindNextHopIf
PROTO ((tVertexId * pVertexId, UINT1 u1Type, tArea * pArea));
PRIVATE VOID        RtcCalAllInterAreaRtsInCxt
PROTO ((tOspfCxt * pOspfCxt, tOspfRt * pCurrRt, INT1 i1Tos));
PRIVATE tRtEntry   *RtcCalculateInterAreaRoute
PROTO ((tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, INT1 i1Tos, tArea * pArea));
PRIVATE VOID        RtcExmnTrnstAreasSmmryLinks
PROTO ((tOspfRt * pCurrRt, tArea * pArea, INT1 i1Tos));
PRIVATE tPath      *RtcAssociateNewPath
PROTO ((tPath * pAreaBdrPath, UINT4 u4Cost, INT1 i1Tos, tLsaInfo * pLsaInfo));

PRIVATE tArea      *RtcGetNextHopArea
PROTO ((tInterface * pInterface, UINT1 u1Status));
PRIVATE VOID        RtcCalculateExtRoute
PROTO ((tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, INT1 i1Tos));
PRIVATE tLsaInfo   *RtcSearchDatabase
PROTO ((UINT1 u1LsaType, tLINKSTATEID * pLinkStateId, tArea * pArea));
PRIVATE tRtEntry   *RtcCreateRtEntry
PROTO ((UINT1 u1DestType, tIPADDR * pDestId, tIPADDRMASK * pIpAddrMask,
        tOPTIONS options));
PRIVATE tPath      *RtcCreatePath
PROTO ((tAreaId * pAreaId, UINT1 u1PathType, UINT4 u4Cost, UINT4 u4Type2Cost));
PRIVATE UINT4       RtcCandteHashFunc
PROTO ((tIPADDR * pVertexId, UINT1 u1VertType));

PRIVATE UINT1       RtcSearchLsaLinks
PROTO ((tLsaInfo * pLsaInfo, tVertexId * pVertexId, INT1 i1Tos));
PRIVATE VOID        RtcChkAndCalAllExtRoutes
PROTO ((tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, tIPADDRMASK * pDestMask));
PRIVATE VOID        RtcPrfrmReqdActn
PROTO ((tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, tArea * pArea,
        tIPADDRMASK * pDestMask, UINT1 u1Action));
PRIVATE VOID RtcCopyAddrRngActvFlgInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID RtcResetAddrRngActvFlgInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID        RtcSetAddrRngActvFlgInCxt
PROTO ((tOspfCxt * pOspfCxt, tIPADDR * pIpAddr, tAreaId * pAreaId));
PRIVATE tCandteNode *RtcSearchCandteLstInCxt
PROTO ((tOspfCxt * pOspfCxt, tVertexId * pVertexId, UINT1 u1VertType));
PRIVATE tCandteNode *RtcSearchSpfTree
PROTO ((tSpf * pSpfTree, tVertexId * pVertexId, UINT1 u1VertType));
PRIVATE tCandteNode *RtcGetNearestCandteNodeInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID RtcAddRtEntry PROTO ((tOspfRt * pCurrRt, tRtEntry * pRtEntry));
PRIVATE VOID RtcAddPath PROTO ((tRtEntry * pRtEntry,
                                tPath * pPath, INT1 i1Tos, UINT1 u1Lock));
PRIVATE VOID        RtcReplacePathInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry,
        tPath * pPath, INT1 i1Tos, UINT1 u1Lock));
PRIVATE VOID        RtcDeleteRtEntry
PROTO ((tOspfRt * pCurrRt, tRtEntry * pRtEntry));
PRIVATE VOID        RtcProcessUnreachableRouteInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry));

PRIVATE tPath      *RtcFindPreferredASBRPath PROTO ((tOspfRt * pCurrRt,
                                                     tIPADDR * pDest,
                                                     INT1 i1Tos));
PRIVATE VOID RtcResetVlReachabilityInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID RtcCheckVlReachabilityInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID RtcSetVlReachability PROTO ((tArea * pArea, tRtEntry * pRtEntry));
PRIVATE VOID RtcAddToSortRtLst PROTO ((tOspfRt * pCurrRt, tRtEntry * pRtEntry));
PRIVATE INT1        RtcIsSameNextHop (tPath *, tPath *, UINT1);
PRIVATE VOID RtcCalculateASExtRoutesInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                  tOspfRt * pCurrRt,
                                                  INT1 i1Tos));
PRIVATE VOID        RtcCalculateAllNssaExtRoutesInCxt
PROTO ((tOspfCxt * pOspfCxt, tOspfRt * pCurrRt, INT1 i1Tos));
PRIVATE VOID        RtcCalculateNssaRoute
PROTO ((tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, INT1 i1Tos));

PRIVATE VOID RtcClearCandteLstInCxt PROTO ((tOspfCxt * pOspfCxt));

PUBLIC VOID RtcFlushOutSummaryFromAllAreasInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                        UINT1 u1LsaType,
                                                        tLINKSTATEID *
                                                        pLinkStateId,
                                                        UINT4 u4IpAddrMask));

PRIVATE INT1        RtcIsLsaValidForExtRtCalc
PROTO ((tLsaInfo * pLsaInfo, INT1 i1Tos));
PRIVATE VOID RtcAddNxtHopForExtRt PROTO ((tLsaInfo * pLsaInfo,
                                          tPath * pDestNewPath,
                                          tPath * pFwdAddrPath,
                                          tExtLsaLink * pExtLsaLink));

PRIVATE tPath      *RtcNewPathForExtRt PROTO ((tLsaInfo * pLsaInfo,
                                               UINT1 u1NewPath,
                                               tPath * pFwdAddrPath,
                                               tPath * pDestRtPath,
                                               tExtLsaLink * pExtLsaLink));
PRIVATE UINT1 RtcRfc1583DisExtRt PROTO ((tPath * pDestRtPath,
                                         tPath * pDestFwdPath,
                                         tPath * pFwdAddrPath,
                                         tExtLsaLink * pExtLsaLink));
PRIVATE VOID        RtcSetAggFlagInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry));
PRIVATE VOID        RtcIncUpdateExtRouteForNetworkInCxt
PROTO ((tOspfCxt * pOspfCxt, tOspfRt * pCurrRt, tOsDbNode * pOsDbNode));

PRIVATE VOID        RtcAddCandNode
PROTO ((tOspfCxt * pOspfCxt, tCandteNode * pNewVertex));
PRIVATE VOID        RtcDelCandNode
PROTO ((tOspfCxt * pOspfCxt, tCandteNode * pNewVertex));
PRIVATE VOID        RtcCandNodeCostChange
PROTO ((tOspfCxt * pOspfCxt, tCandteNode * pNewVertex, UINT4 u4Cost));
PRIVATE UINT4       RtcGenerateCandteHashKey
PROTO ((tOspfCxt * pOspfCxt, UINT4 u4Cost));
PRIVATE VOID RtcMarkAllRoutes PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID        RtcDeleteOldRoutePathInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry));
PRIVATE UINT1 RtcCheckAllTosFlags PROTO ((tRtEntry * pRtEntry));
PRIVATE VOID        RtcProcessRtEntryAndPathChanges
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry));
PRIVATE VOID RtcInitialiseRtEntry PROTO ((tRtEntry * pRtEntry,
                                          UINT1 u1DestType, tIPADDR * pDestId,
                                          tIPADDRMASK * pIpAddrMask,
                                          tOPTIONS options));
PRIVATE VOID        RtcProcessOldNewRtEntryChanges
PROTO ((tOspfRt * pOspfRt, tRtEntry * pOldRtEntry, tRtEntry * pNewRtEntry,
        INT1 i1Tos, tArea * pArea));
PRIVATE tRtEntry   *RtcFindOldAbrAsbrRt
PROTO ((tOspfRt * pCurrRt, tIPADDR * pDest, UINT1 u1DestType));

PRIVATE VOID RtcResetAllSpfNodes PROTO ((tArea * pArea));
PRIVATE VOID RtcDeleteUnusedSpfNodes PROTO ((tArea * pArea));

PRIVATE VOID
     
     
     
     RtcCheckAndReGenerateLsaInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry, INT1 i1Tos));
PRIVATE VOID        RtcDeleteOldAndUnusedPath
PROTO ((tRtEntry * pRtEntry, UINT1 *pu1DeleteRtEntry));
PRIVATE VOID        RtcProcessOldNewPathInCxt
PROTO ((tOspfCxt * pOspfCxt, tRtEntry * pRtEntry, tPath * pRtPath,
        tPath * pOldRtPath, INT1 *pi1Tos, UINT1 *pu1RtChanged, UINT1 *pu1Flag));

PRIVATE INT4
 
 
 
 RtcCheckAndUpdateLeastIfIndexNextHop (tCandteNode * pVertex,
                                       tIPADDR nbrIpAddr,
                                       tInterface * pInterface);

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcInitRt                                                  */
/*                                                                           */
/* Description  : Initializes the routing table.                             */
/*                                                                           */
/* Input        : pOspfRt       : routing table                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :SUCCESS or FAILURE                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
RtcInitRt (tOspfRt * pOspfRt, UINT1 u1ApplnId, UINT4 u4Type)
{
    UINT4               u4MaxRts = 0;
    tTrieCrtParams      ospfTrieCrtParams;

    u4MaxRts = OSPF_MIN (MAX_OSPF_RT_ENTRIES,
                         FsOSPFSizingParams[MAX_OSPF_RT_ENTRIES_SIZING_ID].
                         u4PreAllocatedUnits);

    pOspfRt->pOspfRoot = NULL;
    ospfTrieCrtParams.u2KeySize = 2 * sizeof (UINT4);
    ospfTrieCrtParams.u4Type = u4Type;
    ospfTrieCrtParams.AppFns = &(OspfTrieLibFuncs);
    ospfTrieCrtParams.u1AppId = u1ApplnId;
    ospfTrieCrtParams.u4NumRadixNodes = u4MaxRts + 1;
    ospfTrieCrtParams.u4NumLeafNodes = u4MaxRts;
    ospfTrieCrtParams.u4NoofRoutes = u4MaxRts;
    pOspfRt->i1OspfId = u1ApplnId;
    ospfTrieCrtParams.bPoolPerInst = OSIX_FALSE;
    ospfTrieCrtParams.bSemPerInst = OSIX_FALSE;
    ospfTrieCrtParams.bValidateType = OSIX_FALSE;
    pOspfRt->pOspfRoot = TrieCreateInstance (&ospfTrieCrtParams);

    if (pOspfRt->pOspfRoot == NULL)
    {
        return OSPF_FAILURE;
    }

    TMO_SLL_Init (&(pOspfRt->routesList));
    TMO_SLL_Init (&(pOspfRt->oldAbrAsbrRtList));
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeInitRt                                                  */
/*                                                                           */
/* Description  : Deletes trie instance the routing table.                             */
/*                                                                           */
/* Input        : pOspfRt       : routing table                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :SUCCESS or FAILURE                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
RtcDeInitRt (VOID *pRtRoot, INT1 i1AppId)
{

    tInputParams        Inparams;
    tDeleteOutParams    DelParams;

    Inparams.pRoot = pRtRoot;
    Inparams.i1AppId = i1AppId;

    if (TrieDelInstance (&Inparams, OspfTrieCbDelete, (VOID *) &DelParams) ==
        TRIE_FAILURE)
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;

}
PUBLIC VOID
OspfTrieCbDelete (VOID *pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCalculateRtInCxt                                        */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.                           */
/*                This procedure calculates the entire routing table. If     */
/*                there is any change in any routing table entry appropriate */
/*                LSAs are originated.                                       */
/*                                                                           */
/* Input        : pOspfCxt    - pointer to ospf context                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcCalculateRtInCxt (tOspfCxt * pOspfCxt)
{

    tArea              *pArea;
    INT1                i1Tos;
    UINT4               u4BdrRtrCnt = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4AsbrRtLocalChange = OSPF_FALSE;
    UINT4               u4AsBdrRtrCnt = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC : RtcCalculateRt \n");

    /* Mark All the OSPF Routes as OSPF_UNUSED */
    RtcMarkAllRoutes (pOspfCxt);

    RtcResetAddrRngActvFlgInCxt (pOspfCxt);

    /* Last SPF Max Cost and Min Cost of the candidate node
       used in hash key generation for candidate Hash Table */
    pOspfCxt->u4MaxCost = pOspfCxt->u4CurrSpfMaxCost;
    pOspfCxt->u4MinCost = pOspfCxt->u4CurrSpfMinCost;
    pOspfCxt->u4CurrSpfMaxCost = 0;
    pOspfCxt->u4CurrSpfMinCost = 0;
    /* Update the next relinquish interval */
    if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        pOspfCxt->u4StaggeringDelta =
            (u4CurrentTime + (pOspfCxt->u4RTStaggeringInterval / 1000));
        /* Set the reliquish context ID to invalid context ID */
        gOsRtr.u4RTstaggeredCtxId = OSPF_INVALID_CXT_ID;
    }
    RtcResetVlReachabilityInCxt (pOspfCxt);

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {

        /* Only routers with tos_capability calculate paths for non-zero tos */
#ifdef TOS_SUPPORT
        if ((i1Tos) && (pOspfCxt->bTosCapability == OSPF_FALSE))
        {
            break;
        }
#endif /* TOS_SUPPORT */

        pOspfCxt->u4AsbrRtChange = OSPF_FALSE;
        /* calculate intra-area routes */

        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            /* If it is Cisco Type or IBM Type ABR we dont
             * need to do intra area route calculation for
             * the area which is not having active interfaces. */
            if ((pOspfCxt->u4ABRType != STANDARD_ABR)
                && !(IS_ACTIVELY_ATTACHED_AREA (pArea)))
            {
                continue;
            }
            if (i1Tos == TOS_0)
            {
                pArea->u4AbrStatFlg = OSPF_FALSE;
                u4BdrRtrCnt = pArea->u4AreaBdrRtrCount;
                u4AsBdrRtrCnt = pArea->u4AsBdrRtrCount;
            }
            COUNTER_OP (pArea->u2SpfRuns, 1);

            if (RtcCalculateIntraAreaRoute (pOspfCxt->pOspfRt, pArea,
                                            i1Tos) == OSPF_FAILURE)
            {
                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pOspfCxt->u4OspfCxtId, "RT Calc Err\n");
                /* Cleanup */
                pOspfCxt->u4StaggeringDelta = OSPF_INVALID_STAGGERING_DELTA;
                gOsRtr.u4RTstaggeredCtxId = OSPF_INVALID_CXT_ID;
                return;
            }
            if (u4BdrRtrCnt != pArea->u4AreaBdrRtrCount)
            {
                pArea->u4AbrStatFlg = OSPF_TRUE;
            }
            if (u4AsBdrRtrCnt != pArea->u4AsBdrRtrCount)
            {
                u4AsbrRtLocalChange = OSPF_TRUE;
            }

        }                        /* End of TMO_SLL Scan of all the areas */
        if (u4AsbrRtLocalChange == OSPF_TRUE)
        {
            /*This Flag will be set to TRUE , when the ASBR route calculation happens.
             * This will be used further to trigger the NSSA Translator election. */
            pOspfCxt->u4AsbrRtChange = OSPF_TRUE;
        }

        /* calculate inter-area routes */
        RtcCalAllInterAreaRtsInCxt (pOspfCxt, pOspfCxt->pOspfRt, i1Tos);

        if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {

            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {

                if (IS_AREA_TRANSIT_CAPABLE (pArea))
                {
                    RtcExmnTrnstAreasSmmryLinks (pOspfCxt->pOspfRt,
                                                 pArea, i1Tos);
                }
            }
        }

        /* calculate AS external routes */
        RtcCalculateASExtRoutesInCxt (pOspfCxt, pOspfCxt->pOspfRt, i1Tos);

    }

    RtcCheckVlReachabilityInCxt (pOspfCxt);
    RtcCopyAddrRngActvFlgInCxt (pOspfCxt);

    RtcProcessRtChangesInCxt (pOspfCxt);

    RtcGenerateAggLsaInCxt (pOspfCxt);

    OlsChkAndGenerateSumToNewAreasInCxt (pOspfCxt);

    NssaType7To5TranslatorInCxt (pOspfCxt);
    RtcCheckTranslationInCxt (pOspfCxt);

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* Translator election and 
         * external LSA translation cannot be done in GR mode */
        return;
    }

    if (pOspfCxt->u4NssaAreaCount > RAG_NO_CHNG)
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if ((pArea->u4AreaType == NSSA_AREA) &&
                ((pArea->u4AbrStatFlg == OSPF_TRUE)
                 || (pOspfCxt->u4AsbrRtChange == OSPF_TRUE)))

            {
                NssaFsmFindTranslator (pArea);
                pArea->u4AbrStatFlg = OSPF_FALSE;
                pOspfCxt->u4AsbrRtChange = OSPF_FALSE;

            }
        }
    }

    /* Cleanup */
    pOspfCxt->u4StaggeringDelta = OSPF_INVALID_STAGGERING_DELTA;
    gOsRtr.u4RTstaggeredCtxId = OSPF_INVALID_CXT_ID;

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "RT Calc Over\n");
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT : RtcCalculateRt \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessRtChangesInCxt                                   */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.7.                         */
/*                This procedure scans the routing table and generates events*/
/*                corresponding to the routing table changes.                */
/*                                                                           */
/* Input        : pOspfCxt  - pointer to ospf context                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcProcessRtChangesInCxt (tOspfCxt * pOspfCxt)
{
    tRtEntry           *pRtEntry = NULL;
    tRtEntry           *pTempRtEntry = NULL;
    tRtEntry           *pOldRtEnt = NULL;
    tRtEntry           *pOldDestRtEntry = NULL;
    tRtEntry           *pTempDestRtEntry = NULL;
    tIPADDRMASK         DestMask;
    UINT4               u4CurrentTime = 0;

    OSPF_TRC (OSPF_FN_ENTRY
              || OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "FUNC:RtcProcessRtChanges \n");

    OsixGetSysTime ((tOsixSysTime *) & (pOspfCxt->u4LsaGenTime));

    /* scan the new routing table */
    OSPF_DYNM_SLL_SCAN (&(pOspfCxt->pOspfRt->routesList), pRtEntry,
                        pTempRtEntry, tRtEntry *)
    {

        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                && (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
            {
                gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                OSPFRtcRelinquish (pOspfCxt);
            }
        }

        OSPF_CRU_BMC_DWTOPDU (DestMask, pRtEntry->u4IpAddrMask);

        if ((IS_DEST_ABR (pRtEntry)) || (IS_DEST_ASBR (pRtEntry)))
        {
            /* Process the ABR/ASBR route Changes */
            if (RtcIsUnresolvedVirtNextHopInCxt (pOspfCxt, pRtEntry) ==
                OSPF_TRUE)
            {
                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pOspfCxt->u4OspfCxtId, "RT Entry To Be Deleted\n");

                RtcDeleteRtEntry (pOspfCxt->pOspfRt, pRtEntry);

                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pOspfCxt->u4OspfCxtId, "RT Entry Deleted\n");
                RtcFreeRtEntry (pRtEntry);
            }
            else
            {

                /* find corresponding entry in the old abr/asbr routes list */
                pOldRtEnt = RtcFindOldAbrAsbrRt (pOspfCxt->pOspfRt,
                                                 &(pRtEntry->destId),
                                                 pRtEntry->u1DestType);
                RtcProcessRtEntryChangeInCxt (pOspfCxt, pRtEntry, pOldRtEnt);
                if (pOldRtEnt != NULL)
                {
                    TMO_SLL_Delete (&(pOspfCxt->pOspfRt->oldAbrAsbrRtList),
                                    &(pOldRtEnt->nextRtEntryNode));
                    RtcFreeRtEntry (pOldRtEnt);
                }

            }
            continue;

        }

        if (RtcCheckAllTosFlags (pRtEntry) == OSPF_ROUTE_UNUSED)
        {
            RtcProcessUnreachableRouteInCxt (pOspfCxt, pRtEntry);
            RtcDeleteOldRoutePathInCxt (pOspfCxt, pRtEntry);
            RtcDeleteRtEntry (pOspfCxt->pOspfRt, pRtEntry);

            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pOspfCxt->u4OspfCxtId, "RT Entry Deleted\n");
            RtcFreeRtEntry (pRtEntry);
            continue;

        }
        else
        {
            if (RtcIsUnresolvedVirtNextHopInCxt (pOspfCxt, pRtEntry)
                == OSPF_TRUE)
            {
                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pOspfCxt->u4OspfCxtId, "RT Entry To Be Deleted\n");

                RtcDeleteRtEntry (pOspfCxt->pOspfRt, pRtEntry);

                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pOspfCxt->u4OspfCxtId, "RT Entry Deleted\n");
                RtcFreeRtEntry (pRtEntry);
                continue;
            }
            else
            {
                RtcProcessRtEntryAndPathChanges (pOspfCxt, pRtEntry);
            }
        }
    }

    OSPF_DYNM_SLL_SCAN (&(pOspfCxt->pOspfRt->oldAbrAsbrRtList), pOldDestRtEntry,
                        pTempDestRtEntry, tRtEntry *)
    {
        /*
         * find associated summary lsa and delete it if it has not been
         * generated newly
         */

        if (IS_DEST_ASBR (pOldDestRtEntry))
        {
            LsuStartorStopSplAgingTmrInCxt (pOspfCxt, pOldDestRtEntry, START);
        }
        RtcProcessUnreachableRouteInCxt (pOspfCxt, pOldDestRtEntry);
        TMO_SLL_Delete (&(pOspfCxt->pOspfRt->oldAbrAsbrRtList),
                        &(pOldDestRtEntry->nextRtEntryNode));
        RtcFreeRtEntry (pOldDestRtEntry);
    }
    TMO_SLL_Init (&(pOspfCxt->pOspfRt->oldAbrAsbrRtList));

    OSPF_TRC (OSPF_FN_EXIT || OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "EXIT:RtcProcessRtChanges \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessRtEntryChangeInCxt                               */
/*                                                                           */
/* Description  : Updates the changed routing table entry with the specified */
/*                entry and generates events accordingly.                    */
/*                                                                           */
/* Input        : pOspfCxt      : pointer to ospf context                    */
/*                pRtEntry       : new routing table entry                   */
/*                pOldRtEntry   : old routing table entry                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcProcessRtEntryChangeInCxt (tOspfCxt * pOspfCxt,
                              tRtEntry * pRtEntry, tRtEntry * pOldRtEntry)
{
    INT1                i1Tos;
    tPath              *pRtPath;
    tPath              *pOldRtPath;
    tArea              *pArea;
    tRtEntry           *pCurrRtEntry = NULL;
    UINT1               u1RtChanged = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RtcProcessRtEntryChanges \n");
    if (pRtEntry != NULL)
    {
        RtcSetAggFlagInCxt (pOspfCxt, pRtEntry);
    }
    if ((pRtEntry != NULL) && (pOldRtEntry == NULL))
    {
        if (IS_DEST_ASBR (pRtEntry))
        {
            LsuStartorStopSplAgingTmrInCxt (pOspfCxt, pRtEntry, STOP);
        }

    }

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {

        if (pRtEntry != NULL)
        {
            pRtPath = GET_TOS_PATH (pRtEntry, i1Tos);
        }
        else
        {
            pRtPath = NULL;
        }
        if (pOldRtEntry != NULL)
        {
            pOldRtPath = GET_TOS_PATH (pOldRtEntry, i1Tos);
        }
        else
        {
            pOldRtPath = NULL;
        }

        if ((pRtPath != NULL) && (pOldRtPath != NULL))
        {

            /* both paths exist */
            pCurrRtEntry = pRtEntry;

            if (pRtPath->u1PathType != pOldRtPath->u1PathType)
            {
                if (IS_DEST_NETWORK (pCurrRtEntry))
                {
                    RtmTxRtChngNtf (pOspfCxt, pOldRtEntry, RT_ROWSTATUS_MASK);
                    RtmTxRtUpdate (pOspfCxt, pCurrRtEntry);

                }
                if ((pOldRtPath->u1PathType == INTRA_AREA) &&
                    (pRtPath->u1PathType == INTER_AREA))
                {
                    /* This condition can be true only in the 
                     * case of Cisco or IBM ABR */
                    if ((UtilIpAddrComp (pOldRtPath->areaId,
                                         gNullIpAddr) == OSPF_EQUAL) &&
                        (UtilIpAddrComp (pRtPath->areaId, gNullIpAddr) !=
                         OSPF_EQUAL))
                    {
                        RtcFlushOutSummaryFromAllAreasInCxt (pOspfCxt,
                                                             NETWORK_SUM_LSA,
                                                             &(pRtEntry->
                                                               destId),
                                                             pRtEntry->
                                                             u4IpAddrMask);
                        continue;
                    }
                    else
                    {
                        if (IS_DEST_NETWORK (pRtEntry))
                        {
                            RtcFlushOutSummaryLsa (pOspfCxt->pBackbone,
                                                   NETWORK_SUM_LSA,
                                                   &(pRtEntry->destId),
                                                   pRtEntry->u4IpAddrMask);
                        }
                        else if (IS_DEST_ASBR (pRtEntry))
                        {
                            RtcFlushOutSummaryLsa (pOspfCxt->pBackbone,
                                                   ASBR_SUM_LSA,
                                                   &(pRtEntry->destId),
                                                   pRtEntry->u4IpAddrMask);
                        }
                    }

                    pArea = RtcGetNextHopArea (pRtPath->aNextHops[0].pInterface,
                                               pRtPath->aNextHops[0].u1Status);

                    if ((pArea != NULL) && (IS_AREA_TRANSIT_CAPABLE (pArea)))
                    {
                        RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                               &(pRtEntry->destId),
                                               pRtEntry->u4IpAddrMask);
                    }
                }
                u1RtChanged = OSPF_TRUE;
                break;
            }

            if ((pOspfCxt->u4ABRType != STANDARD_ABR) &&
                (pRtPath->u1PathType == INTER_AREA) &&
                (pOldRtPath->u1PathType == INTER_AREA) &&
                (UtilIpAddrComp (pOldRtPath->areaId, gNullIpAddr)
                 != OSPF_EQUAL) &&
                (UtilIpAddrComp (pRtPath->areaId, gNullIpAddr) == OSPF_EQUAL))
            {
                u1RtChanged = OSPF_TRUE;
                break;
            }

            if (pRtPath->u4Cost != pOldRtPath->u4Cost)
            {
                RtcProcessVirtualLinkInCxt (pOspfCxt,
                                            pRtEntry, VIF_COST_CHANGED,
                                            OSPF_NEW_PATH);
                u1RtChanged = OSPF_TRUE;
                if (IS_DEST_NETWORK (pCurrRtEntry))
                {
                    RtcProcessNextHopChangesInCxt (pOspfCxt, pCurrRtEntry,
                                                   pRtPath,
                                                   pOldRtEntry, pOldRtPath);
                }

                break;
            }
            if ((pRtPath->u1PathType == TYPE_2_EXT) &&
                (pRtPath->u4Type2Cost != pOldRtPath->u4Type2Cost))

            {
                RtcProcessNextHopChangesInCxt (pOspfCxt, pCurrRtEntry,
                                               pRtPath,
                                               pOldRtEntry, pOldRtPath);
                break;
            }
            if (IS_DEST_NETWORK (pCurrRtEntry))
            {

                RtcProcessNextHopChangesInCxt (pOspfCxt, pCurrRtEntry,
                                               pRtPath, pOldRtEntry,
                                               pOldRtPath);

                if (UtilIpAddrComp (pRtPath->areaId, pOldRtPath->areaId)
                    != OSPF_EQUAL)
                {
                    RtcCheckAddrRangeFlagInCxt (pOspfCxt,
                                                &(pCurrRtEntry->destId),
                                                &(pRtPath->areaId),
                                                &(pOldRtPath->areaId));
                    u1RtChanged = OSPF_TRUE;
                    break;
                }
            }

        }
        else if ((pRtPath != NULL) && (pOldRtPath == NULL))
        {

            u1RtChanged = OSPF_TRUE;

            /* newly reachable */
            pCurrRtEntry = pRtEntry;

            if (i1Tos == TOS_0)
            {
                RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry, IFE_UP,
                                            OSPF_NEW_PATH);
            }
            else
            {
                RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry,
                                            VIF_COST_CHANGED, OSPF_NEW_PATH);
            }

            if (IS_DEST_NETWORK (pCurrRtEntry))
            {
                RtmTxRtUpdate (pOspfCxt, pCurrRtEntry);
            }
            break;
        }
        else if ((pRtPath == NULL) && (pOldRtPath != NULL))
        {

            /* no longer reachable */
            pCurrRtEntry = pOldRtEntry;
#ifdef TOS_SUPPORT
            if (i1Tos == TOS_0)
            {
                RtcProcessVirtualLinkInCxt (pOspfCxt, pOldRtEntry, IFE_DOWN,
                                            pOldRtPath->u1Flag);
            }
            else
            {
                if (pOldRtEntry->au1Flag[TOS_0] == OSPF_ROUTE_MODIFIED)
                {
                    RtcProcessVirtualLinkInCxt (pOspfCxt, pOldRtEntry,
                                                VIF_COST_CHANGED,
                                                OSPF_OLD_PATH);
                }
                else
                {
                    RtcProcessVirtualLinkInCxt (pOspfCxt, pOldRtEntry,
                                                VIF_COST_CHANGED,
                                                OSPF_NEW_PATH);
                }
            }
#else
            RtcProcessVirtualLinkInCxt (pOspfCxt, pOldRtEntry, IFE_DOWN,
                                        pOldRtPath->u1Flag);
#endif

            if (pRtEntry == NULL)
            {
                RtcProcessUnreachableRouteInCxt (pOspfCxt, pCurrRtEntry);
            }
            else
            {
                pCurrRtEntry = pRtEntry;
                i1Tos = 0;
                u1RtChanged = OSPF_TRUE;
            }

            break;
        }
        else
        {
            /* both paths absent */
        }
    }
    if ((u1RtChanged == OSPF_TRUE) && (i1Tos < OSPF_MAX_METRIC))
    {
        RtcCheckAndReGenerateLsaInCxt (pOspfCxt, pCurrRtEntry, i1Tos);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: RtcProcessRtEntryChange\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessRtEntryAndPathChanges                            */
/*                                                                           */
/* Description  : Updates the changed routing table entry with the specified */
/*                entry and generates events accordingly.                    */
/*                                                                           */
/* Input        : pOspfCxt   : pointer to  Ospf context                      */
/*                pRtEntry   : routing table entry                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcProcessRtEntryAndPathChanges (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry)
{
    INT1                i1Tos;
    tPath              *pRtPath;
    tPath              *pOldRtPath;
    UINT1               u1RtChanged = OSPF_FALSE;
    UINT1               u1DeleteRtEntry = OSPF_TRUE;
    UINT1               u1BreakFlag = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RtcProcessRtEntryChanges \n");

    if (pRtEntry == NULL)
        return;

    if ((pRtEntry->au1Flag[TOS_0] != OSPF_ROUTE_UNUSED) &&
        (TMO_SLL_Count (&(pRtEntry->aTosPath[TOS_0])) != 0))
    {
        RtcSetAggFlagInCxt (pOspfCxt, pRtEntry);
    }
    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
        if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_USED)
        {
            continue;
        }

        if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED)
        {
            /* Unused Routes will not have any new path, 
               the existing path inforamtion should be
               treated as the old path */
            pRtPath = NULL;
            pOldRtPath = GET_TOS_PATH (pRtEntry, i1Tos);
        }
        else
        {
            /* Route is either Added or Modified.
               Get the New and old path for the route */
            pRtPath = RtcGetRtTosPath (pRtEntry, i1Tos, OSPF_NEW_PATH);
            pOldRtPath = RtcGetRtTosPath (pRtEntry, i1Tos, OSPF_OLD_PATH);
        }
        RtcProcessOldNewPathInCxt (pOspfCxt, pRtEntry, pRtPath, pOldRtPath,
                                   &i1Tos, &u1RtChanged, &u1BreakFlag);

        if (u1BreakFlag == OSPF_TRUE)
        {
            break;
        }
    }

    RtcDeleteOldAndUnusedPath (pRtEntry, &u1DeleteRtEntry);

    if ((u1RtChanged == OSPF_TRUE) && (u1DeleteRtEntry == OSPF_FALSE))
    {
        RtcCheckAndReGenerateLsaInCxt (pOspfCxt, pRtEntry, i1Tos);
    }
    if (u1DeleteRtEntry == OSPF_TRUE)
    {
        RtcDeleteRtEntry (pOspfCxt->pOspfRt, pRtEntry);
        RtcFreeRtEntry (pRtEntry);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: RtcProcessRtEntryChange\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessNextHopChangesInCxt                              */
/*                                                                           */
/* Description  : Updates the changed routing table entry with the specified */
/*                entry and generates events accordingly.                    */
/*                                                                           */
/* Input        : pOspfCxt      : Pointer to Ospf Context                    */
/*              : pRtPath       : Path corresponding to new entry            */
/*                pOldRtPath   : Path corresponding to old entry             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcProcessNextHopChangesInCxt (tOspfCxt * pOspfCxt, tRtEntry * pCurrRtEntry,
                               tPath * pRtPath, tRtEntry * pOldRtEntry,
                               tPath * pOldRtPath)
{

    UINT1               u1OldHopIndex;
    UINT1               u1NewHopIndex;
    tTRUTHVALUE         bNextHopFound = OSPF_FALSE;

    for (u1OldHopIndex = 0;
         u1OldHopIndex < OSPF_MIN ((pOldRtPath->u1HopCount), (MAX_NEXT_HOPS));
         u1OldHopIndex++)
    {
        for (u1NewHopIndex = 0;
             u1NewHopIndex < OSPF_MIN ((pRtPath->u1HopCount), (MAX_NEXT_HOPS));
             u1NewHopIndex++)
        {
            if ((OSPF_CRU_BMC_DWFROMPDU
                 (pOldRtPath->aNextHops[u1OldHopIndex].nbrIpAddr)) ==
                (OSPF_CRU_BMC_DWFROMPDU
                 (pRtPath->aNextHops[u1NewHopIndex].nbrIpAddr)))
            {
                if ((pOldRtPath->u4Cost != pRtPath->u4Cost) ||
                    ((pRtPath->u1PathType == TYPE_2_EXT) &&
                     (pRtPath->u4Type2Cost != pOldRtPath->u4Type2Cost)))
                {
                    RtmTxRtChngNextHopNtf (pOspfCxt, pOldRtEntry, u1OldHopIndex,
                                           pOldRtPath, RT_ROWSTATUS_MASK);

                    RtmTxRtChngNextHopNtf (pOspfCxt, pCurrRtEntry,
                                           u1NewHopIndex, pRtPath,
                                           RT_IFINDX_MASK | RT_METRIC_MASK);
                }                /* Incase path interface is unnumbered point to point interface */
                else if (((pOldRtPath->aNextHops[u1OldHopIndex].pInterface !=
                           NULL)
                          && (pRtPath->aNextHops[u1NewHopIndex].pInterface !=
                              NULL)
                          && (pOldRtPath->aNextHops[u1OldHopIndex].pInterface->
                              u4AddrlessIf != 0)
                          && (pRtPath->aNextHops[u1NewHopIndex].pInterface->
                              u4AddrlessIf != 0))
                         && (pOldRtPath->aNextHops[u1OldHopIndex].u4IfIndex !=
                             pRtPath->aNextHops[u1NewHopIndex].u4IfIndex))
                {
                    RtmTxRtChngNextHopNtf (pOspfCxt, pOldRtEntry, u1OldHopIndex,
                                           pOldRtPath, RT_ROWSTATUS_MASK);

                    RtmTxRtChngNextHopNtf (pOspfCxt, pCurrRtEntry,
                                           u1NewHopIndex, pRtPath,
                                           RT_IFINDX_MASK | RT_METRIC_MASK);
                }
                bNextHopFound = OSPF_TRUE;
                break;
            }
        }
        if (bNextHopFound == OSPF_FALSE)
        {

            RtmTxRtChngNextHopNtf (pOspfCxt, pOldRtEntry, u1OldHopIndex,
                                   pOldRtPath, RT_ROWSTATUS_MASK);

        }
        bNextHopFound = OSPF_FALSE;
    }
    for (u1NewHopIndex = 0;
         u1NewHopIndex < OSPF_MIN ((pRtPath->u1HopCount), (MAX_NEXT_HOPS));
         u1NewHopIndex++)
    {
        for (u1OldHopIndex = 0;
             u1OldHopIndex < OSPF_MIN ((pOldRtPath->u1HopCount),
                                       (MAX_NEXT_HOPS)); u1OldHopIndex++)
        {
            if ((OSPF_CRU_BMC_DWFROMPDU
                 (pRtPath->aNextHops[u1NewHopIndex].nbrIpAddr)) ==
                (OSPF_CRU_BMC_DWFROMPDU
                 (pOldRtPath->aNextHops[u1OldHopIndex].nbrIpAddr)))
            {
                bNextHopFound = OSPF_TRUE;
                break;
            }
        }
        if (bNextHopFound == OSPF_FALSE)
        {

            RtmTxNextHopUpdate (pOspfCxt, pCurrRtEntry, pRtPath, u1NewHopIndex);
        }
        bNextHopFound = OSPF_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessVirtualLinkInCxt                                 */
/*                                                                           */
/* Description  : This routine updates the state of the virtual link based on*/
/*                the event.                                                 */
/*                                                                           */
/* Input        : pRtEntry       : routing table entry                       */
/*                u1Event         : event                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcProcessVirtualLinkInCxt (tOspfCxt * pOspfCxt,
                            tRtEntry * pRtEntry, UINT1 u1Event, UINT1 u1Flag)
{

    tInterface         *pInterface = (tInterface *) NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tPath              *pRtPath = NULL;
    tNeighbor          *pNbr = NULL;
    tIPADDR             ipAddr;
    UINT4               u4PrevIfIndex = 0;

    MEMSET (ipAddr, 0, sizeof (tIPADDR));

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RtcProcessVirtualLink \n");

    if (pOspfCxt->bAreaBdrRtr == OSPF_FALSE)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Rtr Not ABR\n");

        return;
    }

    if (!(IS_DEST_ABR (pRtEntry)))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Dst Not ABR\n");

        return;
    }

    TMO_SLL_Scan (&(pRtEntry->aTosPath[TOS_0]), pRtPath, tPath *)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Scannig Virt Ifaces Starts\n");
        if (pRtPath->u1Flag != u1Flag)
        {
            continue;
        }
        if (pRtPath->u1PathType != INTRA_AREA)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                      "Path Not Intra-Area\n");

            return;
        }

        TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {

            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            u4PrevIfIndex = pInterface->u4IfIndex;

            VifSetVifValues (pInterface, pRtEntry);

            if (u4PrevIfIndex != pInterface->u4IfIndex)
            {
                pNbrNode = TMO_SLL_First (&pInterface->nbrsInIf);
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                NbrUpdateNbrTable (pNbr, u4PrevIfIndex, pInterface->u4IfIndex);
            }
            OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                       "Transit AreaId %x NbrId %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pInterface->transitAreaId),
                       OSPF_CRU_BMC_DWFROMPDU (pInterface->destRtrId));

            if ((UtilIpAddrComp (pRtEntry->destId,
                                 pInterface->destRtrId) != OSPF_EQUAL) ||
                (UtilIpAddrComp (pRtPath->areaId,
                                 pInterface->transitAreaId) != OSPF_EQUAL))
            {

                continue;
            }

            pNbrNode = TMO_SLL_First (&pInterface->nbrsInIf);

            if (pNbrNode == NULL)
            {
                OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                           pOspfCxt->u4OspfCxtId,
                           "No Nbr on the Virt Iface %x, %x \n",
                           OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                           pInterface->u4AddrlessIf);
                continue;
            }

            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

            if ((pInterface->u1IsmState != IFS_DOWN) && (u1Event == IFE_DOWN))
            {

                OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                           pOspfCxt->u4OspfCxtId,
                           "Virt If To Be Brought DN Nbr %x\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

                GENERATE_IF_EVENT (pInterface, IFE_DOWN);

            }
            else if ((pInterface->u1IsmState == IFS_DOWN) &&
                     (u1Event == IFE_UP))
            {

                if ((RtcBldSpfTreeTransAreaInCxt
                     (pOspfCxt,
                      &(pInterface->transitAreaId),
                      &(pInterface->destRtrId), &ipAddr)) == OSPF_TRUE)
                {
                    if (pInterface->u4AddrlessIf != 0)
                    {
                        IP_ADDR_COPY (pNbr->nbrIpAddr, pInterface->destRtrId);
                    }
                    else
                    {
                        IP_ADDR_COPY (pNbr->nbrIpAddr, ipAddr);
                    }
                    pInterface->operStatus = OSPF_ENABLED;
                    OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                               pOspfCxt->u4OspfCxtId,
                               "Virt If To Be Brought UP Nbr %x\n",
                               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

                    OspfIfUp (pInterface);
                }
            }
            else if (u1Event == VIF_COST_CHANGED)
            {
                if (pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
                {
                    OlsSignalLsaRegenInCxt (pOspfCxt, SIG_VIRT_LINK_COST_CHANGE,
                                            (UINT1 *) pInterface);
                }
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: RtcProcessVirtualLink \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetLinksFromLsa                                         */
/*                                                                           */
/* Description  : This procedure scans the specified LSA and returns a linked*/
/*                list of links advertised in the LSA.                       */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                i1Tos              : type of service                       */
/*                pLinkNode         : link nodestructure used to store       */
/*                                      links                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pCurrPtr, if links found                                   */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT1      *
RtcGetLinksFromLsa (tLsaInfo * pLsaInfo,
                    INT1 i1Tos, UINT1 *pCurrPtr, tLinkNode * pLinkNode)
{

    UINT1               i1TosCount;
    UINT1               u1LsaTos;
    UINT1               u1LinkFound = OSPF_FALSE;

    if (pCurrPtr >= (pLsaInfo->pLsa + pLsaInfo->u2LsaLen))
    {

        return NULL;
    }

    if (pLsaInfo->lsaId.u1LsaType == ROUTER_LSA)
    {
        while (!(pCurrPtr >= (pLsaInfo->pLsa + pLsaInfo->u2LsaLen)))
        {
            LGETSTR (pCurrPtr, pLinkNode->linkId, MAX_IP_ADDR_LEN);
            LGETSTR (pCurrPtr, pLinkNode->linkData, MAX_IP_ADDR_LEN);
            pLinkNode->u1LinkType = LGET1BYTE (pCurrPtr);
            i1TosCount = LGET1BYTE (pCurrPtr);

            /* TOS-0 metric */
            pLinkNode->u2LinkCost = LGET2BYTE (pCurrPtr);

            u1LinkFound = OSPF_FALSE;

            if (i1Tos == TOS_0)
                u1LinkFound = OSPF_TRUE;

            while (i1TosCount--)
            {

                u1LsaTos = LGET1BYTE (pCurrPtr);
                if (u1LsaTos == ENCODE_TOS (i1Tos))
                {

                    /* skip 1 byte unused */
                    pCurrPtr += 1;

                    pLinkNode->u2LinkCost = LGET2BYTE (pCurrPtr);
                    u1LinkFound = OSPF_TRUE;

                }
                else
                {
                    /* skip 1 byte unused */
                    pCurrPtr += 1;
                    /* skip 2 bytes - cost */
                    pCurrPtr += 2;
                }
            }
            if (u1LinkFound == OSPF_TRUE)
            {
                return pCurrPtr;
            }
        }
        if (u1LinkFound == OSPF_FALSE)
        {
            return NULL;
        }
    }
    else if (pLsaInfo->lsaId.u1LsaType == NETWORK_LSA)
    {

        LGETSTR (pCurrPtr, pLinkNode->linkId, MAX_IP_ADDR_LEN);
        pLinkNode->u2LinkCost = 0;
        pLinkNode->u1LinkType = TRANSIT_LINK;
    }

    return pCurrPtr;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcAddCandNode                                             */
/*                                                                           */
/* Description  : Adds the candte node in the sorted hash table              */
/*                                                                           */
/* Input        : pOspfCxt          : pointer to context                     */
/*                pNewVertex        : Candte node vertex                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : none                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcAddCandNode (tOspfCxt * pOspfCxt, tCandteNode * pNewVertex)
{
    UINT4               u4HashKey;
    tCandteNode        *pCandteNode = NULL;
    tCandteNode        *pCandteNxt = NULL;
    tCandteNode        *pCandtePrv = NULL;

    TMO_HASH_Add_Node (pOspfCxt->pCandteLst,
                       &pNewVertex->candteHashNode,
                       RtcCandteHashFunc (&(pNewVertex->vertexId),
                                          pNewVertex->u1VertType), NULL);

    u4HashKey = RtcGenerateCandteHashKey (pOspfCxt, pNewVertex->u4Cost);

    UTL_HASH_Scan_Bucket (pOspfCxt->pCandteLst2, u4HashKey,
                          pCandteNode, pCandteNxt, tCandteNode *)
    {
        if (pCandteNode->u4Cost > pNewVertex->u4Cost)
            break;

        if ((pCandteNode->u4Cost == pNewVertex->u4Cost) &&
            (pNewVertex->u1VertType == VERT_NETWORK))
        {
            break;
        }
        pCandtePrv = pCandteNode;
    }

    if (pCandtePrv != NULL)
    {
        TMO_HASH_Insert_Bucket (pOspfCxt->pCandteLst2, u4HashKey,
                                &pCandtePrv->candteHashNode2,
                                &pNewVertex->candteHashNode2);
    }
    else
    {
        TMO_HASH_Insert_Bucket (pOspfCxt->pCandteLst2, u4HashKey, NULL,
                                &pNewVertex->candteHashNode2);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDelCandNode                                             */
/*                                                                           */
/* Description  : Deletes the candte node in the sorted hash table           */
/*                                                                           */
/* Input        : pOspfCxt          : pointer to context                     */
/*                pNewVertex        : Candte node vertex                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : none                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcDelCandNode (tOspfCxt * pOspfCxt, tCandteNode * pNewVertex)
{
    TMO_HASH_Delete_Node (pOspfCxt->pCandteLst,
                          &pNewVertex->candteHashNode,
                          RtcCandteHashFunc (&(pNewVertex->vertexId),
                                             pNewVertex->u1VertType));

    TMO_HASH_Delete_Node (pOspfCxt->pCandteLst2,
                          &pNewVertex->candteHashNode2,
                          RtcGenerateCandteHashKey (pOspfCxt,
                                                    pNewVertex->u4Cost));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCandNodeCostChange                                      */
/*                                                                           */
/* Description  : Adds the candte node with new cost in the sorted hash table*/
/*                                                                           */
/* Input        : pOspfCxt          : pointer to context                     */
/*                pNewVertex        : Candte node vertex                     */
/*                u1Cost            : cost                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : none                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcCandNodeCostChange (tOspfCxt * pOspfCxt, tCandteNode * pNewVertex,
                       UINT4 u4Cost)
{
    UINT4               u4HashKey;
    tCandteNode        *pCandteNode = NULL;
    tCandteNode        *pCandteNxt = NULL;
    tCandteNode        *pCandtePrv = NULL;

    TMO_HASH_Delete_Node (pOspfCxt->pCandteLst2,
                          &pNewVertex->candteHashNode2,
                          RtcGenerateCandteHashKey (pOspfCxt,
                                                    pNewVertex->u4Cost));

    pNewVertex->u4Cost = u4Cost;

    u4HashKey = RtcGenerateCandteHashKey (pOspfCxt, pNewVertex->u4Cost);
    UTL_HASH_Scan_Bucket (pOspfCxt->pCandteLst2, u4HashKey,
                          pCandteNode, pCandteNxt, tCandteNode *)
    {
        if (pCandteNode->u4Cost > pNewVertex->u4Cost)
            break;
        if ((pCandteNode->u4Cost == pNewVertex->u4Cost) &&
            (pNewVertex->u1VertType == VERT_NETWORK))
        {
            break;
        }
        pCandtePrv = pCandteNode;
    }

    if (pCandtePrv != NULL)
    {
        TMO_HASH_Insert_Bucket (pOspfCxt->pCandteLst2, u4HashKey,
                                &pCandtePrv->candteHashNode2,
                                &pNewVertex->candteHashNode2);
    }
    else
    {
        TMO_HASH_Insert_Bucket (pOspfCxt->pCandteLst2, u4HashKey,
                                NULL, &pNewVertex->candteHashNode2);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCalculateIntraAreaRoute                                 */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.1.                         */
/*                This procedure calculates the intra-area routes correspon- */
/*                ding to the specified area.                                */
/*                                                                           */
/* Input        : pCurrRt       : routing table                              */
/*                pArea          : AREA whose intra-area routes are to be    */
/*                                  calculated                               */
/*                i1Tos          : type of service                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
RtcCalculateIntraAreaRoute (tOspfRt * pCurrRt, tArea * pArea, INT1 i1Tos)
{

    tCandteNode        *pNewVertex;
    tLinkNode           linkNode;
    UINT1               u1VertType;
    UINT1              *pCurrPtr;
    tInterface         *pInterface = (tInterface *) NULL;
    tTMO_SLL_NODE      *pLstNode;
    tCandteNode        *spRoot;

    tCandteNode        *pNodeInSpf;
    tCandteNode        *pNodeInPrevSpf;
    UINT4               u4Hindex;
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    tIPADDR             ipAddr;
    UINT1               u1VlCostChFlag = OSPF_FALSE;
    UINT4               u4CurrentTime = 0;
    UINT1               u1Action;

    MEMSET (ipAddr, 0, sizeof (tIPADDR));

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RtcCalculateIntraAreaRoute  \n");
    OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "Intra-Area Route Calc Starts Area %x TOS %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId), i1Tos);

    /* Mark the flag in all SPF nodes as OSPF_SPF_UNUSED */
    RtcResetAllSpfNodes (pArea);

    /* Check whther the root node is present in the SPF Tree 
     * If the node is available, mark the flag as OSPF_SPF_MODIFIED 
     * Else create a new node and mark the node as OSPF_SPF_ADDED
     */
    spRoot = RtcSearchSpfTree (pArea->pSpf, &(pArea->pOspfCxt->rtrId),
                               VERT_ROUTER);

    if (spRoot == NULL)
    {
        if ((spRoot =
             RtcGetCandteNode (&(pArea->pOspfCxt->rtrId), VERT_ROUTER)) == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId,
                      " Candidate Node Alloc Failure\n");
            return OSPF_FAILURE;
        }
        else
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId,
                      "New Candidate Node Created\n");
        }

        spRoot->u1SpfFlag = OSPF_SPF_ADDED;

        TMO_HASH_Add_Node (pArea->pSpf, &spRoot->candteHashNode,
                           RtcCandteHashFunc (&(spRoot->vertexId),
                                              VERT_ROUTER), NULL);
    }
    else
    {
        spRoot->u1SpfFlag = OSPF_SPF_MODIFIED;
    }

    if (i1Tos == TOS_0)
    {
        pArea->bPreviousTransitCapability = pArea->bTransitCapability;
        pArea->bTransitCapability = OSPF_FALSE;
        pArea->u4AreaBdrRtrCount = 0;
    }

    pArea->u4AsBdrRtrCount = 0;

    spRoot->u4Cost = 0;

    if ((spRoot->pLsaInfo
         =
         RtcSearchDatabase (ROUTER_LSA, &(pArea->pOspfCxt->rtrId),
                            pArea)) == NULL)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "LSA For Root Not Fnd\n");
        return OSPF_SUCCESS;
    }

    pNewVertex = spRoot;
    do
    {
        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta)
            {
                gOsRtr.u4RTstaggeredCtxId = pArea->pOspfCxt->u4OspfCxtId;
                OSPFRtcRelinquish (pArea->pOspfCxt);
            }
        }

        /* Set the Area to be transit capable if the Vertex's router LSA
         * has teh 'V' bit set, as per Section 16.1 (2) of RFC 2328
         */

        if (!IS_AREA_TRANSIT_CAPABLE (pArea))
        {
            if (pNewVertex->u1VertType == VERT_ROUTER)
            {
                if (IS_V_BIT_SET (pNewVertex->pLsaInfo->pLsa))
                {
                    pArea->bTransitCapability = OSPF_TRUE;
                }
            }
        }

        /* 
         * move to start of links
         * skip mask in network lsa, flags and #links in router lsa
         */

        pCurrPtr = pNewVertex->pLsaInfo->pLsa + LS_HEADER_SIZE + 4;

        OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Links Of Vertex %x Considered\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId));
#ifdef TOS_SUPPORT
        if (((pNewVertex->pLsaInfo->lsaOptions & T_BIT_MASK) !=
             T_BIT_MASK) && (i1Tos != TOS_0))
        {

            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId,
                      "Tos Option is not enabled\n");
        }
        else
        {
#endif /* TOS_SUPPORT */

            while ((pCurrPtr =
                    RtcGetLinksFromLsa (pNewVertex->pLsaInfo,
                                        i1Tos, pCurrPtr, &linkNode)) != NULL)
            {
                /* Check whether we need to relinquish the route calculation
                 * to do other OSPF processings */
                if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than relinquished interval */
                    if (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta)
                    {
                        gOsRtr.u4RTstaggeredCtxId =
                            pArea->pOspfCxt->u4OspfCxtId;
                        OSPFRtcRelinquish (pArea->pOspfCxt);
                    }
                }
                /* 
                 * if the new vertex is a router and the link is a transit link 
                 * then the next vertex should be a network. Otherwise the next 
                 * vertex is a router.
                 */

                if ((pNewVertex->u1VertType == VERT_ROUTER) &&
                    (linkNode.u1LinkType == TRANSIT_LINK))
                {
                    u1VertType = VERT_NETWORK;
                }
                else
                {
                    u1VertType = VERT_ROUTER;
                }
                UNUSED_PARAM (u1VertType);

                /* 
                 * If stub link add to stub_link_lst. This will be considered
                 * in the next phase of routing table calculation.
                 */

                if (linkNode.u1LinkType == STUB_LINK)
                {
                    RtcAddStubLinkToVertex (pNewVertex, &linkNode,
                                            pArea, i1Tos);

                    /* consider next link */
                    continue;
                }

                if ((linkNode.u1LinkType == VIRTUAL_LINK)
                    && !(BACKBONE_ID (pArea->areaId)))
                {
                    OSPF_TRC (OSPF_CRITICAL_TRC,
                              pArea->pOspfCxt->u4OspfCxtId,
                              "!!! Virtual link is configured in non-backbone area\n !!!");
                    continue;
                }

                /* add to candidate list if necessary */
                if (RtcUpdateCandteLst
                    (spRoot, pArea->pSpf, pNewVertex, &linkNode,
                     pArea, i1Tos) == OSPF_FAILURE)
                {
                    continue;
                }

                /* while */
            }
#ifdef TOS_SUPPORT
        }                        /* else - for TOS_SUPPORT */
#endif /* TOS_SUPPORT */
        /* else */

        /* if candidate list is empty this phase of algorithm is complete
         * otherwise remove the vertex which is nearest to root from candidate
         * list and add it to the spf tree.
         */

        if ((pNewVertex =
             RtcGetNearestCandteNodeInCxt (pArea->pOspfCxt)) == NULL)
        {

            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId, "Candidate Lst Empty\n");
            break;
        }

        RtcDelCandNode (pArea->pOspfCxt, pNewVertex);

        pNodeInPrevSpf =
            RtcSearchSpfTree (pArea->pSpf,
                              &(pNewVertex->vertexId), pNewVertex->u1VertType);

        if (pNodeInPrevSpf == NULL)
        {
            pNewVertex->u1SpfFlag = OSPF_SPF_ADDED;
        }
        else
        {
            /* Route changes need to be performed here */

            /* Delete the node from the SPF tree
             * Mark the flag in the candidate node as OSPF_SPF_MODIFIED
             * to indicate that this node is modified
             */

            TMO_HASH_Delete_Node
                (pArea->pSpf, &(pNodeInPrevSpf->candteHashNode),
                 RtcCandteHashFunc (&(pNodeInPrevSpf->vertexId),
                                    pNodeInPrevSpf->u1VertType));
            SPF_FREE (pNodeInPrevSpf);

            pNewVertex->u1SpfFlag = OSPF_SPF_MODIFIED;
        }

        /* Add the candidate node to the SPF Tree */
        TMO_HASH_Add_Node (pArea->pSpf, &pNewVertex->candteHashNode,
                           RtcCandteHashFunc (&(pNewVertex->vertexId),
                                              pNewVertex->u1VertType), NULL);

        OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Vertex %x Added To SPF Tree\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId));

        /* If the router is Cisco Type or IBM Type ABR, then making
         * virtual link up will be considered in the intra area 
         * route calculation through transit area. */
        /* Newly added Router vertex can be other end of virtual 
         * link. */
        if ((pArea->pOspfCxt->u4ABRType != STANDARD_ABR) &&
            (pNewVertex->u1VertType == VERT_ROUTER))
        {
            TMO_SLL_Scan (&(pArea->pOspfCxt->virtIfLst), pLstNode,
                          tTMO_SLL_NODE *)
            {
                pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
                /* If the virtaul link transit area is same as the area for
                 * which route calculation is happening consider the 
                 * virtual Interface for making it up, otherwise consider 
                 * next virtual links. */
                if (UtilIpAddrComp (pInterface->transitAreaId,
                                    pArea->areaId) != OSPF_EQUAL)
                {
                    continue;
                }

                /* If the Router Vertex is other end of the virtual link 
                 * consider the Virtual Interface for making up */
                if (UtilIpAddrComp (pInterface->destRtrId,
                                    pNewVertex->vertexId) == OSPF_EQUAL)
                {
                    if (i1Tos == TOS_0)
                    {
                        if (VifSetVLValues (pInterface, pNewVertex) ==
                            OSPF_FAILURE)
                        {
                            continue;
                        }
                    }
                    if ((pInterface->u1IsmState != IFS_DOWN)
                        && ((i1Tos != TOS_0)
                            || (pInterface->aIfOpCost[i1Tos].u4Value !=
                                pNewVertex->u4Cost)))
                    {
                        u1VlCostChFlag = OSPF_TRUE;
                    }
                    pInterface->aIfOpCost[i1Tos].u4Value = pNewVertex->u4Cost;
                    pInterface->aIfOpCost[i1Tos].rowStatus = ACTIVE;

                    pInterface->VlReachability = OSPF_TRUE;
                }
            }
        }

        /* update the routing table entry corresponding to the new vertex */

        if (pNewVertex->u1VertType == VERT_ROUTER)
        {

            if (IS_AREA_BORDER_RTR_LSA (pNewVertex->pLsaInfo->pLsa))
            {

                /* the newly added vertex is an area border router */

                RtcUpdateRtIntraAreaRoute (pCurrRt, pNewVertex,
                                           DEST_AREA_BORDER, pArea, i1Tos);

            }
            if (IS_AS_BOUNDARY_RTR_LSA (pNewVertex->pLsaInfo->pLsa))
            {

                /* the newly added vertex is an AS boundary router */

                RtcUpdateRtIntraAreaRoute (pCurrRt, pNewVertex,
                                           DEST_AS_BOUNDARY, pArea, i1Tos);

            }
        }
        else
        {

            /* the newly added vertex is an Network */

            RtcUpdateRtIntraAreaRoute (pCurrRt, pNewVertex,
                                       DEST_NETWORK, pArea, i1Tos);
        }

    }
    while (1);

    RtcProcessStubLinks (pCurrRt, pArea, i1Tos);

    TMO_HASH_Scan_Table (pArea->pSpf, u4Hindex)
    {
        TMO_HASH_Scan_Bucket (pArea->pSpf, u4Hindex, pNodeInSpf, tCandteNode *)
        {

            /* Check whether we need to relinquish the route calculation
             * to do other OSPF processings */
            if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than relinquished interval */
                if (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta)
                {
                    gOsRtr.u4RTstaggeredCtxId = pArea->pOspfCxt->u4OspfCxtId;
                    OSPFRtcRelinquish (pArea->pOspfCxt);
                }
            }

            /* Consider only the router vertices */

            if (pNodeInSpf->u1VertType == VERT_NETWORK)
            {
                continue;
            }

            if (pNodeInSpf->u1SpfFlag == OSPF_SPF_ADDED)
            {
                /* If the node is added in the present RT calculation
                 * do nothing
                 */
                continue;
            }
            else if (pNodeInSpf->u1SpfFlag == OSPF_SPF_MODIFIED)
            {
                /* If the node has been modified
                 * Stop the spl_aging timer for all lsas as the router would 
                 * have become newly reachable for some of them 
                 */

                LsuSplAgeTmrAction (&(pArea->rtrLsaLst),
                                    pNodeInSpf->vertexId, STOP);
                LsuSplAgeTmrAction (&(pArea->networkLsaLst),
                                    pNodeInSpf->vertexId, STOP);
                u1Action = STOP;
                RBTreeWalk (pArea->pSummaryLsaRoot,
                            RbWalkHandleLsuSplAgeTmrAction,
                            &(pNodeInSpf->vertexId), &u1Action);
                LsuSplAgeTmrAction (&(pArea->nssaLSALst), pNodeInSpf->vertexId,
                                    STOP);
            }
            else
            {
                /* The node should be deleted. This means that the router
                 * has become unreachable. Start the spl_aging timer for
                 * all lsas
                 */
                LsuSplAgeTmrAction (&(pArea->rtrLsaLst),
                                    pNodeInSpf->vertexId, START);
                LsuSplAgeTmrAction (&(pArea->networkLsaLst),
                                    pNodeInSpf->vertexId, START);
                u1Action = START;
                RBTreeWalk (pArea->pSummaryLsaRoot,
                            RbWalkHandleLsuSplAgeTmrAction,
                            &(pNodeInSpf->vertexId), &u1Action);
                LsuSplAgeTmrAction (&(pArea->nssaLSALst), pNodeInSpf->vertexId,
                                    START);
            }
        }
    }

    if (pArea->pOspfCxt->u4ABRType != STANDARD_ABR)
    {
        TMO_SLL_Scan (&(pArea->pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            /* If the virtaul link transit area is same as the area for
             * which route calculation is happening consider the 
             * virtual Interface for making it up, otherwise consider 
             * next virtual links. */
            if (UtilIpAddrComp (pInterface->transitAreaId,
                                pArea->areaId) != OSPF_EQUAL)
            {
                continue;
            }

            if (pInterface->VlReachability == OSPF_TRUE)
            {
                if ((i1Tos == TOS_0) && (u1VlCostChFlag != OSPF_TRUE))
                {
                    pNbrNode = TMO_SLL_First (&pInterface->nbrsInIf);
                    if (pNbrNode == NULL)
                    {
                        continue;
                    }
                    pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                    if ((RtcBldSpfTreeTransAreaInCxt
                         (pArea->pOspfCxt, &(pInterface->transitAreaId),
                          &(pInterface->destRtrId), &ipAddr)) == OSPF_TRUE)
                    {
                        if (pInterface->u4AddrlessIf != 0)
                        {
                            IP_ADDR_COPY (pNbr->nbrIpAddr,
                                          pInterface->destRtrId);
                        }
                        else
                        {
                            IP_ADDR_COPY (pNbr->nbrIpAddr, ipAddr);
                        }
                    }

                    pInterface->operStatus = OSPF_ENABLED;

                    OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                               pArea->pOspfCxt->u4OspfCxtId,
                               "Virt If To Be Brought UP Nbr %x\n",
                               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
                    /* If the Virtual Interface is already enabled, 
                     * no need to consider for enabling. */
                    if (pInterface->u1IsmState == IFS_DOWN)
                    {
                        OspfIfUp (pInterface);
                    }
                }
                else
                {
                    OlsSignalLsaRegenInCxt (pArea->pOspfCxt,
                                            SIG_VIRT_LINK_COST_CHANGE,
                                            (UINT1 *) pInterface);
                }
            }
        }
    }

    RtcDeleteUnusedSpfNodes (pArea);

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: RtcCalculateIntraAreaRoute  \n");

    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcClearCandteLstInCxt                                     */
/*                                                                           */
/* Description  : Clears the Global Candate List                             */
/*                                                                           */
/* Input        : pOspfCxt - context pointer                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcClearCandteLstInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4HashKey;
    tCandteNode        *pCandteNode;
    tCandteNode        *pCandteNxt;

    /* delete all the nodes in the candidate list */

    TMO_HASH_Scan_Table (pOspfCxt->pCandteLst, u4HashKey)
    {

        TMO_HASH_Scan_Bucket (pOspfCxt->pCandteLst,
                              u4HashKey, pCandteNode, tCandteNode *)
        {

            TMO_HASH_Delete_Node (pOspfCxt->pCandteLst,
                                  &(pCandteNode->candteHashNode),
                                  RtcCandteHashFunc (&
                                                     (pCandteNode->
                                                      vertexId),
                                                     pCandteNode->u1VertType));
        }
    }

    TMO_HASH_Scan_Table (pOspfCxt->pCandteLst2, u4HashKey)
    {
        UTL_HASH_Scan_Bucket (pOspfCxt->pCandteLst2, u4HashKey,
                              pCandteNode, pCandteNxt, tCandteNode *)
        {

            TMO_HASH_Delete_Node (pOspfCxt->pCandteLst2,
                                  &pCandteNode->candteHashNode2,
                                  RtcGenerateCandteHashKey (pOspfCxt,
                                                            pCandteNode->
                                                            u4Cost));
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcClearSpfTree                                            */
/*                                                                           */
/* Description  : Clears the SPF tree.                                       */
/*                                                                           */
/* Input        : pSpfTree   :  the SPF tree                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcClearSpfTree (tSpf * pSpfTree)
{

    UINT4               u4Hindex;
    tCandteNode        *pCandteNode;
    TMO_HASH_Scan_Table (pSpfTree, u4Hindex)
    {

        while ((pCandteNode
                = (tCandteNode *) TMO_HASH_Get_First_Bucket_Node (pSpfTree,
                                                                  u4Hindex))
               != NULL)
        {

            TMO_HASH_Delete_Node (pSpfTree, &pCandteNode->candteHashNode,
                                  u4Hindex);
            CANDTE_FREE (pCandteNode);
        }
    }

    TMO_HASH_Delete_Table (pSpfTree, NULL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcUpdateRtIntraAreaRoute                                  */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.1.                         */
/*                This procedure makes the necessary changes in the routing  */
/*                table when a new vertex from the candidate list is added to*/
/*                the spf tree.                                              */
/*                                                                           */
/* Input        : pCurrRt           : routing table                          */
/*                pNewVertex        : vertex that has been added to the spf  */
/*                                      tree                                 */
/*                u1DestType        : type of router                         */
/*                pArea              : AREA                                  */
/*                i1Tos              : type of service                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcUpdateRtIntraAreaRoute (tOspfRt * pCurrRt,
                           tCandteNode * pNewVertex,
                           UINT1 u1DestType, tArea * pArea, INT1 i1Tos)
{

    tIPADDRMASK         ipAddrMask;
    tRtEntry           *pOldRtEntry = NULL;
    tRtEntry            newRtEntry;
    tRtEntry           *pNewRtEntry = NULL;
    tPath              *pOldDestRtPath = NULL;
    tPath              *pDestNewPath;
    tDestId             destId;
    tOPTIONS            options;
    UINT1               u1Ind;
    UINT1               u1NewRtEntry = 0;
    UINT1               u1NewPath = 0;
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;

    /* 
     *   if u1NewPath is 1 - construct new path to replace old path
     *                   2 - add list of next hops to the old path
     *                   3 - construct new path to add to list of paths 
     */

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RtcUpdateRtIntraAreaRoute  \n");

    OSPF_TRC4 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
               pArea->pOspfCxt->u4OspfCxtId,
               "Updt RT Adding Vertex To SPF Tree. "
               "Vertex %x DstType %s AreaId %x TOS %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId),
               au1DbgRtDestType[u1DestType],
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId), i1Tos);

    pNewRtEntry = &newRtEntry;
    MEMSET (pNewRtEntry, 0, sizeof (tRtEntry));

    if (pNewVertex->u1VertType == VERT_ROUTER)
    {

        options = GET_LSA_OPTIONS (pNewVertex->pLsaInfo->pLsa);
        IP_ADDR_COPY (ipAddrMask, gNullIpAddr);
        IP_ADDR_COPY (destId, pNewVertex->vertexId);

    }
    else
    {
        options = 0;
        OS_MEM_CPY (ipAddrMask,
                    (UINT1 *) (pNewVertex->pLsaInfo->pLsa +
                               LS_HEADER_SIZE), MAX_IP_ADDR_LEN);
        UtilIpAddrMaskCopy (destId, pNewVertex->vertexId, ipAddrMask);
    }
    pOldRtEntry = RtcFindRtEntry (pCurrRt, &destId, &ipAddrMask, u1DestType);

    if ((pOldRtEntry == NULL) ||
        (pOldRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
    {
        /* either Route is not present 
           Or the route is old route (calculated in Last route calculation)
           so create a new route */
        u1NewRtEntry = 1;
        /* Incrementing of ABR Count & ASBR Count handled */
        if (u1DestType == DEST_AREA_BORDER)
        {
            pArea->u4AreaBdrRtrCount += 1;
        }
        if (u1DestType == DEST_AS_BOUNDARY)
        {
            pArea->u4AsBdrRtrCount += 1;
        }

    }
    /* update addr range active flag */
    RtcSetAddrRngActvFlgInCxt (pArea->pOspfCxt, &destId, &(pArea->areaId));
    if (!u1NewRtEntry)
    {
        if (u1DestType == DEST_AREA_BORDER)
        {
            pArea->u4AbrStatFlg = OSPF_TRUE;

            OSPF_TRC (OSPF_RT_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "FUNC : RtcGetPathInArea \n");

            if ((pOldDestRtPath = RtcGetPathInArea (pOldRtEntry,
                                                    pArea->areaId,
                                                    i1Tos)) != NULL)
            {
                u1NewPath = 2;
            }
            else
            {
                u1NewPath = 3;
            }

            OSPF_TRC (OSPF_RT_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "EXIT : RtcGetPathInArea \n");
        }
        else if (u1DestType == DEST_AS_BOUNDARY)
        {
            pArea->pOspfCxt->u4AsbrRtChange = OSPF_TRUE;
            /* Multiple entries for ASBR are stored */
            OSPF_TRC (OSPF_RT_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "FUNC : RtcGetPathInArea \n");

            if ((pOldDestRtPath = RtcGetPathInArea (pOldRtEntry,
                                                    pArea->areaId,
                                                    i1Tos)) != NULL)
            {
                u1NewPath = 2;
            }
            else
            {
                u1NewPath = 3;
            }

            OSPF_TRC (OSPF_RT_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "EXIT : RtcGetPathInArea \n");
        }
        else
        {

            /* destination is of type network */

            pOldDestRtPath =
                RtcGetRtTosPath (pOldRtEntry, i1Tos, OSPF_NEW_PATH);

            if ((pOldDestRtPath == NULL)
                || (pOldDestRtPath->u1PathType != INTRA_AREA))
            {
                /* if no path exists or if new path type is preferrable */
                u1NewPath = 1;
            }
            else
            {

                if ((pNewVertex->u1VertType == VERT_NETWORK) &&
                    (UtilIpAddrComp
                     (pOldDestRtPath->pLsaInfo->lsaId.linkStateId,
                      pNewVertex->pLsaInfo->lsaId.linkStateId) == OSPF_LESS))
                {
                    u1NewPath = 1;
                }
            }
        }
    }
    if ((!u1NewPath) && (!u1NewRtEntry))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "RT Not Updated\n");

        return;
    }

    /* create new path */
    if (u1NewRtEntry)
    {
        RtcInitialiseRtEntry (pNewRtEntry, u1DestType,
                              &destId, &ipAddrMask, options);
    }
    if ((pDestNewPath =
         RtcCreatePath (&(pArea->areaId), INTRA_AREA,
                        pNewVertex->u4Cost, 0)) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "RT Path Creation Failure\n");

        return;
    }
    else
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Path Created\n");
    }
    pDestNewPath->pLsaInfo = pNewVertex->pLsaInfo;

    /* set next hops in new path */

    for (u1Ind = 0;
         u1Ind < OSPF_MIN ((pNewVertex->u1HopCount), (MAX_NEXT_HOPS)); u1Ind++)
    {

        pDestNewPath->aNextHops[u1Ind].u1Status =
            pNewVertex->aNextHops[u1Ind].u1Status;
        pDestNewPath->aNextHops[u1Ind].pInterface =
            pNewVertex->aNextHops[u1Ind].pInterface;
        IP_ADDR_COPY (pDestNewPath->aNextHops[u1Ind].nbrIpAddr,
                      pNewVertex->aNextHops[u1Ind].nbrIpAddr);
        IP_ADDR_COPY (pDestNewPath->aNextHops[u1Ind].advRtrId,
                      pNewVertex->pLsaInfo->lsaId.advRtrId);
        RtcUpdateNextHopInCxt (pArea,
                               &(pDestNewPath->aNextHops[u1Ind]), destId);
    }
    pDestNewPath->u1HopCount = pNewVertex->u1HopCount;

    /* update routing table */

    if (u1NewRtEntry)
    {

        TMO_SLL_Add (&(pNewRtEntry->aTosPath[(INT2) i1Tos]),
                     &(pDestNewPath->nextPath));
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Entry To Be Added\n");
        RtcProcessOldNewRtEntryChanges (pCurrRt, pOldRtEntry, pNewRtEntry,
                                        i1Tos, pArea);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Entry Added\n");

    }
    else if (u1NewPath == 1)
    {
        RtcReplacePathInCxt (pArea->pOspfCxt, pOldRtEntry,
                             pDestNewPath, i1Tos, OSPF_FALSE);
    }
    else if (u1NewPath == 3)
    {
        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Adding Path To The Specified RT Entry Dstn %x TOS %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pOldRtEntry->destId), i1Tos);
        RtcAddPath (pOldRtEntry, pDestNewPath, i1Tos, OSPF_FALSE);
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New Pth Added To RT Entry\n");
    }
    else
    {
        /* If we reach here, u1NewPath = 2 */
        /* Add the New Next Hop To the existing PATH */
        for (u1Index1 = 0, u1Index2 = pOldDestRtPath->u1HopCount;
             u1Index1 < pDestNewPath->u1HopCount && u1Index2 < MAX_NEXT_HOPS;
             u1Index1++, u1Index2++)
        {
            OS_MEM_CPY (&pOldDestRtPath->aNextHops[u1Index2],
                        &pDestNewPath->aNextHops[u1Index1],
                        sizeof (tRouteNextHop));
        }
        pOldDestRtPath->u1HopCount = u1Index2;
        if (pDestNewPath != NULL)
        {
            PATH_FREE (pDestNewPath);
        }
    }

    if (u1DestType == DEST_AREA_BORDER)
    {
        if (u1NewRtEntry)
        {
            if ((pOldRtEntry != NULL) &&
                ((pOldRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_MODIFIED) ||
                 (pOldRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_USED)))
            {
                /* u1Flag in the route is updated to 
                   OSPF_ROUTE_MODIFIED or OSPF_ROUTE_USED */
                RtcSetVlReachability (pArea, pOldRtEntry);
            }
            else
            {
                RtcSetVlReachability (pArea, pNewRtEntry);
            }
        }
        else
        {
            RtcSetVlReachability (pArea, pOldRtEntry);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: RtcUpdateRtIntraAreaRoute  \n");
    KW_FALSEPOSITIVE_FIX (pDestNewPath);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcUpdateCandteLst                                         */
/*                                                                           */
/* Description  : If the vertex specified in the link is already present in  */
/*                the candidate list its contents are updated. Otherwise a   */
/*                new candidate node is created for the vertex.              */
/*                                                                           */
/* Input        : pSpfTree           : SPF Tree                              */
/*                pNewVertex         : vertex whose links are processed      */
/*                pLinkNode          : the link that is being processed      */
/*                pArea               : the area for which spf_tree is       */
/*                                       computed                            */
/*                i1Tos               : type of service                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS or FAILURE                                         */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
RtcUpdateCandteLst (tCandteNode * spRoot, tSpf * pSpfTree,
                    tCandteNode * pNewVertex,
                    tLinkNode * pLinkNode, tArea * pArea, INT1 i1Tos)
{

    tCandteNode        *pCandteNode;
    UINT4               u4Cost;
    UINT1               u1VertType;
    tLsaInfo           *pLsaInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC : RtcUpdateCandteLst \n");

    OSPF_TRC3 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
               pArea->pOspfCxt->u4OspfCxtId,
               "Process Lnk From Vertex %x To Vertex %x Through Area %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId),
               OSPF_CRU_BMC_DWFROMPDU (pLinkNode->linkId),
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));

    /* Validation of Cost of the link against the LSInfinity is omitted
     * here. This validation was present in RFC 1247 but has been
     * obsoleted from the RFC 1583.*/

    /* 
     * if the new vertex is a router and the link is a transit link then
     * the next vertex should be a network. Otherwise the next vertex is
     * a router.
     */

    if ((pNewVertex->u1VertType == VERT_ROUTER) &&
        (pLinkNode->u1LinkType == TRANSIT_LINK))
    {
        u1VertType = VERT_NETWORK;
    }
    else
    {
        u1VertType = VERT_ROUTER;
    }
    if ((pLsaInfo =
         RtcSearchDatabase ((UINT1) ((u1VertType == VERT_ROUTER) ? ROUTER_LSA :
                                     NETWORK_LSA), &(pLinkNode->linkId),
                            pArea)) == NULL)
    {

        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Rtr LSA Not Present Parent %x Dstn %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId),
                   OSPF_CRU_BMC_DWFROMPDU (pLinkNode->linkId));

        return OSPF_FAILURE;
    }
    if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {

        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "LSA With MaxAge Present Parent %x Dstn %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId),
                   OSPF_CRU_BMC_DWFROMPDU (pLinkNode->linkId));

        return SUCCESS;
    }
    if (RtcSearchLsaLinks (pLsaInfo, &(pNewVertex->vertexId), i1Tos)
        == OSPF_FALSE)
    {

        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Lnk Bk To Parent Not Present Parent %x Dstn %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNewVertex->vertexId),
                   OSPF_CRU_BMC_DWFROMPDU (pLinkNode->linkId));

        return OSPF_FAILURE;
    }

    if (((pCandteNode =
          (RtcSearchSpfTree (pSpfTree, &(pLinkNode->linkId), u1VertType)))
         != NULL) && (pCandteNode->u1SpfFlag != OSPF_SPF_UNUSED))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "Node Already Present in SPF Tree\n");

        return SUCCESS;
    }

    pCandteNode = RtcSearchCandteLstInCxt (pArea->pOspfCxt,
                                           &(pLinkNode->linkId), u1VertType);
    if (pLinkNode->u1LinkType == VIRTUAL_LINK)
    {
        tRtEntry           *pDestRtEntry = NULL;
        tPath              *pPath = NULL;

        if ((pDestRtEntry =
             RtcFindRtEntry (pArea->pOspfCxt->pOspfRt, &(pLinkNode->linkId),
                             &gNullIpAddr, DEST_AREA_BORDER)) != NULL)
        {
            /* Next Hop is taken to be the very first Path's Nbr
             * IpAddress
             */
            pPath = GET_TOS_PATH (pDestRtEntry, TOS_0);
            if (pPath != NULL)
            {
                u4Cost = pNewVertex->u4Cost + pPath->u4Cost;
            }
            else
            {
                u4Cost = pNewVertex->u4Cost;
            }
        }
        else
        {
            u4Cost = pNewVertex->u4Cost + pLinkNode->u2LinkCost;
        }
    }
    else
    {
        u4Cost = pNewVertex->u4Cost + pLinkNode->u2LinkCost;
    }

    if ((pCandteNode != NULL) && (u4Cost > pCandteNode->u4Cost))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "Candidate Lst Not Updated (Existing Cost Less)\n");

        return SUCCESS;

    }
    else if ((pCandteNode != NULL) && (u4Cost == pCandteNode->u4Cost))
    {

        RtcSetNextHops (spRoot, pNewVertex, pCandteNode, i1Tos);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "Candidate Lst Updated (New Nxt Hops Added)\n");

        return SUCCESS;

    }
    else
    {

        if (pCandteNode == NULL)
        {
            if ((pCandteNode =
                 RtcGetCandteNode (&(pLinkNode->linkId), u1VertType)) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC,
                          pArea->pOspfCxt->u4OspfCxtId,
                          "Candidate Alloc Failure\n");

                return OSPF_FAILURE;
            }
            else
            {
                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pArea->pOspfCxt->u4OspfCxtId,
                          "New Candidate Node Created\n");
            }
            pCandteNode->pLsaInfo = pLsaInfo;

            pCandteNode->u4Cost = u4Cost;
            pCandteNode->u1HopCount = 0;
            RtcAddCandNode (pArea->pOspfCxt, pCandteNode);

        }
        else
        {
            /* new cost equal is lesser than the old cost */
            RtcCandNodeCostChange (pArea->pOspfCxt, pCandteNode, u4Cost);
            pCandteNode->u1HopCount = 0;
        }

        RtcSetNextHops (spRoot, pNewVertex, pCandteNode, i1Tos);

        OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC : RtcUpdateCandteLst \n");

        KW_FALSEPOSITIVE_FIX (pCandteNode);
        return SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcAddStubLinkToVertex                                     */
/*                                                                           */
/* Description  : Adds the new stub link to the specified vertex.            */
/*                                                                           */
/* Input        : pNewVertex        : vertex                                 */
/*                pLinkNode         : stub link that is to be added          */
/*                pArea              : AREA                                  */
/*                i1Tos              : type of service                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcAddStubLinkToVertex (tCandteNode * pNewVertex,
                        tLinkNode * pLinkNode, tArea * pArea, INT1 i1Tos)
{

    tRtEntry           *pRtEntry;
    tPath              *pPath;
    tDestId             destId;

    tInterface         *pInterface = NULL;
    UINT1               u1PathType = INTRA_AREA;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC :  RtcAddStubLinkToVertex \n");

    UtilIpAddrMaskCopy (destId, pLinkNode->linkId, pLinkNode->linkData);
    if ((pRtEntry =
         RtcCreateRtEntry (DEST_NETWORK, &destId, &(pLinkNode->linkData),
                           0)) == NULL)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "RT Entry Creation Failure\n");
        return;
    }
    else
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Entry Created\n");
    }
    if (pNewVertex->aNextHops[0].u1Status == OSPF_NEXT_HOP_VALID)
    {
        pInterface = pNewVertex->aNextHops[0].pInterface;
        if ((pInterface != NULL) && (pInterface->u1NetworkType == IF_VIRTUAL))
        {
            u1PathType = INTER_AREA;
        }
    }
    if ((pPath =
         RtcCreatePath (&(pArea->areaId), u1PathType,
                        pLinkNode->u2LinkCost, 0)) == NULL)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "RT Path Creation Failure\n");
        RT_ENTRY_FREE (pRtEntry);
        return;
    }
    else
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Path Created\n");
    }
    RtcSetAddrRngActvFlgInCxt (pArea->pOspfCxt,
                               &(pRtEntry->destId), &(pArea->areaId));
    TMO_SLL_Add (&(pRtEntry->aTosPath[(INT2) i1Tos]), &(pPath->nextPath));
    TMO_SLL_Add (&(pNewVertex->stubLinks), &(pRtEntry->nextRtEntryNode));
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT :  RtcAddStubLinkToVertex \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessStubLinks                                        */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.1.                         */
/*                This procedure processes stub links in the calculated spf  */
/*                tree and adds appropriate entries in the routing table.    */
/*                                                                           */
/* Input        : pCurrRt       : routing table                              */
/*                pArea          : the area for which spf tree is computed   */
/*                i1Tos          : type of service                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcProcessStubLinks (tOspfRt * pCurrRt, tArea * pArea, INT1 i1Tos)
{

    tCandteNode        *pCandteNode;
    UINT4               u4HashKey;
    UINT1               u1NewRtEntry = 0;
    UINT1               u1NewPath = 0;
    tRtEntry           *pStubEntry;
    tRtEntry           *pOldRtEntry = NULL;
    tPath              *pDestNewPath;
    tPath              *pOldDestRtPath = NULL;
    tDestId             destId;
    tIPADDRMASK         DestMask;

    OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
               pArea->pOspfCxt->u4OspfCxtId,
               "Stub Lnks To Be Added To SPF Tree Area %x TOS %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId), i1Tos);

    TMO_HASH_Scan_Table (pArea->pSpf, u4HashKey)
    {

        TMO_HASH_Scan_Bucket (pArea->pSpf, u4HashKey,
                              pCandteNode, tCandteNode *)
        {

            /* Consider only the nodes added present RT calculation */
            if (pCandteNode->u1SpfFlag == OSPF_SPF_UNUSED)
            {
                continue;
            }
            /* consider only router vertices */

            if (pCandteNode->u1VertType == VERT_NETWORK)
                continue;

            while ((pStubEntry =
                    (tRtEntry *) TMO_SLL_Get (&(pCandteNode->stubLinks)))
                   != NULL)
            {

                u1NewPath = u1NewRtEntry = 0;
                if ((pDestNewPath = GET_TOS_PATH (pStubEntry, i1Tos)) == NULL)
                {
                    RtcFreeRtEntry (pStubEntry);
                    continue;
                }
                pDestNewPath->u4Cost += pCandteNode->u4Cost;
                pDestNewPath->pLsaInfo = pCandteNode->pLsaInfo;
                IP_ADDR_COPY (destId, pStubEntry->destId);

                /* if routing table entry does not exist create new entry */

                OSPF_CRU_BMC_DWTOPDU (DestMask, pStubEntry->u4IpAddrMask);
                pOldRtEntry = RtcFindRtEntry (pCurrRt, &destId,
                                              &DestMask, DEST_NETWORK);

                if ((pOldRtEntry == NULL) ||
                    (pOldRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
                {

                    u1NewRtEntry = 1;
                    u1NewPath = 1;
                }
                else
                {

                    if ((pOldDestRtPath =
                         RtcGetRtTosPath (pOldRtEntry,
                                          i1Tos, OSPF_NEW_PATH)) == NULL)
                    {
                        u1NewPath = 1;
                    }
                    else if (pOldDestRtPath->u4Cost < pDestNewPath->u4Cost)
                    {

                        /* 
                         * if current routing table cost is lesser consider 
                         * next link 
                         */
                        RtcFreeRtEntry (pStubEntry);
                        continue;

                    }

                    else if (pOldDestRtPath->u4Cost == pDestNewPath->u4Cost)
                    {
                        u1NewPath = 2;
                    }
                    else
                    {

                        /* existing cost is greater than new cost */
                        u1NewPath = 1;
                    }
                }

                /* 
                 * if u1NewPath = 1 add new entry
                 * if u1NewPath = 2 add set of next hops
                 */

                if (u1NewPath == 2)
                {
                    OS_MEM_CPY (pDestNewPath->areaId, pOldDestRtPath->areaId,
                                sizeof (pOldDestRtPath->areaId));
                    pDestNewPath->pLsaInfo = pOldDestRtPath->pLsaInfo;
                    pDestNewPath->u4Cost = pOldDestRtPath->u4Cost;
                    pDestNewPath->u4Type2Cost = pOldDestRtPath->u4Type2Cost;
                    pDestNewPath->u1PathType = pOldDestRtPath->u1PathType;
                    pDestNewPath->u1HopCount = pOldDestRtPath->u1HopCount;
                    pDestNewPath->u1NextHopIndex =
                        pOldDestRtPath->u1NextHopIndex;
                    pDestNewPath->u1Flag = pOldDestRtPath->u1Flag;
                    OS_MEM_CPY (pDestNewPath->aNextHops,
                                pOldDestRtPath->aNextHops,
                                sizeof (pOldDestRtPath->aNextHops));

                    if (UtilIpAddrComp (pCandteNode->vertexId,
                                        pDestNewPath->pLsaInfo->lsaId.
                                        linkStateId) == OSPF_LESS)
                    {

                        /* update link state origin */
                        pDestNewPath->pLsaInfo = pCandteNode->pLsaInfo;
                    }
                }

                /* add set of next hops */
                if (u1NewPath > 0)
                {
                    RtcSetStubNextHops (pDestNewPath, pCandteNode,
                                        &destId, pArea);
                }

                /* update routing table */
                if (u1NewRtEntry == 1)
                {
                    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                              pArea->pOspfCxt->u4OspfCxtId,
                              "New RT Entry To Be Added\n");
                    RtcProcessOldNewRtEntryChanges (pCurrRt, pOldRtEntry,
                                                    pStubEntry, i1Tos, pArea);
                    RtcFreeRtEntry (pStubEntry);

                    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                              pArea->pOspfCxt->u4OspfCxtId,
                              "New RT Entry Added\n");
                }
                else if (u1NewPath > 0)
                {

                    TMO_SLL_Delete (&(pStubEntry->aTosPath[(INT2) i1Tos]),
                                    &pDestNewPath->nextPath);
                    RtcFreeRtEntry (pStubEntry);
                    RtcReplacePathInCxt (pArea->pOspfCxt, pOldRtEntry,
                                         pDestNewPath, i1Tos, OSPF_FALSE);
                }
                /* scan list of stub links */
            }
            /* hash bucket scan */
        }
        /* spf hash table scan */
    }

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Stub Lnks Processed\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetCandteNode                                           */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure allocates a candidate node and initialises  */
/*                the vertexId, associated lsa, vert_type fields.            */
/*                                                                           */
/* Input        : pVertexId       : vertex id                                */
/*                u1VertType      : vertex type                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to candte node, if node successfully allocated and */
/*                initialized                                                */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tCandteNode *
RtcGetCandteNode (tVertexId * pVertexId, UINT1 u1VertType)
{

    tCandteNode        *pCandteNode;
    UINT1               u1Count;
    if (CANDTE_ALLOC (pCandteNode) == NULL)
    {

        return NULL;
    }
    IP_ADDR_COPY (pCandteNode->vertexId, *pVertexId);
    pCandteNode->u4Cost = LS_INFINITY_16BIT;
    pCandteNode->u1VertType = u1VertType;
    pCandteNode->u1RtrEncnted = OSPF_FALSE;
    pCandteNode->u1HopCount = 0;
    TMO_SLL_Init (&pCandteNode->stubLinks);
    TMO_HASH_INIT_NODE (&pCandteNode->candteHashNode);
    TMO_HASH_INIT_NODE (&pCandteNode->candteHashNode2);
    for (u1Count = 0; u1Count < MAX_NEXT_HOPS; u1Count++)
    {
        pCandteNode->aNextHops[u1Count].pInterface = NULL;
        OS_MEM_SET ((char *) pCandteNode->aNextHops[u1Count].nbrIpAddr, 0, 4);
        pCandteNode->aNextHops[u1Count].u1Status = OSPF_NEXT_HOP_INVALID;
    }

    return pCandteNode;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSetStubNextHops                                         */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.1.1.                       */
/*                This procedure calculates the set of next hops for the     */
/*                specified destination and parent and adds it to the set of */
/*                next hops of the given path node.                          */
/*                                                                           */
/* Input        : pPath              : the path whose next hops is to be     */
/*                                      updated                              */
/*                p-parent            : the parent node which is a router    */
/*                pDest              : the IP address of the destination     */
/*                                      stub network                         */
/*                pArea              : the area whose intra-area routes are  */
/*                                      computed                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcSetStubNextHops (tPath * pPath,
                    tCandteNode * pParent, tIPADDR * pDest, tArea * pArea)
{

    UINT1               u1Ind;
    tHost              *pHost;

    UINT1               u1HcFlag = OSPF_TRUE;
    UINT1               u1Value;

    OSPF_TRC3 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
               pArea->pOspfCxt->u4OspfCxtId,
               "Calc For Nxt Hops For Paths To Detn %x Through Parent %x"
               "  Area %x Starts\n",
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pDest),
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pParent->vertexId)),
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pArea->areaId)));

    if (pPath->u1HopCount >= MAX_NEXT_HOPS)    /*Klocwork */
    {
        return;
    }

    if (UtilIpAddrComp (pParent->vertexId, pArea->pOspfCxt->rtrId) ==
        OSPF_EQUAL)
    {

        if (((pPath->aNextHops[pPath->u1HopCount].pInterface =
              RtcFindNextHopIf (pDest, VERT_NETWORK, pArea))
             != NULL) ||
            ((pPath->aNextHops[pPath->u1HopCount].pInterface =
              RtcFindNextHopIf (pDest, VERT_ROUTER, pArea)) != NULL))
        {
            pPath->aNextHops[pPath->u1HopCount].u1Status = OSPF_NEXT_HOP_VALID;
            RtcUpdateNextHopInCxt (pArea,
                                   &(pPath->aNextHops[pPath->u1HopCount]),
                                   *pDest);
            pPath->u1HopCount++;

        }
        else
        {

            /* consider directly attatched host routes */

            TMO_SLL_Scan (&(pArea->pOspfCxt->hostLst), pHost, tHost *)
            {

                if (UtilIpAddrComp (pHost->hostIpAddr, *pDest) == OSPF_EQUAL)
                {
                    pPath->aNextHops[pPath->u1HopCount].u1Status =
                        OSPF_NEXT_HOP_VALID;
                    pPath->aNextHops[pPath->u1HopCount].u4IfIndex =
                        pHost->u4FwdIfIndex;
                    pPath->u1HopCount++;
                    break;
                }
            }
        }
    }
    else
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "The Vertex Inherits Nxt Hops Of Parent\n");

        for (u1Ind = 0; ((u1Ind < pParent->u1HopCount) &&
                         (u1Ind < MAX_NEXT_HOPS)); u1Ind++)
        {

            for (u1Value = 0; u1Value < pPath->u1HopCount &&
                 u1Value < MAX_NEXT_HOPS; u1Value++)
            {
                if ((pParent->aNextHops[u1Ind].pInterface ==
                     pPath->aNextHops[u1Value].pInterface) &&
                    (pParent->aNextHops[u1Ind].u1Status ==
                     pPath->aNextHops[u1Value].u1Status) &&
                    (UtilIpAddrComp (pParent->aNextHops[u1Ind].nbrIpAddr,
                                     pPath->aNextHops[u1Value].nbrIpAddr) ==
                     OSPF_EQUAL))
                {
                    u1HcFlag = OSPF_FALSE;
                }
            }

            if ((u1HcFlag == OSPF_TRUE) && (pPath->u1HopCount < MAX_NEXT_HOPS))
            {

                pPath->aNextHops[pPath->u1HopCount].u1Status =
                    pParent->aNextHops[u1Ind].u1Status;
                pPath->aNextHops[pPath->u1HopCount].pInterface =
                    pParent->aNextHops[u1Ind].pInterface;
                IP_ADDR_COPY
                    (pPath->aNextHops[pPath->u1HopCount].nbrIpAddr,
                     pParent->aNextHops[u1Ind].nbrIpAddr);
                RtcUpdateNextHopInCxt (pArea,
                                       &(pPath->aNextHops[pPath->u1HopCount]),
                                       *pDest);
                pPath->u1HopCount++;
            }
        }
    }

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Nxt Hops Set\n");
}

PRIVATE INT4
RtcCheckAndUpdateLeastIfIndexNextHop (tCandteNode * pVertex,
                                      tIPADDR nbrIpAddr,
                                      tInterface * pInterface)
{
    UINT4               u4Value = 0;

    for (u4Value = 0;
         u4Value < OSPF_MIN ((pVertex->u1HopCount), (MAX_NEXT_HOPS)); u4Value++)
    {
        if (UtilIpAddrComp (nbrIpAddr,
                            pVertex->aNextHops[u4Value].nbrIpAddr) ==
            OSPF_EQUAL)
        {
            if (pVertex->aNextHops[u4Value].pInterface->u4AddrlessIf
                < pInterface->u4AddrlessIf)
            {

                return OSPF_TRUE;    /* Skip */
            }
            else if (pVertex->aNextHops[u4Value].pInterface->u4AddrlessIf
                     > pInterface->u4AddrlessIf)
            {
                pVertex->aNextHops[u4Value].pInterface = pInterface;
                return OSPF_TRUE;    /* To replace the existing one and skip */
            }
        }
    }
    return OSPF_FALSE;            /* Not to skip */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSetNextHops                                             */
/*                                                                           */
/* Description  : Reference RFC-2178 section 16.1.1.                         */
/*                This function calculates a set of next hops for destination*/
/*                'vertex' and specified parent. The next hops field in the  */
/*                vertex node is appropriately updated.                      */
/*                                                                           */
/* Input        : pParent            : parent node                           */
/*                pVertex            : vertex                                */
/*                i1Tos              : type of service                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcSetNextHops (tCandteNode * spRoot, tCandteNode * pParent,
                tCandteNode * pVertex, INT1 i1Tos)
{

    UINT1               u1Ind;
    tLinkNode           linkNode;
    UINT1              *pCurrPtr;
    tInterface         *pInterface;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pLstNode;

    UINT1               u1HcFlag = OSPF_TRUE;
    UINT1               u1Value;

    OSPF_TRC (OSPF_FN_ENTRY, pVertex->pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC :  RtcSetNextHops \n");

    if (pVertex->u1HopCount >= MAX_NEXT_HOPS)
    {
        return;
    }

    if (pParent->u1RtrEncnted == OSPF_TRUE ||
        (pParent->u1VertType == VERT_ROUTER && pParent != spRoot))
    {

        for (u1Ind = 0;
             u1Ind < OSPF_MIN ((pParent->u1HopCount), (MAX_NEXT_HOPS)); u1Ind++)
        {

            for (u1Value = 0;
                 u1Value < OSPF_MIN ((pVertex->u1HopCount), (MAX_NEXT_HOPS));
                 u1Value++)
            {
                if ((pParent->aNextHops[u1Ind].pInterface ==
                     pVertex->aNextHops[u1Value].pInterface) &&
                    (pParent->aNextHops[u1Ind].u1Status ==
                     pVertex->aNextHops[u1Value].u1Status) &&
                    (UtilIpAddrComp (pParent->aNextHops[u1Ind].nbrIpAddr,
                                     pVertex->aNextHops[u1Value].nbrIpAddr) ==
                     OSPF_EQUAL))
                {
                    u1HcFlag = OSPF_FALSE;
                }
            }

            if (u1HcFlag == OSPF_TRUE)
            {
                if (RtcCheckAndUpdateLeastIfIndexNextHop (pVertex,
                                                          pParent->
                                                          aNextHops[u1Ind].
                                                          nbrIpAddr,
                                                          pParent->
                                                          aNextHops[u1Ind].
                                                          pInterface) ==
                    OSPF_FALSE)
                {
                    pVertex->aNextHops[pVertex->u1HopCount].u1Status =
                        pParent->aNextHops[u1Ind].u1Status;
                    pVertex->aNextHops[pVertex->u1HopCount].pInterface =
                        pParent->aNextHops[u1Ind].pInterface;
                    IP_ADDR_COPY (pVertex->aNextHops[pVertex->u1HopCount].
                                  nbrIpAddr,
                                  pParent->aNextHops[u1Ind].nbrIpAddr);
                    pVertex->u1HopCount++;
                }
            }

            /* check if the number of next hops in node reaches max limit */

            if (pVertex->u1HopCount == MAX_NEXT_HOPS)
            {
                break;
            }
        }
        pVertex->u1RtrEncnted = OSPF_TRUE;

    }
    else if (pParent == spRoot)
    {
        pInterface = (tInterface *) RtcFindIfId (pVertex,
                                                 pVertex->u1VertType,
                                                 pVertex->pLsaInfo->pArea,
                                                 i1Tos);
        if (pInterface != NULL)
        {
            if (pInterface->u1NetworkType == IF_PTOMP)
            {
                pCurrPtr = pVertex->pLsaInfo->pLsa + LS_HEADER_SIZE + 4;
                while ((pCurrPtr =
                        RtcGetLinksFromLsa (pVertex->pLsaInfo, i1Tos,
                                            pCurrPtr, &linkNode)) != NULL)
                {

                    if ((linkNode.u1LinkType == PTOP_LINK) &&
                        (UtilIpAddrComp (linkNode.linkId,
                                         pParent->vertexId) == OSPF_EQUAL))
                    {

                        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode,
                                      tTMO_SLL_NODE *)
                        {
                            pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);
                            if (UtilIpAddrComp (pNbr->nbrIpAddr,
                                                linkNode.linkData) ==
                                OSPF_EQUAL)
                            {
                                if (pVertex->u1HopCount >= MAX_NEXT_HOPS)
                                {
                                    return;
                                }
                                if (RtcCheckAndUpdateLeastIfIndexNextHop
                                    (pVertex, linkNode.linkData,
                                     pInterface) == OSPF_FALSE)
                                {
                                    IP_ADDR_COPY (pVertex->aNextHops
                                                  [pVertex->u1HopCount].
                                                  nbrIpAddr, linkNode.linkData);
                                    pVertex->aNextHops[pVertex->u1HopCount].
                                        u1Status = OSPF_NEXT_HOP_VALID;
                                    pVertex->aNextHops[pVertex->u1HopCount].
                                        pInterface = pInterface;
                                    pVertex->u1HopCount++;
                                }
                            }
                        }
                    }
                }
            }
            else if ((pInterface->u1NetworkType == IF_VIRTUAL)
                     && (pVertex->u1VertType == VERT_ROUTER))
            {
                /* Setting of the next hop should defer till 16.3 */
            }

            else if (pInterface->u1NetworkType == IF_PTOP)
            {
                if (pVertex->u1HopCount >= MAX_NEXT_HOPS)
                {
                    return;
                }
                if (pInterface->u4AddrlessIf != 0)
                {
                    pCurrPtr = pVertex->pLsaInfo->pLsa + LS_HEADER_SIZE + 4;

                    while ((pCurrPtr =
                            RtcGetLinksFromLsa (pVertex->pLsaInfo, i1Tos,
                                                pCurrPtr, &linkNode)) != NULL)
                    {
                        if ((linkNode.u1LinkType == PTOP_LINK) &&
                            (UtilIpAddrComp (linkNode.linkId,
                                             pParent->vertexId) == OSPF_EQUAL))
                        {
                            break;
                        }
                    }
                }

                if (RtcCheckAndUpdateLeastIfIndexNextHop (pVertex,
                                                          linkNode.linkId,
                                                          pInterface) ==
                    OSPF_FALSE)
                {
                    if (pInterface->u4AddrlessIf != 0)
                    {
                        IP_ADDR_COPY (pVertex->aNextHops[pVertex->u1HopCount].
                                      nbrIpAddr, linkNode.linkId);
                    }
                    pVertex->aNextHops[pVertex->u1HopCount].u1Status =
                        OSPF_NEXT_HOP_VALID;
                    pVertex->aNextHops[pVertex->u1HopCount].pInterface =
                        pInterface;
                    pVertex->u1HopCount++;
                }
            }
            else
            {
                if (pVertex->u1HopCount >= MAX_NEXT_HOPS)
                {
                    return;
                }

                pVertex->aNextHops[pVertex->u1HopCount].u1Status =
                    OSPF_NEXT_HOP_VALID;
                pVertex->aNextHops[pVertex->u1HopCount].pInterface = pInterface;
                pVertex->u1HopCount++;
            }
        }
    }
    else if ((pParent->u1VertType == VERT_NETWORK) &&
             (pVertex->u1VertType == VERT_ROUTER))
    {

        pCurrPtr = pVertex->pLsaInfo->pLsa + LS_HEADER_SIZE + 4;
        while ((pCurrPtr =
                RtcGetLinksFromLsa (pVertex->pLsaInfo, i1Tos,
                                    pCurrPtr, &linkNode)) != NULL)
        {

            if ((linkNode.u1LinkType == TRANSIT_LINK) &&
                (UtilIpAddrComp (linkNode.linkId, pParent->vertexId) ==
                 OSPF_EQUAL))
            {

                if ((pInterface =
                     RtcFindIfId (pParent,
                                  VERT_NETWORK,
                                  pVertex->pLsaInfo->pArea, i1Tos)) == NULL)
                {
                    continue;
                }

                if (pVertex->u1HopCount >= MAX_NEXT_HOPS)
                {
                    return;
                }

                if (RtcCheckAndUpdateLeastIfIndexNextHop (pVertex,
                                                          linkNode.linkData,
                                                          pInterface) ==
                    OSPF_FALSE)
                {
                    IP_ADDR_COPY (pVertex->aNextHops[pVertex->u1HopCount].
                                  nbrIpAddr, linkNode.linkData);
                    pVertex->aNextHops[pVertex->u1HopCount].u1Status =
                        OSPF_NEXT_HOP_VALID;
                    pVertex->aNextHops[pVertex->u1HopCount].pInterface =
                        pInterface;
                    pVertex->u1HopCount++;
                }
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pVertex->pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT :  RtcSetNextHops \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcUpdateNextHop                                           */
/*                                                                           */
/* Description  : Updates the next hop.                                      */
/*                                                                           */
/* Input        : pRtNextHop           : next hop                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcUpdateNextHopInCxt (tArea * pArea, tRouteNextHop * pRtNextHop, tIPADDR dest)
{
    tInterface         *pInterface = NULL;
    tInterface         *ptmpInterface = NULL;
    tNeighbor          *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tPath              *pPath = NULL;
    tRtEntry           *pDestRtEntry = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tTMO_SLL_NODE      *pIfNode;

    UINT4               u4IfIndex;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT4               u4UnnumAssocIPAddr = 0;
    UINT4               u4IsUpdated = 0;
    tIPADDR             SrcIpAddr;

    pOspfCxt = pArea->pOspfCxt;
    if (pRtNextHop->u1Status == OSPF_NEXT_HOP_VALID)
    {

        pRtNextHop->u4IfIndex = pRtNextHop->pInterface->u4IfIndex;
        pInterface = pRtNextHop->pInterface;

        /* Only when the interface type is PTOP
         * next hop should be nbr ip addr
         */

        if (pInterface->u1NetworkType == IF_PTOP)
        {
            pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
            if (NULL != pNbrNode)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if (NULL != pNbr)
                {
                    if (!IS_UNNUMBERED_PTOP_IFACE (pInterface))
                    {
                        if ((UtilIpAddrMaskComp (pInterface->ifIpAddr,
                                                 pNbr->nbrIpAddr,
                                                 pInterface->ifIpAddrMask) !=
                             OSPF_EQUAL) &&
                            (UtilIpAddrMaskComp (dest,
                                                 pInterface->ifIpAddr,
                                                 pInterface->ifIpAddrMask)
                             == OSPF_EQUAL))
                        {
                            IP_ADDR_COPY (pRtNextHop->nbrIpAddr, gNullIpAddr);
                        }
                        else
                        {
                            IP_ADDR_COPY (pRtNextHop->nbrIpAddr,
                                          pNbr->nbrIpAddr);
                        }
                    }
                    else
                    {
                        TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode,
                                      tTMO_SLL_NODE *)
                        {
                            ptmpInterface = GET_IF_PTR_FROM_LST (pIfNode);
                            if (ptmpInterface->u4AddrlessIf != 0)
                            {
                                u4IfIndex =
                                    OspfGetIfIndexFromPort (ptmpInterface->
                                                            u4IfIndex);
                                CfaGetIfUnnumAssocIPIf (u4IfIndex,
                                                        &u4UnnumAssocIPIf);
                                if (u4UnnumAssocIPIf != 0)
                                {
                                    CfaIpIfGetSrcAddressOnInterface
                                        (u4UnnumAssocIPIf, (UINT4) 0,
                                         &u4UnnumAssocIPAddr);
                                    u4UnnumAssocIPAddr =
                                        OSIX_HTONL (u4UnnumAssocIPAddr);
                                    /* convert u4UnnumAssocIPAddr to
                                     * network byte order and then copy */
                                    IP_ADDR_COPY (SrcIpAddr,
                                                  (UINT1 *)
                                                  &u4UnnumAssocIPAddr);
                                    if (MEMCMP (SrcIpAddr, dest, sizeof (UINT4))
                                        == 0)
                                    {

                                        IP_ADDR_COPY (pRtNextHop->nbrIpAddr,
                                                      gNullIpAddr);
                                        u4IsUpdated = 1;
                                        break;
                                    }
                                }
                            }

                        }
                        if (u4IsUpdated == 0)
                        {
                            if (UtilIpAddrComp (dest, pArea->pOspfCxt->rtrId) !=
                                OSPF_EQUAL)
                            {
                                IP_ADDR_COPY (pRtNextHop->nbrIpAddr,
                                              pNbr->nbrIpAddr);
                            }
                        }
                    }
                }
            }
        }

        /* when the interface type is VIRTUAL
         * next hop should be nbr ip addr of the ABR through which
         * the Virtual Link is formed
         */

        if (pInterface->u1NetworkType == IF_VIRTUAL)
        {
            pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
            if (pNbrNode != NULL)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if ((pDestRtEntry =
                     RtcFindRtEntry (pOspfCxt->pOspfRt, &(pNbr->nbrId),
                                     &gNullIpAddr, DEST_AREA_BORDER)) != NULL)
                {
                    /* Next Hop is taken to be the very first Path's Nbr
                     * IpAddress
                     */

                    pPath = GET_TOS_PATH (pDestRtEntry, TOS_0);
                    IP_ADDR_COPY (pRtNextHop->nbrIpAddr,
                                  pPath->aNextHops[0].nbrIpAddr);

                }
            }
        }

    }

    OSPF_TRC3 (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
               "Nxt Hop Updated Type %d IfIndx %d Nbr %x\n",
               pRtNextHop->u1Status,
               pRtNextHop->u4IfIndex,
               OSPF_CRU_BMC_DWFROMPDU (pRtNextHop->nbrIpAddr));

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFindNextHopIf                                           */
/*                                                                           */
/* Description  : This procedure finds the next hop interface for directly   */
/*                reachable destinations.                                    */
/*                                                                           */
/* Input        : pVertexId       : the vertex id of the vertex for which    */
/*                                    next hop is calculated                 */
/*                u1Type           : either VERT_ROUTER or VERT_NETWORK      */
/*                pArea            : the area for which spf_tree is computed */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to interface, if next hop found                    */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tInterface *
RtcFindNextHopIf (tVertexId * pVertexId, UINT1 u1Type, tArea * pArea)
{

    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    INT1                i1Count;

    OSPF_TRC3 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
               pArea->pOspfCxt->u4OspfCxtId,
               "Find Nxt Hop If For Dstn %x VertexType %d Through Area %x\n",
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pVertexId)),
               u1Type, OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pArea->areaId)));

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_LST (pLstNode);

        if (pInterface->operStatus != OSPF_ENABLED)
        {
            continue;            /* looks only for enabled interfaces */
        }

        switch (pInterface->u1NetworkType)
        {
            case IF_BROADCAST:
            case IF_NBMA:
            case IF_LOOPBACK:
                if ((u1Type == VERT_NETWORK) &&
                    (UtilIpAddrMaskComp (*pVertexId,
                                         pInterface->ifIpAddr,
                                         pInterface->ifIpAddrMask) ==
                     OSPF_EQUAL))
                {

                    OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                               pArea->pOspfCxt->u4OspfCxtId,
                               "Nxt Hop Found If %x.%d\n",
                               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                               pInterface->u4AddrlessIf);

                    return pInterface;
                }

                for (i1Count = 0;
                     i1Count < pInterface->secondaryIP.i1SecIpCount; i1Count++)
                {
                    if ((u1Type == VERT_NETWORK) &&
                        (UtilIpAddrMaskComp (*pVertexId,
                                             pInterface->secondaryIP.
                                             ifSecIp[i1Count].ifSecIpAddr,
                                             pInterface->secondaryIP.
                                             ifSecIp[i1Count].
                                             ifSecIpAddrMask) == OSPF_EQUAL))
                    {
                        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                                   pArea->pOspfCxt->u4OspfCxtId,
                                   "Nxt Hop Found If %x.%d\n",
                                   OSPF_CRU_BMC_DWFROMPDU (pInterface->
                                                           ifIpAddr),
                                   pInterface->u4AddrlessIf);

                        return pInterface;

                    }

                }

                break;

            case IF_PTOP:
            case IF_VIRTUAL:
                if (u1Type == VERT_ROUTER)
                {

                    if ((pNbrNode =
                         TMO_SLL_First (&(pInterface->nbrsInIf))) == NULL)
                    {
                        if (UtilIpAddrMaskComp
                            (*pVertexId, pInterface->ifIpAddr,
                             pInterface->ifIpAddrMask) == OSPF_EQUAL)
                        {
                            return pInterface;
                        }

                        break;
                    }

                    pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                    if (UtilIpAddrMaskComp
                        (*pVertexId, pNbr->nbrIpAddr,
                         pInterface->ifIpAddrMask) == OSPF_EQUAL)

                    {

                        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                                   pArea->pOspfCxt->u4OspfCxtId,
                                   "Nxt Hop Found If %x.%d\n",
                                   OSPF_CRU_BMC_DWFROMPDU
                                   (pInterface->ifIpAddr),
                                   pInterface->u4AddrlessIf);

                        return pInterface;
                    }
                    else if (UtilIpAddrMaskComp
                             (*pVertexId, pInterface->ifIpAddr,
                              pInterface->ifIpAddrMask) == OSPF_EQUAL)
                    {
                        return pInterface;
                    }
                }
                break;
            case IF_PTOMP:
                if (u1Type == VERT_ROUTER)
                {

                    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                                  tTMO_SLL_NODE *)
                    {
                        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                        if (UtilIpAddrComp (*pVertexId, pNbr->nbrIpAddr)
                            == OSPF_EQUAL)
                        {

                            OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                                       pArea->pOspfCxt->u4OspfCxtId,
                                       "Nxt Hop Found If %x.%d\n",
                                       OSPF_CRU_BMC_DWFROMPDU
                                       (pInterface->ifIpAddr),
                                       pInterface->u4AddrlessIf);

                            return pInterface;
                        }
                    }

                    if (UtilIpAddrMaskComp (*pVertexId,
                                            pInterface->ifIpAddr,
                                            pInterface->ifIpAddrMask)
                        == OSPF_EQUAL)
                    {
                        return pInterface;
                    }

                }
                break;

            default:
                break;
        }
    }

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Nxt Hop Not Found\n");

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCalAllInterAreaRts                                      */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 16.2.                         */
/*                This procedure calculates the inter-area routes considering*/
/*                the summary links advertisements. If this router is an     */
/*                area-border-router the summary LSAs in the backbone alone  */
/*                are considered. Otherwise the summary LSAs in the attatched*/
/*                area are considered.                                       */
/*                                                                           */
/* Input        : pCurrRt       : routing table                              */
/*                i1Tos          : type of service                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcCalAllInterAreaRtsInCxt (tOspfCxt * pOspfCxt, tOspfRt * pCurrRt, INT1 i1Tos)
{

    tArea              *pArea;
    tLsaInfo           *pLsaInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC :  RtcCalAllInterAreaRts\n");

    if (pOspfCxt->u4ABRType == STANDARD_ABR)
    {
        /* Ref : RFC 2328 Section 16.2 */
        /* If the router has active attachments to multiple areas,
         * (STANDARD ABR Type), only Backbone summary LSAs are 
         * examined */
        if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                      "ABR (Only BB DataBase Considered)\n");
            pArea = pOspfCxt->pBackbone;
        }
        else
        {
            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                if (IS_ACTIVELY_ATTACHED_AREA (pArea))
                {
                    break;
                }
            }
        }

        if (pArea == NULL)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                      "No Active area present\n");
            return;
        }
        pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
        while (pLsaInfo != NULL)
        {

            RtcCalculateInterAreaRoute (pCurrRt, pLsaInfo, i1Tos, pArea);
            pLsaInfo =
                (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot, pLsaInfo,
                                            NULL);
        }
    }
    else
    {
        /* RFC 3509 : Section 2.2 point 2 */
        /* If the router is an ABR and having one active backbone 
         * connection, then consider only Backbone summary LSAs. */
        if ((pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
            && (pOspfCxt->pBackbone->u4FullNbrCount > 0))
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                      "ABR (Only BB DataBase Considered)\n");
            pArea = pOspfCxt->pBackbone;
            pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
            while (pLsaInfo != NULL)
            {

                RtcCalculateInterAreaRoute (pCurrRt, pLsaInfo, i1Tos, pArea);
                pLsaInfo =
                    (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot,
                                                pLsaInfo, NULL);
            }
        }
        else
        {
            /* RFC 3509 : Section 2.2 */
            /* Consider summary LSAs from all actively Attached areas */
            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                if (IS_ACTIVELY_ATTACHED_AREA (pArea))
                {
                    pLsaInfo =
                        (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
                    while (pLsaInfo != NULL)
                    {
                        RtcCalculateInterAreaRoute (pCurrRt, pLsaInfo,
                                                    i1Tos, pArea);
                        pLsaInfo =
                            (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot,
                                                        pLsaInfo, NULL);
                    }
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCalculateInterAreaRoute                                 */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.2.                         */
/*                The inter-area route to the destination described by the   */
/*                specified LSA is updated.                                  */
/*                                                                           */
/* Input        : pCurrRt         : routing table                            */
/*                pLsaInfo        : pointer to the advertisement             */
/*                i1Tos            : type of service                         */
/*                pArea            : AREA for which the inter area route is  */
/*                                    calculated                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the updated router table entry, if route is     */
/*                calculated successfully                                    */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tRtEntry   *
RtcCalculateInterAreaRoute (tOspfRt * pCurrRt,
                            tLsaInfo * pLsaInfo, INT1 i1Tos, tArea * pArea)
{

    UINT1               u1Lock;
    UINT4               u4Cost;
    tRtEntry           *pDestRtEntry = NULL;
    UINT1               u1NewRtEntry = 0;
    UINT1               u1NewPath = 0;
    UINT1               u1Index1;
    UINT1               u1Index2;
    tIPADDR             destMask;
    tPath              *pOldDestRtPath = NULL;
    tPath              *pDestNewPath;
    tPath              *pAreaBdrPath;
    tAddrRange         *pAddrRange;
    tRtEntry            newRtEntry;
    tRtEntry           *pNewRtEntry = NULL;
    tRtEntry           *pOldRtEntry = NULL;
    INT1                i1PathExists = OSPF_TRUE;
    UINT4               u4CurrentTime = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC :RtcCalculateInterAreaRoute\n");

    /* Check whether we need to relinquish the route calculation
     * to do other OSPF processings */
    if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        /* current time greater than relinquished interval */
        if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId) &&
            (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta))
        {
            gOsRtr.u4RTstaggeredCtxId = pArea->pOspfCxt->u4OspfCxtId;
            OSPFRtcRelinquish (pArea->pOspfCxt);
        }
    }

    u1Lock = (pCurrRt == pLsaInfo->pOspfCxt->pOspfRt) ? OSPF_TRUE : OSPF_FALSE;

    /* MAX_AGE LSAs are omitted from routing table calculation */
    pNewRtEntry = &newRtEntry;
    MEMSET (pNewRtEntry, 0, sizeof (tRtEntry));

    if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "MaxAge LSA Not Considered For RT Calculation\n");

        return NULL;
    }

#ifdef TOS_SUPPORT
    /*T-bit not set, do not consider the lsa when calculating non 0 
       tos routes */

    if (((pLsaInfo->lsaOptions & T_BIT_MASK) != T_BIT_MASK) && (i1Tos != TOS_0))
    {

        return NULL;
    }
#endif /* TOS_SUPPORT */

    /* Self originated advertisements are not considered */

    if (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "Self Originated Sum LSA "
                  "Not Considered For RT Calculation\n");

        return NULL;
    }

    /* if the cost specified is LS_INFINITY_24BIT ignore advertisement */

    u4Cost =
        RtcGetCostFromSummaryLsa (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, i1Tos);
    if (u4Cost >= LS_INFINITY_24BIT)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "LSA Cost Adv LSInfinity)\n");

        return NULL;
    }

    /* get network mask from summary lsa */

    if (pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA)
    {
        IP_ADDR_COPY (destMask, (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE));
    }
    else
    {
        IP_ADDR_COPY (destMask, gNullIpAddr);
    }

    /* 
     * if the  destination falls into one of the area's active address ranges
     * ignore the advertisement 
     */

    if ((pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA) &&
        ((pAddrRange =
          UtilFindAssoAllAreaAddrRangeInCxt (pLsaInfo->pOspfCxt,
                                             &(pLsaInfo->lsaId.linkStateId)))
         != NULL) && (pAddrRange->u1CalActive == OSPF_TRUE))
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "Dstn Falls Into One Of Attached Area Addr Ranges)\n");

        return NULL;
    }

    /* 
     * if the advertising router does not have a routing table entry with 
     * pLsaInfo->pArea as the associated area ignore lsa .
     */

    if ((pAreaBdrPath =
         RtcFindPath (pCurrRt, &(pLsaInfo->lsaId.advRtrId),
                      DEST_AREA_BORDER, i1Tos, pLsaInfo->pArea)) == NULL)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "ABR Unreachable\n");

        return NULL;
    }

    /* add cost of area border router to the cost advertised in summary lsa */

    u4Cost += pAreaBdrPath->u4Cost;

    /* find routing table entry for the destination described by lsa */

    pOldRtEntry =
        RtcFindRtEntry (pCurrRt, &(pLsaInfo->lsaId.linkStateId),
                        &destMask, ((UINT1) ((pLsaInfo->lsaId.u1LsaType ==
                                              NETWORK_SUM_LSA) ? DEST_NETWORK
                                             : DEST_AS_BOUNDARY)));

    if (pOldRtEntry == NULL)
    {
        /* if no entry exists create new entry */
        u1NewRtEntry = 1;
        u1NewPath = 1;
    }
    else if (pOldRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED)
    {
        /* Only Old Route Entry Exist, create new entry */
        u1NewRtEntry = 1;
        u1NewPath = 1;
    }
    else if (pOldRtEntry->au1Flag[i1Tos] != OSPF_ROUTE_UNUSED)
    {
        if ((pOldDestRtPath = RtcGetRtTosPath (pOldRtEntry,
                                               i1Tos, OSPF_NEW_PATH)) != NULL)
        {
            if (pOldDestRtPath->u1PathType == INTRA_AREA)
            {
                /* if existing routing table entry is intra_area ignore lsa */

                OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                          pArea->pOspfCxt->u4OspfCxtId,
                          "Dstn Already Has Intra Area path\n");
                return NULL;
            }
            else if (pOldDestRtPath->u1PathType == INTER_AREA)
            {

                /* if existing entry is of type inter area update entry */

                if (pOldDestRtPath->u4Cost > u4Cost)
                {

                    /* if new cost is cheaper replace entry */

                    u1NewPath = 1;

                }
                else if (pOldDestRtPath->u4Cost == u4Cost)
                {

                    /* if cost is equal add next hops */

                    u1NewPath = 2;
                }
                else
                {

                    /* existing cost is cheaper */

                    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                              pArea->pOspfCxt->u4OspfCxtId,
                              "Existing Cost Cheaper)\n");

                    return NULL;
                }

            }
            else
            {

                /* if existing entry is of type AS ext install new entry */

                u1NewPath = 1;
            }

        }
        else
        {
            u1NewPath = 1;
        }
    }

    /* 
     * if u1NewPath flag is set to 1 then replace path 
     * if u1NewPath flag is set to 2 then add to list of paths
     */

    pDestNewPath = (tPath *) NULL;
    if (u1NewPath == 1)
    {

        if ((pDestNewPath =
             RtcCreatePath (&(pArea->areaId), INTER_AREA, u4Cost, 0)) == NULL)
        {

            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId, "Path Creation Failure\n");
            return NULL;
        }
        else
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId, "New RT Path Created\n");
        }
    }
    else if (u1NewPath == 2)
    {
        if (PATH_ALLOC (pDestNewPath) == NULL)
        {

            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pArea->pOspfCxt->u4OspfCxtId, "Path Alloc Failure\n");

            return NULL;
        }
        OS_MEM_CPY (pDestNewPath, pOldDestRtPath, sizeof (tPath));
    }

    if (pDestNewPath == NULL)    /* Klocwork */
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "Path is NULL\n");
        return NULL;
    }

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
              pArea->pOspfCxt->u4OspfCxtId,
              "Nxt Hop Of ABR To Be Added To The Inter Area Path\n");

    for (u1Index1 = 0, u1Index2 = pDestNewPath->u1HopCount;
         u1Index1 < OSPF_MIN ((pAreaBdrPath->u1HopCount), (MAX_NEXT_HOPS)) &&
         u1Index2 < MAX_NEXT_HOPS; u1Index1++)
    {
        OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC : RtcIsSameNextHop \n");
        i1PathExists = RtcIsSameNextHop (pDestNewPath, pAreaBdrPath, u1Index1);
        OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT : RtcIsSameNextHop \n");
        if (i1PathExists == OSPF_FALSE)
        {
            OS_MEM_CPY (&pDestNewPath->aNextHops[u1Index2],
                        &pAreaBdrPath->aNextHops[u1Index1],
                        sizeof (tRouteNextHop));
            IP_ADDR_COPY (pDestNewPath->aNextHops[u1Index2].advRtrId,
                          pLsaInfo->lsaId.advRtrId);
            u1Index2++;
        }
    }
    pDestNewPath->u1HopCount = u1Index2;

    /* update routing table */

    if (u1NewRtEntry)
    {
        if (pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA)
        {
            RtcInitialiseRtEntry (pNewRtEntry, DEST_NETWORK,
                                  &(pLsaInfo->lsaId.linkStateId), &destMask, 0);
        }
        else
        {
            RtcInitialiseRtEntry (pNewRtEntry, DEST_AS_BOUNDARY,
                                  &(pLsaInfo->lsaId.linkStateId),
                                  &destMask,
                                  *(UINT1 *) (pLsaInfo->pLsa +
                                              OPTIONS_OFFSET_IN_LS_HEADER));

        }
        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Adding Path To The Specified RT Entry Dstn %x TOS %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNewRtEntry->destId), i1Tos);
        RtcAddPath (pNewRtEntry, pDestNewPath, i1Tos, u1Lock);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New Pth Added To RT Entry\n");
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Entry To Be Added\n");
        RtcProcessOldNewRtEntryChanges (pCurrRt, pOldRtEntry, pNewRtEntry,
                                        i1Tos, pArea);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "New RT Entry Added\n");

    }
    else if (u1NewPath)
    {
        RtcReplacePathInCxt (pLsaInfo->pOspfCxt, pOldRtEntry,
                             pDestNewPath, i1Tos, u1Lock);
    }

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "RT Entry Updated\n");
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT :RtcCalculateInterAreaRoute\n");
    KW_FALSEPOSITIVE_FIX (pDestNewPath);
    return pDestRtEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetCostFromSummaryLsa                                   */
/*                                                                           */
/* Description  : Reference : Utility.                                       */
/*                This procedure gets the cost for the specified TOS from the*/
/*                given LSA.                                                 */
/*                                                                           */
/* Input        : pLsa       : pointer to LSA                                */
/*                u2LsaLen  : length of LSA                                  */
/*                i1Tos      : type of service                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : cost for the specified TOS                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
RtcGetCostFromSummaryLsa (UINT1 *pLsa, UINT2 u2LsaLen, INT1 i1Tos)
{

    UINT4               u4Cost = LS_INFINITY_24BIT;
    UINT2               u2TosCount;

    u2TosCount =
        (UINT2) ((u2LsaLen - (LS_HEADER_SIZE + sizeof (tIPADDRMASK))) / 4);
    pLsa += LS_HEADER_SIZE + sizeof (tIPADDRMASK);
    while (u2TosCount--)
    {

        if (*((UINT1 *) pLsa) == ENCODE_TOS (i1Tos))
        {
            pLsa++;
            u4Cost = LGET3BYTE (pLsa);
            break;
        }
        else
        {
            pLsa += 4;
        }

    }

    return u4Cost;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcExmnTrnstAreasSmmryLinks                                */
/*                                                                           */
/* Description  : This procedure examines transit area summary links to find */
/*                better inter area routes via transit area.                 */
/*                                                                           */
/* Input        : pCurrRt       : routing table                              */
/*                pArea          : area                                      */
/*                i1Tos          : type of service                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcExmnTrnstAreasSmmryLinks (tOspfRt * pCurrRt, tArea * pArea, INT1 i1Tos)
{
    tLsaInfo           *pLsaInfo;
    tLsaInfo           *pNextLsaInfo;
    UINT4               u4Cost;
    UINT4               u4Len;
    tIPADDRMASK         destIpAddrMask;
    tRtEntry           *pDestRtEntry;
    tPath              *pDestRtPath;
    tPath              *pAreaBdrPath;
    tPath              *pDestNewPath;
    INT1                i1PathExists = OSPF_TRUE;
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;
    UINT1               u1DestType;
    UINT4               u4CurrentTime = 0;

    pNextLsaInfo = (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
    while (pNextLsaInfo != NULL)
    {
        pLsaInfo = pNextLsaInfo;
        pNextLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot, pLsaInfo, NULL);

        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId) &&
                (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta))
            {
                gOsRtr.u4RTstaggeredCtxId = pArea->pOspfCxt->u4OspfCxtId;
                OSPFRtcRelinquish (pArea->pOspfCxt);
            }
        }

        /* check lsa cost corresponding to i1Tos for lsinfinity */
        u4Len = *(pLsaInfo->pLsa + (LS_HEADER_SIZE - 1));
        u4Cost =
            RtcGetCostFromSummaryLsa (pLsaInfo->pLsa, (UINT2) u4Len, i1Tos);
        if (u4Cost >= LS_INFINITY_24BIT)
        {
            continue;
        }
        if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
        {
            continue;
        }
        if (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))
        {
            continue;
        }
#ifdef TOS_SUPPORT
        if ((i1Tos != TOS_0) &&
            ((pLsaInfo->lsaOptions & T_BIT_MASK) != T_BIT_MASK))
        {
            continue;
        }
#endif /* TOS_SUPPORT */

        OS_MEM_CPY (destIpAddrMask,
                    (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE),
                    MAX_IP_ADDR_LEN);
        if (pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA)
        {
            u1DestType = DEST_NETWORK;
        }
        else
        {
            u1DestType = DEST_AS_BOUNDARY;
        }
        pDestRtEntry =
            RtcFindRtEntry (pCurrRt, &pLsaInfo->lsaId.linkStateId,
                            &(destIpAddrMask), u1DestType);

        if ((pDestRtEntry == NULL) ||
            (pDestRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
        {
            continue;
        }
        TMO_SLL_Scan (&(pDestRtEntry->aTosPath[TOS_0]), pDestRtPath, tPath *)
        {

            if (pDestRtPath->u1PathType == TYPE_1_EXT ||
                pDestRtPath->u1PathType == TYPE_2_EXT ||
                !BACKBONE_ID (pDestRtPath->areaId) ||
                pDestRtPath->u1Flag == OSPF_OLD_PATH)
            {

                continue;
            }

            if ((pAreaBdrPath = RtcFindPath (pCurrRt,
                                             &(pLsaInfo->lsaId.advRtrId),
                                             DEST_AREA_BORDER, i1Tos,
                                             pLsaInfo->pArea)) == NULL)
            {
                continue;
            }

            u4Cost += pAreaBdrPath->u4Cost;

            if (u4Cost < pDestRtPath->u4Cost)
            {
                pDestNewPath = RtcAssociateNewPath (pAreaBdrPath, u4Cost,
                                                    i1Tos, pLsaInfo);
                if (pDestNewPath != NULL)
                {
                    pDestNewPath->u1PathType = INTER_AREA;

                    RtcReplacePathInCxt (pArea->pOspfCxt, pDestRtEntry,
                                         pDestNewPath, i1Tos, OSPF_FALSE);
                }
            }
            else if (u4Cost == pDestRtPath->u4Cost)
            {
                /* In this funtion RtcExmnTrnstAreasSmmryLinks () the better routes
                 * for already calculated Virtual links are calculated. Hence the
                 * Path Type is INTER_AREA and the area Id is Backbone AreaId
                 * Ref. RFC 2328 Sec 16.3
                 */

                pDestNewPath =
                    RtcCreatePath (&gBackboneAreaId, INTER_AREA, u4Cost, 0);

                if (pDestNewPath == NULL)
                {
                    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                              pArea->pOspfCxt->u4OspfCxtId,
                              "Path Creation Failure\n");
                    continue;
                }
                else
                {
                    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                              pArea->pOspfCxt->u4OspfCxtId,
                              "New RT Path Created\n");
                }

                /* ECMP Condition, Create a new Path 'pDestNewPath' and copy the
                 * existing 'pDestRtPath' to 'pDestNewPath' and add the ABR Path
                 * Next Hops to the New Path only if the same next Hop is not
                 * present already
                 */

                OS_MEM_CPY (pDestNewPath, pDestRtPath, PATH_SIZE);
                pDestNewPath->u1PathType = INTER_AREA;

                for (u1Index1 = 0, u1Index2 = pDestNewPath->u1HopCount;
                     (u1Index1 <
                      OSPF_MIN ((pAreaBdrPath->u1HopCount), (MAX_NEXT_HOPS)) &&
                      u1Index2 < MAX_NEXT_HOPS); u1Index1++)
                {
                    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
                              "FUNC : RtcIsSameNextHop \n");
                    i1PathExists =
                        RtcIsSameNextHop (pDestRtPath, pAreaBdrPath, u1Index1);
                    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
                              "EXIT : RtcIsSameNextHop \n");

                    if (i1PathExists == OSPF_FALSE)
                    {
                        OS_MEM_CPY (&pDestNewPath->aNextHops[u1Index2],
                                    &pAreaBdrPath->aNextHops[u1Index1],
                                    sizeof (tRouteNextHop));
                        IP_ADDR_COPY (pDestNewPath->
                                      aNextHops[u1Index2].advRtrId,
                                      pLsaInfo->lsaId.advRtrId);
                        u1Index2++;
                    }
                }

                pDestNewPath->u1HopCount = u1Index2;

                RtcReplacePathInCxt (pArea->pOspfCxt, pDestRtEntry,
                                     pDestNewPath, i1Tos, OSPF_FALSE);
            }
        }                        /* End of Tmo sll scan for paths */
    }
    /* End of Tmo sll scan for summary lsas */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcAssociateNewPath                                        */
/*                                                                           */
/* Description  : This routine associates the new path with the corresponding*/
/*                areaId, Lsa information and Next hops etc.,                */
/*                                                                           */
/* Input        : pAreaBdrPath        : path information that has to be      */
/*                                         associated with the new path.     */
/*                u4Cost                : cost                               */
/*                i1Tos                 : Type of service.                   */
/*                pLsaInfo             : Pointer to the Lsa information      */
/*                                         associated with destination       */
/*                                         network.                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the PATH, on success                            */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tPath      *
RtcAssociateNewPath (tPath * pAreaBdrPath,
                     UINT4 u4Cost, INT1 i1Tos, tLsaInfo * pLsaInfo)
{
    UINT1               u1Ind;
    tPath              *pNewPath;

    UNUSED_PARAM (i1Tos);        /* Added to remove warning */

    if (PATH_ALLOC (pNewPath) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "Path Alloc Failure\n");

        return NULL;
    }

    pNewPath->u4Cost = u4Cost;
    IP_ADDR_COPY (pNewPath->areaId, gBackboneAreaId);

    /* This funtion RtcAssociateNewPath () is called only from
     * RtcExmnTrnstAreasSmmryLinks () function. The ABR through which the Path
     * is formed can be of INTRA_AREA path type also. Hence explicity here the
     * Path Type is made to be INTER_AREA and not the Path Type of the ABR and
     * the AreaId is Backbone as this route is learnt only from Backbone.
     */

    pNewPath->pLsaInfo = pLsaInfo;

    /* set next hops in new path */

    for (u1Ind = 0;
         u1Ind < OSPF_MIN ((pAreaBdrPath->u1HopCount), (MAX_NEXT_HOPS));
         u1Ind++)
    {
        OS_MEM_CPY (&pNewPath->aNextHops[u1Ind],
                    &pAreaBdrPath->aNextHops[u1Ind], sizeof (tRouteNextHop));
    }
    pNewPath->u1HopCount = pAreaBdrPath->u1HopCount;
    pNewPath->u1PathType = pAreaBdrPath->u1PathType;

    return pNewPath;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetNextHopArea                                          */
/*                                                                           */
/* Description  : Finds the AREA for the next hop.                           */
/*                                                                           */
/* Input        : pRtNextHop          : next hop                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the AREA, on success                            */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE tArea      *
RtcGetNextHopArea (tInterface * pInterface, UINT1 u1Status)
{

    /* For directly attatched host routes pNextHop will be  NULL */

    if (pInterface == NULL)
    {
        return NULL;
    }
    if (u1Status == OSPF_NEXT_HOP_VALID)
    {
        if (pInterface->u1NetworkType == IF_VIRTUAL)
        {
            return GetFindAreaInCxt (pInterface->pArea->pOspfCxt,
                                     &(pInterface->transitAreaId));
        }
        else
        {
            return pInterface->pArea;
        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCalculateAllExtRoutesInCxt                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.4.                         */
/*                This procedure calculates routes to destinations external  */
/*                to the AS by considering all the as external LSAs.         */
/*                                                                           */
/* Input        : pOspfCxt      : context pointer                            */
/*                pCurrRt       : routing table                              */
/*                i1Tos         : type of service                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcCalculateAllExtRoutesInCxt (tOspfCxt * pOspfCxt,
                               tOspfRt * pCurrRt, INT1 i1Tos)
{
    tOsDbNode          *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pLsaNode;
    tLsaInfo           *pLsaInfo;
    UINT4               u4HashKey = 0;
    UINT4               u4CurrentTime = 0;

    OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
               "AS Ext Route Calculation Starts TOS %d\n", i1Tos);

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLsaNode);
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
                RtcCalculateExtRoute (pCurrRt, pLsaInfo, i1Tos);

                if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than relinquished interval */
                    if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId) &&
                        (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
                    {
                        gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                        OSPFRtcRelinquish (pOspfCxt);
                    }
                }

            }
        }
    }

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "AS Ext Routes Calculated\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCalculateExtRoute                                       */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 16.4.                         */
/*                This procedure updates the route to the destination        */
/*                described by the specified LSA.                            */
/*                                                                           */
/* Input        : pCurrRt        : routing table                             */
/*                pLsaInfo       : pointer to the advertisement              */
/*                i1Tos           : type of service                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcCalculateExtRoute (tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, INT1 i1Tos)
{

    UINT1               u1Lock;
    tRtEntry            newRtEntry;
    tRtEntry           *pNewRtEntry = NULL;
    UINT1               u1NewRtEntry = OSPF_ZERO;
    UINT1               u1NewPath = OSPF_ZERO;
    tPath              *pAsbrPath = NULL;
    tPath              *pDestNewPath = NULL;
    tPath              *pFwdAddrPath = NULL;
    tIPADDRMASK         destIpAddrMask;
    tIPADDR             destId;
    tExtLsaLink         extLsaLink;
    tExtLsaLink         lsdbExtLsaLnk, tempExtLsaLnk;
    UINT1               u1IntraAsPathFnd = OSPF_ZERO;
    tIPADDR             FwdAddr;
    tPath              *pDestFwdPath = NULL;
    tPath              *pOldDestRtPath = NULL;
    UINT1               u1AsbrExist = OSPF_FALSE;
    tRtEntry           *pAsbrRtEntry = NULL;
    tRtEntry           *pOldDestRtEntry = NULL;
    tArea              *pArea = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC : RtcCalculateExtRoute \n");

    MEMSET (&newRtEntry, 0, sizeof (tRtEntry));
    pNewRtEntry = &newRtEntry;

    if (RtcIsLsaValidForExtRtCalc (pLsaInfo, i1Tos) == OSPF_FALSE)
    {
        return;
    }
    u1Lock = (pCurrRt == pLsaInfo->pOspfCxt->pOspfRt) ? OSPF_TRUE : OSPF_FALSE;

    OS_MEM_CPY (destIpAddrMask,
                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

    UtilIpAddrMaskCopy (destId, pLsaInfo->lsaId.linkStateId, destIpAddrMask);

    RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, i1Tos, &extLsaLink);

    /* 
     * If no path exists to the ASBR that originated the AS ext LSA, 
     * ignore LSA 
     */
    pAsbrRtEntry = RtcFindRtEntry (pCurrRt, &(pLsaInfo->lsaId.advRtrId),
                                   &(gNullIpAddr), DEST_AS_BOUNDARY);
    if ((pAsbrRtEntry == NULL)
        || (pAsbrRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
    {
        return;
    }
    TMO_SLL_Scan (&(pAsbrRtEntry->aTosPath[(INT2) TOS_0]), pAsbrPath, tPath *)
    {
        pArea = GetFindAreaInCxt (pLsaInfo->pOspfCxt, &(pAsbrPath->areaId));
        if ((pArea != NULL) && (pArea->u4AreaType != NSSA_AREA))
        {
            u1AsbrExist = OSPF_TRUE;
        }
    }
    if (u1AsbrExist == OSPF_FALSE)
    {
        return;
    }

    if (UtilIpAddrComp (extLsaLink.fwdAddr, gNullIpAddr) == OSPF_EQUAL)
    {
        if (pLsaInfo->pOspfCxt->rfc1583Compatibility == OSPF_ENABLED)
        {
            pFwdAddrPath =
                RtcFindPath (pCurrRt, &(pLsaInfo->lsaId.advRtrId),
                             DEST_AS_BOUNDARY, i1Tos, NULL);
        }
        else
        {
            pFwdAddrPath =
                RtcFindPreferredASBRPath (pCurrRt,
                                          &(pLsaInfo->lsaId.advRtrId), i1Tos);
        }
    }
    else
    {
        pFwdAddrPath = RtlRtLookupInCxt (pLsaInfo->pOspfCxt,
                                         pCurrRt, &(extLsaLink.fwdAddr), i1Tos);
        if (pFwdAddrPath != NULL)
        {
            pArea = GetFindAreaInCxt (pLsaInfo->pOspfCxt,
                                      &(pFwdAddrPath->areaId));
            if ((pArea == NULL) || (pArea->u4AreaType == NSSA_AREA))
            {

                return;
            }
        }
    }

    if (pFwdAddrPath == NULL)
    {

        OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pLsaInfo->pOspfCxt->u4OspfCxtId,
                   "Fwd Addr %x Path Not Present\n",
                   OSPF_CRU_BMC_DWFROMPDU (extLsaLink.fwdAddr));
        return;
    }

    if ((pFwdAddrPath->u1PathType != INTRA_AREA) &&
        (pFwdAddrPath->u1PathType != INTER_AREA))
    {
        return;
    }

    /* look up routing table entry for destination if it exists */
    pOldDestRtEntry =
        RtcFindRtEntry (pCurrRt, &destId, &(destIpAddrMask), DEST_NETWORK);

    /* if routing table entry not present create new one */

    if ((pOldDestRtEntry == NULL)
        || (pOldDestRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
    {
        RtcInitialiseRtEntry (pNewRtEntry, DEST_NETWORK,
                              &destId, &destIpAddrMask, 0);
        u1NewRtEntry = OSPF_NEW_RT_ENTRY;
        u1NewPath = OSPF_NEW_PATH;
    }
    else
    {
        pOldDestRtPath =
            RtcGetRtTosPath (pOldDestRtEntry, i1Tos, OSPF_NEW_PATH);
    }

    /* compare costs of existing entry and new entry */

    if ((pOldDestRtPath == NULL) ||
        ((pOldDestRtPath->u1PathType == TYPE_2_EXT) &&
         (extLsaLink.u1MetricType == TYPE_1_METRIC)))
    {

        /* 
         * if entry not present for required TOS or if new entry is of
         * preferred path type set new path flag 
         */

        u1NewPath = OSPF_NEW_PATH;
    }
    else if ((pOldDestRtPath->u1PathType == INTRA_AREA) ||
             (pOldDestRtPath->u1PathType == INTER_AREA) ||
             ((pOldDestRtPath->u1PathType == TYPE_1_EXT) &&
              (extLsaLink.u1MetricType == TYPE_2_METRIC)))
    {

        /* if existing entry is of preferred path type do nothing */

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "Existing Path Type Preferred\n");

        return;

    }
    else if ((pOldDestRtPath->u1PathType == TYPE_2_EXT) &&
             (extLsaLink.u4Cost != pOldDestRtPath->u4Type2Cost))
    {
        /* both old and new are of path type TYPE_2_EXT */
        if (pLsaInfo->pOspfCxt->rfc1583Compatibility == OSPF_ENABLED)
        {
            if (extLsaLink.u4Cost < pOldDestRtPath->u4Type2Cost)
            {
                u1NewPath = OSPF_NEW_PATH;
            }
        }
        else
        {
            /*If rfc1583Compatibility is disabled, it always preferred
               intra-area non-backbone path (immaterial of cost) */
            if (extLsaLink.u4Cost < pOldDestRtPath->u4Type2Cost)
            {
                u1NewPath = OSPF_NO_NEW_PATH;
            }
            else
            {
                u1NewPath = OSPF_NEW_PATH;
            }
        }
    }
    else
    {
        RtcGetExtLsaLink (pOldDestRtPath->pLsaInfo->pLsa,
                          pOldDestRtPath->pLsaInfo->u2LsaLen,
                          i1Tos, &tempExtLsaLnk);
        IP_ADDR_COPY (FwdAddr, tempExtLsaLnk.fwdAddr);

        if (pLsaInfo->pOspfCxt->rfc1583Compatibility == OSPF_DISABLED)
        {
            if (UtilIpAddrComp (FwdAddr, gNullIpAddr) == OSPF_EQUAL)
            {
                if ((pDestFwdPath = RtcFindPreferredASBRPath (pCurrRt,
                                                              &
                                                              (pOldDestRtPath->
                                                               pLsaInfo->lsaId.
                                                               advRtrId),
                                                              i1Tos)) == NULL)
                {
                    return;
                }
            }
            else
            {
                if ((pDestFwdPath = RtlRtLookupInCxt (pLsaInfo->pOspfCxt,
                                                      pCurrRt, &FwdAddr,
                                                      i1Tos)) == NULL)
                {
                    return;
                }
            }
            u1NewPath = RtcRfc1583DisExtRt (pOldDestRtPath, pDestFwdPath,
                                            pFwdAddrPath, &extLsaLink);
            if (u1NewPath == OSPF_NO_NEW_PATH)
            {
                return;
            }
            else if ((u1NewPath == OSPF_NEW_PATH) ||
                     (u1NewPath == OSPF_ADD_TO_LIST_OF_PATH))
            {
                u1IntraAsPathFnd = OSPF_TRUE;
            }
        }
        /* Get the most preferred entry to Fwd/ASBR in case
         * RFC1583 is enabled 
         * */
        else
        {
            if (UtilIpAddrComp (FwdAddr, gNullIpAddr) == OSPF_EQUAL)
            {
                pDestFwdPath =
                    RtcFindPath (pCurrRt, &(pOldDestRtPath->pLsaInfo->lsaId.
                                            advRtrId),
                                 DEST_AS_BOUNDARY, i1Tos, NULL);
            }
            else
            {
                pDestFwdPath = RtlRtLookupInCxt (pLsaInfo->pOspfCxt,
                                                 pCurrRt, &FwdAddr, i1Tos);
            }
        }
        if ((!u1IntraAsPathFnd) && (pDestFwdPath != NULL))
        {
            if (pOldDestRtPath->u1PathType == TYPE_1_EXT)
            {
                /* Get the Link cost advertised by 
                 * LSA who path is already present 
                 */
                RtcGetExtLsaLink (pOldDestRtPath->pLsaInfo->pLsa,
                                  pOldDestRtPath->pLsaInfo->u2LsaLen,
                                  i1Tos, &lsdbExtLsaLnk);

                /* both old and new are of path type TYPE_1_EXT */

                if ((pFwdAddrPath->u4Cost + extLsaLink.u4Cost) <
                    (pDestFwdPath->u4Cost + lsdbExtLsaLnk.u4Cost))
                {
                    u1NewPath = OSPF_NEW_PATH;
                }
                else if ((pFwdAddrPath->u4Cost + extLsaLink.u4Cost) ==
                         (pDestFwdPath->u4Cost + lsdbExtLsaLnk.u4Cost))
                {
                    u1NewPath = OSPF_REPLACE_PATH;
                }
            }
            else if (extLsaLink.u4Cost == pOldDestRtPath->u4Type2Cost)
            {
                /* Distance to Fwd address needs to be compared */
                if (pFwdAddrPath->u4Cost < pDestFwdPath->u4Cost)
                {
                    u1NewPath = OSPF_NEW_PATH;
                }
                else if (pFwdAddrPath->u4Cost == pDestFwdPath->u4Cost)
                {
                    u1NewPath = OSPF_REPLACE_PATH;
                }
            }
        }
    }
    if (!u1NewPath)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "RT Not Updated\n");
        return;
    }

    pDestNewPath = RtcNewPathForExtRt (pLsaInfo, u1NewPath, pFwdAddrPath,
                                       pOldDestRtPath, &extLsaLink);
    if (pDestNewPath == NULL)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "Path Creation Failed\n");
        return;
    }

    /* add new next hops( the next hops to fwd addr) to pDestNewPath */
    RtcAddNxtHopForExtRt (pLsaInfo, pDestNewPath, pFwdAddrPath, &extLsaLink);

    /* update routing table */

    if (u1NewRtEntry)
    {

        TMO_SLL_Add (&(pNewRtEntry->aTosPath[(INT2) i1Tos]),
                     &(pDestNewPath->nextPath));
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "New RT Entry To Be Added\n");
        RtcProcessOldNewRtEntryChanges (pCurrRt, pOldDestRtEntry, pNewRtEntry,
                                        i1Tos, pArea);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "New RT Entry Added\n");
    }
    else if ((u1NewPath == OSPF_NEW_PATH) || (u1NewPath == OSPF_REPLACE_PATH))
    {
        RtcReplacePathInCxt (pLsaInfo->pOspfCxt, pOldDestRtEntry,
                             pDestNewPath, i1Tos, u1Lock);
        OsixGetSysTime ((tOsixSysTime *) & (pOldDestRtEntry->u4UpdateTime));
    }
    else if (u1NewPath == OSPF_ADD_TO_LIST_OF_PATH)
    {
        OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                   pLsaInfo->pOspfCxt->u4OspfCxtId,
                   "Adding Path To The Specified RT Entry Dstn %x TOS %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pOldDestRtEntry->destId), i1Tos);
        RtcAddPath (pOldDestRtEntry, pDestNewPath, i1Tos, u1Lock);
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "New Pth Added To RT Entry\n");
        OsixGetSysTime ((tOsixSysTime *) & (pOldDestRtEntry->u4UpdateTime));
    }
    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              " :EXIT RtcCalculateExtRoute \n");
    KW_FALSEPOSITIVE_FIX (pDestNewPath);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetExtLsaLink                                           */
/*                                                                           */
/* Description  : Reference : Utility.                                       */
/*                This procedure gets the link information for the specified */
/*                TOS from the AS external LSA.                              */
/*                                                                           */
/* Input        : pLsa              : Pointer to LSA                         */
/*                u2LsaLen          : Length of LSA                          */
/*                i1Tos             : Type Of Service                        */
/*                pExtLsaLink       : Structure to hold external LSA link    */
/*                                    info                                   */
/*                                                                           */
/* Output       : pExtLsaLink       : external LSA link info pointer         */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
RtcGetExtLsaLink (UINT1 *pLsa,
                  UINT2 u2LsaLen, INT1 i1Tos, tExtLsaLink * pExtLsaLink)
{

    UINT1               u1LinkCount;

    /* skip ls header and ip addr mask */
    pLsa += LS_HEADER_SIZE + MAX_IP_ADDR_LEN;

    u1LinkCount =
        (UINT1) ((u2LsaLen -
                  (LS_HEADER_SIZE + MAX_IP_ADDR_LEN)) / EXT_LSA_LINK_SIZE);

    while (u1LinkCount--)
    {

        /* get info from lsa if TOS_0 or desired TOS */
        if ((*pLsa & EXT_LSA_TOS_MASK) == ENCODE_TOS (i1Tos))
        {

            pExtLsaLink->u1MetricType =
                ((*pLsa & EXT_LSA_METRIC_TYPE_MASK) ? TYPE_2_METRIC :
                 TYPE_1_METRIC);
            pExtLsaLink->i1Tos = (*pLsa & EXT_LSA_TOS_MASK);
            pLsa++;
            pExtLsaLink->u4Cost = LGET3BYTE (pLsa);
            LGETSTR (pLsa, (pExtLsaLink->fwdAddr), MAX_IP_ADDR_LEN);
            pExtLsaLink->u4ExtRouteTag = LGET4BYTE (pLsa);
            return OSPF_SUCCESS;
        }
        else
        {
            pLsa += EXT_LSA_LINK_SIZE;
        }
    }
    return OSPF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSearchDatabase                                          */
/*                                                                           */
/* Description  : This procedure searches the area database based on lsa_type*/
/*                and linkStateId. This procedure is called during           */
/*                intra-area route calculation to find router lsa and network*/
/*                lsa corresponding to vertices in the SPF tree.             */
/*                                                                           */
/* Input        : u1LsaType           : type of LSA                          */
/*                pLinkStateId       : link state id                         */
/*                pArea                : AREA for which the inter-area route */
/*                                        calculation is being done          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA info record, if found                       */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tLsaInfo   *
RtcSearchDatabase (UINT1 u1LsaType, tLINKSTATEID * pLinkStateId, tArea * pArea)
{

    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode;

    if (u1LsaType == ROUTER_LSA)
    {

        pLsaInfo =
            LsuSearchDatabase (u1LsaType, pLinkStateId,
                               pLinkStateId, (UINT1 *) NULL, (UINT1 *) pArea);

        return pLsaInfo;

    }
    else if (u1LsaType == NETWORK_LSA)
    {

        TMO_SLL_Scan (&(pArea->networkLsaLst), pLsaNode, tTMO_SLL_NODE *)
        {

            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
            if (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId,
                                *pLinkStateId) == OSPF_EQUAL)
            {

                return (pLsaInfo);
            }
        }

        return NULL;

    }
    else
    {

        return NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCreateRtEntry                                           */
/*                                                                           */
/* Description  : Reference : Utility.                                       */
/*                This procedure creates a new routing table entry and sets  */
/*                the values of the specified fields. The entry is not added */
/*                to the routing table.                                      */
/*                                                                           */
/* Input        : u1DestType         : destination type                      */
/*                pDestId            : destination IP address                */
/*                pIpAddrMask        : IP address mask                       */
/*                options            : options                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to router entry, if successfully created           */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tRtEntry   *
RtcCreateRtEntry (UINT1 u1DestType,
                  tIPADDR * pDestId,
                  tIPADDRMASK * pIpAddrMask, tOPTIONS options)
{

    tRtEntry           *pRtEntry;
    INT1                i1Tos;

    if (RT_ENTRY_ALLOC (pRtEntry) == NULL)
    {
        return NULL;
    }
    TMO_HASH_INIT_NODE (&pRtEntry->nextRtEntryNode);
    pRtEntry->u1DestType = u1DestType;

    if (u1DestType == DEST_NETWORK)
    {
        UtilIpAddrMaskCopy (pRtEntry->destId, *pDestId, *pIpAddrMask);
    }
    else
    {
        IP_ADDR_COPY (pRtEntry->destId, *pDestId);
    }

    pRtEntry->u4IpAddrMask = OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pIpAddrMask);
    pRtEntry->options = options;
    pRtEntry->u4UpdateTime = 0;
    pRtEntry->pNextAlternatePath = NULL;
    pRtEntry->u1IsRmapAffected = OSPF_FALSE;

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
        pRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_ADDED;
        TMO_SLL_Init (&(pRtEntry->aTosPath[(INT2) i1Tos]));
    }

    return pRtEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcInitialiseRtEntry                                       */
/*                                                                           */
/* Description  : Reference : Utility.                                       */
/*                This procedure creates a new routing table entry and sets  */
/*                the values of the specified fields. The entry is not added */
/*                to the routing table.                                      */
/*                                                                           */
/* Input        : pRtEntry           : pointer to the routing entry          */
/*                u1DestType         : destination type                      */
/*                pDestId            : destination IP address                */
/*                pIpAddrMask        : IP address mask                       */
/*                options            : options                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to router entry, if successfully created           */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcInitialiseRtEntry (tRtEntry * pRtEntry, UINT1 u1DestType,
                      tIPADDR * pDestId, tIPADDRMASK * pIpAddrMask,
                      tOPTIONS options)
{

    INT1                i1Tos;

    TMO_HASH_INIT_NODE (&pRtEntry->nextRtEntryNode);
    pRtEntry->u1DestType = u1DestType;

    if (u1DestType == DEST_NETWORK)
    {
        UtilIpAddrMaskCopy (pRtEntry->destId, *pDestId, *pIpAddrMask);
    }
    else
    {
        IP_ADDR_COPY (pRtEntry->destId, *pDestId);
    }

    pRtEntry->u4IpAddrMask = OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pIpAddrMask);
    pRtEntry->options = options;
    pRtEntry->u4UpdateTime = 0;
    pRtEntry->pNextAlternatePath = NULL;

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
        TMO_SLL_Init (&(pRtEntry->aTosPath[(INT2) i1Tos]));
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCreatePath                                              */
/*                                                                           */
/* Description  : Reference : Utility.                                       */
/*                This procedure creates a new path entry and sets the       */
/*                specified values for the appropriate fields. The path is   */
/*                not added to the routing table entry.                      */
/*                                                                           */
/* Input        : pAreaId        : AREA identifier                           */
/*                u1PathType     : type of path                              */
/*                u4Cost          : cost                                     */
/*                u4Type2Cost    : type2 cost                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the path created, if successfully created       */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tPath      *
RtcCreatePath (tAreaId * pAreaId,
               UINT1 u1PathType, UINT4 u4Cost, UINT4 u4Type2Cost)
{

    tPath              *pPath;

    if (PATH_ALLOC (pPath) == NULL)
    {
        return NULL;
    }

    TMO_SLL_Init_Node (&(pPath->nextPath));
    IP_ADDR_COPY (pPath->areaId, *pAreaId);
    pPath->u1PathType = u1PathType;
    pPath->u4Cost = u4Cost;
    pPath->u1Flag = OSPF_NEW_PATH;
    pPath->u4Type2Cost = u4Type2Cost;
    pPath->u1HopCount = 0;
    pPath->u1NextHopIndex = 0;

    OS_MEM_SET (&pPath->aNextHops[0], 0,
                sizeof (tRouteNextHop) * MAX_NEXT_HOPS);

    return pPath;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCandteHashFunc                                          */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : pVertexId      : vertex id                                 */
/*                u1VertType     : type of vertex                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
RtcCandteHashFunc (tVertexId * pVertexId, UINT1 u1VertType)
{

    UINT1               au1Buf[MAX_IP_ADDR_LEN + 1];

    OS_MEM_CPY (au1Buf, pVertexId, MAX_IP_ADDR_LEN);
    au1Buf[MAX_IP_ADDR_LEN] = u1VertType;
    return UtilHashGetValue (CANDTE_HASH_TABLE_SIZE, au1Buf,
                             (MAX_IP_ADDR_LEN + 1));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSearchLsaLinks                                          */
/*                                                                           */
/* Description  : This routine searches for all links whose link id's matches*/
/*                the specified vertex id.                                   */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                pVertexId         : vertex id                              */
/*                i1Tos             : TOS value                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if found                                        */
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT1
RtcSearchLsaLinks (tLsaInfo * pLsaInfo, tVertexId * pVertexId, INT1 i1Tos)
{

    tVertexId           linkId;
    UINT2               u2LinkCount;
    UINT1              *currPtr = NULL;
    UINT1               i1TosCount;
    UINT1               u1LsaTos;

    currPtr = pLsaInfo->pLsa + LS_HEADER_SIZE;

    if (pLsaInfo->lsaId.u1LsaType == ROUTER_LSA)
    {
        /* skip two bytes unused */
        currPtr += 2;
        u2LinkCount = LGET2BYTE (currPtr);

        while (u2LinkCount--)
        {
            /* linkId */
            LGETSTR (currPtr, linkId, MAX_IP_ADDR_LEN);
            /* linkData */
            currPtr += MAX_IP_ADDR_LEN;
            /* link_type */
            currPtr += 1;
            i1TosCount = LGET1BYTE (currPtr);
            /* TOS-0 aMetric  */
            currPtr += 2;

            if (UtilIpAddrComp (*pVertexId, linkId) == OSPF_EQUAL)
            {
                if (i1Tos == TOS_0)
                {
                    return OSPF_TRUE;
                }
                else
                {
                    while (i1TosCount--)
                    {
                        u1LsaTos = LGET1BYTE (currPtr);
                        if (u1LsaTos == ENCODE_TOS (i1Tos))
                        {
                            return OSPF_TRUE;
                        }
                        else
                        {
                            /* skip 1 byte unused */
                            currPtr += 1;
                            /* skip 2 bytes - cost */
                            currPtr += 2;
                        }
                    }
                    return OSPF_FALSE;
                }
            }
            /* non-zero TOS  */
            currPtr += (i1TosCount * 4);
        }
    }
    else if (pLsaInfo->lsaId.u1LsaType == NETWORK_LSA)
    {

        /* skip network mask */
        currPtr += MAX_IP_ADDR_LEN;

        while (currPtr < (pLsaInfo->pLsa + pLsaInfo->u2LsaLen))
        {

            LGETSTR (currPtr, linkId, MAX_IP_ADDR_LEN);
            if (UtilIpAddrComp (*pVertexId, linkId) == OSPF_EQUAL)
            {

                return OSPF_TRUE;
            }
        }
    }
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcChkAndCalAllExtRoutes                                   */
/*                                                                           */
/* Description  : checks if cost or path to an ASBR or to any forwarding     */
/*                address is changed , if so all exterior routes are         */
/*                recalculated else checks if destination is unreachable,if  */
/*                so it calculates exterior routes only for that destination */
/*                                                                           */
/* Input        : pCurrRt           : Pointer to current routing table       */
/*                pLsaInfo          : Pointer to LSA                         */
/*                pDestMask         : Destination ip addr mask               */
/*                pOldRtEntry       : Pointer to Old Route Entry             */
/*                pNewRtEntry       : Pointer to New Route Entry             */
/*                u1DestType        : Destination Type                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcChkAndCalAllExtRoutes (tOspfRt * pCurrRt,
                          tLsaInfo * pLsaInfo, tIPADDRMASK * pDestMask)
{
    UINT4               u4HashKey = 0;
    INT1                i1Tos;
    tExtLsaLink         extLsaLink;
    tTMO_SLL_NODE      *pExtLsaNode;
    tLsaInfo           *pExtLsaInfo;
    tOsDbNode          *pOsDbNode = NULL;
    UINT1               u1CalExtRoutes = OSPF_FALSE;

    TMO_HASH_Scan_Table (pLsaInfo->pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pLsaInfo->pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pExtLsaNode, tTMO_SLL_NODE *)
            {
                pExtLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo,
                                                 nextLsaInfo, pExtLsaNode);

                for (i1Tos = TOS_0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                {
#ifdef TOS_SUPPORT
                    /*  No need see for Non-zero TOS if the Router is not 
                     *  capable of calculating Non-zero routes of T-BIT is 
                     *  not set in the LSA. */
                    if ((i1Tos != TOS_0) &&
                        ((!(pLsaInfo->pOspfCxt->bTosCapability == OSPF_TRUE)) ||
                         ((pLsaInfo->lsaOptions & T_BIT_MASK) != T_BIT_MASK)))
                    {
                        break;
                    }
#endif /* TOS_SUPPORT */

                    if (RtcGetExtLsaLink (pExtLsaInfo->pLsa,
                                          pExtLsaInfo->u2LsaLen, i1Tos,
                                          &extLsaLink) == OSPF_FAILURE)
                    {
                        continue;
                    }

                    if (pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA)
                    {
                        if ((UtilIpAddrComp (extLsaLink.fwdAddr, gNullIpAddr)
                             != OSPF_EQUAL) &&
                            (UtilIpAddrMaskComp (extLsaLink.fwdAddr,
                                                 pLsaInfo->lsaId.linkStateId,
                                                 *pDestMask) == OSPF_EQUAL))
                        {
                            /* RtcCalculateExtRoute (pCurrRt, pExtLsaInfo, i1Tos); */
                            u1CalExtRoutes = OSPF_TRUE;
                            break;
                        }
                    }
                    else
                    {
                        if (UtilIpAddrComp (pExtLsaInfo->lsaId.advRtrId,
                                            pLsaInfo->lsaId.linkStateId) ==
                            OSPF_EQUAL)
                        {
                            /* RtcCalculateExtRoute (pCurrRt, pExtLsaInfo, i1Tos); */
                            u1CalExtRoutes = OSPF_TRUE;
                            break;
                        }
                    }
                }
                if (u1CalExtRoutes == OSPF_TRUE)
                {
                    RtcIncUpdateAllAsExtRouteInCxt (pLsaInfo->pOspfCxt,
                                                    pCurrRt);
                    return;
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcPrfrmReqdActn                                           */
/*                                                                           */
/* Description  : Performs the action specified by the CONSTANT passed as    */
/*                a parameter, on summary lsa's of the area which describes  */
/*                route to the specified destination.                        */
/*                                                                           */
/* Input        : pCurrRt        : routing table                             */
/*                pLsaInfo       : pointer to advertisement                  */
/*                pDestMask      : pointer to destination mask               */
/*                u1Action        : if it has RT_CAL_INTER_AREA_ROUTE        */
/*                                   call RtcCalculateInterAreaRoute         */
/*                                   else if it has EXMN_TRNST_SMMRY_LNKS    */
/*                                   call RtcExmnTrnstAreasSmmryLinks        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcPrfrmReqdActn (tOspfRt * pCurrRt,
                  tLsaInfo * pLsaInfo, tArea * pArea, tIPADDRMASK * pDestMask,
                  UINT1 u1Action)
{
    tLsaInfo           *pLstLsaInfo;
    tIPADDRMASK        *pLstDestMask;
    UINT1               i1Tos;
    tLsaInfo            CurrLsaInfo;

    MEMSET (&CurrLsaInfo, 0, sizeof (tLsaInfo));
    CurrLsaInfo.lsaId.u1LsaType = pLsaInfo->lsaId.u1LsaType;
    IP_ADDR_COPY (CurrLsaInfo.lsaId.linkStateId, pLsaInfo->lsaId.linkStateId);
    pLstLsaInfo =
        (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot, &CurrLsaInfo, NULL);
    while (pLstLsaInfo != NULL)
    {

        pLstDestMask = (tIPADDRMASK *) (pLstLsaInfo->pLsa + LS_HEADER_SIZE);

        /* 
         * consider only that summary lsas which describe the 
         * same destination 
         */
        if (UtilIpAddrComp (pLstLsaInfo->lsaId.linkStateId,
                            pLsaInfo->lsaId.linkStateId) != OSPF_EQUAL)
        {
            return;
        }
        if (UtilIpAddrComp (*pLstDestMask, *pDestMask) == OSPF_EQUAL)
        {

            for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
            {
#ifdef TOS_SUPPORT
                if ((i1Tos != 0)
                    && (pArea->pOspfCxt->bTosCapability == OSPF_FALSE))
                {
                    break;
                }
#endif /* TOS_SUPPORT */
                if (u1Action == RT_CAL_INTER_AREA_ROUTE)
                {
                    RtcCalculateInterAreaRoute (pCurrRt,
                                                pLstLsaInfo,
                                                i1Tos, pLstLsaInfo->pArea);
                }
                else if (u1Action == EXMN_TRNST_SMMRY_LNKS)
                {
                    RtcExmnTrnstAreasSmmryLinks (pCurrRt,
                                                 pLstLsaInfo->pArea, i1Tos);
                }
            }
        }
        /* end of TMO_SLL_Scan */
        pLstLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot, pLstLsaInfo,
                                        NULL);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcIncUpdtRtSmmryLinkLsa                                   */
/*                                                                           */
/* Description  : Incremental updates - summary link advertisements.         */
/*                Reference sec 16.5 RFC 2328.                               */
/*                                                                           */
/* Input        : pCurrRt        : Routing Table                             */
/*                pLsaInfo       : pointer to advertisement                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
RtcIncUpdtRtSmmryLinkLsa (tOspfRt * pCurrRt, tLsaInfo * pLsaInfo)
{

    UINT1               u1DestType;
    UINT1               u1RunRtCalFlag;
    UINT1               u1DelFlag = OSPF_FALSE;
    UINT4               au4PrevCost[OSPF_MAX_METRIC];
    INT1                i1Tos;
    tIPADDRMASK        *pDestMask;
    tRtEntry           *pOldRtEntry = NULL;
    tRtEntry           *pNewRtEntry = NULL;
    tPath              *pOldPath;
    tPath              *pPath;
    tArea              *pArea;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC : RtcIncUpdtRtSmmryLinkLsa \n");
    u1DestType = OSPF_ZERO;
    pDestMask = (tIPADDR *) NULL;
    if (pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA)
    {

        u1DestType = DEST_NETWORK;
        pDestMask = (tIPADDRMASK *) (pLsaInfo->pLsa + LS_HEADER_SIZE);
    }
    else if (pLsaInfo->lsaId.u1LsaType == ASBR_SUM_LSA)
    {

        u1DestType = DEST_AS_BOUNDARY;
        pDestMask = (tIPADDRMASK *) gNullIpAddr;
    }
    if (pDestMask == NULL)
    {
        return;
    }

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {
#ifdef TOS_SUPPORT
        if ((i1Tos != 0) && (pLsaInfo->pOspfCxt->bTosCapability == OSPF_FALSE))
        {
            break;
        }
#endif /* TOS_SUPPORT */
        au4PrevCost[(INT2) i1Tos] = LS_INFINITY_24BIT;
    }

    if ((BACKBONE_ID (pLsaInfo->pArea->areaId) ||
         !(pLsaInfo->pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
        || ((pLsaInfo->pOspfCxt->bAreaBdrRtr == OSPF_TRUE) &&
            !(pLsaInfo->pOspfCxt->pBackbone->u4FullNbrCount > 0)
            && (pLsaInfo->pOspfCxt->u4ABRType != STANDARD_ABR)))
    {
        /* Case 1: */

        pOldRtEntry = RtcFindRtEntry (pCurrRt,
                                      &(pLsaInfo->lsaId.linkStateId),
                                      pDestMask, u1DestType);

        /* check for an inter area route for destination */
        if (pOldRtEntry != NULL &&
            (GET_PATH_TYPE (pOldRtEntry, TOS_0) != INTRA_AREA))
        {

            /* As the entry is saved in pOldRtEntry, delete the RtEntry from the
             * Ospf Routes List and then set the 'u1DelFlag' to OSPF_TRUE, so
             * that, the memorey of 'pOldRtEntry' is freed later.
             */
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "RT Entry To Be Deleted\n");

            RtcDeleteRtEntry (pCurrRt, pOldRtEntry);

            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pLsaInfo->pOspfCxt->u4OspfCxtId, "RT Entry Deleted\n");
            u1DelFlag = OSPF_TRUE;

        }

        /* 
         * perform inter area calculation for all summary lsa's of area 
         * pointed by lsa that describe route to destination 
         */
        if (!(pLsaInfo->pOspfCxt->pBackbone->u4FullNbrCount > 0)
            && (pLsaInfo->pOspfCxt->u4ABRType != STANDARD_ABR))
        {
            TMO_SLL_Scan (&(pLsaInfo->pOspfCxt->areasLst), pArea, tArea *)
            {
                RtcPrfrmReqdActn (pCurrRt, pLsaInfo, pArea,
                                  pDestMask, RT_CAL_INTER_AREA_ROUTE);
            }
        }
        else
        {
            RtcPrfrmReqdActn (pCurrRt, pLsaInfo, pLsaInfo->pArea,
                              pDestMask, RT_CAL_INTER_AREA_ROUTE);
        }

        pNewRtEntry = RtcFindRtEntry (pCurrRt,
                                      &(pLsaInfo->lsaId.linkStateId),
                                      pDestMask, u1DestType);

        if (pNewRtEntry != NULL || pOldRtEntry != NULL)
        {
            RtcProcessRtEntryChangeInCxt (pLsaInfo->pOspfCxt,
                                          pNewRtEntry, pOldRtEntry);
        }

        if (pNewRtEntry == NULL && pOldRtEntry != NULL)
        {
            if (IS_DEST_NETWORK (pOldRtEntry))
            {
                RtmTxRtChngNtf (pLsaInfo->pOspfCxt, pOldRtEntry,
                                RT_ROWSTATUS_MASK);
            }
        }

        /* 
         * if router is area border router & area is attached to one or 
         * more transit area. 
         */
        if (pLsaInfo->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {

            /* 
             * Examine transit areas summary links  for all summary lsa's 
             * of area pArea that describe route to destination 
             */

            TMO_SLL_Scan (&(pLsaInfo->pOspfCxt->areasLst), pArea, tArea *)
            {
                if (pArea->bTransitCapability == OSPF_TRUE)
                {
                    RtcPrfrmReqdActn (pCurrRt, pLsaInfo, pArea,
                                      pDestMask, EXMN_TRNST_SMMRY_LNKS);
                }
            }
        }

    }
    if (IS_AREA_TRANSIT_CAPABLE (pLsaInfo->pArea) &&
        (pLsaInfo->pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
    {
        /* Case 2: */

        pOldRtEntry = RtcFindRtEntry (pCurrRt,
                                      &(pLsaInfo->lsaId.linkStateId),
                                      pDestMask, u1DestType);
        if (pOldRtEntry == NULL)
        {
            pLsaInfo->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
            RtcSetRtTimer (pLsaInfo->pOspfCxt);
            return;
        }

        /* check for an inter area route for destination */
        if (pOldRtEntry != NULL &&
            (GET_PATH_TYPE (pOldRtEntry, TOS_0) != INTRA_AREA))
        {
            for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
            {
#ifdef TOS_SUPPORT
                if ((i1Tos != 0) &&
                    (pLsaInfo->pOspfCxt->bTosCapability == OSPF_FALSE))
                {
                    break;
                }
#endif /* TOS_SUPPORT */

                /* check for any next hop lying in area pointed by lsa */
                TMO_SLL_Scan (&(pOldRtEntry->aTosPath[(INT2) i1Tos]),
                              pOldPath, tPath *)
                {
                    if (UtilIpAddrComp (pOldPath->areaId,
                                        pLsaInfo->pArea->areaId) == OSPF_EQUAL)
                    {
                        au4PrevCost[(INT2) i1Tos] = pOldPath->u4Cost;

                        /* If only one Path is remaining, then the Route Entry
                         * can be deleted from the Routes list.
                         */

                        if ((TMO_SLL_Count
                             (&(pOldRtEntry->aTosPath[(INT2) i1Tos]))) == 1)
                        {
                            /* As the entry is saved in pOldRtEntry, delete
                             * the RtEntry from the Ospf Routes List and then
                             * set the 'u1DelFlag' to OSPF_TRUE, so
                             * that, the memorey of 'pOldRtEntry' is
                             * freed later.
                             */
                            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                                      pLsaInfo->pOspfCxt->u4OspfCxtId,
                                      "RT Entry To Be Deleted\n");

                            RtcDeleteRtEntry (pCurrRt, pOldRtEntry);

                            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                                      pLsaInfo->pOspfCxt->u4OspfCxtId,
                                      "RT Entry Deleted\n");
                            u1DelFlag = OSPF_TRUE;
                            break;
                        }
                        else
                        {
                            TMO_SLL_Delete (&
                                            (pOldRtEntry->aTosPath
                                             [(INT2) i1Tos]),
                                            &pOldPath->nextPath);
                        }
                    }
                }
            }
            /* endif for check of inter area route */
        }

        /* 
         * Examine transit areas summary links  for all summary lsa's
         * of area pArea that describe route to destination 
         */
        RtcPrfrmReqdActn (pCurrRt, pLsaInfo, pLsaInfo->pArea, pDestMask,
                          EXMN_TRNST_SMMRY_LNKS);

        pNewRtEntry = RtcFindRtEntry (pCurrRt,
                                      &(pLsaInfo->lsaId.linkStateId),
                                      pDestMask, u1DestType);

        if (pNewRtEntry != NULL)
        {
            u1RunRtCalFlag = OSPF_FALSE;
            for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
            {
#ifdef TOS_SUPPORT
                if ((i1Tos != 0) &&
                    (pLsaInfo->pOspfCxt->bTosCapability == OSPF_FALSE))
                {
                    break;
                }
#endif /* TOS_SUPPORT */

                /* check for any next hop lying in area pointed by lsa */
                TMO_SLL_Scan (&(pNewRtEntry->aTosPath[(INT2) i1Tos]),
                              pPath, tPath *)
                {
                    if (UtilIpAddrComp (pPath->areaId,
                                        pLsaInfo->pArea->areaId) == OSPF_EQUAL)
                    {
                        if (pPath->u4Cost > au4PrevCost[(INT2) i1Tos])
                        {
                            u1RunRtCalFlag = OSPF_TRUE;
                            /* break from TMO_SLL_Scan */
                            break;
                        }
                    }
                }
                if (u1RunRtCalFlag == OSPF_TRUE)
                {
                    /* break from for loop */
                    break;
                }
            }
            if (u1RunRtCalFlag == OSPF_TRUE)
            {

                pLsaInfo->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
                RtcSetRtTimer (pLsaInfo->pOspfCxt);
                return;
            }
        }                        /* endif for pNewRtEntry != NULL */
    }                            /* endif for if elseif for Case :1 & Case :2 */

    if ((pOldRtEntry != NULL) || (pNewRtEntry != NULL))
    {
        RtcChkAndCalAllExtRoutes (pCurrRt, pLsaInfo, pDestMask);
    }

    if (u1DelFlag != OSPF_FALSE)
    {
        if (pOldRtEntry != NULL)
        {
            RtcFreeRtEntry (pOldRtEntry);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT : RtcIncUpdtRtSmmryLinkLsa \n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcIncUpdateAllAsExtRoute                                  */
/*                                                                           */
/* Description  : Updates the AS external route with the specified LSA info. */
/*                                                                           */
/* Input        : pCurrRt           : routing table                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
RtcIncUpdateAllAsExtRouteInCxt (tOspfCxt * pOspfCxt, tOspfRt * pCurrRt)
{
    UINT4               u4HashKey = 0;
    tOsDbNode          *pOsDbNode = NULL;

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            RtcIncUpdateExtRouteForNetworkInCxt (pOspfCxt, pCurrRt, pOsDbNode);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCopyAddrRngActvFlg                                      */
/*                                                                           */
/* Description  : Copies the address range active flags value.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcCopyAddrRngActvFlgInCxt (tOspfCxt * pOspfCxt)
{

    tArea              *pArea;
    UINT1               u1Index;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* LSA cannot be generated/flushed in standby node */
        return;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {
            pArea->aAddrRange[u1Index].u1Active =
                pArea->aAddrRange[u1Index].u1CalActive;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcResetAddrRngActvFlg                                     */
/*                                                                           */
/* Description  : Resets the address range active flags value.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcResetAddrRngActvFlgInCxt (tOspfCxt * pOspfCxt)
{

    tArea              *pArea;
    UINT1               u1Index;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* LSA cannot be generated/flushed in standby node */
        return;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        AreaResetAggFlag (pArea, OSPF_TRUE);
        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {
            pArea->aAddrRange[u1Index].u1CalActive = OSPF_FALSE;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSetAddrRngActvFlgInCxt                                  */
/*                                                                           */
/* Description  : Resets the address range active flags value.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcSetAddrRngActvFlgInCxt (tOspfCxt * pOspfCxt, tIPADDR * pIpAddr,
                           tAreaId * pAreaId)
{

    tAddrRange         *pAddrRange;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* LSA cannot be generated/flushed in standby node */
        return;
    }

    if ((pAddrRange =
         UtilFindAssoAreaAddrRangeInCxt (pOspfCxt, pIpAddr, pAreaId)) != NULL)
    {

        pAddrRange->u1CalActive = OSPF_TRUE;

        return;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSearchCandteLst                                         */
/*                                                                           */
/* Description  : Searches the candate list & returns the particular         */
/*                candidate node.                                            */
/*                                                                           */
/* Input        : pVertexId     : vertex id.                                 */
/*              : u1VertType    : vertex type.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to candidate node , if found.                      */
/*              : Null , otherwise.                                          */
/*                                                                           */
/*****************************************************************************/

PRIVATE tCandteNode *
RtcSearchCandteLstInCxt (tOspfCxt * pOspfCxt,
                         tVertexId * pVertexId, UINT1 u1VertType)
{

    tCandteNode        *pCandteNode;

    TMO_HASH_Scan_Bucket (pOspfCxt->pCandteLst,
                          RtcCandteHashFunc (pVertexId, u1VertType),
                          pCandteNode, tCandteNode *)
    {
        if (UtilIpAddrComp (pCandteNode->vertexId, *pVertexId) == OSPF_EQUAL)
        {
            return pCandteNode;
        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSearchSpfTree                                           */
/*                                                                           */
/* Description  : Searches the spf_tree & returns the particular             */
/*                candidate node.                                            */
/*                                                                           */
/* Input        :pSpfTree      : Spf Tree.                                   */
/*              :pVertexId     : vertex id.                                  */
/*              : u1VertType    : vertex type.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to candidate node , if found.                      */
/*              : Null , otherwise.                                          */
/*                                                                           */
/*****************************************************************************/

PRIVATE tCandteNode *
RtcSearchSpfTree (tSpf * pSpfTree, tVertexId * pVertexId, UINT1 u1VertType)
{

    tCandteNode        *pCandteNode;

    TMO_HASH_Scan_Bucket (pSpfTree,
                          RtcCandteHashFunc (pVertexId, u1VertType),
                          pCandteNode, tCandteNode *)
    {
        if (UtilIpAddrComp (pCandteNode->vertexId, *pVertexId) == OSPF_EQUAL)
        {
            return pCandteNode;
        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetNearestCandteNode                                    */
/*                                                                           */
/* Description  : This function gets the nearest candate node.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE tCandteNode *
RtcGetNearestCandteNodeInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4HashKey;
    tTMO_SLL_NODE      *pCandteNode = NULL;

    TMO_HASH_Scan_Table (pOspfCxt->pCandteLst2, u4HashKey)
    {
        pCandteNode =
            TMO_HASH_Get_First_Bucket_Node (pOspfCxt->pCandteLst2, u4HashKey);
        if (pCandteNode != NULL)
        {
            return (tCandteNode *) (VOID *) ((UINT1 *) pCandteNode -
                                             OSPF_OFFSET (tCandteNode,
                                                          candteHashNode2));
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcAddRtEntry                                              */
/*                                                                           */
/* Description  : Adds the specified routing entry to the routing table.     */
/*                                                                           */
/* Input        : pCurrRt         : routing table                            */
/*                pRtEntry        : entry to be added                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcAddRtEntry (tOspfRt * pCurrRt, tRtEntry * pRtEntry)
{

    UINT4               au4Index[2];
    tInputParams        inParams;
    tOutputParams       outParams;
    tPath              *pCurrPath;
    UINT1               u1HopIndex = 0;

    if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
    {
        return;
    }

    /* Always the pRtEntry->u4NextHop is given as zero, as this NextHop is not
     * used in Route Calculation, but used only while deleting a entry from
     * TRIE
     */

    pRtEntry->u4NxtHop = OSPF_CRU_BMC_DWFROMPDU (gNullIpAddr);

    pRtEntry->u4RtIfIndx = pCurrPath->aNextHops[u1HopIndex].u4IfIndex;
    pRtEntry->u4Metric = pCurrPath->u4Cost;
    pRtEntry->u4Tos = TOS_0;
    pRtEntry->u4RtAge = pRtEntry->u4UpdateTime;
    pRtEntry->u2RtProto = OSPF_ID;
    pRtEntry->u4RtNxtHopAs = 0;

    /* If the Next Hop of the very first Path is NULL, then the route is a
     * Directly reachable route by the Router, otherwise it is a indirect Route
     * reachabality
     */

    if (IS_EQUAL_ADDR (pCurrPath->aNextHops[0].nbrIpAddr, gNullIpAddr))
    {
        pRtEntry->u2RtType = DIRECT_ROUTE;
    }
    else
    {
        pRtEntry->u2RtType = INDIRECT_ROUTE;
    }
    pRtEntry->u4RowStatus = ACTIVE;

    if (IS_DEST_NETWORK (pRtEntry))
    {
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = UtilFindMaskLen (pRtEntry->u4IpAddrMask);
        inParams.pRoot = pCurrRt->pOspfRoot;
        inParams.i1AppId = pCurrRt->i1OspfId;
        au4Index[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId));
        au4Index[1] = CRU_HTONL (pRtEntry->u4IpAddrMask);
        inParams.Key.pKey = (UINT1 *) au4Index;

        outParams.pAppSpecInfo = NULL;
        outParams.Key.pKey = NULL;

        if ((TrieAdd (&inParams, (VOID *) pRtEntry, &outParams)) == SUCCESS)
        {
            OsixGetSysTime ((tOsixSysTime *) & (pRtEntry->u4UpdateTime));
            TMO_SLL_Add (&(pCurrRt->routesList), &(pRtEntry->nextRtEntryNode));

        }
    }
    else
    {
        OsixGetSysTime ((tOsixSysTime *) & (pRtEntry->u4UpdateTime));
        RtcAddToSortRtLst (pCurrRt, pRtEntry);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcAddPath                                                 */
/*                                                                           */
/* Description  : Adds the specified path to the specified routing table     */
/*                entry.                                                     */
/*                                                                           */
/* Input        : pRtEntry       : routing table entry that is to be         */
/*                                   updated                                 */
/*                pPath           : path to be added                         */
/*                i1Tos           : type of service                          */
/*                u1Lock          : lock                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcAddPath (tRtEntry * pRtEntry, tPath * pPath, INT1 i1Tos, UINT1 u1Lock)
{

    if (u1Lock == OSPF_TRUE)
    {
        OSPF_TASK_LOCK ();
    }
    TMO_SLL_Add (&(pRtEntry->aTosPath[(INT2) i1Tos]), &(pPath->nextPath));
    OsixGetSysTime ((tOsixSysTime *) & (pRtEntry->u4UpdateTime));

    if (u1Lock == OSPF_TRUE)
    {
        OSPF_TASK_UNLOCK ();
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcReplacePath                                             */
/*                                                                           */
/* Description  : This routine clears the existing path for the specified    */
/*                router entry and adds the new specified path.              */
/*                                                                           */
/* Input        : pRtEntry        : routing table entry that is to be        */
/*                                   updated                                 */
/*                pPath           : path to be added                         */
/*                i1Tos           : type of service                          */
/*                u1Lock          : lock                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcReplacePathInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry, tPath * pPath,
                     INT1 i1Tos, UINT1 u1Lock)
{
    tPath              *pOldPath;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pOspfCxt);

    OSPF_TRC2 (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
               "Replacing Path To The Specified RT Entry Dstn %x TOS %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId), i1Tos);

    if (u1Lock == OSPF_TRUE)
    {
        OSPF_TASK_LOCK ();
    }
    if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_ADDED)
    {
        if ((pOldPath = GET_TOS_PATH (pRtEntry, i1Tos)) != NULL)
        {
            TMO_SLL_Delete (&(pRtEntry->aTosPath[(INT2) i1Tos]),
                            &pOldPath->nextPath);
            PATH_FREE (pOldPath);
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                      "Old Path Freed\n");
        }

        pPath->u1Flag = OSPF_NEW_PATH;
        TMO_SLL_Add (&(pRtEntry->aTosPath[(INT2) i1Tos]), &pPath->nextPath);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "New Path Added\n");

    }
    else if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_MODIFIED)
    {
        if ((pOldPath =
             RtcGetRtTosPath (pRtEntry, i1Tos, OSPF_NEW_PATH)) != NULL)
        {
            TMO_SLL_Delete (&(pRtEntry->aTosPath[(INT2) i1Tos]),
                            &pOldPath->nextPath);
            PATH_FREE (pOldPath);
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                      "Old Path Freed\n");
        }

        pPath->u1Flag = OSPF_NEW_PATH;
        TMO_SLL_Add (&(pRtEntry->aTosPath[(INT2) i1Tos]), &pPath->nextPath);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "New Path Added\n");

    }
    else if ((pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED) ||
             (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_USED))
    {
        if ((pOldPath = GET_TOS_PATH (pRtEntry, i1Tos)) != NULL)
        {
            if (pOldPath->u1Flag == OSPF_NEW_PATH)
            {
                pOldPath->u1Flag = OSPF_OLD_PATH;
            }
        }

        pPath->u1Flag = OSPF_NEW_PATH;
        TMO_SLL_Add (&(pRtEntry->aTosPath[(INT2) i1Tos]), &pPath->nextPath);
        pRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_MODIFIED;
    }
    else
    {
        /* Freeing pPath as it is not added into any list */
        PATH_FREE (pPath);
    }

    OsixGetSysTime ((tOsixSysTime *) & (pRtEntry->u4UpdateTime));

    if (u1Lock == OSPF_TRUE)
    {
        OSPF_TASK_UNLOCK ();
    }
    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "Path Replaced\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeleteRtEntry                                           */
/*                                                                           */
/* Description  : This routine deletes the specified routing entry from the  */
/*                routing table.                                             */
/*                                                                           */
/* Input        : pCurrRt         : routing table                            */
/*                pRtEntry        : entry to be deleted                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcDeleteRtEntry (tOspfRt * pCurrRt, tRtEntry * pRtEntry)
{

    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4Key[2];
    UINT4               u4NextHop;

    if (IS_DEST_NETWORK (pRtEntry))
    {
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        inParams.pRoot = pCurrRt->pOspfRoot;
        inParams.i1AppId = pCurrRt->i1OspfId;
        au4Key[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId));
        au4Key[1] = CRU_HTONL (pRtEntry->u4IpAddrMask);
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.u1Reserved1 = (UINT1) pRtEntry->u2RtType;

        outParams.pAppSpecInfo = NULL;
        outParams.Key.pKey = NULL;

        /* While deleting entry from TRIE, Next Hop is not necessary, hence,
         * always the pRtEntry->u4NextHop is given as the Next Hop which is
         * always zero.
         */

        u4NextHop = pRtEntry->u4NxtHop;

        /* Deleting the Route entry from the Trie data structure */
        if (TrieRemove (&inParams, &outParams, &u4NextHop) == TRIE_SUCCESS)
        {
            /* Once the route entry is removed from the Trie, it has to be
             * removed from the Local SLL routeList also.                */
            TMO_SLL_Delete (&(pCurrRt->routesList),
                            &(pRtEntry->nextRtEntryNode));
        }
    }
    else
    {
        TMO_SLL_Delete (&(pCurrRt->routesList), &(pRtEntry->nextRtEntryNode));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFreeRtEntry                                             */
/*                                                                           */
/* Description  : This routine frees the specified routing entry.            */
/*                                                                           */
/* Input        : pRtEntry       : entry to be freed                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
RtcFreeRtEntry (tRtEntry * pRtEntry)
{

    tPath              *pPath;
    INT1                i1Tos;
    tTMO_SLL           *pLst;

    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
    {

        pLst = &(pRtEntry->aTosPath[(INT2) i1Tos]);
        while ((pPath = (tPath *) TMO_SLL_Get (pLst)) != NULL)
        {
            PATH_FREE (pPath);
        }
    }
    RT_ENTRY_FREE (pRtEntry);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFindRtEntry                                             */
/*                                                                           */
/* Description  : This procedure finds the routing table corresponding to the*/
/*                destination and specified dest_type.Used during routing    */
/*                table calculation.                                         */
/*                                                                           */
/* Input        : pCurrRt            : routing table                         */
/*                pDest               : IP address                           */
/*                pDestMask          : IP address mask                       */
/*                u1DestType         : DEST_NETWORK/DEST_AREA_BORDER/        */
/*                                       DEST_AS_BOUNDARY                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to routing entry, if found                         */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tRtEntry    *
RtcFindRtEntry (tOspfRt * pCurrRt,
                tIPADDR * pDest, tIPADDRMASK * pDestMask, UINT1 u1DestType)
{

    UINT4               au4Index[2];
    tInputParams        inParams;
    tOutputParams       outParams;
    tRtEntry           *pRtEntry;
    tIPADDR             maskedAddr;
    VOID               *pTemp = NULL;

    if ((u1DestType == DEST_AREA_BORDER) || (u1DestType == DEST_AS_BOUNDARY))
    {
        IP_ADDR_COPY (maskedAddr, *pDest);
        TMO_SLL_Scan (&(pCurrRt->routesList), pRtEntry, tRtEntry *)
        {
            if (IS_DEST_NETWORK (pRtEntry) ||
                (UtilIpAddrComp (pRtEntry->destId, maskedAddr) == OSPF_GREATER))
            {
                break;
            }
            if ((UtilIpAddrComp (pRtEntry->destId, maskedAddr) == OSPF_EQUAL)
                && (pRtEntry->u1DestType == u1DestType))
            {
                return pRtEntry;
            }
        }
    }
    else
    {
        UtilIpAddrMaskCopy (maskedAddr, *pDest, *pDestMask);
        au4Index[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (maskedAddr));
        au4Index[1] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (*pDestMask));
        inParams.pRoot = pCurrRt->pOspfRoot;
        inParams.pLeafNode = NULL;
        inParams.i1AppId = pCurrRt->i1OspfId;
        inParams.u1PrefixLen = 0;
        inParams.Key.pKey = (UINT1 *) au4Index;
        outParams.pAppSpecInfo = NULL;
        outParams.Key.pKey = NULL;

        if ((TrieSearch (&inParams, &outParams, (VOID **) &pTemp)) == SUCCESS)
        {
            pRtEntry = ((tRtEntry *) outParams.pAppSpecInfo);

            if (UtilIpAddrComp (((tRtEntry *) outParams.pAppSpecInfo)->destId,
                                maskedAddr) == OSPF_EQUAL)
            {
                return pRtEntry;
            }
        }
    }
    /* Returs NULL if no match is found */
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFindRtEntry                                             */
/*                                                                           */
/* Description  : This procedure finds the Old ABR/ASBR path                 */
/*                                                                           */
/* Input        : pCurrRt            : routing table                         */
/*                pDest               : IP address                           */
/*                u1DestType         : DEST_AREA_BORDER/                     */
/*                                       DEST_AS_BOUNDARY                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to routing entry, if found                         */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tRtEntry   *
RtcFindOldAbrAsbrRt (tOspfRt * pCurrRt, tIPADDR * pDest, UINT1 u1DestType)
{

    tRtEntry           *pRtEntry;
    tIPADDR             maskedAddr;

    if ((u1DestType == DEST_AREA_BORDER) || (u1DestType == DEST_AS_BOUNDARY))
    {
        IP_ADDR_COPY (maskedAddr, *pDest);
        TMO_SLL_Scan (&(pCurrRt->oldAbrAsbrRtList), pRtEntry, tRtEntry *)
        {
            if ((UtilIpAddrComp (pRtEntry->destId, maskedAddr) == OSPF_EQUAL)
                && (pRtEntry->u1DestType == u1DestType))
            {
                return pRtEntry;
            }
        }
    }

    /* Returns NULL if no match is found */
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFindPath                                                */
/*                                                                           */
/* Description  : This procedure finds the routing table corresponding to the*/
/*                destination and returns the path of the specified TOS and  */
/*                associated area. The destination should be a router i.e.   */
/*                AREA BORDER / AS BOUNDARY.                                 */
/*                                                                           */
/* Input        : pCurrRt          : routing table                           */
/*                pDest             : destination IP address                 */
/*                u1DestType       : IP address mask                         */
/*                i1Tos             : type of service                        */
/*                p-area             : pointer to AREA                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to path, if found                                  */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tPath       *
RtcFindPath (tOspfRt * pCurrRt,
             tIPADDR * pDest, UINT1 u1DestType, INT1 i1Tos, tArea * pArea)
{

    tRtEntry           *pRtEntry;
    tPath              *pPath = NULL;
    tPath              *pLstPath = NULL;
    tAreaId             areaId;
    UINT4               u4BestCost = LS_INFINITY_24BIT;

    IP_ADDR_COPY (areaId, gNullIpAddr);

    /* destination should be a router */

    if ((u1DestType != DEST_AREA_BORDER) && (u1DestType != DEST_AS_BOUNDARY))
    {

        return NULL;
    }

    pRtEntry = RtcFindRtEntry (pCurrRt, pDest, &gNullIpAddr, u1DestType);
    if ((pRtEntry != NULL) && (pRtEntry->au1Flag[i1Tos] != OSPF_ROUTE_UNUSED))
    {

        /* if pArea is null return first path in required TOS */

        if (pArea == NULL)
        {
            if (u1DestType == DEST_AS_BOUNDARY)
            {
                /* From multiple paths present for ASBR,
                 * get the least cost path
                 * If multiple least cost path, get one
                 * associated with highest Area Id
                 */
                TMO_SLL_Scan (&(pRtEntry->aTosPath[(INT2) i1Tos]),
                              pLstPath, tPath *)
                {
                    if (pLstPath->u1Flag == OSPF_OLD_PATH)
                    {
                        /* This Path needs to be deleted */
                        continue;
                    }
                    if (pLstPath->u1PathType == INTER_AREA)
                    {
                        return pLstPath;
                    }

                    if (pLstPath->u4Cost < u4BestCost)
                    {
                        pPath = pLstPath;
                        u4BestCost = pLstPath->u4Cost;
                        IP_ADDR_COPY (areaId, pLstPath->areaId);

                    }
                    else if (pLstPath->u4Cost == u4BestCost)
                    {
                        if (UtilIpAddrComp (pLstPath->areaId,
                                            areaId) == OSPF_GREATER)
                        {
                            pPath = pLstPath;
                            IP_ADDR_COPY (areaId, pLstPath->areaId);
                        }
                    }
                }                /* Scan */
                return pPath;
            }
            else
                return (RtcGetRtTosPath (pRtEntry, i1Tos, OSPF_NEW_PATH));
        }

        /* if pArea is non_null find path with pArea as associated area */

        TMO_SLL_Scan (&(pRtEntry->aTosPath[(INT2) i1Tos]), pPath, tPath *)
        {
            if (pPath->u1Flag == OSPF_OLD_PATH)
            {
                /* This Path needs to be deleted */
                continue;
            }

            if (UtilIpAddrComp (pArea->areaId, pPath->areaId) == OSPF_EQUAL)
            {

                return pPath;
            }
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessUnreachableRouteInCxt                            */
/*                                                                           */
/* Description  : This routine flushes all the unreachable route summaries.  */
/*                                                                           */
/* Input        : pOspfCxt       : context pointer                           */
/*                pRtEntry       : route whose summaries are to be flushed   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcProcessUnreachableRouteInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry)
{

    tArea              *pArea;
    tPath              *pPath;
    tPath              *pNssaPath;
    tArea              *pNssaArea;
    tExtRoute          *pExtRoute;

    tIPADDRMASK         destIpAddrMask;

    /* skip routes to area border routers */
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : RtcProcessUnreachableRouteInCxt \n");
    if (IS_DEST_ABR (pRtEntry))
    {
        if (pRtEntry->au1Flag[TOS_0] == OSPF_ROUTE_MODIFIED)
        {
            RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry, IFE_DOWN,
                                        OSPF_OLD_PATH);
        }
        else
        {
            RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry, IFE_DOWN,
                                        OSPF_NEW_PATH);
        }

        TMO_SLL_Scan (&(pRtEntry->aTosPath[(INT2) TOS_0]), pPath, tPath *)
        {
            pArea = GetFindAreaInCxt (pOspfCxt, &(pPath->areaId));
            if (pArea != NULL)
            {
                pArea->u4AbrStatFlg = OSPF_TRUE;
            }
        }
        return;
    }
    if (IS_DEST_ASBR (pRtEntry))
    {
        pOspfCxt->u4AsbrRtChange = OSPF_TRUE;
    }

    /* consider only inter-area and intra-area paths */

    if ((GET_PATH_TYPE (pRtEntry, TOS_0) == TYPE_1_EXT) ||
        (GET_PATH_TYPE (pRtEntry, TOS_0) == TYPE_2_EXT))
    {
        /* Handling when Type 7 route is lost and LSA was translated to     */
        /* Type 5                                               */

        if ((pNssaPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
        {
            return;
        }

        if ((pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
            (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
        {
            /* LSA cannot be Flushed in GR mode 
             * or in standby node */
            return;
        }
        if ((pNssaPath->pLsaInfo->lsaId.u1LsaType == NSSA_LSA) ||
            (pNssaPath->pLsaInfo->lsaId.u1LsaType == COND_NSSA_LSA) ||
            (pNssaPath->pLsaInfo->lsaId.u1LsaType == DEFAULT_NSSA_LSA))
        {
            pNssaArea = GetFindAreaInCxt (pOspfCxt, &(pNssaPath->areaId));
            if ((pNssaArea != NULL) && (pNssaArea->u4AreaType == NSSA_AREA))
            {
                if (!IS_MAX_AGE (pNssaPath->pLsaInfo->u2LsaAge))
                {
                    pNssaPath->pLsaInfo->u1TrnsltType5 = FLUSHED_LSA;
                    NssaType7LsaTranslation (pNssaArea, pNssaPath->pLsaInfo);
                    NssaAdvertiseType7Rngs (pNssaArea);
                }
                /* Functionally equivalent TYPE 7 LSA                 */
                /* If router has entry for same network in Ext Rt         */
                /* Table, it meant it previously flushed its Type7 LSA    */
                /* because of being less preferred. That Type 7 LSA   */
                /* is now re-originated                           */

                OS_MEM_CPY (destIpAddrMask,
                            (UINT1 *) (pNssaPath->pLsaInfo->pLsa +
                                       LS_HEADER_SIZE), MAX_IP_ADDR_LEN);
                if ((pExtRoute =
                     GetFindExtRouteInCxt
                     (pOspfCxt, &(pNssaPath->pLsaInfo->lsaId.linkStateId),
                      &(destIpAddrMask))) != NULL)
                    RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);
            }
        }
        return;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        /* if associated area is the same area then skip route */
        if (UtilIpAddrComp (GET_PATH_AREA_ID (pRtEntry), pArea->areaId)
            == OSPF_EQUAL)
            continue;

        /* if destination is AS boundary router generate type 4 lsa */
        if (IS_DEST_ASBR (pRtEntry) && pArea->u4AreaType == NORMAL_AREA)
        {
            RtcFlushOutSummaryLsa (pArea, ASBR_SUM_LSA,
                                   &(pRtEntry->destId), pRtEntry->u4IpAddrMask);
        }
        else if ((IS_DEST_NETWORK (pRtEntry)) &&
                 (GET_PATH_TYPE (pRtEntry, TOS_0) == INTER_AREA))
        {
            RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                   &(pRtEntry->destId), pRtEntry->u4IpAddrMask);
        }
        else if ((IS_DEST_NETWORK (pRtEntry)) &&
                 (GET_PATH_TYPE (pRtEntry, TOS_0) == INTRA_AREA))
        {

            /* backbone info is not condensed into  transit areas */

            if (IS_AREA_TRANSIT_CAPABLE (pArea) &&
                (UtilIpAddrComp (GET_PATH_AREA_ID (pRtEntry),
                                 gBackboneAreaId) == OSPF_EQUAL))
            {
                RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                       &(pRtEntry->destId),
                                       pRtEntry->u4IpAddrMask);
            }
            else
            {

                if (UtilFindAssoAreaAddrRangeInCxt (pOspfCxt,
                                                    &(pRtEntry->destId),
                                                    &(GET_PATH_AREA_ID
                                                      (pRtEntry))) == NULL)
                {
                    RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                           &(pRtEntry->destId),
                                           pRtEntry->u4IpAddrMask);

                }
            }
        }

    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : RtcProcessUnreachableRouteInCxt \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFlushOutSummaryLsa                                      */
/*                                                                           */
/* Description  : This routine gets the LSA info based on LSA type and then  */
/*                flushes the LSA.                                           */
/*                                                                           */
/* Input        : pArea                : pointer to AREA                     */
/*                u1LsaType           : type of LSA                          */
/*                pLinkStateId       : IP address                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
RtcFlushOutSummaryLsa (tArea * pArea,
                       UINT1 u1LsaType, tLINKSTATEID * pLinkStateId,
                       UINT4 u4IpAddrMask)
{

    tLsaInfo           *pLsaInfo;
    tIPADDR             ipAddrMask;

    if ((pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        /* LSA cannot be regenerated in GR mode
         * or in standby node */
        return;
    }
    u1LsaType =
        ((u1LsaType == DEFAULT_NETWORK_SUM_LSA) ||
         (u1LsaType == COND_NETWORK_SUM_LSA)) ? NETWORK_SUM_LSA : u1LsaType;
    OSPF_CRU_BMC_DWTOPDU (ipAddrMask, u4IpAddrMask);
    if (u1LsaType == NETWORK_SUM_LSA)
    {
        pLsaInfo = LsuDatabaseLookUp (u1LsaType, pLinkStateId,
                                      &(ipAddrMask), &(pArea->pOspfCxt->rtrId),
                                      (UINT1 *) NULL, (UINT1 *) pArea);

    }
    else
    {
        pLsaInfo = LsuSearchDatabase (u1LsaType, pLinkStateId,
                                      &(pArea->pOspfCxt->rtrId), (UINT1 *) NULL,
                                      (UINT1 *) pArea);
    }
    if (pLsaInfo != NULL)
    {

        /* If the lsa is not newly originated flush out
         * only if the LSA is already not in flushed state.
         * This is checked by examining the pLsaInfo->u1LsaFlushed state
         */

        if (pLsaInfo->u1LsaFlushed != OSPF_TRUE)
        {
            if (pLsaInfo->pLsaDesc->u4LsaGenTime !=
                pArea->pOspfCxt->u4LsaGenTime)
            {
                AgdFlushOut (pLsaInfo);
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFindIfId                                                */
/*                                                                           */
/* Description  : This routine gets the interface id for router and network  */
/*                vertices during the first phase of the SPF tree            */
/*                calculation.                                               */
/*                                                                           */
/* Input        : pVertex              : pointer to vertex node              */
/*                u1Type               : type of network                     */
/*                pArea                : pointer to AREA                     */
/*                i1Tos                : TOS value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pInterface, if interface is found                          */
/*                NULL, Otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE tInterface *
RtcFindIfId (tCandteNode * pVertex, UINT1 u1Type, tArea * pArea, INT1 i1Tos)
{

    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    UINT1               u1Count;

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_LST (pLstNode);

        if (pInterface->operStatus != OSPF_ENABLED)
        {
            continue;            /* looks only for enabled interfaces */
        }

        switch (pInterface->u1NetworkType)
        {
            case IF_BROADCAST:
            case IF_NBMA:
            case IF_LOOPBACK:
                if ((u1Type == VERT_NETWORK)
                    && (UtilIpAddrMaskComp (pVertex->vertexId,
                                            pInterface->ifIpAddr,
                                            pInterface->ifIpAddrMask)
                        == OSPF_EQUAL))
                {

                    return pInterface;
                }
                break;

            case IF_PTOP:
            case IF_VIRTUAL:
                if (u1Type == VERT_ROUTER)
                {

                    if ((pNbrNode =
                         TMO_SLL_First (&(pInterface->nbrsInIf))) == NULL)
                    {
                        break;
                    }
                    pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                    if (UtilIpAddrComp (pVertex->vertexId, pNbr->nbrId)
                        == OSPF_EQUAL)
                    {
                        for (u1Count = 0; u1Count <
                             OSPF_MIN ((pVertex->u1HopCount), (MAX_NEXT_HOPS));
                             u1Count++)
                        {
                            if (pInterface->u4AddrlessIf ==
                                pVertex->aNextHops[u1Count].pInterface->
                                u4AddrlessIf)
                            {
                                if ((UtilIpAddrComp
                                     (pVertex->aNextHops[u1Count].pInterface->
                                      ifIpAddr,
                                      pInterface->ifIpAddr)) == OSPF_EQUAL)
                                {
                                    break;
                                }
                            }
                        }

                        if ((u1Count == pVertex->u1HopCount) &&
                            (pInterface->aIfOpCost[i1Tos].
                             u4Value == pVertex->u4Cost))
                        {
                            return pInterface;
                        }
                    }
                }
                break;
            case IF_PTOMP:
                if (u1Type == VERT_ROUTER)
                {
                    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                                  tTMO_SLL_NODE *)
                    {
                        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                        if (UtilIpAddrComp
                            (pVertex->vertexId, pNbr->nbrId) == OSPF_EQUAL)
                        {
                            for (u1Count = 0; u1Count <
                                 OSPF_MIN ((pVertex->u1HopCount),
                                           (MAX_NEXT_HOPS)); u1Count++)
                            {
                                if (pInterface->u4AddrlessIf ==
                                    pVertex->aNextHops[u1Count].pInterface->
                                    u4AddrlessIf)
                                {
                                    if ((UtilIpAddrComp
                                         (pVertex->aNextHops[u1Count].
                                          pInterface->ifIpAddr,
                                          pInterface->ifIpAddr)) == OSPF_EQUAL)
                                    {
                                        break;
                                    }
                                }
                            }

                            if ((u1Count == pVertex->u1HopCount) &&
                                (pInterface->aIfOpCost[i1Tos].
                                 u4Value == pVertex->u4Cost))
                            {
                                return pInterface;
                            }
                        }
                    }
                }
                break;
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeleteExtRoutes                                         */
/*                                                                           */
/* Description  : This routine deletes all the external routes from the      */
/*                routing table.                                             */
/*                                                                           */
/* Input        : pOspfRt : routing table                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcDeleteExtRoutes (tOspfRt * pOspfRt)
{

    tRtEntry           *pRtEntry;
    tRtEntry           *pTempRtEntry = NULL;

    /* Scan the routing table and to remove the Type 1 and Type 2 external
     * path route entries from the current routing table.
     */

    TMO_SLL_Scan (&(pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        if (pTempRtEntry)
        {
            RtcDeleteRtEntry (pOspfRt, pTempRtEntry);
            RtcFreeRtEntry (pTempRtEntry);
            pTempRtEntry = NULL;
        }
        if ((GET_PATH_TYPE (pRtEntry, TOS_0) == TYPE_1_EXT)
            || (GET_PATH_TYPE (pRtEntry, TOS_0) == TYPE_2_EXT))
        {
            pTempRtEntry = pRtEntry;
        }
    }
    if (pTempRtEntry)
    {
        RtcDeleteRtEntry (pOspfRt, pTempRtEntry);
        RtcFreeRtEntry (pTempRtEntry);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcBldSpfTreeTransAreaInCxt                                */
/*                                                                           */
/* Description  : This routine finds the virtual neighbor's ip address       */
/*                by building the SPF tree.                                  */
/*                                                                           */
/* Input        : pOspfCxt      : context pointer                            */
/*                transitAreaId : Transit Area ID                            */
/*                destRtrId     : Virtual Neighbor's Router ID               */
/*                                                                           */
/*                                                                           */
/* Output       :  pIpAddr       : Virtual Neighbor's IP Address             */
/*                                                                           */
/* Returns      : OSPF_TRUE, if the virtual neighbor's ip address is found.  */
/*                OSPF_FALSE, otherwise.                                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
RtcBldSpfTreeTransAreaInCxt (tOspfCxt * pOspfCxt, tAreaId * transitAreaId,
                             tRouterId * destRtrId, tIPADDR * pIpAddr)
{
    INT1                i1Tos = TOS_0;
    tArea              *pArea;
    tLsaInfo           *pLsaInfo;

    tCandteNode        *pNewVertex;
    tLinkNode           linkNode;
    UINT1               u1VertType;
    UINT1              *pCurrPtr;
    tSpf               *pSpfTree;
    tCandteNode        *spRoot;

    if ((pArea = GetFindAreaInCxt (pOspfCxt, transitAreaId)) == NULL)
    {
        return OSPF_FALSE;
    }

    if ((spRoot = RtcGetCandteNode (&(pOspfCxt->rtrId), VERT_ROUTER)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Candidate Alloc Failure\n");
        return OSPF_FALSE;
    }
    else
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "New Candidate Node Created\n");
    }
    if ((spRoot->pLsaInfo = RtcSearchDatabase (ROUTER_LSA,
                                               &(pOspfCxt->rtrId),
                                               pArea)) == NULL)
    {

        CANDTE_FREE (spRoot);

        return OSPF_FALSE;
    }

    spRoot->u4Cost = 0;
    if ((pSpfTree = TMO_HASH_Create_Table (SPF_HASH_TABLE_SIZE,
                                           NULL, FALSE)) == NULL)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Hash table Creation Failure\n");
        CANDTE_FREE (spRoot);
        return OSPF_FALSE;
    }
    TMO_HASH_Add_Node (pSpfTree, &(spRoot->candteHashNode),
                       RtcCandteHashFunc (&(spRoot->vertexId),
                                          VERT_ROUTER), NULL);

    pNewVertex = spRoot;

    do
    {

        /* move to start of links
         * skip mask in network lsa, flags and #links in router lsa
         */

        pCurrPtr = pNewVertex->pLsaInfo->pLsa + LS_HEADER_SIZE + 4;

#ifdef TOS_SUPPORT
        if (((pNewVertex->pLsaInfo->lsaOptions & T_BIT_MASK) != T_BIT_MASK)
            && (i1Tos != TOS_0))
        {

        }
#endif /* TOS_SUPPORT */

        while ((pCurrPtr = RtcGetLinksFromLsa (pNewVertex->pLsaInfo,
                                               i1Tos, pCurrPtr,
                                               &linkNode)) != NULL)
        {

            /* if the new vertex is a router and the link is a transit link 
             * then the next vertex should be a network. Otherwise the next 
             * vertex is a router.
             */

            if ((pNewVertex->u1VertType == VERT_ROUTER)
                && (linkNode.u1LinkType == TRANSIT_LINK))
            {
                u1VertType = VERT_NETWORK;
            }
            else
            {
                u1VertType = VERT_ROUTER;
            }

            /* If stub link add to stub_link_lst. This will be considered
             * in the next phase of routing table calculation.
             */

            if (linkNode.u1LinkType == STUB_LINK)
            {
                /* No need to consider stub links */
                continue;        /* consider next link */
            }

            /* add to candidate list if necessary */

            if ((UtilIpAddrComp (pNewVertex->vertexId, *destRtrId) ==
                 OSPF_EQUAL) && (pNewVertex->u1VertType == VERT_ROUTER))
            {
                if ((pLsaInfo = (tLsaInfo *)
                     RtcSearchDatabase ((UINT1) ((u1VertType == VERT_ROUTER) ?
                                                 ROUTER_LSA : NETWORK_LSA),
                                        &(linkNode.linkId), pArea)) != NULL)
                {
                    if ((RtcSearchLsaLinks (pLsaInfo,
                                            &(pNewVertex->vertexId), i1Tos)) ==
                        OSPF_TRUE)
                    {
                        IP_ADDR_COPY (pIpAddr, linkNode.linkData);

                        RtcClearCandteLstInCxt (pOspfCxt);
                        RtcClearSpfTree (pSpfTree);
                        pSpfTree = NULL;
                        return OSPF_TRUE;
                    }
                }
            }
            if (RtcUpdateCandteLst (spRoot, pSpfTree, pNewVertex, &linkNode,
                                    pArea, i1Tos) == OSPF_FAILURE)
            {
                continue;
            }
        }                        /* while */

        /* if candidate list is empty this phase of algorithm is complete
         * otherwise remove the vertex which is nearest to root from candidate
         * list and add it to the spf tree.
         */

        if ((pNewVertex = RtcGetNearestCandteNodeInCxt (pOspfCxt)) == NULL)
        {

            break;

        }
        RtcDelCandNode (pArea->pOspfCxt, pNewVertex);

        TMO_HASH_Add_Node (pSpfTree, &(pNewVertex->candteHashNode),
                           RtcCandteHashFunc (&(pNewVertex->vertexId),
                                              pNewVertex->u1VertType), NULL);

    }
    while (1);

    RtcClearSpfTree (pSpfTree);
    pSpfTree = NULL;
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFindPreferredASBRPath                                   */
/*                                                                           */
/* Description  : This procedure finds the preferred path to the ASBR. Refer */
/*                section 16.4.1 (rfc 2328)                                  */
/*                                                                           */
/* Input        : pCurrRt     - pointer to the routing table                 */
/*                pDest       - pointer to the ASBR                          */
/*                i1Tos       - type of service                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pPath, if preferred path found                             */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE tPath      *
RtcFindPreferredASBRPath (tOspfRt * pCurrRt, tIPADDR * pDest, INT1 i1Tos)
{
    tRtEntry           *pRtEntry;
    tPath              *pLstPath = NULL;
    tPath              *pCurrBestPath = NULL;
    tPath              *pBestPath = NULL;
    tPath               tmpBestPath;
    UINT1               u1FndIntraAreaNonBkBonePath = OSPF_FALSE;

    tmpBestPath.u4Cost = (UINT4) -1;
    tmpBestPath.u1PathType = INTER_AREA;
    pCurrBestPath = &tmpBestPath;

    pRtEntry = RtcFindRtEntry (pCurrRt, pDest, &(gNullIpAddr),
                               DEST_AS_BOUNDARY);
    if ((pRtEntry == NULL) || (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
    {
        return (NULL);
    }

    TMO_SLL_Scan (&(pRtEntry->aTosPath[(INT2) i1Tos]), pLstPath, tPath *)
    {
        if (pLstPath->u1Flag == OSPF_OLD_PATH)
        {
            continue;
        }

        if ((pLstPath->u1PathType == INTRA_AREA) &&
            (UtilIpAddrComp (pLstPath->areaId, gNullIpAddr) != OSPF_EQUAL))
        {
            /* intra-area non-backbone path */

            /*
             * prefer this entry (immaterial of cost) if the current best 
             * path is inter-area OR backbone area
             */
            if (!((pCurrBestPath->u1PathType == INTRA_AREA) &&
                  (UtilIpAddrComp (pCurrBestPath->areaId,
                                   gNullIpAddr) != OSPF_EQUAL)))
            {
                pCurrBestPath = pLstPath;
                pBestPath = pLstPath;
                u1FndIntraAreaNonBkBonePath = OSPF_TRUE;
                continue;
            }

            /*
             * if costs are equal, the path having greater areaId is preferred
             * otherwise, the path having lower cost is preferred
             */
            if (pCurrBestPath->u4Cost == pLstPath->u4Cost)
            {
                if (UtilIpAddrComp
                    (pLstPath->areaId, pCurrBestPath->areaId) == OSPF_GREATER)
                {
                    pCurrBestPath = pLstPath;
                    pBestPath = pLstPath;
                }
            }
            else if (pCurrBestPath->u4Cost > pLstPath->u4Cost)
            {
                pCurrBestPath = pLstPath;
                pBestPath = pLstPath;
            }

        }
        else
        {

            /* inter-area path OR backbone path */
            if (u1FndIntraAreaNonBkBonePath == OSPF_FALSE)
            {
                if (pCurrBestPath->u4Cost == pLstPath->u4Cost)
                {
                    if (UtilIpAddrComp
                        (pLstPath->areaId,
                         pCurrBestPath->areaId) == OSPF_GREATER)
                    {
                        pCurrBestPath = pLstPath;
                        pBestPath = pLstPath;
                    }
                }
                else if (pCurrBestPath->u4Cost > pLstPath->u4Cost)
                {
                    pCurrBestPath = pLstPath;
                    pBestPath = pLstPath;
                }
            }
        }
    }

    return pBestPath;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcResetVlReachability                                     */
/*                                                                           */
/* Description  : Set the Virtual Reachability flag  of all Virtual          */
/*                Interfaces to OSPF_FALSE.                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcResetVlReachabilityInCxt (tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        pInterface->VlReachability = OSPF_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSetVlReachability                                       */
/*                                                                           */
/* Description  : Set the Virtual Reachability flag  of Virtual              */
/*                Interface to OSPF_TRUE, if the rechability to ABR exists.  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcSetVlReachability (tArea * pArea, tRtEntry * pRtEntry)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    if (!(pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
    {
        return;
    }

    TMO_SLL_Scan (&(pArea->pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((UtilIpAddrComp (pArea->areaId, pInterface->transitAreaId)
             == OSPF_EQUAL) &&
            (UtilIpAddrComp (pRtEntry->destId, pInterface->destRtrId)
             == OSPF_EQUAL))
        {
            pInterface->VlReachability = OSPF_TRUE;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCheckVlReachability                                     */
/*                                                                           */
/* Description  : Disbable the virtual interfaces, if the reachability to    */
/*                ABR on the other side of the virtual link is not there.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcCheckVlReachabilityInCxt (tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tRtEntry           *pRtEntry;
    UINT4               u4CurrentTime = 0;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pInterface->VlReachability == OSPF_FALSE) &&
            (pInterface->u1IsmState == IFS_PTOP))
        {
            OspfIfDown (pInterface);
        }

        if (pOspfCxt->u4ABRType == STANDARD_ABR)
        {
            if ((pInterface->u1IsmState == IFS_DOWN) &&
                (pInterface->VlReachability == OSPF_TRUE))
            {

                pRtEntry = RtcFindRtEntry (pOspfCxt->pOspfRt,
                                           &(pInterface->destRtrId),
                                           &gNullIpAddr, DEST_AREA_BORDER);
                if ((pRtEntry != NULL) &&
                    (pRtEntry->au1Flag[TOS_0] != OSPF_ROUTE_UNUSED))
                {
                    RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry, IFE_UP,
                                                OSPF_NEW_PATH);
                }
            }
        }
        /* Check for relinquish */
        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                && (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
            {
                gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                OSPFRtcRelinquish (pOspfCxt);
            }
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcClearAndDeleteRtFromIpInCxt                             */
/*                                                                           */
/* Description  : Clears the routing table.                                  */
/*                                                                           */
/* Input        : pOspfCxt      : ptr to context                             */
/*              : pOspfRt       : routing table                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcClearAndDeleteRtFromIpInCxt (tOspfCxt * pOspfCxt, tOspfRt * pOspfRt)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    tRtEntry           *pRtEntry;
    UINT4               au4Indx[2];
    tPath              *pCurrPath;
    UINT4               u4NextHop;

    /* Unlinking all the entries from the local SLL list */
    while ((pRtEntry = (tRtEntry *) TMO_SLL_First (&(pOspfRt->routesList)))
           != NULL)
    {
        if (IS_DEST_NETWORK (pRtEntry))
        {
            au4Indx[0] = CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId));
            au4Indx[1] = CRU_HTONL (pRtEntry->u4IpAddrMask);
            inParams.i1AppId = pOspfRt->i1OspfId;
            inParams.pRoot = pOspfRt->pOspfRoot;
            inParams.Key.pKey = (UINT1 *) au4Indx;
            inParams.u1Reserved1 = (UINT1) pRtEntry->u2RtType;

            outParams.pAppSpecInfo = NULL;
            outParams.Key.pKey = NULL;

            /* While deleting entry from TRIE, Next Hop is not necessary, hence,
             * always the pRtEntry->u4NextHop is given as the Next Hop which is
             * always zero.
             */

            u4NextHop = pRtEntry->u4NxtHop;

            if (TrieRemove (&inParams, &outParams, &u4NextHop) == TRIE_SUCCESS)
            {
                TMO_SLL_Delete (&(pOspfRt->routesList),
                                &(pRtEntry->nextRtEntryNode));
                if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) != NULL)
                {
                    RtmTxRtChngNtf (pOspfCxt, pRtEntry, RT_ROWSTATUS_MASK);
                }
                RtcFreeRtEntry (pRtEntry);
            }
            else
            {
                OSPF_GBL_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                              OSPF_INVALID_CXT_ID, "TrieDeleteEntry Failure\n");
            }
        }
        else
        {
            TMO_SLL_Delete (&(pOspfRt->routesList),
                            &(pRtEntry->nextRtEntryNode));
            RtcFreeRtEntry (pRtEntry);
        }
    }
    TMO_SLL_Init (&(pOspfRt->routesList));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcAddToSortRtLst                                          */
/*                                                                           */
/* Description  : Adds the given interface structure to the sort_if_list.    */
/*                                                                           */
/* Input        : tRtEntry        : the new RtEntry to be added to the       */
/*                                     list                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcAddToSortRtLst (tOspfRt * pCurrRt, tRtEntry * pRtEntry)
{
    tRtEntry           *pListRtEntry;
    tRtEntry           *pPrevLstRtEntry = NULL;

    TMO_SLL_Scan (&(pCurrRt->routesList), pListRtEntry, tRtEntry *)
    {
        if ((UtilIpAddrComp (pRtEntry->destId,
                             pListRtEntry->destId) == OSPF_LESS)
            || (pListRtEntry->u1DestType == DEST_NETWORK))
        {
            break;
        }
        pPrevLstRtEntry = pListRtEntry;
    }
    TMO_SLL_Insert (&(pCurrRt->routesList), &(pPrevLstRtEntry->nextRtEntryNode),
                    &(pRtEntry->nextRtEntryNode));
}

/*****************************************************************************
 *
 * Function     : RtcGetPathInArea
 *
 * Description  : Checks the existence of the Path with the given area Id in
 *                the given Route Entry
 *
 * Input        : pRtEntry - Route Entry where the new Path is to be added.
 *                areaId   - AreaId of the Area whose Path existence in the
 *                           Route Entry is to be checked.
 *                i1Tos    - Type of Service.
 *
 * Output       : None
 *
 * Returns      : OSPF_TRUE - if the Path exists with for the given Area Id.
 *                OSPF_FALSE - Otherwise.
 *
 *****************************************************************************/

PUBLIC tPath       *
RtcGetPathInArea (tRtEntry * pRtEntry, tAreaId areaId, INT1 i1Tos)
{
    tPath              *pPath = NULL;

    TMO_SLL_Scan (&(pRtEntry->aTosPath[(INT2) i1Tos]), pPath, tPath *)
    {
        if (pPath->u1Flag == OSPF_OLD_PATH)
        {
            /* This path is no longer valid */
            continue;
        }
        if (IS_EQUAL_ADDR (pPath->areaId, areaId))
        {
            break;
        }
    }

    return pPath;
}

/******************************************************************************
 *
 * Function     : RtcIsSameNextHop
 *
 * Description  : Checks if the Next Hop present in the 'pAbrPath' with the Hop
 *                Index 'u1HopIndex' is present in the 'pDestNewPath' structure.
 *
 * Input        : pDestNewPath - Path in which the existence of ABR Path is to
 *                               be checked
 *                pAreaBdrPath - ABR Path Entry whose next Hop denotd by
 *                               'u1HopIndex' is to be checked if
 *                               present in pDestNewPath
 *                u1HopIndex   - Index of the ECMP whose path is to be checked
 *
 * Output       : None
 *
 * Returns      : OSPF_TRUE, if the ABR Path's Next Hop is present in the
 *                           given 'pDestNewPath'
 *                OSPF_FALSE, otherwise.
 *
 *****************************************************************************/
PRIVATE INT1
RtcIsSameNextHop (tPath * pDestNewPath, tPath * pAreaBdrPath, UINT1 u1HopIndex)
{
    INT1                i1PathExists = OSPF_FALSE;
    UINT1               u1Ind = 0;

    for (u1Ind = 0;
         u1Ind < OSPF_MIN ((pDestNewPath->u1HopCount), (MAX_NEXT_HOPS));
         u1Ind++)
    {
        if (IS_EQUAL_ADDR (pDestNewPath->aNextHops[u1Ind].nbrIpAddr,
                           pAreaBdrPath->aNextHops[u1HopIndex].nbrIpAddr))
        {
            i1PathExists = OSPF_TRUE;
            break;
        }
    }

    return i1PathExists;
}

/******************************************************************************
 *
 * Function     : RtcIsUnresolvedVirtNextHop            
 * Description  : Checks whether  Next Hop is present in the pRtEntry or not 
 *
 * Input        : pRtEntry - 
 *                          
 * Output       : None
 *
 * Returns      : OSPF_TRUE, if next hop is not present 
 *                OSPF_FALSE, otherwise.
 *
 *****************************************************************************/
PRIVATE INT4
RtcIsUnresolvedVirtNextHopInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry)
{
    tPath              *pRtPath;
    tPath              *pTempRtPath;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               u1DeletePath = OSPF_TRUE;
    UINT1               u1DeleteRtEntry = OSPF_TRUE;
    UINT1               u1Tos;

    if (pRtEntry->u1DestType == DEST_AREA_BORDER)
    {
        return OSPF_FALSE;
    }

    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (((pRtEntry->au1Flag[u1Tos] == OSPF_ROUTE_UNUSED) ||
             (pRtEntry->au1Flag[u1Tos] == OSPF_ROUTE_USED)) &&
            (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0))
        {
            /* Next hop need not be checked in old routes
               and unchanged routes */
            u1DeleteRtEntry = OSPF_FALSE;
            continue;
        }

        OSPF_DYNM_SLL_SCAN (&(pRtEntry->aTosPath[u1Tos]), pRtPath, pTempRtPath,
                            tPath *)
        {
            if (pRtPath->u1Flag == OSPF_OLD_PATH)
            {
                /* route entry cannot be deleted, there is vaild old path */
                u1DeleteRtEntry = OSPF_FALSE;
                continue;
            }

            if ((pRtPath->u1HopCount == 0) &&
                (pRtPath->u1Flag == OSPF_NEW_PATH) &&
                ((pRtEntry->au1Flag[u1Tos] == OSPF_ROUTE_ADDED) ||
                 (pRtEntry->au1Flag[u1Tos] == OSPF_ROUTE_MODIFIED)))
            {
                /*  when there is no next hop for new route path 
                   then free the new route path , this can happen for
                   the newly added routes or modified routes */

                if (pRtEntry->u1DestType == DEST_AS_BOUNDARY)
                {
                    TMO_SLL_Delete (&(pRtEntry->aTosPath[u1Tos]),
                                    &pRtPath->nextPath);
                    PATH_FREE (pRtPath);
                }
                else
                {
                    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode,
                                  tTMO_SLL_NODE *)
                    {
                        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
                        if ((UtilIpAddrMaskComp
                             (pRtEntry->destId, pInterface->ifIpAddr,
                              pInterface->ifIpAddrMask) == OSPF_EQUAL))
                        {
                            u1DeletePath = OSPF_FALSE;
                        }
                    }
                    if (u1DeletePath == OSPF_TRUE)
                    {
                        TMO_SLL_Delete (&(pRtEntry->aTosPath[u1Tos]),
                                        &pRtPath->nextPath);
                        PATH_FREE (pRtPath);
                    }
                    u1DeletePath = OSPF_TRUE;
                }
            }
            else
            {
                /* route entry cannot be deleted, there is a vaild path */
                u1DeleteRtEntry = OSPF_FALSE;
            }
        }

    }
    if (u1DeleteRtEntry == OSPF_FALSE)
    {
        return OSPF_FALSE;
    }
    else
    {
        return OSPF_TRUE;
    }

}

/************************************************************************/
/*                                                                      */
/* Function    : RtcCalculateASExtRoutesInCxt                           */
/*                                                                      */
/* Description : This routine invokes calculation of External routes    */
/*                using Type 5 and Type 7 Ext LSAs                      */
/*                                                                      */
/* Input       :  pOspfCxt - context pointer                            */
/*                pCurrRt  - Routing table                              */
/*                i1Tos    - Type of service                            */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/
PRIVATE VOID
RtcCalculateASExtRoutesInCxt (tOspfCxt * pOspfCxt, tOspfRt * pCurrRt,
                              INT1 i1Tos)
{
/* Pointers Initialized to NULL*/

    OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
               "AS Ext Route Calculation Starts TOS %d\n", i1Tos);

    RtcCalculateAllExtRoutesInCxt (pOspfCxt, pCurrRt, i1Tos);

    RtcCalculateAllNssaExtRoutesInCxt (pOspfCxt, pCurrRt, i1Tos);

    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "AS Ext Routes Calculated\n");
}

/************************************************************************/
/*                                                                      */
/* Function    : RtcCalculateAllNssaExtRoutesInCxt                      */
/*                                                                      */
/* Description    : This routine invokes calculation of External routes */
/*                by scanning NSSA areas                                */
/*                                                                      */
/* Input           :  pOspfCxt - context pointer                        */
/*                    pCurrRt  - Routing table                          */
/*                    i1Tos    - Type of service                        */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/
PRIVATE VOID
RtcCalculateAllNssaExtRoutesInCxt (tOspfCxt * pOspfCxt, tOspfRt * pCurrRt,
                                   INT1 i1Tos)
{
/* Pointers Initialized to NULL*/
    tArea              *pArea = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    UINT4               u4CurrentTime = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: RtcCalculateAllNssaExtRoutes\n");
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType == NSSA_AREA)
        {
            TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                RtcCalculateNssaRoute (pCurrRt, pLsaInfo, i1Tos);

                if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than relinquished interval */
                    if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                        && (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
                    {
                        gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                        OSPFRtcRelinquish (pOspfCxt);
                    }
                }
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: RtcCalculateAllNssaExtRoutes\n");
}

/************************************************************************/
/*                                                                      */
/* Function    : RtcCalculateNssaRoute                                  */
/*                                                                      */
/* Description    : This routine calculates the route to the destination*/
/*           described by the Type 7 LSA                                */
/*                                                                      */
/* Input           :  pCurrRt - Routing table                           */
/*                    pLsaInfo - Type 7 LSA to be considered            */
/*                    i1Tos - Type of service                           */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/
PRIVATE VOID
RtcCalculateNssaRoute (tOspfRt * pCurrRt, tLsaInfo * pLsaInfo, INT1 i1Tos)
{
    tExtLsaLink         extLsaLink;
    tExtLsaLink         lsdbExtLsaLnk, tempExtLsaLnk;
    tIPADDR             destId;
    tIPADDRMASK         destIpAddrMask;
/* Pointers Initialized to NULL*/
    tPath              *pAsbrPath = NULL;
    tPath              *pFwdAddrPath = NULL;
    tRtEntry            newRtEntry;
    tRtEntry           *pNewRtEntry = NULL;
    tRtEntry           *pFwdRtEntry = NULL;
    tRtEntry           *pAsbrRtEntry = NULL;
    tRtEntry           *pOldDestRtEntry = NULL;
    tPath              *pOldDestRtPath = NULL;
    UINT1               u1NewPath = OSPF_ZERO;
    UINT1               u1NewRtEntry = OSPF_ZERO;
    tIPADDR             FwdAddr;
    tPath              *pDestNewPath = NULL;
    tPath              *pDestFwdPath = NULL;
    tArea              *pNssaArea = NULL;
    UINT1               u1Lock;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: RtcCalculateNssaRoute\n");

    MEMSET (&newRtEntry, 0, sizeof (tRtEntry));
    pNewRtEntry = &newRtEntry;

    if (RtcIsLsaValidForExtRtCalc (pLsaInfo, i1Tos) == OSPF_FALSE)
    {
        return;
    }
    u1Lock = (pCurrRt == pLsaInfo->pOspfCxt->pOspfRt) ? OSPF_TRUE : OSPF_FALSE;
    RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, i1Tos, &extLsaLink);
    OS_MEM_CPY (destIpAddrMask,
                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);
    UtilIpAddrMaskCopy (destId, pLsaInfo->lsaId.linkStateId, destIpAddrMask);

    if (UtilIpAddrComp (destId, gNullIpAddr) == OSPF_EQUAL)
    {
        if ((pLsaInfo->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
            && (!(IS_P_BIT_SET (pLsaInfo))))
            return;

        if ((pLsaInfo->pOspfCxt->bAreaBdrRtr == OSPF_TRUE) &&
            (pLsaInfo->pArea->u1SummaryFunctionality == NO_AREA_SUMMARY))
            return;

    }

    /* Check the ASBR reachability */
    pAsbrRtEntry = RtcFindRtEntry (pCurrRt, &(pLsaInfo->lsaId.advRtrId),
                                   &(gNullIpAddr), DEST_AS_BOUNDARY);
    if ((pAsbrRtEntry == NULL) ||
        (pAsbrRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
        return;

    OSPF_TRC (OSPF_RT_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC : RtcGetPathInArea \n");

    pAsbrPath = RtcGetPathInArea (pAsbrRtEntry, pLsaInfo->pArea->areaId, i1Tos);

    OSPF_TRC (OSPF_RT_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT : RtcGetPathInArea \n");

    if (pAsbrPath != NULL)
    {
        if (pAsbrPath->u1PathType != INTRA_AREA)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "ASBR Unreachable thru intra area)\n");
            return;
        }
    }
    else
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "ASBR Unreachable)\n");
        return;
    }

    if (UtilIpAddrComp (extLsaLink.fwdAddr, gNullIpAddr) == OSPF_EQUAL)
    {
        pFwdAddrPath = pAsbrPath;
    }
    else
    {
        pFwdRtEntry = RtlNssaRtLookup (pCurrRt, &(extLsaLink.fwdAddr), i1Tos);
        if ((pFwdRtEntry != NULL) &&
            (pFwdRtEntry->au1Flag[i1Tos] != OSPF_ROUTE_UNUSED))
        {
            OSPF_TRC (OSPF_RT_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "FUNC : RtcGetPathInArea \n");

            pFwdAddrPath =
                RtcGetPathInArea (pFwdRtEntry, pLsaInfo->pArea->areaId, i1Tos);

            OSPF_TRC (OSPF_RT_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "EXIT : RtcGetPathInArea \n");
        }
        if ((pFwdAddrPath == NULL) || (pFwdAddrPath->u1PathType != INTRA_AREA))
            return;
    }

    pOldDestRtEntry = RtcFindRtEntry (pCurrRt, &destId, &(destIpAddrMask),
                                      DEST_NETWORK);
    if ((pOldDestRtEntry == NULL) ||
        (pOldDestRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
    {
        RtcInitialiseRtEntry (pNewRtEntry, DEST_NETWORK,
                              &destId, &destIpAddrMask, RAG_NO_CHNG);
        u1NewRtEntry = OSPF_NEW_RT_ENTRY;
        u1NewPath = OSPF_NEW_PATH;
    }
    else
    {
        pOldDestRtPath =
            RtcGetRtTosPath (pOldDestRtEntry, i1Tos, OSPF_NEW_PATH);
    }

    if ((pOldDestRtPath == NULL) ||
        ((pOldDestRtPath->u1PathType == TYPE_2_EXT) &&
         (extLsaLink.u1MetricType == TYPE_1_METRIC)))
    {

        /* If entry not present for required TOS or if new entry is of  */
        /* preferred path type set new path flag                    */
        u1NewPath = OSPF_NEW_PATH;
    }
    else if ((pOldDestRtPath->u1PathType == INTRA_AREA) ||
             (pOldDestRtPath->u1PathType == INTER_AREA) ||
             ((pOldDestRtPath->u1PathType == TYPE_1_EXT) &&
              (extLsaLink.u1MetricType == TYPE_2_METRIC)))
    {

        /* if existing entry is of preferred path type do nothing */

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "Existing Path Type Preferred\n");
        return;
    }
    else if ((pOldDestRtPath->u1PathType == TYPE_2_EXT) &&
             (extLsaLink.u4Cost != pOldDestRtPath->u4Type2Cost))
    {
        /* both old and new are of path type TYPE_2_EXT */
        if (extLsaLink.u4Cost < pOldDestRtPath->u4Type2Cost)
        {
            u1NewPath = OSPF_NEW_PATH;
        }
    }
    else
    {
        /* Get the forwarding address for path already existing path */

        RtcGetExtLsaLink (pOldDestRtPath->pLsaInfo->pLsa,
                          pOldDestRtPath->pLsaInfo->u2LsaLen,
                          i1Tos, &tempExtLsaLnk);
        IP_ADDR_COPY (FwdAddr, tempExtLsaLnk.fwdAddr);

        if (pLsaInfo->pOspfCxt->rfc1583Compatibility == OSPF_DISABLED)
        {
            if (UtilIpAddrComp (FwdAddr, gNullIpAddr) == OSPF_EQUAL)
            {
                pDestFwdPath = RtcFindPreferredASBRPath (pCurrRt,
                                                         &
                                                         (pOldDestRtPath->
                                                          pLsaInfo->lsaId.
                                                          advRtrId), i1Tos);
            }
            else
            {
                pDestFwdPath =
                    RtlRtLookupInCxt (pLsaInfo->pOspfCxt, pCurrRt, &FwdAddr,
                                      i1Tos);
            }
            if (pDestFwdPath == NULL)
            {
                return;
            }
            u1NewPath = RtcRfc1583DisExtRt (pOldDestRtPath, pDestFwdPath,
                                            pFwdAddrPath, &extLsaLink);
            if (u1NewPath == OSPF_NO_NEW_PATH)
            {
                return;
            }
        }                        /* RFC 1583 disabled */
        else                    /* RFC 1583 enabled */
        {
            if (UtilIpAddrComp (FwdAddr, gNullIpAddr) == OSPF_EQUAL)
            {
                pDestFwdPath =
                    RtcFindPath (pCurrRt, &(pOldDestRtPath->pLsaInfo->lsaId.
                                            advRtrId),
                                 DEST_AS_BOUNDARY, i1Tos, NULL);

            }
            else
            {
                pDestFwdPath =
                    RtlRtLookupInCxt (pLsaInfo->pOspfCxt, pCurrRt, &FwdAddr,
                                      i1Tos);
            }
            if (pDestFwdPath == NULL)
            {
                return;
            }

            if (pOldDestRtPath->u1PathType == TYPE_1_EXT)
            {

                /* both old and new are of path type TYPE_1_EXT */
                RtcGetExtLsaLink (pOldDestRtPath->pLsaInfo->pLsa,
                                  pOldDestRtPath->pLsaInfo->u2LsaLen,
                                  i1Tos, &lsdbExtLsaLnk);

                if (((pFwdAddrPath->u4Cost) + extLsaLink.u4Cost) <
                    (pDestFwdPath->u4Cost + lsdbExtLsaLnk.u4Cost))
                {
                    u1NewPath = OSPF_NEW_PATH;
                }
                else if (((pFwdAddrPath->u4Cost) + extLsaLink.u4Cost) ==
                         (pDestFwdPath->u4Cost + lsdbExtLsaLnk.u4Cost))
                {

                    u1NewPath = OSPF_ADD_TO_LIST_OF_PATH;
                }
            }
            else if (extLsaLink.u4Cost == pOldDestRtPath->u4Type2Cost)
            {

                /* if type 2 metrics are equal compare internal costs */

                if (pFwdAddrPath->u4Cost < pDestFwdPath->u4Cost)
                {
                    u1NewPath = OSPF_NEW_PATH;
                }
                else if (pFwdAddrPath->u4Cost == pDestFwdPath->u4Cost)
                {
                    u1NewPath = OSPF_ADD_TO_LIST_OF_PATH;
                }
            }
        }                        /* RFC 1583 enabled */

    }
/*
  u1newPath = 3 can come under following cases RFC 1583 disabled:
    a) When there was an existing NSSA non bkbone
        path (intra - area) of equal priority.
        b) When there is an existing Normal intra - area
        (non bkbone) path of equal priority.
        RFC 1583 enabled
        c) When there is an existing Normal intra - inter path of
        equal priority.
        Taking care of LSA priority - Same Non zero forwarding address
        Following combination can exist
        a) Type 7 with P bit Set +
        Type 5 existing route:Type 7 LSA is preferred
        b) Type 7 with P bit Clear +
        Type 5 existing route:Type 5 LSA is preferred
*/

    if ((UtilIpAddrComp (extLsaLink.fwdAddr, gNullIpAddr) != OSPF_EQUAL)
        && (u1NewPath == OSPF_ADD_TO_LIST_OF_PATH))
    {
        if ((pOldDestRtPath->pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA) &&
            (UtilIpAddrComp (FwdAddr, extLsaLink.fwdAddr) == OSPF_EQUAL))
        {
            if (IS_P_BIT_SET (pLsaInfo))
            {
                u1NewPath = OSPF_NEW_PATH;
            }
            else
                return;
        }
    }
    if (!u1NewPath)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "RT Not Updated\n");
        return;
    }

    pDestNewPath = RtcNewPathForExtRt (pLsaInfo, u1NewPath, pFwdAddrPath,
                                       pOldDestRtPath, &extLsaLink);
    if (pDestNewPath == NULL)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "Path Creation Failed\n");
        return;
    }
    if ((pLsaInfo->lsaId.u1LsaType == NSSA_LSA) ||
        (pLsaInfo->lsaId.u1LsaType == COND_NSSA_LSA) ||
        (pLsaInfo->lsaId.u1LsaType == DEFAULT_NSSA_LSA))
    {
        pNssaArea =
            GetFindAreaInCxt (pLsaInfo->pOspfCxt, &(pDestNewPath->areaId));
        if ((pNssaArea != NULL) && (pNssaArea->u4AreaType == NSSA_AREA))
        {
            if ((!IS_MAX_AGE (pLsaInfo->u2LsaAge)) &&
                (pLsaInfo->u1TrnsltType5 == FLUSHED_LSA))
            {
                pLsaInfo->u1TrnsltType5 = OSPF_FALSE;
            }
        }
    }

    /* add new next hops( the next hops to fwd addr) to pDestNewPath */
    RtcAddNxtHopForExtRt (pLsaInfo, pDestNewPath, pFwdAddrPath, &extLsaLink);

    if (u1NewRtEntry == OSPF_NEW_RT_ENTRY)
    {

        TMO_SLL_Add (&(pNewRtEntry->aTosPath[(INT2) i1Tos]),
                     &(pDestNewPath->nextPath));
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "New RT Entry To Be Added\n");

        RtcProcessOldNewRtEntryChanges (pCurrRt, pOldDestRtEntry, pNewRtEntry,
                                        i1Tos, pLsaInfo->pArea);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "New RT Entry Added\n");
    }
    else if (u1NewPath != OSPF_ZERO)
    {
        RtcReplacePathInCxt (pLsaInfo->pOspfCxt, pOldDestRtEntry,
                             pDestNewPath, i1Tos, u1Lock);
        OsixGetSysTime ((tOsixSysTime *) & (pOldDestRtEntry->u4UpdateTime));
    }
    else
    {
        PATH_FREE (pDestNewPath);
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: RtcCalculateNssaRoute\n");
}

/************************************************************************/
/*                                                                      */
/* Function       : RtcIncUpdateExtRoute                                */
/*                                                                      */
/* Description    : This routine updates the route to the destination   */
/*                described by the Type 5 and Type 7 LSAs.              */
/*                                                                      */
/* Input          :  pCurrRt  - Routing table                           */
/*                   pLsaInfo - Type 5 or Type 7 LSA to be considered   */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RtcIncUpdateExtRoute (tOspfRt * pCurrRt, tLsaInfo * pLsaInfo)
{
    UINT4               u4HashIndex = 0;
    INT1                i1Tos;
    INT1                u1DeleteRoute = OSPF_FALSE;
    tIPADDRMASK         DestMask;
    tIPADDRMASK         DbdestIpAddrMask;
    tIPADDRMASK        *pLstDestMask = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tLsaInfo           *pNssaLsaInfo = NULL;
    tLsaInfo           *pExtLsaInfo = NULL;
    tOsDbNode          *pOsDbNode = NULL;
    tLsaInfo           *pDbLsaInfo = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tRtEntry           *pOldRtEntry = NULL;
    tRtEntry           *pNewRtEntry = NULL;
    tArea              *pArea = NULL;
    tPath              *pNssaPath = NULL;
    tArea              *pNssaArea = NULL;
    tLsaInfo           *pFlshLsdbLsaInfo = NULL;
    tIPADDR             maskedAddr;
    INT4                i4RouteFlag = 0;

    OS_MEM_CPY (DestMask, (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE),
                MAX_IP_ADDR_LEN);
    if ((pOldRtEntry =
         RtcFindRtEntry (pCurrRt, &(pLsaInfo->lsaId.linkStateId),
                         &DestMask, DEST_NETWORK)) != NULL)
    {
        if ((i4RouteFlag =
             RtcCheckAllTosFlags (pOldRtEntry)) == OSPF_ROUTE_UNUSED)
        {
            return;
        }
    }
    if (pOldRtEntry != NULL)
    {
        for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
        {
            /* check whether the old external route needs to be deleted  
             * and new ext route needs to be recalculated  */
            /* if the route flag is marked as OSPF_ROUTE_ADDED/OSPF_ROUTE_MODIFIED
             * /OSPF_ROUTE_USED then the route should have atleast one valid path with
             * path-flag set to OSPF_NEW_PATH. if this condition is true then delete 
             * the old route and calculate the new ext route        
             */
            if (RtcGetRtTosPath (pOldRtEntry, i1Tos, OSPF_NEW_PATH) != NULL)
            {
                u1DeleteRoute = OSPF_TRUE;
            }
        }
        if (u1DeleteRoute == OSPF_FALSE)
        {
            return;
        }
    }
    if ((pOldRtEntry != NULL) && (i4RouteFlag != OSPF_ROUTE_UNUSED))
        /* Find current routing table entry */
    {
        /* If existing entry is of preferrable path type do nothing */
        if ((GET_PATH_TYPE (pOldRtEntry, TOS_0) == INTRA_AREA) ||
            (GET_PATH_TYPE (pOldRtEntry, TOS_0) == INTER_AREA))
        {
            return;
        }

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "RT Entry To Be Deleted\n");

        RtcDeleteRtEntry (pCurrRt, pOldRtEntry);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "RT Entry Deleted\n");

        /* Check router is not performing GR
         * and Check If the node is active node */
        if ((pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
            (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE))
        {
            if (pLsaInfo->lsaId.u1LsaType == NSSA_LSA)
            {
                OSPF_CRU_BMC_DWTOPDU (maskedAddr, pOldRtEntry->u4IpAddrMask);
                pFlshLsdbLsaInfo =
                    LsuDatabaseLookUp (AS_EXT_LSA, &(pOldRtEntry->destId),
                                       &(maskedAddr),
                                       &(pLsaInfo->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pLsaInfo->pOspfCxt->pBackbone);

                if (pFlshLsdbLsaInfo != NULL)
                {
                    if ((pFlshLsdbLsaInfo->pLsaDesc != NULL) &&
                        (pFlshLsdbLsaInfo->pLsaDesc->u1InternalLsaType
                         == AS_TRNSLTD_EXT_LSA))

                    {
                        pFlshLsdbLsaInfo->pLsaDesc->pAssoPrimitive = NULL;
                        AgdFlushOut (pFlshLsdbLsaInfo);
                    }
                }
            }
        }
    }

    u4HashIndex = LsuPrefixLsaHashFunc (pLsaInfo->lsaId.linkStateId, DestMask);

    TMO_HASH_Scan_Bucket (pLsaInfo->pOspfCxt->pExtLsaHashTable, u4HashIndex,
                          pOsDbNode, tOsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pDbLstNode);

        OS_MEM_CPY (DbdestIpAddrMask,
                    (UINT1 *) (pDbLsaInfo->pLsa + LS_HEADER_SIZE),
                    MAX_IP_ADDR_LEN);

        if ((MEMCMP (DestMask, DbdestIpAddrMask, MAX_IPV4_ADDR_LEN)
             == OSPF_EQUAL) &&
            (MEMCMP (pLsaInfo->lsaId.linkStateId,
                     pDbLsaInfo->lsaId.linkStateId, MAX_IPV4_ADDR_LEN)
             == OSPF_EQUAL))
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pExtLsaInfo =
                    OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLstNode);
                for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                {
#ifdef TOS_SUPPORT
                    if ((i1Tos != 0)
                        && (pLsaInfo->pOspfCxt->bTosCapability == OSPF_FALSE))
                    {
                        break;
                    }
#endif /* TOS_SUPPORT */
                    RtcCalculateExtRoute (pCurrRt, pExtLsaInfo, i1Tos);
                }
            }
            break;
        }
    }

    TMO_SLL_Scan (&(pLsaInfo->pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }

        TMO_SLL_Scan (&(pArea->nssaLSALst), pLstNode, tTMO_SLL_NODE *)
        {

            pNssaLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
            pLstDestMask =
                (tIPADDRMASK *) (pNssaLsaInfo->pLsa + LS_HEADER_SIZE);

            /* Consider all NSSA LSAs which describe the same destination */

            if ((UtilIpAddrComp (*pLstDestMask, DestMask) == OSPF_EQUAL)
                &&
                (UtilIpAddrMaskComp
                 (pNssaLsaInfo->lsaId.linkStateId,
                  pLsaInfo->lsaId.linkStateId, *pLstDestMask) == OSPF_EQUAL))
            {

                for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                {
#ifdef TOS_SUPPORT
                    if ((i1Tos != TOS_0)
                        && (pLsaInfo->pOspfCxt->bTosCapability == OSPF_FALSE))
                    {
                        break;
                    }
#endif /* TOS_SUPPORT */
                    RtcCalculateNssaRoute (pCurrRt, pNssaLsaInfo, i1Tos);
                }
            }
        }
    }

    pNewRtEntry =
        RtcFindRtEntry (pCurrRt, &(pLsaInfo->lsaId.linkStateId),
                        &DestMask, DEST_NETWORK);
    if (pNewRtEntry != NULL)
    {

        RtcProcessRtEntryChangeInCxt (pLsaInfo->pOspfCxt,
                                      pNewRtEntry, pOldRtEntry);

        pNssaPath = GET_TOS_PATH (pNewRtEntry, TOS_0);
        if ((pNssaPath != NULL) &&
            (pNssaPath->pLsaInfo->lsaId.u1LsaType == NSSA_LSA) &&
            (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA))
        {
            pNssaArea =
                GetFindAreaInCxt (pLsaInfo->pOspfCxt, &(pNssaPath->areaId));
            if ((pNssaArea != NULL) && (pNssaArea->u4AreaType == NSSA_AREA) &&
                (pNssaArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
                (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE))
            {
                /* Translation should be done only in active node */
                NssaType7LsaTranslation (pNssaArea, pNssaPath->pLsaInfo);
                NssaAdvertiseType7Rngs (pNssaArea);
            }
        }
    }
    if ((pOldRtEntry != NULL) && (pNewRtEntry == NULL))
    {
        if (IS_DEST_NETWORK (pOldRtEntry))
        {
            RtmTxRtChngNtf (pLsaInfo->pOspfCxt, pOldRtEntry, RT_ROWSTATUS_MASK);
            RtcProcessUnreachableRouteInCxt (pLsaInfo->pOspfCxt, pOldRtEntry);
        }
    }
    if (pOldRtEntry != NULL)
    {
        RtcFreeRtEntry (pOldRtEntry);
    }
    if ((pLsaInfo->lsaId.u1LsaType == NSSA_LSA) &&
        (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
        (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE))
    {
        /* Translation should be done only in active node */
        NssaType7LsaTranslation (pLsaInfo->pArea, pLsaInfo);
        NssaAdvertiseType7Rngs (pLsaInfo->pArea);
    }
    return;
}

/************************************************************************/
/*                                                                      */
/* Function       : RtcIncUpdateExtRouteForNetworkInCxt                 */
/*                                                                      */
/* Description    : This routine updates the route to the destination   */
/*                described by the DBNode for a External LSA Network    */
/*                                                                      */
/* Input          :  pOspfCxt - context pointer                         */
/*                   pCurrRt  - Routing table                           */
/*                   pLsaInfo - Type 5 or Type 7 LSA to be considered   */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/

PRIVATE VOID
RtcIncUpdateExtRouteForNetworkInCxt (tOspfCxt * pOspfCxt,
                                     tOspfRt * pCurrRt, tOsDbNode * pOsDbNode)
{
    INT1                i1Tos;
    tIPADDRMASK         LstDestMask;
    tIPADDRMASK         DbdestIpAddrMask;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tLsaInfo           *pNssaLsaInfo = NULL;
    tLsaInfo           *pExtLsaInfo = NULL;
    tLsaInfo           *pDbLsaInfo = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tRtEntry           *pOldRtEntry = NULL;
    tRtEntry           *pNewRtEntry = NULL;
    tPath              *pNssaPath = NULL;
    tArea              *pArea = NULL;
    tArea              *pNssaArea = NULL;

    pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
    pDbLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pDbLstNode);

    OS_MEM_CPY (DbdestIpAddrMask, (UINT1 *) (pDbLsaInfo->pLsa
                                             + LS_HEADER_SIZE),
                MAX_IP_ADDR_LEN);
    /* Find current routing table entry */
    if ((pOldRtEntry =
         RtcFindRtEntry (pCurrRt, &(pDbLsaInfo->lsaId.linkStateId),
                         &DbdestIpAddrMask, DEST_NETWORK)) != NULL)
    {
        /* If existing entry is of preferrable path type do nothing */
        if ((GET_PATH_TYPE (pOldRtEntry, TOS_0) == INTRA_AREA) ||
            (GET_PATH_TYPE (pOldRtEntry, TOS_0) == INTER_AREA))
        {
            return;
        }

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pOspfCxt->u4OspfCxtId, "RT Entry To Be Deleted\n");

        RtcDeleteRtEntry (pCurrRt, pOldRtEntry);

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pOspfCxt->u4OspfCxtId, "RT Entry Deleted\n");
    }

    TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
    {
        pExtLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLstNode);
        for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
        {
#ifdef TOS_SUPPORT
            if ((i1Tos != 0) && (pOspfCxt->bTosCapability == OSPF_FALSE))
            {
                break;
            }
#endif /* TOS_SUPPORT */
            RtcCalculateExtRoute (pCurrRt, pExtLsaInfo, i1Tos);
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }

        TMO_SLL_Scan (&(pArea->nssaLSALst), pLstNode, tTMO_SLL_NODE *)
        {

            pNssaLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
            OS_MEM_CPY (LstDestMask, (UINT1 *)
                        (pNssaLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

            /* Consider all NSSA LSAs which describe the same destination */
            if ((UtilIpAddrComp (LstDestMask, DbdestIpAddrMask) == OSPF_EQUAL)
                &&
                (UtilIpAddrMaskComp
                 (pNssaLsaInfo->lsaId.linkStateId,
                  pDbLsaInfo->lsaId.linkStateId, LstDestMask) == OSPF_EQUAL))
            {

                for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                {
#ifdef TOS_SUPPORT
                    if ((i1Tos != TOS_0)
                        && (pOspfCxt->bTosCapability == OSPF_FALSE))
                    {
                        break;
                    }
#endif /* TOS_SUPPORT */
                    RtcCalculateNssaRoute (pCurrRt, pNssaLsaInfo, i1Tos);
                }
            }
        }
    }

    pNewRtEntry =
        RtcFindRtEntry (pCurrRt, &(pDbLsaInfo->lsaId.linkStateId),
                        &DbdestIpAddrMask, DEST_NETWORK);
    if (pNewRtEntry != NULL)
    {

        RtcProcessRtEntryChangeInCxt (pOspfCxt, pNewRtEntry, pOldRtEntry);

        pNssaPath = GET_TOS_PATH (pNewRtEntry, TOS_0);
        if ((pNssaPath != NULL) &&
            (pNssaPath->pLsaInfo->lsaId.u1LsaType == NSSA_LSA) &&
            (pDbLsaInfo->lsaId.u1LsaType == AS_EXT_LSA))
        {
            pNssaArea = GetFindAreaInCxt (pOspfCxt, &(pNssaPath->areaId));
            if ((pNssaArea != NULL) && (pNssaArea->u4AreaType == NSSA_AREA) &&
                (pNssaArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
                (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE))
            {
                /* Translation should be done only in active node */
                NssaType7LsaTranslation (pNssaArea, pNssaPath->pLsaInfo);
                NssaAdvertiseType7Rngs (pNssaArea);
            }
        }
    }
    if ((pOldRtEntry != NULL) && (pNewRtEntry == NULL))
    {
        if (IS_DEST_NETWORK (pOldRtEntry))
        {
            RtmTxRtChngNtf (pOspfCxt, pOldRtEntry, RT_ROWSTATUS_MASK);
            RtcProcessUnreachableRouteInCxt (pOspfCxt, pOldRtEntry);
        }
    }

    if (pOldRtEntry != NULL)
    {
        RtcFreeRtEntry (pOldRtEntry);
    }
    return;
}

/************************************************************************/
/*                                                                      */
/* Function       : RtcCheckTranslationInCxt                            */
/*                                                                      */
/* Description    : Checks and translates external LSA                  */
/*                                                                      */
/* Input          :  pOspfCxt - context pointer                         */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Void                                                  */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
RtcCheckTranslationInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tTMO_SLL_NODE      *pTmpLsaNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tOsDbNode          *pOsDbNode = NULL;
    tOsDbNode          *pTmpOsDbNode = NULL;
    tExtRoute          *pExtRoute = NULL;
    tIPADDRMASK         destIpAddrMask;
    UINT4               u4CurrentTime = 0;

    if ((pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        /* LSA cannot be Flushed in GR mode */
        /* LSA cannot be flused in standby node */
        return;
    }

    /* Flushing any unapproved Translated Type 5 LSAs   */
    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_DYN_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                                  pOsDbNode, pTmpOsDbNode, tOsDbNode *)
        {
            TMO_DYN_SLL_Scan (&pOsDbNode->lsaLst, pLsaNode,
                              pTmpLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLsaNode);
                if ((pLsaInfo->u1TrnsltType5 == OSPF_FALSE) &&
                    (UtilIpAddrComp (pOspfCxt->rtrId,
                                     pLsaInfo->lsaId.advRtrId) == OSPF_EQUAL))
                {

                    /* While flushing check what all clean up is required */
                    OS_MEM_CPY (destIpAddrMask,
                                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE),
                                MAX_IP_ADDR_LEN);

                    AgdFlushOut (pLsaInfo);

                    if ((pExtRoute = GetFindExtRouteInCxt
                         (pOspfCxt, &(pLsaInfo->lsaId.linkStateId),
                          &(destIpAddrMask))) != NULL)
                    {
                        RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);
                    }
                }
                /* Check for relinquish */
                if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than relinquished interval */
                    if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                        && (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
                    {
                        gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                        OSPFRtcRelinquish (pOspfCxt);
                    }
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCheckAddrRngActvFlg                                     */
/*                                                                           */
/* Description  : Check the address range active flags value.                */
/*                                                                           */
/* Input        : pLinkStateId : Link State Id                               */
/*              : pNewAreaId   : Area Id                                     */
/*              : pOldAreaId   : Area id                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcCheckAddrRangeFlagInCxt (tOspfCxt * pOspfCxt,
                            tLINKSTATEID * pLinkStateId, tAreaId * pNewAreaId,
                            tAreaId * pOldAreaId)
{

    tAddrRange         *pNewAddrRange;
    tAddrRange         *pOldAddrRange;
    tArea              *pArea;
    UINT4               u4IpAddrMask;

    pNewAddrRange = UtilFindAssoAreaAddrRangeInCxt (pOspfCxt,
                                                    pLinkStateId, pNewAreaId);
    pOldAddrRange = UtilFindAssoAreaAddrRangeInCxt (pOspfCxt,
                                                    pLinkStateId, pOldAreaId);

    if ((pOldAddrRange != NULL) && (pOldAddrRange->u1CalActive == OSPF_FALSE)
        && (pNewAddrRange == NULL))
    {

        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if (UtilIpAddrComp (pArea->areaId, *pOldAreaId) == OSPF_EQUAL)
            {
                continue;
            }
            u4IpAddrMask = OSPF_CRU_BMC_DWFROMPDU (pOldAddrRange->ipAddrMask);
            RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                   &(pOldAddrRange->ipAddr), u4IpAddrMask);
        }

    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcFlushOutSummaryFromAllAreas                             */
/*                                                                           */
/* Description  : This routine flushes summary LSA correspoding to the given */
/*                route entry from all areas except Backbone                 */
/*                                                                           */
/* Input        : pLinkStateId : Pointer to Network Id of the route to be    */
/*                               flushed out.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
RtcFlushOutSummaryFromAllAreasInCxt (tOspfCxt * pOspfCxt, UINT1 u1LsaType,
                                     tLINKSTATEID * pLinkStateId,
                                     UINT4 u4IpAddrMask)
{
    tArea              *pArea;

    if ((pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        /* LSA cannot be Flushed in GR mode */
        /* LSA cannot be flused in standby node */
        return;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (UtilIpAddrComp (pArea->areaId, gNullIpAddr) == OSPF_EQUAL)
        {
            continue;
        }
        RtcFlushOutSummaryLsa (pArea, u1LsaType, pLinkStateId, u4IpAddrMask);
    }
}

/************************************************************************/
/*                                                                      */
/* Function    : RtcIsLsaValidForExtRtCalc                              */
/*                                                                      */
/* Description    : This routine checks if LSA (Type5/7)                */
/*                  is valid for Ext Rt Calculation                     */
/*                                                                      */
/* Input           :  pLsaInfo - Type 5/7 LSA                           */
/*                    i1Tos - Type of service                           */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : OSPF_TRUE                                             */
/*                OSPF_FALSE                                            */
/************************************************************************/
PRIVATE INT1
RtcIsLsaValidForExtRtCalc (tLsaInfo * pLsaInfo, INT1 i1Tos)
{
    tExtLsaLink         extLsaLink;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: RtcIsLsaValidForExtRtCalc\n");

    if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "Ext LSA is MaxAge\n");
        return OSPF_FALSE;
    }
    if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                        pLsaInfo->pOspfCxt->rtrId) == OSPF_EQUAL)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "Ext LSA is Self Orig\n");
        return OSPF_FALSE;
    }
#ifdef TOS_SUPPORT
    if (((pLsaInfo->lsaOptions & T_BIT_MASK) != T_BIT_MASK) && (i1Tos != TOS_0))
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "TOS Support Mismatch\n");
        return OSPF_FALSE;
    }
#endif /* TOS_SUPPORT */

    /* Cost is initialized to avoid random behaviour if lsa
     * contains only header */

    extLsaLink.u4Cost = LS_INFINITY_24BIT;
    RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, i1Tos, &extLsaLink);

    /* If the cost sepecified by the advertisement is LS_INFINITY, ignore LSA */
    if (extLsaLink.u4Cost > LS_INFINITY_24BIT)
    {

        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "LSA Cost Adv LSInfinity)\n");
        return OSPF_FALSE;
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: RtcIsLsaValidForExtRtCalc\n");
    return OSPF_TRUE;
}

/************************************************************************/
/*                                                                      */
/* Function    :   RtcAddNxtHopForExtRt                                 */
/*                                                                      */
/* Description    : This routine adds set of next hops for              */
/*                  Ext Rt Calculation                                  */
/*                                                                      */
/* Input           :  pLsaInfo - Type 5/7 LSA                           */
/*                    pDestNewPath - Path to be updated                 */
/*                    pFwdAddrPath - Path whose next hops are added in  */
/*                                   pDestNewPath                       */
/*                    pExtLsaLink - Pointer to Ext Lsa Link Info        */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/*                                                                      */
/************************************************************************/
PRIVATE VOID
RtcAddNxtHopForExtRt (tLsaInfo * pLsaInfo, tPath * pDestNewPath,
                      tPath * pFwdAddrPath, tExtLsaLink * pExtLsaLink)
{
    UINT1               u1Index1, u1Index2;
    INT1                i1PathExists = OSPF_ZERO;
    tPath               DestTempPath;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: RtcAddNxtHopForExtRt\n");
    MEMCPY (&DestTempPath, pFwdAddrPath, sizeof (tPath));

    for (u1Index1 = 0, u1Index2 = pDestNewPath->u1HopCount;
         u1Index1 < OSPF_MIN ((pFwdAddrPath->u1HopCount), (MAX_NEXT_HOPS)) &&
         u1Index2 < MAX_NEXT_HOPS; u1Index1++)

    {
        /* This check is done here before calling RtcIsSameNextHop
         * to prevent addition of route entry with same next hop twice*/
        if (UtilIpAddrComp
            ((DestTempPath.aNextHops[u1Index1].nbrIpAddr),
             gNullIpAddr) == OSPF_EQUAL)

        {

            IP_ADDR_COPY (DestTempPath.aNextHops[u1Index1].
                          nbrIpAddr, pExtLsaLink->fwdAddr);
        }

        OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "FUNC : RtcIsSameNextHop \n");
        i1PathExists = RtcIsSameNextHop (pDestNewPath, &DestTempPath, u1Index1);
        OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "EXIT : RtcIsSameNextHop \n");
        if (i1PathExists == OSPF_FALSE)
        {

            OS_MEM_CPY (&pDestNewPath->aNextHops[u1Index2],
                        &DestTempPath.aNextHops[u1Index1],
                        sizeof (tRouteNextHop));
            IP_ADDR_COPY (pDestNewPath->aNextHops[u1Index2].advRtrId,
                          pLsaInfo->lsaId.advRtrId);

            u1Index2++;
        }
    }
    pDestNewPath->u1HopCount = u1Index2;
    pDestNewPath->pLsaInfo = pLsaInfo;

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: RtcAddNxtHopForExtRt\n");
}

/************************************************************************/
/*                                                                      */
/* Function    :    RtcNewPathForExtRt                                  */
/*                                                                      */
/* Description    : This routine creates/updates the path for           */
/*                  Ext Rt Calculation                                  */
/*                                                                      */
/* Input           :  u1NewPath -  Path Flag                            */
/*                    pFwdAddrPath - Path whose next hops are added in  */
/*                                   pDestNewPath                       */
/*                    pDestRtPath - Existing Path                       */
/*                    pExtLsaLink - Pointer to Ext Lsa Link Info        */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : New Path                                              */
/*                                                                      */
/************************************************************************/
PRIVATE tPath      *
RtcNewPathForExtRt (tLsaInfo * pLsaInfo, UINT1 u1NewPath, tPath * pFwdAddrPath,
                    tPath * pDestRtPath, tExtLsaLink * pExtLsaLink)
{
    tPath              *pDestNewPath = NULL;
    tIPADDR             areaId;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: RtcNewPathForExtRt\n");
    /* 
     * if u1NewPath flag is set to OSPF_GREATER then replace path 
     * if u1NewPath flag is set to OSPF_LESS then add to list of paths
     */

    if (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA)
    {
        IP_ADDR_COPY (areaId, pFwdAddrPath->areaId);
    }
    else
    {
        IP_ADDR_COPY (areaId, pLsaInfo->pArea->areaId);
    }
    if (u1NewPath == OSPF_NEW_PATH)
    {
        OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId, "New Path Preferred\n");
        if (pExtLsaLink->u1MetricType == TYPE_2_METRIC)
        {
            pDestNewPath =
                RtcCreatePath (&areaId, TYPE_2_EXT,
                               pFwdAddrPath->u4Cost, pExtLsaLink->u4Cost);
        }
        else
        {
            pDestNewPath =
                RtcCreatePath (&areaId, TYPE_1_EXT,
                               (pFwdAddrPath->u4Cost + pExtLsaLink->u4Cost), 0);
        }
        if (pDestNewPath == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "RT Path Alloc Failure\n");
        }
        else
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pLsaInfo->pOspfCxt->u4OspfCxtId, "New RT Path Created\n");
        }
    }
    else if ((u1NewPath == OSPF_REPLACE_PATH)
             || (u1NewPath == OSPF_ADD_TO_LIST_OF_PATH))
    {

        if (PATH_ALLOC (pDestNewPath) == NULL)
        {

            OSPF_TRC (OS_RESOURCE_TRC | OSPF_RTMODULE_TRC,
                      pLsaInfo->pOspfCxt->u4OspfCxtId, "Path Alloc Failure\n");
            return NULL;
        }
        OS_MEM_CPY (pDestNewPath, pDestRtPath, sizeof (tPath));
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: RtcNewPathForExtRt\n");
    return pDestNewPath;
}

/**************************************************************************/
/*                                                                        */
/* Function    :    RtcRfc1583DisExtRt                                    */
/*                                                                        */
/* Description    : This routine checks if new path is to be created      */
/*                  or existing one is to be replaced when RFC 1583       */
/*                  support is disabled in Ext Rt Calculation             */
/*                                                                        */
/* Input           :  pDestRtPath - Existing path for Network             */
/*                    pDestFwdPath - Fwd Addr path for existing RT entry  */
/*                    pFwdAddrPath - Fwd Addr path for LSA being processed*/
/*                    pExtLsaLink - Pointer to Ext Lsa Link Info          */
/*                                                                        */
/* Output       : None                                                    */
/*                                                                        */
/* Returns      : Path Flag                                               */
/*                                                                        */
/**************************************************************************/
PRIVATE UINT1
RtcRfc1583DisExtRt (tPath * pDestRtPath, tPath * pDestFwdPath,
                    tPath * pFwdAddrPath, tExtLsaLink * pExtLsaLink)
{
    UINT1               u1NewPath = OSPF_ZERO;
    OSPF_TRC (OSPF_FN_ENTRY, pDestRtPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: RtcRfc1583DisExtRt\n");

    /* Compare pFwdAddrPath & pDestFwdPath */
    if ((pDestFwdPath->u1PathType == INTRA_AREA) &&
        (UtilIpAddrComp (pDestFwdPath->areaId, gNullIpAddr) != OSPF_EQUAL))
    {
        /* intra-area non-backbone path */

        if ((pFwdAddrPath->u1PathType != INTRA_AREA) ||
            (UtilIpAddrComp (pFwdAddrPath->areaId, gNullIpAddr) == OSPF_EQUAL))
        {
            /* already preferred path exists. Dont install this path */
            return OSPF_NO_NEW_PATH;
        }

        if (pDestRtPath->u1PathType == TYPE_2_EXT)
        {
            if (pDestFwdPath->u4Cost > pFwdAddrPath->u4Cost)
            {
                /* Prefer the passed entry */
                u1NewPath = OSPF_NEW_PATH;
            }
            else if (pDestFwdPath->u4Cost == pFwdAddrPath->u4Cost)
            {
                /* add to list of paths */
                u1NewPath = OSPF_ADD_TO_LIST_OF_PATH;
            }
        }
        else
        {                        /* type 1 external paths  */

            if (((pFwdAddrPath->u4Cost) + pExtLsaLink->u4Cost) <
                pDestRtPath->u4Cost)
            {
                /* Install the new path  and delete the old path */
                u1NewPath = OSPF_NEW_PATH;
            }
            else if (((pFwdAddrPath->u4Cost) + pExtLsaLink->u4Cost)
                     == pDestRtPath->u4Cost)
            {
                /* Add it  to list of paths  */
                u1NewPath = OSPF_ADD_TO_LIST_OF_PATH;
            }
        }
    }
    else if ((pFwdAddrPath->u1PathType == INTRA_AREA) &&
             (UtilIpAddrComp (pFwdAddrPath->areaId, gNullIpAddr) != OSPF_EQUAL))
    {

        /* Delete the current paths and install this path */
        u1NewPath = OSPF_NEW_PATH;
    }
    else
    {
        /* inter-area path OR backbone path */
        if (pFwdAddrPath->u1PathType == INTRA_AREA)
        {
            /* Delete the current paths and install this path */
            u1NewPath = OSPF_NEW_PATH;
        }
        if (pDestRtPath->u1PathType == TYPE_2_EXT)
        {
            if (pDestFwdPath->u4Cost > pFwdAddrPath->u4Cost)
            {
                /* Prefer the passed entry */
                u1NewPath = OSPF_NEW_PATH;
            }
            else if (pDestFwdPath->u4Cost == pFwdAddrPath->u4Cost)
            {
                /* add to list of paths */
                u1NewPath = OSPF_ADD_TO_LIST_OF_PATH;
            }
        }
        else
        {                        /* type 1 external paths  */
            if (((pFwdAddrPath->u4Cost) + pExtLsaLink->u4Cost) <
                pDestRtPath->u4Cost)
            {
                /* Install the new path  and delete the old path */
                u1NewPath = OSPF_NEW_PATH;
            }
            else if (((pFwdAddrPath->u4Cost) + pExtLsaLink->u4Cost)
                     == pDestRtPath->u4Cost)
            {
                /* Add it  to list of paths  */
                u1NewPath = OSPF_ADD_TO_LIST_OF_PATH;
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pDestRtPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: RtcRfc1583DisExtRt\n");
    return u1NewPath;
}

/************************************************************************/
/*                                                                      */
/* Function     : RtcSetAggFlagInCxt                                    */
/*                                                                      */
/* Description  : This Function Set the Address Range Flag if           */
 /*               route entry falls in Address range                    */
/*                this flag will be used for generation of agg lsa      */
/*                                                                      */
/* Input        : pOspfCxt        :  context pointer                    */
/*                pRtEntry        :  pointer to the RtEntry             */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns     : None                                                   */
/*                                                                      */
/************************************************************************/
PRIVATE VOID
RtcSetAggFlagInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry)
{
    tAddrRange         *pAddrRange;
    tPath              *pPath;
    UINT1               u1Tos;
    UINT1               u1Index;
    tArea              *pArea;
    UINT4               u4IpAddrMask;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* LSA cannot be flused in standby node */
        return;
    }

    if (pRtEntry == NULL)
    {
        return;
    }
    pArea = GetFindAreaInCxt (pOspfCxt, &GET_PATH_AREA_ID (pRtEntry));
    if (pArea == NULL)
    {
        return;
    }

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        u4IpAddrMask =
            OSPF_CRU_BMC_DWFROMPDU (pArea->aAddrRange[u1Index].ipAddrMask);

        if ((pArea->aAddrRange[u1Index].areaAggStatus !=
             INVALID)
            &&
            (UtilIpAddrMaskComp
             (pRtEntry->destId, pArea->aAddrRange[u1Index].ipAddr,
              pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL) &&
            (pRtEntry->u4IpAddrMask >= u4IpAddrMask))

        {

            pAddrRange = (&(pArea->aAddrRange[u1Index]));
            if (pAddrRange->areaAggStatus == INVALID)
            {
                break;
            }

            if (!IS_NOT_ADVERTISABLE_ADDR_RANGE (pAddrRange))
            {
                if (IS_DEST_NETWORK (pRtEntry) &&
                    IS_INTRA_AREA_PATH (pRtEntry) &&
                    (UtilIpAddrMaskComp (pRtEntry->destId,
                                         pAddrRange->ipAddr,
                                         pAddrRange->ipAddrMask) == OSPF_EQUAL))
                {

                    OSPF_TRC1 (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                               "Route to dest : %x processed.",
                               OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId));
                    /* update cost for each TOS */

                    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                    {

                        pPath = GET_TOS_PATH (pRtEntry, u1Tos);
                        if ((pPath == NULL)
                            || (pPath->u1PathType != INTRA_AREA))
                        {
                            continue;
                        }

                        if (pAddrRange->aMetric[u1Tos].bStatus == OSPF_VALID)
                        {
                            if (pOspfCxt->rfc1583Compatibility == OSPF_DISABLED)
                            {
                                pAddrRange->aMetric[u1Tos].u4Value
                                    = OSPF_MAX (pPath->u4Cost,
                                                pAddrRange->aMetric[u1Tos].
                                                u4Value);
                            }
                            else
                            {
                                pAddrRange->aMetric[u1Tos].u4Value
                                    = OSPF_MIN (pPath->u4Cost,
                                                pAddrRange->aMetric[u1Tos].
                                                u4Value);
                            }
                        }
                        else
                        {
                            pAddrRange->aMetric[u1Tos].u4Value = pPath->u4Cost;
                            pAddrRange->aMetric[u1Tos].bStatus = OSPF_VALID;
                        }
                    }
                    if (pAddrRange->aMetric[TOS_0].u4Value != LS_INFINITY_24BIT)
                    {
                        pAddrRange->aggFlag = OSPF_TRUE;
                    }

                }

                break;
            }
        }

    }

}

/************************************************************************/
/*                                                                      */
/* Function     : RtcGenerateAggLsa                                     */
/*                                                                      */
/* Description  : This Function Scan the Area List and call             */
/*                function AreaGenerateAggLsa                           */
/*                                                                      */
/* Input        : pArea        :  pointer to the AREA.                  */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns     : None                                                   */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RtcGenerateAggLsaInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;

    if ((pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
        (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            AreaGenerateAggLsa (pArea);
        }
    }
}

/* TRIE callback functions */

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieAddRtEntry                                         */
/*                                                                           */
/* Description  : This is the callback function to add the route entry to    */
/*                OSPF routing table                                         */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by OSPF                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  OSPF specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  OSPF specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                  OSPF specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfTrieAddRtEntry (VOID *pInputParams, VOID *pOutputParams,
                    VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieDeleteRtEntry                                      */
/*                                                                           */
/* Description  : This is the callback function to delete the route entry    */
/*                from OSPF routing table                                    */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by OSPF                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  OSPF specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  OSPF specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                  OSPF specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfTrieDeleteRtEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                       VOID *pOutputParams, VOID *pNextHop, tKey Key)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (pNextHop);
    UNUSED_PARAM (Key);
    (*ppAppSpecInfo) = NULL;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieDelAllRtEntry                                      */
/*                                                                           */
/* Description  : This is the callback function to delete the all the route  */
/*                entries from OSPF routing table. This is used when the     */
/*                entire routing table needs to be cleared.                  */
/*                                                                           */
/* Input        : pInputParams    - Pointer used pointing to input parameters*/
/*                pOutputParams   - Pointer not used by OSPF                 */
/*                VOID (*)(VOID *)- Dummy pointer to the function for future */
/*                                  use.                                     */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : Pointer to the value which helps in deciding whether to    */
/*                delete the route entry at IP level                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1       *
OspfTrieDelAllRtEntry (tInputParams * pInputParams, VOID *dummy,
                       tOutputParams * pOutputParams)
{
    UINT1              *pInpRtInfo;
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (dummy);
    pInpRtInfo = (UINT1 *) pInputParams;
    return (pInpRtInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieSearchRtEntry                                      */
/*                                                                           */
/* Description  : This is the callback function to search the route entry    */
/*                from OSPF routing table                                    */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by OSPF to store the OSPF   */
/*                                  route entry                              */
/*                pAppSpecInfo    - Pointer to the OSPF specific information */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to the output parameters         */
/*                                  which has the route entry information    */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfTrieSearchRtEntry (tInputParams * pInputParams,
                       tOutputParams * pOutputParams, VOID *pAppSpecInfo)
{

    UNUSED_PARAM (pInputParams);

    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieLookUpEntry                                        */
/*                                                                           */
/* Description  : This is the callback function for the lookup of the        */
/*                route entry in OSPF routing table                          */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by OSPF to store the OSPF   */
/*                                  route entry                              */
/*                pAppSpecInfo    - Pointer to the OSPF specific information */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to the output parameters         */
/*                                  which has the route entry information    */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfTrieLookUpEntry (tInputParams * pInputParams, tOutputParams * pOutputParams,
                     VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (key);

    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieBestPAth                                           */
/*                                                                           */
/* Description  : This is the callback function for the lookup of the        */
/*                route entry in OSPF routing table                          */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by OSPF to store the OSPF   */
/*                                  route entry                              */
/*                pAppSpecInfo    - Pointer to the OSPF specific information */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
               VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);
    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTrieGetNextRtEntry                                     */
/*                                                                           */
/* Description  : This is the callback function to get the next route entry  */
/*                from OSPF routing table                                    */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  OSPF specific information present in     */
/*                                  trie leaf node                           */
/*                Key             - Pointer to the key                       */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to store                         */
/*                                  the next route entry which can be        */
/*                                  accessed by module calling TrieGetNext   */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfTrieGetNextRtEntry (tInputParams * pInputParams, VOID *pOutputParams,
                        VOID *ppAppSpecInfo, tKey Key)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (Key);

    /* Copying the pointer to next routing entry in trie to output params */
    ((tOutputParams *) pOutputParams)->pAppSpecInfo = ppAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcSetRtTimer                                              */
/*                                                                           */
/* Description  : This is the function starts ths route calcualtion timer    */
/*                                                                           */
/* Input        : pOspfCxt -   Pointer to Ospf Context                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcSetRtTimer (tOspfCxt * pOspfCxt)
{
    UINT4               u4RtInterval = 0;
    UINT4               u4CurrTimeSTUPS = 0;
    INT4                i4Temp = 0;
    UINT4               u4SpfHoldTimeInterval;
    UINT4               u4SpfInterval;

    /* Start this timer only if Admin Status of the OSPF is enabled in this
     * context. */
    if (pOspfCxt->admnStat == OSPF_DISABLED)
    {
        return;
    }

    /* convert the milli second to STUPS */
    if ((u4SpfHoldTimeInterval =
         OSPF_TIME_IN_STUPS (pOspfCxt->u4SpfHoldTimeInterval)) == 0)
    {
        /* Fsap Timer can be started only with minimum value 10 ms i.e 1 STUPS */
        u4SpfHoldTimeInterval = 1;
    }

    if (pOspfCxt->u4SpfInterval == OSPF_MIN_SPF_DELAY)
    {
        /* pOspfCxt->u4SpfInterval == 0 then there should not *
         *  be any delay when there is any topology change, 
         *  so start the route timer with value as SpfHoldTime */
        TmrSetTimer (&(pOspfCxt->runRtTimer), RUN_RT_TIMER,
                     u4SpfHoldTimeInterval);
        return;
    }

    /* convert the milli second to STUPS */
    if ((u4SpfInterval = OSPF_TIME_IN_STUPS (pOspfCxt->u4SpfInterval)) == 0)
    {
        /* Fsap Timer can be started only with minimum value 10 ms i.e 1 STUPS */
        u4SpfInterval = 1;
    }

    OsixGetSysTime (&u4CurrTimeSTUPS);
    i4Temp =
        (pOspfCxt->u4LastCalTime - u4CurrTimeSTUPS) +
        (INT4) u4SpfHoldTimeInterval;

    if (i4Temp < 0)
    {
        u4RtInterval = u4SpfInterval;
    }
    else if ((UINT4) i4Temp > u4SpfInterval)
    {
        u4RtInterval = i4Temp;
    }
    else
    {
        u4RtInterval = u4SpfInterval;
    }

    TmrSetTimer (&(pOspfCxt->runRtTimer), RUN_RT_TIMER, (u4RtInterval));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGenerateCandteHashKey                                   */
/*                                                                           */
/* Description  : Generates the hash key for cost based hash table           */
/*                                                                           */
/*                                                                           */
/* Input        : pOspfCxt        - Pointer to ospf context                  */
/*                u4Cost          - cost                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Generated hash key                                         */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
RtcGenerateCandteHashKey (tOspfCxt * pOspfCxt, UINT4 u4Cost)
{
    UINT4               u4Bucket;
    UINT4               u4BucketLength;

    /* First update the Max cost and Min cost for the current Spf Calculation */
    if ((pOspfCxt->u4CurrSpfMinCost == 0) && (pOspfCxt->u4CurrSpfMaxCost == 0))
    {
        pOspfCxt->u4CurrSpfMinCost = u4Cost;
        pOspfCxt->u4CurrSpfMaxCost = u4Cost;
    }
    else if (u4Cost < pOspfCxt->u4CurrSpfMinCost)
    {
        pOspfCxt->u4CurrSpfMinCost = u4Cost;
    }
    else if (u4Cost > pOspfCxt->u4CurrSpfMaxCost)
    {
        pOspfCxt->u4CurrSpfMaxCost = u4Cost;
    }

    /* The very first time, both Max and Min are 0 */
    if ((pOspfCxt->u4MaxCost == 0) && (pOspfCxt->u4MinCost == 0))
    {
        u4Bucket = (u4Cost ? CANDTE_COST_HASH_TABLE_SIZE - 1 : 0);
    }
    else
    {
        /* 
           Ci -> current Cost, 
           mincost ->LastSpf Min cost, 
           maxcost ->LastSpf Max cost

           if (Ci < mincost)
           bucket = 0;
           else if (Ci > maxcost)
           bucket = Max Bucket -1;
           else 
           bucket = ((Ci-mincost) / (F (Max-min))/ Total Hash buckets); */

        if (u4Cost < pOspfCxt->u4MinCost)
        {
            u4Bucket = 0;
        }
        else if (u4Cost > pOspfCxt->u4MaxCost)
        {
            u4Bucket = (CANDTE_COST_HASH_TABLE_SIZE - 1);
        }
        else
        {
            u4BucketLength =
                ((pOspfCxt->u4MaxCost -
                  pOspfCxt->u4MinCost) / CANDTE_COST_HASH_TABLE_SIZE);

            if (u4BucketLength == 0)
            {
                u4BucketLength = 1;
            }

            u4Bucket = ((u4Cost - pOspfCxt->u4MinCost) / u4BucketLength);

            if (u4Bucket >= CANDTE_COST_HASH_TABLE_SIZE)
            {
                u4Bucket = (CANDTE_COST_HASH_TABLE_SIZE - 1);
            }
        }
    }

    return u4Bucket;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetRtTosPath                                            */
/*                                                                           */
/* Description  : This function returns the route path for the given TOS     */
/*                                                                           */
/* Input        : pRtEntry - route entry                                     */
/*                1Tos  - route TOS                                          */
/*                u1Flag - Flag to get New Route path or Old Route Path      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the route path or NULL                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC tPath       *
RtcGetRtTosPath (tRtEntry * pRtEntry, INT1 i1Tos, UINT1 u1Flag)
{
    tPath              *pPath = NULL;
    if (u1Flag == OSPF_OLD_PATH)
    {
        if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_MODIFIED)
        {
            pPath =
                (tPath *) TMO_SLL_First (&(pRtEntry->aTosPath[(INT2) i1Tos]));
        }
    }
    else if (u1Flag == OSPF_NEW_PATH)
    {
        if (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_MODIFIED)
        {
            pPath =
                (tPath *) TMO_SLL_Last (&(pRtEntry->aTosPath[(INT2) i1Tos]));
        }
        else if ((pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_ADDED) ||
                 (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_USED) ||
                 (pRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED))
        {
            pPath =
                (tPath *) TMO_SLL_First (&(pRtEntry->aTosPath[(INT2) i1Tos]));
        }

    }

    if ((pPath != NULL) && (pPath->u1Flag == u1Flag))
    {
        return pPath;
    }
    return ((tPath *) NULL);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeleteOldRoutePathInCxt                                 */
/*                                                                           */
/* Description  : This function deletes the route from RTM                   */
/*                 and frees the old route path                              */
/*                                                                           */
/* Input        : pOspfCxt - pointer to ospf cxt                             */
/*              : pRtEntry - route entry                                     */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RtcDeleteOldRoutePathInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry)
{
    tPath              *pRtPath;
    tPath              *pTempRtPath;
    UINT1               u1Tos;
    UINT1               u1HopIndex;
    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        OSPF_DYNM_SLL_SCAN (&(pRtEntry->aTosPath[u1Tos]), pRtPath, pTempRtPath,
                            tPath *)
        {
            if (pRtEntry->u1DestType == DEST_NETWORK)
            {
                if (u1Tos == TOS_0)
                {
                    for (u1HopIndex = 0;
                         u1HopIndex < pRtPath->u1HopCount; u1HopIndex++)
                    {
                        RtmTxRtChngNextHopNtf (pOspfCxt, pRtEntry, u1HopIndex,
                                               pRtPath, RT_ROWSTATUS_MASK);
                    }
                }
            }

            TMO_SLL_Delete (&(pRtEntry->aTosPath[u1Tos]), &pRtPath->nextPath);
            PATH_FREE (pRtPath);

        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcMarkAllRoutes                                           */
/*                                                                           */
/* Description  : This function Marks all the ospf routes                    */
/*                 as OSPF_ROUTE_UNUSED and moves the ABR/ASBR routes to     */
/*                 oldAbrAsbrRoutes List.                                    */
/*                and updates the route flag                                 */
/*                                                                           */
/* Input        : pOspfCxt    -  pointer to Ospf Context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcMarkAllRoutes (tOspfCxt * pOspfCxt)
{
    tRtEntry           *pRtEntry = NULL;
    tRtEntry           *pTempRtEntry = NULL;
    INT1                i1Tos;

    /* Mark all route entries as not found */
    OSPF_DYNM_SLL_SCAN (&(pOspfCxt->pOspfRt->routesList), pRtEntry,
                        pTempRtEntry, tRtEntry *)
    {
        for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
        {
            pRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_UNUSED;
        }

        if (pRtEntry->u1DestType != DEST_NETWORK)
        {
            /* Moving the ABR/ASBR routes to old ABR/ASBR routes list */
            TMO_SLL_Delete (&(pOspfCxt->pOspfRt->routesList),
                            &(pRtEntry->nextRtEntryNode));
            TMO_SLL_Add (&(pOspfCxt->pOspfRt->oldAbrAsbrRtList),
                         &(pRtEntry->nextRtEntryNode));

        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessOldNewRtEntryChanges                             */
/*                                                                           */
/* Description  : This function compares the old and new route               */
/*                and updates the route flag                                 */
/*                                                                           */
/* Input        : pOspfRt       pointer to tOspfRt (stores the ospf routes)  */
/*                pOldRtEntry - old route entry                              */
/*                pNewRtEntry - new route entry                              */
/*                i1Tos  - OSPF route TOS                                    */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcProcessOldNewRtEntryChanges (tOspfRt * pOspfRt,
                                tRtEntry * pOldRtEntry,
                                tRtEntry * pNewRtEntry, INT1 i1Tos,
                                tArea * pArea)
{
    tPath              *pOldPath = NULL;
    tPath              *pNewPath = NULL;
    tRtEntry           *pRtEntry = NULL;
    tPath              *pPath = NULL;
    UINT1               u1NewPathCount = 0;
    UINT1               u1HopIndex = 0;
    tIPADDR             destMask;
    UINT1               i1SamePathExists = OSPF_TRUE;

    u1NewPathCount = (UINT1) (TMO_SLL_Count (&(pNewRtEntry->aTosPath[i1Tos])));

    if ((pOldRtEntry == NULL) && (u1NewPathCount == 0))
    {
        return;
    }
    else if ((pOldRtEntry == NULL) && (u1NewPathCount != 0))
    {
        /* New Route is added to the routing table first time */
        OSPF_CRU_BMC_DWTOPDU (destMask, pNewRtEntry->u4IpAddrMask);

        /* pNewRtEntry - is a local pointer,
         * Allocate memory for the new route "pRtEntry" 
         * and copy the route info from the pNewRtEntry to
         *  the newly created Route pRtEntry */
        if ((pRtEntry = RtcCreateRtEntry (pNewRtEntry->u1DestType,
                                          &(pNewRtEntry->destId),
                                          &destMask,
                                          pNewRtEntry->options)) == NULL)
        {
            /* Route creation Failed so free the pPath memory */
            while ((pPath = (tPath *)
                    TMO_SLL_Get (&(pNewRtEntry->aTosPath[(INT2) i1Tos]))) !=
                   NULL)
            {
                PATH_FREE (pPath);
            }
            return;
        }

        while ((pPath = (tPath *)
                TMO_SLL_Get (&(pNewRtEntry->aTosPath[(INT2) i1Tos]))) != NULL)
        {
            TMO_SLL_Add (&(pRtEntry->aTosPath[(INT2) i1Tos]),
                         &(pPath->nextPath));
        }
        pRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_ADDED;
        RtcAddRtEntry (pOspfRt, pRtEntry);
    }
    /*If the new route comes with the same destination id which has already
       been added (pOldRtEntry) and destination id  is same as router id,
       then mark the old entry as OSPF_ROUTE_USED as it was marked
       OSPF_ROUTE_UNUSED in RtcMarkAllRoutes , return without adding this route.
     */
    else if ((pOldRtEntry != NULL) && ((pArea != NULL) &&
                                       (UtilIpAddrComp
                                        (pNewRtEntry->destId,
                                         pArea->pOspfCxt->rtrId) ==
                                        OSPF_EQUAL)))
    {
        pOldRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_USED;
        return;
    }

    else if ((pOldRtEntry != NULL) &&
             (pOldRtEntry->au1Flag[i1Tos] == OSPF_ROUTE_UNUSED) &&
             (u1NewPathCount != 0))
    {
        /* Compare the Old and New paths of the pOldRtEntry and pNewRtEntry
         *  to find whether Route is changed or not. 
         *  Route Entry Flag will be update to
         *             - OSPF_ROUTE_USED ( - no change in old and new routes)
         OSPF_ROUTE_MODIFIED (Old and new routes differ)*/
        pOldPath = GET_TOS_PATH (pOldRtEntry, i1Tos);
        pNewPath = (tPath *)
            TMO_SLL_Get (&(pNewRtEntry->aTosPath[(INT2) i1Tos]));

        if (pNewPath == NULL)
        {
            /* There is no valid new path 
             * Route remains unused */
            return;
        }

        if (pOldPath == NULL)
        {
            pNewPath->u1Flag = OSPF_NEW_PATH;
            TMO_SLL_Add (&(pOldRtEntry->aTosPath[(INT2) i1Tos]),
                         &(pNewPath->nextPath));
            pOldRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_MODIFIED;

            return;
        }
        else if ((pOldPath->u1PathType == pNewPath->u1PathType) &&
                 (pOldPath->u1HopCount == pNewPath->u1HopCount))
        {
            for (u1HopIndex = 0;
                 u1HopIndex < OSPF_MIN ((pNewPath->u1HopCount),
                                        (MAX_NEXT_HOPS)); u1HopIndex++)
            {
                if (!(IS_EQUAL_ADDR (pNewPath->aNextHops[u1HopIndex].nbrIpAddr,
                                     pOldPath->aNextHops[u1HopIndex].
                                     nbrIpAddr)))
                {
                    i1SamePathExists = OSPF_FALSE;
                }
                if ((pNewPath->aNextHops[u1HopIndex].pInterface != NULL) &&
                    (pOldPath->aNextHops[u1HopIndex].pInterface != NULL) &&
                    (pNewPath->aNextHops[u1HopIndex].pInterface->u4AddrlessIf !=
                     pOldPath->aNextHops[u1HopIndex].pInterface->u4AddrlessIf))
                {
                    i1SamePathExists = OSPF_FALSE;
                }
            }
            if ((i1SamePathExists == OSPF_TRUE) &&
                (pOldPath->u4Cost == pNewPath->u4Cost) &&
                (UtilIpAddrComp (pOldPath->areaId, pNewPath->areaId)
                 == OSPF_EQUAL))
            {
                /* No Change in the Route */
                pOldRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_USED;
                PATH_FREE (pNewPath);
                return;
            }
        }
        /* Route is modified */
        pOldPath->u1Flag = OSPF_OLD_PATH;
        pNewPath->u1Flag = OSPF_NEW_PATH;
        TMO_SLL_Add (&(pOldRtEntry->aTosPath[(INT2) i1Tos]),
                     &(pNewPath->nextPath));
        pOldRtEntry->au1Flag[i1Tos] = OSPF_ROUTE_MODIFIED;

    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcResetAllSpfNodes                                        */
/*                                                                           */
/* Description  : This function marks all the nodes in the SPF as            */
/*                OSPF_SPF_UNUSED                                            */
/*                                                                           */
/* Input        : pArea - pointer to the area for which                      */
/*                the calculation is being performed                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcResetAllSpfNodes (tArea * pArea)
{
    tCandteNode        *pSpfNode = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (pArea->pSpf, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pArea->pSpf, u4HashIndex, pSpfNode, tCandteNode *)
        {
            pSpfNode->u1SpfFlag = OSPF_SPF_UNUSED;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeleteUnusedSpfNodes                                    */
/*                                                                           */
/* Description  : This function deletes all nodes in the SPF that            */
/*                area marked as OSPF_SPF_UNUSED                             */
/*                                                                           */
/* Input        : pArea - pointer to the area for which                      */
/*                the calculation is being performed                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcDeleteUnusedSpfNodes (tArea * pArea)
{
    tCandteNode        *pSpfNode = NULL;
    tCandteNode        *pNextSpfNode = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (pArea->pSpf, u4HashIndex)
    {
        UTL_HASH_Scan_Bucket (pArea->pSpf, u4HashIndex,
                              pSpfNode, pNextSpfNode, tCandteNode *)
        {
            if (pSpfNode->u1SpfFlag == OSPF_SPF_UNUSED)
            {
                TMO_HASH_Delete_Node
                    (pArea->pSpf,
                     &(pSpfNode->candteHashNode),
                     RtcCandteHashFunc (&(pSpfNode->vertexId),
                                        pSpfNode->u1VertType));
                SPF_FREE (pSpfNode);
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeleteNodesFromSpf                                      */
/*                                                                           */
/* Description  : Deletes the nodes from SPF Tree                            */
/*                                                                           */
/* Input        : pSpfTree   :  the SPF tree                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtcDeleteNodesFromSpf (tSpf * pSpfTree)
{
    UINT4               u4Hindex;
    tCandteNode        *pCandteNode;

    TMO_HASH_Scan_Table (pSpfTree, u4Hindex)
    {
        while ((pCandteNode
                = (tCandteNode *) TMO_HASH_Get_First_Bucket_Node (pSpfTree,
                                                                  u4Hindex))
               != NULL)
        {

            TMO_HASH_Delete_Node (pSpfTree, &pCandteNode->candteHashNode,
                                  u4Hindex);
            CANDTE_FREE (pCandteNode);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCheckAllTosFlags                                        */
/*                                                                           */
/* Description  : Checks route flags in all TOS                              */
/*                                                                           */
/* Input        : pRtEntry   :  route entry                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_ROUTE_UNUSED if all the route flags are unused        */
/*                else returns the route flag                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
RtcCheckAllTosFlags (tRtEntry * pRtEntry)
{
    UINT1               u1Tos;
    UINT1               u1Flag = OSPF_ROUTE_UNUSED;

    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (pRtEntry->au1Flag[u1Tos] != OSPF_ROUTE_UNUSED)
        {
            /* This route is used */
            u1Flag = OSPF_ROUTE_USED;
            break;
        }
    }
    return u1Flag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcCheckAndReGenerateLsaInCxt                              */
/*                                                                           */
/* Description  : Generates LSA for the changed routes                       */
/*                                                                           */
/* Input        : pOspfCxt   :  context pointer                              */
/*                pRtEntry   :  pointer to route entry                       */
/*                i1Tos      :  TOS value                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcCheckAndReGenerateLsaInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry,
                               INT1 i1Tos)
{
    OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
              "Route Changed\n");

    if ((pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        /* LSA cannot be regenerated in GR mode
         * or in standby node */
        return;
    }

    if ((IS_DEST_NETWORK (pRtEntry)) || IS_DEST_ASBR (pRtEntry))
    {

        if (GET_PATH_TYPE (pRtEntry, i1Tos) == INTRA_AREA)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Summary LSA Generated Due To Intra Area Route "
                      "  Change\n");
            OlsSignalLsaRegenInCxt (pOspfCxt, SIG_INTRA_AREA_CHANGE,
                                    (UINT1 *) pRtEntry);
        }
        else if (GET_PATH_TYPE (pRtEntry, i1Tos) == INTER_AREA)
        {
            OSPF_TRC (OSPF_RT_TRC | OSPF_RTMODULE_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Summary LSA Generated "
                      "Due To Inter Area Route Change\n");
            OlsSignalLsaRegenInCxt (pOspfCxt, SIG_INTER_AREA_CHANGE,
                                    (UINT1 *) pRtEntry);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcDeleteOldAndUnusedPath                                  */
/*                                                                           */
/* Description  : Deletes unused routes and old paths                        */
/*                                                                           */
/* Input        : pRtEntry   :  context pointer                              */
/*                                                                           */
/* Output       : u1DeleteRtEntry - False  if the route should not be deleted*/
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcDeleteOldAndUnusedPath (tRtEntry * pRtEntry, UINT1 *pu1DeleteRtEntry)
{
    INT1                i1TempTos;
    tPath              *pRtPath;
    tPath              *pTempRtPath;

    for (i1TempTos = 0; i1TempTos < OSPF_MAX_METRIC; i1TempTos++)
    {
        /* Free the Memory for the old route path */
        OSPF_DYNM_SLL_SCAN (&(pRtEntry->aTosPath[i1TempTos]), pRtPath,
                            pTempRtPath, tPath *)
        {
            if ((pRtPath->u1Flag == OSPF_OLD_PATH) ||
                (pRtEntry->au1Flag[i1TempTos] == OSPF_ROUTE_UNUSED))
            {
                TMO_SLL_Delete (&(pRtEntry->aTosPath[i1TempTos]),
                                &pRtPath->nextPath);
                PATH_FREE (pRtPath);

            }
        }
        if (TMO_SLL_Count (&(pRtEntry->aTosPath[i1TempTos])) != 0)
        {
            *pu1DeleteRtEntry = OSPF_FALSE;
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcProcessOldNewPathInCxt                                  */
/*                                                                           */
/* Description  : Process the old and new paths                              */
/*                                                                           */
/* Input        : pOspfCxt   :  context pointer                              */
/*                pRtEntry   :  pointer to routing entry                     */
/*                pRtPath    :  new path                                     */
/*                pOldRtPath :  old path                                     */
/*                pi1Tos     :  TOS value                                    */
/*                                                                           */
/* Output       : pu1RtChanged : Flag to indate that the route is changed    */
/*                pu1Flag      : Flag to indate that the loop must break     */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtcProcessOldNewPathInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry,
                           tPath * pRtPath, tPath * pOldRtPath,
                           INT1 *pi1Tos, UINT1 *pu1RtChanged, UINT1 *pu1Flag)
{
    UINT1               u1HopIndex = 0;
    tArea              *pArea;
    tRtEntry           *pCurrRtEntry;

    if ((pRtPath != NULL) && (pOldRtPath != NULL))
    {

        /* both paths exist */
        pCurrRtEntry = pRtEntry;

        if (pRtPath->u1PathType != pOldRtPath->u1PathType)
        {

            if ((IS_DEST_NETWORK (pCurrRtEntry)) && (*pi1Tos == TOS_0))
            {
                for (u1HopIndex = 0; u1HopIndex < pOldRtPath->u1HopCount;
                     u1HopIndex++)
                {
                    RtmTxRtChngNextHopNtf (pOspfCxt, pRtEntry, u1HopIndex,
                                           pOldRtPath, RT_ROWSTATUS_MASK);
                }
                for (u1HopIndex = 0; u1HopIndex < pRtPath->u1HopCount;
                     u1HopIndex++)
                {
                    RtmTxRtChngNextHopNtf (pOspfCxt, pRtEntry, u1HopIndex,
                                           pRtPath,
                                           RT_IFINDX_MASK | RT_METRIC_MASK);
                }
                if ((pOspfCxt->bAreaBdrRtr == OSPF_TRUE) &&
                    (pOldRtPath->u1PathType == INTER_AREA)
                    && (pRtPath->u1PathType == INTRA_AREA))
                {
                    pArea = RtcGetNextHopArea (pRtPath->aNextHops[0].pInterface,
                                               pRtPath->aNextHops[0].u1Status);
                    if (pArea == NULL)
                    {
                        return;
                    }
                    RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                           &(pRtEntry->destId),
                                           pRtEntry->u4IpAddrMask);
                }

            }

            if ((pOldRtPath->u1PathType == INTRA_AREA) &&
                (pRtPath->u1PathType >= INTER_AREA))
            {
                /* This condition can be true only in the 
                 * case of Cisco or IBM ABR */
                if ((UtilIpAddrComp (pOldRtPath->areaId,
                                     gNullIpAddr) == OSPF_EQUAL) &&
                    (UtilIpAddrComp (pRtPath->areaId, gNullIpAddr) !=
                     OSPF_EQUAL))
                {
                    RtcFlushOutSummaryFromAllAreasInCxt (pOspfCxt,
                                                         NETWORK_SUM_LSA,
                                                         &(pRtEntry->
                                                           destId),
                                                         pRtEntry->
                                                         u4IpAddrMask);
                    return;
                }
                else
                {
                    if (IS_DEST_NETWORK (pRtEntry))
                    {
                        RtcFlushOutSummaryLsa (pOspfCxt->pBackbone,
                                               NETWORK_SUM_LSA,
                                               &(pRtEntry->destId),
                                               pRtEntry->u4IpAddrMask);
                    }
                    else if (IS_DEST_ASBR (pRtEntry))
                    {
                        RtcFlushOutSummaryLsa (pOspfCxt->pBackbone,
                                               ASBR_SUM_LSA,
                                               &(pRtEntry->destId),
                                               pRtEntry->u4IpAddrMask);
                    }
                }

                pArea = RtcGetNextHopArea (pRtPath->aNextHops[0].pInterface,
                                           pRtPath->aNextHops[0].u1Status);

                if ((pArea != NULL) && (IS_AREA_TRANSIT_CAPABLE (pArea)))
                {
                    RtcFlushOutSummaryLsa (pArea, NETWORK_SUM_LSA,
                                           &(pRtEntry->destId),
                                           pRtEntry->u4IpAddrMask);
                }
            }
            *pu1RtChanged = OSPF_TRUE;
            *pu1Flag = OSPF_TRUE;
            return;
        }

        if ((pOspfCxt->u4ABRType != STANDARD_ABR) &&
            (pRtPath->u1PathType == INTER_AREA) &&
            (pOldRtPath->u1PathType == INTER_AREA) &&
            (UtilIpAddrComp (pOldRtPath->areaId, gNullIpAddr)
             != OSPF_EQUAL) &&
            (UtilIpAddrComp (pRtPath->areaId, gNullIpAddr) == OSPF_EQUAL))
        {
            *pu1RtChanged = OSPF_TRUE;
            *pu1Flag = OSPF_TRUE;
            return;
        }

        if (pRtPath->u4Cost != pOldRtPath->u4Cost)
        {
            RtcProcessVirtualLinkInCxt (pOspfCxt,
                                        pRtEntry, VIF_COST_CHANGED,
                                        OSPF_NEW_PATH);
            *pu1RtChanged = OSPF_TRUE;
            if (IS_DEST_NETWORK (pCurrRtEntry) && (*pi1Tos == TOS_0))
            {
                RtcProcessNextHopChangesInCxt (pOspfCxt, pCurrRtEntry, pRtPath,
                                               pRtEntry, pOldRtPath);
            }

            *pu1Flag = OSPF_TRUE;
            return;
        }
        if ((pRtPath->u1PathType == TYPE_2_EXT) &&
            (pRtPath->u4Type2Cost != pOldRtPath->u4Type2Cost) &&
            (*pi1Tos == TOS_0))

        {
            RtcProcessNextHopChangesInCxt (pOspfCxt, pCurrRtEntry, pRtPath,
                                           pRtEntry, pOldRtPath);
            *pu1Flag = OSPF_TRUE;
            return;
        }
        if (IS_DEST_NETWORK (pCurrRtEntry) && (*pi1Tos == TOS_0))
        {

            RtcProcessNextHopChangesInCxt (pOspfCxt, pCurrRtEntry, pRtPath,
                                           pRtEntry, pOldRtPath);

            if (UtilIpAddrComp (pRtPath->areaId, pOldRtPath->areaId)
                != OSPF_EQUAL)
            {
                RtcCheckAddrRangeFlagInCxt (pOspfCxt,
                                            &(pCurrRtEntry->destId),
                                            &(pRtPath->areaId),
                                            &(pRtPath->areaId));
                *pu1RtChanged = OSPF_TRUE;
                *pu1Flag = OSPF_TRUE;
                return;
            }
        }

    }
    else if ((pRtPath != NULL) && (pOldRtPath == NULL))
    {

        *pu1RtChanged = OSPF_TRUE;

        /* newly reachable */
        pCurrRtEntry = pRtEntry;

        if (*pi1Tos == TOS_0)
        {
            RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry, IFE_UP,
                                        OSPF_NEW_PATH);
        }
        else
        {
            RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry,
                                        VIF_COST_CHANGED, OSPF_NEW_PATH);
        }

        if (IS_DEST_NETWORK (pCurrRtEntry))
        {
            RtmTxRtUpdate (pOspfCxt, pCurrRtEntry);
        }
        *pu1Flag = OSPF_TRUE;
        return;
    }
    else if ((pRtPath == NULL) && (pOldRtPath != NULL))
    {

        if (*pi1Tos == TOS_0)
        {
            RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry, IFE_DOWN,
                                        pOldRtPath->u1Flag);
        }
        else
        {
            if (pRtEntry->au1Flag[TOS_0] == OSPF_ROUTE_MODIFIED)
            {
                RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry,
                                            VIF_COST_CHANGED, OSPF_OLD_PATH);
            }
            else
            {
                RtcProcessVirtualLinkInCxt (pOspfCxt, pRtEntry,
                                            VIF_COST_CHANGED, OSPF_NEW_PATH);
            }
        }

        RtcProcessUnreachableRouteInCxt (pOspfCxt, pRtEntry);

        if (*pi1Tos == TOS_0)
        {
            for (u1HopIndex = 0; u1HopIndex < pOldRtPath->u1HopCount;
                 u1HopIndex++)
            {
                RtmTxRtChngNextHopNtf (pOspfCxt, pRtEntry, u1HopIndex,
                                       pOldRtPath, RT_ROWSTATUS_MASK);
            }
        }

        /* Only in this case, the TOS value is set to Tos0 for LSA
           Regeneration. It is used as an output parameter in this case */
        *pi1Tos = TOS_0;

        *pu1RtChanged = OSPF_TRUE;
        *pu1Flag = OSPF_TRUE;
        return;
    }
    else
    {
        /* both paths absent */
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtcGetPathType                                             */
/*                                                                           */
/* Description  : Gets the path type of the routing entry.                   */
/*                                                                           */
/* Input        : pRtEntry   :  pointer to routing entry                     */
/*                i1Tos      :  TOS value                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
RtcGetPathType (tRtEntry * pRtEntry, INT1 i1Tos)
{
    tPath              *pPath = NULL;

    if ((TMO_SLL_Count (&(pRtEntry->aTosPath[i1Tos]))) == 0)
    {
        return INVALID_ROUTE;
    }

    pPath = ((tPath *) TMO_SLL_First (&(pRtEntry->aTosPath[i1Tos])));
    if (pPath != NULL)
    {
        return (pPath->u1PathType);
    }
    return INVALID_ROUTE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file osrtc.c                         */
/*-----------------------------------------------------------------------*/
