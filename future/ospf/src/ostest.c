/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ostest.c,v 1.96 2017/12/21 10:22:06 siva Exp $
 *
 * Description:This file contains the Snmp low level test
 *             routines.
 *
 *******************************************************************/

#include  "osinc.h"
#include  "ospfcli.h"
#include "stdoslow.h"
#include "fsostlow.h"
#include "ospftlow.h"

/* Prototypes specific to ostest.c */

PRIVATE INT1 ValidateOspfAreaTableSetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                        tAreaId * pAreaId));

PRIVATE INT1        ValidateOspfStubAreaTableSetIndexInCxt
PROTO ((tOspfCxt * pOspfCxt, tAreaId * pAreaId, INT1 i1Tos));

PRIVATE INT1 ValidateOspfHostTableSetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                        tIPADDR * pIfIpAddr,
                                                        INT1 i1Tos));

PRIVATE INT1 ValidateOspfHostTableGetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                        tIPADDR * pIfIpAddr,
                                                        INT1 i1Tos));

PRIVATE INT1 ValidateOspfIfTableSetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                      tIPADDR pIfIpAddr,
                                                      UINT4 u4AddrlessIf));

PRIVATE INT1 ValidateOspfIfTableGetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                      tIPADDR pIfIpAddr,
                                                      UINT4 u4AddrlessIf));

PRIVATE INT1        ValidateOspfIfMetricTableSetIndexInCxt
PROTO ((tOspfCxt * pOspfCxt, tIPADDR pIfIpAddr, UINT4 u4AddrlessIf,
        INT1 i1Tos));

PRIVATE INT1 ValidateOspfVirtIfTableSetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                          tAreaId * pAreaId,
                                                          tRouterId * pNbrId));

PRIVATE INT1 ValidateOspfVirtIfTableGetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                          tAreaId * pAreaId,
                                                          tRouterId * pNbrId));

PRIVATE INT1 ValidateOspfNbrTableSetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                       tIPADDR * pNbrIpAddr,
                                                       UINT4 u4NbrAddrlessIf));

PRIVATE INT1 ValidateOspfNbrTableGetIndexInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                       tIPADDR * pNbrIpAddr,
                                                       UINT4 u4NbrAddrlessIf));

PRIVATE INT1        ValidateOspfAreaAggregateTableSetIndexInCxt
PROTO ((tOspfCxt * pOspfCxt, tAreaId * pAreaId,
        INT4 i4LsdbType, tIPADDR * pIpAddr, tIPADDR * pAddrMask));

PRIVATE INT1        ValidateOspfAreaAggregateTableGetIndexInCxt
PROTO ((tOspfCxt * pOspfCxt, tAreaId * pAreaId,
        INT4 i4LsdbType, tIPADDR * pIpAddr, tIPADDR * pAddrMask));

PRIVATE INT1        ValidateOspfExtRouteTableSetIndexInCxt
PROTO ((tOspfCxt * pOspfCxt, tIPADDR * pExtRouteDest, tIPADDR * pExtRouteMask,
        INT1 i1Tos));

PRIVATE INT1        ValidateOspfExtRouteTableGetIndexInCxt
PROTO ((tOspfCxt * pOspfCxt, tIPADDR * pExtRouteDest, tIPADDR * pExtRouteMask,
        INT1 i1Tos));

PRIVATE tArea      *ValidateOspfIsSameAddrRangeConfiguredInCxt
PROTO ((tOspfCxt * pOspfCxt, INT4 i4LsdbType, tIPADDR * pIpAddr,
        tIPADDR * pAddrMask));

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2OspfRouterId
 Input       :  The Indices

                The Object 
                testValOspfRouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfRouterId (UINT4 *pu4ErrorCode, UINT4 u4TestValOspfRouterId)
{
    tRouterId           rtrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    OSPF_GBL_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, OSPF_INVALID_CXT_ID,
                   "Test Routine For OspfRouterId %x\n", u4TestValOspfRouterId);

    OSPF_CRU_BMC_DWTOPDU (rtrId, u4TestValOspfRouterId);

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (IS_VALID_ROUTER_ID (rtrId))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfRouterId Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfRouterId Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_RTR_ID);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfAdminStat
 Input       :  The Indices

                The Object 
                testValOspfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAdminStat (UINT4 *pu4ErrorCode, INT4 i4TestValOspfAdminStat)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    OSPF_GBL_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, OSPF_INVALID_CXT_ID,
                   "Test Routine For OspfAdminStat %d\n",
                   i4TestValOspfAdminStat);
    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (IS_VALID_STATUS_VALUE ((UINT4) i4TestValOspfAdminStat))
    {
        OSPF_GBL_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, OSPF_INVALID_CXT_ID,
                      "Test For OspfAdminStat Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_GBL_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, OSPF_INVALID_CXT_ID,
                  "Test For OspfAdminStat Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfASBdrRtrStatus
 Input       :  The Indices

                The Object 
                testValOspfASBdrRtrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfASBdrRtrStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValOspfASBdrRtrStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4TestValOspfASBdrRtrStatus == OSPF_FALSE)
    {
        if (TMO_SLL_Count (&(pOspfCxt->RedistrRouteConfigLst)) != OSPF_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_OSPF_REDIS_ENABLED);
            return SNMP_FAILURE;
        }

        if (pOspfCxt->u4RrdSrcProtoBitMask & (RRD_SRC_PROTO_BIT_MASK))
        {
            CLI_SET_ERR (CLI_OSPF_REDIS_ENABLED);
            return SNMP_FAILURE;
        }
    }

    OSPF_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test Routine For OspfASBdrRtrStatus %d\n",
               i4TestValOspfASBdrRtrStatus);

    if (IS_VALID_TRUTH_VALUE ((UINT4) i4TestValOspfASBdrRtrStatus))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfASBdrRtrStatus Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfASBdrRtrStatus Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfTOSSupport
 Input       :  The Indices

                The Object 
                testValOspfTOSSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfTOSSupport (UINT4 *pu4ErrorCode, INT4 i4TestValOspfTOSSupport)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test Routine For OspfTOSSupport %d\n", i4TestValOspfTOSSupport);

    if (IS_VALID_TRUTH_VALUE ((UINT4) i4TestValOspfTOSSupport))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfTOSSupport Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfTOSSupport Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfExtLsdbLimit
 Input       :  The Indices

                The Object 
                testValOspfExtLsdbLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfExtLsdbLimit (UINT4 *pu4ErrorCode, INT4 i4TestValOspfExtLsdbLimit)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test Routine For OspfExtLsdbLimit %d\n",
               i4TestValOspfExtLsdbLimit);

    if (IS_VALID_EXT_LSDB_LIMIT (i4TestValOspfExtLsdbLimit))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfExtLsdbLimit Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfExtLsdbLimit Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfMulticastExtensions
 Input       :  The Indices

                The Object 
                testValOspfMulticastExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfMulticastExtensions (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValOspfMulticastExtensions)
{
    UNUSED_PARAM (i4TestValOspfMulticastExtensions);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfExitOverflowInterval
 Input       :  The Indices

                The Object 
                testValOspfExitOverflowInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfExitOverflowInterval (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValOspfExitOverflowInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test Routine For OspfExitOverflowInterval %x\n",
               i4TestValOspfExitOverflowInterval);

    if (IS_VALID_EXIT_OVERFLOW_INT (i4TestValOspfExitOverflowInterval))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "Test For OspfExitOverflowInterval Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId,
              "Test For OspfExitOverflowInterval Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfDemandExtensions
 Input       :  The Indices

                The Object 
                testValOspfDemandExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfDemandExtensions (UINT4 *pu4ErrorCode,
                               INT4 i4TestValOspfDemandExtensions)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test Routine For OspfDemandExtensions %d\n",
               i4TestValOspfDemandExtensions);

    if (IS_VALID_TRUTH_VALUE ((UINT4) i4TestValOspfDemandExtensions))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "Test For OspfDemandExtensions Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfDemandExtensions Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : OspfAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfAreaTable
 Input       :  The Indices
                OspfAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfAreaTable (UINT4 u4OspfAreaId)
{
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC1 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Validate Index For Area Table OspfAreaId %x\n", u4OspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
    if (GetFindAreaInCxt (pOspfCxt, &areaId) != NULL)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Validate Index For Area Table Success\n");
        return SNMP_SUCCESS;
    }
    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Validate Index For Area Table Failed\n");
    CLI_SET_ERR (CLI_OSPF_INV_AREA);
    return SNMP_FAILURE;

}

/*******************************************************************************
* Function : ValidateOspfAreaTableSetIndexInCxt 
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre
*            pAreaId - Identifier for the area.
* Output   : None
* Returns  : Success or Failure
*******************************************************************************/
PRIVATE INT1
ValidateOspfAreaTableSetIndexInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId)
{
    if ((GetFindAreaInCxt (pOspfCxt, pAreaId) != NULL) ||
        (TMO_SLL_Count (&(pOspfCxt->areasLst)) < MAX_AREAS))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2OspfAuthType
 Input       :  The Indices
                OspfAreaId

                The Object 
                testValOspfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAuthType (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaId,
                       INT4 i4TestValOspfAuthType)
{

    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId) !=
         SNMP_SUCCESS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValOspfAuthType);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2OspfImportAsExtern
 Input       :  The Indices
                OspfAreaId

                The Object 
                testValOspfImportAsExtern
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfImportAsExtern (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaId,
                             INT4 i4TestValOspfImportAsExtern)
{
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tArea              *pArea = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfImportAsExtern %d Index AreaId %x \n",
               i4TestValOspfImportAsExtern, u4OspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    switch (i4TestValOspfImportAsExtern)
    {
        case NSSA_AREA:
            pArea = GetFindAreaInCxt (pOspfCxt, &(areaId));
            if (pArea != NULL)
            {
                if (UtilFindIsTransitArea (pArea) == OSPF_TRUE)
                {
                    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "Test For OspfImportAsExtern Failed\n");
                    CLI_SET_ERR (CLI_OSPF_INV_NSSA_VIRT_LINK);
                    return SNMP_FAILURE;
                }
            }
            if (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId)
                == SNMP_SUCCESS)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfImportAsExtern Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfImportAsExtern Failed\n");
            CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
            break;
        case NORMAL_AREA:
        case STUB_AREA:
            if (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId)
                == SNMP_SUCCESS)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfImportAsExtern Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfImportAsExtern Failed\n");
            CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfImportAsExtern Failed\n");
            CLI_SET_ERR (CLI_OSPF_INV_AREA);
            return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfAreaSummary
 Input       :  The Indices
                OspfAreaId

                The Object 
                testValOspfAreaSummary
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaSummary (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaId,
                          INT4 i4TestValOspfAreaSummary)
{
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfAreaSummary %d Index AreaId %x \n",
               i4TestValOspfAreaSummary, u4OspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((i4TestValOspfAreaSummary == NO_AREA_SUMMARY ||
         i4TestValOspfAreaSummary == SEND_AREA_SUMMARY) &&
        (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId)
         == SNMP_SUCCESS))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfAreaSummary Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfAreaSummary Failed\n");
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfAreaStatus
 Input       :  The Indices
                OspfAreaId

                The Object 
                testValOspfAreaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaId,
                         INT4 i4TestValOspfAreaStatus)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfAreaStatus %d Index AreaId %x \n",
               i4TestValOspfAreaStatus, u4OspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
    if (UtilIpAddrComp (areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        /* Silver Creek Change 2.4.3.1 */
        switch (i4TestValOspfAreaStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case NOT_READY:
            case CREATE_AND_GO:
            case CREATE_AND_WAIT:
            case DESTROY:

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Configuration is not allowed on Backbone\n");
                CLI_SET_ERR (CLI_OSPF_INV_AREA);
                return SNMP_FAILURE;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
        }
    }
    if (i4TestValOspfAreaStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &(areaId))
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfAreaStatus Failed\n");
        CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
        return SNMP_FAILURE;
    }
    switch ((UINT1) i4TestValOspfAreaStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (GetFindAreaInCxt (pOspfCxt, &areaId) != NULL)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfAreaStatus Failed\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Test For OspfAreaStatus Success\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pArea = GetFindAreaInCxt (pOspfCxt, &(areaId))) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfAreaStatus Failed\n");
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pArea->areaStatus == NOT_IN_SERVICE) ||
                     (pArea->areaStatus == ACTIVE) ||
                     (pArea->areaStatus == NOT_READY))
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfAreaStatus Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfAreaStatus Failed\n");
            CLI_SET_ERR (CLI_OSPF_INV_STATUS);
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;
            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfAreaStatus Failed\n");
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : OspfStubAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfStubAreaTable
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfStubAreaTable (UINT4 u4OspfStubAreaId,
                                           INT4 i4OspfStubTOS)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
               pOspfCxt->u4OspfCxtId,
               "Validate Index For StubAreaTable OspfStubAreaId %x OspfStubTOS %d\n",
               u4OspfStubAreaId, i4OspfStubTOS);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);

    if ((IS_VALID_TOS_VALUE ((INT1) i4OspfStubTOS)) &&
        ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL) &&
        ((pArea->u4AreaType == NSSA_AREA) ||
         (pArea->u4AreaType == STUB_AREA)) &&
        (IS_VALID_ROWSTATUS_VALUE
         (pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
          rowStatus)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "Validate Index For StubAreaTable Success\n");
        return SNMP_SUCCESS;
    }
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId,
              "Validate Index For StubAreaTable Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_AREA);
    return SNMP_FAILURE;

}

/*******************************************************************************
* Function : ValidateOspfStubAreaTableSetIndexInCxt 
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre
*            pAreaId - Identifier for the area.
             i1Tos    - Type of service
* Output   : None
* Returns  : Success or Failure
*******************************************************************************/

PRIVATE INT1
ValidateOspfStubAreaTableSetIndexInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId,
                                        INT1 i1Tos)
{

    if (!IS_VALID_TOS_VALUE (i1Tos))
    {
        return SNMP_FAILURE;
    }

    if (GetFindAreaInCxt (pOspfCxt, pAreaId) != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2OspfStubMetric
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                testValOspfStubMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfStubMetric (UINT4 *pu4ErrorCode, UINT4 u4OspfStubAreaId,
                         INT4 i4OspfStubTOS, INT4 i4TestValOspfStubMetric)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfStubMetric %d Index StubAreaId %x TOS %d\n",
               i4TestValOspfStubMetric, u4OspfStubAreaId, i4OspfStubTOS);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfStubMetric Failure\n");
        CLI_SET_ERR (CLI_OSPF_AREA_NOT_CONFIG);
        return SNMP_FAILURE;
    }

    if (pArea->u4AreaType != NSSA_AREA && pArea->u4AreaType != STUB_AREA)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_DEF_COST);
        return SNMP_FAILURE;
    }

    if ((IS_VALID_BIG_METRIC (i4TestValOspfStubMetric)) &&
        (ValidateOspfStubAreaTableSetIndexInCxt (pOspfCxt, &areaId,
                                                 (INT1) i4OspfStubTOS)
         == SNMP_SUCCESS) && pArea->u4AreaType != NORMAL_AREA)

    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfStubMetric Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfStubMetric Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_METRIC);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfStubStatus
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                testValOspfStubStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfStubStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfStubAreaId,
                         INT4 i4OspfStubTOS, INT4 i4TestValOspfStubStatus)
{

    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfStubStatus %d Index StubAreaId %x TOS %d\n",
               i4TestValOspfStubStatus, u4OspfStubAreaId, i4OspfStubTOS);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);

    if (i4TestValOspfStubStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (ValidateOspfStubAreaTableSetIndexInCxt (pOspfCxt, &areaId,
                                                (INT1) i4OspfStubTOS)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfStubStatus Failure\n");
        CLI_SET_ERR (CLI_OSPF_AREA_NOT_CONFIG);
        return SNMP_FAILURE;
    }

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfStubStatus Failure\n");
        CLI_SET_ERR (CLI_OSPF_INV_AREA);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValOspfStubStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus
                != INVALID)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfStubStatus Failure\n");
                CLI_SET_ERR (CLI_OSPF_INV_STATUS);
                return SNMP_FAILURE;
            }
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Test For OspfStubStatus Success\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            if (pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus
                == INVALID)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfStubStatus Failure\n");
                CLI_SET_ERR (CLI_OSPF_INV_STATUS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfStubStatus Failure\n");
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfStubMetricType
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                testValOspfStubMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfStubMetricType (UINT4 *pu4ErrorCode, UINT4 u4OspfStubAreaId,
                             INT4 i4OspfStubTOS,
                             INT4 i4TestValOspfStubMetricType)
{

    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfStubMetricType %d Index StubAreaId %x TOS %d\n",
               i4TestValOspfStubMetricType, u4OspfStubAreaId, i4OspfStubTOS);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);

    switch (i4TestValOspfStubMetricType)
    {

        case OSPF_METRIC:
        case TYPE1EXT_METRIC:
        case TYPE2EXT_METRIC:

            if (ValidateOspfStubAreaTableSetIndexInCxt (pOspfCxt, &areaId,
                                                        (INT1) i4OspfStubTOS)
                == SNMP_SUCCESS)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfStubMetricType Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfStubMetricType Failure\n");
            CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfStubMetricType Failure\n");
            CLI_SET_ERR (CLI_OSPF_INV_STATUS);
            return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : OspfLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfLsdbTable
 Input       :  The Indices
                OspfLsdbAreaId
                OspfLsdbType
                OspfLsdbLsid
                OspfLsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfLsdbTable (UINT4 u4OspfLsdbAreaId,
                                       INT4 i4OspfLsdbType,
                                       UINT4 u4OspfLsdbLsid,
                                       UINT4 u4OspfLsdbRouterId)
{
    tAreaId             areaId;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC4 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Valdt OspfLsdbAreaId %x OspfLsdbType %d OspfLsdbLsid %x"
               " OspfLsdbRouterId %x\n",
               u4OspfLsdbAreaId, i4OspfLsdbType, u4OspfLsdbLsid,
               u4OspfLsdbRouterId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfLsdbAreaId);
    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfLsdbRouterId);

    pArea = (tArea *) NULL;
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "Validate Index For LsdbTable Failed\n");
        CLI_SET_ERR (CLI_OSPF_INV_AREA);
        return SNMP_FAILURE;
    }

    if (LsuSearchDatabase ((UINT1) i4OspfLsdbType, &linkStateId,
                           &advRtrId, (UINT1 *) NULL, (UINT1 *) pArea) != NULL)
    {

        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Validate Index For LsdbTable Succeeded\n");
        return SNMP_SUCCESS;
    }
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Validate Index For LsdbTable Failed\n");
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2OspfAreaRangeMask
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                testValOspfAreaRangeMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaRangeMask (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaRangeAreaId,
                            UINT4 u4OspfAreaRangeNet,
                            UINT4 u4TestValOspfAreaRangeMask)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (u4TestValOspfAreaRangeMask);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;    /*   NEED_MACRO  */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfAreaRangeStatus
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                testValOspfAreaRangeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaRangeStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaRangeAreaId,
                              UINT4 u4OspfAreaRangeNet,
                              INT4 i4TestValOspfAreaRangeStatus)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (i4TestValOspfAreaRangeStatus);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;    /* NEED_MACRO */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfAreaRangeEffect
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                testValOspfAreaRangeEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaRangeEffect (UINT4 *pu4ErrorCode, UINT4 u4OspfAreaRangeAreaId,
                              UINT4 u4OspfAreaRangeNet,
                              INT4 i4TestValOspfAreaRangeEffect)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (i4TestValOspfAreaRangeEffect);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;        /* NEED_MACRO */
}

/* LOW LEVEL Routines for Table : OspfHostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfHostTable
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfHostTable (UINT4 u4OspfHostIpAddress,
                                       INT4 i4OspfHostTOS)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Validate Index For HostTable OspfHostIpAddress %x OspfHostTOS %d\n",
               u4OspfHostIpAddress, i4OspfHostTOS);

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);

    if (IS_VALID_TOS_VALUE ((INT1) i4OspfHostTOS))
    {
        if (((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) != NULL) &&
            (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus
             != INVALID))
        {
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Validate Index For HostTable Succeeded\n");
            return SNMP_SUCCESS;
        }
    }
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Validate Index For HostTable Failed\n");
    return SNMP_FAILURE;

}

/******************************************************************************
* Function : ValidateOspfHostTableSetIndexInCxt                               *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pHostIpAddr - Ip addr of the host                                *
*            i1Tos         - Type of Service                                  *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfHostTableSetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR * pHostIpAddr,
                                    INT1 i1Tos)
{
    tHost              *pHost = NULL;
    UNUSED_PARAM (i1Tos);
    pHost = GetFindHostInCxt (pOspfCxt, pHostIpAddr);
    /* Host Entry is not created and number of host
     * entries are less than MAX_HOSTS */
    if ((pHost == NULL) && (TMO_SLL_Count (&(pOspfCxt->hostLst)) < MAX_HOSTS))
    {
        return SNMP_SUCCESS;
    }
    /* Host entry is present and not active for 
     * this tos i1Tos */
    else if ((pHost != NULL) &&
             (pHost->aHostCost[DECODE_TOS (i1Tos)].rowStatus == INVALID))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/******************************************************************************
* Function : ValidateOspfHostTableGetIndexInCxt                               *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pHostIpAddr - Ip addr of the host                                *
*            i1Tos         - Type of Service                                  *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfHostTableGetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR * pHostIpAddr,
                                    INT1 i1Tos)
{
    tHost              *pHost;

    if (!(IS_VALID_TOS_VALUE (i1Tos)))
    {
        return SNMP_FAILURE;
    }
    if (((pHost = GetFindHostInCxt (pOspfCxt, pHostIpAddr)) != NULL) &&
        (pHost->aHostCost[DECODE_TOS (i1Tos)].rowStatus != INVALID))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfHostMetric
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                testValOspfHostMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfHostMetric (UINT4 *pu4ErrorCode, UINT4 u4OspfHostIpAddress,
                         INT4 i4OspfHostTOS, INT4 i4TestValOspfHostMetric)
{
    tIPADDR             hostIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfHostMetric %d Indices OspfHostIpAddress %x"
               " OspfHostTOS %d \n",
               i4TestValOspfHostMetric, u4OspfHostIpAddress, i4OspfHostTOS);
    /* to not allow address of Class D,E */
    if (!((IS_CLASS_A_ADDR (u4OspfHostIpAddress)) ||
          (IS_CLASS_B_ADDR (u4OspfHostIpAddress))
          || (IS_CLASS_C_ADDR (u4OspfHostIpAddress))))
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);
    if ((ValidateOspfHostTableGetIndexInCxt (pOspfCxt, &hostIpAddr,
                                             (INT1) i4OspfHostTOS) ==
         SNMP_SUCCESS) && (IS_VALID_METRIC (i4TestValOspfHostMetric)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfHostMetric Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfHostMetric Failure\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfHostStatus
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                testValOspfHostStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfHostStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfHostIpAddress,
                         INT4 i4OspfHostTOS, INT4 i4TestValOspfHostStatus)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfHostStatus %d Indices OspfHostIpAddress %x"
               " OspfHostTOS %d \n",
               i4TestValOspfHostStatus, u4OspfHostIpAddress, i4OspfHostTOS);

    if (i4TestValOspfHostStatus == DESTROY)
        return SNMP_SUCCESS;

    if (!(IS_VALID_TOS_VALUE (i4OspfHostTOS)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Test For OspfHostStatus Failure\n");
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);

    switch ((UINT1) i4TestValOspfHostStatus)
    {
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfHostStatus Failure\n");
            CLI_SET_ERR (CLI_OSPF_INV_STATUS);
            return SNMP_FAILURE;

        case CREATE_AND_WAIT:
            if (ValidateOspfHostTableSetIndexInCxt (pOspfCxt, &hostIpAddr,
                                                    (INT1) i4OspfHostTOS)
                == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfHostStatus Failure\n");
                CLI_SET_ERR (CLI_OSPF_INV_VALUE);
                return SNMP_FAILURE;
            }
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfHostStatus Success\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfHostStatus Failure\n");
                return SNMP_FAILURE;
            }
            if (ValidateOspfHostTableGetIndexInCxt (pOspfCxt, &hostIpAddr,
                                                    (INT1) i4OspfHostTOS)
                == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfHostStatus Failure\n");
                CLI_SET_ERR (CLI_OSPF_INV_VALUE);
                return SNMP_FAILURE;
            }
            else if ((pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].
                      rowStatus == NOT_IN_SERVICE)
                     || (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].
                         rowStatus == ACTIVE))
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfHostStatus Success\n");
                return SNMP_SUCCESS;
            }
            else if ((pHost->aHostCost
                      [DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus ==
                      NOT_READY)
                     && (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].
                         rowMask == HOST_METRIC_MASK))
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfHostStatus Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Test For OspfHostStatus Failure\n");
            CLI_SET_ERR (CLI_OSPF_INV_VALUE);
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : OspfIfTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfIfTable
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfIfTable (UINT4 u4OspfIfIpAddress,
                                     INT4 i4OspfAddressLessIf)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if (GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf)
        != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/******************************************************************************
* Function : ValidateOspfIfTableSetIndexInCxt                                 *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pIfIpAddr - Ip addr of the interface                             *
*            u4AddrlessIf - AddressLess Interface                             *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/

PRIVATE INT1
ValidateOspfIfTableSetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR pIfIpAddr,
                                  UINT4 u4AddrlessIf)
{
    if ((GetFindIfInCxt (pOspfCxt, pIfIpAddr, u4AddrlessIf) != NULL) ||
        (TMO_SLL_Count (&(pOspfCxt->sortIfLst)) <
         FsOSPFSizingParams[MAX_OSPF_INTERFACES_SIZING_ID].u4PreAllocatedUnits))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/******************************************************************************
* Function : ValidateOspfIfTableGetIndexInCxt                                 *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pIfIpAddr - Ip addr of the interface                             *
*            u4AddrlessIf - AddressLess Interface                             *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfIfTableGetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR pIfIpAddr,
                                  UINT4 u4AddrlessIf)
{
    if (GetFindIfInCxt (pOspfCxt, pIfIpAddr, u4AddrlessIf) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfIfAreaId
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfAreaId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfAreaId (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                       INT4 i4OspfAddressLessIf, UINT4 u4TestValOspfIfAreaId)
{
    tIPADDR             ifIpAddr;
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfAreaId %x Indices OspfIfIpAddress %x "
               " OspfAddressLessIf %d\n",
               u4TestValOspfIfAreaId, u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4TestValOspfIfAreaId);

    if (ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                          (UINT4) i4OspfAddressLessIf) ==
        SNMP_SUCCESS)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfAreaId Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfAreaId Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_AREA);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfType
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfType (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                     INT4 i4OspfAddressLessIf, INT4 i4TestValOspfIfType)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tTMO_SLL_NODE      *pNbrNode;
    tInterface         *pInterface;
    tNeighbor          *pNbr;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValOspfIfType);

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfType %d Indices OspfIfIpAddress %x "
               " OspfAddressLessIf %d\n",
               i4TestValOspfIfType, u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

            if (pNbr->u1ConfigStatus == CONFIGURED_NBR)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_AREA_NEIGHBOR);
                return SNMP_FAILURE;
            }
        }
    }

    if ((ValidateOspfIfTableSetIndexInCxt
         (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf) == SNMP_SUCCESS)
        && IS_VALID_IF_TYPE_VALUE (i4TestValOspfIfType))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfType Succeeds\n");
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfType Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_IFTYPE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfAdminStat
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfAdminStat (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                          INT4 i4OspfAddressLessIf,
                          INT4 i4TestValOspfIfAdminStat)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfAdminStat %d Indices OspfIfIpAddress %x"
               " OspfAddrLessIf %d\n",
               i4TestValOspfIfAdminStat, u4OspfIfIpAddress,
               i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS)
        && (IS_VALID_STATUS_VALUE ((UINT4) i4TestValOspfIfAdminStat)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfAdminStat Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfAdminStat Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfIfRtrPriority
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfRtrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfRtrPriority (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                            INT4 i4OspfAddressLessIf,
                            INT4 i4TestValOspfIfRtrPriority)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfRtrPriority %d Indices OspfIfIpAddress %x"
               " OspfAddrLessIf %d\n",
               i4TestValOspfIfRtrPriority, u4OspfIfIpAddress,
               i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (IS_VALID_PRIORITY (i4TestValOspfIfRtrPriority)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfRtrPriority Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfRtrPriority Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfTransitDelay
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfTransitDelay (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                             INT4 i4OspfAddressLessIf,
                             INT4 i4TestValOspfIfTransitDelay)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfTransitDelay %d Indices OspfIfIpAddress %x"
               " OspfAddrLessIf %d\n",
               i4TestValOspfIfTransitDelay, u4OspfIfIpAddress,
               i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (IS_VALID_AGE (i4TestValOspfIfTransitDelay)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfTransitDelay Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfTransitDelay Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfRetransInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfRetransInterval (UINT4 *pu4ErrorCode,
                                UINT4 u4OspfIfIpAddress,
                                INT4 i4OspfAddressLessIf,
                                INT4 i4TestValOspfIfRetransInterval)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfRtrnsIntrvl %d Indics OspfIfIpAddr %x "
               " OspfAddrLssIf %d\n",
               i4TestValOspfIfRetransInterval, u4OspfIfIpAddress,
               i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (IS_VALID_AGE (i4TestValOspfIfRetransInterval)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfRetransInterval Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfRetransInterval Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfHelloInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfHelloInterval (UINT4 *pu4ErrorCode,
                              UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                              INT4 i4TestValOspfIfHelloInterval)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfHelloInterval %d "
               "Indices OspfIfIpAddress %x"
               "OspfAddressLessIf %d\n",
               i4TestValOspfIfHelloInterval,
               u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (IS_VALID_HELLO_RANGE (i4TestValOspfIfHelloInterval)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfHelloInterval Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfHelloInterval Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfRtrDeadInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                UINT4 u4OspfIfIpAddress,
                                INT4 i4OspfAddressLessIf,
                                INT4 i4TestValOspfIfRtrDeadInterval)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfRtrDeadInterval %d "
               "Indices OspfIfIpAddress %x "
               "OspfAddressLessIf %d\n",
               i4TestValOspfIfRtrDeadInterval,
               u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf) == SNMP_SUCCESS) && (i4TestValOspfIfRtrDeadInterval >= OSPF_MIN_DEAD_INTERVAL)    /* Dead interval range (1 - 65535) */
        && (i4TestValOspfIfRtrDeadInterval <= OSPF_MAX_DEAD_INTERVAL))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfRtrDeadInterval Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfRtrDeadInterval Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfPollInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfPollInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfPollInterval (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                             INT4 i4OspfAddressLessIf,
                             INT4 i4TestValOspfIfPollInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4TestValOspfIfPollInterval = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfPollInterval %d"
               "Indices OspfIfIpAddress %x"
               "OspfAddressLessIf %d\n",
               i4TestValOspfIfPollInterval,
               u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        if (pInterface->u1NetworkType != IF_NBMA)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        u4TestValOspfIfPollInterval = (UINT4) i4TestValOspfIfPollInterval;
        if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                               (UINT4) i4OspfAddressLessIf) ==
             SNMP_SUCCESS)
            && (IS_VALID_POSITIVE_INT (u4TestValOspfIfPollInterval)))
        {
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Test For OspfIfPollInterval Succeeds\n");

            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfPollInterval Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfAuthType
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfAuthType (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                         INT4 i4OspfAddressLessIf, INT4 i4TestValOspfIfAuthType)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfAuthType %d Indices OspfIfIpAddress %x"
               " OspfAddressLessIf %d\n",
               i4TestValOspfIfAuthType, u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (i4TestValOspfIfAuthType == NO_AUTHENTICATION
                           || i4TestValOspfIfAuthType == SIMPLE_PASSWORD))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfAuthType Succeeds\n");
        return SNMP_SUCCESS;
    }
    if (i4TestValOspfIfAuthType == CRYPT_AUTHENTICATION)
    {
        pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                     (UINT4) i4OspfAddressLessIf);
        if (pInterface)
        {
            if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) > 0)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfAuthType Succeeds\n");
                return (SNMP_SUCCESS);
            }
            CLI_SET_ERR (CLI_OSPF_NO_MD5_KEY);
        }
    }
    else
    {
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfAuthType Fails\n");
    return SNMP_FAILURE;

}

INT1
nmhTestv2OspfIfCryptoAuthType (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                               INT4 i4OspfAddressLessIf,
                               INT4 i4TestValOspfIfCryptoAuthType)
{

    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfCryptoAuthType %d Indices OspfIfIpAddress %x"
               " OspfAddressLessIf %d\n",
               i4TestValOspfIfCryptoAuthType, u4OspfIfIpAddress,
               i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) !=
         SNMP_SUCCESS))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfCryptoAuthType Failed\n");
        return SNMP_FAILURE;
    }
    if (i4TestValOspfIfCryptoAuthType >= OSPF_MIN_CRYPTOTYPE_VALUE &&
        i4TestValOspfIfCryptoAuthType <= OSPF_MAX_CRYPTOTYPE_VALUE)
    {
        pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                     (UINT4) i4OspfAddressLessIf);
        if (pInterface)
        {
            if (pInterface->u2AuthType == CRYPT_AUTHENTICATION)
            {
                if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) > 0)
                {
                    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                              pOspfCxt->u4OspfCxtId,
                              "Test For OspfAuthIfAuthType Succeeds\n");
                    return (SNMP_SUCCESS);
                }
                CLI_SET_ERR (CLI_OSPF_NO_MD5_KEY);
            }
        }
    }
    else
    {
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfCryptoAuthType Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfAuthKey
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfAuthKey (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                        INT4 i4OspfAddressLessIf,
                        tSNMP_OCTET_STRING_TYPE * pTestValOspfIfAuthKey)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfAuthKey %x Indices OspfIfIpAddr %x"
               " OspfAddrLssIf %d\n",
               pTestValOspfIfAuthKey, u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (pTestValOspfIfAuthKey->i4_Length <= AUTH_KEY_SIZE))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfAuthKey Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfAuthKey Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfStatus
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                       INT4 i4OspfAddressLessIf, INT4 i4TestValOspfIfStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4IfIndex = 0;
    UINT4               u4CxtId = 0;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfStatus %d Indices OspfIfIpAddress %x "
               "OspfAddressLessIf %d\n", i4TestValOspfIfStatus,
               u4OspfIfIpAddress, i4OspfAddressLessIf);
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((i4TestValOspfIfStatus == DESTROY)
        || (i4TestValOspfIfStatus == NOT_IN_SERVICE))
    {
        if (((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf)) !=
             NULL) && (pInterface->secondaryIP.i1SecIpCount != 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId, "Test For OspfIfStatus Fails\n");
            CLI_SET_ERR (CLI_OSPF_PRI_INV_DELETE);
            return SNMP_FAILURE;
        }
        else if (i4TestValOspfIfStatus == DESTROY)
        {
            return SNMP_SUCCESS;
        }
    }
    if (ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                          (UINT4) i4OspfAddressLessIf) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfStatus Fails\n");
        CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
        return SNMP_FAILURE;
    }
    if (pOspfCxt->admnStat != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfStatus Fails\n");
        CLI_SET_ERR (CLI_OSPF_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    switch ((UINT1) i4TestValOspfIfStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (ValidateOspfIfTableGetIndexInCxt
                (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf)
                == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Fails\n");
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }

            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: ValidIpInterfaceInCxt\n");

            if (ValidIpInterfaceInCxt (pOspfCxt->u4OspfCxtId,
                                       ifIpAddr,
                                       (UINT4) i4OspfAddressLessIf) !=
                SNMP_SUCCESS)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "FUNC Exit : ValidIpInterfaceInCxt\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Fails\n");
                CLI_SET_ERR (CLI_OSPF_INTERFACE_NOT_EXISTS);
                return SNMP_FAILURE;
            }
            else
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "FUNC Exit : ValidIpInterfaceInCxt\n");
            }

            if (i4OspfAddressLessIf == 0)
            {
                if (NetIpv4GetIfIndexFromAddrInCxt (pOspfCxt->u4OspfCxtId,
                                                    u4OspfIfIpAddress,
                                                    &u4IfIndex) ==
                    NETIPV4_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
                    return SNMP_FAILURE;
                }

                if (NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo)
                    == NETIPV4_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    CLI_SET_ERR (CLI_OSPF_SECONDARY_IP_NOT_ALLOWED);
                    return OSPF_FAILURE;
                }

                if (NetIpIfInfo.u4Addr != u4OspfIfIpAddress)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    CLI_SET_ERR (CLI_OSPF_SECONDARY_IP_NOT_ALLOWED);
                    return SNMP_FAILURE;
                }

            }
            else
            {
                if (NetIpv4GetPortFromIfIndex (i4OspfAddressLessIf, &u4IfIndex)
                    == NETIPV4_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
                    return SNMP_FAILURE;
                }
                /*checking whether the interface is an unnumbered interface or not */
                if ((NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo) ==
                     NETIPV4_SUCCESS) && (NetIpIfInfo.u4Addr != 0))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (CLI_OSPF_NOT_UNNUM_IF);
                    return SNMP_FAILURE;
                }
            }

            /* Check if this interface is mapped to the corresponding
             * context */
            if (NetIpv4GetCxtId (u4IfIndex, &u4CxtId) == NETIPV4_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_VALUE);
                return SNMP_FAILURE;
            }

            if (u4CxtId != pOspfCxt->u4OspfCxtId)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_VALUE);
                return SNMP_FAILURE;
            }

            OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Test For OspfIfStatus Succeeds\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                              (UINT4) i4OspfAddressLessIf)) ==
                NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Fails\n");
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if (pInterface->ifStatus == NOT_IN_SERVICE ||
                     pInterface->ifStatus == ACTIVE)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }
            else if ((pInterface->ifStatus == NOT_READY) &&
                     (pInterface->rowMask == IF_TABLE_STD_MASK))
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }
            break;
        case NOT_IN_SERVICE:

            if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                              (UINT4) i4OspfAddressLessIf)) ==
                NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Fails\n");
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if (pInterface->ifStatus == NOT_IN_SERVICE ||
                     pInterface->ifStatus == ACTIVE)
            {
                OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Test For OspfIfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }
            break;
        default:
            break;
            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC,
              pOspfCxt->u4OspfCxtId, "Test For OspfIfStatus Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfMulticastForwarding
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfMulticastForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfMulticastForwarding (UINT4 *pu4ErrorCode,
                                    UINT4 u4OspfIfIpAddress,
                                    INT4 i4OspfAddressLessIf,
                                    INT4 i4TestValOspfIfMulticastForwarding)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValOspfIfMulticastForwarding);
    UNUSED_PARAM (u4OspfIfIpAddress);
    UNUSED_PARAM (i4OspfAddressLessIf);

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfMulticastForwarding %d "
               "Indices OspfIfIpAddress %x OspfAddressLessIf %d\n",
               i4TestValOspfIfMulticastForwarding, u4OspfIfIpAddress,
               i4OspfAddressLessIf);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;    /*NEED_MACRO */
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfMulticastForwarding Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfDemand
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                testValOspfIfDemand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfDemand (UINT4 *pu4ErrorCode, UINT4 u4OspfIfIpAddress,
                       INT4 i4OspfAddressLessIf, INT4 i4TestValOspfIfDemand)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (*pu4ErrorCode);

    OSPF_TRC3 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfIfDemand %d Indices OspfIfIpAddr %x"
               " OspfAddrLessIf %d\n",
               i4TestValOspfIfDemand, u4OspfIfIpAddress, i4OspfAddressLessIf);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4) i4OspfAddressLessIf) ==
         SNMP_SUCCESS) && (IS_VALID_TRUTH_VALUE (i4TestValOspfIfDemand)))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfDemand Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For OspfIfDemand Fails\n");
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : OspfIfMetricTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfIfMetricTable
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfIfMetricTable (UINT4 u4OspfIfMetricIpAddress,
                                           INT4 i4OspfIfMetricAddressLessIf,
                                           INT4 i4OspfIfMetricTOS)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);

    if (IS_VALID_TOS_VALUE ((INT1) i4OspfIfMetricTOS) &&
        ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                       (UINT4) i4OspfIfMetricAddressLessIf))
         != NULL)
        && (pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
            rowStatus != INVALID))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/******************************************************************************
* Function : ValidateOspfIfMetricTableSetIndexInCxt                           *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pIfIpAddr - Ip addr of the interface                             *
*            u4AddrlessIf                                                     *
*            i1Tos       - Type of Service                                    *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfIfMetricTableSetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR pIfIpAddr,
                                        UINT4 u4AddrlessIf, INT1 i1Tos)
{
    if (IS_VALID_TOS_VALUE (i1Tos) &&
        (GetFindIfInCxt (pOspfCxt, pIfIpAddr, u4AddrlessIf) != NULL))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2OspfIfMetricValue
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS

                The Object 
                testValOspfIfMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfMetricValue (UINT4 *pu4ErrorCode,
                            UINT4 u4OspfIfMetricIpAddress,
                            INT4 i4OspfIfMetricAddressLessIf,
                            INT4 i4OspfIfMetricTOS,
                            INT4 i4TestValOspfIfMetricValue)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);
    if ((ValidateOspfIfMetricTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                                 i4OspfIfMetricAddressLessIf,
                                                 (INT1) i4OspfIfMetricTOS) ==
         SNMP_SUCCESS) && IS_VALID_METRIC (i4TestValOspfIfMetricValue))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfIfMetricStatus
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS

                The Object 
                testValOspfIfMetricStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfIfMetricStatus (UINT4 *pu4ErrorCode,
                             UINT4 u4OspfIfMetricIpAddress,
                             INT4 i4OspfIfMetricAddressLessIf,
                             INT4 i4OspfIfMetricTOS,
                             INT4 i4TestValOspfIfMetricStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->admnStat != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For OspfIfStatus Fails\n");
        CLI_SET_ERR (CLI_OSPF_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);
    if (i4TestValOspfIfMetricStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }
    if (ValidateOspfIfMetricTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                                (UINT4)
                                                i4OspfIfMetricAddressLessIf,
                                                (INT1) i4OspfIfMetricTOS) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfIfMetricAddressLessIf))
        == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }
    switch ((UINT1) i4TestValOspfIfMetricStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (pInterface->aIfOpCost
                [DECODE_TOS ((INT1) i4OspfIfMetricTOS)].rowStatus != INVALID)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
            if (pInterface->aIfOpCost
                [DECODE_TOS ((INT1) i4OspfIfMetricTOS)].rowStatus == INVALID)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_TOS_STATUS);
                return SNMP_FAILURE;
            }
            else if ((pInterface->aIfOpCost
                      [DECODE_TOS ((INT1) i4OspfIfMetricTOS)].rowStatus ==
                      NOT_IN_SERVICE)
                     ||
                     (pInterface->aIfOpCost
                      [DECODE_TOS ((INT1) i4OspfIfMetricTOS)].rowStatus ==
                      ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_STATUS);
                return SNMP_FAILURE;
            }

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : OspfVirtIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfVirtIfTable
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfVirtIfTable (UINT4 u4OspfVirtIfAreaId,
                                         UINT4 u4OspfVirtIfNeighbor)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if (GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/******************************************************************************
* Function : ValidateOspfVirtIfTableSetIndexInCxt                             *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pAreaId - Identifier for area                                    *
*            pNbrId  - Router Id of Neighbour                                 *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/

PRIVATE INT1
ValidateOspfVirtIfTableSetIndexInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId,
                                      tRouterId * pNbrId)
{
    tArea              *pArea;

    pArea = GetFindAreaInCxt (pOspfCxt, pAreaId);
    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    if (GetFindVirtIfInCxt (pOspfCxt, pAreaId, pNbrId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    if ((IS_VALID_ROUTER_ID (*pNbrId))
        && (TMO_SLL_Count (&(pOspfCxt->virtIfLst)) < MAX_VIRTUAL_IFS)
        && (!(IS_STUB_AREA (pArea))))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/******************************************************************************
* Function : ValidateOspfVirtIfTableGetIndexInCxt                             *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pAreaId - Identifier for area                                    *
*            pNbrId  - Router Id of Neighbour                                 *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfVirtIfTableGetIndexInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId,
                                      tRouterId * pNbrId)
{
    if (GetFindVirtIfInCxt (pOspfCxt, pAreaId, pNbrId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfTransitDelay
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfTransitDelay (UINT4 *pu4ErrorCode,
                                 UINT4 u4OspfVirtIfAreaId,
                                 UINT4 u4OspfVirtIfNeighbor,
                                 INT4 i4TestValOspfVirtIfTransitDelay)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId) ==
         SNMP_SUCCESS) && (IS_VALID_AGE (i4TestValOspfVirtIfTransitDelay)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfRetransInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfRetransInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4OspfVirtIfAreaId,
                                    UINT4 u4OspfVirtIfNeighbor,
                                    INT4 i4TestValOspfVirtIfRetransInterval)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId) ==
         SNMP_SUCCESS) && (IS_VALID_AGE (i4TestValOspfVirtIfRetransInterval)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfHelloInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfHelloInterval (UINT4 *pu4ErrorCode,
                                  UINT4 u4OspfVirtIfAreaId,
                                  UINT4 u4OspfVirtIfNeighbor,
                                  INT4 i4TestValOspfVirtIfHelloInterval)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId)
         == SNMP_SUCCESS)
        && (IS_VALID_HELLO_RANGE (i4TestValOspfVirtIfHelloInterval)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfRtrDeadInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4OspfVirtIfAreaId,
                                    UINT4 u4OspfVirtIfNeighbor,
                                    INT4 i4TestValOspfVirtIfRtrDeadInterval)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId)
         == SNMP_SUCCESS)
        && (IS_VALID_POS_INTEGER (i4TestValOspfVirtIfRtrDeadInterval)))
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfAuthType
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfAuthType (UINT4 *pu4ErrorCode,
                             UINT4 u4OspfVirtIfAreaId,
                             UINT4 u4OspfVirtIfNeighbor,
                             INT4 i4TestValOspfVirtIfAuthType)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId)
         == SNMP_SUCCESS)
        && ((UINT2) i4TestValOspfVirtIfAuthType == NO_AUTHENTICATION
            || (UINT2) i4TestValOspfVirtIfAuthType == SIMPLE_PASSWORD))
    {
        return SNMP_SUCCESS;
    }
    if (i4TestValOspfVirtIfAuthType == CRYPT_AUTHENTICATION)
    {
        pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
        if (!pInterface)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_OSPF_INV_VIRT_AREA);
            return SNMP_FAILURE;
        }
        if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) > 0)
        {
            return (SNMP_SUCCESS);
        }
        CLI_SET_ERR (CLI_OSPF_NO_MD5_KEY);
    }
    else
    {
        CLI_SET_ERR (CLI_OSPF_INV_AUTHTYPE);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfAuthKey
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfAuthKey (UINT4 *pu4ErrorCode, UINT4 u4OspfVirtIfAreaId,
                            UINT4 u4OspfVirtIfNeighbor,
                            tSNMP_OCTET_STRING_TYPE * pTestValOspfVirtIfAuthKey)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId)
         == SNMP_SUCCESS)
        && (pTestValOspfVirtIfAuthKey->i4_Length <= AUTH_KEY_SIZE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    CLI_SET_ERR (CLI_OSPF_INV_LENGTH);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfVirtIfStatus
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                testValOspfVirtIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfVirtIfStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfVirtIfAreaId,
                           UINT4 u4OspfVirtIfNeighbor,
                           INT4 i4TestValOspfVirtIfStatus)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if (i4TestValOspfVirtIfStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (ValidateOspfVirtIfTableSetIndexInCxt (pOspfCxt, &areaId, &nbrId)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VIRT_AREA);
        return SNMP_FAILURE;
    }
    switch ((UINT1) i4TestValOspfVirtIfStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if ((pInterface->u1NetworkType == IF_VIRTUAL)
                && (UtilIpAddrComp (nbrId, pInterface->destRtrId) ==
                    OSPF_EQUAL))
            {
                if ((pInterface->u4IfIndex == 0) &&
                    (pInterface->u4AddrlessIf == 0))
                {
                    CLI_SET_ERR (CLI_OSPF_VIRT_LINK_EXISTS);
                    return SNMP_FAILURE;
                }
            }
        }

            if (ValidateOspfVirtIfTableGetIndexInCxt
                (pOspfCxt, &areaId, &nbrId) == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pInterface = GetFindVirtIfInCxt (pOspfCxt,
                                                  &areaId, &nbrId)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if (pInterface->ifStatus == NOT_IN_SERVICE ||
                     pInterface->ifStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            else if (pInterface->ifStatus == NOT_READY &&
                     pInterface->rowMask == IF_TABLE_STD_MASK)
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_VAREA_STATUS);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : OspfNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfNbrTable
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfNbrTable (UINT4 u4OspfNbrIpAddr,
                                      INT4 i4OspfNbrAddressLessIndex)
{
    tIPADDR             nbrIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);

    if (GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                         (UINT4) i4OspfNbrAddressLessIndex) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/******************************************************************************
* Function : ValidateOspfNbrTableSetIndexInCxt                                *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pNbrIpAddr - IP addr of neighbour                                *
*            u4NbrAddrlessIf - Neighbour's addressless interface              *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfNbrTableSetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR * pNbrIpAddr,
                                   UINT4 u4NbrAddrlessIf)
{
    if (GetFindNbrInCxt (pOspfCxt, pNbrIpAddr, u4NbrAddrlessIf) != NULL)
    {
        return SNMP_SUCCESS;
    }
    else if (TMO_SLL_Count (&(pOspfCxt->sortNbrLst)) < MAX_OSPF_NBRS_LIMIT)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */
/******************************************************************************
* Function : ValidateOspfNbrTableGetIndexInCxt                                *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pNbrIpAddr - IP addr of neighbour                                *
*            u4NbrAddrlessIf - Neighbour's addressless interface              *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfNbrTableGetIndexInCxt (tOspfCxt * pOspfCxt, tIPADDR * pNbrIpAddr,
                                   UINT4 u4NbrAddrlessIf)
{
    if (GetFindNbrInCxt (pOspfCxt, pNbrIpAddr, u4NbrAddrlessIf) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfNbrPriority
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                testValOspfNbrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfNbrPriority (UINT4 *pu4ErrorCode, UINT4 u4OspfNbrIpAddr,
                          INT4 i4OspfNbrAddressLessIndex,
                          INT4 i4TestValOspfNbrPriority)
{
    tIPADDR             nbrIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);

    if ((ValidateOspfNbrTableSetIndexInCxt (pOspfCxt, &nbrIpAddr,
                                            i4OspfNbrAddressLessIndex) ==
         SNMP_SUCCESS) && (IS_VALID_PRIORITY (i4TestValOspfNbrPriority)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfNbmaNbrStatus
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                testValOspfNbmaNbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfNbmaNbrStatus (UINT4 *pu4ErrorCode, UINT4 u4OspfNbrIpAddr,
                            INT4 i4OspfNbrAddressLessIndex,
                            INT4 i4TestValOspfNbmaNbrStatus)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               bInterfaceFound = OSPF_FALSE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);

    if (i4TestValOspfNbmaNbrStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (ValidateOspfNbrTableSetIndexInCxt (pOspfCxt, &nbrIpAddr,
                                           (UINT4) i4OspfNbrAddressLessIndex)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }
    switch ((UINT1) i4TestValOspfNbmaNbrStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (ValidateOspfNbrTableGetIndexInCxt (pOspfCxt, &nbrIpAddr,
                                                   (UINT4)
                                                   i4OspfNbrAddressLessIndex)
                == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }

            /*Neighbor can be configured on NBMA or point-to-multipoint networks */
            TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
            {
                pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

                if (UtilIpAddrMaskComp (pInterface->ifIpAddr,
                                        nbrIpAddr,
                                        pInterface->ifIpAddrMask) == OSPF_EQUAL)
                {
                    bInterfaceFound = OSPF_TRUE;
                    if ((pInterface->u1NetworkType != IF_NBMA) &&
                        (pInterface->u1NetworkType != IF_PTOMP))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        CLI_SET_ERR (CLI_OSPF_NBR_INV_IFACE);
                        return SNMP_FAILURE;
                    }
                }
                if (UtilIpAddrComp (pInterface->ifIpAddr, nbrIpAddr) ==
                    OSPF_EQUAL)
                {
                    CLI_SET_ERR (CLI_OSPF_NBR_LOCAL_IFACE);
                    return SNMP_FAILURE;
                }
            }
            if (bInterfaceFound == OSPF_FALSE)
            {
                CLI_SET_ERR (CLI_OSPF_INV_NET);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                         (UINT4) i4OspfNbrAddressLessIndex)) ==
                NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if (pNbr->nbrStatus == NOT_IN_SERVICE ||
                     pNbr->nbrStatus == ACTIVE || pNbr->nbrStatus == NOT_READY)
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_STATUS);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : OspfVirtNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfVirtNbrTable
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfVirtNbrTable (UINT4 u4OspfVirtNbrArea,
                                          UINT4 u4OspfVirtNbrRtrId)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    return ValidateOspfVirtIfTableGetIndexInCxt (pOspfCxt, &transitAreaId,
                                                 &nbrId);
}

/******************************************************************************
* Function : ValidateOspfAreaAggregateTableSetIndexInCxt                      *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pAreaId - Identifier for area                                    *
*            i4LsdbType - type of link state data base                        *
*            pIpAddr -  IP address                                            *
*            pAddrMask - address mask                                         *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfAreaAggregateTableSetIndexInCxt (tOspfCxt * pOspfCxt,
                                             tAreaId * pAreaId,
                                             INT4 i4LsdbType,
                                             tIPADDR * pIpAddr,
                                             tIPADDR * pAddrMask)
{
    tArea              *pArea = NULL;
    UINT1               u1Index;
    INT1                i1EntryFree = OSPF_FALSE;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (UtilIpAddrComp (*pAreaId, pArea->areaId) == OSPF_EQUAL)
        {
            break;
        }
    }

    if (pArea != NULL)
    {

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {

            /* if entry exists return success */
            if ((pArea->aAddrRange[u1Index].areaAggStatus != INVALID) &&
                (UtilIpAddrComp (*pIpAddr,
                                 pArea->aAddrRange[u1Index].ipAddr)
                 == OSPF_EQUAL)
                &&
                (UtilIpAddrComp
                 (*pAddrMask,
                  pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL)
                && ((UINT1) i4LsdbType ==
                    pArea->aAddrRange[u1Index].u1LsdbType))

            {
                return SNMP_SUCCESS;
            }

            if (pArea->aAddrRange[u1Index].areaAggStatus == INVALID)
                i1EntryFree = OSPF_TRUE;
        }
        /* if free entry exists return success */
        if (i1EntryFree == OSPF_TRUE)
            return SNMP_SUCCESS;
        else
        {
            CLI_SET_ERR (CLI_OSPF_EXCEEDS_MAX_ADDR_RANGES_PER_AREA);
            return SNMP_FAILURE;
        }
    }
    CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/******************************************************************************
* Function : ValidateOspfAreaAggregateTableGetIndexInCxt                      *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pAreaId - Identifier for area                                    *
*            i4LsdbType - type of link state data base                        *
*            pIpAddr -  IP address                                            *
*            pAddrMask - address mask                                         *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfAreaAggregateTableGetIndexInCxt (tOspfCxt * pOspfCxt,
                                             tAreaId * pAreaId,
                                             INT4 i4LsdbType,
                                             tIPADDR * pIpAddr,
                                             tIPADDR * pAddrMask)
{
    tArea              *pArea;
    INT1                i1Index;

    if ((pArea = GetFindAreaInCxt (pOspfCxt, pAreaId)) != NULL)
    {
        if (GetFindAreaAggrIndex (pArea, pIpAddr, pAddrMask,
                                  (UINT1) i4LsdbType, &i1Index) != OSPF_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/******************************************************************************
* Function : ValidateOspfIsSameAddrRangeConfiguredInCxt                       *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            i4LsdbType - type of link state data base                        *
*            pIpAddr -  IP address                                            *
*            pAddrMask - address mask                                         *
* Output   : None                                                             *
* Returns  : Area Pointer or NULL                                             *
******************************************************************************/
PRIVATE tArea      *
ValidateOspfIsSameAddrRangeConfiguredInCxt (tOspfCxt * pOspfCxt,
                                            INT4 i4LsdbType,
                                            tIPADDR * pIpAddr,
                                            tIPADDR * pAddrMask)
{
    tArea              *pArea;
    INT1                i1Index;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (GetFindAreaAggrIndex (pArea, pIpAddr, pAddrMask,
                                  (UINT1) i4LsdbType, &i1Index) != OSPF_FAILURE)
        {
            return pArea;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  nmhTestv2OspfAreaAggregateStatus
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask

                The Object 
                testValOspfAreaAggregateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaAggregateStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4OspfAreaAggregateAreaID,
                                  INT4 i4OspfAreaAggregateLsdbType,
                                  UINT4 u4OspfAreaAggregateNet,
                                  UINT4 u4OspfAreaAggregateMask,
                                  INT4 i4TestValOspfAreaAggregateStatus)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    tArea              *pLstArea;
    UINT4               u4NetworkAddr = 0;
    INT1                i1Index;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);
    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if (i4TestValOspfAreaAggregateStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_AREA_NOT_CONFIG);
        return SNMP_FAILURE;
    }

    if (ValidateOspfAreaAggregateTableSetIndexInCxt
        (pOspfCxt, &areaId, i4OspfAreaAggregateLsdbType, &ipAddr,
         &addrMask) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Check whether given range is a network IP address  */
    u4NetworkAddr = u4OspfAreaAggregateNet & (~(u4OspfAreaAggregateMask));
    if (u4NetworkAddr != 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INCONS_ADDR_MASK);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValOspfAreaAggregateStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if ((pLstArea = ValidateOspfIsSameAddrRangeConfiguredInCxt
                 (pOspfCxt, i4OspfAreaAggregateLsdbType, &ipAddr, &addrMask))
                != NULL)
            {
                OSPF_TRC1 (OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                           " !!!!! Same Address Range is configured in Area %08x    !!!!! \n",
                           OSPF_CRU_BMC_DWFROMPDU (pLstArea->areaId));
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (ValidateOspfAreaAggregateTableGetIndexInCxt (pOspfCxt, &areaId,
                                                             i4OspfAreaAggregateLsdbType,
                                                             &ipAddr,
                                                             &addrMask)
                == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
            if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                      (UINT1)
                                      i4OspfAreaAggregateLsdbType,
                                      &i1Index) != OSPF_FAILURE)
            {
                if ((pArea->aAddrRange[i1Index].areaAggStatus
                     == NOT_IN_SERVICE) ||
                    (pArea->aAddrRange[i1Index].areaAggStatus == ACTIVE))
                {
                    return SNMP_SUCCESS;
                }
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfAreaAggregateEffect
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask

                The Object 
                testValOspfAreaAggregateEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfAreaAggregateEffect (UINT4 *pu4ErrorCode,
                                  UINT4 u4OspfAreaAggregateAreaID,
                                  INT4 i4OspfAreaAggregateLsdbType,
                                  UINT4 u4OspfAreaAggregateNet,
                                  UINT4 u4OspfAreaAggregateMask,
                                  INT4 i4TestValOspfAreaAggregateEffect)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);
    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if ((IS_VALID_AGG_SFFECT (i4TestValOspfAreaAggregateEffect)) &&
        (ValidateOspfAreaAggregateTableGetIndexInCxt (pOspfCxt, &areaId,
                                                      i4OspfAreaAggregateLsdbType,
                                                      &ipAddr,
                                                      &addrMask) ==
         SNMP_SUCCESS))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/* low level routines for the extension MIB */
/****************************************************************************
 Function    :  nmhTestv2FutOspfMaxLsaSize
 Input       :  The Indices

                The Object 
                testValFutOspfMaxLsaSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMaxLsaSize (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFutOspfMaxLsaSize)
{
    UNUSED_PARAM (i4TestValFutOspfMaxLsaSize);
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfMaxAreas
 Input       :  The Indices

                The Object 
                testValFutOspfMaxAreas
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMaxAreas (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfMaxAreas)
{
    UNUSED_PARAM (i4TestValFutOspfMaxAreas);
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfMaxLSAperArea
 Input       :  The Indices

                The Object 
                testValFutOspfMaxLSAperArea
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMaxLSAperArea (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFutOspfMaxLSAperArea)
{
    UNUSED_PARAM (i4TestValFutOspfMaxLSAperArea);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfMaxExtLSAs
 Input       :  The Indices

                The Object 
                testValFutOspfMaxExtLSAs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMaxExtLSAs (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFutOspfMaxExtLSAs)
{
    UNUSED_PARAM (i4TestValFutOspfMaxExtLSAs);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfMaxSelfOrgLSAs
 Input       :  The Indices

                The Object 
                testValFutOspfMaxSelfOrgLSAs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMaxSelfOrgLSAs (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFutOspfMaxSelfOrgLSAs)
{
    UNUSED_PARAM (i4TestValFutOspfMaxSelfOrgLSAs);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfMaxRoutes
 Input       :  The Indices

                The Object 
                testValFutOspfMaxRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMaxRoutes (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfMaxRoutes)
{
    UNUSED_PARAM (i4TestValFutOspfMaxRoutes);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfTraceLevel
 Input       :  The Indices

                The Object 
                testValFutOspfTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfTraceLevel (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFutOspfTraceLevel)
{
    if (((UINT4) i4TestValFutOspfTraceLevel < OSPF_DBG_MIN_TRC) ||
        ((UINT4) i4TestValFutOspfTraceLevel > OSPF_DBG_MAX_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfMinLsaInterval
 Input       :  The Indices

                The Object 
                testValFutOspfMinLsaInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfMinLsaInterval (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFutOspfMinLsaInterval)
{
    if (IS_VALID_MIN_LSA_TIME (i4TestValFutOspfMinLsaInterval))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfABRType
 Input       :  The Indices

                The Object
                testValFutOspfABRType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfABRType (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfABRType)
{
    if (IS_VALID_ABR_TYPE (i4TestValFutOspfABRType))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfNssaAsbrDefRtTrans
 Input       :  The Indices

                The Object 
                testValFutOspfNssaAsbrDefRtTrans
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfNssaAsbrDefRtTrans (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFutOspfNssaAsbrDefRtTrans)
{

    if (IS_VALID_TRUTH_VALUE (i4TestValFutOspfNssaAsbrDefRtTrans))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfDefaultPassiveInterface
 Input       :  The Indices

                The Object
                testValFutOspfDefaultPassiveInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfDefaultPassiveInterface (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFutOspfDefaultPassiveInterface)
{
    if (IS_VALID_TRUTH_VALUE (i4TestValFutOspfDefaultPassiveInterface))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfSpfHoldtime
 Input       :  The Indices

                The Object
                testValFutOspfSpfHoldtime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfSpfHoldtime (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFutOspfSpfHoldtime)
{
    if ((i4TestValFutOspfSpfHoldtime > OSPF_MAX_SPF_HOLDTIME) ||
        (i4TestValFutOspfSpfHoldtime < OSPF_MIN_SPF_HOLDTIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfSpfDelay
 Input       :  The Indices

                The Object
                testValFutOspfSpfDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfSpfDelay (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfSpfDelay)
{
    if ((i4TestValFutOspfSpfDelay > OSPF_MAX_SPF_DELAY) ||
        (i4TestValFutOspfSpfDelay < OSPF_MIN_SPF_DELAY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRestartSupport
 Input       :  The Indices

                The Object
                testValFutOspfRestartSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRestartSupport (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFutOspfRestartSupport)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfRestartSupport < OSPF_RESTART_NONE) ||
        (i4TestValFutOspfRestartSupport > OSPF_RESTART_BOTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((i4TestValFutOspfRestartSupport == OSPF_RESTART_PLANNED) ||
         (i4TestValFutOspfRestartSupport == OSPF_RESTART_BOTH)) &&
        (pOspfCxt->u1OpqCapableRtr == OSPF_FALSE))
    {
        /* Restart cannot be supported if opaque capability
         * is false */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_OPQ_DISABLED);
        return SNMP_FAILURE;
    }
    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSPF_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRestartInterval
 Input       :  The Indices

                The Object
                testValFutOspfRestartInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRestartInterval (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFutOspfRestartInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSPF_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfRestartInterval < OSPF_GR_MIN_INTERVAL) ||
        (i4TestValFutOspfRestartInterval > OSPF_GR_MAX_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRestartStrictLsaChecking
 Input       :  The Indices

                The Object
                testValFutOspfRestartStrictLsaChecking
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FutOspfRestartStrictLsaChecking
    (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfRestartStrictLsaChecking)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!(((pOspfCxt->u1HelperSupport) & OSPF_GRACE_UNKNOWN) ||
          ((pOspfCxt->u1HelperSupport) & OSPF_GRACE_SW_RESTART) ||
          ((pOspfCxt->u1HelperSupport) & OSPF_GRACE_SW_RELOADUPGRADE) ||
          ((pOspfCxt->u1HelperSupport) & OSPF_GRACE_SW_REDUNDANT)))
    {
        CLI_SET_ERR (CLI_OSPF_HELPER_DISABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfRestartStrictLsaChecking != OSPF_TRUE) &&
        (i4TestValFutOspfRestartStrictLsaChecking != OSPF_FALSE))
    {
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfHelperSupport
 Input       :  The Indices

                The Object
                testValFutOspfHelperSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfHelperSupport (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFutOspfHelperSupport)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (*pTestValFutOspfHelperSupport->pu1_OctetList == 0)
    {
        return SNMP_SUCCESS;
    }
    if (((*pTestValFutOspfHelperSupport->pu1_OctetList
          & OSPF_GRACE_UNKNOWN) ||
         (*pTestValFutOspfHelperSupport->pu1_OctetList
          & OSPF_GRACE_SW_RESTART) ||
         (*pTestValFutOspfHelperSupport->pu1_OctetList
          & OSPF_GRACE_SW_RELOADUPGRADE) ||
         (*pTestValFutOspfHelperSupport->pu1_OctetList
          & OSPF_GRACE_SW_REDUNDANT)))
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtTraceLevel
 Input       :  The Indices

                The Object
                testValFutOspfExtTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtTraceLevel (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFutOspfExtTraceLevel)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (((UINT4) i4TestValFutOspfExtTraceLevel < OSPF_DBG_MIN_TRC) ||
        ((UINT4) i4TestValFutOspfExtTraceLevel > OSPF_MAX_EXT_TRC))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (((i4TestValFutOspfExtTraceLevel & OSPF_RESTART_TRC) != OSPF_RESTART_TRC)
        && ((i4TestValFutOspfExtTraceLevel & OSPF_HELPER_TRC) !=
            OSPF_HELPER_TRC)
        && ((i4TestValFutOspfExtTraceLevel & OSPF_REDUNDANCY_TRC) !=
            OSPF_REDUNDANCY_TRC) && (i4TestValFutOspfExtTraceLevel != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhTestv2FutospfRouterIdPermanence
  Input       :  The Indices
                 The Object
                 testValFutospfRouterIdPermanence
  Output      :  The Test Low Lev Routine Take the Indices &                                                      Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                           SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)                                            SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)                     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutospfRouterIdPermanence (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFutospfRouterIdPermanence)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFutospfRouterIdPermanence);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfHelperGraceTimeLimit
 Input       :  The Indices

                The Object 
                testValFutOspfHelperGraceTimeLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfHelperGraceTimeLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFutOspfHelperGraceTimeLimit)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(((pOspfCxt->u1HelperSupport) & OSPF_GRACE_UNKNOWN) ||
          ((pOspfCxt->u1HelperSupport) & OSPF_GRACE_SW_RESTART) ||
          ((pOspfCxt->u1HelperSupport) & OSPF_GRACE_SW_RELOADUPGRADE) ||
          ((pOspfCxt->u1HelperSupport) & OSPF_GRACE_SW_REDUNDANT)))
    {
        CLI_SET_ERR (CLI_OSPF_HELPER_DISABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfHelperGraceTimeLimit < OSPF_GR_HELPER_MIN_INTERVAL) ||
        (i4TestValFutOspfHelperGraceTimeLimit > OSPF_GR_HELPER_MAX_INTERVAL))
    {
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRestartAckState
 Input       :  The Indices

                The Object 
                testValFutOspfRestartAckState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRestartAckState (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFutOspfRestartAckState)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSPF_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfRestartAckState < OSPF_ENABLED) ||
        (i4TestValFutOspfRestartAckState > OSPF_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfGraceLsaRetransmitCount
 Input       :  The Indices

                The Object 
                testValFutOspfGraceLsaRetransmitCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfGraceLsaRetransmitCount (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFutOspfGraceLsaRetransmitCount)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSPF_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfGraceLsaRetransmitCount >= 0) &&
        (i4TestValFutOspfGraceLsaRetransmitCount <= MAX_GRACE_LSA_SENT))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRestartReason
 Input       :  The Indices

                The Object 
                testValFutOspfRestartReason
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRestartReason (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFutOspfRestartReason)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSPF_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfRestartReason == OSPF_GR_UNKNOWN) ||
        (i4TestValFutOspfRestartReason == OSPF_GR_SW_RESTART) ||
        (i4TestValFutOspfRestartReason == OSPF_GR_SW_UPGRADE) ||
        (i4TestValFutOspfRestartReason == OSPF_GR_SW_RED))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRTStaggeringInterval
 Input       :  The Indices

                The Object 
                testValFutOspfRTStaggeringInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRTStaggeringInterval (UINT4 *pu4ErrorCode,
                                      UINT4
                                      u4TestValFutOspfRTStaggeringInterval)
{

    if (gOsRtr.u4RTStaggeringStatus != OSPF_STAGGERING_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_STAG_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    if ((u4TestValFutOspfRTStaggeringInterval < OSPF_MIN_STAGGERING_INTERVAL) ||
        (u4TestValFutOspfRTStaggeringInterval > OSPF_MAX_STAGGERING_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhTestv2FutOspfBfdStatus
 *  Input       :  The Indices
 *
 *                 The Object
 *                 testValFutOspfBfdStatus
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *                 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FutOspfBfdStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfBfdStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfBfdStatus != OSPF_BFD_ENABLED) &&
        (i4TestValFutOspfBfdStatus != OSPF_BFD_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhTestv2FutOspfBfdAllIfState
 *  Input       :  The Indices
 *
 *                 The Object
 *                 testValFutOspfBfdAllIfState
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  ****************************************************************************/
INT1
nmhTestv2FutOspfBfdAllIfState (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFutOspfBfdAllIfState)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfBfdAllIfState != OSPF_BFD_ENABLED) &&
        (i4TestValFutOspfBfdAllIfState != OSPF_BFD_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u1BfdAdminStatus == OSPF_BFD_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_BFD_ADMIN_STATUS_DISABLED);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRTStaggeringStatus
 Input       :  The Indices

                The Object 
                testValFutOspfRTStaggeringStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRTStaggeringStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFutOspfRTStaggeringStatus)
{
    if ((i4TestValFutOspfRTStaggeringStatus != OSPF_STAGGERING_ENABLED) &&
        (i4TestValFutOspfRTStaggeringStatus != OSPF_STAGGERING_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAreaNSSATranslatorRole
 Input       :  The Indices
                OspfAreaId

                The Object 
                testValOspfAreaNSSATranslatorRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAreaNSSATranslatorRole (UINT4 *pu4ErrorCode,
                                        UINT4 u4FutOspfAreaId,
                                        INT4
                                        i4TestValFutOspfAreaNSSATranslatorRole)
{
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tArea              *pArea;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfAreaNSSATranslatorRole %d Index AreaId %x \n",
               i4TestValFutOspfAreaNSSATranslatorRole, u4FutOspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);
    if (pOspfCxt->admnStat != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);
    if (pArea == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_AREA_NOT_CONFIG);
        return SNMP_FAILURE;
    }

    if (pArea->u4AreaType != NSSA_AREA)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_TRNS_ROLE);
        return SNMP_FAILURE;
    }

    if (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }

    switch (i4TestValFutOspfAreaNSSATranslatorRole)
    {
        case (TRNSLTR_ROLE_ALWAYS):
        case (TRNSLTR_ROLE_CANDIDATE):
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_VALUE);
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval
 Input       :  The Indices
                OspfAreaId

                The Object 
                testValOspfAreaNSSATranslatorStabilityInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval (UINT4 *pu4ErrorCode,
                                                     UINT4 u4FutOspfAreaId,
                                                     INT4
                                                     i4TestValFutOspfAreaNSSATranslatorStabilityInterval)
{
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tArea              *pArea;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfAreaNSSATranslatorRole %d Index AreaId %x \n",
               i4TestValFutOspfAreaNSSATranslatorStabilityInterval,
               u4FutOspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);

    if (pArea == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_AREA_NOT_CONFIG);
        return SNMP_FAILURE;
    }
    if (pArea->u4AreaType != NSSA_AREA)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_STAB_INT);
        return SNMP_FAILURE;
    }

    if (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (IS_VALID_POS_INTEGER
        (i4TestValFutOspfAreaNSSATranslatorStabilityInterval))
        return SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
   Function    :  nmhTestv2FutOspfAreaDfInfOriginate
   Input       :  The Indices
                   FutOspfAreaId
            The Object
           testValFutOspfAreaDfInfOriginate
   Output      :  The Test Low Lev Routine Take the Indices & Test whether
           that Value is Valid Input for Set. Stores the value of
           error code in the Return val 
   Error Codes : The following error codes are to be returned 
                  SNMP_ERR_WRONG_LENGTH
          ref:(4 of Sect 4.2.5 of rfc1905)
          SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
          SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
          SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
          SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
   Returns     : SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAreaDfInfOriginate (UINT4 *pu4ErrorCode, UINT4 u4FutOspfAreaId,
                                    INT4 i4TestValFutOspfAreaDfInfOriginate)
{
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tArea              *pArea;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC2 (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Test For OspfAreaDfInfOriginate %d Index AreaId %x \n",
               i4TestValFutOspfAreaDfInfOriginate, u4FutOspfAreaId);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);
    if (pOspfCxt->admnStat != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);
    if (pArea == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_AREA_NOT_CONFIG);
        return SNMP_FAILURE;
    }

    if (pArea->u4AreaType != NSSA_AREA)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_TRNS_ROLE);
        return SNMP_FAILURE;
    }

    if (ValidateOspfAreaTableSetIndexInCxt (pOspfCxt, &areaId) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }

    switch (i4TestValFutOspfAreaDfInfOriginate)
    {
        case (DEFAULT_INFO_ORIGINATE):
        case (NO_DEFAULT_INFO_ORIGINATE):
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_VALUE);
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfHostRouteIfIndex
 Input       :  The Indices
                FutOspfHostIpAddress
                FutOspfHostTOS

                The Object 
                testValFutOspfHostRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfHostRouteIfIndex (UINT4 *pu4ErrorCode,
                                  UINT4 u4FutOspfHostIpAddress,
                                  INT4 i4FutOspfHostTOS,
                                  INT4 i4TestValFutOspfHostRouteIfIndex)
{
    tIPADDR             hostIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4FutOspfHostIpAddress);

    if ((ValidateOspfHostTableGetIndexInCxt (pOspfCxt, &hostIpAddr,
                                             (INT1) i4FutOspfHostTOS) ==
         SNMP_SUCCESS)
        && (IS_VALID_IF_INDEX (i4TestValFutOspfHostRouteIfIndex)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfIfTable
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfIfTable (UINT4 u4FutOspfIfIpAddress,
                                        INT4 i4FutOspfAddressLessIf)
{
    return nmhValidateIndexInstanceOspfIfTable (u4FutOspfIfIpAddress,
                                                i4FutOspfAddressLessIf);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfOperState
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                testValFutOspfIfOperState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfOperState (UINT4 *pu4ErrorCode, UINT4 u4FutOspfIfIpAddress,
                             INT4 i4FutOspfAddressLessIf,
                             INT4 i4TestValFutOspfIfOperState)
{
    tIPADDR             ifIpAddr;
    UINT4               u4Value = (UINT4) i4TestValFutOspfIfOperState;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((ValidateOspfIfTableSetIndexInCxt
         (pOspfCxt, ifIpAddr, (UINT4) i4FutOspfAddressLessIf) == SNMP_SUCCESS)
        && ((u4Value == OSPF_OPERUP) || (u4Value == OSPF_OPERDOWN)
            || (u4Value == OSPF_LOOPBACK_IND) || (u4Value == OSPF_UNLOOP_IND)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfPassive
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object
                testValFutOspfIfPassive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfPassive (UINT4 *pu4ErrorCode, UINT4 u4FutOspfIfIpAddress,
                           INT4 i4FutOspfAddressLessIf,
                           INT4 i4TestValFutOspfIfPassive)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((ValidateOspfIfTableGetIndexInCxt
         (pOspfCxt, ifIpAddr, (UINT4) i4FutOspfAddressLessIf) == SNMP_SUCCESS)
        && (IS_VALID_TRUTH_VALUE (i4TestValFutOspfIfPassive)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_OSPFIF);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfBfdState
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
                The Object
                testValFutOspfIfBfdState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhTestv2FutOspfIfBfdState (UINT4 *pu4ErrorCode, UINT4 u4FutOspfIfIpAddress,
                            INT4 i4FutOspfAddressLessIf,
                            INT4 i4TestValFutOspfIfBfdState)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (i4TestValFutOspfIfBfdState != OSPF_BFD_ENABLED
        && i4TestValFutOspfIfBfdState != OSPF_BFD_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
    /* Checking BFD Admin status */
    if (pOspfCxt == NULL || pOspfCxt->u1BfdAdminStatus != OSPF_BFD_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_BFD_ADMIN_STATUS_DISABLED);
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    pInterface =
        GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4FutOspfAddressLessIf);
    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_BFD_INTF_DISABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfNbrTable
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfNbrTable (UINT4 u4FutOspfNbrIpAddr,
                                         INT4 i4FutOspfNbrAddressLessIndex)
{
    return nmhValidateIndexInstanceOspfNbrTable (u4FutOspfNbrIpAddr,
                                                 i4FutOspfNbrAddressLessIndex);
}

/* Low Level TEST Routines for All Objects  */

/******************************************************************************
* Function : ValidateOspfExtRouteTableSetIndexInCxt                           *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pExtRouteDest - ext route destination ip address                 *
*            pExtRouteMask                                                    *
*            i1Tos - Type of Service                                          *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfExtRouteTableSetIndexInCxt (tOspfCxt * pOspfCxt,
                                        tIPADDR * pExtRouteDest,
                                        tIPADDR * pExtRouteMask, INT1 i1Tos)
{
    tExtRoute          *pExtRoute;

    if (i1Tos >= ENCODE_TOS (OSPF_MAX_METRIC))
    {
        return SNMP_FAILURE;
    }

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, pExtRouteDest, pExtRouteMask);
    if (pExtRoute != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/******************************************************************************
* Function : ValidateOspfExtRouteTableGetIndexInCxt                           *
* Input    : tOspfCxt - Pointer to tOspfCxt strucutre                         *
*            pExtRouteDest - ext route destination ip address                 *
*            pExtRouteMask                                                    *
*            i1Tos - Type of Service                                          *
* Output   : None                                                             *
* Returns  : SNMP_FAILURE or SUCCESS                                          *
******************************************************************************/
PRIVATE INT1
ValidateOspfExtRouteTableGetIndexInCxt (tOspfCxt * pOspfCxt,
                                        tIPADDR * pExtRouteDest,
                                        tIPADDR * pExtRouteMask, INT1 i1Tos)
{
    tExtRoute          *pExtRoute;

    if (i1Tos >= ENCODE_TOS (OSPF_MAX_METRIC))
    {
        return SNMP_FAILURE;
    }

    if (((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, pExtRouteDest,
                                            pExtRouteMask)) != NULL) &&
        (pExtRoute->aMetric[DECODE_TOS (i1Tos)].rowStatus != INVALID))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteMetric
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteMetric (UINT4 *pu4ErrorCode,
                                UINT4 u4FutOspfExtRouteDest,
                                UINT4 u4FutOspfExtRouteMask,
                                INT4 i4FutOspfExtRouteTOS,
                                INT4 i4TestValFutOspfExtRouteMetric)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((ValidateOspfExtRouteTableSetIndexInCxt (pOspfCxt, &extRouteDest,
                                                 &extRouteMask,
                                                 (INT1) i4FutOspfExtRouteTOS) ==
         SNMP_SUCCESS) && IS_VALID_BIG_METRIC (i4TestValFutOspfExtRouteMetric))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteMetricType
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteMetricType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FutOspfExtRouteDest,
                                    UINT4 u4FutOspfExtRouteMask,
                                    INT4 i4FutOspfExtRouteTOS,
                                    INT4 i4TestValFutOspfExtRouteMetricType)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((ValidateOspfExtRouteTableSetIndexInCxt (pOspfCxt, &extRouteDest,
                                                 &extRouteMask,
                                                 (INT1) i4FutOspfExtRouteTOS) ==
         SNMP_SUCCESS)
        && IS_VALID_EXT_METRIC_TYPE (i4TestValFutOspfExtRouteMetricType))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteTag
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteTag (UINT4 *pu4ErrorCode, UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             INT4 i4TestValFutOspfExtRouteTag)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((ValidateOspfExtRouteTableSetIndexInCxt
         (pOspfCxt, &extRouteDest, &extRouteMask,
          (INT1) i4FutOspfExtRouteTOS) == SNMP_SUCCESS)
        && IS_VALID_POS_INTEGER (i4TestValFutOspfExtRouteTag))
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteFwdAdr
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteFwdAdr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteFwdAdr (UINT4 *pu4ErrorCode,
                                UINT4 u4FutOspfExtRouteDest,
                                UINT4 u4FutOspfExtRouteMask,
                                INT4 i4FutOspfExtRouteTOS,
                                UINT4 u4TestValFutOspfExtRouteFwdAdr)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tIPADDR             fwdAdr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);
    OSPF_CRU_BMC_DWTOPDU (fwdAdr, u4TestValFutOspfExtRouteFwdAdr);

    if ((ValidateOspfExtRouteTableSetIndexInCxt (pOspfCxt, &extRouteDest,
                                                 &extRouteMask,
                                                 (INT1) i4FutOspfExtRouteTOS) ==
         SNMP_SUCCESS) && (IS_VALID_IP_ADDR (u4TestValFutOspfExtRouteFwdAdr)))
    {
        return SNMP_SUCCESS;
    }

    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteIfIndex
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteIfIndex (UINT4 *pu4ErrorCode,
                                 UINT4 u4FutOspfExtRouteDest,
                                 UINT4 u4FutOspfExtRouteMask,
                                 INT4 i4FutOspfExtRouteTOS,
                                 INT4 i4TestValFutOspfExtRouteIfIndex)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((ValidateOspfExtRouteTableSetIndexInCxt (pOspfCxt, &extRouteDest,
                                                 &extRouteMask,
                                                 (INT1) i4FutOspfExtRouteTOS) ==
         SNMP_SUCCESS) && (IS_VALID_IF_INDEX (i4TestValFutOspfExtRouteIfIndex)))
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteNextHop
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteNextHop
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteNextHop (UINT4 *pu4ErrorCode,
                                 UINT4 u4FutOspfExtRouteDest,
                                 UINT4 u4FutOspfExtRouteMask,
                                 INT4 i4FutOspfExtRouteTOS,
                                 UINT4 u4TestValFutOspfExtRouteNextHop)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tIPADDR             nextHop;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);
    OSPF_CRU_BMC_DWTOPDU (nextHop, u4TestValFutOspfExtRouteNextHop);

    if ((ValidateOspfExtRouteTableSetIndexInCxt (pOspfCxt, &extRouteDest,
                                                 &extRouteMask,
                                                 (INT1) i4FutOspfExtRouteTOS) ==
         SNMP_SUCCESS) && (IS_VALID_IP_ADDR (u4TestValFutOspfExtRouteNextHop)))
    {
        return SNMP_SUCCESS;
    }

    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfExtRouteStatus
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                testValFutOspfExtRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfExtRouteStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FutOspfExtRouteDest,
                                UINT4 u4FutOspfExtRouteMask,
                                INT4 i4FutOspfExtRouteTOS,
                                INT4 i4TestValFutOspfExtRouteStatus)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute = NULL;
    tIPADDR             nullIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    SET_NULL_IP_ADDR (nullIpAddr);

    if (i4TestValFutOspfExtRouteStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    switch ((UINT1) i4TestValFutOspfExtRouteStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (pOspfCxt->u4ExtRtCount > MAX_EXT_ROUTES)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (ValidateOspfExtRouteTableGetIndexInCxt (pOspfCxt,
                                                        &extRouteDest,
                                                        &extRouteMask,
                                                        (INT1)
                                                        i4FutOspfExtRouteTOS) ==
                SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            if ((pExtRoute =
                 GetFindExtRouteInCxt (pOspfCxt,
                                       &extRouteDest, &extRouteMask)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }
            if (ValidateOspfExtRouteTableGetIndexInCxt (pOspfCxt,
                                                        &extRouteDest,
                                                        &extRouteMask,
                                                        (INT1)
                                                        i4FutOspfExtRouteTOS) ==
                SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
                return SNMP_FAILURE;
            }

            if ((pExtRoute->aMetric
                 [DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].rowStatus ==
                 NOT_IN_SERVICE)
                ||
                (pExtRoute->aMetric
                 [DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].rowStatus ==
                 ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            else if (pExtRoute->aMetric[DECODE_TOS ((INT1)
                                                    i4FutOspfExtRouteTOS)].
                     rowStatus == NOT_READY)
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
            return SNMP_FAILURE;
        default:
            break;

            /* end switch */
    }
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfRoutingTable
 Input       :  The Indices
                FutOspfRouteIpAddr
                FutOspfRouteIpAddrMask
                FutOspfRouteIpTos
                FutOspfRouteIpNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfRoutingTable (UINT4 u4FutOspfRouteIpAddr,
                                             UINT4 u4FutOspfRouteIpAddrMask,
                                             INT4 i4FutOspfRouteIpTos,
                                             UINT4 u4FutOspfRouteIpNextHop)
{
    tRtEntry           *pRtEntry;
    tIPADDR             nextHopIpAddr;
    tPath              *pPath;
    INT2                i1NextHopCount;
    tIPADDR             destIpAddr;
    tIPADDR             destIpAddrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (destIpAddr, u4FutOspfRouteIpAddr);
    OSPF_CRU_BMC_DWTOPDU (destIpAddrMask, u4FutOspfRouteIpAddrMask);
    OSPF_CRU_BMC_DWTOPDU (nextHopIpAddr, u4FutOspfRouteIpNextHop);

    i1NextHopCount = 0;

    if (!IS_VALID_TOS_VALUE (i4FutOspfRouteIpTos))
    {
        return SNMP_FAILURE;
    }

    if ((pRtEntry =
         (tRtEntry *) RtcFindRtEntry (pOspfCxt->pOspfRt, &destIpAddr,
                                      &destIpAddrMask, DEST_NETWORK)) != NULL)
    {
        TMO_SLL_Scan (&(pRtEntry->aTosPath[DECODE_TOS (i4FutOspfRouteIpTos)]),
                      pPath, tPath *)
        {
            while ((i1NextHopCount < pPath->u1HopCount) &&
                   (i1NextHopCount < MAX_NEXT_HOPS))
            {
                if (UtilIpAddrComp (nextHopIpAddr,
                                    pPath->aNextHops
                                    [i1NextHopCount++].nbrIpAddr) == OSPF_EQUAL)
                {
                    return SNMP_SUCCESS;
                }
            }
            i1NextHopCount = 0;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfBRRouteTable
 Input       :  The Indices
                FutOspfBRRouteIpAddr
                FutOspfBRRouteIpAddrMask
                FutOspfBRRouteIpTos
                FutOspfBRRouteIpNextHop
                FutOspfBRRouteDestType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfBRRouteTable (UINT4 u4FutOspfBRRouteIpAddr,
                                             UINT4 u4FutOspfBRRouteIpAddrMask,
                                             UINT4 u4FutOspfBRRouteIpTos,
                                             UINT4 u4FutOspfBRRouteIpNextHop,
                                             INT4 i4FutOspfBRRouteDestType)
{
    tRtEntry           *pRtEntry;
    tIPADDR             nextHopIpAddr;
    tPath              *pPath;
    INT2                i1NextHopCount;
    tIPADDR             destIpAddr;
    tIPADDR             destIpAddrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (destIpAddr, u4FutOspfBRRouteIpAddr);
    OSPF_CRU_BMC_DWTOPDU (destIpAddrMask, u4FutOspfBRRouteIpAddrMask);
    OSPF_CRU_BMC_DWTOPDU (nextHopIpAddr, u4FutOspfBRRouteIpNextHop);

    if (!IS_VALID_TOS_VALUE ((INT4) u4FutOspfBRRouteIpTos))
    {
        return SNMP_FAILURE;
    }

    i1NextHopCount = 0;

    if ((pRtEntry =
         (tRtEntry *) RtcFindRtEntry (pOspfCxt->pOspfRt, &destIpAddr,
                                      &destIpAddrMask,
                                      (UINT1) i4FutOspfBRRouteDestType))
        != NULL)
    {
        TMO_SLL_Scan (&(pRtEntry->aTosPath[DECODE_TOS (u4FutOspfBRRouteIpTos)]),
                      pPath, tPath *)
        {
            while ((i1NextHopCount < pPath->u1HopCount) &&
                   (i1NextHopCount < MAX_NEXT_HOPS))
            {
                if (UtilIpAddrComp (nextHopIpAddr,
                                    pPath->aNextHops
                                    [i1NextHopCount++].nbrIpAddr) == OSPF_EQUAL)
                {
                    return SNMP_SUCCESS;
                }
            }
            i1NextHopCount = 0;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FutOspfSecIfStatus
 Input       :  The Indices
                FutOspfPrimIpAddr
                FutOspfPrimAddresslessIf
                FutOspfSecIpAddr
                FutOspfSecIpAddrMask

                The Object 
                testValFutOspfSecIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfSecIfStatus (UINT4 *pu4ErrorCode, UINT4 u4FutOspfPrimIpAddr,
                             INT4 i4FutOspfPrimAddresslessIf,
                             UINT4 u4FutOspfSecIpAddr,
                             UINT4 u4FutOspfSecIpAddrMask,
                             INT4 i4TestValFutOspfSecIfStatus)
{
    tIPADDR             primIpAddr;
    tIPADDR             secIpAddr;
    tInterface         *pInterface;
    tIPADDRMASK         secIpAddrMask;
    INT2                i2SecCount = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT1               u1IsSecondary = OSPF_FALSE;
    UINT4               u4SecIpAddr = 0;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4Index = 0;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (NetIpIfInfo));

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, u4FutOspfPrimIpAddr);
    OSPF_CRU_BMC_DWTOPDU (secIpAddr, u4FutOspfSecIpAddr);
    OSPF_CRU_BMC_DWTOPDU (secIpAddrMask, u4FutOspfSecIpAddrMask);

    if (i4TestValFutOspfSecIfStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }
    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr,
                                      (UINT4) i4FutOspfPrimAddresslessIf))
        != NULL)
    {
        if (pInterface->u1NetworkType == IF_VIRTUAL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (i4FutOspfPrimAddresslessIf == 0)
    {
        if (NetIpv4GetIfIndexFromAddrInCxt (pOspfCxt->u4OspfCxtId,
                                            u4FutOspfPrimIpAddr,
                                            &u4Index) == NETIPV4_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (NetIpv4GetPortFromIfIndex (u4FutOspfPrimIpAddr, &u4Index)
            == NETIPV4_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (NetIpv4GetIfInfo (u4Index, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4PrevIpAddr = u4FutOspfPrimIpAddr;

    while (NetIpv4GetNextSecondaryAddress (NetIpIfInfo.u4IfIndex, u4PrevIpAddr,
                                           &u4SecIpAddr,
                                           &u4NetMask) == NETIPV4_SUCCESS)
    {
        if (u4FutOspfSecIpAddr == u4SecIpAddr)
        {
            u1IsSecondary = OSPF_TRUE;
            break;
        }
        u4PrevIpAddr = u4SecIpAddr;
    }

    if (u1IsSecondary == OSPF_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValFutOspfSecIfStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (pInterface->secondaryIP.i1SecIpCount == MAX_SEC_INTERFACES)
            {
                CLI_SET_ERR (CLI_OSPF_MAX_SEC_CONF);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
            {
                if (UtilAddrMaskCmpe
                    (OSPF_CRU_BMC_DWFROMPDU
                     (pInterface->secondaryIP.ifSecIp[i2SecCount].
                      ifSecIpAddr),
                     OSPF_CRU_BMC_DWFROMPDU (pInterface->
                                             secondaryIP.ifSecIp
                                             [i2SecCount].
                                             ifSecIpAddrMask),
                     u4FutOspfSecIpAddr, u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                    /* Since the row already exists, no need to create it 
                       again */
                }
                i2SecCount++;
            }                    /* end while */
            return SNMP_SUCCESS;

        case ACTIVE:
            while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
            {
                if (UtilAddrMaskCmpe
                    (OSPF_CRU_BMC_DWFROMPDU
                     (pInterface->secondaryIP.ifSecIp[i2SecCount].
                      ifSecIpAddr),
                     OSPF_CRU_BMC_DWFROMPDU (pInterface->
                                             secondaryIP.ifSecIp
                                             [i2SecCount].
                                             ifSecIpAddrMask),
                     u4FutOspfSecIpAddr, u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
                {
                    /* Both prim and sec ifaces exist. */
                    return SNMP_SUCCESS;
                }
                i2SecCount++;
            }                    /* End of While */
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_VALUE);
            return SNMP_FAILURE;

    }                            /* end switch */
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAreaAggregateExternalTag
 Input       :  The Indices
                FutOspfAreaAggregateAreaID
                FutOspfAreaAggregateLsdbType
                FutOspfAreaAggregateNet
                FutOspfAreaAggregateMask

                The Object 
                testValFutOspfAreaAggregateExternalTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAreaAggregateExternalTag (UINT4 *pu4ErrorCode,
                                          UINT4 u4FutOspfAreaAggregateAreaID,
                                          INT4 i4FutOspfAreaAggregateLsdbType,
                                          UINT4 u4FutOspfAreaAggregateNet,
                                          UINT4 u4FutOspfAreaAggregateMask,
                                          INT4
                                          i4TestValFutOspfAreaAggregateExternalTag)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4FutOspfAreaAggregateNet);
    OSPF_CRU_BMC_DWTOPDU (addrMask, u4FutOspfAreaAggregateMask);

    if (ValidateOspfAreaAggregateTableGetIndexInCxt (pOspfCxt, &areaId,
                                                     i4FutOspfAreaAggregateLsdbType,
                                                     &ipAddr,
                                                     &addrMask) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (IS_VALID_POS_INTEGER (i4TestValFutOspfAreaAggregateExternalTag))
        return SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAsExternalAggregationEffect
 Input       :  The Indices
                FutOspfAsExternalAggregationNet
                FutOspfAsExternalAggregationMask
                FutOspfAsExternalAggregationAreaId

                The Object 
                testValFutOspfAsExternalAggregationEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAsExternalAggregationEffect (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4FutOspfAsExternalAggregationNet,
                                             UINT4
                                             u4FutOspfAsExternalAggregationMask,
                                             UINT4
                                             u4FutOspfAsExternalAggregationAreaId,
                                             INT4
                                             i4TestValFutOspfAsExternalAggregationEffect)
{
    tAsExtAddrRange    *pAsExtAddrRng;
    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4FutOspfAsExternalAggregationNet =
        (u4FutOspfAsExternalAggregationNet &
         u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);
    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);
    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);

    if (pAsExtAddrRng == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Cannot find aggregation range\n");
        CLI_SET_ERR (CLI_OSPF_INV_EXT_AGGR);
        return SNMP_FAILURE;
    }

    if (pAsExtAddrRng->rangeStatus == ACTIVE)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId,
                  " AS Ext AGG Active - Make not in Service\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_STATUS);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValFutOspfAsExternalAggregationEffect)
    {
        case RAG_ADVERTISE:
        case RAG_DO_NOT_ADVERTISE:
            return SNMP_SUCCESS;

        case RAG_ALLOW_ALL:
        case RAG_DENY_ALL:

            if (UtilIpAddrComp (pAsExtAddrRng->areaId,
                                gNullIpAddr) == OSPF_EQUAL)
            {
                return SNMP_SUCCESS;
            }
            break;

        default:
            break;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAsExternalAggregationTranslation
 Input       :  The Indices
                FutOspfAsExternalAggregationNet
                FutOspfAsExternalAggregationMask
                FutOspfAsExternalAggregationAreaId

                The Object 
                testValFutOspfAsExternalAggregationTranslation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAsExternalAggregationTranslation (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4FutOspfAsExternalAggregationNet,
                                                  UINT4
                                                  u4FutOspfAsExternalAggregationMask,
                                                  UINT4
                                                  u4FutOspfAsExternalAggregationAreaId,
                                                  INT4
                                                  i4TestValFutOspfAsExternalAggregationTranslation)
{
    UNUSED_PARAM (u4FutOspfAsExternalAggregationNet);
    UNUSED_PARAM (u4FutOspfAsExternalAggregationMask);
    UNUSED_PARAM (u4FutOspfAsExternalAggregationAreaId);

    if (((UINT1) i4TestValFutOspfAsExternalAggregationTranslation
         == OSPF_TRUE) ||
        ((UINT1) i4TestValFutOspfAsExternalAggregationTranslation
         == OSPF_FALSE))
        return SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAsExternalAggregationStatus
 Input       :  The Indices
                FutOspfAsExternalAggregationNet
                FutOspfAsExternalAggregationMask
                FutOspfAsExternalAggregationAreaId

                The Object 
                testValFutOspfAsExternalAggregationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAsExternalAggregationStatus (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4FutOspfAsExternalAggregationNet,
                                             UINT4
                                             u4FutOspfAsExternalAggregationMask,
                                             UINT4
                                             u4FutOspfAsExternalAggregationAreaId,
                                             INT4
                                             i4TestValFutOspfAsExternalAggregationStatus)
{
    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAsExtAddrRange    *pAsExtAddrRng;

    tArea              *pArea;
    tAreaId             areaId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4FutOspfAsExternalAggregationNet =
        (u4FutOspfAsExternalAggregationNet &
         u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);
    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);
    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);

    if (i4TestValFutOspfAsExternalAggregationStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (pArea == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_AREA);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValFutOspfAsExternalAggregationStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                                  areaId);
            if (pAsExtAddrRng != NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          " AS Ext AGG already existing\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_SUMMARY_ADDR_ALREADY_EXISTS);
                return SNMP_FAILURE;
            }

            if (gOsRtr.u4AsExtRouteAggrCnt ==
                FsOSPFSizingParams[MAX_OSPF_EXT_ADDR_RANGES_SIZING_ID].
                u4PreAllocatedUnits)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            return SNMP_SUCCESS;

        default:
            break;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2OspfSetTrap
 Input       :  The Indices

                The Object 
                testValOspfSetTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2OspfSetTrap (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValOspfSetTrap)
{
    if (pTestValOspfSetTrap->i4_Length != 4)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_OSPF_INV_LENGTH);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfMD5AuthKey
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                testValFutOspfIfMD5AuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfMD5AuthKey (UINT4 *pu4ErrorCode,
                              UINT4 u4FutOspfIfMD5AuthIpAddress,
                              INT4 i4FutOspfIfMD5AuthAddressLessIf,
                              INT4 i4FutOspfIfMD5AuthKeyId,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFutOspfIfMD5AuthKey)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);

    if (GetFindIfInCxt (pOspfCxt, ifIpAddr,
                        (UINT4) i4FutOspfIfMD5AuthAddressLessIf) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_OSPF_NOT_ENABLED);

        return SNMP_FAILURE;
    }
    if ((MemGetFreeUnits (MD5AUTH_QID)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_OSPF_MAX_MD5);
        return SNMP_FAILURE;
    }

    if ((ValidateOspfIfTableSetIndexInCxt (pOspfCxt, ifIpAddr,
                                           (UINT4)
                                           i4FutOspfIfMD5AuthAddressLessIf) ==
         SNMP_SUCCESS)
        && (pTestValFutOspfIfMD5AuthKey->i4_Length <= MAX_AUTHKEY_LEN)
        && (i4FutOspfIfMD5AuthKeyId >= OSPF_MIN_AUTHKEY_VALUE)
        && (i4FutOspfIfMD5AuthKeyId <= OSPF_MAX_AUTHKEY_VALUE))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For FutOspfIfMD5AuthKey Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For FutOspfIfMD5AuthKey Fails\n");
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                testValFutOspfIfMD5AuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfMD5AuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                         UINT4 u4FutOspfIfMD5AuthIpAddress,
                                         INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                         INT4 i4FutOspfIfMD5AuthKeyId,
                                         INT4
                                         i4TestValFutOspfIfMD5AuthKeyStartAccept)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4DiffTime = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4FutOspfIfMD5AuthAddressLessIf))
        != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u1AuthkeyId != (UINT1) i4FutOspfIfMD5AuthKeyId)
            {
                u4DiffTime =
                    (UINT4) (pAuthkeyInfo->u4KeyStopAccept - gOspf1sTimer);

                if (u4DiffTime >
                    (UINT4) i4TestValFutOspfIfMD5AuthKeyStartAccept)
                {
                    return SNMP_SUCCESS;
                }
            }
            else if (pInterface->sortMd5authkeyLst.u4_Count == 1)
            {
                return SNMP_SUCCESS;
            }
        }
    }

    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                testValFutOspfIfMD5AuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfMD5AuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                           UINT4 u4FutOspfIfMD5AuthIpAddress,
                                           INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                           INT4 i4FutOspfIfMD5AuthKeyId,
                                           INT4
                                           i4TestValFutOspfIfMD5AuthKeyStartGenerate)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4DiffTime = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4FutOspfIfMD5AuthAddressLessIf))
        != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u1AuthkeyId != (UINT1) i4FutOspfIfMD5AuthKeyId)
            {
                u4DiffTime =
                    (UINT4) (pAuthkeyInfo->u4KeyStopGenerate - gOspf1sTimer);

                if (u4DiffTime >
                    (UINT4) i4TestValFutOspfIfMD5AuthKeyStartGenerate)
                {
                    return SNMP_SUCCESS;
                }
            }
            else if (pInterface->sortMd5authkeyLst.u4_Count == 1)
            {
                return SNMP_SUCCESS;
            }
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                testValFutOspfIfMD5AuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfMD5AuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                          UINT4 u4FutOspfIfMD5AuthIpAddress,
                                          INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                          INT4 i4FutOspfIfMD5AuthKeyId,
                                          INT4
                                          i4TestValFutOspfIfMD5AuthKeyStopGenerate)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    INT4                i4CurrentTime = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Getting the system time for u44CurrentTime */
    UtlGetTime (&tm);
    i4CurrentTime = (INT4) GrGetSecondsSinceBase (tm);
    if (i4TestValFutOspfIfMD5AuthKeyStopGenerate < i4CurrentTime)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if (GetFindIfAuthkeyInfoInCxt (pOspfCxt, ifIpAddr,
                                   (UINT4)
                                   i4FutOspfIfMD5AuthAddressLessIf,
                                   (UINT1) i4FutOspfIfMD5AuthKeyId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                testValFutOspfIfMD5AuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfMD5AuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                        UINT4 u4FutOspfIfMD5AuthIpAddress,
                                        INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                        INT4 i4FutOspfIfMD5AuthKeyId,
                                        INT4
                                        i4TestValFutOspfIfMD5AuthKeyStopAccept)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    INT4                i4CurrentTime = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Getting the system time for u44CurrentTime */
    UtlGetTime (&tm);
    i4CurrentTime = (INT4) GrGetSecondsSinceBase (tm);
    if (i4TestValFutOspfIfMD5AuthKeyStopAccept < i4CurrentTime)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if (GetFindIfAuthkeyInfoInCxt (pOspfCxt, ifIpAddr,
                                   (UINT4)
                                   i4FutOspfIfMD5AuthAddressLessIf,
                                   (UINT1) i4FutOspfIfMD5AuthKeyId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfMD5AuthKeyStatus
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                testValFutOspfIfMD5AuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfMD5AuthKeyStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FutOspfIfMD5AuthIpAddress,
                                    INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                    INT4 i4FutOspfIfMD5AuthKeyId,
                                    INT4 i4TestValFutOspfIfMD5AuthKeyStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFutOspfIfMD5AuthKeyStatus);

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                 (UINT4) i4FutOspfIfMD5AuthAddressLessIf);
    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfIfMD5AuthKeyStatus != AUTHKEY_STATUS_VALID) &&
        (i4TestValFutOspfIfMD5AuthKeyStatus != AUTHKEY_STATUS_DELETE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_AUTHKEY_STATUS);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType == CRYPT_AUTHENTICATION
        && (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) < 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_NO_MD5_KEY);
        return SNMP_FAILURE;
    }

    if (GetFindIfAuthkeyInfoInCxt (pOspfCxt, ifIpAddr,
                                   (UINT4)
                                   i4FutOspfIfMD5AuthAddressLessIf,
                                   (UINT1) i4FutOspfIfMD5AuthKeyId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_NO_MD5_KEY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfMD5AuthKey
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                testValFutOspfVirtIfMD5AuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfMD5AuthKey (UINT4 *pu4ErrorCode,
                                  UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                  UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                                  INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFutOspfVirtIfMD5AuthKey)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);

    if ((GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId) == NULL)
        || (pTestValFutOspfVirtIfMD5AuthKey->i4_Length > MAX_AUTHKEY_LEN)
        || (i4FutOspfVirtIfMD5AuthKeyId > OSPF_MAX_AUTHKEY_VALUE))
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Test For FutOspfVirtIfMD5AuthKey Fails\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_OSPF_INV_LENGTH);
        return SNMP_FAILURE;
    }

    OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
              "Test For FutOspfVirtIfMD5AuthKey Succeeds\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                testValFutOspfVirtIfMD5AuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                             UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                             UINT4
                                             u4FutOspfVirtIfMD5AuthNeighbor,
                                             INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                             INT4
                                             i4TestValFutOspfVirtIfMD5AuthKeyStartAccept)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tTMO_SLL_NODE      *pLstNode;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pMd5AuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
            if (pMd5AuthkeyInfo->u1AuthkeyId !=
                (UINT1) i4FutOspfVirtIfMD5AuthKeyId)
            {
                if ((pMd5AuthkeyInfo->u4KeyStopAccept - gOspf1sTimer) >
                    (UINT4) i4TestValFutOspfVirtIfMD5AuthKeyStartAccept)
                {
                    return SNMP_SUCCESS;
                }
            }
            else if (pInterface->sortMd5authkeyLst.u4_Count == 1)
            {
                return SNMP_SUCCESS;
            }
        }
    }

    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                testValFutOspfVirtIfMD5AuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4FutOspfVirtIfMD5AuthAreaId,
                                               UINT4
                                               u4FutOspfVirtIfMD5AuthNeighbor,
                                               INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                               INT4
                                               i4TestValFutOspfVirtIfMD5AuthKeyStartGenerate)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo = NULL;
    tTMO_SLL_NODE      *pLstNode;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pMd5AuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
            if (pMd5AuthkeyInfo->u1AuthkeyId ==
                (UINT1) i4FutOspfVirtIfMD5AuthKeyId)
            {
                if ((pMd5AuthkeyInfo->u4KeyStopGenerate - gOspf1sTimer) >
                    (UINT4) i4TestValFutOspfVirtIfMD5AuthKeyStartGenerate)
                {
                    return SNMP_SUCCESS;
                }
            }
            else if (pInterface->sortMd5authkeyLst.u4_Count == 1)
            {
                return SNMP_SUCCESS;
            }
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                testValFutOspfVirtIfMD5AuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4FutOspfVirtIfMD5AuthAreaId,
                                              UINT4
                                              u4FutOspfVirtIfMD5AuthNeighbor,
                                              INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                              INT4
                                              i4TestValFutOspfVirtIfMD5AuthKeyStopGenerate)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFutOspfVirtIfMD5AuthKeyStopGenerate);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if (GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                       &(nbrId),
                                       (UINT1)
                                       i4FutOspfVirtIfMD5AuthKeyId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                testValFutOspfVirtIfMD5AuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                            UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                            UINT4
                                            u4FutOspfVirtIfMD5AuthNeighbor,
                                            INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                            INT4
                                            i4TestValFutOspfVirtIfMD5AuthKeyStopAccept)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFutOspfVirtIfMD5AuthKeyStopAccept);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if (GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                       &(nbrId),
                                       (UINT1)
                                       i4FutOspfVirtIfMD5AuthKeyId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfMD5AuthKeyStatus
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                testValFutOspfVirtIfMD5AuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfMD5AuthKeyStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                        UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                                        INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                        INT4
                                        i4TestValFutOspfVirtIfMD5AuthKeyStatus)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo = NULL;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFutOspfVirtIfMD5AuthKeyStatus);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfVirtIfMD5AuthKeyStatus != AUTHKEY_STATUS_VALID) &&
        (i4TestValFutOspfVirtIfMD5AuthKeyStatus != AUTHKEY_STATUS_DELETE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType == CRYPT_AUTHENTICATION
        && (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) < 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_MD5KEY);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType == CRYPT_AUTHENTICATION
        && (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) > 0))
    {

        pMd5AuthkeyInfo = GetAuthkeyTouse (pInterface);

        if (pMd5AuthkeyInfo != NULL)
        {
            if (pMd5AuthkeyInfo->u1AuthkeyId == i4FutOspfVirtIfMD5AuthKeyId)
            {

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_MD5_ENABLED);
                return SNMP_FAILURE;
            }
        }
    }

    if (GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                       &(nbrId),
                                       (UINT1)
                                       i4FutOspfVirtIfMD5AuthKeyId) != NULL)
    {

        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_MD5KEY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRFC1583Compatibility
 Input       :  The Indices

                The Object 
                testValFutOspfRFC1583Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRFC1583Compatibility (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFutOspfRFC1583Compatibility)
{
    if ((i4TestValFutOspfRFC1583Compatibility != OSPF_ENABLED) &&
        (i4TestValFutOspfRFC1583Compatibility != OSPF_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfRRDRouteConfigTable (UINT4 u4FutOspfRRDRouteDest,
                                                    UINT4 u4FutOspfRRDRouteMask)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if (GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr, &RrdDestAddrMask)
        != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDRouteMetric
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                testValFutOspfRRDRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDRouteMetric (UINT4 *pu4ErrorCode,
                                UINT4 u4FutOspfRRDRouteDest,
                                UINT4 u4FutOspfRRDRouteMask,
                                INT4 i4TestValFutOspfRRDRouteMetric)
{
    UNUSED_PARAM (u4FutOspfRRDRouteDest);
    UNUSED_PARAM (u4FutOspfRRDRouteMask);

    /*
     * No need to check the existence of this instance as it is already 
     * done in the corresponding ValidateIndex routine
     */

    if (IS_VALID_METRIC_VALUE (i4TestValFutOspfRRDRouteMetric))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDRouteMetricType
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                testValFutOspfRRDRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDRouteMetricType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FutOspfRRDRouteDest,
                                    UINT4 u4FutOspfRRDRouteMask,
                                    INT4 i4TestValFutOspfRRDRouteMetricType)
{
    UNUSED_PARAM (u4FutOspfRRDRouteMask);
    UNUSED_PARAM (u4FutOspfRRDRouteDest);

    /*
     * No need to check the existence of this instance as it is already 
     * done in the corresponding ValidateIndex routine
     */

    if (IS_VALID_EXT_METRIC_TYPE (i4TestValFutOspfRRDRouteMetricType))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_OSPF_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDRouteTagType
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                testValFutOspfRRDRouteTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDRouteTagType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FutOspfRRDRouteDest,
                                 UINT4 u4FutOspfRRDRouteMask,
                                 INT4 i4TestValFutOspfRRDRouteTagType)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UNUSED_PARAM (u4FutOspfRRDRouteDest);
    UNUSED_PARAM (u4FutOspfRRDRouteMask);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->bAsBdrRtr != OSPF_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!IS_VALID_RRD_TAG_TYPE_VALUE (i4TestValFutOspfRRDRouteTagType))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDRouteTag
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                testValFutOspfRRDRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDRouteTag (UINT4 *pu4ErrorCode,
                             UINT4 u4FutOspfRRDRouteDest,
                             UINT4 u4FutOspfRRDRouteMask,
                             UINT4 u4TestValFutOspfRRDRouteTag)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4TestValFutOspfRRDRouteTag);

    if (pOspfCxt->bAsBdrRtr != OSPF_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_NO_BDR_RTR);
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        if (pRrdConfRtInfo->u1RedistrTagType != MANUAL_TAG)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_TAGTYPE);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDRouteStatus
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                testValFutOspfRRDRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDRouteStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FutOspfRRDRouteDest,
                                UINT4 u4FutOspfRRDRouteMask,
                                INT4 i4TestValFutOspfRRDRouteStatus)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4NetworkAddr = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->bAsBdrRtr != OSPF_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_NO_BDR_RTR);
        return SNMP_FAILURE;
    }

    if (u4FutOspfRRDRouteDest != 0)
    {
        if (OspfutilValidateDestAddr (u4FutOspfRRDRouteDest))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INVALID_PEER_ADDRESS_ERR);
            return SNMP_FAILURE;
        }
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if (i4TestValFutOspfRRDRouteStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    /* Check whether given range is a network IP address  */
    u4NetworkAddr = u4FutOspfRRDRouteDest & (~(u4FutOspfRRDRouteMask));
    if (u4NetworkAddr != 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INCONS_ADDR_MASK);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValFutOspfRRDRouteStatus)
    {

        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                           &RrdDestAddrMask) != NULL)
            {
                /* row already exists */
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_OSPF_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            if (TMO_SLL_Count (&(pOspfCxt->RedistrRouteConfigLst)) ==
                MAX_REDISTR_ROUTES)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:
            if (GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                           &RrdDestAddrMask) != NULL)
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_ENTRY);
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_OSPF_INV_STATUS);
            return SNMP_FAILURE;

    }                            /* switch */

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDStatus
 Input       :  The Indices

                The Object 
                testValFutOspfRRDStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfRRDStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (!IS_VALID_RRD_STATUS_VALUE (i4TestValFutOspfRRDStatus))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDSrcProtoMaskEnable
 Input       :  The Indices

                The Object 
                testValFutOspfRRDSrcProtoMaskEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDSrcProtoMaskEnable (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFutOspfRRDSrcProtoMaskEnable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->redistrAdmnStatus != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_REDIS_DISABLED);
        return SNMP_FAILURE;
    }

    if (!IS_VALID_RRD_SRC_PROTO_ENABLE_MASK_VALUE
        (i4TestValFutOspfRRDSrcProtoMaskEnable))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDSrcProtoMaskDisable
 Input       :  The Indices

                The Object 
                testValFutOspfRRDSrcProtoMaskDisable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDSrcProtoMaskDisable (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFutOspfRRDSrcProtoMaskDisable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->redistrAdmnStatus != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_REDIS_DISABLED);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&(pOspfCxt->RedistrRouteConfigLst)) != OSPF_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_OSPF_REDIS_ENABLED);
        return SNMP_FAILURE;
    }

    if (!IS_VALID_RRD_SRC_PROTO_DISABLE_MASK_VALUE
        (i4TestValFutOspfRRDSrcProtoMaskDisable))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfRRDRouteMapEnable
 Input       :  The Indices

                The Object 
                testValFutOspfRRDRouteMapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfRRDRouteMapEnable (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTestValFutOspfRRDRouteMapEnable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFutOspfRRDRouteMapEnable->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pOspfCxt->bAsBdrRtr != OSPF_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_NO_BDR_RTR);
        return SNMP_FAILURE;
    }

    if (pOspfCxt->redistrAdmnStatus != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_ASSOC_RMAP);
        return SNMP_FAILURE;
    }

    if (pTestValFutOspfRRDRouteMapEnable->i4_Length != OSPF_ZERO)
    {
        if (STRLEN (pOspfCxt->au1RMapName) != OSPF_ZERO)
        {
            if (STRLEN (pOspfCxt->au1RMapName) !=
                (UINT4) pTestValFutOspfRRDRouteMapEnable->i4_Length)
            {
                CLI_SET_ERR (CLI_OSPF_MAX_SEC_CONF);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
            /* Check for existence of Route Map with different Name. 
             * If So, return failure, */
            if (MEMCMP (pOspfCxt->au1RMapName,
                        pTestValFutOspfRRDRouteMapEnable->pu1_OctetList,
                        pTestValFutOspfRRDRouteMapEnable->i4_Length)
                != OSPF_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfDistInOutRouteMapTable. */
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfDistInOutRouteMapValue
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType

                The Object
                testValFutOspfDistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfDistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFutOspfDistInOutRouteMapName,
                                        INT4 i4FutOspfDistInOutRouteMapType,
                                        INT4
                                        i4TestValFutOspfDistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFutOspfDistInOutRouteMapValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (OspfCmpFilterRMapName
            (pOspfCxt->pDistanceFilterRMap, pFutOspfDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (OspfCmpFilterRMapName
            (pOspfCxt->pDistributeInFilterRMap,
             pFutOspfDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFutOspfDistInOutRouteMapValue);
#endif /*ROUTEMAP_WANTED */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfDistInOutRouteMapRowStatus
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType

                The Object
                testValFutOspfDistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfDistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFutOspfDistInOutRouteMapName,
                                            INT4 i4FutOspfDistInOutRouteMapType,
                                            INT4
                                            i4TestValFutOspfDistInOutRouteMapRowStatus)
{

#ifdef ROUTEMAP_WANTED

    INT1                i1Exists = 0;
    INT1                i1Match = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)

    {
        if (pOspfCxt->pDistanceFilterRMap != NULL)
        {
            i1Exists = 1;
            if (OspfCmpFilterRMapName
                (pOspfCxt->pDistanceFilterRMap,
                 pFutOspfDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (pOspfCxt->pDistributeInFilterRMap != NULL)
        {
            i1Exists = 1;
            if (OspfCmpFilterRMapName
                (pOspfCxt->pDistributeInFilterRMap,
                 pFutOspfDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFutOspfDistInOutRouteMapRowStatus)
    {
        case ACTIVE:
        case DESTROY:
            if (i1Match)
            {
                return SNMP_SUCCESS;
            }
            else if (i1Exists)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            break;
        case CREATE_AND_WAIT:
            if (!i1Exists)
            {
                return SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFutOspfDistInOutRouteMapRowStatus);
#endif /*ROUTEMAP_WANTED */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  OspfIsValidIpAddress
 Input       :  The IP Address
 Description :  This is used validate the IP Adress in the SNMP test routines
 Returns     :  TRUE or FALSE
****************************************************************************/
UINT1
OspfIsValidIpAddress (UINT4 u4IpAddress)
{
    if (u4IpAddress == LTD_B_CAST_ADDR)
    {
        return (FALSE);
    }

    if (!((IS_CLASS_A_ADDR (u4IpAddress)) ||
          (IS_CLASS_B_ADDR (u4IpAddress)) || (IS_CLASS_C_ADDR (u4IpAddress))))
    {
        return FALSE;
    }

    if ((u4IpAddress & LOOP_BACK_ADDR_MASK) == LOOP_BACK_ADDRESES)
    {
        /* Address is Loop back adress of 127.x.x.x */
        return (FALSE);
    }

    /* Valid IP address can be assigned to router */
    return (TRUE);
}

/* Low level routines for FutOspfVirtNbrTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfVirtNbrTable
 Input       :  The Indices
                FutOspfVirtNbrArea
                FutOspfVirtNbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfVirtNbrTable (UINT4 u4OspfVirtNbrArea,
                                             UINT4 u4OspfVirtNbrRtrId)
{
    return nmhValidateIndexInstanceOspfVirtNbrTable (u4OspfVirtNbrArea,
                                                     u4OspfVirtNbrRtrId);
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfGrShutdown
 Input       :  The Indices

                The Object
                testValFutOspfGrShutdown
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfGrShutdown (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFutOspfGrShutdown)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfGrShutdown != OSPF_RESTART_NONE) &&
        (i4TestValFutOspfGrShutdown != OSPF_GR_UNPLANNED_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFutOspfGrShutdown == OSPF_RESTART_NONE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u1RestartSupport != OSPF_RESTART_BOTH)
    {
        /* Restart is not supported */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSPF_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
   Function    :  nmhTestv2FutOspfPreferenceValue
   Input       :  The Indices
 
                  The Object 
                  testValFutOspfPreferenceValue
   Output      :  The Test Low Lev Routine Take the Indices &
                  Test whether that Value is Valid Input for Set.
                  Stores the value of error code in the Return val
   Error Codes :  The following error codes are to be returned
                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfPreferenceValue (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFutOspfPreferenceValue)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pOspfCxt->admnStat != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFutOspfPreferenceValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfAuthKey
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                testValFutOspfIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfAuthKey (UINT4 *pu4ErrorCode,
                           UINT4 u4FutOspfIfAuthIpAddress,
                           INT4 i4FutOspfIfAuthAddressLessIf,
                           INT4 i4FutOspfIfAuthKeyId,
                           tSNMP_OCTET_STRING_TYPE * pTestValFutOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FutOspfIfMD5AuthKey (pu4ErrorCode,
                                             u4FutOspfIfAuthIpAddress,
                                             i4FutOspfIfAuthAddressLessIf,
                                             i4FutOspfIfAuthKeyId,
                                             pTestValFutOspfIfAuthKey);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                testValFutOspfIfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                      UINT4 u4FutOspfIfAuthIpAddress,
                                      INT4 i4FutOspfIfAuthAddressLessIf,
                                      INT4 i4FutOspfIfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFutOspfIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4AuthKeyStartAccept = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pTestValFutOspfIfAuthKeyStartAccept->pu1_OctetList,
                            &tm);
    i4AuthKeyStartAccept = (INT4) GrGetSecondsSinceBase (tm);

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStartAccept (pu4ErrorCode,
                                                        u4FutOspfIfAuthIpAddress,
                                                        i4FutOspfIfAuthAddressLessIf,
                                                        i4FutOspfIfAuthKeyId,
                                                        i4AuthKeyStartAccept);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                testValFutOspfIfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                        UINT4 u4FutOspfIfAuthIpAddress,
                                        INT4 i4FutOspfIfAuthAddressLessIf,
                                        INT4 i4FutOspfIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFutOspfIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pTestValFutOspfIfAuthKeyStartGenerate->
                            pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) GrGetSecondsSinceBase (tm);

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStartGenerate (pu4ErrorCode,
                                                          u4FutOspfIfAuthIpAddress,
                                                          i4FutOspfIfAuthAddressLessIf,
                                                          i4FutOspfIfAuthKeyId,
                                                          i4AuthKeyStartGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                testValFutOspfIfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                       UINT4 u4FutOspfIfAuthIpAddress,
                                       INT4 i4FutOspfIfAuthAddressLessIf,
                                       INT4 i4FutOspfIfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFutOspfIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pTestValFutOspfIfAuthKeyStopGenerate->pu1_OctetList,
                            &tm);
    i4AuthKeyStopGenerate = (INT4) GrGetSecondsSinceBase (tm);

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStopGenerate (pu4ErrorCode,
                                                         u4FutOspfIfAuthIpAddress,
                                                         i4FutOspfIfAuthAddressLessIf,
                                                         i4FutOspfIfAuthKeyId,
                                                         i4AuthKeyStopGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                testValFutOspfIfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                     UINT4 u4FutOspfIfAuthIpAddress,
                                     INT4 i4FutOspfIfAuthAddressLessIf,
                                     INT4 i4FutOspfIfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFutOspfIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    INT4                i4AuthKeyStopAccept = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pTestValFutOspfIfAuthKeyStopAccept->pu1_OctetList,
                            &tm);
    i4AuthKeyStopAccept = (INT4) GrGetSecondsSinceBase (tm);

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStopAccept (pu4ErrorCode,
                                                       u4FutOspfIfAuthIpAddress,
                                                       i4FutOspfIfAuthAddressLessIf,
                                                       i4FutOspfIfAuthKeyId,
                                                       i4AuthKeyStopAccept);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfIfAuthKeyStatus
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                testValFutOspfIfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfIfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FutOspfIfAuthIpAddress,
                                 INT4 i4FutOspfIfAuthAddressLessIf,
                                 INT4 i4FutOspfIfAuthKeyId,
                                 INT4 i4TestValFutOspfIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStatus (pu4ErrorCode,
                                                   u4FutOspfIfAuthIpAddress,
                                                   i4FutOspfIfAuthAddressLessIf,
                                                   i4FutOspfIfAuthKeyId,
                                                   i4TestValFutOspfIfAuthKeyStatus);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfAuthKey
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                testValFutOspfVirtIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfAuthKey (UINT4 *pu4ErrorCode,
                               UINT4 u4FutOspfVirtIfAuthAreaId,
                               UINT4 u4FutOspfVirtIfAuthNeighbor,
                               INT4 i4FutOspfVirtIfAuthKeyId,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFutOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKey (pu4ErrorCode,
                                                 u4FutOspfVirtIfAuthAreaId,
                                                 u4FutOspfVirtIfAuthNeighbor,
                                                 i4FutOspfVirtIfAuthKeyId,
                                                 pTestValFutOspfVirtIfAuthKey);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                testValFutOspfVirtIfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                          UINT4 u4FutOspfVirtIfAuthAreaId,
                                          UINT4
                                          u4FutOspfVirtIfAuthNeighbor,
                                          INT4 i4FutOspfVirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFutOspfVirtIfAuthKeyStartAccept)
{
    INT4                i4AuthKeyStartAccept = 0;
    INT1                i1Return = SNMP_FAILURE;
    tUtlTm              tm;
    MEMSET (&tm, 0, sizeof (tUtlTm));
    OspfConvertTimeForSnmp (pTestValFutOspfVirtIfAuthKeyStartAccept->
                            pu1_OctetList, &tm);
    i4AuthKeyStartAccept = (INT4) GrGetSecondsSinceBase (tm);

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStartAccept (pu4ErrorCode,
                                                            u4FutOspfVirtIfAuthAreaId,
                                                            u4FutOspfVirtIfAuthNeighbor,
                                                            i4FutOspfVirtIfAuthKeyId,
                                                            i4AuthKeyStartAccept);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                testValFutOspfVirtIfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4FutOspfVirtIfAuthAreaId,
                                            UINT4
                                            u4FutOspfVirtIfAuthNeighbor,
                                            INT4 i4FutOspfVirtIfAuthKeyId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValFutOspfVirtIfAuthKeyStartGenerate)
{
    INT4                i4AuthKeyStartGenerate = 0;
    INT1                i1Return = SNMP_FAILURE;
    tUtlTm              tm;
    MEMSET (&tm, 0, sizeof (tUtlTm));
    OspfConvertTimeForSnmp (pTestValFutOspfVirtIfAuthKeyStartGenerate->
                            pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) GrGetSecondsSinceBase (tm);

    i1Return =
        nmhTestv2FutOspfVirtIfMD5AuthKeyStartGenerate (pu4ErrorCode,
                                                       u4FutOspfVirtIfAuthAreaId,
                                                       u4FutOspfVirtIfAuthNeighbor,
                                                       i4FutOspfVirtIfAuthKeyId,
                                                       i4AuthKeyStartGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                testValFutOspfVirtIfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4FutOspfVirtIfAuthAreaId,
                                           UINT4
                                           u4FutOspfVirtIfAuthNeighbor,
                                           INT4 i4FutOspfVirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFutOspfVirtIfAuthKeyStopGenerate)
{
    INT4                i4AuthKeyStoptGenerate = 0;
    INT1                i1Return = SNMP_FAILURE;
    tUtlTm              tm;
    MEMSET (&tm, 0, sizeof (tUtlTm));
    OspfConvertTimeForSnmp (pTestValFutOspfVirtIfAuthKeyStopGenerate->
                            pu1_OctetList, &tm);
    i4AuthKeyStoptGenerate = (INT4) GrGetSecondsSinceBase (tm);

    i1Return =
        nmhTestv2FutOspfVirtIfMD5AuthKeyStopGenerate (pu4ErrorCode,
                                                      u4FutOspfVirtIfAuthAreaId,
                                                      u4FutOspfVirtIfAuthNeighbor,
                                                      i4FutOspfVirtIfAuthKeyId,
                                                      i4AuthKeyStoptGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                testValFutOspfVirtIfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                         UINT4 u4FutOspfVirtIfAuthAreaId,
                                         UINT4
                                         u4FutOspfVirtIfAuthNeighbor,
                                         INT4 i4FutOspfVirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFutOspfVirtIfAuthKeyStopAccept)
{
    INT4                i4AuthKeyStopAcceptt = 0;
    INT1                i1Return = SNMP_FAILURE;
    tUtlTm              tm;
    MEMSET (&tm, 0, sizeof (tUtlTm));
    OspfConvertTimeForSnmp (pTestValFutOspfVirtIfAuthKeyStopAccept->
                            pu1_OctetList, &tm);
    i4AuthKeyStopAcceptt = (INT4) GrGetSecondsSinceBase (tm);

    i1Return =
        nmhTestv2FutOspfVirtIfMD5AuthKeyStopAccept (pu4ErrorCode,
                                                    u4FutOspfVirtIfAuthAreaId,
                                                    u4FutOspfVirtIfAuthNeighbor,
                                                    i4FutOspfVirtIfAuthKeyId,
                                                    i4AuthKeyStopAcceptt);

    return i1Return;
}

/****************************************************************************
 *  Function    :  nmhValidateIndexInstanceFutOspfIfCryptoAuthTable
 *  Input       :  The Indices
 *                 OspfIfIpAddress
 *                 OspfAddressLessIf
 *  Output      :  The Routines Validates the Given Indices.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfIfCryptoAuthTable (UINT4 u4OspfIfIpAddress,
                                                  INT4 i4OspfAddressLessIf)
{
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if (GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf)
        != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfAuthKeyStatus
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                testValFutOspfVirtIfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfVirtIfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FutOspfVirtIfAuthAreaId,
                                     UINT4 u4FutOspfVirtIfAuthNeighbor,
                                     INT4 i4FutOspfVirtIfAuthKeyId,
                                     INT4 i4TestValFutOspfVirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStatus (pu4ErrorCode,
                                                       u4FutOspfVirtIfAuthAreaId,
                                                       u4FutOspfVirtIfAuthNeighbor,
                                                       i4FutOspfVirtIfAuthKeyId,
                                                       i4TestValFutOspfVirtIfAuthKeyStatus);

    return i1Return;

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfVirtIfCryptoAuthTable
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfVirtIfCryptoAuthTable (UINT4 u4OspfVirtIfAreaId,
                                                      UINT4
                                                      u4OspfVirtIfNeighbor)
{

    tAreaId             areaId;
    tRouterId           nbrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if (GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfVirtIfCryptoAuthType
 Input       :  The Indices
                FsFutOspfContextId
                FsFutOspfVirtIfAreaId
                FsFutOspfVirtIfNeighbor

                The Object 
                testValFutOspfVirtIfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2OspfVirtIfCryptoAuthType (UINT4 *pu4ErrorCode,
                                   UINT4 u4FutOspfVirtIfAreaId,
                                   UINT4 u4FutOspfVirtIfNeighbor,
                                   INT4 i4TestValFutOspfVirtIfCryptoAuthType)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfNeighbor);
    if (i4TestValFutOspfVirtIfCryptoAuthType >= OSPF_MIN_CRYPTOTYPE_VALUE &&
        i4TestValFutOspfVirtIfCryptoAuthType <= OSPF_MAX_CRYPTOTYPE_VALUE)

    {
        pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
        if (!pInterface)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_OSPF_INV_VIRT_AREA);
            return SNMP_FAILURE;
        }
        /* Auth Type can be configured only when the key is configured */
        if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) > 0)
        {
            return (SNMP_SUCCESS);
        }
        CLI_SET_ERR (CLI_OSPF_NO_MD5_KEY);
    }
    else
    {
        CLI_SET_ERR (CLI_OSPF_INV_AUTHTYPE);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/*PopulateOspfCounters is test code.This function is invoked from ISS. 
 *It populates the ospf counters/statistics for verfying the ospf clear counters*/

/*****************************************************************************/
/*                                                                           */
/* Function     : PopulateOspfCounters                                       */
/*                                                                           */
/* Description    This function populates the ospf counters.                 */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
PopulateOspfCounters ()
{
    tOspfCxt           *pOspfCxt = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tNeighbor          *pNbr = NULL;
    UINT4               u4defaultvalue = 1;
    tAreaId             areaId;
    tArea              *pArea;
    UINT4               u4ContextId = 0;
    UINT4               u4MaxCxt = 0;
    UINT4               u4OspfAreaId = 0;
    UINT4               u4OspfPrevAreaId = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    OspfLock ();
    for (u4ContextId = 0; u4ContextId < u4MaxCxt; u4ContextId++)
    {
        if (gOsRtr.apOspfCxt[u4ContextId] == NULL)
        {
            /* Get the next context id */
            continue;
        }
        pOspfCxt = gOsRtr.apOspfCxt[u4ContextId];
        pOspfCxt->u4OriginateNewLsa = u4defaultvalue;
        pOspfCxt->u4RcvNewLsa = u4defaultvalue;
        if (nmhGetFirstIndexOspfAreaTable (&u4OspfAreaId) != SNMP_FAILURE)
        {
            gOsRtr.pOspfCxt = pOspfCxt;
            do
            {

                OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
                if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
                {
                    pArea->u2SpfRuns = (UINT2) u4defaultvalue;
                    pArea->u4AreaBdrRtrCount = u4defaultvalue;
                    pArea->u4AsBdrRtrCount = u4defaultvalue;
                    pArea->u4NssaTrnsltrEvents = u4defaultvalue;

                }
                u4OspfPrevAreaId = u4OspfAreaId;
            }
            while (nmhGetNextIndexOspfAreaTable
                   (u4OspfPrevAreaId, &u4OspfAreaId) != SNMP_FAILURE);
        }

    }

    pOspfCxt = gOsRtr.pOspfCxt;
    pOspfCxt->u4RcvNewLsa = u4defaultvalue;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        pInterface->aIfEvents = (COUNTER16) u4defaultvalue;
        pInterface->u4HelloRcvdCount = u4defaultvalue;
        pInterface->u4DdpRcvdCount = u4defaultvalue;
        pInterface->u4LsaReqRcvdCount = u4defaultvalue;
        pInterface->u4LsaUpdateRcvdCount = u4defaultvalue;
        pInterface->u4LsaAckRcvdCount = u4defaultvalue;
        pInterface->u4HelloTxedCount = u4defaultvalue;
        pInterface->u4DdpTxedCount = u4defaultvalue;
        pInterface->u4LsaReqTxedCount = u4defaultvalue;
        pInterface->u4LsaUpdateTxedCount = u4defaultvalue;
        pInterface->u4LsaAckTxedCount = u4defaultvalue;
        pInterface->u4HelloDisdCount = u4defaultvalue;
        pInterface->u4DdpDisdCount = u4defaultvalue;
        pInterface->u4LsaReqDisdCount = u4defaultvalue;
        pInterface->u4LsaUpdateDisdCount = u4defaultvalue;
        pInterface->u4LsaAckDisdCount = u4defaultvalue;

    }

    pLstNode = NULL;
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        pInterface->aIfEvents = (COUNTER16) u4defaultvalue;
        pInterface->u4HelloRcvdCount = u4defaultvalue;
        pInterface->u4DdpRcvdCount = u4defaultvalue;
        pInterface->u4LsaReqRcvdCount = u4defaultvalue;
        pInterface->u4LsaUpdateRcvdCount = u4defaultvalue;
        pInterface->u4LsaAckRcvdCount = u4defaultvalue;
        pInterface->u4HelloTxedCount = u4defaultvalue;
        pInterface->u4DdpTxedCount = u4defaultvalue;
        pInterface->u4LsaReqTxedCount = u4defaultvalue;
        pInterface->u4LsaUpdateTxedCount = u4defaultvalue;
        pInterface->u4LsaAckTxedCount = u4defaultvalue;
        pInterface->u4HelloDisdCount = u4defaultvalue;
        pInterface->u4DdpDisdCount = u4defaultvalue;
        pInterface->u4LsaReqDisdCount = u4defaultvalue;
        pInterface->u4LsaUpdateDisdCount = u4defaultvalue;
        pInterface->u4LsaAckDisdCount = u4defaultvalue;

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            pNbr->u4NbrEvents = (COUNTER16) u4defaultvalue;
        }

    }
    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
        pNbr->u4NbrEvents = (COUNTER16) u4defaultvalue;
    }

    OspfUnLock ();

    return;
}
