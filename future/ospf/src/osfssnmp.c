/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osfssnmp.c,v 1.23 2014/10/29 12:51:00 siva Exp $
 *
 * Description: This file contain routines for ospf traps to SNMP 
 *          Agent and parameter modification SNMP event routines
 *          
 *******************************************************************/

#include "osinc.h"
#include "ospf-mib.h"
#include "snmputil.h"

/* Proto types of the functions private to this file only */

tSNMP_OID_TYPE     *MakeObjIdFromDotNew (INT1 *textStr);

INT4                ParseSubIdNew (UINT1 **tempPtr);

UINT4               OSPF_TRAPS_OID[] = { 1, 3, 6, 1, 2, 1, 14, 16, 2 };
UINT4               SNMP_TRAP_OID[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

/*****************************************************************************/
/*                                                                           */
/* Function     : SnmpifSendTrap                                           */
/*                                                                           */
/* Description  : Routine to send trap to SNMP Agent.                        */
/*                                                                           */
/* Input        : u1TrapId  : Trap identifier                              */
/*                p_trap_info : Pointer to structure having the trap info.   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
SnmpifSendTrapInCxt (tOspfCxt * pOspfCxt, UINT1 u1TrapId, VOID *pTrapInfo)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||defined (SNMPV3_WANTED)
    tVifOrVnbrStChgTrapInfo *pVifOrVnbrStChgTrap;
    tIfStChgTrapInfo   *pIfStChgTrap = NULL;
    tNbrStChgTrapInfo  *pNbrStChgTrap = NULL;
    tIfConfErrTrapInfo *pIfCfgErTrap = NULL;
    tVifConfErrTrapInfo *pVirtIfCfgErTrap = NULL;
    tIfRxBadPktTrapInfo *pIfRxBadPktTrap = NULL;
    tVifRxBadPktTrapInfo *pVirtIfRxBadPktTrap = NULL;
    tIfRxmtTrapInfo    *pIfRxmtTrap = NULL;
    tVifRxmtTrapInfo   *pVirtIfRxmtTrap = NULL;
    tLsaTrapInfo       *pOrgLsaTrap = NULL;
    tLsaTrapInfo       *pMaxAgeLsaTrap = NULL;
    tExtLsdbOverflowTrapInfo *pLsdbOvrFlwTrap = NULL;
    tExtLsdbOverflowTrapInfo *pLsdbAppOvrFlwTrap = NULL;
    tRstStatChgTrapInfo *pRstStatChgTrapInfo = NULL;
    tNbrRstStatChgTrapInfo *pNbrRstStatChgTrapInfo = NULL;
    tVifRstStatChgTrapInfo *pVifRstStatChgTrapInfo = NULL;

    tOsRedStatChgTrapInfo *pOsRedStatChgTrapInfo = NULL;

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[256];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;

    OSPF_TRC1 (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
               "Trap to be generated: %d\n", u1TrapId);

    /* check for the sliding window way of trap limitations */

    if (!(pOspfCxt->u1TrapCount < ALLOWABLE_TRAP_COUNT_IN_SWP))
        return;

    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    MEMCPY (pEnterpriseOid->pu4_OidList, OSPF_TRAPS_OID,
            sizeof (OSPF_TRAPS_OID));
    pEnterpriseOid->u4_Length = sizeof (OSPF_TRAPS_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;
    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, SNMP_TRAP_OID, sizeof (SNMP_TRAP_OID));

    pSnmpTrapOid->u4_Length = sizeof (SNMP_TRAP_OID) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {

        case VIRT_IF_STATE_CHANGE_TRAP:

            pVifOrVnbrStChgTrap = (tVifOrVnbrStChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifOrVnbrStChgTrap->rtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfAreaId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifOrVnbrStChgTrap->tranAreaId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfNeighbor");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifOrVnbrStChgTrap->virtNbr,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfState");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         (pVifOrVnbrStChgTrap->
                                                          u1State + 1), NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case IF_STATE_CHANGE_TRAP:

            pIfStChgTrap = (tIfStChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfStChgTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfIfIpAddress");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfStChgTrap->ifIpAddr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfAddressLessIf");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfStChgTrap->
                                                         u4AddrlessIf, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfIfState");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         (pIfStChgTrap->
                                                          u1State + 1), NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case NBR_STATE_CHANGE_TRAP:

            pNbrStChgTrap = (tNbrStChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pNbrStChgTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrIpAddr");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pNbrStChgTrap->nbrIpAddr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrAddressLessIndex");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pNbrStChgTrap->
                                                         u4NbrAddrlessIf, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrRtrId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pNbrStChgTrap->nbrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrState");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         (pNbrStChgTrap->
                                                          u1State + 1), NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case VIRT_NBR_STATE_CHANGE_TRAP:

            pVifOrVnbrStChgTrap = (tVifOrVnbrStChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifOrVnbrStChgTrap->rtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrArea");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVifOrVnbrStChgTrap->
                                                      tranAreaId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrRtrId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVifOrVnbrStChgTrap->
                                                      virtNbr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrState");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         (pVifOrVnbrStChgTrap->
                                                          u1State + 1), NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case IF_CONFIG_ERROR_TRAP:
        case IF_AUTH_FAILURE_TRAP:

            pIfCfgErTrap = (tIfConfErrTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfCfgErTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfIfIpAddress");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfCfgErTrap->ifIpAddr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfAddressLessIf");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfCfgErTrap->
                                                         u4AddrlessIf, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketSrc");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            IP_ADDR_COPY (pOspfCxt->pktSrcAddr, pIfCfgErTrap->packetSrcAdr);
            if ((pOstring =
                 SNMP_AGT_FormOctetString (pIfCfgErTrap->packetSrcAdr,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfConfigErrorType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1CnfgErrType = pIfCfgErTrap->u1ErrType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfCfgErTrap->
                                                         u1ErrType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1PktType = pIfCfgErTrap->u1PktType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfCfgErTrap->
                                                         u1PktType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case VIRT_IF_CONFIG_ERROR_TRAP:
        case VIRT_IF_AUTH_FAILURE_TRAP:

            pVirtIfCfgErTrap = (tVifConfErrTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfCfgErTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfAreaId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfCfgErTrap->
                                                      tranAreaId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfNeighbor");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfCfgErTrap->virtNbr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfConfigErrorType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1CnfgErrType = pVirtIfCfgErTrap->u1ErrType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pVirtIfCfgErTrap->
                                                         u1ErrType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1PktType = pVirtIfCfgErTrap->u1PktType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pVirtIfCfgErTrap->
                                                         u1PktType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case IF_RX_BAD_PACKET_TRAP:

            pIfRxBadPktTrap = (tIfRxBadPktTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxBadPktTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfIfIpAddress");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxBadPktTrap->ifIpAddr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfAddressLessIf");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfRxBadPktTrap->
                                                         u4AddrlessIf, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketSrc");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            IP_ADDR_COPY (pOspfCxt->pktSrcAddr, pIfRxBadPktTrap->packetSrcAdr);
            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxBadPktTrap->
                                                      packetSrcAdr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1PktType = pIfRxBadPktTrap->u1PktType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfRxBadPktTrap->
                                                         u1PktType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case VIRT_IF_RX_BAD_PACKET_TRAP:

            pVirtIfRxBadPktTrap = (tVifRxBadPktTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVirtIfRxBadPktTrap->rtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfAreaId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfRxBadPktTrap->
                                                      tranAreaId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfNeighbor");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfRxBadPktTrap->
                                                      virtNbr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1PktType = pVirtIfRxBadPktTrap->u1PktType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pVirtIfRxBadPktTrap->
                                                         u1PktType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case IF_TX_RETRANSMIT_TRAP:

            pIfRxmtTrap = (tIfRxmtTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxmtTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfIfIpAddress");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxmtTrap->ifIpAddr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfAddressLessIf");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfRxmtTrap->
                                                         u4AddrlessIf, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrRtrId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxmtTrap->nbrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1PktType = pIfRxmtTrap->u1PktType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfRxmtTrap->u1PktType,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pIfRxmtTrap->
                                                         u1LsdbType, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbLsid");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxmtTrap->lsdbLsid,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pIfRxmtTrap->lsdbRtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case VIRT_IF_TX_RETRANSMIT_TRAP:

            pVirtIfRxmtTrap = (tVifRxmtTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfRxmtTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfAreaId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVirtIfRxmtTrap->tranAreaId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtIfNeighbor");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfRxmtTrap->virtNbr,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfPacketType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOspfCxt->u1PktType = pVirtIfRxmtTrap->u1PktType;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pVirtIfRxmtTrap->
                                                         u1PktType, NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pVirtIfRxmtTrap->
                                                         u1LsdbType, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbLsid");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pVirtIfRxmtTrap->lsdbLsid,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVirtIfRxmtTrap->lsdbRtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case ORIGINATE_LSA_TRAP:

            pOrgLsaTrap = (tLsaTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pOrgLsaTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring,
                                                         NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbAreaId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pOrgLsaTrap->lsdbAreaId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pOrgLsaTrap->
                                                         u1LsdbType, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbLsid");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pOrgLsaTrap->lsdbLsid,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pOrgLsaTrap->lsdbRtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case MAX_AGE_LSA_TRAP:

            pMaxAgeLsaTrap = (tLsaTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pMaxAgeLsaTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbAreaId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pMaxAgeLsaTrap->lsdbAreaId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbType");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pMaxAgeLsaTrap->
                                                         u1LsdbType, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbLsid");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pMaxAgeLsaTrap->lsdbLsid,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfLsdbRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pMaxAgeLsaTrap->lsdbRtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case LSDB_OVERFLOW_TRAP:

            pLsdbOvrFlwTrap = (tExtLsdbOverflowTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pLsdbOvrFlwTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfExtLsdbLimit");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pLsdbOvrFlwTrap->
                                                         i4ExtLsdbLimit, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case LSDB_APPROACHING_OVERFLOW_TRAP:

            pLsdbAppOvrFlwTrap = (tExtLsdbOverflowTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring = SNMP_AGT_FormOctetString (pLsdbAppOvrFlwTrap->rtrId,
                                                      MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfExtLsdbLimit");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER32,
                                                         0,
                                                         pLsdbAppOvrFlwTrap->
                                                         i4ExtLsdbLimit, NULL,
                                                         NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;
        case RST_STATUS_CHANGE_TRAP:

            pRstStatChgTrapInfo = (tRstStatChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pRstStatChgTrapInfo->rtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfRestartStatus");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pRstStatChgTrapInfo->u1RestartStatus,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfRestartInterval");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pRstStatChgTrapInfo->u4GracePeriod,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfRestartExitReason");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pRstStatChgTrapInfo->u1RestartExitReason,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case NBR_RST_STATUS_CHANGE_TRAP:

            pNbrRstStatChgTrapInfo = (tNbrRstStatChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pNbrRstStatChgTrapInfo->rtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrIpAddr");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pNbrRstStatChgTrapInfo->nbrIpAddr,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrAddressLessIndex");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->
                 u4NbrAddrlessIf, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfNbrRtrId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pNbrRstStatChgTrapInfo->nbrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfNbrRestartHelperStatus");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->
                 u1NbrHelperStatus, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfNbrRestartHelperAge");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->u4NbrHelperAge,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfNbrRestartHelperExitReason");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->
                 u1NbrHelperExitReason, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case VIRT_NBR_RST_STATUS_CHANGE_TRAP:

            pVifRstStatChgTrapInfo = (tVifRstStatChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifRstStatChgTrapInfo->rtrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrArea");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifRstStatChgTrapInfo->
                                           VifNbrAreaId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrRtrId");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifRstStatChgTrapInfo->VifNbrId,
                                           MAX_IP_ADDR_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrRestartHelperStatus");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pVifRstStatChgTrapInfo->
                 u1VifNbrHelperStatus, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrRestartHelperAge");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pVifRstStatChgTrapInfo->u4VifNbrHelperAge,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfVirtNbrRestartHelperExitReason");
            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pVifRstStatChgTrapInfo->
                 u1VifNbrHelperExitReason, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;
        case OSPF_RED_STATE_CHANGE_TRAP:

            pOsRedStatChgTrapInfo = (tOsRedStatChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfRouterId");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString (pOsRedStatChgTrapInfo->RtrId,
                                                 MAX_IP_ADDR_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                free_octetstring (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfHotStandbyState");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         pOsRedStatChgTrapInfo->
                                                         i4HotStandbyState,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfDynamicBulkUpdStatus");

            if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         pOsRedStatChgTrapInfo->
                                                         i4DynamicBulkUpdStatus,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
#else

    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pTrapInfo);

#endif /* FUTURE_SNMP_WANTED */

#ifdef DEBUG_WANTED
    DbgNotifyTrap (u1TrapId, pTrapInfo);
#endif
    pOspfCxt->u1TrapCount++;
}

/******************************************************************************
* Function : MakeObjIdFromDotNew                                         *
* Input    : textStr
* Output   : NONE
* Returns  : oidPtr or NULL
*******************************************************************************/
#define   OSPF_SNMP_TEMP_BUFF   280
INT1                tempBuffer[OSPF_SNMP_TEMP_BUFF];
tSNMP_OID_TYPE     *
MakeObjIdFromDotNew (INT1 *textStr)
{
    tSNMP_OID_TYPE     *oidPtr = NULL;
    INT1               *tempPtr = NULL, *dotPtr = NULL;
    UINT2               i = 0;
    UINT2               dotCount = 0;
    UINT1              *pu1TmpPtr = NULL;
    UINT2               u2BufferLen = 0;

    MEMSET (tempBuffer, 0, sizeof (tempBuffer));
    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*textStr))
    {
        dotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (dotPtr == NULL)
            dotPtr = textStr + STRLEN ((INT1 *) textStr);
        tempPtr = textStr;

        for (i = 0; ((tempPtr < dotPtr) && (i < 256)); i++)
        {
            if (i < OSPF_SNMP_TEMP_BUFF)
            {
                tempBuffer[i] = *tempPtr++;
            }
        }
        if (i < OSPF_SNMP_TEMP_BUFF)
        {
            tempBuffer[i] = '\0';
        }

        for (i = 0;
             i < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))
             && orig_mib_oid_table[i].pName != NULL; i++)
        {
            if ((STRCMP (orig_mib_oid_table[i].pName,
                         (INT1 *) tempBuffer) == 0)
                && (STRLEN ((INT1 *) tempBuffer) ==
                    STRLEN (orig_mib_oid_table[i].pName)))
            {
                STRNCPY ((INT1 *) tempBuffer, orig_mib_oid_table[i].pNumber,
                         (sizeof (tempBuffer) - 1));
                break;
            }
        }

        if ((i < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))) &&
            orig_mib_oid_table[i].pName == NULL)
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = sizeof (tempBuffer) - STRLEN (tempBuffer);
        STRNCAT ((INT1 *) tempBuffer, (INT1 *) dotPtr,
                 (u2BufferLen <
                  STRLEN (dotPtr) ? u2BufferLen : STRLEN (dotPtr)));

    }
    else
    {                            /* is not alpha, so just copy into tempBuffer */
        STRNCPY ((INT1 *) tempBuffer, (INT1 *) textStr,
                 (sizeof (tempBuffer) - 1));

    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    dotCount = 0;
    for (i = 0; i < OSPF_SNMP_TEMP_BUFF && tempBuffer[i] != '\0'; i++)
    {
        if (tempBuffer[i] == '.')
            dotCount++;
    }
    if ((oidPtr = alloc_oid ((INT4) (dotCount + 1))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) tempBuffer;
    for (i = 0; i < dotCount + 1; i++)
    {
        if ((oidPtr->pu4_OidList[i] =
             ((UINT4) (ParseSubIdNew (&pu1TmpPtr)))) == (UINT4) -1)
        {
            free_oid (oidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
            pu1TmpPtr++;        /* to skip over dot */
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (oidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (oidPtr);
}

/******************************************************************************
* Function : ParseSubIdNew
* Input    : tempPtr
* Output   : None
* Returns  : value of tempPtr or -1
*******************************************************************************/

INT4
ParseSubIdNew (UINT1 **tempPtr)
{
    INT4                value = 0;
    UINT1              *tmp = NULL;

    for (tmp = *tempPtr; (((*tmp >= '0') && (*tmp <= '9')) ||
                          ((*tmp >= 'a') && (*tmp <= 'f')) ||
                          ((*tmp >= 'A') && (*tmp <= 'F'))); tmp++)
    {
        value = (value * 10) + (*tmp & 0xf);
    }

    if (*tempPtr == tmp)
    {
        value = -1;
    }
    *tempPtr = tmp;
    return (value);
}

PUBLIC tOspfQMsg   *
SnmpOspfIfMsgAlloc (VOID)
{
    tOspfQMsg          *pOspfQMsg;
    QMSG_ALLOC (pOspfQMsg);
    return pOspfQMsg;
}
PUBLIC INT4
SnmpOspfIfMsgSend (tOspfQMsg * pOspfQMsg)
{
    UINT1               u1OpCode = pOspfQMsg->ospfSnmpIfParam.u1OpCode;

    pOspfQMsg->u4MsgType = OSPF_SNMP_IF_MSG;
    if (OspfSnmpIfHandler (&(pOspfQMsg->ospfSnmpIfParam)) == OSPF_SUCCESS)
    {
        if (u1OpCode != OSPF_GR_PERFORM_RESTART)
        {
            QMSG_FREE (pOspfQMsg);
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SnmpSendStatusChgTrapInCxt                                 */
/*                                                                           */
/* Description  : This routine send the trap regarding the status change     */
/*                in restarting router and helper                            */
/*                                                                           */
/*                1. Restart status change                                   */
/*                2. Helper status change                                    */
/*                3. Virtual Neighbor Helper status change                   */
/*                                                                           */
/* Input        : pOspfCxt     - Pointer to context                          */
/*                pNbr         - Pointer to neighbor                         */
/*                u4Trap       - Trap info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
SnmpSendStatusChgTrapInCxt (tOspfCxt * pOspfCxt, tNeighbor * pNbr, UINT4 u4Trap)
{
    tRstStatChgTrapInfo rstStatChgTrapInfo;
    tNbrRstStatChgTrapInfo nbrRstStatChgTrapInfo;
    tVifRstStatChgTrapInfo vifRstStatChgTrapInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSendStatusChgTrap\r\n");

    if (u4Trap == RST_STATUS_CHANGE_TRAP)
    {
        MEMSET (&rstStatChgTrapInfo, 0, sizeof (tRstStatChgTrapInfo));

        if (IS_TRAP_ENABLED_IN_CXT (RST_STATUS_CHANGE_TRAP, pOspfCxt))
        {
            IP_ADDR_COPY (rstStatChgTrapInfo.rtrId, pOspfCxt->rtrId);
            rstStatChgTrapInfo.u1RestartStatus = pOspfCxt->u1RestartStatus;
            rstStatChgTrapInfo.u4GracePeriod = pOspfCxt->u4GracePeriod;
            rstStatChgTrapInfo.u1RestartExitReason
                = pOspfCxt->u1RestartExitReason;
            SnmpifSendTrapInCxt (pOspfCxt, RST_STATUS_CHANGE_TRAP,
                                 &rstStatChgTrapInfo);
        }
    }
    else if (u4Trap == NBR_RST_STATUS_CHANGE_TRAP)
    {
        MEMSET (&nbrRstStatChgTrapInfo, 0, sizeof (tNbrRstStatChgTrapInfo));

        if (IS_TRAP_ENABLED_IN_CXT (NBR_RST_STATUS_CHANGE_TRAP,
                                    pNbr->pInterface->pArea->pOspfCxt))
        {
            IP_ADDR_COPY (nbrRstStatChgTrapInfo.rtrId,
                          pNbr->pInterface->pArea->pOspfCxt->rtrId);
            IP_ADDR_COPY (nbrRstStatChgTrapInfo.nbrId, pNbr->nbrId);
            IP_ADDR_COPY (nbrRstStatChgTrapInfo.nbrIpAddr, pNbr->nbrIpAddr);
            nbrRstStatChgTrapInfo.u4NbrAddrlessIf = pNbr->u4NbrAddrlessIf;
            nbrRstStatChgTrapInfo.u1NbrHelperStatus = pNbr->u1NbrHelperStatus;
            nbrRstStatChgTrapInfo.u1NbrHelperExitReason
                = pNbr->u1NbrHelperExitReason;
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                if (TmrGetRemainingTime
                    (gTimerLst,
                     (tTmrAppTimer *) (&(pNbr->inactivityTimer.timerNode)),
                     &nbrRstStatChgTrapInfo.u4NbrHelperAge) == TMR_FAILURE)
                {
                    nbrRstStatChgTrapInfo.u4NbrHelperAge = 0;
                }
                else
                {
                    nbrRstStatChgTrapInfo.u4NbrHelperAge =
                        (nbrRstStatChgTrapInfo.u4NbrHelperAge
                         / NO_OF_TICKS_PER_SEC);
                }
            }
        }
        SnmpifSendTrapInCxt (pOspfCxt, NBR_RST_STATUS_CHANGE_TRAP,
                             &nbrRstStatChgTrapInfo);
    }
    else if (u4Trap == VIRT_NBR_RST_STATUS_CHANGE_TRAP)
    {
        MEMSET (&vifRstStatChgTrapInfo, 0, sizeof (tVifRstStatChgTrapInfo));

        if (IS_TRAP_ENABLED_IN_CXT (VIRT_NBR_RST_STATUS_CHANGE_TRAP,
                                    pNbr->pInterface->pArea->pOspfCxt))
        {
            IP_ADDR_COPY (vifRstStatChgTrapInfo.rtrId,
                          pNbr->pInterface->pArea->pOspfCxt->rtrId);
            IP_ADDR_COPY (vifRstStatChgTrapInfo.VifNbrId, pNbr->nbrId);
            IP_ADDR_COPY (vifRstStatChgTrapInfo.VifNbrAreaId,
                          pNbr->pInterface->pArea->areaId);
            vifRstStatChgTrapInfo.u1VifNbrHelperStatus
                = pNbr->u1NbrHelperStatus;
            vifRstStatChgTrapInfo.u1VifNbrHelperExitReason
                = pNbr->u1NbrHelperExitReason;
            if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                if (TmrGetRemainingTime
                    (gTimerLst,
                     (tTmrAppTimer *) (&(pNbr->inactivityTimer.timerNode)),
                     &vifRstStatChgTrapInfo.u4VifNbrHelperAge) == TMR_FAILURE)
                {
                    vifRstStatChgTrapInfo.u4VifNbrHelperAge = 0;
                }
                else
                {
                    vifRstStatChgTrapInfo.u4VifNbrHelperAge =
                        (vifRstStatChgTrapInfo.u4VifNbrHelperAge
                         / NO_OF_TICKS_PER_SEC);
                }
            }
        }
        SnmpifSendTrapInCxt (pOspfCxt, VIRT_NBR_RST_STATUS_CHANGE_TRAP,
                             &vifRstStatChgTrapInfo);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSendStatusChgTrap\r\n");
}

/********************************************************************
                    End  of  file ossnmpif.c
********************************************************************/
