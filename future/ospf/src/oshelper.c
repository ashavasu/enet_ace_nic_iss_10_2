
#include "osinc.h"
/*****************************************************************************/
/*                                                                           */
/* $Id: oshelper.c,v 1.9 2014/03/01 11:40:45 siva Exp $                      */

/* Function     : GrHelperProcessGraceLSA                                    */
/*                                                                           */
/* Description  : Reference : RFC-3623 section 3.1                           */
/*                This procedure takes care of the processing of a grace     */
/*                LSA received.                                              */
/*                This function  will be called when the Helper receives     */
/*                Grace LSA from the Restarting router                       */
/*                                                                           */
/* Input        : pNbr    - Pointer to the neighbor from where the GraceLSA  */
/*                          is received                                      */
/*                pHeader - Pointer to the LSA header                        */
/*                pLsa    - Pointer to the received Grace LSA                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS, if successfully processed                    */
/*                OSPF_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrHelperProcessGraceLSA (tNeighbor * pNbr, tLsHeader * pHeader, UINT1 *pLsa)
{
    UINT4               u4GracePeriod;
    UINT1              *pu1CurPtr = NULL;
#ifdef HIGH_PERF_RXMT_LST_WANTED
    tRxmtNode           RxmtNode;
    tRxmtNode          *pRxmtNode = NULL;
    tRxmtNode          *pSearchRxmtNode = NULL;
#endif
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               u1RestartReason;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tInterface         *pInterface = NULL;
    tNeighbor          *pAdvNbr = NULL;

    if (pNbr == NULL || pNbr->pInterface == NULL ||
        pNbr->pInterface->pArea == NULL ||
        pNbr->pInterface->pArea->pOspfCxt == NULL)
    {
        return OSPF_FAILURE;
    }

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;
    pInterface = pNbr->pInterface;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrHelperProcessGraceLSA\r\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pAdvNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

        if (UtilIpAddrComp (pAdvNbr->nbrId, pHeader->advRtrId) == OSPF_EQUAL)
        {
            break;
        }
    }
    if (pAdvNbr == NULL)
    {
        return OSPF_FAILURE;
    }
    pu1CurPtr = pLsa;
    pu1CurPtr = pu1CurPtr + OSPF_LSA_HDR_GRACEPERIOD_OFFSET;
    u4GracePeriod = LGET4BYTE (pu1CurPtr);    /* pLsa starts from TLV for Grace period */
    pu1CurPtr = pu1CurPtr + OSPF_LSA_HDR_RESTARTREASON_OFFSET;
    u1RestartReason = LGET1BYTE (pu1CurPtr);

    /* 1. Check if the helper is performing GR */
    if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
    {
        OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                      "This router is a GR router,Cann't act as Helper\r\n");
        pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
        return OSPF_FAILURE;
    }
    /* 2. Check Whether Helper is having support for this restart */
    if (!((OSPF_ONE << u1RestartReason) & (pOspfCxt->u1HelperSupport)))
    {
        OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                      "Cann't act as helper,Helper is not having support for this restart \r\n");
        pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
        return OSPF_FAILURE;
    }

    /* 3. Check if the neighbor from where grace LSA is received is fully adjacent,
       If not fully adjacent, exit the helper  */

    if ((!(IS_NBR_FULL (pAdvNbr))) && (NsmToBecomeAdj (pAdvNbr) == OSPF_TRUE))
    {
        OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                      "Cann't act as Helper,Neighbor Status is not full\r\n");
        pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
        return OSPF_FAILURE;
    }
    if ((NsmToBecomeAdj (pAdvNbr) == OSPF_FALSE)
        && (!((pAdvNbr->u1NsmState == NBRS_2WAY)
              && (pInterface->u1IsmState == IFS_DR_OTHER))))
    {
        OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                      "Cann't act as Helper,Neighbor Status is not full\r\n");
        pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
        return OSPF_FAILURE;
    }

    /* 4. LSA Age should be less than the Grace Period in Grace LSA */
    if (pHeader->u2LsaAge >= u4GracePeriod)
    {
        OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                      "Cann't act as helper,Max aged Grace LSA received \r\n");
        pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
        return OSPF_FAILURE;
    }

#ifdef HIGH_PERF_RXMT_LST_WANTED
    /* Check for LSA retransmission list  for  GR Router */
    pSearchRxmtNode = &RxmtNode;

    pSearchRxmtNode->pNbr = pAdvNbr;

    /* If strict LSA check is enabled, check for topology change */
    if (pAdvNbr->pInterface->pArea->pOspfCxt->u1StrictLsaCheck == OSPF_TRUE)
    {
        if ((pRxmtNode = RBTreeGet (gOsRtr.pRxmtLstRBRoot,
                                    pSearchRxmtNode)) != NULL)
        {
            /* If topology change is detected, exit the helper */
            if (pRxmtNode->pLsaInfo->u1LsaRefresh == OSPF_TRUE)
            {

                OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                              "Cann't act as helper,Topology Change detected \r\n");
                pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
                return OSPF_FAILURE;
            }
        }
    }
#endif

    OSPF_EXT_TRC1 (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                   " Acting as Helper for the Neighbor %x \r\n",
                   OSPF_CRU_BMC_DWFROMPDU (pAdvNbr->nbrId));

    /* This router can enter into the helper process */
    pAdvNbr->u1NbrHelperStatus = OSPF_GR_HELPING;
    pOspfCxt->u1HelperStatus = OSPF_GR_HELPING;
    pAdvNbr->u1NbrHelperExitReason = OSPF_HELPER_INPROGRESS;

    /* Update u1GRSupportFlag in pInterface, this flag is required 
       in helper mode to know atlease on GR router present in this 
       interface as a neighbor */
    pAdvNbr->pInterface->u1GRSupportFlag = OSPF_TRUE;

    /* Start the Grace Timer for GR Neighbor */
    /* Helper will support upto the configured Grace time limit only */
    if ((pOspfCxt->u4HelperGrTimeLimit > 0) &&
        (u4GracePeriod > pOspfCxt->u4HelperGrTimeLimit))
    {
        u4GracePeriod = pOspfCxt->u4HelperGrTimeLimit;
    }
    OspfRmSendNbrStateChange (pAdvNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              pAdvNbr,
                              OSPF_RED_SYNC_NBR_STATE_HELPER, u4GracePeriod);
    TmrSetTimer (&(pAdvNbr->helperGraceTimer), HELPER_GRACE_TIMER,
                 (NO_OF_TICKS_PER_SEC * u4GracePeriod));

    /* Send trap notification regarding the status change.
     * Nbr parameter is required for Neighbor status change
     * when the router is acting as helper */
    SnmpSendStatusChgTrapInCxt (pOspfCxt, pAdvNbr, NBR_RST_STATUS_CHANGE_TRAP);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrHelperProcessGraceLSA\r\n");

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrExitHelper                                               */
/*                                                                           */
/* Description  : Reference : RFC-3623 section 3.2                           */
/*                This function will be called when the router exits from    */
/*                from helper mode. It dows the following                    */
/*                Stops the helper grace timer,  does DR/BDR election,       */
/*                If the exit reason is unsuccessful, makes the GR router    */
/*                down and generates N/W LSA if the router is elected as DR  */
/*                                                                           */
/* Input        : pNbr    - Pointer to the neighbor for which the helper     */
/*                          support to be exited                             */
/*                u1ExitReason - Helper exit reason                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS, if successfully exited                       */
/*                OSPF_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrExitHelper (UINT1 u1ExitReason, tNeighbor * pNbr)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tArea              *pArea;

    tOspfCxt           *pOspfCxt = NULL;
    tNeighbor          *pNeighbor = NULL;

    if (pNbr == NULL || pNbr->pInterface == NULL)
    {
        return OSPF_FAILURE;
    }

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: GrExitHelper\r\n");

    pNbr->u1NbrHelperExitReason = u1ExitReason;
    pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;

    /* Update the u1GRSupportFlag, If none of the neighbors in this interface
     *        are GR router, reset the u1GRSupportFlag */
    TMO_SLL_Scan (&(pNbr->pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {
        pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pLstNode);
        if (pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING)
        {
            pNeighbor->pInterface->u1GRSupportFlag = OSPF_TRUE;
            break;
        }
        pNeighbor->pInterface->u1GRSupportFlag = OSPF_FALSE;
    }

    /* Reset Helper Status */
    pOspfCxt->u1HelperStatus = OSPF_GR_NOT_HELPING;
    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
        if (pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING)
        {
            pOspfCxt->u1HelperStatus = OSPF_GR_HELPING;
            break;
        }
    }

    TmrStopTimer (gTimerLst, &(pNbr->helperGraceTimer.timerNode));

    OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                  "Start the DR/BDR election for Helper exit\r\n");
    /* Start the DR/BDR election for the interface which was undergone GR */
    if ((pNbr->pInterface->u1NetworkType != IF_PTOP)
        && (pNbr->pInterface->u1NetworkType != IF_PTOMP))
    {
        HpElectDesgRtr (pNbr->pInterface);
    }

    /* Successful Helper exit, Generate Router and N/w LSA */
    OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                  " Successful Helper exit,Sending Router and N/W LSAs \r\n");
    /* Send Router LSA in All area associated with this context */

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        OlsGenerateLsa (pArea, ROUTER_LSA, &(pArea->pOspfCxt->rtrId),
                        (UINT1 *) pArea);

    }

    if (IS_DR (pNbr->pInterface))
    {
        /* If this interface is DR, generate N/W LSA */
        OlsGenerateLsa (pNbr->pInterface->pArea, NETWORK_LSA,
                        &(pNbr->pInterface->ifIpAddr),
                        (UINT1 *) pNbr->pInterface);
    }
    if (u1ExitReason != (UINT1) OSPF_HELPER_COMPLETED)
    {
        OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                      "Unsuccessful Helper exit,Making the neighbor down \r\n");
        NsmDown (pNbr);
    }
    /* If the unsuccessful helper exit, Make the Restarting Neighbor down */

    /* Send trap notification regarding the status change.
     *  Nbr parameter is required for Neighbor status change
     *  when the router is acting as helper */
    SnmpSendStatusChgTrapInCxt (pOspfCxt, pNbr, NBR_RST_STATUS_CHANGE_TRAP);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: GrExitHelper\r\n");

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrFindTopologyChangeLSA                                    */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the LSA. Compares the two given LSAs and returns */
/*                TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE else     */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received LSA1                     */
/*                pLsa2   - Pointer to the received LSA2                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSPF_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

INT4
GrFindTopologyChangeLSA (tLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tOspfCxt           *pOspfCxt = NULL;

    pOspfCxt = pLsa1->pOspfCxt;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrFindTopologyChangeLSA\r\n");

    switch (pLsa1->lsaId.u1LsaType)
    {
        case ROUTER_LSA:

            OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                          "Checking Topology Change for Router LSA \r\n");
            return (CheckRtLSAForTopologyChg (pLsa1, pLsa2));

        case NETWORK_LSA:
            OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                          "Checking Topology Change for Network LSA \r\n");
            return (CheckNwLSAForTopologyChg (pLsa1, pLsa2));

        case NETWORK_SUM_LSA:
            OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                          "Checking Topology Change for Summary LSA \r\n");
            return (CheckSumLSAForTopologyChg (pLsa1, pLsa2));

        case AS_EXT_LSA:
            OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                          "Checking Topology Change for External LSA \r\n");
            return (CheckExtLSAForTopologyChg (pLsa1, pLsa2));

        default:
            OSPF_EXT_TRC (OSPF_HELPER_TRC, pOspfCxt->u4OspfCxtId,
                          "Unknown LSA Type\r\n");
            return OSPF_FAILURE;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : CheckRtLSAForTopologyChg                                   */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the Router LSA. Compares the two given LSAs and  */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Router LSA1              */
/*                pLsa2   - Pointer to the received Router LSA2              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSPF_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

INT4
CheckRtLSAForTopologyChg (tLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tRtrLsaRtInfo      *pRtrLsaLink = NULL;
    tRtrLsaRtInfo       RtrLsaLink;
    tRtrLsaRtInfo      *pRtrLsaLinkFromLSDB = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1              *pTmpLsa1 = NULL;
    UINT1              *pTmpLsa2 = NULL;
    INT4                u4Cnt = 0;
    UINT2               u2NoOfRouterLSALinks1;
    UINT2               u2NoOfRouterLSALinks2;
    UINT1               u1TosCnt = 0;
    UINT1               u1Tos = 0;

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))

    {
        return OSPF_FAILURE;
    }

    pOspfCxt = pLsa1->pOspfCxt;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: CheckRtLSAForTopologyChg\r\n");

    pTmpLsa1 = pLsa1->pLsa + LS_HEADER_SIZE;
    pTmpLsa2 = pLsa2 + LS_HEADER_SIZE;

    /* Compare the W, E, B bits in the LSA */
    if (MEMCMP (pTmpLsa1, pTmpLsa2, 1) != 0)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    pTmpLsa1 = pTmpLsa1 + 2;
    pTmpLsa2 = pTmpLsa2 + 2;

    /* Compare the  No.of Links in the Router LSA */
    u2NoOfRouterLSALinks1 = LGET2BYTE (pTmpLsa1);
    u2NoOfRouterLSALinks2 = LGET2BYTE (pTmpLsa2);

    if (u2NoOfRouterLSALinks1 != u2NoOfRouterLSALinks2)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    /* Compare the  LinkType, LinkID and LinkData */
    /* Construct the Router LSA RBTree with the Router LSA derived from OSPF LSDB */
    for (u4Cnt = 0; u4Cnt < u2NoOfRouterLSALinks1; u4Cnt++)
    {
        RTR_LSA_LINK_ALLOC (pRtrLsaLink);

        if (pRtrLsaLink == NULL)
        {
            RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);
            return OSPF_FAILURE;
        }
        MEMSET (pRtrLsaLink, 0, sizeof (tRtrLsaRtInfo));

        pRtrLsaLink->u4LinkId = LGET4BYTE (pTmpLsa1);
        pRtrLsaLink->u4LinkData = LGET4BYTE (pTmpLsa1);
        pRtrLsaLink->u1Type = LGET1BYTE (pTmpLsa1);
        u1TosCnt = LGET1BYTE (pTmpLsa1);
        pRtrLsaLink->u4Metric[TOS_0] = LGET2BYTE (pTmpLsa1);
        while (u1TosCnt--)
        {
            u1Tos = LGET1BYTE (pTmpLsa1);
            /* Skip one unsed byte */
            pTmpLsa1 = pTmpLsa1 + 1;
            pRtrLsaLink->u4Metric[DECODE_TOS (u1Tos)] = LGET2BYTE (pTmpLsa1);
        }

        if (RBTreeAdd (gOsRtr.pRtrLsaCheck, pRtrLsaLink) == RB_FAILURE)
        {
            RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);
            return OSPF_FAILURE;
        }
    }

    for (u4Cnt = 0; u4Cnt < u2NoOfRouterLSALinks2; u4Cnt++)
    {
        MEMSET (&RtrLsaLink, 0, sizeof (tRtrLsaRtInfo));

        RtrLsaLink.u4LinkId = LGET4BYTE (pTmpLsa2);
        RtrLsaLink.u4LinkData = LGET4BYTE (pTmpLsa2);
        RtrLsaLink.u1Type = LGET1BYTE (pTmpLsa2);
        u1TosCnt = LGET1BYTE (pTmpLsa2);
        RtrLsaLink.u4Metric[TOS_0] = LGET2BYTE (pTmpLsa2);
        while (u1TosCnt--)
        {
            u1Tos = LGET1BYTE (pTmpLsa2);
            /* Skip one unsed byte */
            pTmpLsa2 = pTmpLsa2 + 1;
            RtrLsaLink.u4Metric[DECODE_TOS (u1Tos)] = LGET2BYTE (pTmpLsa2);
        }

        pRtrLsaLinkFromLSDB = RBTreeGet (gOsRtr.pRtrLsaCheck, &RtrLsaLink);

        if (pRtrLsaLinkFromLSDB == NULL)
        {
            RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);
            /* Delete the creates Router LSA RBTree */
            return OSPF_TOPOLOGY_CHANGE;
        }

    }

    RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: CheckRtLSAForTopologyChg\r\n");
    return OSPF_NO_TOPOLOGY_CHANGE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CheckNwLSAForTopologyChg                                   */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the Network LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Network LSA1             */
/*                pLsa2   - Pointer to the received Network LSA2             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSPF_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

INT4
CheckNwLSAForTopologyChg (tLsaInfo * pLsa1, UINT1 *pLsa2)
{
    UINT2               u2Cnt = 0;
    UINT2               u2NtLSALen = 0;
    UINT2               u2NtLSAFromLSDBLen = 0;
    tNtLsaRtInfo       *pNtLsaRtrInfo;
    tNtLsaRtInfo        NtLsaRtrInfo;
    tNtLsaRtInfo       *pNtLsaFromLSDB = NULL;
    UINT1              *pTmpLsa1 = NULL;
    UINT1              *pTmpLsa2 = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tLsHeader           lsHeader;

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSPF_FAILURE;
    }
    MEMSET (&NtLsaRtrInfo, 0, sizeof (tNtLsaRtInfo));
    MEMSET (&lsHeader, 0, sizeof (tLsHeader));

    pOspfCxt = pLsa1->pOspfCxt;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: CheckNwLSAForTopologyChg\r\n");

    UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    /* Construct the Network  LSA RBTree with the Network LSA derived from OSPF LSDB,
       RBTree Key is Attached Router Id */

    /* Reduce 20 bytes (LSA Header from Nt LSA Length */
    u2NtLSALen = (UINT2) (pLsa1->u2LsaLen - 20);

    u2NtLSAFromLSDBLen = (UINT2) (lsHeader.u2LsaLen - 20);

    pTmpLsa1 = pLsa1->pLsa + LS_HEADER_SIZE;
    pTmpLsa2 = pLsa2;

    if (MEMCMP (pTmpLsa1, pTmpLsa2, sizeof (tIPADDRMASK) != 0))
    {
        return OSPF_TOPOLOGY_CHANGE;
    }
    pTmpLsa1 = pTmpLsa1 + sizeof (tIPADDRMASK);
    pTmpLsa2 = pTmpLsa2 + sizeof (tIPADDRMASK);

    u2NtLSALen = (UINT2) ((u2NtLSALen - MAX_IP_ADDR_LEN) / MAX_IP_ADDR_LEN);
    u2NtLSAFromLSDBLen =
        (UINT2) ((u2NtLSAFromLSDBLen - MAX_IP_ADDR_LEN) / MAX_IP_ADDR_LEN);

    for (u2Cnt = 0; u2Cnt < u2NtLSAFromLSDBLen; u2Cnt++)
    {
        NW_LSA_LINK_ALLOC (pNtLsaRtrInfo);

        if (pNtLsaRtrInfo == NULL)
        {
            RBTreeDrain (gOsRtr.pNwLsaCheck, UtilRBFreeNwLinks, 0);
            return OSPF_FAILURE;
        }

        MEMSET (pNtLsaRtrInfo, 0, sizeof (tNtLsaRtInfo));

        pNtLsaRtrInfo->u4AttachedRtrId = LGET4BYTE (pTmpLsa1);

        if (RBTreeAdd (gOsRtr.pNwLsaCheck, pNtLsaRtrInfo) == RB_FAILURE)
        {
            RBTreeDrain (gOsRtr.pNwLsaCheck, UtilRBFreeNwLinks, 0);
            return OSPF_FAILURE;
        }
    }

    for (u2Cnt = 0; u2Cnt < u2NtLSALen; u2Cnt++)
    {
        MEMSET (&NtLsaRtrInfo, 0, sizeof (tNtLsaRtInfo));

        NtLsaRtrInfo.u4AttachedRtrId = LGET4BYTE (pTmpLsa2);

        pNtLsaFromLSDB = RBTreeGet (gOsRtr.pNwLsaCheck, &NtLsaRtrInfo);

        if (pNtLsaFromLSDB == NULL)
        {
            RBTreeDrain (gOsRtr.pNwLsaCheck, UtilRBFreeNwLinks, 0);
            return OSPF_TOPOLOGY_CHANGE;
        }
    }

    RBTreeDrain (gOsRtr.pNwLsaCheck, UtilRBFreeNwLinks, 0);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: CheckNwLSAForTopologyChg\r\n");

    return OSPF_NO_TOPOLOGY_CHANGE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CheckSumLSAForTopologyChg                                  */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the summary LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Summary LSA1             */
/*                pLsa2   - Pointer to the received summary LSA2             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSPF_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

INT4
CheckSumLSAForTopologyChg (tLsaInfo * pLsa1, UINT1 *pLsa2)
{

    tLsaTosMetric       aLsa1TosMetric[OSPF_MAX_METRIC];
    tLsaTosMetric       aLsa2TosMetric[OSPF_MAX_METRIC];
    UINT1              *pTmpLsa1 = NULL;
    UINT1              *pTmpLsa2 = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tLsHeader           lsHeader;
    UINT1               u1Tos = 0;

    MEMSET (&aLsa1TosMetric, 0, OSPF_MAX_METRIC * sizeof (tLsaTosMetric));
    MEMSET (&aLsa2TosMetric, 0, OSPF_MAX_METRIC * sizeof (tLsaTosMetric));
    MEMSET (&lsHeader, 0, sizeof (tLsHeader));
    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSPF_FAILURE;
    }
    pOspfCxt = pLsa1->pOspfCxt;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: CheckSumLSAForTopologyChg\r\n");

    UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);
    pTmpLsa1 = pLsa1->pLsa + LS_HEADER_SIZE;
    pTmpLsa2 = pLsa2;
    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    /* Compare  the netmask */
    if (MEMCMP (pTmpLsa1, pTmpLsa2, MAX_IP_ADDR_LEN) != 0)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    pTmpLsa1 = pTmpLsa1 + MAX_IP_ADDR_LEN;
    pTmpLsa2 = pTmpLsa2 + MAX_IP_ADDR_LEN;

    /* construct the TosMetric array with summary LSA's TOS and Metric field */
    while ((pTmpLsa1 - pLsa1->pLsa) < pLsa1->u2LsaLen)
    {
        if ((*pTmpLsa1 > OSPF_MAX_METRIC) || (*pTmpLsa2 > OSPF_MAX_METRIC))
        {
            return OSPF_TOPOLOGY_CHANGE;
        }
        u1Tos = LGET1BYTE (pTmpLsa1);
        aLsa1TosMetric[DECODE_TOS (u1Tos)].u1Tos = u1Tos;
        aLsa1TosMetric[DECODE_TOS (u1Tos)].u4Metric = LGET3BYTE (pTmpLsa1);
        u1Tos = LGET1BYTE (pTmpLsa2);
        aLsa2TosMetric[DECODE_TOS (u1Tos)].u1Tos = u1Tos;
        aLsa2TosMetric[DECODE_TOS (u1Tos)].u4Metric = LGET3BYTE (pTmpLsa2);
    }

    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (MEMCMP
            (&aLsa1TosMetric[u1Tos], &aLsa2TosMetric[u1Tos],
             sizeof (tLsaTosMetric)))
        {
            return OSPF_TOPOLOGY_CHANGE;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: CheckSumLSAForTopologyChg\r\n");
    return OSPF_NO_TOPOLOGY_CHANGE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CheckExtLSAForTopologyChg                                  */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*               receiving the external LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received External LSA1            */
/*                pLsa2   - Pointer to the received External LSA2            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSPF_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

INT4
CheckExtLSAForTopologyChg (tLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tExtLsaRtInfo       aExtLsa1Info[OSPF_MAX_METRIC];
    tExtLsaRtInfo       aExtLsa2Info[OSPF_MAX_METRIC];
    tLsHeader           lsHeader;
    UINT1              *pTmpLsa1 = NULL;
    UINT1              *pTmpLsa2 = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               u1Tos = 0;

    MEMSET (&aExtLsa1Info, 0, OSPF_MAX_METRIC * sizeof (tExtLsaRtInfo));
    MEMSET (&aExtLsa2Info, 0, OSPF_MAX_METRIC * sizeof (tExtLsaRtInfo));
    MEMSET (&lsHeader, 0, sizeof (tLsHeader));

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSPF_FAILURE;
    }

    pOspfCxt = pLsa1->pOspfCxt;
    UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: CheckExtLSAForTopologyChg\r\n");

    pTmpLsa1 = pLsa1->pLsa + LS_HEADER_SIZE;
    pTmpLsa2 = pLsa2;

    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    /* Compare  the netmask */
    if (MEMCMP (pTmpLsa1, pTmpLsa2, MAX_IP_ADDR_LEN) != 0)
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    pTmpLsa1 = pTmpLsa1 + MAX_IP_ADDR_LEN;
    pTmpLsa2 = pTmpLsa2 + MAX_IP_ADDR_LEN;

    /* construct the TosMetric array with ExternalLSA's TOS and Metric field */
    while ((pTmpLsa1 - pLsa1->pLsa) < pLsa1->u2LsaLen)
    {
        if (((*pTmpLsa1 & OSPF_TOS_IN_EXT_LSA) > OSPF_MAX_METRIC) ||
            ((*pTmpLsa2 & OSPF_TOS_IN_EXT_LSA) > OSPF_MAX_METRIC))
        {
            return OSPF_TOPOLOGY_CHANGE;
        }
        u1Tos = LGET1BYTE (pTmpLsa1);
        u1Tos = (UINT1) DECODE_TOS ((u1Tos & OSPF_TOS_IN_EXT_LSA));
        aExtLsa1Info[u1Tos].aExtLinkMetric.u4Metric = LGET3BYTE (pTmpLsa1);
        aExtLsa1Info[u1Tos].u4ForwardingAddress = LGET4BYTE (pTmpLsa1);
        aExtLsa1Info[u1Tos].u4ExtRtTag = LGET4BYTE (pTmpLsa1);
        u1Tos = LGET1BYTE (pTmpLsa2);
        u1Tos = (UINT1) DECODE_TOS ((u1Tos & OSPF_TOS_IN_EXT_LSA));
        aExtLsa2Info[u1Tos].aExtLinkMetric.u4Metric = LGET3BYTE (pTmpLsa2);
        aExtLsa2Info[u1Tos].u4ForwardingAddress = LGET4BYTE (pTmpLsa2);
        aExtLsa2Info[u1Tos].u4ExtRtTag = LGET4BYTE (pTmpLsa2);
    }

    if (MEMCMP
        (&aExtLsa1Info, &aExtLsa2Info,
         (OSPF_MAX_METRIC * sizeof (tExtLsaRtInfo))))
    {
        return OSPF_TOPOLOGY_CHANGE;
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: CheckExtLSAForTopologyChg\r\n");
    return OSPF_NO_TOPOLOGY_CHANGE;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrCompareRouterLsaEntry                                    */
/*                                                                           */
/* Description  : Key comparision function for router LSA RBTree search      */
/*                Compare the Link Id, Link Data and Link Type of the Links  */
/*                in LSA                                                     */
/*                                                                           */
/* Input        : e1   - Pointer to the Router LSA1                          */
/*                e2   - Pointer to the Router LSA2                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
GrCompareRouterLsaEntry (tRBElem * e1, tRBElem * e2)
{
    tRtrLsaRtInfo      *pRtrLsa1 = e1;
    tRtrLsaRtInfo      *pRtrLsa2 = e2;

    if (pRtrLsa1->u4LinkId > pRtrLsa2->u4LinkId)
    {
        return RB_GREATER;
    }
    else if (pRtrLsa1->u4LinkId < pRtrLsa2->u4LinkId)
    {
        return RB_LESSER;
    }
    else
    {
        if (pRtrLsa1->u4LinkData > pRtrLsa2->u4LinkData)
            return RB_GREATER;
        else if (pRtrLsa1->u4LinkData < pRtrLsa2->u4LinkData)
            return RB_LESSER;
    }

    if (pRtrLsa1->u1Type > pRtrLsa2->u1Type)
    {
        return RB_GREATER;
    }
    else if (pRtrLsa1->u1Type < pRtrLsa2->u1Type)
    {
        return RB_LESSER;
    }
    else
    {
        return RB_EQUAL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrCompareNetworkLsaEntry                                   */
/*                                                                           */
/* Description  : Key comparision function for network LSA RBTree search     */
/*                Compares the attached Router ID                            */
/*                                                                           */
/* Input        : e1   - Pointer to the Network LSA1                         */
/*                e2   - Pointer to the Network LSA2                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/

INT4
GrCompareNetworkLsaEntry (tRBElem * e1, tRBElem * e2)
{
    tNtLsaRtInfo       *pRtrLsa1 = e1;
    tNtLsaRtInfo       *pRtrLsa2 = e2;

    if (pRtrLsa1->u4AttachedRtrId > pRtrLsa2->u4AttachedRtrId)
    {
        return RB_GREATER;
    }

    else if (pRtrLsa1->u4AttachedRtrId > pRtrLsa2->u4AttachedRtrId)
    {
        return RB_LESSER;
    }
    else
    {
        return RB_EQUAL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetHelperSupport                                         */
/*                                                                           */
/* Description  : Routine to set the Helper Support for graceful restart     */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                u1HelperSupport  - Helper Support                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetHelperSupport (tOspfCxt * pOspfCxt, UINT1 u1HelperSupport)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetHelperSupport\r\n");

    if (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
    {
        /* Graceful restart is already in progress. Configuration
         * is not allowed */
        OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                      "Restart is already in progress\r\n");
        return OSPF_FAILURE;
    }
    else
    {
        pOspfCxt->u1HelperSupport = u1HelperSupport;
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetHelperSupport\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetStrictLsaCheck                                        */
/*                                                                           */
/* Description  : Routine to set the Strict LSA Check for graceful restart   */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                u1StrictLsaCheck  -Strict Lsa Check Value                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetStrictLsaCheck (tOspfCxt * pOspfCxt, UINT1 u1StrictLsaCheck)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetStrictLsaCheck\r\n");
    pOspfCxt->u1StrictLsaCheck = u1StrictLsaCheck;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetStrictLsaCheck\r\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrSetHelperGrTimeLimit                                     */
/*                                                                           */
/* Description  : Routine to set the Helper Grace time limit                 */
/*                                                                           */
/* Input        : pOspfCxt         - pointer to OSPF context                 */
/*                u4HelperGrTimeLimit  -Helper Grace time limit              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GrSetHelperGrTimeLimit (tOspfCxt * pOspfCxt, UINT4 u4HelperGrTimeLimit)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: GrSetHelperGrTimeLimit\r\n");

    if (!(pOspfCxt->u1HelperSupport))
    {
        /* Helper Mode is not Enabled. Configuration
         * is not allowed */
        OSPF_EXT_TRC (OSPF_RESTART_TRC, pOspfCxt->u4OspfCxtId,
                      "Restart is already in progress\r\n");
        return OSPF_FAILURE;
    }
    else
    {
        pOspfCxt->u4HelperGrTimeLimit = u4HelperGrTimeLimit;
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: GrSetHelperGrTimeLimit\r\n");
    return OSPF_SUCCESS;
}
