/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osnrlsu.c,v 1.12 2013/12/16 15:36:22 siva Exp $
 *
 * Description: This file also contains procedures 
 *              for maintaining the normal performance
 *              LSA retransmission List
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuAddToRxmtLst                                          */
/*                                                                           */
/* Description  : The specified LSA is added to the link state retransmission*/
/*                list of this neighbor.                                     */
/*                                                                           */
/* Input        : pNbr             : neighbour to which the LSA is to be     */
/*                                    added                                  */
/*                pLsaInfo        : the LSA to be added                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuAddToRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    UINT4               u4BitPosMask;
    UINT4               u4TmrInterval;

    OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "LSA to be added to rxmt list of Nbr %x.%d \n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr), pNbr->u4NbrAddrlessIf);

    if (pLsaInfo->u4RxmtCount == 0)
    {
        /* If the Grace LSA is to be added, add it at the beginning of
         * the list */
        if ((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
            (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
        {
            TMO_DLL_Insert (&(pLsaInfo->pOspfCxt->rxmtLst), NULL,
                            &(pLsaInfo->rxmtNode));
        }
        else
        {
            TMO_DLL_Add (&(pLsaInfo->pOspfCxt->rxmtLst), &(pLsaInfo->rxmtNode));
        }
    }
    if (!(IS_RXMT_BIT_SET ((pNbr->i2NbrsTableIndex), (pLsaInfo))))
    {
        u4BitPosMask = FIRST_BIT_SET_MASK;
        SET_BIT_POSITION (pNbr->i2NbrsTableIndex, u4BitPosMask,
                          pLsaInfo->aRxmtBitMapFlag);
        pLsaInfo->u4RxmtCount++;
        pNbr->lsaRxmtDesc.u4LsaRxmtCount++;

        /* The Rxmt timer of neighbohr is being 
           started only if LSA is added on to Rxmt list */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 1)
        {
            u4TmrInterval = NO_OF_TICKS_PER_SEC *
                (pNbr->pInterface->u2RxmtInterval);
            TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer), LSA_RXMT_TIMER,
                         ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ?
                          UtilJitter (u4TmrInterval,
                                      OSPF_JITTER) : u4TmrInterval));
        }
    }

    OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "LSA Added To Rxmt Lst Of Nbr %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuRxmtLsu                                               */
/*                                                                           */
/* Description  : This procedure is called when the rxmt timer fires for a   */
/*                particular neighbor. This procedure constructs a ls update */
/*                packet from the top of the retransmission list and         */
/*                transmits it to the neighbor.                              */
/*                                                                           */
/* Input        : pNbr            : neighbour to whom the LS update packet   */
/*                                   is to be retransmitted                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuRxmtLsu (tNeighbor * pNbr)
{

    tLsUpdate           lsUpdate;
    tTMO_DLL_NODE      *pLstNode;
    tLsaInfo           *pLsaInfo;
    tLsHeader          *pLsHeader;
    tLsaInfo           *pGrLsaInfo = NULL;    /* pointer to Grace LSA
                                             * info structure */
    INT4                i4Counter = OSPF_ZERO;

    /* check rxmt count for neighbor */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        return;
    }

    OSPF_NBR_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Retransmission to be done to Nbr %x Address %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    lsUpdate.pLsuPkt = NULL;
    LsuClearUpdate (&lsUpdate);
    TMO_DLL_Scan (&(pNbr->pInterface->pArea->pOspfCxt->rxmtLst),
                  pLstNode, tTMO_DLL_NODE *)
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_RXMT_LST (pLstNode);

        /* if lsa is not present in nbr's rxmt list consider next lsa in list */

        if (!IS_RXMT_BIT_SET ((pNbr->i2NbrsTableIndex), (pLsaInfo)))
        {
            continue;
        }
        /* Grace LSA should be transmitted twice to each neighbor.
         * If no ack has been recieved for this LSA, the process
         * switched to graceful restart mode */
        if ((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
            (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
        {
            if ((pNbr->pInterface->pArea->pOspfCxt->u1RestartStatus
                 == OSPF_RESTART_UNPLANNED) && (pNbr->u1GraceLsaTxCount > 0))
            {
                pGrLsaInfo = pLsaInfo;
                /* This LSA has been re-tranmitted once */
                continue;
            }
            else if (pNbr->u1GraceLsaTxCount >=
                     pNbr->pInterface->pArea->pOspfCxt->u1GrLsaMaxTxCount)
            {
                pGrLsaInfo = pLsaInfo;
                continue;
            }
            pNbr->u1GraceLsaTxCount++;
        }

        if (lsUpdate.pLsuPkt == NULL)
        {
            pLsHeader = (tLsHeader *) (VOID *) (pLsaInfo->pLsa);
            if (IS_VIRTUAL_IFACE (pNbr->pInterface))
            {
                VifSendRxmtTrap (pNbr, pLsHeader, LS_UPDATE_PKT);
            }
            else
            {
                IfSendRxmtTrap (pNbr, pLsHeader, LS_UPDATE_PKT);
            }
        }

        if (LsuAddToLsu (pNbr, pLsaInfo, &lsUpdate, TX_ONCE) == LSU_TXED)
        {
            i4Counter++;
            if (i4Counter == MAX_OSPF_TX_RXMT_LS_UPDATES)
            {
                break;
            }
        }
    }

    /* The above loop might end without the update packet being transmitted */

    LsuSendLsu (pNbr, &lsUpdate);

    /* If pGrLsaInfo is not NULL, then the grace LSA to be re-transmitted
     * to this neighbor has reached the threshold */
    if (pGrLsaInfo != NULL)
    {
        GrDeleteRxmtLst (pNbr, pGrLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuDeleteFromRxmtLst                                     */
/*                                                                           */
/* Description  : The specified LSA is removed from the neighbor's           */
/*                retransmission list. If the LSA is no longer present in any*/
/*                retransmission list then it is removed from the doubly     */
/*                linked list containing all LSAs to be retransmitted. The   */
/*                rxmt_count field is updated and seq_num_wrap_around is     */
/*                verified.                                                  */
/*                                                                           */
/* Input        : pNbr             : neighbour from which the LSA is to be   */
/*                                    deleted                                */
/*                pLsaInfo        : the LSA to be deleted                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuDeleteFromRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    UINT4               u4BitPosMask;
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;

    OSPF_NBR_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pNbr, u4OspfCxtId,
                   "LSA To Be Deleted From Rxmt Lst Of Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    if (pLsaInfo->u4RxmtCount == 0)
    {

        return;
    }
    if (IS_RXMT_BIT_SET ((pNbr->i2NbrsTableIndex), (pLsaInfo)))
    {
        u4BitPosMask = FIRST_BIT_SET_MASK;
        RESET_BIT_POSITION (pNbr->i2NbrsTableIndex, u4BitPosMask,
                            pLsaInfo->aRxmtBitMapFlag);
        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;

        /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
        {
            TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        }

        /* if lsa is not present in any rxmt list then delete from linked list */

        if (--pLsaInfo->u4RxmtCount == 0)
        {
            OspfRmSendLsAckInfo (pLsaInfo);
            TMO_DLL_Delete (&(pLsaInfo->pOspfCxt->rxmtLst),
                            &(pLsaInfo->rxmtNode));
            /* if advt age is MAX_AGE remove it from database */
            if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
            {
                LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
            }
        }
    }
    else
    {
        OSPF_NBR_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, u4OspfCxtId,
                       "LSA Not Present In Rxmt Lst Of Nbr %x.%d\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf);

        return;
    }

    OSPF_NBR_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, u4OspfCxtId,
                   "LSA Deleted From Rxmt Lst Of Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuDeleteNodeFromAllRxmtLst                              */
/*                                                                           */
/* Description  : The specified LSA is removed from all neighbor's rxmt      */
/*                lists. The LSA is also removed from the doubly linked list */
/*                containing LSAs to be retransmitted.                       */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
NrLsuDeleteNodeFromAllRxmtLst (tLsaInfo * pLsaInfo)
{

    UINT4               u4Index;
    tNeighbor          *pNbr;
    UINT4               u4BitPosMask;
    UINT4               u4MaxNbrs = 0;

    u4MaxNbrs = OSPF_MIN (MAX_OSPF_NBRS_LIMIT,
                          FsOSPFSizingParams[MAX_OSPF_NBRS_SIZING_ID].
                          u4PreAllocatedUnits);

    OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "Delete LSA Type %s linkStateId %x adv rtr id  %x area id  %x\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType],
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
               (pLsaInfo->pArea == NULL ? 0 :
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->areaId)));

    for (u4Index = 0; u4Index < u4MaxNbrs; u4Index++)
    {
        if ((pLsaInfo->pOspfCxt->nbrsTable.apNbr[u4Index] != NULL) &&
            (IS_RXMT_BIT_SET (((pLsaInfo->pOspfCxt->
                                nbrsTable.
                                apNbr[u4Index])->i2NbrsTableIndex),
                              (pLsaInfo))))
        {
            pNbr = pLsaInfo->pOspfCxt->nbrsTable.apNbr[u4Index];
            u4BitPosMask = FIRST_BIT_SET_MASK;
            RESET_BIT_POSITION (pNbr->i2NbrsTableIndex, u4BitPosMask,
                                pLsaInfo->aRxmtBitMapFlag);
            pNbr->lsaRxmtDesc.u4LsaRxmtCount--;

            /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
            if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
            {
                TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
            }
        }
    }

    pLsaInfo->u4RxmtCount = 0;
    TMO_DLL_Delete (&(pLsaInfo->pOspfCxt->rxmtLst), &(pLsaInfo->rxmtNode));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuClearRxmtLst                                          */
/*                                                                           */
/* Description  : This procedure clears the retransmission list associated   */
/*                with this neighbor.                                        */
/*                                                                           */
/* Input        : pNbr            : neighbour whose rxmt list has to be      */
/*                                   cleared                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuClearRxmtLst (tNeighbor * pNbr)
{
    tTMO_DLL_NODE      *pLstNode;
    tTMO_DLL_NODE      *pPrevNode = NULL;
    tLsaInfo           *pLsaInfo;
    while ((pLstNode =
            TMO_DLL_Next (&(pNbr->pInterface->pArea->pOspfCxt->rxmtLst),
                          pPrevNode)) != NULL)
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_RXMT_LST (pLstNode);
        if (IS_RXMT_BIT_SET ((pNbr->i2NbrsTableIndex), (pLsaInfo)))
        {
            LsuDeleteFromRxmtLst (pNbr, pLsaInfo);
        }
        else
        {
            pPrevNode = pLstNode;
        }
    }

    pNbr->lsaRxmtDesc.u4LsaRxmtCount = 0;
    TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuFindAndProcessAckFlag                                 */
/*                                                                           */
/* Description  : This funnction processes the LSA ack. If the node is       */
/*                for the LSA and Nbr is present in the retransmission       */
/*                list, it is removed from all the lists.                    */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*                pLsaInfo          : pointer to the advertisement           */
/*                pLsa              : pointer to the LSA packet              */
/*                pRcvdLsHeader     : receiver LSA header                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to tRxmtNode                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuFindAndProcessAckFlag (tNeighbor * pNbr, tLsaInfo * pLsaInfo,
                            UINT1 *pLsa, tLsHeader * pRcvdLsHeader)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pRcvdLsHeader);

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LsuFindAndProcessAckFlag\n");

    if (IS_RXMT_BIT_SET ((pNbr->i2NbrsTableIndex), (pLsaInfo)))
    {
        OSPF_NBR_TRC3 (OSPF_LSU_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->
                       u4OspfCxtId,
                       "Dup LSA Rcvd And Treated As Implied ACK"
                       "Nbr %x LSType %s LSId %x \n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                       au1DbgLsaType[pRcvdLsHeader->u1LsaType],
                       OSPF_CRU_BMC_DWFROMPDU (pRcvdLsHeader->linkStateId));

        LsuDeleteFromRxmtLst (pNbr, pLsaInfo);

        LsuProcessAckFlag (pNbr, pLsa, IMPLIED_ACK);
    }
    else
    {
        OSPF_NBR_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->
                       u4OspfCxtId,
                       "Dup LSA Rcvd And Not Treated As Implied ACK"
                       "Nbr %x LSType %s LSId %x \n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                       au1DbgLsaType[pRcvdLsHeader->u1LsaType],
                       OSPF_CRU_BMC_DWFROMPDU (pRcvdLsHeader->linkStateId));

        LsuProcessAckFlag (pNbr, pLsa, NO_IMPLIED_ACK);
    }
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LsuFindAndProcessAckFlag\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuDeleteFromAllRxmtLst                                  */
/*                                                                           */
/* Description  : The specified LSA is removed from all neighbor's rxmt      */
/*                lists. The LSA is also removed from the doubly linked list */
/*                containing LSAs to be retransmitted.                       */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuDeleteFromAllRxmtLst (tLsaInfo * pLsaInfo)
{

    UINT4               u4Index;
    tOspfCxt           *pOspfCxt = pLsaInfo->pOspfCxt;
    UINT4               u4MaxNbrs = 0;

    u4MaxNbrs = OSPF_MIN (MAX_OSPF_NBRS_LIMIT,
                          FsOSPFSizingParams[MAX_OSPF_NBRS_SIZING_ID].
                          u4PreAllocatedUnits);

    OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "Delete LSA Type %s linkStateId %x adv rtr id  %x area id  %x\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType],
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
               (pLsaInfo->pArea == NULL ? 0 :
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->areaId)));

    for (u4Index = 0; u4Index < u4MaxNbrs; u4Index++)
    {
        if ((pOspfCxt->nbrsTable.apNbr[u4Index] != NULL) &&
            (IS_RXMT_BIT_SET (((pOspfCxt->
                                nbrsTable.apNbr[u4Index])->i2NbrsTableIndex),
                              (pLsaInfo))))
        {
            LsuDeleteFromRxmtLst (pOspfCxt->nbrsTable.apNbr[u4Index], pLsaInfo);
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuCheckAndDelLsaFromRxmtLst                             */
/*                                                                           */
/* Description  : This funnction clears the retransmission node if LSA Ack   */
/*                is pending for the LSA pLsaInfo from the neighbor pNbr     */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*              : pLsaInfo          : pointer to the LsaInfo                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
NrLsuCheckAndDelLsaFromRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    if (IS_RXMT_BIT_SET (pNbr->i2NbrsTableIndex, pLsaInfo))
    {
        LsuDeleteFromRxmtLst (pNbr, pLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuCheckInRxmtLst                                        */
/*                                                                           */
/* Description  : This funnction finds whether the LSA is in the RXMT        */
/*                list or not                                                */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*              : pLsaInfo          : pointer to the LsaInfo                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : non-zero - if LSA is present in RXMT list                  */
/*                zero - if LSA is NOT present in RXMT list                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
NrLsuCheckInRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    return (IS_RXMT_BIT_SET (pNbr->i2NbrsTableIndex, pLsaInfo));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NrLsuInitialiseLsaRxmtInfo                                 */
/*                                                                           */
/* Description  : This funnction Initialises the retransmission node and     */
/*                LSA retransmission bit map                                 */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*              : pLsaInfo          : pointer to the LsaInfo                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NrLsuInitialiseLsaRxmtInfo (tLsaInfo * pLsaInfo)
{
    UINT4               u4MaxNbrs = 0;

    u4MaxNbrs = OSPF_MIN (MAX_OSPF_NBRS_LIMIT,
                          FsOSPFSizingParams[MAX_OSPF_NBRS_SIZING_ID].
                          u4PreAllocatedUnits);
    /* Initialise the Retransmission Node */
    TMO_DLL_Init_Node (&(pLsaInfo->rxmtNode));
    /* Initialise the Retransmission BitMap */
    OS_MEM_SET (pLsaInfo->aRxmtBitMapFlag, 0,
                (u4MaxNbrs / BIT_MAP_FLAG_UNIT_SIZE));
}
