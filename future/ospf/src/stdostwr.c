
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdostwr.c,v 1.9 2014/03/01 11:40:49 siva Exp $
 * 
 * Description: This file contain routines for ospf traps to SNMP
 *    Agent and parameter modification SNMP event routines
 *
 *******************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "ospftlow.h"
# include  "stdostwr.h"
# include  "stdostdb.h"
# include  "osinc.h"

VOID
RegisterSTDOST ()
{
    SNMPRegisterMibWithContextIdAndLock (&stdostOID, &stdostEntry, OspfLock,
                                         OspfUnLock, UtilOspfSetContext,
                                         UtilOspfResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdostOID, (const UINT1 *) "stdostrp");
}

VOID
UnRegisterSTDOST ()
{
    SNMPUnRegisterMib (&stdostOID, &stdostEntry);
    SNMPDelSysorEntry (&stdostOID, (const UINT1 *) "stdostrp");
}

INT4
OspfSetTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetOspfSetTrap (pMultiData->pOctetStrValue));
}

INT4
OspfConfigErrorTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetOspfConfigErrorType (&(pMultiData->i4_SLongValue)));
}

INT4
OspfPacketTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetOspfPacketType (&(pMultiData->i4_SLongValue)));
}

INT4
OspfPacketSrcGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetOspfPacketSrc (&(pMultiData->u4_ULongValue)));
}

INT4
OspfSetTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetOspfSetTrap (pMultiData->pOctetStrValue));
}

INT4
OspfSetTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2OspfSetTrap (pu4Error, pMultiData->pOctetStrValue));
}

INT4
OspfSetTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2OspfSetTrap (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
