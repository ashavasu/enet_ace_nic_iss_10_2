/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: osrtstg.c,v 1.10 2015/10/29 07:27:52 siva Exp $
 *
 * Description:This file contains procedures used for route
 *             calculation staggering
 *
 *******************************************************************/
#include "osinc.h"
#include "osglob.h"
#include "osrtstg.h"

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
extern tOsixQId            gu4OspfVrfMsgQId;
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRtcRelinquish                                          */
/*                                                                           */
/* Description  : This  function is called for OSPF route calculation        */
/*                staggering                                                 */
/*                                                                           */
/* Input        : OSPF Context                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFRtcRelinquish (tOspfCxt * pOspfCxt)
{
    UINT4               u4CurrentTime = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : OSPFRtcRelinquish \n");
    /* Check whether current route calculation is staggered or not */
    if (gOsRtr.u4RTstaggeredCtxId == pOspfCxt->u4OspfCxtId)
    {
        /* Call the event processing function */
        OSPFRtRelinquishProcessEvents ();

        /* Update the next relinquish interval */
        u4CurrentTime = OsixGetSysUpTime ();
        pOspfCxt->u4StaggeringDelta =
            (u4CurrentTime + (pOspfCxt->u4RTStaggeringInterval / 1000));
        /* for wrap around case of u4CurrentTime */
        if (pOspfCxt->u4StaggeringDelta < u4CurrentTime)
        {
            pOspfCxt->u4StaggeringDelta =
                (pOspfCxt->u4RTStaggeringInterval / 1000);
        }

        /* Reset the Context ID */
        gOsRtr.u4RTstaggeredCtxId = OSPF_INVALID_CXT_ID;
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : OSPFRtcRelinquish \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRtRelinquishProcessEvents                              */
/*                                                                           */
/* Description  : This is the function will process pending OSPF events      */
/*                during CPU relinquish                                      */
/*                                                                           */
/* Input        : OSPF Context                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OSPFRtRelinquishProcessEvents ()
{
    UINT4               u4Event;
    UINT4               u41sTicks = 1 * NO_OF_TICKS_PER_SEC;
    tTmrAppTimer       *pExpiredTimers;
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    INT4                i4SockId = -1;
    INT4                i4Count = 0;
    UINT1               u1Registerflag = 0;
    tOspfVrfSock       *pQueMsg = NULL;
#endif

    /* Receive the pending events. We will go for non-blocking event
     * reveive since we need to process only pending events */
    OsixReceiveEvent (OSPF_TIMER_EVENT | OSPF_MSGQ_IF_EVENT |
                      OSPF_RTM_RTMSG_EVENT | OSPF_PKT_ARRIVAL_EVENT |
                      HELLO_TIMER_EXP_EVENT | TMO1_TIMER_01_EXP_EVENT,
                      OSIX_NO_WAIT, (UINT4) 0, (UINT4 *) &(u4Event));

    OSPF_GBL_TRC1 (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                   "Rx Event %d\n", u4Event);

    if (u4Event & HELLO_TIMER_EXP_EVENT)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Rx Timer Event\n");
        TmrHandleHelloExpiry ();
    }

    /* Process message queue event */
    if (u4Event & OSPF_MSGQ_IF_EVENT)
    {
        OspfProcessQMsg ((const UINT1 *) "OSPQ");
    }
    /* Process the Routes received from RTM */
    if (u4Event & OSPF_RTM_RTMSG_EVENT)
    {
        OspfProcessRtmRts ();
    }

#ifdef RAWSOCK_WANTED
    /* Process the Packets received on Socket */
    if (u4Event & OSPF_PKT_ARRIVAL_EVENT)
    {
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        while (OsixReceiveFromQ (SELF, (const UINT1 *) "OSPFLNXVRF",
                                 OSIX_NO_WAIT, 0,
                                 (tOsixMsg** ) (&pQueMsg)) == OSIX_SUCCESS)
        {
            u1Registerflag = 0;
            i4SockId = pQueMsg->i4SockFd;
            IpifGetPkt (i4SockId);

            MemReleaseMemBlock (VRF_SOCK_FD, (UINT1 *) pQueMsg);
            pQueMsg = NULL;

            if (i4SockId != -1)
            {
                for (i4Count = 0; i4Count < SYS_DEF_MAX_NUM_CONTEXTS; i4Count++)
                {
                    if (gOsRtr.ai4SockId[i4Count] == i4SockId)
                    {
                        u1Registerflag = 1;
                        break;
                    }
                }
                if (u1Registerflag == 1)
                {
                    if (SelAddFd (i4SockId, OspfPacketOnSocket) !=
                        OSIX_SUCCESS)
                    {
                        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                                      "Adding Socket Descriptor to Select utility Failed \r\n ");
                    }
                }
            }
        }
#else
        IpifGetPkt (gOsRtr.ai4SockId[0]);
        /* Add the Socket Descriptor to Select utility 
         * for Packet Reception */
        /* This check must be done because thro. 
         * CLI we might delete the socket and initialized to -1*/
        if (gOsRtr.ai4SockId[0] != -1)
        {
            if (SelAddFd (gOsRtr.ai4SockId[0], OspfPacketOnSocket) !=
                OSIX_SUCCESS)
            {
                OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                              "Adding Socket Descriptor to Select utility Failed \r\n ");
            }
        }
#endif
    }
#endif
    if (u4Event & TMO1_TIMER_01_EXP_EVENT)
    {
        gOspf1sTimer++;

        /* One second timer is expired, hence the timer node can be
         * removed from the timer list and then the timer can be
         * started.
         */

        pExpiredTimers = TmrGetNextExpiredTimer (gOnesecTimerLst);
        if (TmrStartTimer (gOnesecTimerLst, &(gOnesecTimerNode),
                           (UINT4) u41sTicks) != TMR_SUCCESS)
        {
            OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                          "Restarting one second timer failed \r\n ");
        }
    }
    /* Process the timer event */
    if (u4Event & OSPF_TIMER_EVENT)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Rx Timer Event\n");
        TmrHandleExpiry ();
    }

    /* Any event posted to the ISM process queue is executed now */
    IsmProcessSchedQueue ();
    UNUSED_PARAM (pExpiredTimers);
}
