/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osragutl.c,v 1.13 2014/10/01 11:34:53 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *                   route aggregation feature
 *
 *******************************************************************/
#include "osinc.h"

/************************************************************************/
/*                                                                    */
/* Function     : RagCompAddrRng                        */
/*                                                                         */
/* Description     : Compare two Address ranges                  */
/*                                       */
/*                                                                      */
/* Input        : pAsExtRange1 -Pointer to first Range           */
/*          pAsExtRange2 -Pointer to second Range            */
/*                                                                      */
/* Output       : None                                              */
/*                                                                      */
/* Returns         : OSPF_GREATER - Range1 is more specific         */
/*         : OSPF_EQUAL - Both ranges equal            */
/*        : OSPF_LESS - Range1 is less specific            */
/*        : NOT_EQUAL - Ranges are in different             */
/*          subnets                         */
/*                                                                      */
/************************************************************************/
PUBLIC UINT1
RagCompAddrRng (tAsExtAddrRange * pAsExtRange1, tAsExtAddrRange * pAsExtRange2)
{
    tIPADDR             newRngNet;
    tIPADDR             tmpRngNet;

    UtilIpAddrMaskCopy (newRngNet, pAsExtRange1->ipAddr,
                        pAsExtRange1->ipAddrMask);

    UtilIpAddrMaskCopy (tmpRngNet,
                        pAsExtRange2->ipAddr, pAsExtRange2->ipAddrMask);

    if ((UtilIpAddrComp (tmpRngNet, newRngNet)) == OSPF_LESS)
    {
        if ((UtilIpAddrMaskComp (pAsExtRange1->ipAddr,
                                 pAsExtRange2->ipAddr,
                                 pAsExtRange2->ipAddrMask)) == OSPF_EQUAL)
        {
            return OSPF_GREATER;
        }
        else
        {
            return NOT_EQUAL;
        }
    }
    else if ((UtilIpAddrComp (tmpRngNet, newRngNet)) == OSPF_GREATER)
    {
        if ((UtilIpAddrMaskComp (pAsExtRange1->ipAddr,
                                 pAsExtRange2->ipAddr,
                                 pAsExtRange1->ipAddrMask)) == OSPF_EQUAL)
        {
            return OSPF_LESS;
        }
        else
        {
            return NOT_EQUAL;
        }
    }
    else
    {
        if ((UtilIpAddrComp (pAsExtRange1->ipAddrMask,
                             pAsExtRange2->ipAddrMask)) == OSPF_GREATER)
        {
            return OSPF_GREATER;
        }
        else if ((UtilIpAddrComp (pAsExtRange1->ipAddrMask,
                                  pAsExtRange2->ipAddrMask)) == OSPF_LESS)
        {
            return OSPF_LESS;
        }
        else
        {
            return OSPF_EQUAL;
        }
    }

}

/************************************************************************/
/*                                                                  */
/* Function         : RagUpdtBboneRngForNssa                            */
/*                                                                      */
/* Description      : Updates Bkbone Range cost/type for NSSA           */
/*                    for routes maintained in its link lst             */
/*                                                                      */
/* Input            : pAsExtRange - Pointer to new Range                */
/*                    pArea       - Pointer to area                     */
/*                                                                      */
/* Output           : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagUpdtBboneRngForNssa (tArea * pArea, tAsExtAddrRange * pAsExtRange)
{
    tExtRoute          *pExtRoute = NULL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tAsExtAddrRange    *pPrvNssaRange = NULL;
    UINT1               u1Tos = TOS_0;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagUpdtBboneRngForNssa\n");

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pAsExtRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
        pAsExtRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
        pAsExtRange->aMetric[u1Tos].bStatus = OSPF_INVALID;
    }

    TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
    {
        pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);

        pPrvNssaRange = RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);

        if (pPrvNssaRange == NULL)
        {
            RagUpdatCostTypeForRange (pAsExtRange, pExtRoute);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagUpdtBboneRngForNssaInCxt\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function         : RagUpdtBboneRngFrmExtLstInCxt                */
/*                                                                        */
/* Description         : Updates Bkbone Range cost/type          */
/*              for routes maintained in its link lst         */
/*                                                                      */
/* Input               : pAsExtRange -Pointer to new Range           */
/*                                                                      */
/* Output           : None                                          */
/*                                                                      */
/* Returns          : None                                              */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagUpdtBboneRngFrmExtLstInCxt (tOspfCxt * pOspfCxt,
                               tAsExtAddrRange * pAsExtRange)
{
    tArea              *pArea = NULL;
    tExtRoute          *pExtRoute = NULL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    UINT1               u1Tos = TOS_0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagUpdtBboneRngFrmExtLstInCxt\n");

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pAsExtRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
        pAsExtRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
        pAsExtRange->aMetric[u1Tos].bStatus = OSPF_INVALID;
    }

    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " BkBone area not existing \n");
        return;
    }

    TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
    {

        pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);

        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  "FUNC: RagUpdatCostTypeForRange\n");

        RagUpdatCostTypeForRange (pAsExtRange, pExtRoute);

        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  "Exit FUNC: RagUpdatCostTypeForRange\n");
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagUpdtBboneRngFrmExtLstInCxt\n");
    return;
}

/************************************************************************/
/*                                                                  */
/* Function     : RagIsExtRtFallInRange                        */
/*                                                                         */
/* Description     : Checks whether Ext Rt falls in range          */
/*                                      */
/*                                                                      */
/* Input           : pExtRoute - Pointer to Ext Rt               */
/*                pAsExtRange -Pointer to Range                   */
/* Output          : None                                                  */
/*                                                                      */
/* Returns         : OSPF_TRUE  - If yes                             */
/*                OSPF_FALSE  - If no                           */
/************************************************************************/
PUBLIC UINT1
RagIsExtRtFallInRange (tExtRoute * pExtRoute, tAsExtAddrRange * pAsExtRange)
{

    tIPADDR             newRngNet;
    tIPADDR             tmpRngNet;

    UtilIpAddrMaskCopy (newRngNet, pAsExtRange->ipAddr,
                        pAsExtRange->ipAddrMask);

    UtilIpAddrMaskCopy (tmpRngNet, pExtRoute->ipNetNum,
                        pAsExtRange->ipAddrMask);

    if ((UtilIpAddrComp (newRngNet, tmpRngNet) == OSPF_EQUAL) &&
        (UtilIpAddrComp (pAsExtRange->ipAddrMask,
                         pExtRoute->ipAddrMask) != OSPF_GREATER))
    {
        return OSPF_TRUE;
    }
    else
    {
        return OSPF_FALSE;
    }
}

/************************************************************************/
/*                                                                  */
/* Function     : RagFindMatChngBkBoneRange                    */
/*                                                                    */
/* Description     : Finds Bkbone range that subsumes             */
/*          the Ext Rt                         */
/*                                      */
/*                                                                      */
/* Input        : pOspfCxt - Pointer to OSPF context.                   */
/*                pExtRoute -Pointer to Ext Rt                          */
/*                                                                      */
/* Output       : None                           */
/*                                      */
/*                                                                      */
/* Returns      : Bkbone range that subsumes                 */
/*          the Ext Rt or NULL                    */
/*                                                          */
/************************************************************************/
PUBLIC tAsExtAddrRange *
RagFindMatChngBkBoneRangeInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{

    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    tMetric             aTempMetric[OSPF_MAX_METRIC];
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;
    UINT1               u1Tos = TOS_0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagFindMatChngBkBoneRange\n");

    inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pExtRoute->ipAddrMask));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng)
        == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  "Exit FUNC: RagIsExtRtFallInRange\n");

        for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            aTempMetric[u1Tos].u4Value = pScanAsExtRng->aMetric[u1Tos].u4Value;
            aTempMetric[u1Tos].u4MetricType =
                pScanAsExtRng->aMetric[u1Tos].u4MetricType;
            aTempMetric[u1Tos].bStatus = pScanAsExtRng->aMetric[u1Tos].bStatus;
        }

        if (RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) == OSPF_TRUE)
        {
            /* Insert Ext Rt in Range */
            RagInsertRouteInBkboneAggAddrRangeInCxt (pOspfCxt, pScanAsExtRng,
                                                     pExtRoute);

            if (RagIsChangeInMetric (aTempMetric, pScanAsExtRng->aMetric)
                == OSPF_TRUE)
            {
                /* Process the range   */
                RagProcessBkBoneRangeInCxt (pOspfCxt, pScanAsExtRng);
            }
            return pScanAsExtRng;
        }
    }
    /*  No Mtchng Bkbone range found : So originate 
       non-aggregated Type 5 LSA
     */
    RagOriginateNonAggLsaInCxt (pOspfCxt, pExtRoute, NULL);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagFindMatChngBkBoneRange\n");
    return NULL;
}

/************************************************************************/
/*                                                                 */
/* Function        : RagFindAggAddrRangeInBkboneInCxt                   */
/*                                                                      */
/* Description     : Finds the range in backbone area that              */
/*                   subsumes Ext Rt in context                         */
/*                                                                      */
/* Input           : pOspfCxt   - Pointer to OSPF Context               */
/*                   pExtRoute -Pointer to Ext Rt                       */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : Range if found or NULL                             */
/*                                                                      */
/************************************************************************/
PUBLIC tAsExtAddrRange *
RagFindAggAddrRangeInBkboneInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{

    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagFindAggAddrRangeInBkboneInCxt\n");

    inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pExtRoute->ipAddrMask));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&inParams, &outParams,
                     (VOID **) &pAsExtRng)) == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);
        if (RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) == OSPF_TRUE)
        {
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagIsExtRtFallInRange\n");
            return pScanAsExtRng;
        }
    }

    return NULL;
}

/************************************************************************/
/*                                                                 */
/* Function     : RagChkLsaFallInRng                     */
/*                                                                        */
/* Description     : Finds whether LSA is subsumed by              */
/*          AS Ext Range                         */
/*                                      */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pLsaInfo - Pointer to LSA                */
/*                                      */
/* Output          : None                           */
/*                                      */
/*                                                                      */
/* Returns         : OSPF_EQUAL - LSA net equals  range            */
/*              OSPF_GREATER - LSA falls in range             */
/*          OSPF_LESS - LSA does not fall in range        */
/*                                      */
/************************************************************************/
PUBLIC UINT1
RagChkLsaFallInRng (tAsExtAddrRange * pAsExtRange, tLsaInfo * pLsaInfo)
{

    tIPADDR             newRngNet;
    tIPADDR             tmpRngNet;
    tIPADDRMASK         lsaMask;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: RagChkLsaFallInRng\n");

    UtilIpAddrMaskCopy (newRngNet, pAsExtRange->ipAddr,
                        pAsExtRange->ipAddrMask);

    UtilIpAddrMaskCopy (tmpRngNet, pLsaInfo->lsaId.linkStateId,
                        pAsExtRange->ipAddrMask);

    OS_MEM_CPY (lsaMask,
                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

    if ((UtilIpAddrComp (pAsExtRange->ipAddr, pLsaInfo->lsaId.linkStateId)
         == OSPF_EQUAL) &&
        (UtilIpAddrComp (pAsExtRange->ipAddrMask, lsaMask) == OSPF_EQUAL))
    {
        return OSPF_EQUAL;
    }
    else if ((UtilIpAddrComp (newRngNet, tmpRngNet) == OSPF_EQUAL)
             &&
             (UtilIpAddrComp (pAsExtRange->ipAddrMask, lsaMask) == OSPF_LESS))
    {

        return OSPF_GREATER;
    }
    else if ((UtilIpAddrComp (newRngNet, tmpRngNet) == OSPF_EQUAL)
             &&
             (UtilIpAddrComp (pAsExtRange->ipAddrMask, lsaMask) == OSPF_EQUAL))
    {
        return OSPF_EQUAL;
    }
    else
        return OSPF_LESS;
}

/************************************************************************/
/*                                                                  */
/* Function     : RagCompRngAndLsa                     */
/*                                                                    */
/* Description     : Finds whether cost/Path                  */
/*          advertised in Range is different             */
/*          from that in LSA                    */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*          pLsaInfo - Pointer to LSA                */
/*                                      */
/* Output       : None                           */
/*                                                                      */
/* Returns      : OSPF_TRUE -Change present                 */
/*          OSPF_FALSE -No change                 */
/*                                      */
/************************************************************************/
PUBLIC UINT1
RagCompRngAndLsa (tAsExtAddrRange * pAsExtRange, tLsaInfo * pLsaInfo)
{

    UINT1               u1Tos;
    tExtLsaLink         extLsaLink;
    UINT1               u1LsaPbit = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: RagCompRngAndLsa\n");
    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {

        RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                          u1Tos, &extLsaLink);

        if ((pAsExtRange->aMetric[u1Tos].u4MetricType !=
             extLsaLink.u1MetricType) ||
            (pAsExtRange->aMetric[u1Tos].u4Value != extLsaLink.u4Cost))
        {
            return OSPF_TRUE;
        }
    }

    if (IS_P_BIT_SET (pLsaInfo))
    {
        u1LsaPbit = OSPF_TRUE;
    }

    if (pAsExtRange->u1AttrMask & RAG_TRANS_MASK)
    {
        if (u1LsaPbit != pAsExtRange->u1AggTranslation)
        {
            return OSPF_TRUE;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: RagCompRngAndLsa\n");

    return OSPF_FALSE;
}

/***********************************************************************/
/*                                                                     */
/* Function        : RagChkLsaFallInPrvRng                 */
/*                                                                         */
/* Description     : Finds whether LSA is subsumed by              */
/*          previous AS Ext Range                 */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pLsaInfo - Pointer to LSA                */
/*                                      */
/* Output          : None                           */
/*                                                                      */
/* Returns         : OSPF_TRUE                         */
/*          OSPF_FALSE                         */
/*                                                          */
/***********************************************************************/
PUBLIC UINT1
RagChkLsaFallInPrvRng (tAsExtAddrRange * pAsExtRange, tLsaInfo * pLsaInfo)
{
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tArea              *pArea = NULL;
    UINT4               au4ExtIndex[2];
    tInputParams        inParams;
    tOutputParams       outParams;
    VOID               *pAsExtRng = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: RagChkLsaFallInPrvRng\n");

    pArea = GetFindAreaInCxt (pLsaInfo->pOspfCxt, &(pAsExtRange->areaId));

    if (pArea == NULL)
    {
        return OSPF_FALSE;
    }

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pLsaInfo->lsaId.linkStateId));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pLsaInfo->pLsa + LS_HEADER_SIZE));
    inParams.Key.pKey = (UINT1 *) au4ExtIndex;
    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng)
        == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        if (RagCompAddrRng (pAsExtRange, pScanAsExtRng) == OSPF_LESS)
        {
            return OSPF_TRUE;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: RagChkLsaFallInPrvRng\n");
    return OSPF_FALSE;
}

/***********************************************************************/
/*                                                                     */
/* Function        : RagChkLsaFallInBackboneRng                        */
/*                                                                     */
/* Description     : Finds whether LSA is subsumed by backbone range   */
/*                                                                     */
/* Input           : pOspfCxt    - Pointer to OSPF Context             */
/*                   pAsExtRange - Pointer to Range                    */
/*                   pLsaInfo - Pointer to LSA                         */
/*                                                                     */
/* Output          : None                                              */
/*                                                                     */
/* Returns         : OSPF_TRUE if falls in the range                   */
/*                   OSPF_FALSE                                        */
/*                                                                     */
/***********************************************************************/
PUBLIC UINT1
RagChkLsaFallInBackboneRng (tOspfCxt * pOspfCxt,
                            tAsExtAddrRange * pAsExtRange, tLsaInfo * pLsaInfo)
{
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagFindMatChngBkBoneRange\n");

    inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pLsaInfo->lsaId.linkStateId));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pLsaInfo->pLsa + LS_HEADER_SIZE));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&inParams, &outParams,
                     (VOID **) &pAsExtRng)) == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        if (RagCompAddrRng (pAsExtRange, pScanAsExtRng) == OSPF_LESS)
        {
            return OSPF_TRUE;
        }
    }

    return OSPF_FALSE;
}

/***********************************************************************/
/*                                                                     */
/* Function        : RagChkLsaFallInNssaRng                            */
/*                                                                     */
/* Description     : Finds whether LSA is subsumed by any previous     */
/*                   NSSA range. This function should be called when   */
/*                   processing NSSA List for backbone range           */
/*                                                                     */
/* Input           : pOspfCxt    - Pointer to Area                     */
/*                   pAsExtRange - Pointer to Range                    */
/*                   pLsaInfo - Pointer to LSA                         */
/*                                                                     */
/* Output          : None                                              */
/*                                                                     */
/* Returns         : OSPF_TRUE if falls in the range                   */
/*                   OSPF_FALSE                                        */
/*                                                                     */
/***********************************************************************/
PUBLIC UINT1
RagChkLsaFallInNssaRng (tArea * pArea,
                        tAsExtAddrRange * pAsExtRange, tLsaInfo * pLsaInfo)
{
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pLsaInfo->lsaId.linkStateId));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pLsaInfo->pLsa + LS_HEADER_SIZE));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams,
                    (VOID **) &pAsExtRng) == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        if (RagCompAddrRng (pAsExtRange, pScanAsExtRng) == OSPF_LESS)
        {
            return OSPF_TRUE;
        }
    }

    return OSPF_FALSE;
}

/************************************************************************/
/*                                                                 */
/* Function        : RagChkExtRtFallInPrvRng                 */
/*                                                                     */
/* Description     : Finds whether Ext Rt is subsumed by              */
/*          previous AS Ext Range                 */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pExtRoute - Pointer to Ext Rt                */
/*                                      */
/* Output          : None                           */
/*                                      */
/* Returns         : OSPF_TRUE                         */
/*          OSPF_FALSE                         */
/*                                                          */
/************************************************************************/
PUBLIC UINT1
RagChkExtRtFallInPrvRngInCxt (tOspfCxt * pOspfCxt,
                              tAsExtAddrRange * pAsExtRange,
                              tExtRoute * pExtRoute)
{
    tArea              *pArea = NULL;
    tAsExtAddrRange    *pPrevAsExtRange = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagChkExtRtFallInPrvRng\n");
    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));

    if (pArea == NULL)
    {
        return OSPF_FALSE;
    }

    pPrevAsExtRange = RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);

    if (pPrevAsExtRange != NULL)
    {
        if (RagIsExtRtFallInRange (pExtRoute, pPrevAsExtRange) == OSPF_TRUE)
        {
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagIsExtRtFallInRange\n");
            return OSPF_TRUE;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagChkExtRtFallInPrvRng\n");
    return OSPF_FALSE;
}

/************************************************************************/
/*                                                                   */
/* Function        : RagFindAggAddrRangeInNssaForRt             */
/*                                                                   */
/* Description     : Finds the range in NSSA area that              */
/*          subsumes Ext Rt                     */
/*                                      */
/* Input           : pExtRoute -Pointer to Ext Rt               */
/*          pArea - Pointer to Area                */
/*                                      */
/* Output          : None                           */
/*                                                                      */
/* Returns         : Range if found or NULL                 */
/*                                                          */
/************************************************************************/
PUBLIC tAsExtAddrRange *
RagFindAggAddrRangeInNssaForRt (tExtRoute * pExtRoute, tArea * pArea)
{

    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagFindAggAddrRangeInNssaForRt\n");

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
    au4ExtIndex[1] =
        OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&inParams, &outParams,
                     (VOID **) &pAsExtRng)) == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);
        if (RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) == OSPF_TRUE)
        {
            OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagIsExtRtFallInRange\n");
            return pScanAsExtRng;
        }
    }

    return NULL;
}

/************************************************************************/
/*                                                                  */
/* Function     : RagChkExtRtFallInBbRngInCxt                           */
/*                                                                      */
/* Description  : Finds Bkbone range that subsumes                      */
/*                the Ext Rt                                            */
/*                                                                      */
/*                                                                      */
/* Input        : pOspfCxt    - Pointer to OSPF Context                 */
/*                pExtRoute   - Pointer to Ext Rt                       */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/*                                                                      */
/* Returns      : OSPF_TRUE or OSPF_FALSE                               */
/************************************************************************/
PUBLIC UINT1
RagChkExtRtFallInBbRngInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{

    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;

    inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pExtRoute->ipAddrMask));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&inParams, &outParams,
                     (VOID **) &pAsExtRng)) == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        if (RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) == OSPF_TRUE)
        {
            return OSPF_TRUE;
        }
    }

    return OSPF_FALSE;
}

/************************************************************************/
/*                                                                  */
/* Function     : RagUpdatCostTypeForRange                 */
/*                                                                         */
/* Description     : Updates Cost/Path Type for the range            */
/*          if affected by Ext Rt                 */
/*                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*          pExtRoute - Pointer to Ext Rt                */
/*                                      */
/* Output       : None                           */
/*                                      */
/* Returns      : OSPF_TRUE - If any change                 */
/*          OSPF_FALSE - No update                  */
/*                                                          */
/************************************************************************/
PUBLIC UINT1
RagUpdatCostTypeForRange (tAsExtAddrRange * pAsExtRange, tExtRoute * pExtRoute)
{

    UINT1               u1Tos;
    UINT1               u1Change = OSPF_FALSE;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (pExtRoute->aMetric[u1Tos].rowStatus != ACTIVE)
        {
            continue;
        }

        if (pExtRoute->extrtParam[u1Tos].u1MetricType ==
            pAsExtRange->aMetric[u1Tos].u4MetricType)
        {
            if ((pAsExtRange->aMetric[u1Tos].u4Value == LS_INFINITY_24BIT) ||
                (pExtRoute->aMetric[u1Tos].u4Value >
                 pAsExtRange->aMetric[u1Tos].u4Value))
            {
                pAsExtRange->aMetric[u1Tos].u4Value =
                    pExtRoute->aMetric[u1Tos].u4Value;
                pAsExtRange->aMetric[u1Tos].bStatus = OSPF_VALID;
                u1Change = OSPF_TRUE;
            }
        }
        else if (pExtRoute->extrtParam[u1Tos].u1MetricType == TYPE_2_METRIC)
        {
            pAsExtRange->aMetric[u1Tos].u4MetricType =
                pExtRoute->extrtParam[u1Tos].u1MetricType;
            pAsExtRange->aMetric[u1Tos].u4Value =
                pExtRoute->aMetric[u1Tos].u4Value;
            pAsExtRange->aMetric[u1Tos].bStatus = OSPF_VALID;
            u1Change = OSPF_TRUE;
        }
    }

    if (u1Change == OSPF_TRUE)
    {
        return OSPF_TRUE;
    }

    return OSPF_FALSE;
}

/************************************************************************/
/*                                                               */
/* Function        : RagUpdtRtLstBtwRanges                 */
/*                                                                         */
/* Description     : Updates the ranges for cost/ Path Type         */
/*          to be advertised                    */
/*                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pSpecLessRange - Pointer to less spec            */
/*          range                         */
/*          u1Flag - Flag denoting addition/deletion         */
/*                                      */
/* Output          : None                           */
/*                                                             */
/* Returns         : VOID                             */
/*                                       */
/************************************************************************/
PUBLIC VOID
RagUpdtRtLstBtwRangesInCxt (tOspfCxt * pOspfCxt,
                            tAsExtAddrRange * pAsExtRange,
                            tAsExtAddrRange * pSpecLessRange, UINT1 u1Flag)
{

    tExtRoute          *pExtRoute = NULL;
    UINT1               u1Tos;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;

    /* Denoted pAsExtRange(bkbone) is added */
    if (u1Flag == OSPF_TRUE)
    {

        /* Scan through Ext Rt falling in less spec range */
        TMO_SLL_Scan_Ospf (&(pSpecLessRange->extRtLst),
                           pExtRtNode, pTempNode, tTMO_SLL_NODE *)
        {
            pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagIsExtRtFallInRange\n");

            if (RagIsExtRtFallInRange (pExtRoute, pAsExtRange) == OSPF_TRUE)
            {

                /* If Ext Rt falls in more specific newly added range:
                   Delete it from less specific range and
                   Add it to new range
                 */
                TMO_SLL_Delete (&(pSpecLessRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                TMO_SLL_Add (&(pAsExtRange->extRtLst),
                             &(pExtRoute->nextExtRouteInRange));
                pExtRoute->pAsExtAddrRange = pAsExtRange;
            }
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagIsExtRtFallInRange\n");
        }

        /* Now update both the ranges */
        RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pAsExtRange);

        RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pSpecLessRange);
        return;
    }
    else if (u1Flag == OSPF_FALSE)
    {
        /* Denotes pAsExtRange is deleted */
        if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
        {
            TMO_SLL_Scan_Ospf (&(pAsExtRange->extRtLst),
                               pExtRtNode, pTempNode, tTMO_SLL_NODE *)
            {
                pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
                TMO_SLL_Delete (&(pAsExtRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                TMO_SLL_Add (&(pSpecLessRange->extRtLst),
                             &(pExtRoute->nextExtRouteInRange));
                pExtRoute->pAsExtAddrRange = pSpecLessRange;
            }
            RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pSpecLessRange);
            return;
        }
        else
        {
            for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {

                if (pAsExtRange->aMetric[u1Tos].u4Value == LS_INFINITY_24BIT)
                {
                    continue;
                }

                if (pAsExtRange->aMetric[u1Tos].u4MetricType ==
                    pSpecLessRange->aMetric[u1Tos].u4MetricType)
                {
                    if (pAsExtRange->aMetric[u1Tos].u4Value >
                        pSpecLessRange->aMetric[u1Tos].u4Value)
                    {
                        pSpecLessRange->aMetric[u1Tos].u4Value =
                            pAsExtRange->aMetric[u1Tos].u4Value;
                        pSpecLessRange->aMetric[u1Tos].bStatus = OSPF_VALID;

                    }
                }
                else if (pAsExtRange->aMetric[u1Tos].u4MetricType ==
                         TYPE_2_METRIC)
                {
                    pSpecLessRange->aMetric[u1Tos].u4MetricType =
                        pAsExtRange->aMetric[u1Tos].u4MetricType;
                    pSpecLessRange->aMetric[u1Tos].u4Value =
                        pAsExtRange->aMetric[u1Tos].u4Value;
                    pSpecLessRange->aMetric[u1Tos].bStatus = OSPF_VALID;

                }
            }
        }
    }
}

/************************************************************************/
/*                                                           */
/* Function        : RagUpdtRtLstFromExtRtTable                 */
/*                                                                     */
/* Description     : Updates the range for cost/ Path Type         */
/*          by scanning Ext Rt Table                */
/*                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*                                       */
/* Output          : None                           */
/*                                                                     */
/* Returns         : VOID                             */
/*                                                          */
/************************************************************************/
PUBLIC VOID
RagUpdtRtLstFromExtRtTableInCxt (tOspfCxt * pOspfCxt,
                                 tAsExtAddrRange * pAsExtRange)
{

    tExtRoute          *pExtRoute = NULL;
    tAsExtAddrRange    *pRtRange = NULL;
    tAsExtAddrRange    *pPrvAsExtRng = NULL;

    tIPADDR             newRngNet;
    UINT1               u1Tos;
    tArea              *pArea = NULL;
    UINT4               au4Key[2];
    UINT4               au4ExtIndex[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    INT4                i4RetVal;
    VOID               *pAppSpecPtr = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagUpdtRtLstFromExtRtTable\n");

    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId, " Area not found \n");
        return;
    }

    if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) != OSPF_EQUAL)
    {
        for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            pAsExtRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
            pAsExtRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
            pAsExtRange->aMetric[u1Tos].bStatus = OSPF_INVALID;
        }
    }

    UtilIpAddrMaskCopy (newRngNet, pAsExtRange->ipAddr,
                        pAsExtRange->ipAddrMask);

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            /* Bkbone range */
            if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
            {
                OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                          "FUNC: RagIsExtRtFallInRange\n");

                if (RagIsExtRtFallInRange (pExtRoute, pAsExtRange) == OSPF_TRUE)
                {
                    if (pExtRoute->pAsExtAddrRange == NULL)
                    {
                        TMO_SLL_Add (&(pAsExtRange->extRtLst),
                                     &(pExtRoute->nextExtRouteInRange));
                        pExtRoute->pAsExtAddrRange = pAsExtRange;
                    }
                    else
                    {
                        pRtRange = pExtRoute->pAsExtAddrRange;
                        /* If this range is more specific than continue */
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "FUNC: RagCompAddrRng\n");

                        if (RagCompAddrRng (pRtRange, pAsExtRange) ==
                            OSPF_GREATER)
                        {
                            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                      "Exit FUNC: RagCompAddrRng\n");
                            /* Ext Rt is attached to more specific range */
                            inParams.pRoot = pOspfCxt->pExtRtRoot;
                            inParams.i1AppId = OSPF_EXT_RT_ID;
                            inParams.pLeafNode = NULL;
                            inParams.u1PrefixLen = 0;
                            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                                    (pExtRoute->ipNetNum));
                            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                                    (pExtRoute->ipAddrMask));
                            inParams.Key.pKey = (UINT1 *) au4Key;
                            pLeafNode = inParams.pLeafNode;
                            pExtRoute = NULL;
                            continue;
                        }
                        else
                        {
                            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                      "Exit FUNC: RagCompAddrRng\n");
                            /* Remove the Ext Rt from and add to new range */
                            TMO_SLL_Delete (&(pRtRange->extRtLst),
                                            &(pExtRoute->nextExtRouteInRange));
                            TMO_SLL_Add (&(pAsExtRange->extRtLst),
                                         &(pExtRoute->nextExtRouteInRange));
                            pExtRoute->pAsExtAddrRange = pAsExtRange;
                        }
                    }
                }
            }
            /* NSSA range */
            else
            {
                /* Check if the previous range is more specific */
                inParams.pRoot = pArea->pAsExtAddrRangeTrie;
                inParams.pLeafNode = NULL;
                inParams.u1PrefixLen = 0;
                au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                             (pAsExtRange->ipAddr));
                au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                             (pAsExtRange->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4ExtIndex;
                i4RetVal = TrieGetNextNode (&inParams, NULL,
                                            &pAppSpecPtr,
                                            (VOID **) &(inParams.pLeafNode));
                pPrvAsExtRng = (tAsExtAddrRange *) pAppSpecPtr;

                OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                          "FUNC: RagIsExtRtFallInRange\n");

                if ((i4RetVal == TRIE_SUCCESS) &&
                    (RagIsExtRtFallInRange (pExtRoute,
                                            pPrvAsExtRng)) == OSPF_TRUE)
                {
                    OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                              "Exit FUNC: RagIsExtRtFallInRange\n");
                    inParams.pRoot = pOspfCxt->pExtRtRoot;
                    inParams.i1AppId = OSPF_EXT_RT_ID;
                    inParams.pLeafNode = NULL;
                    au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipNetNum));
                    au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                            (pExtRoute->ipAddrMask));
                    inParams.Key.pKey = (UINT1 *) au4Key;
                    pLeafNode = inParams.pLeafNode;
                    pExtRoute = NULL;
                    continue;
                }
                else
                {
                    OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                              "Exit FUNC: RagIsExtRtFallInRange\n");
                    if (RagIsExtRtFallInRange (pExtRoute,
                                               pAsExtRange) == OSPF_TRUE)
                    {
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "FUNC: RagUpdatCostTypeForRange\n");

                        RagUpdatCostTypeForRange (pAsExtRange, pExtRoute);

                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "Exit FUNC: RagUpdatCostTypeForRange\n");
                    }
                }
            }
            inParams.pRoot = pOspfCxt->pExtRtRoot;
            inParams.i1AppId = OSPF_EXT_RT_ID;
            inParams.pLeafNode = NULL;
            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipNetNum));
            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipAddrMask));
            inParams.Key.pKey = (UINT1 *) au4Key;
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }

    if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pAsExtRange);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagUpdtRtLstFromExtRtTable\n");
    return;
}

/************************************************************************/
/*                                                                */
/* Function        : RagScanRtTableForAddrRanges                 */
/*                                                           */
/* Description     : Updates the ranges for cost/ Path Type         */
/*          by scanning Ext Rt Table for NSSA ranges        */
/*                                      */
/* Input           : pMoreSpecRange - Pointer to more specific range    */
/*                 : pLessSpecRange - Pointer to less specific range    */
/*                   pArea          - Pointer to Area                   */
/*                   u4AreaType     - Area Type should be NSSA_AREA or  */
/*                                    NORMAL_AREA                       */
/*                                    If NSSA_AREA pointer pArea        */
/*                                    should be filled correctly        */
/*                                      */
/* Output          : None                           */
/*                                                              */
/* Returns         : None                                               */
/*                                                       */
/************************************************************************/
PUBLIC VOID
RagScanRtTableForAddrRangesInCxt (tOspfCxt * pOspfCxt,
                                  tAsExtAddrRange * pMoreSpecRange,
                                  tAsExtAddrRange * pLessSpecRange,
                                  tArea * pArea, UINT4 u4AreaType)
{

    tExtRoute          *pExtRoute = NULL;
    UINT1               u1Tos;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    tAsExtAddrRange    *pTmpAsExtRange = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagScanRtTableForAddrRanges\n");

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pMoreSpecRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
        pMoreSpecRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;

        pLessSpecRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
        pLessSpecRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;

    }

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pExtRoute = (tExtRoute *) pAppSpecPtr;

        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  "FUNC: RagIsExtRtFallInRange\n");

        if (RagIsExtRtFallInRange (pExtRoute, pMoreSpecRange) == OSPF_TRUE)
        {
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagUpdatCostTypeForRange\n");

            RagUpdatCostTypeForRange (pMoreSpecRange, pExtRoute);

            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagUpdatCostTypeForRange\n");
        }

        else if (RagIsExtRtFallInRange (pExtRoute, pLessSpecRange) == OSPF_TRUE)
        {
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagUpdatCostTypeForRange\n");

            /* If the external route falling in less specific range
             * does not fall in any other better address range cost will
             * be updated for this less specific range. */

            if (u4AreaType == NSSA_AREA)
            {
                pTmpAsExtRange =
                    RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);
            }
            else
            {
                pTmpAsExtRange =
                    RagFindAggAddrRangeInBkboneInCxt (pOspfCxt, pExtRoute);

                /* If External route does not fall in backbone range,
                 * check whether it falls in NSSA range. */
                if (pTmpAsExtRange == NULL)
                {
                    pTmpAsExtRange =
                        RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);
                }
            }

            if ((pTmpAsExtRange == NULL) ||
                (RagCompAddrRng (pLessSpecRange, pTmpAsExtRange) == OSPF_EQUAL))
            {
                RagUpdatCostTypeForRange (pLessSpecRange, pExtRoute);
            }

            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagUpdatCostTypeForRange\n");

        }
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  "Exit FUNC: RagIsExtRtFallInRange\n");
        au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
        au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4Key;
        pLeafNode = inParams.pLeafNode;
        pExtRoute = NULL;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pAppSpecPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagScanRtTableForAddrRanges\n");
    return;
}

/************************************************************************/
/*                                                               */
/* Function        : RagChkValidMetricInRng                 */
/*                                                                         */
/* Description     : Checks if there is any valid TOS             */
/*          metric present                    */
/*                                      */
/* Input           : pAsExtRange -Pointer to range              */
/*                                       */
/* Output          : None                           */
/*                                                                  */
/* Returns         : OSPF_TRUE - Any valid TOS  metric            */
/*          OSPF_FALSE - No valid TOS metric              */
/*                                                      */
/************************************************************************/
PUBLIC UINT1
RagChkValidMetricInRng (tAsExtAddrRange * pAsExtRange)
{
    UINT1               u1Tos;
    UINT1               u1RetFlg = OSPF_FALSE;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (pAsExtRange->aMetric[u1Tos].u4Value != LS_INFINITY_24BIT)
        {
            u1RetFlg = OSPF_TRUE;
        }
    }

    return u1RetFlg;
}

/************************************************************************/
/*                                                                   */
/* Function        : RagGenExtLsa                         */
/*                                                                    */
/* Description     : Checks whether Ext Lsa can be             */
/*           generated                           */
/*                                      */
/* Input           : u1Type - Internal LSA Type                   */
/*          pPtr - Struct associated with LSA             */
/* Output          : None                           */
/*                                      */
/* Returns         : OSPF_TRUE - Generate Ext Lsa                 */
/*          OSPF_FALSE - Do Not generate                  */
/*                                      */
/************************************************************************/
PUBLIC UINT1
RagGenExtLsaInCxt (tOspfCxt * pOspfCxt, UINT1 u1Type, UINT1 *pPtr)
{
    tExtRoute          *pExtRoute = NULL;
    tAsExtAddrRange    *pAsExtRange = NULL;
    tIPADDR             extNet;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: RagGenExtLsa\n");
    if ((pOspfCxt->bAsBdrRtr == OSPF_FALSE) &&
        ((pOspfCxt->bAreaBdrRtr == OSPF_FALSE) ||
         (pOspfCxt->u4NssaAreaCount == RAG_NO_CHNG)))
    {
        OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                  "Router is not an ASBR\n");
        return OSPF_FALSE;
    }

    if (u1Type == AS_EXT_LSA)
    {
        pExtRoute = (tExtRoute *) (VOID *) pPtr;
        IP_ADDR_COPY (extNet, pExtRoute->ipNetNum);
        if (OlsIsEligibleToGenAseLsaInCxt (pOspfCxt, pExtRoute) == OSPF_FAILURE)
        {

            OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                      "cant generate ASE LSA, since"
                      "another one exists with same functionality\n");
            return OSPF_FALSE;
        }
    }
    else
    {
        pAsExtRange = (tAsExtAddrRange *) (VOID *) pPtr;
        IP_ADDR_COPY (extNet, pAsExtRange->ipAddr);
    }
    if (UtilIpAddrComp (extNet, gNullIpAddr) != OSPF_EQUAL)
    {
        if (pOspfCxt->bOverflowState == OSPF_TRUE)
        {

            OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                      "no non-default ASE LSA is"
                      "originated in overflow state\n");
            return OSPF_FALSE;
        }
        if ((pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
            (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT (pOspfCxt)
             == (pOspfCxt->i4ExtLsdbLimit - OSPF_TRUE)))
        {

            OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                      "Router enters the overflow state"
                      "when trying to orgnte a non-def lsa\n");
            RtrEnterOverflowStateInCxt (pOspfCxt);
            return OSPF_FALSE;
        }
    }
    else
    {
        /* Set the Flag indicating that Default AS_EXT lsa is Present */
        pOspfCxt->bDefaultAseLsaPresenc = OSPF_TRUE;
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "FUNC: RagGenExtLsa\n");
    return OSPF_TRUE;
}

/************************************************************************/
/*                                                                      */
/* Function        : RagIsChangeInMetric                                */
/*                                                                      */
/* Description     : Checks if there is any change in the metric        */
/*                                                                      */
/* Input           : pMetric1 - Pointer to Metric 1                     */
/*                   pMetric2 - Pointer to Metric 2                     */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : OSPF_TRUE - If there is change                     */
/*                   OSPF_FALSE - If there is no change                 */
/************************************************************************/
PUBLIC UINT1
RagIsChangeInMetric (tMetric * pMetric1, tMetric * pMetric2)
{
    UINT1               u1Tos;
    UINT1               u1RetFlg = OSPF_FALSE;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (pMetric1[u1Tos].u4Value != pMetric2[u1Tos].u4Value)
        {
            u1RetFlg = OSPF_TRUE;
        }

        if (pMetric1[u1Tos].u4MetricType != pMetric2[u1Tos].u4MetricType)
        {
            u1RetFlg = OSPF_TRUE;
        }

        if (pMetric1[u1Tos].bStatus != pMetric2[u1Tos].bStatus)
        {
            u1RetFlg = OSPF_TRUE;
        }
    }

    return u1RetFlg;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osragutil.c                        */
/*-----------------------------------------------------------------------*/
