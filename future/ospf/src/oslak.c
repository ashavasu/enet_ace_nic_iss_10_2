/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oslak.c,v 1.16 2013/09/07 10:40:37 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             sending and receiving of acknowledgement packets.
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : LakRcvLsAck                                             */
/*                                                                           */
/* Description  : This procedure takes care of processing  a received        */
/*                ls_ack_pkt. Its validity is checked and its contents are   */
/*                processed.                                                 */
/*                                                                           */
/* Input        : pLsAckPkt      : the ack pkt received                   */
/*                u2PktLen        : the length of the pkt                  */
/*                pNbr             : the nbr from whom ack was received     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LakRcvLsAck (tCRU_BUF_CHAIN_HEADER * pLsAckPkt,
             UINT2 u2PktLen, tNeighbor * pNbr)
{

    UINT2               u2AckCount;
    tLsHeader           ackLsHeader;
    tLsaInfo           *pLsaInfo;
    INT4                i4AckCount = 0;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LakRcvLsAck\n");
    if (pNbr->u1NsmState < MAX_NBR_STATE)
    {
        OSPF_NBR_TRC3 (OSPF_LAK_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "Rcvd LAK Nbr %x.%d Nbr State %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf, au1DbgNbrState[pNbr->u1NsmState]);
    }

    if (pNbr->u1NsmState >= NBRS_EXCHANGE)
    {

        u2AckCount = (UINT2) ((u2PktLen - OS_HEADER_SIZE) / LS_HEADER_SIZE);

        while (u2AckCount--)
        {

            /* extract header from packet */
            UtilExtractLsHeaderFromPkt (pLsAckPkt, &ackLsHeader, i4AckCount, 1);

            OSPF_NBR_TRC3 (OSPF_LAK_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "ACK for LSType %d LSId %x AdvRtrId %x\n",
                           ackLsHeader.u1LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (ackLsHeader.linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (ackLsHeader.advRtrId));

            /* find instance of advt in database */

            pLsaInfo = LsuSearchDatabase (ackLsHeader.u1LsaType,
                                          &(ackLsHeader.linkStateId),
                                          &(ackLsHeader.advRtrId),
                                          (UINT1 *) pNbr->pInterface,
                                          (UINT1 *) pNbr->pInterface->pArea);

            if (pLsaInfo != NULL)
            {
                if (LsuCompLsaInstance (NULL, pLsaInfo, &ackLsHeader) ==
                    OSPF_EQUAL)
                {

                    LsuCheckAndDelLsaFromRxmtLst (pNbr, pLsaInfo);
                    i4AckCount++;
                    /* Set the u1GrLsaAckRcvd flag to indicate that
                     * an acknowledgement is received for the grace
                     * LSA */
                    if ((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
                        (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
                    {
                        pNbr->pInterface->u1GrLsaAckRcvd = OSPF_TRUE;
                    }

                    continue;
                }
            }

            OSPF_NBR_TRC (OSPF_LAK_TRC,
                          pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "ACK Rcvd For Inst Not Present In Rxmt Lst\n");

            /* while */
            i4AckCount++;
        }
        if (pNbr->pInterface->u1GrLsaAckRcvd == OSPF_TRUE)
        {
            /* Grace LSA has been acknowledged by the neighbor */
            if ((pNbr->pInterface->pArea->pOspfCxt->u1RestartStatus
                 == OSPF_RESTART_PLANNED) &&
                (pNbr->pInterface->pArea->pOspfCxt->u1PrevOspfRestartState
                 != OSPF_GR_SHUTDOWN))
            {
                GrCheckRxmtLst (pNbr->pInterface->pArea->pOspfCxt);
            }
        }
    }
    else
    {

        INC_DISCARD_LAK_CNT (pNbr->pInterface);

        OSPF_NBR_TRC3 (OSPF_LAK_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "LAK Disc Due To Invalid Nbr State Nbr %x.%d Nbr State %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf, au1DbgNbrState[pNbr->u1NsmState]);
    }
    /* UtilOsMsgFree( pLsAckPkt, NORMAL_RELEASE); */

    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: LakRcvLsAck\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LakSendDirect                                            */
/*                                                                           */
/* Description  : This procedure constructs a ls_ack packet containing the   */
/*                specified lsHeader and transmits this packet to the       */
/*                specified neighbor.                                        */
/*                                                                           */
/* Input        : pNbr             : the nbr to whom ack is to be sent      */
/*                pLsa             : the lsa which is to be sent in ack pkt */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LakSendDirect (tNeighbor * pNbr, UINT1 *pLsa)
{

    tCRU_BUF_CHAIN_HEADER *pLsAckPkt;
    UINT2               u2DigestLen;
    UINT1               u1NoDNAMask = 0x7f;

    u2DigestLen = DIGEST_LEN_IF_CRYPT (pNbr->pInterface);

    if ((pLsAckPkt =
         UtilOsMsgAlloc (OS_HEADER_SIZE + LS_HEADER_SIZE
                         + MD5_AUTH_DATA_LEN)) == NULL)
    {

        return;
    }
    OSPF_CRU_BMC_ASSIGN_STRING (pLsAckPkt, pLsa, OS_HEADER_SIZE,
                                LS_HEADER_SIZE);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pLsAckPkt, OS_HEADER_SIZE,
                                (*(UINT1 *) pLsa & u1NoDNAMask));
    UtilConstructHdr (pNbr->pInterface, pLsAckPkt, LSA_ACK_PKT,
                      OS_HEADER_SIZE + LS_HEADER_SIZE);
    PppSendPkt (pLsAckPkt,
                (UINT2) (OS_HEADER_SIZE + LS_HEADER_SIZE + u2DigestLen),
                pNbr->pInterface, &(pNbr->nbrIpAddr), RELEASE);

    OSPF_NBR_TRC2 (OSPF_LAK_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Direct ACK Sent To Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);
}

/*****************************************************************************/
/* Function     : LakSendDelayedAck                                          */
/*                                                                           */
/* Description  : This procedure is invoked to send a link state ack packet  */
/*                that was  built during processing the delayed acks for a   */
/*                received LSU packet. The ls_ack packet is just transmitted */
/*                as a multicast on specified interface.                     */
/*                                                                           */
/* Input        : pArg            : interface on which delayed ack is to     */
/*                                     be sent                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
LakSendDelayedAck (VOID *pArg)
{
    tInterface         *pInterface = pArg;

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    UINT2               u2DigestLen;

    OSPF_TRC2 (OSPF_LAK_TRC | OSPF_ADJACENCY_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Delayed ack to be transmitted on interface : %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);

    u2DigestLen = DIGEST_LEN_IF_CRYPT (pInterface);
    if ((pInterface->delLsAck.pAckPkt == NULL) ||
        (pInterface->delLsAck.u2CurrentLen == 0))
    {
        return;
    }
    UtilConstructHdr (pInterface, pInterface->delLsAck.pAckPkt,
                      LSA_ACK_PKT, pInterface->delLsAck.u2CurrentLen);
    if (pInterface->u1NetworkType != IF_BROADCAST)
    {

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {

            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            if (pNbr->u1NsmState >= NBRS_EXCHANGE)
            {

                PppSendPkt (pInterface->delLsAck.pAckPkt,
                            (UINT2) (pInterface->delLsAck.u2CurrentLen
                                     + u2DigestLen), pInterface,
                            &(pNbr->nbrIpAddr), NO_RELEASE);
            }
        }
        UtilOsMsgFree (pInterface->delLsAck.pAckPkt, NORMAL_RELEASE);
    }
    else if ((pInterface->u1IsmState == IFS_DR) ||
             (pInterface->u1IsmState == IFS_BACKUP))
    {

        PppSendPkt (pInterface->delLsAck.pAckPkt,
                    (UINT2) (pInterface->delLsAck.u2CurrentLen
                             + u2DigestLen), pInterface, &gAllSpfRtrs, RELEASE);
    }
    else
    {

        PppSendPkt (pInterface->delLsAck.pAckPkt,
                    (UINT2) (pInterface->delLsAck.u2CurrentLen
                             + u2DigestLen), pInterface, &gAllDRtrs, RELEASE);
    }
    pInterface->delLsAck.pAckPkt = NULL;
    LakClearDelayedAck (pInterface);
    OSPF_TRC (OSPF_LAK_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId, "Delayed ACK Sent\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LakAddToDelayedAck                                     */
/*                                                                           */
/* Description  : This procedure adds the specified link state advt to the   */
/*                delayed ack being constructed.                             */
/*                                                                           */
/* Input        : pInterface       : interface for which del_ack is         */
/*                                    constructed                            */
/*                pLsa             : the lsa that is to be added to the ack */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LakAddToDelayedAck (tInterface * pInterface, UINT1 *pLsa)
{
    UINT1               u1NoDNAMask = 0x7f;

    if (pInterface->delLsAck.u2CurrentLen == 0)
    {

        if ((pInterface->delLsAck.pAckPkt =
             UtilOsMsgAlloc (IFACE_MTU (pInterface))) == NULL)
        {

            return;
        }
        pInterface->delLsAck.u2CurrentLen = OS_HEADER_SIZE;

        /* set delayed ack_timer */

        TmrSetTimer (&(pInterface->delLsAck.delAckTimer), DEL_ACK_TIMER,
                     NO_OF_TICKS_PER_SEC * (pInterface->u2RxmtInterval / 2));
    }
    OSPF_CRU_BMC_ASSIGN_STRING (pInterface->delLsAck.pAckPkt, pLsa,
                                pInterface->delLsAck.u2CurrentLen,
                                LS_HEADER_SIZE);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pInterface->delLsAck.pAckPkt,
                                pInterface->delLsAck.u2CurrentLen,
                                (*(UINT1 *) pLsa & u1NoDNAMask));
    pInterface->delLsAck.u2CurrentLen += LS_HEADER_SIZE;

    if ((IFACE_MTU (pInterface) - pInterface->delLsAck.u2CurrentLen) <
        LS_HEADER_SIZE)
    {
        LakSendDelayedAck (pInterface);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LakClearDelayedAck                                      */
/*                                                                           */
/* Description  : This routine clears the delayed ack.                       */
/*                                                                           */
/* Input        : pInterface        : interface whose delayed ack is being  */
/*                                     cleared.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LakClearDelayedAck (tInterface * pInterface)
{

    if (pInterface->delLsAck.pAckPkt != NULL)
    {
        UtilOsMsgFree (pInterface->delLsAck.pAckPkt, FORCED_RELEASE);
    }
    pInterface->delLsAck.u2CurrentLen = 0;
    pInterface->delLsAck.pAckPkt = NULL;
    TmrDeleteTimer (&pInterface->delLsAck.delAckTimer);

}

/*----------------------------------------------------------------------*/
/*                      End of the file  oslak.c                        */
/*----------------------------------------------------------------------*/
