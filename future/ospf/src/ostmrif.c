/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ostmrif.c,v 1.33 2017/12/21 10:22:06 siva Exp $
 *
 * Description:This file contains procedures related to starting
 *             and resetting of timers and also actions routines
 *             for handling timer expiry.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID TmrWaitTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrInactivityTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrDdInitRxmtTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrDdRxmtTimer PROTO ((VOID *pAgr));
PRIVATE VOID TmrLsaNormalAgingTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrLsaPrematureAgingTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrMinLsaIntervalTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrLsaRxmtTimer PROTO ((VOID *pAgr));
PRIVATE VOID TmrRunRtTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrRestartGraceTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrHelperGraceTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrDistanceTimer PROTO ((VOID *pArg));

#ifdef DEBUG_WANTED
PRIVATE VOID TmrDumpTimer PROTO ((VOID *pArg));
#endif /* DEBUG_WANTED */

PRIVATE VOID TmrExitOverflowTimer PROTO ((VOID *pArg));

PRIVATE VOID TmrDnaLsaSplAgingTimer PROTO ((VOID *pArg));

PRIVATE VOID TmrTrapLimitTimer PROTO ((VOID *pArg));
PRIVATE VOID TmrNssaFsmStbltyTimer PROTO ((VOID *pArg));

/* Prototype added for VRF SPF Timer */
PRIVATE VOID TmrVrfSpfTimer PROTO ((VOID *pArg));
/* 
 * The following data structure contains function pointers for timer handling
 * routines and the offsets used to identify the data structure containing the 
 * timer block. The timer id is used to index into the following array.
 */

tTimerDesc          aTimerDesc[MAX_TIMERS];

/*****************************************************************************/
/* Function     : TmrInitTimerDesc                                           */
/*                                                                           */
/* Description  : Initializes the timer descriptor structure.                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/

PUBLIC VOID
TmrInitTimerDesc (void)
{

    aTimerDesc[HELLO_TIMER].pTimerExpFunc = TmrHelloTimer;
    aTimerDesc[HELLO_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tInterface, helloTimer));

    aTimerDesc[POLL_TIMER].pTimerExpFunc = TmrPollTimer;
    aTimerDesc[POLL_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tInterface, pollTimer));

    aTimerDesc[WAIT_TIMER].pTimerExpFunc = TmrWaitTimer;
    aTimerDesc[WAIT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tInterface, waitTimer));

    aTimerDesc[INACTIVITY_TIMER].pTimerExpFunc = TmrInactivityTimer;
    aTimerDesc[INACTIVITY_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, inactivityTimer));

    aTimerDesc[DD_INIT_RXMT_TIMER].pTimerExpFunc = TmrDdInitRxmtTimer;
    aTimerDesc[DD_INIT_RXMT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, dbSummary.ddTimer));

    aTimerDesc[DD_RXMT_TIMER].pTimerExpFunc = TmrDdRxmtTimer;
    aTimerDesc[DD_RXMT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, dbSummary.ddTimer));

    aTimerDesc[DD_LAST_PKT_LIFE_TIME_TIMER].pTimerExpFunc = DdpClearSummary;
    aTimerDesc[DD_LAST_PKT_LIFE_TIME_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, dbSummary.ddTimer));

    aTimerDesc[LSA_REQ_RXMT_TIMER].pTimerExpFunc = LrqRxmtLsaReq;
    aTimerDesc[LSA_REQ_RXMT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, lsaReqDesc.lsaReqRxmtTimer));

    aTimerDesc[LSA_NORMAL_AGING_TIMER].pTimerExpFunc = TmrLsaNormalAgingTimer;
    aTimerDesc[LSA_NORMAL_AGING_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tLsaInfo, lsaAgingTimer));

    aTimerDesc[LSA_PREMATURE_AGING_TIMER].pTimerExpFunc =
        TmrLsaPrematureAgingTimer;
    aTimerDesc[LSA_PREMATURE_AGING_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tLsaInfo, lsaAgingTimer));

    aTimerDesc[MIN_LSA_INTERVAL_TIMER].pTimerExpFunc = TmrMinLsaIntervalTimer;
    aTimerDesc[MIN_LSA_INTERVAL_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tLsaDesc, minLsaIntervalTimer));

    aTimerDesc[LSA_RXMT_TIMER].pTimerExpFunc = TmrLsaRxmtTimer;
    aTimerDesc[LSA_RXMT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, lsaRxmtDesc.lsaRxmtTimer));

    aTimerDesc[DEL_ACK_TIMER].pTimerExpFunc = LakSendDelayedAck;
    aTimerDesc[DEL_ACK_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tInterface, delLsAck.delAckTimer));

    aTimerDesc[RUN_RT_TIMER].pTimerExpFunc = TmrRunRtTimer;
    aTimerDesc[RUN_RT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tOspfCxt, runRtTimer));

#ifdef DEBUG_WANTED
    aTimerDesc[DUMP_TIMER].pTimerExpFunc = TmrDumpTimer;
    aTimerDesc[DUMP_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tOspfCxt, dumpTimer));
#endif /* DEBUG_WANTED */

    aTimerDesc[EXIT_OVERFLOW_TIMER].pTimerExpFunc = TmrExitOverflowTimer;
    aTimerDesc[EXIT_OVERFLOW_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tOspfCxt, exitOverflowTimer));

    aTimerDesc[DNA_LSA_SPL_AGING_TIMER].pTimerExpFunc = TmrDnaLsaSplAgingTimer;
    aTimerDesc[DNA_LSA_SPL_AGING_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tLsaInfo, dnaLsaSplAgingTimer));

    aTimerDesc[TRAP_LIMIT_TIMER].pTimerExpFunc = TmrTrapLimitTimer;
    aTimerDesc[TRAP_LIMIT_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tOspfCxt, trapLimitTimer));

    aTimerDesc[VRF_SPF_TIMER].pTimerExpFunc = TmrVrfSpfTimer;
    aTimerDesc[VRF_SPF_TIMER].i2Offset = -1;

    aTimerDesc[NSSA_STBLTY_INTERVAL_TIMER].pTimerExpFunc =
        TmrNssaFsmStbltyTimer;
    aTimerDesc[NSSA_STBLTY_INTERVAL_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tArea, nssaStbltyIntrvlTmr));

    aTimerDesc[RESTART_GRACE_TIMER].pTimerExpFunc = TmrRestartGraceTimer;
    aTimerDesc[RESTART_GRACE_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tOspfCxt, graceTimer));
    aTimerDesc[HELPER_GRACE_TIMER].pTimerExpFunc = TmrHelperGraceTimer;
    aTimerDesc[HELPER_GRACE_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tNeighbor, helperGraceTimer));
    /* Distance timer for update configured distance value for 
     * existing routes */
    aTimerDesc[DISTANCE_TIMER].pTimerExpFunc = TmrDistanceTimer;
    aTimerDesc[DISTANCE_TIMER].i2Offset =
        (INT2) (OSPF_OFFSET (tOspfCxt, distanceTimer));

    OSPF_GBL_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC, OSPF_INVALID_CXT_ID,
                  "TMR Desc Initialized\n");

}

/*****************************************************************************/
/* Function     : TmrSetTimer                                                */
/*                                                                           */
/* Description  : Sets the timer id in the pTimer struct. Sets no_ticks in   */
/*                pTimer->app_timer. Calls oss_link_timer routine.           */
/*                                                                           */
/* Input        : pTimer       : pointer to timer structure                  */
/*                u1TimerId   : the id ofthe timer to be started             */
/*                i4NrTicks   : the value to which the timer is to be set    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
PUBLIC INT4
TmrSetTimer (tOspfTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks)
{
    pTimer->u1TimerId = u1TimerId;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* If the node state is STANDBY, Then do not start  
         *  1. Hello Timer
         *  2. Poll Timer
         *  3. Inactivity Timer
         *  4. DD Init Retransmit timer
         *  5. DD Retransmit timer
         *  6. LRQ Retransmit timer
         *  7. LSA Retransmit timer
         *  8. Delayed Ack Timer
         *  9. DDP last packet life time timer
         * 10. DUMP timer
         * 11. Trace Limit Timer 
         * 11. Wait Timer
         */

        switch (u1TimerId)
        {
            case HELLO_TIMER:
            case POLL_TIMER:
            case WAIT_TIMER:
            case INACTIVITY_TIMER:
            case DD_INIT_RXMT_TIMER:
            case DD_RXMT_TIMER:
            case LSA_REQ_RXMT_TIMER:
            case LSA_RXMT_TIMER:
            case DEL_ACK_TIMER:
            case DD_LAST_PKT_LIFE_TIME_TIMER:
            case DUMP_TIMER:
            case TRAP_LIMIT_TIMER:
                return TMR_SUCCESS;
            default:
                break;
        }
    }
    if (u1TimerId == HELLO_TIMER)

    {
        if (TmrStartTimer (gHelloTimerLst, &(pTimer->timerNode),
                           (UINT4) i4NrTicks) != TMR_SUCCESS)
        {
            OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                          "Tmr Link Failure\n");
            return ((INT4) TMR_FAILURE);
        }
    }
    else
    {
        if (TmrStartTimer (gTimerLst, &(pTimer->timerNode),
                           (UINT4) i4NrTicks) != TMR_SUCCESS)
        {
            OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                          "Tmr Link Failure\n");
            return ((INT4) TMR_FAILURE);
        }
    }
    return TMR_SUCCESS;
}

/*****************************************************************************/
/* Function     : TmrRestartTimer                                            */
/*                                                                           */
/* Description  : Restarts the timer with the specified number of ticks.     */
/*                                                                           */
/* Input        : pTimer       : pointer to timer structure                  */
/*                u1TimerId   : the id ofthe timer to be started             */
/*                i4NrTicks   : the value to which the timer is to be set    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
TmrRestartTimer (tOspfTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks)
{
    OSPF_GBL_TRC3 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                   "Restarting the TMR BLK %x TMR ID %s NoOfTicks %d\n",
                   pTimer, au1DbgTimerId[u1TimerId], i4NrTicks);
    if (u1TimerId == HELLO_TIMER)
    {
        TmrStopTimer (gHelloTimerLst, &(pTimer->timerNode));
        TmrSetTimer (pTimer, u1TimerId, i4NrTicks);
    }
    else
    {
        TmrStopTimer (gTimerLst, &(pTimer->timerNode));
        TmrSetTimer (pTimer, u1TimerId, i4NrTicks);
    }
    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID, "TMR Restarted\n");
}

/*****************************************************************************/
/* Function     : TmrDeleteTimer                                             */
/*                                                                           */
/* Description  : Deletes the timer from the timer list.                     */
/*                                                                           */
/* Input        : pTimer       : pointer to timer structure                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
TmrDeleteTimer (tOspfTimer * pTimer)
{
    OSPF_GBL_TRC2 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                   "Deleting the TMR BLK %x TMR ID %s\n",
                   pTimer, au1DbgTimerId[pTimer->u1TimerId]);

    TmrStopTimer (gTimerLst, &(pTimer->timerNode));

    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID, "TMR Deleted\n");
}

/*****************************************************************************/
/* Function     : TmrHelloDeleteTimer                                        */
/*                                                                           */
/* Description  : Deletes the  hello timer from the hello timer list.        */
/*                                                                           */
/* Input        : pTimer       : pointer to timer structure                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
TmrHelloDeleteTimer (tOspfTimer * pTimer)
{
    OSPF_GBL_TRC2 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                   "Deleting the TMR BLK %x TMR ID %s\n",
                   pTimer, au1DbgTimerId[pTimer->u1TimerId]);

    TmrStopTimer (gHelloTimerLst, &(pTimer->timerNode));
    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID, "TMR Deleted\n");
}

/*****************************************************************************/
/* Function     : TmrHandleHelloExpiry                                       */
/*                                                                           */
/* Description  : This procedure is invoked when the event indicating a hello*/
/*                timer expiry occurs. This procedure finds the expired timers*/
/*                and invokes the corresponding timer routines.              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
TmrHandleHelloExpiry (void)
{
    tTmrAppTimer       *pExpiredTimers;
    UINT1               u1TimerId;
    INT2                i2Offset;

    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                  "Tmr Expiry To Be Handled\n");
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gHelloTimerLst)) != NULL)
    {
        OSPF_GBL_TRC1 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                       "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        u1TimerId = ((tOspfTimer *) pExpiredTimers)->u1TimerId;

        i2Offset = aTimerDesc[u1TimerId].i2Offset;

        OSPF_GBL_TRC2 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                       "TmrId %s Offset %d \n", au1DbgTimerId[u1TimerId],
                       i2Offset);
        if (gOsRtr.bIgnoreExpiredTimers == OSPF_FALSE)
        {
            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter */
                (*(aTimerDesc[u1TimerId].pTimerExpFunc)) (NULL);
            }
            else
            {
                (*(aTimerDesc[u1TimerId].pTimerExpFunc))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    gOsRtr.bIgnoreExpiredTimers = OSPF_FALSE;
    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                  "Tmr Expiry Processing Over\n");
}

/*****************************************************************************/
/* Function     : TmrHandleExpiry                                            */
/*                                                                           */
/* Description  : This procedure is invoked when the event indicating a timer*/
/*                expiry occurs. This procedure finds the expired timers and */
/*                invokes the corresponding timer routines.                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
TmrHandleExpiry (void)
{
    tTmrAppTimer       *pExpiredTimers;
    UINT1               u1TimerId;
    INT2                i2Offset;
    INT2                i2TmrCnt = 0;

    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                  "Tmr Expiry To Be Handled\n");
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gTimerLst)) != NULL)
    {
        OSPF_GBL_TRC1 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                       "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        u1TimerId = ((tOspfTimer *) pExpiredTimers)->u1TimerId;

        if (u1TimerId >= MAX_TIMERS)    /* Klocwork */
        {
            continue;
        }
        i2Offset = aTimerDesc[u1TimerId].i2Offset;

        OSPF_GBL_TRC2 (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                       "TmrId %s Offset %d \n", au1DbgTimerId[u1TimerId],
                       i2Offset);
        if (gOsRtr.bIgnoreExpiredTimers == OSPF_FALSE)
        {
            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter */
                (*(aTimerDesc[u1TimerId].pTimerExpFunc)) (NULL);
            }
            else
            {
                (*(aTimerDesc[u1TimerId].pTimerExpFunc))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }

        i2TmrCnt++;
        if (i2TmrCnt == MAX_OSPF_TIMER_COUNTS)
        {
            if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_TIMER_EVENT)
                != OSIX_SUCCESS)
            {
                OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                              "Send Evt Failed\n");
            }
            break;
        }

    }
    gOsRtr.bIgnoreExpiredTimers = OSPF_FALSE;
    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                  "Tmr Expiry Processing Over\n");
}

/*****************************************************************************/
/* Function     : TmrHelloTimer                                              */
/*                                                                           */
/* Description  : This procedure restarts the timer and invokes the procedure*/
/*                to send hello on this interface.                           */
/*                                                                           */
/* Input        : pArg        : the interface whose hello timer has          */
/*                                    fired                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
TmrHelloTimer (VOID *pArg)
{
    tInterface         *pInterface = pArg;
    UINT1               u1SendHelloFlag = OSPF_TRUE;
    UINT4               u4HelloTmrInterval;
    tOspfCxt           *pOspfCxt = NULL;

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr = (tNeighbor *) NULL;

    OSPF_TRC1 (OS_RESOURCE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Hello Tmr Exp If %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));
    pOspfCxt = pInterface->pArea->pOspfCxt;
    if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS) &&
        (pOspfCxt->u1RestartStatus == OSPF_RESTART_UNPLANNED) &&
        (pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART) &&
        (pOspfCxt->u1GraceLsaTxCount != pOspfCxt->u1GrLsaMaxTxCount))
    {
        /* Incase of unplanned restart, grace LSA should be sent out
           before sending the first Hello Packet */
        u1SendHelloFlag = OSPF_FALSE;
    }
    else if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pInterface))
    {
        /* since PTOP_IF, only one neighbour will be in nbrsInIf list */
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            if (NULL == pNbr)
            {
                continue;
            }

            if ((pNbr != NULL && pNbr->u1NsmState >= NBRS_LOADING) &&
                (IS_HELLO_SUPPRESSED_NBR (pNbr)))
            {
                u1SendHelloFlag = OSPF_FALSE;
            }
            else if ((pInterface->u1NetworkType == IF_PTOMP) &&
                     (pNbr->u1NsmState > NBRS_DOWN))
            {
                HpSendHello (pInterface, pNbr, HELLO_TIMER);
                if (pNbr->u4HelloSuppressPtoMp == OSPF_TRUE)
                {
                    pNbr->u4HelloSuppressPtoMp = OSPF_FALSE;
                }
                u1SendHelloFlag = OSPF_FALSE;
            }
            else if (pNbr->u4HelloSuppressPtoMp == OSPF_TRUE)
            {
                u1SendHelloFlag = OSPF_FALSE;
            }
        }
    }

    if (u1SendHelloFlag == OSPF_TRUE)
    {
        /* 
         * The second parameter is NULL indicating that the hello pkts
         * are to be sent to all neighbors on the interface 
         */
        HpSendHello (pInterface, NULL, HELLO_TIMER);
    }
    u4HelloTmrInterval = NO_OF_TICKS_PER_SEC * (pInterface->u2HelloInterval);

    TmrSetTimer (&(pInterface->helloTimer), HELLO_TIMER,
                 ((pInterface->u1NetworkType == IF_BROADCAST) ?
                  UtilJitter (NO_OF_TICKS_PER_SEC *
                              (pInterface->u2HelloInterval),
                              OSPF_JITTER) : u4HelloTmrInterval));

    if (IS_INITIAL_IFACE_ACTIVITY_TRAP (pInterface))
        pInterface->u1HelloIntervalsElapsed++;

    OSPF_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Hello Sent To All Nbrs\n");
}

/*****************************************************************************/
/* Function     : TmrPollTimer                                               */
/*                                                                           */
/* Description  : This procedure restarts the timer and invokes the procedure*/
/*                to send hello on this interface.                           */
/*                                                                           */
/* Input        : pArg        : the interface whose poll timer has           */
/*                                    fired                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
TmrPollTimer (VOID *pArg)
{
    tInterface         *pInterface = pArg;

    OSPF_TRC1 (OS_RESOURCE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Poll Tmr Exp If %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    /* 
     * The second parameter is NULL indicating that the hello pkts
     * are to be sent to all neighbors on the interface 
     */

    HpSendHello (pInterface, NULL, POLL_TIMER);
    TmrSetTimer (&(pInterface->pollTimer), POLL_TIMER,
                 NO_OF_TICKS_PER_SEC * (pInterface->i4PollInterval));

    OSPF_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Hello Sent To All Nbrs That Are DN\n");
}

/*****************************************************************************/
/* Function     : TmrWaitTimer                                               */
/*                                                                           */
/* Description  : The interface state machine is invoked with event          */
/*                IFE_WAIT_TIMER.                                            */
/*                                                                           */
/* Input        : pArg        : the interface whose wait timer has           */
/*                                    fired                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrWaitTimer (VOID *pArg)
{
    tInterface         *pInterface = pArg;
    OSPF_TRC1 (OS_RESOURCE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Wait Tmr Exp If %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    GENERATE_IF_EVENT (pInterface, IFE_WAIT_TIMER);

    OSPF_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "ISM Scheduled\n");
}

/*****************************************************************************/
/* Function     : TmrInactivityTimer                                         */
/*                                                                           */
/* Description  : The firing of the inactivity timer indicates that the      */
/*                neighbor can be considered dead. The neighbor state machine*/
/*                is invoked with the event NBRE_INACTIVITY_TIMER.           */
/*                                                                           */
/* Input        : pArg        : nbr whose inactivity timer has fired         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrInactivityTimer (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;

    OSPF_NBR_TRC1 (OS_RESOURCE_TRC, pNbr,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Inactivity Tmr Exp Nbr %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

    if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface) &&
        (pNbr->bHelloSuppression == OSPF_TRUE) &&
        (pNbr->u1NsmState >= NBRS_LOADING))
    {
        return;
    }

    /* If helper is down then graceful restart should exit */
    if ((pNbr->pInterface->pArea->pOspfCxt != NULL)
        && (pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState ==
            OSPF_GR_RESTART))
    {
        GrExitGracefultRestart (pNbr->pInterface->pArea->pOspfCxt,
                                OSPF_RESTART_TOP_CHG);
    }
    /* if the neighbor is in graful restart process, don't make the neighbor state
       as down */
    if (pNbr->u1NbrHelperStatus != OSPF_GR_HELPING)
    {
        GENERATE_NBR_EVENT (pNbr, NBRE_INACTIVITY_TIMER);
    }

    OSPF_NBR_TRC (CONTROL_PLANE_TRC, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Nbr Evt Generated\n");
}

/*****************************************************************************/
/* Function     : TmrDdInitRxmtTimer                                         */
/*                                                                           */
/* Description  : The timer is restarted and the procedure to send empty DDP */
/*                is invoked.                                                */
/*                                                                           */
/* Input        : pAgr            : the nbr associated with the timer        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrDdInitRxmtTimer (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;

    UINT4               u4RxmtTmrInterval;

    OSPF_NBR_TRC1 (OS_RESOURCE_TRC, pNbr,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "InitRxmt Tmr Exp Nbr %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

    DdpRxmtDdp (pNbr);
    u4RxmtTmrInterval = NO_OF_TICKS_PER_SEC *
        (pNbr->pInterface->u2RxmtInterval);

    TmrSetTimer (&(pNbr->dbSummary.ddTimer), DD_INIT_RXMT_TIMER,
                 ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ?
                  UtilJitter (NO_OF_TICKS_PER_SEC *
                              (pNbr->pInterface->u2RxmtInterval),
                              OSPF_JITTER) : u4RxmtTmrInterval));

    OSPF_NBR_TRC (CONTROL_PLANE_TRC, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  " DDP Sent To Nbr\n");
}

/*****************************************************************************/
/* Function     : TmrDdRxmtTimer                                             */
/*                                                                           */
/* Description  : The timer is restarted and the procedure to retransmit the */
/*                previous DDP is invoked.                                   */
/*                                                                           */
/* Input        : pAgr            : the nbr associated with the timer        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrDdRxmtTimer (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;

    UINT4               u4RxmtTmrInterval;

    OSPF_NBR_TRC1 (OS_RESOURCE_TRC, pNbr,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "DDP Rxmt Tmr Exp Nbr %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

    DdpRxmtDdp (pNbr);
    u4RxmtTmrInterval = NO_OF_TICKS_PER_SEC *
        (pNbr->pInterface->u2RxmtInterval);

    TmrSetTimer (&(pNbr->dbSummary.ddTimer), DD_RXMT_TIMER,
                 ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ?
                  UtilJitter (NO_OF_TICKS_PER_SEC *
                              (pNbr->pInterface->u2RxmtInterval),
                              OSPF_JITTER) : u4RxmtTmrInterval));

    OSPF_NBR_TRC (CONTROL_PLANE_TRC, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId, "DDP Rxmt\n");
}

/*****************************************************************************/
/* Function     :  TmrLsaNormalAgingTimer                                    */
/*                                                                           */
/* Description  : The timer is restarted and the procedure to retransmit     */
/*                request packet is invoked.                                 */
/*                                                                           */
/* Input        : pArg              : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrLsaNormalAgingTimer (VOID *pArg)
{
    tLsaInfo           *pLsaInfo = pArg;
    UINT4               u4RemGraceTime = 0;

    OSPF_TRC1 (OS_RESOURCE_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
               "LSA Normal Aging Tmr Exp LSType %s\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType]);

    if (!IS_DNA_LSA (pLsaInfo))
    {
        pLsaInfo->u2LsaAge += CHECK_AGE - (pLsaInfo->u2LsaAge % CHECK_AGE);
    }

    if ((IS_DNA_LSA (pLsaInfo)) && IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        /*set DO_NOT_AGE in the lsa */
        pLsaInfo->u2LsaAge = DO_NOT_AGE | MAX_AGE;
    }
    else
    {
        if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
        {
            pLsaInfo->u2LsaAge = MAX_AGE;
            OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "FUNC :TmrLsaNormalAgingTimer, LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                       pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                       pLsaInfo->u2LsaAge, pLsaInfo->lsaSeqNum);
        }
    }

    if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            AgdFlushOut (pLsaInfo);
        }
        else
        {
            /* During graceful restart operation LSA's cannot be flushed */
            pLsaInfo->lsaAgingTimer.u1TimerId = LSA_NORMAL_AGING_TIMER;
            TmrGetRemainingTime (gTimerLst, (tTmrAppTimer *)
                                 & (pLsaInfo->pOspfCxt->graceTimer.timerNode),
                                 &u4RemGraceTime);
            TmrStartTimer (gTimerLst, &(pLsaInfo->lsaAgingTimer.timerNode),
                           u4RemGraceTime);
        }
    }
    else
    {
        /* Check sum of the LSA is checked every  CHECK_AGE Interval */
        if (AgdCheckChksum (pLsaInfo) == OSPF_FAILURE)
        {
            gOsRtr.bIgnoreExpiredTimers = OSPF_TRUE;
            return;
        }

        pLsaInfo->lsaAgingTimer.u1TimerId = LSA_NORMAL_AGING_TIMER;
        TmrStartTimer (gTimerLst, &(pLsaInfo->lsaAgingTimer.timerNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * CHECK_AGE);

        /* 
         * In case of Self originated LSA's if age of the LSA is equal to
         * LSA_REFRESH_INTERVAL & LSA does not describe any unreachable 
         * Destination than that lsa is reoriginated . 
         * REF Point 1 of LSA origination RFC 2178. 
         */
        if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            if (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo) &&
                (pLsaInfo->u1LsaFlushed == OSPF_FALSE) &&
                (pLsaInfo->u2LsaAge ==
                 LSA_REFRESH_INTERVAL / NO_OF_TICKS_PER_SEC))
            {
                if ((pLsaInfo->pArea->u4ActIntCount != 0)
                    && (pLsaInfo->pLsaDesc != NULL))
                {
                    pLsaInfo->u1LsaRefresh = OSPF_FALSE;
                    OlsSignalLsaRegenInCxt (pLsaInfo->pOspfCxt,
                                            SIG_LS_REFRESH,
                                            (UINT1 *) pLsaInfo->pLsaDesc);
                }
            }
            OSPF_TRC (CONTROL_PLANE_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "Chk Aged And Refreshed\n");
        }
        else
        {
            /* Graceful restart is in progress, LSA can be reoriginated
             * only after exiting graceful restart */
            pLsaInfo->lsaAgingTimer.u1TimerId = LSA_NORMAL_AGING_TIMER;
            TmrGetRemainingTime (gTimerLst, (tTmrAppTimer *)
                                 & (pLsaInfo->pOspfCxt->graceTimer.timerNode),
                                 &u4RemGraceTime);
            TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                             LSA_NORMAL_AGING_TIMER, u4RemGraceTime);
        }
    }
}

/*****************************************************************************/
/* Function     : TmrLsaPrematureAgingTimer                                  */
/*                                                                           */
/* Description  : The lsa has reached Max_age and it is flushed out of the   */
/*                routing domain by setting its age to Max_age and           */
/*                reflooding.                                                */
/*                                                                           */
/* Input        : pArg                  : pointer to the advertisement       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrLsaPrematureAgingTimer (VOID *pArg)
{
    tLsaInfo           *pLsaInfo = pArg;
    UINT4               u4OspfCxtId;
    UINT4               u4RemGraceTime = 0;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;
    OSPF_TRC1 (OS_RESOURCE_TRC, u4OspfCxtId,
               "Premature Aging Tmr Exp LSType %s\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType]);
    if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
    {
        AgdFlushOut (pLsaInfo);
    }
    else
    {
        /* During graceful restart operation LSA's cannot be flushed */
        pLsaInfo->lsaAgingTimer.u1TimerId = LSA_NORMAL_AGING_TIMER;
        TmrGetRemainingTime (gTimerLst,
                             &(pLsaInfo->pOspfCxt->graceTimer.timerNode),
                             &u4RemGraceTime);
        TmrStartTimer (gTimerLst, &(pLsaInfo->lsaAgingTimer.timerNode),
                       u4RemGraceTime);
    }

    OSPF_TRC (CONTROL_PLANE_TRC, u4OspfCxtId,
              "LSA Prematurely Aged And Flooded Out\n");
}

/*****************************************************************************/
/* Function     : TmrMinLsaIntervalTimer                                     */
/*                                                                           */
/* Description  : The min_lsa_interval_exp flag is set. If the lsa_changed   */
/*                flag is set a new instance of the advertisement is         */
/*                originated.                                                */
/*                                                                           */
/* Input        : pArg               : the lsa_desc of the associated lsa    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrMinLsaIntervalTimer (VOID *pArg)
{
    tLsaDesc           *pLsaDesc = pArg;
    OSPF_TRC1 (OS_RESOURCE_TRC, pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
               "MinLSAInt Exp LSType %s\n",
               au1DbgLsaType[pLsaDesc->pLsaInfo->lsaId.u1LsaType]);

    pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;

    if ((pLsaDesc->u1LsaChanged == OSPF_TRUE) &&
        (pLsaDesc->pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE))
    {

        pLsaDesc->u1LsaChanged = OSPF_FALSE;
        OlsSignalLsaRegenInCxt (pLsaDesc->pLsaInfo->pOspfCxt,
                                SIG_NEXT_INSTANCE, (UINT1 *) pLsaDesc);
    }
}

/*****************************************************************************/
/* Function     : TmrLsaRxmtTimer                                            */
/*                                                                           */
/* Description  : The timer is restarted and the procedure to construct and  */
/*                send a LSU pkt from the rxmt list is invoked.              */
/*                                                                           */
/* Input        : pArg            : the nbr associated with the timer        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrLsaRxmtTimer (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;
    UINT4               u4RxmtTmrInterval;

    OSPF_NBR_TRC1 (OS_RESOURCE_TRC, pNbr,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "LSA Rxmt Exp Nbr %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

    /* start timer if nbr rxmt count present */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount > 0)
    {
        LsuRxmtLsu (pNbr);

        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount > 0)
        {
            /* Neigbor LSA Rxntcount can become 0 in case of re-transmitting
             * Grace LSA. If the re-transmission count has reached the
             * threshold, Grace LSA will be deleted from the re-transmission
             * list without re-transmitting to the neighbor */
            /* If the context is undergoing graceful restart, then re-transmit
             * the LSA with 1s interval */
            if (pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState
                == OSPF_GR_SHUTDOWN)
            {
                u4RxmtTmrInterval = NO_OF_TICKS_PER_SEC;
            }
            else
            {
                u4RxmtTmrInterval = NO_OF_TICKS_PER_SEC *
                    (pNbr->pInterface->u2RxmtInterval);
            }
            TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer), LSA_RXMT_TIMER,
                         ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ?
                          UtilJitter (u4RxmtTmrInterval, OSPF_JITTER) :
                          u4RxmtTmrInterval));
        }
    }
    OSPF_NBR_TRC (CONTROL_PLANE_TRC, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId, "LSU Rxmt\n");
}

/*****************************************************************************/
/* Function     : TmrRunRtTimer                                              */
/*                                                                           */
/* Description  : The router_network_lsa_changed flag is checked. If it is   */
/*                set then the procedure to calculate the entire routing     */
/*                table is invoked. The flag is also reset.                  */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrRunRtTimer (VOID *pArg)
{

    tOspfCxt           *pOspfCxt = pArg;
    UINT4               u4CurrentTime = 0;
    UINT4               u4RtInterval = 0;
    UINT4               u4SpfHoldTimeInterval;
    INT4                i4Temp = 0;

    if (pOspfCxt == NULL)
    {
        return;
    }
    OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId, "RT Tmr Exp\n");

    if (gOsRtr.u4RTstaggeredCtxId != OSPF_INVALID_CXT_ID)
    {
        /* This means that route calculation is relinquished in some context.
         * So don't restart the route calculation now. Defer the route 
         * calculation by adding this context to the spacing timer
         */
        if (pOspfCxt->u1VrfSpfTmrStatus != OSPF_TRUE)
        {
            TMO_DLL_Add (&(gOsRtr.vrfSpfRtcList), &(pOspfCxt->vrfSpfNode));
            pOspfCxt->u1VrfSpfTmrStatus = OSPF_TRUE;
            /* Restart the relinquish timer */
            OsixGetSysTime ((tOsixSysTime *) & (u4CurrentTime));
            TmrSetTimer (&(gOsRtr.vrfSpfTimer), VRF_SPF_TIMER,
                         (u4CurrentTime - pOspfCxt->u4LastCalTime));

            OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId,
                      "Relinquish state. Added Ctx to SPF timer list\n");
        }
        return;
    }

    if ((TMO_DLL_Count (&(gOsRtr.vrfSpfRtcList)) != 0) ||
        (gOsRtr.u1RouteCalcCompleted != OSPF_ROUTE_CALC_COMPLETED))
    {
        if (pOspfCxt->u1VrfSpfTmrStatus != OSPF_TRUE)
        {
            /* when pOspfCxt->u1VrfSpfTmrStatus is set to OSPF_TRUE
             * route calculation is pending for this context and the 
             * context is added to the global VrfSpfRtcList
             */
            TMO_DLL_Add (&(gOsRtr.vrfSpfRtcList), &(pOspfCxt->vrfSpfNode));
            pOspfCxt->u1VrfSpfTmrStatus = OSPF_TRUE;

            /* convert the milli second to STUPS */
            if ((u4SpfHoldTimeInterval =
                 OSPF_TIME_IN_STUPS (pOspfCxt->u4SpfHoldTimeInterval)) == 0)
            {
                /* Fsap Timer can be started only with minimum value 10 ms i.e 1 STUPS */
                u4SpfHoldTimeInterval = 1;
            }

            /* Restart the relinquish timer */
            OsixGetSysTime ((tOsixSysTime *) & (u4CurrentTime));

            i4Temp =
                (INT4) ((pOspfCxt->u4LastCalTime - u4CurrentTime) +
                        u4SpfHoldTimeInterval);
            if (i4Temp < 0)
            {
                u4RtInterval = OSPF_TIME_IN_STUPS (OSPF_DEF_VRF_SPF_INTERVAL);
            }
            else if (((UINT4) i4Temp > OSPF_DEF_VRF_SPF_INTERVAL))
            {
                u4RtInterval = (UINT4) i4Temp;
            }
            else
            {
                u4RtInterval = OSPF_TIME_IN_STUPS (OSPF_DEF_VRF_SPF_INTERVAL);
            }

            TmrSetTimer (&(gOsRtr.vrfSpfTimer), VRF_SPF_TIMER, u4RtInterval);

        }
        return;
    }

    if (gOsRtr.u1RouteCalcCompleted == OSPF_ROUTE_CALC_COMPLETED)
    {
        OsixGetSysTime ((tOsixSysTime *) & (u4CurrentTime));

        if (((u4CurrentTime - pOspfCxt->u4LastCalTime)
             > gOsRtr.u4VrfSpfInterval) &&
            (pOspfCxt->u1RtrNetworkLsaChanged == OSPF_TRUE))
        {
            pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;

            pOspfCxt->u1AsbSummaryLsaChanged = OSPF_FALSE;

            RtcCalculateRtInCxt (pOspfCxt);

            OsixGetSysTime (&(pOspfCxt->u4LastCalTime));
        }
        else
        {
            if (pOspfCxt->u1VrfSpfTmrStatus != OSPF_TRUE)
            {
                /* when pOspfCxt->u1VrfSpfTmrStatus is set to OSPF_TRUE
                 * route calculation is pending for this context and the 
                 * context is added to the global VrfSpfRtcList 
                 */

                TMO_DLL_Add (&(gOsRtr.vrfSpfRtcList), &(pOspfCxt->vrfSpfNode));
                pOspfCxt->u1VrfSpfTmrStatus = OSPF_TRUE;
                TmrSetTimer (&(gOsRtr.vrfSpfTimer), VRF_SPF_TIMER,
                             (u4CurrentTime - pOspfCxt->u4LastCalTime));
            }
        }
    }
    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
              "Entire RT Calculated\n");
}

/****************************************************************************/
/* Function     : TmrNssaFsmStbltyTimer                                     */
/*                                                                          */
/* Description  : This routine handles NSSA Stblty Timer timeout            */
/*                                                                          */
/* Input        : pArg                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/****************************************************************************/
PRIVATE VOID
TmrNssaFsmStbltyTimer (VOID *pArg)
{
    tArea              *pArea = pArg;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: TmrNssaFsmStbltyTimer\n");
    switch (pArea->u1NssaTrnsltrState)
    {

        case TRNSLTR_STATE_ENAELCTD:
            NssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                     TRNSLTR_STATE_DISABLED);
            break;

        case TRNSLTR_STATE_DISABLED:
            OSPF_TRC2 (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                       "Error : StbltyIntrvlTimeout Event in \
                Invalid State %d in Area %x\n", pArea->u1NssaTrnsltrState, pArea->areaId);
            break;

        default:
            OSPF_TRC2 (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                       "Error : Invalid State %d in Area %x\n",
                       pArea->u1NssaTrnsltrState, pArea->areaId);
            break;
    }
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: TmrNssaFsmStbltyTimer\n");
}

#ifdef DEBUG_WANTED
/*****************************************************************************/
/* Function     : TmrDumpTimer                                               */
/*                                                                           */
/* Description  : This routine initiates the dump of all the router's data-  */
/*                structure                                                  */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrDumpTimer (VOID *pArg)
{
    tOspfCxt           *pOspfCxt = pArg;
    DbgDumpAllInCxt (pOspfCxt);
    TmrSetTimer (&(pOspfCxt->dumpTimer), DUMP_TIMER,
                 NO_OF_TICKS_PER_SEC * 15 * 60);
}
#endif /* DEBUG_WANTED */

/*****************************************************************************/
/* Function     : TmrExitOverflowTimer                                       */
/*                                                                           */
/* Description  : in this procedure, the router attempts to leave the        */
/*                overflow state                                             */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrExitOverflowTimer (VOID *pArg)
{
    UINT4               u4ASEDefLsaCnt;
    tOspfCxt           *pOspfCxt = pArg;
    UINT4               u4TranslatedLsaCount = 0;

    OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId, "ExitOvrFlw Tmr Exp\n");

    u4ASEDefLsaCnt = (pOspfCxt->bDefaultAseLsaPresenc == OSPF_TRUE) ? 1 : 0;

    u4TranslatedLsaCount = LsuGetTranslatedLsaCount (pOspfCxt);

    if ((pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
        (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT (pOspfCxt)
         + pOspfCxt->u4ExtRtCount + u4TranslatedLsaCount -
         u4ASEDefLsaCnt < (UINT4) pOspfCxt->i4ExtLsdbLimit))
    {
        /* router leaves the overflow state */
        pOspfCxt->bOverflowState = OSPF_FALSE;
        LsuGenerateNonDefaultAsExtLsasInCxt (pOspfCxt);

        OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                  "Rtr Leaves OverFlw State\n");
    }
    else
    {
        /* router re-enters the overflow state */
        if (pOspfCxt->u4ExitOverflowInterval != DONT_LEAVE_OVERFLOW_STATE)
        {
            TmrSetTimer (&(pOspfCxt->exitOverflowTimer), EXIT_OVERFLOW_TIMER,
                         UtilJitter (NO_OF_TICKS_PER_SEC *
                                     (pOspfCxt->u4ExitOverflowInterval),
                                     OSPF_JITTER));
        }
        OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                  "Rtr ReEnters OverFlw State\n");
    }
}

/*****************************************************************************/
/* Function     : TmrDnaLsaSplAgingTimer                                     */
/*                                                                           */
/* Description  : The DNA stale lsa's are present for  Max_age  hence  they  */
/*                are flushed out of the routing domain by setting           */
/*                its age to Max_age and reflooding.                         */
/*                                                                           */
/* Input        : pArg          : pointer to the advertisement               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrDnaLsaSplAgingTimer (VOID *pArg)
{
    tLsaInfo           *pLsaInfo = pArg;
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;
    OSPF_TRC1 (OS_RESOURCE_TRC, u4OspfCxtId,
               "DNA Spl Aging Tmr Exp LSType %s\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType]);

    AgdFlushOut (pLsaInfo);

    OSPF_TRC (CONTROL_PLANE_TRC, u4OspfCxtId,
              "Stale DNA LSA Flushed Out Of Routing Domain\n");
}

/*****************************************************************************/
/* Function     : TmrTrapLimitTimer                                          */
/*                                                                           */
/* Description  : This routine initializes tap limit count, and restarts once*/
/*                again he trap limit timer.                                 */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*****************************************************************************/
PRIVATE VOID
TmrTrapLimitTimer (VOID *pArg)
{
    tOspfCxt           *pOspfCxt = pArg;

    /* Reset the trap count value to 0 to start a new window */
    OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId, "Trap Limit Tmr Exp\n");
    pOspfCxt->u1TrapCount = 0;
    TmrRestartTimer (&(pOspfCxt->trapLimitTimer), TRAP_LIMIT_TIMER,
                     TRAP_LIMIT_TIMER_INTERVAL);
    if (IS_INITIAL_RTR_ACTIVITY_TRAP_IN_CXT (pOspfCxt))
        pOspfCxt->u1SwPeriodsElapsed++;

    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
              "New Window Started For Limiting Trap\n");
}

/*****************************************************************************/
/* Function     : TmrVrfSpfTimer                                             */
/*                                                                           */
/* Description  : This routine initializes tap limit count, and restarts once*/
/*                again global spacing timer.                                 */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*****************************************************************************/
PRIVATE VOID
TmrVrfSpfTimer (VOID *pArg)
{
    tOspfCxt           *pOspfCxt = NULL;
    tTMO_DLL_NODE      *pVrfSpfNode = NULL;
    UNUSED_PARAM (pArg);

    if (gOsRtr.u4RTstaggeredCtxId != OSPF_INVALID_CXT_ID)
    {
        /* This means that route calculation is relinquished in some context.
         * So don't go for route re-calculation,just restart the SPF timer
         */
        if (TMO_DLL_Count (&(gOsRtr.vrfSpfRtcList)) != 0)
        {
            TmrSetTimer (&(gOsRtr.vrfSpfTimer), VRF_SPF_TIMER,
                         gOsRtr.u4VrfSpfInterval);
        }
        return;
    }

    if (TMO_DLL_Count (&(gOsRtr.vrfSpfRtcList)) != 0)
    {
        if (gOsRtr.u1RouteCalcCompleted != OSPF_ROUTE_CALC_COMPLETED)
        {
            TmrSetTimer (&(gOsRtr.vrfSpfTimer), VRF_SPF_TIMER,
                         gOsRtr.u4VrfSpfInterval);
            return;
        }
        else
        {
            pVrfSpfNode = TMO_DLL_Get (&(gOsRtr.vrfSpfRtcList));
            pOspfCxt = (tOspfCxt *) (VOID *) ((UINT1 *) pVrfSpfNode -
                                              OSPF_OFFSET (tOspfCxt,
                                                           vrfSpfNode));
            pOspfCxt->u1VrfSpfTmrStatus = OSPF_FALSE;
        }
    }
    else
    {
        return;
    }

    if (pOspfCxt->u1RtrNetworkLsaChanged == OSPF_TRUE)
    {
        pOspfCxt->u1RtrNetworkLsaChanged = OSPF_FALSE;

        pOspfCxt->u1AsbSummaryLsaChanged = OSPF_FALSE;
        if (pOspfCxt->admnStat == OSPF_ENABLED)
        {
            RtcCalculateRtInCxt (pOspfCxt);
        }
        OsixGetSysTime (&(pOspfCxt->u4LastCalTime));
    }
    if (TMO_DLL_Count (&(gOsRtr.vrfSpfRtcList)) != 0)
    {
        TmrSetTimer (&(gOsRtr.vrfSpfTimer), VRF_SPF_TIMER,
                     gOsRtr.u4VrfSpfInterval);
    }
}

/*****************************************************************************/
/* Function     : TmrRestartGraceTimer                                       */
/*                                                                           */
/* Description  : This routine exits the graceful restart process for        */
/*                this instance                                              */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*****************************************************************************/
PRIVATE VOID
TmrRestartGraceTimer (VOID *pArg)
{
    tOspfCxt           *pOspfCxt = pArg;

    /* Reset the trap count value to 0 to start a new window */
    OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId, "Grace Tmr Exp\n");

    GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_TIMEDOUT);
}

/*****************************************************************************/
/* Function     : TmrHelperGraceTimer                                        */
/*                                                                           */
/* Description  :  Timer expiry handler for grace timer in helper module     */
/*                 If this timer expires, router should exit the helper mode */
/*                                                                           */
/* Input        : pArg          : pointer to the neighbor stucture           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrHelperGraceTimer (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;
    tOspfCxt           *pOspfCxt = pArg;
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pOspfCxt->u4OspfCxtId;
    OSPF_TRC1 (OS_RESOURCE_TRC, u4OspfCxtId,
               "Grace timer expired in helper for the neighbor %s\n",
               pNbr->nbrId);

    GrExitHelper (OSPF_HELPER_GRACE_TIMEDOUT, pNbr);

}

/*****************************************************************************/
/* Function     : TmrDistanceTimer                                           */
/* Description  :  Timer expiry handler for update configured distance       */
/*                 value for all existing routes.                            */
/* Input        : pArg                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrDistanceTimer (VOID *pArg)
{
    tOspfCxt           *pOspfCxt = pArg;
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pOspfCxt->u4OspfCxtId;
    OSPF_TRC (OS_RESOURCE_TRC, u4OspfCxtId, "Distance timer expired \n");
    /* Configure Distance value for exit routes */
    OspfUpdateDistanceForExitRoutes (pOspfCxt);
}

/*---------------------------------------------------------------------------*/
/*                         End of file ostmrif.c                             */
/*---------------------------------------------------------------------------*/
