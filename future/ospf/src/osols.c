/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osols.c,v 1.58 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures related to the
 *             link state advertisements
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */

PRIVATE INT4        OlsCondenseSummary
PROTO ((tArea * pArea, tRtEntry * pRtEntry));
PRIVATE UINT1      *OlsConstructOpaqueLsa
PROTO ((tOpqLSAInfo * pOpqLSAInfo, tLSASEQNUM seqNum, UINT2 *pLen));
PRIVATE UINT1      *OlsConstructRtrLsa
PROTO ((tArea * pArea, tLSASEQNUM seqNum, UINT2 *pLen));
PRIVATE UINT1      *OlsConstructNetworkLsa
PROTO ((tInterface * pInterface, tLSASEQNUM seqNum, UINT2 *pLen));
PRIVATE UINT1      *OlsConstructAsExtLsaInCxt
PROTO ((tOspfCxt * pOspfCxt, UINT1 *pPtr, UINT1 u1InternalType,
        tLSASEQNUM seqNum, UINT2 *pLen, tLINKSTATEID * pLsId));
PRIVATE UINT1      *OlsConstructSummaryLsa
PROTO ((tArea * pArea, UINT1 u1Type, UINT1 *pStruct, tLSASEQNUM seqNum,
        UINT2 *pLen, tLINKSTATEID * pLsId));
PRIVATE UINT1      *OlsRtrLsaAddCosts
PROTO ((UINT1 *pBuf, tMibMetric * pMetricLst));
PRIVATE VOID        OlsConstructLsHeaderInCxt
PROTO ((tOspfCxt * pOspfCxt, UINT1 *pLsa, UINT2 u2Len,
        UINT1 u1LsaType, tLINKSTATEID * pLinkStateId,
        tLSASEQNUM lsaSeqNum, tOPTIONS options));
PRIVATE UINT1      *OlsRtrLsaAddRtrIdInCxt PROTO ((tArea * pArea,
                                                   UINT1 *pBuf,
                                                   UINT2 *pLinkCount));
PRIVATE INT4        OlsGetSummaryCost
PROTO ((UINT1 *pStruct, UINT1 u1Type, UINT1 u1Tos));
PRIVATE UINT1 OlsIsTransitLink PROTO ((tInterface * pInterface));
PRIVATE INT4 OlsIsEligibleToGenLsaInCxt PROTO ((tArea * pArea,
                                                UINT1 u1InternalType));

PRIVATE UINT1      *OlsConstructType7Lsa
PROTO ((UINT1 *pPtr, UINT1 u1InternalType, tArea * pArea,
        tLSASEQNUM seqNum, UINT2 *pLen, tLINKSTATEID * pLsId));
PRIVATE UINT1       OlsSetType7FwdAddr
PROTO ((tArea * pArea, UINT1 *pType7Lsa));

PRIVATE INT4 OlsTrapCheckLsuChange PROTO ((UINT1 *pDbLsa, UINT1 *pRcvdLsa));
PRIVATE INT4
    OlsIsEligibleToGenNssaLsa PROTO ((tLsaInfo * pLsaInfo, tArea * pArea));

PRIVATE VOID
    OlsBuildDefaultSummaryParam PROTO ((tArea * pArea,
                                        tSummaryParam * pSummaryParam));

PRIVATE tAreaId    *OlsGetPathAreaId (tRtEntry * pRtEntry);
/******************************************************************************

Function        :   OlsStartTimerForLsaRegen

Description     :   starts the min_lsa_interval timer for the lsa regeneration

Input           :
                    pLsaInfo  :   ptr to lsa info structure

Output          :   None

Returns         :   None

******************************************************************************/

EXPORT VOID
OlsStartTimerForLsaRegen (tLsaInfo * pLsaInfo)
{
    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsStartTimerForLsaRegen \n");
    if (pLsaInfo->pLsaDesc == NULL)
        return;

    pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
    if (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired == OSPF_TRUE)
    {

        TmrRestartTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer),
                         MIN_LSA_INTERVAL_TIMER,
                         (NO_OF_TICKS_PER_SEC *
                          pLsaInfo->pOspfCxt->u2MinLsaInterval));
        pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_FALSE;
    }
}

/******************************************************************************

Function        :   OlsGenerateAsExtLsas

Description     :   generates all ASE lsas

Input           :   None

Output          :   None

Returns         :   None

******************************************************************************/

EXPORT VOID
OlsGenerateAsExtLsasInCxt (tOspfCxt * pOspfCxt)
{

    tExtRoute          *pExtRoute;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGenerateAsExtLsas\n");

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pExtRoute = (tExtRoute *) pAppSpecPtr;
        RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);
        au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
        au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4Key;
        pLeafNode = inParams.pLeafNode;
        pExtRoute = NULL;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pAppSpecPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);
}

/******************************************************************************

Function        :   OlsGenerateIndicationLsa

Description     :   This function is added for supporting DEMAND_CIRCUIT.
                    It generates Indication lsas

Input           :   
                    pArea  : pointer to area to which indication lsa has to be
                              generated.

Output          :   None

Returns         :   None

******************************************************************************/

EXPORT VOID
OlsGenerateIndicationLsa (tArea * pArea)
{
    tSummaryParam       summaryParam;
    tIPADDRMASK         mask;

    IP_ADDR_COPY (mask, gHostRouteMask);

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGenerateIndicationLsa\n");

    summaryParam.aMetric[DECODE_TOS (TOS_0)].u4Value = LS_INFINITY_24BIT;
    IP_ADDR_COPY (&summaryParam.ipAddr, &(pArea->pOspfCxt->rtrId));
    IP_ADDR_COPY (&summaryParam.ipAddrMask, mask);

#ifdef TOS_SUPPORT
    summaryParam.options =
        (UINT1) ((((pArea->pOspfCxt->bTosCapability ==
                    OSPF_TRUE) ? T_BIT_MASK : 0) | ((pArea->u4AreaType !=
                                                     NORMAL_AREA) ? 0 :
                                                    E_BIT_MASK)) &
                 ~DC_BIT_MASK);

#else /* TOS_SUPPORT */
/* TOSDEL */
    summaryParam.options =
        (UINT1) (((0) |
                  ((pArea->u4AreaType != NORMAL_AREA) ?
                   0 : E_BIT_MASK)) & ~DC_BIT_MASK);
#endif /* TOS_SUPPORT */

    if (pArea->u4AreaType == NORMAL_AREA)
    {
        pArea->bIndicationLsaPresence = OSPF_TRUE;
        OlsGenerateLsa (pArea, INDICATION_LSA, &(pArea->pOspfCxt->rtrId),
                        (UINT1 *) &summaryParam);
    }

}

/******************************************************************************

Function        :   OlsSignalLsaRegenInCxt

Description     :   (re)generates an lsa depending on the type of signal

Input           :
                    u1Type     :   signal type
                    pPtr       :   pointer to the structure associated with the
                                    signal/lsa 

Output          :   None

Returns         :   None

******************************************************************************/

EXPORT INT4
OlsSignalLsaRegenInCxt (tOspfCxt * pOspfCxt, UINT1 u1Type, UINT1 *pPtr)
{
    tLsaDesc           *pLsaDesc;
    tInterface         *pInterface;
    tArea              *pArea = (tArea *) NULL;
    tLsaInfo           *pLsaInfo;
    UINT1               u1Flag = OSPF_FALSE;
    tOpqLSAInfo        *pOpqLSAInfo;
    tOpqLSAInfo        *pOpqLSA;
    tArea              *pTmpArea;
    tAreaId             AreaId;
    tAppInfo           *pAppInfo = (tAppInfo *) NULL;
    tSummaryParam       defSummaryParam;
    tRtEntry           *pRtEntry;
    UINT1               u1LsaType = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OlsSignalLsaRegenInCxt\n");
    if (!(pOspfCxt->admnStat == OSPF_ENABLED))
    {
        return OSPF_FAILURE;
    }
    pOpqLSA = (tOpqLSAInfo *) (VOID *) pPtr;

    if ((pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
        && ((pOpqLSA->u1OpqLSAType != TYPE9_OPQ_LSA)
            && (pOspfCxt->u1OspfRestartState != OSPF_GR_RESTART)))
    {
        /* LSA cannot be (re)generated in GR mode */
        return OSPF_SUCCESS;
    }
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return OSPF_SUCCESS;
    }
    switch (u1Type)
    {

        case SIG_LS_REFRESH:
        case SIG_NEXT_INSTANCE:
            pLsaDesc = (tLsaDesc *) (VOID *) pPtr;
            OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
                       "FUNC :OlsSignalLsaRegenInCxt LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                       pLsaDesc->pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->lsaId.
                                               linkStateId),
                       pLsaDesc->pLsaInfo->u2LsaAge,
                       pLsaDesc->pLsaInfo->lsaSeqNum);

            OlsGenerateLsa (pLsaDesc->pLsaInfo->pArea,
                            pLsaDesc->u1InternalLsaType,
                            &(pLsaDesc->pLsaInfo->lsaId.linkStateId),
                            pLsaDesc->pAssoPrimitive);
            break;

        case SIG_HOST_ATTACHED:
        case SIG_HOST_DETACHED:
            pArea = (tArea *) (VOID *) pPtr;
            OlsGenerateLsa (pArea, ROUTER_LSA, &(pOspfCxt->rtrId),
                            (UINT1 *) pArea);
            break;

        case SIG_IF_STATE_CHANGE:
        case SIG_IF_METRIC_PARAM_CHANGE:
        case SIG_VIRT_LINK_COST_CHANGE:
            pInterface = (tInterface *) (VOID *) pPtr;
            OlsGenerateLsa (pInterface->pArea, ROUTER_LSA, &(pOspfCxt->rtrId),
                            (UINT1 *) pInterface->pArea);

            break;

        case SIG_DR_CHANGE:
            pInterface = (tInterface *) (VOID *) pPtr;
            OlsGenerateLsa (pInterface->pArea, ROUTER_LSA, &(pOspfCxt->rtrId),
                            (UINT1 *) pInterface->pArea);

            if (IS_DR (pInterface))
            {
                OlsGenerateLsa (pInterface->pArea, NETWORK_LSA,
                                &(pInterface->ifIpAddr), (UINT1 *) pInterface);
            }

            /* when the nbr attached to a virtual interface becomes full 
               then a new router lsa for the interface's transit area 
               needs to be generated - to set the "V" bit */
            if (IS_VIRTUAL_IFACE (pInterface))
            {
                pArea = GetFindAreaInCxt (pOspfCxt,
                                          &(pInterface->transitAreaId));
                if (pArea == NULL)
                {
                    return OSPF_FAILURE;
                }
                OlsGenerateLsa (pArea, ROUTER_LSA, &(pOspfCxt->rtrId),
                                (UINT1 *) pInterface->pArea);
            }

            break;

        case SIG_INTRA_AREA_CHANGE:
        case SIG_INTER_AREA_CHANGE:
            pRtEntry = (tRtEntry *) (VOID *) pPtr;

            if (!(pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
            {

                OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                          "Router is not an ABR\n");
                return OSPF_FAILURE;
            }

            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                if (TMO_SLL_First (&(pArea->ifsInArea)) != NULL)
                {
                    if (pRtEntry->u1DestType == DEST_NETWORK)
                    {
                        u1LsaType = NETWORK_SUM_LSA;
                    }
                    else
                    {
                        u1LsaType = ASBR_SUM_LSA;
                    }

                    if (((pLsaInfo = LsuSearchDatabase (u1LsaType,
                                                        &(pRtEntry->destId),
                                                        &(pOspfCxt->rtrId),
                                                        NULL,
                                                        (UINT1 *) pArea)) !=
                         NULL) && (pLsaInfo->u1LsaFlushed == OSPF_TRUE))
                    {
                        if (pLsaInfo->pLsaDesc != NULL)
                        {
                            pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
                            LsuDeleteNodeFromAllRxmtLst (pLsaInfo);
                        }
                        pLsaInfo->u1LsaFlushed = OSPF_FALSE;
                        OlsGenerateSummary (pArea, (tRtEntry *) (VOID *) pPtr);
                    }
                    else
                    {
                        OlsGenerateSummary (pArea, (tRtEntry *) (VOID *) pPtr);
                    }
                }
            }
            break;

        case SIG_NEW_AREA_ATTACHED:
            OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                      "No Action Required\n");
            break;

        case SIG_DEFAULT_SUMMARY:
            if (!(pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
            {

                OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                          "Router is not an ABR\n");
                return OSPF_FAILURE;
            }
            OlsBuildDefaultSummaryParam ((tArea *) (VOID *) pPtr,
                                         &defSummaryParam);
            OlsGenerateLsa ((tArea *) (VOID *) pPtr, NETWORK_SUM_LSA,
                            &gDefaultDest, (UINT1 *) &defSummaryParam);
            break;

        case SIG_INDICATION:
            if (!(pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
            {

                OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                          "Router is not an ABR\n");
                return OSPF_FAILURE;
            }
            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                if (pArea == (tArea *) (VOID *) pPtr)
                {
                    continue;
                }
                if ((pArea->u4DcBitResetLsaCount > pArea->u4IndicationLsaCount))
                {

                    /* area has non indication dc bit reset lsa */
                    continue;
                }

                if (TMO_SLL_Count (&(pArea->ifsInArea)) == 0)
                {
                    /* Area is not connected to any of the 
                     * Interfaces of the Router */
                    continue;
                }
                RBTreeWalk (pArea->pSummaryLsaRoot, RbWalkIndicationLsa, NULL,
                            (void *) &u1Flag);

                if (u1Flag != OSPF_TRUE)
                {
                    OlsGenerateIndicationLsa (pArea);
                }
            }
            break;

        case SIG_OPQ_LSA_GEN:
        case SIG_OPQ_LSA_NEXT_INSTANCE:

            pOpqLSAInfo = (tOpqLSAInfo *) (VOID *) pPtr;
            pAppInfo = APP_INFO_IN_CXT (pOspfCxt, pOpqLSAInfo->LsId[0]);

            if (pOpqLSAInfo->u1OpqLSAType == TYPE9_OPQ_LSA)
            {

                OlsGenerateLsa (pOspfCxt->pBackbone, TYPE9_OPQ_LSA,
                                &(pOpqLSAInfo->LsId), (UINT1 *) pOpqLSAInfo);
                COUNTER_OP (pAppInfo->Type9LSAGen, 1);
                return OSPF_SUCCESS;
            }

            if (pOpqLSAInfo->u1OpqLSAType == TYPE11_OPQ_LSA)
            {
                OlsGenerateLsa (pOspfCxt->pBackbone, TYPE11_OPQ_LSA,
                                &(pOpqLSAInfo->LsId), (UINT1 *) pOpqLSAInfo);
                COUNTER_OP (pAppInfo->Type11LSAGen, 1);
                return OSPF_SUCCESS;
            }
            else
            {

                OSPF_CRU_BMC_DWTOPDU (AreaId, pOpqLSAInfo->u4AreaID);
                pTmpArea = GetFindAreaInCxt (pOspfCxt, &AreaId);
                if (pTmpArea == NULL)
                {
                    return OSPF_FAILURE;
                }
                OlsGenerateLsa (pTmpArea, TYPE10_OPQ_LSA,
                                &(pOpqLSAInfo->LsId), (UINT1 *) pOpqLSAInfo);
                COUNTER_OP (pAppInfo->Type10LSAGen, 1);
            }
            break;

        default:
            break;

    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: OlsSignalLsaRegenInCxt\n");
    return OSPF_SUCCESS;
}

/******************************************************************************

Function        :   OlsGenerateSummaryToArea

Description     :   originates summary of all routes to the newly attatched area

Input           :
                    pNewArea  : area to which the summarization to be done 

Output          :   None

Returns         :   None

******************************************************************************/
PUBLIC VOID
OlsGenerateSummaryToArea (tArea * pNewArea)
{

    tRtEntry           *pRtEntry;

    OSPF_TRC (OSPF_FN_ENTRY, pNewArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGenerateSummaryToArea\n");

    OSPF_TRC1 (OSPF_LSU_TRC, pNewArea->pOspfCxt->u4OspfCxtId,
               "All paths are to be summarized into area : %x.",
               OSPF_CRU_BMC_DWFROMPDU (pNewArea->areaId));

    TMO_SLL_Scan (&(pNewArea->pOspfCxt->pOspfRt->routesList), pRtEntry,
                  tRtEntry *)
    {
        OlsGenerateSummary (pNewArea, pRtEntry);
    }

    return;
}

/*****************************************************************************/
/* Function        :   OlsGenerateASSummaryToArea                            */
/*                                                                           */
/* Description     :   Originates summary of ASBR routes to the newly        */
/*                     attatched area.                                       */
/*                                                                           */
/* Input           :   pNewArea  : area to which the summarization to be done*/
/*                                                                           */
/* Output          :   None                                                  */
/*                                                                           */
/* Returns         :   None                                                  */
/*****************************************************************************/
PUBLIC VOID
OlsGenerateASSummaryToArea (tArea * pNewArea)
{

    tRtEntry           *pRtEntry;

    OSPF_TRC (OSPF_FN_ENTRY, pNewArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGenerateASSummaryToArea\n");

    TMO_SLL_Scan (&(pNewArea->pOspfCxt->pOspfRt->routesList), pRtEntry,
                  tRtEntry *)
    {
        if (pRtEntry->u1DestType == DEST_AS_BOUNDARY)
        {
            OlsGenerateSummary (pNewArea, pRtEntry);
        }
        else if (pRtEntry->u1DestType == DEST_NETWORK)
        {
            /* Always ASBR/ABR entries will be present at the starting of the 
             * list. So if the route entry is NETWORK, then no need to 
             * scan the list further. */
            return;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pNewArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsGenerateASSummaryToArea\n");
}

/*****************************************************************************/
/* Function        : OlsGenerateSummary                                      */
/* Reference       : RFC-1247 section 12.4.3                                 */
/* Description     : Generates Summary LSA for the given routing table       */
/*                   entry into the specified area.                          */
/* Input           : pArea     : Area to which the summarization to be done  */
/*                   pRtEntry  : Ptr to route entry structure which is to    */
/*                                   be summarized.                          */
/* Output          : None.                                                   */
/* Returns         : None.                                                   */
/*****************************************************************************/
PUBLIC VOID
OlsGenerateSummary (tArea * pArea, tRtEntry * pRtEntry)
{

    tSummaryParam       summaryParam;
    tPath              *pPath;
    tArea              *pTmpArea;
    tIPADDR             lsId;
    tAreaId            *AreaId;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGenerateSummary\n");
    OSPF_TRC4 (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "area: %x, path type: %d, destId: %x,destMask: %x",
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId),
               GET_PATH_TYPE (pRtEntry, TOS_0),
               OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId),
               pRtEntry->u4IpAddrMask);
    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        return;
    }
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }

    /* Proceed only if the stub area needs Summary LSAs */
    if (IS_STUB_AREA (pArea) && !IS_SEND_AREA_SUMMARY (pArea))
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Area is stub area - does not allow summarization\n");
        return;
    }

    /* Proceed only if the NSSA area needs Summary LSAs */
    if ((pArea->u4AreaType == NSSA_AREA) && !IS_SEND_AREA_SUMMARY (pArea))
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Area is NSSA area - does not allow summarization\n");
        return;
    }

    /* ASBR summary LSAs are not generated in NSSA area */
    if (IS_DEST_ASBR (pRtEntry) && (pArea->u4AreaType == NSSA_AREA))
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Area is NSSA area - does not allow ASBR summarization\n");
        return;
    }

    /* ASBR internal to NSSA are not summarized in other areas */
    if (IS_DEST_ASBR (pRtEntry))
    {
        pPath = GET_TOS_PATH (pRtEntry, TOS_0);
        if (pPath != NULL)
        {
            if ((pTmpArea = GetFindAreaInCxt (pArea->pOspfCxt,
                                              &(pPath->areaId))) != NULL)
            {
                if (pTmpArea->u4AreaType == NSSA_AREA)
                {
                    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                              "Internal NSSA ASBR - does not allow"
                              "ASBR summarization\n");
                    return;
                }
            }
        }
    }

    /* Skip routes to Area Border Routers */
    if (IS_DEST_ABR (pRtEntry))
    {
        return;
    }

    /* Consider only INTER-AREA and INTRA-AREA paths */
    if (IS_TYPE_1_EXT_PATH (pRtEntry) || IS_TYPE_2_EXT_PATH (pRtEntry))
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Path type is TYPE_1_EXT or TYPE_2_EXT.\n");
        return;
    }

    if ((OlsBuildSummaryParam (pArea, pRtEntry, &summaryParam) == OSPF_FAILURE))
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Path or the Next hop is associated with same area.");
        return;
    }

    if (IS_DEST_ASBR (pRtEntry) && !IS_STUB_AREA (pArea))
    {
        /* Ref : RFC 3509 Section 2.2 Point 3 */
        if ((pArea->pOspfCxt->u4ABRType != STANDARD_ABR) &&
            !(pArea->pOspfCxt->pBackbone->u4FullNbrCount > 0) &&
            !(IS_INTRA_AREA_PATH (pRtEntry)))
        {
            return;
        }
        OlsGenerateLsa (pArea, ASBR_SUM_LSA, &(pRtEntry->destId),
                        (UINT1 *) &summaryParam);
    }
    else if (IS_DEST_NETWORK (pRtEntry) && IS_INTER_AREA_PATH (pRtEntry))
    {
        /* Ref : RFC 3509 Section 2.2 Point 3 */
        if ((pArea->pOspfCxt->u4ABRType != STANDARD_ABR) &&
            !(pArea->pOspfCxt->pBackbone->u4FullNbrCount > 0))
        {
            return;
        }
        IP_ADDR_COPY (lsId, pRtEntry->destId);
        OlsModifyLsId (pArea, NETWORK_SUM_LSA, &(lsId),
                       (UINT1 *) &summaryParam);
        OlsGenerateLsa (pArea, NETWORK_SUM_LSA, &(lsId),
                        (UINT1 *) &summaryParam);
    }
    else if (IS_DEST_NETWORK (pRtEntry) && IS_INTRA_AREA_PATH (pRtEntry))
    {
        /* Backbone info is not condensed into transit areas */
        AreaId = OlsGetPathAreaId (pRtEntry);
        if (IS_AREA_TRANSIT_CAPABLE (pArea) &&
            (TMO_SLL_Count (&(pRtEntry->aTosPath[TOS_0])) != 0) &&
            ((AreaId != NULL) && (IS_NULL_IP_ADDR (*AreaId))))
        {
            IP_ADDR_COPY (lsId, pRtEntry->destId);
            OlsModifyLsId (pArea, NETWORK_SUM_LSA, &(lsId),
                           (UINT1 *) &summaryParam);

            OlsGenerateLsa (pArea, NETWORK_SUM_LSA, &(lsId),
                            (UINT1 *) &summaryParam);
        }
        else
        {
            if (OlsCondenseSummary (pArea, pRtEntry) == OSPF_FAILURE)
            {
                /* 
                 * if the condensed summary lsa was not generated
                 * generate network lsa for that network alone
                 */
                IP_ADDR_COPY (lsId, pRtEntry->destId);
                OlsModifyLsId (pArea, NETWORK_SUM_LSA, &(lsId),
                               (UINT1 *) &summaryParam);
                OlsGenerateLsa (pArea, NETWORK_SUM_LSA,
                                &(lsId), (UINT1 *) &summaryParam);
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsGenerateSummary\n");
}

/******************************************************************************

Function        :   OlsCondenseSummary

Description     :   This procedure condenses the info related to the specified
                    routing table entry into the specified area

Input           :
                    pArea      :   area to which the summarization to be done 
                    pRtEntry  :   ptr to route entry structure which is to 
                                    be summarized

Output          :   None

Returns         :   SUCCESS/FAILURE

******************************************************************************/
PRIVATE INT4
OlsCondenseSummary (tArea * pArea, tRtEntry * pRtEntry)
{
    tAddrRange         *pAddrRange;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsCondenseSummary\n");

    OSPF_TRC4 (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "area : %x, path type : %d, destId : %x,"
               "destMask : %x.", OSPF_CRU_BMC_DWFROMPDU (pArea->areaId),
               GET_PATH_TYPE (pRtEntry, TOS_0),
               OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId),
               pRtEntry->u4IpAddrMask);

    if ((pAddrRange = UtilFindAssoAreaAddrRangeInCxt (pArea->pOspfCxt,
                                                      &(pRtEntry->destId),
                                                      &(GET_PATH_AREA_ID
                                                        (pRtEntry)))) == NULL)
    {

        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  " Dest addr does not fall into any of the configured addr ranges");
        return OSPF_FAILURE;
    }

    if (IS_NOT_ADVERTISABLE_ADDR_RANGE (pAddrRange))
    {

        OSPF_TRC1 (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                   "addr range %x has dont advertise bStatus",
                   OSPF_CRU_BMC_DWFROMPDU (pAddrRange->ipAddr));
        return SUCCESS;
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsCondenseSummary\n");
    return SUCCESS;
}

/******************************************************************************

Function        :   OlsGenerateLsa

Description     :   generates lsa if min_ls_interval_expired, else updates
                    lsa_changed flag. 

Input           :
                    pArea          :   area to which lsa is generated
                    u1InternalType:   internal type of lsa
                    pLsId         :   link state id
                    pPtr           :   pointer to the structure associated 
                                        with the lsa

Output          :   None

Returns         :   None

******************************************************************************/

PUBLIC VOID
OlsGenerateLsa (tArea * pArea,
                UINT1 u1InternalType, tLINKSTATEID * pLsId, UINT1 *pPtr)
{

    UINT1               u1LsaType = 0;
    UINT1               u1TrapFlag = OSPF_FALSE;
    UINT1               u1NewDescFlag = OSPF_FALSE;
    tLsaDesc           *pLsaDesc;
    UINT1              *pLsa;
    UINT2               u2LsaLen;
    tLSASEQNUM          seqNum = 0;
    tLsaTrapInfo        trapOrgLsa;
    tAsExtAddrRange    *pAsExtRange = NULL;
    tIPADDRMASK         destIpAddrMask;

    UINT1              *pTmpPtr;
    UINT1               u1LsaChngdFlag = OSPF_TRUE;
    tOpqLSAInfo        *pOpqLSAInfo;
    tInterface         *pInterface = NULL;
    tIPADDR             IfaceID;
    tArea              *pType7Area;
    tLsaInfo           *pType7LsaInfo;
    tCRU_BUF_CHAIN_HEADER *pLsuPkt;
    UINT2               u2LsaAge;
    INT4                i4RetCode;
    UINT1               u1LsaRegenNeededFlag = OSPF_TRUE;

    if (pArea == NULL)
    {
        return;
    }
    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGenerateLsa\n");

    OSPF_TRC2 (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "New LSA to be generated for lsa_type: %d,"
               "linkStateId: %x\n", u1InternalType,
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pLsId));
    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* LSA cannot be regenerated during GR restart */
        /* In case of unplanned graceful restart, grace LSA should be sent */
        if (!((pArea->pOspfCxt->u1RestartStatus == OSPF_RESTART_UNPLANNED) &&
              (u1InternalType == TYPE9_OPQ_LSA) &&
              (*pLsId[0] == GRACE_LSA_OPQ_TYPE)))
        {
            return;
        }
    }
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can generate LSA */
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }
    if ((pPtr == NULL) && (u1InternalType != ROUTER_LSA))
    {
        return;
    }

    if ((i4RetCode =
         OlsIsEligibleToGenLsaInCxt (pArea, u1InternalType)) == OSPF_FAILURE)
    {
        return;
    }

    u1LsaType = ((u1InternalType == DEFAULT_NETWORK_SUM_LSA) ||
                 (u1InternalType == COND_NETWORK_SUM_LSA)) ?
        NETWORK_SUM_LSA : u1InternalType;

    if (u1InternalType == INDICATION_LSA)
    {
        u1LsaType = ASBR_SUM_LSA;
    }

    if (u1InternalType == COND_NSSA_LSA || u1InternalType == DEFAULT_NSSA_LSA)
    {
        u1LsaType = NSSA_LSA;
    }

    if (u1InternalType == AS_TRNSLTD_EXT_RNG_LSA ||
        u1InternalType == AS_TRNSLTD_EXT_LSA ||
        u1InternalType == COND_AS_EXT_LSA)
    {
        u1LsaType = AS_EXT_LSA;
    }
    if ((u1LsaType == TYPE11_OPQ_LSA) || (u1LsaType == AS_EXT_LSA))
    {
        pTmpPtr = NULL;
    }
    else if (u1LsaType == TYPE9_OPQ_LSA)
    {
        pOpqLSAInfo = (tOpqLSAInfo *) (VOID *) pPtr;
        if (pOpqLSAInfo == NULL)
        {
            OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "Opq Lsa Info pointer NULL, Don't generate LSA.");
            return;
        }
        OSPF_CRU_BMC_DWTOPDU (IfaceID, pOpqLSAInfo->u4IfaceID);
        pInterface =
            GetFindIfInCxt (pOpqLSAInfo->pOspfCxt, IfaceID,
                            pOpqLSAInfo->u4AddrlessIf);
        if (pInterface == NULL)
        {
            return;
        }
        pTmpPtr = (UINT1 *) pInterface;
    }
    else
    {
        pTmpPtr = (UINT1 *) pArea;
    }

    /* RFC 5250 - Type 11 Opaque Lsas should be generated only by ASBR */
    if (u1LsaType == TYPE11_OPQ_LSA)
    {
        if (pArea->pOspfCxt->bAsBdrRtr != TRUE)
        {
            OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "Router is not an ASBR; Type 11 LSAs should be generated by ASBR\n");
            return;
        }
    }

    if ((pLsaDesc = OlsGetLsaDescInCxt (pArea->pOspfCxt, u1LsaType,
                                        pLsId, pTmpPtr,
                                        &u1NewDescFlag)) == NULL)
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Cannot find lsa desc for this advt.");
        return;
    }

    if (pLsaDesc->pLsaInfo != NULL)
    {
        TmrDeleteTimer (&(pLsaDesc->pLsaInfo->lsaAgingTimer));
    }
    pLsaDesc->u1InternalLsaType = u1InternalType;

    /* implementation specific validity conditions */
    if (pLsaDesc->u1SeqNumWrapAround == OSPF_TRUE)
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "This advt has been flooded out for seq number wrap around");
        if (u1NewDescFlag == OSPF_TRUE)
        {
            LSA_DESC_FREE (pLsaDesc);
        }
        return;
    }

    /* add associated primitive */
    if ((u1InternalType == NETWORK_SUM_LSA) ||
        (u1InternalType == ASBR_SUM_LSA) || (u1InternalType == INDICATION_LSA))
    {
        if ((pLsaDesc->pAssoPrimitive == NULL) &&
            (SUM_PARAM_ALLOC (pLsaDesc->pAssoPrimitive) == NULL))
        {
            if ((pLsaDesc->pLsaInfo == NULL) && (pLsaDesc->u4LsaGenTime == 0))
            {
                LSA_DESC_FREE (pLsaDesc);
            }
            OSPF_TRC (OS_RESOURCE_TRC
                      | OSPF_LSU_TRC | OSPF_CRITICAL_TRC,
                      pArea->pOspfCxt->u4OspfCxtId,
                      "Summary Param Alloc Failure\n");
            KW_FALSEPOSITIVE_FIX (pLsaDesc);
            return;
        }
        else
        {
            OS_MEM_CPY (pLsaDesc->pAssoPrimitive, pPtr, sizeof (tSummaryParam));
        }
    }
    else
    {
        pLsaDesc->pAssoPrimitive = pPtr;
    }
    if (pLsaDesc->u1MinLsaIntervalExpired != OSPF_TRUE)
    {
        pLsaDesc->u1LsaChanged = OSPF_TRUE;
        if ((u1InternalType == AS_TRNSLTD_EXT_LSA) ||
            (u1InternalType == AS_TRNSLTD_EXT_RNG_LSA))
        {
            if (pLsaDesc->pLsaInfo != NULL)
            {
                pLsaDesc->pLsaInfo->u1TrnsltType5 = OSPF_TRUE;
            }
        }
        /* sync up the change in the LsaDesc and assoc primitive 
         * to ensure the changes are reflected in the standby.
         * In case switchover happens before the min LS timer expiry. 
         * The new LSA can be re generated in the new active.
         * using the synced up data.
         */

        if (pLsaDesc->pLsaInfo != NULL)
        {
            if (OsRmDynLsSendLsa (pLsaDesc->pLsaInfo) == OSIX_FAILURE)
            {
                OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                               pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                               "Failed to sync the originated LSU to the standby node "
                               "LSA Type : %d, Link state id : %x "
                               "Advertising router id : %x\r\n",
                               pLsaDesc->pLsaInfo->lsaId.u1LsaType,
                               OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                       lsaId.linkStateId),
                               OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                       lsaId.advRtrId));
                if (u1NewDescFlag == OSPF_TRUE)
                {
                    LSA_DESC_FREE (pLsaDesc);
                }
            }
        }
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "MinLsInterval timer has not fired for this lsa\n");
        KW_FALSEPOSITIVE_FIX (pLsaDesc);
        return;

    }

    if (pLsaDesc->pLsaInfo == NULL)
    {
        /* this is the first time that it generates the LSA */
        seqNum = (INT4) MIN_SEQ_NUM;
        OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "FUNC :OlsGenerateLsa, seqNum %d\n", seqNum);

    }
    else
    {
        if (pLsaDesc->pLsaInfo->lsaSeqNum == MAX_SEQ_NUM)
        {
            /* seq num wraps around */
            if (IS_MAX_AGE (pLsaDesc->pLsaInfo->u2LsaAge))
            {
                /* 
                 * the max_seq_num lsa has been flushed and new instance
                 * has to be generated
                 */
                seqNum = (INT4) MIN_SEQ_NUM;
                OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                           "lsaSeqNum = %d\n", pLsaDesc->pLsaInfo->lsaSeqNum);
                OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                           "FUNC:OlsGenerateLsa, LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                           pLsaDesc->pLsaInfo->lsaId.u1LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->lsaId.
                                                   linkStateId),
                           pLsaDesc->pLsaInfo->u2LsaAge, seqNum);

            }
            else
            {
                /* the max_seq_num lsa has to be flushed */
                seqNum = pLsaDesc->pLsaInfo->lsaSeqNum;
                pLsaDesc->u1SeqNumWrapAround = OSPF_TRUE;
                OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                           "lsaSeqNum = %d\n", pLsaDesc->pLsaInfo->lsaSeqNum);
                OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                           "FUNC:OlsGenerateLsa, LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                           pLsaDesc->pLsaInfo->lsaId.u1LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->lsaId.
                                                   linkStateId),
                           pLsaDesc->pLsaInfo->u2LsaAge, seqNum);

            }
        }
        else
        {
            seqNum = (pLsaDesc->pLsaInfo->lsaSeqNum + 1);
            OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "lsaSeqNum = %d\n", pLsaDesc->pLsaInfo->lsaSeqNum);
            OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "FUNC:OlsGenerateLsa, LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                       pLsaDesc->pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->lsaId.
                                               linkStateId),
                       pLsaDesc->pLsaInfo->u2LsaAge, seqNum);
        }
    }

    /* Construct LSA */
    switch (u1InternalType)
    {
        case ROUTER_LSA:
            pLsa = OlsConstructRtrLsa (pArea, seqNum, &u2LsaLen);
            break;

        case NETWORK_LSA:
            pLsa = OlsConstructNetworkLsa ((tInterface *) (VOID *) pPtr, seqNum,
                                           &u2LsaLen);
            break;

        case NETWORK_SUM_LSA:
        case COND_NETWORK_SUM_LSA:
        case DEFAULT_NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case INDICATION_LSA:
            pLsa = OlsConstructSummaryLsa (pArea, u1InternalType, pPtr,
                                           seqNum, &u2LsaLen, pLsId);
            break;

        case AS_EXT_LSA:
        case AS_TRNSLTD_EXT_LSA:
        case AS_TRNSLTD_EXT_RNG_LSA:
        case COND_AS_EXT_LSA:
            pLsa =
                OlsConstructAsExtLsaInCxt (pArea->pOspfCxt, pPtr,
                                           u1InternalType,
                                           seqNum, &u2LsaLen, pLsId);
            break;

        case NSSA_LSA:
        case COND_NSSA_LSA:
        case DEFAULT_NSSA_LSA:
            pLsa =
                OlsConstructType7Lsa (pPtr, u1InternalType, pArea, seqNum,
                                      &u2LsaLen, pLsId);
            break;

        case TYPE9_OPQ_LSA:
        case TYPE10_OPQ_LSA:
        case TYPE11_OPQ_LSA:
            pLsa = OlsConstructOpaqueLsa ((tOpqLSAInfo *) (VOID *) pPtr, seqNum,
                                          &u2LsaLen);
            break;

        default:
            pLsa = (UINT1 *) NULL;
            break;
    }

    /* Check for failure in lsa construction */
    if (pLsa == NULL)
    {
        if (pLsaDesc->pLsaInfo == NULL)
        {
            if ((TMO_DLL_Find
                 (&(pArea->pOspfCxt->origLsaDescLst),
                  &(pLsaDesc->lsaDescNode)) != 0)
                && (u1InternalType == NETWORK_LSA))
            {
                TMO_DLL_Delete (&(pArea->pOspfCxt->origLsaDescLst),
                                &(pLsaDesc->lsaDescNode));
                LSA_DESC_FREE (pLsaDesc);
            }
            else
            {
                LSA_DESC_FREE (pLsaDesc);
            }
        }
        OSPF_TRC (OSPF_LSU_TRC | OSPF_CRITICAL_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "LSA construction failure.");
        KW_FALSEPOSITIVE_FIX (pLsaDesc);
        return;
    }

    /* If new advt allocate lsa_info and add to hash table */
    if (pLsaDesc->pLsaInfo == NULL)
    {
        if ((pLsaDesc->pLsaInfo =
             LsuAddToLsdb (pArea, u1LsaType,
                           pLsId, &(pArea->pOspfCxt->rtrId),
                           (tInterface *) (VOID *) pTmpPtr, NEW_LSA, pLsa))
            == NULL)
        {
            LSA_FREE (u1LsaType, pLsa);
            LSA_DESC_FREE (pLsaDesc);
            OSPF_TRC (OSPF_LSU_TRC
                      | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                      pArea->pOspfCxt->u4OspfCxtId,
                      "lsa_info allocation failure");
            return;
        }
        pLsaDesc->pLsaInfo->pLsaDesc = pLsaDesc;
        TMO_DLL_Add (&(pArea->pOspfCxt->origLsaDescLst),
                     &(pLsaDesc->lsaDescNode));
    }

    if (pLsaDesc->pLsaInfo->pLsa != NULL)
    {
        u1LsaChngdFlag =
            (UINT1) LsuCheckActualChange (pLsaDesc->pLsaInfo->pLsa, pLsa);
    }

    if (((pLsaDesc->pLsaInfo->pLsa != NULL) && (pLsa != NULL)) &&
        (IS_TRAP_ENABLED_IN_CXT (ORIGINATE_LSA_TRAP, pArea->pOspfCxt) &&
         !(pArea->pOspfCxt->u1SwPeriodsElapsed < 16)))
    {
        u1TrapFlag =
            (UINT1) OlsTrapCheckLsuChange (pLsaDesc->pLsaInfo->pLsa, pLsa);
    }

    /* Flood out the lsa */
    if (LsuInstallLsa (pLsa, pLsaDesc->pLsaInfo) == OSPF_FAILURE)
    {
        LSA_FREE (u1LsaType, pLsa);
        LSA_DESC_FREE (pLsaDesc);
        return;
    }

    if ((u1InternalType == NSSA_LSA) || (u1InternalType == COND_NSSA_LSA))
    {
        /* Handling functionally Equ Type 7 */
        if (OlsIsEligibleToGenNssaLsa (pLsaDesc->pLsaInfo,
                                       pLsaDesc->pLsaInfo->pArea) == OSPF_FALSE)
        {
            /* Already added in the list. Should be removed */
            LsuDeleteLsaFromDatabase (pLsaDesc->pLsaInfo, OSPF_TRUE);
            OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "cant generate ASE LSA, since"
                      "another one exists with same functionality\n");
            return;
        }
    }

    if ((u1InternalType == AS_EXT_LSA) || (u1InternalType == COND_AS_EXT_LSA))
    {
        pLsaDesc->pLsaInfo->u1TrnsltType5 = REDISTRIBUTED_TYPE5;
    }
    else if ((u1InternalType == AS_TRNSLTD_EXT_LSA) ||
             (u1InternalType == AS_TRNSLTD_EXT_RNG_LSA))
    {
        pLsaDesc->pLsaInfo->u1TrnsltType5 = OSPF_TRUE;
    }
    if ((u1InternalType == AS_EXT_LSA) || (u1InternalType == COND_AS_EXT_LSA) ||
        (u1InternalType == AS_TRNSLTD_EXT_LSA) ||
        (u1InternalType == AS_TRNSLTD_EXT_RNG_LSA))
    {
        /* Send the originated LSA to the standby node */
        if (OsRmDynLsSendLsa (pLsaDesc->pLsaInfo) == OSIX_FAILURE)
        {
            OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                           pLsaDesc->pLsaInfo->pOspfCxt->u4OspfCxtId,
                           "Failed to sync the originated LSU to the standby node "
                           "LSA Type : %d, Link state id : %x "
                           "Advertising router id : %x\r\n",
                           pLsaDesc->pLsaInfo->lsaId.u1LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                   lsaId.linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                   lsaId.advRtrId));
        }
    }

    /* Triggering Translation of self originated Type 7 LSA */
    if ((u1InternalType == NSSA_LSA) ||
        (u1InternalType == COND_NSSA_LSA) ||
        (u1InternalType == DEFAULT_NSSA_LSA))
    {
        NssaType7LsaTranslation (pLsaDesc->pLsaInfo->pArea, pLsaDesc->pLsaInfo);
        NssaAdvertiseType7Rngs (pLsaDesc->pLsaInfo->pArea);
    }

    if ((u1InternalType == AS_EXT_LSA) || (u1InternalType == COND_AS_EXT_LSA))
    {
        TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pType7Area, tArea *)
        {
            if (pType7Area->u4AreaType == NSSA_AREA)
            {
                pType7LsaInfo =
                    LsuSearchDatabase (NSSA_LSA,
                                       &(pLsaDesc->pLsaInfo->lsaId.linkStateId),
                                       &(pArea->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pType7Area);
                if (pType7LsaInfo != NULL)
                {
                    if (u1InternalType == COND_AS_EXT_LSA)
                    {
                        pAsExtRange = (tAsExtAddrRange *) (VOID *) pPtr;
                        OS_MEM_CPY (destIpAddrMask,
                                    (UINT1 *) (pType7LsaInfo->pLsa +
                                               LS_HEADER_SIZE),
                                    MAX_IP_ADDR_LEN);

                        if (UtilIpAddrComp
                            (destIpAddrMask,
                             pAsExtRange->ipAddrMask) != OSPF_EQUAL)
                        {
                            /* Regeneration of LSA is not needed for the case network being same
                             * and different in masks */
                            u1LsaRegenNeededFlag = OSPF_FALSE;
                        }
                    }
                    if (u1LsaRegenNeededFlag == OSPF_TRUE)
                    {
                        pType7LsaInfo->pLsaDesc->u1MinLsaIntervalExpired
                            = OSPF_TRUE;
                        OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                                   pType7LsaInfo->pLsaDesc->pLsaInfo->pOspfCxt->
                                   u4OspfCxtId,
                                   "FUNC:OlsGenerateLsa, LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                                   pType7LsaInfo->pLsaDesc->pLsaInfo->lsaId.
                                   u1LsaType,
                                   OSPF_CRU_BMC_DWFROMPDU (pType7LsaInfo->
                                                           pLsaDesc->pLsaInfo->
                                                           lsaId.linkStateId),
                                   pType7LsaInfo->pLsaDesc->pLsaInfo->u2LsaAge,
                                   pType7LsaInfo->pLsaDesc->pLsaInfo->
                                   lsaSeqNum);

                        OlsSignalLsaRegenInCxt (pType7LsaInfo->pOspfCxt,
                                                SIG_NEXT_INSTANCE,
                                                (UINT1 *) pType7LsaInfo->
                                                pLsaDesc);
                    }

                }
            }
        }
    }

    if (IS_TRAP_ENABLED_IN_CXT (ORIGINATE_LSA_TRAP, pArea->pOspfCxt) &&
        !(pArea->pOspfCxt->u1SwPeriodsElapsed < 16)
        && (u1TrapFlag == OSPF_TRUE))
    {
        if (u1LsaType == AS_EXT_LSA)
        {
            SET_NULL_IP_ADDR (trapOrgLsa.lsdbAreaId);
        }
        else
        {
            IP_ADDR_COPY (trapOrgLsa.lsdbAreaId, pArea->areaId);
        }

        trapOrgLsa.u1LsdbType = u1LsaType;
        IP_ADDR_COPY (trapOrgLsa.lsdbLsid, pLsId);
        IP_ADDR_COPY (trapOrgLsa.lsdbRtrId, pArea->pOspfCxt->rtrId);
        IP_ADDR_COPY (trapOrgLsa.rtrId, pArea->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pArea->pOspfCxt, ORIGINATE_LSA_TRAP, &trapOrgLsa);
    }

    /* If the instance is in unplanned restart state and the LSA
     * to be transmitted is self-originated grace LSA, then
     * transmit the LSA to All SPF routers */
    if ((pArea->pOspfCxt->u1RestartStatus == OSPF_RESTART_UNPLANNED) && (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaDesc->pLsaInfo)) && (pLsaDesc->pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) && (pLsaDesc->pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE) && (pInterface != NULL) &&    /*Klocwork */
        ((pInterface->u1NetworkType == IF_BROADCAST) ||
         (pInterface->u1NetworkType == IF_VIRTUAL) ||
         (pInterface->u1NetworkType == IF_PTOP)))
    {
        /* Add the LSA to the ls update packet in the interface
         * and transmit to all SPF routers */
        pInterface->lsUpdate.u2CurrentLen = 0;
        if ((pInterface->lsUpdate.pLsuPkt =
             UtilOsMsgAlloc ((pLsaDesc->pLsaInfo->u2LsaLen + OS_HEADER_SIZE
                              + IP_HEADER_SIZE + LSU_FIXED_PORTION_SIZE +
                              DIGEST_LEN_IF_CRYPT (pInterface)))) == NULL)
        {
            return;
        }
        pInterface->lsUpdate.u2CurrentLen =
            OS_HEADER_SIZE + LSU_FIXED_PORTION_SIZE;
        pInterface->lsUpdate.u4LsaCount = 1;

        pLsuPkt = pInterface->lsUpdate.pLsuPkt;
        OSPF_CRU_BMC_ASSIGN_STRING (pLsuPkt, pLsa,
                                    pInterface->lsUpdate.u2CurrentLen,
                                    u2LsaLen);
        u2LsaAge = OSPF_CRU_BMC_WFROMPDU ((pLsa + AGE_OFFSET_IN_LS_HEADER));
        u2LsaAge = (UINT2) (INC_LSA_AGE (u2LsaAge, pInterface->u2IfTransDelay));

        OSPF_CRU_BMC_ASSIGN_2_BYTE (pLsuPkt,
                                    ((UINT4)
                                     (pInterface->lsUpdate.u2CurrentLen +
                                      AGE_OFFSET_IN_LS_HEADER)), u2LsaAge);

        OSPF_CRU_BMC_ASSIGN_4_BYTE (pInterface->lsUpdate.pLsuPkt,
                                    OS_HEADER_SIZE,
                                    pInterface->lsUpdate.u4LsaCount);

        pInterface->lsUpdate.u2CurrentLen = (UINT2)
            (pInterface->lsUpdate.u2CurrentLen + u2LsaLen);

        UtilConstructHdr (pInterface, pInterface->lsUpdate.pLsuPkt,
                          LS_UPDATE_PKT, pInterface->lsUpdate.u2CurrentLen);

        /* Timestamp database copy for each LSA transmitted in LSU packet */
        LsuPutTxTime (pInterface->lsUpdate.pLsuPkt, pInterface);

        PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                    (UINT2) (pInterface->lsUpdate.u2CurrentLen +
                             DIGEST_LEN_IF_CRYPT (pInterface)),
                    pInterface, &gAllSpfRtrs, RELEASE);

        pInterface->lsUpdate.pLsuPkt = NULL;
        LsuClearUpdate (&pInterface->lsUpdate);

    }
    else
    {
        LsuFloodOut (pLsa, u2LsaLen, NULL, pArea, u1LsaChngdFlag,
                     pLsaDesc->pLsaInfo->pInterface);

        LsuSendAllFloodUpdatesInCxt (pArea->pOspfCxt);
    }
    COUNTER_OP (pArea->pOspfCxt->u4OriginateNewLsa, 1);

    OSPF_TRC3 (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "Lsa originated type: %d, link state id: %x, adv rtr id: %x\n",
               pLsaDesc->pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->
                                       lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaDesc->pLsaInfo->lsaId.advRtrId));

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "LSA Generation Complete\n");
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsGenerateLsa\n");
    KW_FALSEPOSITIVE_FIX (pLsaDesc);
}

/******************************************************************************
                                                                          
Function        :   OlsConstructRtrLsa                                      
                                                                               
Description     :   constructs a router LSA.                                   
                                                                               
Input           :   
                    pArea      :   pointer to area to which the router LSA
                                    is to be generated
                    seqNum     :   sequence number                      
                                                                               
Output          :   
                    pLen       :   length of constructed LSA
                                                                               
Returns         :   pointer to router LSA, if successfully constructed
                    NULL,                   otherwise
                                                                          
******************************************************************************/

PRIVATE UINT1      *
OlsConstructRtrLsa (tArea * pArea, tLSASEQNUM seqNum, UINT2 *pLen)
{

    tTMO_SLL_NODE      *pIfNode;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr = NULL;
    tHost              *pHost;
    UINT1              *pTempRtrLsa = NULL;
    UINT1              *pRtrLsa = NULL;
    UINT1              *pCurrent;
    UINT1              *pVebField;
    UINT1              *pLinkCount;
    UINT2               u2LinkCount = 0;
    UINT1               u1LinkType;
    tIPADDR             netNum = { 0 };
    tOPTIONS            options = { 0 };
    INT2                i2Index;
    tIPADDR             ipAux;
    tIPADDR             ipSubnetAddr;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               u1Tos = 0;
    UINT1              *pTosCount;
    UINT1               u1TosCount = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsConstructRtrLsa\n");
    OSPF_TRC1 (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "Router LSA to be constructed for area : %x.\n",
               OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));

    LSA_ALLOC (ROUTER_LSA, pTempRtrLsa, MAX_LSA_SIZE4096 - 1);

    if (pTempRtrLsa == NULL)
    {
        OSPF_TRC (OSPF_LSU_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "LSA_ALLOC FAILURE\n");
        OSPF_TRC1 (OSPF_CRITICAL_TRC | OS_RESOURCE_TRC | OSPF_LSU_TRC |
                   OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                   pArea->pOspfCxt->u4OspfCxtId,
                   "Rtr LSA Alloc Failure for Size %d\n",
                   (MAX_LSA_SIZE4096 - 1));
        return NULL;
    }

    pCurrent = pTempRtrLsa;

    OS_MEM_SET (pTempRtrLsa, 0, LS_HEADER_SIZE);

    /* Leave space for header */
    pCurrent += LS_HEADER_SIZE;

    /* V bit alone to be filled up later */
    pVebField = pCurrent;

    options =
        (UINT1) (((pArea->pOspfCxt->bAreaBdrRtr ==
                   OSPF_TRUE) ? ABR_MASK : 0) | ((pArea->pOspfCxt->bAsBdrRtr ==
                                                  OSPF_TRUE)
                                                 && !IS_STUB_AREA (pArea) ?
                                                 ASBR_MASK : 0));
    if ((pArea->u4AreaType == NSSA_AREA)
        && (pArea->u1NssaTrnsltrRole == TRNSLTR_ROLE_ALWAYS))
        options |= NT_BIT_MASK;

    if ((pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE) &&
        (pArea->pOspfCxt->u4NssaAreaCount > 0) && (!IS_STUB_AREA (pArea))
        && (pArea->u4AreaType != NSSA_AREA))
        options |= ASBR_MASK;

    LADD1BYTE (pCurrent, options);

    /* Field reset to be reused */
    options = 0;

    /* unused */
    LADD1BYTE (pCurrent, 0);

    pLinkCount = pCurrent;

    /* # links to be filled later */
    LADD2BYTE (pCurrent, 0);

    u2LinkCount = 0;

    /* Setting the Option field of RTR LSA */
#ifdef TOS_SUPPORT
    options = ((pArea->pOspfCxt->bTosCapability == OSPF_TRUE) ? T_BIT_MASK : 0);
#endif /* TOS_SUPPORT */

    options = (IS_STUB_AREA (pArea) ? (options | 0) : (options | E_BIT_MASK));
    if (pArea->pOspfCxt->bDemandExtension == OSPF_TRUE)
    {
        options = options | DC_BIT_MASK;
    }

    /* Setting the V_BIT in the RTR LSA */
    TMO_SLL_Scan (&(pArea->pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (IS_EQUAL_ADDR (pInterface->transitAreaId, pArea->areaId))
        {
            if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if (pNbr->u1NsmState == NBRS_FULL)
                {
                    *pVebField = *pVebField | V_BIT_MASK;
                    break;
                }
            }
        }
    }

    TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pIfNode);
        switch (pInterface->u1IsmState)
        {
            case IFS_DOWN:
                break;

            case IFS_PTOP:
                if ((pInterface->u1NetworkType == IF_PTOP) ||
                    (pInterface->u1NetworkType == IF_VIRTUAL))
                {
                    if ((pNbrNode =
                         TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
                    {
                        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                        /* Add PTOP or virtual link */
                        if (IS_NBR_FULL (pNbr))
                        {
                            if (IS_VIRTUAL_IFACE (pInterface))
                            {
                                u1LinkType = VIRTUAL_LINK;
                            }
                            else
                            {
                                u1LinkType = PTOP_LINK;
                            }

                            if (IS_UNNUMBERED_PTOP_IFACE (pInterface))
                            {
                                OLS_ADD_UN_NUM_PTP_LNK (pCurrent, pNbr->nbrId,
                                                        pInterface->
                                                        u4AddrlessIf,
                                                        u1LinkType,
                                                        u2LinkCount);
                            }
                            else
                            {
                                OLS_RTR_LSA_ADD_LINK (pCurrent, pNbr->nbrId,
                                                      pInterface->ifIpAddr,
                                                      u1LinkType, u2LinkCount);
                            }
                            OSPF_TRC (OSPF_LSU_TRC,
                                      pArea->pOspfCxt->u4OspfCxtId,
                                      "FUNC: OlsRtrLsaAddCosts\n");
                            pCurrent =
                                OlsRtrLsaAddCosts (pCurrent,
                                                   pInterface->aIfOpCost);
                            OSPF_TRC (OSPF_LSU_TRC,
                                      pArea->pOspfCxt->u4OspfCxtId,
                                      "Existing FUNC: OlsRtrLsaAddCosts\n");
                        }
                    }
                    /* If numbered ptop link, then add stub link */
                    if (IS_NUMBERED_PTOP_IFACE (pInterface))
                    {
                        OSPF_CRU_BMC_DWTOPDU (ipSubnetAddr,
                                              (OSPF_CRU_BMC_DWFROMPDU
                                               (pInterface->ifIpAddr) &
                                               OSPF_CRU_BMC_DWFROMPDU
                                               (pInterface->ifIpAddrMask)));
                        OLS_RTR_LSA_ADD_LINK (pCurrent, ipSubnetAddr,
                                              pInterface->ifIpAddrMask,
                                              STUB_LINK, u2LinkCount);
                        OSPF_TRC (OSPF_LSU_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "FUNC: OlsRtrLsaAddCosts\n");
                        pCurrent =
                            OlsRtrLsaAddCosts (pCurrent, pInterface->aIfOpCost);
                        OSPF_TRC (OSPF_LSU_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "FUNC Exit : OlsRtrLsaAddCosts\n");
                    }
                }
                else if (pInterface->u1NetworkType == IF_PTOMP)
                {
                    OLS_RTR_LSA_ADD_LINK (pCurrent, pInterface->ifIpAddr,
                                          gHostRouteMask,
                                          STUB_LINK, u2LinkCount);
                    pTosCount = pCurrent;
                    LADD1BYTE (pCurrent, 0);
                    LADD2BYTE (pCurrent, 0);

                    u1TosCount = 0;
                    /* To fill other TOS information */
#ifdef TOS_SUPPORT
                    for (u1Tos = 1; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                    {
                        if (pInterface->aIfOpCost[u1Tos].rowStatus == ACTIVE)
                        {
                            u1TosCount++;
                            /* ospf TOS encoding */
                            LADD1BYTE (pCurrent, ((UINT1) ENCODE_TOS (u1Tos)));
                            /* unused */
                            LADD1BYTE (pCurrent, 0);
                            /* cost */
                            LADD2BYTE (pCurrent, 0);
                        }
                    }
#endif
                    /* Filling Number of Non-Zero TOS information */
                    LADD1BYTE (pTosCount, u1TosCount);
                    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                                  tTMO_SLL_NODE *)
                    {
                        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                        if (IS_NBR_FULL (pNbr))
                        {
                            OLS_RTR_LSA_ADD_LINK (pCurrent, pNbr->nbrId,
                                                  pInterface->ifIpAddr,
                                                  PTOP_LINK, u2LinkCount);
                            OSPF_TRC (OSPF_LSU_TRC,
                                      pArea->pOspfCxt->u4OspfCxtId,
                                      "FUNC: OlsRtrLsaAddCosts\n");
                            pCurrent =
                                OlsRtrLsaAddCosts (pCurrent,
                                                   pInterface->aIfOpCost);
                            OSPF_TRC (OSPF_LSU_TRC,
                                      pArea->pOspfCxt->u4OspfCxtId,
                                      "Exiting FUNC: OlsRtrLsaAddCosts\n");
                        }
                    }
                }
                /* Advertise secondary IP address */
                if (pInterface->secondaryIP.i1SecIpCount)
                {
                    /* If non-zero, there is at least one secondary ip */
                    for (i2Index = 0;
                         i2Index < OSPF_MIN (pInterface->secondaryIP.
                                             i1SecIpCount, MAX_SEC_INTERFACES);
                         i2Index++)
                    {
                        UtilIpAddrMaskCopy (ipAux,
                                            pInterface->
                                            secondaryIP.ifSecIp
                                            [i2Index].ifSecIpAddr,
                                            pInterface->
                                            secondaryIP.ifSecIp
                                            [i2Index].ifSecIpAddrMask);
                        OLS_RTR_LSA_ADD_LINK (pCurrent, ipAux,
                                              &(pInterface->
                                                secondaryIP.ifSecIp
                                                [i2Index].ifSecIpAddrMask),
                                              STUB_LINK, u2LinkCount);
                        OSPF_TRC (OSPF_LSU_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "FUNC: OlsRtrLsaAddCosts\n");
                        pCurrent = OlsRtrLsaAddCosts (pCurrent,
                                                      pInterface->aIfOpCost);
                        OSPF_TRC (OSPF_LSU_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "Exiting FUNC: OlsRtrLsaAddCosts\n");
                    }
                }
                break;
            default:
                /*both Loopback and normal scenario are handled here */
                if ((pInterface->u1IsmState == IFS_LOOP_BACK)
                    && (pInterface->u4AddrlessIf == 0))
                {
                    OLS_RTR_LSA_ADD_LINK (pCurrent, pInterface->ifIpAddr,
                                          gHostRouteMask, STUB_LINK,
                                          u2LinkCount);
                    /* #TOS = 0 */
                    LADD1BYTE (pCurrent, 0);
                    /* TOS-0 aMetric */
                    LADD2BYTE (pCurrent, pInterface->aIfOpCost[TOS_0].u4Value);

                }
                else
                {
                    if (OlsIsTransitLink (pInterface) == OSPF_TRUE)
                    {
                        OLS_RTR_LSA_ADD_LINK (pCurrent, GET_DR (pInterface),
                                              pInterface->ifIpAddr,
                                              TRANSIT_LINK, u2LinkCount);
                    }
                    else
                    {
                        GET_IP_NET_NUM (netNum, pInterface->ifIpAddr,
                                        pInterface->ifIpAddrMask);
                        OLS_RTR_LSA_ADD_LINK (pCurrent, netNum,
                                              pInterface->ifIpAddrMask,
                                              STUB_LINK, u2LinkCount);
                    }
                    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                              "FUNC: OlsRtrLsaAddCosts\n");
                    pCurrent =
                        OlsRtrLsaAddCosts (pCurrent, pInterface->aIfOpCost);
                    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                              "Exiting FUNC: OlsRtrLsaAddCosts\n");
                }
                /* Advertise secondary IP address */
                if (pInterface->secondaryIP.i1SecIpCount)
                {
                    /* If non-zero, there is at least one secondary ip */
                    for (i2Index = 0;
                         i2Index < OSPF_MIN (pInterface->secondaryIP.
                                             i1SecIpCount, MAX_SEC_INTERFACES);
                         i2Index++)
                    {
                        UtilIpAddrMaskCopy (ipAux,
                                            pInterface->
                                            secondaryIP.ifSecIp
                                            [i2Index].ifSecIpAddr,
                                            pInterface->
                                            secondaryIP.ifSecIp
                                            [i2Index].ifSecIpAddrMask);
                        OLS_RTR_LSA_ADD_LINK (pCurrent, ipAux,
                                              &(pInterface->
                                                secondaryIP.ifSecIp
                                                [i2Index].ifSecIpAddrMask),
                                              STUB_LINK, u2LinkCount);
                        OSPF_TRC (OSPF_LSU_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "FUNC: OlsRtrLsaAddCosts\n");
                        pCurrent = OlsRtrLsaAddCosts (pCurrent,
                                                      pInterface->aIfOpCost);
                        OSPF_TRC (OSPF_LSU_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "Exiting FUNC: OlsRtrLsaAddCosts\n");
                    }
                }
        }
    }

    /* Add host routes */
    TMO_SLL_Scan (&(pArea->pOspfCxt->hostLst), pHost, tHost *)
    {
        if (pHost->pArea == pArea)
        {
            OSPF_TOS_LOOP (u1Tos)
            {
                if (pHost->aHostCost[u1Tos].rowStatus == ACTIVE &&
                    pHost->aHostCost[u1Tos].u4Value < LS_INFINITY_16BIT)
                {
                    OLS_RTR_LSA_ADD_LINK (pCurrent, pHost->hostIpAddr,
                                          gHostRouteMask, STUB_LINK,
                                          u2LinkCount);
                    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                              "FUNC: OlsRtrLsaAddCosts\n");
                    pCurrent = OlsRtrLsaAddCosts (pCurrent, pHost->aHostCost);
                    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                              "Exiting FUNC: OlsRtrLsaAddCosts\n");
                }
            }
        }
    }
    pCurrent = OlsRtrLsaAddRtrIdInCxt (pArea, pCurrent, &u2LinkCount);

    /* Fill #links field in lsa */
    LADD2BYTE (pLinkCount, u2LinkCount);
    *pLen = (UINT2) (pCurrent - pTempRtrLsa);

    if (*pLen > (UINT2) (MAX_LSA_SIZE4096 - 1))
    {
        OSPF_TRC2 (OSPF_CRITICAL_TRC, pArea->pOspfCxt->u4OspfCxtId,
                   "Router LSA size is more than Allocated size! \
                Allocated size is %d bytes and Required size is %d bytes. \
                This is non-recoverable condition, \
                This will need a change in MAX LSA size and restart of the system \n", (MAX_LSA_SIZE4096 - 1), *pLen);
        LSA_FREE (ROUTER_LSA, pTempRtrLsa);
        return NULL;
    }

    LSA_ALLOC (ROUTER_LSA, pRtrLsa, *pLen);
    if (pRtrLsa == NULL)
    {
        OSPF_TRC (OSPF_LSU_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "LSA_ALLOC FAILURE\n");
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_LSU_TRC | OSPF_MEM_FAIL |
                  OSPF_CRITICAL_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Rtr LSA Alloc Failure\n");
        LSA_FREE (ROUTER_LSA, pTempRtrLsa);
        return NULL;
    }

    MEMCPY (pRtrLsa, pTempRtrLsa, *pLen);
    LSA_FREE (ROUTER_LSA, pTempRtrLsa);

    OlsConstructLsHeaderInCxt (pArea->pOspfCxt, pRtrLsa, *pLen, ROUTER_LSA,
                               &(pArea->pOspfCxt->rtrId), seqNum, options);

    OSPF_TRC (CONTROL_PLANE_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Rtr LSA Constructed\n");
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsConstructRtrLsa\n");
    return pRtrLsa;
}

/******************************************************************************
                                                                          
Function        :   OlsConstructNetworkLsa
                                                                               
Description     :   constructs a network LSA.                                   
                                                                               
Input           :   
                    pInterface :   pointer to interface to which the network 
                                    LSA is to be generated
                    seqNum     :   sequence number                      
                                                                               
Output          :   
                    pLen       :   length of constructed LSA
                                                                               
Returns         :   pointer to network LSA, if successfully constructed
                    NULL,                   otherwise
                                                                          
******************************************************************************/

PRIVATE UINT1      *
OlsConstructNetworkLsa (tInterface * pInterface, tLSASEQNUM seqNum, UINT2 *pLen)
{

    UINT1              *pNetworkLsa;
    UINT1              *pCurrent;
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    UINT1               u1Found = OSPF_FALSE;
    tOPTIONS            options;
    tLsaInfo           *pLsaInfo;
    UINT4               u4NetworkLsaSize;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsConstructNetworkLsa\n");

    OSPF_TRC1 (OSPF_LSU_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Network LSA to be constructed for interface : %x.\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    /* verify whether the DR is fully adj with atleast one router */
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->u1NsmState == NBRS_FULL)
        {
            u1Found = OSPF_TRUE;
            break;
        }
    }
    if (u1Found == OSPF_FALSE)
    {
        if ((pLsaInfo = LsuSearchDatabase (NETWORK_LSA,
                                           &(pInterface->desgRtr),
                                           &(pInterface->pArea->pOspfCxt->
                                             rtrId), (UINT1 *) NULL,
                                           (UINT1 *) (pInterface->pArea))) !=
            NULL)
        {
            AgdFlushOut (pLsaInfo);
        }
        OSPF_TRC1 (OSPF_LSU_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "DR is not fully adj with any of the router "
                   "Network LSA can not be constructed for interface : "
                   " %x", OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

        return (NULL);
    }

    u4NetworkLsaSize = NETWORK_LSA_FIXED_PORTION_SIZE +
        ((pInterface->u4NbrFullCount + 1) * (sizeof (tRouterId)));

    LSA_ALLOC (NETWORK_LSA, pNetworkLsa, (UINT2) u4NetworkLsaSize);
    if (pNetworkLsa == NULL)
    {
        OSPF_TRC (OSPF_LSU_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "LSA_ALLOC FAILURE\n");
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "N/W LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pNetworkLsa;

    /* leave space for header */
    OS_MEM_SET (pNetworkLsa, 0, LS_HEADER_SIZE);
    pCurrent += LS_HEADER_SIZE;

    /* network mask */
    LADDSTR (pCurrent, pInterface->ifIpAddrMask, MAX_IP_ADDR_LEN);

    /* router id of this router. */
    LADDSTR (pCurrent, pInterface->pArea->pOspfCxt->rtrId, MAX_IP_ADDR_LEN);

    /* router ids of all neighbors who are fully adjacent */
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->u1NsmState == NBRS_FULL)
        {
            LADDSTR (pCurrent, pNbr->nbrId, MAX_IP_ADDR_LEN);
        }
    }

    options = (T_BIT_MASK |
               ((IS_STUB_AREA (pInterface->pArea)) ? 0 : E_BIT_MASK));

    if (pInterface->pArea->pOspfCxt->bDemandExtension == OSPF_TRUE)
    {
        options = options | DC_BIT_MASK;
    }

    *pLen = (UINT2) (pCurrent - pNetworkLsa);

    OlsConstructLsHeaderInCxt (pInterface->pArea->pOspfCxt, pNetworkLsa, *pLen,
                               NETWORK_LSA, &(pInterface->ifIpAddr), seqNum,
                               options);

    OSPF_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "N/W LSA Constructed\n");
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsConstructNetworkLsa\n");
    return pNetworkLsa;
}

/******************************************************************************
                                                                          
Function        :   OlsConstructSummaryLsa
                                                                               
Description     :   constructs a summary LSA.                                   
                                                                               
Input           :   
                    pArea      :   pointer to area to which the
                                    summary LSA is to be generated
                    u1Type     :   internal type 
                                    DEFAULT_NETWORK_SUM_LSA |
                                    COND_NETWORK_SUM_LSA |
                                    NETWORK_SUM_LSA | ASBR_SUM_LSA |
                                    INDICATION_LSA
                    pStruct    :   structure associated with it
                                    pArea / pAddrRange / pSummaryParam
                    seqNum     :   sequence number                      
                                                                               
Output          :   
                    pLen      :   length of constructed LSA
                                                                               
Returns         :   pointer to summary LSA, if successfully constructed
                    NULL,                   otherwise
                                                                          
******************************************************************************/

PRIVATE UINT1      *
OlsConstructSummaryLsa (tArea * pArea,
                        UINT1 u1Type,
                        UINT1 *pStruct, tLSASEQNUM seqNum, UINT2 *pLen,
                        tLINKSTATEID * pLsId)
{
    UINT1              *pSummaryLsa;
    UINT1              *pCurrent;
    UINT1               u1Tos;
    UINT1               u1LsaType = 0;
    INT4                i4Cost;
    INT4                i4Tos0Cost;
    tIPADDRMASK         ipAddrMask;
    tLINKSTATEID        linkStateId;
    tTRUTHVALUE         bTBitFlag;
    tOPTIONS            options;
    tOspfCxt           *pOspfCxt = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsConstructSummaryLsa\n");
    IP_ADDR_COPY (ipAddrMask, gNullIpAddr);

    LSA_ALLOC (NETWORK_SUM_LSA, pSummaryLsa, MAX_LSA_SIZE);
    if (pSummaryLsa == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC
                  | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "Summary LSA Alloc Failure\n");
        return (NULL);
    }

    pCurrent = pSummaryLsa;

    /* leave space for header */
    OS_MEM_SET (pSummaryLsa, 0, LS_HEADER_SIZE);
    pCurrent += LS_HEADER_SIZE;
    options = 0;

    if (pArea->pOspfCxt->bDemandExtension == OSPF_TRUE)
    {
        options = options | DC_BIT_MASK;
    }

    switch (u1Type)
    {
        case COND_NETWORK_SUM_LSA:
            IP_ADDR_COPY (ipAddrMask, ((tAddrRange *) (VOID *) pStruct)->
                          ipAddrMask);
            IP_ADDR_COPY (linkStateId,
                          ((tAddrRange *) (VOID *) pStruct)->ipAddr);
            pOspfCxt = pArea->pOspfCxt;
            pArea =
                UtilFindAssoAreaInCxt (pArea->pOspfCxt,
                                       &(((tAddrRange *) (VOID *) pStruct)->
                                         ipAddr));
            if (pArea == NULL)
            {
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_LSU_TRC,
                          pOspfCxt->u4OspfCxtId, "Area Not Found\n");
                LSA_FREE (u1Type, pSummaryLsa);
                return (NULL);
            }
            options = options | ((!IS_STUB_AREA (pArea)) ? E_BIT_MASK : 0);
            u1LsaType = NETWORK_SUM_LSA;
            break;

        case NETWORK_SUM_LSA:
            IP_ADDR_COPY (ipAddrMask, ((tSummaryParam *) (VOID *) pStruct)->
                          ipAddrMask);
            IP_ADDR_COPY (linkStateId, ((tSummaryParam *) (VOID *) pStruct)->
                          ipAddr);
            options = options | ((!IS_STUB_AREA (pArea)) ? E_BIT_MASK : 0);
            u1LsaType = NETWORK_SUM_LSA;
            break;

        case INDICATION_LSA:
            IP_ADDR_COPY (ipAddrMask, gNullIpAddr);
            IP_ADDR_COPY (linkStateId, ((tSummaryParam *) (VOID *) pStruct)->
                          ipAddr);
            options = options | E_BIT_MASK;
            options = (UINT1) (options & ~DC_BIT_MASK);
            u1LsaType = ASBR_SUM_LSA;
            break;

        case ASBR_SUM_LSA:
            IP_ADDR_COPY (ipAddrMask, gNullIpAddr);
            IP_ADDR_COPY (linkStateId, ((tSummaryParam *) (VOID *) pStruct)->
                          ipAddr);
            options = options | E_BIT_MASK;
            u1LsaType = ASBR_SUM_LSA;
            break;

        default:
            break;
    }

    LADDSTR (pCurrent, ipAddrMask, MAX_IP_ADDR_LEN);

    /* add cost for each TOS */
    bTBitFlag = OSPF_FALSE;
    i4Tos0Cost = 0;

    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGetSummaryCost\n");

    if ((i4Tos0Cost = OlsGetSummaryCost (pStruct, u1Type, TOS_0)) ==
        OSPF_FAILURE)
    {
        /* Checking for the Boundary condition */
        i4Tos0Cost = LS_INFINITY_24BIT;
    }
    OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Exiting Function: OlsGetSummaryCost\n");
    LADD1BYTE (pCurrent, ENCODE_TOS (TOS_0));
    LADD3BYTE (pCurrent, i4Tos0Cost);
#ifdef TOS_SUPPORT
    for (u1Tos = 1; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: OlsGetSummaryCost\n");

        if ((i4Cost = OlsGetSummaryCost (pStruct, u1Type, u1Tos)) !=
            OSPF_FAILURE)
        {
            bTBitFlag = OSPF_TRUE;
            LADD1BYTE (pCurrent, ((UINT1) ENCODE_TOS (u1Tos)));
            LADD3BYTE (pCurrent, i4Cost);
        }
        OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Existing FUNC: OlsGetSummaryCost\n");
    }
#else
    UNUSED_PARAM (u1Tos);
#endif

    options = options | ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : 0);

    *pLen = (UINT2) (pCurrent - pSummaryLsa);
    OlsConstructLsHeaderInCxt (pArea->pOspfCxt, pSummaryLsa, *pLen, u1LsaType,
                               pLsId, seqNum, options);

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Summary LSA Constructed\n");
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsConstructSummaryLsa\n");
    UNUSED_PARAM (i4Cost);
    return pSummaryLsa;
}

/******************************************************************************
                                                                          
Function        :   OlsConstructAsExtLsaInCxt
                                                                               
Description     :   constructs an ASE LSA.                                   
                                                                               
Input           :   
                    pExtRoute :   pointer to ext_route to which the ASE LSA
                                    is to be generated
                    seqNum     :   sequence number                      
                                                                                
Output          :               
                    pLen       :   length of constructed LSA
                                                                               
Returns         :   pointer to ASE LSA,    if successfully constructed
                    NULL,                  otherwise
                                                                          
******************************************************************************/

PRIVATE UINT1      *
OlsConstructAsExtLsaInCxt (tOspfCxt * pOspfCxt, UINT1 *pPtr,
                           UINT1 u1InternalType, tLSASEQNUM seqNum,
                           UINT2 *pLen, tLINKSTATEID * pLsId)
{

    UINT1              *pAsExtLsa;
    UINT1              *pCurrent;
    UINT1               u1Tos = 0;
    UINT1               u1EBitTos = 0;
    UINT4               u4RangeValue;
    tTRUTHVALUE         bTBitFlag;
    tOPTIONS            options = 0;
    tExtRoute          *pExtRoute = NULL;
    tAsExtAddrRange    *pAsExtRange = NULL;
    tRtEntry           *pRtEntry = NULL;
    tAddrRange         *pAddrRange = NULL;
    tPath              *pPath;
    tIPADDR             destIpAddrMask;
    tExtLsaLink         extLsaLink;
    INT1                i1RetCode;
    tArea              *pArea = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OlsConstructAsExtLsaInCxt\n");
    if ((!(pOspfCxt->bAsBdrRtr == OSPF_TRUE)) &&
        (!((pOspfCxt->bAreaBdrRtr == OSPF_TRUE) &&
           (pOspfCxt->u4NssaAreaCount > 0))))
    {
        OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                  "Router is not an ABR\n");
        return NULL;
    }

    LSA_ALLOC (AS_EXT_LSA, pAsExtLsa, MAX_EXT_LSA_SIZE);
    if (pAsExtLsa == NULL)
    {
        OSPF_TRC (OSPF_MEM_FAIL | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "LSA_ALLOC FAILURE\n");
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_LSU_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "N/W Ext LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pAsExtLsa;

    /* leave space for header */
    OS_MEM_SET (pAsExtLsa, 0, LS_HEADER_SIZE);
    pCurrent += LS_HEADER_SIZE;

    if (pOspfCxt->bDemandExtension == OSPF_TRUE)
    {
        options = options | DC_BIT_MASK;
    }

    if (u1InternalType == COND_AS_EXT_LSA)
    {
        pAsExtRange = (tAsExtAddrRange *) (VOID *) pPtr;
        LADDSTR (pCurrent, pAsExtRange->ipAddrMask, MAX_IP_ADDR_LEN);
        bTBitFlag = OSPF_FALSE;

        /* TosSupport */
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            if (pAsExtRange->aMetric[u1Tos].bStatus == OSPF_VALID)
            {
                u1EBitTos =
                    ((pAsExtRange->aMetric[u1Tos].u4MetricType ==
                      TYPE_1_METRIC) ? 0 : EXT_LSA_METRIC_TYPE_MASK);
                u1EBitTos = (UINT1) (u1EBitTos | ENCODE_TOS (u1Tos));

                LADD1BYTE (pCurrent, u1EBitTos);
                /* Metric */
                LADD3BYTE (pCurrent, pAsExtRange->aMetric[u1Tos].u4Value);
                /* Fwd Addr */
                LADDSTR (pCurrent, gNullIpAddr, MAX_IP_ADDR_LEN);
                /* Ext RT Tag */
                LADD4BYTE (pCurrent, 0);
#ifdef TOS_SUPPORT
                if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                {
                    bTBitFlag = OSPF_TRUE;
                }
#endif
            }
            else if (u1Tos == TOS_0)
            {
                /* Without TOS 0, Generating LSA is useless */
                LSA_FREE (AS_EXT_LSA, pAsExtLsa);
                return NULL;
            }
        }

        options = ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : 0) | E_BIT_MASK;
        *pLen = (UINT2) (pCurrent - pAsExtLsa);
        OlsConstructLsHeaderInCxt (pOspfCxt, pAsExtLsa, *pLen, AS_EXT_LSA,
                                   pLsId, seqNum, options);

    }
    else if (u1InternalType == AS_TRNSLTD_EXT_LSA)
    {
        pRtEntry = (tRtEntry *) (VOID *) pPtr;
        if ((pPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
        {
            LSA_FREE (AS_EXT_LSA, pAsExtLsa);
            return NULL;
        }
        pArea = GetFindAreaInCxt (pOspfCxt, &(pPath->areaId));
        if ((pArea != NULL) && (pArea->u4AreaType == NSSA_AREA) &&
            (pArea->u1NssaTrnsltrState == TRNSLTR_STATE_DISABLED))
        {
            LSA_FREE (AS_EXT_LSA, pAsExtLsa);
            return NULL;
        }
        OS_MEM_CPY (destIpAddrMask,
                    (UINT1 *) (pPath->pLsaInfo->pLsa + LS_HEADER_SIZE),
                    MAX_IP_ADDR_LEN);
        LADDSTR (pCurrent, destIpAddrMask, MAX_IP_ADDR_LEN);

        /* TosSupport */
        bTBitFlag = OSPF_FALSE;
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            if ((i1RetCode = RtcGetExtLsaLink (pPath->pLsaInfo->pLsa,
                                               pPath->pLsaInfo->u2LsaLen, u1Tos,
                                               &extLsaLink)) == OSPF_SUCCESS)
            {
                u1EBitTos =
                    ((extLsaLink.u1MetricType ==
                      TYPE_1_METRIC) ? 0 : EXT_LSA_METRIC_TYPE_MASK);

                u1EBitTos = (UINT1) (u1EBitTos | ENCODE_TOS (u1Tos));
                LADD1BYTE (pCurrent, u1EBitTos);
                /* Metric */
                LADD3BYTE (pCurrent, extLsaLink.u4Cost);
                /* Fwd Addr */
                LADDSTR (pCurrent, extLsaLink.fwdAddr, MAX_IP_ADDR_LEN);
                /* Ext RT Tag */
                LADD4BYTE (pCurrent, extLsaLink.u4ExtRouteTag);
#ifdef TOS_SUPPORT
                if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                {
                    bTBitFlag = OSPF_TRUE;
                }
#endif
            }
            else if (u1Tos == TOS_0)
            {
                /* Without TOS 0, Generating LSA is useless */
                LSA_FREE (AS_EXT_LSA, pAsExtLsa);
                return NULL;
            }
        }

        options = ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : 0) | E_BIT_MASK;
        *pLen = (UINT2) (pCurrent - pAsExtLsa);
        OlsConstructLsHeaderInCxt (pOspfCxt, pAsExtLsa, *pLen, AS_EXT_LSA,
                                   pLsId, seqNum, options);
    }
    else if (u1InternalType == AS_TRNSLTD_EXT_RNG_LSA)
    {
        pAddrRange = (tAddrRange *) (VOID *) pPtr;
        LADDSTR (pCurrent, pAddrRange->ipAddrMask, MAX_IP_ADDR_LEN);

        /* TosSupport */
        bTBitFlag = OSPF_FALSE;
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            if (pAddrRange->aMetric[u1Tos].bStatus == OSPF_VALID)
            {
                u1EBitTos =
                    ((pAddrRange->aMetric[u1Tos].u4MetricType ==
                      TYPE_1_METRIC) ? 0 : EXT_LSA_METRIC_TYPE_MASK);
                u1EBitTos = (UINT1) (u1EBitTos | ENCODE_TOS (u1Tos));
                LADD1BYTE (pCurrent, u1EBitTos);

                u4RangeValue = pAddrRange->aMetric[u1Tos].u4Value;
                if (pAddrRange->aMetric[u1Tos].u4MetricType == TYPE_2_METRIC)
                {
                    u4RangeValue++;
                }
                /* Metric */
                LADD3BYTE (pCurrent, u4RangeValue);
                /* Fwd Addr */
                LADDSTR (pCurrent, gNullIpAddr, MAX_IP_ADDR_LEN);
                /* Ext RT Tag */
                LADD4BYTE (pCurrent, pAddrRange->u4ExtRtTag);
#ifdef TOS_SUPPORT
                if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                {
                    bTBitFlag = OSPF_TRUE;
                }
#endif
            }
            else if (u1Tos == TOS_0)
            {
                /* Without TOS 0, Generating LSA is useless */
                LSA_FREE (AS_EXT_LSA, pAsExtLsa);
                return NULL;
            }
        }

        options = ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : 0) | E_BIT_MASK;
        *pLen = (UINT2) (pCurrent - pAsExtLsa);
        OlsConstructLsHeaderInCxt (pOspfCxt, pAsExtLsa, *pLen, AS_EXT_LSA,
                                   pLsId, seqNum, options);

    }
    else
    {
        pExtRoute = (tExtRoute *) (VOID *) pPtr;
        LADDSTR (pCurrent, pExtRoute->ipAddrMask, MAX_IP_ADDR_LEN);
        bTBitFlag = OSPF_FALSE;
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            if (pExtRoute->aMetric[u1Tos].rowStatus == ACTIVE)
            {
                /* type of aMetric E-bit in TOS */
                u1EBitTos =
                    ((pExtRoute->extrtParam[u1Tos].u1MetricType ==
                      TYPE_1_METRIC) ? 0 : EXT_LSA_METRIC_TYPE_MASK);

                u1EBitTos = (UINT1) (u1EBitTos | ENCODE_TOS (u1Tos));
                LADD1BYTE (pCurrent, u1EBitTos);
                /* aMetric value */
                LADD3BYTE (pCurrent, pExtRoute->aMetric[u1Tos].u4Value);
                /* forwarding addr */
                LADDSTR (pCurrent, pExtRoute->extrtParam[u1Tos].fwdAddr,
                         MAX_IP_ADDR_LEN);
                /* ext route tag */
                LADD4BYTE (pCurrent, pExtRoute->extrtParam[u1Tos].u4ExtTag);
#ifdef TOS_SUPPORT
                if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                {
                    bTBitFlag = OSPF_TRUE;
                }
#endif
            }
            else if (u1Tos == TOS_0)
            {
                /* Without TOS 0, Generating LSA is useless */
                LSA_FREE (AS_EXT_LSA, pAsExtLsa);
                return NULL;
            }

        }
        options = ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : 0) | E_BIT_MASK;
        *pLen = (UINT2) (pCurrent - pAsExtLsa);
        OlsConstructLsHeaderInCxt (pOspfCxt, pAsExtLsa, *pLen, AS_EXT_LSA,
                                   pLsId, seqNum, options);
    }
    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
              "ASExt LSA Constructed\n");
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: OlsConstructAsExtLsaInCxt\n");

    return pAsExtLsa;
}

/******************************************************************************

Function        :   OlsConstructOpaqueLsa

Description     :  constructs an Opaque LSA.

Input           :  pOpqLSAInfo  :   Pointer to the Opaque LSA related
                                    information.

                    seqNum     :   sequence number

Output          :
                    pLen       :   length of constructed LSA

Returns         :                          if successfully constructed
                    NULL,                  otherwise

******************************************************************************/

PRIVATE UINT1      *
OlsConstructOpaqueLsa (tOpqLSAInfo * pOpqLSAInfo,
                       tLSASEQNUM seqNum, UINT2 *pLen)
{

    UINT1              *pOpqLsa;
    UINT1              *pCurrent;
    tOPTIONS            options;

    options = 0;

    LSA_ALLOC (TYPE10_OPQ_LSA, pOpqLsa, pOpqLSAInfo->u2LSALen);
    if (pOpqLsa == NULL)
    {
        return NULL;
    }
    if (pOpqLSAInfo->pOspfCxt->bDemandExtension == OSPF_TRUE)
    {
        options |= DC_BIT_MASK;
    }

    pCurrent = pOpqLsa;

    /* leave space for header */
    OS_MEM_SET (pOpqLsa, 0, LS_HEADER_SIZE);
    pCurrent += LS_HEADER_SIZE;

    LADDSTR (pCurrent, pOpqLSAInfo->pu1LSA, pOpqLSAInfo->u2LSALen);

    *pLen = (UINT2) (pCurrent - pOpqLsa);

    OlsConstructLsHeaderInCxt (pOpqLSAInfo->pOspfCxt, pOpqLsa, *pLen,
                               pOpqLSAInfo->u1OpqLSAType,
                               &(pOpqLSAInfo->LsId), seqNum, options);
    return pOpqLsa;
}

/******************************************************************************

Function        :   OlsConstructLsHeaderInCxt

Description     :   constructs the LS header for the given LSA

Input           :
                    pLsa           :   pointer to LSA        
                    u2Len          :   length of LSA         
                    u1LsaType     :   type of LSA           
                    pLinkStateId :   LS Id
                    lsaSeqNum     :   sequence number       
                    options         :   optional capabilities 

Output          :   None

Returns         :   None

******************************************************************************/

PRIVATE VOID
OlsConstructLsHeaderInCxt (tOspfCxt * pOspfCxt,
                           UINT1 *pLsa,
                           UINT2 u2Len,
                           UINT1 u1LsaType,
                           tLINKSTATEID * pLinkStateId,
                           tLSASEQNUM lsaSeqNum, tOPTIONS options)
{
    UINT1              *pCurr = pLsa;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OlsConstructLsHeaderInCxt\n");
    OSPF_TRC5 (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId, "pLsa : %x, lsa_type : %d,"
               "linkStateId : %x, seqNum : %x, options : %d.\n",
               pLsa, u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pLinkStateId), lsaSeqNum,
               options);

    if (lsaSeqNum == MAX_SEQ_NUM)
    {
        LADD2BYTE (pCurr, MAX_AGE);
        OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
                  "FUNC :OlsConstructLsHeaderInCxt, Adding MAX_AGE in pkt\n");

    }

    else
    {
        /* age set to 0     */
        LADD2BYTE (pCurr, 0);
        OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
                  "FUNC :OlsConstructLsHeaderInCxt, age set to 0 in pkt\n");

    }

    LADD1BYTE (pCurr, options);
    LADD1BYTE (pCurr, u1LsaType);
    LADDSTR (pCurr, pLinkStateId, MAX_IP_ADDR_LEN);

    /* advRtrId       */
    LADDSTR (pCurr, pOspfCxt->rtrId, MAX_IP_ADDR_LEN);
    LADD4BYTE (pCurr, lsaSeqNum);

    /* chksum set to 0  */
    LADD2BYTE (pCurr, 0);
    LADD2BYTE (pCurr, u2Len);

    UtilComputeLsaFletChksum (pLsa, u2Len);

    OSPF_TRC1 (CONTROL_PLANE_TRC
               | OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
               "LS Hdr Constructed LSType %s\n", au1DbgLsaType[u1LsaType]);
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: OlsConstructLsHeaderInCxt\n");
}

/******************************************************************************

Function        :   OlsRtrLsaAddCosts                                     
                
Description     :   This function adds the cost of transmission on this       
                    interface for various TOS from the tMibMetric structure
                    to the router LSA being constructed in buffer.
                
Input           :   
                    pBuf          :   buffer to hold the router LSA  
                                       being constructed              
                    p_metric_list  :   aMetric list                    
                
Output          :   None
                
Returns         :   pointer to buffer which holds the router LSA              

******************************************************************************/

PRIVATE UINT1      *
OlsRtrLsaAddCosts (UINT1 *pBuf, tMibMetric * pMetricLst)
{

    UINT1               u1Tos;
    UINT1              *pTosCount;
    UINT1               u1TosCount = 0;

    /* store ptr to tos count field to increment later */
    pTosCount = pBuf;

    /* tos count initialised to 0 */
    LADD1BYTE (pBuf, 0);

    /* add TOS-0 aMetric */
    LADD2BYTE (pBuf, (UINT2) pMetricLst[TOS_0].u4Value);
#ifdef TOS_SUPPORT
    for (u1Tos = 1; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if (pMetricLst[u1Tos].rowStatus == ACTIVE &&
            pMetricLst[u1Tos].u4Value < LS_INFINITY_16BIT)
        {
            u1TosCount++;

            /* ospf TOS encoding */
            LADD1BYTE (pBuf, ((UINT1) ENCODE_TOS (u1Tos)));

            /* unused */
            LADD1BYTE (pBuf, 0);

            /* cost */
            LADD2BYTE (pBuf, (UINT2) pMetricLst[u1Tos].u4Value);
        }
    }
#else
    UNUSED_PARAM (u1Tos);
#endif
    LADD1BYTE (pTosCount, u1TosCount);
    return pBuf;
}

/******************************************************************************

Function        :   OlsRtrLsaAddRtrIdInCxt

Description     :   if all of the router's interfaces are unnumbered ptop 
                    interfaces, then the router adds itself as a host route

Input           : 
                    pBuf          :   buffer to hold the router LSA  
                                       being constructed              
                    pLinkCount   :   current link count

Output          :
                    pLinkCount   :   link count after the routine

Returns         :   pointer to buffer which holds the router LSA              

******************************************************************************/

PRIVATE UINT1      *
OlsRtrLsaAddRtrIdInCxt (tArea * pArea, UINT1 *pBuf, UINT2 *pLinkCount)
{

    tTMO_SLL_NODE      *pIfNode;
    tInterface         *pInterface;
    UINT2               u2LinkCount = 0;
    UINT1               u1Flag = OSPF_FALSE;
    UINT4               u4IfIndex;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT4               u4UnnumAssocIPAddr = 0;
    tIPADDR             SrcIpAddr;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsRtrLsaAddRtrIdInCxt\n");
    /* check whether any interface is numbered, if so no need to proceed */
    TMO_SLL_Scan (&(pArea->pOspfCxt->sortIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfNode);
        if (MEMCMP (pArea->areaId, pInterface->pArea->areaId,
                    MAX_IPV4_ADDR_LEN) == 0)
        {
            if (pInterface->u1IsmState == IFS_DOWN)
            {
                continue;
            }
            /* for unnumbered interface need to 
             * add mapped loopback interface . 
             * if loopback interface not mapped  
             * then add router-id */
            if (pInterface->u4AddrlessIf != 0)
            {
                /*Get interface index from IP port */
                u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
                CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
                if (u4UnnumAssocIPIf != 0)
                {
                    CfaIpIfGetSrcAddressOnInterface (u4UnnumAssocIPIf,
                                                     (UINT4) 0,
                                                     &u4UnnumAssocIPAddr);
                    u4UnnumAssocIPAddr = OSIX_HTONL (u4UnnumAssocIPAddr);
                    /* convert u4UnnumAssocIPAddr to 
                     * network byte order and then copy */
                    IP_ADDR_COPY (SrcIpAddr, (UINT1 *) &u4UnnumAssocIPAddr);
                    if (MEMCMP
                        (pArea->pOspfCxt->rtrId, SrcIpAddr,
                         sizeof (UINT4)) != 0)
                    {
                        OLS_RTR_LSA_ADD_LINK (pBuf, SrcIpAddr, gHostRouteMask,
                                              STUB_LINK, u2LinkCount);
                        *pLinkCount += 1;
                        pBuf = OlsRtrLsaAddCosts (pBuf, pInterface->aIfOpCost);
                    }
                }
                u1Flag = OSPF_TRUE;
            }
        }
    }
    if ((u1Flag == OSPF_TRUE) &&
        (NetIpv4IfIsOurAddressInCxt (pArea->pOspfCxt->u4OspfCxtId,
                                     OSPF_CRU_BMC_DWFROMPDU (pArea->pOspfCxt->
                                                             rtrId)) ==
         NETIPV4_SUCCESS))
    {
        /* the param u2LinkCount in the following macro is dummy */
        OLS_RTR_LSA_ADD_LINK (pBuf, pArea->pOspfCxt->rtrId, gHostRouteMask,
                              STUB_LINK, u2LinkCount);
        *pLinkCount += 1;

        pBuf = OlsRtrLsaAddCosts (pBuf, pInterface->aIfOpCost);
    }
    return pBuf;
}

/******************************************************************************

Function        :   OlsGetSummaryCost

Description     :   finds the cost based on tos specified and type of lsa

Input           :
                    u1Type         :   DEFAULT_NETWORK_SUM_LSA  or      
                                        COND_NETWORK_SUM_LSA or          
                                        NETWORK_SUM_LSA  or  
                                        ASBR_SUM_LSA
                    pStruct        :   pArea or
                                        pAddrRange or
                                        pSummaryParam 
                    u1Tos          :   tos

Output          :   None

Returns         :   cost,       if the type of LSA
                    FAILURE,    otherwise

******************************************************************************/

PRIVATE INT4
OlsGetSummaryCost (UINT1 *pStruct, UINT1 u1Type, UINT1 u1Tos)
{
    if (u1Tos >= OSPF_MAX_METRIC)    /* klocwork */
    {
        return OSPF_FAILURE;
    }

    switch (u1Type)
    {
        case COND_NETWORK_SUM_LSA:
            if (IS_VALID_ADDR_RANGE_COST (((tAddrRange *) (VOID *) pStruct),
                                          u1Tos))
                return ((INT4) ((tAddrRange *) (VOID *) pStruct)->
                        aMetric[u1Tos].u4Value);
            break;

        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
            if (IS_VALID_SUM_PARAM_COST (((tSummaryParam *) (VOID *) pStruct),
                                         u1Tos))
                return ((INT4) ((tSummaryParam *) (VOID *) pStruct)->
                        aMetric[u1Tos].u4Value);
            break;

        default:
            break;
    }

    return OSPF_FAILURE;
}

/******************************************************************************

Function        :   OlsIsEligibleToGenAseLsaInCxt

Description     :   the ASE lsa list is scanned to check whether a functionally
                    equivalent lsa which is generated by a router with a greater
                    router id exists. If that lsa exists, then it is ineligible
                    and eligible otherwise.

Input           :
                    pExtRt    :   ptr to the external route structure

Output          :   None

Returns         :   SUCCESS/FAILURE

******************************************************************************/

PUBLIC INT4
OlsIsEligibleToGenAseLsaInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRt)
{

#ifdef TOS_SUPPORT
    tExtLsaLink         extLsaLink;
    UINT1               u1Tos;
#endif
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tOsDbNode          *pOsDbNode = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OlsIsEligibleToGenAseLsaInCxt\n");
    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLsaNode);
                if ((UtilIpAddrComp (pOspfCxt->rtrId,
                                     pLsaInfo->lsaId.advRtrId) == OSPF_LESS) &&
                    IS_EQUAL_ADDR (pExtRt->ipNetNum,
                                   pLsaInfo->lsaId.linkStateId) &&
                    (RtcFindPath
                     (pOspfCxt->pOspfRt, &(pLsaInfo->lsaId.advRtrId),
                      DEST_AS_BOUNDARY, TOS_0, NULL) != NULL))
                {
                    /* check for the equivalent functionality */
#ifdef TOS_SUPPORT
                    for (u1Tos = 1; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                    {
                        RtcGetExtLsaLink (pLsaInfo->pLsa,
                                          pLsaInfo->u2LsaLen, u1Tos,
                                          &extLsaLink);
                        if (pExtRt->aMetric[u1Tos].u4Value != extLsaLink.u4Cost)
                        {
                            return SUCCESS;
                        }
                        if (IS_EQUAL_ADDR (extLsaLink.fwdAddr,
                                           pExtRt->extrtParam[u1Tos].fwdAddr))
                        {
                            if (IS_NULL_IP_ADDR (extLsaLink.fwdAddr))
                            {
                                return SUCCESS;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            return SUCCESS;
                        }
                    }
                    return OSPF_FAILURE;
#endif
                }
            }
        }
    }
    return SUCCESS;
}

/******************************************************************************

Function        :   OlsIsEligibleToGenNssaLsa

Description     :  Handles functionally equv Type 7 LSA
                        while originating Type 7 LSA

Input           :
                    pExtRt    :   ptr to the external route structure

Output          :   None

Returns         :   OSPF_TRUE/OSPF_FALSE

******************************************************************************/

PRIVATE INT4
OlsIsEligibleToGenNssaLsa (tLsaInfo * pLsaInfo, tArea * pArea)
{
    tLsaInfo           *pType7LsaInfo = NULL;
    tExtLsaLink         lsdbExtLsaLink;
    tExtLsaLink         nssaExtLsaLink;
    UINT1               u1Tos = 0;
    UINT1               u1Mtchs = 0;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tPath              *pAsbrPath;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsIsEligibleToGenNssaLsa\n");
    TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
    {
        pType7LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
        if (UtilIpAddrComp (pType7LsaInfo->lsaId.advRtrId,
                            pArea->pOspfCxt->rtrId) == OSPF_EQUAL)
            continue;

        if (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId,
                            pType7LsaInfo->lsaId.linkStateId) != OSPF_EQUAL)
            continue;

        if (IS_MAX_AGE (pType7LsaInfo->u2LsaAge))
            continue;

        pAsbrPath =
            RtcFindPath (pArea->pOspfCxt->pOspfRt,
                         &(pType7LsaInfo->lsaId.advRtrId), DEST_AS_BOUNDARY,
                         TOS_0, NULL);

        if (pAsbrPath == NULL)
            continue;
        u1Mtchs = 0;
        OSPF_TOS_LOOP (u1Tos)
        {
            RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                              u1Tos, &nssaExtLsaLink);
            RtcGetExtLsaLink (pType7LsaInfo->pLsa,
                              pType7LsaInfo->u2LsaLen, u1Tos, &lsdbExtLsaLink);

            if ((UtilIpAddrComp (nssaExtLsaLink.fwdAddr,
                                 lsdbExtLsaLink.fwdAddr) == OSPF_EQUAL) &&
                (nssaExtLsaLink.u4Cost == lsdbExtLsaLink.u4Cost) &&
                (nssaExtLsaLink.u1MetricType ==
                 lsdbExtLsaLink.u1MetricType) &&
                (UtilIpAddrComp (nssaExtLsaLink.fwdAddr, gNullIpAddr)
                 != OSPF_EQUAL) &&
                (UtilIpAddrComp (lsdbExtLsaLink.fwdAddr, gNullIpAddr) !=
                 OSPF_EQUAL))
            {
                u1Mtchs++;
            }
        }

        if (u1Mtchs == OSPF_MAX_METRIC)
        {
            if (IS_P_BIT_SET (pLsaInfo))
            {
                if (IS_P_BIT_SET (pType7LsaInfo))
                {
                    if (UtilIpAddrComp
                        (pType7LsaInfo->lsaId.advRtrId,
                         pLsaInfo->lsaId.advRtrId) == OSPF_GREATER)
                    {
                        return OSPF_FALSE;
                    }
                    else
                    {
                        return OSPF_TRUE;
                    }
                }
                else
                {
                    return OSPF_TRUE;
                }
            }
            else
            {
                if (IS_P_BIT_SET (pType7LsaInfo))
                {
                    return OSPF_FALSE;
                }
                else
                {
                    if (UtilIpAddrComp
                        (pType7LsaInfo->lsaId.advRtrId,
                         pLsaInfo->lsaId.advRtrId) == OSPF_GREATER)
                    {
                        return OSPF_FALSE;
                    }
                    else
                    {
                        return OSPF_TRUE;
                    }
                }
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsIsEligibleToGenNssaLsa\n");
    return OSPF_TRUE;
}

/******************************************************************************

Function        :   OlsBuildSummaryParam

Description     :   builds the summary param structure required for the
                    construction of the uncondensed summary lsas

Input           :
                    pArea          :   area to which the summarization to be 
                                        done 
                    pRtEntry      :   ptr to route entry structure which is 
                                        to be summarized

Output          :
                    pSummaryParam :   ptr to summary param structure which is
                                        getting filled up the routine

Returns         :   SUCCESS/FAILURE

******************************************************************************/

PUBLIC INT4
OlsBuildSummaryParam (tArea * pArea,
                      tRtEntry * pRtEntry, tSummaryParam * pSummaryParam)
{
    UINT1               u1Tos;
    tPath              *pPath;
    tPath              *pTos0Path = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsBuildSummaryParam\n");

    IP_ADDR_COPY (pSummaryParam->ipAddr, pRtEntry->destId);

    OSPF_CRU_BMC_DWTOPDU (pSummaryParam->ipAddrMask, pRtEntry->u4IpAddrMask);
    TMO_SLL_Scan (&(pRtEntry->aTosPath[TOS_0]), pPath, tPath *)
    {
        if (IS_EQUAL_ADDR (pPath->areaId, pArea->areaId))
        {
            /* path is associated with the same area */
            continue;
        }
        else
        {
            if (IS_AREA_TRANSIT_CAPABLE (pArea))
            {
                if ((UINT1) (pPath->aNextHops[0].u1Status) ==
                    OSPF_NEXT_HOP_VALID)
                {
                    if (IS_EQUAL_ADDR
                        (pPath->aNextHops[0].pInterface->pArea->areaId,
                         pArea->areaId))
                    {
                        continue;
                    }
                }
            }
            pTos0Path = pPath;
            pSummaryParam->aMetric[TOS_0].u4Value = pTos0Path->u4Cost;
            pSummaryParam->aMetric[TOS_0].bStatus = OSPF_VALID;
            break;
        }
    }
    if (pTos0Path == NULL)
    {
        /* 
         * all the tos 0 paths are associated with the same area 
         * and no need to summarize 
         */
        return OSPF_FAILURE;
    }
#ifdef TOS_SUPPORT
    for (u1Tos = 1; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pSummaryParam->aMetric[u1Tos].bStatus = OSPF_INVALID;
        TMO_SLL_Scan (&(pRtEntry->aTosPath[u1Tos]), pPath, tPath *)
        {
            if (IS_EQUAL_ADDR (pPath->areaId, pArea->areaId))
            {
                /* path is associated with the same area */
                continue;
            }
            else
            {
                if (IS_AREA_TRANSIT_CAPABLE (pArea))
                {
                    if ((UINT1) (pPath->aNextHops[0].u1Status) ==
                        OSPF_NEXT_HOP_VALID)
                    {
                        if (IS_EQUAL_ADDR
                            (pPath->aNextHops[0].pInterface->pArea->areaId,
                             pArea->areaId))
                        {
                            continue;
                        }
                    }

                }
                if (pPath->u1PathType == pTos0Path->u1PathType)
                {
                    pSummaryParam->aMetric[u1Tos].u4Value = pPath->u4Cost;
                }
                else
                {
                    pSummaryParam->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
                }
                pSummaryParam->aMetric[u1Tos].bStatus = OSPF_VALID;
                break;
            }
        }
    }
#else
    UNUSED_PARAM (u1Tos);
#endif
    pSummaryParam->options = pRtEntry->options;
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsBuildSummaryParam\n");
    return SUCCESS;
}

/******************************************************************************

Function        :   OlsGetLsaDescInCxt

Description     :   this is an utility routine returns the ptr to the lsa_desc.
                    if the lsa_desc doesnt exist, it is created.
                    if allocation fails, null is returned.

Input           :
                    u1LsaType         :  The LSA Type
                    pLinkStateId     :  The LinkState ID
                    pPtr                :  NULL, for TYPE11_OPQ_LSA and
                                                 AS_EXT_LSAs
                                           Pointer to the Interface for
                                                 TYPE9_OPQ_LSA
                                           Pointer to tArea for other LSA TYPES

Output          :   None

Returns         :   ptr to lsa_desc or null

******************************************************************************/
PUBLIC tLsaDesc    *
OlsGetLsaDescInCxt (tOspfCxt * pOspfCxt, UINT1 u1LsaType,
                    tLINKSTATEID * pLinkStateId, UINT1 *pPtr,
                    UINT1 *pu1NewDescFlag)
{

    tLsaDesc           *pLsaDesc;
    tLsaInfo           *pLsaInfo;
    UINT1              *pTmpPtr = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OlsGetLsaDescInCxt\n");

    OSPF_TRC2 (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
               "lsa_type : %d, linkStateId : %x",
               u1LsaType, OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pLinkStateId));
    if ((u1LsaType == TYPE11_OPQ_LSA) || (u1LsaType == AS_EXT_LSA))
    {
        pTmpPtr = (UINT1 *) pOspfCxt->pBackbone;
    }
    else
    {
        pTmpPtr = pPtr;
    }

    pLsaInfo = LsuSearchDatabase (u1LsaType, pLinkStateId,
                                  &(pOspfCxt->rtrId), pPtr, pTmpPtr);
    if (pLsaInfo != NULL)
    {
        /* Reset the GR bit in the LSA info structure. This indicates
         * that the LSA has been re-originated */
        LSA_REFRESH_BIT_RESET (pLsaInfo, OSPF_GR_LSA_BIT);
        if ((pLsaDesc = pLsaInfo->pLsaDesc) != NULL)
        {
            *(pu1NewDescFlag) = OSPF_FALSE;
            return pLsaDesc;
        }
        else
        {
            LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
        }

    }
    if (LSA_DESC_ALLOC (pLsaDesc) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "LSA Descr Alloc Failure\n");

        return (NULL);
    }

    OSPF_TRC (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId, "LSA_DESC created\n");

    TMO_DLL_Init_Node (&(pLsaDesc->lsaDescNode));
    pLsaDesc->pLsaInfo = NULL;
    pLsaDesc->pAssoPrimitive = NULL;
    pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
    pLsaDesc->u1LsaChanged = OSPF_FALSE;
    pLsaDesc->u1SeqNumWrapAround = OSPF_FALSE;
    pLsaDesc->u4LsaGenTime = 0;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: OlsGetLsaDescInCxt\n");
    *(pu1NewDescFlag) = OSPF_TRUE;
    return (pLsaDesc);
}

/*****************************************************************************

Function        :   OlsModifyLsId

Reference       :   RFC2178/Appendix E

Description     :   If the router had originated the same type of LSA with the
                    same ls_id and different mask, then the ls_id is
                    appropriately modified.

Input           :
                    pArea          :   area to which lsa is generated
                    u1InternalType:   internal type of lsa
                    pLsId         :   link state id
                    pPtr           :   pointer to the structure associated 
                                        with the lsa

Output          :   
                    pLsId         :   the modified link state id,
                                        if modification is forced

Returns         :   None

******************************************************************************/

PUBLIC VOID
OlsModifyLsId (tArea * pArea,
               UINT1 u1InternalType, tLINKSTATEID * pLsId, UINT1 *pPtr)
{
    UINT1               u1LsaType = 0;
    tLsaInfo           *pLsaInfo = NULL;
    UINT4               netMask1 = 0;    /* New LSAs Mask            */
    UINT4               netMask2 = 0;    /* Existing LSAs Mask       */
    tIPADDR             net1;
    tIPADDR             net2;
    UINT4               tmpLsId;
    UINT4               newLsId;
    tIPADDR             modLsId;
    tIPADDR             destIpAddrMask;
    tRtEntry           *pRtEntry = NULL;
    tPath              *pPath = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsModifyLsId\n");

    if ((u1InternalType == COND_NETWORK_SUM_LSA) ||
        (u1InternalType == NETWORK_SUM_LSA))
    {
        u1LsaType = NETWORK_SUM_LSA;
    }
    else if ((u1InternalType == AS_TRNSLTD_EXT_LSA) ||
             (u1InternalType == AS_TRNSLTD_EXT_RNG_LSA) ||
             (u1InternalType == COND_AS_EXT_LSA) ||
             (u1InternalType == AS_EXT_LSA))
    {
        u1LsaType = AS_EXT_LSA;
    }
    else if ((u1InternalType == COND_NSSA_LSA) || (u1InternalType == NSSA_LSA))
    {
        u1LsaType = NSSA_LSA;
    }
    else
    {
        return;
    }

    pLsaInfo =
        LsuSearchDatabase (u1LsaType, pLsId, &(pArea->pOspfCxt->rtrId), NULL,
                           (UINT1 *) pArea);
    if ((pLsaInfo == NULL) || (pLsaInfo->pLsaDesc == NULL))
    {
        return;
    }

    /* New LSAs mask is extracted    */
    switch (u1InternalType)
    {
        case NETWORK_SUM_LSA:
            netMask1 =
                OSPF_CRU_BMC_DWFROMPDU (((tSummaryParam *) (VOID *) pPtr)->
                                        ipAddrMask);
            break;

        case COND_NETWORK_SUM_LSA:
        case AS_TRNSLTD_EXT_RNG_LSA:
            netMask1 =
                OSPF_CRU_BMC_DWFROMPDU (((tAddrRange *) (VOID *) pPtr)->
                                        ipAddrMask);
            break;

        case AS_EXT_LSA:
        case NSSA_LSA:
            netMask1 =
                OSPF_CRU_BMC_DWFROMPDU (((tExtRoute *) (VOID *) pPtr)->
                                        ipAddrMask);
            break;

        case AS_TRNSLTD_EXT_LSA:
            if ((pRtEntry = (tRtEntry *) (VOID *) pPtr) == NULL)
            {
                return;
            }
            if ((pPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
            {
                return;
            }
            OS_MEM_CPY (destIpAddrMask,
                        (UINT1 *) (pPath->pLsaInfo->pLsa + LS_HEADER_SIZE),
                        MAX_IP_ADDR_LEN);
            netMask1 = OSPF_CRU_BMC_DWFROMPDU (destIpAddrMask);
            break;

        case COND_AS_EXT_LSA:
        case COND_NSSA_LSA:
        default:
            netMask1 =
                OSPF_CRU_BMC_DWFROMPDU (((tAsExtAddrRange *) (VOID *) pPtr)->
                                        ipAddrMask);
            break;
    }

    netMask2 = OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pLsa + LS_HEADER_SIZE);
    OSPF_CRU_BMC_DWTOPDU (net1, netMask1);
    OSPF_CRU_BMC_DWTOPDU (net2, netMask2);

    if (UtilIpAddrComp (net1, net2) == OSPF_EQUAL)
    {
        return;
    }

    tmpLsId = OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pLsId);
    if (UtilIpAddrComp (net1, net2) == OSPF_GREATER)
    {
        tmpLsId |= (~netMask1);
    }
    else
    {
        /* 
         * change the existing LSA to reference the new network 
         * [lsid, netMask1]
         */
        switch (u1InternalType)
        {
            case NETWORK_SUM_LSA:
            case COND_NETWORK_SUM_LSA:
            case AS_TRNSLTD_EXT_RNG_LSA:
            case AS_EXT_LSA:
            case NSSA_LSA:
            case AS_TRNSLTD_EXT_LSA:
            case COND_AS_EXT_LSA:
            case COND_NSSA_LSA:
            default:
                newLsId =
                    OSPF_CRU_BMC_DWFROMPDU ((pLsaInfo->lsaId.linkStateId));
                newLsId |= (~netMask2);
                OSPF_CRU_BMC_DWTOPDU (modLsId, newLsId);
                break;
        }

        /* originate a new LSA for the old network 
         * [lsid, netMask2] 
         */
        if (pLsaInfo->u2LsaAge == MAX_AGE)
        {
            LsuDeleteNodeFromAllRxmtLst (pLsaInfo);
            pLsaInfo->u1LsaFlushed = OSPF_FALSE;
        }
        else
        {
            OlsGenerateLsa (pLsaInfo->pArea,
                            pLsaInfo->pLsaDesc->u1InternalLsaType,
                            &(modLsId),
                            (UINT1 *) pLsaInfo->pLsaDesc->pAssoPrimitive);
        }
    }

    OSPF_CRU_BMC_DWTOPDU ((UINT1 *) pLsId, tmpLsId);

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsModifyLsId\n");
    return;
}

/******************************************************************************

Function        :   OlsIsTransitLink

Description     :   checks the transit bStatus of the interface

Input           :
                    pInterface :   interface whose bStatus is to be tested

Output          :   None

Returns         :   OSPF_TRUE,  if the link is a transit link
                    OSPF_FALSE, otherwise   

******************************************************************************/

PRIVATE UINT1
OlsIsTransitLink (tInterface * pInterface)
{
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsIsTransitLink\n");
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

        if (IS_NBR_FULL (pNbr))
        {

            if (IS_DR (pInterface))
            {
                return OSPF_TRUE;
            }
            if (IS_EQUAL_ADDR (GET_DR (pInterface), pNbr->nbrIpAddr))
            {
                return OSPF_TRUE;
            }
        }
    }
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OlsifParamChange                                        */
/*                                                                           */
/* Description  : This routine invokes the area, host and extrt associated   */
/*                parameter modification routines. This is done based on the */
/*                LSA type.                                                  */
/*                                                                           */
/* Input        : pStruct     :  Pointer to UINT1, suitably typecast based  */
/*                                on the LSA type.                           */
/*                i1LsaType  :  The type of LSA.                           */
/*                                AS_EXT_LSA                                 */
/*                                ROUTER_LSA                                 */
/*                                DEFAULT_NETWORK_SUM_LSA                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OlsifParamChangeInCxt (tOspfCxt * pOspfCxt, UINT1 *pStruct, INT1 i1LsaType)
{
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* LSA cannot be (re)generated in GR mode */
        return OSPF_SUCCESS;
    }
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return OSPF_SUCCESS;
    }
    switch (i1LsaType)
    {
        case AS_EXT_LSA:
            ExtrtParamChangeInCxt (pOspfCxt, (tExtRoute *) (VOID *) pStruct);
            break;

        case ROUTER_LSA:
            HostIfmtrcPrmChange ((tArea *) (VOID *) pStruct);
            break;

        case DEFAULT_NETWORK_SUM_LSA:
            AreaParamChange ((tArea *) (VOID *) pStruct);
            break;

        default:
            return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/************************************************************************/
/*                                      */
/* Function        : OlsConstructType7Lsa              */
/*                                         */
/* Description      : This routine constructs Type 7 LSA         */
/*                                      */
/* Input              : pPtr - Pointer to struct associated with LSA     */
/*                         : u1InternalType - LSA Type             */
/*                     : pArea - Pointer to NSSA area            */
/*                         : seqNum - LSA sequence number            */
/*                                       */
/* Output           : pLen - Length of Constructed LSA         */
/*                                         */
/* Returns            : Pointer to Type 7 LSA if constructed      */
/*                        successfully else NULL             */
/*                                      */
/************************************************************************/
PRIVATE UINT1      *
OlsConstructType7Lsa (UINT1 *pPtr, UINT1 u1InternalType, tArea * pArea,
                      tLSASEQNUM seqNum, UINT2 *pLen, tLINKSTATEID * pLsId)
{
    tOPTIONS            options = OSPF_EQUAL;
    tTRUTHVALUE         bTBitFlag = OSPF_FALSE;
    UINT1              *pType7Lsa = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               u1Tos = 0;
    UINT1               u1EBitTos = OSPF_EQUAL;
    tExtRoute          *pExtRoute = NULL;
    tAsExtAddrRange    *pAsExtRange = NULL;
    tIPADDRMASK         ipAddrMask;
    tLINKSTATEID        linkStateId;
    UINT4               u4Cost;
    tLsaInfo           *pLsaInfo = NULL;
    UINT1               u1FwdAddrSet = OSPF_FALSE;
    UINT1               u1Tos0Fwd = OSPF_FALSE;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC:OlsConstructType7Lsa\n");
    LSA_ALLOC (NSSA_LSA, pType7Lsa, MAX_EXT_LSA_SIZE);
    if (pType7Lsa == NULL)
    {
        OSPF_TRC (OSPF_MEM_FAIL | OSPF_CRITICAL_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "LSA_ALLOC FAILURE\n");
        OSPF_TRC (OS_RESOURCE_TRC
                  || OSPF_LSU_TRC | OSPF_CRITICAL_TRC,
                  pArea->pOspfCxt->u4OspfCxtId,
                  "N/W Type7 LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pType7Lsa;

    /* leave space for header */
    OS_MEM_SET (pType7Lsa, OSPF_EQUAL, LS_HEADER_SIZE);
    pCurrent += LS_HEADER_SIZE;

    if (pArea->pOspfCxt->bDemandExtension == OSPF_TRUE)
    {
        options |= DC_BIT_MASK;
    }

    if (u1InternalType == COND_NSSA_LSA)
    {
        pAsExtRange = (tAsExtAddrRange *) (VOID *) pPtr;
        LADDSTR (pCurrent, pAsExtRange->ipAddrMask, MAX_IP_ADDR_LEN);
        bTBitFlag = OSPF_FALSE;

        /* TosSupport */
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            u1FwdAddrSet = OSPF_FALSE;
            if (pAsExtRange->aMetric[u1Tos].bStatus == OSPF_VALID)
            {
                u1EBitTos =
                    ((pAsExtRange->aMetric[u1Tos].u4MetricType ==
                      TYPE_1_METRIC) ? OSPF_EQUAL : EXT_LSA_METRIC_TYPE_MASK);
                u1EBitTos = (UINT1) (u1EBitTos | ENCODE_TOS (u1Tos));
                LADD1BYTE (pCurrent, u1EBitTos);
                LADD3BYTE (pCurrent, pAsExtRange->aMetric[u1Tos].u4Value);
                /* forwarding addr */
                /* Set-Clear "P" bit in the Options field */

                u1FwdAddrSet = OlsSetType7FwdAddr (pArea, pCurrent);
                if (u1Tos == TOS_0)
                {
                    u1Tos0Fwd = u1FwdAddrSet;
                }

                pCurrent += MAX_IP_ADDR_LEN;
                /* Set Ext Rt Tag to 0 for aggregated route */
                LADD4BYTE (pCurrent, OSPF_EQUAL);
#ifdef TOS_SUPPORT
                if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                {
                    bTBitFlag = OSPF_TRUE;
                }
#endif
            }
            else if (u1Tos == TOS_0)
            {
                /* Without TOS 0, Generating LSA is useless */
                LSA_FREE (NSSA_LSA, pType7Lsa);
                return NULL;
            }

        }

        /* TOS bit is set in Options field, if multiple TOS reachability
         * info is passed in this LSA */
        pLsaInfo = LsuSearchDatabase (AS_EXT_LSA,
                                      pLsId, &(pArea->pOspfCxt->rtrId),
                                      NULL,
                                      (UINT1 *) pArea->pOspfCxt->pBackbone);

        if ((pAsExtRange->u1AggTranslation == OSPF_TRUE) && (pLsaInfo == NULL)
            && (u1Tos0Fwd == OSPF_TRUE))
            options = options | P_BIT_MASK;

        options |= ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : OSPF_EQUAL);
        *pLen = (UINT2) (pCurrent - pType7Lsa);
        OlsConstructLsHeaderInCxt (pArea->pOspfCxt, pType7Lsa,
                                   *pLen, NSSA_LSA, pLsId, seqNum, options);

    }
    else if (u1InternalType == NSSA_LSA)
    {
        pExtRoute = (tExtRoute *) (VOID *) pPtr;
        LADDSTR (pCurrent, pExtRoute->ipAddrMask, MAX_IP_ADDR_LEN);
        bTBitFlag = OSPF_FALSE;
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            u1FwdAddrSet = OSPF_FALSE;
            if (pExtRoute->aMetric[u1Tos].rowStatus == ACTIVE)
            {
                /* type of aMetric E-bit in TOS */

                u1EBitTos =
                    ((pExtRoute->extrtParam[u1Tos].u1MetricType ==
                      TYPE_1_METRIC) ? OSPF_EQUAL : EXT_LSA_METRIC_TYPE_MASK);

                u1EBitTos |= ENCODE_TOS (u1Tos);

                LADD1BYTE (pCurrent, u1EBitTos);

                /* aMetric value */
                LADD3BYTE (pCurrent, pExtRoute->aMetric[u1Tos].u4Value);

                /* forwarding addr */
                if (UtilIpAddrComp (pExtRoute->extrtParam[u1Tos].fwdAddr,
                                    gNullIpAddr) == OSPF_EQUAL)
                {
                    u1FwdAddrSet = OlsSetType7FwdAddr (pArea, pCurrent);
                    pCurrent += MAX_IP_ADDR_LEN;
                }
                else
                {
                    /* fwdAddrUpdate */
                    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode,
                                  tTMO_SLL_NODE *)
                    {
                        pInterface = GET_IF_PTR_FROM_LST (pLstNode);
                        if (UtilIpAddrMaskComp
                            (pInterface->ifIpAddr,
                             pExtRoute->extrtParam[u1Tos].fwdAddr,
                             pInterface->ifIpAddrMask) == OSPF_EQUAL)
                        {
                            u1FwdAddrSet = OSPF_TRUE;
                            LADDSTR (pCurrent,
                                     pExtRoute->extrtParam[u1Tos].fwdAddr,
                                     MAX_IP_ADDR_LEN);
                            break;
                        }
                    }

                    if (u1FwdAddrSet == OSPF_FALSE)
                    {
                        u1FwdAddrSet = OlsSetType7FwdAddr (pArea, pCurrent);
                        pCurrent += MAX_IP_ADDR_LEN;
                    }
                }

                /* ext route tag */
                LADD4BYTE (pCurrent, pExtRoute->extrtParam[u1Tos].u4ExtTag);
#ifdef TOS_SUPPORT
                if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                {
                    bTBitFlag = OSPF_TRUE;
                }
#endif
                if (u1Tos == TOS_0)
                {
                    u1Tos0Fwd = u1FwdAddrSet;
                }
            }
            else if (u1Tos == TOS_0)
            {
                /* Without TOS 0, Generating LSA is useless */
                LSA_FREE (NSSA_LSA, pType7Lsa);
                return NULL;
            }
        }

        /* TOS bit is set in Options field, if multiple TOS reachability
         * info is passed in this LSA */
        options |= ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : TOS_0);
        /* Set P bit in the Options field */
        if (LsuSearchDatabase (AS_EXT_LSA, pLsId,
                               &(pArea->pOspfCxt->rtrId),
                               NULL,
                               (UINT1 *) pArea->pOspfCxt->pBackbone) == NULL)
        {
            if (u1Tos0Fwd == OSPF_TRUE)
                options |= P_BIT_MASK;
        }

        *pLen = (UINT2) (pCurrent - pType7Lsa);
        OlsConstructLsHeaderInCxt (pArea->pOspfCxt, pType7Lsa, *pLen, NSSA_LSA,
                                   pLsId, seqNum, options);

    }
    else
    {
        if (pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {
            IP_ADDR_COPY (ipAddrMask, gNullIpAddr);
            IP_ADDR_COPY (linkStateId, gDefaultDest);
            LADDSTR (pCurrent, ipAddrMask, MAX_IP_ADDR_LEN);

            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                if (pArea->aStubDefaultCost[u1Tos].rowStatus == ACTIVE)
                {
                    if (pArea->aStubDefaultCost[u1Tos].u4MetricType ==
                        TYPE1EXT_METRIC)
                        u1EBitTos = TOS_0;
                    else if (pArea->aStubDefaultCost[u1Tos].u4MetricType
                             == TYPE2EXT_METRIC)
                        u1EBitTos = EXT_LSA_METRIC_TYPE_MASK;
                    else
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC,
                                  pArea->pOspfCxt->u4OspfCxtId,
                                  "Type7 DF LSA not constructed - Metric Type Invalid\n");
                        LSA_FREE (NSSA_LSA, pType7Lsa);
                        return NULL;
                    }

                    u1EBitTos = (UINT1) (u1EBitTos | ENCODE_TOS (u1Tos));
                    LADD1BYTE (pCurrent, u1EBitTos);
                    if (IS_VALID_STUB_AREA_COST (pArea, u1Tos))
                        u4Cost = pArea->aStubDefaultCost[u1Tos].u4Value;
                    else
                        u4Cost = LS_INFINITY_24BIT;
                    LADD3BYTE (pCurrent, u4Cost);
                    /* Fwd Addr */
                    LADDSTR (pCurrent, gNullIpAddr, MAX_IP_ADDR_LEN);
                    /* Ext Tag */
                    /* Set Ext Rt Tag to 0 for default route */
                    LADD4BYTE (pCurrent, OSPF_EQUAL);
#ifdef TOS_SUPPORT
                    if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                    {
                        bTBitFlag = OSPF_TRUE;
                    }
#endif
                }
            }
            options |= ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : TOS_0);
            *pLen = (UINT2) (pCurrent - pType7Lsa);

            OlsConstructLsHeaderInCxt (pArea->pOspfCxt, pType7Lsa, *pLen,
                                       NSSA_LSA, &(linkStateId), seqNum,
                                       options);
        }
        else
        {
            pExtRoute = GetFindExtRouteInCxt (pArea->pOspfCxt,
                                              &gNullIpAddr, &gNullIpAddr);
            if (pExtRoute == NULL)
            {
                LSA_FREE (NSSA_LSA, pType7Lsa);
                return NULL;
            }

            LADDSTR (pCurrent, pExtRoute->ipAddrMask, MAX_IP_ADDR_LEN);
            bTBitFlag = OSPF_FALSE;
            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                u1FwdAddrSet = OSPF_FALSE;
                if (pExtRoute->aMetric[u1Tos].rowStatus == ACTIVE)
                {

                    /* type of aMetric E-bit in TOS */

                    u1EBitTos =
                        ((pExtRoute->extrtParam[u1Tos].u1MetricType ==
                          TYPE_1_METRIC) ? TOS_0 : EXT_LSA_METRIC_TYPE_MASK);

                    u1EBitTos |= ENCODE_TOS (u1Tos);

                    LADD1BYTE (pCurrent, u1EBitTos);

                    /* aMetric value */

                    LADD3BYTE (pCurrent, pExtRoute->aMetric[u1Tos].u4Value);

                    /* forwarding addr */
                    if (UtilIpAddrComp (pExtRoute->extrtParam[u1Tos].fwdAddr,
                                        gNullIpAddr) == OSPF_EQUAL)
                    {

                        u1FwdAddrSet = OlsSetType7FwdAddr (pArea, pCurrent);
                        pCurrent += MAX_IP_ADDR_LEN;
                    }
                    else
                    {
                        /* fwdAddrUpdate */
                        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode,
                                      tTMO_SLL_NODE *)
                        {
                            pInterface = GET_IF_PTR_FROM_LST (pLstNode);
                            if (UtilIpAddrMaskComp
                                (pInterface->ifIpAddr,
                                 pExtRoute->extrtParam[u1Tos].fwdAddr,
                                 pInterface->ifIpAddrMask) == OSPF_EQUAL)
                            {
                                u1FwdAddrSet = OSPF_TRUE;
                                LADDSTR (pCurrent,
                                         pExtRoute->extrtParam[u1Tos].fwdAddr,
                                         MAX_IP_ADDR_LEN);
                                break;
                            }
                        }

                        if (u1FwdAddrSet == OSPF_FALSE)
                        {
                            u1FwdAddrSet = OlsSetType7FwdAddr (pArea, pCurrent);
                            pCurrent += MAX_IP_ADDR_LEN;
                        }
                    }

                    /* ext route tag */
                    LADD4BYTE (pCurrent, pExtRoute->extrtParam[u1Tos].u4ExtTag);
#ifdef TOS_SUPPORT
                    if ((u1Tos & EXT_LSA_TOS_MASK) != TOS_0)
                    {
                        bTBitFlag = OSPF_TRUE;
                    }
#endif
                    if (u1Tos == TOS_0)
                    {
                        u1Tos0Fwd = u1FwdAddrSet;
                    }
                }
                else if (u1Tos == TOS_0)
                {
                    /* Without TOS 0, Generating LSA is useless */
                    LSA_FREE (NSSA_LSA, pType7Lsa);
                    return NULL;
                }
            }

            /* TOS bit is set in Options field, if multiple TOS reachability
             * info is passed in this LSA */
            options |= ((bTBitFlag == OSPF_TRUE) ? T_BIT_MASK : TOS_0);

            /* Set P bit in the Options field */

            if ((pArea->pOspfCxt->bNssaAsbrDefRtTrans == OSPF_TRUE) &&
                (u1Tos0Fwd == OSPF_TRUE))
            {
                options |= P_BIT_MASK;
            }

            *pLen = (UINT2) (pCurrent - pType7Lsa);
            OlsConstructLsHeaderInCxt (pArea->pOspfCxt, pType7Lsa, *pLen,
                                       NSSA_LSA, &(pExtRoute->ipNetNum), seqNum,
                                       options);
        }
    }
    OSPF_TRC (CONTROL_PLANE_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Type7 LSA Constructed\n");
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT:OlsConstructType7Lsa\n");
    return pType7Lsa;
}

/************************************************************************/
/*                                                     */
/* Function    : OlsResetType7FwdAddr                  */
/*                                       */
/* Description     : This routine recalculates Fwd address for those     */
/*          Type 7 LSAs for whom  interface acting as  fwd     */
/*                address transitions to down state             */
/*                                     */
/* Input           : pInterface - Pointer to interface structure         */
/*                                     */
/* Output          : None                            */
/*                                        */
/* Returns         : None                            */
/*                                       */
/************************************************************************/
PUBLIC VOID
OlsResetType7FwdAddr (tInterface * pInterface)
{
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tIPADDR             fwdAddress;
    UINT1               u1FwdAddrRes = NOT_EQUAL;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsResetType7FwdAddr\n");
    if (pInterface->pArea->u4AreaType != NSSA_AREA)
    {
        OSPF_TRC1 (OSPF_LSU_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "pInterface : %x, attached to area type NORMAL",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));
        return;
    }
    else
    {
        TMO_SLL_Scan (&(pInterface->pArea->nssaLSALst),
                      pLsaNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
            /* nssaLSALst contains self originated LSAs and LSAs generated by other
             * routers also. Regeneration should be done for self originated
             * LSAs only. */
            if (pLsaInfo->pLsaDesc != NULL)
            {
                OS_GET_FWD_ADDR (pLsaInfo->pLsa, TOS_0, fwdAddress);
                /* In case of unnumbered interface, Router ID needs to be compared with 
                 * Forward Address. Because in unnumbered interface IP Address is NULL.
                 * */
                if (pInterface->u4AddrlessIf == 0)
                {
                    u1FwdAddrRes =
                        UtilIpAddrComp (fwdAddress, pInterface->ifIpAddr);
                }
                else
                {
                    u1FwdAddrRes =
                        UtilIpAddrComp (fwdAddress,
                                        pInterface->pArea->pOspfCxt->rtrId);
                }
                if (u1FwdAddrRes == OSPF_EQUAL)
                {
                    pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
                    OlsSignalLsaRegenInCxt (pLsaInfo->pOspfCxt,
                                            SIG_NEXT_INSTANCE,
                                            (UINT1 *) pLsaInfo->pLsaDesc);
                }
            }
        }
    }
}

/************************************************************************/
/*                                                        */
/* Function        : OlsGenerateNssaDfLsa              */
/*                                          */
/* Description      : Generates Type 7 Default LSA in NSSA area     */
/*                                           */
/* Input               : pArea - Pointer to area structure         */
/*            : u1InternalType - LSA Type            */
/*                     : pLsId - Link state ID             */
/*                                       */
/* Output           : None                        */
/*                                      */
/* Returns          : None                        */
/*                                           */
/************************************************************************/
PUBLIC VOID
OlsGenerateNssaDfLsa (tArea * pArea, UINT1 u1InternalType, tLINKSTATEID * pLsId)
{
    tExtRoute          *pExtRoute = NULL;

    /* If the router is ABR and ASBR then Generate Type7 LSA with Default route.
     * If the router is only ABR, also then Generate the Type7 LSA with Default route.
     * If the router is only ASBR, then Check whether the default route is present 
     * in the External routing Table. If present then Generate the Type7 LSA with the default route.
     * if not present then return. */

    /*  The core logic inside this function is as per the following table
     *           ABR | ASBR  
     0  |  0 --> Do Nothing.
     -------
     0  |  1 --> Look External IP Table
     -------
     1  |  0 --> Generate Type 7 LSA
     -------
     1  |  1 --> Generate Type 7 LSA */

/* If the Router is not ABR and ASBR then do nothing,return*/
    if ((pArea->pOspfCxt->bAreaBdrRtr != OSPF_TRUE) &&
        (pArea->pOspfCxt->bAsBdrRtr != OSPF_TRUE))
    {
        return;
    }

    if (pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
    {
        OlsGenerateLsa (pArea, u1InternalType, pLsId, (UINT1 *) pArea);
    }
    else
    {
        /* Check whether route is present in EXT RT table       */
        if ((pExtRoute = GetFindExtRouteInCxt (pArea->pOspfCxt,
                                               &gNullIpAddr,
                                               &gNullIpAddr)) == NULL)
        {
            return;
        }
        OlsGenerateLsa (pArea, u1InternalType, pLsId, (UINT1 *) pExtRoute);
    }
}

/************************************************************************/
/*                                       */
/* Function        : OlsHandleFuncEqvType7                */
/*                                        */
/* Description      : Generates Type 7 Default LSA in NSSA area     */
/*                                       */
/* Input               : pLsa - Pointer to received NSSA LSA         */
/*            : pNbr - Nbr who sent this LSA             */
/*                                          */
/* Output           : None                        */
/*                                           */
/* Returns          : VOID                        */
/*                                         */
/************************************************************************/
PUBLIC VOID
OlsHandleFuncEqvType7 (UINT1 *pLsa, tNeighbor * pNbr)
{
    tLsHeader           lsHeader;
    tLsaInfo           *pLsaInfo = NULL;
    tExtLsaLink         extLsaLink;
    tExtLsaLink         lsdbExtLsaLink;
    UINT4               lsaMask;
    tIPADDR             linkMask;
    tOspfCxt           *pOspfCxt = NULL;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: OlsHandleFuncEqvType7\n");

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;
    /* extract ls header info */
    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    lsaMask = OSPF_CRU_BMC_DWFROMPDU (pLsa + LS_HEADER_SIZE);
    OSPF_CRU_BMC_DWTOPDU (linkMask, lsaMask);
    pLsaInfo = LsuDatabaseLookUp (lsHeader.u1LsaType,
                                  &(lsHeader.linkStateId), &(linkMask),
                                  &(pOspfCxt->rtrId),
                                  (UINT1 *) pNbr->pInterface,
                                  (UINT1 *) pNbr->pInterface->pArea);

    if (pLsaInfo != NULL)
    {
        OS_MEM_CPY ((UINT1 *) &(extLsaLink.fwdAddr),
                    (UINT1 *) (pLsa + LS_HEADER_SIZE +
                               FWD_ADDR_OFFSET_IN_EXT_LSA), MAX_IP_ADDR_LEN);

        OS_MEM_CPY ((UINT1 *) &(extLsaLink.u4Cost),
                    (UINT1 *) (pLsa + LS_HEADER_SIZE +
                               MAX_IP_ADDR_LEN),
                    METRIC_TYPE_SIZE_IN_EXT_LSA + METRIC_SIZE_IN_EXT_LSA);

        OS_MEM_CPY ((UINT1 *) &(lsdbExtLsaLink.fwdAddr),
                    (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE +
                               FWD_ADDR_OFFSET_IN_EXT_LSA), MAX_IP_ADDR_LEN);

        OS_MEM_CPY ((UINT1 *) &(lsdbExtLsaLink.u4Cost),
                    (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE +
                               MAX_IP_ADDR_LEN),
                    METRIC_TYPE_SIZE_IN_EXT_LSA + METRIC_SIZE_IN_EXT_LSA);
        if ((UtilIpAddrComp
             (extLsaLink.fwdAddr,
              lsdbExtLsaLink.fwdAddr) == OSPF_EQUAL)
            && (extLsaLink.u4Cost == lsdbExtLsaLink.u4Cost)
            && (UtilIpAddrComp
                (extLsaLink.fwdAddr, gNullIpAddr) != OSPF_EQUAL)
            && (UtilIpAddrComp
                (lsdbExtLsaLink.fwdAddr, gNullIpAddr) != OSPF_EQUAL))
        {
            if (IS_P_BIT_SET (pLsaInfo))
            {
                if (lsHeader.lsaOptions & P_BIT_MASK)
                {
                    if (UtilIpAddrComp
                        (lsHeader.advRtrId,
                         pLsaInfo->lsaId.advRtrId) == OSPF_GREATER)
                    {
                        AgdFlushOut (pLsaInfo);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                if (lsHeader.lsaOptions & P_BIT_MASK)
                {
                    AgdFlushOut (pLsaInfo);
                }
                else
                {
                    if (UtilIpAddrComp
                        (lsHeader.advRtrId,
                         pLsaInfo->lsaId.advRtrId) == OSPF_GREATER)
                    {
                        AgdFlushOut (pLsaInfo);
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }

    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: OlsHandleFuncEqvType7\n");
    return;
}

/************************************************************************/
/*                                                               */
/* Function    : OlsSetType7FwdAddr                     */
/*                                         */
/* Description     : This routine calculates and sets Fwd address         */
/*           for Type 7 LSA                     */
/*                                       */
/* Input        : pArea - Pointer to NSSA area                 */
/*          pType7Lsa - Pointer to Type 7 LSA             */
/*                                                            */
/* Output       : None                            */
/*                                            */
/* Returns      : VOID                            */
/*                                       */
/************************************************************************/
PRIVATE UINT1
OlsSetType7FwdAddr (tArea * pArea, UINT1 *pType7Lsa)
{
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1FwdAddrSet = OSPF_FALSE;

/* This function sets the fwd address to one of the following                 */
/* 1) IF Loop back address support is there, fwd address is set to loopback.     */
/* Currently we do not support loop back address, so this case is not needed.    */
/* 2) Fwd Address is active NSSA interface in NSSA.                        */
/* 3) Active OSPF Stub network address                                */
/* 4) Fwd address set to 0.0.0.0                                        */

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "OlsSetType7FwdAddr\n");

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pLstNode);

        /* FwdAddrUpdate */
        if ((OlsIsTransitLink (pInterface) != OSPF_TRUE) &&
            (pInterface->u1IsmState != IFS_DOWN))
        {
            /* Incase of Unnumbered interface, Router ID need to be filled 
             * as Forward Address 
             */
            if (pInterface->u4AddrlessIf == 0)
            {
                LADDSTR (pType7Lsa, pInterface->ifIpAddr, MAX_IP_ADDR_LEN);
                u1FwdAddrSet = OSPF_TRUE;
                break;
            }
            else
            {
                LADDSTR (pType7Lsa, pArea->pOspfCxt->rtrId, MAX_IP_ADDR_LEN);
                u1FwdAddrSet = OSPF_TRUE;
                break;
            }
        }
    }

    if (u1FwdAddrSet == OSPF_FALSE)
    {
        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_LST (pLstNode);
            /* FwdAddrUpdate */
            if (pInterface->u1IsmState != IFS_DOWN)
            {
                /* Incase of Unnumbered interface, Router ID need to be filled 
                 * as Forward Address 
                 */
                if (pInterface->u4AddrlessIf == 0)
                {
                    LADDSTR (pType7Lsa, pInterface->ifIpAddr, MAX_IP_ADDR_LEN);
                    u1FwdAddrSet = OSPF_TRUE;
                    break;
                }
                else
                {
                    LADDSTR (pType7Lsa, pArea->pOspfCxt->rtrId,
                             MAX_IP_ADDR_LEN);
                    u1FwdAddrSet = OSPF_TRUE;
                    break;
                }
            }
        }
    }

    if (u1FwdAddrSet == OSPF_FALSE)
    {
        LADDSTR (pType7Lsa, gNullIpAddr, MAX_IP_ADDR_LEN);
    }
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "OlsSetType7FwdAddr\n");
    return u1FwdAddrSet;
}

/************************************************************************/
/*                                        */
/* Function    : OlsModifyNssaAsbrDefRtTransInCxt               */
/*                                       */
/* Description     : Processes change in P bit setting of default  Type 7     */
/*           LSA generated    by NSSA ASBR (not ABR)             */
/*                                     */
/* Input         : None                                  */
/*                                         */
/* Output          : None                            */
/*                                      */
/* Returns         : VOID                            */
/*                                      */
/************************************************************************/
PUBLIC INT4
OlsModifyNssaAsbrDefRtTransInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    tLsaInfo           *pLsaInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "OlsModifyNssaAsbrDefRtTransInCxt\n");
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* LSA's Cannot be (e)orginated in graceful restart mode */
        return OSPF_SUCCESS;
    }
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return OSPF_SUCCESS;
    }
    if ((pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        || !(pOspfCxt->bAsBdrRtr == OSPF_TRUE))
    {
        return OSPF_SUCCESS;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType == NSSA_AREA)
        {
            if ((pLsaInfo =
                 LsuSearchDatabase (NSSA_LSA,
                                    &gDefaultDest,
                                    &(pOspfCxt->rtrId), (UINT1 *) NULL,
                                    (UINT1 *) pArea)) != NULL)
            {

                pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
                OlsSignalLsaRegenInCxt (pOspfCxt, SIG_NEXT_INSTANCE,
                                        (UINT1 *) pLsaInfo->pLsaDesc);
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "OlsModifyNssaAsbrDefRtTransInCxt\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OlsTrapCheckLsuChange                                      */
/*                                                                           */
/* Description  : This procedure compares the database copy with the         */
/*                received lsa & returns OSPF_TRUE if there is any change    */
/*                else returns OSPF_FALSE                                    */
/*                                                                           */
/* Input        : pDbLsa   : Lsa present in the database.                  */
/*                pRcvdLsa : Received LSA                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE : If changed.                                    */
/*                OSPF_FALSE : If not changed                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
OlsTrapCheckLsuChange (UINT1 *pDbLsa, UINT1 *pRcvdLsa)
{
    tLsHeader           dbLsHeader;
    tLsHeader           rcvdLsHeader;
    UINT4               u4LsaBodyLen;

    UtilExtractLsHeaderFromLbuf (pDbLsa, &dbLsHeader);
    UtilExtractLsHeaderFromLbuf (pRcvdLsa, &rcvdLsHeader);

    /* check for any change in LSAs option field */
    if (dbLsHeader.lsaOptions != rcvdLsHeader.lsaOptions)
    {
        return OSPF_TRUE;
    }

    /* If any one of the lsas age is set to Max age */
    if ((IS_MAX_AGE (dbLsHeader.u2LsaAge)) ||
        (IS_MAX_AGE (rcvdLsHeader.u2LsaAge)))
    {
        return OSPF_FALSE;
    }

    /* check for change in the length field of ls header */
    if (dbLsHeader.u2LsaLen != rcvdLsHeader.u2LsaLen)
    {
        return OSPF_TRUE;
    }

    u4LsaBodyLen = (UINT4) (dbLsHeader.u2LsaLen - LS_HEADER_SIZE);

    /* check for any change in the body of the lsa */
    if ((OS_MEM_CMP (((pDbLsa) + LS_HEADER_SIZE),
                     ((pRcvdLsa) + LS_HEADER_SIZE),
                     u4LsaBodyLen)) != OSPF_EQUAL)
    {
        return OSPF_TRUE;
    }
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OlsIsEligibleToGenLsaInCxt                                 */
/*                                                                           */
/* Description  : This function checks whether the router is eligible to     */
/*                generate particular type of LSA or not.                    */
/*                                                                           */
/* Input        : u1InternalType : Internal LSA type                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS: If eligible to generate LSA..                */
/*                OSPF_FAILURE : If not eligible to generate LSA.            */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
OlsIsEligibleToGenLsaInCxt (tArea * pArea, UINT1 u1InternalType)
{
    INT4                i4RetValue = OSPF_SUCCESS;

    switch (u1InternalType)
    {
        case NETWORK_SUM_LSA:
        case COND_NETWORK_SUM_LSA:
        case DEFAULT_NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case INDICATION_LSA:
            if (!(pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE))
            {

                OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "Router is not an ABR");
                i4RetValue = OSPF_FAILURE;
            }
            break;

        case AS_EXT_LSA:
        case AS_TRNSLTD_EXT_LSA:
        case AS_TRNSLTD_EXT_RNG_LSA:
        case COND_AS_EXT_LSA:

            if ((!(pArea->pOspfCxt->bAsBdrRtr == OSPF_TRUE)) &&
                (!((pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE) &&
                   (pArea->pOspfCxt->u4NssaAreaCount > 0))))
            {

                OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "Router is not an ASBR or NSSA ABR");
                i4RetValue = OSPF_FAILURE;
            }

            /* If router has no  interface attached to any Type-5 capable 
             * area, it does not generate Type-5 LSAs for the external 
             * imported routes. */
            if (pArea->pOspfCxt->bGenType5Flag == OSPF_FALSE)
            {
                OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "Router has no Type 5 capable "
                          "area - Type 5 not generated");
                i4RetValue = OSPF_FAILURE;
            }
            break;

        case NSSA_LSA:
        case COND_NSSA_LSA:
        case DEFAULT_NSSA_LSA:
            if ((!(pArea->pOspfCxt->bAsBdrRtr == OSPF_TRUE)) &&
                (!((pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
                   && (pArea->pOspfCxt->u4NssaAreaCount > 0))))
            {

                OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "Router is not an ABR");
                i4RetValue = OSPF_FAILURE;
            }
            /* If router has no  interface attached to any Type-7 capable 
             * area, it does not generate Type-7 LSA . */
            if (pArea->u4ActIntCount == 0)
            {
                OSPF_TRC (OSPF_LSU_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "Router has no interface attached to any Type 7 capable "
                          "area - Type 7 not generated");
                i4RetValue = OSPF_FAILURE;
            }

            break;

        default:
            break;
    }
    return i4RetValue;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OlsChkAndGenerateSumToNewAreasInCxt                        */
/*                                                                           */
/* Description  : This function checks and generate the summary LSAs into    */
/*                the newly attached areas.                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OlsChkAndGenerateSumToNewAreasInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* LSA cannot be generated in GR mode */
        return;
    }
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can generate the summary LSA, 
         * reset the attached area flags */
        if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {
            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                if (pArea->bNewlyAttached == OSPF_TRUE)
                {
                    pArea->bNewlyAttached = OSPF_FALSE;
                }
            }
        }
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }
    if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if (pArea->bNewlyAttached == OSPF_TRUE)
            {
                if (pArea->u4AreaType != NORMAL_AREA)
                {
                    IfSendDefaultSumToStubNssaArea (pArea);
                }
                OlsGenerateSummaryToArea (pArea);
                pArea->bNewlyAttached = OSPF_FALSE;
            }
        }
    }
}

/******************************************************************************

Function        :   OlsBuildDefaultSummaryParam

Description     :   builds the summary param structure required for the
                    construction of the uncondensed summary lsas

Input           :
                    pArea          :   area to which the summarization to be 
                                        done 
Output          :
                    pSummaryParam :   ptr to summary param structure which is
                                        getting filled up the routine

Returns         :   SUCCESS/FAILURE

******************************************************************************/
PRIVATE VOID
OlsBuildDefaultSummaryParam (tArea * pArea, tSummaryParam * pSummaryParam)
{
    UINT1               u1Tos = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: OlsBuildDefaultSummaryParam\n");

    IP_ADDR_COPY (pSummaryParam->ipAddr, gDefaultDest);
    IP_ADDR_COPY (pSummaryParam->ipAddrMask, gNullIpAddr);

    OSPF_TOS_LOOP (u1Tos)
    {
        pSummaryParam->aMetric[u1Tos].bStatus = OSPF_INVALID;
        if (pArea->aStubDefaultCost[u1Tos].rowStatus != INVALID)
        {
            pSummaryParam->aMetric[u1Tos].u4Value =
                pArea->aStubDefaultCost[u1Tos].u4Value;
            pSummaryParam->aMetric[u1Tos].bStatus = OSPF_VALID;
        }
        else
        {
            pSummaryParam->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
        }
    }
    pSummaryParam->options = 0;
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: OlsBuildDefaultSummaryParam\n");
}
PRIVATE tAreaId    *
OlsGetPathAreaId (tRtEntry * pRtEntry)
{
    tPath              *pPath = NULL;

    if ((TMO_SLL_Count (&(pRtEntry->aTosPath[TOS_0]))) == 0)
    {
        return NULL;
    }

    pPath = ((tPath *) TMO_SLL_First (&(pRtEntry->aTosPath[TOS_0])));
    if (pPath != NULL)
    {
        return (&(pPath->areaId));
    }
    return NULL;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file osols.c                         */
/*-----------------------------------------------------------------------*/
