#include "lr.h"
#include "fssnmp.h"
#include "fsostewr.h"
#include "fsostlow.h"
#include "fsostedb.h"

VOID
RegisterFSOSTE ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsosteOID, &fsosteEntry, OspfLock,
                                         OspfUnLock, UtilOspfSetContext,
                                         UtilOspfResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsosteOID, (const UINT1 *) "futOspfTestGroup");
}

VOID
UnRegisterFSOSTE ()
{
    SNMPUnRegisterMib (&fsosteOID, &fsosteEntry);
    SNMPDelSysorEntry (&fsosteOID, (const UINT1 *) "futOspfTestGroup");
}

INT4
FutOspfBRRouteIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfBRRouteIpAddrMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfBRRouteIpTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[2].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfBRRouteIpNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfBRRouteDestTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[4].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfBRRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfBRRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
FutOspfBRRouteAreaIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfBRRouteAreaId (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        &pMultiData->u4_ULongValue));
}

INT4
FutOspfBRRouteCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfBRRouteCost (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
FutOspfBRRouteInterfaceIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfBRRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfBRRouteInterfaceIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfExtRouteMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfExtRouteTOSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
FutOspfExtRouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteMetric (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteMetric (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteMetric (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteMetricType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteMetricType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteMetricType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteTag (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteTag (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteTag (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteFwdAdrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteFwdAdr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue));
}

INT4
FutOspfExtRouteFwdAdrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteFwdAdr (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->u4_ULongValue));
}

INT4
FutOspfExtRouteFwdAdrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteFwdAdr (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
FutOspfExtRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteIfIndex (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteNextHopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteNextHop (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));
}

INT4
FutOspfExtRouteNextHopSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteNextHop (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->u4_ULongValue));
}

INT4
FutOspfExtRouteNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteNextHop (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
FutOspfExtRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FutOspfExtRouteStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFutOspfExtRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FutOspfExtRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFutOspfExtRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfExtRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
GetNextIndexFutOspfExtRouteTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4futOspfExtRouteDest;
    UINT4               u4futOspfExtRouteMask;
    INT4                i4futOspfExtRouteTOS;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfExtRouteTable (&u4futOspfExtRouteDest,
                                                  &u4futOspfExtRouteMask,
                                                  &i4futOspfExtRouteTOS)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfExtRouteTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4futOspfExtRouteDest,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4futOspfExtRouteMask,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4futOspfExtRouteTOS) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4futOspfExtRouteDest;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4futOspfExtRouteMask;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4futOspfExtRouteTOS;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFutOspfBRRouteTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4futOspfBRRouteIpAddr;
    UINT4               u4futOspfBRRouteIpAddrMask;
    UINT4               u4futOspfBRRouteIpTos;
    UINT4               u4futOspfBRRouteIpNextHop;
    INT4                i4futOspfBRRouteDestType;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfBRRouteTable (&u4futOspfBRRouteIpAddr,
                                                 &u4futOspfBRRouteIpAddrMask,
                                                 &u4futOspfBRRouteIpTos,
                                                 &u4futOspfBRRouteIpNextHop,
                                                 &i4futOspfBRRouteDestType)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfBRRouteTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4futOspfBRRouteIpAddr,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4futOspfBRRouteIpAddrMask,
             pFirstMultiIndex->pIndex[2].u4_ULongValue, &u4futOspfBRRouteIpTos,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &u4futOspfBRRouteIpNextHop,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &i4futOspfBRRouteDestType) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4futOspfBRRouteIpAddr;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4futOspfBRRouteIpAddrMask;
    pNextMultiIndex->pIndex[2].u4_ULongValue = u4futOspfBRRouteIpTos;
    pNextMultiIndex->pIndex[3].u4_ULongValue = u4futOspfBRRouteIpNextHop;
    pNextMultiIndex->pIndex[4].i4_SLongValue = i4futOspfBRRouteDestType;
    return SNMP_SUCCESS;
}

INT4
FutOspfExtRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfExtRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfGrShutdownGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfGrShutdown (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfGrShutdownSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfGrShutdown (pMultiData->i4_SLongValue));
}

INT4
FutOspfGrShutdownTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfGrShutdown (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfGrShutdownDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfGrShutdown
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
