/*$Id: fsmioswr.c,v 1.15 2014/03/01 11:40:44 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmioswr.h"
# include  "fsmiosdb.h"
# include  "osinc.h"

VOID
RegisterFSMIOS ()
{
    SNMPRegisterMibWithLock (&fsmiosOID, &fsmiosEntry, OspfLock, OspfUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmiosOID, (const UINT1 *) "fsmios");
}

VOID
UnRegisterFSMIOS ()
{
    SNMPUnRegisterMib (&fsmiosOID, &fsmiosEntry);
    SNMPDelSysorEntry (&fsmiosOID, (const UINT1 *) "fsmios");
}

INT4
FsMIOspfGlobalTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfGlobalTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfVrfSpfIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfVrfSpfInterval (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfGlobalTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfGlobalTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfVrfSpfIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfVrfSpfInterval (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfGlobalTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfGlobalTraceLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfVrfSpfIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfVrfSpfInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfGlobalTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfGlobalTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfVrfSpfIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfVrfSpfInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfOverFlowStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfOverFlowState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfPktsRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfPktsRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfPktsTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfPktsTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfPktsDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfPktsDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfRFC1583CompatibilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRFC1583Compatibility
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfTraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfMinLsaIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfMinLsaInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfABRTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfABRType (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfNssaAsbrDefRtTransGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNssaAsbrDefRtTrans
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfDefaultPassiveInterfaceGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfDefaultPassiveInterface
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfSpfHoldtimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfSpfHoldtime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfSpfDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfSpfDelay (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRTStaggeringIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRTStaggeringInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfRouterIdPermanenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRouterIdPermanence
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfBfdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfBfdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfBfdAllIfStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfBfdAllIfState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartSupport (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartStrictLsaCheckingGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartStrictLsaChecking
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfRestartExitReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartExitReason
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfHelperSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfHelperSupport (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsMIOspfExtTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfExtTraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfHelperGraceTimeLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfHelperGraceTimeLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartAckStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartAckState (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfGraceLsaRetransmitCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfGraceLsaRetransmitCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRestartReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRestartReason (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRFC1583CompatibilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRFC1583Compatibility
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfTraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMIOspfMinLsaIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfMinLsaInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfABRTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfABRType (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfNssaAsbrDefRtTransSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfNssaAsbrDefRtTrans
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfDefaultPassiveInterfaceSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfDefaultPassiveInterface
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfSpfHoldtimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfSpfHoldtime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfSpfDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfSpfDelay (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRTStaggeringIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRTStaggeringInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsMIOspfRouterIdPermanenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRouterIdPermanence
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfBfdStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfBfdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsMIOspfBfdAllIfStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfBfdAllIfState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRestartSupport (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRestartInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartStrictLsaCheckingSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRestartStrictLsaChecking
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfHelperSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfHelperSupport (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsMIOspfExtTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfExtTraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMIOspfHelperGraceTimeLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfHelperGraceTimeLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartAckStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRestartAckState (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfGraceLsaRetransmitCountSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfGraceLsaRetransmitCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartReasonSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRestartReason (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfHelperGraceTimeLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfHelperGraceTimeLimit (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartAckStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRestartAckState (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfGraceLsaRetransmitCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfGraceLsaRetransmitCount (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsMIOspfRestartReasonTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRestartReason (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRFC1583CompatibilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRFC1583Compatibility (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfTraceLevel (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfMinLsaIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfMinLsaInterval (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsMIOspfABRTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfABRType (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMIOspfNssaAsbrDefRtTransTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfNssaAsbrDefRtTrans (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsMIOspfDefaultPassiveInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfDefaultPassiveInterface (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsMIOspfSpfHoldtimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfSpfHoldtime (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfSpfDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfSpfDelay (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRTStaggeringIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRTStaggeringInterval (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsMIOspfRouterIdPermanenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRouterIdPermanence (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));
}

INT4
FsMIOspfBfdStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfBfdStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfBfdAllIfStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfBfdAllIfState (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRestartSupport (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRestartInterval (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRestartStrictLsaCheckingTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRestartStrictLsaChecking (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsMIOspfHelperSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfHelperSupport (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsMIOspfExtTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfExtTraceLevel (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMIOspfGlobalExtTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfGlobalExtTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfGlobalExtTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfGlobalExtTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfGlobalExtTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfGlobalExtTraceLevel (pu4Error,
                                                  pMultiData->i4_SLongValue));
}

INT4
FsMIOspfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfRTStaggeringStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfRTStaggeringStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfHotStandbyAdminStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfHotStandbyAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfHotStandbyStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfHotStandbyState (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfDynamicBulkUpdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfDynamicBulkUpdStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfStanbyHelloSyncCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfStanbyHelloSyncCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsMIOspfStanbyLsaSyncCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfStanbyLsaSyncCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsMIOspfRTStaggeringStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfRTStaggeringStatus (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfRTStaggeringStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfRTStaggeringStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfGlobalExtTraceLevelDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfGlobalExtTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfRTStaggeringStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfRTStaggeringStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfAreaTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfAreaTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfAreaTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfAreaIfCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaIfCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAreaNetCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaNetCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAreaRtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaRtrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAreaNSSATranslatorRoleGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaNSSATranslatorRole
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAreaNSSATranslatorStateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaNSSATranslatorState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAreaNSSATranslatorStabilityIntervalGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaNSSATranslatorStabilityInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAreaNSSATranslatorEventsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaNSSATranslatorEvents
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAreaDfInfOriginateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaDfInfOriginate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAreaNSSATranslatorRoleSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAreaNSSATranslatorRole
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaNSSATranslatorStabilityIntervalSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAreaNSSATranslatorStabilityInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaDfInfOriginateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAreaDfInfOriginate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaNSSATranslatorRoleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAreaNSSATranslatorRole (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfAreaNSSATranslatorStabilityIntervalTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAreaNSSATranslatorStabilityInterval (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiIndex->
                                                                  pIndex[1].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue));

}

INT4
FsMIOspfAreaDfInfOriginateTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAreaDfInfOriginate (pu4Error,
                                                 pMultiIndex->
                                                 pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->
                                                 pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfAreaTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfHostTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfHostTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfHostTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfHostRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfHostTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfHostRouteIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfHostRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfHostRouteIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfHostRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfHostRouteIfIndex (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsMIOspfHostTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfHostTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfIfTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfIfOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfOperState (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfPassiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfPassive (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfNbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfNbrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfAdjCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAdjCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfHelloRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfHelloRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfHelloTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfHelloTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfHelloDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfHelloDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfDdpRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfDdpRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfDdpTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfDdpTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfDdpDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfDdpDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLrqRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLrqRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLrqTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLrqTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLrqDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLrqDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLsuRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLsuRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLsuTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLsuTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLsuDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLsuDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLakRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLakRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLakTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLakTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfLakDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfLakDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfIfBfdStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfBfdState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfPassiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfPassive (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfBfdStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfBfdState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfPassiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfPassive (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfBfdStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfBfdState (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfIfMD5AuthTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfIfMD5AuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfIfMD5AuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfIfMD5AuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfMD5AuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfMD5AuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfMD5AuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfMD5AuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfMD5AuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfMD5AuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfMD5AuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfMD5AuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfMD5AuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfMD5AuthKey (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfMD5AuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfMD5AuthKeyStartAccept (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStartGenerateTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfMD5AuthKeyStartGenerate (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[3].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfMD5AuthKeyStopGenerate (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[3].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfMD5AuthKeyStopAccept (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[3].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfMD5AuthKeyStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[3].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfMD5AuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfIfMD5AuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfVirtIfMD5AuthTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfVirtIfMD5AuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfVirtIfMD5AuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfVirtIfMD5AuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfMD5AuthKey
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtIfMD5AuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfMD5AuthKey
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfMD5AuthKey (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStartAcceptTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartAccept (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[3].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStartGenerateTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartGenerate (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStopGenerateTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopGenerate (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[3].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStopAcceptTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopAccept (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[3].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfVirtIfMD5AuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfVirtIfMD5AuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfNbrTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfNbrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfNbrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfNbrDBSummaryQLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNbrDBSummaryQLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfNbrLSReqQLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNbrLSReqQLen (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfNbrRestartHelperStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNbrRestartHelperStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfNbrRestartHelperAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNbrRestartHelperAge
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfNbrRestartHelperExitReasonGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNbrRestartHelperExitReason
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfNbrBfdStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfNbrBfdState (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfRoutingTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfRoutingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfRoutingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRouteAreaIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRouteAreaId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfRouteCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRouteCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRouteType2CostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRouteType2Cost (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRouteInterfaceIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRouteInterfaceIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfSecIfTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfSecIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfSecIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfSecIfStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfSecIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfSecIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfSecIfStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfSecIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfSecIfStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfSecIfStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfSecIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfSecIfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfAreaAggregateTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfAreaAggregateTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfAreaAggregateTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfAreaAggregateExternalTagGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAreaAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaAggregateExternalTag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAreaAggregateExternalTagSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAreaAggregateExternalTag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaAggregateExternalTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAreaAggregateExternalTag (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[3].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[4].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsMIOspfAreaAggregateTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfAreaAggregateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfAsExternalAggregationTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfAsExternalAggregationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfAsExternalAggregationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfAsExternalAggregationEffectGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAsExternalAggregationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAsExternalAggregationEffect
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAsExternalAggregationTranslationGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAsExternalAggregationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAsExternalAggregationTranslation
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAsExternalAggregationStatusGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAsExternalAggregationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAsExternalAggregationStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAsExternalAggregationEffectSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAsExternalAggregationEffect
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAsExternalAggregationTranslationSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAsExternalAggregationTranslation
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAsExternalAggregationStatusSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAsExternalAggregationStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAsExternalAggregationEffectTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAsExternalAggregationEffect (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[3].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsMIOspfAsExternalAggregationTranslationTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAsExternalAggregationTranslation (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[3].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsMIOspfAsExternalAggregationStatusTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAsExternalAggregationStatus (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[3].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsMIOspfAsExternalAggregationTableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfAsExternalAggregationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfOpaqueTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfOpaqueTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfOpaqueTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfOpaqueOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfOpaqueTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfOpaqueOption (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType11LsaCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfOpaqueTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType11LsaCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfType11LsaCksumSumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfOpaqueTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType11LsaCksumSum
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAreaIDValidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfOpaqueTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAreaIDValid (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfOpaqueOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfOpaqueOption (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaIDValidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfAreaIDValid (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfOpaqueOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfOpaqueOption (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfAreaIDValidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfAreaIDValid (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfOpaqueTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfOpaqueTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfOpaqueInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfOpaqueInterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfOpaqueInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfOpaqueType9LsaCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfOpaqueInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfOpaqueType9LsaCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfOpaqueType9LsaCksumSumGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfOpaqueInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfOpaqueType9LsaCksumSum
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfType9LsdbTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfType9LsdbTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfType9LsdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfType9LsdbSequenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType9LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType9LsdbSequence
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType9LsdbAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType9LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType9LsdbAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType9LsdbChecksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType9LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType9LsdbChecksum
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType9LsdbAdvertisementGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType9LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType9LsdbAdvertisement
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFsMIOspfType11LsdbTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfType11LsdbTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfType11LsdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfType11LsdbSequenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType11LsdbSequence
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType11LsdbAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType11LsdbAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType11LsdbChecksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType11LsdbChecksum
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfType11LsdbAdvertisementGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfType11LsdbAdvertisement
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFsMIOspfAppInfoDbTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfAppInfoDbTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfAppInfoDbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfAppInfoDbOpaqueTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbOpaqueType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAppInfoDbLsaTypesSupportedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbLsaTypesSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfAppInfoDbType9GenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbType9Gen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAppInfoDbType9RcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbType9Rcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAppInfoDbType10GenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbType10Gen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAppInfoDbType10RcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbType10Rcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAppInfoDbType11GenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbType11Gen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfAppInfoDbType11RcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfAppInfoDbType11Rcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsMIOspfRRDRouteTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfRRDRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfRRDRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfRRDStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDSrcProtoMaskEnableGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDSrcProtoMaskEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDSrcProtoMaskDisableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDSrcProtoMaskDisable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDRouteMapEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDRouteMapEnable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
}

INT4
FsMIOspfRRDStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDSrcProtoMaskEnableSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDSrcProtoMaskEnable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDSrcProtoMaskDisableSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDSrcProtoMaskDisable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteMapEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDRouteMapEnable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
}

INT4
FsMIOspfRRDStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDSrcProtoMaskEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDSrcProtoMaskEnable (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDSrcProtoMaskDisableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDSrcProtoMaskDisable (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfRRDRouteMapEnableTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDRouteMapEnable (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));
}

INT4
FsMIOspfRRDRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfRRDRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfRRDRouteConfigTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfRRDRouteConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfRRDRouteConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfRRDRouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDRouteMetric (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDRouteMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDRouteTagTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDRouteTagType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDRouteTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDRouteTag (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfRRDRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDRouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDRouteMetric (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteTagTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDRouteTagType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDRouteTag (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsMIOspfRRDRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDRouteMetric (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDRouteMetricType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteTagTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDRouteTagType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDRouteTag (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsMIOspfRRDRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDRouteStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDRouteConfigTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfRRDRouteConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfVirtNbrTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfVirtNbrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfVirtNbrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfVirtNbrRestartHelperStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtNbrRestartHelperStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtNbrRestartHelperAgeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtNbrRestartHelperAge
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfVirtNbrRestartHelperExitReasonGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtNbrRestartHelperExitReason
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfDistInOutRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfDistInOutRouteMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfDistInOutRouteMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfDistInOutRouteMapValueGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfDistInOutRouteMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfDistInOutRouteMapValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfDistInOutRouteMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfDistInOutRouteMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfDistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfDistInOutRouteMapValueSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfDistInOutRouteMapValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfDistInOutRouteMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfDistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfDistInOutRouteMapValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfDistInOutRouteMapValue (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfDistInOutRouteMapRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfDistInOutRouteMapRowStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         pOctetStrValue,
                                                         pMultiIndex->pIndex[2].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsMIOspfDistInOutRouteMapTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfDistInOutRouteMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfPreferenceTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfPreferenceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfPreferenceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfPreferenceValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfPreferenceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfPreferenceValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfPreferenceValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfPreferenceValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfPreferenceValueTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfPreferenceValue (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfPreferenceTableDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfPreferenceTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfIfAuthTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfIfAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfIfAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfIfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfIfAuthKeyStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfIfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfIfAuthKeyStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfAuthKey (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfAuthKeyStartAccept (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[3].
                                                   i4_SLongValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStartGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfAuthKeyStartGenerate (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[3].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfAuthKeyStopGenerate (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[3].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfAuthKeyStopAccept (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[3].
                                                  i4_SLongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
FsMIOspfIfAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfIfAuthKeyStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[3].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfIfAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfIfAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfVirtIfAuthTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfVirtIfAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfVirtIfAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfVirtIfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfVirtIfAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfVirtIfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfVirtIfAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfAuthKey (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfAuthKeyStartAccept (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[3].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStartGenerateTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfAuthKeyStartGenerate (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStopGenerateTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfAuthKeyStopGenerate (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[3].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfAuthKeyStopAccept (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      pOctetStrValue));

}

INT4
FsMIOspfVirtIfAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfVirtIfAuthKeyStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsMIOspfVirtIfAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfVirtIfAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfRRDMetricTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfRRDMetricTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfRRDMetricTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfRRDMetricValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDMetricTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDMetricValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfRRDMetricTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfRRDMetricType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfRRDMetricValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDMetricValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfRRDMetricType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDMetricValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDMetricValue (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfRRDMetricType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsMIOspfRRDMetricTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfRRDMetricTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
