/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osrtm.c,v 1.56 2017/03/10 12:45:35 siva Exp $
 *
 * Description:This file contains route redistribution related 
 *             procedures.
 *
 *******************************************************************/

#include "osinc.h"
extern UINT4 RMapGetMapStatus (UINT1 *pu1RMapName);
typedef tNetIpv4RtInfo tRtmRtChgNotifyMsg;

/* Proto types of the functions private to this file only */

PRIVATE VOID RtmProcessRegAckMsg PROTO ((tOsixMsg * pRtmMsg));

PRIVATE VOID RtmProcessRtChgNtfInCxt PROTO ((tOspfCxt * pOspfCxt,
                                             tRtmRtChgNotifyMsg *
                                             pRtmRtChgNtf));

PRIVATE INT4 RtmGetRtTag PROTO ((tPath * pPath, UINT4 *pu4RtTag));
PRIVATE INT1 OspfApplyInFilter PROTO ((tFilteringRMap * pFilterRMap,
                                       tNetIpv4RtInfo * pRtEntry));

/*****************************************************************************/
/*                                                                           */
/* Function        : OspfRtmInit                                             */
/*                                                                           */
/* Description     : This procedure initializes the route redistribution     */
/*                   related parameters.                                     */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS, if successful                             */
/*                   OSPF_FAILURE, otherwise                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfRtmInitInCxt (tOspfCxt * pOspfCxt)
{

    tRedistrRegInfo    *pRedistrRegInfo = NULL;

    TMO_SLL_Init (&(pOspfCxt->RedistrRouteConfigLst));

    pOspfCxt->redistrAdmnStatus = OSPF_DISABLED;
    pOspfCxt->u4RrdSrcProtoBitMask = 0;
    pOspfCxt->u2ASNo = 0;        /* AS No is determined during registration with RTM */

    REDIST_REG_ALLOC (pRedistrRegInfo);

    if (pRedistrRegInfo == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId,
                  "Unable to allocate memory for Redistribution Register\n");
        return OSPF_FAILURE;
    }

    /* Registration Id and MaxMsgQLen are obtained after registration with RTM */

    pRedistrRegInfo->u4MaxQMsgLength = 0;

    pOspfCxt->pRedistribute = pRedistrRegInfo;

    return OSPF_SUCCESS;

}

/*****************************************************************************/
/* Function        : OspfUpdateDistanceForExitRoutes                         */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        /* Description     : Ospf update configured distance value for exits         *//*                   routes in database                                      */
/* Input           : pOspfCxt - Context entry Info.                          */
/* Output          : None                                                    */
/* Returns         : None                                                    */
/*****************************************************************************/
PUBLIC VOID
OspfUpdateDistanceForExitRoutes (tOspfCxt * pOspfCxt)
{
    tOspfRt            *pOspfRt = pOspfCxt->pOspfRt;
    tRtEntry           *pRtEntry = NULL;
    tArea              *pArea = NULL;
    UINT1               u1BitMask = 0;
    tPath              *pCurrPath;
    UINT1               u1HopIndex;
    tNetIpv4RtInfo      NetRtInfo;
    tFilteringRMap     *pInFilterRMap = NULL;
    UINT4               u4RtCount = 0;
    UINT1               u1CmdType = 0;

    if (pOspfCxt->pDistributeInFilterRMap != NULL)
    {
        pInFilterRMap = pOspfCxt->pDistributeInFilterRMap;
    }

    if (pOspfCxt->pLastUpdateRtLst == NULL)
    {
        /* Get First valid route entry in ospf route list */
        pRtEntry = (tRtEntry *) TMO_SLL_First (&(pOspfRt->routesList));
        pOspfCxt->pLastUpdateRtLst = pRtEntry;
    }
    else
    {
        pRtEntry = pOspfCxt->pLastUpdateRtLst;
        /* Get Next valid route entry in ospf route list */
        pRtEntry = (tRtEntry *) TMO_SLL_Next (&(pOspfRt->routesList),
                                              &(pRtEntry->nextRtEntryNode));
        pOspfCxt->pLastUpdateRtLst = pRtEntry;
    }

    while (pRtEntry != NULL)
    {
        if ((pRtEntry->u2RtProto == OSPF_ID) &&
            (pRtEntry->u2RtType == INDIRECT_ROUTE) &&
            (pRtEntry->u4RowStatus == ACTIVE) && (IS_DEST_NETWORK (pRtEntry)))
        {
            if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
            {
                return;
            }
            for (u1HopIndex = 0;
                 u1HopIndex < OSPF_MIN ((pCurrPath->u1HopCount),
                                        (MAX_NEXT_HOPS)); u1HopIndex++)
            {
                MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
                NetRtInfo.u4DestNet = OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                NetRtInfo.u4DestMask = pRtEntry->u4IpAddrMask;
                if (pCurrPath->aNextHops[u1HopIndex].pInterface->u4AddrlessIf ==
                    0)
                {
                    NetRtInfo.u4NextHop =
                        OSPF_CRU_BMC_DWFROMPDU (pCurrPath->
                                                aNextHops[u1HopIndex].
                                                nbrIpAddr);
                }
                if (NetRtInfo.u4DestNet == NetRtInfo.u4NextHop)
                {
                    continue;
                }
                NetRtInfo.u4RtIfIndx =
                    pCurrPath->aNextHops[u1HopIndex].u4IfIndex;
                NetRtInfo.u4Tos = TOS_0;
                NetRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
                NetRtInfo.u4RtNxtHopAs = 0;
                NetRtInfo.u2RtProto = OSPF_ID;
                NetRtInfo.u4RtAge = pRtEntry->u4UpdateTime;
                RtmGetRtTag (pCurrPath, &NetRtInfo.u4RouteTag);
                NetRtInfo.u1BitMask = u1BitMask;
                /* Update newly configured distance value for Route */
                NetRtInfo.u1Preference = OspfFilterRouteSourceInCxt (pOspfCxt,
                                                                     &NetRtInfo);

                if (pCurrPath->u1PathType != TYPE_2_EXT)
                {
                    NetRtInfo.i4Metric1 = (INT4) pCurrPath->u4Cost;
                }
                else
                {
                    NetRtInfo.i4Metric1 = (INT4) pCurrPath->u4Type2Cost;
                }

                if ((pCurrPath->u1PathType == TYPE_1_EXT)
                    || (pCurrPath->u1PathType == TYPE_2_EXT))
                {
                    pArea = GetFindAreaInCxt (pOspfCxt, &(pCurrPath->areaId));
                    /* When the area type is NSSA area then the pathtype is 
                     * incremented by 2 to inform ip that the ospf route is
                     * type NSSA external type1/type2 route.
                     * */
                    if ((pArea != NULL) && (pArea->u4AreaType == NSSA_AREA))
                    {
                        NetRtInfo.u1MetricType = pCurrPath->u1PathType + 2;
                    }
                    else
                    {
                        NetRtInfo.u1MetricType = pCurrPath->u1PathType;
                    }
                }
                else
                {
                    NetRtInfo.u1MetricType = pCurrPath->u1PathType;
                }

                NetRtInfo.u4RowStatus = ACTIVE;
                NetRtInfo.u4ContextId = pOspfCxt->u4OspfCxtId;
                if (OSPF_FAILURE == OspfApplyInFilter (pInFilterRMap, &NetRtInfo))
                {
                    /* Dont update the route to RTM */
                    return;
                }

                /* If the administrative distance is 255, the router does not believe
                   the source of that route and does not install the route in the
                   routing table.So,for UNKNOWN AD(255) send delete action to RTM*/

                if(NetRtInfo.u1Preference == IP_UNKNOWN_AD)
                {
                   u1CmdType = NETIPV4_DELETE_ROUTE;
                }
                else
                {
                   u1CmdType = NETIPV4_MODIFY_ROUTE;
                }

                /* Update modified route to RTM */
                if (NetIpv4LeakRoute (u1CmdType, &NetRtInfo) ==
                    NETIPV4_FAILURE)
                {
                    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                                  "NetIpv4LeakRoute failed\r\n");
                }

            }
            u4RtCount++;
        }
        if (u4RtCount == MAX_RT_FOR_UPDATE_DISTANCE)
        {
            /* Restart distance timer for update configured distance 
             * value for existing routes */
            TmrSetTimer (&(pOspfCxt->distanceTimer),
                         DISTANCE_TIMER, OSPF_DISTANCE_TIME_INTERVAL);
            return;
        }

        /* Get Next valid route entry in ospf route list */
        pRtEntry = (tRtEntry *) TMO_SLL_Next (&(pOspfRt->routesList),
                                              &(pRtEntry->nextRtEntryNode));
        pOspfCxt->pLastUpdateRtLst = pRtEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function        : OspfProcessRtmRts                                       */
/*                                                                           */
/* Description     : This procedure Process the Routes from RTM              */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfProcessRtmRts (VOID)
{
    tExpRtNode         *pRtmRouteMsg = NULL;
    tExpRtNode         *pRtmNextRouteMsg = NULL;
    tNetIpv4RtInfo     *pRtInfo = NULL;
    tOspfCxt           *pOspfCxt = NULL;

    /* Take the DataLock for accessing RTM Route List */
    OsixSemTake (gOsRtr.RtmLstSemId);

    if ((pRtmRouteMsg = (tExpRtNode *)
         TMO_SLL_First (&gOsRtr.RtmRtLst)) == NULL)
    {
        OsixSemGive (gOsRtr.RtmLstSemId);
        return;
    }
    do
    {
        pRtmNextRouteMsg = (tExpRtNode *) TMO_SLL_Next ((&gOsRtr.RtmRtLst),
                                                        (tTMO_SLL_NODE *)(pRtmRouteMsg
                                                          ));
        pRtInfo = &(pRtmRouteMsg->RtInfo);

        if ((pOspfCxt = UtilOspfGetCxt (pRtInfo->u4ContextId)) != NULL)
        {

            /* Updation for the Route already Distributed */
            if ((pRtInfo->u4RowStatus == RTM_DESTROY) ||
                ((pRtInfo->u4RowStatus == RTM_ACTIVE)
                 && (pRtInfo->u2ChgBit != IP_BIT_ALL)))
            {
                RtmProcessRtChgNtfInCxt (pOspfCxt, pRtInfo);
            }
            else
            {
                RtmProcessRtUpdateInCxt (pOspfCxt, pRtInfo);
            }

        }

        TMO_SLL_Delete (&gOsRtr.RtmRtLst, (tTMO_SLL_NODE *)(pRtmRouteMsg));
        RTM_ROUTE_FREE (pRtmRouteMsg);
        pOspfCxt = NULL;
        pRtmRouteMsg = pRtmNextRouteMsg;

    }
    while (pRtmRouteMsg != NULL);
    /* Release the DataLock for accessing RTM Route List */
    OsixSemGive (gOsRtr.RtmLstSemId);
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmMsgHandler                                           */
/*                                                                           */
/* Description     : This procedure dequeues the messages from RTM and       */
/*                   processes it based on the message type.                 */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtmMsgHandler (tOsixMsg * pRtmMsg)
{

    tRtmMsgHdr         *pRtmMsgHdr;

    pRtmMsgHdr = (tRtmMsgHdr *) OSPF_RTM_IF_GET_MSG_HDR_PTR (pRtmMsg);

    switch (pRtmMsgHdr->u1MessageType)
    {

        case RTM_REGISTRATION_ACK_MESSAGE:
            OSPF_TRC (OSPF_RTMODULE_TRC, pRtmMsgHdr->RegnId.u4ContextId,
                      "Received Acknowledge Message from RTM\n");
            RtmProcessRegAckMsg (pRtmMsg);
            break;

        default:
            break;

    }                            /* switch */

    CRU_BUF_Release_MsgBufChain (pRtmMsg, 0);

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmProcessRegAckMsg                                     */
/*                                                                           */
/* Description     : This procedure processes the reg ack message from RTM   */
/*                   and updates the OSPF global structure.                  */
/*                                                                           */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtmProcessRegAckMsg (tOsixMsg * pRtmMsg)
{
    tRtmMsgHdr         *pRtmMsgHdr;
    tRtmRegnAckMsg      RtmRegnAckMsg;

    pRtmMsgHdr = (tRtmMsgHdr *) OSPF_RTM_IF_GET_MSG_HDR_PTR (pRtmMsg);

    /* Extract the parameters from the buffer */
    if (IP_COPY_FROM_BUF (pRtmMsg, &RtmRegnAckMsg, 0, sizeof (tRtmRegnAckMsg))
        != sizeof (tRtmRegnAckMsg))

    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pRtmMsgHdr->RegnId.u4ContextId,
                  "Unable to Copy from BUF\n");
        return;
    }
    /* Update the OSPF global structure */
    /* uncomment the below line if context specific ack is receivec from rtm */
    /*  pOspfCxt->pRedistribute->u4MaxQMsgLength = RtmRegnAckMsg.u4MaxMessageSize; */

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmProcessRtUpdateInCxt                                 */
/*                                                                           */
/* Description     : This procedure processes the route updates from RTM. It */
/*                   updates the import list appropriately and generates     */
/*                   AS-Ext LSA by invoking the LSDB module.                 */
/*                                                                           */
/* Input           : pRtmRtUpdate  -  pointer to the RtUpdate                */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RtmProcessRtUpdateInCxt (tOspfCxt * pOspfCxt, tRtmRtUpdateMsg * pRtmRtUpdate)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tInterface         *pLstIface;
    tTMO_SLL_NODE      *pLstNode;
    tIPADDR             DestNet;
    tIPADDRMASK         DestMask;
    tIPADDR             NextHop;
    tIPADDR             FwdAddr;
    tExtRoute          *pExtRoute;
    UINT4               u4Metric;
    UINT1               u1MetricType;
    UINT4               u4RouteTag;
    UINT4               u4RowStatus;
    UINT2               u2SrcProto;
    UINT4               u4IfIndex = 0;
    UINT4               u4ProtoIndex = -1;

    u2SrcProto = pRtmRtUpdate->u2RtProto;
    if (pRtmRtUpdate->u2RtProto == BGP_ID)
    {
        u4ProtoIndex = REDISTRUTE_ARR_BGP;
    }
    else if (pRtmRtUpdate->u2RtProto == RIP_ID)
    {
        u4ProtoIndex = REDISTRUTE_ARR_RIP;
    }
    else if (pRtmRtUpdate->u2RtProto == LOCAL_ID)
    {
        u4ProtoIndex = REDISTRUTE_ARR_CONNECTED;
    }
    else if (pRtmRtUpdate->u2RtProto == STATIC_ID)
    {
        u4ProtoIndex = REDISTRUTE_ARR_STATIC;
    }
    else if (pRtmRtUpdate->u2RtProto == ISIS_ID)
    {
        u4ProtoIndex = REDISTRUTE_ARR_ISIS;
    }

    OSPF_CRU_BMC_DWTOPDU (DestNet, pRtmRtUpdate->u4DestNet);
    OSPF_CRU_BMC_DWTOPDU (DestMask, pRtmRtUpdate->u4DestMask);
    OSPF_CRU_BMC_DWTOPDU (NextHop, pRtmRtUpdate->u4NextHop);

    IP_ADDR_COPY (FwdAddr, gNullIpAddr);
    NetIpv4GetCfaIfIndexFromPort (pRtmRtUpdate->u4RtIfIndx, &u4IfIndex);
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE)
        && (CFA_OOB_MGMT_INTF_ROUTING == FALSE))
    {
        return;
    }

    /*
     * Get the configured metric & metric-type from RedistrRouteConfigLst if 
     * update from RTM is subsumed by the corresponding entry in the list
     * Otherwise, use default values                                   
     */
    pRrdConfRtInfo = FindBestRrdConfRtInfoInCxt (pOspfCxt, &DestNet, &DestMask);
    if (pRrdConfRtInfo != NULL)
    {
        u4Metric = pRrdConfRtInfo->u4RrdRouteMetricValue;
        u1MetricType = pRrdConfRtInfo->u1RrdRouteMetricType;
        if (pRrdConfRtInfo->u1RedistrTagType == MANUAL_TAG)
        {
            u4RouteTag = pRrdConfRtInfo->u4ManualTagValue;
            u4RouteTag &= MANUAL_TAG_MASK;
        }
        else
        {
            u4RouteTag = pRtmRtUpdate->u4RouteTag;
        }

    }
    else
    {
        if(pRtmRtUpdate->i4RMapFlag != 0)
        {
            u4Metric = (UINT4) pRtmRtUpdate->i4Metric1;
        }
        else
        {
            u4Metric = pOspfCxt->au4MetricValue[u4ProtoIndex - 1];
        }
        if(pRtmRtUpdate->u1MetricType == 0)
        {
            u1MetricType = pOspfCxt->au1MetricType[u4ProtoIndex - 1];
        }
        else
        {
            if(pRtmRtUpdate->u1MetricType == RMAP_METRIC_TYPE_TYPE1EXTERNAL)
            {
                u1MetricType = TYPE_1_METRIC;
            }
            if(pRtmRtUpdate->u1MetricType == RMAP_METRIC_TYPE_TYPE2EXTERNAL)
            {
                u1MetricType = TYPE_2_METRIC;
            }
        }
        u4RouteTag = pRtmRtUpdate->u4RouteTag;
    }

    /* route tag setting */
    u4RowStatus = pRtmRtUpdate->u4RowStatus;

    /*
     * forwarding-address setting
     * If OSPF is enabled on the common-network on which the next hop is 
     * present, next hop addr is the forwarding address. Else set it to NULL  
     */
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pLstIface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if(pLstIface->ifStatus == ACTIVE)
        {
            if (pLstIface->u4IfIndex == pRtmRtUpdate->u4RtIfIndx)
            {
                IP_ADDR_COPY (FwdAddr, NextHop);
                break;
            }
        }

    }

    if ((pExtRoute = ExtrtAddInCxt (pOspfCxt, DestNet, DestMask,
                                    (INT1) pRtmRtUpdate->u4Tos, u4Metric,
                                    u1MetricType, u4RouteTag, FwdAddr,
                                    (UINT2) pRtmRtUpdate->u4RtIfIndx,
                                    NextHop, u2SrcProto, u4RowStatus,
                                    pRtmRtUpdate->u1MetricType)) == NULL)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Route Addition to Import List Failed\n");
        return;
    }
    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "External route is added in GR state\n");
        return;
    }
    pExtRoute->u1ExtRtIsStatic = OSIX_FALSE;
    /* flood this update to the OSPF domain */
    ExtrtActivateInCxt (pOspfCxt, pExtRoute);

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmProcessRtChgNtf                                      */
/*                                                                           */
/* Description     : This procedure processes the route change notifications */
/*                   from RTM. It scans the import list and gets the         */
/*                   relevant entry. Based on the value in the bitmask, it   */
/*                   either generates a new instance of AS-Ext LSA (after    */
/*                   updating the corresponding import list entry) OR flushes*/
/*                   out the AS-Ext LSA from the OSPF routing domain.        */
/*                   Note : Metric Change doesnt result in generating a new  */
/*                          instance.                                        */
/*                                                                           */
/* Input           : pRtmRtChgNtf  -  pointer to the RtChgNtf                */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
RtmProcessRtChgNtfInCxt (tOspfCxt * pOspfCxt, tRtmRtChgNotifyMsg * pRtmRtChgNtf)
{
    tIPADDR             DestNet;
    tIPADDRMASK         DestMask;
    tIPADDR             NextHop;
    tIPADDR             FwdAddr;
    tExtRoute          *pExtRoute;
    UINT2               u2RtBitMask;
    UINT4               u4RowStatus;
    UINT1               u1Tos;
    UINT4               u4IfIndex;
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pLstIface;

    OSPF_CRU_BMC_DWTOPDU (DestNet, pRtmRtChgNtf->u4DestNet);
    OSPF_CRU_BMC_DWTOPDU (DestMask, pRtmRtChgNtf->u4DestMask);
    OSPF_CRU_BMC_DWTOPDU (NextHop, pRtmRtChgNtf->u4NextHop);

    u2RtBitMask = pRtmRtChgNtf->u2ChgBit;
    u4RowStatus = pRtmRtChgNtf->u4RowStatus;
    u1Tos = (UINT1) pRtmRtChgNtf->u4Tos;

    NetIpv4GetCfaIfIndexFromPort (pRtmRtChgNtf->u4RtIfIndx, &u4IfIndex);
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE)
        && (CFA_OOB_MGMT_INTF_ROUTING == FALSE))
    {
        return;
    }
    if ((pExtRoute =
         GetFindExtRouteInCxt (pOspfCxt, &DestNet, &DestMask)) == NULL)
    {
        OSPF_TRC2 (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                   "Import Lst GET Failed for Net=%x Msk=%x\n",
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) DestNet),
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) DestMask));
        return;
    }
    if (!(DECODE_TOS ((INT1) u1Tos) < OSPF_MAX_METRIC))
    {
        return;
    }
    IP_ADDR_COPY (FwdAddr,
                  pExtRoute->extrtParam[DECODE_TOS ((INT1) u1Tos)].fwdAddr);

    if ((u2RtBitMask & RT_ROWSTATUS_MASK) && (u4RowStatus == DESTROY))
    {
        /* flush the corresponding AS-Ext LSA */
        pExtRoute->u1ExtRtDelFlag = DELETE_EXT_ROUTE;
        if ((INT2) u1Tos < OSPF_MAX_METRIC)
        {
            pExtRoute->aMetric[(INT2) u1Tos].rowStatus = INVALID;
        }
        ExtrtDeleteInCxt (pOspfCxt, pExtRoute);
        return;
    }

    if (u2RtBitMask & RT_IFINDX_MASK)
    {
        /*
         * This may result in changing the forwarding address
         */
        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {

            pLstIface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if (pLstIface->u4IfIndex == pRtmRtChgNtf->u4RtIfIndx)
            {
                /* update with new forwarding address & generate new instance */
                IP_ADDR_COPY (pExtRoute->extrtParam[DECODE_TOS ((INT1) u1Tos)].
                              fwdAddr, NextHop);
                OlsifParamChangeInCxt (pOspfCxt, (UINT1 *) pExtRoute,
                                       AS_EXT_LSA);
                return;
            }
        }

        if (!IS_NULL_IP_ADDR (FwdAddr))
        {
            /* forwarding address now becomes NULL */
            IP_ADDR_COPY (pExtRoute->extrtParam[DECODE_TOS ((INT1) u1Tos)].
                          fwdAddr, gNullIpAddr);

            /* generate new instance */
            OlsifParamChangeInCxt (pOspfCxt, (UINT1 *) pExtRoute, AS_EXT_LSA);
            return;
        }

    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmTxRtUpdate                                           */
/*                                                                           */
/* Description     : This procedure sends route update message to RTM. It is */
/*                   called from RT Module.                                  */
/*                                                                           */
/* Input           : pRtEntry    - ptr to the routing update                 */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmTxRtUpdate (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry)
{
    tPath              *pCurrPath;
    UINT1               u1HopIndex;

    if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
    {
        return OSPF_FAILURE;
    }

    for (u1HopIndex = 0;
         u1HopIndex < OSPF_MIN ((pCurrPath->u1HopCount), (MAX_NEXT_HOPS));
         u1HopIndex++)
    {
        if ((RtmTxNextHopUpdate (pOspfCxt, pRtEntry, pCurrPath, u1HopIndex))
            == OSPF_FAILURE)
        {
            return OSPF_FAILURE;
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmTxNextHopUpdate                                      */
/*                                                                           */
/* Description     : This procedure sends route update message to RTM. It is */
/*                   called from RT Module.                                  */
/*                                                                           */
/* Input           : pRtEntry    - ptr to the routing update                 */
/*                 : pCurrPath   - ptr to the path corresponding to this
                                    RtEntry                                  */
/*                 : u1HopIndex  - Index to which the nexthop to be updated  */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmTxNextHopUpdate (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry,
                    tPath * pCurrPath, UINT1 u1HopIndex)
{
    tNetIpv4RtInfo      NetRtInfo;
    tNetIpv4RtInfo      RmapRtInfo;
    tTMO_SLL_NODE      *pLstNode;
    tNeighbor          *pNbr = NULL;
    tArea              *pArea = NULL;
    UINT1               u1CmdType;
    UINT1               u1BitMask = 0;
    tFilteringRMap     *pInFilterRMap = NULL;

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* If this router is performing GR,
         * then route updates should not be given to RTM */
        return OSPF_SUCCESS;
    }

    if ((pCurrPath->pLsaInfo != NULL)
        && (pCurrPath->pLsaInfo->pOspfCxt != NULL))
    {
        pInFilterRMap = pCurrPath->pLsaInfo->pOspfCxt->pDistributeInFilterRMap;
    }

    /* Addition of Route Entries with Next Hop 0.0.0.0 to
     * the IP Routing Table is not required. */
    if (UtilIpAddrComp (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr,
                        gNullIpAddr) == OSPF_EQUAL)
    {
        return OSPF_FAILURE;
    }

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RmapRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4DestNet = OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
    NetRtInfo.u4DestMask = pRtEntry->u4IpAddrMask;
    if (pCurrPath->aNextHops[u1HopIndex].pInterface->u4AddrlessIf == 0)
    {
        NetRtInfo.u4NextHop =
            OSPF_CRU_BMC_DWFROMPDU (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr);
    }
    NetRtInfo.u4RtIfIndx = pCurrPath->aNextHops[u1HopIndex].u4IfIndex;
    NetRtInfo.u4Tos = TOS_0;
    NetRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u2RtProto = OSPF_ID;
    NetRtInfo.u4RtAge = pRtEntry->u4UpdateTime;
    RtmGetRtTag (pCurrPath, &NetRtInfo.u4RouteTag);
    NetRtInfo.u1BitMask = u1BitMask;
    NetRtInfo.u1Preference = OspfFilterRouteSourceInCxt (pOspfCxt,
                                                         &NetRtInfo);

    if (pCurrPath->u1PathType != TYPE_2_EXT)
    {
        NetRtInfo.i4Metric1 = (INT4) pCurrPath->u4Cost;
    }
    else
    {
        NetRtInfo.i4Metric1 = (INT4) pCurrPath->u4Type2Cost;
    }

    if ((pCurrPath->u1PathType == TYPE_1_EXT)
        || (pCurrPath->u1PathType == TYPE_2_EXT))
    {
        pArea = GetFindAreaInCxt (pOspfCxt, &(pCurrPath->areaId));
        /* When the area type is NSSA area then the pathtype is 
         * incremented by 2 to inform ip that the ospf route is
         * type NSSA external type1/type2 route.
         * */
        if ((pArea != NULL) && (pArea->u4AreaType == NSSA_AREA))
        {
            NetRtInfo.u1MetricType = pCurrPath->u1PathType + 2;
        }
        else
        {
            NetRtInfo.u1MetricType = pCurrPath->u1PathType;
        }
    }
    else
    {
        NetRtInfo.u1MetricType = pCurrPath->u1PathType;
    }

    NetRtInfo.u4RowStatus = ACTIVE;
    u1CmdType = NETIPV4_ADD_ROUTE;

    MEMCPY (&RmapRtInfo, &NetRtInfo, sizeof (tNetIpv4RtInfo));
    if (OSPF_FAILURE == OspfApplyInFilter (pInFilterRMap, &NetRtInfo))
    {
        pRtEntry->u1IsRmapAffected = OSPF_TRUE;
        /* Stop processing this route. */
        return OSPF_SUCCESS;
    }
    /* If Route entry was modified by RMap set rules, then mark the entry.
     * If Route map was disabled, these routes needs to be updated in RTM */
    if (MEMCMP (&RmapRtInfo, &RmapRtInfo, sizeof (tNetIpv4RtInfo)) != OSPF_ZERO)
    {
        pRtEntry->u1IsRmapAffected = OSPF_TRUE;
    }

    if ((pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
        (pOspfCxt->u1PrevOspfRestartState == OSPF_GR_RESTART) &&
        (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
    {
        /* To Indicate RTM that this route update is given  
           at the end of graceful restart exit */

        NetRtInfo.u2ChgBit |= IP_BIT_GR;
    }
    NetRtInfo.u4ContextId = pOspfCxt->u4OspfCxtId;
    if (NetIpv4LeakRoute (u1CmdType, &NetRtInfo) == NETIPV4_FAILURE)
    {
        return OSPF_FAILURE;
    }

    /* If this router is a helper to GR router, and the nexthop of the
       Route added is the IP addr of the GR router, then we should exit
       the helper.  */
    if ((u1CmdType == NETIPV4_ADD_ROUTE) &&
        (pCurrPath->aNextHops[u1HopIndex].pInterface->u1GRSupportFlag ==
         OSPF_TRUE) && (pOspfCxt->u1StrictLsaCheck == OSPF_TRUE))
    {
        TMO_SLL_Scan (&(pCurrPath->aNextHops[u1HopIndex].pInterface->nbrsInIf),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);

            if ((pNbr->u1NbrHelperStatus == OSPF_GR_HELPING) &&
                (pNbr->nbrIpAddr == pCurrPath->aNextHops[u1HopIndex].nbrIpAddr))
            {
                GrExitHelper (OSPF_HELPER_TOP_CHG, pNbr);
                break;
            }
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmTxRtChngNtf                                          */
/*                                                                           */
/* Description     : This procedure sends route change notification message  */
/*                   to RTM. It is invoked from RT Module.                   */
/*                                                                           */
/* Input           : pRtEntry   - ptr to the routing update                  */
/*                   u1BitMask  - Bit Mask identifying the parameter that is */
/*                                changed                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmTxRtChngNtf (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry, UINT1 u1BitMask)
{
    tPath              *pCurrPath;
    UINT1               u1HopIndex;

    if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
    {
        return OSPF_FAILURE;
    }

    for (u1HopIndex = 0;
         u1HopIndex < OSPF_MIN ((pCurrPath->u1HopCount), (MAX_NEXT_HOPS));
         u1HopIndex++)
    {
        if ((RtmTxRtChngNextHopNtf (pOspfCxt, pRtEntry, u1HopIndex, pCurrPath,
                                    u1BitMask)) == OSPF_FAILURE)
        {
            return OSPF_FAILURE;
        }
    }
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmTxRtChngNextHopNtf                                   */
/*                                                                           */
/* Description     : This procedure sends route change notification message  */
/*                   to RTM. It is invoked from RT Module.                   */
/*                                                                           */
/* Input           : pOspfCxt   - ptr to the ospf context                    */
/* Input           : pRtEntry   - ptr to the routing update                  */
/*                   u1HopIndex   Index to the NextHop to be changed         */
/*                   pCurrPath    ptr to the Path corresponding to the dest  */
/*                   u1BitMask  - Bit Mask identifying the parameter that is */
/*                                changed                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmTxRtChngNextHopNtf (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry,
                       UINT1 u1HopIndex, tPath * pCurrPath, UINT1 u1BitMask)
{
    tNetIpv4RtInfo      NetRtInfo;
    UINT1               u1CmdType;
    tFilteringRMap     *pInFilterRMap = NULL;
    tArea              *pArea = NULL;

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* If this router is performing GR,
         * then route updates should not be given to RTM */
        return OSPF_SUCCESS;
    }

    if ((pCurrPath->pLsaInfo != NULL)
        && (pCurrPath->pLsaInfo->pOspfCxt != NULL))
    {
        pInFilterRMap = pCurrPath->pLsaInfo->pOspfCxt->pDistributeInFilterRMap;
    }

    if (UtilIpAddrComp (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr,
                        gNullIpAddr) == OSPF_EQUAL)
    {
        return OSPF_SUCCESS;
    }

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4DestNet = OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
    NetRtInfo.u4DestMask = pRtEntry->u4IpAddrMask;
    if (pCurrPath->aNextHops[u1HopIndex].pInterface->u4AddrlessIf == 0)
    {
        NetRtInfo.u4NextHop =
            OSPF_CRU_BMC_DWFROMPDU (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr);
    }
    NetRtInfo.u4RtIfIndx = pCurrPath->aNextHops[u1HopIndex].u4IfIndex;
    NetRtInfo.u4Tos = TOS_0;
    NetRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u2RtProto = OSPF_ID;
    NetRtInfo.u4RtAge = pRtEntry->u4UpdateTime;
    RtmGetRtTag (pCurrPath, &NetRtInfo.u4RouteTag);
    NetRtInfo.u1BitMask = u1BitMask;
    NetRtInfo.u1Preference = OspfFilterRouteSourceInCxt (pOspfCxt,
                                                         &NetRtInfo);

    if (pCurrPath->u1PathType != TYPE_2_EXT)
    {
        NetRtInfo.i4Metric1 = pCurrPath->u4Cost;
    }
    else
    {
        NetRtInfo.i4Metric1 = pCurrPath->u4Type2Cost;
    }

    if ((pCurrPath->u1PathType == TYPE_1_EXT)
        || (pCurrPath->u1PathType == TYPE_2_EXT))
    {
        pArea = GetFindAreaInCxt (pOspfCxt, &(pCurrPath->areaId));
        /* When the area type is NSSA area then the pathtype is 
         * incremented by 2 to inform ip that the ospf route is
         * type NSSA external type1/type2 route.
         * */
        if ((pArea != NULL) && (pArea->u4AreaType == NSSA_AREA))
        {
            NetRtInfo.u1MetricType = pCurrPath->u1PathType + 2;
        }
        else
        {
            NetRtInfo.u1MetricType = pCurrPath->u1PathType;
        }
    }
    else
    {
        NetRtInfo.u1MetricType = pCurrPath->u1PathType;
    }

    if (NetRtInfo.u1BitMask & RT_ROWSTATUS_MASK)
    {
        NetRtInfo.u4RowStatus = DESTROY;
        u1CmdType = NETIPV4_DELETE_ROUTE;
    }
    else
    {
        NetRtInfo.u4RowStatus = ACTIVE;
        u1CmdType = NETIPV4_ADD_ROUTE;
    }

    if ((u1CmdType == NETIPV4_ADD_ROUTE) || (u1CmdType == NETIPV4_MODIFY_ROUTE))
    {
        if (OSPF_FAILURE == OspfApplyInFilter (pInFilterRMap, &NetRtInfo))
        {
            pRtEntry->u1IsRmapAffected = OSPF_TRUE;
            /* Stop processing this route. */
            return OSPF_SUCCESS;
        }
    }

    if ((pOspfCxt->u1OspfRestartState == OSPF_GR_NONE) &&
        (pOspfCxt->u1PrevOspfRestartState == OSPF_GR_RESTART) &&
        (pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS))
    {
        /* To Indicate RTM that this route update is given  
           at the end of graceful restart exit */

        NetRtInfo.u2ChgBit |= IP_BIT_GR;
    }
    NetRtInfo.u4ContextId = pOspfCxt->u4OspfCxtId;
    if (NetIpv4LeakRoute (u1CmdType, &NetRtInfo) == NETIPV4_FAILURE)
    {
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmDeRegister                                           */
/*                                                                           */
/* Description     : This procedure sends the de-registration mesage to RTM. */
/*                   It is called when OSPF is disabled/shutdown.            */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmDeRegisterInCxt (tOspfCxt * pOspfCxt)
{
    tRtmRegnId          RegnId;

    ExtrtDeleteAllInCxt (pOspfCxt);
    RegnId.u2ProtoId = OSPF_ID;
    RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;
    RtmDeregister (&RegnId);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmTxRedistributeEnableInCxt                            */
/*                                                                           */
/* Description     : This procedure sends redistribute enable message to RTM */
/*                                                                           */
/* Input           : pOspfCxt - Pointer to OSPF Context                      */
/*                   u4SrcProtoBitMask - Bit Mask of source protocols from   */
/*                                       redistribution is enabled           */
/*                   pu1RMapName - Pointer to Route Map Name                 */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmTxRedistributeEnableInCxt (tOspfCxt * pOspfCxt, UINT4 u4SrcProtoBitMask,
                              UINT1 *pu1RMapName)
{
    tOsixMsg           *pRtmMsg;
    tRtmMsgHdr         *pRtmMsgHdr;
    UINT2               u2DestProtoMask;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];    /*trailing zero */

    /**prepare intermediate buf for route-map,
        ensure no garbage after map name */
    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    if (NULL != pu1RMapName)
    {
        STRNCPY (au1RMapName, pu1RMapName, (sizeof (au1RMapName) - 1));
    }

    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtmMsgHdr) +
           sizeof (UINT2) + RMAP_MAX_NAME_LEN), 0)) == NULL)

    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Buffer Allocation Failure\n");
        return OSPF_FAILURE;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) OSPF_RTM_IF_GET_MSG_HDR_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_REDISTRIBUTE_ENABLE_MESSAGE;
    pRtmMsgHdr->RegnId.u2ProtoId = OSPF_ID;
    pRtmMsgHdr->RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2) + RMAP_MAX_NAME_LEN;

    /* fill redistribution enable message specific field */
    u2DestProtoMask = (UINT2) u4SrcProtoBitMask;

    /*put protocol-mask in packet on offset 0 */
    pRtmMsgHdr->RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;
    if (IP_COPY_TO_BUF (pRtmMsg, &u2DestProtoMask,
                        0, sizeof (UINT2)) == IP_BUF_FAILURE)
    {

        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Buffer Copy Failure \n");
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return OSPF_FAILURE;
    }

    /*put route-map in packet on offset 2 */
    if (IP_COPY_TO_BUF (pRtmMsg, au1RMapName,
                        sizeof (UINT2), RMAP_MAX_NAME_LEN) == IP_BUF_FAILURE)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Buffer Copy Failure \n");
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return OSPF_FAILURE;
    }
    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) != IP_SUCCESS)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Failure in sending buffer to RTM\n");
        CRU_BUF_Release_MsgBufChain (pRtmMsg, 0);
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmTxRedistributeDisableInCxt                           */
/*                                                                           */
/* Description     : This procedure sends redistribute disable message to RTM*/
/*                                                                           */
/* Input           : pOspfCxt - Pointer to OSPF Context                      */
/*                   u4SrcProtoBitMask - Bit Mask of source protocols from   */
/*                                       redistribution is disabled          */
/*                   pu1RMapName - Pointer to Route Map Name                 */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmTxRedistributeDisableInCxt (tOspfCxt * pOspfCxt, UINT4 u4SrcProtoBitMask,
                               UINT1 *pu1RMapName)
{
    tOsixMsg           *pRtmMsg;
    tRtmMsgHdr         *pRtmMsgHdr;

    UINT2               u2DestProtoMask;

    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain (sizeof (UINT4)
                                                 + RMAP_MAX_NAME_LEN, 0))
        == NULL)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Buffer Allocation Failure\n");
        return OSPF_FAILURE;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) OSPF_RTM_IF_GET_MSG_HDR_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_REDISTRIBUTE_DISABLE_MESSAGE;
    pRtmMsgHdr->RegnId.u2ProtoId = OSPF_ID;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2);

    /* fill redistribution disable message specific field */
    /*OSPF_CRU_BMC_ASSIGN_4_BYTE (pRtmMsg, RTM_REDDSB_SRC_PROTO_MSK_OFFSET,
       u4SrcProtoBitMask); */

    u2DestProtoMask = (UINT2) u4SrcProtoBitMask;

    if (IP_COPY_TO_BUF (pRtmMsg, &u2DestProtoMask,
                        0, sizeof (UINT2)) == IP_BUF_FAILURE)
    {

        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Buffer Copy Failure \n");
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return OSPF_FAILURE;
    }

    if (IP_COPY_TO_BUF (pRtmMsg, pu1RMapName, sizeof (UINT2),
                        RMAP_MAX_NAME_LEN) == IP_BUF_FAILURE)
    {

        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Buffer Copy Failure \n");
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return OSPF_FAILURE;
    }

    pRtmMsgHdr->RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) != IP_SUCCESS)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  " Failure in sending buffer to RTM\n");
        CRU_BUF_Release_MsgBufChain (pRtmMsg, 0);
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmSrcProtoDisable                                      */
/*                                                                           */
/* Description     : This procedure is invoked from the SNMP Message Handler.*/
/*                   It scans the entire import list and flushes out all the */
/*                   previously advertised routes that are indicated by the  */
/*                   source protocol bit mask. Finally it sends a            */
/*                   redistribution disable message to RTM so as to block    */
/*                   further route updates from the corresponding source     */
/*                   protocols.                                              */
/*                                                                           */
/* Input           : u4SrcProtoBitMask - Bit Mask of source protocols from   */
/*                                       which redistribution is disabled    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmSrcProtoDisableInCxt (tOspfCxt * pOspfCxt, UINT4 u4SrcProtoBitMask)
{
    tExtRoute          *pExtRoute = NULL;
    tExtRoute          *pRouteToDelete = NULL;
    UINT1               u1Tos;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            pRouteToDelete = (tExtRoute *) pExtRoute;
            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipNetNum));
            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipAddrMask));
            inParams.Key.pKey = (UINT1 *) au4Key;
            inParams.pRoot = pOspfCxt->pExtRtRoot;
            inParams.i1AppId = OSPF_EXT_RT_ID;
            inParams.pLeafNode = NULL;
            inParams.u1PrefixLen = 0;
            pExtRoute = NULL;
            if (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                 &pAppSpecPtr,
                                 (VOID **) &(inParams.pLeafNode)) ==
                TRIE_FAILURE)
            {
                OSPF_GBL_TRC (OSPF_RTMODULE_TRC, OSPF_INVALID_CXT_ID,
                              "TrieGetNextNode failed\r\n");
            }

            pExtRoute = (tExtRoute *) pAppSpecPtr;
            if ((UtilIpAddrComp
                 (pRouteToDelete->extrtParam[TOS_0].fwdNextHop,
                  gNullIpAddr) == OSPF_EQUAL)
                && (UtilIpAddrComp (pRouteToDelete->ipNetNum, gNullIpAddr) ==
                    OSPF_EQUAL)
                && (UtilIpAddrComp (pRouteToDelete->ipAddrMask, gNullIpAddr) ==
                    OSPF_EQUAL))
            {
                continue;
            }
            switch (pRouteToDelete->u2SrcProto)
            {
                case LOCAL_RT:
                    if (u4SrcProtoBitMask & LOCAL_RT_MASK)
                    {
                        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                            pRouteToDelete->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                        }    
                        ExtrtDeleteInCxt (pOspfCxt, pRouteToDelete);
                    }
                    break;

                case STATIC_RT:
                    if (u4SrcProtoBitMask & STATIC_RT_MASK)
                    {
                        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                            pRouteToDelete->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                        }    
                        ExtrtDeleteInCxt (pOspfCxt, pRouteToDelete);
                    }
                    break;


                case RIP_RT:
                    if (u4SrcProtoBitMask & RIP_RT_MASK)
                    {
                        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                            pRouteToDelete->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                        }    
                        ExtrtDeleteInCxt (pOspfCxt, pRouteToDelete);
                    }
                    break;


                case BGP_RT:
                    if (u4SrcProtoBitMask & BGP_RT_MASK)
                    {
                        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                            pRouteToDelete->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                        }    
                        ExtrtDeleteInCxt (pOspfCxt, pRouteToDelete);
                    }
                    break;
                case ISIS_RT:
                    if ((u4SrcProtoBitMask & ISISL1_RT_MASK) &&
                        (pRouteToDelete->u1Level == IP_ISIS_LEVEL1))
                    {
                        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                            pRouteToDelete->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                        }    
                        ExtrtDeleteInCxt (pOspfCxt, pRouteToDelete);
                    }
                    else if ((u4SrcProtoBitMask & ISISL2_RT_MASK) &&
                             (pRouteToDelete->u1Level == IP_ISIS_LEVEL2))
                    {
                        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                             pRouteToDelete->aMetric[(INT2) u1Tos].rowStatus = INVALID;
                        }     
                        ExtrtDeleteInCxt (pOspfCxt, pRouteToDelete);
                    }
                    break;
                default:
                    break;

            }                    /* switch */
        }
        while (pExtRoute != NULL);
    }

    /* send redistribution disable message to RTM */
    if (RtmTxRedistributeDisableInCxt (pOspfCxt,
                                       u4SrcProtoBitMask,
                                       pOspfCxt->au1RMapName) == OSPF_FAILURE)
    {
        OSPF_TRC (OSPF_RTMODULE_TRC, pOspfCxt->u4OspfCxtId,
                  "Redistribution Disable Message Transmit to RTM Failure\n");
        return OSPF_FAILURE;
    }

    /* Update the SrcProtoBitMask */
    if (u4SrcProtoBitMask & LOCAL_RT_MASK)
    {
        pOspfCxt->u4RrdSrcProtoBitMask &= ~LOCAL_RT_MASK;
    }
    if (u4SrcProtoBitMask & STATIC_RT_MASK)
    {
        pOspfCxt->u4RrdSrcProtoBitMask &= ~STATIC_RT_MASK;
    }
    if (u4SrcProtoBitMask & RIP_RT_MASK)
    {
        pOspfCxt->u4RrdSrcProtoBitMask &= ~RIP_RT_MASK;
    }
    if (u4SrcProtoBitMask & BGP_RT_MASK)
    {
        pOspfCxt->u4RrdSrcProtoBitMask &= ~BGP_RT_MASK;
    }
    if (u4SrcProtoBitMask & ISISL1_RT_MASK)
    {
        pOspfCxt->u4RrdSrcProtoBitMask =
            (UINT4) ((pOspfCxt->
                      u4RrdSrcProtoBitMask) & (UINT4) (~ISISL1_RT_MASK));
    }
    if (u4SrcProtoBitMask & ISISL2_RT_MASK)
    {
        pOspfCxt->u4RrdSrcProtoBitMask =
            (UINT4) ((pOspfCxt->
                      u4RrdSrcProtoBitMask) & (UINT4) (~ISISL2_RT_MASK));
    }
    if (u4SrcProtoBitMask == RRD_SRC_PROTO_BIT_MASK)
    {
        MEMSET (pOspfCxt->au1RMapName, 0, RMAP_MAX_NAME_LEN);
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmGetRtTag                                             */
/*                                                                           */
/* Description     : This procedure gets the tag value for the current path. */
/*                                                                           */
/* Input           : pPath    - pointer to the path structure                */
/*                                                                           */
/* Output          : pu4RtTag - pointer to the route tag                     */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
RtmGetRtTag (tPath * pPath, UINT4 *pu4RtTag)
{
    tLsaInfo           *pLsaInfo;
    tExtLsaLink         extLsaLink;

    if ((pLsaInfo = pPath->pLsaInfo) == NULL)
    {
        return OSPF_FAILURE;
    }

    if (pLsaInfo->lsaId.u1LsaType != AS_EXT_LSA)
    {
        *pu4RtTag = 0;            /* intra-AS routes have null tag value */
    }
    else
    {
        RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, TOS_0,
                          &extLsaLink);
        *pu4RtTag = extLsaLink.u4ExtRouteTag;
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmSetRRDConfigRecordInCxt                              */
/*                                                                           */
/* Description     : This function regenarates the Type 5 Lsa for each       */
/*                   External route which is matching with the configured    */
/*                   config record.                                          */
/*                                                                           */
/* Input           : pDestId    - pointer to DestId                          */
/*                   pDestMask  - pointer to DestMask                        */
/*                                                                           */
/* Output          : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtmSetRRDConfigRecordInCxt (tOspfCxt * pOspfCxt,
                            tIPADDR * pRrdDestIPAddr,
                            tIPADDRMASK * pRrdDestAddrMask)
{
    tExtRoute          *pExtRoute = NULL;
    tRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tRedistrConfigRouteInfo *pBestRrdConfRtInfo = NULL;
    INT1                i1Tos;
    INT4                i4RetStat = OSPF_SUCCESS;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;

    pRrdConfRtInfo = GetFindRrdConfRtInfoInCxt (pOspfCxt,
                                                pRrdDestIPAddr,
                                                pRrdDestAddrMask);

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return i4RetStat;
    }
    do
    {
        pExtRoute = (tExtRoute *) pAppSpecPtr;
        /* If the TOS metric status is not ACTIVE for external route, 
         * no need to take any action.*/
        if (pExtRoute->aMetric[TOS_0].rowStatus != ACTIVE)
        {
            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipNetNum));
            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipAddrMask));
            inParams.Key.pKey = (UINT1 *) au4Key;
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
            continue;
        }
        pBestRrdConfRtInfo = FindBestRrdConfRtInfoInCxt
            (pOspfCxt, &pExtRoute->ipNetNum, &pExtRoute->ipAddrMask);

        /* If the configured record is the best match for this 
         * external route */
        if (pRrdConfRtInfo != NULL)
        {
            if (pRrdConfRtInfo == pBestRrdConfRtInfo)
            {
                for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                {
                    pExtRoute->aMetric[i1Tos].u4Value =
                        pRrdConfRtInfo->u4RrdRouteMetricValue;
                    pExtRoute->extrtParam[i1Tos].u1MetricType =
                        pRrdConfRtInfo->u1RrdRouteMetricType;
                    if (pRrdConfRtInfo->u1RedistrTagType == MANUAL_TAG)
                    {
                        pExtRoute->extrtParam[i1Tos].u4ExtTag =
                            (pRrdConfRtInfo->u4ManualTagValue &
                             MANUAL_TAG_MASK);
                    }
                }
                i4RetStat =
                    OlsifParamChangeInCxt (pOspfCxt, (UINT1 *) pExtRoute,
                                           AS_EXT_LSA);
            }
        }

        au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
        au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4Key;
        pLeafNode = inParams.pLeafNode;
        pExtRoute = NULL;

    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pAppSpecPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    return i4RetStat;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmDelRRDConfigRecord                                   */
/*                                                                           */
/* Description     : This function regenarates the Type 5 Lsa for each       */
/*                   External route which is matching with the deleted       */
/*                   config record.                                          */
/*                                                                           */
/* Input           : pDestId    - pointer to DestId                          */
/*                   pDestMask  - pointer to DestMask                        */
/*                   pRrdConfRtInfo - pointer to Config record which is      */
/*                   going to be deleted.                                    */
/*                                                                           */
/* Output          : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RtmDelRRDConfigRecordInCxt (tOspfCxt * pOspfCxt,
                            tIPADDR * pRrdDestIPAddr,
                            tIPADDRMASK * pRrdDestAddrMask)
{
    tExtRoute          *pExtRoute = NULL;
    tRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tRedistrConfigRouteInfo *pBestRrdConfRtInfo = NULL;
    tRedistrConfigRouteInfo *pNextBestRrdConfRtInfo = NULL;
    INT1                i1Tos;
    INT4                i4RetStat = OSPF_SUCCESS;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;

    pRrdConfRtInfo = GetFindRrdConfRtInfoInCxt (pOspfCxt,
                                                pRrdDestIPAddr,
                                                pRrdDestAddrMask);

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            /* If the TOS metric status is not ACTIVE for external route, 
             * no need to take any action.*/
            if (pExtRoute->aMetric[TOS_0].rowStatus != ACTIVE)
            {
                au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipNetNum));
                au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4Key;
                pLeafNode = inParams.pLeafNode;
                pExtRoute = NULL;
                continue;
            }
            pBestRrdConfRtInfo = FindBestRrdConfRtInfoInCxt
                (pOspfCxt, &pExtRoute->ipNetNum, &pExtRoute->ipAddrMask);

            /* If the deleting record is the best match for this 
             * external route, then Lsa should be regenarated  */
            if (pRrdConfRtInfo == pBestRrdConfRtInfo)
            {
                pNextBestRrdConfRtInfo = FindNextBestRrdConfRtInfoInCxt
                    (pOspfCxt, &pExtRoute->ipNetNum,
                     &pExtRoute->ipAddrMask, pBestRrdConfRtInfo);

                /* If there is no next best config record, then 
                 * the external route parameters should take default values */
                if (pNextBestRrdConfRtInfo == NULL)
                {
                    /* Fill With Default values */
                    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                    {
                        pExtRoute->aMetric[i1Tos].u4Value = AS_EXT_DEF_METRIC;
                        pExtRoute->extrtParam[i1Tos].u1MetricType
                            = TYPE_2_METRIC;
                        pExtRoute->extrtParam[i1Tos].u4ExtTag = 0;
                    }
                }
                else
                {
                    for (i1Tos = 0; i1Tos < OSPF_MAX_METRIC; i1Tos++)
                    {
                        pExtRoute->aMetric[i1Tos].u4Value =
                            pNextBestRrdConfRtInfo->u4RrdRouteMetricValue;
                        pExtRoute->extrtParam[i1Tos].u1MetricType =
                            pNextBestRrdConfRtInfo->u1RrdRouteMetricType;
                        if (pNextBestRrdConfRtInfo->u1RedistrTagType
                            == MANUAL_TAG)
                        {
                            pExtRoute->extrtParam[i1Tos].u4ExtTag =
                                (pNextBestRrdConfRtInfo->u4ManualTagValue
                                 & MANUAL_TAG_MASK);
                        }
                    }
                }
                i4RetStat = OlsifParamChangeInCxt (pOspfCxt,
                                                   (UINT1 *) pExtRoute,
                                                   AS_EXT_LSA);
            }
            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipNetNum));
            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipAddrMask));
            inParams.Key.pKey = (UINT1 *) au4Key;
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;

        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }

    TMO_SLL_Delete (&(pOspfCxt->RedistrRouteConfigLst),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    pOspfCxt->pLastConfigRouteInfo = NULL;

    REDISTR_CONFIG_INFO_FREE (pRrdConfRtInfo);
    return i4RetStat;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmGrNotifInd                                           */
/*                                                                           */
/* Description     : This routine sends an event to RTM indicating the       */
/*                   Grace period                                            */
/*                                                                           */
/* Input           : pOspfCxt         -   pointer to OSPF context            */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmGrNotifInd (tOspfCxt * pOspfCxt)
{
    tOsixMsg           *pRtmMsg;
    tRtmMsgHdr         *pRtmMsgHdr;

    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (UINT2) + sizeof (UINT4)), 0)) == NULL)

    {
        OSPF_GBL_TRC (OSPF_RTMODULE_TRC, OSPF_INVALID_CXT_ID,
                      " Buffer Allocation Failure\n");
        return OSPF_FAILURE;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) OSPF_RTM_IF_GET_MSG_HDR_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_GR_NOTIFY_MSG;
    pRtmMsgHdr->RegnId.u2ProtoId = OSPF_ID;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP_COPY_TO_BUF (pRtmMsg, &(pOspfCxt->u4GracePeriod),
                        0, sizeof (UINT4)) == IP_BUF_FAILURE)
    {
        OSPF_GBL_TRC (OSPF_RTMODULE_TRC, OSPF_INVALID_CXT_ID,
                      " Buffer Copy Failure \n");
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return OSPF_FAILURE;
    }

    pRtmMsgHdr->RegnId.u4ContextId = pOspfCxt->u4OspfCxtId;

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) != IP_SUCCESS)
    {
        OSPF_GBL_TRC (OSPF_RTMODULE_TRC, OSPF_INVALID_CXT_ID,
                      " Failure in sending buffer to RTM\n");
        CRU_BUF_Release_MsgBufChain (pRtmMsg, 0);
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : OspfApplyInFilter                                       */
/*                                                                           */
/* Description     : This function will check whether the route can be added */
/*                   or dropped. If route is permitted then returned         */
/*                   OSPF_SUCCESS else return OSPF_FAILURE.                  */
/*                                                                           */
/* Input           : pInFilterRMap  - route map data                         */
/*                   pRtEntry       - ptr to the routing update              */
/*                                                                           */
/* Output          : None.                                                   */
/*                                                                           */
/* Returns         : OSPF_SUCCESS/OSPF_FAILURE.                              */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT1
OspfApplyInFilter (tFilteringRMap * pFilterRMap, tNetIpv4RtInfo * pRtEntry)
{
#ifdef ROUTEMAP_WANTED
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;
    UINT1               u1Status = 0;

    if ((pFilterRMap == NULL) || (pRtEntry == NULL))
    {
        return OSPF_SUCCESS;
    }

/*  If status of route map is disable then this route should not be droped */
    u1Status = RMapGetMapStatus (pFilterRMap->au1DistInOutFilterRMapName );
    if (u1Status == FILTERNIG_STAT_DISABLE)
    {
        return OSPF_SUCCESS;
    }

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));

    IPVX_ADDR_INIT_FROMV4 (RtInfoIn.DstXAddr, pRtEntry->u4DestNet);
    IPV4_MASK_TO_MASKLEN (RtInfoIn.u2DstPrefixLen, pRtEntry->u4DestMask);
    RtInfoIn.u1MetricType = pRtEntry->u1MetricType;
    RtInfoIn.i4Metric = pRtEntry->i4Metric1;
    RtInfoIn.i2RouteType = (INT2) pRtEntry->u2RtType;
    RtInfoIn.u4RouteTag = pRtEntry->u4RouteTag;
    RtInfoIn.u4IfIndex = pRtEntry->u4RtIfIndx;
    IPVX_ADDR_INIT_FROMV4 (RtInfoIn.NextHopXAddr, pRtEntry->u4NextHop);

/*  Ignore modification of route attributes by route map */

    if (RMAP_ROUTE_PERMIT == RMapApplyRule (&RtInfoIn, &RtInfoOut,
                                            pFilterRMap->
                                            au1DistInOutFilterRMapName))
    {

        pRtEntry->u1MetricType = RtInfoOut.u1MetricType;
        pRtEntry->u2RtType = RtInfoOut.i2RouteType;
        pRtEntry->u4RouteTag = RtInfoOut.u4RouteTag;
        pRtEntry->i4Metric1 = RtInfoOut.i4Metric;
        pRtEntry->u4RtIfIndx = RtInfoOut.u4IfIndex;
        pRtEntry->u4NextHop =
            OSIX_NTOHL (OSPF_CRU_BMC_DWFROMPDU
                        (RtInfoOut.NextHopXAddr.au1Addr));
        return OSPF_SUCCESS;
    }
    else
    {
        return OSPF_FAILURE;
    }
#else
    UNUSED_PARAM (pFilterRMap);
    UNUSED_PARAM (pRtEntry);
    return OSPF_SUCCESS;
#endif /*ROUTEMAP_WANTED */
}

/**************************************************************************/
/* Function Name : OspfFilterRouteSourceInCxt                             */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. pOspfCxt  - OspfContext Pointer                     */
/*               : 2. pNetIpRtInfo - NetIpv4 Pointer                      */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
OspfFilterRouteSourceInCxt (tOspfCxt * pOspfCxt, tNetIpv4RtInfo * pNetIpRtInfo)
{
    UINT1               u1Distance = pOspfCxt->u1Distance;
#ifdef ROUTEMAP_WANTED

    if (pOspfCxt->pDistanceFilterRMap != NULL
        && pOspfCxt->pDistanceFilterRMap->u1Status == FILTERNIG_STAT_ENABLE)
    {
        tRtMapInfo          MapInfo;
        MEMSET (&MapInfo, 0, sizeof (MapInfo));
        IPVX_ADDR_INIT_FROMV4 (MapInfo.DstXAddr, pNetIpRtInfo->u4DestNet);
        IPVX_ADDR_INIT_FROMV4 (MapInfo.NextHopXAddr, pNetIpRtInfo->u4NextHop);
        IPV4_MASK_TO_MASKLEN (MapInfo.u2DstPrefixLen,pNetIpRtInfo->u4DestMask);
        MapInfo.u4RouteTag = pNetIpRtInfo->u4RouteTag;
        if (RMapApplyRule2 (&MapInfo,
                            pOspfCxt->pDistanceFilterRMap->
                            au1DistInOutFilterRMapName) == RMAP_ENTRY_MATCHED)
        {
            u1Distance = pOspfCxt->pDistanceFilterRMap->u1RMapDistance;
        }
    }

#else
    UNUSED_PARAM (pNetIpRtInfo);
#endif /*ROUTEMAP_WANTED */
    return u1Distance;
}

#ifdef ROUTEMAP_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfGetMinFilterRMap                                       */
/*                                                                           */
/* Description  : This function returns the lexicographically smaller        */
/*                filter                                                     */
/*                                                                           */
/* Input        : pFilterRMap1  -   pointer to first RMAP filter             */
/*                i1Type1       -   First RMAP type                          */
/*                pFilterRMap2  -   pointer to second RMAP filter            */
/*                i1Type2       -   Second RMAP type                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to RMAP filter or NULL                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tFilteringRMap *
OspfGetMinFilterRMap (tFilteringRMap * pFilterRMap1, INT1 i1Type1,
                      tFilteringRMap * pFilterRMap2, INT1 i1Type2)
{
    INT4                i4RetVal = 0;
    if (pFilterRMap1 == NULL && pFilterRMap2 != NULL)
    {
        return pFilterRMap2;
    }

    if (pFilterRMap1 != NULL && pFilterRMap2 == NULL)
    {
        return pFilterRMap1;
    }

    if (pFilterRMap1 == NULL && pFilterRMap2 == NULL)
    {
        return pFilterRMap1;
    }

    if (STRLEN (pFilterRMap1->au1DistInOutFilterRMapName) >
        STRLEN (pFilterRMap2->au1DistInOutFilterRMapName))
    {
        return pFilterRMap2;
    }
    else if (STRLEN (pFilterRMap1->au1DistInOutFilterRMapName) <
             STRLEN (pFilterRMap2->au1DistInOutFilterRMapName))
    {
        return pFilterRMap1;
    }
    else
    {
        i4RetVal = MEMCMP (pFilterRMap1->au1DistInOutFilterRMapName,
                           pFilterRMap2->au1DistInOutFilterRMapName,
                           STRLEN (pFilterRMap1->au1DistInOutFilterRMapName));
        if (i4RetVal < 0)
        {
            return pFilterRMap1;
        }
        else if (i4RetVal > 0)
        {
            return pFilterRMap2;
        }
    }

    if (i1Type1 > i1Type2)
    {
        return pFilterRMap2;
    }
    return pFilterRMap1;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCmpFilterRMapName                                      */
/*                                                                           */
/* Description  : This function compares the filters and return the status   */
/*                                                                           */
/* Input        : pFilterRMap   -   pointer to first RMAP filter             */
/*                pRMapName     -   RMAP name                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Comparison result                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfCmpFilterRMapName (tFilteringRMap * pFilterRMap,
                       tSNMP_OCTET_STRING_TYPE * pRMapName)
{
    INT1                i1RetVal = 1;

    if (pFilterRMap != NULL
        && pRMapName != NULL
        && (UINT4) pRMapName->i4_Length ==
        STRLEN (pFilterRMap->au1DistInOutFilterRMapName))
    {
        return STRNCMP (pFilterRMap->au1DistInOutFilterRMapName,
                        pRMapName->pu1_OctetList, pRMapName->i4_Length);
    }

    return i1RetVal;
}
#endif
