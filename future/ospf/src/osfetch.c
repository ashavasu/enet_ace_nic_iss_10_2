/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osfetch.c,v 1.59 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains the Snmp low level get 
 *             routines
 *
 *******************************************************************/

#include  "osinc.h"
#include "stdoslow.h"
#include "fsostlow.h"
#include "ospftlow.h"
#include "ospfcli.h"
PRIVATE INT1 GetLeastNextHopIpAddr PROTO ((tRtEntry * pRtEntry,
                                           tIPADDR * pLeastNextHopIpAddr,
                                           UINT1 u1Tos));

PRIVATE INT1 GetNextLeastNextHopIpAddr PROTO ((tRtEntry * pRtEntry,
                                               tIPADDR * pNextHopIpAddr,
                                               tIPADDR *
                                               pNextLeastNextHopIpAddr,
                                               UINT1 u1Tos));

PRIVATE INT1        GetIsNextLsaTableIndex
PROTO ((tAreaId * pCurrAreaId,
        UINT1 u1AreaIdLen,
        UINT1 u1CurrLsaType,
        tLINKSTATEID * pCurrLinkStateId,
        UINT1 u1LsIdLen,
        tRouterId * pCurrAdvRtrId, UINT1 u1AdvRtrIdLen, tLsaInfo * pLsaInfo));

PRIVATE tNeighbor  *GetFindVirtNbrInCxt PROTO ((tOspfCxt * pOspfCxt,
                                                tAreaId * pTransitAreaId,
                                                tRouterId * pNbrId));

PRIVATE INT1 GetLeastSecIpAddrInCxt PROTO ((tOspfCxt * pOspfCxt,
                                            UINT4 u4OspfPrimIpAddr,
                                            INT4 i4OspfPrimAddresslessIf,
                                            tIPADDR * pSecIpAddr,
                                            tIPADDRMASK * pSecIpAddrMask));

PRIVATE INT1
    OspfRtTblGetObjectInCxt PROTO ((tOspfCxt * pOspfCxt, UINT4 u4Dest,
                                    UINT4 u4Mask, INT4 i4Tos,
                                    UINT4 u4NextHop, UINT4 *pu4Val,
                                    UINT1 u1ObjType, UINT1 u1DestType));

PRIVATE VOID OspfGetRTAttribute PROTO ((tRtEntry * pRtEntry,
                                        UINT4 u4NextHop,
                                        UINT1 u1ObjType, UINT4 *pu4ObjVal,
                                        INT4 i4Tos));
PRIVATE INT1 OspfGetNextAsExternalAggregationIndex PROTO
    ((tIPADDR currAsNet, tIPADDR nextAsNet,
      tIPADDRMASK currAsExtMask, tIPADDRMASK nextAsExtMask,
      tIPADDR currAsExtAreaId, tIPADDR nextAsExtAreaId));

PRIVATE INT1 OspfFindNextHopIpAddr PROTO
    ((tRtEntry * pRtEntry, tIPADDR * pNextHopIpAddr, UINT1 u1Tos));

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfRouterId
 Input       :  The Indices

                The Object 
                retValOspfRouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfRouterId (UINT4 *pu4RetValOspfRouterId)
{
    tRouterId           rtrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    IP_ADDR_COPY (rtrId, pOspfCxt->rtrId);
    *pu4RetValOspfRouterId = OSPF_CRU_BMC_DWFROMPDU (rtrId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfAdminStat
 Input       :  The Indices

                The Object 
                retValOspfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAdminStat (INT4 *pi4RetValOspfAdminStat)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValOspfAdminStat = (INT4) pOspfCxt->admnStat;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfVersionNumber
 Input       :  The Indices

                The Object 
                retValOspfVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVersionNumber (INT4 *pi4RetValOspfVersionNumber)
{
    *pi4RetValOspfVersionNumber = OSPF_VERSION_NO_2;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaBdrRtrStatus
 Input       :  The Indices

                The Object 
                retValOspfAreaBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaBdrRtrStatus (INT4 *pi4RetValOspfAreaBdrRtrStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfAreaBdrRtrStatus = (INT4) pOspfCxt->bAreaBdrRtr;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfASBdrRtrStatus
 Input       :  The Indices

                The Object 
                retValOspfASBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfASBdrRtrStatus (INT4 *pi4RetValOspfASBdrRtrStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfASBdrRtrStatus = (INT4) pOspfCxt->bAsBdrRtr;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfExternLsaCount
 Input       :  The Indices

                The Object 
                retValOspfExternLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExternLsaCount (UINT4 *pu4RetValOspfExternLsaCount)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfExternLsaCount = GET_ASE_LSA_COUNT_IN_CXT (pOspfCxt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfExternLsaCksumSum
 Input       :  The Indices

                The Object 
                retValOspfExternLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExternLsaCksumSum (INT4 *pi4RetValOspfExternLsaCksumSum)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfExternLsaCksumSum = (INT4) pOspfCxt->u4ExtLsaChksumSum;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfTOSSupport
 Input       :  The Indices

                The Object 
                retValOspfTOSSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfTOSSupport (INT4 *pi4RetValOspfTOSSupport)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

#ifndef TOS_SUPPORT
    *pi4RetValOspfTOSSupport = OSPF_FALSE;
#else /* TOS_SUPPORT */
    *pi4RetValOspfTOSSupport = (INT4) pOspfCxt->bTosCapability;
#endif /* TOS_SUPPORT */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfOriginateNewLsas
 Input       :  The Indices

                The Object 
                retValOspfOriginateNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfOriginateNewLsas (UINT4 *pu4RetValOspfOriginateNewLsas)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfOriginateNewLsas = pOspfCxt->u4OriginateNewLsa;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfRxNewLsas
 Input       :  The Indices

                The Object 
                retValOspfRxNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfRxNewLsas (UINT4 *pu4RetValOspfRxNewLsas)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfRxNewLsas = pOspfCxt->u4RcvNewLsa;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfExtLsdbLimit
 Input       :  The Indices

                The Object 
                retValOspfExtLsdbLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExtLsdbLimit (INT4 *pi4RetValOspfExtLsdbLimit)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfExtLsdbLimit = pOspfCxt->i4ExtLsdbLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfMulticastExtensions
 Input       :  The Indices

                The Object 
                retValOspfMulticastExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfMulticastExtensions (INT4 *pi4RetValOspfMulticastExtensions)
{
    *pi4RetValOspfMulticastExtensions = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfExitOverflowInterval
 Input       :  The Indices

                The Object 
                retValOspfExitOverflowInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExitOverflowInterval (INT4 *pi4RetValOspfExitOverflowInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfExitOverflowInterval =
        (INT4) (pOspfCxt->u4ExitOverflowInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfDemandExtensions
 Input       :  The Indices

                The Object 
                retValOspfDemandExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfDemandExtensions (INT4 *pi4RetValOspfDemandExtensions)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfDemandExtensions = (INT4) pOspfCxt->bDemandExtension;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfAreaTable
 Input       :  The Indices
                OspfAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexOspfAreaTable (UINT4 *pu4OspfAreaId)
{
    *pu4OspfAreaId = OSPF_CRU_BMC_DWFROMPDU (gNullIpAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfAreaTable
 Input       :  The Indices
                OspfAreaId
                nextOspfAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfAreaTable (UINT4 u4OspfAreaId, UINT4 *pu4NextOspfAreaId)
{
    tAreaId             currAreaId;
    tAreaId             nextAreaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    IP_ADDR_COPY (nextAreaId, gNullIpAddr);

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4OspfAreaId);
    pArea = (tArea *) TMO_SLL_First (&(pOspfCxt->areasLst));
    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    if (UtilIpAddrComp (pArea->areaId, currAreaId) != OSPF_GREATER)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (UtilIpAddrComp (currAreaId, pArea->areaId) == OSPF_LESS)
        {
            IP_ADDR_COPY (nextAreaId, pArea->areaId);
        }
        else
        {
            *pu4NextOspfAreaId = OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
            return SNMP_SUCCESS;
        }
    }
    if (UtilIpAddrComp (currAreaId, nextAreaId) == OSPF_LESS)
    {
        *pu4NextOspfAreaId = OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  GetFindAreaInCxt
 Input       :  The Indices
                pOspfCxt
                pAreaId
 
 Output      :  None
                
 Returns     : pointer to the area if successfull
               NULL  otherwise  
****************************************************************************/

PUBLIC tArea       *
GetFindAreaInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId)
{
    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (UtilIpAddrComp (*pAreaId, pArea->areaId) == OSPF_EQUAL)
        {
            return pArea;
        }
    }
    if ((UtilIpAddrComp (gBackboneAreaId, *pAreaId)) == OSPF_EQUAL)
    {
        return (pOspfCxt->pBackbone);
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfAuthType
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAuthType (UINT4 u4OspfAreaId, INT4 *pu4RetValOspfAuthType)
{

    UNUSED_PARAM (u4OspfAreaId);

    *pu4RetValOspfAuthType = NO_AUTHENTICATION;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfImportAsExtern
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfImportAsExtern
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfImportAsExtern (UINT4 u4OspfAreaId, INT4 *pi4RetValOspfImportAsExtern)
{

    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {

        *pi4RetValOspfImportAsExtern = (INT4) (pArea->u4AreaType);
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfSpfRuns
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfSpfRuns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfSpfRuns (UINT4 u4OspfAreaId, UINT4 *pu4RetValOspfSpfRuns)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfSpfRuns = pArea->u2SpfRuns;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaBdrRtrCount
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAreaBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaBdrRtrCount (UINT4 u4OspfAreaId,
                           UINT4 *pu4RetValOspfAreaBdrRtrCount)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfAreaBdrRtrCount = pArea->u4AreaBdrRtrCount;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAsBdrRtrCount
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAsBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAsBdrRtrCount (UINT4 u4OspfAreaId, UINT4 *pu4RetValOspfAsBdrRtrCount)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfAsBdrRtrCount = pArea->u4AsBdrRtrCount;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaLsaCount
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAreaLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaLsaCount (UINT4 u4OspfAreaId, UINT4 *pu4RetValOspfAreaLsaCount)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SummaryLsaCount = 0;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        RBTreeCount (pArea->pSummaryLsaRoot, &u4SummaryLsaCount);
        *pu4RetValOspfAreaLsaCount = (TMO_SLL_Count (&pArea->rtrLsaLst) +
                                      TMO_SLL_Count (&pArea->networkLsaLst) +
                                      u4SummaryLsaCount +
                                      TMO_SLL_Count (&pArea->nssaLSALst));
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaLsaCksumSum
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAreaLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaLsaCksumSum (UINT4 u4OspfAreaId,
                           INT4 *pi4RetValOspfAreaLsaCksumSum)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfAreaLsaCksumSum = (INT4) pArea->u4AreaLsaChksumSum;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaSummary
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAreaSummary
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaSummary (UINT4 u4OspfAreaId, INT4 *pi4RetValOspfAreaSummary)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfAreaSummary = (INT4) pArea->u1SummaryFunctionality;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaStatus
 Input       :  The Indices
                OspfAreaId

                The Object 
                retValOspfAreaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaStatus (UINT4 u4OspfAreaId, INT4 *pi4RetValOspfAreaStatus)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfAreaStatus = (INT4) pArea->areaStatus;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfStubAreaTable
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfStubAreaTable (UINT4 *pu4OspfStubAreaId,
                                   INT4 *pi4OspfStubTOS)
{
    tArea              *pArea = NULL;
    tArea              *pNextArea = NULL;
    UINT1               u1Tos;
    UINT1               u1Found = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if ((pArea->u4AreaType == NORMAL_AREA))
        {
            continue;
        }
        u1Found = OSPF_TRUE;
        pNextArea = pArea;
    }

    if (u1Found == OSPF_TRUE)
    {
        for (u1Tos = 0; u1Tos < ENCODE_TOS (OSPF_MAX_METRIC); u1Tos += 2)
        {
            if ((pNextArea->
                 aStubDefaultCost[DECODE_TOS (u1Tos)].rowStatus != INVALID))
            {
                *pi4OspfStubTOS = u1Tos;
                *pu4OspfStubAreaId = OSPF_CRU_BMC_DWFROMPDU (pNextArea->areaId);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfStubAreaTable
 Input       :  The Indices
                OspfStubAreaId
                nextOspfStubAreaId
                OspfStubTOS
                nextOspfStubTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfStubAreaTable (UINT4 u4OspfStubAreaId,
                                  UINT4 *pu4NextOspfStubAreaId,
                                  INT4 i4OspfStubTOS, INT4 *pi4NextOspfStubTOS)
{
    tAreaId             currAreaId;
    tArea              *pArea = NULL;
    tArea              *pNextArea = NULL;
    UINT1               u1CmpVal;
    UINT1               u1Tos;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4OspfStubAreaId);

    if (i4OspfStubTOS < 0
        && (UtilIpAddrComp (currAreaId, gNullIpAddr) != OSPF_EQUAL))
    {
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType == NORMAL_AREA)
        {
            continue;
        }

        u1CmpVal = UtilIpAddrComp (currAreaId, (pArea->areaId));

        if (u1CmpVal == OSPF_EQUAL)
        {
            /* To See if any other TOS entries are present */
            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos += 2)
            {
                if ((pArea->
                     aStubDefaultCost[DECODE_TOS (u1Tos)].rowStatus !=
                     INVALID) && (i4OspfStubTOS < u1Tos))
                {
                    *pu4NextOspfStubAreaId =
                        OSPF_CRU_BMC_DWFROMPDU (pArea->areaId);
                    *pi4NextOspfStubTOS = u1Tos;
                    return SNMP_SUCCESS;
                }
            }

            break;
        }
        else if (u1CmpVal == OSPF_LESS)
        {
            /* The Area List is in decreasing order and
             * so no need to scan further in the area list. */
            pNextArea = pArea;
        }
    }
    /* If pNextArea is NULL, means no more entries in this table
     * So terminate the walk. */
    if (pNextArea == NULL)
    {
        return SNMP_FAILURE;
    }
    for (u1Tos = 0; u1Tos < ENCODE_TOS (OSPF_MAX_METRIC); u1Tos += 2)
    {
        if ((pNextArea->
             aStubDefaultCost[DECODE_TOS (u1Tos)].rowStatus != INVALID))
        {
            *pi4NextOspfStubTOS = u1Tos;
            *pu4NextOspfStubAreaId = OSPF_CRU_BMC_DWFROMPDU (pNextArea->areaId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfStubMetric
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                retValOspfStubMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfStubMetric (UINT4 u4OspfStubAreaId, INT4 i4OspfStubTOS,
                      INT4 *pi4RetValOspfStubMetric)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValOspfStubMetric = (INT4)
        pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].u4Value;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfStubStatus
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                retValOspfStubStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfStubStatus (UINT4 u4OspfStubAreaId, INT4 i4OspfStubTOS,
                      INT4 *pi4RetValOspfStubStatus)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);
    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);
    if (pArea != NULL)
    {
        *pi4RetValOspfStubStatus =
            pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
            rowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfStubMetricType
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                retValOspfStubMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfStubMetricType (UINT4 u4OspfStubAreaId, INT4 i4OspfStubTOS,
                          INT4 *pi4RetValOspfStubMetricType)
{

    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not Found \n");
        return SNMP_FAILURE;
    }

    *pi4RetValOspfStubMetricType =
        (INT4) pArea->aStubDefaultCost[DECODE_TOS ((INT1)
                                                   i4OspfStubTOS)].u4MetricType;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  GetIsNextLsaTableIndex  
 Input       :  The Indices
                pCurrAreaId               
                u1AreaIdLen
                u1CurrLsaType
                pCurrLinkStateId
                u1LsIdLen
                pCurrAdvRtrId
                u1AdvRtrIdLen
                pLsaInfo    

 Output      :  None 
               
 Returns     :  OSPF_TRUE or OSPF_FALSE
****************************************************************************/
PUBLIC INT1
GetIsNextLsaTableIndex (tAreaId * pCurrAreaId,
                        UINT1 u1AreaIdLen,
                        UINT1 u1CurrLsaType,
                        tLINKSTATEID * pCurrLinkStateId,
                        UINT1 u1LsIdLen,
                        tRouterId * pCurrAdvRtrId,
                        UINT1 u1AdvRtrIdLen, tLsaInfo * pLsaInfo)
{
    /* compare areaId */
    switch (UtilIpAddrNComp (&(pLsaInfo->pArea->areaId),
                             pCurrAreaId, u1AreaIdLen))
    {
        case OSPF_GREATER:
            return OSPF_TRUE;
        case OSPF_LESS:
            return OSPF_FALSE;
        case OSPF_EQUAL:
            if (u1AreaIdLen < MAX_IP_ADDR_LEN)
            {
                return OSPF_TRUE;
            }
            break;
        default:
            break;
    }
    /* compare lsa type */
    if (pLsaInfo->lsaId.u1LsaType < u1CurrLsaType)
    {
        return OSPF_FALSE;
    }
    else if (pLsaInfo->lsaId.u1LsaType > u1CurrLsaType)
    {
        return OSPF_TRUE;
    }
    /* compare link state id */
    switch (UtilIpAddrNComp (&(pLsaInfo->lsaId.linkStateId),
                             pCurrLinkStateId, u1LsIdLen))
    {
        case OSPF_LESS:
            return OSPF_FALSE;
        case OSPF_GREATER:
            return OSPF_TRUE;
        case OSPF_EQUAL:
            if (u1LsIdLen < MAX_IP_ADDR_LEN)
            {
                return OSPF_TRUE;
            }
            break;
        default:
            break;
    }
    /* compare advertising router id */
    switch (UtilIpAddrNComp (&(pLsaInfo->lsaId.advRtrId),
                             pCurrAdvRtrId, u1AdvRtrIdLen))
    {
        case OSPF_LESS:
            return OSPF_FALSE;
        case OSPF_GREATER:
            return OSPF_TRUE;
        case OSPF_EQUAL:
            if (u1AdvRtrIdLen < MAX_IP_ADDR_LEN)
            {
                return OSPF_TRUE;
            }
            break;
        default:
            break;
    }
    return OSPF_FALSE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfLsdbTable
 Input       :  The Indices
                OspfLsdbAreaId
                OspfLsdbType
                OspfLsdbLsid
                OspfLsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfLsdbTable (UINT4 *pu4OspfLsdbAreaId,
                               INT4 *pi4OspfLsdbType,
                               UINT4 *pu4OspfLsdbLsid,
                               UINT4 *pu4OspfLsdbRouterId)
{
    tAreaId             areaId;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tArea              *pArea;
    tTMO_SLL           *pLsaLst;
    tTMO_SLL_NODE      *pLstNode;
    tLsaInfo           *pLsaInfo;
    UINT1               i1LsaType;
    INT1                i1AreaCount;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pOspfCxt->pLastLsdbLst = NULL;
    pOspfCxt->pLastLsdbNode = NULL;

    for (i1AreaCount = (INT1) TMO_SLL_Count (&(pOspfCxt->areasLst));
         i1AreaCount > 0; i1AreaCount--)
    {
        pArea = (tArea *) TMO_SLL_Nth (&(pOspfCxt->areasLst), i1AreaCount);
        if (pArea == NULL)
        {
            return SNMP_FAILURE;
        }
        for (i1LsaType = ROUTER_LSA; i1LsaType <= TYPE10_OPQ_LSA; i1LsaType++)
        {
            if ((i1LsaType == NETWORK_SUM_LSA) || (i1LsaType == ASBR_SUM_LSA))
            {
                pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
                if (pLsaInfo != NULL)
                {
                    IP_ADDR_COPY (areaId, pArea->areaId);
                    *pi4OspfLsdbType = (INT4) pLsaInfo->lsaId.u1LsaType;
                    IP_ADDR_COPY (linkStateId, pLsaInfo->lsaId.linkStateId);
                    IP_ADDR_COPY (advRtrId, pLsaInfo->lsaId.advRtrId);
                    *pu4OspfLsdbAreaId = OSPF_CRU_BMC_DWFROMPDU (areaId);
                    *pu4OspfLsdbLsid = OSPF_CRU_BMC_DWFROMPDU (linkStateId);
                    *pu4OspfLsdbRouterId = OSPF_CRU_BMC_DWFROMPDU (advRtrId);
                    pOspfCxt->pLastLsdbLst = NULL;
                    pOspfCxt->pLastLsdbNode = NULL;
                    return SNMP_SUCCESS;
                }
            }

            switch (i1LsaType)
            {
                case ROUTER_LSA:
                    pLsaLst = &(pArea->rtrLsaLst);
                    break;
                case NETWORK_LSA:
                    pLsaLst = &(pArea->networkLsaLst);
                    break;
                case TYPE10_OPQ_LSA:
                    pLsaLst = &(pArea->Type10OpqLSALst);
                    break;
                case NSSA_LSA:
                    pLsaLst = &(pArea->nssaLSALst);
                    break;
                default:
                    pLsaLst = NULL;
                    break;
            }

            if (pLsaLst != NULL)
            {
                if ((pLstNode = TMO_SLL_First (pLsaLst)) != NULL)
                {
                    pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
                    IP_ADDR_COPY (areaId, pArea->areaId);
                    *pi4OspfLsdbType = (INT4) pLsaInfo->lsaId.u1LsaType;
                    IP_ADDR_COPY (linkStateId, pLsaInfo->lsaId.linkStateId);
                    IP_ADDR_COPY (advRtrId, pLsaInfo->lsaId.advRtrId);
                    *pu4OspfLsdbAreaId = OSPF_CRU_BMC_DWFROMPDU (areaId);
                    *pu4OspfLsdbLsid = OSPF_CRU_BMC_DWFROMPDU (linkStateId);
                    *pu4OspfLsdbRouterId = OSPF_CRU_BMC_DWFROMPDU (advRtrId);
                    pOspfCxt->pLastLsdbLst = pLsaLst;
                    pOspfCxt->pLastLsdbNode = pLstNode;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfLsdbTable
 Input       :  The Indices
                OspfLsdbAreaId
                nextOspfLsdbAreaId
                OspfLsdbType
                nextOspfLsdbType
                OspfLsdbLsid
                nextOspfLsdbLsid
                OspfLsdbRouterId
                nextOspfLsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfLsdbTable (UINT4 u4OspfLsdbAreaId,
                              UINT4 *pu4NextOspfLsdbAreaId,
                              INT4 i4OspfLsdbType,
                              INT4 *pi4NextOspfLsdbType,
                              UINT4 u4OspfLsdbLsid,
                              UINT4 *pu4NextOspfLsdbLsid,
                              UINT4 u4OspfLsdbRouterId,
                              UINT4 *pu4NextOspfLsdbRouterId)
{
    tAreaId             currAreaId;
    tAreaId             nextAreaId;
    tLINKSTATEID        currLinkStateId;
    tLINKSTATEID        nextLinkStateId;
    tRouterId           currAdvRtrId;
    tRouterId           nextAdvRtrId;
    tArea              *pArea;
    INT1                i1LsaType;
    tTMO_SLL           *pLsaLst;
    tLsaInfo           *pLsaInfo;
    tLsaInfo            LsaInfo;
    tLsaInfo           *pNextLsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode;
    INT1                i1AreaCount = 0;
    UINT1               u1AreaIdLen = MAX_IP_ADDR_LEN;
    UINT1               u1LsIdLen = MAX_IP_ADDR_LEN;
    UINT1               u1AdvRtrIdLen = MAX_IP_ADDR_LEN;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    INT1                i1CurrentLsaType;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4OspfLsdbAreaId);
    OSPF_CRU_BMC_DWTOPDU (currLinkStateId, u4OspfLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (currAdvRtrId, u4OspfLsdbRouterId);

    i1CurrentLsaType = ROUTER_LSA;

    if ((pOspfCxt->pLastLsdbLst != NULL) && (pOspfCxt->pLastLsdbNode != NULL))
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);

        if ((UtilIpAddrNComp
             (&(pLsaInfo->pArea->areaId), &currAreaId,
              u1AreaIdLen) == OSPF_EQUAL)
            && (pLsaInfo->lsaId.u1LsaType == i4OspfLsdbType)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.linkStateId), &currLinkStateId,
              u1LsIdLen) == OSPF_EQUAL)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.advRtrId), &currAdvRtrId,
              u1AdvRtrIdLen) == OSPF_EQUAL))
        {
            /* Get the Next Lsa from the List */
            pOspfCxt->pLastLsdbNode =
                (tTMO_SLL_NODE *) TMO_SLL_Next (pOspfCxt->pLastLsdbLst,
                                                pOspfCxt->pLastLsdbNode);
            if (pOspfCxt->pLastLsdbNode != NULL)
            {

                pNextLsaInfo =
                    GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);
                IP_ADDR_COPY (nextAreaId, pNextLsaInfo->pArea->areaId);
                *pi4NextOspfLsdbType = (INT4) (pNextLsaInfo->lsaId.u1LsaType);
                IP_ADDR_COPY (nextLinkStateId, pNextLsaInfo->lsaId.linkStateId);
                IP_ADDR_COPY (nextAdvRtrId, pNextLsaInfo->lsaId.advRtrId);
                *pu4NextOspfLsdbAreaId = OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                *pu4NextOspfLsdbLsid = OSPF_CRU_BMC_DWFROMPDU (nextLinkStateId);
                *pu4NextOspfLsdbRouterId =
                    OSPF_CRU_BMC_DWFROMPDU (nextAdvRtrId);

                return SNMP_SUCCESS;
            }
            else
            {
                if (i4OspfLsdbType < TYPE10_OPQ_LSA)
                {
                    i1CurrentLsaType = (INT1) (i4OspfLsdbType + 1);
                }
            }
        }
    }
    else
    {
        if (i4OspfLsdbType < TYPE10_OPQ_LSA)
        {
            i1CurrentLsaType = (INT1) i4OspfLsdbType;
        }
    }
    /* Reset  the Cache pointers */
    pOspfCxt->pLastLsdbLst = NULL;
    pOspfCxt->pLastLsdbNode = NULL;

    for (i1AreaCount = (INT1) TMO_SLL_Count (&(pOspfCxt->areasLst));
         i1AreaCount > 0; i1AreaCount--)
    {
        pArea =
            (tArea *) TMO_SLL_Nth (&(pOspfCxt->areasLst), (UINT4) i1AreaCount);

        if (NULL == pArea)
        {
            continue;
        }
        if (UtilIpAddrNComp (&(pArea->areaId), &currAreaId,
                             MAX_IP_ADDR_LEN) == OSPF_LESS)
        {
            /* Current Area ID is less than the input Area ID
             * hence skip and continue to the next area equal or
             * greater area 
             */
            continue;
        }

        for (i1LsaType = i1CurrentLsaType; i1LsaType <= TYPE10_OPQ_LSA;
             i1LsaType++)
        {
            if ((i1LsaType == NETWORK_SUM_LSA) || (i1LsaType == ASBR_SUM_LSA))
            {
                MEMSET (&LsaInfo, 0, sizeof (tLsaInfo));
                LsaInfo.lsaId.u1LsaType = (UINT1) i1LsaType;
                if (i1LsaType == i4OspfLsdbType)
                {
                    /* Fill in the current LSA info only if the
                     * i4OspfLsdbType and i1LsaType matches
                     */
                    IP_ADDR_COPY (LsaInfo.lsaId.linkStateId, currLinkStateId);
                    IP_ADDR_COPY (LsaInfo.lsaId.advRtrId, currAdvRtrId);
                }
                pLsaInfo = NULL;
                pLsaInfo =
                    (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot,
                                                &LsaInfo, NULL);
                if (pLsaInfo != NULL)
                {
                    IP_ADDR_COPY (nextAreaId, pArea->areaId);
                    *pi4NextOspfLsdbType = (INT4) (pLsaInfo->lsaId.u1LsaType);
                    IP_ADDR_COPY (nextLinkStateId, pLsaInfo->lsaId.linkStateId);
                    IP_ADDR_COPY (nextAdvRtrId, pLsaInfo->lsaId.advRtrId);
                    *pu4NextOspfLsdbAreaId =
                        OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                    *pu4NextOspfLsdbLsid =
                        OSPF_CRU_BMC_DWFROMPDU (nextLinkStateId);
                    *pu4NextOspfLsdbRouterId =
                        OSPF_CRU_BMC_DWFROMPDU (nextAdvRtrId);
                    pOspfCxt->pLastLsdbLst = NULL;
                    pOspfCxt->pLastLsdbNode = NULL;
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                switch (i1LsaType)
                {
                    case ROUTER_LSA:
                        pLsaLst = &(pArea->rtrLsaLst);
                        break;
                    case NETWORK_LSA:
                        pLsaLst = &(pArea->networkLsaLst);
                        break;
                    case TYPE10_OPQ_LSA:
                        pLsaLst = &(pArea->Type10OpqLSALst);
                        break;

                    case NSSA_LSA:
                        pLsaLst = &(pArea->nssaLSALst);
                        break;
                    default:
                        pLsaLst = NULL;
                        break;
                }

                if (pLsaLst != NULL)
                {
                    TMO_SLL_Scan (pLsaLst, pLsaNode, tTMO_SLL_NODE *)
                    {
                        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                        if (GetIsNextLsaTableIndex
                            (&(currAreaId), u1AreaIdLen,
                             (UINT1) i4OspfLsdbType, &(currLinkStateId),
                             u1LsIdLen, &(currAdvRtrId), u1AdvRtrIdLen,
                             pLsaInfo) == OSPF_TRUE)
                        {

                            IP_ADDR_COPY (nextAreaId, pArea->areaId);
                            *pi4NextOspfLsdbType = (INT4)
                                (pLsaInfo->lsaId.u1LsaType);
                            IP_ADDR_COPY (nextLinkStateId,
                                          pLsaInfo->lsaId.linkStateId);
                            IP_ADDR_COPY (nextAdvRtrId,
                                          pLsaInfo->lsaId.advRtrId);
                            *pu4NextOspfLsdbAreaId =
                                OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                            *pu4NextOspfLsdbLsid =
                                OSPF_CRU_BMC_DWFROMPDU (nextLinkStateId);
                            *pu4NextOspfLsdbRouterId =
                                OSPF_CRU_BMC_DWFROMPDU (nextAdvRtrId);
                            pOspfCxt->pLastLsdbLst = pLsaLst;
                            pOspfCxt->pLastLsdbNode = pLsaNode;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
        }
        i1CurrentLsaType = ROUTER_LSA;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfLsdbSequence
 Input       :  The Indices
                OspfLsdbAreaId
                OspfLsdbType
                OspfLsdbLsid
                OspfLsdbRouterId

                The Object 
                retValOspfLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfLsdbSequence (UINT4 u4OspfLsdbAreaId, INT4 i4OspfLsdbType,
                        UINT4 u4OspfLsdbLsid, UINT4 u4OspfLsdbRouterId,
                        INT4 *pi4RetValOspfLsdbSequence)
{
    tAreaId             areaId;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfLsdbAreaId);
    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfLsdbRouterId);

    if ((pOspfCxt->pLastLsdbLst != NULL) && (pOspfCxt->pLastLsdbNode != NULL))
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);
        if ((UtilIpAddrNComp
             (&(pLsaInfo->pArea->areaId), &areaId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            && (pLsaInfo->lsaId.u1LsaType == i4OspfLsdbType)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.linkStateId), &linkStateId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.advRtrId), &advRtrId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL))
        {
            *pi4RetValOspfLsdbSequence = pLsaInfo->lsaSeqNum;
            return SNMP_SUCCESS;
        }
    }

    pArea = (tArea *) NULL;
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = LsuSearchDatabase ((UINT1) i4OspfLsdbType,
                                           &(linkStateId),
                                           &(advRtrId), (UINT1 *) NULL,
                                           (UINT1 *) pArea)) != NULL)
        {

            *pi4RetValOspfLsdbSequence = pLsaInfo->lsaSeqNum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfLsdbAge
 Input       :  The Indices
                OspfLsdbAreaId
                OspfLsdbType
                OspfLsdbLsid
                OspfLsdbRouterId

                The Object 
                retValOspfLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfLsdbAge (UINT4 u4OspfLsdbAreaId, INT4 i4OspfLsdbType,
                   UINT4 u4OspfLsdbLsid, UINT4 u4OspfLsdbRouterId,
                   INT4 *pi4RetValOspfLsdbAge)
{
    tAreaId             areaId;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfLsdbAreaId);
    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfLsdbRouterId);

    if ((pOspfCxt->pLastLsdbLst != NULL) && (pOspfCxt->pLastLsdbNode != NULL))
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);
        if ((UtilIpAddrNComp
             (&(pLsaInfo->pArea->areaId), &areaId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            && (pLsaInfo->lsaId.u1LsaType == i4OspfLsdbType)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.linkStateId), &linkStateId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.advRtrId), &advRtrId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL))
        {
            GET_LSA_AGE (pLsaInfo, pi4RetValOspfLsdbAge);
            return SNMP_SUCCESS;
        }
    }

    pArea = (tArea *) NULL;
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = LsuSearchDatabase ((UINT1) i4OspfLsdbType,
                                           &(linkStateId),
                                           &(advRtrId), (UINT1 *) NULL,
                                           (UINT1 *) pArea)) != NULL)
        {

            GET_LSA_AGE (pLsaInfo, pi4RetValOspfLsdbAge);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfLsdbChecksum
 Input       :  The Indices
                OspfLsdbAreaId
                OspfLsdbType
                OspfLsdbLsid
                OspfLsdbRouterId

                The Object 
                retValOspfLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfLsdbChecksum (UINT4 u4OspfLsdbAreaId, INT4 i4OspfLsdbType,
                        UINT4 u4OspfLsdbLsid, UINT4 u4OspfLsdbRouterId,
                        INT4 *pi4RetValOspfLsdbChecksum)
{
    tAreaId             areaId;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfLsdbAreaId);
    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfLsdbRouterId);

    if ((pOspfCxt->pLastLsdbLst != NULL) && (pOspfCxt->pLastLsdbNode != NULL))
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);
        if ((UtilIpAddrNComp
             (&(pLsaInfo->pArea->areaId), &areaId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            && (pLsaInfo->lsaId.u1LsaType == i4OspfLsdbType)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.linkStateId), &linkStateId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.advRtrId), &advRtrId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL))
        {
            *pi4RetValOspfLsdbChecksum = pLsaInfo->u2LsaChksum;
            return SNMP_SUCCESS;
        }
    }
    pArea = (tArea *) NULL;
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = LsuSearchDatabase ((UINT1) i4OspfLsdbType,
                                           &linkStateId,
                                           &advRtrId, (UINT1 *) NULL,
                                           (UINT1 *) pArea)) != NULL)
        {

            *pi4RetValOspfLsdbChecksum = pLsaInfo->u2LsaChksum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfLsdbAdvertisement
 Input       :  The Indices
                OspfLsdbAreaId
                OspfLsdbType
                OspfLsdbLsid
                OspfLsdbRouterId

                The Object 
                retValOspfLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfLsdbAdvertisement (UINT4 u4OspfLsdbAreaId, INT4 i4OspfLsdbType,
                             UINT4 u4OspfLsdbLsid, UINT4 u4OspfLsdbRouterId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValOspfLsdbAdvertisement)
{
    tAreaId             areaId;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfLsdbAreaId);
    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfLsdbRouterId);
    if ((pOspfCxt->pLastLsdbLst != NULL) && (pOspfCxt->pLastLsdbNode != NULL))
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);
        if ((UtilIpAddrNComp
             (&(pLsaInfo->pArea->areaId), &areaId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            && (pLsaInfo->lsaId.u1LsaType == i4OspfLsdbType)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.linkStateId), &linkStateId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.advRtrId), &advRtrId,
              MAX_IP_ADDR_LEN) == OSPF_EQUAL))
        {
            /* As the SNMP implemenation supports only 
             * maximum of SNMP_MAX_OCTETSTRING_SIZE 
             * We fill only upto SNMP_MAX_OCTETSTRING_SIZE*/
            pRetValOspfLsdbAdvertisement->i4_Length =
                ((pLsaInfo->u2LsaLen <= SNMP_MAX_OCTETSTRING_SIZE) ?
                 pLsaInfo->u2LsaLen : SNMP_MAX_OCTETSTRING_SIZE);
            OS_MEM_CPY (pRetValOspfLsdbAdvertisement->pu1_OctetList,
                        pLsaInfo->pLsa,
                        pRetValOspfLsdbAdvertisement->i4_Length);
            return SNMP_SUCCESS;
        }
    }
    pArea = (tArea *) NULL;
    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = LsuSearchDatabase ((UINT1) i4OspfLsdbType,
                                           &(linkStateId),
                                           &(advRtrId), (UINT1 *) NULL,
                                           (UINT1 *) pArea)) != NULL)
        {
            /* As the SNMP implemenation supports only 
             * maximum of SNMP_MAX_OCTETSTRING_SIZE 
             * We fill only upto SNMP_MAX_OCTETSTRING_SIZE*/
            pRetValOspfLsdbAdvertisement->i4_Length =
                ((pLsaInfo->u2LsaLen <= SNMP_MAX_OCTETSTRING_SIZE) ?
                 pLsaInfo->u2LsaLen : SNMP_MAX_OCTETSTRING_SIZE);
            OS_MEM_CPY (pRetValOspfLsdbAdvertisement->pu1_OctetList,
                        pLsaInfo->pLsa,
                        pRetValOspfLsdbAdvertisement->i4_Length);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : OspfAreaRangeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfAreaRangeTable
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfAreaRangeTable (UINT4 u4OspfAreaRangeAreaId,
                                            UINT4 u4OspfAreaRangeNet)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfAreaRangeTable
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfAreaRangeTable (UINT4 *pu4OspfAreaRangeAreaId,
                                    UINT4 *pu4OspfAreaRangeNet)
{
    UNUSED_PARAM (pu4OspfAreaRangeAreaId);
    UNUSED_PARAM (pu4OspfAreaRangeNet);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfAreaRangeTable
 Input       :  The Indices
                OspfAreaRangeAreaId
                nextOspfAreaRangeAreaId
                OspfAreaRangeNet
                nextOspfAreaRangeNet
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfAreaRangeTable (UINT4 u4OspfAreaRangeAreaId,
                                   UINT4 *pu4NextOspfAreaRangeAreaId,
                                   UINT4 u4OspfAreaRangeNet,
                                   UINT4 *pu4NextOspfAreaRangeNet)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (pu4NextOspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (pu4NextOspfAreaRangeNet);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfAreaRangeMask
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                retValOspfAreaRangeMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaRangeMask (UINT4 u4OspfAreaRangeAreaId,
                         UINT4 u4OspfAreaRangeNet,
                         UINT4 *pu4RetValOspfAreaRangeMask)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (pu4RetValOspfAreaRangeMask);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaRangeStatus
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                retValOspfAreaRangeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaRangeStatus (UINT4 u4OspfAreaRangeAreaId,
                           UINT4 u4OspfAreaRangeNet,
                           INT4 *pi4RetValOspfAreaRangeStatus)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (pi4RetValOspfAreaRangeStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaRangeEffect
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                retValOspfAreaRangeEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaRangeEffect (UINT4 u4OspfAreaRangeAreaId,
                           UINT4 u4OspfAreaRangeNet,
                           INT4 *pi4RetValOspfAreaRangeEffect)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (pi4RetValOspfAreaRangeEffect);

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfHostTable
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfHostTable (UINT4 *pu4OspfHostIpAddress,
                               INT4 *pi4OspfHostTOS)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    INT1                i1Tos;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pHost = (tHost *) TMO_SLL_First (&pOspfCxt->hostLst)) != NULL)
    {
        IP_ADDR_COPY (hostIpAddr, pHost->hostIpAddr);
        for (i1Tos = 0; i1Tos < ENCODE_TOS (OSPF_MAX_METRIC); i1Tos += 2)
        {
            if (pHost->aHostCost[DECODE_TOS (i1Tos)].rowStatus != INVALID)
            {
                *pi4OspfHostTOS = (INT4) i1Tos;
                *pu4OspfHostIpAddress = OSPF_CRU_BMC_DWFROMPDU (hostIpAddr);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfHostTable
 Input       :  The Indices
                OspfHostIpAddress
                nextOspfHostIpAddress
                OspfHostTOS
                nextOspfHostTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfHostTable (UINT4 u4OspfHostIpAddress,
                              UINT4 *pu4NextOspfHostIpAddress,
                              INT4 i4OspfHostTOS, INT4 *pi4NextOspfHostTOS)
{
    tIPADDR             currHostIpAddr;
    tIPADDR             nextHostIpAddr;
    tHost              *pHost;
    INT1                i1Tos;
    UINT1               u1Len = MAX_IP_ADDR_LEN;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currHostIpAddr, u4OspfHostIpAddress);

    if (i4OspfHostTOS < 0)
        return SNMP_FAILURE;

    TMO_SLL_Scan (&(pOspfCxt->hostLst), pHost, tHost *)
    {

        for (i1Tos = 0; i1Tos < ENCODE_TOS (OSPF_MAX_METRIC); i1Tos += 2)
        {

            if (pHost->aHostCost[DECODE_TOS (i1Tos)].rowStatus == INVALID)
                continue;

            switch (UtilIpAddrComp (currHostIpAddr, pHost->hostIpAddr))
            {

                case OSPF_GREATER:
                    continue;

                case OSPF_EQUAL:
                    if ((u1Len == MAX_IP_ADDR_LEN)
                        && (i4OspfHostTOS >= (INT4) i1Tos))
                    {
                        continue;
                    }
                    /* fall through */

                case OSPF_LESS:
                    IP_ADDR_COPY (nextHostIpAddr, pHost->hostIpAddr);
                    *pi4NextOspfHostTOS = (INT4) i1Tos;
                    *pu4NextOspfHostIpAddress =
                        OSPF_CRU_BMC_DWFROMPDU (nextHostIpAddr);
                    return SNMP_SUCCESS;

                default:
                    break;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  GetFindHostInCxt
 Input       :  The Indices
                pOspfCxt 
                pHostIpAddr
           
 Output      :  None 
 Returns     :  pointer to the host on success
                NULL  otherwise
****************************************************************************/
tHost              *
GetFindHostInCxt (tOspfCxt * pOspfCxt, tIPADDR * pHostIpAddr)
{
    tHost              *pHost;

    TMO_SLL_Scan (&(pOspfCxt->hostLst), pHost, tHost *)
    {
        if (UtilIpAddrComp (*pHostIpAddr, pHost->hostIpAddr) == OSPF_EQUAL)
        {
            return pHost;
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfHostMetric
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                retValOspfHostMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfHostMetric (UINT4 u4OspfHostIpAddress, INT4 i4OspfHostTOS,
                      INT4 *pi4RetValOspfHostMetric)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);
    if ((IS_VALID_TOS_VALUE ((INT1) i4OspfHostTOS))
        && ((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) != NULL))
    {
        if (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus
            != INVALID)
        {
            *pi4RetValOspfHostMetric =
                pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].u4Value;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfHostStatus
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                retValOspfHostStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfHostStatus (UINT4 u4OspfHostIpAddress, INT4 i4OspfHostTOS,
                      INT4 *pi4RetValOspfHostStatus)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);
    if ((IS_VALID_TOS_VALUE ((INT1) i4OspfHostTOS))
        && ((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) != NULL))
    {
        if (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus
            != INVALID)
        {
            *pi4RetValOspfHostStatus =
                pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfHostAreaID
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                retValOspfHostAreaID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfHostAreaID (UINT4 u4OspfHostIpAddress, INT4 i4OspfHostTOS,
                      UINT4 *pu4RetValOspfHostAreaID)
{
    tIPADDR             hostIpAddr;
    tAreaId             areaId;
    tHost              *pHost;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);
    if ((IS_VALID_TOS_VALUE ((INT1) i4OspfHostTOS))
        && ((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) != NULL))
    {
        if (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus
            != INVALID)
        {
            IP_ADDR_COPY (areaId, pHost->pArea->areaId);
            *pu4RetValOspfHostAreaID = OSPF_CRU_BMC_DWFROMPDU (areaId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfIfTable
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfIfTable (UINT4 *pu4OspfIfIpAddress,
                             INT4 *pi4OspfAddressLessIf)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pOspfCxt->pLastOspfIf = NULL;

    if ((pLstNode = TMO_SLL_First (&(pOspfCxt->sortIfLst))) != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IP_ADDR_COPY (ifIpAddr, pInterface->ifIpAddr);
        *pi4OspfAddressLessIf = (INT4) pInterface->u4AddrlessIf;
        *pu4OspfIfIpAddress = OSPF_CRU_BMC_DWFROMPDU (ifIpAddr);
        pOspfCxt->pLastOspfIf = pLstNode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfIfTable
 Input       :  The Indices
                OspfIfIpAddress
                nextOspfIfIpAddress
                OspfAddressLessIf
                nextOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfIfTable (UINT4 u4OspfIfIpAddress,
                            UINT4 *pu4NextOspfIfIpAddress,
                            INT4 i4OspfAddressLessIf,
                            INT4 *pi4NextOspfAddressLessIf)
{
    tIPADDR             currIfIpAddr;
    tIPADDR             nextIfIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               u1Len = MAX_IP_ADDR_LEN;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currIfIpAddr, u4OspfIfIpAddress);

    if (pOspfCxt->pLastOspfIf != NULL)
    {
        for (;;)
        {
            pOspfCxt->pLastOspfIf =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&pOspfCxt->sortIfLst,
                                                (tTMO_SLL_NODE *) pOspfCxt->
                                                pLastOspfIf);

            if (pOspfCxt->pLastOspfIf == NULL)
            {
                return SNMP_FAILURE;
            }

            pInterface = GET_IF_PTR_FROM_SORT_LST (pOspfCxt->pLastOspfIf);

            switch (UtilIpAddrNComp
                    (&(currIfIpAddr), &(pInterface->ifIpAddr), u1Len))
            {

                case OSPF_GREATER:
                    continue;

                case OSPF_EQUAL:
                    if ((UINT4) i4OspfAddressLessIf >= pInterface->u4AddrlessIf)
                    {
                        continue;
                    }
                    /* fall through */

                case OSPF_LESS:
                    IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                    *pi4NextOspfAddressLessIf =
                        (INT4) (pInterface->u4AddrlessIf);
                    *pu4NextOspfIfIpAddress =
                        OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                    return SNMP_SUCCESS;

                default:
                    break;
            }
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOspfCxt->pLastOspfIf = pLstNode;
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilIpAddrNComp
                (&(currIfIpAddr), &(pInterface->ifIpAddr), u1Len))
        {

            case OSPF_GREATER:
                continue;

            case OSPF_EQUAL:
                if ((UINT4) i4OspfAddressLessIf >= pInterface->u4AddrlessIf)
                {
                    continue;
                }
                /* fall through */

            case OSPF_LESS:
                IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                *pi4NextOspfAddressLessIf = (INT4) (pInterface->u4AddrlessIf);
                *pu4NextOspfIfIpAddress = OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                return SNMP_SUCCESS;

            default:
                break;

        }
    }

    pOspfCxt->pLastOspfIf = NULL;
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  GetFindIfInCxt
 Input       :  The Indices
                 pOspfCxt
                 ifIpAddr
                 u4AddrlessIf
 Output      :  The Get  function gets interface for
                the given ip address and addressless interface Values.
 Returns     :  pointer to the interface on success
                NULL  otherwise 
****************************************************************************/
tInterface         *
GetFindIfInCxt (tOspfCxt * pOspfCxt, tIPADDR ifIpAddr, UINT4 u4AddrlessIf)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pLastOspfCxt = gOsRtr.pOspfCxt;

    if (pLastOspfCxt != NULL && pLastOspfCxt->pLastOspfIf != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLastOspfCxt->pLastOspfIf);
        if (UtilIpAddrIndComp (ifIpAddr, u4AddrlessIf,
                               pInterface->ifIpAddr,
                               pInterface->u4AddrlessIf) == OSPF_EQUAL)
        {
            return pInterface;
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (UtilIpAddrIndComp (ifIpAddr, u4AddrlessIf,
                               pInterface->ifIpAddr,
                               pInterface->u4AddrlessIf) == OSPF_EQUAL)
        {
            return pInterface;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  GetFindIfAuthkeyInfoInCxt
 Input       :  The Indices
                 pOspfCxt
                 ifIpAddr
                 u4AddrlessIf
                 u1AuthkeyId
 Output      :  The Get  function gets interface authentication info for
                the given ip address , addressless & authkey_id  Values.
 Returns     :  pointer to the  authkey_info on success
                NULL  otherwise
****************************************************************************/
tMd5AuthkeyInfo    *
GetFindIfAuthkeyInfoInCxt (tOspfCxt * pOspfCxt, tIPADDR ifIpAddr,
                           UINT4 u4AddrlessIf, UINT1 u1AuthkeyId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tMd5AuthkeyInfo    *pAuthkeyInfo;

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, u4AddrlessIf))
        != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u1AuthkeyId == u1AuthkeyId)
                return (pAuthkeyInfo);
            else if (pAuthkeyInfo->u1AuthkeyId > u1AuthkeyId)
                return (NULL);
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfIfAreaId
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfAreaId (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    UINT4 *pu4RetValOspfIfAreaId)
{
    tIPADDR             ifIpAddr;
    tAreaId             areaId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        if (pInterface->pArea != NULL)
        {
            IP_ADDR_COPY (areaId, pInterface->pArea->areaId);
            *pu4RetValOspfIfAreaId = OSPF_CRU_BMC_DWFROMPDU (areaId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfType
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfType (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                  INT4 *pi4RetValOspfIfType)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfType = pInterface->u1NetworkType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfAdminStat
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfAdminStat (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                       INT4 *pi4RetValOspfIfAdminStat)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfAdminStat = pInterface->admnStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfRtrPriority
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfRtrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfRtrPriority (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                         INT4 *pi4RetValOspfIfRtrPriority)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfRtrPriority = pInterface->u1RtrPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfTransitDelay
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfTransitDelay (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                          INT4 *pi4RetValOspfIfTransitDelay)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfTransitDelay = pInterface->u2IfTransDelay;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfRetransInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfRetransInterval (UINT4 u4OspfIfIpAddress,
                             INT4 i4OspfAddressLessIf,
                             INT4 *pi4RetValOspfIfRetransInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfRetransInterval = pInterface->u2RxmtInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfHelloInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfHelloInterval (UINT4 u4OspfIfIpAddress,
                           INT4 i4OspfAddressLessIf,
                           INT4 *pi4RetValOspfIfHelloInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfHelloInterval = pInterface->u2HelloInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfRtrDeadInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfRtrDeadInterval (UINT4 u4OspfIfIpAddress,
                             INT4 i4OspfAddressLessIf,
                             INT4 *pi4RetValOspfIfRtrDeadInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfRtrDeadInterval = pInterface->i4RtrDeadInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfPollInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfPollInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfPollInterval (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                          INT4 *pi4RetValOspfIfPollInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfPollInterval = pInterface->i4PollInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfState
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfState (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                   INT4 *pi4RetValOspfIfState)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfState = pInterface->u1IsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfDesignatedRouter
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfDesignatedRouter (UINT4 u4OspfIfIpAddress,
                              INT4 i4OspfAddressLessIf,
                              UINT4 *pu4RetValOspfIfDesignatedRouter)
{
    tIPADDR             ifIpAddr;
    tIPADDR             desgRtr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        IP_ADDR_COPY (desgRtr, GET_DR (pInterface));
        *pu4RetValOspfIfDesignatedRouter = OSPF_CRU_BMC_DWFROMPDU (desgRtr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfBackupDesignatedRouter
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfBackupDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfBackupDesignatedRouter (UINT4 u4OspfIfIpAddress,
                                    INT4 i4OspfAddressLessIf,
                                    UINT4
                                    *pu4RetValOspfIfBackupDesignatedRouter)
{
    tIPADDR             ifIpAddr;
    tIPADDR             backup;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        IP_ADDR_COPY (backup, GET_BDR (pInterface));
        *pu4RetValOspfIfBackupDesignatedRouter =
            OSPF_CRU_BMC_DWFROMPDU (backup);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfEvents
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfEvents (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    UINT4 *pu4RetValOspfIfEvents)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pu4RetValOspfIfEvents = pInterface->aIfEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfAuthType
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfAuthType (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                      INT4 *pi4RetValOspfIfAuthType)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfAuthType = pInterface->u2AuthType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfAuthKey
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfAuthKey (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                     tSNMP_OCTET_STRING_TYPE * pRetValOspfIfAuthKey)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        if (MsrGetSaveStatus () == ISS_TRUE)
        {
            pRetValOspfIfAuthKey->i4_Length =
                STRLEN ((CHR1 *) pInterface->authKey);
            MEMCPY (pRetValOspfIfAuthKey->pu1_OctetList,
                    pInterface->authKey, pRetValOspfIfAuthKey->i4_Length);
        }
        else
        {
            pRetValOspfIfAuthKey->i4_Length = 0;
        }
        /*
         * The Auth key when read should return an octet string of length 0.
         */
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfStatus
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfStatus (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    INT4 *pi4RetValOspfIfStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfStatus = pInterface->ifStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfMulticastForwarding
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfMulticastForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfMulticastForwarding (UINT4 u4OspfIfIpAddress,
                                 INT4 i4OspfAddressLessIf,
                                 INT4 *pi4RetValOspfIfMulticastForwarding)
{
    UNUSED_PARAM (u4OspfIfIpAddress);
    UNUSED_PARAM (i4OspfAddressLessIf);

    *pi4RetValOspfIfMulticastForwarding = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfIfDemand
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                retValOspfIfDemand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfDemand (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    INT4 *pi4RetValOspfIfDemand)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * The SNMP Manager should know if DC support is configured
     * on this interface. Added the else part.
     */

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfDemand = pInterface->bDcEndpt;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfIfMetricTable
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfIfMetricTable (UINT4 *pu4OspfIfMetricIpAddress,
                                   INT4 *pi4OspfIfMetricAddressLessIf,
                                   INT4 *pi4OspfIfMetricTOS)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tInterface         *pInterface = NULL;
    INT1                i1Tos;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pOspfCxt->pLastOspfIf = NULL;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        for (i1Tos = 0; i1Tos < ENCODE_TOS (OSPF_MAX_METRIC); i1Tos += 2)
        {
            if (pInterface->aIfOpCost[DECODE_TOS (i1Tos)].rowStatus == INVALID)
            {
                continue;
            }
            *pu4OspfIfMetricIpAddress =
                OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
            *pi4OspfIfMetricAddressLessIf = pInterface->u4AddrlessIf;
            *pi4OspfIfMetricTOS = i1Tos;
            pOspfCxt->pLastOspfIf = pLstNode;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfIfMetricTable
 Input       :  The Indices
                OspfIfMetricIpAddress
                nextOspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                nextOspfIfMetricAddressLessIf
                OspfIfMetricTOS
                nextOspfIfMetricTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfIfMetricTable (UINT4 u4OspfIfMetricIpAddress,
                                  UINT4 *pu4NextOspfIfMetricIpAddress,
                                  INT4 i4OspfIfMetricAddressLessIf,
                                  INT4 *pi4NextOspfIfMetricAddressLessIf,
                                  INT4 i4OspfIfMetricTOS,
                                  INT4 *pi4NextOspfIfMetricTOS)
{
    tIPADDR             currIfIpAddr;
    tIPADDR             nextIfIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               u1Tos = 0;
    UINT1               u1Found = OSPF_FALSE;
    UINT1               u1CurAddrStatus = OSPF_EQUAL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currIfIpAddr, u4OspfIfMetricIpAddress);

    if (pOspfCxt->pLastOspfIf != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pOspfCxt->pLastOspfIf);

        /* Scanning all the VALID TOS values in the Current Interface */
        for (u1Tos = (i4OspfIfMetricTOS + 2);
             u1Tos < ENCODE_TOS (OSPF_MAX_METRIC); u1Tos += 2)
        {
            if (pInterface->aIfOpCost[DECODE_TOS (u1Tos)].rowStatus == ACTIVE)
            {
                IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                *pu4NextOspfIfMetricIpAddress =
                    OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                *pi4NextOspfIfMetricAddressLessIf = (INT4)
                    (pInterface->u4AddrlessIf);
                *pi4NextOspfIfMetricTOS = (INT4) u1Tos;
                return SNMP_SUCCESS;
            }
        }

        for (;;)
        {
            pOspfCxt->pLastOspfIf =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&pOspfCxt->sortIfLst,
                                                (tTMO_SLL_NODE *) pOspfCxt->
                                                pLastOspfIf);

            if (pOspfCxt->pLastOspfIf == NULL)
            {
                return SNMP_FAILURE;
            }

            pInterface = GET_IF_PTR_FROM_SORT_LST (pOspfCxt->pLastOspfIf);

            for (u1Tos = 0; u1Tos < ENCODE_TOS (OSPF_MAX_METRIC); u1Tos += 2)
            {
                if (pInterface->
                    aIfOpCost[DECODE_TOS (u1Tos)].rowStatus == ACTIVE)
                {
                    IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                    *pu4NextOspfIfMetricIpAddress =
                        OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                    *pi4NextOspfIfMetricAddressLessIf = (INT4)
                        (pInterface->u4AddrlessIf);
                    *pi4NextOspfIfMetricTOS = (INT4) u1Tos;
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOspfCxt->pLastOspfIf = pLstNode;
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        u1CurAddrStatus =
            UtilIpAddrIndComp (currIfIpAddr, i4OspfIfMetricAddressLessIf,
                               pInterface->ifIpAddr, pInterface->u4AddrlessIf);

        if (u1CurAddrStatus == OSPF_EQUAL)
        {
            /* Scanning all the VALID TOS values in the Current Interface */
            for (u1Tos = (i4OspfIfMetricTOS + 2);
                 u1Tos < ENCODE_TOS (OSPF_MAX_METRIC); u1Tos += 2)
            {
                if (pInterface->
                    aIfOpCost[DECODE_TOS (u1Tos)].rowStatus == ACTIVE)
                {
                    u1Found = OSPF_TRUE;
                    break;
                }
            }
        }
        else if (u1CurAddrStatus == OSPF_LESS)
        {

            for (u1Tos = 0; u1Tos < ENCODE_TOS (OSPF_MAX_METRIC); u1Tos += 2)
            {
                if (pInterface->
                    aIfOpCost[DECODE_TOS (u1Tos)].rowStatus == ACTIVE)
                {
                    u1Found = OSPF_TRUE;
                    break;
                }
            }
        }

        if (u1Found == OSPF_TRUE)
        {
            IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
            *pu4NextOspfIfMetricIpAddress =
                OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
            *pi4NextOspfIfMetricAddressLessIf = (INT4)
                (pInterface->u4AddrlessIf);
            *pi4NextOspfIfMetricTOS = (INT4) u1Tos;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfIfMetricValue
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS

                The Object 
                retValOspfIfMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfMetricValue (UINT4 u4OspfIfMetricIpAddress,
                         INT4 i4OspfIfMetricAddressLessIf,
                         INT4 i4OspfIfMetricTOS,
                         INT4 *pi4RetValOspfIfMetricValue)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);
    if ((IS_VALID_TOS_VALUE ((INT1) i4OspfIfMetricTOS))
        && ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                          (UINT4)
                                          i4OspfIfMetricAddressLessIf))
            != NULL))
    {
        if (pInterface->
            aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].rowStatus !=
            INVALID)
        {
            *pi4RetValOspfIfMetricValue = (INT4)
                (pInterface->aIfOpCost[DECODE_TOS
                                       ((INT1) i4OspfIfMetricTOS)].u4Value);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfIfMetricStatus
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS

                The Object 
                retValOspfIfMetricStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfIfMetricStatus (UINT4 u4OspfIfMetricIpAddress,
                          INT4 i4OspfIfMetricAddressLessIf,
                          INT4 i4OspfIfMetricTOS,
                          INT4 *pi4RetValOspfIfMetricStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);
    if ((IS_VALID_TOS_VALUE ((INT1) i4OspfIfMetricTOS))
        && ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                          (UINT4)
                                          i4OspfIfMetricAddressLessIf))
            != NULL))
    {
        if (pInterface->
            aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].rowStatus !=
            INVALID)
        {
            *pi4RetValOspfIfMetricStatus =
                pInterface->aIfOpCost[DECODE_TOS
                                      ((INT1) i4OspfIfMetricTOS)].rowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfVirtIfTable
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfVirtIfTable (UINT4 *pu4OspfVirtIfAreaId,
                                 UINT4 *pu4OspfVirtIfNeighbor)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLstNode = TMO_SLL_First (&(pOspfCxt->virtIfLst))) != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IP_ADDR_COPY (transitAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (nbrId, pInterface->destRtrId);
        *pu4OspfVirtIfAreaId = OSPF_CRU_BMC_DWFROMPDU (transitAreaId);
        *pu4OspfVirtIfNeighbor = OSPF_CRU_BMC_DWFROMPDU (nbrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfVirtIfTable
 Input       :  The Indices
                OspfVirtIfAreaId
                nextOspfVirtIfAreaId
                OspfVirtIfNeighbor
                nextOspfVirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfVirtIfTable (UINT4 u4OspfVirtIfAreaId,
                                UINT4 *pu4NextOspfVirtIfAreaId,
                                UINT4 u4OspfVirtIfNeighbor,
                                UINT4 *pu4NextOspfVirtIfNeighbor)
{
    tAreaId             currAreaId;
    tRouterId           currNbrId;
    tAreaId             nextAreaId;
    tRouterId           nextNbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (currNbrId, u4OspfVirtIfNeighbor);

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilIpAddrComp (currAreaId, pInterface->transitAreaId))
        {
            case OSPF_GREATER:
                /* try next entry */
                continue;

            case OSPF_EQUAL:
                /* first index is complete */
                switch (UtilIpAddrComp (currNbrId, pInterface->destRtrId))
                {
                    case OSPF_GREATER:
                        /* try next entry */
                        continue;
                    case OSPF_EQUAL:
                        /* second index is complete */
                        /* try next entry */
                        continue;
                        /* next found, fall through */
                    case OSPF_LESS:
                        break;
                    default:
                        break;
                        /* next found, fall through */
                }
                /* next found, fall through */

            case OSPF_LESS:
                IP_ADDR_COPY (nextAreaId, pInterface->transitAreaId);
                IP_ADDR_COPY (nextNbrId, pInterface->destRtrId);
                *pu4NextOspfVirtIfAreaId = OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                *pu4NextOspfVirtIfNeighbor = OSPF_CRU_BMC_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  GetFindVirtIfInCxt
 Input       :  The Indices
                pOspfCxt
                pTransitAreaId
                pNbrId
 
 Output      :  The Get function gets the virtual interface for
                the given transit area Id & Nbr Id values. 
 Returns     :  pointer to the interface on success
                NULL otherwise 
****************************************************************************/

tInterface         *
GetFindVirtIfInCxt (tOspfCxt * pOspfCxt, tAreaId * pTransitAreaId,
                    tRouterId * pNbrId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pInterface->u1NetworkType == IF_VIRTUAL)
            && (UtilVirtIfIndComp (*pTransitAreaId, *pNbrId,
                                   pInterface->transitAreaId,
                                   pInterface->destRtrId) == OSPF_EQUAL))
        {
            return pInterface;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  GetFindVirtIfAuthkeyInfoInCxt
 Input       :  The Indices
                pOspfCxt
                pTransitAreaId
                pNbrId
                u1AuthkeyId   
 Output      :  The Get  function gets virtual interface authentication info for
                the given transit area Id , Nbr Id & authkey_id  Values.
 Returns     :  pointer to the  authkey_info on success
                NULL  otherwise
****************************************************************************/
tMd5AuthkeyInfo    *
GetFindVirtIfAuthkeyInfoInCxt (tOspfCxt * pOspfCxt, tAreaId * pTransitAreaId,
                               tRouterId * pNbrId, UINT1 u1AuthkeyId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tMd5AuthkeyInfo    *pAuthkeyInfo;

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, pTransitAreaId, pNbrId))
        != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u1AuthkeyId == u1AuthkeyId)
                return (pAuthkeyInfo);
            else if (pAuthkeyInfo->u1AuthkeyId > u1AuthkeyId)
                return (NULL);
        }
    }
    return NULL;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfVirtIfTransitDelay
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfTransitDelay (UINT4 u4OspfVirtIfAreaId,
                              UINT4 u4OspfVirtIfNeighbor,
                              INT4 *pi4RetValOspfVirtIfTransitDelay)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &(nbrId)))
        != NULL)
    {
        *pi4RetValOspfVirtIfTransitDelay = pInterface->u2IfTransDelay;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfRetransInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfRetransInterval (UINT4 u4OspfVirtIfAreaId,
                                 UINT4 u4OspfVirtIfNeighbor,
                                 INT4 *pi4RetValOspfVirtIfRetransInterval)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfRetransInterval = pInterface->u2RxmtInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfHelloInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfHelloInterval (UINT4 u4OspfVirtIfAreaId,
                               UINT4 u4OspfVirtIfNeighbor,
                               INT4 *pi4RetValOspfVirtIfHelloInterval)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfHelloInterval = pInterface->u2HelloInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfRtrDeadInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfRtrDeadInterval (UINT4 u4OspfVirtIfAreaId,
                                 UINT4 u4OspfVirtIfNeighbor,
                                 INT4 *pi4RetValOspfVirtIfRtrDeadInterval)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfRtrDeadInterval = pInterface->i4RtrDeadInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfState
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfState (UINT4 u4OspfVirtIfAreaId, UINT4 u4OspfVirtIfNeighbor,
                       INT4 *pi4RetValOspfVirtIfState)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfState = pInterface->u1IsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfEvents
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfEvents (UINT4 u4OspfVirtIfAreaId,
                        UINT4 u4OspfVirtIfNeighbor,
                        UINT4 *pu4RetValOspfVirtIfEvents)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pu4RetValOspfVirtIfEvents = pInterface->aIfEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfAuthType
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfAuthType (UINT4 u4OspfVirtIfAreaId,
                          UINT4 u4OspfVirtIfNeighbor,
                          INT4 *pi4RetValOspfVirtIfAuthType)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfAuthType = pInterface->u2AuthType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfAuthKey
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfAuthKey (UINT4 u4OspfVirtIfAreaId,
                         UINT4 u4OspfVirtIfNeighbor,
                         tSNMP_OCTET_STRING_TYPE * pRetValOspfVirtIfAuthKey)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {

        if (MsrGetSaveStatus () == ISS_TRUE)
        {
            pRetValOspfVirtIfAuthKey->i4_Length =
                STRLEN ((CHR1 *) pInterface->authKey);
            MEMCPY (pRetValOspfVirtIfAuthKey->pu1_OctetList,
                    pInterface->authKey, pRetValOspfVirtIfAuthKey->i4_Length);
        }
        else
        {

            pRetValOspfVirtIfAuthKey->i4_Length = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtIfStatus
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                retValOspfVirtIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtIfStatus (UINT4 u4OspfVirtIfAreaId,
                        UINT4 u4OspfVirtIfNeighbor,
                        INT4 *pi4RetValOspfVirtIfStatus)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfStatus = pInterface->ifStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfNbrTable
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfNbrTable (UINT4 *pu4OspfNbrIpAddr,
                              INT4 *pi4OspfNbrAddressLessIndex)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pOspfCxt->pLastNbr = NULL;

    if ((pLstNode = TMO_SLL_First (&(pOspfCxt->sortNbrLst))) != NULL)
    {
        pNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
        IP_ADDR_COPY (nbrIpAddr, pNbr->nbrIpAddr);
        *pi4OspfNbrAddressLessIndex = (INT4) pNbr->u4NbrAddrlessIf;
        *pu4OspfNbrIpAddr = OSPF_CRU_BMC_DWFROMPDU (nbrIpAddr);
        pOspfCxt->pLastNbr = pLstNode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfNbrTable
 Input       :  The Indices
                OspfNbrIpAddr
                nextOspfNbrIpAddr
                OspfNbrAddressLessIndex
                nextOspfNbrAddressLessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfNbrTable (UINT4 u4OspfNbrIpAddr,
                             UINT4 *pu4NextOspfNbrIpAddr,
                             INT4 i4OspfNbrAddressLessIndex,
                             INT4 *pi4NextOspfNbrAddressLessIndex)
{
    tIPADDR             currNbrIpAddr;
    tIPADDR             nextNbrIpAddr;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currNbrIpAddr, u4OspfNbrIpAddr);

    if (pOspfCxt->pLastNbr != NULL)
    {
        for (;;)
        {
            pOspfCxt->pLastNbr =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&pOspfCxt->sortNbrLst,
                                                (tTMO_SLL_NODE *) pOspfCxt->
                                                pLastNbr);

            if (pOspfCxt->pLastNbr == NULL)
            {
                return SNMP_FAILURE;
            }

            pNbr = GET_NBR_PTR_FROM_SORT_LST (pOspfCxt->pLastNbr);

            switch (UtilIpAddrComp (currNbrIpAddr, pNbr->nbrIpAddr))
            {
                case OSPF_GREATER:
                    continue;
                case OSPF_EQUAL:
                    if ((UINT4) i4OspfNbrAddressLessIndex >=
                        pNbr->u4NbrAddrlessIf)
                    {
                        continue;
                    }
                    /* fall through */
                case OSPF_LESS:
                    IP_ADDR_COPY (nextNbrIpAddr, pNbr->nbrIpAddr);
                    *pi4NextOspfNbrAddressLessIndex =
                        (INT4) pNbr->u4NbrAddrlessIf;
                    *pu4NextOspfNbrIpAddr =
                        OSPF_CRU_BMC_DWFROMPDU (nextNbrIpAddr);
                    return SNMP_SUCCESS;
                default:
                    break;
            }
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOspfCxt->pLastNbr = pLstNode;
        pNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilIpAddrComp (currNbrIpAddr, pNbr->nbrIpAddr))
        {
            case OSPF_GREATER:
                continue;
            case OSPF_EQUAL:
                if ((UINT4) i4OspfNbrAddressLessIndex >= pNbr->u4NbrAddrlessIf)
                {
                    continue;
                }
                /* fall through */
            case OSPF_LESS:
                IP_ADDR_COPY (nextNbrIpAddr, pNbr->nbrIpAddr);
                *pi4NextOspfNbrAddressLessIndex = (INT4) pNbr->u4NbrAddrlessIf;
                *pu4NextOspfNbrIpAddr = OSPF_CRU_BMC_DWFROMPDU (nextNbrIpAddr);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }

    pOspfCxt->pLastNbr = NULL;
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************** * ************************************************************
Function : GetFindNbrInCxt

Input    : pOspfCxt
           pNbrIpAddr
           u4NbrAddrlessIf

Output   : None

Returns  : Neighbour structure
******************************************************************************/
tNeighbor          *
GetFindNbrInCxt (tOspfCxt * pOspfCxt, tIPADDR * pNbrIpAddr,
                 UINT4 u4NbrAddrlessIf)
{
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pLastOspfCxt = gOsRtr.pOspfCxt;

    if (pLastOspfCxt != NULL && pLastOspfCxt->pLastNbr != NULL)
    {
        pNbr = GET_NBR_PTR_FROM_SORT_LST (pLastOspfCxt->pLastNbr);
        if (UtilIpAddrIndComp (*pNbrIpAddr, u4NbrAddrlessIf,
                               pNbr->nbrIpAddr,
                               pNbr->u4NbrAddrlessIf) == OSPF_EQUAL)
        {
            return pNbr;
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

        if (UtilIpAddrIndComp (*pNbrIpAddr, u4NbrAddrlessIf,
                               pNbr->nbrIpAddr,
                               pNbr->u4NbrAddrlessIf) == OSPF_EQUAL)
        {
            return pNbr;
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfNbrRtrId
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrRtrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrRtrId (UINT4 u4OspfNbrIpAddr, INT4 i4OspfNbrAddressLessIndex,
                    UINT4 *pu4RetValOspfNbrRtrId)
{
    tIPADDR             nbrIpAddr;
    tRouterId           rtrId;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &(nbrIpAddr),
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        IP_ADDR_COPY (&rtrId, pNbr->nbrId);
        *pu4RetValOspfNbrRtrId = OSPF_CRU_BMC_DWFROMPDU (rtrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbrOptions
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrOptions (UINT4 u4OspfNbrIpAddr, INT4 i4OspfNbrAddressLessIndex,
                      INT4 *pi4RetValOspfNbrOptions)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &(nbrIpAddr),
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValOspfNbrOptions = pNbr->nbrOptions;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbrPriority
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrPriority (UINT4 u4OspfNbrIpAddr,
                       INT4 i4OspfNbrAddressLessIndex,
                       INT4 *pi4RetValOspfNbrPriority)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &(nbrIpAddr),
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValOspfNbrPriority = pNbr->u1NbrRtrPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbrState
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrState (UINT4 u4OspfNbrIpAddr, INT4 i4OspfNbrAddressLessIndex,
                    INT4 *pi4RetValOspfNbrState)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValOspfNbrState = pNbr->u1NsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbrEvents
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrEvents (UINT4 u4OspfNbrIpAddr, INT4 i4OspfNbrAddressLessIndex,
                     UINT4 *pu4RetValOspfNbrEvents)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pu4RetValOspfNbrEvents = pNbr->u4NbrEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbrLsRetransQLen
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrLsRetransQLen (UINT4 u4OspfNbrIpAddr,
                            INT4 i4OspfNbrAddressLessIndex,
                            UINT4 *pu4RetValOspfNbrLsRetransQLen)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pu4RetValOspfNbrLsRetransQLen = pNbr->lsaRxmtDesc.u4LsaRxmtCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbmaNbrStatus
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbmaNbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbmaNbrStatus (UINT4 u4OspfNbrIpAddr,
                         INT4 i4OspfNbrAddressLessIndex,
                         INT4 *pi4RetValOspfNbmaNbrStatus)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValOspfNbmaNbrStatus = pNbr->nbrStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbmaNbrPermanence
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbmaNbrPermanence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbmaNbrPermanence (UINT4 u4OspfNbrIpAddr,
                             INT4 i4OspfNbrAddressLessIndex,
                             INT4 *pi4RetValOspfNbmaNbrPermanence)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValOspfNbmaNbrPermanence = pNbr->u1ConfigStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfNbrHelloSuppressed
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                retValOspfNbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfNbrHelloSuppressed (UINT4 u4OspfNbrIpAddr,
                              INT4 i4OspfNbrAddressLessIndex,
                              INT4 *pi4RetValOspfNbrHelloSuppressed)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4OspfNbrIpAddr);
    UNUSED_PARAM (i4OspfNbrAddressLessIndex);

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                          (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValOspfNbrHelloSuppressed = pNbr->bHelloSuppression;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/******************************************************************************
Function : GetFindVirtNbrInCxt

Input    : pOspfCxt
           pTransitAreaId
           pNbrId

Output   : None

Returns  : Neighbour structure
******************************************************************************/
tNeighbor          *
GetFindVirtNbrInCxt (tOspfCxt * pOspfCxt, tAreaId * pTransitAreaId,
                     tRouterId * pNbrId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, pTransitAreaId, pNbrId))
        != NULL)
    {

        if ((pLstNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
        {
            return GET_NBR_PTR_FROM_NBR_LST (pLstNode);
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfVirtNbrTable
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfVirtNbrTable (UINT4 *pu4OspfVirtNbrArea,
                                  UINT4 *pu4OspfVirtNbrRtrId)
{
    return nmhGetFirstIndexOspfVirtIfTable (pu4OspfVirtNbrArea,
                                            pu4OspfVirtNbrRtrId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfVirtNbrTable
 Input       :  The Indices
                OspfVirtNbrArea
                nextOspfVirtNbrArea
                OspfVirtNbrRtrId
                nextOspfVirtNbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfVirtNbrTable (UINT4 u4OspfVirtNbrArea,
                                 UINT4 *pu4NextOspfVirtNbrArea,
                                 UINT4 u4OspfVirtNbrRtrId,
                                 UINT4 *pu4NextOspfVirtNbrRtrId)
{
    return nmhGetNextIndexOspfVirtIfTable (u4OspfVirtNbrArea,
                                           pu4NextOspfVirtNbrArea,
                                           u4OspfVirtNbrRtrId,
                                           pu4NextOspfVirtNbrRtrId);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfVirtNbrIpAddr
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId

                The Object 
                retValOspfVirtNbrIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtNbrIpAddr (UINT4 u4OspfVirtNbrArea, UINT4 u4OspfVirtNbrRtrId,
                         UINT4 *pu4RetValOspfVirtNbrIpAddr)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tIPADDR             nbrAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {

        IP_ADDR_COPY (&nbrAddr, pNbr->nbrIpAddr);
        *pu4RetValOspfVirtNbrIpAddr = OSPF_CRU_BMC_DWFROMPDU (nbrAddr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtNbrOptions
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId

                The Object 
                retValOspfVirtNbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtNbrOptions (UINT4 u4OspfVirtNbrArea, UINT4 u4OspfVirtNbrRtrId,
                          INT4 *pi4RetValOspfVirtNbrOptions)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        *pi4RetValOspfVirtNbrOptions = pNbr->nbrOptions;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtNbrState
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId

                The Object 
                retValOspfVirtNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtNbrState (UINT4 u4OspfVirtNbrArea, UINT4 u4OspfVirtNbrRtrId,
                        INT4 *pi4RetValOspfVirtNbrState)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        *pi4RetValOspfVirtNbrState = pNbr->u1NsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtNbrEvents
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId

                The Object 
                retValOspfVirtNbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtNbrEvents (UINT4 u4OspfVirtNbrArea, UINT4 u4OspfVirtNbrRtrId,
                         UINT4 *pu4RetValOspfVirtNbrEvents)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        *pu4RetValOspfVirtNbrEvents = pNbr->u4NbrEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtNbrLsRetransQLen
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId

                The Object 
                retValOspfVirtNbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtNbrLsRetransQLen (UINT4 u4OspfVirtNbrArea,
                                UINT4 u4OspfVirtNbrRtrId,
                                UINT4 *pu4RetValOspfVirtNbrLsRetransQLen)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        *pu4RetValOspfVirtNbrLsRetransQLen = pNbr->lsaRxmtDesc.u4LsaRxmtCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfVirtNbrHelloSuppressed
 Input       :  The Indices
                OspfVirtNbrArea
                OspfVirtNbrRtrId

                The Object 
                retValOspfVirtNbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfVirtNbrHelloSuppressed (UINT4 u4OspfVirtNbrArea,
                                  UINT4 u4OspfVirtNbrRtrId,
                                  INT4 *pi4RetValOspfVirtNbrHelloSuppressed)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4OspfVirtNbrArea);
    UNUSED_PARAM (u4OspfVirtNbrRtrId);

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &(transitAreaId), &(nbrId)))
        != NULL)
    {
        *pi4RetValOspfVirtNbrHelloSuppressed = pNbr->bHelloSuppression;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfExtLsdbTable
 Input       :  The Indices
                OspfExtLsdbType
                OspfExtLsdbLsid
                OspfExtLsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfExtLsdbTable (INT4 i4OspfExtLsdbType,
                                          UINT4 u4OspfExtLsdbLsid,
                                          UINT4 u4OspfExtLsdbRouterId)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfExtLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfExtLsdbRouterId);

    if (LsuSearchDatabase
        ((UINT1) i4OspfExtLsdbType, &linkStateId, &advRtrId, (UINT1 *) NULL,
         (UINT1 *) (pOspfCxt->pBackbone)) != NULL)
    {

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfExtLsdbTable
 Input       :  The Indices
                OspfExtLsdbType
                OspfExtLsdbLsid
                OspfExtLsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfExtLsdbTable (INT4 *pi4OspfExtLsdbType,
                                  UINT4 *pu4OspfExtLsdbLsid,
                                  UINT4 *pu4OspfExtLsdbRouterId)
{
    tLsaInfo           *pLsaInfo = NULL;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->pAsExtLsaRBRoot == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLsaInfo =
         (tLsaInfo *) RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot)) != NULL)
    {
        *pi4OspfExtLsdbType = pLsaInfo->lsaId.u1LsaType;
        IP_ADDR_COPY (linkStateId, pLsaInfo->lsaId.linkStateId);
        IP_ADDR_COPY (advRtrId, pLsaInfo->lsaId.advRtrId);
        *pu4OspfExtLsdbLsid = OSPF_CRU_BMC_DWFROMPDU (linkStateId);
        *pu4OspfExtLsdbRouterId = OSPF_CRU_BMC_DWFROMPDU (advRtrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfExtLsdbTable
 Input       :  The Indices
                OspfExtLsdbType
                nextOspfExtLsdbType
                OspfExtLsdbLsid
                nextOspfExtLsdbLsid
                OspfExtLsdbRouterId
                nextOspfExtLsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfExtLsdbTable (INT4 i4OspfExtLsdbType,
                                 INT4 *pi4NextOspfExtLsdbType,
                                 UINT4 u4OspfExtLsdbLsid,
                                 UINT4 *pu4NextOspfExtLsdbLsid,
                                 UINT4 u4OspfExtLsdbRouterId,
                                 UINT4 *pu4NextOspfExtLsdbRouterId)
{
    tLsaInfo            lsaInf;
    tLsaInfo           *pLsaInfo = NULL;
    tRouterId           advRtrId;
    tLINKSTATEID        linkStateId;
    tLINKSTATEID        nextLinkStateId;
    tRouterId           nextAdvRtrId;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    lsaInf.lsaId.u1LsaType = (UINT1) i4OspfExtLsdbType;

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfExtLsdbLsid);
    IP_ADDR_COPY (lsaInf.lsaId.linkStateId, linkStateId);

    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfExtLsdbRouterId);
    IP_ADDR_COPY (lsaInf.lsaId.advRtrId, advRtrId);

    if ((pLsaInfo =
         (tLsaInfo *) RBTreeGetNext (pOspfCxt->pAsExtLsaRBRoot,
                                     (tRBElem *) & lsaInf, NULL)) != NULL)
    {
        *pi4NextOspfExtLsdbType = pLsaInfo->lsaId.u1LsaType;
        IP_ADDR_COPY (nextAdvRtrId, pLsaInfo->lsaId.advRtrId);
        IP_ADDR_COPY (nextLinkStateId, pLsaInfo->lsaId.linkStateId);
        *pu4NextOspfExtLsdbLsid = OSPF_CRU_BMC_DWFROMPDU (nextLinkStateId);
        *pu4NextOspfExtLsdbRouterId = OSPF_CRU_BMC_DWFROMPDU (nextAdvRtrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfExtLsdbSequence
 Input       :  The Indices
                OspfExtLsdbType
                OspfExtLsdbLsid
                OspfExtLsdbRouterId

                The Object 
                retValOspfExtLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExtLsdbSequence (INT4 i4OspfExtLsdbType, UINT4 u4OspfExtLsdbLsid,
                           UINT4 u4OspfExtLsdbRouterId,
                           INT4 *pi4RetValOspfExtLsdbSequence)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfExtLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfExtLsdbRouterId);

    if ((pLsaInfo =
         LsuSearchDatabase ((UINT1) i4OspfExtLsdbType, &linkStateId, &advRtrId,
                            (UINT1 *) NULL, (UINT1 *) (pOspfCxt->pBackbone)))
        != NULL)
    {

        *pi4RetValOspfExtLsdbSequence = pLsaInfo->lsaSeqNum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfExtLsdbAge
 Input       :  The Indices
                OspfExtLsdbType
                OspfExtLsdbLsid
                OspfExtLsdbRouterId

                The Object 
                retValOspfExtLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExtLsdbAge (INT4 i4OspfExtLsdbType, UINT4 u4OspfExtLsdbLsid,
                      UINT4 u4OspfExtLsdbRouterId,
                      INT4 *pu4RetValOspfExtLsdbAge)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfExtLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfExtLsdbRouterId);

    if ((pLsaInfo =
         LsuSearchDatabase ((UINT1) i4OspfExtLsdbType, &linkStateId, &advRtrId,
                            (UINT1 *) NULL, (UINT1 *) (pOspfCxt->pBackbone)))
        != NULL)
    {

        GET_LSA_AGE (pLsaInfo, pu4RetValOspfExtLsdbAge);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfExtLsdbChecksum
 Input       :  The Indices
                OspfExtLsdbType
                OspfExtLsdbLsid
                OspfExtLsdbRouterId

                The Object 
                retValOspfExtLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExtLsdbChecksum (INT4 i4OspfExtLsdbType, UINT4 u4OspfExtLsdbLsid,
                           UINT4 u4OspfExtLsdbRouterId,
                           INT4 *pi4RetValOspfExtLsdbChecksum)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfExtLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfExtLsdbRouterId);

    if ((pLsaInfo =
         LsuSearchDatabase ((UINT1) i4OspfExtLsdbType, &linkStateId, &advRtrId,
                            (UINT1 *) NULL, (UINT1 *) (pOspfCxt->pBackbone)))
        != NULL)
    {

        *pi4RetValOspfExtLsdbChecksum = pLsaInfo->u2LsaChksum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfExtLsdbAdvertisement
 Input       :  The Indices
                OspfExtLsdbType
                OspfExtLsdbLsid
                OspfExtLsdbRouterId

                The Object 
                retValOspfExtLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfExtLsdbAdvertisement (INT4 i4OspfExtLsdbType,
                                UINT4 u4OspfExtLsdbLsid,
                                UINT4 u4OspfExtLsdbRouterId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValOspfExtLsdbAdvertisement)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4OspfExtLsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4OspfExtLsdbRouterId);

    if ((pLsaInfo =
         LsuSearchDatabase ((UINT1) i4OspfExtLsdbType, &linkStateId, &advRtrId,
                            (UINT1 *) NULL, (UINT1 *) (pOspfCxt->pBackbone)))
        != NULL)
    {

        /* As the SNMP implemenation supports only 
         * maximum of SNMP_MAX_OCTETSTRING_SIZE ,
         * We fill only upto SNMP_MAX_OCTETSTRING_SIZE*/
        pRetValOspfExtLsdbAdvertisement->i4_Length =
            ((pLsaInfo->u2LsaLen <= SNMP_MAX_OCTETSTRING_SIZE) ?
             pLsaInfo->u2LsaLen : SNMP_MAX_OCTETSTRING_SIZE);
        OS_MEM_CPY (pRetValOspfExtLsdbAdvertisement->pu1_OctetList,
                    pLsaInfo->pLsa, pRetValOspfExtLsdbAdvertisement->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : OspfAreaAggregateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfAreaAggregateTable
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfAreaAggregateTable (UINT4
                                                u4OspfAreaAggregateAreaID,
                                                INT4
                                                i4OspfAreaAggregateLsdbType,
                                                UINT4
                                                u4OspfAreaAggregateNet,
                                                UINT4 u4OspfAreaAggregateMask)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    INT1                i1Index;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);

    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                  (UINT1) i4OspfAreaAggregateLsdbType,
                                  &i1Index) != OSPF_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfAreaAggregateTable
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfAreaAggregateTable (UINT4 *pu4OspfAreaAggregateAreaID,
                                        INT4 *pi4OspfAreaAggregateLsdbType,
                                        UINT4 *pu4OspfAreaAggregateNet,
                                        UINT4 *pu4OspfAreaAggregateMask)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    INT1                i1Index;
    INT1                i1AreaCount;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (i1AreaCount = (INT1) TMO_SLL_Count (&(pOspfCxt->areasLst));
         i1AreaCount > 0; i1AreaCount--)
    {
        pArea = (tArea *) TMO_SLL_Nth (&(pOspfCxt->areasLst), i1AreaCount);

        if (NULL == pArea)
        {
            continue;
        }

        IP_ADDR_COPY (areaId, pArea->areaId);
        for (i1Index = MAX_ADDR_RANGES_PER_AREA - 1; i1Index >= 0; i1Index--)
        {
            if (pArea->aAddrRange[i1Index].areaAggStatus != INVALID)
            {
                *pi4OspfAreaAggregateLsdbType =
                    pArea->aAddrRange[i1Index].u1LsdbType;
                IP_ADDR_COPY (ipAddr, pArea->aAddrRange[i1Index].ipAddr);
                IP_ADDR_COPY (addrMask, pArea->aAddrRange[i1Index].ipAddrMask);
                *pu4OspfAreaAggregateAreaID = OSPF_CRU_BMC_DWFROMPDU (areaId);
                *pu4OspfAreaAggregateNet = OSPF_CRU_BMC_DWFROMPDU (ipAddr);
                *pu4OspfAreaAggregateMask = OSPF_CRU_BMC_DWFROMPDU (addrMask);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfAreaAggregateTable
 Input       :  The Indices
                OspfAreaAggregateAreaID
                nextOspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                nextOspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                nextOspfAreaAggregateNet
                OspfAreaAggregateMask
                nextOspfAreaAggregateMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfAreaAggregateTable (UINT4 u4OspfAreaAggregateAreaID,
                                       UINT4
                                       *pu4NextOspfAreaAggregateAreaID,
                                       INT4 i4OspfAreaAggregateLsdbType,
                                       INT4
                                       *pi4NextOspfAreaAggregateLsdbType,
                                       UINT4 u4OspfAreaAggregateNet,
                                       UINT4 *pu4NextOspfAreaAggregateNet,
                                       UINT4 u4OspfAreaAggregateMask,
                                       UINT4 *pu4NextOspfAreaAggregateMask)
{
    tAreaId             currAreaId;
    tIPADDR             currIpAddr;
    tIPADDR             currAddrMask;
    tAreaId             nextAreaId;
    tIPADDR             nextIpAddr;
    tIPADDR             nextAddrMask;
    tArea              *pArea;
    INT1                i1Index;
    INT1                i1AreaCount;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4OspfAreaAggregateAreaID);

    OSPF_CRU_BMC_DWTOPDU (currIpAddr, u4OspfAreaAggregateNet);

    OSPF_CRU_BMC_DWTOPDU (currAddrMask, u4OspfAreaAggregateMask);

    for (i1AreaCount = (INT1) TMO_SLL_Count (&(pOspfCxt->areasLst));
         i1AreaCount > 0; i1AreaCount--)
    {
        pArea = (tArea *) TMO_SLL_Nth (&(pOspfCxt->areasLst), i1AreaCount);

        if (NULL == pArea)
        {
            continue;
        }

        IP_ADDR_COPY (nextAreaId, pArea->areaId);

        for (i1Index = MAX_ADDR_RANGES_PER_AREA - 1; i1Index >= 0; i1Index--)
        {

            if (pArea->aAddrRange[i1Index].areaAggStatus == INVALID)
            {
                continue;
            }
            switch (UtilIpAddrComp (currAreaId, pArea->areaId))
            {
                case OSPF_GREATER:
                    /* try next entry */
                    continue;

                case OSPF_EQUAL:
                    /* first index is complete */
                    switch (UtilIpAddrComp (currIpAddr,
                                            pArea->aAddrRange[i1Index].ipAddr))
                    {
                        case OSPF_GREATER:
                            /* try next entry */
                            continue;
                        case OSPF_EQUAL:
                            /* second index is complete */
                            switch (UtilIpAddrComp (currAddrMask,
                                                    pArea->aAddrRange
                                                    [i1Index].ipAddrMask))
                            {
                                case OSPF_GREATER:
                                    /* try next entry */
                                    continue;
                                case OSPF_EQUAL:
                                {
                                    if (pArea->aAddrRange[i1Index].u1LsdbType
                                        <= i4OspfAreaAggregateLsdbType)
                                    {
                                        /* try next entry */
                                        continue;
                                    }
                                    /* next found, fall through */
                                }
                                    /* next found, fall through */
                                case OSPF_LESS:;
                                    /* next found, fall through */
                            }
                            /* next found, fall through */
                        case OSPF_LESS:;
                    }
                    /* next found, fall through */
                    /* next found, fall through */
                case OSPF_LESS:
                    IP_ADDR_COPY (nextIpAddr,
                                  pArea->aAddrRange[i1Index].ipAddr);
                    IP_ADDR_COPY (nextAddrMask,
                                  pArea->aAddrRange[i1Index].ipAddrMask);
                    *pi4NextOspfAreaAggregateLsdbType =
                        pArea->aAddrRange[i1Index].u1LsdbType;
                    *pu4NextOspfAreaAggregateAreaID =
                        OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                    *pu4NextOspfAreaAggregateNet =
                        OSPF_CRU_BMC_DWFROMPDU (nextIpAddr);
                    *pu4NextOspfAreaAggregateMask =
                        OSPF_CRU_BMC_DWFROMPDU (nextAddrMask);
                    return SNMP_SUCCESS;
                default:
                    break;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfAreaAggregateStatus
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask

                The Object 
                retValOspfAreaAggregateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaAggregateStatus (UINT4 u4OspfAreaAggregateAreaID,
                               INT4 i4OspfAreaAggregateLsdbType,
                               UINT4 u4OspfAreaAggregateNet,
                               UINT4 u4OspfAreaAggregateMask,
                               INT4 *pi4RetValOspfAreaAggregateStatus)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    INT1                i1Index;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);

    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                  (UINT1)
                                  i4OspfAreaAggregateLsdbType,
                                  &i1Index) != OSPF_FAILURE)
        {
            *pi4RetValOspfAreaAggregateStatus =
                pArea->aAddrRange[i1Index].areaAggStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfAreaAggregateEffect
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask

                The Object 
                retValOspfAreaAggregateEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfAreaAggregateEffect (UINT4 u4OspfAreaAggregateAreaID,
                               INT4 i4OspfAreaAggregateLsdbType,
                               UINT4 u4OspfAreaAggregateNet,
                               UINT4 u4OspfAreaAggregateMask,
                               INT4 *pi4RetValOspfAreaAggregateEffect)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    INT1                i1Index;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);

    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                  (UINT1)
                                  i4OspfAreaAggregateLsdbType,
                                  &i1Index) != OSPF_FAILURE)
        {
            *pi4RetValOspfAreaAggregateEffect =
                pArea->aAddrRange[i1Index].u1AdvertiseStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfOverFlowState
 Input       :  The Indices

                The Object 
                retValFutOspfOverFlowState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfOverFlowState (INT4 *pi4RetValFutOspfOverFlowState)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pi4RetValFutOspfOverFlowState);

    *pi4RetValFutOspfOverFlowState = pOspfCxt->bOverflowState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfPktsRcvd
 Input       :  The Indices

                The Object 
                retValFutOspfPktsRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfPktsRcvd (UINT4 *pu4RetValFutOspfPktsRcvd)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    *pu4RetValFutOspfPktsRcvd = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Sum up all the packets received via each of the interfaces . */

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        *pu4RetValFutOspfPktsRcvd += pInterface->u4HelloRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4DdpRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4LsaReqRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4LsaUpdateRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4LsaAckRcvdCount;
    }

    /* Sum up all the packets received via virtual interfaces also. */

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        *pu4RetValFutOspfPktsRcvd += pInterface->u4HelloRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4DdpRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4LsaReqRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4LsaUpdateRcvdCount;
        *pu4RetValFutOspfPktsRcvd += pInterface->u4LsaAckRcvdCount;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfPktsTxed
 Input       :  The Indices

                The Object 
                retValFutOspfPktsTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfPktsTxed (UINT4 *pu4RetValFutOspfPktsTxed)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    *pu4RetValFutOspfPktsTxed = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Sum up all the packets transmitted via each of the interfaces . */

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        *pu4RetValFutOspfPktsTxed += pInterface->u4HelloTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4DdpTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4LsaReqTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4LsaUpdateTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4LsaAckTxedCount;
    }

    /* Sum up all the packets transmitted via virtual interfaces also. */

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        *pu4RetValFutOspfPktsTxed += pInterface->u4HelloTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4DdpTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4LsaReqTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4LsaUpdateTxedCount;
        *pu4RetValFutOspfPktsTxed += pInterface->u4LsaAckTxedCount;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfPktsDisd
 Input       :  The Indices

                The Object 
                retValFutOspfPktsDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfPktsDisd (UINT4 *pu4RetValFutOspfPktsDisd)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFutOspfPktsDisd = pOspfCxt->u4OspfPktsDisd;

    /* Sum up all the packets discarded via each of the interfaces . */

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        *pu4RetValFutOspfPktsDisd += pInterface->u4HelloDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4DdpDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4LsaReqDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4LsaUpdateDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4LsaAckDisdCount;
    }

    /* Sum up all the packets discarded via virtual interfaces also. */

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        *pu4RetValFutOspfPktsDisd += pInterface->u4HelloDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4DdpDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4LsaReqDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4LsaUpdateDisdCount;
        *pu4RetValFutOspfPktsDisd += pInterface->u4LsaAckDisdCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMaxAreas
 Input       :  The Indices

                The Object 
                retValFutOspfMaxAreas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfMaxAreas (INT4 *pi4RetValFutOspfMaxAreas)
{
    *pi4RetValFutOspfMaxAreas =
        FsOSPFSizingParams[MAX_OSPF_AREAS_SIZING_ID].u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMaxLSAperArea
 Input       :  The Indices

                The Object 
                retValFutOspfMaxLSAperArea
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfMaxLSAperArea (INT4 *pi4RetValFutOspfMaxLSAperArea)
{
    *pi4RetValFutOspfMaxLSAperArea = MAX_LSAS_PER_AREA;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMaxExtLSAs
 Input       :  The Indices

                The Object 
                retValFutOspfMaxExtLSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfMaxExtLSAs (INT4 *pi4RetValFutOspfMaxExtLSAs)
{
    *pi4RetValFutOspfMaxExtLSAs =
        FsOSPFSizingParams[MAX_OSPF_EXT_LSA_DB_NODES_SIZING_ID].
        u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMaxSelfOrgLSAs
 Input       :  The Indices

                The Object 
                retValFutOspfMaxSelfOrgLSAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfMaxSelfOrgLSAs (INT4 *pi4RetValFutOspfMaxSelfOrgLSAs)
{
    *pi4RetValFutOspfMaxSelfOrgLSAs =
        FsOSPFSizingParams[MAX_OSPF_LSA_DESCRIPTORS_SIZING_ID].
        u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMaxRoutes
 Input       :  The Indices

                The Object 
                retValFutOspfMaxRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfMaxRoutes (INT4 *pi4RetValFutOspfMaxRoutes)
{
    *pi4RetValFutOspfMaxRoutes =
        FsOSPFSizingParams[MAX_OSPF_RT_ENTRIES_SIZING_ID].u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMaxLsaSize
 Input       :  The Indices

                The Object 
                retValFutOspfMaxLsaSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfMaxLsaSize (INT4 *pi4RetValFutOspfMaxLsaSize)
{
    *pi4RetValFutOspfMaxLsaSize = MAX_LSA_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTraceLevel
 Input       :  The Indices

                The Object 
                retValFutOspfTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTraceLevel (INT4 *pi4RetValFutOspfTraceLevel)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef TRACE_WANTED
    *pi4RetValFutOspfTraceLevel = pOspfCxt->u4OspfTrace;
#else
    /* If Trace wanted switch is not defined then Trace Value
     * will be returned as 0, That means no trace level.*/
    *pi4RetValFutOspfTraceLevel = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfMinLsaInterval
 Input       :  The Indices

                The Object 
                retValFutOspfMinLsaInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1
nmhGetFutOspfMinLsaInterval (INT4 *pi4RetValFutOspfMinLsaInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfMinLsaInterval = pOspfCxt->u2MinLsaInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfABRType
 Input       :  The Indices

                The Object
                retValFutOspfABRType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfABRType (INT4 *pi4RetValFutOspfABRType)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfABRType = pOspfCxt->u4ABRType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfNssaAsbrDefRtTrans
 Input       :  The Indices

                The Object 
                retValFutOspfMinLsaInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1
nmhGetFutOspfNssaAsbrDefRtTrans (INT4 *pi4RetValFutOspfNssaAsbrDefRtTrans)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfNssaAsbrDefRtTrans = (INT4) pOspfCxt->bNssaAsbrDefRtTrans;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfDefaultPassiveInterface
 Input       :  The Indices

                The Object
                retValFutOspfDefaultPassiveInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfDefaultPassiveInterface (INT4
                                      *pi4RetValFutOspfDefaultPassiveInterface)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfDefaultPassiveInterface =
        (INT4) pOspfCxt->bDefaultPassiveInterface;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfSpfHoldtime
 Input       :  The Indices

                The Object
                retValFutOspfSpfHoldtime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfSpfHoldtime (INT4 *pi4RetValFutOspfSpfHoldtime)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfSpfHoldtime = (INT4) pOspfCxt->u4SpfHoldTimeInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfSpfDelay
 Input       :  The Indices

                The Object
                retValFutOspfSpfDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfSpfDelay (INT4 *pi4RetValFutOspfSpfDelay)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfSpfDelay = (INT4) pOspfCxt->u4SpfInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartSupport
 Input       :  The Indices

                The Object
                retValFutOspfRestartSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartSupport (INT4 *pi4RetValFutOspfRestartSupport)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartSupport = pOspfCxt->u1RestartSupport;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartInterval
 Input       :  The Indices

                The Object
                retValFutOspfRestartInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartInterval (INT4 *pi4RetValFutOspfRestartInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartInterval = pOspfCxt->u4GracePeriod;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartStrictLsaChecking
 Input       :  The Indices

                The Object
                retValFutOspfRestartStrictLsaChecking
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFutOspfRestartStrictLsaChecking
    (INT4 *pi4RetValFutOspfRestartStrictLsaChecking)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartStrictLsaChecking = pOspfCxt->u1StrictLsaCheck;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartStatus
 Input       :  The Indices

                The Object
                retValFutOspfRestartStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartStatus (INT4 *pi4RetValFutOspfRestartStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartStatus = pOspfCxt->u1RestartStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartAge
 Input       :  The Indices

                The Object
                retValFutOspfRestartAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartAge (UINT4 *pu4RetValFutOspfRestartAge)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TmrGetRemainingTime
        (gTimerLst,
         (tTmrAppTimer *) (&(pOspfCxt->graceTimer.timerNode)),
         pu4RetValFutOspfRestartAge) != TMR_SUCCESS)
    {
        *pu4RetValFutOspfRestartAge = 0;
    }
    else
    {
        *pu4RetValFutOspfRestartAge =
            (*pu4RetValFutOspfRestartAge / NO_OF_TICKS_PER_SEC);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartExitReason
 Input       :  The Indices

                The Object
                retValFutOspfRestartExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartExitReason (INT4 *pi4RetValFutOspfRestartExitReason)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartExitReason = pOspfCxt->u1RestartExitReason;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfHelperSupport
 Input       :  The Indices

                The Object
                retValFutOspfHelperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfHelperSupport (tSNMP_OCTET_STRING_TYPE *
                            pRetValFutOspfHelperSupport)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFutOspfHelperSupport->pu1_OctetList,
            &pOspfCxt->u1HelperSupport, 1);
    pRetValFutOspfHelperSupport->i4_Length = sizeof (UINT1);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfExtTraceLevel
 Input       :  The Indices

                The Object
                retValFutOspfExtTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtTraceLevel (INT4 *pi4RetValFutOspfExtTraceLevel)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfExtTraceLevel = pOspfCxt->u4OspfExtTrace;
    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhGetFutospfRouterIdPermanence
  Input       :  The Indices
                 The Object
                 retValFutospfRouterIdPermanence
  Output      :  The Get Low Lev Routine Take the Indices &                                                       store the Value requested in the Return val.
                 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutospfRouterIdPermanence (INT4 *pi4RetValFutospfRouterIdPermanence)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutospfRouterIdPermanence = pOspfCxt->i4RouterIdStatus;
    return SNMP_SUCCESS;
}

/***************************************************************************
 Function    :  nmhGetFutOspfBfdStatus
 Input       :  The Indices
                The Object
                retValFutOspfBfdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *********************************************************************/
INT1
nmhGetFutOspfBfdStatus (INT4 *pi4RetValFutOspfBfdStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfBfdStatus = pOspfCxt->u1BfdAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfBfdAllIfState
 Input       :  The Indices
                The Object
                retValFutOspfBfdAllIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfBfdAllIfState (INT4 *pi4RetValFutOspfBfdAllIfState)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfBfdAllIfState = pOspfCxt->u1BfdStatusInAllIf;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfHelperGraceTimeLimit
 Input       :  The Indices

                The Object 
                retValFutOspfHelperGraceTimeLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfHelperGraceTimeLimit (INT4 *pi4RetValFutOspfHelperGraceTimeLimit)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfHelperGraceTimeLimit = pOspfCxt->u4HelperGrTimeLimit;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartAckState
 Input       :  The Indices

                The Object 
                retValFutOspfRestartAckState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartAckState (INT4 *pi4RetValFutOspfRestartAckState)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartAckState = pOspfCxt->u4GraceAckState;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfGraceLsaRetransmitCount
 Input       :  The Indices

                The Object 
                retValFutOspfGraceLsaRetransmitCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfGraceLsaRetransmitCount (INT4
                                      *pi4RetValFutOspfGraceLsaRetransmitCount)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfGraceLsaRetransmitCount = pOspfCxt->u1GrLsaMaxTxCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRestartReason
 Input       :  The Indices

                The Object 
                retValFutOspfRestartReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRestartReason (INT4 *pi4RetValFutOspfRestartReason)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRestartReason = pOspfCxt->u1RestartReason;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfRTStaggeringInterval
 Input       :  The Indices

                The Object 
                retValFutOspfRTStaggeringInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRTStaggeringInterval (UINT4 *pu4RetValFutOspfRTStaggeringInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFutOspfRTStaggeringInterval = (pOspfCxt->u4RTStaggeringInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRTStaggeringStatus
 Input       :  The Indices

                The Object 
                retValFutOspfRTStaggeringStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRTStaggeringStatus (INT4 *pi4RetValFutOspfRTStaggeringStatus)
{
    *pi4RetValFutOspfRTStaggeringStatus = (INT4) gOsRtr.u4RTStaggeringStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfHotStandbyAdminStatus
 Input       :  The Indices

                The Object
                retValFutOspfHotStandbyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfHotStandbyAdminStatus (INT4 *pi4RetValFutOspfHotStandbyAdminStatus)
{
    *pi4RetValFutOspfHotStandbyAdminStatus = gOsRtr.osRedInfo.u1AdminState;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfHotStandbyState
 Input       :  The Indices

                The Object
                retValFutOspfHotStandbyState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfHotStandbyState (INT4 *pi4RetValFutOspfHotStandbyState)
{
    *pi4RetValFutOspfHotStandbyState = gOsRtr.osRedInfo.u4OsRmState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfDynamicBulkUpdStatus
 Input       :  The Indices

                The Object
                retValFutOspfDynamicBulkUpdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfDynamicBulkUpdStatus (INT4 *pi4RetValFutOspfDynamicBulkUpdStatus)
{
    *pi4RetValFutOspfDynamicBulkUpdStatus =
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfStanbyHelloSyncCount
 Input       :  The Indices

                The Object
                retValFutOspfStanbyHelloSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfStanbyHelloSyncCount (UINT4 *pu4RetValFutOspfStanbyHelloSyncCount)
{
    *pu4RetValFutOspfStanbyHelloSyncCount = gOsRtr.osRedInfo.u4HelloSyncCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfStanbyLsaSyncCount
 Input       :  The Indices

                The Object
                retValFutOspfStanbyLsaSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfStanbyLsaSyncCount (UINT4 *pu4RetValFutOspfStanbyLsaSyncCount)
{
    *pu4RetValFutOspfStanbyLsaSyncCount = gOsRtr.osRedInfo.u4LsaSyncCount;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutospfAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfAreaTable
 Input       :  The Indices
                FutOspfAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfAreaTable (UINT4 u4FutOspfAreaId)
{
    return nmhValidateIndexInstanceOspfAreaTable (u4FutOspfAreaId);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfAreaTable
 Input       :  The Indices
                FutOspfAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfAreaTable (UINT4 *pu4FutOspfAreaId)
{
    return nmhGetFirstIndexOspfAreaTable (pu4FutOspfAreaId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfAreaTable
 Input       :  The Indices
                FutOspfAreaId
                nextFutOspfAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfAreaTable (UINT4 u4FutOspfAreaId,
                                 UINT4 *pu4NextFutOspfAreaId)
{
    return nmhGetNextIndexOspfAreaTable (u4FutOspfAreaId, pu4NextFutOspfAreaId);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfAreaIfCount
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaIfCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaIfCount (UINT4 u4FutOspfAreaId,
                          UINT4 *pu4RetValFutOspfAreaIfCount)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValFutOspfAreaIfCount = TMO_SLL_Count (&(pArea->ifsInArea));
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pu4RetValFutOspfAreaIfCount = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfAreaNetCount
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaNetCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaNetCount (UINT4 u4FutOspfAreaId,
                           UINT4 *pu4RetValFutOspfAreaNetCount)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValFutOspfAreaNetCount = TMO_SLL_Count (&(pArea->networkLsaLst));
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pu4RetValFutOspfAreaNetCount = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAreaRtrCount
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaRtrCount (UINT4 u4FutOspfAreaId,
                           UINT4 *pu4RetValFutOspfAreaRtrCount)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValFutOspfAreaRtrCount = TMO_SLL_Count (&(pArea->rtrLsaLst));
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pu4RetValFutOspfAreaRtrCount = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/******************************************************************
 Function    :  nmhGetFutOspfAreaNSSATranslatorRole
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaNSSATranslatorRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*******************************************************************/
INT1
nmhGetFutOspfAreaNSSATranslatorRole (UINT4 u4FutOspfAreaId,
                                     INT4
                                     *pi4RetValFutOspfAreaNSSATranslatorRole)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pi4RetValFutOspfAreaNSSATranslatorRole = pArea->u1NssaTrnsltrRole;
        return SNMP_SUCCESS;
    }
    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pi4RetValFutOspfAreaNSSATranslatorRole = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAreaNSSATranslatorState
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaNSSATranslatorState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaNSSATranslatorState (UINT4 u4FutOspfAreaId,
                                      INT4
                                      *pi4RetValFutOspfAreaNSSATranslatorState)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if (pArea->u1NssaTrnsltrState == TRNSLTR_STATE_ENAELCTD)
        {
            if (pArea->u1NssaTrnsltrRole == TRNSLTR_ROLE_ALWAYS)
            {
                *pi4RetValFutOspfAreaNSSATranslatorState = 1;
            }
            else
            {
                *pi4RetValFutOspfAreaNSSATranslatorState = 2;
            }
        }
        else
        {
            *pi4RetValFutOspfAreaNSSATranslatorState = 3;
        }
        return SNMP_SUCCESS;

    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pi4RetValFutOspfAreaNSSATranslatorState = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAreaNSSATranslatorStabilityInterval
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaNSSATranslatorStabilityInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaNSSATranslatorStabilityInterval (UINT4 u4FutOspfAreaId,
                                                  INT4
                                                  *pi4RetValFutOspfAreaNSSATranslatorStabilityInterval)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pi4RetValFutOspfAreaNSSATranslatorStabilityInterval =
            pArea->u4NssaTrnsltrStbltyInterval;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pi4RetValFutOspfAreaNSSATranslatorStabilityInterval = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAreaNSSATranslatorEvents
 Input       :  The Indices
                FutOspfAreaId

                The Object 
                retValFutOspfAreaNSSATranslatorEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaNSSATranslatorEvents (UINT4 u4FutOspfAreaId,
                                       UINT4
                                       *pu4RetValFutOspfAreaNSSATranslatorEvents)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pu4RetValFutOspfAreaNSSATranslatorEvents = pArea->u4NssaTrnsltrEvents;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pu4RetValFutOspfAreaNSSATranslatorEvents = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetFutOspfAreaDfInfOriginate
 *  Input       :  The Indices
 *                 FutOspfAreaId
 *
 *                 The Object
 *                 retValFutOspfAreaDfInfOriginate
 *  Output      : The Get Low Lev Routine Take the Indices
 *                & store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS  or  SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFutOspfAreaDfInfOriginate (UINT4 u4FutOspfAreaId,
                                 INT4 *pi4RetValFutOspfAreaDfInfOriginate)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        *pi4RetValFutOspfAreaDfInfOriginate = pArea->u4DfInfOriginate;
        return SNMP_SUCCESS;
    }

    if (pOspfCxt->pBackbone == NULL && pOspfCxt->admnStat == OSPF_DISABLED)
    {
        *pi4RetValFutOspfAreaDfInfOriginate = 0;
        OSPF_TRC (MGMT_TRC | OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                  "Backbone area will be created only when ospf admin status is enabled\n");
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutospfHostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfHostTable
 Input       :  The Indices
                FutOspfHostIpAddress
                FutOspfHostTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfHostTable (UINT4 u4FutOspfHostIpAddress,
                                          INT4 i4FutOspfHostTOS)
{
    return nmhValidateIndexInstanceOspfHostTable (u4FutOspfHostIpAddress,
                                                  i4FutOspfHostTOS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfHostTable
 Input       :  The Indices
                FutOspfHostIpAddress
                FutOspfHostTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfHostTable (UINT4 *pu4FutOspfHostIpAddress,
                                  INT4 *pi4FutOspfHostTOS)
{
    return nmhGetFirstIndexOspfHostTable (pu4FutOspfHostIpAddress,
                                          pi4FutOspfHostTOS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfHostTable
 Input       :  The Indices
                FutOspfHostIpAddress
                nextFutOspfHostIpAddress
                FutOspfHostTOS
                nextFutOspfHostTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfHostTable (UINT4 u4FutOspfHostIpAddress,
                                 UINT4 *pu4NextFutOspfHostIpAddress,
                                 INT4 i4FutOspfHostTOS,
                                 INT4 *pi4NextFutOspfHostTOS)
{
    return nmhGetNextIndexOspfHostTable (u4FutOspfHostIpAddress,
                                         pu4NextFutOspfHostIpAddress,
                                         i4FutOspfHostTOS,
                                         pi4NextFutOspfHostTOS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfHostRouteIfIndex
 Input       :  The Indices
                FutOspfHostIpAddress
                FutOspfHostTOS

                The Object 
                retValFutOspfHostRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfHostRouteIfIndex (UINT4 u4FutOspfHostIpAddress,
                               INT4 i4FutOspfHostTOS,
                               INT4 *pi4RetValFutOspfHostRouteIfIndex)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4FutOspfHostIpAddress);
    if ((IS_VALID_TOS_VALUE (i4FutOspfHostTOS))
        && ((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) != NULL))
    {
        if (pHost->aHostCost[DECODE_TOS (i4FutOspfHostTOS)].rowStatus
            != INVALID)
        {
            *pi4RetValFutOspfHostRouteIfIndex = pHost->u4FwdIfIndex + 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfIfTable
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfIfTable (UINT4 *pu4FutOspfIfIpAddress,
                                INT4 *pi4FutOspfAddressLessIf)
{
    return nmhGetFirstIndexOspfIfTable (pu4FutOspfIfIpAddress,
                                        pi4FutOspfAddressLessIf);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfIfTable
 Input       :  The Indices
                FutOspfIfIpAddress
                nextFutOspfIfIpAddress
                FutOspfAddressLessIf
                nextFutOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfIfTable (UINT4 u4FutOspfIfIpAddress,
                               UINT4 *pu4NextFutOspfIfIpAddress,
                               INT4 i4FutOspfAddressLessIf,
                               INT4 *pi4NextFutOspfAddressLessIf)
{
    return nmhGetNextIndexOspfIfTable (u4FutOspfIfIpAddress,
                                       pu4NextFutOspfIfIpAddress,
                                       i4FutOspfAddressLessIf,
                                       pi4NextFutOspfAddressLessIf);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfIfOperState
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfOperState (UINT4 u4FutOspfIfIpAddress,
                          INT4 i4FutOspfAddressLessIf,
                          INT4 *pi4RetValFutOspfIfOperState)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4FutOspfAddressLessIf))
        != NULL)
    {
        *pi4RetValFutOspfIfOperState = pInterface->operStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfPassive
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object
                retValFutOspfIfPassive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfPassive (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        INT4 *pi4RetValFutOspfIfPassive)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfIfPassive = pInterface->bPassive;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfNbrCount
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfNbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfNbrCount (UINT4 u4FutOspfIfIpAddress,
                         INT4 i4FutOspfAddressLessIf,
                         UINT4 *pu4RetValFutOspfIfNbrCount)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfNbrCount = TMO_SLL_Count (&(pInterface->nbrsInIf));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfAdjCount
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfAdjCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfAdjCount (UINT4 u4FutOspfIfIpAddress,
                         INT4 i4FutOspfAddressLessIf,
                         UINT4 *pu4RetValFutOspfIfAdjCount)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tNeighbor          *pNbr;
    COUNTER16          *pCount;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pCount = (COUNTER16 *) pu4RetValFutOspfIfAdjCount;

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pCount = 0;
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);
            if (pNbr->u1NsmState == NBRS_FULL)
                (*pCount)++;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfIfHelloRcvd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfHelloRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfHelloRcvd (UINT4 u4FutOspfIfIpAddress,
                          INT4 i4FutOspfAddressLessIf,
                          UINT4 *pu4RetValFutOspfIfHelloRcvd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfHelloRcvd = pInterface->u4HelloRcvdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfHelloTxed
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfHelloTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfHelloTxed (UINT4 u4FutOspfIfIpAddress,
                          INT4 i4FutOspfAddressLessIf,
                          UINT4 *pu4RetValFutOspfIfHelloTxed)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfHelloTxed = pInterface->u4HelloTxedCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfHelloDisd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfHelloDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfHelloDisd (UINT4 u4FutOspfIfIpAddress,
                          INT4 i4FutOspfAddressLessIf,
                          UINT4 *pu4RetValFutOspfIfHelloDisd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfHelloDisd = pInterface->u4HelloDisdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfDdpRcvd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfDdpRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfDdpRcvd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfDdpRcvd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfDdpRcvd = pInterface->u4DdpRcvdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfDdpTxed
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfDdpTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfDdpTxed (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfDdpTxed)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfDdpTxed = pInterface->u4DdpTxedCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfDdpDisd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfDdpDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfDdpDisd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfDdpDisd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfDdpDisd = pInterface->u4DdpDisdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLrqRcvd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLrqRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLrqRcvd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLrqRcvd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLrqRcvd = pInterface->u4LsaReqRcvdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLrqTxed
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLrqTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLrqTxed (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLrqTxed)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLrqTxed = pInterface->u4LsaReqTxedCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLrqDisd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLrqDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLrqDisd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLrqDisd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLrqDisd = pInterface->u4LsaReqDisdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLsuRcvd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLsuRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLsuRcvd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLsuRcvd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLsuRcvd = pInterface->u4LsaUpdateRcvdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLsuTxed
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLsuTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLsuTxed (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLsuTxed)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLsuTxed = pInterface->u4LsaUpdateTxedCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLsuDisd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLsuDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLsuDisd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLsuDisd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLsuDisd = pInterface->u4LsaUpdateDisdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLakRcvd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLakRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLakRcvd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLakRcvd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLakRcvd = pInterface->u4LsaAckRcvdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLakTxed
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLakTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLakTxed (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLakTxed)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLakTxed = pInterface->u4LsaAckTxedCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfLakDisd
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfIfLakDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfLakDisd (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        UINT4 *pu4RetValFutOspfIfLakDisd)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfIfLakDisd = pInterface->u4LsaAckDisdCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfBfdState
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
                The Object
                retValFutOspfIfBfdState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfIfBfdState (UINT4 u4FutOspfIfIpAddress,
                         INT4 i4FutOspfAddressLessIf,
                         INT4 *pi4RetValFutOspfIfBfdState)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr,
                         (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfIfBfdState = pInterface->u1BfdIfStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfNbrTable
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfNbrTable (UINT4 *pu4FutOspfNbrIpAddr,
                                 INT4 *pi4FutOspfNbrAddressLessIndex)
{
    return nmhGetFirstIndexOspfNbrTable (pu4FutOspfNbrIpAddr,
                                         pi4FutOspfNbrAddressLessIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfNbrTable
 Input       :  The Indices
                FutOspfNbrIpAddr
                nextFutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex
                nextFutOspfNbrAddressLessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfNbrTable (UINT4 u4FutOspfNbrIpAddr,
                                UINT4 *pu4NextFutOspfNbrIpAddr,
                                INT4 i4FutOspfNbrAddressLessIndex,
                                INT4 *pi4NextFutOspfNbrAddressLessIndex)
{
    return nmhGetNextIndexOspfNbrTable (u4FutOspfNbrIpAddr,
                                        pu4NextFutOspfNbrIpAddr,
                                        i4FutOspfNbrAddressLessIndex,
                                        pi4NextFutOspfNbrAddressLessIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfNbrDBSummaryQLen
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex

                The Object 
                retValFutOspfNbrDBSummaryQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfNbrDBSummaryQLen (UINT4 u4FutOspfNbrIpAddr,
                               INT4 i4FutOspfNbrAddressLessIndex,
                               UINT4 *pu4RetValFutOspfNbrDBSummaryQLen)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4FutOspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4FutOspfNbrAddressLessIndex)) != NULL)
    {
        *pu4RetValFutOspfNbrDBSummaryQLen = pNbr->dbSummary.u2Len;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfNbrLSReqQLen
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex

                The Object 
                retValFutOspfNbrLSReqQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfNbrLSReqQLen (UINT4 u4FutOspfNbrIpAddr,
                           INT4 i4FutOspfNbrAddressLessIndex,
                           UINT4 *pu4RetValFutOspfNbrLSReqQLen)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4FutOspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4FutOspfNbrAddressLessIndex)) != NULL)
    {
        *pu4RetValFutOspfNbrLSReqQLen =
            TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfNbrRestartHelperStatus
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex

                The Object
                retValFutOspfNbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFutOspfNbrRestartHelperStatus
    (UINT4 u4FutOspfNbrIpAddr,
     INT4 i4FutOspfNbrAddressLessIndex,
     INT4 *pi4RetValFutOspfNbrRestartHelperStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&nbrIpAddr, 0, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4FutOspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4FutOspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValFutOspfNbrRestartHelperStatus = pNbr->u1NbrHelperStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfNbrRestartHelperAge
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex

                The Object
                retValFutOspfNbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfNbrRestartHelperAge (UINT4 u4FutOspfNbrIpAddr,
                                  INT4 i4FutOspfNbrAddressLessIndex,
                                  UINT4 *pu4RetValFutOspfNbrRestartHelperAge)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tNeighbor          *pNbr = NULL;
    tIPADDR             nbrIpAddr;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&nbrIpAddr, 0, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4FutOspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4FutOspfNbrAddressLessIndex)) != NULL)
    {
        if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
        {
            if (TmrGetRemainingTime
                (gTimerLst,
                 (tTmrAppTimer *) (&(pNbr->helperGraceTimer.timerNode)),
                 pu4RetValFutOspfNbrRestartHelperAge) == TMR_FAILURE)
            {
                *pu4RetValFutOspfNbrRestartHelperAge = 0;
            }
            else
            {
                *pu4RetValFutOspfNbrRestartHelperAge =
                    (*pu4RetValFutOspfNbrRestartHelperAge /
                     NO_OF_TICKS_PER_SEC);
            }
        }
        else
        {
            *pu4RetValFutOspfNbrRestartHelperAge = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfNbrRestartHelperExitReason
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex

                The Object
                retValFutOspfNbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFutOspfNbrRestartHelperExitReason
    (UINT4 u4FutOspfNbrIpAddr,
     INT4 i4FutOspfNbrAddressLessIndex,
     INT4 *pi4RetValFutOspfNbrRestartHelperExitReason)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&nbrIpAddr, 0, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4FutOspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4FutOspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValFutOspfNbrRestartHelperExitReason =
            pNbr->u1NbrHelperExitReason;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfNbrBfdState
 Input       :  The Indices
                FutOspfNbrIpAddr
                FutOspfNbrAddressLessIndex
                The Object
                retValFutOspfNbrBfdState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfNbrBfdState (UINT4 u4FutOspfNbrIpAddr,
                          INT4 i4FutOspfNbrAddressLessIndex,
                          INT4 *pi4RetValFutOspfNbrBfdState)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&nbrIpAddr, 0, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4FutOspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4FutOspfNbrAddressLessIndex)) != NULL)
    {
        *pi4RetValFutOspfNbrBfdState = pNbr->u1NbrBfdState;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfExtRouteTable. */
/******************************************************************************
Function : GetFindExtRouteInCxt

Input    : pOspfCxt
           pExtRouteDest
           pExtRouteMask

Output   : None

Returns  : pExtRoute on success
           NULL otherwise  
******************************************************************************/
tExtRoute          *
GetFindExtRouteInCxt (tOspfCxt * pOspfCxt, tIPADDR * pExtRouteDest,
                      tIPADDR * pExtRouteMask)
{
    UINT4               au4Index[2];
    tInputParams        inParams;
    tOutputParams       outParams;
    tExtRoute          *pExtRoute = NULL;
    VOID               *pTemp = NULL;

    au4Index[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (*pExtRouteDest));
    au4Index[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (*pExtRouteMask));

    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.pLeafNode = NULL;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = (UINT1 *) au4Index;
    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;

    if (TrieSearch (&inParams, &outParams, (VOID **) &pTemp) == TRIE_SUCCESS)
    {
        pExtRoute = ((tExtRoute *) outParams.pAppSpecInfo);
        return pExtRoute;
    }
    return NULL;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfExtRouteTable
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfExtRouteTable (UINT4 u4FutOspfExtRouteDest,
                                              UINT4 u4FutOspfExtRouteMask,
                                              INT4 i4FutOspfExtRouteTOS)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);
    if ((IS_VALID_TOS_VALUE ((INT1) i4FutOspfExtRouteTOS)) &&
        (((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                             &extRouteMask)) != NULL)
         && (pExtRoute->
             aMetric[DECODE_TOS (i4FutOspfExtRouteTOS)].rowStatus != INVALID)))
    {
        if ((MsrGetSaveStatus () == ISS_FALSE) ||
            (pExtRoute->u1ExtRtIsStatic == OSIX_TRUE))
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfExtRouteTable
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfExtRouteTable (UINT4 *pu4FutOspfExtRouteDest,
                                      UINT4 *pu4FutOspfExtRouteMask,
                                      INT4 *pi4FutOspfExtRouteTOS)
{
    return nmhGetNextIndexFutOspfExtRouteTable (0, pu4FutOspfExtRouteDest,
                                                0, pu4FutOspfExtRouteMask,
                                                -1, pi4FutOspfExtRouteTOS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfExtRouteTable
 Input       :  The Indices
                FutOspfExtRouteDest
                nextFutOspfExtRouteDest
                FutOspfExtRouteMask
                nextFutOspfExtRouteMask
                FutOspfExtRouteTOS
                nextFutOspfExtRouteTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfExtRouteTable (UINT4 u4FutOspfExtRouteDest,
                                     UINT4 *pu4NextFutOspfExtRouteDest,
                                     UINT4 u4FutOspfExtRouteMask,
                                     UINT4 *pu4NextFutOspfExtRouteMask,
                                     INT4 i4FutOspfExtRouteTOS,
                                     INT4 *pi4NextFutOspfExtRouteTOS)
{
    tIPADDR             currExtRouteDest;
    tIPADDR             currExtRouteMask;
    tIPADDR             nextExtRouteDest;
    tIPADDR             nextExtRouteMask;
    tExtRoute          *pExtRoute;
    INT1                i1Tos;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    VOID               *pAppSpecPtr = NULL;
    UINT1               u1CheckIpAddr = OSPF_FALSE;
    UINT1               u1CheckMask = OSPF_FALSE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currExtRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (currExtRouteMask, u4FutOspfExtRouteMask);

    au4Key[0] = OSIX_HTONL (u4FutOspfExtRouteDest);
    au4Key[1] = OSIX_HTONL (u4FutOspfExtRouteMask);
    inParams.pRoot = pOspfCxt->pExtRtRoot;

    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    pLeafNode = NULL;
    pExtRoute = NULL;

    /* There can be multiple TOS routes for same destination and next hop.
     * If the given route is present in the routing table, next TOS route
     * will be returned */

    if (TrieLookup (&inParams, &outParams, (VOID **) &(inParams.pLeafNode))
        == TRIE_SUCCESS)
    {
        pExtRoute = ((tExtRoute *) outParams.pAppSpecInfo);

        for (i1Tos = 0; i1Tos < ENCODE_TOS (OSPF_MAX_METRIC); i1Tos += 2)
        {
            if ((pExtRoute->aMetric[DECODE_TOS (i1Tos)].
                 rowStatus != INVALID) && (i1Tos > i4FutOspfExtRouteTOS) &&
                ((MsrGetSaveStatus () == ISS_FALSE) ||
                 (pExtRoute->u1ExtRtIsStatic == OSIX_TRUE)))
            {
                IP_ADDR_COPY (nextExtRouteDest, pExtRoute->ipNetNum);
                IP_ADDR_COPY (nextExtRouteMask, pExtRoute->ipAddrMask);
                *pu4NextFutOspfExtRouteDest =
                    OSPF_CRU_BMC_DWFROMPDU (nextExtRouteDest);
                *pu4NextFutOspfExtRouteMask =
                    OSPF_CRU_BMC_DWFROMPDU (nextExtRouteMask);
                *pi4NextFutOspfExtRouteTOS = (INT4) i1Tos;
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pOspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPF_EXT_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;

        if (TrieGetFirstNode (&inParams,
                              &pAppSpecPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return SNMP_FAILURE;
        }

        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;

            u1CheckIpAddr =
                UtilIpAddrComp (pExtRoute->ipNetNum, currExtRouteDest);
            u1CheckMask =
                UtilIpAddrComp (pExtRoute->ipAddrMask, currExtRouteMask);

            if (((u1CheckIpAddr == OSPF_LESS) || (u1CheckIpAddr == OSPF_EQUAL))
                && ((u1CheckMask == OSPF_LESS) || (u1CheckMask == OSPF_EQUAL)))
            {
                inParams.pRoot = pOspfCxt->pExtRtRoot;
                inParams.i1AppId = OSPF_EXT_RT_ID;
                inParams.pLeafNode = NULL;
                au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipNetNum));
                au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4Key;
                pLeafNode = inParams.pLeafNode;
                continue;
            }

            for (i1Tos = 0; i1Tos < ENCODE_TOS (OSPF_MAX_METRIC); i1Tos += 2)
            {
                if ((pExtRoute->aMetric[DECODE_TOS (i1Tos)].rowStatus !=
                     INVALID) && ((MsrGetSaveStatus () == ISS_FALSE) ||
                                  (pExtRoute->u1ExtRtIsStatic == OSIX_TRUE)))
                {
                    IP_ADDR_COPY (nextExtRouteDest, pExtRoute->ipNetNum);
                    IP_ADDR_COPY (nextExtRouteMask, pExtRoute->ipAddrMask);
                    *pu4NextFutOspfExtRouteDest =
                        OSPF_CRU_BMC_DWFROMPDU (nextExtRouteDest);
                    *pu4NextFutOspfExtRouteMask =
                        OSPF_CRU_BMC_DWFROMPDU (nextExtRouteMask);
                    *pi4NextFutOspfExtRouteTOS = (INT4) i1Tos;
                    return SNMP_SUCCESS;
                }
            }
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);

        return SNMP_FAILURE;
    }

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if ((TrieGetNext (&(inParams), &(outParams),
                      (VOID **) &(inParams.pLeafNode))) != TRIE_FAILURE)
    {
        pExtRoute = ((tExtRoute *) outParams.pAppSpecInfo);

        for (i1Tos = 0; i1Tos < ENCODE_TOS (OSPF_MAX_METRIC); i1Tos += 2)
        {
            if ((pExtRoute->aMetric[DECODE_TOS (i1Tos)].rowStatus != INVALID)
                && ((MsrGetSaveStatus () == ISS_FALSE) ||
                    (pExtRoute->u1ExtRtIsStatic == OSIX_TRUE)))
            {
                IP_ADDR_COPY (nextExtRouteDest, pExtRoute->ipNetNum);
                IP_ADDR_COPY (nextExtRouteMask, pExtRoute->ipAddrMask);
                *pu4NextFutOspfExtRouteDest =
                    OSPF_CRU_BMC_DWFROMPDU (nextExtRouteDest);
                *pu4NextFutOspfExtRouteMask =
                    OSPF_CRU_BMC_DWFROMPDU (nextExtRouteMask);
                *pi4NextFutOspfExtRouteTOS = (INT4) i1Tos;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteMetric
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteMetric (UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             INT4 *pi4RetValFutOspfExtRouteMetric)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                           &extRouteMask)) != NULL)
    {
        *pi4RetValFutOspfExtRouteMetric =
            pExtRoute->
            aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].u4Value;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteMetricType
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteMetricType (UINT4 u4FutOspfExtRouteDest,
                                 UINT4 u4FutOspfExtRouteMask,
                                 INT4 i4FutOspfExtRouteTOS,
                                 INT4 *pi4RetValFutOspfExtRouteMetricType)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute =
         GetFindExtRouteInCxt (pOspfCxt, &(extRouteDest),
                               &(extRouteMask))) != NULL)
    {
        *pi4RetValFutOspfExtRouteMetricType =
            pExtRoute->
            extrtParam[DECODE_TOS (i4FutOspfExtRouteTOS)].u1MetricType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteTag
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteTag (UINT4 u4FutOspfExtRouteDest,
                          UINT4 u4FutOspfExtRouteMask,
                          INT4 i4FutOspfExtRouteTOS,
                          INT4 *pi4RetValFutOspfExtRouteTag)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                           &extRouteMask)) != NULL)
    {
        *pi4RetValFutOspfExtRouteTag =
            pExtRoute->
            extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].u4ExtTag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteFwdAdr
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteFwdAdr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteFwdAdr (UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             UINT4 *pu4RetValFutOspfExtRouteFwdAdr)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tIPADDR             fwdAdr;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                           &extRouteMask)) != NULL)
    {
        IP_ADDR_COPY (&fwdAdr,
                      pExtRoute->extrtParam[DECODE_TOS
                                            ((INT1)
                                             i4FutOspfExtRouteTOS)].fwdAddr);
        *pu4RetValFutOspfExtRouteFwdAdr = OSPF_CRU_BMC_DWFROMPDU (fwdAdr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteIfIndex
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteIfIndex (UINT4 u4FutOspfExtRouteDest,
                              UINT4 u4FutOspfExtRouteMask,
                              INT4 i4FutOspfExtRouteTOS,
                              INT4 *pi4RetValFutOspfExtRouteIfIndex)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                           &extRouteMask)) != NULL)
    {
        *pi4RetValFutOspfExtRouteIfIndex =
            pExtRoute->
            extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].u4FwdIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteNextHop
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteNextHop (UINT4 u4FutOspfExtRouteDest,
                              UINT4 u4FutOspfExtRouteMask,
                              INT4 i4FutOspfExtRouteTOS,
                              UINT4 *pu4RetValFutOspfExtRouteNextHop)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tIPADDR             nextHop;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                           &extRouteMask)) != NULL)
    {
        IP_ADDR_COPY (&nextHop,
                      pExtRoute->extrtParam[DECODE_TOS
                                            ((INT1)
                                             i4FutOspfExtRouteTOS)].fwdNextHop);
        *pu4RetValFutOspfExtRouteNextHop = OSPF_CRU_BMC_DWFROMPDU (nextHop);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfExtRouteStatus
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                retValFutOspfExtRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfExtRouteStatus (UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             INT4 *pi4RetValFutOspfExtRouteStatus)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);

    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                           &extRouteMask)) != NULL)
    {
        *pi4RetValFutOspfExtRouteStatus =
            pExtRoute->
            aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].rowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfRoutingTable
 Input       :  The Indices
                FutOspfRouteIpAddr
                FutOspfRouteIpAddrMask
                FutOspfRouteIpTos
                FutOspfRouteIpNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfRoutingTable (UINT4 *pu4FutOspfRouteIpAddr,
                                     UINT4 *pu4FutOspfRouteIpAddrMask,
                                     INT4 *pi4FutOspfRouteIpTos,
                                     UINT4 *pu4FutOspfRouteIpNextHop)
{

    tRtEntry           *pRtEntry;
    tIPADDR             leastNextHopIpAddr;
    INT4                i4OutCome;
    UINT4               au4Key[2];
    tInputParams        inParams;    /* Index of Trie is made-up of address and
                                       mask */
    UINT1               u1Tos;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    IP_ADDR_COPY (leastNextHopIpAddr, gNullIpAddr);
    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pOspfRt->pOspfRoot;
    inParams.i1AppId = pOspfCxt->pOspfRt->i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    i4OutCome =
        TrieGetFirstNode (&inParams, (VOID *) &pRtEntry, &(inParams.pLeafNode));

    if (i4OutCome == SUCCESS)
    {
        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
            {
                *pu4FutOspfRouteIpAddr =
                    OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                *pu4FutOspfRouteIpAddrMask = pRtEntry->u4IpAddrMask;
                *pi4FutOspfRouteIpTos = ENCODE_TOS (u1Tos);
                GetLeastNextHopIpAddr (pRtEntry, &leastNextHopIpAddr, u1Tos);
                *pu4FutOspfRouteIpNextHop =
                    OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfRoutingTable
 Input       :  The Indices
                FutOspfRouteIpAddr
                nextFutOspfRouteIpAddr
                FutOspfRouteIpAddrMask
                nextFutOspfRouteIpAddrMask
                FutOspfRouteIpTos
                nextFutOspfRouteIpTos
                FutOspfRouteIpNextHop
                nextFutOspfRouteIpNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfRoutingTable (UINT4 u4FutOspfRouteIpAddr,
                                    UINT4 *pu4NextFutOspfRouteIpAddr,
                                    UINT4 u4FutOspfRouteIpAddrMask,
                                    UINT4 *pu4NextFutOspfRouteIpAddrMask,
                                    INT4 i4FutOspfRouteIpTos,
                                    INT4 *pi4NextFutOspfRouteIpTos,
                                    UINT4 u4FutOspfRouteIpNextHop,
                                    UINT4 *pu4NextFutOspfRouteIpNextHop)
{
    tRtEntry           *pRtEntry = NULL;
    tIPADDR             leastNextHopIpAddr;
    tIPADDR             nextLeastNextHopIpAddr;
    tIPADDR             destIpAddr;
    tIPADDR             nextHopIpAddr;
    UINT4               au4Key[2];
    tInputParams        inParams;    /* Index of Trie is made-up of address and
                                       mask */
    tOutputParams       outParams;
    UINT1               u1Tos;
    UINT1               u1TempFlag = OSPF_FALSE;
    VOID               *pNextRtEntry = NULL;
    UINT1               u1RtFound = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    VOID               *pLeafNode = NULL;
    VOID               *pAppSpecPtr = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    IP_ADDR_COPY (leastNextHopIpAddr, gNullIpAddr);
    IP_ADDR_COPY (nextLeastNextHopIpAddr, gNullIpAddr);
    OSPF_CRU_BMC_DWTOPDU (destIpAddr, u4FutOspfRouteIpAddr);
    OSPF_CRU_BMC_DWTOPDU (nextHopIpAddr, u4FutOspfRouteIpNextHop);

    au4Key[0] = CRU_HTONL (u4FutOspfRouteIpAddr);
    au4Key[1] = CRU_HTONL (u4FutOspfRouteIpAddrMask);
    inParams.pRoot = pOspfCxt->pOspfRt->pOspfRoot;
    inParams.i1AppId = pOspfCxt->pOspfRt->i1OspfId;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams, &pNextRtEntry) == SUCCESS)
    {
        pRtEntry = ((tRtEntry *) outParams.pAppSpecInfo);
        switch (UtilIpAddrComp (pRtEntry->destId, destIpAddr))
        {
            case OSPF_LESS:
                break;
            case OSPF_EQUAL:
            {
                if (pRtEntry->u4IpAddrMask == u4FutOspfRouteIpAddrMask)
                {
                    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                    {
                        if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
                        {
                            if (ENCODE_TOS (u1Tos)
                                == (UINT1) i4FutOspfRouteIpTos)
                            {
                                u1TempFlag = OSPF_FALSE;
                            }
                            else if (ENCODE_TOS (u1Tos) >
                                     (UINT1) i4FutOspfRouteIpTos)
                            {
                                u1TempFlag = OSPF_TRUE;
                            }
                            else if (ENCODE_TOS (u1Tos) <
                                     (UINT1) i4FutOspfRouteIpTos)
                            {
                                continue;
                            }
                            if (u1TempFlag == OSPF_TRUE)
                            {
                                *pu4NextFutOspfRouteIpAddr =
                                    u4FutOspfRouteIpAddr;
                                *pu4NextFutOspfRouteIpAddrMask =
                                    u4FutOspfRouteIpAddrMask;
                                *pi4NextFutOspfRouteIpTos = ENCODE_TOS (u1Tos);
                                GetLeastNextHopIpAddr
                                    (pRtEntry, &leastNextHopIpAddr, u1Tos);
                                *pu4NextFutOspfRouteIpNextHop =
                                    OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                                return SNMP_SUCCESS;
                            }
                            if (GetNextLeastNextHopIpAddr
                                (pRtEntry, &nextHopIpAddr,
                                 &nextLeastNextHopIpAddr,
                                 u1Tos) == SNMP_SUCCESS)
                            {
                                *pu4NextFutOspfRouteIpAddr =
                                    u4FutOspfRouteIpAddr;
                                *pu4NextFutOspfRouteIpAddrMask =
                                    u4FutOspfRouteIpAddrMask;
                                *pi4NextFutOspfRouteIpTos = ENCODE_TOS (u1Tos);
                                *pu4NextFutOspfRouteIpNextHop =
                                    OSPF_CRU_BMC_DWFROMPDU
                                    (nextLeastNextHopIpAddr);
                                return SNMP_SUCCESS;
                            }
                        }
                    }
                }
                if (pRtEntry->u4IpAddrMask > u4FutOspfRouteIpAddrMask)
                {
                    u1RtFound = OSPF_TRUE;
                }
            }
                break;
            case OSPF_GREATER:
            {
                u1RtFound = OSPF_TRUE;
            }
                break;
            default:
                break;

        }
        if (u1RtFound == OSPF_TRUE)
        {
            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
                {
                    *pu4NextFutOspfRouteIpAddr =
                        OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                    *pu4NextFutOspfRouteIpAddrMask = pRtEntry->u4IpAddrMask;
                    *pi4NextFutOspfRouteIpTos = ENCODE_TOS (u1Tos);
                    GetLeastNextHopIpAddr (pRtEntry,
                                           &leastNextHopIpAddr, u1Tos);
                    *pu4NextFutOspfRouteIpNextHop =
                        OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                    return SNMP_SUCCESS;
                }
            }
        }
    }                            /* if */
    else
    {
        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pOspfCxt->pOspfRt->pOspfRoot;
        inParams.i1AppId = pOspfCxt->pOspfRt->i1OspfId;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;

        if (TrieGetFirstNode (&inParams,
                              &pAppSpecPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return SNMP_FAILURE;
        }

        do
        {
            pRtEntry = (tRtEntry *) pAppSpecPtr;

            if (((UtilIpAddrComp (pRtEntry->destId, destIpAddr) == OSPF_LESS) ||
                 (UtilIpAddrComp (pRtEntry->destId, destIpAddr) == OSPF_EQUAL))
                && (pRtEntry->u4IpAddrMask <= u4FutOspfRouteIpAddrMask))
            {
                inParams.pRoot = pOspfCxt->pOspfRt->pOspfRoot;
                inParams.i1AppId = pOspfCxt->pOspfRt->i1OspfId;
                inParams.pLeafNode = NULL;
                au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pRtEntry->destId));
                au4Key[1] = OSIX_HTONL (pRtEntry->u4IpAddrMask);
                inParams.Key.pKey = (UINT1 *) au4Key;
                pLeafNode = inParams.pLeafNode;
                continue;
            }

            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
                {
                    u1RtFound = OSPF_TRUE;
                    *pu4NextFutOspfRouteIpAddr =
                        OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                    *pu4NextFutOspfRouteIpAddrMask = pRtEntry->u4IpAddrMask;
                    *pi4NextFutOspfRouteIpTos = ENCODE_TOS (u1Tos);
                    GetLeastNextHopIpAddr (pRtEntry,
                                           &leastNextHopIpAddr, u1Tos);
                    *pu4NextFutOspfRouteIpNextHop =
                        OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                    return SNMP_SUCCESS;
                }
            }
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);

        return SNMP_FAILURE;
    }

    inParams.pLeafNode = pNextRtEntry;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;

    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    while ((TrieGetNext (&(inParams), &(outParams),
                         (VOID *) &(inParams.pLeafNode))) == TRIE_SUCCESS)
    {
        pRtEntry = ((tRtEntry *) outParams.pAppSpecInfo);

        if ((UtilIpAddrComp (pRtEntry->destId, destIpAddr) == OSPF_LESS) ||
            ((UtilIpAddrComp (pRtEntry->destId, destIpAddr) == OSPF_EQUAL)
             && (pRtEntry->u4IpAddrMask < u4FutOspfRouteIpAddrMask)))
        {
            au4Key[0] = PTR_TO_U4 (pRtEntry->destId);
            au4Key[1] = pRtEntry->u4IpAddrMask;
            inParams.pLeafNode = NULL;
            inParams.Key.pKey = (UINT1 *) au4Key;
            pNextRtEntry = inParams.pLeafNode;
            pRtEntry = NULL;
            continue;
        }

        for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
        {
            if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
            {
                *pu4NextFutOspfRouteIpAddr =
                    OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                *pu4NextFutOspfRouteIpAddrMask = pRtEntry->u4IpAddrMask;
                *pi4NextFutOspfRouteIpTos = ENCODE_TOS (u1Tos);
                GetLeastNextHopIpAddr (pRtEntry, &leastNextHopIpAddr, u1Tos);
                *pu4NextFutOspfRouteIpNextHop =
                    OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */
    /****************************************************************************
     Function    :  nmhGetFutOspfRouteType
     Input       :  The Indices
            FutOspfRouteIpAddr
            FutOspfRouteIpAddrMask
            FutOspfRouteIpTos
            FutOspfRouteIpNextHop

            The Object 
            retValFutOspfRouteType
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRouteType (UINT4 u4FutOspfRouteIpAddr,
                        UINT4 u4FutOspfRouteIpAddrMask,
                        INT4 i4FutOspfRouteIpTos,
                        UINT4 u4FutOspfRouteIpNextHop,
                        INT4 *pi4RetValFutOspfRouteType)
{

    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfRouteType,
                                             OSPF_ROUTE_RTTYPE, DEST_NETWORK);

    return i1ReturnValue;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfRouteAreaId
     Input       :  The Indices
            FutOspfRouteIpAddr
            FutOspfRouteIpAddrMask
            FutOspfRouteIpTos
            FutOspfRouteIpNextHop

            The Object 
            retValFutOspfRouteAreaId
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRouteAreaId (UINT4 u4FutOspfRouteIpAddr,
                          UINT4 u4FutOspfRouteIpAddrMask,
                          INT4 i4FutOspfRouteIpTos,
                          UINT4 u4FutOspfRouteIpNextHop,
                          UINT4 *pu4RetValFutOspfRouteAreaId)
{

    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             pu4RetValFutOspfRouteAreaId,
                                             OSPF_ROUTE_AREAID, DEST_NETWORK);

    return i1ReturnValue;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRouteCost
     Input       :  The Indices
            FutOspfRouteIpAddr
            FutOspfRouteIpAddrMask
            FutOspfRouteIpTos
            FutOspfRouteIpNextHop

            The Object 
            retValFutOspfRouteCost
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRouteCost (UINT4 u4FutOspfRouteIpAddr,
                        UINT4 u4FutOspfRouteIpAddrMask,
                        INT4 i4FutOspfRouteIpTos,
                        UINT4 u4FutOspfRouteIpNextHop,
                        INT4 *pi4RetValFutOspfRouteCost)
{
    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfRouteCost,
                                             OSPF_ROUTE_COST, DEST_NETWORK);

    return i1ReturnValue;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRouteType2Cost
     Input       :  The Indices
            FutOspfRouteIpAddr
            FutOspfRouteIpAddrMask
            FutOspfRouteIpTos
            FutOspfRouteIpNextHop

            The Object 
            retValFutOspfRouteType2Cost
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRouteType2Cost (UINT4 u4FutOspfRouteIpAddr,
                             UINT4 u4FutOspfRouteIpAddrMask,
                             INT4 i4FutOspfRouteIpTos,
                             UINT4 u4FutOspfRouteIpNextHop,
                             INT4 *pi4RetValFutOspfRouteType2Cost)
{
    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfRouteType2Cost,
                                             OSPF_ROUTE_TYPE2COST,
                                             DEST_NETWORK);
    return i1ReturnValue;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRouteInterfaceIndex
     Input       :  The Indices
            FutOspfRouteIpAddr
            FutOspfRouteIpAddrMask
            FutOspfRouteIpTos
            FutOspfRouteIpNextHop

            The Object 
            retValFutOspfRouteInterfaceIndex
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRouteInterfaceIndex (UINT4 u4FutOspfRouteIpAddr,
                                  UINT4 u4FutOspfRouteIpAddrMask,
                                  INT4 i4FutOspfRouteIpTos,
                                  UINT4 u4FutOspfRouteIpNextHop,
                                  INT4 *pi4RetValFutOspfRouteInterfaceIndex)
{
    INT4                i4ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i4ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfRouteInterfaceIndex,
                                             OSPF_ROUTE_IFACEINDEX,
                                             DEST_NETWORK);
    return (INT1) i4ReturnValue;
}

    /*****************************************************************************/
    /* Function     : GetLeastSecIpAddrInCxt                                      */
    /* Description  : The routine returns the least of the secondary IP address  */
    /*                contained in an array corresponding to the given primary   */
    /*                IP address                                                 */
    /* Input        : pOspfCxt
     *                u4OspfPrimIpAddr                                           */
    /*                i4OspfPrimAddresslessIf                                    */
    /*                pSecIpAddr                                              */
    /*                pSecIpAddrMask                                         */
    /* Output       : None                                                       */
    /* Returns      : SUCCESS, if least secondary IP address is found            */
    /*                FAILURE, otherwise                                         */
    /*****************************************************************************/
INT1
GetLeastSecIpAddrInCxt (tOspfCxt * pOspfCxt, UINT4 u4OspfPrimIpAddr,
                        INT4 i4OspfPrimAddresslessIf,
                        tIPADDR * pSecIpAddr, tIPADDRMASK * pSecIpAddrMask)
{
    tInterface         *pInterface;
    tIPADDR             primIpAddr;
    tTRUTHVALUE         bSecFound = OSPF_FALSE;
    INT2                i2SecCount = 0;

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, u4OspfPrimIpAddr);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr,
                                      (UINT4) i4OspfPrimAddresslessIf)) == NULL)
    {
        return OSPF_FAILURE;
    }

    /* Getting the first ACTIVE secondary IP addr & the corresponding mask */
    while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
    {
        if ((pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status == ACTIVE))
        {
            IP_ADDR_COPY (*pSecIpAddr,
                          pInterface->secondaryIP.
                          ifSecIp[i2SecCount].ifSecIpAddr);
            IP_ADDR_COPY (*pSecIpAddrMask,
                          pInterface->secondaryIP.
                          ifSecIp[i2SecCount++].ifSecIpAddrMask);
            bSecFound = OSPF_TRUE;
            break;
        }                        /* end if */
        i2SecCount++;
    }                            /* end while */

    /* If all the entries are not ACTIVE, return FAILURE */
    if (bSecFound != OSPF_TRUE)
    {
        return OSPF_FAILURE;
    }

    /* Comparing with the rest of the secondary IP addresses to get the least value
     */
    while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
    {
        if ((pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status ==
             ACTIVE) &&
            (UtilIpAddrComp
             (pInterface->secondaryIP.ifSecIp[i2SecCount].ifSecIpAddr,
              *pSecIpAddr) == OSPF_LESS))
        {
            IP_ADDR_COPY (*pSecIpAddr,
                          pInterface->secondaryIP.
                          ifSecIp[i2SecCount].ifSecIpAddr);
            IP_ADDR_COPY (*pSecIpAddrMask,
                          pInterface->secondaryIP.
                          ifSecIp[i2SecCount].ifSecIpAddrMask);
        }                        /* end if */
        i2SecCount++;
    }                            /* end while */
    return SUCCESS;
}

    /* LOW LEVEL Routines for Table : FutOspfSecIfTable. */

    /****************************************************************************
     Function    :  nmhValidateIndexInstanceFutOspfSecIfTable
     Input       :  The Indices
            FutOspfPrimIpAddr
            FutOspfPrimAddresslessIf
            FutOspfSecIpAddr
            FutOspfSecIpAddrMask
     Output      :  The Routines Validates the Given Indices.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfSecIfTable (UINT4 u4FutOspfPrimIpAddr,
                                           INT4 i4FutOspfPrimAddresslessIf,
                                           UINT4 u4FutOspfSecIpAddr,
                                           UINT4 u4FutOspfSecIpAddrMask)
{
    tInterface         *pInterface;
    tIPADDR             primIpAddr, secIpAddr, sec_ip_mask;
    INT2                i2SecCount;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i2SecCount = 0;

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, u4FutOspfPrimIpAddr);
    OSPF_CRU_BMC_DWTOPDU (secIpAddr, u4FutOspfSecIpAddr);

    OSPF_CRU_BMC_DWTOPDU (sec_ip_mask, u4FutOspfSecIpAddrMask);

    pOspfCxt->pLastOspfIf = NULL;

    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr,
                                      (UINT4) i4FutOspfPrimAddresslessIf))
        != NULL)
    {
        while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
        {
            if (UtilAddrMaskCmpe
                (OSPF_CRU_BMC_DWFROMPDU
                 (pInterface->secondaryIP.
                  ifSecIp[i2SecCount].ifSecIpAddr),
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->secondaryIP.
                                         ifSecIp
                                         [i2SecCount].ifSecIpAddrMask),
                 u4FutOspfSecIpAddr, u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
            {
                /* Both prim and sec ifaces exist. */
                return SNMP_SUCCESS;
            }
            i2SecCount++;
        }                        /* End of While */
    }                            /* End of if */
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfSecIfTable
     Input       :  The Indices
            FutOspfPrimIpAddr
            FutOspfPrimAddresslessIf
            FutOspfSecIpAddr
            FutOspfSecIpAddrMask
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfSecIfTable (UINT4 *pu4FutOspfPrimIpAddr,
                                   INT4 *pi4FutOspfPrimAddresslessIf,
                                   UINT4 *pu4FutOspfSecIpAddr,
                                   UINT4 *pu4FutOspfSecIpAddrMask)
{
    UINT4               u4NextPrimIpAddr;
    INT4                i4NextPrimAddrlessIf;
    tIPADDR             secIpAddr;
    tIPADDRMASK         secIpAddrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    IP_ADDR_COPY (secIpAddr, gNullIpAddr);
    IP_ADDR_COPY (secIpAddrMask, gNullIpAddr);
    /* a search for the least secondary is done if the get first is successful
       on primary */

    if (nmhGetFirstIndexOspfIfTable (pu4FutOspfPrimIpAddr,
                                     pi4FutOspfPrimAddresslessIf)
        != SNMP_FAILURE)
    {
        if (GetLeastSecIpAddrInCxt
            (pOspfCxt, *pu4FutOspfPrimIpAddr, *pi4FutOspfPrimAddresslessIf,
             &secIpAddr, &secIpAddrMask) == SUCCESS)
        {
            *pu4FutOspfSecIpAddr = OSPF_CRU_BMC_DWFROMPDU (secIpAddr);
            *pu4FutOspfSecIpAddrMask = OSPF_CRU_BMC_DWFROMPDU (secIpAddrMask);
            return SNMP_SUCCESS;
        }

        while (nmhGetNextIndexOspfIfTable
               (*pu4FutOspfPrimIpAddr, &u4NextPrimIpAddr,
                *pi4FutOspfPrimAddresslessIf,
                &i4NextPrimAddrlessIf) != SNMP_FAILURE)
        {
            if (GetLeastSecIpAddrInCxt
                (pOspfCxt, u4NextPrimIpAddr, i4NextPrimAddrlessIf, &secIpAddr,
                 &secIpAddrMask) == SUCCESS)
            {
                *pu4FutOspfPrimIpAddr = u4NextPrimIpAddr;
                *pi4FutOspfPrimAddresslessIf = i4NextPrimAddrlessIf;

                *pu4FutOspfSecIpAddr = OSPF_CRU_BMC_DWFROMPDU (secIpAddr);
                *pu4FutOspfSecIpAddrMask =
                    OSPF_CRU_BMC_DWFROMPDU (secIpAddrMask);
                return SNMP_SUCCESS;
            }
            *pu4FutOspfPrimIpAddr = u4NextPrimIpAddr;
            *pi4FutOspfPrimAddresslessIf = i4NextPrimAddrlessIf;
        }
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfSecIfTable
     Input       :  The Indices
            FutOspfPrimIpAddr
            nextFutOspfPrimIpAddr
            FutOspfPrimAddresslessIf
            nextFutOspfPrimAddresslessIf
            FutOspfSecIpAddr
            nextFutOspfSecIpAddr
            FutOspfSecIpAddrMask
            nextFutOspfSecIpAddrMask
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfSecIfTable (UINT4 u4FutOspfPrimIpAddr,
                                  UINT4 *pu4NextFutOspfPrimIpAddr,
                                  INT4 i4FutOspfPrimAddresslessIf,
                                  INT4 *pi4NextFutOspfPrimAddresslessIf,
                                  UINT4 u4FutOspfSecIpAddr,
                                  UINT4 *pu4NextFutOspfSecIpAddr,
                                  UINT4 u4FutOspfSecIpAddrMask,
                                  UINT4 *pu4NextFutOspfSecIpAddrMask)
{
    tInterface         *pInterface;
    tIPADDR             primIpAddr;
    tIPADDR             currSecIpAddr;
    tIPADDRMASK         nextSecIpAddrMask;
    tIPADDR             nextSecIpAddr;
    tTRUTHVALUE         bSecFound = OSPF_FALSE;
    INT2                i2SecCount = 0;
    tTMO_SLL_NODE      *pLstNode;
    tIPADDRMASK         currSecIpAddrMask;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    IP_ADDR_COPY (nextSecIpAddr, gNullIpAddr);
    IP_ADDR_COPY (nextSecIpAddrMask, gNullIpAddr);

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, u4FutOspfPrimIpAddr);

    OSPF_CRU_BMC_DWTOPDU (currSecIpAddr, u4FutOspfSecIpAddr);

    OSPF_CRU_BMC_DWTOPDU (currSecIpAddrMask, u4FutOspfSecIpAddrMask);

    pOspfCxt->pLastOspfIf = NULL;

    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr,
                                      (UINT4) i4FutOspfPrimAddresslessIf)) ==
        NULL)
    {
        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if (UtilIpAddrComp (pInterface->ifIpAddr,
                                primIpAddr) == OSPF_GREATER)
            {
                if (pInterface->secondaryIP.i1SecIpCount != 0)
                {
                    bSecFound = OSPF_TRUE;
                    break;
                }
            }
        }
        if (bSecFound != OSPF_TRUE)
        {
            return SNMP_FAILURE;
        }
        bSecFound = OSPF_FALSE;
    }

    while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
    {
        if (pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status == ACTIVE)
        {
            switch (UtilIpAddrComp
                    (pInterface->secondaryIP.ifSecIp[i2SecCount].ifSecIpAddr,
                     currSecIpAddr))
            {
                case OSPF_EQUAL:
                    if (UtilIpAddrComp
                        (pInterface->secondaryIP.ifSecIp[i2SecCount].
                         ifSecIpAddrMask, currSecIpAddrMask) != OSPF_GREATER)
                    {
                        break;
                    }
                    /* fall through */
                case OSPF_GREATER:
                    IP_ADDR_COPY (nextSecIpAddr,
                                  pInterface->secondaryIP.
                                  ifSecIp[i2SecCount].ifSecIpAddr);
                    IP_ADDR_COPY (nextSecIpAddrMask,
                                  pInterface->secondaryIP.
                                  ifSecIp[i2SecCount].ifSecIpAddrMask);
                    bSecFound = OSPF_TRUE;
                    break;
                default:
                    break;
            }
        }                        /* end if */
        i2SecCount++;
        if (bSecFound == OSPF_TRUE)
        {
            break;
        }
    }                            /* end while */

    if (bSecFound == OSPF_TRUE)
    {
        while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
        {
            if ((pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status ==
                 ACTIVE)
                &&
                (UtilIpAddrComp
                 (pInterface->secondaryIP.
                  ifSecIp[i2SecCount].ifSecIpAddr,
                  nextSecIpAddr) == OSPF_LESS)
                &&
                (UtilIpAddrComp
                 (pInterface->secondaryIP.
                  ifSecIp[i2SecCount].ifSecIpAddr,
                  currSecIpAddr) == OSPF_GREATER))
            {
                IP_ADDR_COPY (nextSecIpAddr,
                              pInterface->secondaryIP.
                              ifSecIp[i2SecCount].ifSecIpAddr);
                IP_ADDR_COPY (nextSecIpAddrMask,
                              pInterface->secondaryIP.
                              ifSecIp[i2SecCount].ifSecIpAddrMask);
                i2SecCount++;
            }                    /* end if */
            else
            {
                i2SecCount++;
            }

        }
        *pu4NextFutOspfPrimIpAddr = OSPF_CRU_BMC_DWFROMPDU
            (pInterface->ifIpAddr);
        *pi4NextFutOspfPrimAddresslessIf = pInterface->u4AddrlessIf;
        *pu4NextFutOspfSecIpAddr = OSPF_CRU_BMC_DWFROMPDU (nextSecIpAddr);
        *pu4NextFutOspfSecIpAddrMask =
            OSPF_CRU_BMC_DWFROMPDU (nextSecIpAddrMask);

        return SNMP_SUCCESS;
    }

    while (nmhGetNextIndexOspfIfTable
           (u4FutOspfPrimIpAddr, pu4NextFutOspfPrimIpAddr,
            i4FutOspfPrimAddresslessIf,
            pi4NextFutOspfPrimAddresslessIf) != SNMP_FAILURE)
    {
        if (GetLeastSecIpAddrInCxt
            (pOspfCxt, *pu4NextFutOspfPrimIpAddr,
             *pi4NextFutOspfPrimAddresslessIf,
             &nextSecIpAddr, &nextSecIpAddrMask) == SUCCESS)
        {
            *pu4NextFutOspfSecIpAddr = OSPF_CRU_BMC_DWFROMPDU (nextSecIpAddr);
            *pu4NextFutOspfSecIpAddrMask =
                OSPF_CRU_BMC_DWFROMPDU (nextSecIpAddrMask);
            return SNMP_SUCCESS;
        }
        u4FutOspfPrimIpAddr = *pu4NextFutOspfPrimIpAddr;
        i4FutOspfPrimAddresslessIf = *pi4NextFutOspfPrimAddresslessIf;
    }
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfSecIfStatus
     Input       :  The Indices
            FutOspfPrimIpAddr
            FutOspfPrimAddresslessIf
            FutOspfSecIpAddr
            FutOspfSecIpAddrMask

            The Object 
            retValFutOspfSecIfStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfSecIfStatus (UINT4 u4FutOspfPrimIpAddr,
                          INT4 i4FutOspfPrimAddresslessIf,
                          UINT4 u4FutOspfSecIpAddr,
                          UINT4 u4FutOspfSecIpAddrMask,
                          INT4 *pi4RetValFutOspfSecIfStatus)
{
    tInterface         *pInterface;
    tIPADDR             primIpAddr;
    tIPADDR             secIpAddr;
    tIPADDRMASK         secIpAddrMask;
    INT2                i2SecCount = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, u4FutOspfPrimIpAddr);
    OSPF_CRU_BMC_DWTOPDU (secIpAddr, u4FutOspfSecIpAddr);

    OSPF_CRU_BMC_DWTOPDU (secIpAddrMask, u4FutOspfSecIpAddrMask);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr,
                                      (UINT4) i4FutOspfPrimAddresslessIf)) !=
        NULL)
    {
        while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
        {
            if (UtilAddrMaskCmpe
                (OSPF_CRU_BMC_DWFROMPDU
                 (pInterface->secondaryIP.
                  ifSecIp[i2SecCount].ifSecIpAddr),
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->
                                         secondaryIP.ifSecIp
                                         [i2SecCount].ifSecIpAddrMask),
                 u4FutOspfSecIpAddr, u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
            {
                *pi4RetValFutOspfSecIfStatus =
                    pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status;
                return SNMP_SUCCESS;
            }
            i2SecCount++;
        }                        /* end while */
    }
    return SNMP_FAILURE;
}

    /* Change in func name */
    /* LOW LEVEL Routines for Table : FutOspfAreaAggregateTable. */

    /****************************************************************************
     Function    :  nmhValidateIndexInstanceFutOspfAreaAggregateTable
     Input       :  The Indices
            FutOspfAreaAggregateAreaID
            FutOspfAreaAggregateLsdbType
            FutOspfAreaAggregateNet
            FutOspfAreaAggregateMask
     Output      :  The Routines Validates the Given Indices.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfAreaAggregateTable (UINT4
                                                   u4FutOspfAreaAggregateAreaID,
                                                   INT4
                                                   i4FutOspfAreaAggregateLsdbType,
                                                   UINT4
                                                   u4FutOspfAreaAggregateNet,
                                                   UINT4
                                                   u4FutOspfAreaAggregateMask)
{

    return
        nmhValidateIndexInstanceOspfAreaAggregateTable
        (u4FutOspfAreaAggregateAreaID, i4FutOspfAreaAggregateLsdbType,
         u4FutOspfAreaAggregateNet, u4FutOspfAreaAggregateMask);
}

    /* Change in func name */

    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfAreaAggregateTable
     Input       :  The Indices
            FutOspfAreaAggregateAreaID
            FutOspfAreaAggregateLsdbType
            FutOspfAreaAggregateNet
            FutOspfAreaAggregateMask
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfAreaAggregateTable (UINT4
                                           *pu4FutOspfAreaAggregateAreaID,
                                           INT4
                                           *pi4FutOspfAreaAggregateLsdbType,
                                           UINT4
                                           *pu4FutOspfAreaAggregateNet,
                                           UINT4 *pu4FutOspfAreaAggregateMask)
{
    return
        nmhGetFirstIndexOspfAreaAggregateTable (pu4FutOspfAreaAggregateAreaID,
                                                pi4FutOspfAreaAggregateLsdbType,
                                                pu4FutOspfAreaAggregateNet,
                                                pu4FutOspfAreaAggregateMask);

}

    /* Change in func name */

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfAreaAggregateTable
     Input       :  The Indices
            FutOspfAreaAggregateAreaID
            nextFutOspfAreaAggregateAreaID
            FutOspfAreaAggregateLsdbType
            nextFutOspfAreaAggregateLsdbType
            FutOspfAreaAggregateNet
            nextFutOspfAreaAggregateNet
            FutOspfAreaAggregateMask
            nextFutOspfAreaAggregateMask
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfAreaAggregateTable (UINT4
                                          u4FutOspfAreaAggregateAreaID,
                                          UINT4
                                          *pu4NextFutOspfAreaAggregateAreaID,
                                          INT4
                                          i4FutOspfAreaAggregateLsdbType,
                                          INT4
                                          *pi4NextFutOspfAreaAggregateLsdbType,
                                          UINT4 u4FutOspfAreaAggregateNet,
                                          UINT4
                                          *pu4NextFutOspfAreaAggregateNet,
                                          UINT4 u4FutOspfAreaAggregateMask,
                                          UINT4
                                          *pu4NextFutOspfAreaAggregateMask)
{

    return nmhGetNextIndexOspfAreaAggregateTable (u4FutOspfAreaAggregateAreaID,
                                                  pu4NextFutOspfAreaAggregateAreaID,
                                                  i4FutOspfAreaAggregateLsdbType,
                                                  pi4NextFutOspfAreaAggregateLsdbType,
                                                  u4FutOspfAreaAggregateNet,
                                                  pu4NextFutOspfAreaAggregateNet,
                                                  u4FutOspfAreaAggregateMask,
                                                  pu4NextFutOspfAreaAggregateMask);

}

    /****************************************************************************
     Function    :  nmhGetFutOspfAreaAggregateExternalTag
     Input       :  The Indices
            FutOspfAreaAggregateAreaID
            FutOspfAreaAggregateLsdbType
            FutOspfAreaAggregateNet
            FutOspfAreaAggregateMask

            The Object 
            retValFutOspfAreaAggregateExternalTag
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfAreaAggregateExternalTag (UINT4 u4FutOspfAreaAggregateAreaID,
                                       INT4 i4FutOspfAreaAggregateLsdbType,
                                       UINT4 u4FutOspfAreaAggregateNet,
                                       UINT4 u4FutOspfAreaAggregateMask,
                                       INT4
                                       *pi4RetValFutOspfAreaAggregateExternalTag)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    INT1                i1Index;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaAggregateAreaID);

    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4FutOspfAreaAggregateNet);

    OSPF_CRU_BMC_DWTOPDU (addrMask, u4FutOspfAreaAggregateMask);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) != NULL)
    {
        if ((GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                   (UINT1)
                                   i4FutOspfAreaAggregateLsdbType,
                                   &i1Index)) != OSPF_FAILURE)
        {
            *pi4RetValFutOspfAreaAggregateExternalTag =
                (INT4) pArea->aAddrRange[i1Index].u4ExtRtTag;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhValidateIndexInstanceFutOspfAsExternalAggregationTable
     Input       :  The Indices
            FutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId

     Output      :  The Routines Validates the Given Indices.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfAsExternalAggregationTable (UINT4
                                                           u4FutOspfAsExternalAggregationNet,
                                                           UINT4
                                                           u4FutOspfAsExternalAggregationMask,
                                                           UINT4
                                                           u4FutOspfAsExternalAggregationAreaId)
{
    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);

    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);

    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);
    if (pAsExtAddrRng == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

    /* Change in func name */

    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfAreaAggregateTable
     Input       :  The Indices
            FutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId

     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfAsExternalAggregationTable (UINT4
                                                   *pu4FutOspfAsExternalAggregationNet,
                                                   UINT4
                                                   *pu4FutOspfAsExternalAggregationMask,
                                                   UINT4
                                                   *pu4FutOspfAsExternalAggregationAreaId)
{

    tAsExtAddrRange    *pAsExtAddrRng = NULL;
    tTMO_SLL_NODE      *pAsExtRngNode = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAsExtRngNode = TMO_SLL_First (&(pOspfCxt->asExtAddrRangeLstInRtr));
    pOspfCxt->pLastExtAgg = NULL;

    if (pAsExtRngNode != NULL)
    {
        pAsExtAddrRng = GET_ASEXTRNG_FROM_RTRLST (pAsExtRngNode);
        *pu4FutOspfAsExternalAggregationNet =
            OSPF_CRU_BMC_DWFROMPDU (pAsExtAddrRng->ipAddr);

        *pu4FutOspfAsExternalAggregationMask =
            OSPF_CRU_BMC_DWFROMPDU (pAsExtAddrRng->ipAddrMask);

        *pu4FutOspfAsExternalAggregationAreaId =
            OSPF_CRU_BMC_DWFROMPDU (pAsExtAddrRng->areaId);
        pOspfCxt->pLastExtAgg = pAsExtRngNode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfAsExternalAggregationTable
     Input       :  The Indices
            FutOspfAsExternalAggregationNet
            nextFutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            nextFutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId
            nextFutOspfAsExternalAggregationAreaId
            
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFutOspfAsExternalAggregationTable
    (UINT4 u4FutOspfAsExternalAggregationNet,
     UINT4 *pu4NextFutOspfAsExternalAggregationNet,
     UINT4 u4FutOspfAsExternalAggregationMask,
     UINT4 *pu4NextFutOspfAsExternalAggregationMask,
     UINT4 u4FutOspfAsExternalAggregationAreaId,
     UINT4 *pu4NextFutOspfAsExternalAggregationAreaId)
{

    tIPADDR             currAsExtNet, nxtAsExtNet;
    tIPADDRMASK         currAsExtMask, nxtAsExtMask;
    tAreaId             currAsExtAreaId, nxtAsExtAreaId;
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tAsExtAddrRange    *pNxtAsExtRng = NULL;
    tTMO_SLL_NODE      *pScanNode = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAsExtNet, u4FutOspfAsExternalAggregationNet);

    OSPF_CRU_BMC_DWTOPDU (currAsExtMask, u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (currAsExtAreaId,
                          u4FutOspfAsExternalAggregationAreaId);

    if (pOspfCxt->pLastExtAgg != NULL)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pOspfCxt->pLastExtAgg);

        if ((UtilIpAddrComp (pScanAsExtRng->ipAddr, currAsExtNet) == OSPF_EQUAL)
            && (UtilIpAddrComp (pScanAsExtRng->ipAddrMask, currAsExtMask) ==
                OSPF_EQUAL)
            && (UtilIpAddrComp (pScanAsExtRng->areaId, currAsExtAreaId) ==
                OSPF_EQUAL))
        {
            pOspfCxt->pLastExtAgg =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&pOspfCxt->
                                                asExtAddrRangeLstInRtr,
                                                &(pScanAsExtRng->
                                                  nextAsExtAddrRngInRtr));
            if (pOspfCxt->pLastExtAgg == NULL)
            {
                return SNMP_FAILURE;
            }
            pNxtAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pOspfCxt->pLastExtAgg);
            IP_ADDR_COPY (nxtAsExtNet, pNxtAsExtRng->ipAddr);
            IP_ADDR_COPY (nxtAsExtMask, pNxtAsExtRng->ipAddrMask);
            IP_ADDR_COPY (nxtAsExtAreaId, pNxtAsExtRng->areaId);
            *pu4NextFutOspfAsExternalAggregationNet =
                OSPF_CRU_BMC_DWFROMPDU (nxtAsExtNet);
            *pu4NextFutOspfAsExternalAggregationMask =
                OSPF_CRU_BMC_DWFROMPDU (nxtAsExtMask);
            *pu4NextFutOspfAsExternalAggregationAreaId =
                OSPF_CRU_BMC_DWFROMPDU (nxtAsExtAreaId);

            return SNMP_SUCCESS;
        }

        pOspfCxt->pLastExtAgg = NULL;
    }
    TMO_SLL_Scan (&(pOspfCxt->asExtAddrRangeLstInRtr), pScanNode,
                  tTMO_SLL_NODE *)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pScanNode);
        if (OspfGetNextAsExternalAggregationIndex (currAsExtNet,
                                                   pScanAsExtRng->ipAddr,
                                                   currAsExtMask,
                                                   pScanAsExtRng->ipAddrMask,
                                                   currAsExtAreaId,
                                                   pScanAsExtRng->areaId) ==
            OSPF_SUCCESS)
        {

            IP_ADDR_COPY (nxtAsExtNet, pScanAsExtRng->ipAddr);
            IP_ADDR_COPY (nxtAsExtMask, pScanAsExtRng->ipAddrMask);
            IP_ADDR_COPY (nxtAsExtAreaId, pScanAsExtRng->areaId);
            *pu4NextFutOspfAsExternalAggregationNet =
                OSPF_CRU_BMC_DWFROMPDU (nxtAsExtNet);
            *pu4NextFutOspfAsExternalAggregationMask =
                OSPF_CRU_BMC_DWFROMPDU (nxtAsExtMask);
            *pu4NextFutOspfAsExternalAggregationAreaId =
                OSPF_CRU_BMC_DWFROMPDU (nxtAsExtAreaId);
            pOspfCxt->pLastExtAgg = pScanNode;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  GetFindAsExtRngInCxt
     Input       :    Utility routine used by other GET functions
            pOspfCxt
            FutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId

     Output      :  None

     Returns     :  As ext Range OR NULL
    ****************************************************************************/
PUBLIC tAsExtAddrRange *
GetFindAsExtRngInCxt (tOspfCxt * pOspfCxt, tIPADDR asExtNet, tIPADDR asExtMask,
                      tAreaId areaId)
{
    tAsExtAddrRange    *pScanAsExtRng;
    tTMO_SLL_NODE      *pScanAsExtNode;

    TMO_SLL_Scan (&(pOspfCxt->asExtAddrRangeLstInRtr),
                  pScanAsExtNode, tTMO_SLL_NODE *)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pScanAsExtNode);
        if ((UtilIpAddrComp (pScanAsExtRng->ipAddr, asExtNet)
             == OSPF_EQUAL) &&
            (UtilIpAddrComp (pScanAsExtRng->ipAddrMask, asExtMask)
             == OSPF_EQUAL) &&
            (UtilIpAddrComp (pScanAsExtRng->areaId, areaId) == OSPF_EQUAL))
            return pScanAsExtRng;
    }
    return NULL;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfAsExternalAggregationEffect
     Input       :  The Indices
            FutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId

            The Object 
            retValFutOspfAsExternalAggregationEffect
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfAsExternalAggregationEffect (UINT4
                                          u4FutOspfAsExternalAggregationNet,
                                          UINT4
                                          u4FutOspfAsExternalAggregationMask,
                                          UINT4
                                          u4FutOspfAsExternalAggregationAreaId,
                                          INT4
                                          *pi4RetValFutOspfAsExternalAggregationEffect)
{

    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tAsExtAddrRange    *pScanAsExtRng = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);
    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);

    if (pOspfCxt->pLastExtAgg != NULL)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pOspfCxt->pLastExtAgg);

        if ((UtilIpAddrComp (pScanAsExtRng->ipAddr, asExtNet) == OSPF_EQUAL) &&
            (UtilIpAddrComp (pScanAsExtRng->ipAddrMask, asExtMask) ==
             OSPF_EQUAL)
            && (UtilIpAddrComp (pScanAsExtRng->areaId, areaId) == OSPF_EQUAL))
        {
            *pi4RetValFutOspfAsExternalAggregationEffect =
                (INT4) pScanAsExtRng->u1RangeEffect;
            return SNMP_SUCCESS;
        }
    }

    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);

    if (pAsExtAddrRng != NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG not existing\n");
        *pi4RetValFutOspfAsExternalAggregationEffect =
            (INT4) pAsExtAddrRng->u1RangeEffect;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfAsExternalAggregationTranslation
     Input       :  The Indices
            FutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId

            The Object 
            retValFutOspfAsExternalAggregationTranslation
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfAsExternalAggregationTranslation (UINT4
                                               u4FutOspfAsExternalAggregationNet,
                                               UINT4
                                               u4FutOspfAsExternalAggregationMask,
                                               UINT4
                                               u4FutOspfAsExternalAggregationAreaId,
                                               INT4
                                               *pi4RetValFutOspfAsExternalAggregationTranslation)
{

    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tAsExtAddrRange    *pScanAsExtRng = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);

    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);

    if (pOspfCxt->pLastExtAgg != NULL)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pOspfCxt->pLastExtAgg);

        if ((UtilIpAddrComp (pScanAsExtRng->ipAddr, asExtNet) == OSPF_EQUAL) &&
            (UtilIpAddrComp (pScanAsExtRng->ipAddrMask, asExtMask) ==
             OSPF_EQUAL)
            && (UtilIpAddrComp (pScanAsExtRng->areaId, areaId) == OSPF_EQUAL))
        {
            *pi4RetValFutOspfAsExternalAggregationTranslation =
                (INT4) pScanAsExtRng->u1AggTranslation;
            return SNMP_SUCCESS;
        }
    }
    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);

    if (pAsExtAddrRng != NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG not existing\n");
        *pi4RetValFutOspfAsExternalAggregationTranslation =
            (INT4) pAsExtAddrRng->u1AggTranslation;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfAsExternalAggregationStatus
     Input       :  The Indices
            FutOspfAsExternalAggregationNet
            FutOspfAsExternalAggregationMask
            FutOspfAsExternalAggregationAreaId

            The Object 
            retValFutOspfAsExternalAggregationStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfAsExternalAggregationStatus (UINT4
                                          u4FutOspfAsExternalAggregationNet,
                                          UINT4
                                          u4FutOspfAsExternalAggregationMask,
                                          UINT4
                                          u4FutOspfAsExternalAggregationAreaId,
                                          INT4
                                          *pi4RetValFutOspfAsExternalAggregationStatus)
{

    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tAsExtAddrRange    *pScanAsExtRng = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);

    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);

    if (pOspfCxt->pLastExtAgg != NULL)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pOspfCxt->pLastExtAgg);

        if ((UtilIpAddrComp (pScanAsExtRng->ipAddr, asExtNet) == OSPF_EQUAL) &&
            (UtilIpAddrComp (pScanAsExtRng->ipAddrMask, asExtMask) ==
             OSPF_EQUAL)
            && (UtilIpAddrComp (pScanAsExtRng->areaId, areaId) == OSPF_EQUAL))
        {
            *pi4RetValFutOspfAsExternalAggregationStatus =
                (INT4) pScanAsExtRng->rangeStatus;
            return SNMP_SUCCESS;
        }
    }
    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);
    if (pAsExtAddrRng != NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG not existing\n");
        *pi4RetValFutOspfAsExternalAggregationStatus =
            (INT4) pAsExtAddrRng->rangeStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetOspfSetTrap
     Input       :  The Indices

            The Object 
            retValOspfSetTrap
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetOspfSetTrap (tSNMP_OCTET_STRING_TYPE * pRetValOspfSetTrap)
{
    UINT1               u1Index;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1Index = 0; u1Index < 4; u1Index++)
    {
        pRetValOspfSetTrap->pu1_OctetList[3 - u1Index] =
            (UINT1) (pOspfCxt->u4TrapControl >> (8 * u1Index)) & (0x000000ff);
    }
    pRetValOspfSetTrap->i4_Length = 4;
    return SNMP_SUCCESS;
}

    /****************************************************************************
     Function    :  nmhGetOspfConfigErrorType
     Input       :  The Indices

            The Object 
            retValOspfConfigErrorType
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetOspfConfigErrorType (INT4 *pi4RetValOspfConfigErrorType)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfConfigErrorType = (UINT4) pOspfCxt->u1CnfgErrType;
    return SNMP_SUCCESS;
}

    /****************************************************************************
     Function    :  nmhGetOspfPacketType
     Input       :  The Indices

            The Object 
            retValOspfPacketType
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetOspfPacketType (INT4 *pi4RetValOspfPacketType)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfPacketType = pOspfCxt->u1PktType;
    return SNMP_SUCCESS;
}

    /****************************************************************************
     Function    :  nmhGetOspfPacketSrc
     Input       :  The Indices

            The Object 
            retValOspfPacketSrc
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetOspfPacketSrc (UINT4 *pu4RetValOspfPacketSrc)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfPacketSrc = OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->pktSrcAddr);
    return SNMP_SUCCESS;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRFC1583Compatibility
     Input       :  The Indices

            The Object
            retValOspfRFC1583Compatibility
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRFC1583Compatibility (INT4 *pi4RetValFutOspfRFC1583Compatibility)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRFC1583Compatibility = pOspfCxt->rfc1583Compatibility;
    return SNMP_SUCCESS;
}

    /* LOW LEVEL Routines for Table : FutOspfIfMD5AuthTable. */

    /****************************************************************************
     Function    :  nmhValidateIndexInstanceFutOspfIfMD5AuthTable
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId
     Output      :  The Routines Validates the Given Indices.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfIfMD5AuthTable (UINT4
                                               u4FutOspfIfMD5AuthIpAddress,
                                               INT4
                                               i4FutOspfIfMD5AuthAddressLessIf,
                                               INT4 i4FutOspfIfMD5AuthKeyId)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfIfMD5AuthKeyId > 255)
        return SNMP_FAILURE;

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if (GetFindIfAuthkeyInfoInCxt (pOspfCxt, ifIpAddr,
                                   (UINT4)
                                   i4FutOspfIfMD5AuthAddressLessIf,
                                   (UINT1) i4FutOspfIfMD5AuthKeyId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                 (UINT4) i4FutOspfIfMD5AuthAddressLessIf);
    if (pInterface == NULL)
        return SNMP_FAILURE;
    if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst))
        < MAX_MD5AUTHKEYS_PER_IF)
    {
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfIfMD5AuthTable
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfIfMD5AuthTable (UINT4 *pu4FutOspfIfMD5AuthIpAddress,
                                       INT4
                                       *pi4FutOspfIfMD5AuthAddressLessIf,
                                       INT4 *pi4FutOspfIfMD5AuthKeyId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode, *p_md5_lst_node;
    tMd5AuthkeyInfo    *pMd5authkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((p_md5_lst_node =
             TMO_SLL_First (&(pInterface->sortMd5authkeyLst))) == NULL)
            continue;
        IP_ADDR_COPY (ifIpAddr, pInterface->ifIpAddr);
        *pu4FutOspfIfMD5AuthIpAddress = OSPF_CRU_BMC_DWFROMPDU (ifIpAddr);
        *pi4FutOspfIfMD5AuthAddressLessIf = pInterface->u4AddrlessIf;
        pMd5authkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
        *pi4FutOspfIfMD5AuthKeyId = (INT4) pMd5authkeyInfo->u1AuthkeyId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfIfMD5AuthTable
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            nextFutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            nextFutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId
            nextFutOspfIfMD5AuthKeyId
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfIfMD5AuthTable (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                      UINT4
                                      *pu4NextFutOspfIfMD5AuthIpAddress,
                                      INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                      INT4
                                      *pi4NextFutOspfIfMD5AuthAddressLessIf,
                                      INT4 i4FutOspfIfMD5AuthKeyId,
                                      INT4 *pi4NextFutOspfIfMD5AuthKeyId)
{
    tIPADDR             currIfIpAddr;
    tIPADDR             nextIfIpAddr;
    INT4                i4AuthkeyId;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pIfLstNode;
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i4AuthkeyId = i4FutOspfIfMD5AuthKeyId;

    OSPF_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfIfMD5AuthIpAddress);

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfLstNode);

        switch (UtilIpAddrComp (pInterface->ifIpAddr, currIfIpAddr))
        {
            case OSPF_LESS:
                continue;

            case OSPF_GREATER:
                if ((pLstNode = TMO_SLL_First
                     (&(pInterface->sortMd5authkeyLst))) == NULL)
                {
                    continue;
                }
                pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
                *pi4NextFutOspfIfMD5AuthKeyId =
                    (INT4) pAuthkeyInfo->u1AuthkeyId;
                IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                *pu4NextFutOspfIfMD5AuthIpAddress =
                    OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                *pi4NextFutOspfIfMD5AuthAddressLessIf =
                    pInterface->u4AddrlessIf;
                return SNMP_SUCCESS;
            case OSPF_EQUAL:
                if (pInterface->u4AddrlessIf <
                    (UINT4) i4FutOspfIfMD5AuthAddressLessIf)
                {
                    continue;
                }
                else
                {
                    TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                                  pLstNode, tTMO_SLL_NODE *)
                    {
                        pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
                        if ((INT4) pAuthkeyInfo->u1AuthkeyId <= i4AuthkeyId)
                        {
                            continue;
                        }
                        else
                        {
                            *pi4NextFutOspfIfMD5AuthKeyId =
                                (INT4) pAuthkeyInfo->u1AuthkeyId;
                            IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                            *pu4NextFutOspfIfMD5AuthIpAddress =
                                OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                            *pi4NextFutOspfIfMD5AuthAddressLessIf =
                                pInterface->u4AddrlessIf;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfIfMD5AuthKey
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId

            The Object 
            retValFutOspfIfMD5AuthKey
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfMD5AuthKey (UINT4 u4FutOspfIfMD5AuthIpAddress,
                           INT4 i4FutOspfIfMD5AuthAddressLessIf,
                           INT4 i4FutOspfIfMD5AuthKeyId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFutOspfIfMD5AuthKey)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        if (MsrGetSaveStatus () == ISS_TRUE)
        {
            AuthKeyCopy (pRetValFutOspfIfMD5AuthKey->pu1_OctetList,
                         pAuthkeyInfo->authKey,
                         STRLEN ((CHR1 *) pAuthkeyInfo->authKey));
            pRetValFutOspfIfMD5AuthKey->i4_Length =
                STRLEN ((CHR1 *) pAuthkeyInfo->authKey);
        }
        else
        {
            pRetValFutOspfIfMD5AuthKey->i4_Length = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfMD5AuthKeyStartAccept
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId

            The Object 
            retValFutOspfIfMD5AuthKeyStartAccept
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfMD5AuthKeyStartAccept (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                      INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                      INT4 i4FutOspfIfMD5AuthKeyId,
                                      INT4
                                      *pi4RetValFutOspfIfMD5AuthKeyStartAccept)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        *pi4RetValFutOspfIfMD5AuthKeyStartAccept =
            (INT4) pAuthkeyInfo->u4KeyStartAccept;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfMD5AuthKeyStartGenerate
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId

            The Object 
            retValFutOspfIfMD5AuthKeyStartGenerate
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfMD5AuthKeyStartGenerate (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                        INT4
                                        i4FutOspfIfMD5AuthAddressLessIf,
                                        INT4 i4FutOspfIfMD5AuthKeyId,
                                        INT4
                                        *pi4RetValFutOspfIfMD5AuthKeyStartGenerate)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        *pi4RetValFutOspfIfMD5AuthKeyStartGenerate
            = pAuthkeyInfo->u4KeyStartGenerate;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfMD5AuthKeyStopGenerate
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId

            The Object 
            retValFutOspfIfMD5AuthKeyStopGenerate
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfMD5AuthKeyStopGenerate (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                       INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                       INT4 i4FutOspfIfMD5AuthKeyId,
                                       INT4
                                       *pi4RetValFutOspfIfMD5AuthKeyStopGenerate)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        *pi4RetValFutOspfIfMD5AuthKeyStopGenerate
            = pAuthkeyInfo->u4KeyStopGenerate;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfMD5AuthKeyStopAccept
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId

            The Object 
            retValFutOspfIfMD5AuthKeyStopAccept
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfMD5AuthKeyStopAccept (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                     INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                     INT4 i4FutOspfIfMD5AuthKeyId,
                                     INT4
                                     *pi4RetValFutOspfIfMD5AuthKeyStopAccept)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        *pi4RetValFutOspfIfMD5AuthKeyStopAccept = pAuthkeyInfo->u4KeyStopAccept;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfMD5AuthKeyStatus
     Input       :  The Indices
            FutOspfIfMD5AuthIpAddress
            FutOspfIfMD5AuthAddressLessIf
            FutOspfIfMD5AuthKeyId

            The Object 
            retValFutOspfIfMD5AuthKeyStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfMD5AuthKeyStatus (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                 INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                 INT4 i4FutOspfIfMD5AuthKeyId,
                                 INT4 *pi4RetValFutOspfIfMD5AuthKeyStatus)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        *pi4RetValFutOspfIfMD5AuthKeyStatus = pAuthkeyInfo->u1AuthkeyStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhValidateIndexInstanceFutOspfIfAuthTable
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId
     Output      :  The Routines Validates the Given Indices.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfIfAuthTable (UINT4
                                            u4FutOspfIfAuthIpAddress,
                                            INT4
                                            i4FutOspfIfAuthAddressLessIf,
                                            INT4 i4FutOspfIfAuthKeyId)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfIfAuthKeyId > 255)
        return SNMP_FAILURE;

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfAuthIpAddress);
    if (GetFindIfAuthkeyInfoInCxt (pOspfCxt, ifIpAddr,
                                   (UINT4)
                                   i4FutOspfIfAuthAddressLessIf,
                                   (UINT1) i4FutOspfIfAuthKeyId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                 (UINT4) i4FutOspfIfAuthAddressLessIf);
    if (pInterface == NULL)
        return SNMP_FAILURE;
    if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst))
        < MAX_MD5AUTHKEYS_PER_IF)
    {
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfIfAuthTable
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfIfAuthTable (UINT4 *pu4FutOspfIfAuthIpAddress,
                                    INT4
                                    *pi4FutOspfIfAuthAddressLessIf,
                                    INT4 *pi4FutOspfIfAuthKeyId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode, *p_md5_lst_node;
    tMd5AuthkeyInfo    *pMd5authkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((p_md5_lst_node =
             TMO_SLL_First (&(pInterface->sortMd5authkeyLst))) == NULL)
            continue;
        IP_ADDR_COPY (ifIpAddr, pInterface->ifIpAddr);
        *pu4FutOspfIfAuthIpAddress = (UINT4) OSPF_CRU_BMC_DWFROMPDU (ifIpAddr);
        *pi4FutOspfIfAuthAddressLessIf = (INT4) pInterface->u4AddrlessIf;
        pMd5authkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
        *pi4FutOspfIfAuthKeyId = (INT4) pMd5authkeyInfo->u1AuthkeyId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfIfAuthTable
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            nextFutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            nextFutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId
            nextFutOspfIfAuthKeyId
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfIfAuthTable (UINT4 u4FutOspfIfAuthIpAddress,
                                   UINT4
                                   *pu4NextFutOspfIfAuthIpAddress,
                                   INT4 i4FutOspfIfAuthAddressLessIf,
                                   INT4
                                   *pi4NextFutOspfIfAuthAddressLessIf,
                                   INT4 i4FutOspfIfAuthKeyId,
                                   INT4 *pi4NextFutOspfIfAuthKeyId)
{
    tIPADDR             currIfIpAddr;
    tIPADDR             nextIfIpAddr;
    UINT1               u1AuthkeyId;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pIfLstNode;
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u1AuthkeyId = (UINT1) i4FutOspfIfAuthKeyId;

    OSPF_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfIfAuthIpAddress);

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfLstNode);

        switch (UtilIpAddrComp (pInterface->ifIpAddr, currIfIpAddr))
        {
            case OSPF_LESS:
                continue;

            case OSPF_GREATER:
                if ((pLstNode = TMO_SLL_First
                     (&(pInterface->sortMd5authkeyLst))) == NULL)
                {
                    continue;
                }
                pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
                *pi4NextFutOspfIfAuthKeyId = (INT4) pAuthkeyInfo->u1AuthkeyId;
                IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                *pu4NextFutOspfIfAuthIpAddress =
                    OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                *pi4NextFutOspfIfAuthAddressLessIf =
                    (INT4) pInterface->u4AddrlessIf;
                return SNMP_SUCCESS;
            case OSPF_EQUAL:
                if (pInterface->u4AddrlessIf <
                    (UINT4) i4FutOspfIfAuthAddressLessIf)
                {
                    continue;
                }
                else
                {
                    TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                                  pLstNode, tTMO_SLL_NODE *)
                    {
                        pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
                        if (pAuthkeyInfo->u1AuthkeyId <= u1AuthkeyId)
                        {
                            continue;
                        }
                        else
                        {
                            *pi4NextFutOspfIfAuthKeyId =
                                (INT4) pAuthkeyInfo->u1AuthkeyId;
                            IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                            *pu4NextFutOspfIfAuthIpAddress =
                                OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                            *pi4NextFutOspfIfAuthAddressLessIf =
                                (INT4) pInterface->u4AddrlessIf;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfIfAuthKey
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId

            The Object 
            retValFutOspfIfAuthKey
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfAuthKey (UINT4 u4FutOspfIfAuthIpAddress,
                        INT4 i4FutOspfIfAuthAddressLessIf,
                        INT4 i4FutOspfIfAuthKeyId,
                        tSNMP_OCTET_STRING_TYPE * pRetValFutOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhGetFutOspfIfMD5AuthKey (u4FutOspfIfAuthIpAddress,
                                          i4FutOspfIfAuthAddressLessIf,
                                          i4FutOspfIfAuthKeyId,
                                          pRetValFutOspfIfAuthKey);
    return i1Return;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfAuthKeyStartAccept
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId

            The Object 
            retValFutOspfIfAuthKeyStartAccept
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfAuthKeyStartAccept (UINT4 u4FutOspfIfAuthIpAddress,
                                   INT4 i4FutOspfIfAuthAddressLessIf,
                                   INT4 i4FutOspfIfAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFutOspfIfAuthKeyStartAccept)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                pi4AuthKeyStartAccept = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfIfMD5AuthKeyStartAccept (u4FutOspfIfAuthIpAddress,
                                              i4FutOspfIfAuthAddressLessIf,
                                              i4FutOspfIfAuthKeyId,
                                              &pi4AuthKeyStartAccept);

    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) pi4AuthKeyStartAccept, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;

        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY ((UINT1 *) pRetValFutOspfIfAuthKeyStartAccept->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfIfAuthKeyStartAccept->i4_Length = i4KeyLen;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfAuthKeyStartGenerate
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId

            The Object 
            retValFutOspfIfAuthKeyStartGenerate
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfAuthKeyStartGenerate (UINT4 u4FutOspfIfAuthIpAddress,
                                     INT4
                                     i4FutOspfIfAuthAddressLessIf,
                                     INT4 i4FutOspfIfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFutOspfIfAuthKeyStartGenerate)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStartGenerate = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfIfMD5AuthKeyStartGenerate (u4FutOspfIfAuthIpAddress,
                                                i4FutOspfIfAuthAddressLessIf,
                                                i4FutOspfIfAuthKeyId,
                                                &i4AuthKeyStartGenerate);

    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStartGenerate, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;
        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfIfAuthKeyStartGenerate->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfIfAuthKeyStartGenerate->i4_Length = i4KeyLen;

        return i1ReturnValue;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfAuthKeyStopGenerate
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId

            The Object 
            retValFutOspfIfAuthKeyStopGenerate
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfAuthKeyStopGenerate (UINT4 u4FutOspfIfAuthIpAddress,
                                    INT4 i4FutOspfIfAuthAddressLessIf,
                                    INT4 i4FutOspfIfAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFutOspfIfAuthKeyStopGenerate)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStopGenerate = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfIfMD5AuthKeyStopGenerate (u4FutOspfIfAuthIpAddress,
                                               i4FutOspfIfAuthAddressLessIf,
                                               i4FutOspfIfAuthKeyId,
                                               &i4AuthKeyStopGenerate);

    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStopGenerate, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;
        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfIfAuthKeyStopGenerate->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfIfAuthKeyStopGenerate->i4_Length = i4KeyLen;

        return i1ReturnValue;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfAuthKeyStopAccept
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId

            The Object 
            retValFutOspfIfAuthKeyStopAccept
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfAuthKeyStopAccept (UINT4 u4FutOspfIfAuthIpAddress,
                                  INT4 i4FutOspfIfAuthAddressLessIf,
                                  INT4 i4FutOspfIfAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFutOspfIfAuthKeyStopAccept)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStopAccept = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfIfMD5AuthKeyStopAccept (u4FutOspfIfAuthIpAddress,
                                             i4FutOspfIfAuthAddressLessIf,
                                             i4FutOspfIfAuthKeyId,
                                             &i4AuthKeyStopAccept);

    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStopAccept, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;
        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfIfAuthKeyStopAccept->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfIfAuthKeyStopAccept->i4_Length = i4KeyLen;

        return i1ReturnValue;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfIfAuthKeyStatus
     Input       :  The Indices
            FutOspfIfAuthIpAddress
            FutOspfIfAuthAddressLessIf
            FutOspfIfAuthKeyId

            The Object 
            retValFutOspfIfAuthKeyStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfIfAuthKeyStatus (UINT4 u4FutOspfIfAuthIpAddress,
                              INT4 i4FutOspfIfAuthAddressLessIf,
                              INT4 i4FutOspfIfAuthKeyId,
                              INT4 *pi4RetValFutOspfIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhGetFutOspfIfMD5AuthKeyStatus (u4FutOspfIfAuthIpAddress,
                                                i4FutOspfIfAuthAddressLessIf,
                                                i4FutOspfIfAuthKeyId,
                                                pi4RetValFutOspfIfAuthKeyStatus);

    return i1Return;
}

    /* Low Level SET Routine for All Objects  */

    /* LOW LEVEL Routines for Table : FutOspfVirtIfMD5AuthTable. */

    /****************************************************************************
     Function    :  nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId
     Output      :  The Routines Validates the Given Indices.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable (UINT4
                                                   u4FutOspfVirtIfMD5AuthAreaId,
                                                   UINT4
                                                   u4FutOspfVirtIfMD5AuthNeighbor,
                                                   INT4
                                                   i4FutOspfVirtIfMD5AuthKeyId)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfVirtIfMD5AuthKeyId > 255)
        return SNMP_FAILURE;

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if (GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                       (UINT1) i4FutOspfVirtIfMD5AuthKeyId)
        != NULL)
    {
        return (SNMP_SUCCESS);
    }
    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
        return SNMP_FAILURE;
    if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst))
        < MAX_MD5AUTHKEYS_PER_IF)
    {
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfVirtIfMD5AuthTable
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfVirtIfMD5AuthTable (UINT4
                                           *pu4FutOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           *pu4FutOspfVirtIfMD5AuthNeighbor,
                                           INT4 *pi4FutOspfVirtIfMD5AuthKeyId)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode, *p_md5_lst_node;
    tMd5AuthkeyInfo    *pMd5authkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((p_md5_lst_node =
             TMO_SLL_First ((tTMO_SLL *) & (pInterface->sortMd5authkeyLst)))
            == NULL)
            continue;
        IP_ADDR_COPY (transitAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (nbrId, pInterface->destRtrId);
        *pu4FutOspfVirtIfMD5AuthAreaId = OSPF_CRU_BMC_DWFROMPDU (transitAreaId);
        *pu4FutOspfVirtIfMD5AuthNeighbor = OSPF_CRU_BMC_DWFROMPDU (nbrId);
        pMd5authkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
        *pi4FutOspfVirtIfMD5AuthKeyId = (INT4) pMd5authkeyInfo->u1AuthkeyId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfVirtIfAuthTable
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfVirtIfAuthTable (UINT4 *pu4FutOspfVirtIfAuthAreaId,
                                        UINT4 *pu4FutOspfVirtIfAuthNeighbor,
                                        INT4 *pi4FutOspfVirtIfAuthKeyId)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode, *p_md5_lst_node;
    tMd5AuthkeyInfo    *pMd5authkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((p_md5_lst_node =
             TMO_SLL_First ((tTMO_SLL *) & (pInterface->sortMd5authkeyLst)))
            == NULL)
            continue;
        IP_ADDR_COPY (transitAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (nbrId, pInterface->destRtrId);
        *pu4FutOspfVirtIfAuthAreaId = OSPF_CRU_BMC_DWFROMPDU (transitAreaId);
        *pu4FutOspfVirtIfAuthNeighbor = OSPF_CRU_BMC_DWFROMPDU (nbrId);
        pMd5authkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
        *pi4FutOspfVirtIfAuthKeyId = (INT4) pMd5authkeyInfo->u1AuthkeyId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfVirtIfMD5AuthTable
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            nextFutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            nextFutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId
            nextFutOspfVirtIfMD5AuthKeyId
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfVirtIfMD5AuthTable (UINT4
                                          u4FutOspfVirtIfMD5AuthAreaId,
                                          UINT4
                                          *pu4NextFutOspfVirtIfMD5AuthAreaId,
                                          UINT4
                                          u4FutOspfVirtIfMD5AuthNeighbor,
                                          UINT4
                                          *pu4NextFutOspfVirtIfMD5AuthNeighbor,
                                          INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                          INT4
                                          *pi4NextFutOspfVirtIfMD5AuthKeyId)
{
    UINT1               u1AuthkeyId;
    tAreaId             currAreaId;
    tRouterId           currNbrId;
    tAreaId             nextAreaId;
    tRouterId           nextNbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode, *p_md5_lst_node;
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (currNbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    u1AuthkeyId = (UINT1) i4FutOspfVirtIfMD5AuthKeyId;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilVirtIfIndComp (pInterface->transitAreaId,
                                   pInterface->destRtrId,
                                   currAreaId, currNbrId))
        {
            case OSPF_LESS:
            {
                continue;
            }
            case OSPF_GREATER:
            {
                if ((p_md5_lst_node = TMO_SLL_First
                     (&(pInterface->sortMd5authkeyLst))) == NULL)
                {
                    continue;
                }
                pAuthkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
                *pi4NextFutOspfVirtIfMD5AuthKeyId =
                    (INT4) pAuthkeyInfo->u1AuthkeyId;
                IP_ADDR_COPY (nextAreaId, pInterface->transitAreaId);
                IP_ADDR_COPY (nextNbrId, pInterface->destRtrId);
                *pu4NextFutOspfVirtIfMD5AuthAreaId =
                    OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                *pu4NextFutOspfVirtIfMD5AuthNeighbor =
                    OSPF_CRU_BMC_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;
            }
            case OSPF_EQUAL:
            {
                TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                              p_md5_lst_node, tTMO_SLL_NODE *)
                {
                    pAuthkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
                    if (pAuthkeyInfo->u1AuthkeyId <= u1AuthkeyId)
                    {
                        continue;
                    }
                    else if (pAuthkeyInfo->u1AuthkeyId > u1AuthkeyId)
                    {
                        *pi4NextFutOspfVirtIfMD5AuthKeyId =
                            (INT4) pAuthkeyInfo->u1AuthkeyId;
                        IP_ADDR_COPY (nextAreaId, pInterface->transitAreaId);
                        IP_ADDR_COPY (nextNbrId, pInterface->destRtrId);
                        *pu4NextFutOspfVirtIfMD5AuthAreaId =
                            OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                        *pu4NextFutOspfVirtIfMD5AuthNeighbor =
                            OSPF_CRU_BMC_DWFROMPDU (nextNbrId);
                        return (SNMP_SUCCESS);
                    }
                }
            }
            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfVirtIfAuthTable
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                nextFutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                nextFutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId
                nextFutOspfVirtIfAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfVirtIfAuthTable (UINT4 u4FutOspfVirtIfAuthAreaId,
                                       UINT4 *pu4NextFutOspfVirtIfAuthAreaId,
                                       UINT4 u4FutOspfVirtIfAuthNeighbor,
                                       UINT4 *pu4NextFutOspfVirtIfAuthNeighbor,
                                       INT4 i4FutOspfVirtIfAuthKeyId,
                                       INT4 *pi4NextFutOspfVirtIfAuthKeyId)
{
    UINT1               u1AuthkeyId;
    tAreaId             currAreaId;
    tRouterId           currNbrId;
    tAreaId             nextAreaId;
    tRouterId           nextNbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode, *p_md5_lst_node;
    tMd5AuthkeyInfo    *pAuthkeyInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4FutOspfVirtIfAuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (currNbrId, u4FutOspfVirtIfAuthNeighbor);
    u1AuthkeyId = (UINT1) i4FutOspfVirtIfAuthKeyId;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilVirtIfIndComp (pInterface->transitAreaId,
                                   pInterface->destRtrId,
                                   currAreaId, currNbrId))
        {
            case OSPF_LESS:
            {
                continue;
            }
            case OSPF_GREATER:
            {
                if ((p_md5_lst_node = TMO_SLL_First
                     (&(pInterface->sortMd5authkeyLst))) == NULL)
                {
                    continue;
                }
                pAuthkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
                *pi4NextFutOspfVirtIfAuthKeyId =
                    (INT4) pAuthkeyInfo->u1AuthkeyId;
                IP_ADDR_COPY (nextAreaId, pInterface->transitAreaId);
                IP_ADDR_COPY (nextNbrId, pInterface->destRtrId);
                *pu4NextFutOspfVirtIfAuthAreaId =
                    OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                *pu4NextFutOspfVirtIfAuthNeighbor =
                    OSPF_CRU_BMC_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;
            }
            case OSPF_EQUAL:
            {
                TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst),
                              p_md5_lst_node, tTMO_SLL_NODE *)
                {
                    pAuthkeyInfo = (tMd5AuthkeyInfo *) p_md5_lst_node;
                    if (pAuthkeyInfo->u1AuthkeyId <= u1AuthkeyId)
                    {
                        continue;
                    }
                    else if (pAuthkeyInfo->u1AuthkeyId > u1AuthkeyId)
                    {
                        *pi4NextFutOspfVirtIfAuthKeyId =
                            (INT4) pAuthkeyInfo->u1AuthkeyId;
                        IP_ADDR_COPY (nextAreaId, pInterface->transitAreaId);
                        IP_ADDR_COPY (nextNbrId, pInterface->destRtrId);
                        *pu4NextFutOspfVirtIfAuthAreaId =
                            OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                        *pu4NextFutOspfVirtIfAuthNeighbor =
                            OSPF_CRU_BMC_DWFROMPDU (nextNbrId);
                        return (SNMP_SUCCESS);
                    }
                }
            }
            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfVirtIfMD5AuthKey
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId

            The Object 
            retValFutOspfVirtIfMD5AuthKey
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfVirtIfMD5AuthKey (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                               UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                               INT4 i4FutOspfVirtIfMD5AuthKeyId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFutOspfVirtIfMD5AuthKey)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo =
         GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                        (UINT1) i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        if (MsrGetSaveStatus () == ISS_TRUE)
        {
            AuthKeyCopy (pRetValFutOspfVirtIfMD5AuthKey->pu1_OctetList,
                         pMd5AuthkeyInfo->authKey,
                         (INT4) STRLEN ((CHR1 *) pMd5AuthkeyInfo->authKey));
            pRetValFutOspfVirtIfMD5AuthKey->i4_Length =
                (INT4) STRLEN ((CHR1 *) pMd5AuthkeyInfo->authKey);
        }
        else
            pRetValFutOspfVirtIfMD5AuthKey->i4_Length = 0;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfVirtIfMD5AuthKeyStartAccept
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId

            The Object 
            retValFutOspfVirtIfMD5AuthKeyStartAccept
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfVirtIfMD5AuthKeyStartAccept (UINT4
                                          u4FutOspfVirtIfMD5AuthAreaId,
                                          UINT4
                                          u4FutOspfVirtIfMD5AuthNeighbor,
                                          INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                          INT4
                                          *pi4RetValFutOspfVirtIfMD5AuthKeyStartAccept)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo =
         GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                        (UINT1) i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        *pi4RetValFutOspfVirtIfMD5AuthKeyStartAccept =
            pMd5AuthkeyInfo->u4KeyStartAccept;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfVirtIfMD5AuthKeyStartGenerate
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId

            The Object 
            retValFutOspfVirtIfMD5AuthKeyStartGenerate
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfVirtIfMD5AuthKeyStartGenerate (UINT4
                                            u4FutOspfVirtIfMD5AuthAreaId,
                                            UINT4
                                            u4FutOspfVirtIfMD5AuthNeighbor,
                                            INT4
                                            i4FutOspfVirtIfMD5AuthKeyId,
                                            INT4
                                            *pi4RetValFutOspfVirtIfMD5AuthKeyStartGenerate)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo =
         GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                        (UINT1) i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        *pi4RetValFutOspfVirtIfMD5AuthKeyStartGenerate =
            pMd5AuthkeyInfo->u4KeyStartGenerate;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfVirtIfMD5AuthKeyStopGenerate
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId

            The Object 
            retValFutOspfVirtIfMD5AuthKeyStopGenerate
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfVirtIfMD5AuthKeyStopGenerate (UINT4
                                           u4FutOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           u4FutOspfVirtIfMD5AuthNeighbor,
                                           INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                           INT4
                                           *pi4RetValFutOspfVirtIfMD5AuthKeyStopGenerate)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo =
         GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                        (UINT1) i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        *pi4RetValFutOspfVirtIfMD5AuthKeyStopGenerate =
            pMd5AuthkeyInfo->u4KeyStopGenerate;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfVirtIfMD5AuthKeyStopAccept
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId

            The Object 
            retValFutOspfVirtIfMD5AuthKeyStopAccept
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfVirtIfMD5AuthKeyStopAccept (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                         UINT4
                                         u4FutOspfVirtIfMD5AuthNeighbor,
                                         INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                         INT4
                                         *pi4RetValFutOspfVirtIfMD5AuthKeyStopAccept)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo =
         GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                        (UINT1) i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        *pi4RetValFutOspfVirtIfMD5AuthKeyStopAccept =
            pMd5AuthkeyInfo->u4KeyStopAccept;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfVirtIfMD5AuthKeyStatus
     Input       :  The Indices
            FutOspfVirtIfMD5AuthAreaId
            FutOspfVirtIfMD5AuthNeighbor
            FutOspfVirtIfMD5AuthKeyId

            The Object 
            retValFutOspfVirtIfMD5AuthKeyStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfVirtIfMD5AuthKeyStatus (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                     UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                                     INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                     INT4
                                     *pi4RetValFutOspfVirtIfMD5AuthKeyStatus)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo =
         GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                        (UINT1) i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        *pi4RetValFutOspfVirtIfMD5AuthKeyStatus =
            pMd5AuthkeyInfo->u1AuthkeyStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

    /* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfAuthKey
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                retValFutOspfVirtIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfAuthKey (UINT4 u4FutOspfVirtIfAuthAreaId,
                            UINT4 u4FutOspfVirtIfAuthNeighbor,
                            INT4 i4FutOspfVirtIfAuthKeyId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFutOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFutOspfVirtIfMD5AuthKey (u4FutOspfVirtIfAuthAreaId,
                                              u4FutOspfVirtIfAuthNeighbor,
                                              i4FutOspfVirtIfAuthKeyId,
                                              pRetValFutOspfVirtIfAuthKey);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                retValFutOspfVirtIfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfAuthKeyStartAccept (UINT4 u4FutOspfVirtIfAuthAreaId,
                                       UINT4 u4FutOspfVirtIfAuthNeighbor,
                                       INT4 i4FutOspfVirtIfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValFutOspfVirtIfAuthKeyStartAccept)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStartAccept = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfVirtIfMD5AuthKeyStartAccept (u4FutOspfVirtIfAuthAreaId,
                                                  u4FutOspfVirtIfAuthNeighbor,
                                                  i4FutOspfVirtIfAuthKeyId,
                                                  &i4AuthKeyStartAccept);
    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStartAccept, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;
        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        /*        OspfPrintKeyTime (pi4AuthKeyStartAccept , au1KeyConstantTime); */
        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfVirtIfAuthKeyStartAccept->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfVirtIfAuthKeyStartAccept->i4_Length = i4KeyLen;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                retValFutOspfVirtIfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfAuthKeyStartGenerate (UINT4 u4FutOspfVirtIfAuthAreaId,
                                         UINT4 u4FutOspfVirtIfAuthNeighbor,
                                         INT4 i4FutOspfVirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pRetValFutOspfVirtIfAuthKeyStartGenerate)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStartGenerate = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfVirtIfMD5AuthKeyStartGenerate (u4FutOspfVirtIfAuthAreaId,
                                                    u4FutOspfVirtIfAuthNeighbor,
                                                    i4FutOspfVirtIfAuthKeyId,
                                                    &i4AuthKeyStartGenerate);
    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStartGenerate, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;
        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        /*        OspfPrintKeyTime (pi4AuthKeyStartAccept , au1KeyConstantTime); */
        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfVirtIfAuthKeyStartGenerate->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfVirtIfAuthKeyStartGenerate->i4_Length = i4KeyLen;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                retValFutOspfVirtIfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfAuthKeyStopGenerate (UINT4 u4FutOspfVirtIfAuthAreaId,
                                        UINT4 u4FutOspfVirtIfAuthNeighbor,
                                        INT4 i4FutOspfVirtIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValFutOspfVirtIfAuthKeyStopGenerate)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStopGenerate = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfVirtIfMD5AuthKeyStopGenerate (u4FutOspfVirtIfAuthAreaId,
                                                   u4FutOspfVirtIfAuthNeighbor,
                                                   i4FutOspfVirtIfAuthKeyId,
                                                   &i4AuthKeyStopGenerate);
    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStopGenerate, &tm);

        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;

        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfVirtIfAuthKeyStopGenerate->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfVirtIfAuthKeyStopGenerate->i4_Length = i4KeyLen;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                retValFutOspfVirtIfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfAuthKeyStopAccept (UINT4 u4FutOspfVirtIfAuthAreaId,
                                      UINT4 u4FutOspfVirtIfAuthNeighbor,
                                      INT4 i4FutOspfVirtIfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pRetValFutOspfVirtIfAuthKeyStopAccept)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    INT4                i4AuthKeyStopAccept = 0;
    INT4                i4KeyLen = 0;
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    i1ReturnValue =
        nmhGetFutOspfVirtIfMD5AuthKeyStopAccept (u4FutOspfVirtIfAuthAreaId,
                                                 u4FutOspfVirtIfAuthNeighbor,
                                                 i4FutOspfVirtIfAuthKeyId,
                                                 &i4AuthKeyStopAccept);
    if (i1ReturnValue == SNMP_SUCCESS)
    {

        UtlGetTimeForSeconds ((UINT4) i4AuthKeyStopAccept, &tm);
        tm.tm_year = tm.tm_year % 10000;
        tm.tm_mon = tm.tm_mon % 100;
        tm.tm_mday = tm.tm_mday % 100;
        tm.tm_hour = tm.tm_hour % 100;
        tm.tm_min = tm.tm_min % 100;
        tm.tm_sec = tm.tm_sec % 100;
        SPRINTF ((CHR1 *) au1KeyConstantTime,
                 "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
                 tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                 tm.tm_sec);

        i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
        MEMCPY (pRetValFutOspfVirtIfAuthKeyStopAccept->pu1_OctetList,
                au1KeyConstantTime, i4KeyLen);
        pRetValFutOspfVirtIfAuthKeyStopAccept->i4_Length = i4KeyLen;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfAuthKeyStatus
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                retValFutOspfVirtIfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfAuthKeyStatus (UINT4 u4FutOspfVirtIfAuthAreaId,
                                  UINT4 u4FutOspfVirtIfAuthNeighbor,
                                  INT4 i4FutOspfVirtIfAuthKeyId,
                                  INT4 *pi4RetValFutOspfVirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFutOspfVirtIfMD5AuthKeyStatus (u4FutOspfVirtIfAuthAreaId,
                                                    u4FutOspfVirtIfAuthNeighbor,
                                                    i4FutOspfVirtIfAuthKeyId,
                                                    pi4RetValFutOspfVirtIfAuthKeyStatus);

    return i1Return;
}

    /* Low Level SET Routine for All Objects  */
    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfRRDRouteConfigTable
     Input       :  The Indices
            FutOspfRRDRouteDest
            FutOspfRRDRouteMask
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfRRDRouteConfigTable (UINT4 *pu4FutOspfRRDRouteDest,
                                            UINT4 *pu4FutOspfRRDRouteMask)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pOspfCxt->pLastConfigRouteInfo = NULL;

    pRrdConfRtInfo =
        (tRedistrConfigRouteInfo *)
        TMO_SLL_First (&(pOspfCxt->RedistrRouteConfigLst));
    if (pRrdConfRtInfo != NULL)
    {
        *pu4FutOspfRRDRouteDest =
            OSPF_CRU_BMC_DWFROMPDU (pRrdConfRtInfo->rrdDestIPAddr);
        *pu4FutOspfRRDRouteMask =
            OSPF_CRU_BMC_DWFROMPDU (pRrdConfRtInfo->rrdDestAddrMask);
        pOspfCxt->pLastConfigRouteInfo = pRrdConfRtInfo;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfRRDRouteConfigTable
     Input       :  The Indices
            FutOspfRRDRouteDest
            nextFutOspfRRDRouteDest
            FutOspfRRDRouteMask
            nextFutOspfRRDRouteMask
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfRRDRouteConfigTable (UINT4 u4FutOspfRRDRouteDest,
                                           UINT4
                                           *pu4NextFutOspfRRDRouteDest,
                                           UINT4 u4FutOspfRRDRouteMask,
                                           UINT4 *pu4NextFutOspfRRDRouteMask)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);

    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if (pOspfCxt->pLastConfigRouteInfo != NULL)
    {
        for (;;)
        {
            pOspfCxt->pLastConfigRouteInfo =
                (tRedistrConfigRouteInfo *) TMO_SLL_Next (&pOspfCxt->
                                                          RedistrRouteConfigLst,
                                                          &(pOspfCxt->
                                                            pLastConfigRouteInfo->
                                                            nextRrdConfigRoute));

            if (pOspfCxt->pLastConfigRouteInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            switch (UtilIpAddrComp
                    (pOspfCxt->pLastConfigRouteInfo->rrdDestIPAddr,
                     RrdDestIPAddr))
            {
                case OSPF_LESS:
                    continue;
                case OSPF_EQUAL:
                    switch (UtilIpAddrComp
                            (pOspfCxt->pLastConfigRouteInfo->rrdDestAddrMask,
                             RrdDestAddrMask))
                    {
                        case OSPF_EQUAL:
                            continue;
                        case OSPF_LESS:
                            continue;
                        case OSPF_GREATER:;
                            /* fall through */
                    }
                case OSPF_GREATER:
                    *pu4NextFutOspfRRDRouteDest =
                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->pLastConfigRouteInfo->
                                                rrdDestIPAddr);
                    *pu4NextFutOspfRRDRouteMask =
                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->pLastConfigRouteInfo->
                                                rrdDestAddrMask);
                    return SNMP_SUCCESS;
                default:
                    break;
            }
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->RedistrRouteConfigLst), pRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {
        pOspfCxt->pLastConfigRouteInfo = pRrdConfRtInfo;
        switch (UtilIpAddrComp (pRrdConfRtInfo->rrdDestIPAddr, RrdDestIPAddr))
        {
            case OSPF_LESS:
                continue;
            case OSPF_EQUAL:
                switch (UtilIpAddrComp
                        (pRrdConfRtInfo->rrdDestAddrMask, RrdDestAddrMask))
                {
                    case OSPF_EQUAL:
                        continue;
                    case OSPF_LESS:
                        continue;
                    case OSPF_GREATER:;
                        /* fall through */
                    default:
                        break;

                }
            case OSPF_GREATER:
                *pu4NextFutOspfRRDRouteDest =
                    OSPF_CRU_BMC_DWFROMPDU (pRrdConfRtInfo->rrdDestIPAddr);
                *pu4NextFutOspfRRDRouteMask =
                    OSPF_CRU_BMC_DWFROMPDU (pRrdConfRtInfo->rrdDestAddrMask);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }
    pOspfCxt->pLastConfigRouteInfo = NULL;
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDRouteMetric
     Input       :  The Indices
            FutOspfRRDRouteDest
            FutOspfRRDRouteMask

            The Object 
            retValFutOspfRRDRouteMetric
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDRouteMetric (UINT4 u4FutOspfRRDRouteDest,
                             UINT4 u4FutOspfRRDRouteMask,
                             INT4 *pi4RetValFutOspfRRDRouteMetric)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);

    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        *pi4RetValFutOspfRRDRouteMetric = pRrdConfRtInfo->u4RrdRouteMetricValue;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDRouteMetricType
     Input       :  The Indices
            FutOspfRRDRouteDest
            FutOspfRRDRouteMask

            The Object 
            retValFutOspfRRDRouteMetricType
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDRouteMetricType (UINT4 u4FutOspfRRDRouteDest,
                                 UINT4 u4FutOspfRRDRouteMask,
                                 INT4 *pi4RetValFutOspfRRDRouteMetricType)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);

    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        *pi4RetValFutOspfRRDRouteMetricType =
            pRrdConfRtInfo->u1RrdRouteMetricType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDRouteTagType
     Input       :  The Indices
            FutOspfRRDRouteDest
            FutOspfRRDRouteMask
     
            The Object 
            retValFutOspfRRDRouteTagType
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDRouteTagType (UINT4 u4FutOspfRRDRouteDest,
                              UINT4 u4FutOspfRRDRouteMask,
                              INT4 *pi4RetValFutOspfRRDRouteTagType)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);

    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        *pi4RetValFutOspfRRDRouteTagType =
            (INT4) pRrdConfRtInfo->u1RedistrTagType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDRouteTag
     Input       :  The Indices

            The Object 
            retValFutOspfRRDRouteTag
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDRouteTag (UINT4 u4FutOspfRRDRouteDest,
                          UINT4 u4FutOspfRRDRouteMask,
                          UINT4 *pu4RetValFutOspfRRDRouteTag)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);

    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        *pu4RetValFutOspfRRDRouteTag = pRrdConfRtInfo->u4ManualTagValue;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDRouteStatus
     Input       :  The Indices
            FutOspfRRDRouteDest
            FutOspfRRDRouteMask

            The Object 
            retValFutOspfRRDRouteStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDRouteStatus (UINT4 u4FutOspfRRDRouteDest,
                             UINT4 u4FutOspfRRDRouteMask,
                             INT4 *pi4RetValFutOspfRRDRouteStatus)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);

    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        *pi4RetValFutOspfRRDRouteStatus = pRrdConfRtInfo->rrdRtInfoStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

    /*****************************************************************************/
    /*                                                                           */
    /* Function        : GetFindRrdConfRtInfoInCxt                               */
    /*                                                                           */
    /* Description     : This procedure finds the matching entry from            */
    /*                   RedistrRouteConfigLst SLL                               */
    /*                                                                           */
    /* Input           : pRrdDestIPAddr    - Dest IP Address                     */
    /*                   pRrdDestAddrMask  - Dest IP Addr Mask                   */
    /*                                                                           */
    /* Output          : None                                                    */
    /*                                                                           */
    /* Returns         : Pointer to the matching entry if present                */
    /*                   NULL, otherwise                                         */
    /*                                                                           */
    /*****************************************************************************/

PUBLIC tRedistrConfigRouteInfo *
GetFindRrdConfRtInfoInCxt (tOspfCxt * pOspfCxt, tIPADDR * pRrdDestIPAddr,
                           tIPADDRMASK * pRrdDestAddrMask)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pLastOspfCxt = gOsRtr.pOspfCxt;

    if (pLastOspfCxt != NULL && pLastOspfCxt->pLastConfigRouteInfo != NULL)
    {
        pRrdConfRtInfo = pLastOspfCxt->pLastConfigRouteInfo;
        if ((UtilIpAddrComp (*pRrdDestIPAddr,
                             pRrdConfRtInfo->rrdDestIPAddr) == OSPF_EQUAL) &&
            (UtilIpAddrComp (*pRrdDestAddrMask,
                             pRrdConfRtInfo->rrdDestAddrMask) == OSPF_EQUAL))
        {
            return pRrdConfRtInfo;
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->RedistrRouteConfigLst), pRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {
        if ((UtilIpAddrComp (*pRrdDestIPAddr,
                             pRrdConfRtInfo->rrdDestIPAddr) == OSPF_EQUAL) &&
            (UtilIpAddrComp (*pRrdDestAddrMask,
                             pRrdConfRtInfo->rrdDestAddrMask) == OSPF_EQUAL))
        {
            return pRrdConfRtInfo;
        }
    }

    return NULL;
}

    /*****************************************************************************/
    /*                                                                           */
    /* Function        : FindBestRrdConfRtInfo                                   */
    /*                                                                           */
    /* Description     : This procedure scans the RedistrRouteConfigLst list and */
    /*                   returns the entry that subsumes (or is equal to) the    */
    /*                   one received from RTM                                   */
    /*                   It is assumed that the masks are contiguous             */
    /*                                                                           */
    /* Input           : pOspfCxt   - pointer to tOspfCxt structure              */
    /*                   pDestId    - pointer to DestId                          */
    /*                   pDestMask  - pointer to DestMask                        */
    /*                                                                           */
    /* Output          : None                                                    */
    /*                                                                           */
    /* Returns         : Pointer to the matching entry, if present               */
    /*                   NULL, otherwise                                         */
    /*                                                                           */
    /*****************************************************************************/

PUBLIC tRedistrConfigRouteInfo *
FindBestRrdConfRtInfoInCxt (tOspfCxt * pOspfCxt, tIPADDR * pDestId,
                            tIPADDRMASK * pDestMask)
{
    tRedistrConfigRouteInfo *pLstRrdConfRtInfo;
    tRedistrConfigRouteInfo *pBestRrdConfRtInfo = NULL;

    /* Finding the most specific config record */
    TMO_SLL_Scan (&(pOspfCxt->RedistrRouteConfigLst), pLstRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {
        /* If the config record is not ACTIVE, it can not be 
         * best coonfig record. */
        if (pLstRrdConfRtInfo->rrdRtInfoStatus != ACTIVE)
        {
            continue;
        }
        /* If the config record is not ACTIVE, it can not be 
         * best coonfig record. */
        if (UtilIpAddrComp (*pDestMask,
                            pLstRrdConfRtInfo->rrdDestAddrMask) == OSPF_LESS)
        {
            continue;
        }
        if (UtilIpAddrMaskComp (*pDestId,
                                pLstRrdConfRtInfo->rrdDestIPAddr,
                                pLstRrdConfRtInfo->rrdDestAddrMask) ==
            OSPF_EQUAL)
        {
            pBestRrdConfRtInfo = pLstRrdConfRtInfo;
        }
    }
    return pBestRrdConfRtInfo;
}

    /*****************************************************************************/
    /*                                                                           */
    /* Function        : FindNextBestRrdConfRtInfoInCxt                          */
    /*                                                                           */
    /* Description     : This procedure scans the RedistrRouteConfigLst list and */
    /*                   returns the entry that is the next best match for the   */
    /*                   given destination network.                              */
    /*                                                                           */
    /* Input           : pOspfCxt       - pointer to tOspfCxt structure          */
    /*                   pDestId        - pointer to DestId                      */
    /*                   pDestMask      - pointer to DestMask                    */
    /*                   pRrdConfRtInfo - pointer to best config record          */
    /*                                                                           */
    /* Output          : None                                                    */
    /*                                                                           */
    /* Returns         : Pointer to the next best config entry, if present       */
    /*                   NULL, otherwise                                         */
    /*                                                                           */
    /*****************************************************************************/

PUBLIC tRedistrConfigRouteInfo *
FindNextBestRrdConfRtInfoInCxt (tOspfCxt * pOspfCxt,
                                tIPADDR * pDestId, tIPADDRMASK * pDestMask,
                                tRedistrConfigRouteInfo * pRrdConfRtInfo)
{
    tRedistrConfigRouteInfo *pLstRrdConfRtInfo;
    tRedistrConfigRouteInfo *pNextBestRrdConfRtInfo = NULL;

    TMO_SLL_Scan (&(pOspfCxt->RedistrRouteConfigLst), pLstRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {
        /* Best Config record should not be considered for 
         * finding next best config record */
        if (pLstRrdConfRtInfo == pRrdConfRtInfo)
        {
            continue;
        }
        /* If the config record is not ACTIVE, it can not be 
         * best coonfig record. */
        if (pLstRrdConfRtInfo->rrdRtInfoStatus != ACTIVE)
        {
            continue;
        }
        /* If Config record mask is less than the destination 
         * route mask, it can not be the best config record */
        if (UtilIpAddrComp (*pDestMask,
                            pLstRrdConfRtInfo->rrdDestAddrMask) == OSPF_LESS)
        {
            continue;
        }
        if (UtilIpAddrMaskComp (*pDestId,
                                pLstRrdConfRtInfo->rrdDestIPAddr,
                                pLstRrdConfRtInfo->rrdDestAddrMask) ==
            OSPF_EQUAL)
        {
            pNextBestRrdConfRtInfo = pLstRrdConfRtInfo;
        }
    }
    return pNextBestRrdConfRtInfo;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDStatus
     Input       :  The Indices

            The Object 
            retValFutOspfRRDStatus
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDStatus (INT4 *pi4RetValFutOspfRRDStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRRDStatus = (INT4) pOspfCxt->redistrAdmnStatus;

    return SNMP_SUCCESS;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDSrcProtoMaskEnable
     Input       :  The Indices

            The Object 
            retValFutOspfRRDSrcProtoMaskEnable
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDSrcProtoMaskEnable (INT4 *pi4RetValFutOspfRRDSrcProtoMaskEnable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRRDSrcProtoMaskEnable =
        (INT4) pOspfCxt->u4RrdSrcProtoBitMask;

    return SNMP_SUCCESS;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfRRDSrcProtoMaskDisable
     Input       :  The Indices

            The Object 
            retValFutOspfRRDSrcProtoMaskDisable
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfRRDSrcProtoMaskDisable (INT4
                                     *pi4RetValFutOspfRRDSrcProtoMaskDisable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfRRDSrcProtoMaskDisable =
        (INT4) (~(pOspfCxt->u4RrdSrcProtoBitMask) & (RRD_SRC_PROTO_BIT_MASK));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfRRDRouteMapEnable
 Input       :  The Indices

                The Object 
                retValFutOspfRRDRouteMapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfRRDRouteMapEnable (tSNMP_OCTET_STRING_TYPE
                                * pRetValFutOspfRRDRouteMapEnable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFutOspfRRDRouteMapEnable->i4_Length = STRLEN (pOspfCxt->au1RMapName);

    MEMCPY ((UINT1 *) pRetValFutOspfRRDRouteMapEnable->pu1_OctetList,
            (UINT1 *) pOspfCxt->au1RMapName,
            pRetValFutOspfRRDRouteMapEnable->i4_Length);

    return SNMP_SUCCESS;
}

    /***************************************************************************/
    /* Function    :  OspfRtTblGetObjectInCxt                                  */
    /*                                                                         */
    /* Description :  Find the Route Entry in the Routing table for the        */
    /*                given indices                                            */
    /*                                                                         */
    /* Input       :  pOspfCxt : pointer to tOspfCxt structure                 */
    /*                u4Dest : Destination Route                               */
    /*                u4Mask : Mask of the route entry.                        */
    /*                i4Tos  : Tos Value                                       */
    /*                u4NextHop : Next Hop Value                               */
    /*                pu4Val    : Pointet to the variable                      */
    /*                u1ObjType  : Object Identifier.                          */
    /*                u1DestType : Object Identifier.                          */
    /* Output      :  None                                                     */
    /*                                                                         */
    /* Returns     :  SNMP_SUCCESS or SNMP_FAILURE                             */
    /***************************************************************************/

PRIVATE INT1
OspfRtTblGetObjectInCxt (tOspfCxt * pOspfCxt, UINT4 u4Dest, UINT4 u4Mask,
                         INT4 i4Tos, UINT4 u4NextHop, UINT4 *pu4Val,
                         UINT1 u1ObjType, UINT1 u1DestType)
{
    tRtEntry           *pRtEntry = NULL;
    tIPADDR             destIpAddr;
    tIPADDR             destIpAddrMask;

    OSPF_CRU_BMC_DWTOPDU (destIpAddr, u4Dest);
    OSPF_CRU_BMC_DWTOPDU (destIpAddrMask, u4Mask);

    if ((pRtEntry =
         RtcFindRtEntry (pOspfCxt->pOspfRt, &destIpAddr, &destIpAddrMask,
                         u1DestType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u1ObjType)
    {
        case OSPF_ROUTE_RTTYPE:
            OspfGetRTAttribute (pRtEntry, u4NextHop, OSPF_ROUTE_RTTYPE,
                                pu4Val, i4Tos);
            break;

        case OSPF_ROUTE_AREAID:
            OspfGetRTAttribute (pRtEntry, u4NextHop, OSPF_ROUTE_AREAID,
                                pu4Val, i4Tos);
            break;

        case OSPF_ROUTE_COST:
            OspfGetRTAttribute (pRtEntry, u4NextHop, OSPF_ROUTE_COST,
                                pu4Val, i4Tos);
            break;

        case OSPF_ROUTE_TYPE2COST:
            OspfGetRTAttribute (pRtEntry,
                                u4NextHop, OSPF_ROUTE_TYPE2COST, pu4Val, i4Tos);
            break;

        case OSPF_ROUTE_IFACEINDEX:
            OspfGetRTAttribute (pRtEntry,
                                u4NextHop, OSPF_ROUTE_IFACEINDEX,
                                pu4Val, i4Tos);
            break;

        default:
            return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

    /***************************************************************************/
    /* Function    :  OspfGetRTAttribute                                       */
    /*                                                                         */
    /* Description :  Find the attribute value for the given attribute in the  */
    /*                Routing table                                            */
    /*                given indices                                            */
    /*                                                                         */
    /* Input       :  pRtEnrty : Route Entry                                   */
    /*                u4NextHop : Next Hop Value                               */
    /*                u1ObjType  : Object Identifier.                          */
    /*                pu4ObjVal : Object Value                                 */
    /* Output      :  None                                                     */
    /*                                                                         */
    /* Returns     :  None                                                     */
    /***************************************************************************/
VOID
OspfGetRTAttribute (tRtEntry * pRtEntry,
                    UINT4 u4NextHop, UINT1 u1ObjType, UINT4 *pu4ObjVal,
                    INT4 i4Tos)
{
    tPath              *pPath;
    INT2                i2NextHopCount = 0;
    tIPADDR             nextHopIpAddr;

    OSPF_CRU_BMC_DWTOPDU (nextHopIpAddr, u4NextHop);
    *pu4ObjVal = (UINT4) OSPF_ZERO;

    TMO_SLL_Scan (&(pRtEntry->aTosPath[DECODE_TOS (i4Tos)]), pPath, tPath *)
    {
        i2NextHopCount = 0;
        while (i2NextHopCount < (pPath->u1HopCount))
        {
            if (i2NextHopCount < MAX_NEXT_HOPS)
            {
                if (UtilIpAddrComp (nextHopIpAddr,
                                    pPath->aNextHops[i2NextHopCount].
                                    nbrIpAddr) == OSPF_EQUAL)
                {
                    switch (u1ObjType)
                    {
                        case OSPF_ROUTE_RTTYPE:
                            *pu4ObjVal = (UINT4) (pPath->u1PathType);
                            break;

                        case OSPF_ROUTE_AREAID:
                            *pu4ObjVal = OSPF_CRU_BMC_DWFROMPDU (pPath->areaId);
                            break;

                        case OSPF_ROUTE_COST:
                            *pu4ObjVal = pPath->u4Cost;
                            break;

                        case OSPF_ROUTE_TYPE2COST:
                            *pu4ObjVal = pPath->u4Type2Cost;
                            break;

                        case OSPF_ROUTE_IFACEINDEX:
                            *pu4ObjVal =
                                pPath->aNextHops[i2NextHopCount].u4IfIndex;
                            break;

                        default:
                            break;

                    }
                }
            }
            i2NextHopCount++;
        }
    }
}

    /*****************************************************************************/
    /* Function     : GetLeastNextHopIpAddr                                      */
    /* Description  : The routine returns the least of the next hop IP address   */
    /*                contained in an array corresponding to the given path      */
    /* Input        : pRtEntry                                                   */
    /*                pLeastNextHopIpAddr                                        */
    /* Output       : None                                                       */
    /* Returns      : SUCCESS, if least next hop IP address is found             */
    /*                FAILURE, otherwise                                         */
    /*****************************************************************************/
INT1
GetLeastNextHopIpAddr (tRtEntry * pRtEntry, tIPADDR * pLeastNextHopIpAddr,
                       UINT1 u1Tos)
{
    INT2                i2NextHopCount = 0;
    tPath              *pPath = NULL;

    IP_ADDR_COPY (*pLeastNextHopIpAddr, gHostRouteMask);

    TMO_SLL_Scan (&(pRtEntry->aTosPath[u1Tos]), pPath, tPath *)
    {
        i2NextHopCount = 0;
        while (i2NextHopCount < pPath->u1HopCount)
        {
            if (i2NextHopCount < MAX_NEXT_HOPS)
            {
                if (UtilIpAddrComp
                    (pPath->aNextHops[i2NextHopCount].nbrIpAddr,
                     gNullIpAddr) == OSPF_EQUAL)
                {
                    IP_ADDR_COPY (*pLeastNextHopIpAddr, gNullIpAddr);
                    return OSPF_SUCCESS;
                }
                if (UtilIpAddrComp
                    (pPath->aNextHops[i2NextHopCount].nbrIpAddr,
                     *pLeastNextHopIpAddr) == OSPF_LESS)
                {
                    IP_ADDR_COPY (*pLeastNextHopIpAddr,
                                  pPath->aNextHops[i2NextHopCount].nbrIpAddr);
                }
            }
            i2NextHopCount++;
        }
    }
    if (UtilIpAddrComp (*pLeastNextHopIpAddr, gHostRouteMask) != OSPF_EQUAL)
    {
        return OSPF_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

    /*****************************************************************************/
    /* Function     : GetNextLeastNextHopIpAddr                                  */
    /* Description  : The routine returns the next least of the next hop         */
    /*                IP address                                                 */
    /*                contained in an array corresponding to the given path      */
    /* Input        : pPath                                                      */
    /*                pNextLeastNextHopIpAddr                                    */
    /* Output       : None                                                       */
    /* Returns      : SUCCESS, if least next hop IP address is found             */
    /*                FAILURE, otherwise                                         */
    /*****************************************************************************/

INT1
GetNextLeastNextHopIpAddr (tRtEntry * pRtEntry, tIPADDR * pNextHopIpAddr,
                           tIPADDR * pNextLeastNextHopIpAddr, UINT1 u1Tos)
{
    INT2                i2NextHopCount = 0;
    tIPADDR             tempNextHopIpAddr;
    tTRUTHVALUE         bNextHopFound = OSPF_FALSE;
    tPath              *pPath = NULL;

    IP_ADDR_COPY (tempNextHopIpAddr, gHostRouteMask);

    TMO_SLL_Scan (&(pRtEntry->aTosPath[u1Tos]), pPath, tPath *)
    {
        i2NextHopCount = 0;
        while (i2NextHopCount < pPath->u1HopCount)
        {
            if (i2NextHopCount < MAX_NEXT_HOPS)
            {
                if (((UtilIpAddrComp
                      (pPath->aNextHops[i2NextHopCount].nbrIpAddr,
                       *pNextHopIpAddr) == OSPF_GREATER)
                     &&
                     (((UtilIpAddrComp
                        (pPath->aNextHops[i2NextHopCount].nbrIpAddr,
                         tempNextHopIpAddr)) == OSPF_LESS))))
                {
                    IP_ADDR_COPY (tempNextHopIpAddr,
                                  pPath->aNextHops[i2NextHopCount].nbrIpAddr);
                    bNextHopFound = OSPF_TRUE;
                }
            }
            i2NextHopCount++;
        }
    }

    if (bNextHopFound == OSPF_TRUE)
    {
        IP_ADDR_COPY (*pNextLeastNextHopIpAddr, tempNextHopIpAddr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

    /* LOW LEVEL Routines for Table : FutOspfBRRouteTable. */
    /****************************************************************************
     Function    :  nmhGetFirstIndexFutOspfBRRouteTable
     Input       :  The Indices
            FutOspfBRRouteIpAddr
            FutOspfBRRouteIpAddrMask
            FutOspfBRRouteIpTos
            FutOspfBRRouteIpNextHop
            FutOspfBRRouteDestType
     Output      :  The Get First Routines gets the Lexicographicaly
            First Entry from the Table.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfBRRouteTable (UINT4 *pu4FutOspfBRRouteIpAddr,
                                     UINT4 *pu4FutOspfBRRouteIpAddrMask,
                                     UINT4 *pu4FutOspfBRRouteIpTos,
                                     UINT4 *pu4FutOspfBRRouteIpNextHop,
                                     INT4 *pi4FutOspfBRRouteDestType)
{
    tRtEntry           *pRtEntry;
    tIPADDR             leastNextHopIpAddr;
    UINT1               u1Tos;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    IP_ADDR_COPY (leastNextHopIpAddr, gNullIpAddr);
    if ((pRtEntry =
         (tRtEntry *) TMO_SLL_First (&(pOspfCxt->pOspfRt->routesList))) != NULL)
    {
        if (IS_DEST_NETWORK (pRtEntry))
        {
            return SNMP_FAILURE;
        }
        else
        {
            for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
                {
                    *pu4FutOspfBRRouteIpAddr =
                        OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                    *pu4FutOspfBRRouteIpAddrMask = pRtEntry->u4IpAddrMask;
                    *pu4FutOspfBRRouteIpTos = ENCODE_TOS (u1Tos);
                    *pi4FutOspfBRRouteDestType = pRtEntry->u1DestType;
                    GetLeastNextHopIpAddr (pRtEntry, &leastNextHopIpAddr,
                                           u1Tos);
                    *pu4FutOspfBRRouteIpNextHop =
                        OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

    /****************************************************************************
     Function    :  nmhGetNextIndexFutOspfBRRouteTable
     Input       :  The Indices
            FutOspfBRRouteIpAddr
            nextFutOspfBRRouteIpAddr
            FutOspfBRRouteIpAddrMask
            nextFutOspfBRRouteIpAddrMask
            FutOspfBRRouteIpTos
            nextFutOspfBRRouteIpTos
            FutOspfBRRouteIpNextHop
            nextFutOspfBRRouteIpNextHop
            FutOspfBRRouteDestType
            nextFutOspfBRRouteDestType
     Output      :  The Get Next function gets the Next Index for
            the Index Value given in the Index Values. The
            Indices are stored in the next_varname variables.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
    /* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfBRRouteTable (UINT4 u4FutOspfBRRouteIpAddr,
                                    UINT4 *pu4NextFutOspfBRRouteIpAddr,
                                    UINT4 u4FutOspfBRRouteIpAddrMask,
                                    UINT4 *pu4NextFutOspfBRRouteIpAddrMask,
                                    UINT4 u4FutOspfBRRouteIpTos,
                                    UINT4 *pu4NextFutOspfBRRouteIpTos,
                                    UINT4 u4FutOspfBRRouteIpNextHop,
                                    UINT4 *pu4NextFutOspfBRRouteIpNextHop,
                                    INT4 i4FutOspfBRRouteDestType,
                                    INT4 *pi4NextFutOspfBRRouteDestType)
{
    tRtEntry           *pRtEntry;
    tIPADDR             leastNextHopIpAddr;
    tIPADDR             nextLeastNextHopIpAddr;
    tIPADDR             destIpAddr;
    tIPADDR             destIpAddrMask;
    tIPADDR             nextHopIpAddr;
    UINT1               u1Tos;
    UINT1               u1Found = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    IP_ADDR_COPY (nextLeastNextHopIpAddr, gNullIpAddr);
    IP_ADDR_COPY (leastNextHopIpAddr, gNullIpAddr);
    OSPF_CRU_BMC_DWTOPDU (destIpAddr, u4FutOspfBRRouteIpAddr);

    OSPF_CRU_BMC_DWTOPDU (destIpAddrMask, u4FutOspfBRRouteIpAddrMask);

    OSPF_CRU_BMC_DWTOPDU (nextHopIpAddr, u4FutOspfBRRouteIpNextHop);

    TMO_SLL_Scan (&(pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        if (IS_DEST_NETWORK (pRtEntry))
        {
            return SNMP_FAILURE;
        }
        else
        {
            switch (UtilIpAddrComp (pRtEntry->destId, destIpAddr))
            {
                case OSPF_LESS:
                {
                    continue;
                }
                case OSPF_EQUAL:
                    if (pRtEntry->u4IpAddrMask < u4FutOspfBRRouteIpAddrMask)
                    {
                        continue;
                    }
                    else if (pRtEntry->u4IpAddrMask ==
                             u4FutOspfBRRouteIpAddrMask)
                    {
                        for (u1Tos = (UINT1) DECODE_TOS (u4FutOspfBRRouteIpTos);
                             u1Tos < OSPF_MAX_METRIC; u1Tos++)
                        {
                            if (u1Tos == DECODE_TOS (u4FutOspfBRRouteIpTos))
                            {
                                if (TMO_SLL_Count (&(pRtEntry->
                                                     aTosPath[u1Tos])) != 0)
                                {
                                    if (OspfFindNextHopIpAddr (pRtEntry,
                                                               &nextHopIpAddr,
                                                               u1Tos) ==
                                        OSPF_SUCCESS)
                                    {
                                        if ((pRtEntry->u1DestType <=
                                             i4FutOspfBRRouteDestType))
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            IP_ADDR_COPY
                                                (nextLeastNextHopIpAddr,
                                                 nextHopIpAddr);
                                            u1Found = OSPF_TRUE;
                                        }
                                    }

                                    if (GetNextLeastNextHopIpAddr (pRtEntry,
                                                                   &nextHopIpAddr,
                                                                   &nextLeastNextHopIpAddr,
                                                                   u1Tos) !=
                                        SNMP_SUCCESS)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        u1Found = OSPF_TRUE;
                                    }
                                    *pu4NextFutOspfBRRouteIpAddr =
                                        OSPF_CRU_BMC_DWFROMPDU
                                        (pRtEntry->destId);
                                    *pu4NextFutOspfBRRouteIpAddrMask =
                                        pRtEntry->u4IpAddrMask;
                                    *pu4NextFutOspfBRRouteIpTos =
                                        ENCODE_TOS (u1Tos);
                                    *pu4NextFutOspfBRRouteIpNextHop =
                                        OSPF_CRU_BMC_DWFROMPDU
                                        (nextLeastNextHopIpAddr);
                                    *pi4NextFutOspfBRRouteDestType =
                                        pRtEntry->u1DestType;
                                    return SNMP_SUCCESS;

                                }
                            }
                            else if ((u1Tos > DECODE_TOS
                                      (u4FutOspfBRRouteIpTos)) &&
                                     (TMO_SLL_Count (&(pRtEntry->
                                                       aTosPath[u1Tos])) != 0)
                                     && (pRtEntry->u1DestType >=
                                         i4FutOspfBRRouteDestType))
                            {
                                *pu4NextFutOspfBRRouteIpAddr =
                                    OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                                *pu4NextFutOspfBRRouteIpAddrMask =
                                    pRtEntry->u4IpAddrMask;
                                *pu4NextFutOspfBRRouteIpTos =
                                    ENCODE_TOS (u1Tos);

                                GetLeastNextHopIpAddr (pRtEntry,
                                                       &leastNextHopIpAddr,
                                                       u1Tos);
                                *pu4NextFutOspfBRRouteIpNextHop =
                                    OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                                *pi4NextFutOspfBRRouteDestType =
                                    pRtEntry->u1DestType;
                                return SNMP_SUCCESS;
                            }
                        }
                        break;
                    }
                    else
                    {
                        u1Found = OSPF_TRUE;
                    }

                    /* fall through */
                case OSPF_GREATER:
                    u1Found = OSPF_TRUE;
                    break;
                default:
                    break;
            }

            if (u1Found == OSPF_TRUE)
            {
                for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                {
                    if (TMO_SLL_Count (&(pRtEntry->aTosPath[u1Tos])) != 0)
                    {
                        *pu4NextFutOspfBRRouteIpAddr =
                            OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId);
                        *pu4NextFutOspfBRRouteIpAddrMask =
                            pRtEntry->u4IpAddrMask;
                        *pu4NextFutOspfBRRouteIpTos = ENCODE_TOS (u1Tos);
                        GetLeastNextHopIpAddr (pRtEntry,
                                               &leastNextHopIpAddr, u1Tos);
                        *pu4NextFutOspfBRRouteIpNextHop =
                            OSPF_CRU_BMC_DWFROMPDU (leastNextHopIpAddr);
                        *pi4NextFutOspfBRRouteDestType = pRtEntry->u1DestType;
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }
    return SNMP_FAILURE;
}

    /* Low Level GET Routine for All Objects  */

    /****************************************************************************
     Function    :  nmhGetFutOspfBRRouteType
     Input       :  The Indices
            FutOspfBRRouteIpAddr
            FutOspfBRRouteIpAddrMask
            FutOspfBRRouteIpTos
            FutOspfBRRouteIpNextHop
            FutOspfBRRouteDestType

            The Object 
            retValFutOspfBRRouteType
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfBRRouteType (UINT4 u4FutOspfBRRouteIpAddr,
                          UINT4 u4FutOspfBRRouteIpAddrMask,
                          UINT4 u4FutOspfBRRouteIpTos,
                          UINT4 u4FutOspfBRRouteIpNextHop,
                          INT4 i4FutOspfBRRouteDestType,
                          INT4 *pi4RetValFutOspfBRRouteType)
{
    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfBRRouteIpAddr,
                                             u4FutOspfBRRouteIpAddrMask,
                                             u4FutOspfBRRouteIpTos,
                                             u4FutOspfBRRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfBRRouteType,
                                             OSPF_ROUTE_RTTYPE,
                                             (UINT1) i4FutOspfBRRouteDestType);
    return i1ReturnValue;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfBRRouteAreaId
     Input       :  The Indices
            FutOspfBRRouteIpAddr
            FutOspfBRRouteIpAddrMask
            FutOspfBRRouteIpTos
            FutOspfBRRouteIpNextHop
            FutOspfBRRouteDestType

            The Object 
            retValFutOspfBRRouteAreaId
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfBRRouteAreaId (UINT4 u4FutOspfBRRouteIpAddr,
                            UINT4 u4FutOspfBRRouteIpAddrMask,
                            UINT4 u4FutOspfBRRouteIpTos,
                            UINT4 u4FutOspfBRRouteIpNextHop,
                            INT4 i4FutOspfBRRouteDestType,
                            UINT4 *pu4RetValFutOspfBRRouteAreaId)
{
    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfBRRouteIpAddr,
                                             u4FutOspfBRRouteIpAddrMask,
                                             u4FutOspfBRRouteIpTos,
                                             u4FutOspfBRRouteIpNextHop,
                                             pu4RetValFutOspfBRRouteAreaId,
                                             OSPF_ROUTE_AREAID,
                                             (UINT1) i4FutOspfBRRouteDestType);

    return i1ReturnValue;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfBRRouteCost
     Input       :  The Indices
            FutOspfBRRouteIpAddr
            FutOspfBRRouteIpAddrMask
            FutOspfBRRouteIpTos
            FutOspfBRRouteIpNextHop
            FutOspfBRRouteDestType

            The Object 
            retValFutOspfBRRouteCost
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfBRRouteCost (UINT4 u4FutOspfBRRouteIpAddr,
                          UINT4 u4FutOspfBRRouteIpAddrMask,
                          UINT4 u4FutOspfBRRouteIpTos,
                          UINT4 u4FutOspfBRRouteIpNextHop,
                          INT4 i4FutOspfBRRouteDestType,
                          INT4 *pi4RetValFutOspfBRRouteCost)
{
    INT1                i1ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i1ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfBRRouteIpAddr,
                                             u4FutOspfBRRouteIpAddrMask,
                                             u4FutOspfBRRouteIpTos,
                                             u4FutOspfBRRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfBRRouteCost,
                                             OSPF_ROUTE_COST,
                                             (UINT1) i4FutOspfBRRouteDestType);
    return i1ReturnValue;
}

    /****************************************************************************
     Function    :  nmhGetFutOspfBRRouteInterfaceIndex
     Input       :  The Indices
            FutOspfBRRouteIpAddr
            FutOspfBRRouteIpAddrMask
            FutOspfBRRouteIpTos
            FutOspfBRRouteIpNextHop
            FutOspfBRRouteDestType

            The Object 
            retValFutOspfBRRouteInterfaceIndex
     Output      :  The Get Low Lev Routine Take the Indices &
            store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
    ****************************************************************************/
INT1
nmhGetFutOspfBRRouteInterfaceIndex (UINT4 u4FutOspfBRRouteIpAddr,
                                    UINT4 u4FutOspfBRRouteIpAddrMask,
                                    UINT4 u4FutOspfBRRouteIpTos,
                                    UINT4 u4FutOspfBRRouteIpNextHop,
                                    INT4 i4FutOspfBRRouteDestType,
                                    INT4 *pi4RetValFutOspfBRRouteInterfaceIndex)
{
    INT4                i4ReturnValue;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    i4ReturnValue = OspfRtTblGetObjectInCxt (pOspfCxt,
                                             u4FutOspfBRRouteIpAddr,
                                             u4FutOspfBRRouteIpAddrMask,
                                             u4FutOspfBRRouteIpTos,
                                             u4FutOspfBRRouteIpNextHop,
                                             (UINT4 *)
                                             pi4RetValFutOspfBRRouteInterfaceIndex,
                                             OSPF_ROUTE_IFACEINDEX,
                                             (UINT1) i4FutOspfBRRouteDestType);
    return (INT1) i4ReturnValue;
}

/* LOW LEVEL Routines for Table : FutOspfDistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfDistInOutRouteMapTable
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfDistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                                       pFutOspfDistInOutRouteMapName,
                                                       INT4
                                                       i4FutOspfDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (OspfCmpFilterRMapName
            (pOspfCxt->pDistanceFilterRMap, pFutOspfDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (OspfCmpFilterRMapName
            (pOspfCxt->pDistributeInFilterRMap,
             pFutOspfDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
#endif /*ROUTEMAP_WANTED */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfDistInOutRouteMapTable
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfDistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                               pFutOspfDistInOutRouteMapName,
                                               INT4
                                               *pi4FutOspfDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt != NULL)
    {
        tFilteringRMap     *pMinFilter = NULL;
        pMinFilter =
            OspfGetMinFilterRMap (pOspfCxt->pDistanceFilterRMap,
                                  FILTERING_TYPE_DISTANCE,
                                  pOspfCxt->pDistributeInFilterRMap,
                                  FILTERING_TYPE_DISTRIB_IN);

        if (pMinFilter != NULL)
        {
            pFutOspfDistInOutRouteMapName->i4_Length =
                STRLEN (pMinFilter->au1DistInOutFilterRMapName);

            MEMCPY (pFutOspfDistInOutRouteMapName->pu1_OctetList,
                    pMinFilter->au1DistInOutFilterRMapName,
                    pFutOspfDistInOutRouteMapName->i4_Length);

            if (pMinFilter == pOspfCxt->pDistanceFilterRMap)
            {
                *pi4FutOspfDistInOutRouteMapType = FILTERING_TYPE_DISTANCE;
            }
            else
            {
                *pi4FutOspfDistInOutRouteMapType = FILTERING_TYPE_DISTRIB_IN;
            }
            return SNMP_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (pi4FutOspfDistInOutRouteMapType);
#endif /*ROUTEMAP_WANTED */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfDistInOutRouteMapTable
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                nextFutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType
                nextFutOspfDistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfDistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                              pFutOspfDistInOutRouteMapName,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFutOspfDistInOutRouteMapName,
                                              INT4
                                              i4FutOspfDistInOutRouteMapType,
                                              INT4
                                              *pi4NextFutOspfDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt != NULL)
    {
        tFilteringRMap     *pMinFilter = NULL;
        tFilteringRMap     *pRetFilter = NULL;
        pMinFilter =
            OspfGetMinFilterRMap (pOspfCxt->pDistanceFilterRMap,
                                  FILTERING_TYPE_DISTANCE,
                                  pOspfCxt->pDistributeInFilterRMap,
                                  FILTERING_TYPE_DISTRIB_IN);

        if (pMinFilter != NULL)
        {
            if (pMinFilter == pOspfCxt->pDistanceFilterRMap
                && i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
            {
                pRetFilter = pOspfCxt->pDistributeInFilterRMap;
            }
            else if (pMinFilter == pOspfCxt->pDistributeInFilterRMap
                     && i4FutOspfDistInOutRouteMapType ==
                     FILTERING_TYPE_DISTRIB_IN)
            {
                pRetFilter = pOspfCxt->pDistanceFilterRMap;
            }

            if (pRetFilter != NULL
                && OspfCmpFilterRMapName (pMinFilter,
                                          pFutOspfDistInOutRouteMapName) == 0)
            {
                pNextFutOspfDistInOutRouteMapName->i4_Length =
                    STRLEN (pRetFilter->au1DistInOutFilterRMapName);

                MEMCPY (pNextFutOspfDistInOutRouteMapName->pu1_OctetList,
                        pRetFilter->au1DistInOutFilterRMapName,
                        pNextFutOspfDistInOutRouteMapName->i4_Length);

                if (pRetFilter == pOspfCxt->pDistanceFilterRMap)
                {
                    *pi4NextFutOspfDistInOutRouteMapType =
                        FILTERING_TYPE_DISTANCE;
                }
                else
                {
                    *pi4NextFutOspfDistInOutRouteMapType =
                        FILTERING_TYPE_DISTRIB_IN;
                }
                return SNMP_SUCCESS;
            }
        }
    }
#else
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (pNextFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
    UNUSED_PARAM (pi4NextFutOspfDistInOutRouteMapType);
#endif /*ROUTEMAP_WANTED */

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfDistInOutRouteMapValue
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType

                The Object
                retValFutOspfDistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfDistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                     pFutOspfDistInOutRouteMapName,
                                     INT4 i4FutOspfDistInOutRouteMapType,
                                     INT4
                                     *pi4RetValFutOspfDistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE
        &&
        (OspfCmpFilterRMapName
         (pOspfCxt->pDistanceFilterRMap, pFutOspfDistInOutRouteMapName) == 0))
    {
        *pi4RetValFutOspfDistInOutRouteMapValue =
            pOspfCxt->pDistanceFilterRMap->u1RMapDistance;
    }
    else
    {
        *pi4RetValFutOspfDistInOutRouteMapValue = 0;
    }
#else
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFutOspfDistInOutRouteMapValue);
#endif /*ROUTEMAP_WANTED */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfDistInOutRouteMapRowStatus
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType

                The Object
                retValFutOspfDistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfDistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFutOspfDistInOutRouteMapName,
                                         INT4 i4FutOspfDistInOutRouteMapType,
                                         INT4
                                         *pi4RetValFutOspfDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (OspfCmpFilterRMapName
            (pOspfCxt->pDistanceFilterRMap, pFutOspfDistInOutRouteMapName) == 0)
        {
            *pi4RetValFutOspfDistInOutRouteMapRowStatus =
                pOspfCxt->pDistanceFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    else if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (OspfCmpFilterRMapName
            (pOspfCxt->pDistributeInFilterRMap,
             pFutOspfDistInOutRouteMapName) == 0)
        {
            *pi4RetValFutOspfDistInOutRouteMapRowStatus =
                pOspfCxt->pDistributeInFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFutOspfDistInOutRouteMapRowStatus);
    return SNMP_SUCCESS;
#endif /*ROUTEMAP_WANTED */

}

    /****************************************************************************
     Function    :  GetFindAreaAggrIndex
     Input       :  pArea         : Pointer to the Area structure 
            pIpAddr       : Pointer to Aggregate Network Id.
            pIpAddrMask   : Pointer to Aggregate Network Mask.
     Output      :  None
            
     Returns     :  Returns OSPF_SUCCESS/OSPF_FAILURE
    ****************************************************************************/
PUBLIC INT1
GetFindAreaAggrIndex (tArea * pArea, tIPADDR * pIpAddr,
                      tIPADDR * pIpAddrMask, UINT1 u1LsdbType,
                      INT1 *pReturnIndex)
{
    UINT1               u1Index;
    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {
        if ((pArea->aAddrRange[u1Index].areaAggStatus != INVALID)
            && (UtilIpAddrComp (*pIpAddr, pArea->aAddrRange[u1Index].ipAddr)
                == OSPF_EQUAL) &&
            (UtilIpAddrComp (*pIpAddrMask,
                             pArea->aAddrRange[u1Index].ipAddrMask)
             == OSPF_EQUAL)
            && (u1LsdbType == pArea->aAddrRange[u1Index].u1LsdbType))
        {
            *pReturnIndex = (INT1) u1Index;
            return OSPF_SUCCESS;
        }
    }
    return OSPF_FAILURE;
}

    /****************************************************************************
     Function    :  OspfGetNextAsExternalAggregationIndex
     Input       :  currAsNet       - Current AsExternalAggregation NetId 
            nextAsNet       - Next AsExternalAggregation NetId
            currAsExtMask   - Current AsExternalAggregation Mask
            nextAsExtMask   - Next AsExternalAggregation Mask
            currAsExtAreaId - Current AsExternalAggregation AreaId
            nextAsExtAreaId - Next AsExternalAggregation AreaId

            pIpAddr       : Pointer to Aggregate Network Id.
            pIpAddrMask   : Pointer to Aggregate Network Mask.
     Output      :  None
            
     Returns     :  Returns OSPF_SUCCESS/OSPF_FAILURE
    ****************************************************************************/
PRIVATE INT1
OspfGetNextAsExternalAggregationIndex (tIPADDR currAsNet,
                                       tIPADDR nextAsNet,
                                       tIPADDRMASK currAsExtMask,
                                       tIPADDRMASK nextAsExtMask,
                                       tIPADDR currAsExtAreaId,
                                       tIPADDR nextAsExtAreaId)
{
    INT1                i1RetVal = OSPF_FAILURE;

    switch (UtilIpAddrComp (currAsNet, nextAsNet))
    {
        case OSPF_GREATER:
            i1RetVal = OSPF_FAILURE;
            break;
        case OSPF_LESS:
            i1RetVal = OSPF_SUCCESS;
            break;
        case OSPF_EQUAL:
            switch (UtilIpAddrComp (currAsExtMask, nextAsExtMask))
            {
                case OSPF_GREATER:
                    i1RetVal = OSPF_FAILURE;
                    break;
                case OSPF_LESS:
                    i1RetVal = OSPF_SUCCESS;
                    break;
                case OSPF_EQUAL:
                    switch (UtilIpAddrComp (currAsExtAreaId, nextAsExtAreaId))
                    {
                        case OSPF_GREATER:
                            i1RetVal = OSPF_FAILURE;
                            break;
                        case OSPF_LESS:
                            i1RetVal = OSPF_SUCCESS;
                            break;
                        case OSPF_EQUAL:
                            i1RetVal = OSPF_FAILURE;
                            break;
                        default:
                            i1RetVal = OSPF_FAILURE;
                            break;

                    }
                    break;
                default:
                    i1RetVal = OSPF_FAILURE;
                    break;
            }
            break;
        default:
            i1RetVal = OSPF_FAILURE;
            break;
    }
    return i1RetVal;
}

    /****************************************************************************
     Function    :  OspfFindNextHopIpAddr
     Input       :  pRtEntry       - Pointer to RtEntry 
            pNextHopIpAddr - Pointer to the nextHop IP address
            u1Tos          - TOS value  
     Output      :  None
            
     Returns     :  Returns OSPF_SUCCESS/OSPF_FAILURE
    ****************************************************************************/

PRIVATE INT1
OspfFindNextHopIpAddr (tRtEntry * pRtEntry, tIPADDR * pNextHopIpAddr,
                       UINT1 u1Tos)
{
    INT2                i2NextHopCount = 0;
    tPath              *pPath = NULL;

    TMO_SLL_Scan (&(pRtEntry->aTosPath[u1Tos]), pPath, tPath *)
    {
        i2NextHopCount = 0;
        while (i2NextHopCount < pPath->u1HopCount)
        {
            if (i2NextHopCount < MAX_NEXT_HOPS)
            {
                if (UtilIpAddrComp (pPath->aNextHops[i2NextHopCount].nbrIpAddr,
                                    *pNextHopIpAddr) == OSPF_EQUAL)
                {
                    return OSPF_SUCCESS;
                }
            }
            i2NextHopCount++;
        }
    }
    return OSPF_FAILURE;
}

/* Low level routines for FutOspfVirtNbrTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfVirtNbrTable
 Input       :  The Indices
                FutOspfVirtNbrArea
                FutOspfVirtNbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfVirtNbrTable (UINT4 *pu4OspfVirtNbrArea,
                                     UINT4 *pu4OspfVirtNbrRtrId)
{
    return nmhGetFirstIndexOspfVirtNbrTable (pu4OspfVirtNbrArea,
                                             pu4OspfVirtNbrRtrId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfVirtNbrTable
 Input       :  The Indices
                FutOspfVirtNbrArea
                nextFutOspfVirtNbrArea
                FutOspfVirtNbrRtrId
                nextFutOspfVirtNbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfVirtNbrTable (UINT4 u4OspfVirtNbrArea,
                                    UINT4 *pu4NextOspfVirtNbrArea,
                                    UINT4 u4OspfVirtNbrRtrId,
                                    UINT4 *pu4NextOspfVirtNbrRtrId)
{
    return nmhGetNextIndexOspfVirtNbrTable (u4OspfVirtNbrArea,
                                            pu4NextOspfVirtNbrArea,
                                            u4OspfVirtNbrRtrId,
                                            pu4NextOspfVirtNbrRtrId);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfVirtNbrRestartHelperStatus
 Input       :  The Indices
                FutOspfVirtNbrArea
                FutOspfVirtNbrRtrId

                The Object
                retValFutOspfVirtNbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFutOspfVirtNbrRestartHelperStatus
    (UINT4 u4OspfVirtNbrArea,
     UINT4 u4OspfVirtNbrRtrId, INT4 *pi4RetValFutOspfVirtNbrRestartHelperStatus)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&transitAreaId, 0, MAX_IP_ADDR_LEN);
    MEMSET (&nbrId, 0, MAX_IP_ADDR_LEN);

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        *pi4RetValFutOspfVirtNbrRestartHelperStatus = pNbr->u1NbrHelperStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtNbrRestartHelperAge
 Input       :  The Indices
                FutOspfVirtNbrArea
                FutOspfVirtNbrRtrId

                The Object
                retValFutOspfVirtNbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFutOspfVirtNbrRestartHelperAge
    (UINT4 u4OspfVirtNbrArea,
     UINT4 u4OspfVirtNbrRtrId, UINT4 *pu4RetValFutOspfVirtNbrRestartHelperAge)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&transitAreaId, 0, MAX_IP_ADDR_LEN);
    MEMSET (&nbrId, 0, MAX_IP_ADDR_LEN);

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
        {
            if (TmrGetRemainingTime
                (gTimerLst,
                 (tTmrAppTimer *) (&(pNbr->helperGraceTimer.timerNode)),
                 pu4RetValFutOspfVirtNbrRestartHelperAge) == TMR_FAILURE)
            {
                *pu4RetValFutOspfVirtNbrRestartHelperAge = 0;
            }
            else
            {
                *pu4RetValFutOspfVirtNbrRestartHelperAge =
                    (*pu4RetValFutOspfVirtNbrRestartHelperAge /
                     NO_OF_TICKS_PER_SEC);
            }
        }
        else
        {
            *pu4RetValFutOspfVirtNbrRestartHelperAge = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtNbrRestartHelperExitReason
 Input       :  The Indices
                FutOspfVirtNbrArea
                FutOspfVirtNbrRtrId

                The Object
                retValFutOspfVirtNbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFutOspfVirtNbrRestartHelperExitReason
    (UINT4 u4OspfVirtNbrArea,
     UINT4 u4OspfVirtNbrRtrId,
     INT4 *pi4RetValFutOspfVirtNbrRestartHelperExitReason)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tNeighbor          *pNbr = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&transitAreaId, 0, MAX_IP_ADDR_LEN);
    MEMSET (&nbrId, 0, MAX_IP_ADDR_LEN);

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtNbrArea);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtNbrRtrId);
    if ((pNbr = GetFindVirtNbrInCxt (pOspfCxt, &transitAreaId, &nbrId)) != NULL)
    {
        *pi4RetValFutOspfVirtNbrRestartHelperExitReason =
            pNbr->u1NbrHelperExitReason;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfGrShutdown
 Input       :  The Indices

                The Object
                retValFutOspfGrShutdown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfGrShutdown (INT4 *pi4RetValFutOspfGrShutdown)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfGrShutdown = pOspfCxt->u1RestartStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
    Function    :  nmhGetFutOspfPreferenceValue
    Input       :  The Indices

                   The Object 
                   retValFutOspfPreferenceValue
    Output      :  The Get Low Lev Routine Take the Indices &
                   store the Value requested in the Return val.
    Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfPreferenceValue (INT4 *pi4RetValFutOspfPreferenceValue)
{
    *pi4RetValFutOspfPreferenceValue = gOsRtr.pOspfCxt->u1Distance;
    return SNMP_SUCCESS;
}

INT1
nmhValidateIndexInstanceFutOspfVirtIfAuthTable (UINT4
                                                u4FutOspfVirtIfAuthAreaId,
                                                UINT4
                                                u4FutOspfVirtIfAuthNeighbor,
                                                INT4 i4FutOspfVirtIfAuthKeyId)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfVirtIfAuthKeyId > 255)
        return SNMP_FAILURE;

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfAuthAreaId);

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfAuthNeighbor);
    if (GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                       (UINT1) i4FutOspfVirtIfAuthKeyId)
        != NULL)
    {
        return (SNMP_SUCCESS);
    }
    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
        return SNMP_FAILURE;
    if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst))
        < MAX_MD5AUTHKEYS_PER_IF)
    {
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfIfCryptoAuthTable
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfIfCryptoAuthTable (UINT4 *pu4OspfIfIpAddress,
                                          INT4 *pi4OspfAddressLessIf)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pOspfCxt->pLastOspfIf = NULL;

    if ((pLstNode = TMO_SLL_First (&(pOspfCxt->sortIfLst))) != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IP_ADDR_COPY (ifIpAddr, pInterface->ifIpAddr);
        *pi4OspfAddressLessIf = (INT4) pInterface->u4AddrlessIf;
        *pu4OspfIfIpAddress = OSPF_CRU_BMC_DWFROMPDU (ifIpAddr);
        pOspfCxt->pLastOspfIf = pLstNode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfIfCryptoAuthTable
 Input       :  The Indices
                OspfIfIpAddress
                nextOspfIfIpAddress
                OspfAddressLessIf
                nextOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfIfCryptoAuthTable (UINT4 u4OspfIfIpAddress,
                                         UINT4 *pu4NextOspfIfIpAddress,
                                         INT4 i4OspfAddressLessIf,
                                         INT4 *pi4NextOspfAddressLessIf)
{

    tIPADDR             currIfIpAddr;
    tIPADDR             nextIfIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT1               u1Len = MAX_IP_ADDR_LEN;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currIfIpAddr, u4OspfIfIpAddress);

    if (pOspfCxt->pLastOspfIf != NULL)
    {
        for (;;)
        {
            pOspfCxt->pLastOspfIf =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&pOspfCxt->sortIfLst,
                                                (tTMO_SLL_NODE *) pOspfCxt->
                                                pLastOspfIf);

            if (pOspfCxt->pLastOspfIf == NULL)
            {
                return SNMP_FAILURE;
            }

            pInterface = GET_IF_PTR_FROM_SORT_LST (pOspfCxt->pLastOspfIf);

            switch (UtilIpAddrNComp
                    (&(currIfIpAddr), &(pInterface->ifIpAddr), u1Len))
            {

                case OSPF_GREATER:
                    continue;

                case OSPF_EQUAL:
                    if ((UINT4) i4OspfAddressLessIf >= pInterface->u4AddrlessIf)
                    {
                        continue;
                    }
                    /* fall through */

                case OSPF_LESS:
                    IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                    *pi4NextOspfAddressLessIf =
                        (INT4) (pInterface->u4AddrlessIf);
                    *pu4NextOspfIfIpAddress =
                        OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                    return SNMP_SUCCESS;
                default:
                    break;
            }
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOspfCxt->pLastOspfIf = pLstNode;
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilIpAddrNComp
                (&(currIfIpAddr), &(pInterface->ifIpAddr), u1Len))
        {

            case OSPF_GREATER:
                continue;

            case OSPF_EQUAL:
                if ((UINT4) i4OspfAddressLessIf >= pInterface->u4AddrlessIf)
                {
                    continue;
                }
                /* fall through */

            case OSPF_LESS:
                IP_ADDR_COPY (nextIfIpAddr, pInterface->ifIpAddr);
                *pi4NextOspfAddressLessIf = (INT4) (pInterface->u4AddrlessIf);
                *pu4NextOspfIfIpAddress = OSPF_CRU_BMC_DWFROMPDU (nextIfIpAddr);
                return SNMP_SUCCESS;
            default:
                break;

        }
    }

    pOspfCxt->pLastOspfIf = NULL;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfIfCryptoAuthType 
 Input       :  The Indices
                u4OspfIfIpAddress
                u4OspfAddressLessIf

                The Object 
                pi4RetValOspfIfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFutOspfIfCryptoAuthType (UINT4 u4OspfIfIpAddress,
                               UINT4 u4OspfAddressLessIf,
                               INT4 *pi4RetValOspfIfCryptoAuthType)
{

    INT1                i1Return = SNMP_FAILURE;
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return i1Return;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) u4OspfAddressLessIf))
        != NULL)
    {
        *pi4RetValOspfIfCryptoAuthType = (INT4) pInterface->u4CryptoAuthType;
        return SNMP_SUCCESS;
    }

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfVirtIfCryptoAuthTable
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfVirtIfCryptoAuthTable (UINT4 *pu4OspfVirtIfAreaId,
                                              UINT4 *pu4OspfVirtIfNeighbor)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLstNode = TMO_SLL_First (&(pOspfCxt->virtIfLst))) != NULL)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        IP_ADDR_COPY (transitAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (nbrId, pInterface->destRtrId);
        *pu4OspfVirtIfAreaId = OSPF_CRU_BMC_DWFROMPDU (transitAreaId);
        *pu4OspfVirtIfNeighbor = OSPF_CRU_BMC_DWFROMPDU (nbrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfVirtIfCryptoAuthTable
 Input       :  The Indices
                OspfVirtIfAreaId
                nextOspfVirtIfAreaId
                OspfVirtIfNeighbor
                nextOspfVirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfVirtIfCryptoAuthTable (UINT4 u4OspfVirtIfAreaId,
                                             UINT4 *pu4NextOspfVirtIfAreaId,
                                             UINT4 u4OspfVirtIfNeighbor,
                                             UINT4 *pu4NextOspfVirtIfNeighbor)
{
    tAreaId             currAreaId;
    tRouterId           currNbrId;
    tAreaId             nextAreaId;
    tRouterId           nextNbrId;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (currNbrId, u4OspfVirtIfNeighbor);

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (UtilIpAddrComp (currAreaId, pInterface->transitAreaId))
        {
            case OSPF_GREATER:
                /* try next entry */
                continue;

            case OSPF_EQUAL:
                /* first index is complete */
                switch (UtilIpAddrComp (currNbrId, pInterface->destRtrId))
                {
                    case OSPF_GREATER:
                        /* try next entry */
                        continue;
                    case OSPF_EQUAL:
                        /* second index is complete */
                        /* try next entry */
                        continue;
                        /* next found, fall through */
                    case OSPF_LESS:
                        break;
                        /* next found, fall through */
                    default:
                        break;
                }
                /* next found, fall through */

            case OSPF_LESS:
                IP_ADDR_COPY (nextAreaId, pInterface->transitAreaId);
                IP_ADDR_COPY (nextNbrId, pInterface->destRtrId);
                *pu4NextOspfVirtIfAreaId = OSPF_CRU_BMC_DWFROMPDU (nextAreaId);
                *pu4NextOspfVirtIfNeighbor = OSPF_CRU_BMC_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;

            default:
                break;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfVirtIfCryptoAuthType 
 Input       :  The Indices
                u4OspfVirtIfAreaId
                u4OspfVirtIfNeighbor

                The Object 
                pi4RetValOspfVirtIfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfVirtIfCryptoAuthType (UINT4 u4OspfVirtIfAreaId,
                                   UINT4 u4OspfVirtIfNeighbor,
                                   INT4 *pi4RetValOspfVirtIfCryptoAuthType)
{

    INT1                i1Return = SNMP_FAILURE;
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);
    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValOspfVirtIfCryptoAuthType =
            (INT4) pInterface->u4CryptoAuthType;
        return SNMP_SUCCESS;
    }

    return i1Return;
}

tLsaInfo           *
GetFirstLsaBytype (UINT4 u4OspfCxtId, UINT4 u4AreaId, INT4 i4LsaType)
{
    tTMO_SLL           *pLsaLst = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo            LsaInfo;
    tArea              *pArea = NULL;
    tAreaId             OspfAreaId;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4AdminStat;
    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {
        return NULL;
    }
    OSPF_CRU_BMC_DWTOPDU (OspfAreaId, u4AreaId);
    pArea = GetFindAreaInCxt (pOspfCxt, &OspfAreaId);

    if (pArea == NULL)
    {
        return NULL;
    }
    if ((i4LsaType == NETWORK_SUM_LSA) || (i4LsaType == ASBR_SUM_LSA))
    {
        MEMSET (&LsaInfo, 0, sizeof (tLsaInfo));
        LsaInfo.lsaId.u1LsaType = (UINT1) i4LsaType;
        pLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot, &LsaInfo, NULL);
        if (pLsaInfo != NULL)
        {
            if (pLsaInfo->lsaId.u1LsaType == LsaInfo.lsaId.u1LsaType)
            {
                return pLsaInfo;
            }
            else
            {
                return NULL;
            }
        }
    }
    switch (i4LsaType)
    {
        case ROUTER_LSA:
            pLsaLst = &(pArea->rtrLsaLst);
            break;
        case NETWORK_LSA:
            pLsaLst = &(pArea->networkLsaLst);
            break;
        case TYPE10_OPQ_LSA:
            pLsaLst = &(pArea->Type10OpqLSALst);
            break;
        case NSSA_LSA:
            pLsaLst = &(pArea->nssaLSALst);
            break;
        default:
            pLsaLst = NULL;
            break;
    }
    if (pLsaLst != NULL)
    {
        if ((pLstNode = TMO_SLL_First (pLsaLst)) != NULL)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
            if (pLsaInfo != NULL)
            {
                pOspfCxt->pLastLsdbLst = pLsaLst;
                pOspfCxt->pLastLsdbNode = pLstNode;
            }

        }
    }
    return pLsaInfo;
}

tLsaInfo           *
GetNextLsaBytype (UINT4 u4OspfCxtId, UINT4 u4AreaId, tLsaInfo * pFirstLsaInfo,
                  INT4 i4LsaType)
{
    tAreaId             currAreaId;
    tLINKSTATEID        currLinkStateId;
    tRouterId           currAdvRtrId;
    tTMO_SLL           *pLsaLst = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    UINT1               u1AreaIdLen = MAX_IP_ADDR_LEN;
    UINT1               u1LsIdLen = MAX_IP_ADDR_LEN;
    UINT1               u1AdvRtrIdLen = MAX_IP_ADDR_LEN;
    tArea              *pArea = NULL;
    tAreaId             OspfAreaId;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4AdminStat;

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {
        return NULL;
    }
    OSPF_CRU_BMC_DWTOPDU (OspfAreaId, u4AreaId);
    pArea = GetFindAreaInCxt (pOspfCxt, &OspfAreaId);

    if (pArea == NULL)
    {
        return NULL;

    }
    IP_ADDR_COPY (currAreaId, pArea->areaId);
    IP_ADDR_COPY (currLinkStateId, pFirstLsaInfo->lsaId.linkStateId);
    IP_ADDR_COPY (currAdvRtrId, pFirstLsaInfo->lsaId.advRtrId);
    if ((pOspfCxt->pLastLsdbLst != NULL) && (pOspfCxt->pLastLsdbNode != NULL))
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);

        if ((UtilIpAddrNComp
             (&(pLsaInfo->pArea->areaId), &currAreaId,
              u1AreaIdLen) == OSPF_EQUAL)
            && (pLsaInfo->lsaId.u1LsaType == (UINT1) i4LsaType)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.linkStateId), &currLinkStateId,
              u1LsIdLen) == OSPF_EQUAL)
            &&
            (UtilIpAddrNComp
             (&(pLsaInfo->lsaId.advRtrId), &currAdvRtrId,
              u1AdvRtrIdLen) == OSPF_EQUAL))
        {
            /* Get the Next Lsa from the List */
            pOspfCxt->pLastLsdbNode =
                (tTMO_SLL_NODE *) TMO_SLL_Next (pOspfCxt->pLastLsdbLst,
                                                pOspfCxt->pLastLsdbNode);
            if (pOspfCxt->pLastLsdbNode != NULL)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOspfCxt->pLastLsdbNode);
                return pLsaInfo;
            }
        }
    }
    /* Reset  the Cache pointers */
    pOspfCxt->pLastLsdbLst = NULL;
    pOspfCxt->pLastLsdbNode = NULL;

    if ((i4LsaType == NETWORK_SUM_LSA) || (i4LsaType == ASBR_SUM_LSA))
    {
        pLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot,
                                        pFirstLsaInfo, NULL);
        if (pLsaInfo != NULL)
        {
            if (pLsaInfo->lsaId.u1LsaType == pFirstLsaInfo->lsaId.u1LsaType)
            {
                return pLsaInfo;
            }
        }
        return NULL;
    }
    else
    {
        switch (i4LsaType)
        {
            case ROUTER_LSA:
                pLsaLst = &(pArea->rtrLsaLst);
                break;
            case NETWORK_LSA:
                pLsaLst = &(pArea->networkLsaLst);
                break;
            case TYPE10_OPQ_LSA:
                pLsaLst = &(pArea->Type10OpqLSALst);
                break;

            case NSSA_LSA:
                pLsaLst = &(pArea->nssaLSALst);
                break;
            default:
                pLsaLst = NULL;
                break;
        }
        if (pLsaLst != NULL)
        {
            TMO_SLL_Scan (pLsaLst, pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                if (GetIsNextLsaTableIndex
                    (&(currAreaId), u1AreaIdLen,
                     (UINT1) i4LsaType, &(currLinkStateId),
                     u1LsIdLen, &(currAdvRtrId), u1AdvRtrIdLen,
                     pLsaInfo) == OSPF_TRUE)
                {
                    pOspfCxt->pLastLsdbLst = pLsaLst;
                    pOspfCxt->pLastLsdbNode = pLsaNode;
                    return pLsaInfo;
                }
            }
        }
        pLsaInfo = NULL;
    }
    return pLsaInfo;
}

    /*-----------------------------------------------------------------------*/
    /*                       End of the file  osfetch.c                       */
    /*-----------------------------------------------------------------------*/
