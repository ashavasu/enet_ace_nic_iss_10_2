/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osnseln.c,v 1.11 2016/03/18 13:23:31 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             NSSA.
 *
 *******************************************************************/
#include "osinc.h"

/************************************************************************/
/*                                       */
/* Function     : NssaFsmTrnsltrRlChng                   */
/*                                          */
/* Description    : This handles the event NSSA Trnsltr RL CHNG        */
/*      Depending on NSSA State it triggers NSSA Trnsltr Eln         */
/*                                      */
/* Input          : pArea - Pointer to area                 */
/*                                         */
/* Output       : None                             */
/*                                         */
/* Returns      : VOID                             */
/*                                      */
/************************************************************************/
PUBLIC INT4
NssaFsmTrnsltrRlChng (tArea * pArea)
{
    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaFsmTrnsltrRlChng\n");

    if (pArea->pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        if (pArea->u1NssaTrnsltrRole == TRNSLTR_ROLE_ALWAYS)
        {
            pArea->u1NssaTrnsltrState = TRNSLTR_STATE_ENAELCTD;
            pArea->u4NssaTrnsltrEvents++;
        }
        /* Translation election and LSA orignation will not be done during GR */
        return OSPF_SUCCESS;
    }

    if (pArea->u1NssaTrnsltrRole == TRNSLTR_ROLE_ALWAYS)
    {
        if(pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {    
             NssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                 TRNSLTR_STATE_ENAELCTD);
        }
        else
        {
            NssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                TRNSLTR_STATE_DISABLED);
        }    
    }
    else
    {
        NssaFsmFindTranslator (pArea);
    }

    OlsGenerateLsa (pArea, ROUTER_LSA, &pArea->pOspfCxt->rtrId, NULL);

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: NssaFsmTrnsltrRlChng\n");
    return OSPF_SUCCESS;
}

/****************************************************************/
/*                                                              */
/* Function     : NssaFsmTrgrTrnsltrElnInCxt                  */
/*                                     */
/* Description    : This routine scans each NSSA area and triggers*/
/*              NSSA Trnsltr Eln for that area    */
/*                                  */
/* Input          : None                         */
/*                                   */
/* Output       : None                        */
/*                                     */
/* Returns      : VOID                        */
/*                                   */
/****************************************************************/
PUBLIC VOID
NssaFsmTrgrTrnsltrElnInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaFsmTrgrTrnsltrElnInCxt\n");
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType == NSSA_AREA)
        {
            NssaFsmFindTranslator (pArea);
        }
        else
        {
            continue;
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: NssaFsmTrgrTrnsltrElnInCxt\n");
}

/****************************************************************/
/*                                                              */
/* Function     : NssaFsmFindTranslator                  */
/*                                     */
/* Description    : This routine determines new translator     */
/*                                   */
/* Input          : pArea - Pointer to area                 */
/*                                   */
/* Output       : None                        */
/*                                     */
/* Returns      : VOID                         */
/*                                    */
/****************************************************************/
PUBLIC VOID
NssaFsmFindTranslator (tArea * pArea)
{

    UINT1               u1ActNbrs;
    /*tLsaInfo           *pLsaInfo = NULL; */
    tRtEntry           *pRtEntry = NULL;
    tArea              *pScanArea = NULL;
    UINT1               u1NotTrnsltr;
    UINT1               u1PrevTrnsltrState;
    tPath              *pDestRtPath = NULL;
    tRtEntry           *pAbrRtEntry = NULL;
    UINT4               u4CurrentTime = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaFsmFindTranslator\n");
    u1ActNbrs = RAG_NO_CHNG;
    u1NotTrnsltr = OSPF_FALSE;
    if ((pArea->u4AreaType != NSSA_AREA)
        || (!(pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE)))
    {
        OSPF_TRC (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                  "Rtr not ABR or AREA not NSSA \n");
        return;
    }
    u1PrevTrnsltrState = pArea->u1NssaTrnsltrState;
    switch (pArea->u1NssaTrnsltrRole)
    {

        case TRNSLTR_ROLE_ALWAYS:
            NssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                     TRNSLTR_STATE_ENAELCTD);

            if (u1PrevTrnsltrState == TRNSLTR_STATE_DISABLED)
            {
                if (pArea->pOspfCxt->u1RtrNetworkLsaChanged != OSPF_TRUE)
                {
                    pArea->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
                    RtcSetRtTimer (pArea->pOspfCxt);
                }
            }
            OSPF_TRC (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "Rtr Conf Trnsltr - ALWAYS \n");
            break;

        case TRNSLTR_ROLE_CANDIDATE:
            if (pArea->u4AreaBdrRtrCount == u1ActNbrs)
            {

                NssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                         TRNSLTR_STATE_ENAELCTD);

                if (u1PrevTrnsltrState == TRNSLTR_STATE_DISABLED)
                {
                    if (pArea->pOspfCxt->u1RtrNetworkLsaChanged != OSPF_TRUE)
                    {
                        pArea->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
                        RtcSetRtTimer (pArea->pOspfCxt);
                    }
                }

                OSPF_TRC (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "No active ABR in area, so elected Trnsltr \n");
                break;
            }

            TMO_SLL_Scan (&(pArea->pOspfCxt->pOspfRt->routesList), pAbrRtEntry,
                          tRtEntry *)
            {
                if (pAbrRtEntry->u1DestType == DEST_NETWORK)
                {
                    break;
                }

                if (pAbrRtEntry->u1DestType != DEST_AREA_BORDER)
                {
                    continue;
                }

                OSPF_TRC (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "FUNC : RtcGetPathInArea \n");

                pDestRtPath = RtcGetPathInArea (pAbrRtEntry,
                                                pArea->areaId, TOS_0);

                OSPF_TRC (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          "EXIT : RtcGetPathInArea \n");

                if (pDestRtPath == NULL)
                {
                    continue;
                }
                if ((IS_NTBIT_SET_RTR_LSA (pDestRtPath->pLsaInfo->pLsa)) ||
                    (UtilIpAddrComp (pArea->pOspfCxt->rtrId,
                                     pDestRtPath->pLsaInfo->lsaId.advRtrId) ==
                     OSPF_LESS))

                {

                    /* There exist an ABR with either "Nt" bit set OR of    */
                    /* higher Rtr Id Check if this ABR is reachable as  */
                    /* ASBR in other area                   */

                    pRtEntry = RtcFindRtEntry (pArea->pOspfCxt->pOspfRt,
                                               &(pDestRtPath->pLsaInfo->lsaId.
                                                 advRtrId), &gNullIpAddr,
                                               DEST_AS_BOUNDARY);

                    if (pRtEntry != NULL)
                    {
                        TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pScanArea,
                                      tArea *)
                        {
                            if (UtilIpAddrComp (pScanArea->areaId,
                                                pArea->areaId) != OSPF_EQUAL)
                            {
                                OSPF_TRC (OSPF_NSSA_TRC,
                                          pArea->pOspfCxt->u4OspfCxtId,
                                          "FUNC : RtcGetPathInArea \n");
                                pDestRtPath =
                                    RtcGetPathInArea (pRtEntry,
                                                      pScanArea->areaId, TOS_0);

                                OSPF_TRC (OSPF_NSSA_TRC,
                                          pArea->pOspfCxt->u4OspfCxtId,
                                          "EXIT : RtcGetPathInArea \n");

                                if (pDestRtPath != NULL)
                                {
                                    u1NotTrnsltr = OSPF_TRUE;
                                }
                            }
                        }
                    }
                }

                /* Check to see whether we need to relinquish the current 
                 * route calculation to do other OSPF processings */
                if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than relinquished interval */
                    if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId) &&
                        (u4CurrentTime >= pArea->pOspfCxt->u4StaggeringDelta))
                    {
                        gOsRtr.u4RTstaggeredCtxId =
                            pArea->pOspfCxt->u4OspfCxtId;
                        OSPFRtcRelinquish (pArea->pOspfCxt);
                    }
                }
            }                    /* Scan */

            if (u1NotTrnsltr == OSPF_FALSE)
            {
                NssaFsmUpdtTrnsltrState (pArea,
                                         pArea->u1NssaTrnsltrState,
                                         TRNSLTR_STATE_ENAELCTD);

                if (u1PrevTrnsltrState == TRNSLTR_STATE_DISABLED)
                {
                    if (pArea->pOspfCxt->u1RtrNetworkLsaChanged != OSPF_TRUE)
                    {
                        pArea->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
                        RtcSetRtTimer (pArea->pOspfCxt);
                    }
                }
            }

            else if ((u1NotTrnsltr == OSPF_TRUE) &&
                     (u1PrevTrnsltrState == TRNSLTR_STATE_ENAELCTD))
            {

                TmrSetTimer (&(pArea->nssaStbltyIntrvlTmr),
                             NSSA_STBLTY_INTERVAL_TIMER,
                             (NO_OF_TICKS_PER_SEC *
                              pArea->u4NssaTrnsltrStbltyInterval));

                pArea->nssaStbltyIntrvlTmr.i2Status = OSPF_TRUE;
            }

            break;

        default:
            OSPF_TRC (OSPF_NSSA_TRC, pArea->pOspfCxt->u4OspfCxtId,
                      "Error value in u1TrnsltrRole\n");
            break;

    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: NssaFsmFindTranslator\n");
}

/****************************************************************/
/*                                  */
/* Function     : NssaABRStatLostInCxt                     */
/*                                      */
/* Description    : This routine scans through NSSA areas and     */
/*                if the router is NSSA Trnsltr for area, it       */
/*          invokes NssaTrnsltrStatLost            */
/*                                   */
/* Input          : None                        */
/*                                   */
/* Output       : None                        */
/*                                     */
/* Returns      : VOID                       */
/*                                     */
/****************************************************************/
PUBLIC VOID
NssaABRStatLostInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaABRStatLostInCxt\n");
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType == NSSA_AREA)
        {
            OSPF_TRC1 (OSPF_NSSA_TRC, pOspfCxt->u4OspfCxtId,
                       "Lost ABR Stat but was Trnsltr in area %x\n",
                       pArea->areaId);

            NssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                     TRNSLTR_STATE_DISABLED);
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: NssaABRStatLostInCxt\n");
}

/****************************************************************/
/*                                     */
/* Function     : NssaType7To5TranslatorInCxt                */
/*                                     */
/* Description    : This routine scans through Type 7 LSA     */
/*                list in NSSA area and invokes         */
/*                "NssaType7LsaTranslation"  for each LSA     */
/*                                     */
/* Input          : None                        */
/*                                  */
/* Output       : None                      */
/*                                   */
/* Returns      : VOID                         */
/*                                   */
/****************************************************************/
PUBLIC VOID
NssaType7To5TranslatorInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pType5LsaInfo = NULL;
    tAddrRange         *pCurrRange = NULL;
    tOsDbNode          *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4Index;
    UINT4               u4HashKey = 0;
    UINT1               u1Tos;
    UINT4               u4CurrentTime = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaType7To5TranslatorInCxt\n");

    if ((pOspfCxt->u1OspfRestartState != OSPF_GR_NONE) ||
        (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE))
    {
        /* LSA cannot be generated in GR mode in case of graceful restart 
         * (or) in standby node incase of redundancy */
        return;
    }

    if (pOspfCxt->bAreaBdrRtr != OSPF_TRUE)
    {
        return;
    }

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pType5LsaInfo = OSPF_GET_BASE_PTR (tLsaInfo,
                                                   nextLsaInfo, pLstNode);

                if (pType5LsaInfo->u1TrnsltType5 == OSPF_TRUE)
                {
                    pType5LsaInfo->u1TrnsltType5 = OSPF_FALSE;
                }
            }
        }

    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if ((pArea->u4AreaType == NSSA_AREA) &&
            (pArea->u1NssaTrnsltrState == TRNSLTR_STATE_ENAELCTD))
        {
            /* Initialize the Lsacount of the Ranges before starting the 
               Translation */

            for (u4Index = MIN_ADDR_RANGES_PER_AREA;
                 u4Index < MAX_ADDR_RANGES_PER_AREA; u4Index++)
            {
                pCurrRange = &(pArea->aAddrRange[u4Index]);
                if ((pCurrRange->areaAggStatus != ACTIVE) &&
                    (pCurrRange->areaAggStatus != NOT_IN_SERVICE))
                {
                    /* Address Range is maintained in sorted order. 
                     * First empty Row indicates no more valid address 
                     * ranges */
                    break;
                }

                if (pCurrRange->u1LsdbType != NSSA_EXTERNAL_LINK)
                {
                    continue;
                }

                pCurrRange->u4LsaCount = 0;
                for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                {
                    pCurrRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
                    pCurrRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
                }
            }
            TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
                NssaType7LsaTranslation (pArea, pLsaInfo);
            }

            NssaAdvertiseType7Rngs (pArea);

            if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than relinquished interval */
                if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                    && (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
                {
                    gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                    OSPFRtcRelinquish (pOspfCxt);
                }
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: NssaType7To5TranslatorInCxt\n");
}

/****************************************************************/
/*                                   */
/* Function     : NssaUpdtRngForLsa                */
/*                                   */
/* Description    :  Updates the cost/path type for range   */
/*                     by not considering the passed Type 7 LSA*/
/*                   */
/*                                     */
/* Input          : pArea    - Pointer to the area             */
/*          pLsaInfo - Pointer to tLsaInfo for Type 7 LSA */
/*          u4Indice - Index of Rng best mtchng this LSA     */
/*                                     */
/* Output       : None                       */
/*                                    */
/* Returns      : VOID                        */
/*                                     */
/****************************************************************/
PUBLIC VOID
NssaUpdtRngForLsa (tArea * pArea, UINT4 u4Indice)
{
    UINT4               u4RangeAddr;
    UINT4               u4RangeMask;
    UINT4               u4LsaAddr;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tLsaInfo           *pScanLsaInfo = NULL;
    UINT4               u4MtchdRangeMask = RAG_NO_CHNG;
    UINT4               u4Index;
    tAddrRange         *pCurrRange = NULL;
    tAddrRange         *pCurrMtchdRange = NULL;
    tAddrRange         *pType7Range = NULL;
    UINT4               u4MtchIndice = RAG_NO_CHNG;
    UINT1               u1MtchRng = OSPF_FALSE;
    UINT1               u1Tos;

    pType7Range = &(pArea->aAddrRange[u4Indice]);
    pType7Range->u4LsaCount = RAG_NO_CHNG;
    pType7Range->pLsaInfo = NULL;
    pType7Range->u4RngUpdt = OSPF_TRUE;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pType7Range->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
        pType7Range->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
    }

    TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
    {
        u4MtchdRangeMask = RAG_NO_CHNG;

        pScanLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

        if (IS_MAX_AGE (pScanLsaInfo->u2LsaAge) ||
            (pScanLsaInfo->u1TrnsltType5 == FLUSHED_LSA))
        {
            continue;
        }

        for (u4Index = MIN_ADDR_RANGES_PER_AREA;
             u4Index < MAX_ADDR_RANGES_PER_AREA; u4Index++)
        {
            pCurrRange = &(pArea->aAddrRange[u4Index]);

            if ((pCurrRange->areaAggStatus != ACTIVE) &&
                (pCurrRange->areaAggStatus != NOT_IN_SERVICE))
            {
                /* Address Range is maintained in sorted order. First empty
                   Row indicates no more valid address ranges */
                break;
            }

            if (pCurrRange->u1LsdbType != NSSA_EXTERNAL_LINK)
            {
                continue;
            }

            u4RangeAddr = OSPF_CRU_BMC_DWFROMPDU (pCurrRange->ipAddr);
            u4RangeMask = OSPF_CRU_BMC_DWFROMPDU (pCurrRange->ipAddrMask);
            u4LsaAddr =
                OSPF_CRU_BMC_DWFROMPDU (pScanLsaInfo->lsaId.linkStateId);
            if ((IS_THIS_PART_OF_OSPF_AGGR_ROUTE
                 (u4LsaAddr, u4RangeAddr, u4RangeMask))
                && (pCurrRange->areaAggStatus == ACTIVE))
            {
                /* There is a range which has a greater mask matching the earlier 
                   matched range. The new range is the one to be checked */
                if (u4MtchdRangeMask < u4RangeMask)
                {
                    u4MtchdRangeMask = u4RangeMask;
                    pCurrMtchdRange = pCurrRange;
                    u1MtchRng = OSPF_TRUE;
                    u4MtchIndice = u4Index;
                    /* We continue further to check for higher 
                       range if available 
                     */
                }
            }
        }

        if ((u1MtchRng == OSPF_TRUE) && (u4MtchIndice == u4Indice))
        {
            pCurrMtchdRange->u4LsaCount++;

            if (pCurrMtchdRange->u4LsaCount == OSPF_TRUE)
            {
                pCurrMtchdRange->pLsaInfo = pScanLsaInfo;
            }
            else
            {
                pCurrMtchdRange->pLsaInfo = NULL;
            }

            NssaUpdtRngCostType (pScanLsaInfo, pCurrMtchdRange);
        }
    }
}

/****************************************************************/
/*                                   */
/* Function     : NssaUpdtRngCostType                */
/*                                   */
/* Description    : This routine updates range cost    */
/*                    */
/*                .                     */
/*                                     */
/* Input          : pLsaInfo - Pointer to tLsaInfo for Type 7 LSA*/
/*              type7Rng - Type 7 range */
/*                                     */
/* Output       : None                       */
/*                                    */
/* Returns      : VOID       */
/*                        */
/****************************************************************/
PUBLIC VOID
NssaUpdtRngCostType (tLsaInfo * pLsaInfo, tAddrRange * pAddrRng)
{

    tExtLsaLink         extLsaLink;
    UINT1               u1Tos;
    INT1                i1RetValue;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        if ((i1RetValue = RtcGetExtLsaLink (pLsaInfo->pLsa,
                                            pLsaInfo->u2LsaLen, u1Tos,
                                            &extLsaLink)) == OSPF_FAILURE)
        {
            continue;
        }

        if (extLsaLink.u1MetricType == pAddrRng->aMetric[u1Tos].u4MetricType)
        {
            if ((pAddrRng->aMetric[u1Tos].u4Value == LS_INFINITY_24BIT) ||
                (extLsaLink.u4Cost > pAddrRng->aMetric[u1Tos].u4Value))
            {
                pAddrRng->aMetric[u1Tos].u4Value = extLsaLink.u4Cost;
                pAddrRng->aMetric[u1Tos].bStatus = OSPF_VALID;
                pAddrRng->u4RngUpdt = OSPF_TRUE;
            }
        }
        else if (extLsaLink.u1MetricType == TYPE_2_METRIC)
        {
            pAddrRng->aMetric[u1Tos].u4Value = extLsaLink.u4Cost;
            pAddrRng->aMetric[u1Tos].u4MetricType = extLsaLink.u1MetricType;
            pAddrRng->aMetric[u1Tos].bStatus = OSPF_VALID;
            pAddrRng->u4RngUpdt = OSPF_TRUE;
        }

    }
}

/****************************************************************/
/*                                 */
/* Function     : NssaTrnsltrStatLost                  */
/*                                      */
/* Description    : This routine flushes self originated         */
/*                aggregated Type 5 LSAs, generated from      */
/*                configured Type 7 Address Range         */
/*                                  */
/* Input          : pArea - Pointer to the area            */
/*                                     */
/* Output       : None                        */
/*                                     */
/* Returns      : VOID                       */
/*                                      */
/****************************************************************/
PUBLIC VOID
NssaTrnsltrStatLost (tArea * pArea)
{
    tLsaInfo           *pLsaInfo = NULL;
    tAddrRange          currRange;
    tIPADDR             rngIpAddr;
    tExtRoute          *pExtRoute = NULL;
    UINT4               u4Index;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_ENTRY: NssaTrnsltrStatLost\n");

    /* Function is invoked when NSSA ABR looses 
       Translator Status either when it is deposed
       by other NSSA ABR or when it looses its 
       ABR status itself.
       It flushes self-originated Type 5 
       as a result of Type 7 aggregation.
       Further it re-originates imported routes if
       they were previously screened by Type 7 
       range translation.
     */

    for (u4Index = MIN_ADDR_RANGES_PER_AREA;
         u4Index < MAX_ADDR_RANGES_PER_AREA; u4Index++)
    {
        currRange = pArea->aAddrRange[u4Index];
        if ((currRange.areaAggStatus == ACTIVE) &&
            (currRange.u1LsdbType == NSSA_EXTERNAL_LINK))
        {
            UtilIpAddrMaskCopy (rngIpAddr,
                                currRange.ipAddr, currRange.ipAddrMask);

            pLsaInfo =
                LsuDatabaseLookUp (AS_EXT_LSA, &(rngIpAddr),
                                   &(currRange.ipAddrMask),
                                   &(pArea->pOspfCxt->rtrId),
                                   NULL, (UINT1 *) pArea->pOspfCxt->pBackbone);

            if ((pLsaInfo != NULL) &&
                (pLsaInfo->pLsaDesc->u1InternalLsaType ==
                 AS_TRNSLTD_EXT_RNG_LSA))
            {
                AgdFlushOut (pLsaInfo);

                pExtRoute = GetFindExtRouteInCxt (pArea->pOspfCxt,
                                                  &currRange.ipAddr,
                                                  &currRange.ipAddrMask);
                if (pExtRoute != NULL)
                {
                    RagAddRouteInAggAddrRangeInCxt (pArea->pOspfCxt, pExtRoute);
                }
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC_EXIT: NssaTrnsltrStatLost\n");
}

/****************************************************************/
/*                                 */
/* Function     : NssaFsmUpdtTrnsltrState                  */
/*                                      */
/* Description    :  Updates Trnsltr state    and takes    */
/*                     necessary action    */
/*                         */
/*                                  */
/* Input          : pArea - Pointer to the area            */
/*              u1PrvState - Previous State         */
/*               u1NewState - New State        */
/*                                     */
/* Output       : None                        */
/*                                     */
/* Returns      : VOID                       */
/*                                      */
/****************************************************************/
PUBLIC VOID
NssaFsmUpdtTrnsltrState (tArea * pArea, UINT1 u1PrvState, UINT1 u1NewState)
{

    if (pArea->u1NssaTrnsltrState != u1NewState)
    {
        pArea->u4NssaTrnsltrEvents++;
    }
    pArea->u1NssaTrnsltrState = u1NewState;

    if ((u1NewState == TRNSLTR_STATE_ENAELCTD) &&
        (u1PrvState == TRNSLTR_STATE_ENAELCTD))
    {
        if (pArea->nssaStbltyIntrvlTmr.i2Status == OSPF_TRUE)
        {
            pArea->nssaStbltyIntrvlTmr.i2Status = OSPF_FALSE;
            TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);
        }
    }
    else if ((u1PrvState != TRNSLTR_STATE_DISABLED) &&
             (u1NewState == TRNSLTR_STATE_DISABLED))
    {
        if (pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            NssaTrnsltrStatLost (pArea);
        }
    }

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osnseln.c                      */
/*-----------------------------------------------------------------------*/
