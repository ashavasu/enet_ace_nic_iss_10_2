/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: osrm.c,v 1.17 2018/02/02 09:47:36 siva Exp $
 *
 * Description:This file contains the ospf high availability handling
 *             routines.
 *
 *******************************************************************/
#ifndef _OSRED_C_
#define _OSRED_C_

#include "osinc.h"
#ifdef NPAPI_WANTED
#include "ipnp.h"
#endif

PRIVATE VOID        OspfRmSendBulkReq (VOID);
PRIVATE VOID        OspfRmSendBulkTail (VOID);
PRIVATE VOID        OspfRmProcessBulkTail (VOID);
PRIVATE VOID        OspfRmHandleRmMessageEvent (tOsRmMsg * pOsRmMsg);
PRIVATE VOID        OspfRmHandleGoActiveEvent (VOID);
PRIVATE VOID        OspfRmHandleGoStandbyEvent (VOID);
PRIVATE VOID        OspfRmHandleStandbyUpEvent (tRmNodeInfo * pRmNode);
PRIVATE VOID        OspfRmHandleStandByDownEvent (tRmNodeInfo * pRmNode);
PRIVATE VOID        OspfRmHandleConfRestoreCompEvt (VOID);
PRIVATE VOID        OspfRmInitBulkUpdateFlags (VOID);
PRIVATE VOID        OspfRmProcessBulkUpdateMsg (tRmMsg * pMsg, UINT2 u2MsgLen);
PRIVATE VOID        OspfRmHandleIfacesOnGoActive (tOspfCxt *);
PRIVATE VOID        OspfRmHandleNbrsOnGoActive (tOspfCxt *);
PRIVATE VOID        OspfRmHandleLsaRegenOnActive (tOspfCxt *);
PRIVATE VOID        OspfRmHwAudit (VOID);

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessRmMsg                                        */
/*                                                                           */
/* Description  : This function receives the RM events and RM message.       */
/*                Based on the events it will call the corresponding         */
/*                functions to process the events.                           */
/*                                                                           */
/* Input        : pOspfRmIfParam - Pointer to the structure containing RM    */
/*                                events                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmProcessRmMsg (tOsRmMsg * pOspfRmIfParam)
{

    switch (pOspfRmIfParam->u1Event)
    {
        case RM_MESSAGE:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received RM_MESSAGE event \r\n");
            OspfRmHandleRmMessageEvent (pOspfRmIfParam);
            break;

        case GO_ACTIVE:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received GO_ACTIVE event \r\n");
            OspfRmHandleGoActiveEvent ();
            break;

        case GO_STANDBY:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received GO_STANDBY event \r\n");
            OspfRmHandleGoStandbyEvent ();
            break;

        case RM_PEER_UP:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received RM_STANDBY_UP event \r\n");
            OspfRmHandleStandbyUpEvent ((tRmNodeInfo *)
                                        (pOspfRmIfParam->pFrame));
            break;

        case RM_PEER_DOWN:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received RM_STANDBY_DOWN event \r\n");
            OspfRmHandleStandByDownEvent ((tRmNodeInfo *)
                                          (pOspfRmIfParam->pFrame));
            break;

        case L2_INITIATE_BULK_UPDATES:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received L2_INITIATE_BULK_UPDATES event \r\n");
            OspfRmSendBulkReq ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received RM_CONFIG_RESTORE_COMPLETE event \r\n");
            OspfRmHandleConfRestoreCompEvt ();
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            OspfRmHandleDynSyncAudit ();
            break;

        case RM_INIT_HW_AUDIT:
            if (OSPF_IS_NODE_ACTIVE () == OSPF_TRUE)
            {
                OspfRmHwAudit ();
            }
            break;

        default:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received invalid event \r\n");
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleRmMessageEvent                                */
/*                                                                           */
/* Description  : This function process the messages set by the peer.        */
/*                This function is invoked whenever OSPF module receive      */
/*                dynamic sync up/bulk update/bulk request message.          */
/*                This function parses the messages received and updates     */
/*                its database. Following are the messages and related       */
/*                message process functions                                  */
/*                OSPF_RED_BULK_UPDT_REQ_MSG                                 */
/*                RM_BULK_UPDT_MSG                                           */
/*                OSPF_RED_BULK_UPDT_TAIL_MSG                                */
/*                OSPF_SYNC_HELLO_PDU                                        */
/*                OSPF_SYNC_IF_STATE_CHNG_EVENT                              */
/*                OSPF_SYNC_NBR_STATE_CHNG_EVENT                             */
/*                OSPF_SYNC_LSU_INFO                                         */
/*                OSPF_SYNC_LSACK_INFO                                       */
/*                                                                           */
/* OspfRm       : pOspfRmMsg -                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
OspfRmHandleRmMessageEvent (tOsRmMsg * pOsRmMsg)
{
    UINT4               u4SeqNum = 0;
    UINT4               u4OsNodeState = gOsRtr.osRedInfo.u4OsRmState;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    /* Read the sequence number from the RM Header */
    RM_PKT_GET_SEQNUM (pOsRmMsg->pFrame, &u4SeqNum);

    /* Remove the RM Header */
    RM_STRIP_OFF_RM_HDR (pOsRmMsg->pFrame, pOsRmMsg->u2Length);

    /* Get the messages type */
    OSPF_RED_GET_1_BYTE (pOsRmMsg->pFrame, u4Offset, u1MsgType);
    OSPF_RED_GET_2_BYTE (pOsRmMsg->pFrame, u4Offset, u2MsgLen);

    if (pOsRmMsg->u2Length != u2MsgLen)
    {
        /* Some thing is wrong. */
        RM_FREE (pOsRmMsg->pFrame);
        OspfRmApiSendProtoAckToRM (u4SeqNum);
        return;
    }

    if ((u4OsNodeState != OSPF_RED_ACTIVE_STANDBY_UP) &&
        (u1MsgType == OSPF_RED_BULK_UPDT_REQ_MSG))
    {
        /* This may happen when the RM channel is disturbed */
        RM_FREE (pOsRmMsg->pFrame);
        OspfRmApiSendProtoAckToRM (u4SeqNum);
        return;
    }

    switch (u1MsgType)
    {
        case OSPF_RED_BULK_UPDT_REQ_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_BULK_UPDT_REQ_MSG message \r\n");
            if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_PEER_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_PEER_UP event.
                 */

                gOsRtr.osRedInfo.b1IsBulkReqRcvd = OSPF_TRUE;
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                                  "Received OSPF_RED_BULK_UPDT_REQ_MSG message. "
                                  "But peer node is not ready \r\n");
                break;
            }

            OspfRmInitBulkUpdateFlags ();
            gOsRtr.osRedInfo.b1IsBulkReqRcvd = OSPF_FALSE;
            gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;
            OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);
            OspfRmSendBulkExtRouteInfo ();
            OspfRmProcessBulkReq ();
            break;

        case OSPF_RED_BULK_UPDATE_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_BULK_UPDATE_MSG message \r\n");
            OspfRmProcessBulkUpdateMsg (pOsRmMsg->pFrame, u2MsgLen);
            break;

        case OSPF_RED_BULK_UPDT_TAIL_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_BULK_UPDT_TAIL_MSG message \r\n");
            OspfRmProcessBulkTail ();
            break;

        case OSPF_RED_SYNC_HELLO_PDU_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_HELLO_PDU_MSG message \r\n");
            OspfRmProcessHello (pOsRmMsg->pFrame, &u4Offset);
            break;

        case OSPF_RED_SYNC_IFSTATE_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_IF_STATE_CHNG_MSG message \r\n");
            OspfRmProcessIfState (pOsRmMsg->pFrame, &u4Offset);
            break;

        case OSPF_RED_SYNC_NBR_STATE_CHNG_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_NBR_STATE_CHNG_MSG message \r\n");

            OspfRmProcessNbrStateChange (pOsRmMsg->pFrame);
            break;

        case OSPF_RED_SYNC_LSU_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_LSU_MSG message \r\n");

            OspfRmProcessLSA (pOsRmMsg->pFrame, &u4Offset);
            break;

        case OSPF_RED_SYNC_LSACK_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_LSACK_MSG message \r\n");
            OspfRmProcessLsAckInfo (pOsRmMsg->pFrame);
            break;

        case OSPF_RED_SYNC_MD5_SEQ_NO_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_MD5_SEQ_NO_MSG message \r\n");
            OspfRmProcessMd5SeqNo (pOsRmMsg->pFrame);
            break;

        default:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received invalid message \r\n");
            break;
    }

    /* Sending ACK to RM */
    RM_FREE (pOsRmMsg->pFrame);
    OspfRmApiSendProtoAckToRM (u4SeqNum);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessBulkUpdateMsg                                */
/*                                                                           */
/* Description  : This function processes the bulk update message received.  */
/*                                                                           */
/* Input        : pMsg - Bulk update messages                                */
/*                u2MsgLen - Message length                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmProcessBulkUpdateMsg (tRmMsg * pMsg, UINT2 u2MsgLen)
{
    UINT1               u1BulkUpdType = 0;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4ContextId = OSPF_DEFAULT_CXT_ID;

    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;

    /* Read the sub bulk update message type */
    OSPF_CRU_BMC_GET_1_BYTE (pMsg, OSPF_RED_MSG_HDR_SIZE, u1BulkUpdType);

    switch (u1BulkUpdType)
    {
        case OSPF_RED_SYNC_IFSTATE_MSG:
        case OSPF_RED_SYNC_HELLO_PDU_MSG:
        case OSPF_RED_SYNC_NBR_STATE_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_NBR_STATE_MSG message \r\n");
            OspfRmProcessNbrInfo (pMsg);
            break;

        case OSPF_RED_SYNC_LSU_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_LSU_MSG "
                              "bulk update message \r\n");
            OspfRmProcessBulkLsuInfo (pMsg, u2MsgLen);
            break;
        case OSPF_RED_SYNC_EXT_RT_MSG:
            OspfRmProcessBulkExtRouteInfo (pMsg);
            break;
        case OSPF_RED_SYNC_DBOVERFLOW_TMR_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_DBOVERFLOW_TMR_MSG message \r\n");
            OspfRmProcessTmrInfo (pMsg);
            break;
        case OSPF_RED_SYNC_DEF_CXT_MSG:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received OSPF_RED_SYNC_DEF_CXT_MSG message \r\n");
            pOspfCxt = UtilOspfGetCxt (u4ContextId);
            if (pOspfCxt != NULL)
            {
                RtrDeleteCxt (pOspfCxt);
            }
            break;
        default:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Received invalid message \r\n");
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessBulkTail                                     */
/*                                                                           */
/* Description  : This function sends the bulk update complete messages      */
/*                to RM. Sends the RM_PROTOCOL_BULK_UPDT_COMPLETION event    */
/*                to the RM.                                                 */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmProcessBulkTail ()
{
    tRmProtoEvt         ProtoEvt;
    INT4                i4RetVal = OSPF_FAILURE;

    ProtoEvt.u4AppId = RM_OSPF_APP_ID;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
    ProtoEvt.u4Error = RM_NONE;

    i4RetVal = OspfRmSendEventToRm (&ProtoEvt);

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Sending Bulk Update completion event "
                          "(RM_PROTOCOL_BULK_UPDT_COMPLETION) to RM failed.\r\n");
    }
    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_COMPLETED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleGoActiveEvent                                 */
/*                                                                           */
/* Description  : This function handles the GO_ACTIVE event from RM.         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmHandleGoActiveEvent ()
{
    tRmProtoEvt         ProtoEvt;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4NodeState = gOsRtr.osRedInfo.u4OsRmState;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    INT4                i4RetVal = OSPF_FAILURE;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    gOsRtr.osRedInfo.u4PeerCount = OspfRmGetStandbyNodeCount ();
    if (gOsRtr.osRedInfo.u4PeerCount > 0)
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_UP;
    }
    else
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_DOWN;
    }

    if (u4NodeState == OSPF_RED_INIT)
    {

        /* Even when the node is in INIT state, configurations can be made.
         * Framework has to take care of it. Hence check the protocol
         * contexts admin status and enable it the accordingly.
         */

        for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
        {
            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

            if ((pOspfCxt != NULL) && (pOspfCxt->admnStat == OSPF_ENABLED))
            {
                RtrSetProtocolStatusInCxt (u4OspfCxtId, OSPF_ENABLED);
            }
        }

        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (u4NodeState == OSPF_RED_STANDBY)
    {
#if defined (MBSM_WANTED) && defined (NPAPI_WANTED) && defined (CFA_WANTED)
        if (gOsRtr.u4ActiveCxtCount != 0)
        {
            if (OspfFsNpCfaModifyFilter (GO_ACTIVE) != FNP_SUCCESS)
            {
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC | ALL_FAILURE_TRC,
                                  OSPF_INVALID_CXT_ID,
                                  "Failed to Modify Filter CFA_NP_OSPF Action\r\n");
            }
        }
#endif

        OspfRmInitBulkUpdateFlags ();

        /* Start all the required timers and flush the Rxmitlist */
        for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
        {
            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

            if ((pOspfCxt != NULL) && (pOspfCxt->admnStat == OSPF_ENABLED))
            {
                OspfRmHandleNbrsOnGoActive (pOspfCxt);
                OspfRmHandleIfacesOnGoActive (pOspfCxt);
                OspfRmHandleLsaRegenOnActive (pOspfCxt);

                /* Start the trap limit timer */
                pOspfCxt->u1TrapCount = 0;
                TmrSetTimer (&(pOspfCxt->trapLimitTimer), TRAP_LIMIT_TIMER,
                             TRAP_LIMIT_TIMER_INTERVAL);
                NssaType7To5TranslatorInCxt (pOspfCxt);
                RtcCheckTranslationInCxt (pOspfCxt);
            }

        }

        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    else
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Received GO_ACTIVE event while the node is neither "
                          "in INIT nor STANDBY state \r\n");
        return;
    }

    OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);

    /* send go active complete event to RM */
    ProtoEvt.u4AppId = RM_OSPF_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    i4RetVal = OspfRmSendEventToRm (&ProtoEvt);

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_EXT_GBL_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                           "Send event %s to RM failed \r\n",
                           ((ProtoEvt.u4Event ==
                             RM_STANDBY_TO_ACTIVE_EVT_PROCESSED) ?
                            "RM_STANDBY_TO_ACTIVE_EVT_PROCESSED" :
                            "RM_IDLE_TO_ACTIVE_EVT_PROCESSED"));
    }
    else
    {
        OSPF_EXT_GBL_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                           "Send event %s to RM success \r\n",
                           ((ProtoEvt.u4Event ==
                             RM_STANDBY_TO_ACTIVE_EVT_PROCESSED) ?
                            "RM_STANDBY_TO_ACTIVE_EVT_PROCESSED" :
                            "RM_IDLE_TO_ACTIVE_EVT_PROCESSED"));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleIfacesOnGoActive                              */
/*                                                                           */
/* Description  : This function Scan through all the interfaces in the       */
/*                context and start the hello timer/pool timer.              */
/*                                                                           */
/* Input        : pOspfCxt                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
OspfRmHandleIfacesOnGoActive (tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tInterface         *pVirtIf = NULL;
    tTMO_SLL_NODE      *pVirtIfNode = NULL;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pNode);
        if ((pInterface->operStatus == OSPF_DISABLED) ||
            (pInterface->admnStatus == OSPF_DISABLED))
        {
            continue;
        }

        TmrHelloTimer (pInterface);

        /* If the interface is NBMA or PTMP, call the poll timer expiry */
        if ((pInterface->u1NetworkType == IF_NBMA) ||
            (pInterface->u1NetworkType == IF_PTOMP))
        {
            TmrPollTimer (pInterface);
        }
        if (pInterface->u1IsmState == IFS_WAITING)
        {
            TmrSetTimer (&(pInterface->waitTimer), WAIT_TIMER,
                         NO_OF_TICKS_PER_SEC * (pInterface->i4RtrDeadInterval));
        }
    }
    /* Scan the virtual interface list and start the hello timer if interface is up */
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pVirtIfNode, tTMO_SLL_NODE *)
    {
        pVirtIf = GET_IF_PTR_FROM_SORT_LST (pVirtIfNode);

        if (pVirtIf->operStatus == OSPF_DISABLED)
        {
            continue;
        }

        TmrHelloTimer (pVirtIf);

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleNbrsOnGoActive                                */
/*                                                                           */
/* Description  : This function Scan through all the neighbors in the        */
/*                context. If the neighbor is in the EXSTART it flush the    */
/*                LSA Req list and if it is in the FULL state it starts      */
/*                the LSA retrasmission timer.                               */
/*                                                                           */
/* Input        : pOspfCxt                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
OspfRmHandleNbrsOnGoActive (tOspfCxt * pOspfCxt)
{

    tNeighbor          *pNbr = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tNeighbor          *pVirtNbr = NULL;
    tTMO_SLL_NODE      *pVirtNbrNode = NULL;
    tInterface         *pVirtIf = NULL;
    tTMO_SLL_NODE      *pVirtIfNode = NULL;
    UINT4               u4RxmtTmrInterval = 0;
    /* Scan throough all the neighbors in the database. In case of standby to
     * active, the neighbors state can be in INIT/TWO-WAY/EXSTART/FULL state.
     * If the neighbor is in EXSTART state and the router is capable of forming
     * full adjacency with attached neighbor, start the DDP exchange process
     */
    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_SORT_LST (pNode);

        if ((pNbr->u1NsmState == NBRS_DOWN) &&
            (pNbr->u1ConfigStatus == CONFIGURED_NBR))
        {
            continue;
        }
        if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface) &&
            (pNbr->u1NsmState == NBRS_FULL))
        {
            /* no need to re-start inactivity timer for demand circuit on 
             * P2p/P2MP and Virtual interfaces */
            pNbr->bHelloSuppression = OSPF_TRUE;
        }
        else
        {

            /* Start the nbr Inactive timer */
            TmrSetTimer (&pNbr->inactivityTimer, INACTIVITY_TIMER,
                         NO_OF_TICKS_PER_SEC *
                         (pNbr->pInterface->i4RtrDeadInterval));
        }

        if (pNbr->u1NsmState == NBRS_EXSTART)
        {
            LsuClearRxmtLst (pNbr);
            LrqClearLsaReqLst (pNbr);
            DdpClearSummary (pNbr);

            NbrUpdateState (pNbr, NBRS_EXSTART);

            if (pNbr->dbSummary.seqNum == 0)
            {
                OsixGetSysTime ((tOsixSysTime *) & (pNbr->dbSummary.seqNum));
            }
            else
            {
                pNbr->dbSummary.seqNum++;
            }
            pNbr->dbSummary.bMaster = OSIX_FALSE;

            DdpSendDdp (pNbr);
        }
        else if (pNbr->u1NsmState == NBRS_FULL)
        {

            /* Since the neighbor is in FULL state, send the LSA to the
             * neighbor present in the neighbor rxmt list
             */
            if (pNbr->lsaRxmtDesc.u4LsaRxmtCount > 0)
            {
                u4RxmtTmrInterval = NO_OF_TICKS_PER_SEC *
                    (pNbr->pInterface->u2RxmtInterval);
                TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer),
                             LSA_RXMT_TIMER,
                             ((pNbr->pInterface->u1NetworkType ==
                               IF_BROADCAST) ?
                              UtilJitter (u4RxmtTmrInterval, OSPF_JITTER) :
                              u4RxmtTmrInterval));
            }
        }
    }
    /* Scan through all the virtual neighbors in the database. In case of standby to
     * active, the neighbors state can be in DOWN/INIT/TWO-WAY/EXSTART/FULL state.
     * If the neighbor is in EXSTART state and the router is capable of forming
     * full adjacency with attached neighbor, start the DDP exchange process
     */
    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pVirtIfNode, tTMO_SLL_NODE *)
    {
        pVirtIf = GET_IF_PTR_FROM_SORT_LST (pVirtIfNode);

        TMO_SLL_Scan (&(pVirtIf->nbrsInIf), pVirtNbrNode, tTMO_SLL_NODE *)
        {
            pVirtNbr = GET_NBR_PTR_FROM_NBR_LST (pVirtNbrNode);
            if (pVirtNbr->u1NsmState == NBRS_DOWN)
            {
                continue;
            }
            if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF
                (pVirtNbr->pInterface) && (pVirtNbr->u1NsmState == NBRS_FULL))
            {
                /* no need to re-start inactivity timer for demand circuit on 
                 * Virtual interfaces */
                pVirtNbr->bHelloSuppression = OSPF_TRUE;
            }
            else
            {

                /* Start the nbr Inactive timer */
                TmrSetTimer (&pVirtNbr->inactivityTimer, INACTIVITY_TIMER,
                             NO_OF_TICKS_PER_SEC *
                             (pVirtNbr->pInterface->i4RtrDeadInterval));
            }

            if (pVirtNbr->u1NsmState == NBRS_EXSTART)
            {
                LsuClearRxmtLst (pVirtNbr);
                LrqClearLsaReqLst (pVirtNbr);
                DdpClearSummary (pVirtNbr);

                NbrUpdateState (pVirtNbr, NBRS_EXSTART);

                if (pVirtNbr->dbSummary.seqNum == 0)
                {
                    OsixGetSysTime ((tOsixSysTime *) &
                                    (pVirtNbr->dbSummary.seqNum));
                }
                else
                {
                    pVirtNbr->dbSummary.seqNum++;
                }
                pVirtNbr->dbSummary.bMaster = OSIX_FALSE;

                DdpSendDdp (pVirtNbr);
            }
            else if (pVirtNbr->u1NsmState == NBRS_FULL)
            {
                /* Since the neighbor is in FULL state, send the LSA to the
                 * neighbor present in the neighbor rxmt list
                 */
                if (pVirtNbr->lsaRxmtDesc.u4LsaRxmtCount > 0)
                {
                    LsuRxmtLsu (pVirtNbr);
                    u4RxmtTmrInterval = NO_OF_TICKS_PER_SEC *
                        (pVirtNbr->pInterface->u2RxmtInterval);
                    TmrSetTimer (&(pVirtNbr->lsaRxmtDesc.lsaRxmtTimer),
                                 LSA_RXMT_TIMER, u4RxmtTmrInterval);
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleLsaRegenOnActive                               */
/*                                                                           */
/* Description  : This function Scan through all the self orginated          */
/*                LSA descriptors and signals origination of                 */
/*                next instance of the self orginated LSA                    */
/*                                                                           */
/* Input        : pOspfCxt                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
OspfRmHandleLsaRegenOnActive (tOspfCxt * pOspfCxt)
{
    tTMO_DLL_NODE      *pLstNode = NULL;
    tLsaDesc           *pLsaDesc = NULL;
    UINT2               u2Interval = OSPF_ZERO;

    TMO_DLL_Scan (&(pOspfCxt->origLsaDescLst), pLstNode, tTMO_DLL_NODE *)
    {
        pLsaDesc = GET_LSADESC_FROM_LSADESCLST (pLstNode);
        if ((IS_MAX_AGE (pLsaDesc->pLsaInfo->u2LsaAge)) &&
            (pLsaDesc->pLsaInfo->u4RxmtCount > 0))
        {
            if (pLsaDesc->u1MinLsaIntervalExpired != OSPF_TRUE)
            {
                TmrDeleteTimer (&(pLsaDesc->minLsaIntervalTimer));
                pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
            }
            /* This LSA is Max Age LSA and added to retransmission List
             * This LSA will be retransmitted to the intended neighbors and
             * will be deleted from the Database on reception of ACK from the
             * neighbors. No need to re-orginated this LSA */
            continue;
        }

        if (UtilIpAddrComp (pLsaDesc->pLsaInfo->lsaId.advRtrId,
                            pOspfCxt->rtrId) == OSPF_EQUAL)
        {
            pLsaDesc->u1LsaChanged = OSPF_TRUE;
            pLsaDesc->u1MinLsaIntervalExpired = OSPF_FALSE;
            u2Interval = (UINT2)
                UtilJitter ((NO_OF_TICKS_PER_SEC * pOspfCxt->u2MinLsaInterval),
                            OSPF_JITTER);

            TmrRestartTimer (&(pLsaDesc->minLsaIntervalTimer),
                             MIN_LSA_INTERVAL_TIMER, u2Interval);
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleGoStandbyEvent                                */
/*                                                                           */
/* Description  : This function handles the GO_STANDBY event from RM         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmHandleGoStandbyEvent ()
{
    tRmProtoEvt         ProtoEvt;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OsNodeState = gOsRtr.osRedInfo.u4OsRmState;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    INT4                i4RetVal = OSPF_FAILURE;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if ((u4OsNodeState == OSPF_RED_STANDBY) || (u4OsNodeState == OSPF_RED_INIT))
    {
        /* Incase of current node state is RM_INIT, Discard this
         * event, Node will become standby on RM_CONFIG_RESTORE_COMPLETE.
         */
        OSPF_EXT_GBL_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                           "Received GO_STANDBY event when the node is in "
                           "%s state \r\n",
                           ((u4OsNodeState ==
                             OSPF_RED_STANDBY) ? "OSPF_RED_STANDBY" :
                            "OSPF_RED_INIT"));
        return;
    }
    else if ((u4OsNodeState == OSPF_RED_ACTIVE_STANDBY_UP) ||
             (u4OsNodeState == OSPF_RED_ACTIVE_STANDBY_DOWN))
    {
#if defined (MBSM_WANTED) && defined (NPAPI_WANTED) && defined (CFA_WANTED)
        if (gOsRtr.u4ActiveCxtCount != 0)
        {
            if (OspfFsNpCfaModifyFilter (GO_STANDBY) != FNP_SUCCESS)
            {
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC | ALL_FAILURE_TRC,
                                  OSPF_INVALID_CXT_ID,
                                  "Failed to Modify Filter CFA_NP_OSPF Standby\r\n");
            }
        }
#endif

        OspfRmInitBulkUpdateFlags ();
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_STANDBY;
        gOsRtr.osRedInfo.u4PeerCount = 0;
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_NOT_STARTED;
        OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);

        for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
        {
            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

            if ((pOspfCxt != NULL) && (pOspfCxt->admnStat == OSPF_ENABLED))
            {
                /* Disable and Enable the context */
                RtrSetProtocolStatusInCxt (u4OspfCxtId, OSPF_DISABLED);
                RtrSetProtocolStatusInCxt (u4OspfCxtId, OSPF_ENABLED);
            }
        }

        /* Intimate RM about the completion of GO_STANDBY process */
        ProtoEvt.u4AppId = RM_OSPF_APP_ID;
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        ProtoEvt.u4Error = RM_NONE;

        i4RetVal = OspfRmSendEventToRm (&ProtoEvt);

        if (i4RetVal != OSPF_SUCCESS)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Sending event RM_STANDBY_EVT_PROCESSED to "
                              "RM failed \r\n");
        }
        else
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Sending event RM_STANDBY_EVT_PROCESSED to "
                              "RM success \r\n");
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleStandbyUpEvent                                */
/*                                                                           */
/* Description  : This function handles the Standby up event.                */
/*                                                                           */
/* Input        : pRmNode - info contains number of Stanby nodes             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmHandleStandbyUpEvent (tRmNodeInfo * pRmNode)
{
    BOOL1               b1Value = OSPF_FALSE;
    UINT4               u4NodeStatus = gOsRtr.osRedInfo.u4OsRmState;

    if (u4NodeStatus == OSPF_RED_STANDBY)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Got the Standby Up event at standby \r\n");
        OspfRmRelRmMsgMem ((UINT1 *) pRmNode);
        return;
    }
    gOsRtr.osRedInfo.u4PeerCount = pRmNode->u1NumStandby;

    if (pRmNode->u1NumStandby > 0)
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_UP;
    }
    else
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_DOWN;
    }

    OspfRmRelRmMsgMem ((UINT1 *) pRmNode);

    b1Value = gOsRtr.osRedInfo.b1IsBulkReqRcvd;

    if (b1Value == OSPF_TRUE)
    {
        gOsRtr.osRedInfo.b1IsBulkReqRcvd = OSPF_FALSE;

        /* Bulk request msg is recieved before RM_PEER_UP event.
         * So we are sending bulk updates now.
         */
        OspfRmInitBulkUpdateFlags ();

        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;

        OspfRmProcessBulkReq ();
    }
    OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessBulkReq                                      */
/*                                                                           */
/* Description  : This function handles when active node gets a bulk         */
/*                request from the standby node.It call the corresponing     */
/*                module to send the bulk updates.                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmProcessBulkReq ()
{
    INT4                i4NxtBulkMod = gOsRtr.osRedInfo.u1BulkUpdModuleStatus;
    INT4                i4RetValue = 0;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4ContextId = OSPF_DEFAULT_CXT_ID;

    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is not in active State");
        return;
    }

    /* Check If istandby exist if not return */
    if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "No standby Node is existing");
        return;
    }

    /* Get the current bulk update start time */
    gOsRtr.osRedInfo.u4CurrentBulkStartTime = OsixGetSysUpTime ();

    switch (i4NxtBulkMod)
    {
        case OSPF_RED_NBR_MOD:
            OspfRmSendBulkNbrInfo ();
            break;
        case OSPF_RED_VIRT_NBR_MOD:
            OspfRmSendBulkVirtNbrInfo ();
            break;
        case OSPF_RED_LSDB_MOD:
            OspfRmSendBulkLsaInfo ();
            break;
        case OSPF_RED_TMR_MOD:
            OspfRmSendBulkTmrInfo ();
            break;
        case OSPF_RED_DEF_CXT_MOD:
            pOspfCxt = UtilOspfGetCxt (u4ContextId);
            if (pOspfCxt == NULL)
            {
                OspfRmSendBulkDefCxtInfo ();
            }
            else
            {
                gOsRtr.osRedInfo.u4DynBulkUpdatStatus =
                    OSPF_RED_BLKUPDT_COMPLETED;
            }
            break;
        default:
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Invalid  bulk update module \r\n");
            return;
    }

    i4RetValue = gOsRtr.osRedInfo.u4DynBulkUpdatStatus;

    if (i4RetValue == OSPF_RED_BLKUPDT_COMPLETED)
    {
        gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_MOD_COMPLETED;
        OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);
        OspfRmSendBulkTail ();
    }

    if (i4RetValue == OSPF_RED_BLKUPDT_INPROGRESS)
    {
        i4RetValue = OsixSendEvent (SELF, (const UINT1 *) "OSPF",
                                    OSPF_RED_SUB_BULK_UPD_EVENT);

        if (i4RetValue != OSIX_SUCCESS)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Sending OSPF_RED_SUB_BULK_UPD_EVENT failed\r\n");
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendBulkTail                                        */
/*                                                                           */
/* Description  : This function is called when active node has completed     */
/*                sending all the bulk updates or it has no bulk updates     */
/*                to send to the standby. This function constucts and        */
/*                sends the bulk tail message.                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmSendBulkTail ()
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSPF_FAILURE;
    UINT2               u2MsgLen = (UINT2) OSPF_RED_BULK_TAIL_MSG_LEN;
    UINT1               u1MsgType = (UINT1) OSPF_RED_BULK_UPDT_TAIL_MSG;

    /* Update the bulkupdate completed status to RM */
    OspfRmSetBulkUpdateStatus ();

    pRmMsg = OspfRmAllocForRmMsg (u2MsgLen);

    if (pRmMsg == NULL)
    {
        return;
    }

    /* form bulk tail message */
    OSPF_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPF_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);

    i4RetVal = OspfRmSendMsgToRm (pRmMsg, u2MsgLen);

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Sending bulk tail to RM is failed \r\n");
    }
    else
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Sending bulk tail to RM is success \r\n");
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleStandByDownEvent                              */
/*                                                                           */
/* Description  : This function handles the Standby down event               */
/*                                                                           */
/* Input        : pRmNode - info contains number of Stanby nodes             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmHandleStandByDownEvent (tRmNodeInfo * pRmNode)
{
    gOsRtr.osRedInfo.u4PeerCount = pRmNode->u1NumStandby;

    if (gOsRtr.osRedInfo.u4OsRmState == OSPF_RED_STANDBY)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Got the Standby Down event at standby \r\n");
        OspfRmRelRmMsgMem ((UINT1 *) pRmNode);
        return;
    }
    if (pRmNode->u1NumStandby > 0)
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_UP;
    }
    else
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_ACTIVE_STANDBY_DOWN;
    }

    OspfRmRelRmMsgMem ((UINT1 *) pRmNode);
    OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendBulkReq                                         */
/*                                                                           */
/* Description  : This function send the bulk update request.                */
/*                                                                           */
/* Input        : Nine                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmSendBulkReq ()
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = OSPF_FAILURE;
    UINT2               u2MsgLen = (UINT2) OSPF_RED_BULK_UPD_REQ_MSG_LEN;
    UINT1               u1MsgType = (UINT1) OSPF_RED_BULK_UPDT_REQ_MSG;

    pRmMsg = OspfRmAllocForRmMsg (u2MsgLen);

    if (pRmMsg == NULL)
    {
        /* Log */
        return;
    }

    /* form bulk update request message */
    OSPF_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPF_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);

    i4RetVal = OspfRmSendMsgToRm (pRmMsg, u2MsgLen);

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Sending bulk request to RM is failed \r\n");
    }
    else
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Sending bulk request to RM is success \r\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleConfRestoreCompEvt                          */
/*                                                                           */
/* Description  : This function handle the event  RM_CONFIG_RESTORE_COMPLETE */
/*                received from the RM.Move the node to STANDBY state from   */
/*                INIT.                                                      */
/*                                                                           */
/* Input        : Nine                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmHandleConfRestoreCompEvt ()
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4RmNodeState = OspfRmGetRmNodeState ();
    UINT4               u4OsNodeState = gOsRtr.osRedInfo.u4OsRmState;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4OsNodeState == OSPF_RED_INIT) && (u4RmNodeState == RM_STANDBY))
    {
        gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_STANDBY;
        OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);

        for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
        {
            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

            if ((pOspfCxt != NULL) && (pOspfCxt->bIsOspfEnabled == OSPF_TRUE))
            {
                RtrSetProtocolStatusInCxt (u4OspfCxtId, OSPF_ENABLED);
                pOspfCxt->bIsOspfEnabled = OSPF_FALSE;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : OspfRmHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
OspfRmHandleDynSyncAudit (VOID)
{
/*On receiving this event, ospf should execute show cmd and calculate checksum*/
    OspfExecuteCmdAndCalculateChkSum ();
}

/*****************************************************************************/
/* Function Name      : OspfRmHwAudit                                        */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
OspfRmHwAudit (VOID)
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4OspfCxtId;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4               u4BitMap = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfOperStat = OSPF_DISABLED;
    /* Scan the interface list in all the context */

    /* Get the first context */
    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        return;
    }

    do
    {
        /* Get the context pointer corresponding to the
         * context structure */
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

        if (pOspfCxt == NULL)
        {
            /* Invalid context id */
            break;
        }

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);

            if (u4IfIndex == 0)
            {
                u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4AddrlessIf);
            }
            if (NetIpv4GetIfInfo (u4IfIndex, &NetIpv4IfInfo) == NETIPV4_FAILURE)
            {
                break;
            }
            u1IfOperStat = (NetIpv4IfInfo.u4Oper == IPIF_OPER_ENABLE) ?
                OSPF_ENABLED : OSPF_DISABLED;
            if (pInterface->operStatus != u1IfOperStat)
            {
                u4BitMap = OPER_STATE_MASK;
                OspfIfStateChgHdlr (&NetIpv4IfInfo, u4BitMap);
            }
        }

        u4OspfPrevCxtId = u4OspfCxtId;

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmInitBulkUpdateFlags                                 */
/*                                                                           */
/* Description  : This function initialize the flags used to bulk update.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmInitBulkUpdateFlags (VOID)
{
    gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_NBR_MOD;
    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_NOT_STARTED;
    /* Last bulk update information for interface */
    OspfRmInitNbrBulkUpdateFlags ();
    /* Last bulk update information for Virtual interface */
    OspfRmInitVirtNbrBulkUpdateFlags ();
    /* Last bulk update information for LSDB */
    OspfRmInitLsdbBulkUpdateFlags ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmInitNbrBulkUpdateFlags                               */
/*                                                                           */
/* Description  : This function initialize the flags used to relinquish the  */
/*                neighbor bulk update                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmInitNbrBulkUpdateFlags (VOID)
{
    gOsRtr.osRedInfo.u4LastSynCxtId = OSPF_INVALID_CXT_ID;
    /* Last bulk update information for interface */
    IP_ADDR_COPY (gOsRtr.osRedInfo.lastSynIfIpAddr, gNullIpAddr);
    gOsRtr.osRedInfo.u4LastSynIfAddrlessIf = 0;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmInitVirtNbrBulkUpdateFlags                           */
/*                                                                           */
/* Description  : This function initialize the flags used to relinquish the  */
/*                virtual neighbor bulk update                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmInitVirtNbrBulkUpdateFlags (VOID)
{
    gOsRtr.osRedInfo.u4LastSynCxtId = OSPF_INVALID_CXT_ID;
    /* Last bulk update information for Virtual interface */
    IP_ADDR_COPY (gOsRtr.osRedInfo.lastSynVirtRtrId, gNullIpAddr);
    IP_ADDR_COPY (gOsRtr.osRedInfo.lastSynVirtTransitAreaId, gNullIpAddr);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmInitLsdbBulkUpdateFlags                              */
/*                                                                           */
/* Description  : This function initialize the flags used to relinquish the  */
/*                Link state database bulk update                            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmInitLsdbBulkUpdateFlags (VOID)
{
    gOsRtr.osRedInfo.u4LastSynCxtId = OSPF_INVALID_CXT_ID;
    /* Last bulk update information for LSDB */
    IP_ADDR_COPY (gOsRtr.osRedInfo.lastSynAreaId, gNullIpAddr);
    MEMSET (&(gOsRtr.osRedInfo.lsaId), 0, sizeof (tLsaId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmInit                                                */
/*                                                                           */
/* Description  : This function initialize the OSPF RM related objects.      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
OspfRmInit ()
{
    MEMSET (&gOsRtr.osRedInfo, 0, sizeof (tosRedInfo));

    /* Initialize the RM variables */
    gOsRtr.osRedInfo.u4OsRmState = OSPF_RED_INIT;
    gOsRtr.osRedInfo.u4PeerCount = 0;
    gOsRtr.osRedInfo.b1IsBulkReqRcvd = OSPF_FALSE;
    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_NOT_STARTED;
    gOsRtr.osRedInfo.u1AdminState = OSPF_DISABLED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSnmpSendNodeStatusChgTrap                           */
/*                                                                           */
/* Description  : This routine send the trap regarding redundancy status     */
/*                change.                                                    */
/*                                                                           */
/* Input        : i4HotStandbyState - State of the OSPF instance             */
/*                i4DynBlkUpdStatus - Status of dynamic bulk update          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OspfRmSnmpSendNodeStatusChgTrap (INT4 i4HotStandbyState)
{
    tOsRedStatChgTrapInfo OsRedStatChgTrapInfo;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfCxtId = 0;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    OsRedStatChgTrapInfo.i4HotStandbyState = i4HotStandbyState;
    OsRedStatChgTrapInfo.i4DynamicBulkUpdStatus =
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus;

    for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
    {
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

        if ((pOspfCxt != NULL) && (pOspfCxt->admnStat == OSPF_ENABLED))
        {
            IP_ADDR_COPY (OsRedStatChgTrapInfo.RtrId, pOspfCxt->rtrId);

            SnmpifSendTrapInCxt (pOspfCxt, OSPF_RED_STATE_CHANGE_TRAP,
                                 &OsRedStatChgTrapInfo);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendBulkAbort                                        */
/*                                                                           */
/* Description  : This API send the bulk update abort event to RM with the   */
/*                the given error code                                       */
/*                                                                           */
/* Input        : u4ErrCode - Error code.                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmSendBulkAbort (UINT4 u4ErrCode)
{
    tRmProtoEvt         ProtoEvt;
    INT4                i4RetVal = OSPF_FAILURE;

    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
    OspfRmSnmpSendNodeStatusChgTrap (gOsRtr.osRedInfo.u4OsRmState);

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_OSPF_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4Error = u4ErrCode;

    i4RetVal = OspfRmSendEventToRm (&ProtoEvt);

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_EXT_GBL_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                           "Sending event RM_BULK_UPDT_ABORT with error code "
                           "%d failed\r\n", u4ErrCode);
    }
    else
    {
        OSPF_EXT_GBL_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                           "Sending event RM_BULK_UPDT_ABORT with error code "
                           "%d success\r\n", u4ErrCode);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmRtrEnableInCxt                                       */
/*                                                                           */
/* Description  : This function enables the context admin state if the node  */
/*                is not in the OSPF_RED_INIT state. If the node is in       */
/*                OSPF_RED_INIT it set the flag and returns                  */
/*                                                                           */
/* Input        : pOspfCxt - Context.                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfRmRtrEnableInCxt (tOspfCxt * pOspfCxt)
{
    INT4                i4RetStat = OSPF_FAILURE;
    UINT4               u4OsRmState = gOsRtr.osRedInfo.u4OsRmState;

    u4OsRmState = gOsRtr.osRedInfo.u4OsRmState;

    if (u4OsRmState == OSPF_RED_INIT)
    {
        /* Do not enable now. Enable it when RM_CONFIG_RESTORE_COMPLETE
           event is received.
         */
        pOspfCxt->bIsOspfEnabled = OSPF_TRUE;
        i4RetStat = OSPF_SUCCESS;
    }
    else
    {
        i4RetStat = RtrEnableInCxt (pOspfCxt);
    }

    return i4RetStat;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessNbrInfo                                       */
/*                                                                           */
/* Description  : This function processes the neighbor bulk update from the  */
/*                active node. This function invokes OspfRmProcessIfState,   */
/*                or OspfRmProcessHello or OspfRmProcessBulkNbrState based   */
/*                on the sub bulk update type (OSPF_RED_SYNC_IFSTATE_MSG/    */
/*                OSPF_RED_SYNC_HELLO_PDU_MSG/OSPF_RED_SYNC_NBR_STATE_MSG    */
/*                                                                           */
/* Input        : pRmMsg - RM bulk update message                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfRmProcessNbrInfo (tRmMsg * pMsg)
{
    UINT4               u4OffSet = OSPF_ONE;
    UINT1               u1SubMsgType;
    UINT2               u2PktLen = OSPF_ZERO;
    INT4                i4RetVal = OSPF_SUCCESS;

    /* Extract Total Message length */
    OSPF_RED_GET_2_BYTE (pMsg, u4OffSet, u2PktLen);

    while ((u4OffSet < u2PktLen) && (i4RetVal == OSPF_SUCCESS))
    {
        /* Read the SubMessageType */
        OSPF_RED_GET_1_BYTE (pMsg, u4OffSet, u1SubMsgType);
        if (u1SubMsgType == OSPF_RED_SYNC_IFSTATE_MSG)
        {
            i4RetVal = OspfRmProcessIfState (pMsg, &u4OffSet);
        }
        else if (u1SubMsgType == OSPF_RED_SYNC_HELLO_PDU_MSG)
        {
            i4RetVal = OspfRmProcessHello (pMsg, &u4OffSet);
            /* we are dynamically syncing the hello count from active to standby 
               so in bulk update there is no need to increase the hello count */
            gOsRtr.osRedInfo.u4HelloSyncCount--;

        }
        else if (u1SubMsgType == OSPF_RED_SYNC_NBR_STATE_MSG)
        {
            /* Process nbr state bulk update */
            i4RetVal = OspfRmProcessBulkNbrState (pMsg, &u4OffSet);
        }
        else
        {
            /* Error in getting sub message type */
            i4RetVal = OSPF_FAILURE;
        }
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmBulkUpdtRelinquish                                    */
/*                                                                          */
/* Description  : This  function gets the current system time and compares  */
/*                with the current bulk update start time. If the time      */
/*                interval is more than the maximum time taken, then        */
/*                relinquish needs to be done                               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
OsRmBulkUpdtRelinquish (VOID)
{
    UINT4               u4CurrentTime = 0;
    UINT4               u4DeltaTime = 0;

    /* Get the current time */
    u4CurrentTime = OsixGetSysUpTime ();

    /* If the difference between the current time and the current bulk
     * update start time is greater than or equal to delta, then send
     * success to relinquish the bulk update process
     */

    if (gOsRtr.osRedInfo.u4CurrentBulkStartTime > u4CurrentTime)
    {
        /* This is the special scenario where wrap around has occurred.
         * In this case, get the remaining time between max UINT4 value
         * and gOsRtr.osRedInfo.u4CurrentBulkStartTime. Add the result
         * with the current time. Compare the new value with delta
         */
        u4DeltaTime = (OSPF_MAX_TIME -
                       gOsRtr.osRedInfo.u4CurrentBulkStartTime) + u4CurrentTime;
    }
    else
    {
        u4DeltaTime = u4CurrentTime - gOsRtr.osRedInfo.u4CurrentBulkStartTime;
    }

    if (u4DeltaTime >= OSPF_BULK_UPDATE_RELINQUISH_TIME)
    {
        return OSIX_SUCCESS;
    }
    gu4OsRmBulkUpdtRelinquishFail++;
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : OspfExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
OspfExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_OSPF_APP_ID;
    UINT2               u2ChkSum = 0;

    OspfUnLock ();
    if (OspfGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == OSPF_FAILURE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Checksum of calculation failed for VCM\n");
        OspfLock ();
        return;
    }
#ifdef L2RED_WANTED
    if (OspfRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == OSPF_FAILURE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Sending checkum to RM failed\n");
        OspfLock ();
        return;
    }
#else
    UNUSED_PARAM (u2AppId);
#endif
    OspfLock ();
    return;
}
#endif
