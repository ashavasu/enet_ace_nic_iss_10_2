/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osopqtst.c,v 1.10 2014/03/15 14:16:41 siva Exp $
 *
 * Description:This file contains the Snmp low level test  routines for the 
 *             routines for the Opqaue LSA support related objects in the
 *             FutureOSPF Enterprises MIB.
 *
 *******************************************************************/
#include "osinc.h"
#include "ospfcli.h"

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfOpaqueOption
 Input       :  The Indices

                The Object 
                testValFutOspfOpaqueOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfOpaqueOption (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFutOspfOpaqueOption)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4ArrayIndx;
    tAppInfo           *pAppInfo;

    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValFutOspfOpaqueOption)
    {
        case OSPF_DISABLED:

            if (pOspfCxt->u1OpqCapableRtr !=
                (UINT1) i4TestValFutOspfOpaqueOption)
            {
                for (u4ArrayIndx = 0; u4ArrayIndx < MAX_APP_REG; u4ArrayIndx++)
                {
                    pAppInfo = APP_INFO_IN_CXT (pOspfCxt, (UINT1) u4ArrayIndx);
                    if (pAppInfo != NULL)
                    {
                        if (pAppInfo->u1Status == OSPF_VALID)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR (CLI_OSPF_OPAQUE_ENABLED);
                            return SNMP_FAILURE;
                        }
                    }
                }
            }

            if ((pOspfCxt->u1RestartSupport == OSPF_RESTART_BOTH) ||
                (pOspfCxt->u1RestartSupport == OSPF_RESTART_PLANNED))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_OSPF_OPQ_GR_ENABLED);
                return SNMP_FAILURE;
            }

        case OSPF_ENABLED:
            if ((pOspfCxt->u1RestartExitReason == OSPF_RESTART_INPROGRESS)
                && (IssGetCsrRestoreFlag () != OSPF_TRUE))
            {
                /* Restart is in progress */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfAreaIDValid
 Input       :  The Indices

                The Object 
                testValFutOspfAreaIDValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfAreaIDValid (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFutOspfAreaIDValid)
{
    switch (i4TestValFutOspfAreaIDValid)
    {
        case OSPF_TRUE:
        case OSPF_FALSE:
            return SNMP_SUCCESS;
        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/*----------------------------------------------------------------------------*/
/*                          End of file osopqtst.c                            */
/*----------------------------------------------------------------------------*/
