/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ostask.c,v 1.15 2010/01/07 12:43:30 prabuc Exp $
 *
 * Description:This file contains procedures related to
 *             OSPF - IP interface
 *
 *******************************************************************/

#include "osinc.h"

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
#include "fsospfwr.h"
#include "stdospwr.h"
#include "stdostwr.h"
#ifdef OPQAPSIM_WANTED
#include "fsosoawr.h"
#endif
#include "fsostewr.h"
#endif
#include "fsmistwr.h"
#include "fsosmiwr.h"
#include "fsmioswr.h"
#include "fsstdmwr.h"

/* functions given for IP to use
 *
 * ipif_spawn_ospf_task()
 * IpifRcvPkt()
 * ipif_set_if_oper_stat()
 * ipif_set_if_mtu_size()
 */

/* functions expected from IP 
 *
 * for transmitting packet
 * for getting interface specific info
 * for joining the mcast group
 * for leaving the mcast group
 */

/* functions expected from FutureIP 
 *
 * IpGetIfIndexFromIfId() - to get the ifIndex of an interface from
 *                                the interface id
 * IpGetIfIdFromIfIndex() - to get the interface id of an interface from
 *                                the ifIndex
 */

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifSpawnOspfTask                                          */
/*                                                                           */
/* Description  : This procedure is provided by OSPF to IP module            */
/*                to let IP spawn the OSPF task                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if OSPF Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IpifSpawnOspfTask (void)
{
    tOsixTaskId         u4IpOspfTaskId;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC:IpifSpawnOspfTask\n");

    /* spawn ospf task */

    if (OsixCreateTask ((const UINT1 *) "OSPF",
                        OSPF_ROUTING_TASK_PRIORITY | OSIX_SCHED_RR,
                        OSIX_DEFAULT_STACK_SIZE,
                        OSPFTaskMain, NULL, OSIX_DEFAULT_TASK_MODE,
                        (tOsixTaskId *) & (u4IpOspfTaskId)) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT:IpifSpawnOspfTask\n");
    return OSIX_SUCCESS;
}

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
VOID
RegisterOSPFMibs (VOID)
{
    RegisterFSMIST ();
    RegisterFSOSMI ();
    RegisterFSMIOS ();
    RegisterFSSTDM ();
#ifdef OPQAPSIM_WANTED
    RegisterFSOSOA ();
#endif
    RegisterFSOSPF ();
    RegisterSTDOSP ();
    RegisterFSOSTE ();
    RegisterSTDOST ();
}

VOID
UnRegisterOSPFMibs (VOID)
{
    UnRegisterFSMIST ();
    UnRegisterFSOSMI ();
    UnRegisterFSMIOS ();
    UnRegisterFSSTDM ();
#ifdef OPQAPSIM_WANTED
    UnRegisterFSOSOA ();
#endif
    UnRegisterFSOSPF ();
    UnRegisterSTDOSP ();
    UnRegisterFSOSTE ();
    UnRegisterSTDOST ();
}
#endif

/*------------------------------------------------------------------------*/
/*                        End of the file  ostask.c                       */
/*------------------------------------------------------------------------*/
