/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osdbg.c,v 1.26 2017/09/21 13:48:46 siva Exp $
 *
 * Description: This file contains routines used for debugging
 *
 *******************************************************************/

/**** $$TRACE_MODULE_NAME     = OSPF ****/
/**** $$TRACE_SUB_MODULE_NAME = DBG ****/

#include "osinc.h"
#ifdef DEBUG_WANTED

#define LSDB_STUB     1
#define RT_STUB       2
static int          dump_count = 0;
PRIVATE CHR1        gau1LogBuf[OSPF_MAX_DUMP_BUF + 1];

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintTimer                                            */
/*                                                                           */
/* Description  : Prints the details regarding the timer block specified.    */
/*                                                                           */
/* Input        : pAppTimer         : Pointer to the timer block           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintTimer (tTmrAppTimer * pAppTimer)
{
    INT1               *pTmp;
    UINT1               u1TimerId;
    UINT4               u4RemainingTime;

    if (pAppTimer == NULL)
    {
        return;
    }

    pTmp = (INT1 *) NULL;
    u1TimerId = ((tOspfTimer *) pAppTimer)->u1TimerId;

    if (!(u1TimerId < MAX_TIMERS))
    {
        return;
    }
    if (aTimerDesc[u1TimerId].i2Offset != -1)
    {
        pTmp = (INT1 *) pAppTimer - aTimerDesc[u1TimerId].i2Offset;
    }

    if (pTmp == NULL)
    {
        return;
    }
    TmrGetRemainingTime (gTimerLst, (tTmrAppTimer *) pAppTimer,
                         (UINT4 *) &(u4RemainingTime));

    fprintf (gp_ospf_output_file, " %s         %u", au1DbgTimerId[u1TimerId], u4RemainingTime / (NO_OF_TICKS_PER_SEC)    /*TMO_Get_Remaining_Time(&gTimerLst, 
                                                                                                                           pAppTimer) */
        );

    switch (((tOspfTimer *) pAppTimer)->u1TimerId)
    {
        case HELLO_TIMER:
        case POLL_TIMER:
        case WAIT_TIMER:
            fprintf (gp_ospf_output_file, " ipAddr: %08x  addrless_if: %x\n",
                     OSPF_CRU_BMC_DWFROMPDU (((tInterface *) (VOID *)
                                              pTmp)->ifIpAddr),
                     ((tInterface *) (VOID *) pTmp)->u4AddrlessIf);
            break;

        case INACTIVITY_TIMER:
        case DD_INIT_RXMT_TIMER:
        case DD_RXMT_TIMER:
        case DD_LAST_PKT_LIFE_TIME_TIMER:
        case LSA_REQ_RXMT_TIMER:
        case LSA_RXMT_TIMER:
            fprintf (gp_ospf_output_file,
                     " nbrIpAddr: %08x  addrless_if: %x\n",
                     OSPF_CRU_BMC_DWFROMPDU (((tNeighbor *) (VOID *)
                                              pTmp)->nbrIpAddr),
                     ((tNeighbor *) (VOID *) pTmp)->u4NbrAddrlessIf);
            break;

        case LSA_NORMAL_AGING_TIMER:
        case LSA_PREMATURE_AGING_TIMER:
            if (((tLsaInfo *) (VOID *) pTmp)->lsaId.u1LsaType == 5)
            {
                fprintf (gp_ospf_output_file,
                         "  type: %d  ls_id: %08x rtrId:%08x\n",
                         ((tLsaInfo *) (VOID *) pTmp)->lsaId.u1LsaType,
                         OSPF_CRU_BMC_DWFROMPDU (((tLsaInfo *) (VOID *) pTmp)->
                                                 lsaId.
                                                 linkStateId),
                         OSPF_CRU_BMC_DWFROMPDU (((tLsaInfo *) (VOID *) pTmp)->
                                                 lsaId.advRtrId));
            }
            else
            {
                fprintf (gp_ospf_output_file,
                         " areaId: %08x  type: %d  ls_id: %08xrtr_id:%8x\n",
                         OSPF_CRU_BMC_DWFROMPDU (((tLsaInfo *) (VOID *)
                                                  pTmp)->pArea->areaId),
                         ((tLsaInfo *) (VOID *) pTmp)->lsaId.u1LsaType,
                         OSPF_CRU_BMC_DWFROMPDU (((tLsaInfo *) (VOID *) pTmp)->
                                                 lsaId.
                                                 linkStateId),
                         OSPF_CRU_BMC_DWFROMPDU (((tLsaInfo *) (VOID *) pTmp)->
                                                 lsaId.advRtrId));
            }
            break;

        case MIN_LSA_INTERVAL_TIMER:
            fprintf (gp_ospf_output_file,
                     "areaId: %08x  type: %d  ls_id: %08x\n",
                     OSPF_CRU_BMC_DWFROMPDU (((tLsaDesc *) (VOID *)
                                              pTmp)->pLsaInfo->
                                             pArea->areaId),
                     ((tLsaDesc *) (VOID *) pTmp)->pLsaInfo->lsaId.u1LsaType,
                     OSPF_CRU_BMC_DWFROMPDU (((tLsaDesc *) (VOID *) pTmp)->
                                             pLsaInfo->lsaId.linkStateId));
            break;

        case RUN_RT_TIMER:
            fprintf (gp_ospf_output_file, "\n");

        default:
            break;
    }
}

/* Note : these debug routines are specified in Appendix D of 1247 */

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintAreas                                            */
/*                                                                           */
/* Description  : Prints statistics regarding all areas.                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintAreasInCxt (tOspfCxt * pOspfCxt)
{

    tArea              *pArea;

    fprintf (gp_ospf_output_file,
             "\n              OSPF AREAS                 \n");
    fprintf (gp_ospf_output_file, "  AREA ID   #IFs    #Nets   #Rtrs \n");

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        fprintf (gp_ospf_output_file,
                 " %08x    %u              %u       %u  \n",
                 OSPF_CRU_BMC_DWFROMPDU (pArea->areaId),
                 TMO_SLL_Count (&(pArea->ifsInArea)),
                 TMO_SLL_Count (&(pArea->networkLsaLst)),
                 TMO_SLL_Count (&(pArea->rtrLsaLst)));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintAllIfs                                          */
/*                                                                           */
/* Description  : Prints statistics regarding all interfaces.                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintAllIfsInCxt (tOspfCxt * pOspfCxt)
{

    UINT1               u1Index;
    tTMO_SLL           *pLst;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    fprintf (gp_ospf_output_file, "\n              INTERFACES            \n");
    fprintf (gp_ospf_output_file,
             " IF IPaddr   STATE   COST      DR       BDR   #Nbrs   #Adjs   \n");
    for (u1Index = 0; u1Index <= 1; u1Index++)
    {

        if (u1Index == 0)
        {
            pLst = &(pOspfCxt->sortIfLst);
        }
        else
        {
            pLst = &(pOspfCxt->virtIfLst);
        }
        TMO_SLL_Scan (pLst, pLstNode, tTMO_SLL_NODE *)
        {

            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            if (!(pInterface->u1IsmState < MAX_IF_STATE))
            {
                return;
            }
            fprintf (gp_ospf_output_file,
                     " %08x  %6s   %u   %08x   %08x    %u     %u\n",
                     OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                     au1DbgIfState[pInterface->u1IsmState],
                     GET_IF_TOS_0_COST (pInterface),
                     OSPF_CRU_BMC_DWFROMPDU (GET_DR (pInterface)),
                     OSPF_CRU_BMC_DWFROMPDU (GET_BDR (pInterface)),
                     TMO_SLL_Count (&(pInterface->nbrsInIf)),
                     DbgCountAdj (pInterface));
            if (pInterface->pLastAuthkey != NULL)
            {
                fprintf (gp_ospf_output_file,
                         "authKey                      : %8s\n",
                         pInterface->pLastAuthkey->authKey);
                fprintf (gp_ospf_output_file,
                         "u1AuthkeyId                      : %8d\n",
                         pInterface->pLastAuthkey->u1AuthkeyId);
            }
            fprintf (gp_ospf_output_file,
                     "authKey   count              : %8u\n",
                     pInterface->sortMd5authkeyLst.u4_Count);
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintIfsInAreaInCxt                                      */
/*                                                                           */
/* Description  : Prints information of the interfaces in the area, if area  */
/*                exists.                                                    */
/*                                                                           */
/* Input        : areaId      :  AREA whose interface info is to be printed */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintIfsInAreaInCxt (tOspfCxt * pOspfCxt, tAreaId areaId)
{
    tArea              *pArea;
    if ((pArea = GetFindAreaInCxt (pOspfCxt, (tAreaId *) & areaId)) != NULL)
    {
        DbgDisplayIfsInArea (pArea);
    }
    else
    {
        fprintf (gp_ospf_output_file, " No such area.. hence no intf info.\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDisplayIfsInArea                                    */
/*                                                                           */
/* Description  : Displays information regarding all interfaces in the       */
/*                specified area.                                            */
/*                                                                           */
/* Input        : pArea       : Ptr to AREA whose interface info is to be   */
/*                               printed                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgDisplayIfsInArea (tArea * pArea)
{

    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    fprintf (gp_ospf_output_file,
             "              INTERFACES  IN  AREA : %08x \n",
             OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));
    fprintf (gp_ospf_output_file,
             " IF IPaddr   STATE   COST      DR       BDR  #Nbrs  #Adjs   \n");
    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_LST (pLstNode);
        if (!(pInterface->u1IsmState < MAX_IF_STATE))
        {
            return;
        }
        fprintf (gp_ospf_output_file,
                 " %08x   %s   %u   %08x   %08x   %u       %u   \n",
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                 au1DbgIfState[pInterface->u1IsmState],
                 GET_IF_TOS_0_COST (pInterface),
                 OSPF_CRU_BMC_DWFROMPDU (GET_DR (pInterface)),
                 OSPF_CRU_BMC_DWFROMPDU (GET_BDR (pInterface)),
                 TMO_SLL_Count (&(pInterface->nbrsInIf)),
                 DbgCountAdj (pInterface));
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgCountAdj                                              */
/*                                                                           */
/* Description  : Prints the total number of adjacencies in the interface    */
/*                                                                           */
/* Input        : pInterface    : Pointer to the interace whose adjacency   */
/*                                 count is required.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Number of the adjacencies                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
DbgCountAdj (tInterface * pInterface)
{

    UINT4               u4AdjCount = 0;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pLstNode;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);
        if (pNbr->u1NsmState == NBRS_FULL)
            u4AdjCount++;
    }
    return u4AdjCount;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintAllNbrs                                         */
/*                                                                           */
/* Description  : Prints information regarding all neighbours.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintAllNbrsInCxt (tOspfCxt * pOspfCxt)
{

    tTMO_SLL_NODE      *pLstNode;
    tNeighbor          *pNbr;

    fprintf (gp_ospf_output_file, "\n              NEIGHBORS            \n");
    fprintf (gp_ospf_output_file,
             "  NbrAddr    NbrID    STATE    RxmtLen  SummLen  ReqLen\n");
    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
        if (!(pNbr->u1NsmState < MAX_NBR_STATE))
        {
            return;
        }
        fprintf (gp_ospf_output_file,
                 " %08x  %08x  %6s     %u       %d        %u  \n",
                 OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                 OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                 au1DbgNbrState[pNbr->u1NsmState],
                 pNbr->lsaRxmtDesc.u4LsaRxmtCount, pNbr->dbSummary.u2Len,
                 TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintIfInCxt                                            */
/*                                                                           */
/* Description  : Prints information regarding the interfaces whose          */
/*                IP Address is specified.                                   */
/*                                                                           */
/* Input        : pIfIpAddr    : IP address of the interface                 */
/*                u4AddrlessIf  : Contains IfIndex value in case of          */
/*                                  Addressless interface.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintIfInCxt (tOspfCxt * pOspfCxt, tIPADDR pIfIpAddr, UINT4 u4AddrlessIf)
{
    tInterface         *pInterface;

    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, pIfIpAddr, u4AddrlessIf)) == NULL)
    {
        fprintf (gp_ospf_output_file, "Interface not found.\n");
    }
    else
    {
        if (!((pInterface->u1NetworkType < MAX_IF_TYPE) &&
              (pInterface->u1IsmState < MAX_IF_STATE)))
        {
            return;
        }
        fprintf (gp_ospf_output_file, "Interface type               : %s\n",
                 au1DbgIfType[pInterface->u1NetworkType]);
        fprintf (gp_ospf_output_file, "Interface state              : %s\n",
                 au1DbgIfState[pInterface->u1IsmState]);
        fprintf (gp_ospf_output_file, "IP address                   : %8x\n",
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));
        fprintf (gp_ospf_output_file, "IP addr mask                 : %8x\n",
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask));
        fprintf (gp_ospf_output_file, "Address If                  : %d\n",
                 pInterface->u4AddrlessIf);
        if (pInterface->pLastAuthkey != NULL)
        {
            fprintf (gp_ospf_output_file,
                     "authKey                      : %8s\n",
                     pInterface->pLastAuthkey->authKey);
            fprintf (gp_ospf_output_file,
                     "u1AuthkeyId                      : %8d\n",
                     pInterface->pLastAuthkey->u1AuthkeyId);
        }
        fprintf (gp_ospf_output_file, "authKey   count              : %8u\n",
                 pInterface->sortMd5authkeyLst.u4_Count);
        fprintf (gp_ospf_output_file, "Area                         : %8u\n",
                 OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId));
        fprintf (gp_ospf_output_file, "interface_index              : %u\n",
                 pInterface->u4IfIndex);
        fprintf (gp_ospf_output_file, "mtu_size                     : %u\n",
                 pInterface->u4MtuSize);
        fprintf (gp_ospf_output_file, "hello_rcvd_count             : %u\n",
                 pInterface->u4HelloRcvdCount);
        fprintf (gp_ospf_output_file, "hello_txed_count             : %u\n",
                 pInterface->u4HelloTxedCount);
        fprintf (gp_ospf_output_file, "ddp_rcvd_count               : %u\n",
                 pInterface->u4DdpRcvdCount);
        fprintf (gp_ospf_output_file, "ddp_txed_count               : %u\n",
                 pInterface->u4DdpTxedCount);
        fprintf (gp_ospf_output_file, "lsa_req_rcvd_count           : %u\n",
                 pInterface->u4LsaReqRcvdCount);
        fprintf (gp_ospf_output_file, "lsa_req_txed_count           : %u\n",
                 pInterface->u4LsaReqTxedCount);
        fprintf (gp_ospf_output_file, "lsa_ack_rcvd_count           : %u\n",
                 pInterface->u4LsaAckRcvdCount);
        fprintf (gp_ospf_output_file, "lsa_ack_txed_count           : %u\n",
                 pInterface->u4LsaAckTxedCount);
        fprintf (gp_ospf_output_file, "lsa_update_rcvd_count        : %u\n",
                 pInterface->u4LsaUpdateRcvdCount);
        fprintf (gp_ospf_output_file, "lsa_update_txed_count        : %u\n",
                 pInterface->u4LsaUpdateTxedCount);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintNbrInCxt                                           */
/*                                                                           */
/* Description  : Prints info regarding the neighbour whose IP address is    */
/*                specified.                                                 */
/*                                                                           */
/* Input        : pNbrIpAddr  : IP address of the neighbour                  */
/*                u4AddrlessIf  : Contains IfIndex value in case of          */
/*                                  Addressless interface.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintNbrInCxt (tOspfCxt * pOspfCxt, tIPADDR * pNbrIpAddr, UINT4 u4AddrlessIf)
{

    tNeighbor          *pNbr;

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, pNbrIpAddr, u4AddrlessIf)) == NULL)
    {
        fprintf (gp_ospf_output_file, "Neighbor not found.\n");
    }
    else
    {
        if (!(pNbr->u1ConfigStatus < MAX_NBR_CFG_STATUS))
        {
            return;
        }
        fprintf (gp_ospf_output_file, "State              : %d\n",
                 pNbr->u1NsmState);
        fprintf (gp_ospf_output_file, "Neighbor ID        : %08x\n",
                 OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
        fprintf (gp_ospf_output_file, "Priority           : %d\n",
                 pNbr->u1NbrRtrPriority);
        fprintf (gp_ospf_output_file, "IP Address         : %08x\n",
                 OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
        fprintf (gp_ospf_output_file, "Options            : %d\n",
                 pNbr->nbrOptions);
        fprintf (gp_ospf_output_file, "Desg Router        : %08x\n",
                 OSPF_CRU_BMC_DWFROMPDU (pNbr->desgRtr));
        fprintf (gp_ospf_output_file, "Backup Desg Router : %08x\n",
                 OSPF_CRU_BMC_DWFROMPDU (pNbr->backupDesgRtr));
        fprintf (gp_ospf_output_file, "Address If        : %u\n",
                 pNbr->u4NbrAddrlessIf);
        fprintf (gp_ospf_output_file, "Config Status      : %s\n",
                 au1DbgNbrCfgStatus[pNbr->u1ConfigStatus]);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintNbrsInIfInCxt                                      */
/*                                                                           */
/* Description  : Prints all neighbours on the specified interface.          */
/*                                                                           */
/* Input        : pIfIpAddr   : IP address of the interface                  */
/*                u4AddrlessIf  : Contains IfIndex value in case of          */
/*                                  Addressless interface.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintNbrsInIfInCxt (tOspfCxt * pOspfCxt, tIPADDR pIfIpAddr,
                       UINT4 u4AddrlessIf)
{

    tNeighbor          *pNbr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    if ((pInterface = GetFindIfInCxt (pOspfCxt, pIfIpAddr, u4AddrlessIf))
        != NULL)
    {

        fprintf (gp_ospf_output_file,
                 "              NEIGHBORS ON INTERFACE \n");
        fprintf (gp_ospf_output_file,
                 " Nbr IPaddr  NbrID  STATE  LS RxmtLen  DB\
              SummLen  LS ReqLen\n");
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {

            pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);
            if (!(pNbr->u1NsmState < MAX_NBR_STATE))
            {
                return;
            }
            fprintf (gp_ospf_output_file,
                     "  %08x   %08x  %s    %u      %d       %u  \n",
                     OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                     OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                     au1DbgNbrState[pNbr->u1NsmState],
                     pNbr->lsaRxmtDesc.u4LsaRxmtCount,
                     pNbr->dbSummary.u2Len,
                     TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)));
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintAllLsas                                            */
/*                                                                           */
/* Description  : Prints all the LSAs supported by the router                */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintAllLsasInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4HashKey = 0;
    tArea              *pArea;
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLsaNode;
    tTMO_SLL_NODE      *pIfNode;
    tInterface         *pInterface;
    tOsDbNode          *pOsDbNode = NULL;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        DbgDisplayLsasInArea (pArea);
        TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
        {

            pInterface = GET_IF_PTR_FROM_LST (pIfNode);

            fprintf (gp_ospf_output_file, "\n   TYPE9 LSA DATABASE \
               OF AREA : %08x\n", OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));
            TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pLsaNode,
                          tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
                DbgDumpLsa (pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
            }
        }

    }

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLsaNode);
                DbgDumpLsa (pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
            }
        }
    }

    fprintf (gp_ospf_output_file, " \n      TYPE11 LSA DATABASE \n");
    TMO_SLL_Scan (&(pOspfCxt->Type11OpqLSALst), pLsaNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
        DbgDumpLsa (pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintLsasInAreaInCxt                                    */
/*                                                                           */
/* Description  : Prints all the LSAs associated with an area                */
/*                                                                           */
/* Input        : pAreaId :  Pointer to the AREA whose LSA's to be printed.*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintLsasInAreaInCxt (tOspfCxt * pOspfCxt, tAreaId * pAreaId)
{
    tArea              *pArea;

    if ((pArea = GetFindAreaInCxt (pOspfCxt, pAreaId)) != NULL)
        DbgDisplayLsasInArea (pArea);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDisplayLsasInArea                                   */
/*                                                                           */
/* Description  : Prints all the LSAs associated with an area                */
/*                                                                           */
/* Input        : pArea   :  Ptr to the AREA whose LSA's to be displayed.   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgDisplayLsasInArea (tArea * pArea)
{
    UINT4               u4HashIndex;
    tLsaInfo           *pLsaInfo;

    fprintf (gp_ospf_output_file, "\n       LSA DATABASE OF AREA : %08x\n",
             OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));
    TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashIndex)
    {

        TMO_HASH_Scan_Bucket (pArea->pLsaHashTable, u4HashIndex,
                              pLsaInfo, tLsaInfo *)
        {

            DbgDumpLsa (pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintLsaInCxt                                              */
/*                                                                           */
/* Description  : Prints all the Information associated with a LSA           */
/*                                                                           */
/* Input        : areaId       : Id of the area to which the LSA            */
/*                                is associated.                             */
/*                u1LsaType   : type of the LSA                            */
/*                linkStateId : Link State ID                              */
/*                advRtrId    : Router that advertised the LSA             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintLsaInCxt (tOspfCxt * pOspfCxt, tAreaId areaId,
                  UINT1 u1LsaType, tLINKSTATEID linkStateId, tRouterId advRtrId)
{
    tLsaInfo           *pLsaInfo;

    pLsaInfo =
        LsuSearchDatabase (u1LsaType, (tAreaId *) & linkStateId,
                           (tAreaId *) & advRtrId, (UINT1 *) NULL,
                           (UINT1 *) GetFindAreaInCxt (pOspfCxt,
                                                       (tAreaId *) & areaId));

    if (pLsaInfo != NULL)
    {
        DbgDumpLsa (pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintOspfRtInCxt                                        */
/*                                                                           */
/* Description  : Prints the ospf routing information                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintOspfRtInCxt (tOspfCxt * pOspfCxt)
{
    tRtEntry           *pRtEntry;

    UtlTrcPrint ("\nOSPF ROUTING TABLE\n");

    TMO_SLL_Scan (&(pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        DbgPrintRtEntry (pRtEntry);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintRtEntry                                            */
/*                                                                           */
/* Description  : Prints the information of a Route Entry                    */
/*                                                                           */
/* Input        : pRtEntry   : pointer to Route entry whose information is   */
/*                               is required.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgPrintRtEntry (tRtEntry * pRtEntry)
{
    UINT1               u1HopInd;
    tPath              *pPath;
    UINT1               u1Tos;

    for (u1Tos = 0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        TMO_SLL_Scan (&(pRtEntry->aTosPath[u1Tos]), pPath, tPath *)
        {
            UtlTrcLog (1, 1, "", "TOS Value: %d\n", ENCODE_TOS (u1Tos));

            if (pPath->u4Cost == 0 && pPath->u4Type2Cost == 0)
            {
                continue;
            }
            UtlTrcPrint
                ("  Type        Dest        Area        Path Type      Cost \n");
            u1HopInd = 0;
            if (!((pRtEntry->u1DestType < MAX_RT_DST_TYPE) &&
                  (pPath->u1PathType < MAX_RT_PATH_TYPE)))
            {
                return;
            }
            UtlTrcLog (1, 1, "", "%11s   %08x   %08x     %10s    %3u \n",
                       au1DbgRtDestType[pRtEntry->u1DestType],
                       OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId),
                       OSPF_CRU_BMC_DWFROMPDU (pPath->areaId),
                       au1DbgRtPathType[pPath->u1PathType], pPath->u4Cost);
            UtlTrcPrint
                (" Type2Cost If_type if_num if_sub   If_index   Next_hop_Addr"
                 "   Adv_Rtr_ID \n");

            for (u1HopInd = 0; u1HopInd < (pPath->u1HopCount) ||
                 (pPath->u1HopCount == 0 && u1HopInd == 0); u1HopInd++)
            {
                if (!(u1HopInd < MAX_NEXT_HOPS))
                {
                    return;
                }
                UtlTrcLog (1, 1, "",
                           " %3d         %d        "
                           " %08x      %08x\n", pPath->u4Type2Cost,
                           pPath->aNextHops[u1HopInd].u4IfIndex,
                           OSPF_CRU_BMC_DWFROMPDU (pPath->aNextHops[u1HopInd].
                                                   nbrIpAddr),
                           OSPF_CRU_BMC_DWFROMPDU (pPath->aNextHops[u1HopInd].
                                                   advRtrId));
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpOspfRtInCxt                                         */
/*                                                                           */
/* Description  : Writes routing table information.                          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgDumpOspfRtInCxt (tOspfCxt * pOspfCxt)
{
    tRtEntry           *pRtEntry;

    fprintf (gp_ospf_output_file, "\nOspf Routing table \n");
    TMO_SLL_Scan (&(pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        DbgDumpRtEntry (pRtEntry);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpRtEntry                                          */
/*                                                                           */
/* Description  : Writes the information of the particular Route entry.      */
/*                                                                           */
/* Input        : pRtEntry  : pointer to the Route entry.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgDumpRtEntry (tRtEntry * pRtEntry)
{
    UINT1               u1HopInd;
    tPath              *pPath;

    if (TMO_SLL_Count (&(pRtEntry->aTosPath[TOS_0])) == 0)
        return;

    fprintf (gp_ospf_output_file, "Route : \n");
    if (!(pRtEntry->u1DestType < MAX_RT_DST_TYPE))
    {
        return;
    }
    fprintf (gp_ospf_output_file,
             "Dest type : %11s  Dest_id : %08x  Mask : %08x\n",
             au1DbgRtDestType[pRtEntry->u1DestType],
             OSPF_CRU_BMC_DWFROMPDU (pRtEntry->destId), pRtEntry->u4IpAddrMask);
    fprintf (gp_ospf_output_file, "  Options : %d  update_time : %x\n",
             pRtEntry->options, pRtEntry->u4UpdateTime);

    TMO_SLL_Scan (&(pRtEntry->aTosPath[TOS_0]), pPath, tPath *)
    {

        fprintf (gp_ospf_output_file, "Path : \n");
        if (!(pPath->u1PathType < MAX_RT_PATH_TYPE))
        {
            return;
        }
        fprintf (gp_ospf_output_file,
                 "  Type : %s  Area_id : %08x Cost : %u  Type_2_Cost : %u\n",
                 au1DbgRtPathType[pPath->u1PathType],
                 OSPF_CRU_BMC_DWFROMPDU (pPath->areaId), pPath->u4Cost,
                 pPath->u4Type2Cost);
        fprintf (gp_ospf_output_file, "  Hop Count : %d  Hop Index : %d  \n",
                 pPath->u1HopCount, pPath->u1NextHopIndex);
        if (pPath->u1PathType == INTRA_AREA)
        {
            fprintf (gp_ospf_output_file,
                     "  Area_id :%08x ls_id :%08x Adv_rtr_id :%08x"
                     " Lsa Type: %s\n",
                     OSPF_CRU_BMC_DWFROMPDU (pPath->pLsaInfo->pArea->
                                             areaId),
                     OSPF_CRU_BMC_DWFROMPDU (pPath->pLsaInfo->lsaId.
                                             linkStateId),
                     OSPF_CRU_BMC_DWFROMPDU (pPath->pLsaInfo->lsaId.
                                             advRtrId),
                     au1DbgLsaType[pPath->pLsaInfo->lsaId.u1LsaType]);
        }
        u1HopInd = 0;
        fprintf (gp_ospf_output_file,
                 "              Iface_ID   Next_hop_Addr  Adv_Rtr_ID \n");
        for (u1HopInd = 0; u1HopInd < pPath->u1HopCount; u1HopInd++)
        {
            if (!(u1HopInd < MAX_NEXT_HOPS))
            {
                return;
            }
            fprintf (gp_ospf_output_file,
                     "              %d   %08x       %08x \n",
                     pPath->aNextHops[u1HopInd].u1Status,
                     OSPF_CRU_BMC_DWFROMPDU (pPath->aNextHops[u1HopInd].
                                             nbrIpAddr),
                     OSPF_CRU_BMC_DWFROMPDU (pPath->aNextHops[u1HopInd].
                                             advRtrId));
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintLsaInfo                                         */
/*                                                                           */
/* Description  : Checks for the LSA in the database and then prints         */
/*                the information associated with an LSA.                    */
/*                                                                           */
/* Input        : areaId       : ID of the area the LSA is associated with  */
/*                u1LsaType   : Type of the LSA                            */
/*                linkStateId : Link State ID                              */
/*                advRtrId    : Advertising Router ID                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintLsaInfoInCxt (tOspfCxt * pOspfCxt,
                      tAreaId areaId,
                      UINT1 u1LsaType, tLINKSTATEID linkStateId,
                      tRouterId advRtrId)
{
    tLsaInfo           *pLsaInfo;
    pLsaInfo =
        LsuSearchDatabase (u1LsaType, (tAreaId *) & linkStateId,
                           (tAreaId *) & advRtrId, (UINT1 *) NULL,
                           (UINT1 *) GetFindAreaInCxt (pOspfCxt,
                                                       (tAreaId *) & areaId));

    if (pLsaInfo != NULL)
    {
        DbgDisplayLsaInfo (pLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDisplayLsaInfo                                       */
/*                                                                           */
/* Description  : Prints the LSA information.                                */
/*                                                                           */
/* Input        : pLsaInfo  : pointer to LSA information                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgDisplayLsaInfo (tLsaInfo * pLsaInfo)
{
    tOsixSysTime        u4SysTime;

    fprintf (gp_ospf_output_file, "     LSA INFO \n");
    fprintf (gp_ospf_output_file, "  lsa_type      : %d\n",
             pLsaInfo->lsaId.u1LsaType);
    fprintf (gp_ospf_output_file, "  linkStateId : %08x\n",
             OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId));
    fprintf (gp_ospf_output_file, "  advRtrId    : %08x\n",
             OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    fprintf (gp_ospf_output_file, "  lsa_age       : %d\n", pLsaInfo->u2LsaAge);
    fprintf (gp_ospf_output_file, "  lsaOptions   : %d\n",
             pLsaInfo->lsaOptions);
    fprintf (gp_ospf_output_file, "  lsaSeqNum   : %u\n", pLsaInfo->lsaSeqNum);
    fprintf (gp_ospf_output_file, "  lsa_chksum    : %d\n",
             pLsaInfo->u2LsaChksum);
    fprintf (gp_ospf_output_file, "  arrival_time  : %u\n",
             pLsaInfo->u4LsaArrivalTime);
    fprintf (gp_ospf_output_file, "  lsa_len       : %d\n", pLsaInfo->u2LsaLen);
    fprintf (gp_ospf_output_file, "  lsa_flushed   : %d\n",
             pLsaInfo->u1LsaFlushed);
    fprintf (gp_ospf_output_file, "  rxmt_count    : %u\n",
             pLsaInfo->u4RxmtCount);
    OsixGetSysTime ((tOsixSysTime *) & (u4SysTime));
    fprintf (gp_ospf_output_file, "  Current Time  : %u\n", u4SysTime);
    fprintf (gp_ospf_output_file, "  lsaAgingTimer:\n");
    DbgPrintTimer (&(pLsaInfo->lsaAgingTimer.timerNode));

    /* if present display lsa_desc fields */

    if (pLsaInfo->pLsaDesc == NULL)
        return;
    fprintf (gp_ospf_output_file, "     LSA DESC \n");
    fprintf (gp_ospf_output_file, "  pAssoPrimitive             : %p\n",
             pLsaInfo->pLsaDesc->pAssoPrimitive);
    fprintf (gp_ospf_output_file, "  u1MinLsaIntervalExpired  : %d\n",
             pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired);
    fprintf (gp_ospf_output_file, "  u1LsaChanged               : %d\n",
             pLsaInfo->pLsaDesc->u1LsaChanged);
    fprintf (gp_ospf_output_file, "  u1SeqNumWrapAround       : %d\n",
             pLsaInfo->pLsaDesc->u1SeqNumWrapAround);
    fprintf (gp_ospf_output_file, "  minLsaIntervalTimer:\n");
    DbgPrintTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer.timerNode));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintDbSummaryLst                                   */
/*                                                                           */
/* Description  : Prints the neighbour database summary list                 */
/*                                                                           */
/* Input        : pNbrIpAddr       : pointer to the neighbor IP Address   */
/*                u4NbrAddrlessIf  : Contains IfIndex value in case of    */
/*                                      Addressless Interface                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintDbSummaryLstInCxt (tOspfCxt * pOspfCxt, tIPADDR * pNbrIpAddr,
                           UINT4 u4NbrAddrlessIf)
{

    UINT2               u2HdrCount;
    UINT1              *pSummary;
    tNeighbor          *pNbr;

    if ((pNbr =
         GetFindNbrInCxt (pOspfCxt, pNbrIpAddr, u4NbrAddrlessIf)) == NULL)
        return;
    fprintf (gp_ospf_output_file, "  DB SUMMARY LIST \n");
    fprintf (gp_ospf_output_file, "dbSummaryLst = 0x%p\n",
             pNbr->dbSummary.dbSummaryLst);
    fprintf (gp_ospf_output_file, "u2Len = %d\n", pNbr->dbSummary.u2Len);
    fprintf (gp_ospf_output_file, "u2TopDbSummaryLst = %d\n",
             pNbr->dbSummary.u2TopDbSummaryLst);
    fprintf (gp_ospf_output_file, "u2LastTxedSummaryLen = %d\n",
             pNbr->dbSummary.u2LastTxedSummaryLen);
    fprintf (gp_ospf_output_file, "dbSummaryPkt = 0x%p\n",
             (UINT1 *) pNbr->dbSummary.dbSummaryPkt);
    fprintf (gp_ospf_output_file, "seqNum = 0x%x\n", pNbr->dbSummary.seqNum);
    fprintf (gp_ospf_output_file, "bMaster = %u\n\n", pNbr->dbSummary.bMaster);

    fprintf (gp_ospf_output_file, "  Age   Options    Type       ls_id\
                  advRtrId      SeqNum    Length \n");
    u2HdrCount = (UINT2) (pNbr->dbSummary.u2Len / LS_HEADER_SIZE);
    pSummary = pNbr->dbSummary.dbSummaryLst;

    while (u2HdrCount--)
    {

        DbgDumpLsHeader (pSummary);
        pSummary += LS_HEADER_SIZE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintLsaReqLst                                      */
/*                                                                           */
/* Description  : Prints the list of request associated with a neighbour     */
/*                                                                           */
/* Input        : pNbrIpAddr    : pointer to the neighbour IP Address     */
/*                u4AddrlessIf   : contains IfIndex value in case of       */
/*                                   Addressless interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintLsaReqLstInCxt (tOspfCxt * pOspfCxt, tIPADDR * pNbrIpAddr,
                        UINT4 u4AddrlessIf)
{

    tNeighbor          *pNbr;
    tLsaReqNode        *pLsaReqNode;

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, pNbrIpAddr, u4AddrlessIf)) == NULL)
    {
        return;
    }

    fprintf (gp_ospf_output_file, "  LSA REQUEST LIST \n");
    fprintf (gp_ospf_output_file, "startNextReqPkt = 0x%p\n",
             (UINT1 *) pNbr->lsaReqDesc.startNextReqPkt);
    fprintf (gp_ospf_output_file, "bLsaReqOutstanding = 0x%x\n\n",
             pNbr->lsaReqDesc.bLsaReqOutstanding);
    fprintf (gp_ospf_output_file, "  Age   Options    Type       ls_id\
                  advRtrId      SeqNum    Length \n");
    TMO_SLL_Scan (&(pNbr->lsaReqDesc.lsaReqLst), pLsaReqNode, tLsaReqNode *)
    {
        DbgDumpLsHeader ((UINT1 *) &(pLsaReqNode->lsHeader));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintLsaRxmtLst                                     */
/*                                                                           */
/* Description  : Prints the LSA Retransmission list associated with a       */
/*                neighbour.                                                 */
/*                                                                           */
/* Input        : pNbrIpAddr    : pointer to the neighbour IP Address     */
/*                u4AddrlessIf   : contains IfIndex value in case of       */
/*                                   Addressless interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintLsaRxmtLstInCxt (tOspfCxt * pOspfCxt,
                         tIPADDR * pNbrIpAddr, UINT4 u4AddrlessIf)
{

    tNeighbor          *pNbr;
    tLsaInfo           *pLsaInfo;
#ifdef HIGH_PERF_RXMT_LST_WANTED
    UINT4               u4RxmtIndex;
    UINT4               u4NextRxmtIndex;
    tRxmtNode          *pRxmtNode = NULL;
#else /* HIGH_PERF_RXMT_LST_WANTED */
    tTMO_DLL_NODE      *pLstNode;
#endif

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, pNbrIpAddr, u4AddrlessIf)) == NULL)
    {
        return;
    }

    fprintf (gp_ospf_output_file, "       RXMT LIST OF NBR : %08x\n",
             OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
    fprintf (gp_ospf_output_file, "LS type   Link State ID    Adv Rtr   Seq No\
              Age   Chksum\n");

#ifndef HIGH_PERF_RXMT_LST_WANTED
    TMO_DLL_Scan (&(pOspfCxt->rxmtLst), pLstNode, tTMO_DLL_NODE *)
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_RXMT_LST (pLstNode);
        if (IS_RXMT_BIT_SET (pNbr->i2NbrsTableIndex, pLsaInfo))
        {

            fprintf (gp_ospf_output_file,
                     "  %d       %08x      %08x     %u     %u     %u\n",
                     pLsaInfo->lsaId.u1LsaType,
                     OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                     OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
                     pLsaInfo->lsaSeqNum, pLsaInfo->u2LsaAge,
                     pLsaInfo->u2LsaChksum);
        }
    }
#else
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        return;
    }
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX,
                                     u4RxmtIndex);

    if (u4RxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        return;
    }

    do
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        OSPF_GET_DLL_INDEX_IN_RXMT_NODE (pRxmtNode->au1RxmtInfo,
                                         NBR_NEXT_DLL_INDEX, u4NextRxmtIndex);
        pLsaInfo = pRxmtNode->pLsaInfo;
        fprintf (gp_ospf_output_file,
                 "  %d       %08x      %08x     %d     %d     %d\n",
                 pLsaInfo->lsaId.u1LsaType,
                 OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                 OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
                 pLsaInfo->lsaSeqNum, pLsaInfo->u2LsaAge,
                 pLsaInfo->u2LsaChksum);
        u4RxmtIndex = u4NextRxmtIndex;

    }
    while (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX);

#endif /* HIGH_PERF_RXMT_LST_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpBuf                                               */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
/**** $$TRACE_PROCEDURE_NAME  = DbgDumpBuf ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW ****/

PUBLIC VOID
DbgDumpBuf (tCRU_BUF_CHAIN_HEADER * pBuf,
            UINT2 u2Len, UINT1 u1Type, UINT1 u1Flag)
{

    UINT1               u1Temp;
    INT4                i4ReadOffset;

    dump_count++;

   /**** $$TRACE_LOG (ENTRY, "Dump count : %d  pBuf : %u", 
                      dump_count, pBuf); 
   ****/
    if (u1Flag == OSPF_SENT)
    {
        fprintf (gp_ospf_output_file, "\nOutgoing ");
    }
    else
    {
        fprintf (gp_ospf_output_file, "\nIncoming ");
    }
    switch (u1Type)
    {
        case HELLO_PKT:
            fprintf (gp_ospf_output_file, " Hello ");
            break;

        case DD_PKT:
            fprintf (gp_ospf_output_file, " DDP ");
            break;

        case LSA_REQ_PKT:
            fprintf (gp_ospf_output_file, " LS Request ");
            break;

        case LS_UPDATE_PKT:
            fprintf (gp_ospf_output_file, " LS Update ");
            break;

        case LSA_ACK_PKT:
            fprintf (gp_ospf_output_file, " LS Acknowledge ");
            break;

        default:
            break;
    }
    fprintf (gp_ospf_output_file, "Packet Dump : %d\n", dump_count);
    i4ReadOffset = 0;
    while (u2Len--)
    {
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pBuf, i4ReadOffset, u1Temp);
        fprintf (gp_ospf_output_file, "%02x ", u1Temp);
        if ((u2Len % 4) == 0)
            fprintf (gp_ospf_output_file, "\n");
        i4ReadOffset++;
    }
    fflush (gp_ospf_output_file);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpLsa                                               */
/*                                                                           */
/* Description  : Dumps the contents of an LSA                               */
/*                                                                           */
/* Input        : pLsa    :  pointer to the LSA to be dumped                */
/*                u2Len   :  length of the information to be dumped         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
/**** $$TRACE_PROCEDURE_NAME = DbgDumpLsa ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW ****/

PUBLIC VOID
DbgDumpLsa (UINT1 *pLsa, UINT2 u2Len)
{

    UINT2               u2Ind = 0;
    dump_count++;

   /**** $$TRACE_LOG (ENTRY, "Dump count : %d.", dump_count); ****/

    fprintf (gp_ospf_output_file, "LSA Dump : %d\n", dump_count);
    while (u2Ind++ < u2Len)
    {

        fprintf (gp_ospf_output_file, "%02x ", *(pLsa++));
        if ((u2Ind % 4) == 0)
            fprintf (gp_ospf_output_file, "\n");
    }
    fflush (gp_ospf_output_file);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpLsHeader                                         */
/*                                                                           */
/* Description  : Dumps the LS Header                                        */
/*                                                                           */
/* Input        : pu1LsHeader : pointer to the LS Header that is to be    */
/*                                 dumped.                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgDumpLsHeader (UINT1 *pu1LsHeader)
{

    tLsHeader           lsHeader;

    UtilExtractLsHeaderFromLbuf (pu1LsHeader, &lsHeader);

    fprintf (gp_ospf_output_file,
             " %4d     %d      %d       %08x    %08x     " "%x   %3d\n",
             lsHeader.u2LsaAge, lsHeader.lsaOptions, lsHeader.u1LsaType,
             OSPF_CRU_BMC_DWFROMPDU (lsHeader.linkStateId),
             OSPF_CRU_BMC_DWFROMPDU (lsHeader.advRtrId),
             lsHeader.lsaSeqNum, lsHeader.u2LsaLen);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintAddrRange                                       */
/*                                                                           */
/* Description  : Prints all the address ranges handled by this router       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgPrintAddrRangeInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;
    UINT1               u1Index;

    fprintf (gp_ospf_output_file, "\n              OSPF ADDR RANGES\n");
    fprintf (gp_ospf_output_file,
             "   Area Id     Addr      Mask    u1Active    u1CalActive\n");
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {

            if (pArea->aAddrRange[u1Index].areaAggStatus != INVALID)
            {
                fprintf (gp_ospf_output_file,
                         " %08x   %08x   %08x         %d       %d\n",
                         OSPF_CRU_BMC_DWFROMPDU (pArea->areaId),
                         OSPF_CRU_BMC_DWFROMPDU (pArea->aAddrRange
                                                 [u1Index].ipAddr),
                         OSPF_CRU_BMC_DWFROMPDU (pArea->aAddrRange
                                                 [u1Index].ipAddrMask),
                         pArea->aAddrRange[u1Index].u1Active,
                         pArea->aAddrRange[u1Index].u1CalActive);
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgShowRoute                                             */
/*                                                                           */
/* Description  : Displays the route available to the given destination.     */
/*                                                                           */
/* Input        : pDestIpAddr  : pointer to the destination IP Address    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgShowRouteInCxt (tOspfCxt * pOspfCxt, tIPADDR * pDestIpAddr)
{
    tRouteNextHop      *pNextHop;
    tPath              *pPath;

    if ((pPath = RtlRtLookupInCxt (pOspfCxt,
                                   pOspfCxt->pOspfRt, pDestIpAddr,
                                   TOS_0)) != NULL)
    {

        if (pPath->u1HopCount != 0)
        {
            pPath->u1NextHopIndex++;
            pPath->u1NextHopIndex %= pPath->u1HopCount;
            if (!(pPath->u1NextHopIndex < MAX_NEXT_HOPS))
            {
                return;
            }
            pNextHop = &(pPath->aNextHops[pPath->u1NextHopIndex]);
            UtlTrcLog (1, 1, "",
                       "Route - if_index : %d, ipAddr : %08x, advRtrId : %08x\n",
                       pNextHop->u1Status,
                       OSPF_CRU_BMC_DWFROMPDU (pNextHop->nbrIpAddr),
                       OSPF_CRU_BMC_DWFROMPDU (pNextHop->advRtrId));

        }
    }
    else
    {
        UtlTrcPrint ("Route not found.\n");
    }
}

/* The following routine dumps all data structures */

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpAll                                               */
/*                                                                           */
/* Description  : Dumps the information in all the tables                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgDumpAllInCxt (tOspfCxt * pOspfCxt)
{
    DbgPrintAreasInCxt (pOspfCxt);
    DbgPrintAddrRangeInCxt (pOspfCxt);
    DbgPrintAllIfsInCxt (pOspfCxt);
    DbgPrintAllNbrsInCxt (pOspfCxt);
    DbgPrintAllLsasInCxt (pOspfCxt);
    DbgDumpOspfRtInCxt (pOspfCxt);
}

/* The following routine prints the Trap ID's */
/*****************************************************************************/
/*                                                                           */
/* Function     : DbgNotifyTrap                                            */
/*                                                                           */
/* Description  : Prints the trap ID of the trap to be generated.            */
/*                                                                           */
/* Input        : u1TrapId  : Trap ID of the trap generated.               */
/*               *p_trap_info : Trap Information.                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

/**** $$TRACE_PROCEDURE_NAME = DbgNotifyTrap ****/
/**** $$TRACE_PROCEDURE_LEVEL = TOP ****/

PUBLIC VOID
DbgNotifyTrap (UINT1 u1TrapId, void *p_trap_info)
{
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (p_trap_info);
 /**** $$TRACE_LOG (INTMD, 
       "\n The Generated TRAP : %s", au1DbgTrapId[u1TrapId]); ****/
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrntTestCaseNum                                     */
/*                                                                           */
/* Description  : Prints the test case num with delimiters in the log file.  */
/*                                                                           */
/* Input        : au1TstCasNum : Test case Number.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

/**** $$TRACE_PROCEDURE_NAME = DbgPrntTestCaseNum ****/
/**** $$TRACE_PROCEDURE_LEVEL = TOP ****/

PUBLIC VOID
DbgPrntTestCaseNum (UINT1 au1TstCasNum[50], UINT1 *begEndInd)
{
    UNUSED_PARAM (au1TstCasNum);

    if (*begEndInd == 1)
    {
/**** $$TRACE_LOG (INTMD,"\n************************************************");
 ****/
/**** $$TRACE_LOG (INTMD,"\n Begining of the Test Case : %s", au1TstCasNum);
 ****/
/**** $$TRACE_LOG (INTMD,"\n************************************************");
 ****/
    }
    else
    {
/**** $$TRACE_LOG (INTMD,"\n************************************************");
 ****/
/**** $$TRACE_LOG (INTMD,"\n End of the Test Case : %s", au1TstCasNum);
 ****/
/**** $$TRACE_LOG (INTMD,"\n************************************************");
 ****/
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDeleteLsa                                             */
/*                                                                           */
/* Description  : This procedure deletes the lsa by premature aging.         */
/*                                                                           */
/* Input        : u1LsaType      : Type of lsa                             */
/*                linkStateId    : Link state id of the lsa.               */
/*                advRtrId       : Advertising router id of the LSA        */
/*                areaId          : area id                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
/**** $$TRACE_PROCEDURE_NAME = DbgDeleteLsa ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN ****/
PUBLIC VOID
DbgDeleteLsaInCxt (tOspfCxt * pOspfCxt, tAreaId areaId,
                   UINT1 u1LsaType, tLINKSTATEID linkStateId,
                   tRouterId advRtrId)
{
    tLsaInfo           *pLsaInfo;

    pLsaInfo = LsuSearchDatabase (u1LsaType,
                                  (tLINKSTATEID *) & linkStateId,
                                  (tRouterId *) & advRtrId, (UINT1 *) NULL,
                                  (UINT1 *) GetFindAreaInCxt (pOspfCxt,
                                                              (tAreaId *) &
                                                              areaId));

    if (pLsaInfo != NULL)
    {
        AgdFlushOut (pLsaInfo);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgConfDeleteAllExtRoutesInCxt                             */
/*                                                                           */
/* Description  : This procedure deletes all the configured external LSAs    */
/*                at a given time.                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgConfDeleteAllExtRoutesInCxt (tOspfCxt * pOspfCxt)
{
    tExtRoute          *pExtRoute;
    tLsaInfo           *pLsaInfo;
    tIPADDR             DestNet;
    tIPADDRMASK         DestAddrMask;
    UINT4               au4Key[2];
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    while (TrieGetFirstNode (&inParams,
                             &pAppSpecPtr,
                             (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        pExtRoute = (tExtRoute *) pAppSpecPtr;
        /* Delete the route entry from Trie */
        ExtrtDeleteFromTrieInCxt (pOspfCxt, pExtRoute);
        IP_ADDR_COPY (DestNet, pExtRoute->ipNetNum);
        IP_ADDR_COPY (DestAddrMask, pExtRoute->ipAddrMask);
        pLsaInfo = LsuDatabaseLookUp (AS_EXT_LSA, &(DestNet),
                                      &(DestAddrMask),
                                      &(pOspfCxt->rtrId), (UINT1 *) NULL,
                                      (UINT1 *) pOspfCxt->pBackbone);
        if (pLsaInfo != NULL)
        {
            LsuFlushLsaAfterRemovingFromDescLst (pLsaInfo);
        }

        EXT_ROUTE_FREE (pExtRoute);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfDumpPktData                                         */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : u4Trace   : Trace Flags word of the OSPF module            */
/*                u4TrcType : Trace flag for Pkt Dumping                     */
/*                Direction : flag indicating outgoing or incoming message   */
/*                pBuf      : pointer to the buffer to be dumped             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfDumpPktData (UINT4 u4Trace, UINT4 u4TrcType,
                    UINT1 Direction, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               DumpLevel = OSPF_ZERO;

    if (!(u4Trace & u4TrcType))
    {
        /* Dumping messages of this "type" not enabled */
        return;
    }

    if (u4Trace & OSPF_DUMP_HGH_TRC)
    {
        DumpLevel = OSPF_HIGH_DUMP;
    }
    else if (u4Trace & OSPF_DUMP_LOW_TRC)
    {
        DumpLevel = OSPF_LOW_DUMP;
    }
    else if (u4Trace & OSPF_DUMP_HEX_TRC)
    {
        DumpLevel = OSPF_HEX_DUMP;
    }
    else
        return;                    /* Error in trace flag */

    /* Call the packet analyser */
    DbgOspfPktAnalyser (Direction, DumpLevel, pBuf);

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfPktAnalyser                                         */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : Direction : flag indicating outgoing or incoming message   */
/*                DumpLevel : HexDump / Parsing / Highlevel                  */
/*                pBuf      : pointer to the buffer to be dumped             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfPktAnalyser (UINT1 Direction, UINT1 DumpLevel,
                    tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4ReadOffset = 0;
    INT4                i4DataLen = 0;    /* Eligible Pkt Data Length for dump */
    UINT1               u1TmpData = 0;
    UINT2               u2TmpData = 0;
    UINT4               u4TmpData = 0;
    UINT1               u1PktType = 0;
    UINT4               u4RtrSysTime = 0;    /* Time elapsed since Router boot */
    INT4                i4BufLength = 0;    /* DataLength in CRU buffer */
    UINT1               au1TmpBuf[OSPF_MAX_LOG_STR_LEN + 1];

    if ((pBuf == NULL) ||
        (!(i4BufLength = (INT4) CRU_BUF_Get_ChainValidByteCount (pBuf))))
    {
        DbgOspfUtlTrcPrint
            ("\nOSPF: Either invalid data buffer or no data exists\n");
        return;
    }
    OS_MEM_SET (gau1LogBuf, OSPF_ZERO, (OSPF_MAX_DUMP_BUF + 1));

    gau1LogBuf[0] = '\0';
    au1TmpBuf[0] = '\0';

    /* Check the packet length */
    OSPF_CRU_BMC_GET_2_BYTE (pBuf, PKT_LENGTH_OFFSET, u2TmpData);
    i4DataLen = (UINT4) u2TmpData;    /* For avoiding the endian problems */

    if (i4BufLength < i4DataLen)
    {
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "\nOSPF:      Warning: Truncated Packet\n");
        i4DataLen = (INT4) i4BufLength;
    }
    else if (i4BufLength > i4DataLen)
    {
        /* Extract Authentication Type */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf, AUTH_TYPE_OFFSET, u2TmpData);

        if (u2TmpData != CRYPT_AUTHENTICATION)
        {
            SPRINTF (OSPFBUF (gau1LogBuf),
                     "\nOSPF:      Warning: Excess Data in Packet\n");
        }
        /*else *//* Note: The Authentication data is not allways 16 bytes */
        /*{
           if (i4BufLength > (i4DataLen + MAX_AUTHKEY_LEN))
           {
           SPRINTF (OSPFBUF (gau1LogBuf),
           "\nOSPF:      Warning: Excess Data in Packet\n");
           }
           else if (i4BufLength < (i4DataLen + MAX_AUTHKEY_LEN))
           {
           SPRINTF (OSPFBUF (gau1LogBuf),
           "\nOSPF:      Warning: Truncated Packet\n");
           }
           }
         */
    }

    /* Record the Time elapsed since router booting */
    OsixGetSysTime (&u4RtrSysTime);
    /* Covert from Ticks to seconds */
    u4RtrSysTime = u4RtrSysTime / NO_OF_TICKS_PER_SEC;

    /* Extract the packet type */
    OSPF_CRU_BMC_GET_1_BYTE (pBuf, TYPE_OFFSET, u1PktType);

    if ((DumpLevel == OSPF_LOW_DUMP) || (DumpLevel == OSPF_HEX_DUMP))
    {
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "\n\n"
                 "OSPF: Time : %10s\t **** "
                 "%s\n%s",
                 DbgOspfFormatTime (u4RtrSysTime, au1TmpBuf),
                 ((Direction ==
                   OSPF_DIR_IN) ? "Received Packet" : "Sending Packet"),
                 ((DumpLevel ==
                   OSPF_HEX_DUMP) ? "" :
                  "OSPF: ---------------- OSPF Packet ----------------\n"));

        switch (u1PktType)
        {
            case HELLO_PKT:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf), "OSPF Hello");
                break;

            case DD_PKT:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Database Description");
                break;

            case LSA_REQ_PKT:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Link State Request");
                break;

            case LS_UPDATE_PKT:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Link State Update");
                break;

            case LSA_ACK_PKT:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Link State Acknowledge");
                break;

            default:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Unknown Packet");
                break;
        }
    }

    /* OSPF Hex Dumping Only .... */
    if (DumpLevel == OSPF_HEX_DUMP)
    {
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF:  ------ %s%s ------\n"
                 "OSPF:", ((u1PktType != HELLO_PKT) ? "OSPF " : ""), au1TmpBuf);

        for (i4ReadOffset = 1; i4ReadOffset <= i4BufLength; i4ReadOffset++)
        {
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, (i4ReadOffset - 1), u1TmpData);

            SPRINTF (OSPFBUF (gau1LogBuf), " %02x", u1TmpData);

            if (!(i4ReadOffset % 4))
            {
                SPRINTF (OSPFBUF (gau1LogBuf), " ");
            }

            if ((!(i4ReadOffset % 16)) || (i4ReadOffset == i4BufLength))
            {
                SPRINTF (OSPFBUF (gau1LogBuf), "\nOSPF:");
            }

            if (STRLEN (gau1LogBuf))
            {
                DbgOspfUtlTrcPrint (gau1LogBuf);
                gau1LogBuf[0] = '\0';
            }
        }

        SPRINTF (OSPFBUF (gau1LogBuf),
                 "\t------------------ End OSPF Packet ------------------\n");
    }

    /* OSPF Packet detailed parsing */
    if (DumpLevel == OSPF_LOW_DUMP)
    {
        OSPF_CRU_BMC_GET_1_BYTE (pBuf, VERSION_NO_OFFSET, u1TmpData);
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Version No           : %d\n", u1TmpData);

        /* Display the packet type */
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Type                 : %s (%d)\n",
                 au1TmpBuf, u1PktType);

        OSPF_CRU_BMC_GET_2_BYTE (pBuf, PKT_LENGTH_OFFSET, u2TmpData);
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Packet Length        : %d bytes\n", u2TmpData);

        /* Extract Router ID */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, RTR_ID_OFFSET,
                                 MAX_IP_ADDR_LEN);

        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Router ID            : %d.%d.%d.%d\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Extract Area ID */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, AREA_ID_OFFSET,
                                 MAX_IP_ADDR_LEN);

        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Area ID              : %d.%d.%d.%d\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Extract Checksum field */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf, CHKSUM_OFFSET, u2TmpData);
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Checksum             : 0x%04X\n", u2TmpData);

        /* Extract Authentication Type */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf, AUTH_TYPE_OFFSET, u2TmpData);

        switch (u2TmpData)
        {
            case NO_AUTHENTICATION:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "No Authentication (%d)", u2TmpData);
                break;

            case SIMPLE_PASSWORD:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Simple Password (%d)", u2TmpData);
                break;

            case CRYPT_AUTHENTICATION:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "MD5 Authentication (%d)", u2TmpData);
                break;

            default:
                SNPRINTF ((CHR1 *) au1TmpBuf, sizeof (au1TmpBuf),
                          "Unrecognized (%d)", u2TmpData);
                break;
        }

        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: Auth Type            : %s\n", au1TmpBuf);

        if (u2TmpData != CRYPT_AUTHENTICATION)
        {
            /* Extract Authentication word */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     AUTH_KEY_OFFSET_IN_HEADER, AUTH_KEY_SIZE);

            SPRINTF (OSPFBUF (gau1LogBuf),
                     "OSPF: Authentication Word  : "
                     "%02X %02X %02X %02X %02X %02X %02X %02X\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3],
                     au1TmpBuf[4], au1TmpBuf[5], au1TmpBuf[6], au1TmpBuf[7]);
        }
        else
        {
            /*Reserved */
            OSPF_CRU_BMC_GET_2_BYTE (pBuf, AUTH_1_OFFSET, u2TmpData);

            /*Key ID */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, AUTH_2_OFFSET, u1TmpData);

            SPRINTF (OSPFBUF (gau1LogBuf),
                     "OSPF: Reserved             : %u\n"
                     "OSPF: Key ID               : %u\n", u2TmpData, u1TmpData);

            /*Auth Data Len */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, AUTH_3_OFFSET, u1TmpData);

            /*Cryptographic sequence number */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, AUTH_4_OFFSET, u4TmpData);

            SPRINTF (OSPFBUF (gau1LogBuf),
                     "OSPF: Auth Data Len        : %u\n"
                     "OSPF: Crypt. Seq. Number   : %u\n", u1TmpData, u4TmpData);

            au1TmpBuf[0] = '\0';

            if (i4BufLength > i4DataLen)
            {
                UINT2               u2AuthCnt = 0;
                UINT2               u2AuthLen = 0;

                u2AuthLen = u1TmpData;
                /* Digest Data */
                for (i4ReadOffset = i4DataLen, u2AuthCnt = 0;
                     ((i4ReadOffset < i4BufLength) && (u2AuthCnt < u2AuthLen));
                     i4ReadOffset++, u2AuthCnt++)
                {
                    OSPF_CRU_BMC_GET_1_BYTE (pBuf, i4ReadOffset, u1TmpData);

                    SPRINTF (OSPFBUF (gau1LogBuf), "%02X ", u1TmpData);

                    if (!(u2AuthCnt % 16))
                    {
                        SPRINTF (OSPFBUF (gau1LogBuf), "\nOSPF: ");
                    }
                }
            }

            SPRINTF (OSPFBUF (gau1LogBuf),
                     "OSPF: Encrypted Key        : %s\n", au1TmpBuf);
        }

        if (STRLEN (gau1LogBuf))
        {
            /* Reset buffer after displaying,
               to avoid buffer space problems */
            DbgOspfUtlTrcPrint (gau1LogBuf);
            gau1LogBuf[0] = '\0';
        }

        if (i4DataLen > OS_HEADER_SIZE)
        {
            switch (u1PktType)
            {
                case HELLO_PKT:    /* Print the OSPF Hello Packet */
                    DbgOspfHelloDump (pBuf, gau1LogBuf, i4DataLen, DumpLevel);
                    break;

                case DD_PKT:
                    DbgOspfDdpDump (pBuf, gau1LogBuf, i4DataLen, DumpLevel);
                    break;

                case LSA_REQ_PKT:
                    DbgOspfLSRequestDump (pBuf, gau1LogBuf, i4DataLen,
                                          DumpLevel);
                    break;

                case LS_UPDATE_PKT:
                    DbgOspfLSUpdateDump (pBuf, gau1LogBuf, i4DataLen,
                                         DumpLevel);
                    break;

                case LSA_ACK_PKT:
                    DbgOspfLSAckDump (pBuf, gau1LogBuf, i4DataLen, DumpLevel);
                    break;

                default:

                    SPRINTF (OSPFBUF (gau1LogBuf), "\nOSPF:");

                    for (i4ReadOffset = OS_HEADER_SIZE;
                         (i4ReadOffset < i4BufLength); i4ReadOffset++)
                    {
                        OSPF_CRU_BMC_GET_1_BYTE (pBuf, i4ReadOffset, u1TmpData);

                        SPRINTF (OSPFBUF (gau1LogBuf), " %02X", u1TmpData);

                        if (!(i4ReadOffset % 16)
                            || ((i4ReadOffset + 1) == i4BufLength))
                        {
                            SPRINTF (OSPFBUF (gau1LogBuf), "\nOSPF:");
                        }

                        if (STRLEN (gau1LogBuf))
                        {
                            DbgOspfUtlTrcPrint (gau1LogBuf);
                            gau1LogBuf[0] = '\0';
                        }
                    }

                    break;
            }
        }

        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF:\t----------------- End OSPF Packet ----------------\n");
    }

    /* One liner packet dumping */
    if (DumpLevel == OSPF_HIGH_DUMP)
    {
        SPRINTF (OSPFBUF (gau1LogBuf),
                 "OSPF: %10s %s", DbgOspfFormatTime (u4RtrSysTime, au1TmpBuf),
                 ((Direction == OSPF_DIR_IN) ? "< " : "> "));

        OSPF_CRU_BMC_GET_1_BYTE (pBuf, VERSION_NO_OFFSET, u1TmpData);
        SPRINTF (OSPFBUF (gau1LogBuf), "OSPFv%d-", u1TmpData);

        switch (u1PktType)
        {
            case HELLO_PKT:
                SPRINTF (OSPFBUF (gau1LogBuf), "Hello ");
                break;

            case DD_PKT:
                SPRINTF (OSPFBUF (gau1LogBuf), "DDP ");
                break;

            case LSA_REQ_PKT:
                SPRINTF (OSPFBUF (gau1LogBuf), "LS Req ");
                break;

            case LS_UPDATE_PKT:
                SPRINTF (OSPFBUF (gau1LogBuf), "LS Update ");
                break;

            case LSA_ACK_PKT:
                SPRINTF (OSPFBUF (gau1LogBuf), "LS Ack ");
                break;

            default:
                SPRINTF (OSPFBUF (gau1LogBuf), "Unknown ");
                break;
        }

        OSPF_CRU_BMC_GET_2_BYTE (pBuf, PKT_LENGTH_OFFSET, u2TmpData);
        SPRINTF (OSPFBUF (gau1LogBuf), "Len:%d ", u2TmpData);

        /* Extract Router ID */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, RTR_ID_OFFSET,
                                 MAX_IP_ADDR_LEN);

        SPRINTF (OSPFBUF (gau1LogBuf), "Rtr:%d.%d.%d.%d ",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Extract Area ID */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, AREA_ID_OFFSET,
                                 MAX_IP_ADDR_LEN);

        SPRINTF (OSPFBUF (gau1LogBuf), "Area:%d.%d.%d.%d ",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        if (STRLEN (gau1LogBuf))
        {
            /* Reset buffer after displaying,
               to avoid buffer space problems */
            DbgOspfUtlTrcPrint (gau1LogBuf);
            gau1LogBuf[0] = '\0';
        }

        if (i4DataLen > OS_HEADER_SIZE)
        {
            switch (u1PktType)
            {
                case HELLO_PKT:    /* Print the OSPF Hello Packet */
                    DbgOspfHelloDump (pBuf, gau1LogBuf, i4DataLen, DumpLevel);
                    break;

                case DD_PKT:
                    DbgOspfDdpDump (pBuf, gau1LogBuf, i4DataLen, DumpLevel);
                    break;

                case LSA_REQ_PKT:
                    DbgOspfLSRequestDump (pBuf, gau1LogBuf, i4DataLen,
                                          DumpLevel);
                    break;

                case LS_UPDATE_PKT:
                    DbgOspfLSUpdateDump (pBuf, gau1LogBuf, i4DataLen,
                                         DumpLevel);
                    break;

                case LSA_ACK_PKT:
                    DbgOspfLSAckDump (pBuf, gau1LogBuf, i4DataLen, DumpLevel);
                    break;

                default:
                    SPRINTF (OSPFBUF (gau1LogBuf), "Unknown packet");
                    break;
            }
        }

        SPRINTF (OSPFBUF (gau1LogBuf), "\n");

    }                            /* end of high level dumping */

    if (STRLEN (gau1LogBuf))
    {
        DbgOspfUtlTrcPrint (gau1LogBuf);
        gau1LogBuf[0] = '\0';
    }

    OS_MEM_SET (gau1LogBuf, OSPF_ZERO, (OSPF_MAX_DUMP_BUF + 1));
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfHelloDump                                         */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfHelloDump (tCRU_BUF_CHAIN_HEADER * pBuf, CHR1 * pu1LogBuf,
                  INT4 i4DataLen, UINT1 DumpLevel)
{
    UINT1               u1TmpData;
    UINT2               u2TmpData;
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPF_MAX_LOG_STR_LEN + 1];

    au1TmpBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (i4DataLen >= (OSPF_HDR_LEN + OSPF_HELLO_LEN))
    {
        if (DumpLevel != OSPF_HIGH_DUMP)
        {
            /* Network Mask */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, OS_HEADER_SIZE,
                                     MAX_IP_ADDR_LEN);

            /* Hello interval */
            OSPF_CRU_BMC_GET_2_BYTE (pBuf, HP_HELLO_INTERVAL_OFFSET, u2TmpData);

            SPRINTF ((OSPFBUF (pu1LogBuf)),
                     "OSPF: ------ OSPF Hello ------\n"
                     "OSPF: Network Mask              : %d.%d.%d.%d\n"
                     "OSPF: Hello Interval            : %d (seconds)\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3],
                     u2TmpData);

            /* Options */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, HP_OPTIONS_OFFSET, u1TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: Options                   : 0x%02X\n"
                     "%s", u1TmpData,
                     DbgOspfOptionsDump (u1TmpData, "      ", au1TmpBuf));

            /* Router priority */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, HP_RTR_PRI_OFFSET, u1TmpData);

            /*Router Dead Interval */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, HP_RTR_DEAD_INTERVAL_OFFSET,
                                     u4TmpData);

            /* Designated Router */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     HP_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: Router Priority           : %u\n"
                     "OSPF: Router Dead Interval      : %u (seconds)\n"
                     "OSPF: Designated Router         : %d.%d.%d.%d\n",
                     u1TmpData,
                     u4TmpData,
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Backup Designated Router */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     HP_BK_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: Backup Designated Router  : %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Reset buffer after displaying, to avoid buffer space problems */
            DbgOspfUtlTrcPrint (pu1LogBuf);
            pu1LogBuf[0] = '\0';
            au1TmpBuf[0] = '\0';

            for (u2TmpData = HP_NBR_ID_OFFSET, u4TmpData = 1;
                 u2TmpData < i4DataLen;
                 u2TmpData += MAX_IP_ADDR_LEN, u4TmpData++)
            {
                if (i4DataLen < (u2TmpData + MAX_IP_ADDR_LEN))
                    break;

                /* Neighbors... */
                OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, u2TmpData,
                                         MAX_IP_ADDR_LEN);

                SPRINTF (OSPFBUF (pu1LogBuf),
                         "OSPF: Neighbor no.%-15d : %d.%d.%d.%d\n",
                         (UINT2) u4TmpData,
                         au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2],
                         au1TmpBuf[3]);

                if (STRLEN (pu1LogBuf))
                {
                    DbgOspfUtlTrcPrint (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else                    /* DumpLevel == OSPF_HIGH_DUMP */
        {
            /* Options */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, HP_OPTIONS_OFFSET, u1TmpData);
            SPRINTF (OSPFBUF (pu1LogBuf), "%s ",
                     OSPF_BIT (u1TmpData, 1) ? "E" : "");

            /* Router priority */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, HP_RTR_PRI_OFFSET, u1TmpData);

            /* Designated Router */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     HP_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "Pri:%u "
                     "DR:%d.%d.%d.%d ", u1TmpData,
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Backup Designated Router */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     HP_BK_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "BDR:%d.%d.%d.%d ",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Neighbors... */
            u4TmpData = 0;
            for (u2TmpData = HP_NBR_ID_OFFSET; u2TmpData < i4DataLen;
                 u2TmpData += MAX_IP_ADDR_LEN)
            {
                if (i4DataLen < (u2TmpData + MAX_IP_ADDR_LEN))
                    break;
                ++u4TmpData;
            }

            SPRINTF (OSPFBUF (pu1LogBuf), "Nbrs:%u ", u4TmpData);
        }
    }

    if (STRLEN (pu1LogBuf))
    {
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfDdpDump                                             */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped             */
/*                u2Len     : length of the information to be dumped         */
/*                u1Type    : packet type                                    */
/*                u1Flag    : flag indicating outgoing or incoming message   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DbgOspfDdpDump (tCRU_BUF_CHAIN_HEADER * pBuf, CHR1 * pu1LogBuf,
                INT4 i4DataLen, UINT1 DumpLevel)
{
    UINT1               u1TmpData;
    UINT2               u2TmpData;
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPF_MAX_LOG_STR_LEN + 1];

    au1TmpBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (i4DataLen >= (OSPF_HDR_LEN + OSPF_DDP_LEN))
    {
        if (DumpLevel != OSPF_HIGH_DUMP)
        {
            /* Interface MTU */
            OSPF_CRU_BMC_GET_2_BYTE (pBuf, OS_HEADER_SIZE, u2TmpData);

            /* Options */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, DDP_OPTIONS_OFFSET, u1TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF Database Description ----\n"
                     "OSPF: Interface MTU       : %u (bytes)\n"
                     "OSPF: Options             : 0x%02X\n"
                     "%s",
                     u2TmpData, u1TmpData,
                     DbgOspfOptionsDump (u1TmpData, "", au1TmpBuf));

            /* I/M/MS bits */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, DDP_MASK_OFFSET, u1TmpData);

            /* DD Sequence number */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, DDP_SEQ_NUM_OFFSET, u4TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: Flags               : 0x%02X\n"
                     "OSPF:  |01234567\n"
                     "OSPF:  |%u%u%u%u%u...          : Reserved %s\n"
                     "OSPF:  |.....%u..          : %sInitialize Sequence Number (I)\n"
                     "OSPF:  |......%u.          : %sMore Packets Follow (M)\n"
                     "OSPF:  |.......%u          : Router is %s (MS)\n"
                     "OSPF: DD Sequence Number  : %u (0x%08x)\n",
                     u1TmpData,
                     OSPF_BIT (u1TmpData, 7),
                     OSPF_BIT (u1TmpData, 6),
                     OSPF_BIT (u1TmpData, 5),
                     OSPF_BIT (u1TmpData, 4),
                     OSPF_BIT (u1TmpData, 3),
                     (u1TmpData & 0xF8) ? "(*Should be 0*)" : "",
                     OSPF_BIT (u1TmpData, 2),
                     OSPF_BIT (u1TmpData, 2) ? "" : "Do Not ",
                     OSPF_BIT (u1TmpData, 1),
                     OSPF_BIT (u1TmpData, 1) ? "" : "No ",
                     OSPF_BIT (u1TmpData, 0),
                     OSPF_BIT (u1TmpData, 0) ? "Master" : "Slave",
                     u4TmpData, u4TmpData);

            /* Reset buffer after displaying, to avoid buffer space problems */
            DbgOspfUtlTrcPrint (pu1LogBuf);
            pu1LogBuf[0] = '\0';
            au1TmpBuf[0] = '\0';

            DbgOspfLSAdvtDump (pBuf, pu1LogBuf, i4DataLen, OSPF_LSA_HDR);
        }
        else                    /* DumpLevel == OSPF_HIGH_DUMP */
        {
            /* Interface MTU */
            OSPF_CRU_BMC_GET_2_BYTE (pBuf, OS_HEADER_SIZE, u2TmpData);

            /* Options */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, DDP_OPTIONS_OFFSET, u1TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "MTU:%u "
                     "%s ", u2TmpData, OSPF_BIT (u1TmpData, 1) ? "E" : "");

            /* I/M/MS bits */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf, DDP_MASK_OFFSET, u1TmpData);

            /* DD Sequence number */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, DDP_SEQ_NUM_OFFSET, u4TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "%s%s%s "
                     "DD Seq# :%u",
                     OSPF_BIT (u1TmpData, 2) ? "I" : "",
                     OSPF_BIT (u1TmpData,
                               1) ? (OSPF_BIT (u1TmpData, 2) ? "/M" : "M") : "",
                     OSPF_BIT (u1TmpData,
                               0) ? ((OSPF_BIT (u1TmpData, 2)
                                      || OSPF_BIT (u1TmpData,
                                                   1)) ? "/MS" : "MS") : "",
                     u4TmpData);

            /* Reset buffer after displaying */
            DbgOspfUtlTrcPrint (pu1LogBuf);
            pu1LogBuf[0] = '\0';
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfLSRequestDump                                       */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfLSRequestDump (tCRU_BUF_CHAIN_HEADER * pBuf, CHR1 * pu1LogBuf,
                      INT4 i4DataLen, UINT1 DumpLevel)
{
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPF_MAX_LOG_STR_LEN];
    UINT1               au1LsId[MAX_IP_ADDR_LEN + 1];
    UINT1               au1AdvRtr[MAX_IP_ADDR_LEN + 1];

    UINT2               u2DataOffset = 0;    /* Data offset in buffer */
    UINT2               u2DataCnt = 0;    /* Pckt Length counter */

    au1TmpBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (DumpLevel != OSPF_HIGH_DUMP)
    {
        for (u2DataCnt = 1, u2DataOffset = OSPF_HDR_LEN;
             u2DataOffset < i4DataLen;
             u2DataCnt++, u2DataOffset += OSPF_LS_REQ_LEN)
        {
            /* Check for malformed packets */
            if (i4DataLen < (u2DataOffset + OSPF_LS_REQ_LEN))
            {
                break;
            }

            /* LS Type */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, u2DataOffset, u4TmpData);

            /* Link State ID */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1LsId,
                                     (u2DataOffset +
                                      OSPF_LSA_HDR_LNK_ID_OFFSET),
                                     MAX_IP_ADDR_LEN);

            /* Advertising Router */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1AdvRtr,
                                     (u2DataOffset +
                                      OSPF_LSA_HDR_ADV_RTR_OFFSET),
                                     MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF Link State Request %d ----\n"
                     "OSPF: LS Type             : %s (%u)\n"
                     "OSPF: Link State ID       : %d.%d.%d.%d\n"
                     "OSPF: Advertising Router  : %d.%d.%d.%d\n",
                     u2DataCnt,
                     DbgOspfLSTypeString (u4TmpData, au1TmpBuf), u4TmpData,
                     au1LsId[0], au1LsId[1], au1LsId[2], au1LsId[3],
                     au1AdvRtr[0], au1AdvRtr[1], au1AdvRtr[2], au1AdvRtr[3]);

            if (STRLEN (pu1LogBuf))
            {
                DbgOspfUtlTrcPrint (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }
        }
    }
    else                        /* DumpLevel == OSPF_HIGH_DUMP */
    {
        u2DataCnt = 0;
        for (u2DataOffset = OSPF_HDR_LEN; u2DataOffset < i4DataLen;
             u2DataOffset += OSPF_LS_REQ_LEN)
        {
            /* Check for malformed packets */
            if (i4DataLen < (u2DataOffset + OSPF_LS_REQ_LEN))
                break;
            ++u2DataCnt;
        }

        SPRINTF (OSPFBUF (pu1LogBuf), "# LSReq:%d ", u2DataCnt);
    }

    if (STRLEN (pu1LogBuf))
    {
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfLSUpdateDump                                        */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfLSUpdateDump (tCRU_BUF_CHAIN_HEADER * pBuf, CHR1 * pu1LogBuf,
                     INT4 i4DataLen, UINT1 DumpLevel)
{
    UINT4               u4TmpData;

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    /* Check for malformed packets */
    if (i4DataLen >= (OSPF_HDR_LEN + OSPF_LS_UPDATE_LEN))
    {
        if (DumpLevel != OSPF_HIGH_DUMP)
        {
            /* # LSAs */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, OSPF_HDR_LEN, u4TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF Link State Update ----\n"
                     "OSPF: # Advertisements    = %u\n", u4TmpData);

            /* Display Content before LSA data */
            DbgOspfUtlTrcPrint (pu1LogBuf);
            pu1LogBuf[0] = '\0';

            /* Parse the LSAs */
            DbgOspfLSAdvtDump (pBuf, pu1LogBuf, i4DataLen, OSPF_LSA_ALL);
        }
        else                    /* DumpLevel == OSPF_HIGH_DUMP */
        {
            /* # LSAs */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf, OSPF_HDR_LEN, u4TmpData);
            SPRINTF (OSPFBUF (pu1LogBuf), "# LSAs :%u ", u4TmpData);
        }
    }

    if (STRLEN (pu1LogBuf))
    {
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfLSAckDump                                           */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfLSAckDump (tCRU_BUF_CHAIN_HEADER * pBuf, CHR1 * pu1LogBuf,
                  INT4 i4DataLen, UINT1 DumpLevel)
{
    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (DumpLevel == OSPF_HIGH_DUMP)    /* Nothing to Dump */
        return;

    /* Check for malformed packets */
    if (i4DataLen >= (OSPF_HDR_LEN + OSPF_LSA_HDR_LEN))
    {
        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: ---- OSPF Link State Acknowledgment ----\n");

        /* Display Content before LSA data */
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';

        /* Parse the LSAs */
        DbgOspfLSAdvtDump (pBuf, pu1LogBuf, i4DataLen, OSPF_LSA_HDR);
    }

    if (STRLEN (pu1LogBuf))
    {
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfLSAdvtDump                                          */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DbgOspfLSAdvtDump (tCRU_BUF_CHAIN_HEADER * pBuf,
                   CHR1 * pu1LogBuf, INT4 i4DataLen, UINT1 u1Hdr)
{
    UINT1               u1TmpData;
    UINT2               u2TmpData;
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPF_MAX_LOG_STR_LEN + 1];

    UINT2               u2LsaPktLen = 0;
    UINT2               u2LsaOffset = 0;
    UINT4               u4HdrCnt = 0;
    UINT1               u1LsType;

    au1TmpBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    /* Extract the packet type */
    OSPF_CRU_BMC_GET_1_BYTE (pBuf, TYPE_OFFSET, u1TmpData);

    if (u1TmpData == DD_PKT)
        u2LsaOffset = OSPF_DDP_LSA_OFFSET;
    else if (u1TmpData == LS_UPDATE_PKT)
        u2LsaOffset = OSPF_LSU_LSA_OFFSET;
    else if (u1TmpData == LSA_ACK_PKT)
        u2LsaOffset = OSPF_LSA_LSA_OFFSET;
    /* Skip the Dump for other packets */
    else
        u2LsaOffset = (UINT2) i4DataLen;

    for (u4HdrCnt = 1; u2LsaOffset < i4DataLen; u4HdrCnt++)
    {
        /* Check for truncated LSA advertisements */
        if (i4DataLen < (u2LsaOffset + OSPF_LSA_HDR_LEN))
        {
            break;
        }

        /* LS age */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_AGE_OFFSET), u2TmpData);

        /* Options */
        OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_OPT_OFFSET), u1TmpData);

        /* LS type */
        OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_LSTYPE_OFFSET),
                                 u1LsType);

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: ---- OSPF Link State Advertisement Header %u ----\n"
                 "OSPF: LS Age              : %u%s seconds\n"
                 "OSPF: Options             : 0x%02X\n"
                 "%s",
                 u4HdrCnt,
                 u2TmpData, (u2TmpData >= OSPF_MAX_AGE) ? " (MaxAge)" : "",
                 u1TmpData, DbgOspfOptionsDump (u1TmpData, "", au1TmpBuf));

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: LS Type             : %s (%u)\n",
                 DbgOspfLSTypeString ((UINT4) u1LsType, au1TmpBuf), u1LsType);

        /* Link State ID */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_LNK_ID_OFFSET),
                                 MAX_IP_ADDR_LEN);

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: Link State ID       : %d.%d.%d.%d\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Advertising Router */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_ADV_RTR_OFFSET),
                                 MAX_IP_ADDR_LEN);

        /* LS sequence number */
        OSPF_CRU_BMC_GET_4_BYTE (pBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_SEQ_NO_OFFSET),
                                 u4TmpData);

        /* LS checksum */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_CHECKSUM_OFFSET),
                                 u2TmpData);

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: Advertising Router  : %d.%d.%d.%d\n"
                 "OSPF: LS Sequence Number  : 0x%08x\n"
                 "OSPF: LS Checksum         : 0x%04X\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3],
                 u4TmpData, u2TmpData);

        /* LSA length */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf,
                                 (UINT4) (u2LsaOffset +
                                          OSPF_LSA_HDR_LENGTH_OFFSET),
                                 u2LsaPktLen);

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: Length              : %u bytes\n"
                 "OSPF:\n", u2LsaPktLen);

        /* Update the ReadOffset with bytes read */
        u2LsaOffset += OSPF_LSA_HDR_LEN;

        /* Reset buffer after displaying, to avoid buffer space problems */
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';

        /* If only LS header is to be dumped, break now */
        if (u1Hdr)
        {
            continue;
        }

        /* Adjust the packet length with bytes read */
        if (u2LsaPktLen >= OSPF_LSA_HDR_LEN)
        {
            u2LsaPktLen -= OSPF_LSA_HDR_LEN;
        }
        else
        {
            u2LsaPktLen = OSPF_ZERO;
        }

        /* parse the LSA contents */

        if ((u1LsType == ROUTER_LSA) &&
            (i4DataLen >= (u2LsaOffset + OSPF_ROUTER_LSA_HDR_LEN)) &&
            (u2LsaPktLen >= OSPF_ROUTER_LSA_HDR_LEN))
        {

            /* Flags */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                     (u2LsaOffset + OSPF_RTR_LSA_FLAG_OFFSET),
                                     u1TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF Router Links Advertisement ----\n"
                     "OSPF: Flags          : 0x%02X\n"
                     "OSPF:  |01234567\n"
                     "OSPF:  |%u%u%u%u%u...     : Reserved %s\n"
                     "OSPF:  |.....%u..     : Router %s End of Virtual Link (V)\n"
                     "OSPF:  |......%u.     : Router %s an AS Boundary Router (E)\n"
                     "OSPF:  |.......%u     : Router %s an Area Border Router (B)\n",
                     u1TmpData,
                     OSPF_BIT (u1TmpData, 7),
                     OSPF_BIT (u1TmpData, 6),
                     OSPF_BIT (u1TmpData, 5),
                     OSPF_BIT (u1TmpData, 4),
                     OSPF_BIT (u1TmpData, 3),
                     (u1TmpData & 0xF8) ? "(*Should be 0*)" : "",
                     OSPF_BIT (u1TmpData, 2),
                     OSPF_BIT (u1TmpData, 2) ? "Is" : "Is Not",
                     OSPF_BIT (u1TmpData, 1),
                     OSPF_BIT (u1TmpData, 1) ? "Is" : "Is Not",
                     OSPF_BIT (u1TmpData, 0),
                     OSPF_BIT (u1TmpData, 0) ? "Is" : "Is Not");

            /* Reserved word */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                     (u2LsaOffset + OSPF_RTR_LSA_RESERV_OFFSET),
                                     u1TmpData);

            /*  # links */
            OSPF_CRU_BMC_GET_2_BYTE (pBuf,
                                     (u2LsaOffset +
                                      OSPF_RTR_LSA_NO_LINK_OFFSET), u2TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: Reserved       : 0x%02X %s\n"
                     "OSPF: # Links        : %u\n",
                     u1TmpData, (u1TmpData != 0) ? "(*Should be 0*)" : "",
                     u2TmpData);

            /* Update the ReadOffset with bytes read */
            u2LsaOffset += OSPF_ROUTER_LSA_HDR_LEN;

            /* Reset buffer after displaying */
            DbgOspfUtlTrcPrint (pu1LogBuf);
            pu1LogBuf[0] = '\0';
            u4TmpData = 0;

            if ((u4TmpData = DbgOspfRouterLinkTuplesDump (pBuf, pu1LogBuf,
                                                          i4DataLen,
                                                          u2LsaOffset)) >
                u2LsaOffset)
            {

                /* Check weather all LSA data has printed propeply */
                if ((u4TmpData - (u2LsaOffset - OSPF_ROUTER_LSA_HDR_LEN))
                    != u2LsaPktLen)
                {
                    SPRINTF (OSPFBUF (pu1LogBuf),
                             "OSPF:\n"
                             "OSPF: --- Corrupted LSA, packet length error ---\n"
                             "OSPF: --- Remaining data parsing will be erroneous --\n"
                             "OSPF:\n");

                    /*DbgOspfUtlTrcPrint(pu1LogBuf); */
                }

                u2LsaOffset = (UINT2) u4TmpData;
            }
        }
        else if ((u1LsType == NETWORK_LSA) &&
                 (i4DataLen >= (u2LsaOffset + OSPF_NETWORK_LSA_HDR_LEN)) &&
                 (u2LsaPktLen >= OSPF_NETWORK_LSA_HDR_LEN))
        {
            /* Network Mask */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     (u2LsaOffset + OSPF_RTR_LSA_FLAG_OFFSET),
                                     MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF Network Links Advertisement ----\n"
                     "OSPF: Network Mask         : %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Update for read packet data */
            u2LsaOffset += OSPF_NETWORK_LSA_HDR_LEN;

            for (u4TmpData = OSPF_NETWORK_LSA_HDR_LEN;
                 ((u4TmpData < u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u4TmpData += MAX_IP_ADDR_LEN, u2LsaOffset += MAX_IP_ADDR_LEN)
            {
                /* Check for malformed Router link tuples */
                if ((u2LsaPktLen < (u4TmpData + MAX_IP_ADDR_LEN)) ||
                    (i4DataLen < (u2LsaOffset + MAX_IP_ADDR_LEN)))
                {
                    break;
                }

                /* Attached Router */
                OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                         u2LsaOffset, MAX_IP_ADDR_LEN);

                SPRINTF (OSPFBUF (pu1LogBuf),
                         "OSPF: Attached Router      : %d.%d.%d.%d\n",
                         au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2],
                         au1TmpBuf[3]);

                if (STRLEN (pu1LogBuf))
                {
                    /* Reset buffer after displaying */
                    DbgOspfUtlTrcPrint (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else if (((u1LsType == NETWORK_SUM_LSA) || (u1LsType == ASBR_SUM_LSA))
                 && ((i4DataLen >= (u2LsaOffset + OSPF_SUMMARY_LSA_HDR_LEN))
                     && (u2LsaPktLen >= OSPF_SUMMARY_LSA_HDR_LEN)))
        {
            /* Network Mask */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                     MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF Summary Link Advertisement ----\n"
                     "OSPF: Network Mask              : %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Update for remaining packet data */
            u2LsaOffset += OSPF_SUMMARY_LSA_HDR_LEN;

            for (u2TmpData = OSPF_SUMMARY_LSA_HDR_LEN;
                 ((u2TmpData < u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u2TmpData += OSPF_TOS_METRIC_LEN,
                 u2LsaOffset += OSPF_TOS_METRIC_LEN)
            {
                /* Check bad len metrics */
                if ((u2LsaPktLen < (u2TmpData + OSPF_TOS_METRIC_LEN)) ||
                    (i4DataLen < (u2LsaOffset + OSPF_TOS_METRIC_LEN)))
                {
                    break;
                }

                /* TOS */
                OSPF_CRU_BMC_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);

                /* Copy the TOS + Metric */
                OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                         u2LsaOffset, OSPF_TOS_METRIC_LEN);

                /* Extract the 3 byte Metric portion */
                /*u4TmpData = LGET3BYTE ((UINT1 *)&au1TmpBuf[1]); */
                {
                    UINT1              *pu1Metric;
                    pu1Metric = (UINT1 *) &(au1TmpBuf[1]);
                    u4TmpData = LGET3BYTE (pu1Metric);
                }

                SPRINTF (OSPFBUF (pu1LogBuf),
                         "OSPF: ---- OSPF TOS Metric %d ----\n"
                         "OSPF: TOS             : %u (%s)\n"
                         "OSPF: Metric          : %u (0x%06x)%s\n",
                         ((u2TmpData - OSPF_SUMMARY_LSA_HDR_LEN) /
                          OSPF_TOS_METRIC_LEN + 1),
                         u1TmpData,
                         OspfTOSToString (u1TmpData, au1TmpBuf),
                         u4TmpData, u4TmpData,
                         (u4TmpData >= LS_INFINITY_24BIT) ? " LSInfinity" : "");

                if (STRLEN (pu1LogBuf))
                {
                    /* Reset buffer after displaying */
                    DbgOspfUtlTrcPrint (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else if (((u1LsType == AS_EXT_LSA) ||
                  (u1LsType == DEFAULT_NETWORK_SUM_LSA)) &&
                 ((i4DataLen >= (u2LsaOffset + OSPF_AS_EXTERN_LSA_HDR_LEN)) &&
                  (u2LsaPktLen >= OSPF_AS_EXTERN_LSA_HDR_LEN)))
        {
            /* Network Mask */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                     MAX_IP_ADDR_LEN);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF AS External Link Advertisement ----\n"
                     "OSPF: Network Mask              : %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Update for remaining packet data */
            u2LsaOffset += OSPF_AS_EXTERN_LSA_HDR_LEN;

            for (u2TmpData = OSPF_AS_EXTERN_LSA_HDR_LEN;
                 ((u2TmpData < u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u2TmpData += OSPF_TOS_ROUTE_LEN,
                 u2LsaOffset += OSPF_TOS_ROUTE_LEN)
            {
                /* Check bad len metrics */
                if ((u2LsaPktLen < (u2TmpData + OSPF_TOS_ROUTE_LEN)) ||
                    (i4DataLen < (u2LsaOffset + OSPF_TOS_ROUTE_LEN)))
                {
                    break;
                }

                /* E-bit & TOS */
                OSPF_CRU_BMC_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);

                SPRINTF (OSPFBUF (pu1LogBuf),
                         "OSPF: ---- OSPF External TOS Route %d ----\n"
                         "OSPF: External Metric Type  : %s\n",
                         ((u2TmpData - OSPF_AS_EXTERN_LSA_HDR_LEN) /
                          OSPF_TOS_ROUTE_LEN + 1),
                         (u1TmpData & ~((UINT1) OSPF_AS_EXT_LSA_TOS_MASK)) ?
                         "Type 2 (1)" : "Type 1 (0)");

                /* Update the TOS bits */
                u1TmpData &= (UINT1) OSPF_AS_EXT_LSA_TOS_MASK;

                /* Copy the TOS + Metric */
                OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                         u2LsaOffset, OSPF_TOS_METRIC_LEN);

                /* Extract the 3 byte Metric portion */
                /*u4TmpData = LGET3BYTE ((UINT1 *)&au1TmpBuf[1]); */
                {
                    UINT1              *pu1Metric;
                    pu1Metric = (UINT1 *) &(au1TmpBuf[1]);
                    u4TmpData = LGET3BYTE (pu1Metric);
                }

                SPRINTF (OSPFBUF (pu1LogBuf),
                         "OSPF: TOS                   : %u (%s)\n"
                         "OSPF: Metric                : %u (0x%06x)%s\n",
                         u1TmpData, OspfTOSToString (u1TmpData, au1TmpBuf),
                         u4TmpData, u4TmpData,
                         (u4TmpData >= LS_INFINITY_24BIT) ? " LSInfinity" : "");

                /* Forwarding address */
                OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                         (u2LsaOffset + OSPF_TOS_METRIC_LEN),
                                         MAX_IP_ADDR_LEN);

                /* External Route Tag */
                OSPF_CRU_BMC_GET_4_BYTE (pBuf,
                                         (u2LsaOffset +
                                          2 * OSPF_TOS_METRIC_LEN), u4TmpData);

                SPRINTF (OSPFBUF (pu1LogBuf),
                         "OSPF: Forwarding Address    : %d.%d.%d.%d\n"
                         "OSPF: External Route Tag    : 0x%08x\n",
                         au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3],
                         u4TmpData);

                if (STRLEN (pu1LogBuf))
                {
                    /* Reset buffer after displaying */
                    DbgOspfUtlTrcPrint (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else if ((u1LsType == TYPE9_OPQ_LSA) ||
                 (u1LsType == TYPE10_OPQ_LSA) || (u1LsType == TYPE11_OPQ_LSA))
        {
            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- Opaque Data ----\n" "OSPF:");

            for (u2TmpData = 1;
                 ((u2TmpData <= u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u2TmpData++, u2LsaOffset++)
            {
                OSPF_CRU_BMC_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);

                SPRINTF (OSPFBUF (pu1LogBuf), " %02X", u1TmpData);

                if (!(u2TmpData % 4))
                {
                    SPRINTF (OSPFBUF (pu1LogBuf), " ");
                }

                if ((!(u2TmpData % 16)) || (u2TmpData == u2LsaPktLen))
                {
                    SPRINTF (OSPFBUF (pu1LogBuf), "\nOSPF:");
                }

                if (STRLEN (pu1LogBuf))
                {
                    DbgOspfUtlTrcPrint (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else
        {
            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- Unknown OSPF Link State Advertisement ----\n"
                     "OSPF:");

            for (u2TmpData = 1;
                 ((u2TmpData <= u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u2TmpData++, u2LsaOffset++)
            {
                OSPF_CRU_BMC_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);

                SPRINTF (OSPFBUF (pu1LogBuf), " %02X", u1TmpData);

                if (!(u2TmpData % 4))
                {
                    SPRINTF (OSPFBUF (pu1LogBuf), " ");
                }

                if ((!(u2TmpData % 16)) || (u2TmpData == u2LsaPktLen))
                {
                    SPRINTF (OSPFBUF (pu1LogBuf), "\nOSPF:");
                }

                if (STRLEN (pu1LogBuf))
                {
                    DbgOspfUtlTrcPrint (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }

        if (STRLEN (pu1LogBuf))
        {
            /* Reset buffer after displaying */
            DbgOspfUtlTrcPrint (pu1LogBuf);
            pu1LogBuf[0] = '\0';
        }
    }

    if (STRLEN (pu1LogBuf))
    {
        /* Reset buffer after displaying */
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfOptionsDump                                         */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
DbgOspfOptionsDump (UINT1 u1Options, CONST CHR1 * pu1Pad, UINT1 *pu1String)
{
    SPRINTF ((CHR1 *) pu1String,
             "OSPF:  |01234567\n"
             "OSPF:  |%u%u%u%u%u%u..          %s: Reserved %s\n"
             "OSPF:  |......%u.          %s: ExternalRoutingCapability %s\n"
             "OSPF:  |.......%u          %s: %s TOS Capability\n",
             OSPF_BIT (u1Options, 7),
             OSPF_BIT (u1Options, 6),
             OSPF_BIT (u1Options, 5),
             OSPF_BIT (u1Options, 4),
             OSPF_BIT (u1Options, 3),
             OSPF_BIT (u1Options, 2),
             pu1Pad,
             (u1Options & 0xFC) ? "(*Should be 0*)" : "",
             OSPF_BIT (u1Options, 1),
             pu1Pad,
             OSPF_BIT (u1Options, 1) ? "Enabled" : "Disabled",
             OSPF_BIT (u1Options, 0),
             pu1Pad, OSPF_BIT (u1Options, 0) ? "Normal" : "Single");

    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfLSTypeString                                        */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped             */
/*                u2Len     : length of the information to be dumped         */
/*                u1Type    : packet type                                    */
/*                u1Flag    : flag indicating outgoing or incoming message   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
DbgOspfLSTypeString (UINT4 u4LStype, UINT1 *pu1String)
{
    switch (u4LStype)
    {
        case ROUTER_LSA:
            SPRINTF ((CHR1 *) pu1String, "Router LSA");
            break;
        case NETWORK_LSA:
            SPRINTF ((CHR1 *) pu1String, "Network LSA");
            break;
        case NETWORK_SUM_LSA:
            SPRINTF ((CHR1 *) pu1String, "Summary LSA (IP network)");
            break;
        case ASBR_SUM_LSA:
            SPRINTF ((CHR1 *) pu1String, "Summary LSA (ASBR)");
            break;
        case AS_EXT_LSA:
            SPRINTF ((CHR1 *) pu1String, "AS External LSA");
            break;
        case DEFAULT_NETWORK_SUM_LSA:
            SPRINTF ((CHR1 *) pu1String, "NSSA AS External LSA");
            break;
        case TYPE9_OPQ_LSA:
            SPRINTF ((CHR1 *) pu1String, "Link Local Opaque LSA");
            break;
        case TYPE10_OPQ_LSA:
            SPRINTF ((CHR1 *) pu1String, "Area Local Opaque LSA");
            break;
        case TYPE11_OPQ_LSA:
            SPRINTF ((CHR1 *) pu1String, "AS Wide Opaque LSA");
            break;
        default:
            SPRINTF ((CHR1 *) pu1String, "*Unknown OSPF LS type %u*", u4LStype);
            break;
    }

    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfRouterLinkTuplesDump                                */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT2
DbgOspfRouterLinkTuplesDump (tCRU_BUF_CHAIN_HEADER * pBuf,
                             CHR1 * pu1LogBuf, INT4 i4DataLen, UINT2 u2OffSet)
{
    UINT1               u1TmpData;
    UINT2               u2TmpData;
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPF_MAX_LOG_STR_LEN + 1];
    CHR1                au1TypeBuf[OSPF_MAX_LOG_STR_LEN + 1];

    UINT2               u2LsaLen = 0;
    UINT2               u2NumOfLink = 0;
    UINT2               u2LinkLen = 0;
    UINT2               u2LnkCnt = 0;
    UINT1               u1TosCnt = 0;
    UINT1               u1TotTos = 0;

    au1TmpBuf[0] = '\0';
    au1TypeBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL) || (u2OffSet == OSPF_ZERO))
    {
        return u2OffSet;
    }

    /* Get the Length of current LSA */
    OSPF_CRU_BMC_GET_2_BYTE (pBuf, (u2OffSet - OSPF_ROUTER_LSA_HDR_LEN
                                    - OSPF_LSA_HDR_LEN +
                                    OSPF_LSA_HDR_LENGTH_OFFSET), u2LsaLen);

    /* Get no.of links in current LSA */
    OSPF_CRU_BMC_GET_2_BYTE (pBuf, (u2OffSet - OSPF_ROUTER_LSA_HDR_LEN
                                    + OSPF_RTR_LSA_NO_LINK_OFFSET),
                             u2NumOfLink);

    /* Update length with bytes read */
    if (u2LsaLen >= (OSPF_LSA_HDR_LEN + OSPF_ROUTER_LSA_HDR_LEN))
    {
        u2LsaLen -= (OSPF_LSA_HDR_LEN + OSPF_ROUTER_LSA_HDR_LEN);
    }
    else
    {
        u2LsaLen = OSPF_ZERO;
    }

    if (i4DataLen < (u2OffSet + u2LsaLen))
    {
        DbgOspfUtlTrcPrint ("\nOSPF: Invalid Router LSA packet length \n");
        /*return u2OffSet; */
    }

    for (u2LnkCnt = 1, u2LinkLen = 0;
         ((u2LnkCnt <= u2NumOfLink) && (u2LinkLen < u2LsaLen) &&
          (u2OffSet < i4DataLen)); u2LnkCnt++)
    {
        /* Check for malformed Router link tuples */
        if ((u2LsaLen < (u2LinkLen + OSPF_ROUTER_LSA_TUPLE_HDR_LEN)) ||
            (i4DataLen < (u2OffSet + OSPF_ROUTER_LSA_TUPLE_HDR_LEN)))
        {
            break;
        }

        /* Link ID */
        OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                 (u2OffSet + OSPF_RTR_LSA_LINK_ID_OFFSET),
                                 MAX_IP_ADDR_LEN);

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: ---- OSPF Router Link Tuple %d ----\n"
                 "OSPF: Link ID             : %d.%d.%d.%d\n", u2LnkCnt,
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Link Type */
        OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                 (u2OffSet + OSPF_RTR_LSA_LINK_TYPE_OFFSET),
                                 u1TmpData);

        /* Link Data */
        if (u1TmpData == OSPF_ROUTER_LINK_TYPE_PNT_TO_PNT)
        {
            /* MIB - II If index */
            OSPF_CRU_BMC_GET_4_BYTE (pBuf,
                                     (u2OffSet + OSPF_RTR_LSA_LINK_DATA_OFFSET),
                                     u4TmpData);

            SNPRINTF (au1TypeBuf, sizeof (au1TypeBuf),
                      "0x%08x (Interface Index)", u4TmpData);
        }
        else
        {
            /* IP Address or Mask */
            OSPF_CRU_BMC_GET_STRING (pBuf, au1TmpBuf,
                                     (u2OffSet + OSPF_RTR_LSA_LINK_DATA_OFFSET),
                                     MAX_IP_ADDR_LEN);

            SNPRINTF (au1TypeBuf, sizeof (au1TypeBuf), "%d.%d.%d.%d",
                      au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);
        }

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: Link Data           : %s\n"
                 "OSPF: Type                : %s\n",
                 au1TypeBuf, DbgOspfRouterLinkTypeDump (u1TmpData, au1TmpBuf));

        /* # TOS */
        OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                 (u2OffSet + OSPF_RTR_LSA_NUM_OF_TOS_OFFSET),
                                 u1TotTos);

        /* Zero Metric */
        OSPF_CRU_BMC_GET_2_BYTE (pBuf,
                                 (u2OffSet + OSPF_RTR_LSA_ZERO_METRIC_OFFSET),
                                 u2TmpData);

        SPRINTF (OSPFBUF (pu1LogBuf),
                 "OSPF: # TOS               : %u\n"
                 "OSPF: TOS 0 Metric        : %u\n", u1TotTos, u2TmpData);

        /* Update the Bytes read so far */
        u2LinkLen += OSPF_ROUTER_LSA_TUPLE_HDR_LEN;
        u2OffSet += OSPF_ROUTER_LSA_TUPLE_HDR_LEN;

        for (u1TosCnt = 0; u1TosCnt < u1TotTos; u1TosCnt++)
        {
            /* Check for bad tos metric */
            if ((u2LsaLen < (u2LinkLen + OSPF_TOS_METRIC_LEN)) ||
                (i4DataLen < (u2OffSet + OSPF_TOS_METRIC_LEN)))
            {
                break;
            }

            /* TOS */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                     (u2OffSet + OSPF_RTR_LSA_TOS_OFFSET),
                                     u1TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: ---- OSPF TOS Metric %d ----\n"
                     "OSPF: TOS             : %u (%s)\n",
                     (u1TosCnt + 1), u1TmpData,
                     OspfTOSToString (u1TmpData, au1TmpBuf));

            /* Reserved */
            OSPF_CRU_BMC_GET_1_BYTE (pBuf,
                                     (u2OffSet + OSPF_RTR_LSA_RESERV_OFFSET),
                                     u1TmpData);

            /* TOS Metric */
            OSPF_CRU_BMC_GET_2_BYTE (pBuf,
                                     (u2OffSet +
                                      OSPF_RTR_LSA_TOS_METRIC_OFFSET),
                                     u2TmpData);

            SPRINTF (OSPFBUF (pu1LogBuf),
                     "OSPF: Reserved        : 0x%02X %s\n"
                     "OSPF: Metric          : %u (0x%02X)\n",
                     u1TmpData, (u1TmpData != 0) ? "(*Should be 0*)" : "",
                     u2TmpData, u2TmpData);

            /* Update the Bytes read so far */
            u2LinkLen += OSPF_TOS_METRIC_LEN;
            u2OffSet += OSPF_TOS_METRIC_LEN;

            if (STRLEN (pu1LogBuf))
            {
                /* Reset buffer after displaying */
                DbgOspfUtlTrcPrint (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }
        }
    }

    if (STRLEN (pu1LogBuf))
    {
        /* Reset buffer after displaying */
        DbgOspfUtlTrcPrint (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return u2OffSet;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgOspfRouterLinkTypeDump                                  */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped            */
/*                u2Len     : length of the information to be dumped        */
/*                u1Type    : packet type                                   */
/*                u1Flag    : flag indicating outgoing or incoming message  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
DbgOspfRouterLinkTypeDump (UINT1 u1Lnktype, UINT1 *pu1String)
{
    switch (u1Lnktype)
    {
        case OSPF_ROUTER_LINK_TYPE_PNT_TO_PNT:
            SPRINTF ((CHR1 *) pu1String, "Point-to-Point (%u)", u1Lnktype);
            break;

        case OSPF_ROUTER_LINK_TYPE_TRANSIT:
            SPRINTF ((CHR1 *) pu1String, "Connection to Transit Network (%u)",
                     u1Lnktype);
            break;

        case OSPF_ROUTER_LINK_TYPE_STUB:
            SPRINTF ((CHR1 *) pu1String, "Connection to Stub Network (%u)",
                     u1Lnktype);
            break;

        case OSPF_ROUTER_LINK_TYPE_VIRTUAL:
            SPRINTF ((CHR1 *) pu1String, "Virtual Link (%u)", u1Lnktype);
            break;

        default:
            SPRINTF ((CHR1 *) pu1String, "*Unknown OSPF Router Link type (%u)*",
                     u1Lnktype);
            break;
    }
    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  DbgOspfFormatTime                                         */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : u4TimeSec : Time value in sec                              */
/*                pu1String : Formatted time in HH:MM:SS                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
DbgOspfFormatTime (UINT4 u4TimeSec, UINT1 *pu1String)
{
    UINT4               u4Hour = 0;
    UINT4               u4Minute = 0;
    UINT4               u4Sec = 0;

    u4Hour = u4TimeSec / OSPF_SECS_IN_HOUR;
    u4Minute = (u4TimeSec - u4Hour * OSPF_SECS_IN_HOUR) / OSPF_SECS_IN_MIN;
    u4Sec = u4TimeSec % OSPF_SECS_IN_MIN;

    SPRINTF ((CHR1 *) pu1String, "%02u:%02u:%02u", u4Hour, u4Minute, u4Sec);

    return pu1String;
}

/***************************************************************/
/*  Function Name   : DbgOspfUtlTrcPrint                       */
/*  Description     : Prints the input message                 */
/*  Input(s)        : pi1Msg - pointer to the message          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
DbgOspfUtlTrcPrint (CONST CHR1 * pu1Msg)
{
    UtlTrcPrint (pu1Msg);

    /* Logic for outputing to disk file */
    /*   UINT4               u4Count = 0;
       CONST UINT1        *pu1Cnt = pu1Msg; */
    /*INT4            i4Fd=ERROR; */

    /*
       while (*pu1Cnt++)
       {
       u4Count++;
       } */

    /* Prints on screen */
    /*write (1, pu1Msg, u4Count); */

    /* Prints in file */

    /*i4Fd = open("OspfLogFile", O_CREAT|O_WRONLY|O_TRUNC|O_NONBLOCK,
       S_IRWXU|S_IRWXG|S_IRWXO); */

    /* i4Fd = open ("OspfLogFile", O_CREAT | O_WRONLY | O_APPEND | O_NONBLOCK,
       S_IRWXU | S_IRWXG | S_IRWXO);

       if (i4Fd == ERROR)
       return;

       write (i4Fd, pu1Msg, u4Count);

       close (i4Fd);
     */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgPrintOspfRtTable                                        */
/*                                                                           */
/* Description  : Prints the ospf routing information                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
DbgPrintOspfRtTable (tOspfRt * pOspfRt)
#else
PUBLIC VOID
DbgPrintOspfRtTable (pOspfRt)
     tOspfRt            *pOspfRt;
#endif
{
    tRtEntry           *pRtEntry;

    UtlTrcPrint ("\nOSPF ROUTING TABLE\n");

    TMO_SLL_Scan (&(pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        DbgPrintRtEntry (pRtEntry);
    }
}
#endif /* DEBUG_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTOSToString                                            */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : pBuf      : pointer to the buffer to be dumped             */
/*                u2Len     : length of the information to be dumped         */
/*                u1Type    : packet type                                    */
/*                u1Flag    : flag indicating outgoing or incoming message   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTOSToString (UINT1 u1Tos, UINT1 *pu1String)
{
    CHR1 CONST         *au1TosTable[] = {
        "0000 normal service",
        "0001 minimize monetary cost",
        "0010 maximize reliability",
        "0011",
        "0100 maximize throughput",
        "0101",
        "0110",
        "0111",
        "1000 minimize delay",
        "1001",
        "1010",
        "1011",
        "1100",
        "1101",
        "1110",
        "1111"
    };

    if (((u1Tos % 2) == 1) || (u1Tos > 30))
    {
        SPRINTF ((CHR1 *) pu1String, "*Unknown OSPF TOS %u*", u1Tos);
    }
    else
    {
        SPRINTF ((CHR1 *) pu1String, "IP TOS %s", au1TosTable[u1Tos / 2]);
    }

    return pu1String;
}

/*---------------------------------------------------------------------------*/
/*                          End of file osdbg.c                              */
/*---------------------------------------------------------------------------*/
