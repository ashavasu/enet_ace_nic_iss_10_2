/*$Id: fsospfwr.c,v 1.20 2014/07/19 13:06:58 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsospfwr.h"
# include  "fsospfdb.h"
#include    "ospf.h"
#include "osinc.h"
VOID
RegisterFSOSPF ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsospfOID, &fsospfEntry, OspfLock,
                                         OspfUnLock, UtilOspfSetContext,
                                         UtilOspfResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsospfOID, (const UINT1 *) "fsospf");
}

VOID
UnRegisterFSOSPF ()
{
    SNMPUnRegisterMib (&fsospfOID, &fsospfEntry);
    SNMPDelSysorEntry (&fsospfOID, (const UINT1 *) "fsospf");
}

INT4
FutOspfOverFlowStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfOverFlowState (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfPktsRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfPktsRcvd (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfPktsTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfPktsTxed (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfPktsDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfPktsDisd (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfRFC1583CompatibilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRFC1583Compatibility (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMaxAreasGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMaxAreas (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMaxLSAperAreaGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMaxLSAperArea (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMaxExtLSAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMaxExtLSAs (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMaxSelfOrgLSAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMaxSelfOrgLSAs (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMaxRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMaxRoutes (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMaxLsaSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMaxLsaSize (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfMinLsaIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfMinLsaInterval (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfABRTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfABRType (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfNssaAsbrDefRtTransGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfNssaAsbrDefRtTrans (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfDefaultPassiveInterfaceGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfDefaultPassiveInterface
            (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfSpfHoldtimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfSpfHoldtime (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfSpfDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfSpfDelay (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRTStaggeringIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRTStaggeringInterval (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfRTStaggeringStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRTStaggeringStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfHotStandbyAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfHotStandbyAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfHotStandbyStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfHotStandbyState (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfDynamicBulkUpdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfDynamicBulkUpdStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfStanbyHelloSyncCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfStanbyHelloSyncCount (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfStanbyLsaSyncCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfStanbyLsaSyncCount (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfRestartSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartSupport (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRestartIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartInterval (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRestartStrictLsaCheckingGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartStrictLsaChecking
            (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRestartStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRestartAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartAge (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfRestartExitReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartExitReason (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfHelperSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfHelperSupport (pMultiData->pOctetStrValue));
}

INT4
FutOspfExtTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfExtTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfHelperGraceTimeLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfHelperGraceTimeLimit (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRestartAckStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartAckState (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfGraceLsaRetransmitCountGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfGraceLsaRetransmitCount
            (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRestartReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRestartReason (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfBfdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfBfdStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfBfdAllIfStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfBfdAllIfState (&(pMultiData->i4_SLongValue)));
}

INT4
FutospfRouterIdPermanenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutospfRouterIdPermanence (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRFC1583CompatibilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRFC1583Compatibility (pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxAreasSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMaxAreas (pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxLSAperAreaSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMaxLSAperArea (pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxExtLSAsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMaxExtLSAs (pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxSelfOrgLSAsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMaxSelfOrgLSAs (pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMaxRoutes (pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxLsaSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMaxLsaSize (pMultiData->i4_SLongValue));
}

INT4
FutOspfTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FutOspfMinLsaIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfMinLsaInterval (pMultiData->i4_SLongValue));
}

INT4
FutOspfABRTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfABRType (pMultiData->i4_SLongValue));
}

INT4
FutOspfNssaAsbrDefRtTransSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfNssaAsbrDefRtTrans (pMultiData->i4_SLongValue));
}

INT4
FutOspfDefaultPassiveInterfaceSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfDefaultPassiveInterface (pMultiData->i4_SLongValue));
}

INT4
FutOspfSpfHoldtimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfSpfHoldtime (pMultiData->i4_SLongValue));
}

INT4
FutOspfSpfDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfSpfDelay (pMultiData->i4_SLongValue));
}

INT4
FutOspfRTStaggeringIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRTStaggeringInterval (pMultiData->u4_ULongValue));
}

INT4
FutOspfRTStaggeringStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRTStaggeringStatus (pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRestartSupport (pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRestartInterval (pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartStrictLsaCheckingSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRestartStrictLsaChecking (pMultiData->i4_SLongValue));
}

INT4
FutOspfHelperSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfHelperSupport (pMultiData->pOctetStrValue));
}

INT4
FutOspfExtTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfExtTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FutOspfHelperGraceTimeLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfHelperGraceTimeLimit (pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartAckStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRestartAckState (pMultiData->i4_SLongValue));
}

INT4
FutOspfGraceLsaRetransmitCountSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfGraceLsaRetransmitCount (pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartReasonSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRestartReason (pMultiData->i4_SLongValue));
}

INT4
FutospfRouterIdPermanenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutospfRouterIdPermanence (pMultiData->i4_SLongValue));
}

INT4
FutOspfBfdStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfBfdStatus (pMultiData->i4_SLongValue));
}

INT4
FutOspfBfdAllIfStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfBfdAllIfState (pMultiData->i4_SLongValue));
}

INT4
FutOspfRFC1583CompatibilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRFC1583Compatibility
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxAreasTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMaxAreas (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxLSAperAreaTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMaxLSAperArea
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxExtLSAsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMaxExtLSAs (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxSelfOrgLSAsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMaxSelfOrgLSAs
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMaxRoutes (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMaxLsaSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMaxLsaSize (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfTraceLevel (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfMinLsaIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfMinLsaInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfABRTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfABRType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfNssaAsbrDefRtTransTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfNssaAsbrDefRtTrans
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfDefaultPassiveInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfDefaultPassiveInterface
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfSpfHoldtimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfSpfHoldtime (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfSpfDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfSpfDelay (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRTStaggeringIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRTStaggeringInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FutOspfRTStaggeringStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRTStaggeringStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRestartSupport (pu4Error,
                                            pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRestartInterval (pu4Error,
                                             pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartStrictLsaCheckingTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRestartStrictLsaChecking
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfHelperSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfHelperSupport
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FutOspfExtTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfExtTraceLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfHelperGraceTimeLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfHelperGraceTimeLimit
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartAckStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRestartAckState
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfGraceLsaRetransmitCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfGraceLsaRetransmitCount
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRestartReasonTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRestartReason
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutospfRouterIdPermanenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutospfRouterIdPermanence
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfBfdStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfBfdStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfBfdAllIfStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfBfdAllIfState
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRFC1583CompatibilityDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRFC1583Compatibility
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMaxAreasDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMaxAreas (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMaxLSAperAreaDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMaxLSAperArea
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMaxExtLSAsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMaxExtLSAs
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMaxSelfOrgLSAsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMaxSelfOrgLSAs
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMaxRoutesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMaxRoutes (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMaxLsaSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMaxLsaSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfMinLsaIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfMinLsaInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfABRTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfABRType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfNssaAsbrDefRtTransDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfNssaAsbrDefRtTrans
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfDefaultPassiveInterfaceDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfDefaultPassiveInterface
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfSpfHoldtimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfSpfHoldtime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfSpfDelayDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfSpfDelay (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRTStaggeringIntervalDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRTStaggeringInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRTStaggeringStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRTStaggeringStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRestartSupportDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRestartSupport
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRestartIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRestartInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRestartStrictLsaCheckingDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRestartStrictLsaChecking
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfHelperSupportDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfHelperSupport
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfExtTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfExtTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfHelperGraceTimeLimitDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfHelperGraceTimeLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRestartAckStateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRestartAckState
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfGraceLsaRetransmitCountDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfGraceLsaRetransmitCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRestartReasonDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRestartReason
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutospfRouterIdPermanenceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutospfRouterIdPermanence
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfBfdStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfBfdStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfBfdAllIfStateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfBfdAllIfState
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfAreaTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfAreaTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfAreaTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfAreaIfCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaIfCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAreaNetCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaNetCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAreaRtrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaRtrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAreaNSSATranslatorRoleGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaNSSATranslatorRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAreaNSSATranslatorStateGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaNSSATranslatorState
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAreaNSSATranslatorStabilityIntervalGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaNSSATranslatorStabilityInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAreaNSSATranslatorEventsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaNSSATranslatorEvents
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAreaDfInfOriginateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaDfInfOriginate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAreaNSSATranslatorRoleSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFutOspfAreaNSSATranslatorRole
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAreaNSSATranslatorStabilityIntervalSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFutOspfAreaNSSATranslatorStabilityInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAreaDfInfOriginateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfAreaDfInfOriginate
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAreaNSSATranslatorRoleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAreaNSSATranslatorRole (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FutOspfAreaNSSATranslatorStabilityIntervalTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

}

INT4
FutOspfAreaDfInfOriginateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAreaDfInfOriginate (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FutOspfAreaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfAreaTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfHostTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfHostTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfHostTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfHostRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfHostTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfHostRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfHostRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfHostRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FutOspfHostRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfHostRouteIfIndex (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FutOspfHostTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfHostTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfIfTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfIfTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfIfTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfIfOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfOperState (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfPassiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfPassive (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfNbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfNbrCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfAdjCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAdjCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfHelloRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfHelloRcvd (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfHelloTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfHelloTxed (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfHelloDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfHelloDisd (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfDdpRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfDdpRcvd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfDdpTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfDdpTxed (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfDdpDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfDdpDisd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLrqRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLrqRcvd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLrqTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLrqTxed (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLrqDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLrqDisd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLsuRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLsuRcvd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLsuTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLsuTxed (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLsuDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLsuDisd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLakRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLakRcvd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLakTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLakTxed (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfLakDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfLakDisd (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfIfBfdStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfBfdState (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfPassiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfPassive (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FutOspfIfBfdStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfBfdState (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FutOspfIfPassiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfPassive (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FutOspfIfBfdStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfBfdState (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FutOspfIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfIfMD5AuthTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfIfMD5AuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfIfMD5AuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfIfMD5AuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfMD5AuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FutOspfIfMD5AuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfMD5AuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfMD5AuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfMD5AuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfMD5AuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfMD5AuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfMD5AuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FutOspfIfMD5AuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfMD5AuthKey (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FutOspfIfMD5AuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfMD5AuthKeyStartAccept (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStartGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfMD5AuthKeyStartGenerate (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfMD5AuthKeyStopGenerate (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfMD5AuthKeyStopAccept (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfMD5AuthKeyStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FutOspfIfMD5AuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfIfMD5AuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfVirtIfMD5AuthTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfVirtIfMD5AuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfVirtIfMD5AuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfVirtIfMD5AuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfMD5AuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfMD5AuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfMD5AuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfMD5AuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfMD5AuthKeyStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfMD5AuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfMD5AuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfMD5AuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfMD5AuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfMD5AuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfMD5AuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfMD5AuthKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfMD5AuthKey (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStartAcceptTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfMD5AuthKeyStartAccept (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStartGenerateTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfMD5AuthKeyStartGenerate (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStopGenerateTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfMD5AuthKeyStopGenerate (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStopAcceptTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfMD5AuthKeyStopAccept (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfMD5AuthKeyStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfMD5AuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfVirtIfMD5AuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfNbrTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfNbrTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfNbrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfNbrDBSummaryQLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfNbrDBSummaryQLen (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfNbrLSReqQLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfNbrLSReqQLen (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfNbrRestartHelperStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfNbrRestartHelperStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfNbrRestartHelperAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfNbrRestartHelperAge
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfNbrRestartHelperExitReasonGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfNbrRestartHelperExitReason
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfNbrBfdStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfNbrBfdState (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFutOspfRoutingTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfRoutingTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfRoutingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRouteAreaIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRouteAreaId (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfRouteCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRouteCost (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRouteType2CostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRouteType2Cost (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRouteInterfaceIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRouteInterfaceIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFutOspfSecIfTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfSecIfTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfSecIfTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfSecIfStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfSecIfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfSecIfStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfSecIfStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfSecIfStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FutOspfSecIfStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfSecIfStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FutOspfSecIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfSecIfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfAreaAggregateTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfAreaAggregateTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfAreaAggregateTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfAreaAggregateExternalTagGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAreaAggregateTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAreaAggregateExternalTag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAreaAggregateExternalTagSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFutOspfAreaAggregateExternalTag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAreaAggregateExternalTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAreaAggregateExternalTag (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FutOspfAreaAggregateTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfAreaAggregateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfAsExternalAggregationTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfAsExternalAggregationTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfAsExternalAggregationTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfAsExternalAggregationEffectGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAsExternalAggregationTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAsExternalAggregationEffect
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAsExternalAggregationTranslationGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAsExternalAggregationTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAsExternalAggregationTranslation
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAsExternalAggregationStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAsExternalAggregationTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAsExternalAggregationStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAsExternalAggregationEffectSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFutOspfAsExternalAggregationEffect
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAsExternalAggregationTranslationSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFutOspfAsExternalAggregationTranslation
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAsExternalAggregationStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFutOspfAsExternalAggregationStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfAsExternalAggregationEffectTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAsExternalAggregationEffect (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FutOspfAsExternalAggregationTranslationTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAsExternalAggregationTranslation (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[2].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
FutOspfAsExternalAggregationStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfAsExternalAggregationStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FutOspfAsExternalAggregationTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfAsExternalAggregationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfOpaqueOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfOpaqueOption (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfType11LsaCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfType11LsaCount (&(pMultiData->u4_ULongValue)));
}

INT4
FutOspfType11LsaCksumSumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfType11LsaCksumSum (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfAreaIDValidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfAreaIDValid (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfOpaqueOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfOpaqueOption (pMultiData->i4_SLongValue));
}

INT4
FutOspfAreaIDValidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfAreaIDValid (pMultiData->i4_SLongValue));
}

INT4
FutOspfOpaqueOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfOpaqueOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfAreaIDValidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfAreaIDValid (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfOpaqueOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfOpaqueOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfAreaIDValidDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfAreaIDValid
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfOpaqueInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfOpaqueInterfaceTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfOpaqueInterfaceTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfOpaqueType9LsaCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfOpaqueInterfaceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfOpaqueType9LsaCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfOpaqueType9LsaCksumSumGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfOpaqueInterfaceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfOpaqueType9LsaCksumSum
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFutOspfType9LsdbTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfType9LsdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfType9LsdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfType9LsdbSequenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType9LsdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType9LsdbSequence
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfType9LsdbAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType9LsdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType9LsdbAge (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfType9LsdbChecksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType9LsdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType9LsdbChecksum
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfType9LsdbAdvertisementGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType9LsdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType9LsdbAdvertisement
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFutOspfType11LsdbTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfType11LsdbTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfType11LsdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfType11LsdbSequenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType11LsdbSequence
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfType11LsdbAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType11LsdbAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfType11LsdbChecksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType11LsdbChecksum
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfType11LsdbAdvertisementGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfType11LsdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfType11LsdbAdvertisement
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFutOspfAppInfoDbTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfAppInfoDbTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfAppInfoDbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfAppInfoDbOpaqueTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbOpaqueType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAppInfoDbLsaTypesSupportedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbLsaTypesSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfAppInfoDbType9GenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbType9Gen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAppInfoDbType9RcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbType9Rcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAppInfoDbType10GenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbType10Gen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAppInfoDbType10RcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbType10Rcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAppInfoDbType11GenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbType11Gen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfAppInfoDbType11RcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfAppInfoDbType11Rcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfRRDStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRRDStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRRDSrcProtoMaskEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRRDSrcProtoMaskEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRRDSrcProtoMaskDisableGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRRDSrcProtoMaskDisable (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfRRDRouteMapEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfRRDRouteMapEnable (pMultiData->pOctetStrValue));
}

INT4
FutOspfRRDStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRRDStatus (pMultiData->i4_SLongValue));
}

INT4
FutOspfRRDSrcProtoMaskEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRRDSrcProtoMaskEnable (pMultiData->i4_SLongValue));
}

INT4
FutOspfRRDSrcProtoMaskDisableSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRRDSrcProtoMaskDisable (pMultiData->i4_SLongValue));
}

INT4
FutOspfRRDRouteMapEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfRRDRouteMapEnable (pMultiData->pOctetStrValue));
}

INT4
FutOspfRRDStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRRDStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRRDSrcProtoMaskEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRRDSrcProtoMaskEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRRDSrcProtoMaskDisableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRRDSrcProtoMaskDisable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfRRDRouteMapEnableTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfRRDRouteMapEnable
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FutOspfRRDStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRRDStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRRDSrcProtoMaskEnableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRRDSrcProtoMaskEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRRDSrcProtoMaskDisableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRRDSrcProtoMaskDisable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfRRDRouteMapEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRRDRouteMapEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfRRDRouteConfigTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfRRDRouteConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfRRDRouteConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfRRDRouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRRDRouteMetric (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRRDRouteMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRRDRouteMetricType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRRDRouteTagTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRRDRouteTagType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRRDRouteTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRRDRouteTag (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfRRDRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfRRDRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfRRDRouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfRRDRouteMetric (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfRRDRouteMetricType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteTagTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfRRDRouteTagType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfRRDRouteTag (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FutOspfRRDRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfRRDRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfRRDRouteMetric (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfRRDRouteMetricType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteTagTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfRRDRouteTagType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfRRDRouteTag (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FutOspfRRDRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfRRDRouteStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FutOspfRRDRouteConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfRRDRouteConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfVirtNbrTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfVirtNbrTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfVirtNbrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfVirtNbrRestartHelperStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtNbrRestartHelperStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtNbrRestartHelperAgeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtNbrRestartHelperAge
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FutOspfVirtNbrRestartHelperExitReasonGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtNbrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtNbrRestartHelperExitReason
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFutOspfDistInOutRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfDistInOutRouteMapTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfDistInOutRouteMapTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfDistInOutRouteMapValueGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfDistInOutRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfDistInOutRouteMapValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfDistInOutRouteMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfDistInOutRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfDistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfDistInOutRouteMapValueSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFutOspfDistInOutRouteMapValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfDistInOutRouteMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFutOspfDistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfDistInOutRouteMapValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfDistInOutRouteMapValue (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FutOspfDistInOutRouteMapRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfDistInOutRouteMapRowStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FutOspfDistInOutRouteMapTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfDistInOutRouteMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FutOspfPreferenceValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFutOspfPreferenceValue (&(pMultiData->i4_SLongValue)));
}

INT4
FutOspfPreferenceValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFutOspfPreferenceValue (pMultiData->i4_SLongValue));
}

INT4
FutOspfPreferenceValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FutOspfPreferenceValue
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FutOspfPreferenceValueDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfPreferenceValue
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfIfAuthTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfIfAuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfIfAuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfIfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfAuthKeyStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfAuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfAuthKeyStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FutOspfIfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfAuthKey (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfAuthKeyStartAccept (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStartGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfAuthKeyStartGenerate (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfAuthKeyStopGenerate (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfAuthKeyStopAccept (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FutOspfIfAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfIfAuthKeyStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FutOspfIfAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfIfAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfVirtIfAuthTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfVirtIfAuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfVirtIfAuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfVirtIfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfAuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfAuthKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfAuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfAuthKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfAuthKey (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfAuthKeyStartAccept (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStartGenerateTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfAuthKeyStartGenerate (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfAuthKeyStopGenerate (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfAuthKeyStopAccept (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
FutOspfVirtIfAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfVirtIfAuthKeyStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfVirtIfAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfIfCryptoAuthTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfIfCryptoAuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfIfCryptoAuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfIfCryptoAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfIfCryptoAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfIfCryptoAuthType (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfIfCryptoAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfIfCryptoAuthType (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FutOspfIfCryptoAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2OspfIfCryptoAuthType (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FutOspfIfCryptoAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfIfCryptoAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfVirtIfCryptoAuthTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfVirtIfCryptoAuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfVirtIfCryptoAuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfVirtIfCryptoAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfVirtIfCryptoAuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfVirtIfCryptoAuthType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfVirtIfCryptoAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfVirtIfCryptoAuthType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfCryptoAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2OspfVirtIfCryptoAuthType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FutOspfVirtIfCryptoAuthTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfVirtIfCryptoAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
