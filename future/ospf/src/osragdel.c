/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osragdel.c,v 1.19 2017/09/21 13:48:47 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *                   route aggregation feature
 *
 *******************************************************************/
#include "osinc.h"
/************************************************************************/
/*                                                                   */
/* Function     : RagDelAddrRange                        */
/*                                                                        */
/* Description     : Deletes the AS EXT External Range              */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*                                                                      */
/* Output          : None                                                  */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagDelAddrRangeInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange,
                      UINT1 u1Flag)
{
    tArea              *pArea = NULL;
    tIPADDR             ipAddr;
    tIPADDR             ipAddrMask;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: RagDelAddrRange\n");

    IP_ADDR_COPY (ipAddr, pAsExtRange->ipAddr);
    IP_ADDR_COPY (ipAddrMask, pAsExtRange->ipAddrMask);

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        GrExitGracefultRestart (pOspfCxt, OSPF_RESTART_TOP_CHG);
    }

    /* Only active ranges are used for aggregation */
    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        return;
    }
    /* reset pointer to the cached node */
    pOspfCxt->pLastExtAgg = NULL;
    if (pAsExtRange->rangeStatus == ACTIVE)
    {
        if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
        {
            RagDelBkBoneAddrRangeInCxt (pOspfCxt, pAsExtRange);
        }
        else
        {
            RagDelNssaAddrRangeInCxt (pOspfCxt, pAsExtRange);

        }
    }

    if (u1Flag == OSPF_TRUE)
    {

        TMO_SLL_Delete (&(pOspfCxt->asExtAddrRangeLstInRtr),
                        &(pAsExtRange->nextAsExtAddrRngInRtr));

        AS_EXT_AGG_FREE (pAsExtRange);
    }

    if ((UtilIpAddrComp (ipAddr, gNullIpAddr) == OSPF_EQUAL) &&
        (UtilIpAddrComp (ipAddrMask, gNullIpAddr) == OSPF_EQUAL) &&
        pArea->u4AreaType == NSSA_AREA)
    {
        OlsGenerateNssaDfLsa (pArea, DEFAULT_NSSA_LSA, &gDefaultDest);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "FUNC: RagDelAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                    */
/* Function        : RagDelBkBoneAddrRange                        */
/*                                                                        */
/* Description     : Deletes bkbone address range              */
/*                                      */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagDelBkBoneAddrRangeInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{
    tExtRoute          *pExtRoute = NULL;
    tArea              *pArea = NULL;
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    tAsExtAddrRange    *pLessSpecBkboneRng = NULL;
    UINT1               u1LesSpecNssa = OSPF_FALSE;
    UINT1               u1FalNssaRng = OSPF_FALSE;
    UINT1               u1LesSpecBkbone = OSPF_FALSE;
    UINT1               u1RngComp = OSPF_EQUAL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAsExtRng = NULL;
    INT4                i4RetValue;
    INT4                i4RetVal;
    VOID               *pTemp = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagDelBkBoneAddrRange\n");
    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " BkBone area not existing \n");
        return;
    }

    if (pAsExtRange->rangeStatus == ACTIVE)
    {
        inParams.pRoot = pArea->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddr));
        au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4ExtIndex;

        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;
        OSPF_TRC2 (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                   "Ip Addr = %x, Addr Mask = %x\n",
                   pAsExtRange->ipAddr, pAsExtRange->ipAddrMask);
        if (TrieRemove (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
        {
            return;
        }
        gOsRtr.u4AsExtRouteAggrCnt--;
    }

    /* Flush any Type 5 LSA originated for the range    */
    RagFlushBkBoneRangeInCxt (pOspfCxt, pAsExtRange, NULL);

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pAsExtRange->ipAddrMask));
    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    i4RetValue = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

    if (i4RetValue == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        if (RagCompAddrRng (pAsExtRange, pScanAsExtRng) == OSPF_GREATER)
        {
            /* Updates the "pNxtAsExtRng" with routes falling in
             * the deleted range */
            RagUpdtRtLstBtwRangesInCxt (pOspfCxt, pAsExtRange, pScanAsExtRng,
                                        OSPF_FALSE);
            RagProcessBkBoneRangeInCxt (pOspfCxt, pScanAsExtRng);
            pLessSpecBkboneRng = pScanAsExtRng;
            u1LesSpecBkbone = OSPF_TRUE;
        }
        else
        {
            /* Ext Rt falling in the range which is deleted are to be 
               re-originated
             */
            TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
            {
                pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
                pExtRoute->pAsExtAddrRange = NULL;
                if (pOspfCxt->bAsBdrRtr == OSPF_TRUE)
                {
                    RagOriginateNonAggLsaInCxt (pOspfCxt, pExtRoute, NULL);
                }
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
        {
            pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
            pExtRoute->pAsExtAddrRange = NULL;
            if (pOspfCxt->bAsBdrRtr == OSPF_TRUE)
            {
                RagOriginateNonAggLsaInCxt (pOspfCxt, pExtRoute, NULL);
            }
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        u1RngComp = NOT_EQUAL;

        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }

        inParams.pRoot = pArea->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddr));
        au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4ExtIndex;

        i4RetVal = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

        if (i4RetVal == TRIE_SUCCESS)
        {
            /* Check if there exists any NSSA range that is less/equal
               to the deleted bkbone range
             */
            pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

            u1RngComp = RagCompAddrRng (pAsExtRange, pScanAsExtRng);

            if ((u1RngComp == OSPF_GREATER) || (u1RngComp == OSPF_EQUAL))
            {
                u1LesSpecNssa = OSPF_TRUE;
            }
        }

        if (u1RngComp != OSPF_EQUAL)
        {
            /* Take care of flushing in NSSA Area too. */
            RagFlushNssaRange (pAsExtRange, NULL, pArea);
        }

        if (u1LesSpecNssa == OSPF_FALSE)
        {
            /* Scan Ext Rts falling in this bkbone range */
            TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
            {
                u1FalNssaRng = OSPF_FALSE;
                pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);

                inParams.pRoot = pArea->pAsExtAddrRangeTrie;
                inParams.pLeafNode = NULL;
                inParams.u1PrefixLen = 0;
                au4ExtIndex[0] =
                    CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum));
                au4ExtIndex[1] =
                    CRU_HTONL (OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4ExtIndex;
                outParams.Key.pKey = NULL;
                outParams.pAppSpecInfo = NULL;
                if ((TrieLookup (&inParams, &outParams,
                                 (VOID **) &pAsExtRng)) == TRIE_SUCCESS)

                {
                    pScanAsExtRng =
                        ((tAsExtAddrRange *) outParams.pAppSpecInfo);
                    if (RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) ==
                        OSPF_TRUE)
                    {
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "Existing FUNC: RagIsExtRtFallInRange\n");
                        u1FalNssaRng = OSPF_TRUE;
                    }
                }
                if ((u1FalNssaRng == OSPF_FALSE) &&
                    (u1LesSpecBkbone == OSPF_FALSE))
                {
                    RagOriginateNonAggLsaInCxt (pOspfCxt, pExtRoute, pArea);
                    pExtRoute->pAsExtAddrRange = NULL;
                }
            }

            if (u1LesSpecBkbone == OSPF_TRUE)
            {
                RagProcessNssaRange (pLessSpecBkboneRng, pArea);
            }
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagDelBkBoneAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : RagDelNssaAddrRange                        */
/*                                                                         */
/* Description     : Deletes NSSA address range                  */
/*                                      */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagDelNssaAddrRangeInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{
    tArea              *pArea = NULL;
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    UINT1               u1LesSpecNssaFnd = OSPF_FALSE;
    UINT1               u1BkboneFnd = OSPF_FALSE;
    UINT1               u1RngComp = OSPF_EQUAL;
    tExtRoute          *pExtRoute = NULL;
    UINT1               u1DelRng = OSPF_FALSE;
    UINT1               u1PrevRng = OSPF_TRUE;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    UINT1               u1Tos;
    UINT1               u1PrevBbRng = OSPF_FALSE;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    INT4                i4RetValue;
    VOID               *pAppSpecPtr = NULL;
    VOID               *pAsExtRng = NULL;
    VOID               *pTemp = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagDelNssaAddrRange\n");
    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " BkBone area not existing \n");
        return;
    }
    if (pArea->u4AreaType != NSSA_AREA)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " Area Type not NSSA  \n");
        return;
    }

    if (pAsExtRange->rangeStatus == ACTIVE)
    {
        inParams.pRoot = pArea->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddr));
        au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4ExtIndex;

        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;
        OSPF_TRC2 (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId,
                   "IP Addr = %x, Addr Mask = %x\n",
                   pAsExtRange->ipAddr, pAsExtRange->ipAddrMask);
        if (TrieRemove (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
        {
            return;
        }
        gOsRtr.u4AsExtRouteAggrCnt--;
    }

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pAsExtRange->ipAddrMask));
    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    i4RetValue = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

    if (i4RetValue == TRIE_SUCCESS)
    {
        pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        if (RagCompAddrRng (pAsExtRange, pScanAsExtRng) == OSPF_GREATER)
        {
            /* Updates the "pNxtAsExtRng" with routes falling in
             * the deleted range */
            RagUpdtRtLstBtwRangesInCxt (pOspfCxt, pAsExtRange, pScanAsExtRng,
                                        OSPF_FALSE);
            RagProcessNssaRange (pScanAsExtRng, pArea);
            u1LesSpecNssaFnd = OSPF_TRUE;
        }
    }

    /* If no less specific range exists in NSSA area, check 
       if there exists a bkbone range that is equal/less 
       specific to deleted NSSA range
     */
    if (u1LesSpecNssaFnd == OSPF_FALSE)
    {
        inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddr));
        au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4ExtIndex;

        i4RetValue = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

        if (i4RetValue == TRIE_SUCCESS)
        {
            pScanAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

            /* Check if range is less/equal to deleted range */

            u1RngComp = RagCompAddrRng (pAsExtRange, pScanAsExtRng);

            if ((u1RngComp == OSPF_GREATER) || (u1RngComp == OSPF_EQUAL))
            {
                for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                {
                    pScanAsExtRng->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
                    pScanAsExtRng->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
                }
                TMO_SLL_Scan (&(pScanAsExtRng->extRtLst),
                              pExtRtNode, tTMO_SLL_NODE *)
                {
                    pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
                    if (RagChkExtRtFallInPrvRngInCxt (pOspfCxt, pAsExtRange,
                                                      pExtRoute) == OSPF_TRUE)
                    {
                        /* Skip this Ext Rt */
                        continue;
                    }
                    /* Update cost/Path Type for bkbone 
                       range only for Ext Rt 
                       which are not falling in more 
                       specific NSSA range
                     */
                    else
                    {
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "FUNC: RagUpdatCostTypeForRange\n");

                        RagUpdatCostTypeForRange (pScanAsExtRng, pExtRoute);

                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "Exit FUNC: RagUpdatCostTypeForRange\n");
                    }
                }
                RagProcessNssaRange (pScanAsExtRng, pArea);
                u1BkboneFnd = OSPF_TRUE;
                RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pScanAsExtRng);
            }
        }
    }

    if ((u1LesSpecNssaFnd == OSPF_FALSE) && (u1BkboneFnd == OSPF_FALSE))
    {
        /* Flush any Type 7 LSA originated for the range */
        RagFlushNssaRange (pAsExtRange, NULL, pArea);

        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pOspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPF_EXT_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;

        if (TrieGetFirstNode (&inParams,
                              &pAppSpecPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return;
        }

        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagIsExtRtFallInRange\n");
            u1DelRng = RagIsExtRtFallInRange (pExtRoute, pAsExtRange);
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagIsExtRtFallInRange\n");
            u1PrevRng =
                RagChkExtRtFallInPrvRngInCxt (pOspfCxt, pAsExtRange, pExtRoute);

            u1PrevBbRng = RagChkExtRtFallInBbRngInCxt (pOspfCxt, pExtRoute);

            if ((u1DelRng == OSPF_TRUE) && (u1PrevRng == OSPF_FALSE) &&
                (u1PrevBbRng == OSPF_FALSE))
            {
                RagOriginateNonAggLsaInCxt (pOspfCxt, pExtRoute, pArea);
            }
            au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipNetNum));
            au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                    (pExtRoute->ipAddrMask));
            inParams.Key.pKey = (UINT1 *) au4Key;
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagDelNssaAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                 */
/* Function     : RagFlushBkBoneRange                     */
/*                                                                     */
/* Description     : Flushes the Type 5 LSA originated             */
/*          for bkbone range                    */
/*                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*          pExtRoute - Pointer to Ext Rt                */
/*                                      */
/* Output       : None                           */
/*                                                                      */
/* Returns      : VOID                             */
/*                                       */
/************************************************************************/
PUBLIC VOID
RagFlushBkBoneRangeInCxt (tOspfCxt * pOspfCxt,
                          tAsExtAddrRange * pAsExtRange, tExtRoute * pExtRoute)
{

    tArea              *pArea = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tIPADDR             lnkStId;
    tMetric             aTempMetric[OSPF_MAX_METRIC];
    UINT1               u1Tos = TOS_0;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagFlushBkBoneRange\n");

    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));

    /* If area does not exist   */
    if (pArea == NULL)
    {
        return;
    }

    /* If areaId is not 0.0.0.0 OR area Type not NORMAL */
    if ((UtilIpAddrComp (pArea->areaId, gNullIpAddr) != OSPF_EQUAL) ||
        (pArea->u4AreaType != NORMAL_AREA))
    {
        OSPF_TRC2 (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                   "Rng: %x  AreaId : %x \n",
                   OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr),
                   OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->areaId));

        return;
    }

    /* Case will occur when As Ext Range is deleted */
    if (pExtRoute == NULL)
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case RAG_DENY_ALL:
            case RAG_DO_NOT_ADVERTISE:
                OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                          " No flushing required  \n");
                break;

            case RAG_ADVERTISE:
            case RAG_ALLOW_ALL:

                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);

                pLsaInfo =
                    LsuDatabaseLookUp (AS_EXT_LSA, &(lnkStId),
                                       &(pAsExtRange->ipAddrMask),
                                       &(pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pOspfCxt->pBackbone);

                if ((pLsaInfo != NULL) &&
                    (pLsaInfo->u1TrnsltType5 == REDISTRIBUTED_TYPE5))
                {
                    AgdFlushOut (pLsaInfo);
                }

                break;

            default:
                OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                          " Bkbone Range - Invalid Range effect  \n");

                break;
        }
    }

    /*  Case will occur when Ext Rt is deleted from
       range it falls in
     */
    else
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case RAG_DENY_ALL:
            case RAG_DO_NOT_ADVERTISE:

                /* Ext Rt is removed from link list maintained in 
                   in range
                 */
                TMO_SLL_Delete (&(pAsExtRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                OSPF_TRC3 (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                           "Rng: %x  AreaId : %x Effect %d"
                           "- Returning Ext RT not NULL \n",
                           OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr),
                           OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->areaId),
                           pAsExtRange->u1RangeEffect);
                break;

            case RAG_ADVERTISE:
            case RAG_ALLOW_ALL:

                /* Ext Rt is removed from link list maintained in 
                   in range
                 */
                TMO_SLL_Delete (&(pAsExtRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                {
                    aTempMetric[u1Tos].u4Value
                        = pAsExtRange->aMetric[u1Tos].u4Value;
                    aTempMetric[u1Tos].u4MetricType =
                        pAsExtRange->aMetric[u1Tos].u4MetricType;
                    aTempMetric[u1Tos].bStatus =
                        pAsExtRange->aMetric[u1Tos].bStatus;
                }

                RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pAsExtRange);

                /* If currently no Ext Rt falls in Rng - Flush it */
                if (RagChkValidMetricInRng (pAsExtRange) == OSPF_FALSE)
                {
                    RagFlushBkBoneRangeInCxt (pOspfCxt, pAsExtRange, NULL);
                    return;
                }

                if (RagIsChangeInMetric (aTempMetric, pAsExtRange->aMetric)
                    == OSPF_FALSE)
                {
                    return;
                }

                /* Check if there is any change in cost/Type for 
                   each TOS maintained in range and those
                   advertised in Type 5 LSA previously originated
                   for the range
                 */

                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);

                pLsaInfo =
                    LsuDatabaseLookUp (AS_EXT_LSA, &(lnkStId),
                                       &(pAsExtRange->ipAddrMask),
                                       &(pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pOspfCxt->pBackbone);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                if (pLsaInfo->u1TrnsltType5 != REDISTRIBUTED_TYPE5)
                {
                    return;
                }

                if (RagCompRngAndLsa (pAsExtRange, pLsaInfo) == OSPF_TRUE)
                {

                    /* Re-Originate Type 5 LSA      */
                    OlsSignalLsaRegenInCxt (pOspfCxt, SIG_NEXT_INSTANCE,
                                            (UINT1 *) pLsaInfo->pLsaDesc);

                }
                break;

            default:
                OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                          " Bkbone Range - Invalid Range effect  \n");
                break;
        }

    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagFlushBkBoneRange\n");
    return;
}

/************************************************************************/
/*                                                                */
/* Function        : RagFlushNssaRange                     */
/*                                                                      */
/* Description     : Flushes the Type 7 LSA originated             */
/*          for NSSA range                    */
/*                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pExtRoute - Pointer to Ext Rt                */
/*          pArea - Pointer to NSSA                 */
/*                                      */
/* Output          : None                           */
/*                                      */
/* Returns         : VOID                             */
/*                                       */
/************************************************************************/
PUBLIC VOID
RagFlushNssaRange (tAsExtAddrRange * pAsExtRange,
                   tExtRoute * pExtRoute, tArea * pArea)
{

    /* Implies Ext Rt is deleted */
    if (pExtRoute != NULL)
    {

        RagFlshNssaRngExtDel (pAsExtRange, pArea, pExtRoute);
    }
    /* Implies As Ext Range is deleted */
    else
    {
        RagFlshNssaRngDel (pAsExtRange, pArea);
    }
}

/************************************************************************/
/*                                                                  */
/* Function        : RagFlshNssaRngExtDel                     */
/*                                                                         */
/* Description     : Flushes the Type 7 LSA originated             */
/*          for NSSA range when Ext Rt is             */
/*          deleted                         */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pExtRoute - Pointer to Ext Rt                */
/*          pArea - Pointer to NSSA                 */
/*                                      */
/* Output          : None                           */
/*                                      */
/* Returns         : VOID                             */
/*                                                          */
/************************************************************************/
PUBLIC VOID
RagFlshNssaRngExtDel (tAsExtAddrRange * pAsExtRange, tArea * pArea,
                      tExtRoute * pExtRoute)
{

    tLsaInfo           *pLsaInfo = NULL;
    tIPADDR             lnkStId;
    tInputParams        inParams;
    UINT4               au4ExtIndex[2];
    VOID               *pAppSpecPtr = NULL;
    tExtRoute          *pPrvRoute = NULL;
    tExtRoute          *pNxtRoute = NULL;
    tAsExtAddrRange    *pTmpRng = NULL;
    tMetric             aTempMetric[OSPF_MAX_METRIC];
    INT4                i4RetVal = 0;
    UINT1               u1Tos = 0;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        aTempMetric[u1Tos].u4Value = pAsExtRange->aMetric[u1Tos].u4Value;
        aTempMetric[u1Tos].u4MetricType =
            pAsExtRange->aMetric[u1Tos].u4MetricType;
        aTempMetric[u1Tos].bStatus = pAsExtRange->aMetric[u1Tos].bStatus;
    }

    /* Initialise the metric of the address range to infinity. */
    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pAsExtRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
        pAsExtRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
        pAsExtRange->aMetric[u1Tos].bStatus = OSPF_INVALID;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagFlshNssaRngExtDel\n");
    /* Bkbone range was used for this NSSA area */
    if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case RAG_ALLOW_ALL:
            case RAG_DO_NOT_ADVERTISE:
                RagUpdtBboneRngForNssa (pArea, pAsExtRange);

                /* If currently no Ext Rt falls in Rng - Flush it */
                if (RagChkValidMetricInRng (pAsExtRange) == OSPF_FALSE)
                {
                    RagFlushNssaRange (pAsExtRange, NULL, pArea);
                    return;
                }

                /* Check if there is any change in cost/Type for 
                   each TOS maintained in range and those
                   advertised in Type 5 LSA previously originated
                   for the range
                 */
                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);

                pLsaInfo =
                    LsuDatabaseLookUp (NSSA_LSA, &(lnkStId),
                                       &(pAsExtRange->ipAddrMask),
                                       &(pArea->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                if (RagCompRngAndLsa (pAsExtRange, pLsaInfo) == OSPF_TRUE)
                {
                    /* Re-Originate Type 7 LSA      */
                    OlsSignalLsaRegenInCxt (pArea->pOspfCxt, SIG_NEXT_INSTANCE,
                                            (UINT1 *) pLsaInfo->pLsaDesc);
                }
                break;

                /* No LSA for the range was originated in the 
                   area. So no flushing is required.
                   Also Ext Rt is not deleted from list as 
                   that will be done in "FlushBkBoneRng" 
                   handling.
                 */
            case RAG_ADVERTISE:
            case RAG_DENY_ALL:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " No flushing required  \n");
                break;

            default:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " Bkbone Range - Invalid Range effect  \n");
                break;
        }
    }
    else
    {
        switch (pAsExtRange->u1RangeEffect)
        {

            case RAG_DO_NOT_ADVERTISE:
                break;

            case RAG_ADVERTISE:
                /* External routes are grouped in the Trie DLL. */

                /* So if the route previous to the current one and the route
                 * next to current one fall in the passed address range,
                 * update the cost of the range and do not flush the range. */
                inParams.pRoot = pArea->pOspfCxt->pExtRtRoot;
                inParams.pLeafNode = NULL;
                inParams.u1PrefixLen = 0;
                au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                             (pExtRoute->ipNetNum));
                au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                             (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4ExtIndex;

                i4RetVal = TrieGetPrevNode (&inParams, NULL,
                                            &pAppSpecPtr,
                                            (VOID **) &(inParams.pLeafNode));
                pPrvRoute = (tExtRoute *) pAppSpecPtr;

                if ((i4RetVal == TRIE_SUCCESS) &&
                    (RagIsExtRtFallInRange (pPrvRoute, pAsExtRange)
                     == OSPF_TRUE))
                {
                    /* If previous route does not fall in any other address 
                     * range except the current address range, update the cost
                     * of the current address range. */
                    pTmpRng = RagFindAggAddrRangeInNssaForRt (pPrvRoute, pArea);

                    if ((pTmpRng != NULL) &&
                        (pPrvRoute->pAsExtAddrRange == NULL) &&
                        (RagCompAddrRng (pTmpRng, pAsExtRange) == OSPF_EQUAL))
                    {
                        RagUpdatCostTypeForRange (pAsExtRange, pPrvRoute);
                    }
                }

                inParams.pRoot = pArea->pOspfCxt->pExtRtRoot;
                inParams.pLeafNode = NULL;
                inParams.u1PrefixLen = 0;
                au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                             (pExtRoute->ipNetNum));
                au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                             (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4ExtIndex;

                i4RetVal = TrieGetNextNode (&inParams, NULL,
                                            &pAppSpecPtr,
                                            (VOID **) &(inParams.pLeafNode));
                pNxtRoute = (tExtRoute *) pAppSpecPtr;

                if ((i4RetVal == TRIE_SUCCESS) &&
                    (RagIsExtRtFallInRange (pNxtRoute, pAsExtRange)
                     == OSPF_TRUE))
                {
                    /* If next route does not fall in any other address 
                     * range except the current address range, update the cost
                     * of the current address range. */
                    pTmpRng = RagFindAggAddrRangeInNssaForRt (pNxtRoute, pArea);

                    if ((pTmpRng != NULL) &&
                        (pNxtRoute->pAsExtAddrRange == NULL) &&
                        (RagCompAddrRng (pTmpRng, pAsExtRange) == OSPF_EQUAL))
                    {
                        RagUpdatCostTypeForRange (pAsExtRange, pNxtRoute);
                    }
                }

                if (RagChkValidMetricInRng (pAsExtRange) == OSPF_FALSE)
                {
                    RagFlushNssaRange (pAsExtRange, NULL, pArea);
                    return;
                }

                if (RagIsChangeInMetric (aTempMetric, pAsExtRange->aMetric)
                    == OSPF_FALSE)
                {
                    return;
                }

                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);

                pLsaInfo =
                    LsuDatabaseLookUp (NSSA_LSA, &(lnkStId),
                                       &(pAsExtRange->ipAddrMask),
                                       &(pArea->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                if (RagCompRngAndLsa (pAsExtRange, pLsaInfo) == OSPF_TRUE)
                {
                    /* Re-Originate Type 7 LSA      */
                    OlsSignalLsaRegenInCxt (pArea->pOspfCxt, SIG_NEXT_INSTANCE,
                                            (UINT1 *) pLsaInfo->pLsaDesc);
                }
                break;

            case RAG_ALLOW_ALL:
            case RAG_DENY_ALL:
            default:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " Bkbone Range - Invalid Range effect  \n");

                break;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagFlshNssaRngExtDel\n");
    return;
}

/************************************************************************/
/*                                                              */
/* Function        : RagFlshNssaRngDel                     */
/*                                                                 */
/* Description     : Flushes the Type 7 LSA originated             */
/*          for NSSA range when Range is                 */
/*          deleted                         */
/*                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*          pArea - Pointer to NSSA                 */
/*                                      */
/* Output          : None                           */
/*                                      */
/* Returns         : VOID                             */
/*                                       */
/************************************************************************/
PUBLIC VOID
RagFlshNssaRngDel (tAsExtAddrRange * pAsExtRange, tArea * pArea)
{

    tLsaInfo           *pLsaInfo = NULL;
    tIPADDR             lnkStId;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagFlshNssaRngDel\n");
    if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case RAG_ALLOW_ALL:
            case RAG_DO_NOT_ADVERTISE:
                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);

                pLsaInfo =
                    LsuDatabaseLookUp (NSSA_LSA, &(lnkStId),
                                       &(pAsExtRange->ipAddrMask),
                                       &(pArea->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                AgdFlushOut (pLsaInfo);
                break;

            case RAG_DENY_ALL:
            case RAG_ADVERTISE:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " Bkbone Range - No Flushing Required  \n");
                break;

            default:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " Bkbone Range - Invalid Range effect  \n");
                break;
        }
    }
    else
    {
        switch (pAsExtRange->u1RangeEffect)
        {

            case RAG_DO_NOT_ADVERTISE:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " No flushing required  \n");
                break;

            case RAG_ADVERTISE:
                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);
                pLsaInfo =
                    LsuDatabaseLookUp (NSSA_LSA, &(lnkStId),
                                       &(pAsExtRange->ipAddrMask),
                                       &(pArea->pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                AgdFlushOut (pLsaInfo);
                break;

            case RAG_ALLOW_ALL:
            case RAG_DENY_ALL:
            default:

                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " Invalid effect \n");
                break;
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagFlshNssaRngDel\n");
    return;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osragdel.c                        */
/*-----------------------------------------------------------------------*/
