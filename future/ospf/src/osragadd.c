/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osragadd.c,v 1.14 2017/05/12 13:35:39 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *                   route aggregation feature
 *
 *******************************************************************/
#include "osinc.h"

/************************************************************************/
/*                                                                   */
/* Function        : RagAddNewAddrRange                        */
/*                                                                        */
/* Description     : This procedure handles addition             */
/*             of new AS Ext Address Range                 */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to new Range               */
/*                                                                      */
/* Output          : None                                              */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagAddNewAddrRangeInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{

    OSPF_TRC (OSPF_FN_ENTRY,
              pOspfCxt->u4OspfCxtId, "FUNC: RagAddNewAddrRange\n");

    if (RagAddRangeInRtLstInCxt (pOspfCxt, pAsExtRange) == OSPF_FALSE)
    {
        AS_EXT_AGG_FREE (pAsExtRange);
        return;
    }

    /* If range is not active it is not added to its 
       area lst.
     */
    if (pAsExtRange->rangeStatus != ACTIVE)
    {
        return;
    }

    /* If range belongs to Bkbone */
    if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        RagAddNewBkBoneAddrRangeInCxt (pOspfCxt, pAsExtRange);
    }
    else
    {
        RagAddNewNssaAddrRangeInCxt (pOspfCxt, pAsExtRange);
    }

    /* Reset the translation attribute mask. */
    pAsExtRange->u1AttrMask &= ~(RAG_TRANS_MASK);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddNewAddrRange\n");
}

/************************************************************************/
/*                                                                         */
/* Function     : RagAddNewBkBoneAddrRange                    */
/*                                                                         */
/* Description     : Adds the range in bkbone area in              */
/*            sorted order                         */
/*                                                                      */
/* Input          : pAsExtRange -Pointer to new Range               */
/*                                                                      */
/* Output          : None                                              */
/*                                                                      */
/* Returns         : VOID                                              */
/*                                                                      */
/************************************************************************/
PUBLIC INT4
RagAddNewBkBoneAddrRangeInCxt (tOspfCxt * pOspfCxt,
                               tAsExtAddrRange * pAsExtRange)
{
    tArea              *pArea = NULL;
    UINT1               u1Tos;
    tExtRoute          *pExtRoute = NULL;
    UINT1               u1LessSpecNssaExist = OSPF_FALSE;
    UINT1               u1LessSpecBkboneExist = OSPF_FALSE;
    UINT4               au4ExtIndex[2];
    tAsExtAddrRange    *pPrvAsExtRng = NULL;
    tAsExtAddrRange    *pLessSpecBkboneRng = NULL;
    tAsExtAddrRange    *pPrevNssaRange = NULL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    INT4                i4RetVal = TRIE_FAILURE;
    VOID               *pAsExtRng = NULL;
    UINT1               u1RngComp = OSPF_EQUAL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddNewBkBoneAddrRange\n");

    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));

    /* Area has to be backbone area. */
    if (pArea == NULL)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " Area type not back bone - Invalid addition \n");
        return OSPF_FAILURE;
    }

    inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pAsExtRange->ipAddrMask));
    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    i4RetVal = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

    if (RagInsertRngInAreaLst (pAsExtRange, pArea) == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        /* Validation of Less/More specific range and
         * LSA orgination/flush operations will be done at the end of GR */
        return OSPF_FAILURE;
    }

    if (i4RetVal == TRIE_SUCCESS)
    {
        pPrvAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        /* If Added address range is more specific than the previous one,
         * update the routes from less specific range to the recently added 
         * one. */
        if (RagCompAddrRng (pAsExtRange, pPrvAsExtRng) == OSPF_GREATER)
        {
            RagUpdtRtLstBtwRangesInCxt (pOspfCxt, pAsExtRange,
                                        pPrvAsExtRng, OSPF_TRUE);

            /* Now process  both the Bkbone ranges, taking care of changes
               in Rt cost/Type if any, which should be reflected in Type5 
               LSA
             */
            RagProcessBkBoneRangeInCxt (pOspfCxt, pPrvAsExtRng);
            RagProcessBkBoneRangeInCxt (pOspfCxt, pAsExtRange);

            /* Less specific backbone range is copied here. This range needs to
             * be processed for all the NSSA area as well. */
            u1LessSpecBkboneExist = OSPF_TRUE;
            pLessSpecBkboneRng = pPrvAsExtRng;
        }
        else
        {
            /* Since there is no less specific range, new range is updated with
               Ext Rts subsumed by it, by scanning Ext Rt Table
             */
            RagUpdtRtLstFromExtRtTableInCxt (pOspfCxt, pAsExtRange);

            /* Now process the newly added Bkbone range   */
            RagProcessBkBoneRangeInCxt (pOspfCxt, pAsExtRange);

        }
    }
    else
    {
        RagUpdtRtLstFromExtRtTableInCxt (pOspfCxt, pAsExtRange);

        RagProcessBkBoneRangeInCxt (pOspfCxt, pAsExtRange);
    }

    /* NSSA areas are scanned to check if addition of new bkbone requires
     * generation of Type 7 LSA in NSSA, based on Bkbone range */

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }

        inParams.pRoot = pArea->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddr));
        au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4ExtIndex;

        i4RetVal = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

        if (i4RetVal == TRIE_SUCCESS)
        {
            pPrvAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

            u1RngComp = RagCompAddrRng (pAsExtRange, pPrvAsExtRng);

            if (u1RngComp == OSPF_GREATER)
            {
                RagScanRtTableForAddrRangesInCxt (pOspfCxt, pAsExtRange,
                                                  pPrvAsExtRng, pArea,
                                                  NORMAL_AREA);

                RagProcessNssaRange (pPrvAsExtRng, pArea);
                RagProcessNssaRange (pAsExtRange, pArea);

                RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pAsExtRange);

                u1LessSpecNssaExist = OSPF_TRUE;
            }
        }
        else
        {
            for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                pAsExtRange->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
                pAsExtRange->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
            }

            /* Check if any extRoute lie in more specific NSSA range. If so 
             * don't consider it. Move to  next route.In the end check if
             * any aggregated LSA is to be generated based on newRange. IF YES
             * take action based on this new range */

            TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
            {
                pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
                /* If satisfied, implies Ext Rt 
                   falls in more specific NSSA
                   range
                 */
                OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                          "FUNC: RagIsExtRtFallInRange\n");

                pPrevNssaRange =
                    RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);

                if ((pPrevNssaRange != NULL)
                    && (RagIsExtRtFallInRange (pExtRoute, pPrevNssaRange)
                        == OSPF_TRUE))
                {
                    /* Skip this Ext Rt */
                    continue;
                }
                /* Update cost/Path Type for bkbone range only for Ext Rt 
                 * which are not falling in more specific NSSA range */
                else
                {
                    OSPF_TRC (OSPF_RAG_TRC,
                              pOspfCxt->u4OspfCxtId,
                              "FUNC: RagUpdatCostTypeForRange\n");

                    RagUpdatCostTypeForRange (pAsExtRange, pExtRoute);

                    OSPF_TRC (OSPF_RAG_TRC,
                              pOspfCxt->u4OspfCxtId,
                              "FUNC Exit : RagUpdatCostTypeForRange\n");
                }
            }                    /* TMO */
        }
        /* Implies there does not exist any
         * NSSA range which should be considered
         * before processing NSSA area using this 
         * bkbone range
         */
        if (u1LessSpecNssaExist == OSPF_FALSE)
        {
            RagProcessNssaRange (pAsExtRange, pArea);
            if (u1LessSpecBkboneExist == OSPF_TRUE)
            {
                RagProcessNssaRange (pLessSpecBkboneRng, pArea);
            }
        }
    }                            /* End of Area scan */

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddNewBkBoneAddrRange\n");
    return OSPF_SUCCESS;
}

/************************************************************************/
/*                                                                      */
/* Function        : RagAddNewNssaAddrRange                             */
/*                                                                      */
/* Description     : Adds the range in NSSA area in                     */
/*          sorted order                                                */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to new Range                  */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagAddNewNssaAddrRangeInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{
    tArea              *pArea = NULL;
    tAsExtAddrRange    *pPrvAsExtRng = NULL;
    UINT1               u1NssaLesMtch = OSPF_FALSE;
    UINT1               u1NssaMoreMtch = OSPF_FALSE;
    tExtRoute          *pExtRoute = NULL;
    UINT1               u1LesSpecBkbone = OSPF_FALSE;
    UINT1               u1RngComp = OSPF_EQUAL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    UINT1               u1Tos;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];
    INT4                i4RetVal = TRIE_FAILURE;
    VOID               *pAsExtRng = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddNewNssaAddrRange\n");
    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if ((pArea == NULL) || (pArea->u4AreaType != NSSA_AREA))
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " Area type not  NSSA - Invalid addition \n");
        return;
    }

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr));
    au4ExtIndex[1] =
        OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddrMask));

    inParams.Key.pKey = (UINT1 *) au4ExtIndex;

    i4RetVal = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

    RagInsertRngInAreaLst (pAsExtRange, pArea);

    if (pOspfCxt->u1OspfRestartState != OSPF_GR_NONE)
    {
        return;
    }

    if (i4RetVal == TRIE_SUCCESS)
    {
        pPrvAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

        u1RngComp = RagCompAddrRng (pAsExtRange, pPrvAsExtRng);

        if (u1RngComp == OSPF_GREATER)
        {
            RagScanRtTableForAddrRangesInCxt (pOspfCxt, pAsExtRange,
                                              pPrvAsExtRng, pArea, NSSA_AREA);

            RagProcessNssaRange (pPrvAsExtRng, pArea);

            RagProcessNssaRange (pAsExtRange, pArea);

            /* Take care if less spec has no routes falling 
               in it because of this processing
             */
            u1NssaLesMtch = OSPF_TRUE;
        }
        else if (u1RngComp == OSPF_LESS)
        {
            RagScanRtTableForAddrRangesInCxt (pOspfCxt, pPrvAsExtRng,
                                              pAsExtRange, pArea, NSSA_AREA);

            RagProcessNssaRange (pAsExtRange, pArea);
            u1NssaMoreMtch = OSPF_TRUE;
        }
    }

    if ((u1NssaMoreMtch == OSPF_FALSE) && (u1NssaLesMtch == OSPF_FALSE))
    {
        /* If range passed is NSSA, update cost/type for each TOS
           without attaching any Ext Rt
         */
        RagUpdtRtLstFromExtRtTableInCxt (pOspfCxt, pAsExtRange);
    }

    if (u1NssaLesMtch == OSPF_FALSE)
    {

        /* Scan through Bkbone ranges to find any less specific
           bkbone range
         */
        inParams.pRoot = pOspfCxt->pBackbone->pAsExtAddrRangeTrie;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddr));
        au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                     (pAsExtRange->ipAddrMask));
        inParams.Key.pKey = (UINT1 *) au4ExtIndex;

        i4RetVal = TrieLookup (&inParams, &outParams, (VOID **) &pAsExtRng);

        if (i4RetVal == TRIE_SUCCESS)
        {
            pPrvAsExtRng = ((tAsExtAddrRange *) outParams.pAppSpecInfo);

            /* Check if range is less specific than newly added 
               NSSA range */
            u1RngComp = RagCompAddrRng (pAsExtRange, pPrvAsExtRng);

            if (u1RngComp == OSPF_GREATER)
            {
                for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
                {
                    pPrvAsExtRng->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
                    pPrvAsExtRng->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
                }

                u1LesSpecBkbone = OSPF_TRUE;

                /* Update cost/type for Bkbone range by 
                   not taking into account
                   Ext Rt falling in newly added NSSA range
                 */

                TMO_SLL_Scan (&(pPrvAsExtRng->extRtLst),
                              pExtRtNode, tTMO_SLL_NODE *)
                {

                    pExtRoute = GET_EXTRT_FROM_ASEXTLST (pExtRtNode);
                    /* If satisfied, implies Ext Rt falls 
                       in more specific NSSA range
                     */
                    OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                              "FUNC: RagIsExtRtFallInRange\n");
                    if (RagChkExtRtFallInPrvRngInCxt (pOspfCxt,
                                                      pAsExtRange,
                                                      pExtRoute) == OSPF_TRUE)
                    {
                        /* Skip this Ext Rt */
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "Exit FUNC: RagIsExtRtFallInRange\n");
                        continue;
                    }
                    else
                    {
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "Exiting FUNC: RagIsExtRtFallInRange\n");

                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "FUNC: RagUpdatCostTypeForRange\n");

                        RagUpdatCostTypeForRange (pPrvAsExtRng, pExtRoute);

                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  "FUNC Exit : RagUpdatCostTypeForRange\n");
                    }
                }
            }                    /* less Spec Bkbone Range found */
        }

        if (u1LesSpecBkbone == OSPF_TRUE)
        {
            /* Process both the ranges    */
            RagProcessNssaRange (pPrvAsExtRng, pArea);

            RagProcessNssaRange (pAsExtRange, pArea);

            /* Restore backbone range cost/type   */
            RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pPrvAsExtRng);
        }
    }

    /* Handles the case when :
       No less specific NSSA range is present
       No more specific NSSA range is present
       No less specific Bkbone range is present
       No more specific Bkbone range is present
     */
    if ((u1LesSpecBkbone == OSPF_FALSE) &&
        (u1NssaLesMtch == OSPF_FALSE) && (u1NssaMoreMtch == OSPF_FALSE))
    {
        RagProcessNssaRange (pAsExtRange, pArea);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddNewNssaAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function        : RagInsertRouteInBkboneAggAddrRange                 */
/*                                                                      */
/* Description     : Inserts the Ext Rt in SLL maintained in the list   */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range                      */
/*                   pExtRoute -Pointer to Ext Rt                       */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : None                                               */
/************************************************************************/
PUBLIC VOID
RagInsertRouteInBkboneAggAddrRangeInCxt (tOspfCxt * pOspfCxt,
                                         tAsExtAddrRange * pAsExtRange,
                                         tExtRoute * pExtRoute)
{
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagInsertRouteInBkboneAggAddrRange\n");

    /* Check if Ext Rt already present in List */
    if ((TMO_SLL_Find (&(pAsExtRange->extRtLst),
                       &(pExtRoute->nextExtRouteInRange))) == RAG_NO_CHNG)
    {
        /* Add Ext Rt in List */
        TMO_SLL_Add (&(pAsExtRange->extRtLst),
                     &(pExtRoute->nextExtRouteInRange));

        pExtRoute->pAsExtAddrRange = pAsExtRange;

        /* Update Range for Cost/Path Type for each TOS */
    }
    else
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  "Ext Rt already present in Range List \n");
    }

    RagUpdtBboneRngFrmExtLstInCxt (pOspfCxt, pAsExtRange);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagInsertRouteInBkboneAggAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                     */
/* Function     : RagProcessBkBoneRange                 */
/*                                                                     */
/* Description     : Processes the Bkbone AS Ext Range             */
/*          Originates Type 5 LSA based on range.            */
/*                                      */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*                                      */
/* Output       : None                           */
/*                                      */
/*                                                                      */
/* Returns      : None                             */
/*                                      */
/*                                                          */
/************************************************************************/
PUBLIC VOID
RagProcessBkBoneRangeInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{

    tTMO_SLL_NODE      *pLsaNode = NULL;
    tTMO_SLL_NODE      *pTmpLsaNode = NULL;
    tLsaInfo           *pType5LsaInfo = NULL;
    tLsaInfo           *pEquLsaInfo = NULL;
    tOsDbNode          *pOsDbNode = NULL;
    tOsDbNode          *pTmpOsDbNode = NULL;
    UINT4               u4HashKey = 0;
    UINT1               u1LsaFallInRng;
    UINT1               u1Change = OSPF_FALSE;
    UINT1               u1EquLsa = OSPF_FALSE;
    tIPADDR             lnkStId;
    tExtLsdbOverflowTrapInfo trapElsdbOverflow;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagProcessBkBoneRange\n");

    /* If currently no Ext Rt falls in Rng - Flush it */
    if (RagChkValidMetricInRng (pAsExtRange) == OSPF_FALSE)
    {
        RagFlushBkBoneRangeInCxt (pOspfCxt, pAsExtRange, NULL);
        return;
    }
    if ((pAsExtRange->u1RangeEffect == RAG_DO_NOT_ADVERTISE) ||
        (pAsExtRange->u1RangeEffect == RAG_DENY_ALL))
    {
        u1EquLsa = OSPF_TRUE;
    }

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_DYN_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                                  pOsDbNode, pTmpOsDbNode, tOsDbNode *)
        {
            TMO_DYN_SLL_Scan (&pOsDbNode->lsaLst, pLsaNode, pTmpLsaNode,
                              tTMO_SLL_NODE *)
            {
                pType5LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                /* Not Self - originated  : Continue */
                if (UtilIpAddrComp
                    (pType5LsaInfo->lsaId.advRtrId,
                     pOspfCxt->rtrId) != OSPF_EQUAL)
                {
                    continue;
                }

                /* Not originated as a result of route import - Continue    */
                if (pType5LsaInfo->u1TrnsltType5 != REDISTRIBUTED_TYPE5)
                {
                    continue;
                }

                if (RagChkLsaFallInPrvRng (pAsExtRange, pType5LsaInfo)
                    == OSPF_TRUE)
                {
                    continue;
                }

                /* Chk if LSA falls in Range    */
                u1LsaFallInRng =
                    RagChkLsaFallInRng (pAsExtRange, pType5LsaInfo);

                switch (pAsExtRange->u1RangeEffect)
                {

                    case RAG_ADVERTISE:
                    case RAG_ALLOW_ALL:
                        if (u1LsaFallInRng == OSPF_GREATER)
                        {
                            AgdFlushOut (pType5LsaInfo);
                        }

                        if (u1LsaFallInRng == OSPF_EQUAL)
                        {

                            u1EquLsa = OSPF_TRUE;
                            pEquLsaInfo = pType5LsaInfo;
                            u1Change =
                                RagCompRngAndLsa (pAsExtRange, pType5LsaInfo);
                        }
                        break;

                    case RAG_DO_NOT_ADVERTISE:
                    case RAG_DENY_ALL:
                        if ((u1LsaFallInRng == OSPF_GREATER) ||
                            (u1LsaFallInRng == OSPF_EQUAL))
                        {
                            AgdFlushOut (pType5LsaInfo);
                        }
                        break;

                    default:
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  " Invalid Range effect \n");

                        return;

                }
            }                    /* Scan Type 5 LSAs */
        }
    }

    /* If LSA matching the range is present.
       Plus there is some update in range cost/Path Type 
     */
    if ((u1EquLsa == OSPF_TRUE) && (u1Change == OSPF_TRUE))
    {
        /* Re-Originate Type 5 LSA      */
        /* LSA should be re-originated using
         * tAsExtAddrRange.
         * Case : LSA equal to the range is
         * present and it is non-aggregated, then
         * pAssoc should point to tAsExtAddrRange
         * from this time onwards. */
        pEquLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pAsExtRange;
        pEquLsaInfo->pLsaDesc->u1InternalLsaType = COND_AS_EXT_LSA;
        OlsSignalLsaRegenInCxt (pOspfCxt, SIG_NEXT_INSTANCE,
                                (UINT1 *) pEquLsaInfo->pLsaDesc);
    }

    /* If currently there is no LSA originated for
       the range
     */

    else if (u1EquLsa == OSPF_FALSE)
    {
        /* Originate new Type 5 LSA */
        if (pOspfCxt->bAsBdrRtr == OSPF_TRUE)
        {
            if (RagGenExtLsaInCxt (pOspfCxt, COND_AS_EXT_LSA,
                                   (UINT1 *) pAsExtRange) == OSPF_TRUE)
            {
                UtilIpAddrMaskCopy (lnkStId,
                                    pAsExtRange->ipAddr,
                                    pAsExtRange->ipAddrMask);
                OlsModifyLsId (pOspfCxt->pBackbone, COND_AS_EXT_LSA, &(lnkStId),
                               (UINT1 *) pAsExtRange);

                OlsGenerateLsa (pOspfCxt->pBackbone, COND_AS_EXT_LSA,
                                &(lnkStId), (UINT1 *) pAsExtRange);

                if (IS_TRAP_ENABLED_IN_CXT (LSDB_APPROACHING_OVERFLOW_TRAP,
                                            pOspfCxt) &&
                    pOspfCxt->bLsdbApproachingOvflTrapGen == OSPF_FALSE &&
                    GET_NON_DEF_ASE_LSA_COUNT_IN_CXT (pOspfCxt) >=
                    OVERFLOW_APPROACHING_LIMIT_IN_CXT (pOspfCxt) &&
                    (pOspfCxt->i4ExtLsdbLimit != NO_LIMIT))
                {
                    trapElsdbOverflow.i4ExtLsdbLimit = pOspfCxt->i4ExtLsdbLimit;
                    IP_ADDR_COPY (trapElsdbOverflow.rtrId, pOspfCxt->rtrId);
                    SnmpifSendTrapInCxt (pOspfCxt,
                                         LSDB_APPROACHING_OVERFLOW_TRAP,
                                         &trapElsdbOverflow);
                    pOspfCxt->bLsdbApproachingOvflTrapGen = OSPF_TRUE;
                }
            }
        }

    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagProcessBkBoneRange\n");
    return;

}

/************************************************************************/
/*                                                               */
/* Function        : RagProcessNssaRange                     */
/*                                                                         */
/* Description     : Processes the NSSA AS Ext Range             */
/*          Originates Type 7 LSA based on range             */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*            pArea - Pointer to Area                */
/*                                      */
/* Output          : None                           */
/*                                                                      */
/* Returns         : Void                             */
/*                                                          */
/************************************************************************/
PUBLIC VOID
RagProcessNssaRange (tAsExtAddrRange * pAsExtRange, tArea * pArea)
{

    tTMO_SLL_NODE      *pLsaNode = NULL;
    tTMO_SLL_NODE      *pTmpLsaNode = NULL;
    tLsaInfo           *pType7LsaInfo = NULL;
    tLsaInfo           *pEquLsaInfo = NULL;
    UINT1               u1LsaFallInRng;
    UINT1               u1Change = OSPF_FALSE;
    UINT1               u1EquLsa = OSPF_FALSE;
    tIPADDR             lnkStId;
    UINT1               u1LsaFallInBbRng = OSPF_FALSE;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagProcessNssaRange\n");

    /* If no Ext Rt Falls in Rng - Flush it */
    if (RagChkValidMetricInRng (pAsExtRange) == OSPF_FALSE)
    {
        RagFlushNssaRange (pAsExtRange, NULL, pArea);
        return;
    }

    /* If range belongs to bkbone */
    if (UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        RagProcBboneRngForNssa (pAsExtRange, pArea);
        return;
    }

    if (UtilIpAddrComp (pAsExtRange->areaId, pArea->areaId) != OSPF_EQUAL)
    {
        return;
    }
    if (pAsExtRange->u1RangeEffect == RAG_DO_NOT_ADVERTISE)
    {
        u1EquLsa = OSPF_TRUE;
    }

    TMO_DYN_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, pTmpLsaNode,
                      tTMO_SLL_NODE *)
    {

        pType7LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

        /* If not self originated - continue */
        if (UtilIpAddrComp (pType7LsaInfo->lsaId.advRtrId,
                            pArea->pOspfCxt->rtrId) != OSPF_EQUAL)
        {
            continue;
        }

        /* If LSA falls in previous active range - continue */
        if (RagChkLsaFallInPrvRng (pAsExtRange, pType7LsaInfo) == OSPF_TRUE)
        {
            continue;
        }

        u1LsaFallInBbRng =
            RagChkLsaFallInBackboneRng (pArea->pOspfCxt,
                                        pAsExtRange, pType7LsaInfo);

        u1LsaFallInRng = RagChkLsaFallInRng (pAsExtRange, pType7LsaInfo);

        switch (pAsExtRange->u1RangeEffect)
        {

            case RAG_DO_NOT_ADVERTISE:
                if ((UtilIpAddrComp (pAsExtRange->ipAddrMask, gNullIpAddr)
                     == OSPF_EQUAL) &&
                    (UtilIpAddrComp (pAsExtRange->ipAddr, gNullIpAddr)
                     == OSPF_EQUAL) &&
                    (UtilIpAddrComp
                     (pType7LsaInfo->lsaId.linkStateId,
                      gNullIpAddr) == OSPF_EQUAL))
                {
                    continue;
                }

                else if (((u1LsaFallInRng == OSPF_GREATER) ||
                          (u1LsaFallInRng == OSPF_EQUAL)) &&
                         (u1LsaFallInBbRng == OSPF_FALSE))
                {
                    AgdFlushOut (pType7LsaInfo);
                }
                break;

            case RAG_ADVERTISE:
                if ((u1LsaFallInRng == OSPF_GREATER) &&
                    (u1LsaFallInBbRng == OSPF_FALSE))
                {
                    AgdFlushOut (pType7LsaInfo);
                }

                if (u1LsaFallInRng == OSPF_EQUAL)
                {
                    u1EquLsa = OSPF_TRUE;
                    pEquLsaInfo = pType7LsaInfo;
                    u1Change = RagCompRngAndLsa (pAsExtRange, pType7LsaInfo);
                }
                break;

            default:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " NSSA Range - Invalid Range effect  \n");
                return;
        }

    }                            /* LSA Scan */

    if ((u1EquLsa == OSPF_TRUE) && (u1Change == OSPF_TRUE))
    {

        pEquLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pAsExtRange;
        pEquLsaInfo->pLsaDesc->u1InternalLsaType = COND_NSSA_LSA;

        /* Re-Originate Type 7 LSA      */
        OlsSignalLsaRegenInCxt (pArea->pOspfCxt, SIG_NEXT_INSTANCE,
                                (UINT1 *) pEquLsaInfo->pLsaDesc);

    }
    else if (u1EquLsa == OSPF_FALSE)
    {
        UtilIpAddrMaskCopy (lnkStId,
                            pAsExtRange->ipAddr, pAsExtRange->ipAddrMask);

        /* Originate new Type 7 LSA */
        OlsModifyLsId (pArea, COND_NSSA_LSA, &(lnkStId), (UINT1 *) pAsExtRange);

        OlsGenerateLsa (pArea, COND_NSSA_LSA, &(lnkStId),
                        (UINT1 *) pAsExtRange);
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagProcessNssaRange\n");
    return;
}

/************************************************************************/
/*                                                               */
/* Function     : RagProcBboneRngForNssa                 */
/*                                                                        */
/* Description     : Processes the Bkbone AS Ext Range             */
/*          for NSSA area                     */
/*          Originates Type 7 LSA based on range             */
/*                                      */
/* Input        : pAsExtRange -Pointer to Range               */
/*          pArea - Pointer to Area                */
/*                                      */
/* Output       : None                               */
/*                                                                      */
/* Returns      : Void                             */
/*                                        */
/************************************************************************/
PUBLIC VOID
RagProcBboneRngForNssa (tAsExtAddrRange * pAsExtRange, tArea * pArea)
{

    tTMO_SLL_NODE      *pLsaNode = NULL;
    tTMO_SLL_NODE      *pTmpLsaNode = NULL;
    tLsaInfo           *pType7LsaInfo = NULL;
    tLsaInfo           *pEquLsaInfo = NULL;
    UINT1               u1LsaFallInRng;
    UINT1               u1LsaFallInNssaRng = OSPF_EQUAL;
    UINT1               u1Change = OSPF_FALSE;
    UINT1               u1EquLsa = OSPF_FALSE;
    tIPADDR             lnkStId;

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagProcBboneRngForNssa\n");

    if ((pAsExtRange->u1RangeEffect == RAG_DENY_ALL) ||
        (pAsExtRange->u1RangeEffect == RAG_ADVERTISE))
    {
        u1EquLsa = OSPF_TRUE;
    }
    /* Scan Type 7 LSA Lst */
    TMO_DYN_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, pTmpLsaNode,
                      tTMO_SLL_NODE *)
    {
        pType7LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

        /* If not self originated  - continue */
        if (UtilIpAddrComp
            (pType7LsaInfo->lsaId.advRtrId,
             pArea->pOspfCxt->rtrId) != OSPF_EQUAL)
        {
            continue;
        }

        /* If LSA falls in previous active range - continue */
        if (RagChkLsaFallInPrvRng (pAsExtRange, pType7LsaInfo) == OSPF_TRUE)
        {

            continue;
        }

        u1LsaFallInNssaRng = RagChkLsaFallInNssaRng (pArea, pAsExtRange,
                                                     pType7LsaInfo);

        u1LsaFallInRng = RagChkLsaFallInRng (pAsExtRange, pType7LsaInfo);

        switch (pAsExtRange->u1RangeEffect)
        {

            case RAG_ADVERTISE:
            case RAG_DENY_ALL:

                /* If LSA falls/equal the range,
                   flush it
                 */
                if (((u1LsaFallInRng == OSPF_GREATER) ||
                     (u1LsaFallInRng == OSPF_EQUAL)) &&
                    (u1LsaFallInNssaRng == OSPF_FALSE))
                {
                    AgdFlushOut (pType7LsaInfo);
                }
                u1EquLsa = OSPF_TRUE;
                break;

            case RAG_DO_NOT_ADVERTISE:
            case RAG_ALLOW_ALL:

                if ((u1LsaFallInRng == OSPF_GREATER) &&
                    (u1LsaFallInNssaRng == OSPF_FALSE))
                {
                    AgdFlushOut (pType7LsaInfo);
                }

                if (u1LsaFallInRng == OSPF_EQUAL)
                {
                    u1EquLsa = OSPF_TRUE;
                    pEquLsaInfo = pType7LsaInfo;
                    u1Change = RagCompRngAndLsa (pAsExtRange, pType7LsaInfo);
                }
                break;

            default:
                OSPF_TRC (OSPF_RAG_TRC, pArea->pOspfCxt->u4OspfCxtId,
                          " Invalid Range effect \n");
                return;
        }
    }                            /* LSA Scan */

    if ((u1EquLsa == OSPF_TRUE) && (u1Change == OSPF_TRUE))
    {
        /* Re-Originate Type 7 LSA      */

        pEquLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pAsExtRange;
        OlsSignalLsaRegenInCxt (pArea->pOspfCxt, SIG_NEXT_INSTANCE,
                                (UINT1 *) pEquLsaInfo->pLsaDesc);

    }
    else if (u1EquLsa == OSPF_FALSE)
    {
        UtilIpAddrMaskCopy (lnkStId,
                            pAsExtRange->ipAddr, pAsExtRange->ipAddrMask);

        /* Originate new Type 7 LSA */
        OlsModifyLsId (pArea, COND_NSSA_LSA, &(lnkStId), (UINT1 *) pAsExtRange);

        OlsGenerateLsa (pArea, COND_NSSA_LSA, &(lnkStId),
                        (UINT1 *) pAsExtRange);
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagProcBboneRngForNssa\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osragadd.c                        */
/*-----------------------------------------------------------------------*/
