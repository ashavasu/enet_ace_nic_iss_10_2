/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osmbsm.c,v 1.12 2013/03/28 11:50:27 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : osmbsm.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Ospf module                                    */
/*    MODULE NAME           : Ospf module Card Updation                      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 14 Jul 2005                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            the Ospf module.                               */
/*---------------------------------------------------------------------------*/

#include "osinc.h"
#include "ipnp.h"

/*****************************************************************************/
/* Function Name      : OspfMbsmUpdateCardInsertion                          */
/*                                                                           */
/* Description        : This function programs the HW with the Ospf          */
/*                      software configuration.                              */
/*                                                                           */
/*                      This function will be called from the Ospf module    */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           : tMbsmSlotInfo - Structure containing the SlotId      */
/*                                      info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
OspfMbsmUpdateCardInsertion (tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4CxtId;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4MaxCxt = 0;

    if (OSIX_FALSE != MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        return MBSM_SUCCESS;
    }

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    for (u4CxtId = OSPF_DEFAULT_CXT_ID; u4CxtId < u4MaxCxt; u4CxtId++)
    {
        if (gOsRtr.apOspfCxt[u4CxtId] != NULL)
        {
            pOspfCxt = gOsRtr.apOspfCxt[u4CxtId];
            if (pOspfCxt->admnStat == OSPF_ENABLED)
            {
                if (OspfFsNpMbsmOspfInit (pSlotInfo) != FNP_SUCCESS)
                {
                    return MBSM_FAILURE;
                }
                return MBSM_SUCCESS;
            }
        }
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : OspfMbsmUpdateCardStatus                             */
/*                                                                           */
/* Description        : This function constructs the Ospf Q Mesg and calls   */
/*                      the Ospf packet arrival event to process this Mesg.  */
/*                                                                           */
/*                      This function will be called from the IP module      */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
OspfMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tOspfQMsg          *pOspfQMsg = NULL;

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        return (MBSM_FAILURE);
    }
    pOspfQMsg->u4MsgType = (UINT4) i4Event;
    MEMCPY (&(pOspfQMsg->unOspfMsgIfParam.MbsmCardUpdate.mbsmProtoMsg),
            pProtoMsg, sizeof (tMbsmProtoMsg));

    if (OsixSendToQ ((UINT4) 0, (const UINT1 *) "OSPQ",
                     (tCRU_BUF_CHAIN_HEADER *) pOspfQMsg, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        QMSG_FREE (pOspfQMsg);
        return (MBSM_FAILURE);
    }
    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Send Evt Failed\n");
    }

    return MBSM_SUCCESS;
}
