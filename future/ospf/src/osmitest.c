/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: osmitest.c,v 1.22 2017/03/10 12:45:35 siva Exp $
*
* Description: Protocol Low Level MI Test Routines 
*********************************************************************/
#include  "osinc.h"
#include "ospfcli.h"
#include "stdoslow.h"
#include "ospftlow.h"
#include "fsostlow.h"

/* LOW LEVEL  Routines for Table : FsMIStdOspfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfTable (INT4 i4FsMIStdOspfContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIStdOspfContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIStdOspfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfRouterId
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfRouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfRouterId (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfContextId,
                              UINT4 u4TestValFsMIStdOspfRouterId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2OspfRouterId (pu4ErrorCode, u4TestValFsMIStdOspfRouterId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfAdminStat
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfAdminStat (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIStdOspfContextId,
                               INT4 i4TestValFsMIStdOspfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2OspfAdminStat (pu4ErrorCode, i4TestValFsMIStdOspfAdminStat);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfASBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfASBdrRtrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfASBdrRtrStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfContextId,
                                    INT4 i4TestValFsMIStdOspfASBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfASBdrRtrStatus (pu4ErrorCode,
                                            i4TestValFsMIStdOspfASBdrRtrStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfTOSSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfTOSSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfTOSSupport (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                INT4 i4TestValFsMIStdOspfTOSSupport)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfTOSSupport (pu4ErrorCode,
                                        i4TestValFsMIStdOspfTOSSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfExtLsdbLimit
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfExtLsdbLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfExtLsdbLimit (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfContextId,
                                  INT4 i4TestValFsMIStdOspfExtLsdbLimit)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfExtLsdbLimit (pu4ErrorCode,
                                          i4TestValFsMIStdOspfExtLsdbLimit);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfMulticastExtensions
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfMulticastExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfMulticastExtensions (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfContextId,
                                         INT4
                                         i4TestValFsMIStdOspfMulticastExtensions)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfMulticastExtensions (pu4ErrorCode,
                                                 i4TestValFsMIStdOspfMulticastExtensions);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfExitOverflowInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfExitOverflowInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfExitOverflowInterval (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfContextId,
                                          INT4
                                          i4TestValFsMIStdOspfExitOverflowInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfExitOverflowInterval (pu4ErrorCode,
                                                  i4TestValFsMIStdOspfExitOverflowInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfDemandExtensions
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfDemandExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfDemandExtensions (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfContextId,
                                      INT4 i4TestValFsMIStdOspfDemandExtensions)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfDemandExtensions (pu4ErrorCode,
                                              i4TestValFsMIStdOspfDemandExtensions);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDMetricValue
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId

                The Object 
                testValFsMIOspfRRDMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDMetricValue (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIStdOspfContextId,
                                 INT4 i4FsMIOspfRRDProtocolId,
                                 INT4 i4TestValFsMIOspfRRDMetricValue)
{
    tOspfCxt           *pOspfCxt = NULL;
    INT1                i1Return = SNMP_SUCCESS;

    pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId);
    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pOspfCxt->redistrAdmnStatus != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_REDIS_DISABLED);
        return SNMP_FAILURE;
    }
    if ((i4FsMIOspfRRDProtocolId <= 0) || (i4FsMIOspfRRDProtocolId >
                                           MAX_PROTO_REDISTRUTE_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMIOspfRRDMetricValue < MIN_METRIC_VALUE)
        || (i4TestValFsMIOspfRRDMetricValue > MAX_MERTIC_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDMetricType
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId

                The Object 
                testValFsMIOspfRRDMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDMetricType (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                INT4 i4FsMIOspfRRDProtocolId,
                                INT4 i4TestValFsMIOspfRRDMetricType)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    INT1                i1Return = SNMP_SUCCESS;

    pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId);
    if (pOspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->redistrAdmnStatus != OSPF_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_REDIS_DISABLED);
        return SNMP_FAILURE;
    }

    if ((i4FsMIOspfRRDProtocolId <= 0) || (i4FsMIOspfRRDProtocolId >
                                           MAX_PROTO_REDISTRUTE_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIOspfRRDMetricType != TYPE_1_METRIC)
        && (i4TestValFsMIOspfRRDMetricType != TYPE_2_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIStdOspfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIStdOspfContextId,
                            INT4 i4TestValFsMIStdOspfStatus)
{
    tOspfCxt           *pOspfCxt = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4TestValFsMIStdOspfStatus == DESTROY)
    {
        if (i4FsMIStdOspfContextId < 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
        return i1RetVal;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return i1RetVal;
    }

    pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId);

    switch (i4TestValFsMIStdOspfStatus)
    {
        case CREATE_AND_WAIT:
        {
            if (pOspfCxt != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            break;
        }
        case NOT_IN_SERVICE:
        case ACTIVE:
        case NOT_READY:
        {
            if (pOspfCxt == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            break;
        }
        case CREATE_AND_GO:
        {
            if (pOspfCxt != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }
            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfAreaTable
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfAreaTable (INT4 i4FsMIStdOspfAreaContextId,
                                              UINT4 u4FsMIStdOspfAreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIStdOspfAreaContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIStdOspfAreaContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfAreaContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfAreaContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfAreaTable (u4FsMIStdOspfAreaId);

    UtilOspfResetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfImportAsExtern
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                testValFsMIStdOspfImportAsExtern
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfImportAsExtern (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfAreaContextId,
                                    UINT4 u4FsMIStdOspfAreaId,
                                    INT4 i4TestValFsMIStdOspfImportAsExtern)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfImportAsExtern (pu4ErrorCode,
                                            u4FsMIStdOspfAreaId,
                                            i4TestValFsMIStdOspfImportAsExtern);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfAreaSummary
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                testValFsMIStdOspfAreaSummary
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfAreaSummary (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIStdOspfAreaContextId,
                                 UINT4 u4FsMIStdOspfAreaId,
                                 INT4 i4TestValFsMIStdOspfAreaSummary)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfAreaSummary (pu4ErrorCode,
                                         u4FsMIStdOspfAreaId,
                                         i4TestValFsMIStdOspfAreaSummary);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfAreaStatus
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                testValFsMIStdOspfAreaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfAreaStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfAreaContextId,
                                UINT4 u4FsMIStdOspfAreaId,
                                INT4 i4TestValFsMIStdOspfAreaStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfAreaStatus (pu4ErrorCode,
                                        u4FsMIStdOspfAreaId,
                                        i4TestValFsMIStdOspfAreaStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfStubAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfStubAreaTable
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfStubAreaTable (INT4
                                                  i4FsMIStdOspfStubContextId,
                                                  UINT4 u4FsMIStdOspfStubAreaId,
                                                  INT4 i4FsMIStdOspfStubTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfStubContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfStubContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfStubContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfStubContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfStubAreaTable
        (u4FsMIStdOspfStubAreaId, i4FsMIStdOspfStubTOS);

    UtilOspfResetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfStubMetric
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                testValFsMIStdOspfStubMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfStubMetric (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfStubContextId,
                                UINT4 u4FsMIStdOspfStubAreaId,
                                INT4 i4FsMIStdOspfStubTOS,
                                INT4 i4TestValFsMIStdOspfStubMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfStubMetric (pu4ErrorCode,
                                        u4FsMIStdOspfStubAreaId,
                                        i4FsMIStdOspfStubTOS,
                                        i4TestValFsMIStdOspfStubMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfStubStatus
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                testValFsMIStdOspfStubStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfStubStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfStubContextId,
                                UINT4 u4FsMIStdOspfStubAreaId,
                                INT4 i4FsMIStdOspfStubTOS,
                                INT4 i4TestValFsMIStdOspfStubStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfStubStatus (pu4ErrorCode,
                                        u4FsMIStdOspfStubAreaId,
                                        i4FsMIStdOspfStubTOS,
                                        i4TestValFsMIStdOspfStubStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfStubMetricType
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                testValFsMIStdOspfStubMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfStubMetricType (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfStubContextId,
                                    UINT4 u4FsMIStdOspfStubAreaId,
                                    INT4 i4FsMIStdOspfStubTOS,
                                    INT4 i4TestValFsMIStdOspfStubMetricType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfStubMetricType (pu4ErrorCode,
                                            u4FsMIStdOspfStubAreaId,
                                            i4FsMIStdOspfStubTOS,
                                            i4TestValFsMIStdOspfStubMetricType);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfLsdbTable
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfLsdbTable (INT4 i4FsMIStdOspfLsdbContextId,
                                              UINT4 u4FsMIStdOspfLsdbAreaId,
                                              INT4 i4FsMIStdOspfLsdbType,
                                              UINT4 u4FsMIStdOspfLsdbLsid,
                                              UINT4 u4FsMIStdOspfLsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfLsdbContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfLsdbContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfLsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfLsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfLsdbTable (u4FsMIStdOspfLsdbAreaId,
                                                      i4FsMIStdOspfLsdbType,
                                                      u4FsMIStdOspfLsdbLsid,
                                                      u4FsMIStdOspfLsdbRouterId);

    UtilOspfResetContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfHostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfHostTable
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfHostTable (INT4 i4FsMIStdOspfHostContextId,
                                              UINT4 u4FsMIStdOspfHostIpAddress,
                                              INT4 i4FsMIStdOspfHostTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfHostContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfHostContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfHostContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfHostContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfHostTable
        (u4FsMIStdOspfHostIpAddress, i4FsMIStdOspfHostTOS);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfHostMetric
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                testValFsMIStdOspfHostMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfHostMetric (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfHostContextId,
                                UINT4 u4FsMIStdOspfHostIpAddress,
                                INT4 i4FsMIStdOspfHostTOS,
                                INT4 i4TestValFsMIStdOspfHostMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfHostMetric (pu4ErrorCode,
                                        u4FsMIStdOspfHostIpAddress,
                                        i4FsMIStdOspfHostTOS,
                                        i4TestValFsMIStdOspfHostMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfHostStatus
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                testValFsMIStdOspfHostStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfHostStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfHostContextId,
                                UINT4 u4FsMIStdOspfHostIpAddress,
                                INT4 i4FsMIStdOspfHostTOS,
                                INT4 i4TestValFsMIStdOspfHostStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfHostStatus (pu4ErrorCode,
                                        u4FsMIStdOspfHostIpAddress,
                                        i4FsMIStdOspfHostTOS,
                                        i4TestValFsMIStdOspfHostStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfIfTable
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfIfTable (INT4 i4FsMIStdOspfIfContextId,
                                            UINT4 u4FsMIStdOspfIfIpAddress,
                                            INT4 i4FsMIStdOspfAddressLessIf)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfIfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfIfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfIfTable (u4FsMIStdOspfIfIpAddress,
                                                    i4FsMIStdOspfAddressLessIf);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfAreaId
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfAreaId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfAreaId (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfIfContextId,
                              UINT4 u4FsMIStdOspfIfIpAddress,
                              INT4 i4FsMIStdOspfAddressLessIf,
                              UINT4 u4TestValFsMIStdOspfIfAreaId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfAreaId (pu4ErrorCode,
                                      u4FsMIStdOspfIfIpAddress,
                                      i4FsMIStdOspfAddressLessIf,
                                      u4TestValFsMIStdOspfIfAreaId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfType (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIStdOspfIfContextId,
                            UINT4 u4FsMIStdOspfIfIpAddress,
                            INT4 i4FsMIStdOspfAddressLessIf,
                            INT4 i4TestValFsMIStdOspfIfType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfType (pu4ErrorCode,
                                    u4FsMIStdOspfIfIpAddress,
                                    i4FsMIStdOspfAddressLessIf,
                                    i4TestValFsMIStdOspfIfType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfAdminStat
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfAdminStat (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIStdOspfIfContextId,
                                 UINT4 u4FsMIStdOspfIfIpAddress,
                                 INT4 i4FsMIStdOspfAddressLessIf,
                                 INT4 i4TestValFsMIStdOspfIfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfAdminStat (pu4ErrorCode,
                                         u4FsMIStdOspfIfIpAddress,
                                         i4FsMIStdOspfAddressLessIf,
                                         i4TestValFsMIStdOspfIfAdminStat);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfRtrPriority
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfRtrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfRtrPriority (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfIfContextId,
                                   UINT4 u4FsMIStdOspfIfIpAddress,
                                   INT4 i4FsMIStdOspfAddressLessIf,
                                   INT4 i4TestValFsMIStdOspfIfRtrPriority)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfRtrPriority (pu4ErrorCode,
                                           u4FsMIStdOspfIfIpAddress,
                                           i4FsMIStdOspfAddressLessIf,
                                           i4TestValFsMIStdOspfIfRtrPriority);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfTransitDelay (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfIfContextId,
                                    UINT4 u4FsMIStdOspfIfIpAddress,
                                    INT4 i4FsMIStdOspfAddressLessIf,
                                    INT4 i4TestValFsMIStdOspfIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfTransitDelay (pu4ErrorCode,
                                            u4FsMIStdOspfIfIpAddress,
                                            i4FsMIStdOspfAddressLessIf,
                                            i4TestValFsMIStdOspfIfTransitDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfRetransInterval (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfIfContextId,
                                       UINT4 u4FsMIStdOspfIfIpAddress,
                                       INT4 i4FsMIStdOspfAddressLessIf,
                                       INT4
                                       i4TestValFsMIStdOspfIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfRetransInterval (pu4ErrorCode,
                                               u4FsMIStdOspfIfIpAddress,
                                               i4FsMIStdOspfAddressLessIf,
                                               i4TestValFsMIStdOspfIfRetransInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfHelloInterval (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdOspfIfContextId,
                                     UINT4 u4FsMIStdOspfIfIpAddress,
                                     INT4 i4FsMIStdOspfAddressLessIf,
                                     INT4 i4TestValFsMIStdOspfIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfHelloInterval (pu4ErrorCode,
                                             u4FsMIStdOspfIfIpAddress,
                                             i4FsMIStdOspfAddressLessIf,
                                             i4TestValFsMIStdOspfIfHelloInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfIfContextId,
                                       UINT4 u4FsMIStdOspfIfIpAddress,
                                       INT4 i4FsMIStdOspfAddressLessIf,
                                       INT4
                                       i4TestValFsMIStdOspfIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfRtrDeadInterval (pu4ErrorCode,
                                               u4FsMIStdOspfIfIpAddress,
                                               i4FsMIStdOspfAddressLessIf,
                                               i4TestValFsMIStdOspfIfRtrDeadInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfPollInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfPollInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfPollInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfIfContextId,
                                    UINT4 u4FsMIStdOspfIfIpAddress,
                                    INT4 i4FsMIStdOspfAddressLessIf,
                                    INT4 i4TestValFsMIStdOspfIfPollInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfPollInterval (pu4ErrorCode,
                                            u4FsMIStdOspfIfIpAddress,
                                            i4FsMIStdOspfAddressLessIf,
                                            i4TestValFsMIStdOspfIfPollInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfAuthKey
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfAuthKey (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIStdOspfIfContextId,
                               UINT4 u4FsMIStdOspfIfIpAddress,
                               INT4 i4FsMIStdOspfAddressLessIf,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsMIStdOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfAuthKey (pu4ErrorCode,
                                       u4FsMIStdOspfIfIpAddress,
                                       i4FsMIStdOspfAddressLessIf,
                                       pTestValFsMIStdOspfIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfStatus
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfIfContextId,
                              UINT4 u4FsMIStdOspfIfIpAddress,
                              INT4 i4FsMIStdOspfAddressLessIf,
                              INT4 i4TestValFsMIStdOspfIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfStatus (pu4ErrorCode,
                                      u4FsMIStdOspfIfIpAddress,
                                      i4FsMIStdOspfAddressLessIf,
                                      i4TestValFsMIStdOspfIfStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfMulticastForwarding
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfMulticastForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfMulticastForwarding (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfIfContextId,
                                           UINT4 u4FsMIStdOspfIfIpAddress,
                                           INT4 i4FsMIStdOspfAddressLessIf,
                                           INT4
                                           i4TestValFsMIStdOspfIfMulticastForwarding)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfMulticastForwarding (pu4ErrorCode,
                                                   u4FsMIStdOspfIfIpAddress,
                                                   i4FsMIStdOspfAddressLessIf,
                                                   i4TestValFsMIStdOspfIfMulticastForwarding);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfDemand
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfDemand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfDemand (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfIfContextId,
                              UINT4 u4FsMIStdOspfIfIpAddress,
                              INT4 i4FsMIStdOspfAddressLessIf,
                              INT4 i4TestValFsMIStdOspfIfDemand)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfDemand (pu4ErrorCode,
                                      u4FsMIStdOspfIfIpAddress,
                                      i4FsMIStdOspfAddressLessIf,
                                      i4TestValFsMIStdOspfIfDemand);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfAuthType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfAuthType (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfIfContextId,
                                UINT4 u4FsMIStdOspfIfIpAddress,
                                INT4 i4FsMIStdOspfAddressLessIf,
                                INT4 i4TestValFsMIStdOspfIfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfAuthType (pu4ErrorCode,
                                        u4FsMIStdOspfIfIpAddress,
                                        i4FsMIStdOspfAddressLessIf,
                                        i4TestValFsMIStdOspfIfAuthType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                testValFsMIStdOspfIfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfCryptoAuthType (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfIfContextId,
                                      UINT4 u4FsMIStdOspfIfIpAddress,
                                      INT4 i4FsMIStdOspfAddressLessIf,
                                      INT4 i4TestValFsMIStdOspfIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfCryptoAuthType (pu4ErrorCode,
                                              u4FsMIStdOspfIfIpAddress,
                                              i4FsMIStdOspfAddressLessIf,
                                              i4TestValFsMIStdOspfIfCryptoAuthType);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfIfMetricTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfIfMetricTable
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfIfMetricTable (INT4
                                                  i4FsMIStdOspfIfMetricContextId,
                                                  UINT4
                                                  u4FsMIStdOspfIfMetricIpAddress,
                                                  INT4
                                                  i4FsMIStdOspfIfMetricAddressLessIf,
                                                  INT4 i4FsMIStdOspfIfMetricTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfIfMetricContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfIfMetricContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfIfMetricContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfIfMetricContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfIfMetricTable
        (u4FsMIStdOspfIfMetricIpAddress,
         i4FsMIStdOspfIfMetricAddressLessIf, i4FsMIStdOspfIfMetricTOS);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfMetricValue
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS

                The Object 
                testValFsMIStdOspfIfMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfMetricValue (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfIfMetricContextId,
                                   UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                   INT4 i4FsMIStdOspfIfMetricAddressLessIf,
                                   INT4 i4FsMIStdOspfIfMetricTOS,
                                   INT4 i4TestValFsMIStdOspfIfMetricValue)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfMetricValue (pu4ErrorCode,
                                           u4FsMIStdOspfIfMetricIpAddress,
                                           i4FsMIStdOspfIfMetricAddressLessIf,
                                           i4FsMIStdOspfIfMetricTOS,
                                           i4TestValFsMIStdOspfIfMetricValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfIfMetricStatus
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS

                The Object 
                testValFsMIStdOspfIfMetricStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfIfMetricStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfIfMetricContextId,
                                    UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                    INT4 i4FsMIStdOspfIfMetricAddressLessIf,
                                    INT4 i4FsMIStdOspfIfMetricTOS,
                                    INT4 i4TestValFsMIStdOspfIfMetricStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfIfMetricStatus (pu4ErrorCode,
                                            u4FsMIStdOspfIfMetricIpAddress,
                                            i4FsMIStdOspfIfMetricAddressLessIf,
                                            i4FsMIStdOspfIfMetricTOS,
                                            i4TestValFsMIStdOspfIfMetricStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfVirtIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfVirtIfTable
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfVirtIfTable (INT4
                                                i4FsMIStdOspfVirtIfContextId,
                                                UINT4 u4FsMIStdOspfVirtIfAreaId,
                                                UINT4
                                                u4FsMIStdOspfVirtIfNeighbor)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfVirtIfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfVirtIfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfVirtIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfVirtIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfVirtIfTable
        (u4FsMIStdOspfVirtIfAreaId, u4FsMIStdOspfVirtIfNeighbor);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfTransitDelay (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdOspfVirtIfContextId,
                                        UINT4 u4FsMIStdOspfVirtIfAreaId,
                                        UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                        INT4
                                        i4TestValFsMIStdOspfVirtIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfTransitDelay (pu4ErrorCode,
                                                u4FsMIStdOspfVirtIfAreaId,
                                                u4FsMIStdOspfVirtIfNeighbor,
                                                i4TestValFsMIStdOspfVirtIfTransitDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfRetransInterval (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfVirtIfContextId,
                                           UINT4 u4FsMIStdOspfVirtIfAreaId,
                                           UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                           INT4
                                           i4TestValFsMIStdOspfVirtIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfRetransInterval (pu4ErrorCode,
                                                   u4FsMIStdOspfVirtIfAreaId,
                                                   u4FsMIStdOspfVirtIfNeighbor,
                                                   i4TestValFsMIStdOspfVirtIfRetransInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfHelloInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfVirtIfContextId,
                                         UINT4 u4FsMIStdOspfVirtIfAreaId,
                                         UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                         INT4
                                         i4TestValFsMIStdOspfVirtIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfHelloInterval (pu4ErrorCode,
                                                 u4FsMIStdOspfVirtIfAreaId,
                                                 u4FsMIStdOspfVirtIfNeighbor,
                                                 i4TestValFsMIStdOspfVirtIfHelloInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfVirtIfContextId,
                                           UINT4 u4FsMIStdOspfVirtIfAreaId,
                                           UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                           INT4
                                           i4TestValFsMIStdOspfVirtIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfRtrDeadInterval (pu4ErrorCode,
                                                   u4FsMIStdOspfVirtIfAreaId,
                                                   u4FsMIStdOspfVirtIfNeighbor,
                                                   i4TestValFsMIStdOspfVirtIfRtrDeadInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfAuthKey (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfVirtIfContextId,
                                   UINT4 u4FsMIStdOspfVirtIfAreaId,
                                   UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMIStdOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfAuthKey (pu4ErrorCode,
                                           u4FsMIStdOspfVirtIfAreaId,
                                           u4FsMIStdOspfVirtIfNeighbor,
                                           pTestValFsMIStdOspfVirtIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfStatus
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfVirtIfContextId,
                                  UINT4 u4FsMIStdOspfVirtIfAreaId,
                                  UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                  INT4 i4TestValFsMIStdOspfVirtIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfStatus (pu4ErrorCode,
                                          u4FsMIStdOspfVirtIfAreaId,
                                          u4FsMIStdOspfVirtIfNeighbor,
                                          i4TestValFsMIStdOspfVirtIfStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfAuthType
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfAuthType (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfVirtIfContextId,
                                    UINT4 u4FsMIStdOspfVirtIfAreaId,
                                    UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                    INT4 i4TestValFsMIStdOspfVirtIfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfVirtIfAuthType (pu4ErrorCode,
                                            u4FsMIStdOspfVirtIfAreaId,
                                            u4FsMIStdOspfVirtIfNeighbor,
                                            i4TestValFsMIStdOspfVirtIfAuthType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfVirtIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                testValFsMIStdOspfVirtIfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfVirtIfCryptoAuthType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfContextId,
                                          UINT4 u4FsMIStdOspfVirtIfAreaId,
                                          UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                          INT4
                                          i4TestValFsMIStdOspfVirtIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2OspfVirtIfCryptoAuthType (pu4ErrorCode,
                                                  u4FsMIStdOspfVirtIfAreaId,
                                                  u4FsMIStdOspfVirtIfNeighbor,
                                                  i4TestValFsMIStdOspfVirtIfCryptoAuthType);

    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfNbrTable
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfNbrTable (INT4 i4FsMIStdOspfNbrContextId,
                                             UINT4 u4FsMIStdOspfNbrIpAddr,
                                             INT4
                                             i4FsMIStdOspfNbrAddressLessIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfNbrContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfNbrContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfNbrContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfNbrContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfNbrTable (u4FsMIStdOspfNbrIpAddr,
                                                     i4FsMIStdOspfNbrAddressLessIndex);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfNbrPriority
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                testValFsMIStdOspfNbrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfNbrPriority (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIStdOspfNbrContextId,
                                 UINT4 u4FsMIStdOspfNbrIpAddr,
                                 INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                 INT4 i4TestValFsMIStdOspfNbrPriority)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfNbrPriority (pu4ErrorCode,
                                         u4FsMIStdOspfNbrIpAddr,
                                         i4FsMIStdOspfNbrAddressLessIndex,
                                         i4TestValFsMIStdOspfNbrPriority);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfNbmaNbrStatus
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                testValFsMIStdOspfNbmaNbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfNbmaNbrStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfNbrContextId,
                                   UINT4 u4FsMIStdOspfNbrIpAddr,
                                   INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                   INT4 i4TestValFsMIStdOspfNbmaNbrStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfNbmaNbrStatus (pu4ErrorCode,
                                           u4FsMIStdOspfNbrIpAddr,
                                           i4FsMIStdOspfNbrAddressLessIndex,
                                           i4TestValFsMIStdOspfNbmaNbrStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfVirtNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfVirtNbrTable
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfVirtNbrTable (INT4
                                                 i4FsMIStdOspfVirtNbrContextId,
                                                 UINT4 u4FsMIStdOspfVirtNbrArea,
                                                 UINT4
                                                 u4FsMIStdOspfVirtNbrRtrId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfVirtNbrContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfVirtNbrContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfVirtNbrContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfVirtNbrContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfVirtNbrTable
        (u4FsMIStdOspfVirtNbrArea, u4FsMIStdOspfVirtNbrRtrId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfExtLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfExtLsdbTable
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfExtLsdbTable (INT4
                                                 i4FsMIStdOspfExtLsdbContextId,
                                                 INT4 i4FsMIStdOspfExtLsdbType,
                                                 UINT4 u4FsMIStdOspfExtLsdbLsid,
                                                 UINT4
                                                 u4FsMIStdOspfExtLsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfExtLsdbContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfExtLsdbContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfExtLsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfExtLsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfExtLsdbContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfExtLsdbTable
        (i4FsMIStdOspfExtLsdbType, u4FsMIStdOspfExtLsdbLsid,
         u4FsMIStdOspfExtLsdbRouterId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfAreaAggregateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfAreaAggregateTable
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfAreaAggregateTable (INT4
                                                       i4FsMIStdOspfAreaAggregateContextId,
                                                       UINT4
                                                       u4FsMIStdOspfAreaAggregateAreaID,
                                                       INT4
                                                       i4FsMIStdOspfAreaAggregateLsdbType,
                                                       UINT4
                                                       u4FsMIStdOspfAreaAggregateNet,
                                                       UINT4
                                                       u4FsMIStdOspfAreaAggregateMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfAreaAggregateContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfAreaAggregateContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfAreaAggregateContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfAreaAggregateContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfAreaAggregateTable
        (u4FsMIStdOspfAreaAggregateAreaID,
         i4FsMIStdOspfAreaAggregateLsdbType,
         u4FsMIStdOspfAreaAggregateNet, u4FsMIStdOspfAreaAggregateMask);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfAreaAggregateStatus
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask

                The Object 
                testValFsMIStdOspfAreaAggregateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfAreaAggregateStatus (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4FsMIStdOspfAreaAggregateContextId,
                                         UINT4 u4FsMIStdOspfAreaAggregateAreaID,
                                         INT4
                                         i4FsMIStdOspfAreaAggregateLsdbType,
                                         UINT4 u4FsMIStdOspfAreaAggregateNet,
                                         UINT4 u4FsMIStdOspfAreaAggregateMask,
                                         INT4
                                         i4TestValFsMIStdOspfAreaAggregateStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfAreaAggregateStatus (pu4ErrorCode,
                                                 u4FsMIStdOspfAreaAggregateAreaID,
                                                 i4FsMIStdOspfAreaAggregateLsdbType,
                                                 u4FsMIStdOspfAreaAggregateNet,
                                                 u4FsMIStdOspfAreaAggregateMask,
                                                 i4TestValFsMIStdOspfAreaAggregateStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfAreaAggregateEffect
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask

                The Object 
                testValFsMIStdOspfAreaAggregateEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfAreaAggregateEffect (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4FsMIStdOspfAreaAggregateContextId,
                                         UINT4 u4FsMIStdOspfAreaAggregateAreaID,
                                         INT4
                                         i4FsMIStdOspfAreaAggregateLsdbType,
                                         UINT4 u4FsMIStdOspfAreaAggregateNet,
                                         UINT4 u4FsMIStdOspfAreaAggregateMask,
                                         INT4
                                         i4TestValFsMIStdOspfAreaAggregateEffect)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfAreaAggregateEffect (pu4ErrorCode,
                                                 u4FsMIStdOspfAreaAggregateAreaID,
                                                 i4FsMIStdOspfAreaAggregateLsdbType,
                                                 u4FsMIStdOspfAreaAggregateNet,
                                                 u4FsMIStdOspfAreaAggregateMask,
                                                 i4TestValFsMIStdOspfAreaAggregateEffect);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfGlobalTraceLevel
 Input       :  The Indices

                The Object
                testValFsMIOspfGlobalTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfGlobalTraceLevel (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMIOspfGlobalTraceLevel)
{

    if (((UINT4) i4TestValFsMIOspfGlobalTraceLevel < OSPF_DBG_MIN_TRC) ||
        ((UINT4) i4TestValFsMIOspfGlobalTraceLevel > OSPF_DBG_MAX_TRC ))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_OSPF_INV_VALUE);
        return SNMP_FAILURE;
    }    
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVrfSpfInterval
 Input       :  The Indices

                The Object
                testValFsMIOspfVrfSpfInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVrfSpfInterval (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMIOspfVrfSpfInterval)
{
    if ((i4TestValFsMIOspfVrfSpfInterval <
         OSPF_MIN_VRF_SPF_INTERVAL) ||
        (i4TestValFsMIOspfVrfSpfInterval > OSPF_MAX_VRF_SPF_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIOspfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfTable
 Input       :  The Indices
                FsMIOspfContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfTable (INT4 i4FsMIOspfContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIOspfContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIOspfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRFC1583Compatibility
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                testValFsMIOspfRFC1583Compatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRFC1583Compatibility (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIOspfContextId,
                                       INT4
                                       i4TestValFsMIOspfRFC1583Compatibility)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRFC1583Compatibility (pu4ErrorCode,
                                                     i4TestValFsMIOspfRFC1583Compatibility);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfTraceLevel
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                testValFsMIOspfTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfTraceLevel (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIOspfContextId,
                             INT4 i4TestValFsMIOspfTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfTraceLevel (pu4ErrorCode,
                                           i4TestValFsMIOspfTraceLevel);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfMinLsaInterval
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                testValFsMIOspfMinLsaInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfMinLsaInterval (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIOspfContextId,
                                 INT4 i4TestValFsMIOspfMinLsaInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfMinLsaInterval (pu4ErrorCode,
                                               i4TestValFsMIOspfMinLsaInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfABRType
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                testValFsMIOspfABRType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfABRType (UINT4 *pu4ErrorCode,
                          INT4 i4FsMIOspfContextId,
                          INT4 i4TestValFsMIOspfABRType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfABRType (pu4ErrorCode, i4TestValFsMIOspfABRType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfNssaAsbrDefRtTrans
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                testValFsMIOspfNssaAsbrDefRtTrans
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfNssaAsbrDefRtTrans (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIOspfContextId,
                                     INT4 i4TestValFsMIOspfNssaAsbrDefRtTrans)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfNssaAsbrDefRtTrans (pu4ErrorCode,
                                                   i4TestValFsMIOspfNssaAsbrDefRtTrans);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfDefaultPassiveInterface
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                testValFsMIOspfDefaultPassiveInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfDefaultPassiveInterface (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIOspfContextId,
                                          INT4
                                          i4TestValFsMIOspfDefaultPassiveInterface)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfDefaultPassiveInterface (pu4ErrorCode,
                                                        i4TestValFsMIOspfDefaultPassiveInterface);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfSpfHoldtime
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIOspfSpfHoldtime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfSpfHoldtime (UINT4 *pu4ErrorCode, INT4 i4FsMIStdOspfContextId,
                              INT4 i4TestValFsMIOspfSpfHoldtime)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfSpfHoldtime (pu4ErrorCode,
                                     i4TestValFsMIOspfSpfHoldtime);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfSpfDelay
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIOspfSpfDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfSpfDelay (UINT4 *pu4ErrorCode, INT4 i4FsMIStdOspfContextId,
                           INT4 i4TestValFsMIOspfSpfDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhTestv2FutOspfSpfDelay (pu4ErrorCode, i4TestValFsMIOspfSpfDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRestartSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIOspfRestartSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIOspfRestartSupport
    (UINT4 *pu4ErrorCode,
     INT4 i4FsMIStdOspfContextId, INT4 i4TestValFsMIOspfRestartSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRestartSupport (pu4ErrorCode,
                                               i4TestValFsMIOspfRestartSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRestartInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIOspfRestartInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIOspfRestartInterval
    (UINT4 *pu4ErrorCode,
     INT4 i4FsMIStdOspfContextId, INT4 i4TestValFsMIOspfRestartInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRestartInterval
        (pu4ErrorCode, i4TestValFsMIOspfRestartInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRestartStrictLsaChecking
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIOspfRestartStrictLsaChecking
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIOspfRestartStrictLsaChecking
    (UINT4 *pu4ErrorCode,
     INT4 i4FsMIStdOspfContextId,
     INT4 i4TestValFsMIOspfRestartStrictLsaChecking)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRestartStrictLsaChecking
        (pu4ErrorCode, i4TestValFsMIOspfRestartStrictLsaChecking);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfHelperSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfHelperSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfHelperSupport (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsMIOspfHelperSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfHelperSupport (pu4ErrorCode,
                                       pTestValFsMIOspfHelperSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtTraceLevel
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfExtTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtTraceLevel (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                INT4 i4TestValFsMIOspfExtTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfExtTraceLevel (pu4ErrorCode,
                                       i4TestValFsMIOspfExtTraceLevel);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfHelperGraceTimeLimit
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfHelperGraceTimeLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfHelperGraceTimeLimit (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfContextId,
                                       INT4
                                       i4TestValFsMIOspfHelperGraceTimeLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfHelperGraceTimeLimit (pu4ErrorCode,
                                              i4TestValFsMIOspfHelperGraceTimeLimit);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRestartAckState
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfRestartAckState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRestartAckState (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfContextId,
                                  INT4 i4TestValFsMIOspfRestartAckState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfRestartAckState (pu4ErrorCode,
                                         i4TestValFsMIOspfRestartAckState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfGraceLsaRetransmitCount
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfGraceLsaRetransmitCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfGraceLsaRetransmitCount (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfContextId,
                                          INT4
                                          i4TestValFsMIOspfGraceLsaRetransmitCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfGraceLsaRetransmitCount (pu4ErrorCode,
                                                 i4TestValFsMIOspfGraceLsaRetransmitCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRestartReason
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfRestartReason
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRestartReason (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                INT4 i4TestValFsMIOspfRestartReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfRestartReason (pu4ErrorCode,
                                       i4TestValFsMIOspfRestartReason);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfGlobalExtTraceLevel
 Input       :  The Indices

                The Object
                testValFsMIOspfGlobalExtTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfGlobalExtTraceLevel (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsMIOspfGlobalExtTraceLevel)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if ((i4TestValFsMIOspfGlobalExtTraceLevel != 0 ) &&
        (i4TestValFsMIOspfGlobalExtTraceLevel != OSPF_RESTART_TRC) &&
        (i4TestValFsMIOspfGlobalExtTraceLevel != OSPF_HELPER_TRC) &&
        (i4TestValFsMIOspfGlobalExtTraceLevel != OSPF_REDUNDANCY_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
  Function    :  nmhTestv2FsMIOspfRTStaggeringInterval
  Input       :  The Indices
                 FsMIStdOspfContextId
                 The Object
                 testValFsMIOspfRTStaggeringInterval
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRTStaggeringInterval (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfContextId,
                                       UINT4
                                       u4TestValFsMIOspfRTStaggeringInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRTStaggeringInterval (pu4ErrorCode,
                                                     u4TestValFsMIOspfRTStaggeringInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
  Function    :  nmhTestv2FsMIOspfRouterIdPermanence
  Input       :  The Indices
                 FsMIStdOspfContextId
                 The Object
                 testValFsMIOspfRouterIdPermanence
  Output      :  The Test Low Lev Routine Take the Indices &
                 Test whether that Value is Valid Input for Set.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned                                                     SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE                                                     ****************************************************************************/
INT1
nmhTestv2FsMIOspfRouterIdPermanence (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdOspfContextId,
                                     INT4 i4TestValFsMIOspfRouterIdPermanence)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }
    if (nmhTestv2FutospfRouterIdPermanence
        (pu4ErrorCode, i4TestValFsMIOspfRouterIdPermanence) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfBfdStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                The Object
                testValFsMIOspfBfdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/
INT1
nmhTestv2FsMIOspfBfdStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIStdOspfContextId,
                            INT4 i4TestValFsMIOspfBfdStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }
    i1Return = nmhTestv2FutOspfBfdStatus
        (pu4ErrorCode, i4TestValFsMIOspfBfdStatus);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfBfdAllIfState
 Input       :  The Indices
                FsMIStdOspfContextId
                The Object
                testValFsMIOspfBfdAllIfState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**********************************************************************************/
INT1
nmhTestv2FsMIOspfBfdAllIfState (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                INT4 i4TestValFsMIOspfBfdAllIfState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfBfdAllIfState
        (pu4ErrorCode, i4TestValFsMIOspfBfdAllIfState);

    UtilOspfResetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRTStaggeringStatus
 Input       :  The Indices

                The Object
                testValFsMIOspfRTStaggeringStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRTStaggeringStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMIOspfRTStaggeringStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FutOspfRTStaggeringStatus (pu4ErrorCode,
                                            i4TestValFsMIOspfRTStaggeringStatus);
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfAreaTable
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfAreaTable (INT4 i4FsMIOspfAreaContextId,
                                           UINT4 u4FsMIOspfAreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfAreaContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfAreaContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfAreaContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfAreaContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfAreaTable (u4FsMIOspfAreaId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAreaNSSATranslatorRole
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                testValFsMIOspfAreaNSSATranslatorRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAreaNSSATranslatorRole (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIOspfAreaContextId,
                                         UINT4 u4FsMIOspfAreaId,
                                         INT4
                                         i4TestValFsMIOspfAreaNSSATranslatorRole)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfAreaNSSATranslatorRole (pu4ErrorCode,
                                                       u4FsMIOspfAreaId,
                                                       i4TestValFsMIOspfAreaNSSATranslatorRole);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAreaNSSATranslatorStabilityInterval
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                testValFsMIOspfAreaNSSATranslatorStabilityInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAreaNSSATranslatorStabilityInterval (UINT4 *pu4ErrorCode,
                                                      INT4
                                                      i4FsMIOspfAreaContextId,
                                                      UINT4 u4FsMIOspfAreaId,
                                                      INT4
                                                      i4TestValFsMIOspfAreaNSSATranslatorStabilityInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval (pu4ErrorCode,
                                                             u4FsMIOspfAreaId,
                                                             i4TestValFsMIOspfAreaNSSATranslatorStabilityInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAreaDfInfOriginate
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                testValFsMIOspfAreaDfInfOriginate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAreaDfInfOriginate (UINT4 *pu4ErrorCode,
                                     INT4
                                     i4FsMIOspfAreaContextId,
                                     UINT4 u4FsMIOspfAreaId,
                                     INT4 i4TestValFsMIOspfAreaDfInfOriginate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfAreaDfInfOriginate (pu4ErrorCode,
                                            u4FsMIOspfAreaId,
                                            i4TestValFsMIOspfAreaDfInfOriginate);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfHostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfHostTable
 Input       :  The Indices
                FsMIOspfHostContextId
                FsMIOspfHostIpAddress
                FsMIOspfHostTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfHostTable (INT4 i4FsMIOspfHostContextId,
                                           UINT4 u4FsMIOspfHostIpAddress,
                                           INT4 i4FsMIOspfHostTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfHostContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfHostContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfHostContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfHostContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfHostContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfHostTable
        (u4FsMIOspfHostIpAddress, i4FsMIOspfHostTOS);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfHostRouteIfIndex
 Input       :  The Indices
                FsMIOspfHostContextId
                FsMIOspfHostIpAddress
                FsMIOspfHostTOS

                The Object 
                testValFsMIOspfHostRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfHostRouteIfIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIOspfHostContextId,
                                   UINT4 u4FsMIOspfHostIpAddress,
                                   INT4 i4FsMIOspfHostTOS,
                                   INT4 i4TestValFsMIOspfHostRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfHostRouteIfIndex (pu4ErrorCode,
                                                 u4FsMIOspfHostIpAddress,
                                                 i4FsMIOspfHostTOS,
                                                 i4TestValFsMIOspfHostRouteIfIndex);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfIfTable
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfIfTable (INT4 i4FsMIOspfIfContextId,
                                         UINT4 u4FsMIOspfIfIpAddress,
                                         INT4 i4FsMIOspfAddressLessIf)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfIfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfIfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfIfTable (u4FsMIOspfIfIpAddress,
                                                       i4FsMIOspfAddressLessIf);
    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfPassive
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                testValFsMIOspfIfPassive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfPassive (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIOspfIfContextId,
                            UINT4 u4FsMIOspfIfIpAddress,
                            INT4 i4FsMIOspfAddressLessIf,
                            INT4 i4TestValFsMIOspfIfPassive)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfPassive (pu4ErrorCode,
                                          u4FsMIOspfIfIpAddress,
                                          i4FsMIOspfAddressLessIf,
                                          i4TestValFsMIOspfIfPassive);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfBfdState
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
                The Object
                testValFsMIOspfIfBfdState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/
INT1
nmhTestv2FsMIOspfIfBfdState (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIStdOspfContextId,
                             UINT4 u4FsMIOspfIfIpAddress,
                             INT4 i4FsMIOspfAddressLessIf,
                             INT4 i4TestValFsMIOspfIfBfdState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfBfdState (pu4ErrorCode,
                                           u4FsMIOspfIfIpAddress,
                                           i4FsMIOspfAddressLessIf,
                                           i4TestValFsMIOspfIfBfdState);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfIfMD5AuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfIfMD5AuthTable (INT4
                                                i4FsMIOspfIfMD5AuthContextId,
                                                UINT4
                                                u4FsMIOspfIfMD5AuthIpAddress,
                                                INT4
                                                i4FsMIOspfIfMD5AuthAddressLessIf,
                                                INT4 i4FsMIOspfIfMD5AuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfIfMD5AuthContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfIfMD5AuthContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfIfMD5AuthContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfIfMD5AuthContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfIfMD5AuthTable
        (u4FsMIOspfIfMD5AuthIpAddress, i4FsMIOspfIfMD5AuthAddressLessIf,
         i4FsMIOspfIfMD5AuthKeyId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfMD5AuthKey
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                testValFsMIOspfIfMD5AuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfMD5AuthKey (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIOspfIfMD5AuthContextId,
                               UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                               INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                               INT4 i4FsMIOspfIfMD5AuthKeyId,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsMIOspfIfMD5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfMD5AuthKey (pu4ErrorCode,
                                             u4FsMIOspfIfMD5AuthIpAddress,
                                             i4FsMIOspfIfMD5AuthAddressLessIf,
                                             i4FsMIOspfIfMD5AuthKeyId,
                                             pTestValFsMIOspfIfMD5AuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                testValFsMIOspfIfMD5AuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIOspfIfMD5AuthContextId,
                                          UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                          INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                          INT4 i4FsMIOspfIfMD5AuthKeyId,
                                          INT4
                                          i4TestValFsMIOspfIfMD5AuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStartAccept (pu4ErrorCode,
                                                        u4FsMIOspfIfMD5AuthIpAddress,
                                                        i4FsMIOspfIfMD5AuthAddressLessIf,
                                                        i4FsMIOspfIfMD5AuthKeyId,
                                                        i4TestValFsMIOspfIfMD5AuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                testValFsMIOspfIfMD5AuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIOspfIfMD5AuthContextId,
                                            UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                            INT4
                                            i4FsMIOspfIfMD5AuthAddressLessIf,
                                            INT4 i4FsMIOspfIfMD5AuthKeyId,
                                            INT4
                                            i4TestValFsMIOspfIfMD5AuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStartGenerate (pu4ErrorCode,
                                                          u4FsMIOspfIfMD5AuthIpAddress,
                                                          i4FsMIOspfIfMD5AuthAddressLessIf,
                                                          i4FsMIOspfIfMD5AuthKeyId,
                                                          i4TestValFsMIOspfIfMD5AuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                testValFsMIOspfIfMD5AuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIOspfIfMD5AuthContextId,
                                           UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                           INT4
                                           i4FsMIOspfIfMD5AuthAddressLessIf,
                                           INT4 i4FsMIOspfIfMD5AuthKeyId,
                                           INT4
                                           i4TestValFsMIOspfIfMD5AuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStopGenerate (pu4ErrorCode,
                                                         u4FsMIOspfIfMD5AuthIpAddress,
                                                         i4FsMIOspfIfMD5AuthAddressLessIf,
                                                         i4FsMIOspfIfMD5AuthKeyId,
                                                         i4TestValFsMIOspfIfMD5AuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                testValFsMIOspfIfMD5AuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIOspfIfMD5AuthContextId,
                                         UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                         INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                         INT4 i4FsMIOspfIfMD5AuthKeyId,
                                         INT4
                                         i4TestValFsMIOspfIfMD5AuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStopAccept (pu4ErrorCode,
                                                       u4FsMIOspfIfMD5AuthIpAddress,
                                                       i4FsMIOspfIfMD5AuthAddressLessIf,
                                                       i4FsMIOspfIfMD5AuthKeyId,
                                                       i4TestValFsMIOspfIfMD5AuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfMD5AuthKeyStatus
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                testValFsMIOspfIfMD5AuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfMD5AuthKeyStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIOspfIfMD5AuthContextId,
                                     UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                     INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                     INT4 i4FsMIOspfIfMD5AuthKeyId,
                                     INT4 i4TestValFsMIOspfIfMD5AuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfMD5AuthKeyStatus (pu4ErrorCode,
                                                   u4FsMIOspfIfMD5AuthIpAddress,
                                                   i4FsMIOspfIfMD5AuthAddressLessIf,
                                                   i4FsMIOspfIfMD5AuthKeyId,
                                                   i4TestValFsMIOspfIfMD5AuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfVirtIfMD5AuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfVirtIfMD5AuthTable (INT4
                                                    i4FsMIOspfVirtIfMD5AuthContextId,
                                                    UINT4
                                                    u4FsMIOspfVirtIfMD5AuthAreaId,
                                                    UINT4
                                                    u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                    INT4
                                                    i4FsMIOspfVirtIfMD5AuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfVirtIfMD5AuthContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfVirtIfMD5AuthContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfVirtIfMD5AuthContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfVirtIfMD5AuthContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfVirtIfMD5AuthTable
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfMD5AuthKey
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                testValFsMIOspfVirtIfMD5AuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKey (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                   UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                   UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                   INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMIOspfVirtIfMD5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKey (pu4ErrorCode,
                                                 u4FsMIOspfVirtIfMD5AuthAreaId,
                                                 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                 i4FsMIOspfVirtIfMD5AuthKeyId,
                                                 pTestValFsMIOspfVirtIfMD5AuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                testValFsMIOspfVirtIfMD5AuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4FsMIOspfVirtIfMD5AuthContextId,
                                              UINT4
                                              u4FsMIOspfVirtIfMD5AuthAreaId,
                                              UINT4
                                              u4FsMIOspfVirtIfMD5AuthNeighbor,
                                              INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                              INT4
                                              i4TestValFsMIOspfVirtIfMD5AuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStartAccept (pu4ErrorCode,
                                                            u4FsMIOspfVirtIfMD5AuthAreaId,
                                                            u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                            i4FsMIOspfVirtIfMD5AuthKeyId,
                                                            i4TestValFsMIOspfVirtIfMD5AuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                testValFsMIOspfVirtIfMD5AuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4FsMIOspfVirtIfMD5AuthContextId,
                                                UINT4
                                                u4FsMIOspfVirtIfMD5AuthAreaId,
                                                UINT4
                                                u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                INT4
                                                i4FsMIOspfVirtIfMD5AuthKeyId,
                                                INT4
                                                i4TestValFsMIOspfVirtIfMD5AuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStartGenerate (pu4ErrorCode,
                                                              u4FsMIOspfVirtIfMD5AuthAreaId,
                                                              u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                              i4FsMIOspfVirtIfMD5AuthKeyId,
                                                              i4TestValFsMIOspfVirtIfMD5AuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                testValFsMIOspfVirtIfMD5AuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4FsMIOspfVirtIfMD5AuthContextId,
                                               UINT4
                                               u4FsMIOspfVirtIfMD5AuthAreaId,
                                               UINT4
                                               u4FsMIOspfVirtIfMD5AuthNeighbor,
                                               INT4
                                               i4FsMIOspfVirtIfMD5AuthKeyId,
                                               INT4
                                               i4TestValFsMIOspfVirtIfMD5AuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStopGenerate (pu4ErrorCode,
                                                             u4FsMIOspfVirtIfMD5AuthAreaId,
                                                             u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                             i4FsMIOspfVirtIfMD5AuthKeyId,
                                                             i4TestValFsMIOspfVirtIfMD5AuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                testValFsMIOspfVirtIfMD5AuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsMIOspfVirtIfMD5AuthContextId,
                                             UINT4
                                             u4FsMIOspfVirtIfMD5AuthAreaId,
                                             UINT4
                                             u4FsMIOspfVirtIfMD5AuthNeighbor,
                                             INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                             INT4
                                             i4TestValFsMIOspfVirtIfMD5AuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStopAccept (pu4ErrorCode,
                                                           u4FsMIOspfVirtIfMD5AuthAreaId,
                                                           u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                           i4FsMIOspfVirtIfMD5AuthKeyId,
                                                           i4TestValFsMIOspfVirtIfMD5AuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfMD5AuthKeyStatus
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                testValFsMIOspfVirtIfMD5AuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfMD5AuthKeyStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                         UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                         UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                         INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                         INT4
                                         i4TestValFsMIOspfVirtIfMD5AuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfMD5AuthKeyStatus (pu4ErrorCode,
                                                       u4FsMIOspfVirtIfMD5AuthAreaId,
                                                       u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                       i4FsMIOspfVirtIfMD5AuthKeyId,
                                                       i4TestValFsMIOspfVirtIfMD5AuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfNbrTable
 Input       :  The Indices
                FsMIOspfNbrContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfNbrTable (INT4 i4FsMIOspfNbrContextId,
                                          UINT4 u4FsMIOspfNbrIpAddr,
                                          INT4 i4FsMIOspfNbrAddressLessIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfNbrContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfNbrContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfNbrContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfNbrContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfNbrContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfNbrTable (u4FsMIOspfNbrIpAddr,
                                                        i4FsMIOspfNbrAddressLessIndex);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfRoutingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfRoutingTable
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfRoutingTable (INT4 i4FsMIOspfRouteContextId,
                                              UINT4 u4FsMIOspfRouteIpAddr,
                                              UINT4 u4FsMIOspfRouteIpAddrMask,
                                              INT4 i4FsMIOspfRouteIpTos,
                                              UINT4 u4FsMIOspfRouteIpNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfRouteContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfRouteContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfRoutingTable
        (u4FsMIOspfRouteIpAddr, u4FsMIOspfRouteIpAddrMask,
         i4FsMIOspfRouteIpTos, u4FsMIOspfRouteIpNextHop);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfSecIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfSecIfTable
 Input       :  The Indices
                FsMIOspfSecIfContextId
                FsMIOspfPrimIpAddr
                FsMIOspfPrimAddresslessIf
                FsMIOspfSecIpAddr
                FsMIOspfSecIpAddrMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfSecIfTable (INT4 i4FsMIOspfSecIfContextId,
                                            UINT4 u4FsMIOspfPrimIpAddr,
                                            INT4 i4FsMIOspfPrimAddresslessIf,
                                            UINT4 u4FsMIOspfSecIpAddr,
                                            UINT4 u4FsMIOspfSecIpAddrMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfSecIfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfSecIfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfSecIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfSecIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfSecIfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfSecIfTable (u4FsMIOspfPrimIpAddr,
                                                          i4FsMIOspfPrimAddresslessIf,
                                                          u4FsMIOspfSecIpAddr,
                                                          u4FsMIOspfSecIpAddrMask);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfSecIfStatus
 Input       :  The Indices
                FsMIOspfSecIfContextId
                FsMIOspfPrimIpAddr
                FsMIOspfPrimAddresslessIf
                FsMIOspfSecIpAddr
                FsMIOspfSecIpAddrMask

                The Object 
                testValFsMIOspfSecIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfSecIfStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIOspfSecIfContextId,
                              UINT4 u4FsMIOspfPrimIpAddr,
                              INT4 i4FsMIOspfPrimAddresslessIf,
                              UINT4 u4FsMIOspfSecIpAddr,
                              UINT4 u4FsMIOspfSecIpAddrMask,
                              INT4 i4TestValFsMIOspfSecIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfSecIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfSecIfStatus (pu4ErrorCode,
                                            u4FsMIOspfPrimIpAddr,
                                            i4FsMIOspfPrimAddresslessIf,
                                            u4FsMIOspfSecIpAddr,
                                            u4FsMIOspfSecIpAddrMask,
                                            i4TestValFsMIOspfSecIfStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfAreaAggregateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfAreaAggregateTable
 Input       :  The Indices
                FsMIOspfAreaAggregateContextId
                FsMIOspfAreaAggregateAreaID
                FsMIOspfAreaAggregateLsdbType
                FsMIOspfAreaAggregateNet
                FsMIOspfAreaAggregateMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfAreaAggregateTable (INT4
                                                    i4FsMIOspfAreaAggregateContextId,
                                                    UINT4
                                                    u4FsMIOspfAreaAggregateAreaID,
                                                    INT4
                                                    i4FsMIOspfAreaAggregateLsdbType,
                                                    UINT4
                                                    u4FsMIOspfAreaAggregateNet,
                                                    UINT4
                                                    u4FsMIOspfAreaAggregateMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfAreaAggregateContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfAreaAggregateContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfAreaAggregateContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfAreaAggregateContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfAreaAggregateContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfAreaAggregateTable
        (u4FsMIOspfAreaAggregateAreaID, i4FsMIOspfAreaAggregateLsdbType,
         u4FsMIOspfAreaAggregateNet, u4FsMIOspfAreaAggregateMask);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAreaAggregateExternalTag
 Input       :  The Indices
                FsMIOspfAreaAggregateContextId
                FsMIOspfAreaAggregateAreaID
                FsMIOspfAreaAggregateLsdbType
                FsMIOspfAreaAggregateNet
                FsMIOspfAreaAggregateMask

                The Object 
                testValFsMIOspfAreaAggregateExternalTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAreaAggregateExternalTag (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4FsMIOspfAreaAggregateContextId,
                                           UINT4 u4FsMIOspfAreaAggregateAreaID,
                                           INT4 i4FsMIOspfAreaAggregateLsdbType,
                                           UINT4 u4FsMIOspfAreaAggregateNet,
                                           UINT4 u4FsMIOspfAreaAggregateMask,
                                           INT4
                                           i4TestValFsMIOspfAreaAggregateExternalTag)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaAggregateContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfAreaAggregateExternalTag (pu4ErrorCode,
                                                         u4FsMIOspfAreaAggregateAreaID,
                                                         i4FsMIOspfAreaAggregateLsdbType,
                                                         u4FsMIOspfAreaAggregateNet,
                                                         u4FsMIOspfAreaAggregateMask,
                                                         i4TestValFsMIOspfAreaAggregateExternalTag);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfAsExternalAggregationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfAsExternalAggregationTable
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfAsExternalAggregationTable (INT4
                                                            i4FsMIOspfAsExternalAggregationContextId,
                                                            UINT4
                                                            u4FsMIOspfAsExternalAggregationNet,
                                                            UINT4
                                                            u4FsMIOspfAsExternalAggregationMask,
                                                            UINT4
                                                            u4FsMIOspfAsExternalAggregationAreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfAsExternalAggregationContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfAsExternalAggregationContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfAsExternalAggregationContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfAsExternalAggregationContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfAsExternalAggregationTable
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAsExternalAggregationEffect
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                testValFsMIOspfAsExternalAggregationEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAsExternalAggregationEffect (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4FsMIOspfAsExternalAggregationContextId,
                                              UINT4
                                              u4FsMIOspfAsExternalAggregationNet,
                                              UINT4
                                              u4FsMIOspfAsExternalAggregationMask,
                                              UINT4
                                              u4FsMIOspfAsExternalAggregationAreaId,
                                              INT4
                                              i4TestValFsMIOspfAsExternalAggregationEffect)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfAsExternalAggregationEffect (pu4ErrorCode,
                                                            u4FsMIOspfAsExternalAggregationNet,
                                                            u4FsMIOspfAsExternalAggregationMask,
                                                            u4FsMIOspfAsExternalAggregationAreaId,
                                                            i4TestValFsMIOspfAsExternalAggregationEffect);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAsExternalAggregationTranslation
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                testValFsMIOspfAsExternalAggregationTranslation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAsExternalAggregationTranslation (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4FsMIOspfAsExternalAggregationContextId,
                                                   UINT4
                                                   u4FsMIOspfAsExternalAggregationNet,
                                                   UINT4
                                                   u4FsMIOspfAsExternalAggregationMask,
                                                   UINT4
                                                   u4FsMIOspfAsExternalAggregationAreaId,
                                                   INT4
                                                   i4TestValFsMIOspfAsExternalAggregationTranslation)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfAsExternalAggregationTranslation (pu4ErrorCode,
                                                                 u4FsMIOspfAsExternalAggregationNet,
                                                                 u4FsMIOspfAsExternalAggregationMask,
                                                                 u4FsMIOspfAsExternalAggregationAreaId,
                                                                 i4TestValFsMIOspfAsExternalAggregationTranslation);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAsExternalAggregationStatus
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                testValFsMIOspfAsExternalAggregationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAsExternalAggregationStatus (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4FsMIOspfAsExternalAggregationContextId,
                                              UINT4
                                              u4FsMIOspfAsExternalAggregationNet,
                                              UINT4
                                              u4FsMIOspfAsExternalAggregationMask,
                                              UINT4
                                              u4FsMIOspfAsExternalAggregationAreaId,
                                              INT4
                                              i4TestValFsMIOspfAsExternalAggregationStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfAsExternalAggregationStatus (pu4ErrorCode,
                                                            u4FsMIOspfAsExternalAggregationNet,
                                                            u4FsMIOspfAsExternalAggregationMask,
                                                            u4FsMIOspfAsExternalAggregationAreaId,
                                                            i4TestValFsMIOspfAsExternalAggregationStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfTrapTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfTrapTable (INT4 i4FsMIStdOspfContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIStdOspfContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIStdOspfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfSetTrap
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIStdOspfSetTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfSetTrap (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIStdOspfContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsMIStdOspfSetTrap)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2OspfSetTrap (pu4ErrorCode, pTestValFsMIStdOspfSetTrap);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfOpaqueTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfOpaqueTable
 Input       :  The Indices
                FsMIOspfOpaqueContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfOpaqueTable (INT4 i4FsMIOspfOpaqueContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIOspfOpaqueContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIOspfOpaqueContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfOpaqueContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfOpaqueContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfOpaqueOption
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                testValFsMIOspfOpaqueOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfOpaqueOption (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIOspfOpaqueContextId,
                               INT4 i4TestValFsMIOspfOpaqueOption)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfOpaqueOption (pu4ErrorCode,
                                             i4TestValFsMIOspfOpaqueOption);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfAreaIDValid
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                testValFsMIOspfAreaIDValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfAreaIDValid (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIOspfOpaqueContextId,
                              INT4 i4TestValFsMIOspfAreaIDValid)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfAreaIDValid (pu4ErrorCode,
                                            i4TestValFsMIOspfAreaIDValid);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfOpaqueInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfOpaqueInterfaceTable
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfOpaqueInterfaceTable (INT4
                                                      i4FsMIOspfIfContextId,
                                                      UINT4
                                                      u4FsMIOspfIfIpAddress,
                                                      INT4
                                                      i4FsMIOspfAddressLessIf)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfIfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfIfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfIfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfOpaqueInterfaceTable
        (u4FsMIOspfIfIpAddress, i4FsMIOspfAddressLessIf);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfType9LsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfType9LsdbTable
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfType9LsdbTable (INT4
                                                i4FsMIOspfType9LsdbContextId,
                                                UINT4
                                                u4FsMIOspfType9LsdbIfIpAddress,
                                                INT4
                                                i4FsMIOspfType9LsdbOpaqueType,
                                                UINT4 u4FsMIOspfType9LsdbLsid,
                                                UINT4
                                                u4FsMIOspfType9LsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIOspfType9LsdbContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIOspfType9LsdbContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfType9LsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfType9LsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfType9LsdbTable
        (u4FsMIOspfType9LsdbIfIpAddress, i4FsMIOspfType9LsdbOpaqueType,
         u4FsMIOspfType9LsdbLsid, u4FsMIOspfType9LsdbRouterId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfType11LsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfType11LsdbTable
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfType11LsdbTable (INT4
                                                 i4FsMIOspfType11LsdbContextId,
                                                 INT4
                                                 i4FsMIOspfType11LsdbOpaqueType,
                                                 UINT4 u4FsMIOspfType11LsdbLsid,
                                                 UINT4
                                                 u4FsMIOspfType11LsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIOspfType11LsdbContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIOspfType11LsdbContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfType11LsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfType11LsdbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfType11LsdbContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfType11LsdbTable
        (i4FsMIOspfType11LsdbOpaqueType, u4FsMIOspfType11LsdbLsid,
         u4FsMIOspfType11LsdbRouterId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfAppInfoDbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfAppInfoDbTable
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfAppInfoDbTable (INT4
                                                i4FsMIOspfAppInfoDbContextId,
                                                INT4 i4FsMIOspfAppInfoDbAppid)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIOspfAppInfoDbContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIOspfAppInfoDbContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfAppInfoDbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfAppInfoDbContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfAppInfoDbTable
        (i4FsMIOspfAppInfoDbAppid);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfRRDRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfRRDRouteTable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfRRDRouteTable (INT4 i4FsMIOspfRRDRouteContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIOspfRRDRouteContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIOspfRRDRouteContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfRRDRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfRRDRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDStatus
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                testValFsMIOspfRRDStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIOspfRRDRouteContextId,
                            INT4 i4TestValFsMIOspfRRDStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfRRDStatus (pu4ErrorCode, i4TestValFsMIOspfRRDStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDSrcProtoMaskEnable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                testValFsMIOspfRRDSrcProtoMaskEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDSrcProtoMaskEnable (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIOspfRRDRouteContextId,
                                        INT4
                                        i4TestValFsMIOspfRRDSrcProtoMaskEnable)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDSrcProtoMaskEnable (pu4ErrorCode,
                                                      i4TestValFsMIOspfRRDSrcProtoMaskEnable);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDSrcProtoMaskDisable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                testValFsMIOspfRRDSrcProtoMaskDisable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDSrcProtoMaskDisable (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIOspfRRDRouteContextId,
                                         INT4
                                         i4TestValFsMIOspfRRDSrcProtoMaskDisable)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDSrcProtoMaskDisable (pu4ErrorCode,
                                                       i4TestValFsMIOspfRRDSrcProtoMaskDisable);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDRouteMapEnable
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfRRDRouteMapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDRouteMapEnable (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfContextId,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pTestValFsMIOspfRRDRouteMapEnable)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDRouteMapEnable
        (pu4ErrorCode, pTestValFsMIOspfRRDRouteMapEnable);

    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfRRDRouteConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfRRDRouteConfigTable (INT4
                                                     i4FsMIOspfRRDRouteConfigContextId,
                                                     UINT4
                                                     u4FsMIOspfRRDRouteDest,
                                                     UINT4
                                                     u4FsMIOspfRRDRouteMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfRRDRouteConfigContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfRRDRouteConfigContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfRRDRouteConfigContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfRRDRouteConfigContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfRRDRouteConfigTable
        (u4FsMIOspfRRDRouteDest, u4FsMIOspfRRDRouteMask);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDRouteMetric
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                testValFsMIOspfRRDRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDRouteMetric (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIOspfRRDRouteConfigContextId,
                                 UINT4 u4FsMIOspfRRDRouteDest,
                                 UINT4 u4FsMIOspfRRDRouteMask,
                                 INT4 i4TestValFsMIOspfRRDRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDRouteMetric (pu4ErrorCode,
                                               u4FsMIOspfRRDRouteDest,
                                               u4FsMIOspfRRDRouteMask,
                                               i4TestValFsMIOspfRRDRouteMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDRouteMetricType
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                testValFsMIOspfRRDRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDRouteMetricType (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIOspfRRDRouteConfigContextId,
                                     UINT4 u4FsMIOspfRRDRouteDest,
                                     UINT4 u4FsMIOspfRRDRouteMask,
                                     INT4 i4TestValFsMIOspfRRDRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDRouteMetricType (pu4ErrorCode,
                                                   u4FsMIOspfRRDRouteDest,
                                                   u4FsMIOspfRRDRouteMask,
                                                   i4TestValFsMIOspfRRDRouteMetricType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDRouteTagType
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                testValFsMIOspfRRDRouteTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDRouteTagType (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIOspfRRDRouteConfigContextId,
                                  UINT4 u4FsMIOspfRRDRouteDest,
                                  UINT4 u4FsMIOspfRRDRouteMask,
                                  INT4 i4TestValFsMIOspfRRDRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDRouteTagType (pu4ErrorCode,
                                                u4FsMIOspfRRDRouteDest,
                                                u4FsMIOspfRRDRouteMask,
                                                i4TestValFsMIOspfRRDRouteTagType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDRouteTag
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                testValFsMIOspfRRDRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDRouteTag (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIOspfRRDRouteConfigContextId,
                              UINT4 u4FsMIOspfRRDRouteDest,
                              UINT4 u4FsMIOspfRRDRouteMask,
                              UINT4 u4TestValFsMIOspfRRDRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDRouteTag (pu4ErrorCode,
                                            u4FsMIOspfRRDRouteDest,
                                            u4FsMIOspfRRDRouteMask,
                                            u4TestValFsMIOspfRRDRouteTag);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfRRDRouteStatus
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                testValFsMIOspfRRDRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfRRDRouteStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIOspfRRDRouteConfigContextId,
                                 UINT4 u4FsMIOspfRRDRouteDest,
                                 UINT4 u4FsMIOspfRRDRouteMask,
                                 INT4 i4TestValFsMIOspfRRDRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfRRDRouteStatus (pu4ErrorCode,
                                               u4FsMIOspfRRDRouteDest,
                                               u4FsMIOspfRRDRouteMask,
                                               i4TestValFsMIOspfRRDRouteStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfBRRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfBRRouteTable
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfBRRouteTable (INT4 i4FsMIOspfBRRouteContextId,
                                              UINT4 u4FsMIOspfBRRouteIpAddr,
                                              UINT4 u4FsMIOspfBRRouteIpAddrMask,
                                              UINT4 u4FsMIOspfBRRouteIpTos,
                                              UINT4 u4FsMIOspfBRRouteIpNextHop,
                                              INT4 i4FsMIOspfBRRouteDestType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfBRRouteContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfBRRouteContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfBRRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfBRRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfBRRouteTable
        (u4FsMIOspfBRRouteIpAddr, u4FsMIOspfBRRouteIpAddrMask,
         u4FsMIOspfBRRouteIpTos, u4FsMIOspfBRRouteIpNextHop,
         i4FsMIOspfBRRouteDestType);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfExtRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfExtRouteTable
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfExtRouteTable (INT4 i4FsMIOspfExtRouteContextId,
                                               UINT4 u4FsMIOspfExtRouteDest,
                                               UINT4 u4FsMIOspfExtRouteMask,
                                               INT4 i4FsMIOspfExtRouteTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfExtRouteContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfExtRouteContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIOspfExtRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIOspfExtRouteContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfExtRouteTable
        (u4FsMIOspfExtRouteDest, u4FsMIOspfExtRouteMask, i4FsMIOspfExtRouteTOS);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteMetric
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteMetric (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIOspfExtRouteContextId,
                                 UINT4 u4FsMIOspfExtRouteDest,
                                 UINT4 u4FsMIOspfExtRouteMask,
                                 INT4 i4FsMIOspfExtRouteTOS,
                                 INT4 i4TestValFsMIOspfExtRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfExtRouteMetric (pu4ErrorCode,
                                               u4FsMIOspfExtRouteDest,
                                               u4FsMIOspfExtRouteMask,
                                               i4FsMIOspfExtRouteTOS,
                                               i4TestValFsMIOspfExtRouteMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteMetricType
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteMetricType (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIOspfExtRouteContextId,
                                     UINT4 u4FsMIOspfExtRouteDest,
                                     UINT4 u4FsMIOspfExtRouteMask,
                                     INT4 i4FsMIOspfExtRouteTOS,
                                     INT4 i4TestValFsMIOspfExtRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfExtRouteMetricType (pu4ErrorCode,
                                                   u4FsMIOspfExtRouteDest,
                                                   u4FsMIOspfExtRouteMask,
                                                   i4FsMIOspfExtRouteTOS,
                                                   i4TestValFsMIOspfExtRouteMetricType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteTag
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteTag (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              INT4 i4TestValFsMIOspfExtRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)

    {
        return i1Return;
    }
    i1Return = nmhTestv2FutOspfExtRouteTag (pu4ErrorCode,
                                            u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            i4TestValFsMIOspfExtRouteTag);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteFwdAdr
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteFwdAdr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteFwdAdr (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIOspfExtRouteContextId,
                                 UINT4 u4FsMIOspfExtRouteDest,
                                 UINT4 u4FsMIOspfExtRouteMask,
                                 INT4 i4FsMIOspfExtRouteTOS,
                                 UINT4 u4TestValFsMIOspfExtRouteFwdAdr)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfExtRouteFwdAdr (pu4ErrorCode,
                                               u4FsMIOspfExtRouteDest,
                                               u4FsMIOspfExtRouteMask,
                                               i4FsMIOspfExtRouteTOS,
                                               u4TestValFsMIOspfExtRouteFwdAdr);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteIfIndex
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteIfIndex (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIOspfExtRouteContextId,
                                  UINT4 u4FsMIOspfExtRouteDest,
                                  UINT4 u4FsMIOspfExtRouteMask,
                                  INT4 i4FsMIOspfExtRouteTOS,
                                  INT4 i4TestValFsMIOspfExtRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfExtRouteIfIndex (pu4ErrorCode,
                                                u4FsMIOspfExtRouteDest,
                                                u4FsMIOspfExtRouteMask,
                                                i4FsMIOspfExtRouteTOS,
                                                i4TestValFsMIOspfExtRouteIfIndex);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteNextHop
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteNextHop
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteNextHop (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIOspfExtRouteContextId,
                                  UINT4 u4FsMIOspfExtRouteDest,
                                  UINT4 u4FsMIOspfExtRouteMask,
                                  INT4 i4FsMIOspfExtRouteTOS,
                                  UINT4 u4TestValFsMIOspfExtRouteNextHop)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfExtRouteNextHop (pu4ErrorCode,
                                                u4FsMIOspfExtRouteDest,
                                                u4FsMIOspfExtRouteMask,
                                                i4FsMIOspfExtRouteTOS,
                                                u4TestValFsMIOspfExtRouteNextHop);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfExtRouteStatus
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                testValFsMIOspfExtRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfExtRouteStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIOspfExtRouteContextId,
                                 UINT4 u4FsMIOspfExtRouteDest,
                                 UINT4 u4FsMIOspfExtRouteMask,
                                 INT4 i4FsMIOspfExtRouteTOS,
                                 INT4 i4TestValFsMIOspfExtRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfExtRouteStatus (pu4ErrorCode,
                                               u4FsMIOspfExtRouteDest,
                                               u4FsMIOspfExtRouteMask,
                                               i4FsMIOspfExtRouteTOS,
                                               i4TestValFsMIOspfExtRouteStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfVirtNbrTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfVirtNbrTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtNbrArea
                FsMIOspfVirtNbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMIOspfVirtNbrTable
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIStdOspfVirtNbrArea, UINT4 u4FsMIStdOspfVirtNbrRtrId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfVirtNbrTable
        (u4FsMIStdOspfVirtNbrArea, u4FsMIStdOspfVirtNbrRtrId);
    UtilOspfResetContext ();
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfGrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfGrTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfGrTable (INT4 i4FsMIStdOspfContextId)
{
    return nmhValidateIndexInstanceFsMIStdOspfTable (i4FsMIStdOspfContextId);
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfGrShutdown
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                testValFsMIOspfGrShutdown
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfGrShutdown (UINT4 *pu4ErrorCode,
                             INT4 i4FsMIStdOspfContextId,
                             INT4 i4TestValFsMIOspfGrShutdown)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfGrShutdown (pu4ErrorCode, i4TestValFsMIOspfGrShutdown);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfDistInOutRouteMapTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfDistInOutRouteMapTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfDistInOutRouteMapTable (INT4
                                                        i4FsMIStdOspfContextId,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsMIOspfDistInOutRouteMapName,
                                                        INT4
                                                        i4FsMIOspfDistInOutRouteMapType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIStdOspfContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfDistInOutRouteMapTable
        (pFsMIOspfDistInOutRouteMapName, i4FsMIOspfDistInOutRouteMapType);
    UtilOspfResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfDistInOutRouteMapValue
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType

                The Object
                testValFsMIOspfDistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfDistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIOspfDistInOutRouteMapName,
                                         INT4 i4FsMIOspfDistInOutRouteMapType,
                                         INT4
                                         i4TestValFsMIOspfDistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfDistInOutRouteMapValue (pu4ErrorCode,
                                                       pFsMIOspfDistInOutRouteMapName,
                                                       i4FsMIOspfDistInOutRouteMapType,
                                                       i4TestValFsMIOspfDistInOutRouteMapValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType

                The Object
                testValFsMIOspfDistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfDistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfContextId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIOspfDistInOutRouteMapName,
                                             INT4
                                             i4FsMIOspfDistInOutRouteMapType,
                                             INT4
                                             i4TestValFsMIOspfDistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfDistInOutRouteMapRowStatus (pu4ErrorCode,
                                                           pFsMIOspfDistInOutRouteMapName,
                                                           i4FsMIOspfDistInOutRouteMapType,
                                                           i4TestValFsMIOspfDistInOutRouteMapRowStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfPreferenceTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfPreferenceTable (INT4 i4FsMIStdOspfContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4FsMIStdOspfContextId < OSPF_DEFAULT_CXT_ID)
        || (i4FsMIStdOspfContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfPreferenceValue
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                testValFsMIOspfPreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfPreferenceValue (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfContextId,
                                  INT4 i4TestValFsMIOspfPreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfPreferenceValue
        (pu4ErrorCode, i4TestValFsMIOspfPreferenceValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfIfAuthTable
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfIfAuthTable (INT4
                                             i4FsMIOspfIfAuthContextId,
                                             UINT4
                                             u4FsMIOspfIfAuthIpAddress,
                                             INT4
                                             i4FsMIOspfIfAuthAddressLessIf,
                                             INT4 i4FsMIOspfIfAuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfIfAuthContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfIfAuthContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist ((UINT4) i4FsMIOspfIfAuthContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId ((UINT4) i4FsMIOspfIfAuthContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfIfAuthTable
        (u4FsMIOspfIfAuthIpAddress, i4FsMIOspfIfAuthAddressLessIf,
         i4FsMIOspfIfAuthKeyId);

    UtilOspfResetContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfAuthKey
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                testValFsMIOspfIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfAuthKey (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIOspfIfAuthContextId,
                            UINT4 u4FsMIOspfIfAuthIpAddress,
                            INT4 i4FsMIOspfIfAuthAddressLessIf,
                            INT4 i4FsMIOspfIfAuthKeyId,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsMIOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfAuthKey (pu4ErrorCode,
                                          u4FsMIOspfIfAuthIpAddress,
                                          i4FsMIOspfIfAuthAddressLessIf,
                                          i4FsMIOspfIfAuthKeyId,
                                          pTestValFsMIOspfIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                testValFsMIOspfIfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIOspfIfAuthContextId,
                                       UINT4 u4FsMIOspfIfAuthIpAddress,
                                       INT4 i4FsMIOspfIfAuthAddressLessIf,
                                       INT4 i4FsMIOspfIfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsMIOspfIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfAuthKeyStartAccept (pu4ErrorCode,
                                                     u4FsMIOspfIfAuthIpAddress,
                                                     i4FsMIOspfIfAuthAddressLessIf,
                                                     i4FsMIOspfIfAuthKeyId,
                                                     pTestValFsMIOspfIfAuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                testValFsMIOspfIfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIOspfIfAuthContextId,
                                         UINT4 u4FsMIOspfIfAuthIpAddress,
                                         INT4
                                         i4FsMIOspfIfAuthAddressLessIf,
                                         INT4 i4FsMIOspfIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsMIOspfIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfAuthKeyStartGenerate (pu4ErrorCode,
                                                       u4FsMIOspfIfAuthIpAddress,
                                                       i4FsMIOspfIfAuthAddressLessIf,
                                                       i4FsMIOspfIfAuthKeyId,
                                                       pTestValFsMIOspfIfAuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                testValFsMIOspfIfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIOspfIfAuthContextId,
                                        UINT4 u4FsMIOspfIfAuthIpAddress,
                                        INT4
                                        i4FsMIOspfIfAuthAddressLessIf,
                                        INT4 i4FsMIOspfIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsMIOspfIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfAuthKeyStopGenerate (pu4ErrorCode,
                                                      u4FsMIOspfIfAuthIpAddress,
                                                      i4FsMIOspfIfAuthAddressLessIf,
                                                      i4FsMIOspfIfAuthKeyId,
                                                      pTestValFsMIOspfIfAuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                testValFsMIOspfIfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIOspfIfAuthContextId,
                                      UINT4 u4FsMIOspfIfAuthIpAddress,
                                      INT4 i4FsMIOspfIfAuthAddressLessIf,
                                      INT4 i4FsMIOspfIfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMIOspfIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfAuthKeyStopAccept (pu4ErrorCode,
                                                    u4FsMIOspfIfAuthIpAddress,
                                                    i4FsMIOspfIfAuthAddressLessIf,
                                                    i4FsMIOspfIfAuthKeyId,
                                                    pTestValFsMIOspfIfAuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfIfAuthKeyStatus
 Input       :  The Indices
                FsMIOspfIfAuthContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                testValFsMIOspfIfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfIfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIOspfIfAuthContextId,
                                  UINT4 u4FsMIOspfIfAuthIpAddress,
                                  INT4 i4FsMIOspfIfAuthAddressLessIf,
                                  INT4 i4FsMIOspfIfAuthKeyId,
                                  INT4 i4TestValFsMIOspfIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIOspfIfAuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfIfAuthKeyStatus (pu4ErrorCode,
                                                u4FsMIOspfIfAuthIpAddress,
                                                i4FsMIOspfIfAuthAddressLessIf,
                                                i4FsMIOspfIfAuthKeyId,
                                                i4TestValFsMIOspfIfAuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                testValFsMIOspfVirtIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfAuthKey (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfContextId,
                                UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                INT4 i4FsMIOspfVirtIfAuthKeyId,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsMIOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfAuthKey (pu4ErrorCode,
                                              u4FsMIOspfVirtIfAuthAreaId,
                                              u4FsMIOspfVirtIfAuthNeighbor,
                                              i4FsMIOspfVirtIfAuthKeyId,
                                              pTestValFsMIOspfVirtIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                testValFsMIOspfVirtIfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfContextId,
                                           UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                           UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                           INT4 i4FsMIOspfVirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFsMIOspfVirtIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfAuthKeyStartAccept (pu4ErrorCode,
                                                         u4FsMIOspfVirtIfAuthAreaId,
                                                         u4FsMIOspfVirtIfAuthNeighbor,
                                                         i4FsMIOspfVirtIfAuthKeyId,
                                                         pTestValFsMIOspfVirtIfAuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                pTestValFsMIOspfVirtIfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfContextId,
                                             UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                             UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                             INT4 i4FsMIOspfVirtIfAuthKeyId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFsMIOspfVirtIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfAuthKeyStartGenerate (pu4ErrorCode,
                                                           u4FsMIOspfVirtIfAuthAreaId,
                                                           u4FsMIOspfVirtIfAuthNeighbor,
                                                           i4FsMIOspfVirtIfAuthKeyId,
                                                           pTestValFsMIOspfVirtIfAuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                testValFsMIOspfVirtIfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIStdOspfContextId,
                                            UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                            UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                            INT4 i4FsMIOspfVirtIfAuthKeyId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValFsMIOspfVirtIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfAuthKeyStopGenerate (pu4ErrorCode,
                                                          u4FsMIOspfVirtIfAuthAreaId,
                                                          u4FsMIOspfVirtIfAuthNeighbor,
                                                          i4FsMIOspfVirtIfAuthKeyId,
                                                          pTestValFsMIOspfVirtIfAuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                testValFsMIOspfVirtIfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfContextId,
                                          UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                          UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                          INT4 i4FsMIOspfVirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsMIOspfVirtIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfAuthKeyStopAccept (pu4ErrorCode,
                                                        u4FsMIOspfVirtIfAuthAreaId,
                                                        u4FsMIOspfVirtIfAuthNeighbor,
                                                        i4FsMIOspfVirtIfAuthKeyId,
                                                        pTestValFsMIOspfVirtIfAuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfVirtIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                testValFsMIOspfVirtIfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfVirtIfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfContextId,
                                      UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                      UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                      INT4 i4FsMIOspfVirtIfAuthKeyId,
                                      INT4 i4TestValFsMIOspfVirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfVirtIfAuthKeyStatus (pu4ErrorCode,
                                                    u4FsMIOspfVirtIfAuthAreaId,
                                                    u4FsMIOspfVirtIfAuthNeighbor,
                                                    i4FsMIOspfVirtIfAuthKeyId,
                                                    i4TestValFsMIOspfVirtIfAuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfRRDMetricTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfRRDMetricTable (INT4 i4FsMIStdOspfContextId,
                                                INT4 i4FsMIOspfRRDProtocolId)
{
    if (nmhValidateIndexInstanceFsMIStdOspfTable (i4FsMIStdOspfContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsMIOspfRRDProtocolId <= 0) || (i4FsMIOspfRRDProtocolId >
                                           MAX_PROTO_REDISTRUTE_SIZE))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

INT1
nmhValidateIndexInstanceFsMIOspfVirtIfAuthTable (INT4
                                                 i4FsMIOspfVirtIfAuthContextId,
                                                 UINT4
                                                 u4FsMIOspfVirtIfAuthAreaId,
                                                 UINT4
                                                 u4FsMIOspfVirtIfAuthNeighbor,
                                                 INT4 i4FsMIOspfVirtIfAuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIOspfVirtIfAuthContextId < OSPF_DEFAULT_CXT_ID) ||
        (i4FsMIOspfVirtIfAuthContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfVcmIsVcExist ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfIsValidCxtId ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfVirtIfAuthTable
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId);

    UtilOspfResetContext ();
    return i1RetVal;
}
