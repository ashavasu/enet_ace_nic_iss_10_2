/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oshp.c,v 1.23 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             Hello protocol.
 *
 *******************************************************************/

#include "osinc.h"
/* Proto types of the functions private to this file only */
PRIVATE VOID        HpSendHelloNbma
PROTO ((tCRU_BUF_CHAIN_HEADER * pHelloPkt,
        UINT2 u2PktLen, tInterface * pInterface, UINT1 u1TimerId));
PRIVATE VOID        HpSendHelloToAllNbr
PROTO ((tCRU_BUF_CHAIN_HEADER * pHelloPkt,
        UINT2 u2PktLen, tInterface * pInterface, UINT1 u1TimerId));
PRIVATE VOID        HpSendHelloToEligibleNbr
PROTO ((tCRU_BUF_CHAIN_HEADER * pHelloPkt,
        UINT2 u2PktLen, tInterface * pInterface, UINT1 u1TimerId));
PRIVATE UINT2       HpConstructHello
PROTO ((tInterface * pInterface, tCRU_BUF_CHAIN_HEADER * pHelloPkt));
PRIVATE VOID HpStartIneligibleNbr PROTO ((tInterface * pInterface));
PRIVATE VOID HpCheckAllAdj PROTO ((tInterface * pInterface));
PRIVATE VOID HpElectBdrFromLst PROTO ((tInterface * pInterface));
PRIVATE VOID HpElectDrFromLst PROTO ((tInterface * pInterface));
PRIVATE UINT1       HpCheckRtrInRcvdHelloInCxt
PROTO ((tRouterId rtrId, tCRU_BUF_CHAIN_HEADER * pHelloPkt, UINT2 u2PktLen));
static tNeighbor    TempNbr;

/*****************************************************************************/
/*                                                                           */
/* Function     : HpSendHello                                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.5                           */
/*                This procedure  constructs and sends a hello on the        */
/*                specified interface. If the pNbr field is valid then hello*/
/*                packet is sent to that neighbor alone. Otherwise hellos are*/
/*                sent to all neighbors on the interface.  If the timer_id is*/
/*                HELLO_TIMER it indicates that the hello timer has fired.   */
/*                Then hello packets are sent to neighbors in state other    */
/*                than down. If the field is POLL_TIMER then it indicates    */
/*                that  the poll timer has fired. In that case hello packets */
/*                are sent to neighbors in state down only.                  */
/*                                                                           */
/* Input        : pInterface        : the interface on which hello is to be */
/*                                      sent                                 */
/*                pNbr              : non-null if hello is to be sent to    */
/*                                      single nbr                           */
/*                u1TimerId        : value is HELLO_TIMER or POLL_TIMER    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HpSendHello (tInterface * pInterface, tNeighbor * pNbr, UINT1 u1TimerId)
{

    tCRU_BUF_CHAIN_HEADER *pHelloPkt;
    UINT2               u2PktLen;
    UINT1               u1HelloTxed;
    tTMO_SLL_NODE      *pNbrNode;
    tIPADDR             destAddr;

    OSPF_TRC2 (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "FUNC: HpSendHello: Hello tobe sent on iface addr : %x,"
               " addressid : %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    if ((pHelloPkt = UtilOsMsgAlloc (IFACE_MTU (pInterface))) == NULL)
    {

        OSPF_TRC2 (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Alloc Failed For TXmission Of HP If %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u4AddrlessIf);

        return;
    }
    u2PktLen = HpConstructHello (pInterface, pHelloPkt);
    u1HelloTxed = OSPF_FALSE;

    if (pNbr != ((tNeighbor *) 0))
    {

        OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP To Be Sent To Single Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

        IP_ADDR_COPY (destAddr, pNbr->nbrIpAddr);

    }
    else if (pInterface->u1NetworkType == IF_NBMA)
    {

        OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "HP To Be Sent Over NBMA\n");

        HpSendHelloNbma (pHelloPkt, u2PktLen, pInterface, u1TimerId);
        u1HelloTxed = OSPF_TRUE;
    }
    else if (pInterface->u1NetworkType == IF_BROADCAST)
    {

        OSPF_TRC1 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP To Be Sent To MCast Addr %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (gAllSpfRtrs));

        IP_ADDR_COPY (destAddr, gAllSpfRtrs);
    }
    else if (pInterface->u1NetworkType == IF_VIRTUAL)
    {

        pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
        if (pNbrNode == NULL)
        {
            if (u1TimerId == HELLO_TIMER)
            {
                u1HelloTxed = OSPF_FALSE;
            }
        }
        else
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

            OSPF_TRC1 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "HP To Be Sent To Virt Nbr %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));

            IP_ADDR_COPY (destAddr, pNbr->nbrIpAddr);
        }
    }
    else if (pInterface->u1NetworkType == IF_PTOP)
    {

        if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_IF (pInterface))
        {
            pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
            if (pNbrNode == NULL)
            {
                if (u1TimerId == HELLO_TIMER)
                {
                    u1HelloTxed = OSPF_FALSE;
                }
            }
            else
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if (((pNbr->u1NsmState > NBRS_DOWN) &&
                     (u1TimerId == HELLO_TIMER)) ||
                    ((pNbr->u1NsmState == NBRS_DOWN) &&
                     (u1TimerId == POLL_TIMER)))
                {
                    u1HelloTxed = OSPF_FALSE;
                }
                else
                {
                    u1HelloTxed = OSPF_TRUE;
                }
            }
        }
    }
    else if (pInterface->u1NetworkType == IF_PTOMP)
    {
        HpSendHelloToAllNbr (pHelloPkt, u2PktLen, pInterface, u1TimerId);
        u1HelloTxed = OSPF_TRUE;
    }

    if (u1HelloTxed == OSPF_FALSE)
    {
        PppSendPkt (pHelloPkt, u2PktLen, pInterface, &destAddr, NO_RELEASE);
    }

    /* Freeing the memory allocated for Hello Packet */
    UtilOsMsgFree (pHelloPkt, NORMAL_RELEASE);

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId, "HP TXmitted\n");
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpSendHello\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpSendHelloNbma                                         */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.5.1                         */
/*                This procedure takes care of sending hello packets on NBMA */
/*                networks.                                                  */
/*                                                                           */
/* Input        : pHelloPkt         : the hello pkt to be sent             */
/*                u2PktLen          : length of the pkt                    */
/*                pInterface         : the interface to NBMA                */
/*                u1TimerId         : value is HELLO_TIMER or POLL_TIMER   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HpSendHelloNbma (tCRU_BUF_CHAIN_HEADER * pHelloPkt,
                 UINT2 u2PktLen, tInterface * pInterface, UINT1 u1TimerId)
{

    OSPF_TRC1 (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "FUNC: HpSendHelloNbma: HP To Be Sent On NBMA If %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    if (pInterface->u1IsmState >= IFS_WAITING)
    {

        if ((pInterface->u1IsmState == IFS_DR) ||
            (pInterface->u1IsmState == IFS_BACKUP))
        {
            HpSendHelloToAllNbr (pHelloPkt, u2PktLen, pInterface, u1TimerId);
        }
        else if (pInterface->u1RtrPriority > INELIGIBLE_RTR_PRIORITY)
        {
            HpSendHelloToEligibleNbr (pHelloPkt, u2PktLen, pInterface,
                                      u1TimerId);
        }
        else
        {

            OSPF_TRC1 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "HP To Be Sent To DR/BDR Alone If %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

            if (!(IS_NULL_IP_ADDR (pInterface->desgRtr)))
            {
                PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                            &(pInterface->desgRtr), NO_RELEASE);
            }
            if (!(IS_NULL_IP_ADDR (pInterface->backupDesgRtr)))
            {
                PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                            &(pInterface->backupDesgRtr), NO_RELEASE);
            }
        }
    }

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "HP TXmitted On NBMA\n");
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpSendHelloNbma\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpSendHelloToAllNbr                                   */
/*                                                                           */
/* Description  : Transmits the hello packet to all neighbors on the speci-  */
/*                fied interface.                                            */
/*                                                                           */
/* Input        : pHelloPkt         : the hello pkt to be sent             */
/*                u2PktLen          : length of the pkt                    */
/*                pInterface         : the interface on which hello is to be*/
/*                                      sent                                 */
/*                u1TimerId         : value is HELLO_TIMER or POLL_TIMER   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
HpSendHelloToAllNbr (tCRU_BUF_CHAIN_HEADER * pHelloPkt,
                     UINT2 u2PktLen, tInterface * pInterface, UINT1 u1TimerId)
{

    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpSendHelloToAllNbr\n");
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (((pNbr->u1NsmState > NBRS_DOWN) &&
             (u1TimerId == HELLO_TIMER)) ||
            ((pNbr->u1NsmState == NBRS_DOWN) &&
             (u1TimerId == POLL_TIMER)) ||
            ((pInterface->u1NetworkType == IF_PTOMP) &&
             (u1TimerId == HELLO_TIMER)))
        {
            PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                        &(pNbr->nbrIpAddr), NO_RELEASE);
        }
    }

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "HP TXmitted To All Nbrs\n");
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpSendHelloToAllNbr\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpSendHelloToEligibleNbr                              */
/*                                                                           */
/* Description  : Transmits the hello packet to all neighbors eligible to    */
/*                become DR or BDR on this interface.                        */
/*                                                                           */
/* Input        : pHelloPkt         : the hello pkt to be sent             */
/*                u2PktLen          : length of the pkt                    */
/*                pInterface         : the interface on which hello is to be*/
/*                                      sent                                 */
/*                u1TimerId         : value is HELLO_TIMER or POLL_TIMER   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HpSendHelloToEligibleNbr (tCRU_BUF_CHAIN_HEADER * pHelloPkt,
                          UINT2 u2PktLen,
                          tInterface * pInterface, UINT1 u1TimerId)
{

    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpSendHelloToEligibleNbr\n");
    OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "HP To Be Sent To Eligible Nbrs On NBMA If %x TmrId %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr), u1TimerId);

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->u1NbrRtrPriority > INELIGIBLE_RTR_PRIORITY)
        {

            if (((pNbr->u1NsmState > NBRS_DOWN) &&
                 (u1TimerId == HELLO_TIMER)) ||
                ((pNbr->u1NsmState == NBRS_DOWN) && (u1TimerId == POLL_TIMER)))
            {
                PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                            &(pNbr->nbrIpAddr), NO_RELEASE);
            }
        }
    }
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpSendHelloToEligibleNbr\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpConstructHello                                         */
/*                                                                           */
/* Description  : Reference : RFC-2178 section A.3.2                         */
/*                This procedure constructs the hello packet to be transa-   */
/*                mitted on the specified interface.                         */
/*                                                                           */
/* Input        : pInterface        : the interface on which hello is to be */
/*                                     sent                                  */
/*                pHelloPkt        : the buffer in which hello is to be    */
/*                                     constructed                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : packet length                                              */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT2
HpConstructHello (tInterface * pInterface, tCRU_BUF_CHAIN_HEADER * pHelloPkt)
{

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    UINT2               nbrCount;
    UINT2               u2PktLen;
    tOPTIONS            options;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpConstructHello\n");

    OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Hello to be consorted for transmission on iface : %x,"
               " addrless_if : %d\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    OSPF_CRU_BMC_ASSIGN_STRING (pHelloPkt, pInterface->ifIpAddrMask,
                                OS_HEADER_SIZE, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pHelloPkt, HP_HELLO_INTERVAL_OFFSET,
                                pInterface->u2HelloInterval);

    options =
        pInterface->pArea->pOspfCxt->rtrOptions | pInterface->pArea->
        areaOptions;

    if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pInterface))
    {
        options = options | pInterface->ifOptions;
    }
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pHelloPkt, HP_OPTIONS_OFFSET, options);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pHelloPkt, HP_RTR_PRI_OFFSET,
                                pInterface->u1RtrPriority);
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pHelloPkt, HP_RTR_DEAD_INTERVAL_OFFSET,
                                pInterface->i4RtrDeadInterval);
    OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pHelloPkt, pInterface->desgRtr,
                                       HP_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pHelloPkt, pInterface->backupDesgRtr,
                                       HP_BK_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);

    nbrCount = 0;
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->u1NsmState >= NBRS_INIT)
        {
            OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pHelloPkt, pNbr->nbrId,
                                               (UINT4) (HP_NBR_ID_OFFSET +
                                                        (4 * nbrCount)),
                                               (UINT4) MAX_IP_ADDR_LEN);
            nbrCount++;
        }
    }
    u2PktLen = (UINT2) (OS_HEADER_SIZE +
                        HELLO_FIXED_PORTION_SIZE +
                        nbrCount * (sizeof (tRouterId)));
    UtilConstructHdr (pInterface, pHelloPkt, HELLO_PKT, u2PktLen);

    OSPF_TRC (OSPF_HP_TRC | CONTROL_PLANE_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId, "Hello constructed\n");
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpConstructHello\n");

    return ((UINT2) (u2PktLen + DIGEST_LEN_IF_CRYPT (pInterface)));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpSearchNbrLst                                          */
/*                                                                           */
/* Description  : The list of neighbor structures is searched for a matching */
/*                entry. If the interface is multiaccess the search is based */
/*                on ipAddr field. If the interface is point to point or    */
/*                vitrual link then the search is based on rtrId.           */
/*                                                                           */
/* Input        : pIpAddr          : the ip addr of the nbr to be searched */
/*                pInterface        : the interface whose nbr list is       */
/*                                     searched                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to matching neighbour structure, if found          */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tNeighbor   *
HpSearchNbrLst (tIPADDR * pIpAddr, tInterface * pInterface)
{
    tNeighbor          *pNbr;

    MEMSET (&TempNbr, 0, sizeof (tNeighbor));
    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpSearchNbrLst\n");
    OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Search for nbr with addr : %x on interface : %x\n",
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pIpAddr),
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    if ((pInterface->u1NetworkType == IF_BROADCAST) ||
        (pInterface->u1NetworkType == IF_NBMA) ||
        (pInterface->u1NetworkType == IF_PTOMP))
    {
        IP_ADDR_COPY (TempNbr.nbrIpAddr, *pIpAddr);
    }
    else
    {
        IP_ADDR_COPY (TempNbr.nbrId, *pIpAddr);
    }
    TempNbr.pInterface = pInterface;

    pNbr = RBTreeGet (pInterface->pArea->pNbrTbl, (tRBElem *) & TempNbr);
    return (pNbr);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpElectDesgRtr                                          */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.4                           */
/*                This procedure elects a designated router and backup       */
/*                designated router for the specified multi-access network.  */
/*                                                                           */
/* Input        : pInterface        : the interface for which DR is to be   */
/*                                     elected                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HpElectDesgRtr (tInterface * pInterface)
{

    tIPADDR             prevDr;
    tIPADDR             prevBdr;
    UINT1               u1NewlyDr = 0;
    UINT1               u1NewlyBdr = 0;
    UINT1               u1NoLongerDr = 0;
    UINT1               u1NoLongerBdr = 0;
    UINT1               u1PrevIfState;
    tLsaInfo           *pLsaInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpElectDesgRtr\n");

    OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "DR/BDR Election If %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);

    if ((pInterface->pArea->pOspfCxt->u1RestartExitReason
         == OSPF_RESTART_INPROGRESS)
        && (pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART))
    {
        OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "DR/BDR Election Over If %x.%d\n, Router/Network Lsa not"
                   "modified- Router is in GR State",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u4IfIndex);

        HpCheckAllAdj (pInterface);

        OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: HpElectDesgRtr\n");
        return;
    }

    /* save prev DR and BDR value. (RFC-2178, section 9.4 Step 1) */

    IP_ADDR_COPY (prevDr, GET_DR (pInterface));
    IP_ADDR_COPY (prevBdr, GET_BDR (pInterface));
    u1PrevIfState = pInterface->u1IsmState;

    /* elect DR and BDR (RFC-2178, section 9.4 Step 2 and 3) */
    HpElectBdrFromLst (pInterface);
    HpElectDrFromLst (pInterface);

    if ((u1PrevIfState == IFS_DR) &&
        (UtilIpAddrComp (GET_DR (pInterface),
                         pInterface->ifIpAddr) != OSPF_EQUAL))
    {
        u1NoLongerDr = 1;
    }
    if ((u1PrevIfState != IFS_DR) &&
        (UtilIpAddrComp (GET_DR (pInterface),
                         pInterface->ifIpAddr) == OSPF_EQUAL))
    {
        u1NewlyDr = 1;
    }
    if ((u1PrevIfState == IFS_BACKUP) &&
        (UtilIpAddrComp (GET_BDR (pInterface),
                         pInterface->ifIpAddr) != OSPF_EQUAL))
    {
        u1NoLongerBdr = 1;
    }
    if ((u1PrevIfState != IFS_BACKUP) &&
        (UtilIpAddrComp (GET_BDR (pInterface),
                         pInterface->ifIpAddr) == OSPF_EQUAL))
    {
        u1NewlyBdr = 1;
    }

    if (u1NewlyDr || u1NewlyBdr || u1NoLongerDr || u1NoLongerBdr)
    {

        u1NewlyDr = u1NewlyBdr = 0;
        HpElectBdrFromLst (pInterface);
        HpElectDrFromLst (pInterface);
        if ((u1PrevIfState != IFS_DR) &&
            (UtilIpAddrComp (GET_DR (pInterface),
                             pInterface->ifIpAddr) == OSPF_EQUAL))
        {
            u1NewlyDr = 1;
        }
        if ((u1PrevIfState != IFS_BACKUP) &&
            (UtilIpAddrComp (GET_BDR (pInterface),
                             pInterface->ifIpAddr) == OSPF_EQUAL))
        {
            u1NewlyBdr = 1;
        }
    }

    if (UtilIpAddrComp (GET_DR (pInterface), pInterface->ifIpAddr) ==
        OSPF_EQUAL)
    {
        IfUpdateState (pInterface, IFS_DR);
    }
    else if (UtilIpAddrComp (GET_BDR (pInterface), pInterface->ifIpAddr) ==
             OSPF_EQUAL)
    {
        IfUpdateState (pInterface, IFS_BACKUP);
    }
    else
    {
        IfUpdateState (pInterface, IFS_DR_OTHER);
    }

    if ((pInterface->u1NetworkType == IF_NBMA) && (u1NewlyDr || u1NewlyBdr))
    {
        HpStartIneligibleNbr (pInterface);
    }

    HpCheckAllAdj (pInterface);

    if ((UtilIpAddrComp (prevDr, GET_DR (pInterface)) != OSPF_EQUAL))
    {
        if ((pInterface->u4NbrFullCount > 0) && (u1PrevIfState != IFS_WAITING))
        {
            OlsSignalLsaRegenInCxt (pInterface->pArea->pOspfCxt,
                                    SIG_DR_CHANGE, (UINT1 *) pInterface);
        }
        if (pInterface->pArea->pOspfCxt->u1RtrNetworkLsaChanged == OSPF_TRUE)
        {
            TmrStopTimer (gTimerLst,
                          &(pInterface->pArea->pOspfCxt->runRtTimer.timerNode));
        }
        pInterface->pArea->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
        RtcSetRtTimer (pInterface->pArea->pOspfCxt);
    }

    if ((u1PrevIfState == IFS_DR) && (pInterface->u1IsmState != IFS_DR))
    {

        pLsaInfo = LsuSearchDatabase (NETWORK_LSA,
                                      &(pInterface->ifIpAddr),
                                      &(pInterface->pArea->pOspfCxt->rtrId),
                                      (UINT1 *) NULL,
                                      (UINT1 *) pInterface->pArea);

        if (pLsaInfo != NULL)
        {
            LsuFlushLsaAfterRemovingFromDescLst (pLsaInfo);
        }
    }

    OSPF_TRC2 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "DR/BDR Election Over If %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpElectDesgRtr\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpStartIneligibleNbr                                    */
/*                                                                           */
/* Description  : Generates event start for all neighbors ineligible to      */
/*                become DR.                                                 */
/*                                                                           */
/* Input        : pInterface        : interface on which nbrs are to be     */
/*                                     signalled                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HpStartIneligibleNbr (tInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->u1NbrRtrPriority == INELIGIBLE_RTR_PRIORITY)
        {
            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Eligible neighbors are present with Address = %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_START);
        }
    }

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Nbr Evt Start Gen For All InEligible Nbrs\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpCheckAllAdj                                           */
/*                                                                           */
/* Description  : Generates event ADJOK for all neighbors in state atleast   */
/*                2way.                                                      */
/*                                                                           */
/* Input        : pInterface       : interface on which adjacencies are     */
/*                                    checked                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HpCheckAllAdj (tInterface * pInterface)
{
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->u1NsmState >= NBRS_2WAY)
        {
            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Adjacency is ok for Neighbour with Address = %s\n",
                       OspfPrintIpAddr (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_ADJ_OK);
        }
    }

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Adjacencies Verified\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpElectBdrFromLst                                      */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.4 step 2                    */
/*                Elects a backup designated router from the list of eligible*/
/*                routers.                                                   */
/*                                                                           */
/* Input        : pInterface       : interface on which bdr is to be elected*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HpElectBdrFromLst (tInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    UINT1               u1ChangeFlag;
    /* priority of the current bdr */
    UINT1               u1CurrBdrPriority = 0;
    UINT1               u1CurrBdrStatus = ALL_RTRS;

    /*
     * = 1 if current bdr has advertised itself as bdr.
     * If this field is 0 then current bdr was elected
     * based on router priority alone.
     */

    tIPADDR             newBdr;
    tRouterId           newBdrId;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpElectBdrFromLst\n");

    OSPF_TRC1 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "BDR To Be Elected From Eligible Nbr Lst If %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    /* consider this router */

    SET_NULL_IP_ADDR (newBdr);
    SET_NULL_IP_ADDR (newBdrId);
    if (pInterface->u1RtrPriority != INELIGIBLE_RTR_PRIORITY)
    {
        if (UtilIpAddrComp (pInterface->ifIpAddr,
                            GET_DR (pInterface)) != OSPF_EQUAL)
        {

            IP_ADDR_COPY (newBdr, pInterface->ifIpAddr);
            u1CurrBdrPriority = pInterface->u1RtrPriority;
            IP_ADDR_COPY (newBdrId, pInterface->pArea->pOspfCxt->rtrId);
            if (UtilIpAddrComp (pInterface->ifIpAddr,
                                GET_BDR (pInterface)) == OSPF_EQUAL)
            {
                u1CurrBdrStatus = ONLY_BDRS;
            }
        }
    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

        /* Eligible neighbors are present at the start of the nbr list */

        if (pNbr->u1NbrRtrPriority == INELIGIBLE_RTR_PRIORITY)
        {

            OSPF_TRC (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "InEligible Nbr Priority Encountered\n");

            break;
        }

        /* Only nbrs in state >= NBRS_2WAY are considered */

        if (pNbr->u1NsmState < NBRS_2WAY)
        {

            OSPF_TRC (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Nbr State Less Than 2-Way\n");

            continue;
        }

        if (((gOsRtr.osRedInfo.u4OsRmState == OSPF_RED_STANDBY) &&
             (gOsRtr.osRedInfo.u4DynBulkUpdatStatus ==
              OSPF_RED_BLKUPDT_INPROGRESS)))
        {
            if (UtilIpAddrComp (gNullIpAddr, pNbr->backupDesgRtr) != OSPF_EQUAL)
            {
                IP_ADDR_COPY (newBdr, pNbr->backupDesgRtr);
                break;
            }

        }

        if (UtilIpAddrComp (pNbr->nbrIpAddr, pNbr->desgRtr) == OSPF_EQUAL)
        {

            OSPF_TRC (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Nbr Advertises Itself As DR\n");

            continue;
        }
        u1ChangeFlag = 0;

        /* CONSIDER ONLY bdrs, if u1CurrBdrStatus ON */

        if ((UtilIpAddrComp (pNbr->nbrIpAddr,
                             pNbr->backupDesgRtr) != OSPF_EQUAL) &&
            (u1CurrBdrStatus == ONLY_BDRS))
            continue;

        if ((UtilIpAddrComp (pNbr->nbrIpAddr,
                             pNbr->backupDesgRtr) == OSPF_EQUAL) &&
            (u1CurrBdrStatus != ONLY_BDRS))
        {

            u1CurrBdrStatus = ONLY_BDRS;
            u1ChangeFlag = 1;
        }
        else
        {

            /* decision based on priority */

            if (u1CurrBdrPriority < pNbr->u1NbrRtrPriority)
            {
                u1ChangeFlag = 1;
            }
            else if (u1CurrBdrPriority == pNbr->u1NbrRtrPriority)
            {

                if (UtilIpAddrComp (newBdrId, pNbr->nbrId) == OSPF_LESS)
                {
                    u1ChangeFlag = 1;
                }
            }
        }
        if (u1ChangeFlag)
        {

            IP_ADDR_COPY (newBdr, pNbr->nbrIpAddr);
            u1CurrBdrPriority = pNbr->u1NbrRtrPriority;
            IP_ADDR_COPY (newBdrId, pNbr->nbrId);
        }
    }
    IP_ADDR_COPY (pInterface->backupDesgRtr, newBdr);
    OspfRmSendIfState (pInterface, OSPF_ZERO);

    OSPF_TRC1 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "BDR Election Over new BDR %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->backupDesgRtr));
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpElectBdrFromLst\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     :  HpElectDrFromLst                                       */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.4 step 3                    */
/*                Elects a designated router from the list of eligible       */
/*                routers.                                                   */
/*                                                                           */
/* Input        : pInterface        : interface on which dr is to be elected*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HpElectDrFromLst (tInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    tIPADDR             newDr;
    tRouterId           newDrId;
    UINT1               u1ChangeFlag;
    /* priority of the current dr */
    UINT1               u1CurrDrPriority = 0;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpElectDrFromLst\n");
    OSPF_TRC1 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "DR To Be Elected From Eligible Nbr Lst If %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

    SET_NULL_IP_ADDR (newDr);
    SET_NULL_IP_ADDR (newDrId);
    if (pInterface->u1RtrPriority != INELIGIBLE_RTR_PRIORITY)
    {
        if (UtilIpAddrComp (pInterface->ifIpAddr,
                            GET_DR (pInterface)) == OSPF_EQUAL)
        {

            IP_ADDR_COPY (newDr, pInterface->ifIpAddr);
            u1CurrDrPriority = pInterface->u1RtrPriority;
            IP_ADDR_COPY (newDrId, pInterface->pArea->pOspfCxt->rtrId);
        }
    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

        /* Eligible neighbors are present at the start of the nbr list */
        if (pNbr->u1NbrRtrPriority == INELIGIBLE_RTR_PRIORITY)
        {

            OSPF_TRC (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "InEligible Nbr Priority Encountered\n");

            break;
        }

        /* Only nbrs in state >= NBRS_2WAY are considered */
        if (pNbr->u1NsmState < NBRS_2WAY)
        {

            OSPF_TRC (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Nbr State Less Than 2-Way\n");

            continue;
        }
        u1ChangeFlag = 0;
        if (UtilIpAddrComp (pNbr->nbrIpAddr, pNbr->desgRtr) == OSPF_EQUAL)
        {

            if (u1CurrDrPriority < pNbr->u1NbrRtrPriority)
            {
                u1ChangeFlag = 1;
            }
            else if (u1CurrDrPriority == pNbr->u1NbrRtrPriority)
            {

                if (UtilIpAddrComp (newDrId, pNbr->nbrId) == OSPF_LESS)
                {
                    u1ChangeFlag = 1;
                }
            }
        }
        if (u1ChangeFlag)
        {

            IP_ADDR_COPY (newDr, pNbr->nbrIpAddr);
            IP_ADDR_COPY (newDrId, pNbr->nbrId);
            u1CurrDrPriority = pNbr->u1NbrRtrPriority;
        }
    }
    if (IS_NULL_IP_ADDR (newDr))
    {
        IP_ADDR_COPY (pInterface->desgRtr, pInterface->backupDesgRtr);
    }
    else
    {
        IP_ADDR_COPY (pInterface->desgRtr, newDr);
    }
    OspfRmSendIfState (pInterface, OSPF_ZERO);
    OSPF_TRC1 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "DR Election Over New DR %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->desgRtr));
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: HpElectDrFromLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  HpCheckRtrInRcvdHello                                */
/*                                                                           */
/* Description  : Checks whether this router id is present as a neighbor id  */
/*                in the received hello packet                               */
/*                                                                           */
/* Input        : pOspfCxt   : Ospf Context pointer                          */
/*                pHelloPkt  : Pointer to hello packet in which the          */
/*                               check has to be performed.                  */
/*                u2PktLen   : Length of Hello packet.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE if found                                         */
/*                OSPF_FALSE Otherwise.                                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT1
HpCheckRtrInRcvdHelloInCxt (tRouterId rtrId, tCRU_BUF_CHAIN_HEADER * pHelloPkt,
                            UINT2 u2PktLen)
{
    tRouterId           nbrId;
    UINT2               u2NbrCount;
    UINT1               u1Found = OSPF_FALSE;
    UINT4               u4Offset = 0;

    u2NbrCount
        = (UINT2) ((u2PktLen - (OS_HEADER_SIZE + HELLO_FIXED_PORTION_SIZE)) /
                   sizeof (tRouterId));

    while (u2NbrCount--)
    {
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pHelloPkt, nbrId,
                                            (HP_NBR_ID_OFFSET + (4 * u4Offset)),
                                            MAX_IP_ADDR_LEN);
        if (UtilIpAddrComp (rtrId, nbrId) == OSPF_EQUAL)
        {
            u1Found = OSPF_TRUE;
            break;
        }
        u4Offset++;
    }
    return u1Found;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpRcvHello                                                 */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.5                          */
/*                This procedure processes the received hello packet. The    */
/*                received packet is associated with a neighbor and its      */
/*                contents are processed and the neighbor state machine is   */
/*                invoked with the appropriate event.                        */
/*                                                                           */
/* Input        : pHelloPkt           : the received hello pkt               */
/*                u2PktLen            : length of the pkt                    */
/*                pInterface           : the receiving interface             */
/*                pSrcIpAddr         : the source ip addr from IP header     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if packet processed successfully                  */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
HpRcvHello (tCRU_BUF_CHAIN_HEADER * pHelloPkt,
            UINT2 u2PktLen, tInterface * pInterface, tIPADDR * pSrcIpAddr)
{

    tRouterId           headerRtrId;
    tHelloStaticPortion hello;
    tNeighbor          *pNbr = NULL;
    INT4                i4IsmSchedFlag;
    UINT1               u1OldState;

    MEMSET (&headerRtrId, 0, sizeof (tRouterId));
    MEMSET (&hello, 0, sizeof (tHelloStaticPortion));
    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: HpRcvHello\n");
    if (pInterface->u1IsmState < MAX_IF_STATE)
    {
        OSPF_TRC4 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP Rcvd If %x.%d ISM State: %s From Nbr: %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u4AddrlessIf,
                   au1DbgIfState[pInterface->u1IsmState],
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pSrcIpAddr));
    }

    OSPF_CRU_BMC_GET_STRING (pHelloPkt, headerRtrId, RTR_ID_OFFSET_IN_HEADER,
                             MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_GET_STRING (pHelloPkt, hello.networkMask, OS_HEADER_SIZE,
                             MAX_IP_ADDR_LEN);

    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pHelloPkt, HP_HELLO_INTERVAL_OFFSET,
                                        hello.u2HelloInterval);
    OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pHelloPkt, HP_OPTIONS_OFFSET,
                                        hello.options);
    OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pHelloPkt, HP_RTR_PRI_OFFSET,
                                        hello.u1RtrPriority);
    OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pHelloPkt, HP_RTR_DEAD_INTERVAL_OFFSET,
                                        hello.i4RtrDeadInterval);
    OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pHelloPkt, hello.desgRtr,
                                        HP_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pHelloPkt, hello.backupDesgRtr,
                                        HP_BK_DSG_RTR_OFFSET, MAX_IP_ADDR_LEN);

    if ((pInterface->u1NetworkType != IF_VIRTUAL) &&
        (pInterface->u1NetworkType != IF_PTOP) &&
        (UtilIpAddrComp (pInterface->ifIpAddrMask,
                         hello.networkMask) != OSPF_EQUAL))
    {
        INC_DISCARD_HELLO_CNT (pInterface);

        OSPF_TRC4 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP Disc N/W Mask Mismatch If %x If Msk %x"
                   " Src IP Addr %x HP Msk %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask),
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pSrcIpAddr),
                   OSPF_CRU_BMC_DWFROMPDU (hello.networkMask));

        IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                           NETMASK_MISMATCH, HELLO_PKT, pSrcIpAddr);

        return OSPF_FAILURE;
    }

    if (pInterface->u2HelloInterval != hello.u2HelloInterval)
    {
        INC_DISCARD_HELLO_CNT (pInterface);

        OSPF_TRC4 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP Disc Hello Interval Mismtch If %x If Hello Intv %d"
                   " Src IP Addr %x HP Hello Intv %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u2HelloInterval,
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pSrcIpAddr)),
                   hello.u2HelloInterval);

        if (IS_VIRTUAL_IFACE (pInterface))
        {
            VifSendConfErrTrap (pInterface, VIRT_IF_CONFIG_ERROR_TRAP,
                                HELLO_INTERVAL_MISMATCH, HELLO_PKT);
        }
        else
        {
            IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                               HELLO_INTERVAL_MISMATCH, HELLO_PKT, pSrcIpAddr);
        }

        return OSPF_FAILURE;
    }

    if (E_BIT_OPTION_MISMATCH (pInterface, hello.options))
    {
        INC_DISCARD_HELLO_CNT (pInterface);

        OSPF_TRC4 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP Disc E-bit Mismatch If %x Stub Area Status %d"
                   " Src IP Addr %x HP Options %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->pArea->u4AreaType,
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pSrcIpAddr)),
                   hello.options);

        return OSPF_FAILURE;
    }

    /* Check for "N" Bit option */
    if (N_BIT_OPTION_MISMATCH (pInterface, hello.options))
    {
        INC_DISCARD_HELLO_CNT (pInterface);

        OSPF_TRC4 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP Disc N-bit Mismatch If %x Area Type %d"
                   " Src IP Addr %x HP Options %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->pArea->u4AreaType,
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pSrcIpAddr)),
                   hello.options);

        if (IS_VIRTUAL_IFACE (pInterface))
        {
            IfSendBadPktTrap (pInterface, HELLO_PKT, pSrcIpAddr);
        }
        else
        {
            IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                               OPTIONS_MISMATCH, HELLO_PKT, pSrcIpAddr);
        }
        return OSPF_FAILURE;
    }

    if (pInterface->i4RtrDeadInterval != hello.i4RtrDeadInterval)
    {
        INC_DISCARD_HELLO_CNT (pInterface);

        OSPF_TRC4 (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "HP Disc Rtr Dead Interval Mismatch If %x "
                   "If Rtr Dead Intv %d Src IP Addr %x HP Rtr Dead Intv %d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   (pInterface->i4RtrDeadInterval),
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) (pSrcIpAddr)),
                   (hello.i4RtrDeadInterval));

        if (IS_VIRTUAL_IFACE (pInterface))
        {
            VifSendConfErrTrap (pInterface, VIRT_IF_CONFIG_ERROR_TRAP,
                                DEAD_INTERVAL_MISMATCH, HELLO_PKT);
        }
        else
        {
            IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                               DEAD_INTERVAL_MISMATCH, HELLO_PKT, pSrcIpAddr);
        }
        return OSPF_FAILURE;
    }

    pNbr = HpAssociateWithNbr (pSrcIpAddr, &headerRtrId, pInterface);
    if (pNbr == NULL)
    {
        if (((pInterface->u1NetworkType == IF_NBMA) ||
             (pInterface->u1NetworkType == IF_PTOMP)))
        {
            return OSPF_FAILURE;
        }

        if ((pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
            && (pInterface->pArea->pOspfCxt->u1RestartExitReason
                == OSPF_RESTART_INPROGRESS))
        {
            /* check if this router is listed in the received hello */
            if (HpCheckRtrInRcvdHelloInCxt
                (pInterface->pArea->pOspfCxt->rtrId, pHelloPkt,
                 u2PktLen) != OSPF_TRUE)
            {
                /* Exit GR with exit reason as TOPOLOGY change */
                GrExitGracefultRestart (pInterface->pArea->pOspfCxt,
                                        OSPF_RESTART_TOP_CHG);
                return OSPF_FAILURE;

            }
        }

        if ((pNbr = NbrCreate (pSrcIpAddr, &headerRtrId,
                               pInterface->u4AddrlessIf, pInterface,
                               DISCOVERED_NBR)) != NULL)
        {
            NbrProcessPriorityChange (pNbr, hello.u1RtrPriority);
            IP_ADDR_COPY (pNbr->desgRtr, hello.desgRtr);
            IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);
        }
        else
        {
            OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Could Not Associate With Nbr\n");
            return OSPF_FAILURE;
        }
    }

    OspfRmSendHello (pHelloPkt, u2PktLen, pInterface, pNbr, pSrcIpAddr);
    if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pInterface))
    {
        /* suppress hello */
        if ((hello.options & DC_BIT_MASK) &&
            (HpCheckRtrInRcvdHelloInCxt
             (pInterface->pArea->pOspfCxt->rtrId, pHelloPkt,
              u2PktLen) == OSPF_TRUE))
        {
            pNbr->bHelloSuppression = OSPF_TRUE;
        }
        else
        {
            /* check for the validity of discovered end point */
            if (!(hello.options & DC_BIT_MASK) &&
                (HpCheckRtrInRcvdHelloInCxt
                 (pInterface->pArea->pOspfCxt->rtrId, pHelloPkt,
                  u2PktLen) == OSPF_TRUE))
            {
                pNbr->bHelloSuppression = OSPF_FALSE;
                if (IS_DISCOVERED_DC_ENDPOINT_IFACE (pInterface))
                {
                    pInterface->bDcEndpt = OSPF_FALSE;
                    pInterface->ifOptions = RAG_NO_CHNG;
                    TmrDeleteTimer (&pInterface->pollTimer);
                }
            }
        }
    }
    else
    {
        /* making other end of the dc link as discovered end point */
        if ((hello.options & DC_BIT_MASK) &&
            (pInterface->u1NetworkType == IF_PTOP ||
             pInterface->u1NetworkType == IF_VIRTUAL ||
             pInterface->u1NetworkType == IF_PTOMP))
        {
            pInterface->bDcEndpt = OSPF_TRUE;
            pInterface->ifOptions = DC_BIT_MASK;
            pInterface->u1ConfStatus = DISCOVERED_ENDPOINT;
            TmrRestartTimer (&(pInterface->pollTimer), POLL_TIMER,
                             NO_OF_TICKS_PER_SEC *
                             (pInterface->i4PollInterval));
        }
    }

    if ((pInterface->u1NetworkType == IF_BROADCAST) ||
        (pInterface->u1NetworkType == IF_NBMA) ||
        (pInterface->u1NetworkType == IF_PTOMP))
    {
        IP_ADDR_COPY (pNbr->nbrId, headerRtrId);
    }
    else if (pInterface->u1NetworkType == IF_PTOP)
    {
        IP_ADDR_COPY (pNbr->nbrIpAddr, *pSrcIpAddr);
    }

    u1OldState = pNbr->u1NsmState;
    OSPF_TRC1 (CONTROL_PLANE_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "NBRE: Hello recieved for Neighbour with Address = %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
    GENERATE_NBR_EVENT (pNbr, NBRE_HELLO_RCVD);

    if ((pInterface->u1NetworkType == IF_NBMA) &&
        (pInterface->u1RtrPriority == INELIGIBLE_RTR_PRIORITY) &&
        (pNbr->u1NbrRtrPriority > INELIGIBLE_RTR_PRIORITY) &&
        (UtilIpAddrComp (pNbr->nbrIpAddr,
                         GET_DR (pInterface)) != OSPF_EQUAL) &&
        (UtilIpAddrComp (pNbr->nbrIpAddr, GET_BDR (pInterface)) != OSPF_EQUAL))
    {

        HpSendHello (pInterface, pNbr, INVALID_TIMER);
    }

    /* To avoid Improper Election in the case of NBMA network */
    if ((pInterface->u1NetworkType == IF_NBMA) &&
        ((u1OldState == NBRS_DOWN) || u1OldState == NBRS_ATTEMPT))
    {

        if ((pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
            && (pInterface->pArea->pOspfCxt->u1RestartExitReason
                == OSPF_RESTART_INPROGRESS))
        {
            OSPF_TRC1 (CONTROL_PLANE_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Two way state sync is received for Neighbour with Address = %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_2WAY_RCVD);
            if (UtilIpAddrComp (hello.desgRtr, pInterface->ifIpAddr) ==
                OSPF_EQUAL)
            {
                IfUpdateState (pInterface, IFS_DR);
            }
            else if (UtilIpAddrComp (hello.backupDesgRtr, pInterface->ifIpAddr)
                     == OSPF_EQUAL)
            {
                IfUpdateState (pInterface, IFS_BACKUP);
            }
            else
            {
                IfUpdateState (pInterface, IFS_DR_OTHER);
            }

            IP_ADDR_COPY (pInterface->desgRtr, hello.desgRtr);
            IP_ADDR_COPY (pInterface->backupDesgRtr, hello.backupDesgRtr);
        }
        else
        {
            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: One way state sync is received for Neighbour with Address = %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_1WAY_RCVD);
        }

        IP_ADDR_COPY (pNbr->desgRtr, hello.desgRtr);
        IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);
        pNbr->u1NbrRtrPriority = hello.u1RtrPriority;
        return SUCCESS;
    }

    /* check if this router is listed in the received hello */
    if (HpCheckRtrInRcvdHelloInCxt
        (pInterface->pArea->pOspfCxt->rtrId, pHelloPkt, u2PktLen) == OSPF_TRUE)
    {
        OSPF_TRC1 (CONTROL_PLANE_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "NBRE: Two way state sync is received for Neighbour with Address = %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
        GENERATE_NBR_EVENT (pNbr, NBRE_2WAY_RCVD);
    }
    else
    {
        GENERATE_NBR_EVENT (pNbr, NBRE_1WAY_RCVD);
        return SUCCESS;
    }

    /* Added to implement the condition specified in RFC 1583 Section 7.3
     * If the n/w already has a DR & a BDR router elected , the new i/f
     * coming up should accept these as the DR & the BDR routers irrespective
     * of the proiority */

    i4IsmSchedFlag = ISM_NOT_SCHEDULED;
    if ((pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
        && (pInterface->pArea->pOspfCxt->u1RestartExitReason
            == OSPF_RESTART_INPROGRESS)
        && (pInterface->u1IsmState == IFS_WAITING))
    {
        if (((pNbr->pInterface->u1NetworkType != IF_PTOP)
             && (pNbr->pInterface->u1NetworkType != IF_PTOMP)))
        {
            if (UtilIpAddrComp (hello.desgRtr, pInterface->ifIpAddr) ==
                OSPF_EQUAL)
            {
                IfUpdateState (pInterface, IFS_DR);
            }
            else if (UtilIpAddrComp (hello.backupDesgRtr, pInterface->ifIpAddr)
                     == OSPF_EQUAL)
            {
                IfUpdateState (pInterface, IFS_BACKUP);
            }
            else
            {
                IfUpdateState (pInterface, IFS_DR_OTHER);
            }

            IP_ADDR_COPY (pNbr->desgRtr, hello.desgRtr);
            IP_ADDR_COPY (pInterface->desgRtr, hello.desgRtr);

            IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);
            IP_ADDR_COPY (pInterface->backupDesgRtr, hello.backupDesgRtr);
            return SUCCESS;
        }

    }

    if (hello.u1RtrPriority != pNbr->u1NbrRtrPriority)
    {
        if ((i4IsmSchedFlag = NbrProcessPriorityChange (pNbr,
                                                        hello.u1RtrPriority)) ==
            ISM_SCHEDULED)
        {
            IsmSchedule (pNbr->pInterface, IFE_NBR_CHANGE);
        }

    }
    if ((pInterface->u1IsmState == IFS_WAITING) &&
        (UtilIpAddrComp (*pSrcIpAddr, hello.desgRtr) == OSPF_EQUAL) &&
        (IS_NULL_IP_ADDR (hello.backupDesgRtr)))
    {
        if (i4IsmSchedFlag == ISM_NOT_SCHEDULED)
        {
            i4IsmSchedFlag = ISM_SCHEDULED;
            IsmSchedule (pInterface, IFE_BACKUP_SEEN);
        }
    }
    else if (((UtilIpAddrComp (pNbr->nbrIpAddr,
                               pNbr->desgRtr) != OSPF_EQUAL) &&
              (UtilIpAddrComp (pNbr->nbrIpAddr,
                               hello.desgRtr) == OSPF_EQUAL)) ||
             ((UtilIpAddrComp (pNbr->nbrIpAddr,
                               pNbr->desgRtr) == OSPF_EQUAL) &&
              (UtilIpAddrComp (pNbr->nbrIpAddr, hello.desgRtr) != OSPF_EQUAL)))
    {
        if (i4IsmSchedFlag == ISM_NOT_SCHEDULED)
        {
            IP_ADDR_COPY (pNbr->desgRtr, hello.desgRtr);
            IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

            i4IsmSchedFlag = ISM_SCHEDULED;
            IsmSchedule (pInterface, IFE_NBR_CHANGE);
        }
    }
    IP_ADDR_COPY (pNbr->desgRtr, hello.desgRtr);

    if ((pInterface->u1IsmState == IFS_WAITING) &&
        (UtilIpAddrComp (*pSrcIpAddr, hello.backupDesgRtr) == OSPF_EQUAL))
    {
        if (i4IsmSchedFlag == ISM_NOT_SCHEDULED)
        {
            IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

            i4IsmSchedFlag = ISM_SCHEDULED;
            IsmSchedule (pInterface, IFE_BACKUP_SEEN);
        }
    }
    else if (NBR_LOSE_OR_GAIN_DR_OR_BDR (pNbr, hello.backupDesgRtr))
    {
        if (i4IsmSchedFlag == ISM_NOT_SCHEDULED)
        {
            IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

            i4IsmSchedFlag = ISM_SCHEDULED;
            IsmSchedule (pInterface, IFE_NBR_CHANGE);
        }
    }
    IP_ADDR_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId, "HP Processing Over\n");
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HpAssociateWithNbr                                      */
/*                                                                           */
/* Description  : This procedure associates the received hello with a        */
/*                neighbor based on the IP address from the IP header and the*/
/*                router ID from the OSPF header.                            */
/*                                                                           */
/* Input        : pSrcIpAddr     : src ip addr from IP header             */
/*                pHeaderRtrId   : rtrId from OSPF header                */
/*                pInterface       : the receiving interface                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to neighbour structure                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC tNeighbor   *
HpAssociateWithNbr (tIPADDR * pSrcIpAddr,
                    tRouterId * pHeaderRtrId, tInterface * pInterface)
{
    tNeighbor          *pNbr;
    tIPADDR            *pSearchField;

    if ((pInterface->u1NetworkType == IF_BROADCAST) ||
        (pInterface->u1NetworkType == IF_NBMA) ||
        (pInterface->u1NetworkType == IF_PTOMP))
    {
        pSearchField = pSrcIpAddr;
    }
    else
    {
        pSearchField = pHeaderRtrId;
    }
    pNbr = HpSearchNbrLst (pSearchField, pInterface);

    OSPF_TRC (OSPF_HP_TRC | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "HP Associated With Nbr\n");

    return (pNbr);

}

/*------------------------------------------------------------------------*/
/*                        End of the file oshp.c                          */
/*------------------------------------------------------------------------*/
