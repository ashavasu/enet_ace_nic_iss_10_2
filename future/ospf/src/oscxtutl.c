/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oscxtutl.c,v 1.48 2017/09/21 13:48:45 siva Exp $
 *
 * Description: This file contains utility routines.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID        UtilAuthenticatePkt
PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, tInterface * pInterface, UINT2 u2Len));
PRIVATE INT4 UtilMod255 PROTO ((INT4 i4Sum));
PRIVATE UINT1       gau1AuthPkt[MAX_OSPF_PKT_LEN];
static UINT1        au1_bit_count[256] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};
extern UINT4        FsMIStdOspfRouterId[12];

/*****************************************************************************/
/*                                                                           */
/* Function     : OsUtilCalCxtRBTree                                         */
/*                                                                           */
/* Description  : This function is used to generate the semaphore name       */
/*                                                                           */
/* Input        : u4Base                                                     */
/*                u4OspfCxtId                                                */
/*                                                                           */
/* Output       : au1SemName                                                 */
/*                                                                           */
/* Returns      : NONE                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OsUtilCalCxtRBTree (UINT4 u4Base, UINT4 u4OspfCxtId, UINT1 au1SemName[])
{
    UINT4               u4Id = 0;
    u4Id = u4OspfCxtId << 2;

    SNPRINTF ((CHR1 *) au1SemName, OSIX_NAME_LEN + 1, "O%.3x", (u4Base + u4Id));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindMaskLen                                         */
/*                                                                           */
/* Description  : This procedure finds the length of the given mask.         */
/*                                                                           */
/* Input        : u4Mask  : Mask whose length is to be determined.          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Length of the mask.                                        */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilFindMaskLen (UINT4 u4Mask)
{
    return ((UINT1) (au1_bit_count[u4Mask & 0xff]
                     + au1_bit_count[(u4Mask >> 8) & 0xff]
                     + au1_bit_count[(u4Mask >> 16) & 0xff]
                     + au1_bit_count[(u4Mask >> 24) & 0xff]));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilConstructHdr                                         */
/*                                                                           */
/* Description  : This procedure constructs the ospf header for packets to be*/
/*                transmitted on the specified interface. This procedure will*/
/*                be called after the packet is fully constructed.           */
/*                                                                           */
/* Input        : pInterface       : interface over which the packets are   */
/*                                    transmitted                            */
/*                pBuf             : buffer which holds the header          */
/*                u1Type           : type                                   */
/*                u2Len            : length                                 */
/*                                                                           */
/* Output       : pBuf, buffer which holds the header                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilConstructHdr (tInterface * pInterface,
                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Type, UINT2 u2Len)
{
    tRouterId           rtrId;
    UINT4               u4SetValOspfRouterId = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, VERSION_NO_OFFSET, OSPF_VERSION_NO_2);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, TYPE_OFFSET, u1Type);
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, PKT_LENGTH_OFFSET, u2Len);
    MEMSET (&rtrId, 0, sizeof (tRouterId));
    if (OS_MEM_CMP
        (pInterface->pArea->pOspfCxt->rtrId, rtrId, MAX_IPV4_ADDR_LEN) == 0)
    {
        if (UtilSelectOspfRouterId
            (pInterface->pArea->pOspfCxt->u4OspfCxtId, &rtrId) == OSPF_SUCCESS)
        {
            IP_ADDR_COPY (&(pInterface->pArea->pOspfCxt->rtrId), &(rtrId));
            u4SetValOspfRouterId =
                OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->pOspfCxt->rtrId);
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfRouterId;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIStdOspfRouterId) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 1;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", gOsRtr.u4OspfCxtId,
                              u4SetValOspfRouterId));
#endif
        }
    }
    OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pBuf, pInterface->pArea->pOspfCxt->rtrId,
                                       RTR_ID_OFFSET, MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pBuf, GET_IF_AREA_ID (pInterface),
                                       AREA_ID_OFFSET, MAX_IP_ADDR_LEN);

    /* checksum field is set to 0 before calculation */
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, CHKSUM_OFFSET, 0);
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_TYPE_OFFSET, pInterface->u2AuthType);

#ifdef RM_WANTED
    if ((pInterface->u2AuthType == CRYPT_AUTHENTICATION)
        && (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5))

    {
        OspfRmSendMd5SeqNo (pInterface);
    }
#endif

    UtilAuthenticatePkt (pBuf, pInterface, u2Len);

}

/*******************************************************************************
* Function : GetAuthkeyTouse                                                 *
* Input    : pInterface                                                       *
* Output   : None                                                              *
* Returns  : pKeyTouse - The MD5 authentication key to use                   *
*******************************************************************************/
tMd5AuthkeyInfo    *
GetAuthkeyTouse (tInterface * pInterface)
{
    tMd5AuthkeyInfo    *pNode = NULL;
    tMd5AuthkeyInfo    *pExpiredNode = NULL;
    tMd5AuthkeyInfo    *pKeyTouse = NULL;
    UINT4               newDiff;
    UINT4               u4KeyStartGenDiff = (UINT4) -1;
    UINT4               u4Ospf1sTimer = 0;
    tUtlTm              tm;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: GetAuthkeyTouse \n");
    /* Getting the system time */
    UtlGetTime (&tm);
    u4Ospf1sTimer = GrGetSecondsSinceBase (tm);    /* Converting time in string to u4 value */
    TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst), pNode, tMd5AuthkeyInfo *)
    {

        if (pNode->u4KeyStartGenerate <= u4Ospf1sTimer)    /* Comparison is made with system time */
        {
            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Key Start Time is compared with System Time\n");
            /* If atlease one key with start time less than the system time 
               is present, then delete the expired key */
            if (pExpiredNode != NULL)
            {
                /*        TMO_SLL_Delete (&(pInterface->sortMd5authkeyLst),
                   &(pExpiredNode->nextSortKey));
                   MD5AUTH_FREE (pExpiredNode); */
            }
            if ((pNode->u4KeyStopGenerate < u4Ospf1sTimer) &&    /* Comparison is made with system time */
                (pNode->u4KeyStopAccept < u4Ospf1sTimer))
            {
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "OSPF Key Node assigned to expired\n");
                pExpiredNode = pNode;
            }

            else if ((pNode->u4KeyStopGenerate == 0) || (pNode->u4KeyStopGenerate > u4Ospf1sTimer))    /* Comparison is made with system time */
            {
                /* If more than one unexpired key is available then use
                   most recent key having start time maximum */
                newDiff = u4Ospf1sTimer - pNode->u4KeyStartGenerate;
                if (newDiff < u4KeyStartGenDiff)
                {
                    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                              pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              "OSPF Key Id changed to most recent "
                              "key having max start time\n");
                    u4KeyStartGenDiff = newDiff;
                    pKeyTouse = pNode;
                }
            }
        }
    }

    /* If the last key is expired, that key can be used, until the new key 
       is configured */
    if ((pExpiredNode != NULL) && (pKeyTouse == NULL))
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "OSPF Expired Key is returned \n");
        OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: GetAuthkeyTouse \n");
        /*SendTrap for last key expired */
        return pExpiredNode;
    }
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: GetAuthkeyTouse \n");
    return pKeyTouse;
}

/*******************************************************************************
* Function : AppendMd5authDigest                                             *
* Input    : pBuf                                                             *
*            u2Len                                                            *
*            pAuthkeyInfo                                                    *
* Output   : None                                                              *
* Returns  : void                                                              *
*******************************************************************************/
PRIVATE void
AppendMd5authDigest (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2Len, tMd5AuthkeyInfo * pAuthkeyInfo)
{
    UINT1               digest[MAX_AUTHKEY_LEN];

    OSPF_CRU_BMC_ASSIGN_STRING (pBuf, pAuthkeyInfo->authKey, u2Len,
                                MAX_AUTHKEY_LEN);
    md5_get_keyed_digest (pBuf, pAuthkeyInfo->authKey, MAX_AUTHKEY_LEN, digest);
    OSPF_CRU_BMC_ASSIGN_STRING (pBuf, digest, u2Len, MAX_AUTHKEY_LEN);
}

/******************************************************************************************
*  FUNCTION    : UtilHandleShaAuthentication
*  Description : This function will take care of adding the SHA digest with respect to the 
*                 cryptographic algorithm configured by calling the UtilGetShaDigest api.
*                
*  INPUT       : pBuf  Buffer which holds the header 
*                u2Len length of the buffer
*                u4WhichSha which SHA algorithm is used
*                pInterface Interface in which the packet is transmitted
*                tMd5AuthkeyInfo Pointer to the key 
*  OUTPUT      : None
*  Return      : void
* ******************************************************************************************/
PRIVATE VOID
UtilHandleShaAuthentication (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                             UINT4 u4WhichSha, tInterface * pInterface,
                             tMd5AuthkeyInfo * pAuthkeyInfo)
{

    /*Todo Extract the data from the CRU Buf into a linear array and send it to SHA algo */

    INT4                i4ShaDigestLen = 0;
    UINT1               digest[OSPF_SHA2_512_DIGEST_LEN];
    switch (u4WhichSha)
    {

        case OSPF_AUTH_SHA1:
            i4ShaDigestLen = OSPF_SHA1_DIGEST_LEN;
            break;

        case OSPF_AUTH_SHA2_224:
            i4ShaDigestLen = OSPF_SHA2_224_DIGEST_LEN;
            break;

        case OSPF_AUTH_SHA2_256:
            i4ShaDigestLen = OSPF_SHA2_256_DIGEST_LEN;
            break;

        case OSPF_AUTH_SHA2_384:
            i4ShaDigestLen = OSPF_SHA2_384_DIGEST_LEN;
            break;

        case OSPF_AUTH_SHA2_512:
            i4ShaDigestLen = OSPF_SHA2_512_DIGEST_LEN;
            break;

        default:
            break;

    }
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_1_OFFSET, (UINT2) 0);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_2_OFFSET, pAuthkeyInfo->u1AuthkeyId);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_3_OFFSET, i4ShaDigestLen);
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pBuf, AUTH_4_OFFSET, pInterface->u4CryptSeqNum);

    UtilGetShaDigest (pBuf, u2Len, pAuthkeyInfo, u4WhichSha, digest);

    OSPF_CRU_BMC_ASSIGN_STRING (pBuf, digest, u2Len, (UINT2) i4ShaDigestLen);

}

/**********************************************************************************
*  FUNCTION    : OsUtilGetCryptoLen
*  Description : This function will return the Digest length of the Cryptographic .
*                algorithm
*  INPUT       : pInterface Interface in which the packet is transmitted
*               
*  OUTPUT      : None
*  Return      : DigestLength of the Cryptographic algorithm passed
* **************************************************************************************/

PUBLIC INT4
OsUtilGetCryptoLen (tInterface * pInterface)
{

    switch (pInterface->u4CryptoAuthType)
    {

        case OSPF_AUTH_MD5:
            return MD5_MSG_DIGEST_LEN;
        case OSPF_AUTH_SHA1:
            return OSPF_SHA1_DIGEST_LEN;
        case OSPF_AUTH_SHA2_224:
            return OSPF_SHA2_224_DIGEST_LEN;
        case OSPF_AUTH_SHA2_256:
            return OSPF_SHA2_256_DIGEST_LEN;
        case OSPF_AUTH_SHA2_384:
            return OSPF_SHA2_384_DIGEST_LEN;
        case OSPF_AUTH_SHA2_512:
            return OSPF_SHA2_512_DIGEST_LEN;
        default:
            return 0;
    }
}

/******************************************************************************************
*  FUNCTION    : UtilGetShaDigest
*  Description : This function will take care of getting the SHA digest with respect to the 
*                 cryptographic algorithm configured.
*                
*  INPUT       : pBuf  Buffer which holds the header 
*                u2Len length of the buffer
*                u4WhichSha which SHA algorithm is used
*                pInterface Interface in which the packet is transmitted
*                pdigest Pointer to the digest 
*  OUTPUT      : None
*  Return      : void
* ******************************************************************************************/
PUBLIC VOID
UtilGetShaDigest (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                  tMd5AuthkeyInfo * pAuthkeyInfo, UINT4 u4whichSha,
                  UINT1 *pdigest)
{
    UINT1              *pu1DataPtr = (UINT1 *) &gau1AuthPkt;

    /*Copy the CRU buffer in a linear array */
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataPtr, 0, u2Len) == CRU_FAILURE)
    {
        return;

    }
    switch (u4whichSha)
    {
        case OSPF_AUTH_SHA1:
            arHmac_Sha1 ((UINT1 *) pAuthkeyInfo->authKey,
                         pAuthkeyInfo->u1KeyLen, pu1DataPtr, u2Len, pdigest);
            break;

        case OSPF_AUTH_SHA2_224:
            arHmacSha2 (OSPF_AR_SHA224_ALGO, (UINT1 *) pAuthkeyInfo->authKey,
                        pAuthkeyInfo->u1KeyLen, pu1DataPtr, u2Len, pdigest);
            break;

        case OSPF_AUTH_SHA2_256:
            arHmacSha2 (OSPF_AR_SHA256_ALGO, (UINT1 *) pAuthkeyInfo->authKey,
                        pAuthkeyInfo->u1KeyLen, pu1DataPtr, u2Len, pdigest);
            break;

        case OSPF_AUTH_SHA2_384:
            arHmacSha2 (OSPF_AR_SHA384_ALGO, (UINT1 *) pAuthkeyInfo->authKey,
                        pAuthkeyInfo->u1KeyLen, pu1DataPtr, u2Len, pdigest);
            break;

        case OSPF_AUTH_SHA2_512:
            arHmacSha2 (OSPF_AR_SHA512_ALGO, (UINT1 *) pAuthkeyInfo->authKey,
                        pAuthkeyInfo->u1KeyLen, pu1DataPtr, u2Len, pdigest);
            break;

        default:
            break;

    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilAuthenticatePkt                                      */
/*                                                                           */
/* Description  : This procedure authenticates the given ospf packet         */
/*                                                                           */
/* Input        :  pBuf                     : Pointer to the buffer having  */
/*                                             the OSPF packet.              */
/*              :  pInterface               : Pointer to interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
UtilAuthenticatePkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                     tInterface * pInterface, UINT2 u2Len)
{
    UINT2               u2Chksum;
    tMd5AuthkeyInfo    *pAuthkeyInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: UtilAuthenticatePkt\n");

    switch (pInterface->u2AuthType)
    {
        case NO_AUTHENTICATION:
            /* calculate checksum for the packet */
            OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pBuf, 0, AUTH_KEY_OFFSET_IN_HEADER,
                                             AUTH_KEY_SIZE);
            u2Chksum = UtilCalculateChksum (pBuf, u2Len);
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, CHKSUM_OFFSET_IN_HEADER,
                                        u2Chksum);
            break;

        case SIMPLE_PASSWORD:
            /* calculate checksum for the packet */
            OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pBuf, 0, AUTH_KEY_OFFSET_IN_HEADER,
                                             AUTH_KEY_SIZE);
            u2Chksum = UtilCalculateChksum (pBuf, u2Len);
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, CHKSUM_OFFSET_IN_HEADER,
                                        u2Chksum);
            OSPF_CRU_BMC_ASSIGN_STRING (pBuf, pInterface->authKey,
                                        AUTH_KEY_OFFSET_IN_HEADER,
                                        AUTH_KEY_SIZE);
            break;

        case CRYPT_AUTHENTICATION:

            pAuthkeyInfo = GetAuthkeyTouse (pInterface);
            if (pAuthkeyInfo == NULL && pInterface->pLastAuthkey)
            {
                pAuthkeyInfo = pInterface->pLastAuthkey;
            }
            else
            {
                pInterface->pLastAuthkey = pAuthkeyInfo;
            }

            if (pAuthkeyInfo == NULL)
            {
                OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_1_OFFSET, 0);
                OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_2_OFFSET, 0);
                OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_3_OFFSET, 0);
                OSPF_CRU_BMC_ASSIGN_4_BYTE (pBuf, AUTH_4_OFFSET, 0);

                OSPF_TRC (OSPF_FN_EXIT,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "EXIT: UtilAuthenticatePkt\n");
                return;
            }
            /* todo handle sha authentication */
            switch (pInterface->u4CryptoAuthType)
            {
                case OSPF_AUTH_MD5:
                    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_1_OFFSET, (UINT2) 0);
                    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_2_OFFSET,
                                                pAuthkeyInfo->u1AuthkeyId);
                    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_3_OFFSET,
                                                MD5_AUTH_DATA_LEN);
                    OSPF_CRU_BMC_ASSIGN_4_BYTE (pBuf, AUTH_4_OFFSET,
                                                pInterface->u4CryptSeqNum);
                    AppendMd5authDigest (pBuf, u2Len, pAuthkeyInfo);
                    break;
                case OSPF_AUTH_SHA1:
                case OSPF_AUTH_SHA2_224:
                case OSPF_AUTH_SHA2_256:
                case OSPF_AUTH_SHA2_384:
                case OSPF_AUTH_SHA2_512:
                    UtilHandleShaAuthentication (pBuf, u2Len,
                                                 pInterface->u4CryptoAuthType,
                                                 pInterface, pAuthkeyInfo);
                    break;
                default:
                    break;

            }
            pInterface->u4CryptSeqNum++;
            break;

        default:
            break;
    }
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: UtilAuthenticatePkt\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOsMsgAlloc                                          */
/*                                                                           */
/* Description  : This procedure allocates memory for the size               */
/*                u4Size + size of the IP header                            */
/*                                                                           */
/* Input        : u4Size   : size of the buffer required.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the CRU_BUF_CHAIN_HEADER in case of successful  */
/*                allocation.                                                */
/*                NULL, otherwise.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC tCRU_BUF_CHAIN_HEADER *
UtilOsMsgAlloc (UINT4 u4Size)
{

    tCRU_BUF_CHAIN_HEADER *pMsg;
    UINT4               u4AlignByte;    /*no of byte to be appeneded for alignment */
    UINT4               u4AlignSize;    /*smallest size of an aligned variable */

    u4AlignSize = sizeof (UINT4);
    u4AlignByte = (u4AlignSize - (ETH_HEADER_SIZE % u4AlignSize)) % u4AlignSize;
    /* Set valid offset to IP header size. */
    /* Add u4AlignByte with Ethernet header size to make IP header start from
     * aligned byte*/
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((u4Size + IP_HEADER_SIZE +
                                       ETH_HEADER_SIZE + u4AlignByte),
                                      IP_HEADER_SIZE + ETH_HEADER_SIZE +
                                      u4AlignByte);
    if (pMsg == NULL)
    {
        return (tCRU_BUF_CHAIN_HEADER *) NULL;
    }
    MEMSET (pMsg->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pMsg, "UtilOsMsg");
    return pMsg;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOsMsgFree                                           */
/*                                                                           */
/* Description  : This procedure releases memory in the buffer chain         */
/*                                                                           */
/* Input        : msg                  : pointer to the CRU_BUF_CHAIN_HEADER */
/*                u1ReleaseFlag      : Indicates the type of release,      */
/*                                       either NORMAL or FORCED.            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilOsMsgFree (tCRU_BUF_CHAIN_HEADER * msg, UINT1 u1ReleaseFlag)
{
    CRU_BUF_Release_MsgBufChain (msg, u1ReleaseFlag);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrComp                                          */
/*                                                                           */
/* Description  : This procedure compares two ip addresses                   */
/*                                                                           */
/* Input        : addr1   :   IP address 1                                   */
/*                addr2   :   IP address 2                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if the IP addresses are equal                  */
/*                OSPF_GREATER, if IP address 1 is greater                   */
/*                OSPF_LESS, if IP address 2 is greater                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrComp (tIPADDR addr1, tIPADDR addr2)
{

    UINT4               u4Addr1, u4Addr2;

    /* returns OSPF_EQUAL if they are equal 
     * OSPF_GREATER if addr1 is greater
     * OSPF_LESS if addr1 is lesser
     */

    if ((addr1 == NULL) && (addr2 == NULL))
        return OSPF_EQUAL;

    if (addr1 == NULL)
        return OSPF_LESS;
    if (addr2 == NULL)
        return OSPF_GREATER;

    u4Addr1 = OSPF_CRU_BMC_DWFROMPDU (addr1);
    u4Addr2 = OSPF_CRU_BMC_DWFROMPDU (addr2);

    if (u4Addr1 > u4Addr2)
        return OSPF_GREATER;
    else if (u4Addr1 < u4Addr2)
        return OSPF_LESS;

    return (OSPF_EQUAL);
}

/*****************************************************************************/
/* Function Name : OspfPrintIpAddr                                           */
/* Description   : This routine takes the IP address in the decimal form and */
/*                 returns in the dotted notation.                           */
/* Input(s)      : Ip Address (pu1IpAddr)                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Address in dotted notation (pu1IpAddr)                 */
/*****************************************************************************/
UINT1              *
OspfPrintIpAddr (UINT1 *pu1IpAddr)
{
    static UINT1        au1IpAddrBuffer[OSPF_PRINT_BUF_SIZE];

    MEMSET (au1IpAddrBuffer, OSPF_ZERO, sizeof (au1IpAddrBuffer));

    SNPRINTF ((char *) au1IpAddrBuffer, OSPF_PRINT_BUF_SIZE, "%d.%d.%d.%d",
              *(pu1IpAddr), *(pu1IpAddr + 1), *(pu1IpAddr + 2),
              *(pu1IpAddr + 3));
    return (UINT1 *) &au1IpAddrBuffer;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrMaskComp                                     */
/*                                                                           */
/* Description  : This procedure compares two ip addresses after applying    */
/*                the mask.                                                  */
/*                                                                           */
/* Input        : addr1    :  IP address 1                                   */
/*                addr2    :  IP address 2                                   */
/*                mask     :  mask to be applied on IP addresses             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if the IP addresses are equal                  */
/*                OSPF_GREATER, if IP address 1 is greater                   */
/*                OSPF_LESS, if IP address 2 is greater                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrMaskComp (tIPADDR addr1, tIPADDR addr2, tIPADDR mask)
{

    UINT4               u4Addr1;
    UINT4               u4Addr2;

    /* 
     * Compares two IP adresses after applying the specified mask.
     * returns OSPF_EQUAL   if they are equal 
     *         OSPF_GREATER if addr1 is greater
     *         OSPF_LESS    if addr1 is lesser
     */
    u4Addr1 = OSPF_CRU_BMC_DWFROMPDU (addr1) & OSPF_CRU_BMC_DWFROMPDU (mask);
    u4Addr2 = OSPF_CRU_BMC_DWFROMPDU (addr2) & OSPF_CRU_BMC_DWFROMPDU (mask);

    if (u4Addr1 > u4Addr2)
        return OSPF_GREATER;
    else if (u4Addr1 < u4Addr2)
        return OSPF_LESS;

    return (OSPF_EQUAL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrMaskCopy                                     */
/*                                                                           */
/* Description  : This procedure copies IP address from the source to the    */
/*                destination, after MASK is applied on the source address.  */
/*                                                                           */
/* Input        : dest      :  destination address                           */
/*                src       :  source address                                */
/*                mask      :  mask to be applied to both the addresses      */
/*                                                                           */
/* Output       : destination address                                        */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilIpAddrMaskCopy (tIPADDR dest, tIPADDR src, tIPADDR mask)
{

    UINT4               u4Src;
    UINT4               u4Mask;

    u4Src = OSPF_CRU_BMC_DWFROMPDU (src);
    u4Mask = OSPF_CRU_BMC_DWFROMPDU (mask);
    OSPF_CRU_BMC_DWTOPDU (((UINT1 *) dest), (u4Src & u4Mask));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrIndComp                                      */
/*                                                                           */
/* Description  : This procedure compares the ip address and the ip addrless */
/*                ind fields in the two interfaces.                          */
/*                                                                           */
/* Input        : ifIpAddr1      : Interface address of the IP address 1   */
/*                u4AddrlessIf1  : Interface Index 1                       */
/*                ifIpAddr2      : Interface address of the IP address 2   */
/*                u4AddrlessIf2  : Interface Index 2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if they are equal                              */
/*                OSPF_GREATER, if interface1 is greater                     */
/*                OSPF_LESS, if interface2 is greater                        */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrIndComp (tIPADDR ifIpAddr1,
                   UINT4 u4AddrlessIf1, tIPADDR ifIpAddr2, UINT4 u4AddrlessIf2)
{

    UINT1               u1Result;

    u1Result = UtilIpAddrComp (ifIpAddr1, ifIpAddr2);
    if (u1Result == OSPF_EQUAL)
    {
        if (u4AddrlessIf1 > u4AddrlessIf2)
        {
            u1Result = OSPF_GREATER;
        }
        else if (u4AddrlessIf1 < u4AddrlessIf2)
        {
            u1Result = OSPF_LESS;
        }
    }
    return u1Result;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilLsaIdComp                                           */
/*                                                                           */
/* Description  : This procedure compares two LSA IDs                        */
/*                                                                           */
/* Input        : u1LsaType1       :  LSA type 1                           */
/*                linkStateId1     :  Link state Id 1                      */
/*                advRtrId1        :  Advertising Router Id 1              */
/*                u1LsaType2       :  LSA type 2                           */
/*                linkStateId2     :  Link state Id 2                      */
/*                advRtrId2        :  Advertising Router Id 2              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :  OSPF_EQUAL, if the triplets are equal                     */
/*                 OSPF_GREATER, if triplet1 is greater                      */
/*                 OSPF_LESS, if triplet2 is greater                         */
/*                 triplet is <Lsa type, Link state Id, Adv. Rtr Id>         */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilLsaIdComp (UINT1 u1LsaType1, tLINKSTATEID linkStateId1,
               tRouterId advRtrId1, UINT1 u1LsaType2,
               tLINKSTATEID linkStateId2, tRouterId advRtrId2)
{

    if (u1LsaType1 < u1LsaType2)
    {
        return OSPF_LESS;
    }
    else if (u1LsaType1 > u1LsaType2)
    {
        return OSPF_GREATER;
    }
    switch (UtilIpAddrComp (linkStateId1, linkStateId2))
    {
        case OSPF_LESS:
            return OSPF_LESS;

        case OSPF_GREATER:
            return OSPF_GREATER;

        case OSPF_EQUAL:
            return (UtilIpAddrComp (advRtrId1, advRtrId2));

        default:
            return INVALID;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindAssoAreaAddrRangeInCxt                             */
/*                                                                           */
/* Description  : This procedure associates the specified IP addr with one of*/
/*                the router's configured areas based on which address range */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pAddr        : IP address                                 */
/*              : pAreaId      : Area Id                                    */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddrRange  *
UtilFindAssoAreaAddrRangeInCxt (tOspfCxt * pOspfCxt, tIPADDR * pAddr,
                                tAreaId * pAreaId)
{

    UINT1               u1Index;
    tArea              *pArea;

    pArea = GetFindAreaInCxt (pOspfCxt, pAreaId);
    if (pArea == NULL)
    {
        return NULL;
    }

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {

        if ((pArea->aAddrRange[u1Index].areaAggStatus !=
             INVALID)
            &&
            (UtilIpAddrMaskComp
             (*pAddr, pArea->aAddrRange[u1Index].ipAddr,
              pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL))
        {

            return (&(pArea->aAddrRange[u1Index]));
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindAssoAreaInCxt                                      */
/*                                                                           */
/* Description  : This procedure associates the specified IP addr with one of*/
/*                the router's configured areas based on which address range */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pAddr         :  IP address                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to AREA, on success                                */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tArea       *
UtilFindAssoAreaInCxt (tOspfCxt * pOspfCxt, tIPADDR * pAddr)
{

    UINT1               u1Index;
    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {

            if ((pArea->aAddrRange[u1Index].areaAggStatus !=
                 INVALID)
                &&
                (UtilIpAddrMaskComp
                 (*pAddr, pArea->aAddrRange[u1Index].ipAddr,
                  pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL))
            {

                return (pArea);
            }
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilHashGetValue                                        */
/*                                                                           */
/* Description  : This procedure gets the hash value for the key             */
/*                                                                           */
/* Input        : u4HashSize    :  Max. no. of buckets                     */
/*                pKey           :  Pointer to information to be hashed     */
/*                u1KeyLen      :  Length of the information that is to    */
/*                                   be hashed.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hash Bucket value                                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
UtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen)
{

    UINT4               h = 0;
    UINT4               g;
    UINT1              *p;

    for (p = pKey; u1KeyLen; p++, u1KeyLen--)
    {

        h = (h << 4) + (*p);
        g = h & 0xf0000000;
        if (g)
        {

            h = h ^ (g >> 24);
            h = h ^ g;
            g = h & 0xf0000000;
        }
    }
    return h % u4HashSize;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilVirtIfIndComp                                      */
/*                                                                           */
/* Description  : compares the two virtual interface indices.                */
/*                                                                           */
/* Input        : areaId1    :  Area Id 1                                   */
/*                nbrId1     :  Neighbor Id 1                               */
/*                areaId2    :  Area Id 2                                   */
/*                nbrId2     :  Neighbor Id 2                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if the couples are equal                       */
/*                OSPF_GREATER, if couple1 is greater                        */
/*                OSPF_LESS, if couple2 is greater                           */
/*                couple is <Area Id, Neighbor Id>                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilVirtIfIndComp (tAreaId areaId1,
                   tRouterId nbrId1, tAreaId areaId2, tRouterId nbrId2)
{

    if (areaId1 == NULL)
        return OSPF_LESS;
    if (areaId2 == NULL)
        return OSPF_GREATER;

    switch (UtilIpAddrComp (areaId1, areaId2))
    {
        case OSPF_EQUAL:
            if (nbrId1 == NULL)
                return OSPF_LESS;
            if (nbrId2 == NULL)
                return OSPF_GREATER;
            return (UtilIpAddrComp (nbrId1, nbrId2));

        case OSPF_LESS:
            return OSPF_LESS;

        case OSPF_GREATER:
            return OSPF_GREATER;

        default:
            return INVALID;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindIf                                               */
/*                                                                           */
/* Description  : Determines the interface associated with index value       */
/*                u4IfIndex.                                               */
/*                                                                           */
/* Input        : u4IfIndex   : Index value whose interface is to be       */
/*                                determined.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the interface if exists.                        */
/*                NULL, otherwise.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
UtilFindIfInCxt (tOspfCxt * pOspfCxt, UINT4 u4IfIndex)
{
    tInterface         *pInterface;
    UINT4               u4HashIndex;

    u4HashIndex = IF_HASH_FN (u4IfIndex);

    TMO_HASH_Scan_Bucket (gOsRtr.pIfHashTable, u4HashIndex, pInterface,
                          tInterface *)
    {
        if (pInterface->u4IfIndex == u4IfIndex)
        {
            if ((pOspfCxt->u4OspfCxtId) ==
                (pInterface->pArea->pOspfCxt->u4OspfCxtId))
                return pInterface;
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilExtractLsHeaderFromPkt                            */
/*                                                                           */
/* Description  : Extracts the lsHeader information from OSPF packet        */
/*                (CRU_BUF_CHAIN).                                           */
/*                                                                           */
/* Input        : pPkt           : pointer to CRU_BUF_CHAIN_HEADER having   */
/*                                  OSPF packet.                             */
/*                pLsHeader     : Pointer to lsHeader                     */
/*                                                                           */
/* Output       : Filled lsHeader                                           */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilExtractLsHeaderFromPkt (tCRU_BUF_CHAIN_HEADER * pPkt,
                            tLsHeader * pLsHeader,
                            INT4 i4Offset, INT4 i4PktType)
{
    /* PktType = 0 DDP packet 
     * PktType = 1 LAK Packet */
    if (i4PktType == 0)
    {                            /* for the DDP packet header processing */
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_AGE_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaAge);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_OPTIONS_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaOptions);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_TYPE_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u1LsaType);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->linkStateId,
                                            (DDP_LSA_ID_OFFSET +
                                             (20 * i4Offset)), MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->advRtrId,
                                            (DDP_LSA_ADV_RTR_ID_OFFSET +
                                             (20 * i4Offset)), MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_SEQ_NUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaSeqNum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_CHKSUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaChksum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_LEN_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaLen);
    }
    else                        /* for the LAK packet header processing */
    {
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (LSA_AGE_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaAge);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (LSA_OPTIONS_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaOptions);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (LSA_TYPE_OFFSET + (20 * i4Offset)),
                                            pLsHeader->u1LsaType);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->linkStateId,
                                            (LSA_ID_OFFSET + (20 * i4Offset)),
                                            MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->advRtrId,
                                            (LSA_ADV_RTR_ID_OFFSET +
                                             (20 * i4Offset)), MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pPkt,
                                            (LSA_SEQ_NUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaSeqNum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (LSA_CHKSUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaChksum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (LSA_LEN_OFFSET + (20 * i4Offset)),
                                            pLsHeader->u2LsaLen);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilExtractLsHeaderFromLbuf                           */
/*                                                                           */
/* Description  : Extracts LSA information from linear buffer                */
/*                                                                           */
/* Input        : pLsa           : Pointer to linear buffer                 */
/*                pLsHeader     : Pointer to lsHeader                     */
/*                                                                           */
/* Output       : Filled lsHeader                                           */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilExtractLsHeaderFromLbuf (UINT1 *pLsa, tLsHeader * pLsHeader)
{

    pLsHeader->u2LsaAge = LGET2BYTE (pLsa);
    pLsHeader->lsaOptions = LGET1BYTE (pLsa);
    pLsHeader->u1LsaType = LGET1BYTE (pLsa);
    LGETSTR (pLsa, pLsHeader->linkStateId, MAX_IP_ADDR_LEN);
    LGETSTR (pLsa, pLsHeader->advRtrId, MAX_IP_ADDR_LEN);
    pLsHeader->lsaSeqNum = LGET4BYTE (pLsa);
    pLsHeader->u2LsaChksum = LGET2BYTE (pLsa);
    pLsHeader->u2LsaLen = LGET2BYTE (pLsa);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrNComp                                        */
/*                                                                           */
/* Description  : Compares a byte wise comparison to a max of u1Len.        */
/*                                                                           */
/* Input        : pIpAddr1     :  IP address 1                             */
/*                pIpAddr2     :  IP address 2                             */
/*                u1Len         :  n no. of bytes to be compared            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :  OSPF_EQUAL, if the IP addresses are equal                 */
/*                 OSPF_GREATER, if IP address 1 is greater                  */
/*                 OSPF_LESS, if IP address 2 is greater                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrNComp (tIPADDR * pIpAddr1, tIPADDR * pIpAddr2, UINT1 u1Len)
{

    UINT1               u1Index;

    for (u1Index = 0; u1Index < u1Len; u1Index++)
    {

        if (*((UINT1 *) ((UINT1 *) pIpAddr1 + u1Index)) <
            *((UINT1 *) ((UINT1 *) pIpAddr2 + u1Index)))
        {
            return OSPF_LESS;
        }
        else if (*((UINT1 *) ((UINT1 *) pIpAddr1 + u1Index)) >
                 *((UINT1 *) ((UINT1 *) pIpAddr2 + u1Index)))
        {
            return OSPF_GREATER;
        }
    }
    return OSPF_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilCalculateChksum                                      */
/*                                                                           */
/* Description  : This procedure calculates the checksum for the given OSPF  */
/*                packet. If the length of the packet is odd a dummy byte is */
/*                added at the end before calling the exact checksum calcula-*/
/*                tion routine.                                              */
/*                                                                           */
/* Input        : pBuf                         : pointer to message         */
/*                u2Len                        : length of LSA              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Checksum                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT2
UtilCalculateChksum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len)
{

    return (UtlIpCSumCruBuf (pBuf, u2Len, 0));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilMod255                                               */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                a modulo 255 operator using rule of 9's which is much      */
/*                faster than generic '%' (C mod operator)                   */
/*                                                                           */
/* Input        : i4Sum            : value whose mod is to be found         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Result of the mod operation                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
UtilMod255 (INT4 i4Sum)
{

    register UINT1     *pu1_ptr = (UINT1 *) &i4Sum;
    register INT4       i4Res = 0;
    INT1                i1Neg = OSPF_FALSE;

    if (i4Sum < 0)
    {

        i4Sum = -i4Sum;
        i1Neg = OSPF_TRUE;
    }
    i4Res = *pu1_ptr++;
    i4Res += *pu1_ptr++;
    i4Res += *pu1_ptr++;
    i4Res += *pu1_ptr;

    i4Res = (i4Res & 0xff) + (i4Res >> 8);
    i4Res = (i4Res & 0xff) + (i4Res >> 8);

    if (i4Res == 255)
        i4Res = 0;
    if (i1Neg == OSPF_TRUE)
        i4Res = ~i4Res;

    return (i4Res);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilComputeLsaFletChksum                               */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                Generates a fletcher's checksum and inserts in lsa.This    */
/*                procedure is written specifically for the OSPF lsa format. */
/*                The lsa should be present in a linear buffer.              */
/*                                                                           */
/* Input        : pLsa          : pointer to LSA                            */
/*                u2Len         : length of LSA                             */
/*                                                                           */
/* Output       : LSA updated with the computed fletched checksum            */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilComputeLsaFletChksum (UINT1 *pLsa, UINT2 u2Len)
{

    register INT4       i4C0 = 0;
    register INT4       i4C1 = 0;
    register INT4       i4Cmul;
    register UINT1     *pCurr;
    register UINT1     *pEnd;
    INT4                i4X;
    INT4                i4Y;

    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER) = 0;
    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER + 1) = 0;

    pEnd = pLsa + u2Len;

    /* age field is exempted from checksum calculation */

    for (pCurr = pLsa + AGE_SIZE_IN_LS_HEADER; pCurr < pEnd; pCurr++)
    {

        i4C0 += *pCurr;
        i4C1 += i4C0;
    }

    i4C0 = UtilMod255 (i4C0);
    i4C1 = UtilMod255 (i4C1);

    i4Cmul = (u2Len - CHKSUM_OFFSET_IN_LS_HEADER) * (long) i4C0;
    i4X = UtilMod255 (i4Cmul - i4C0 - i4C1);
    i4Y = UtilMod255 (i4C1 - i4Cmul);

    if (i4X == 0)
        i4X = 255;
    if (i4Y == 0)
        i4Y = 255;

    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER) = (UINT1) (i4X & 0xff);
    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER + 1) = (UINT1) (i4Y & 0xff);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilVerifyLsaFletChksum                                */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure verifies the checksum of the lsa. This proc-*/
/*                edure is written specifically for the OSPF lsa format. The */
/*                lsa is assumed to be in a linear buffer. This procedure    */
/*                returns OSPF_TRUE for success and OSPF_FALSE for checksum  */
/*                failure.                                                   */
/*                                                                           */
/* Input        : pLsa         : pointer to LSA                             */
/*                u2Len        : length of LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if checksum is ok                               */
/*                OSPF_FALSE, if checksum is not ok                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
UtilVerifyLsaFletChksum (UINT1 *pLsa, UINT2 u2Len)
{

    register UINT4      u4C0 = 0;
    register UINT4      u4C1 = 0;
    register UINT1     *pCurr;
    UINT2               u2ByteCount = 0;

    /* age field is exempted from checksum calculation */

    pCurr = pLsa + AGE_SIZE_IN_LS_HEADER;
    u2Len -= AGE_SIZE_IN_LS_HEADER;
    while (u2Len)
    {
        u2ByteCount = u2Len > 20 ? 20 : u2Len;
        u2Len -= u2ByteCount;
        do
        {
            u4C0 += *pCurr;
            u4C1 += u4C0;
            pCurr++;
        }
        while (--u2ByteCount);

        /* mod 255 adjustment */

        u4C0 = UtilMod255 (u4C0);
        u4C1 = UtilMod255 (u4C1);
    }
    if (!u4C0 && !u4C1)
    {

        return (OSPF_TRUE);
    }

    return (OSPF_FALSE);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindIsTransitArea                                */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure verifies if area is transit are-*/
/*                                                                           */
/* Input        : pArea         : pointer to area                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if transit area                               */
/*                OSPF_FALSE, if not transit area                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
UtilFindIsTransitArea (tArea * pArea)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;

    TMO_SLL_Scan (&(pArea->pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (UtilIpAddrComp (pInterface->transitAreaId, pArea->areaId)
            == OSPF_EQUAL)
        {
            return OSPF_TRUE;
        }
    }

    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilJitter                                                */
/*                                                                           */
/* Description  : Determines the jitter value based on the jitter percent    */
/*                and adds it to the value to be jittered.                   */
/*                                                                           */
/* Input        : u4Value            :  value to be jittered                */
/*                u4JitterPcent     :  jitter percent                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Modified value after jittering                             */
/*                                                                           */
/*****************************************************************************/

UINT4
UtilJitter (UINT4 u4Value, UINT4 u4JitterPcent)
{
    UINT4               u4Temp;
    static UINT4        su4Next = 1;

    if (u4Value < OSPF_MIN_TIMER_VAL)
    {
        u4JitterPcent =
            OSPF_MAX_JITTER - (UtilCeil (u4Value / OSPF_DIV_FACTOR));
    }

    su4Next = su4Next * 1103515245 + 12345;
    u4Temp =
        (((100
           - u4JitterPcent) +
          ((su4Next / 65536) % u4JitterPcent)) * (u4Value)) / 100;
    if (u4Temp == 0)
    {
        u4Temp = 1;
    }
    return (u4Temp);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilAddrMaskCmpe                                        */
/*                                                                           */
/* Description  : This procedure compares two ip address after applying the  */
/*                mask.                                                      */
/*                                                                           */
/* Input        : addr1 : IP address 1                                       */
/*                mask1 : IP address mask 1                                  */
/*                addr2 : IP address 2                                       */
/*                mask2 : IP address mask 2                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if both are equal                              */
/*                OSPF_GREATER, if addr1 and mask1 is greater                */
/*                OSPF_LESS, if addr2 and mask2 is greater                   */
/*****************************************************************************/

PUBLIC UINT1
UtilAddrMaskCmpe (UINT4 addr1, UINT4 mask1, UINT4 addr2, UINT4 mask2)
{

    UINT4               u4Addr1, u4Addr2;

    /* returns OSPF_EQUAL if they are equal
     *         OSPF_GREATER if addr1 and mask1 is greater
     *         OSPF_LESS if addr1 and mask1 is lesser
     */

    u4Addr1 = (addr1) & (mask1);
    u4Addr2 = (addr2) & (mask2);

    if (u4Addr1 > u4Addr2)
        return OSPF_GREATER;
    else if (u4Addr1 < u4Addr2)
        return OSPF_LESS;

    return (OSPF_EQUAL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetFindHost                                                */
/*                                                                           */
/* Description  : This procedure Create the host Entry with default values   */
/*                otherwise returns the already present Entry                */
/*                                                                           */
/* Input        : pHostAddr : Pointer to Host Ip Address.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to Host Entry Created or Already present           */
/*                Otherwise returns NULL if the creation of new host         */
/*                entry failes.                                              */
/*****************************************************************************/
PUBLIC tHost       *
SetFindHostInCxt (tOspfCxt * pOspfCxt, tIPADDR * pHostAddr)
{
    tHost              *pHost;

    if ((pHost = GetFindHostInCxt (pOspfCxt, pHostAddr)) == NULL)
    {
        /* If entry is not found new one is created */
        if ((pHost = HostAddInCxt (pOspfCxt, pHostAddr, 0)) == NULL)
        {
            return NULL;
        }
    }
    return pHost;
}

PUBLIC VOID
AuthKeyCopy (UINT1 *pu1Dst, UINT1 *pu1Src, INT4 i4Len)
{
    INT4                i4Count;

    for (i4Count = 0; i4Count < MAX_AUTHKEY_LEN; i4Count++)
    {
        if (i4Count < i4Len)
            *(pu1Dst + i4Count) = *(pu1Src + i4Count);
        else
            *(pu1Dst + i4Count) = (UINT1) 0;
    }

}

PUBLIC tMd5AuthkeyInfo *
Md5AuthKeyInfoCreate ()
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    UINT4               u4Ospf1sTimer = 0;
    tUtlTm              tm;

    if (MD5AUTH_ALLOC (pAuthkeyInfo) == NULL)
        return NULL;

    /* Getting the system time for u4Ospf1sTimer */
    UtlGetTime (&tm);
    u4Ospf1sTimer = GrGetSecondsSinceBase (tm);

/* Default values for start accept and generate is assigned to system time */
    pAuthkeyInfo->u4KeyStartAccept = u4Ospf1sTimer;
    pAuthkeyInfo->u4KeyStartGenerate = u4Ospf1sTimer;
    pAuthkeyInfo->u4KeyStopGenerate = DEF_KEY_STOP_GENERATE;
    pAuthkeyInfo->u4KeyStopAccept = DEF_KEY_STOP_ACCEPT;
    pAuthkeyInfo->u1AuthkeyStatus = AUTHKEY_STATUS_VALID;
    return (pAuthkeyInfo);
}

PUBLIC VOID
SortInsertMd5AuthKeyInfo (tTMO_SLL * pMd5authkeyLst,
                          tMd5AuthkeyInfo * pAuthkeyInfo)
{
    UINT1               u1AuthkeyId;
    tTMO_SLL_NODE      *pPrev = NULL;
    tMd5AuthkeyInfo    *pTmpAuthkeyInfo;
    tTMO_SLL_NODE      *pLstNode;

    u1AuthkeyId = pAuthkeyInfo->u1AuthkeyId;
    TMO_SLL_Scan (pMd5authkeyLst, pLstNode, tTMO_SLL_NODE *)
    {
        pTmpAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
        if (pTmpAuthkeyInfo->u1AuthkeyId < u1AuthkeyId)
            pPrev = pLstNode;
        if (pTmpAuthkeyInfo->u1AuthkeyId > u1AuthkeyId)
            break;
    }
    TMO_SLL_Insert (pMd5authkeyLst, pPrev, &(pAuthkeyInfo->nextSortKey));
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RrdConfRtInfoCreate                                     */
/*                                                                           */
/* Description     : This procedure allocates memory for                     */
/*                   tRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : pRrdDestIPAddr    - Dest IP Address                     */
/*                   pRrdDestAddrMask  - Dest IP Addr Mask                   */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to the new entry, if successful                 */
/*                   NULL, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tRedistrConfigRouteInfo *
RrdConfRtInfoCreateInCxt (tOspfCxt * pOspfCxt, tIPADDR * pRrdDestIPAddr,
                          tIPADDRMASK * pRrdDestAddrMask)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tRedistrConfigRouteInfo *pLstRrdConfRtInfo;
    tRedistrConfigRouteInfo *pPrvRrdConfRtInfo;
    UINT1               u1FoundFlag;

    u1FoundFlag = OSPF_FALSE;
    pPrvRrdConfRtInfo = NULL;

    if ((REDISTR_CONFIG_INFO_ALLOC (pRrdConfRtInfo)) == NULL)
    {

        return NULL;
    }

    IP_ADDR_COPY (pRrdConfRtInfo->rrdDestIPAddr, *pRrdDestIPAddr);
    IP_ADDR_COPY (pRrdConfRtInfo->rrdDestAddrMask, *pRrdDestAddrMask);

    /* Initialize to the default values */
    pRrdConfRtInfo->u4RrdRouteMetricValue = AS_EXT_DEF_METRIC;
    pRrdConfRtInfo->u1RrdRouteMetricType = TYPE_2_METRIC;
    pRrdConfRtInfo->u1RedistrTagType = MANUAL_TAG;
    pRrdConfRtInfo->u4ManualTagValue = 0;

    /* Add to the SLL in ascending order */
    TMO_SLL_Scan (&(pOspfCxt->RedistrRouteConfigLst), pLstRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {

        switch (UtilIpAddrComp (pRrdConfRtInfo->rrdDestIPAddr,
                                pLstRrdConfRtInfo->rrdDestIPAddr))
        {
            case OSPF_LESS:
                u1FoundFlag = OSPF_TRUE;
                break;

            case OSPF_EQUAL:
                switch (UtilIpAddrComp (pRrdConfRtInfo->rrdDestAddrMask,
                                        pLstRrdConfRtInfo->rrdDestAddrMask))
                {
                    case OSPF_LESS:
                        u1FoundFlag = OSPF_TRUE;
                        break;

                    case OSPF_EQUAL:
                        REDISTR_CONFIG_INFO_FREE (pRrdConfRtInfo);
                        return (pLstRrdConfRtInfo);

                    case OSPF_GREATER:
                        pPrvRrdConfRtInfo = pLstRrdConfRtInfo;
                        break;
                }                /* switch 2 */
                break;

            case OSPF_GREATER:
                pPrvRrdConfRtInfo = pLstRrdConfRtInfo;
                break;
        }                        /* switch 1 */

        if (u1FoundFlag == OSPF_TRUE)
            break;
    }

    TMO_SLL_Insert (&(pOspfCxt->RedistrRouteConfigLst),
                    &(pPrvRrdConfRtInfo->nextRrdConfigRoute),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    pOspfCxt->pLastConfigRouteInfo = NULL;

    return pRrdConfRtInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RrdConfRtInfoDeleteAll                                 */
/*                                                                           */
/* Description     : This procedure deallocates memory for                     */
/*                   tRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : None                                                  */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RrdConfRtInfoDeleteAllInCxt (tOspfCxt * pOspfCxt)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;

    TMO_SLL_Scan (&(pOspfCxt->RedistrRouteConfigLst), pRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {
        RrdConfRtInfoDeleteInCxt (pOspfCxt,
                                  &pRrdConfRtInfo->rrdDestIPAddr,
                                  &pRrdConfRtInfo->rrdDestAddrMask);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RrdConfRtInfoDelete                                     */
/*                                                                           */
/* Description     : This procedure deallocates memory for                   */
/*                   tRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : pRrdDestIPAddr    - Dest IP Address                     */
/*                   pRrdDestAddrMask  - Dest IP Addr Mask                   */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RrdConfRtInfoDeleteInCxt (tOspfCxt * pOspfCxt, tIPADDR * pRrdDestIPAddr,
                          tIPADDRMASK * pRrdDestAddrMask)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;

    if ((pRrdConfRtInfo = GetFindRrdConfRtInfoInCxt (pOspfCxt,
                                                     pRrdDestIPAddr,
                                                     pRrdDestAddrMask)) == NULL)
    {
        gu4GetFindRrdConfRtInfoInCxtFail++;
        return OSPF_FAILURE;
    }

    TMO_SLL_Delete (&(pOspfCxt->RedistrRouteConfigLst),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    pOspfCxt->pLastConfigRouteInfo = NULL;

    REDISTR_CONFIG_INFO_FREE (pRrdConfRtInfo);

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindAssoAllAreaAddrRange                             */
/*                                                                           */
/* Description  : This procedure associates the specified IP addr with one of*/
/*                the router's configured areas based on which address range */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pAddr        : IP address                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddrRange  *
UtilFindAssoAllAreaAddrRangeInCxt (tOspfCxt * pOspfCxt, tIPADDR * pAddr)
{

    UINT1               u1Index;
    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {

            if ((pArea->aAddrRange[u1Index].areaAggStatus !=
                 INVALID)
                &&
                (UtilIpAddrMaskComp
                 (*pAddr, pArea->aAddrRange[u1Index].ipAddr,
                  pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL))
            {

                return (&(pArea->aAddrRange[u1Index]));
            }
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfSetContext                                         */
/*                                                                           */
/* Description  : This function sets the current ospf                        */
/*                context pointer (gOsRtr.pOspfCxt)                          */
/*                and current ospf context ID (gOsRtr.u4OspfCxtId)           */
/*                 for the given ospf Context ID                             */
/*                                                                           */
/* Input        :  u4OspfCxtId  : Ospf Context Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_FAILURE/SNMP_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfSetContext (UINT4 u4OspfCxtId)
{
    if (UtilOspfGetVcmSystemMode (OSPF_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if ((UtilOspfVcmIsVcExist (u4OspfCxtId) == OSPF_FAILURE))
        {
            return SNMP_FAILURE;
        }
        OSPF_CXT_LOCK ();
        if ((gOsRtr.pOspfCxt = gOsRtr.apOspfCxt[u4OspfCxtId]) == NULL)
        {
            OSPF_CXT_UNLOCK ();
            return SNMP_FAILURE;
        }
        gOsRtr.u4OspfCxtId = u4OspfCxtId;
        OSPF_CXT_UNLOCK ();
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfResetContext                                       */
/*                                                                           */
/* Description  : This function resets the current ospf                      */
/*                context pointer (gOsRtr.pOspfCxt)                          */
/*                and current ospf context ID (gOsRtr.u4OspfCxtId)           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilOspfResetContext ()
{
    OSPF_CXT_LOCK ();
    if (UtilOspfGetVcmSystemMode (OSPF_PROTOCOL_ID) == VCM_MI_MODE)
    {
        gOsRtr.pOspfCxt = NULL;
        gOsRtr.u4OspfCxtId = OSPF_INVALID_CXT_ID;
    }
    OSPF_CXT_UNLOCK ();
    return;
}

/**************************************************************************/
/*                                                                        */
/* Function Name     :  UtilOspfReleaseContext                            */
/*                                                                        */
/* Description       :  Resets the Global Context pointer in web pages    */
/*                                                                        */
/* Input(s)          :  None                                              */
/*                                                                        */
/* Output(s)         :  None.                                             */
/*                                                                        */
/* Returns           :  OSPF_SUCCESS                                      */
/*                                                                        */
/**************************************************************************/
PUBLIC INT4
UtilOspfReleaseContext ()
{
    UtilOspfResetContext ();
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfGetCxt                                             */
/*                                                                           */
/* Description  : This function finds and returns the pointer to the         */
/*                ospf context structure for the given ospf context ID       */
/*                                                                           */
/* Input        :  u4OspfCxtId  : Ospf Context Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to ospf context structure, if context is found     */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tOspfCxt    *
UtilOspfGetCxt (UINT4 u4OspfCxtId)
{
    if ((UtilOspfVcmIsVcExist (u4OspfCxtId) == OSPF_FAILURE) ||
        (gOsRtr.apOspfCxt[u4OspfCxtId] == NULL))
    {
        gu4UtilOspfVcmIsVcExistFail++;
        return NULL;
    }
    return (gOsRtr.apOspfCxt[u4OspfCxtId]);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfGetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the first valid ospf context ID        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu4OspfCxtId                                               */
/*                                                                           */
/* Returns      : OSPF_FAILURE/OSPF_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfGetFirstCxtId (UINT4 *pu4OspfCxtId)
{
    UINT4               u4OspfCxtId = 0;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
    {
        if ((gOsRtr.apOspfCxt[u4OspfCxtId]) != NULL)
        {
            *pu4OspfCxtId = u4OspfCxtId;
            return OSPF_SUCCESS;

        }
    }
    gu4UtilOspfGetFirstCxtIdFail++;
    return OSPF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfGetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the next valid ospf context ID         */
/*                                                                           */
/* Input        : u4OspfCxtId  : Ospf Context Id                             */
/*                                                                           */
/* Output       : pu4NextOspfCxtId                                           */
/*                                                                           */
/* Returns      : OSPF_FAILURE/OSPF_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
UtilOspfGetNextCxtId (UINT4 u4OspfCxtId, UINT4 *pu4NextOspfCxtId)
{
    UINT4               u4CurOspfCxtId = 0;
    UINT4               u4MaxCxt = 0;

    u4OspfCxtId++;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4CurOspfCxtId = u4OspfCxtId; u4CurOspfCxtId < u4MaxCxt;
         u4CurOspfCxtId++)
    {
        if ((gOsRtr.apOspfCxt[u4CurOspfCxtId]) != NULL)
        {
            *pu4NextOspfCxtId = u4CurOspfCxtId;
            return OSPF_SUCCESS;

        }
    }
    gu4UtilOspfGetNextCxtIdFail++;
    return OSPF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfIsValidCxtId                                       */
/*                                                                           */
/* Description  : This function checks whether the given context ID          */
/*                is valid OSPF CONTEXT ID  or INVALID CONTEXT ID            */
/*                                                                           */
/* Input        : u4OspfCxtId  : Ospf Context Id                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_FAILURE/OSPF_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfIsValidCxtId (UINT4 u4OspfCxtId)
{
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (u4OspfCxtId < u4MaxCxt)
    {
        if ((gOsRtr.apOspfCxt[u4OspfCxtId]) != NULL)
        {
            return OSPF_SUCCESS;
        }
    }
    return OSPF_FAILURE;
}

PUBLIC UINT4
UtilOspfGetTraceFlag (UINT4 u4OspfCxtId)
{
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        return (gOsRtr.u4OspfGblTrace);
    }

    if (u4OspfCxtId >= u4MaxCxt)
    {
        return 0;
    }

    if ((UtilOspfVcmIsVcExist (u4OspfCxtId) == OSPF_FAILURE) ||
        (gOsRtr.apOspfCxt[u4OspfCxtId] == NULL))
    {
        return 0;
    }
    return (gOsRtr.apOspfCxt[u4OspfCxtId])->u4OspfTrace;
}

PUBLIC INT4
UtilOspfVcmIsVcExist (UINT4 u4OspfCxtId)
{
    if (UtilOspfGetVcmSystemMode (OSPF_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsL3VcExist (u4OspfCxtId) == VCM_FALSE)
        {
            gu4VcmIsL3VcExistFail++;
            return OSPF_FAILURE;
        }
    }
    else
    {
        if (u4OspfCxtId != OSPF_DEFAULT_CXT_ID)
        {
            gu4UtilOspfVcmIsVcExistFail++;
            return OSPF_FAILURE;
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfIsVcmSwitchExist                           */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Switch-name in VCM.           */
/*                        if yes it will                                     */
/*                        return the Context-Id of the Switch.               */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : OSPF_SUCCESS/OSPF_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfIsVcmSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    if (UtilOspfGetVcmSystemMode (OSPF_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsVrfExist (pu1Alias, pu4VcNum) == VCM_FALSE)
        {
            return OSPF_FAILURE;
        }
    }
    else
    {
        if (STRCMP (pu1Alias, "default") == 0)
        {
            *pu4VcNum = OSPF_DEFAULT_CXT_ID;
        }
        else
        {
            *pu4VcNum = OSPF_INVALID_CXT_ID;
            return OSPF_FAILURE;
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfGetVcmAliasName                            */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : OSPF_SUCCESS / OSPF_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfGetVcmAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    if (UtilOspfGetVcmSystemMode (OSPF_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_FAILURE)
        {
            return OSPF_FAILURE;
        }
    }
    else
    {
        if (u4ContextId == OSPF_DEFAULT_CXT_ID)
        {
            STRNCPY (pu1Alias, "default", STRLEN ("default"));
            pu1Alias[STRLEN ("default")] = '\0';
        }
        else
        {
            return OSPF_FAILURE;
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfIsLowPriorityMessage                       */
/*                                                                           */
/*     DESCRIPTION      : This function checks the priority of the           */
/*                        Ospf message type                                  */
/*                                                                           */
/*     INPUT            : u4MsgType    - Ospf message type                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSPF_TRUE if message type is Low priority          */
/*                        OSPF_FALSE if message type is High priority        */
/*****************************************************************************/
PUBLIC INT4
UtilOspfIsLowPriorityMessage (UINT4 u4MsgType)
{
    if ((u4MsgType == OSPF_IP_IF_MSG) || (u4MsgType == OSPF_VCM_MSG_RCVD))
    {
        /* this is a low priority message */
        return OSPF_TRUE;
    }
    /* Note : If priority based Q support is not needed 
     *        then classify all the message as High priority Message
     */
    gu4UtilOspfIsLowPriorityMessageFail++;
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfIsLowPriorityOspfPkt                       */
/*                                                                           */
/*     DESCRIPTION      : This function checks the priority of the           */
/*                        Ospf packets                                       */
/*                                                                           */
/*     INPUT            : u1PakType    - Ospf packet type                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSPF_TRUE if Ospf packet type is Low priority      */
/*                        OSPF_FALSE if Ospf packet type is High priority    */
/*****************************************************************************/
PUBLIC INT4
UtilOspfIsLowPriorityOspfPkt (UINT1 u1OspfPktType)
{
    if ((u1OspfPktType == DD_PKT) ||
        (u1OspfPktType == LS_UPDATE_PKT) || (u1OspfPktType == LSA_REQ_PKT))
    {
        /* this is a low priority packet */
        return OSPF_TRUE;
    }
    /* Note : If priority based Q support is not needed 
     *        then classify all the packets as High priority packets
     */
    return OSPF_FALSE;
}

#ifdef HIGH_PERF_RXMT_LST_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfBitSet                                     */
/*                                                                           */
/*     DESCRIPTION      : This function is used to set the given value       */
/*                        in the corresponding bit position.                 */
/*                                                                           */
/*     INPUT            : pu1RxmtInfo  - array in which the no is to be      */
/*                                       stored                              */
/*                        u4DllNodeSegment - bits segment to be set          */
/*                        u4value        - value to be stored                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
PUBLIC VOID
UtilOspfBitSet (UINT1 *pu1RxmtInfo, UINT4 u4DllNodeSegment, UINT4 u4value)
{
    UINT4               u4StartBit;    /* First bit in this Segment */
    UINT4               u4NoSetBits = 0;    /* No of bits added in array */
    UINT4               u4NumBitsToSet = 0;    /* No of bits to be added in the array */
    UINT4               u4Index = 0;    /* Array Index */
    UINT4               u4TotalBits = MAX_RXMT_NODE_SIZE;    /* Maximum bits in a Segment */
    UINT4               u4EndBit;    /* Last bit to be added in a byte */
    UINT1               u1Mask = 0;    /* Indicates the bits that has to be set 
                                       in the byte */

    /* u4DllNodeSegment  takes four values
     * 1 - For identifing the Next Index for LSA_DLL_HEAD/NBR_DLL_HEAD/
     *                                       NBR_DLL_NEXT_INDEX
     * 2 - For identifing the Prev Index for LSA_DLL_HEAD/NBR_DLL_HEAD/
     *                                       NBR_DLL_PREV_INDEX
     * 3 - For identifing the Next Next for LSA_DLL_NEXT_INDEX
     * 4 - For identifing the Prev Next for LSA_DLL_PREV_INDEX
     */
    /* StartBit - Starting bit position from which the value will be copied 
       to the array */
    u4StartBit = u4TotalBits * (u4DllNodeSegment - 1);

    u4NumBitsToSet =
        ((8 - (u4StartBit % 8)) <=
         u4TotalBits) ? (8 - (u4StartBit % 8)) : u4TotalBits;
    u4EndBit = (u4StartBit % 8) + u4NumBitsToSet;

    /* Masks the bits that needs to altered in this byte */
    u1Mask = (UINT1) ((0xff << (u4StartBit % 8)) & (0xff >> (8 - u4EndBit)));

    /* Finds the index containing the starting bit position */
    u4Index = u4StartBit / 8;
    pu1RxmtInfo[u4Index] = (UINT1) ((pu1RxmtInfo[u4Index] & ~u1Mask) |
                                    (u4value << u4StartBit % 8));

    u4NoSetBits = u4NumBitsToSet;
    while (u4NoSetBits != u4TotalBits)
    {
        u4Index++;
        u4NumBitsToSet =
            (((u4TotalBits - u4NoSetBits) / 8) ? 8 : ((u4TotalBits -
                                                       u4NoSetBits) % 8));

        /* Masks the bits that needs to altered in this byte */
        u1Mask = (UINT1) (0xff >> (8 - u4NumBitsToSet));
        pu1RxmtInfo[u4Index] = (UINT1) ((pu1RxmtInfo[u4Index] & ~u1Mask) |
                                        (u4value >> u4NoSetBits));
        u4NoSetBits = u4NoSetBits + u4NumBitsToSet;
    }
}
#endif

#ifdef HIGH_PERF_RXMT_LST_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfBitGet                                     */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the value             */
/*                        from the corresponding bits.                       */
/*                                                                           */
/*     INPUT            : pu1RxmtInfo  - array in which the no is to be      */
/*                                       stored                              */
/*                        u4DllNodeSegment - Bits segment from which         */
/*                                           value will be fetched           */
/*                                                                           */
/*     OUTPUT           : pu4Value - The value retrieved                     */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/

PUBLIC VOID
UtilOspfBitGet (UINT1 *pu1RxmtInfo, UINT4 u4DllNodeSegment, UINT4 *pu4Value)
{
    UINT4               u4StartBit;    /* First bit in this segment of bits */
    UINT4               u4Index = 0;    /* No of bits added in the array */
    UINT4               u4NoGetBits = 0;    /* No of bits to be added in the array */
    UINT4               u4NumBitsToGet = 0;    /* Array Index */
    UINT4               u4TotalBits = MAX_RXMT_NODE_SIZE;    /* Maximum bits in this segment of bits */
    UINT4               u4Temp = 0;    /* Temporary variable used for shifting */

    /* StartBit - Starting bit position from which the value will be fetched
       from the array */
    u4StartBit = u4TotalBits * (u4DllNodeSegment - 1);

    /* Finds the index containing the starting bit position */
    u4Index = u4StartBit / 8;
    u4NumBitsToGet =
        ((8 - (u4StartBit % 8)) <=
         u4TotalBits) ? (8 - (u4StartBit % 8)) : u4TotalBits;
    u4NoGetBits = u4StartBit % 8;

    *pu4Value =
        (pu1RxmtInfo[u4Index] >> u4NoGetBits) & ((1 << u4NumBitsToGet) - 1);
    u4NoGetBits = u4NumBitsToGet;

    while (u4NoGetBits != u4TotalBits)
    {
        u4Index++;
        u4NumBitsToGet =
            ((u4TotalBits - u4NoGetBits) / 8) ? 8 : ((u4TotalBits -
                                                      u4NoGetBits) % 8);
        u4Temp = pu1RxmtInfo[u4Index] & ((1 << u4NumBitsToGet) - 1);
        *pu4Value = (u4Temp << u4NoGetBits) | *pu4Value;
        u4NoGetBits = u4NoGetBits + u4NumBitsToGet;
    }
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfGetVcmSystemMode                           */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfGetVcmSystemModeExt                        */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilOspfGetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/***************************************************************/
/*  Function Name   :UtilOspfGetFirstCxt                       */
/*  Description     :This function used to get the first       */
/*                   contest                                   */
/*  Input(s)        :None                                      */
/*                                                             */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :None                                      */
/***************************************************************/
tOspfCxt           *
UtilOspfGetFirstCxt (VOID)
{
    UINT4               u4OspfCxtId;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
    {
        if ((gOsRtr.apOspfCxt[u4OspfCxtId]) != NULL)
        {
            return gOsRtr.apOspfCxt[u4OspfCxtId];

        }
    }
    return NULL;

}

/***************************************************************/
/*  Function Name   :UtilOspfGetNextCxt                        */
/*  Description     :This function used to get the next        */
/*                   context of given context id               */
/*  Input(s)        :u4OspfCxtId - Context Id                  */
/*                                                             */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :None                                      */
/***************************************************************/
tOspfCxt           *
UtilOspfGetNextCxt (UINT4 u4OspfCxtId)
{
    UINT4               u4CurOspfCxtId;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    u4OspfCxtId++;
    for (u4CurOspfCxtId = u4OspfCxtId; u4CurOspfCxtId < u4MaxCxt;
         u4CurOspfCxtId++)
    {
        if ((gOsRtr.apOspfCxt[u4CurOspfCxtId]) != NULL)
        {
            return gOsRtr.apOspfCxt[u4CurOspfCxtId];

        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfGetNextAreaInCxt                                   */
/*                                                                           */
/* Description  : This function returns the next area of given area-id       */
/*                                                                           */
/* Input        : pOspfCxt : Ospf Context                                    */
/*                areaId   : Area Id                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the area/NULL                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC tArea       *
UtilOspfGetNextAreaInCxt (tOspfCxt * pOspfCxt, tAreaId areaId)
{
    tArea              *pArea = NULL;
    INT4                i4RetVal = NOT_VALID;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        i4RetVal = UtilIpAddrComp (areaId, pArea->areaId);

        if (i4RetVal == OSPF_LESS)
        {
            break;
        }
    }
    return pArea;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfGetNextInterfaceInCxt                              */
/*                                                                           */
/* Description  : This function returns the next nterface of given interface */
/*                IP address and interface index                             */
/*                                                                           */
/* Input        : pOspfCxt : Pointer to Ospf Context                         */
/*                ifIpAddr : Interface IP address                            */
/*                u4AddrlessIf : Interface index.                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the Interface/NULL                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tInterface  *
UtilOspfGetNextInterfaceInCxt (tOspfCxt * pOspfCxt, tIPADDR ifIpAddr,
                               UINT4 u4AddrlessIf)
{
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    INT4                i4RetVal = NOT_VALID;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pNode);

        i4RetVal = UtilIpAddrIndComp (ifIpAddr, u4AddrlessIf,
                                      pInterface->ifIpAddr,
                                      pInterface->u4AddrlessIf);
        if (i4RetVal == OSPF_LESS)
        {
            break;
        }
        pInterface = NULL;
    }
    return pInterface;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfGetExtTraceFlag                            */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the tarce object      */
/*                        for OSPF graceful restart                          */
/*                                                                           */
/*     INPUT            : u4OspfCxtId  - Context ID                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
PUBLIC UINT4
UtilOspfGetExtTraceFlag (UINT4 u4OspfCxtId)
{
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        return (gOsRtr.u4OspfGblExtTrace);
    }

    if (u4OspfCxtId >= u4MaxCxt)
    {
        return 0;
    }

    if ((UtilOspfVcmIsVcExist (u4OspfCxtId) == OSPF_FAILURE) ||
        (gOsRtr.apOspfCxt[u4OspfCxtId] == NULL))
    {
        return 0;
    }
    return (gOsRtr.apOspfCxt[u4OspfCxtId])->u4OspfExtTrace;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfGetIfIndex                                 */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the IfIndex for       */
/*                        gven address and addressless  if index             */
/*                                                                           */
/*     INPUT            : u4OspfCxtId  - Context ID                          */
/*                      : u4Address  - Address                               */
/*                      : i4AddresslessIf - address less if index            */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex - InterfaceIndex                        */
/*                                                                           */
/*     RETURNS          : OSPF_FAILURE, OSPF_SUCCESS                         */
/*****************************************************************************/
PUBLIC UINT4
UtilOspfGetIfIndex (UINT4 u4OspfCxtId, UINT4 u4Address,
                    INT4 i4AddresslessIf, UINT4 *pu4IfIndex)
{
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;
    tIPADDR             ifIpAddr;

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return ((UINT4) (OSPF_FAILURE));
    }
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4Address);
    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4AddresslessIf);
    if (pInterface == NULL)
    {
        return ((UINT4) (OSPF_FAILURE));
    }
    *pu4IfIndex = pInterface->u4IfIndex;
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilConstRtrLsaRBTree                                      */
/*                                                                           */
/* Description  : This procedure populates router lsa link RBTree for GR     */
/*                                                                           */
/* Input        : pLsa          : pointer to lsa                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilConstRtrLsaRBTree (UINT1 *pLsa)
{
    tRtrLsaRtInfo      *pRtrLsaLink = NULL;

    UINT1              *pTmpLsa = NULL;
    UINT2               u2NoOfRouterLSALinks;
    INT4                u4Cnt = 0;
    UINT1               u1TosCnt = 0;
    UINT1               u1Tos = 0;

    pTmpLsa = pLsa;

    pTmpLsa = pTmpLsa + LS_HEADER_SIZE + 1;

    /* Compare the  No.of Links in the Router LSA */
    u2NoOfRouterLSALinks = LGET2BYTE (pTmpLsa);

    /* Compare the  LinkType, LinkID and LinkData */
    /* Construct the Router LSA RBTree with the Router LSA derived from OSPF LSDB */
    for (u4Cnt = 0; u4Cnt < u2NoOfRouterLSALinks; u4Cnt++)
    {
        pRtrLsaLink = NULL;
        RTR_LSA_LINK_ALLOC (pRtrLsaLink);

        if (pRtrLsaLink == NULL)
        {
            RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);
            return;
        }
        MEMSET (pRtrLsaLink, 0, sizeof (tRtrLsaRtInfo));

        pRtrLsaLink->u4LinkId = LGET4BYTE (pTmpLsa);
        pRtrLsaLink->u4LinkData = LGET4BYTE (pTmpLsa);
        pRtrLsaLink->u1Type = LGET1BYTE (pTmpLsa);
        u1TosCnt = LGET1BYTE (pTmpLsa);
        pRtrLsaLink->u4Metric[TOS_0] = LGET2BYTE (pTmpLsa);
        while (u1TosCnt--)
        {
            u1Tos = LGET1BYTE (pTmpLsa);
            /* Skip one unused byte */
            pTmpLsa = pTmpLsa + 1;
            pRtrLsaLink->u4Metric[DECODE_TOS (u1Tos)] = LGET2BYTE (pTmpLsa);
        }

        if (RBTreeAdd (gOsRtr.pRtrLsaCheck, pRtrLsaLink) == RB_FAILURE)
        {
            RBTreeDrain (gOsRtr.pRtrLsaCheck, UtilRBFreeRouterLinks, 0);
            return;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRBFreeRouterLinks                                      */
/*                                                                           */
/* Description  : Frees router lsa link nodes used for GR consistency check  */
/*                                                                           */
/* Input        : pRBElem- Pointer to the Router Lsa Link                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
UtilRBFreeRouterLinks (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        RTR_LSA_LINK_FREE (pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRBFreeNwLinks                                          */
/*                                                                           */
/* Description  : Frees network lsa link nodes used for GR consistency check */
/*                                                                           */
/* Input        : pRBElem- Pointer to the network Lsa Link                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
UtilRBFreeNwLinks (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        NW_LSA_LINK_FREE ((UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : GetfsMIStdOspfIfAuthKey                                 */
/*                                                                           */
/* Description     : This procedure gets the value of GetfsMIStdOspfIfAuthKey*/
/*                   from ospf date structure                                */
/*                                                                           */
/* Input           : u4OspfCxtId                                             */
/*                   u4OspfIfIpAddress                                       */
/*                   u4AddrlessIf                                            */
/* Output          : pOctetAuthKeyValue                                      */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
GetfsMIStdOspfIfAuthKey (UINT4 u4OspfCxtId, UINT4 u4OspfIfIpAddress,
                         UINT4 u4AddrlessIf,
                         tSNMP_OCTET_STRING_TYPE * pOctetAuthKeyValue)
{
    tInterface         *pInterface;
    tIPADDR             ifIpAddr;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return OSPF_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (gOsRtr.pOspfCxt, ifIpAddr,
                                      u4AddrlessIf)) == NULL)
    {
        UtilOspfResetContext ();
        return OSPF_FAILURE;

    }
    pOctetAuthKeyValue->i4_Length = STRLEN (pInterface->authKey);
    MEMCPY (pOctetAuthKeyValue->pu1_OctetList, pInterface->authKey,
            pOctetAuthKeyValue->i4_Length);
    UtilOspfResetContext ();
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilApplyRMapRule                                          */
/*                                                                           */
/* Description  : This procedure applies routemap rules to route entries that*/
/*                are already dropped by rmap rules                          */
/*                                                                           */
/* Input        : pAddr        : IP address                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilOspfApplyRMapRule (tOspfCxt * pOspfCxt)
{
    tRtEntry           *pRtEntry;
    tOsixMsg           *pBuf = NULL;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Status = 0;
    UINT2               u2Index = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN];

    TMO_SLL_Scan (&(pOspfCxt->pOspfRt->routesList), pRtEntry, tRtEntry *)
    {
        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if ((OSPF_INVALID_CXT_ID == gOsRtr.u4RTstaggeredCtxId)
                && (u4CurrentTime >= pOspfCxt->u4StaggeringDelta))
            {
                gOsRtr.u4RTstaggeredCtxId = pOspfCxt->u4OspfCxtId;
                OSPFRtcRelinquish (pOspfCxt);
            }
        }
        for (; u2Index < MAX_OSPF_RMAP_MSGS; u2Index++)
        {
            if (gOsRmapMsg[u2Index] != NULL)
            {
                pBuf = gOsRmapMsg[u2Index];
                if (CRU_BUF_Copy_FromBufChain
                    (pBuf, (UINT1 *) &u4Status, 0,
                     sizeof (u4Status)) != sizeof (u4Status))
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    gOsRmapMsg[u2Index] = NULL;
                    continue;
                }

                MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN));
                if (CRU_BUF_Copy_FromBufChain (pBuf, au1RMapName,
                                               sizeof (u4Status),
                                               RMAP_MAX_NAME_LEN) !=
                    RMAP_MAX_NAME_LEN)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    gOsRmapMsg[u2Index] = NULL;
                    continue;
                }
#ifdef ROUTEMAP_WANTED
                if (OspfSendRouteMapUpdateMsg (au1RMapName, u4Status)
                    == OSPF_FAILURE)
                {
                    break;
                }
#endif
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                gOsRmapMsg[u2Index] = NULL;
            }
        }

        if (pRtEntry->u1IsRmapAffected == OSPF_TRUE)
        {
            pRtEntry->u1IsRmapAffected = OSPF_FALSE;
            RtmTxRtUpdate (pOspfCxt, pRtEntry);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfDwordFromPdu                                       */
/*                                                                           */
/* Description  : This procedure converts Address which is in Network Byte   */
/*                Order to Host Byte Order and returns the same              */
/*                                                                           */
/* Input        : pu1PduAddr   : IP address in Network Byte Order            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address in Host Byte Order                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
UtilOspfDwordFromPdu (UINT1 *pu1PduAddr)
{
    UINT4               u4TempData = 0;
    MEMCPY (&u4TempData, pu1PduAddr, MAX_IPV4_ADDR_LEN);
    return (OSIX_NTOHL (u4TempData));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfDwordToPdu                                         */
/*                                                                           */
/* Description  : This procedure converts Address which is in Host Byte      */
/*                Order to Network Byte Order                                */
/*                                                                           */
/* Input        : u4Value      : IP Address in Host Byte Order               */
/*                                                                           */
/* Output       : pu1PduAddr                                                 */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilOspfDwordToPdu (UINT1 *pu1PduAddr, UINT4 u4Value)
{
    UINT4               u4TempData = 0;
    u4TempData = OSIX_HTONL (u4Value);
    MEMCPY (pu1PduAddr, &u4TempData, MAX_IPV4_ADDR_LEN);
}

/*****************************************************************************/
/* Function     : UtilSelectOspfRouterId                                     */
/* Description  : This procedure selecting router-id of smallest Ip          */
/*                of interface address.                                      */
/* Input        : None                                                       */
/* Output       : pAddr        : IP address                                  */
/* Returns      : success/failure                                            */
/*****************************************************************************/
PUBLIC INT4
UtilSelectOspfRouterId (UINT4 u4OspfCxtId, tRouterId * pRtrId)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4LoopbackAddr = 0xffffffff;
    UINT4               u4Addr = 0xffffffff;
    UINT4               u4IfAddr = 0;
    UINT1               u1LoopbackFlag = FALSE;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetFirstIfInfoInCxt (u4OspfCxtId,
                                    &NetIpIfInfo) == NETIPV4_SUCCESS)
    {
        do
        {
            u4IfIndex = NetIpIfInfo.u4IfIndex;
            u4IfAddr = NetIpIfInfo.u4Addr;

            if (NetIpIfInfo.u4ContextId == u4OspfCxtId)
            {
                if ((NetIpIfInfo.u4IfType == CFA_LOOPBACK)
                    && (u4IfAddr < u4LoopbackAddr) && (u4IfAddr != 0))
                {
                    /* highest priority is lowest loopback
                     * address should be select as ospf router-id */

                    u4LoopbackAddr = u4IfAddr;
                    u1LoopbackFlag = TRUE;
                }
                else
                {
#ifdef ICCH_WANTED
                    /* The Smallest IP Addres will choosen as Router Id. 
                     * Incase ICCH Is enabled on the system ICCH IP Address 
                     * shouldnt be choosen as RouterId  */

                    if (u4IfAddr != IcchApiGetIcclIpAddr (0))
#endif
                    {
                        if ((u1LoopbackFlag == FALSE)
                            && (u4IfAddr < u4Addr) && (u4IfAddr != 0))
                        {
                            u4Addr = u4IfAddr;
                        }
                    }
                }
            }

            MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        }
        while (NetIpv4GetNextIfInfoInCxt (u4OspfCxtId,
                                          u4IfIndex,
                                          &NetIpIfInfo) == NETIPV4_SUCCESS);
        if (u1LoopbackFlag == TRUE)
        {
            OSPF_CRU_BMC_DWTOPDU ((UINT1 *) pRtrId, u4LoopbackAddr);
        }
        else
        {
            if (u4Addr == 0xffffffff)
            {
                u4Addr = 0;
            }

            OSPF_CRU_BMC_DWTOPDU ((UINT1 *) pRtrId, u4Addr);
        }
        return OSPF_SUCCESS;
    }
    gu4UtilSelectOspfRouterIdFail++;
    return OSPF_FAILURE;
}

/************************************************************************
 *  Function Name   : OspfConvertTime
 *  Description     : This function will validate the time
              by parsing through the string against DD-MON-YEAR,HH:MM
              format and convert the time in string to structure.
 *  Input           : pu1TimeStr -
 *                    tm - Time structure to set or get time of key constants.
 *  Output          : None
 *  Returns         : OSPF_SUCCESS/OSPF_FAILURE
 ************************************************************************/

INT4
OspfConvertTime (UINT1 *pu1TimeStr, tUtlTm * tm)
{
    UINT1               au1Temp[OSPF_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (au1Temp, OSPF_ZERO, sizeof (au1Temp));
    /* Parsing through the function  for date */
    if (pu1CurrPtr != NULL)
    {
        /* Checking for date */
        MEMCPY (au1Temp, pu1BasePtr, 2);
        if ((ISDIGIT (pu1BasePtr[OSPF_ZERO]))    /* Check if date is an integer */
            && (ISDIGIT (pu1BasePtr[OSPF_ONE])))
        {
            tm->tm_mday = ATOI (au1Temp);
            tm->tm_yday = tm->tm_mday;
            /* Checking for the validity of Date */
            if ((tm->tm_mday < OSPF_ONE)
                || (tm->tm_mday > OSPF_MAX_MONTH_DAYS + 1))
            {
                return OSPF_INVALID_DATE;
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_DATE;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-MON-YEAR,HH:MM" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPF_ZERO] == '-')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "MON-YEAR,HH:MM" */
        }
        else
        {
            return OSPF_FAILURE;
        }
        MEMSET (au1Temp, OSPF_ZERO, sizeof (au1Temp));
        MEMCPY (au1Temp, pu1BasePtr, 3);
        /* Month Validations  and check if date is valid for respective month */
        if (STRCASECMP (au1Temp, "Jan") == 0)
        {
            tm->tm_mon = 0;
        }
        else if (STRCASECMP (au1Temp, "Feb") == 0)
        {

            tm->tm_mon = 1;
            tm->tm_yday += OSPF_MAX_MONTH_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Mar") == 0)
        {
            tm->tm_mon = 2;
            tm->tm_yday += OSPF_MAX_MONTH_DAYS;

        }
        else if (STRCASECMP (au1Temp, "Apr") == 0)
        {
            if (tm->tm_mday > OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INVALID_DATE;
            }
            tm->tm_mon = 3;
            tm->tm_yday += OSPF_MAX_MAR_DAYS;
        }
        else if (STRCASECMP (au1Temp, "May") == 0)
        {
            tm->tm_mon = 4;
            tm->tm_yday += OSPF_MAX_APR_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Jun") == 0)
        {
            if (tm->tm_mday >= OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INVALID_DATE;
            }
            tm->tm_mon = 5;
            tm->tm_yday += OSPF_MAX_MAY_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Jul") == 0)
        {
            tm->tm_mon = 6;
            tm->tm_yday += OSPF_MAX_JUN_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Aug") == 0)
        {
            tm->tm_mon = 7;
            tm->tm_yday += OSPF_MAX_JUL_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Sep") == 0)
        {
            if (tm->tm_mday >= OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INVALID_DATE;
            }
            tm->tm_mon = 8;
            tm->tm_yday += OSPF_MAX_AUG_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Oct") == 0)
        {
            tm->tm_mon = 9;
            tm->tm_yday += OSPF_MAX_SEP_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Nov") == 0)
        {
            if (tm->tm_mday >= OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INVALID_DATE;
            }
            tm->tm_mon = 10;
            tm->tm_yday += OSPF_MAX_OCT_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Dec") == 0)
        {
            tm->tm_mon = 11;
            tm->tm_yday += OSPF_MAX_NOV_DAYS;
        }
        else
        {
            return OSPF_INVALID_MONTH;
        }
        /* End of Month Validation */
    }
    else
    {
        return OSPF_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-YEAR,HH:MM" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPF_ZERO] == '-')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "YEAR,HH:MM" */
        }
        else
        {
            return OSPF_FAILURE;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[OSPF_ZERO]))    /* Check if year is integer or not */
            && (ISDIGIT (pu1CurrPtr[OSPF_ONE]))
            && (ISDIGIT (pu1CurrPtr[2])) && (ISDIGIT (pu1CurrPtr[3])))
        {
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
            MEMCPY (au1Temp, pu1CurrPtr, 4);
            tm->tm_year = ATOI (au1Temp);

            /* Checking for the validity of Year */
            if (tm->tm_year < OSPF_BASE_YEAR)
            {
                return OSPF_INVALID_YEAR;
            }
            if (tm->tm_mon == 1)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    if (tm->tm_mday > OSPF_MAX_LEAP_FEB_DAYS)
                    {
                        return OSPF_INVALID_DATE;
                    }
                }
                else
                {
                    if (tm->tm_mday > OSPF_MIN_LEAP_FEB_DAYS)
                    {
                        return OSPF_INVALID_DATE;
                    }
                }

            }

            if (tm->tm_mon >= 2)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    tm->tm_yday += OSPF_MAX_LEAP_FEB_DAYS;

                }
                else
                {
                    tm->tm_yday += OSPF_MIN_LEAP_FEB_DAYS;
                }
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_YEAR;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');    /*pu1CurrPtr postioned at ",HH:MM" */

    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPF_ZERO] == ',')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /*pu1CurrPtr postioned at "HH:MM" */
        }
        else
        {
            return OSPF_FAILURE;
        }
        if ((ISDIGIT (pu1CurrPtr[OSPF_ZERO]))    /* check if hour is integer */
            && (ISDIGIT (pu1CurrPtr[OSPF_ONE])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_hour = ATOI (au1Temp);
            /* Checking for the validity of Hour */
            if (tm->tm_hour > OSPF_MAX_HOUR)
            {
                return OSPF_INVALID_HOUR;
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_HOUR;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":MM" */

    if (pu1CurrPtr != NULL)
    {
        /* Checking for Separator */
        if (pu1CurrPtr[OSPF_ZERO] == ':')
        {
            pu1CurrPtr++;        /* pu1CurrPtr postioned at "MM" */
        }
        else
        {
            return OSPF_INVALID_HOUR;
        }
    }

    if (pu1CurrPtr != NULL)
    {

        if ((ISDIGIT (pu1CurrPtr[OSPF_ZERO]))    /* Check if min is integer or not */
            && (ISDIGIT (pu1CurrPtr[OSPF_ONE])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_min = ATOI (au1Temp);
            /* Checking for the validity of Min */
            if (tm->tm_min > OSPF_MAX_MIN)
            {
                return OSPF_INVALID_MIN;
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_MIN;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

INT4
OspfConvertTimeForSnmp (UINT1 *pu1TimeStr, tUtlTm * tm)
{

    UINT4               u4Month = 0;
    UINT1               au1Temp[OSPF_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;
    UINT4               au4YearDays[] = { 0, 31, 31, 62, 92, 123,
        153, 184, 215, 245, 276, 306
    };
    UINT1               au1MonthDays[] = { 31, 29, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31
    };

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);

    /* Parsing through the function  for year */
    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[0]))    /* Check if year is integer or not */
            && (ISDIGIT (pu1CurrPtr[1]))
            && (ISDIGIT (pu1CurrPtr[2])) && (ISDIGIT (pu1CurrPtr[3])))
        {
            MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
            MEMCPY (au1Temp, pu1CurrPtr, 4);
            tm->tm_year = ATOI (au1Temp);

            /* Checking for the validity of Year */
            if (tm->tm_year < TM_BASE_YEAR)
            {
                return OSPF_INVALID_YEAR;
            }
            MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_YEAR;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-MM-DD,hh:mm:ss" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "MM-DD,hh:mm:ss" */
        MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        MEMCPY (au1Temp, pu1BasePtr, 2);
        /* Month Validations  and check if date is valid for respective month */
        if ((ISDIGIT (pu1BasePtr[0]))    /* Check if month is an integer */
            && (ISDIGIT (pu1BasePtr[1])))
        {
            u4Month = ATOI (au1Temp);

            if ((u4Month > 0) && (u4Month <= 12))
            {
                tm->tm_mon = u4Month - 1;
                tm->tm_yday += au4YearDays[tm->tm_mon];
            }
            else
            {
                return OSPF_INVALID_MONTH;
            }
        }
        else
        {
            return OSPF_INVALID_MONTH;
        }
        /* End of Month Validation */
    }
    else
    {
        return OSPF_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-DD,hh:mm:ss" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "DD,hh:mm:ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        /* Checking for date */
        MEMCPY (au1Temp, pu1BasePtr, 2);
        if ((ISDIGIT (pu1BasePtr[0]))    /* Check if date is an integer */
            && (ISDIGIT (pu1BasePtr[1])))
        {
            tm->tm_mday = ATOI (au1Temp);
            tm->tm_yday += tm->tm_mday;
            /* Checking for the validity of Date */
            if ((tm->tm_mday < 1) || (tm->tm_mday >= OSPF_MAX_MONTH_DAYS + 1))
            {
                return OSPF_INVALID_DATE;
            }
            if (tm->tm_mday > au1MonthDays[tm->tm_mon])
            {
                return OSPF_INVALID_DATE;
            }
            if ((tm->tm_mon == 1) && (!(IS_LEAP (tm->tm_year)))
                && (tm->tm_mday == OSPF_MAX_LEAP_FEB_DAYS))
            {
                return OSPF_INVALID_DATE;
            }
            if (tm->tm_mon >= 2)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    tm->tm_yday += OSPF_MAX_LEAP_FEB_DAYS;

                }
                else
                {
                    tm->tm_yday += OSPF_MIN_LEAP_FEB_DAYS;
                }
            }
            MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_DATE;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');    /*pu1CurrPtr postioned at ",hh:mm:ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /*pu1CurrPtr postioned at "hh:mm:ss" */
        if ((ISDIGIT (pu1CurrPtr[0]))    /* check if hour is integer */
            && (ISDIGIT (pu1CurrPtr[1])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_hour = ATOI (au1Temp);
            /* Checking for the validity of Hour */
            if (tm->tm_hour > OSPF_MAX_HOUR)
            {
                return OSPF_INVALID_HOUR;
            }
            MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_HOUR;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":mm:ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;            /* pu1CurrPtr postioned at "mm:ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[0]))    /* Check if min is integer or not */
            && (ISDIGIT (pu1CurrPtr[1])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_min = ATOI (au1Temp);
            /* Checking for the validity of min */
            if (tm->tm_min > OSPF_MAX_MIN)
            {
                return OSPF_INVALID_MIN;
            }
            MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_MIN;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;            /* pu1CurrPtr postioned at "ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT ((int) pu1CurrPtr[0]))    /* Check if sec is integer or not */
            && (ISDIGIT ((int) pu1CurrPtr[1])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_sec = ATOI (au1Temp);
            /* Checking for the validity of sec */
            if (tm->tm_sec > 60)
            {
                return OSPF_INVALID_SEC;
            }
            MEMSET (au1Temp, 0, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INVALID_SEC;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/************************************************************************
 *  Function Name   : OspfPrintKeyTime
 *  Description     : This function will convert the time in structure to 
              string.
 *  Input           : u4Secs -Seconds
 *  Output          : pOspfKeyTime- Returns the Value in array
 *  Returns         : NONE
 ************************************************************************/
VOID
OspfPrintKeyTime (INT4 u4Secs, UINT1 *pOspfKeyTime)
{
    tUtlTm              tm;
    UINT1               au1Mon[12];

    MEMSET (au1Mon, OSPF_ZERO, sizeof (au1Mon));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    UtlGetTimeForSeconds (u4Secs, &tm);    /* Tm structure is filled for secs */

/* Month is filled in array and year_day is reduced to date in this process*/
    if (tm.tm_mon == 0)
    {
        STRNCPY (au1Mon, "Jan", STRLEN ("Jan"));
        au1Mon[STRLEN ("Jan")] = '\0';
    }
    else if (tm.tm_mon == 1)
    {
        STRNCPY (au1Mon, "Feb", STRLEN ("Feb"));
        au1Mon[STRLEN ("Feb")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MONTH_DAYS;
    }
    else if (tm.tm_mon == 2)
    {
        STRNCPY (au1Mon, "Mar", STRLEN ("Mar"));
        au1Mon[STRLEN ("Mar")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MONTH_DAYS;
    }
    else if (tm.tm_mon == 3)
    {
        STRNCPY (au1Mon, "Apr", STRLEN ("Apr"));
        au1Mon[STRLEN ("Apr")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MAR_DAYS;
    }
    else if (tm.tm_mon == 4)
    {
        STRNCPY (au1Mon, "May", STRLEN ("May"));
        au1Mon[STRLEN ("May")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_APR_DAYS;
    }
    else if (tm.tm_mon == 5)
    {
        STRNCPY (au1Mon, "Jun", STRLEN ("Jun"));
        au1Mon[STRLEN ("Jun")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MAY_DAYS;
    }
    else if (tm.tm_mon == 6)
    {
        STRNCPY (au1Mon, "Jul", STRLEN ("Jul"));
        au1Mon[STRLEN ("Jul")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_JUN_DAYS;
    }
    else if (tm.tm_mon == 7)
    {
        STRNCPY (au1Mon, "Aug", STRLEN ("Aug"));
        au1Mon[STRLEN ("Aug")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_JUL_DAYS;
    }
    else if (tm.tm_mon == 8)
    {
        STRNCPY (au1Mon, "Sep", STRLEN ("Sep"));
        au1Mon[STRLEN ("Sep")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_AUG_DAYS;
    }
    else if (tm.tm_mon == 9)
    {
        STRNCPY (au1Mon, "Oct", STRLEN ("Oct"));
        au1Mon[STRLEN ("Oct")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_SEP_DAYS;
    }
    else if (tm.tm_mon == 10)
    {
        STRNCPY (au1Mon, "Nov", STRLEN ("Nov"));
        au1Mon[STRLEN ("Nov")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_OCT_DAYS;
    }
    else if (tm.tm_mon == 11)
    {
        STRNCPY (au1Mon, "Dec", STRLEN ("Dec"));
        au1Mon[STRLEN ("Dec")] = '\0';
        tm.tm_yday = tm.tm_yday - OSPF_MAX_NOV_DAYS;
    }

    if (tm.tm_mon >= 2)            /* In the case of Leap Year for the month of Feb Year_Day is reduced */
    {
        if (IS_LEAP (tm.tm_year))
        {
            tm.tm_yday = tm.tm_yday - OSPF_MAX_LEAP_FEB_DAYS;
        }
        else
        {
            tm.tm_yday = tm.tm_yday - OSPF_MIN_LEAP_FEB_DAYS;
        }
    }
    /* Values are printed in the structure */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%.2u-%s-%4u,%.2u:%.2u \r\n",
             tm.tm_yday, au1Mon, tm.tm_year, tm.tm_hour, tm.tm_min);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RouterLsaAlloc                                             */
/*                                                                           */
/* Description  : This function allocates Memory based on the required       */
/*                LSA size                                                   */
/*                                                                           */
/* Input          pLsaBlk              : pointer to Lsa.                     */
/*                length               : Length of LSA.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PUBLIC UINT1       *
RouterLsaAlloc (UINT2 length)
{
    UINT1               sizeInfo = 0;
    UINT1              *pLsaAlloc = NULL;

    /*OSPF_TRC (OSPF_FN_ENTRY, "FUNC: RouterLsaAlloc\n"); */
    if (length < MAX_LSA_SIZE256)
    {
        sizeInfo = SZ256_LEN;
        pLsaAlloc = (UINT1 *) MemAllocMemBlk (LSA_QID1);
    }
    else if (length >= MAX_LSA_SIZE256 && length < MAX_LSA_SIZE512)
    {
        sizeInfo = SZ512_LEN;
        pLsaAlloc = (UINT1 *) MemAllocMemBlk (LSA_QID2);
    }
    else if (length >= MAX_LSA_SIZE512 && length < MAX_LSA_SIZE1024)
    {
        sizeInfo = SZ1024_LEN;
        pLsaAlloc = (UINT1 *) MemAllocMemBlk (LSA_QID);
    }
    else if (length >= MAX_LSA_SIZE1024 && length < MAX_LSA_SIZE2048)
    {
        sizeInfo = SZ2048_LEN;
        pLsaAlloc = (UINT1 *) MemAllocMemBlk (LSA_QID4);
    }
    else if (length >= MAX_LSA_SIZE2048 && length < MAX_LSA_SIZE4096)
    {
        sizeInfo = SZ4096_LEN;
        pLsaAlloc = (UINT1 *) MemAllocMemBlk (LSA_QID5);
    }
    else
        sizeInfo = SZMAX_LEN;
    /*Update first byte of allocated memory with SIZE BLOCK info */
    if (pLsaAlloc != NULL)
    {
        LADD1BYTE (pLsaAlloc, sizeInfo);
    }
    return pLsaAlloc;
}

/****************************************************************************/
/*Function      : UtilOspfClearAreaDb                                       */
/*Input         : pArea                                                     */
/*Output        : None                                                      */
/*Description   : This function removes the Self orig Lsas from the router  */
/*                      upon OSPF session disable                           */
/*Returns       : None                                                      */
/****************************************************************************/

PUBLIC VOID
UtilOspfClearAreaDb (tArea * pArea)
{
    UNUSED_PARAM (pArea);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RouterLsaFree                                              */
/*                                                                           */
/* Description  : This function allocates Memory based on the required       */
/*                LSA size                                                   */
/*                                                                           */
/* Input          pLsaBlk              : pointer to Lsa.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PUBLIC VOID
RouterLsaFree (UINT1 *pLsaBlk)
{
    UINT1               sizeInfo = 0;

    if (pLsaBlk != NULL)
    {
        pLsaBlk = (pLsaBlk - 1);
        sizeInfo = *((UINT1 *) pLsaBlk);
    }
    switch (sizeInfo)
    {
        case SZ256_LEN:
            OSPF_CRU_BUF_RELEASE_FREE_OBJ (LSA_QID1, (UINT1 *) pLsaBlk);
            break;
        case SZ512_LEN:
            OSPF_CRU_BUF_RELEASE_FREE_OBJ (LSA_QID2, (UINT1 *) pLsaBlk);
            break;
        case SZ1024_LEN:
            OSPF_CRU_BUF_RELEASE_FREE_OBJ (LSA_QID, (UINT1 *) pLsaBlk);
            break;
        case SZ2048_LEN:
            OSPF_CRU_BUF_RELEASE_FREE_OBJ (LSA_QID4, (UINT1 *) pLsaBlk);
            break;
        case SZ4096_LEN:
            OSPF_CRU_BUF_RELEASE_FREE_OBJ (LSA_QID5, (UINT1 *) pLsaBlk);
            break;
        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name : OspfutilValidateDestAddr                                  */
/* Description   : IpAddress Validation                                      */
/* Input(s)      : IP Address of the Peer (PeerAddr)                         */
/* Output(s)     : OSPF_FAILURE                                              */
/*****************************************************************************/

UINT4
OspfutilValidateDestAddr (UINT4 u4FutOspfRRDRouteDest)
{

    if (OSPF_IS_MULTICAST_ADDR (u4FutOspfRRDRouteDest))
    {
        return OSPF_FAILURE;
    }
    if (OSPF_IS_HOST_ADDR (u4FutOspfRRDRouteDest))
    {
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : OspfGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPF_SUCESS/OSPF_FAILURE                             */
/*****************************************************************************/
INT4
OspfGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (OspfCliGetShowCmdOutputToFile ((UINT1 *) OSPF_AUDIT_FILE_ACTIVE) !=
            OSPF_SUCCESS)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "GetShRunFile Failed\n");
            return OSPF_FAILURE;
        }
        if (OspfCliCalcSwAudCheckSum
            ((UINT1 *) OSPF_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != OSPF_SUCCESS)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "CalcSwAudChkSum Failed\n");
            return OSPF_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (OspfCliGetShowCmdOutputToFile ((UINT1 *) OSPF_AUDIT_FILE_STDBY) !=
            OSPF_SUCCESS)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "GetShRunFile Failed\n");
            return OSPF_FAILURE;
        }
        if (OspfCliCalcSwAudCheckSum
            ((UINT1 *) OSPF_AUDIT_FILE_STDBY, pu2SwAudChkSum) != OSPF_SUCCESS)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "CalcSwAudChkSum Failed\n");
            return OSPF_FAILURE;
        }
    }
    else
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node State is neither active nor standby\n");
        return OSPF_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return OSPF_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                          End of file osutil.c                             */
/*---------------------------------------------------------------------------*/
