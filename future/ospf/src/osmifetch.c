/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: osmifetch.c,v 1.17 2014/10/29 12:51:00 siva Exp $
*
* Description: Protocol Low Level MI Get Routines 
*********************************************************************/
#include  "osinc.h"
#include "ospfcli.h"
#include "stdoslow.h"
#include "ospftlow.h"
#include "fsostlow.h"
/* LOW LEVEL Get Routines for Table : FsMIStdOspfTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfTable (INT4 *pi4FsMIStdOspfContextId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfTable (INT4 i4FsMIStdOspfContextId,
                                 INT4 *pi4NextFsMIStdOspfContextId)
{
    if (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                              (UINT4 *) pi4NextFsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfRouterId
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfRouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfRouterId (INT4 i4FsMIStdOspfContextId,
                           UINT4 *pu4RetValFsMIStdOspfRouterId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfRouterId (pu4RetValFsMIStdOspfRouterId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAdminStat
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAdminStat (INT4 i4FsMIStdOspfContextId,
                            INT4 *pi4RetValFsMIStdOspfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAdminStat (pi4RetValFsMIStdOspfAdminStat);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVersionNumber
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVersionNumber (INT4 i4FsMIStdOspfContextId,
                                INT4 *pi4RetValFsMIStdOspfVersionNumber)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVersionNumber (pi4RetValFsMIStdOspfVersionNumber);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfAreaBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaBdrRtrStatus (INT4 i4FsMIStdOspfContextId,
                                   INT4 *pi4RetValFsMIStdOspfAreaBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfAreaBdrRtrStatus (pi4RetValFsMIStdOspfAreaBdrRtrStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfASBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfASBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfASBdrRtrStatus (INT4 i4FsMIStdOspfContextId,
                                 INT4 *pi4RetValFsMIStdOspfASBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfASBdrRtrStatus (pi4RetValFsMIStdOspfASBdrRtrStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExternLsaCount
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfExternLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExternLsaCount (INT4 i4FsMIStdOspfContextId,
                                 UINT4 *pu4RetValFsMIStdOspfExternLsaCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfExternLsaCount (pu4RetValFsMIStdOspfExternLsaCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExternLsaCksumSum
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfExternLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExternLsaCksumSum (INT4 i4FsMIStdOspfContextId,
                                    INT4 *pi4RetValFsMIStdOspfExternLsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfExternLsaCksumSum (pi4RetValFsMIStdOspfExternLsaCksumSum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfTOSSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfTOSSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfTOSSupport (INT4 i4FsMIStdOspfContextId,
                             INT4 *pi4RetValFsMIStdOspfTOSSupport)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfTOSSupport (pi4RetValFsMIStdOspfTOSSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfOriginateNewLsas
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfOriginateNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfOriginateNewLsas (INT4 i4FsMIStdOspfContextId,
                                   UINT4 *pu4RetValFsMIStdOspfOriginateNewLsas)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfOriginateNewLsas (pu4RetValFsMIStdOspfOriginateNewLsas);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfRxNewLsas
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfRxNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfRxNewLsas (INT4 i4FsMIStdOspfContextId,
                            UINT4 *pu4RetValFsMIStdOspfRxNewLsas)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfRxNewLsas (pu4RetValFsMIStdOspfRxNewLsas);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExtLsdbLimit
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfExtLsdbLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExtLsdbLimit (INT4 i4FsMIStdOspfContextId,
                               INT4 *pi4RetValFsMIStdOspfExtLsdbLimit)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfExtLsdbLimit (pi4RetValFsMIStdOspfExtLsdbLimit);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfMulticastExtensions
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfMulticastExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfMulticastExtensions (INT4 i4FsMIStdOspfContextId,
                                      INT4
                                      *pi4RetValFsMIStdOspfMulticastExtensions)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfMulticastExtensions (pi4RetValFsMIStdOspfMulticastExtensions);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExitOverflowInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfExitOverflowInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExitOverflowInterval (INT4 i4FsMIStdOspfContextId,
                                       INT4
                                       *pi4RetValFsMIStdOspfExitOverflowInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfExitOverflowInterval
        (pi4RetValFsMIStdOspfExitOverflowInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfDemandExtensions
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfDemandExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfDemandExtensions (INT4 i4FsMIStdOspfContextId,
                                   INT4 *pi4RetValFsMIStdOspfDemandExtensions)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfDemandExtensions (pi4RetValFsMIStdOspfDemandExtensions);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIStdOspfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfStatus (INT4 i4FsMIStdOspfContextId,
                         INT4 *pi4RetValFsMIStdOspfStatus)
{
    tOspfCxt           *pOspfCxt = NULL;

    if ((pOspfCxt = UtilOspfGetCxt ((UINT4) i4FsMIStdOspfContextId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIStdOspfStatus = pOspfCxt->contextStatus;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfAreaTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfAreaTable
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfAreaTable (INT4 *pi4FsMIStdOspfAreaContextId,
                                      UINT4 *pu4FsMIStdOspfAreaId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfAreaContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }

    if (UtilOspfSetContext (*pi4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i4RetVal;
    }

    i4RetVal = nmhGetFirstIndexOspfAreaTable (pu4FsMIStdOspfAreaId);

    UtilOspfResetContext ();

    return i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfAreaTable
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                nextFsMIStdOspfAreaContextId
                FsMIStdOspfAreaId
                nextFsMIStdOspfAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfAreaTable (INT4 i4FsMIStdOspfAreaContextId,
                                     INT4 *pi4NextFsMIStdOspfAreaContextId,
                                     UINT4 u4FsMIStdOspfAreaId,
                                     UINT4 *pu4NextFsMIStdOspfAreaId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfAreaContextId) == OSPF_SUCCESS)
    {

        if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfAreaTable (u4FsMIStdOspfAreaId,
                                                 pu4NextFsMIStdOspfAreaId);

        UtilOspfResetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfAreaContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfAreaContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfAreaContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal = nmhGetFirstIndexOspfAreaTable (pu4NextFsMIStdOspfAreaId);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfAreaContextId = *pi4NextFsMIStdOspfAreaContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfAreaContextId = i4FsMIStdOspfAreaContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfImportAsExtern
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfImportAsExtern
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfImportAsExtern (INT4 i4FsMIStdOspfAreaContextId,
                                 UINT4 u4FsMIStdOspfAreaId,
                                 INT4 *pi4RetValFsMIStdOspfImportAsExtern)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfImportAsExtern (u4FsMIStdOspfAreaId,
                                         pi4RetValFsMIStdOspfImportAsExtern);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfSpfRuns
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfSpfRuns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfSpfRuns (INT4 i4FsMIStdOspfAreaContextId,
                          UINT4 u4FsMIStdOspfAreaId,
                          UINT4 *pu4RetValFsMIStdOspfSpfRuns)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfSpfRuns (u4FsMIStdOspfAreaId,
                                  pu4RetValFsMIStdOspfSpfRuns);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaBdrRtrCount
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfAreaBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaBdrRtrCount (INT4 i4FsMIStdOspfAreaContextId,
                                  UINT4 u4FsMIStdOspfAreaId,
                                  UINT4 *pu4RetValFsMIStdOspfAreaBdrRtrCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaBdrRtrCount (u4FsMIStdOspfAreaId,
                                          pu4RetValFsMIStdOspfAreaBdrRtrCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAsBdrRtrCount
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfAsBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAsBdrRtrCount (INT4 i4FsMIStdOspfAreaContextId,
                                UINT4 u4FsMIStdOspfAreaId,
                                UINT4 *pu4RetValFsMIStdOspfAsBdrRtrCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAsBdrRtrCount (u4FsMIStdOspfAreaId,
                                        pu4RetValFsMIStdOspfAsBdrRtrCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaLsaCount
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfAreaLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaLsaCount (INT4 i4FsMIStdOspfAreaContextId,
                               UINT4 u4FsMIStdOspfAreaId,
                               UINT4 *pu4RetValFsMIStdOspfAreaLsaCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaLsaCount (u4FsMIStdOspfAreaId,
                                       pu4RetValFsMIStdOspfAreaLsaCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaLsaCksumSum
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfAreaLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaLsaCksumSum (INT4 i4FsMIStdOspfAreaContextId,
                                  UINT4 u4FsMIStdOspfAreaId,
                                  INT4 *pi4RetValFsMIStdOspfAreaLsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaLsaCksumSum (u4FsMIStdOspfAreaId,
                                          pi4RetValFsMIStdOspfAreaLsaCksumSum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaSummary
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfAreaSummary
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaSummary (INT4 i4FsMIStdOspfAreaContextId,
                              UINT4 u4FsMIStdOspfAreaId,
                              INT4 *pi4RetValFsMIStdOspfAreaSummary)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaSummary (u4FsMIStdOspfAreaId,
                                      pi4RetValFsMIStdOspfAreaSummary);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaStatus
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                retValFsMIStdOspfAreaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaStatus (INT4 i4FsMIStdOspfAreaContextId,
                             UINT4 u4FsMIStdOspfAreaId,
                             INT4 *pi4RetValFsMIStdOspfAreaStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaStatus (u4FsMIStdOspfAreaId,
                                     pi4RetValFsMIStdOspfAreaStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfStubAreaTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfStubAreaTable
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfStubAreaTable (INT4 *pi4FsMIStdOspfStubContextId,
                                          UINT4 *pu4FsMIStdOspfStubAreaId,
                                          INT4 *pi4FsMIStdOspfStubTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfStubContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfStubContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfStubContextId = *pi4FsMIStdOspfStubContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfStubContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfStubAreaTable (pu4FsMIStdOspfStubAreaId,
                                                      pi4FsMIStdOspfStubTOS);
        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfStubContextId,
                                 (UINT4 *) pi4FsMIStdOspfStubContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfStubAreaTable
 Input       :  The Indices
                FsMIStdOspfStubContextId
                nextFsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                nextFsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS
                nextFsMIStdOspfStubTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfStubAreaTable (INT4 i4FsMIStdOspfStubContextId,
                                         INT4 *pi4NextFsMIStdOspfStubContextId,
                                         UINT4 u4FsMIStdOspfStubAreaId,
                                         UINT4 *pu4NextFsMIStdOspfStubAreaId,
                                         INT4 i4FsMIStdOspfStubTOS,
                                         INT4 *pi4NextFsMIStdOspfStubTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfStubContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfStubAreaTable (u4FsMIStdOspfStubAreaId,
                                                     pu4NextFsMIStdOspfStubAreaId,
                                                     i4FsMIStdOspfStubTOS,
                                                     pi4NextFsMIStdOspfStubTOS);
        UtilOspfResetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfStubContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfStubContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfStubContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfStubAreaTable (pu4NextFsMIStdOspfStubAreaId,
                                                   pi4NextFsMIStdOspfStubTOS);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfStubContextId = *pi4NextFsMIStdOspfStubContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfStubContextId = i4FsMIStdOspfStubContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfStubMetric
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                retValFsMIStdOspfStubMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfStubMetric (INT4 i4FsMIStdOspfStubContextId,
                             UINT4 u4FsMIStdOspfStubAreaId,
                             INT4 i4FsMIStdOspfStubTOS,
                             INT4 *pi4RetValFsMIStdOspfStubMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfStubMetric (u4FsMIStdOspfStubAreaId,
                                     i4FsMIStdOspfStubTOS,
                                     pi4RetValFsMIStdOspfStubMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfStubStatus
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                retValFsMIStdOspfStubStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfStubStatus (INT4 i4FsMIStdOspfStubContextId,
                             UINT4 u4FsMIStdOspfStubAreaId,
                             INT4 i4FsMIStdOspfStubTOS,
                             INT4 *pi4RetValFsMIStdOspfStubStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfStubStatus (u4FsMIStdOspfStubAreaId,
                                     i4FsMIStdOspfStubTOS,
                                     pi4RetValFsMIStdOspfStubStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfStubMetricType
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                retValFsMIStdOspfStubMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfStubMetricType (INT4 i4FsMIStdOspfStubContextId,
                                 UINT4 u4FsMIStdOspfStubAreaId,
                                 INT4 i4FsMIStdOspfStubTOS,
                                 INT4 *pi4RetValFsMIStdOspfStubMetricType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfStubMetricType (u4FsMIStdOspfStubAreaId,
                                         i4FsMIStdOspfStubTOS,
                                         pi4RetValFsMIStdOspfStubMetricType);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfLsdbTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfLsdbTable
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfLsdbTable (INT4 *pi4FsMIStdOspfLsdbContextId,
                                      UINT4 *pu4FsMIStdOspfLsdbAreaId,
                                      INT4 *pi4FsMIStdOspfLsdbType,
                                      UINT4 *pu4FsMIStdOspfLsdbLsid,
                                      UINT4 *pu4FsMIStdOspfLsdbRouterId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfLsdbContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfLsdbContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfLsdbContextId = *pi4FsMIStdOspfLsdbContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfLsdbTable (pu4FsMIStdOspfLsdbAreaId,
                                                  pi4FsMIStdOspfLsdbType,
                                                  pu4FsMIStdOspfLsdbLsid,
                                                  pu4FsMIStdOspfLsdbRouterId);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfLsdbContextId,
                                 (UINT4 *) pi4FsMIStdOspfLsdbContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfLsdbTable
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                nextFsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                nextFsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                nextFsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                nextFsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId
                nextFsMIStdOspfLsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfLsdbTable (INT4 i4FsMIStdOspfLsdbContextId,
                                     INT4 *pi4NextFsMIStdOspfLsdbContextId,
                                     UINT4 u4FsMIStdOspfLsdbAreaId,
                                     UINT4 *pu4NextFsMIStdOspfLsdbAreaId,
                                     INT4 i4FsMIStdOspfLsdbType,
                                     INT4 *pi4NextFsMIStdOspfLsdbType,
                                     UINT4 u4FsMIStdOspfLsdbLsid,
                                     UINT4 *pu4NextFsMIStdOspfLsdbLsid,
                                     UINT4 u4FsMIStdOspfLsdbRouterId,
                                     UINT4 *pu4NextFsMIStdOspfLsdbRouterId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfLsdbContextId) == OSPF_SUCCESS)
    {

        if (UtilOspfSetContext (i4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfLsdbTable (u4FsMIStdOspfLsdbAreaId,
                                                 pu4NextFsMIStdOspfLsdbAreaId,
                                                 i4FsMIStdOspfLsdbType,
                                                 pi4NextFsMIStdOspfLsdbType,
                                                 u4FsMIStdOspfLsdbLsid,
                                                 pu4NextFsMIStdOspfLsdbLsid,
                                                 u4FsMIStdOspfLsdbRouterId,
                                                 pu4NextFsMIStdOspfLsdbRouterId);
        UtilOspfResetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfLsdbContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfLsdbContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfLsdbContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfLsdbTable (pu4NextFsMIStdOspfLsdbAreaId,
                                               pi4NextFsMIStdOspfLsdbType,
                                               pu4NextFsMIStdOspfLsdbLsid,
                                               pu4NextFsMIStdOspfLsdbRouterId);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfLsdbContextId = *pi4NextFsMIStdOspfLsdbContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfLsdbContextId = i4FsMIStdOspfLsdbContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfLsdbSequence
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId

                The Object 
                retValFsMIStdOspfLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfLsdbSequence (INT4 i4FsMIStdOspfLsdbContextId,
                               UINT4 u4FsMIStdOspfLsdbAreaId,
                               INT4 i4FsMIStdOspfLsdbType,
                               UINT4 u4FsMIStdOspfLsdbLsid,
                               UINT4 u4FsMIStdOspfLsdbRouterId,
                               INT4 *pi4RetValFsMIStdOspfLsdbSequence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfLsdbSequence (u4FsMIStdOspfLsdbAreaId,
                                       i4FsMIStdOspfLsdbType,
                                       u4FsMIStdOspfLsdbLsid,
                                       u4FsMIStdOspfLsdbRouterId,
                                       pi4RetValFsMIStdOspfLsdbSequence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfLsdbAge
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId

                The Object 
                retValFsMIStdOspfLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfLsdbAge (INT4 i4FsMIStdOspfLsdbContextId,
                          UINT4 u4FsMIStdOspfLsdbAreaId,
                          INT4 i4FsMIStdOspfLsdbType,
                          UINT4 u4FsMIStdOspfLsdbLsid,
                          UINT4 u4FsMIStdOspfLsdbRouterId,
                          INT4 *pi4RetValFsMIStdOspfLsdbAge)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfLsdbAge (u4FsMIStdOspfLsdbAreaId,
                                  i4FsMIStdOspfLsdbType,
                                  u4FsMIStdOspfLsdbLsid,
                                  u4FsMIStdOspfLsdbRouterId,
                                  pi4RetValFsMIStdOspfLsdbAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfLsdbChecksum
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId

                The Object 
                retValFsMIStdOspfLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfLsdbChecksum (INT4 i4FsMIStdOspfLsdbContextId,
                               UINT4 u4FsMIStdOspfLsdbAreaId,
                               INT4 i4FsMIStdOspfLsdbType,
                               UINT4 u4FsMIStdOspfLsdbLsid,
                               UINT4 u4FsMIStdOspfLsdbRouterId,
                               INT4 *pi4RetValFsMIStdOspfLsdbChecksum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfLsdbChecksum (u4FsMIStdOspfLsdbAreaId,
                                       i4FsMIStdOspfLsdbType,
                                       u4FsMIStdOspfLsdbLsid,
                                       u4FsMIStdOspfLsdbRouterId,
                                       pi4RetValFsMIStdOspfLsdbChecksum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfLsdbAdvertisement
 Input       :  The Indices
                FsMIStdOspfLsdbContextId
                FsMIStdOspfLsdbAreaId
                FsMIStdOspfLsdbType
                FsMIStdOspfLsdbLsid
                FsMIStdOspfLsdbRouterId

                The Object 
                retValFsMIStdOspfLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfLsdbAdvertisement (INT4 i4FsMIStdOspfLsdbContextId,
                                    UINT4 u4FsMIStdOspfLsdbAreaId,
                                    INT4 i4FsMIStdOspfLsdbType,
                                    UINT4 u4FsMIStdOspfLsdbLsid,
                                    UINT4 u4FsMIStdOspfLsdbRouterId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMIStdOspfLsdbAdvertisement)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfLsdbAdvertisement (u4FsMIStdOspfLsdbAreaId,
                                            i4FsMIStdOspfLsdbType,
                                            u4FsMIStdOspfLsdbLsid,
                                            u4FsMIStdOspfLsdbRouterId,
                                            pRetValFsMIStdOspfLsdbAdvertisement);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfHostTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfHostTable
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfHostTable (INT4 *pi4FsMIStdOspfHostContextId,
                                      UINT4 *pu4FsMIStdOspfHostIpAddress,
                                      INT4 *pi4FsMIStdOspfHostTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfHostContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfHostContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfHostContextId = *pi4FsMIStdOspfHostContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfHostContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfHostTable (pu4FsMIStdOspfHostIpAddress,
                                                  pi4FsMIStdOspfHostTOS);
        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfHostContextId,
                                 (UINT4 *) pi4FsMIStdOspfHostContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfHostTable
 Input       :  The Indices
                FsMIStdOspfHostContextId
                nextFsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                nextFsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS
                nextFsMIStdOspfHostTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfHostTable (INT4 i4FsMIStdOspfHostContextId,
                                     INT4 *pi4NextFsMIStdOspfHostContextId,
                                     UINT4 u4FsMIStdOspfHostIpAddress,
                                     UINT4 *pu4NextFsMIStdOspfHostIpAddress,
                                     INT4 i4FsMIStdOspfHostTOS,
                                     INT4 *pi4NextFsMIStdOspfHostTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;
    if (UtilOspfIsValidCxtId (i4FsMIStdOspfHostContextId) == OSPF_SUCCESS)
    {

        if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfHostTable (u4FsMIStdOspfHostIpAddress,
                                                 pu4NextFsMIStdOspfHostIpAddress,
                                                 i4FsMIStdOspfHostTOS,
                                                 pi4NextFsMIStdOspfHostTOS);
        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfHostContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfHostContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfHostContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfHostTable (pu4NextFsMIStdOspfHostIpAddress,
                                               pi4NextFsMIStdOspfHostTOS);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfHostContextId = *pi4NextFsMIStdOspfHostContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfHostContextId = i4FsMIStdOspfHostContextId;
    }
    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfHostMetric
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                retValFsMIStdOspfHostMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfHostMetric (INT4 i4FsMIStdOspfHostContextId,
                             UINT4 u4FsMIStdOspfHostIpAddress,
                             INT4 i4FsMIStdOspfHostTOS,
                             INT4 *pi4RetValFsMIStdOspfHostMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfHostMetric (u4FsMIStdOspfHostIpAddress,
                                     i4FsMIStdOspfHostTOS,
                                     pi4RetValFsMIStdOspfHostMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfHostStatus
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                retValFsMIStdOspfHostStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfHostStatus (INT4 i4FsMIStdOspfHostContextId,
                             UINT4 u4FsMIStdOspfHostIpAddress,
                             INT4 i4FsMIStdOspfHostTOS,
                             INT4 *pi4RetValFsMIStdOspfHostStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfHostStatus (u4FsMIStdOspfHostIpAddress,
                                     i4FsMIStdOspfHostTOS,
                                     pi4RetValFsMIStdOspfHostStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfHostAreaID
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                retValFsMIStdOspfHostAreaID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfHostAreaID (INT4 i4FsMIStdOspfHostContextId,
                             UINT4 u4FsMIStdOspfHostIpAddress,
                             INT4 i4FsMIStdOspfHostTOS,
                             UINT4 *pu4RetValFsMIStdOspfHostAreaID)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfHostAreaID (u4FsMIStdOspfHostIpAddress,
                                     i4FsMIStdOspfHostTOS,
                                     pu4RetValFsMIStdOspfHostAreaID);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfIfTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfIfTable
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfIfTable (INT4 *pi4FsMIStdOspfIfContextId,
                                    UINT4 *pu4FsMIStdOspfIfIpAddress,
                                    INT4 *pi4FsMIStdOspfAddressLessIf)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfIfContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfIfContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfIfContextId = *pi4FsMIStdOspfIfContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfIfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfIfTable (pu4FsMIStdOspfIfIpAddress,
                                                pi4FsMIStdOspfAddressLessIf);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfIfContextId,
                                 (UINT4 *) pi4FsMIStdOspfIfContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfIfTable
 Input       :  The Indices
                FsMIStdOspfIfContextId
                nextFsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                nextFsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf
                nextFsMIStdOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfIfTable (INT4 i4FsMIStdOspfIfContextId,
                                   INT4 *pi4NextFsMIStdOspfIfContextId,
                                   UINT4 u4FsMIStdOspfIfIpAddress,
                                   UINT4 *pu4NextFsMIStdOspfIfIpAddress,
                                   INT4 i4FsMIStdOspfAddressLessIf,
                                   INT4 *pi4NextFsMIStdOspfAddressLessIf)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfIfContextId) == OSPF_SUCCESS)
    {

        if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfIfTable (u4FsMIStdOspfIfIpAddress,
                                               pu4NextFsMIStdOspfIfIpAddress,
                                               i4FsMIStdOspfAddressLessIf,
                                               pi4NextFsMIStdOspfAddressLessIf);

        UtilOspfResetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfIfContextId,
                                      (UINT4 *) pi4NextFsMIStdOspfIfContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfIfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfIfTable (pu4NextFsMIStdOspfIfIpAddress,
                                             pi4NextFsMIStdOspfAddressLessIf);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfIfContextId = *pi4NextFsMIStdOspfIfContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfIfContextId = i4FsMIStdOspfIfContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfAreaId
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfAreaId (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           UINT4 *pu4RetValFsMIStdOspfIfAreaId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfAreaId (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   pu4RetValFsMIStdOspfIfAreaId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfType (INT4 i4FsMIStdOspfIfContextId,
                         UINT4 u4FsMIStdOspfIfIpAddress,
                         INT4 i4FsMIStdOspfAddressLessIf,
                         INT4 *pi4RetValFsMIStdOspfIfType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfType (u4FsMIStdOspfIfIpAddress,
                                 i4FsMIStdOspfAddressLessIf,
                                 pi4RetValFsMIStdOspfIfType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfAdminStat
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfAdminStat (INT4 i4FsMIStdOspfIfContextId,
                              UINT4 u4FsMIStdOspfIfIpAddress,
                              INT4 i4FsMIStdOspfAddressLessIf,
                              INT4 *pi4RetValFsMIStdOspfIfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfAdminStat (u4FsMIStdOspfIfIpAddress,
                                      i4FsMIStdOspfAddressLessIf,
                                      pi4RetValFsMIStdOspfIfAdminStat);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfRtrPriority
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfRtrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfRtrPriority (INT4 i4FsMIStdOspfIfContextId,
                                UINT4 u4FsMIStdOspfIfIpAddress,
                                INT4 i4FsMIStdOspfAddressLessIf,
                                INT4 *pi4RetValFsMIStdOspfIfRtrPriority)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfRtrPriority (u4FsMIStdOspfIfIpAddress,
                                        i4FsMIStdOspfAddressLessIf,
                                        pi4RetValFsMIStdOspfIfRtrPriority);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfTransitDelay (INT4 i4FsMIStdOspfIfContextId,
                                 UINT4 u4FsMIStdOspfIfIpAddress,
                                 INT4 i4FsMIStdOspfAddressLessIf,
                                 INT4 *pi4RetValFsMIStdOspfIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfTransitDelay (u4FsMIStdOspfIfIpAddress,
                                         i4FsMIStdOspfAddressLessIf,
                                         pi4RetValFsMIStdOspfIfTransitDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfRetransInterval (INT4 i4FsMIStdOspfIfContextId,
                                    UINT4 u4FsMIStdOspfIfIpAddress,
                                    INT4 i4FsMIStdOspfAddressLessIf,
                                    INT4 *pi4RetValFsMIStdOspfIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfRetransInterval (u4FsMIStdOspfIfIpAddress,
                                            i4FsMIStdOspfAddressLessIf,
                                            pi4RetValFsMIStdOspfIfRetransInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfHelloInterval (INT4 i4FsMIStdOspfIfContextId,
                                  UINT4 u4FsMIStdOspfIfIpAddress,
                                  INT4 i4FsMIStdOspfAddressLessIf,
                                  INT4 *pi4RetValFsMIStdOspfIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfHelloInterval (u4FsMIStdOspfIfIpAddress,
                                          i4FsMIStdOspfAddressLessIf,
                                          pi4RetValFsMIStdOspfIfHelloInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfRtrDeadInterval (INT4 i4FsMIStdOspfIfContextId,
                                    UINT4 u4FsMIStdOspfIfIpAddress,
                                    INT4 i4FsMIStdOspfAddressLessIf,
                                    INT4 *pi4RetValFsMIStdOspfIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfRtrDeadInterval (u4FsMIStdOspfIfIpAddress,
                                            i4FsMIStdOspfAddressLessIf,
                                            pi4RetValFsMIStdOspfIfRtrDeadInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfPollInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfPollInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfPollInterval (INT4 i4FsMIStdOspfIfContextId,
                                 UINT4 u4FsMIStdOspfIfIpAddress,
                                 INT4 i4FsMIStdOspfAddressLessIf,
                                 INT4 *pi4RetValFsMIStdOspfIfPollInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfPollInterval (u4FsMIStdOspfIfIpAddress,
                                         i4FsMIStdOspfAddressLessIf,
                                         pi4RetValFsMIStdOspfIfPollInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfState
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfState (INT4 i4FsMIStdOspfIfContextId,
                          UINT4 u4FsMIStdOspfIfIpAddress,
                          INT4 i4FsMIStdOspfAddressLessIf,
                          INT4 *pi4RetValFsMIStdOspfIfState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfState (u4FsMIStdOspfIfIpAddress,
                                  i4FsMIStdOspfAddressLessIf,
                                  pi4RetValFsMIStdOspfIfState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfDesignatedRouter
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfDesignatedRouter (INT4 i4FsMIStdOspfIfContextId,
                                     UINT4 u4FsMIStdOspfIfIpAddress,
                                     INT4 i4FsMIStdOspfAddressLessIf,
                                     UINT4
                                     *pu4RetValFsMIStdOspfIfDesignatedRouter)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfDesignatedRouter (u4FsMIStdOspfIfIpAddress,
                                             i4FsMIStdOspfAddressLessIf,
                                             pu4RetValFsMIStdOspfIfDesignatedRouter);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfBackupDesignatedRouter
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfBackupDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfBackupDesignatedRouter (INT4 i4FsMIStdOspfIfContextId,
                                           UINT4 u4FsMIStdOspfIfIpAddress,
                                           INT4 i4FsMIStdOspfAddressLessIf,
                                           UINT4
                                           *pu4RetValFsMIStdOspfIfBackupDesignatedRouter)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfBackupDesignatedRouter (u4FsMIStdOspfIfIpAddress,
                                                   i4FsMIStdOspfAddressLessIf,
                                                   pu4RetValFsMIStdOspfIfBackupDesignatedRouter);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfEvents
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfEvents (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           UINT4 *pu4RetValFsMIStdOspfIfEvents)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfEvents (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   pu4RetValFsMIStdOspfIfEvents);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfAuthKey
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfAuthKey (INT4 i4FsMIStdOspfIfContextId,
                            UINT4 u4FsMIStdOspfIfIpAddress,
                            INT4 i4FsMIStdOspfAddressLessIf,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIStdOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfAuthKey (u4FsMIStdOspfIfIpAddress,
                                    i4FsMIStdOspfAddressLessIf,
                                    pRetValFsMIStdOspfIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfStatus
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfStatus (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           INT4 *pi4RetValFsMIStdOspfIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfStatus (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   pi4RetValFsMIStdOspfIfStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfMulticastForwarding
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfMulticastForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfMulticastForwarding (INT4 i4FsMIStdOspfIfContextId,
                                        UINT4 u4FsMIStdOspfIfIpAddress,
                                        INT4 i4FsMIStdOspfAddressLessIf,
                                        INT4
                                        *pi4RetValFsMIStdOspfIfMulticastForwarding)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfMulticastForwarding (u4FsMIStdOspfIfIpAddress,
                                                i4FsMIStdOspfAddressLessIf,
                                                pi4RetValFsMIStdOspfIfMulticastForwarding);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfDemand
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfDemand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfDemand (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           INT4 *pi4RetValFsMIStdOspfIfDemand)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfDemand (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   pi4RetValFsMIStdOspfIfDemand);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfAuthType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfAuthType (INT4 i4FsMIStdOspfIfContextId,
                             UINT4 u4FsMIStdOspfIfIpAddress,
                             INT4 i4FsMIStdOspfAddressLessIf,
                             INT4 *pi4RetValFsMIStdOspfIfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfAuthType (u4FsMIStdOspfIfIpAddress,
                                     i4FsMIStdOspfAddressLessIf,
                                     pi4RetValFsMIStdOspfIfAuthType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfCryptoAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfCryptoAuthType (INT4 i4FsMIStdOspfIfContextId,
                                   UINT4 u4FsMIStdOspfIfIpAddress,
                                   INT4 i4FsMIStdOspfAddressLessIf,
                                   INT4 *pi4RetValFsMIStdOspfIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFutOspfIfCryptoAuthType (u4FsMIStdOspfIfIpAddress,
                                              (UINT4)
                                              i4FsMIStdOspfAddressLessIf,
                                              pi4RetValFsMIStdOspfIfCryptoAuthType);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfIfMetricTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfIfMetricTable
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfIfMetricTable (INT4 *pi4FsMIStdOspfIfMetricContextId,
                                          UINT4
                                          *pu4FsMIStdOspfIfMetricIpAddress,
                                          INT4
                                          *pi4FsMIStdOspfIfMetricAddressLessIf,
                                          INT4 *pi4FsMIStdOspfIfMetricTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfIfMetricContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfIfMetricContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfIfMetricContextId = *pi4FsMIStdOspfIfMetricContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfIfMetricContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal =
            nmhGetFirstIndexOspfIfMetricTable (pu4FsMIStdOspfIfMetricIpAddress,
                                               pi4FsMIStdOspfIfMetricAddressLessIf,
                                               pi4FsMIStdOspfIfMetricTOS);
        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfIfMetricContextId,
                                 (UINT4 *) pi4FsMIStdOspfIfMetricContextId)
           != OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfIfMetricTable
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                nextFsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                nextFsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                nextFsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS
                nextFsMIStdOspfIfMetricTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfIfMetricTable (INT4 i4FsMIStdOspfIfMetricContextId,
                                         INT4
                                         *pi4NextFsMIStdOspfIfMetricContextId,
                                         UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                         UINT4
                                         *pu4NextFsMIStdOspfIfMetricIpAddress,
                                         INT4
                                         i4FsMIStdOspfIfMetricAddressLessIf,
                                         INT4
                                         *pi4NextFsMIStdOspfIfMetricAddressLessIf,
                                         INT4 i4FsMIStdOspfIfMetricTOS,
                                         INT4 *pi4NextFsMIStdOspfIfMetricTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfIfMetricContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal =
            nmhGetNextIndexOspfIfMetricTable (u4FsMIStdOspfIfMetricIpAddress,
                                              pu4NextFsMIStdOspfIfMetricIpAddress,
                                              i4FsMIStdOspfIfMetricAddressLessIf,
                                              pi4NextFsMIStdOspfIfMetricAddressLessIf,
                                              i4FsMIStdOspfIfMetricTOS,
                                              pi4NextFsMIStdOspfIfMetricTOS);

        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfIfMetricContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfIfMetricContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfIfMetricContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfIfMetricTable
                (pu4NextFsMIStdOspfIfMetricIpAddress,
                 pi4NextFsMIStdOspfIfMetricAddressLessIf,
                 pi4NextFsMIStdOspfIfMetricTOS);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfIfMetricContextId =
                *pi4NextFsMIStdOspfIfMetricContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfIfMetricContextId = i4FsMIStdOspfIfMetricContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfMetricValue
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS

                The Object 
                retValFsMIStdOspfIfMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfMetricValue (INT4 i4FsMIStdOspfIfMetricContextId,
                                UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                INT4 i4FsMIStdOspfIfMetricAddressLessIf,
                                INT4 i4FsMIStdOspfIfMetricTOS,
                                INT4 *pi4RetValFsMIStdOspfIfMetricValue)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfMetricValue (u4FsMIStdOspfIfMetricIpAddress,
                                        i4FsMIStdOspfIfMetricAddressLessIf,
                                        i4FsMIStdOspfIfMetricTOS,
                                        pi4RetValFsMIStdOspfIfMetricValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfMetricStatus
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS

                The Object 
                retValFsMIStdOspfIfMetricStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfIfMetricStatus (INT4 i4FsMIStdOspfIfMetricContextId,
                                 UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                 INT4 i4FsMIStdOspfIfMetricAddressLessIf,
                                 INT4 i4FsMIStdOspfIfMetricTOS,
                                 INT4 *pi4RetValFsMIStdOspfIfMetricStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfIfMetricStatus (u4FsMIStdOspfIfMetricIpAddress,
                                         i4FsMIStdOspfIfMetricAddressLessIf,
                                         i4FsMIStdOspfIfMetricTOS,
                                         pi4RetValFsMIStdOspfIfMetricStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfVirtIfTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfVirtIfTable
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfVirtIfTable (INT4 *pi4FsMIStdOspfVirtIfContextId,
                                        UINT4 *pu4FsMIStdOspfVirtIfAreaId,
                                        UINT4 *pu4FsMIStdOspfVirtIfNeighbor)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfVirtIfContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfVirtIfContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfVirtIfContextId = *pi4FsMIStdOspfVirtIfContextId;
        if (UtilOspfSetContext (*pi4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfVirtIfTable (pu4FsMIStdOspfVirtIfAreaId,
                                                    pu4FsMIStdOspfVirtIfNeighbor);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfVirtIfContextId,
                                 (UINT4 *) pi4FsMIStdOspfVirtIfContextId)
           != OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfVirtIfTable
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                nextFsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                nextFsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor
                nextFsMIStdOspfVirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfVirtIfTable (INT4 i4FsMIStdOspfVirtIfContextId,
                                       INT4 *pi4NextFsMIStdOspfVirtIfContextId,
                                       UINT4 u4FsMIStdOspfVirtIfAreaId,
                                       UINT4 *pu4NextFsMIStdOspfVirtIfAreaId,
                                       UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                       UINT4 *pu4NextFsMIStdOspfVirtIfNeighbor)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfVirtIfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfVirtIfTable (u4FsMIStdOspfVirtIfAreaId,
                                                   pu4NextFsMIStdOspfVirtIfAreaId,
                                                   u4FsMIStdOspfVirtIfNeighbor,
                                                   pu4NextFsMIStdOspfVirtIfNeighbor);

        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfVirtIfContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfVirtIfContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfVirtIfContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfVirtIfTable (pu4NextFsMIStdOspfVirtIfAreaId,
                                                 pu4NextFsMIStdOspfVirtIfNeighbor);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfVirtIfContextId = *pi4NextFsMIStdOspfVirtIfContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfVirtIfContextId = i4FsMIStdOspfVirtIfContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfTransitDelay (INT4 i4FsMIStdOspfVirtIfContextId,
                                     UINT4 u4FsMIStdOspfVirtIfAreaId,
                                     UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                     INT4
                                     *pi4RetValFsMIStdOspfVirtIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfTransitDelay (u4FsMIStdOspfVirtIfAreaId,
                                             u4FsMIStdOspfVirtIfNeighbor,
                                             pi4RetValFsMIStdOspfVirtIfTransitDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfRetransInterval (INT4 i4FsMIStdOspfVirtIfContextId,
                                        UINT4 u4FsMIStdOspfVirtIfAreaId,
                                        UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                        INT4
                                        *pi4RetValFsMIStdOspfVirtIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfRetransInterval (u4FsMIStdOspfVirtIfAreaId,
                                                u4FsMIStdOspfVirtIfNeighbor,
                                                pi4RetValFsMIStdOspfVirtIfRetransInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfHelloInterval (INT4 i4FsMIStdOspfVirtIfContextId,
                                      UINT4 u4FsMIStdOspfVirtIfAreaId,
                                      UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                      INT4
                                      *pi4RetValFsMIStdOspfVirtIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfHelloInterval (u4FsMIStdOspfVirtIfAreaId,
                                              u4FsMIStdOspfVirtIfNeighbor,
                                              pi4RetValFsMIStdOspfVirtIfHelloInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfRtrDeadInterval (INT4 i4FsMIStdOspfVirtIfContextId,
                                        UINT4 u4FsMIStdOspfVirtIfAreaId,
                                        UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                        INT4
                                        *pi4RetValFsMIStdOspfVirtIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfRtrDeadInterval (u4FsMIStdOspfVirtIfAreaId,
                                                u4FsMIStdOspfVirtIfNeighbor,
                                                pi4RetValFsMIStdOspfVirtIfRtrDeadInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfState
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfState (INT4 i4FsMIStdOspfVirtIfContextId,
                              UINT4 u4FsMIStdOspfVirtIfAreaId,
                              UINT4 u4FsMIStdOspfVirtIfNeighbor,
                              INT4 *pi4RetValFsMIStdOspfVirtIfState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfState (u4FsMIStdOspfVirtIfAreaId,
                                      u4FsMIStdOspfVirtIfNeighbor,
                                      pi4RetValFsMIStdOspfVirtIfState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfEvents
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfEvents (INT4 i4FsMIStdOspfVirtIfContextId,
                               UINT4 u4FsMIStdOspfVirtIfAreaId,
                               UINT4 u4FsMIStdOspfVirtIfNeighbor,
                               UINT4 *pu4RetValFsMIStdOspfVirtIfEvents)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfEvents (u4FsMIStdOspfVirtIfAreaId,
                                       u4FsMIStdOspfVirtIfNeighbor,
                                       pu4RetValFsMIStdOspfVirtIfEvents);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfAuthKey (INT4 i4FsMIStdOspfVirtIfContextId,
                                UINT4 u4FsMIStdOspfVirtIfAreaId,
                                UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIStdOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfAuthKey (u4FsMIStdOspfVirtIfAreaId,
                                        u4FsMIStdOspfVirtIfNeighbor,
                                        pRetValFsMIStdOspfVirtIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfStatus
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfStatus (INT4 i4FsMIStdOspfVirtIfContextId,
                               UINT4 u4FsMIStdOspfVirtIfAreaId,
                               UINT4 u4FsMIStdOspfVirtIfNeighbor,
                               INT4 *pi4RetValFsMIStdOspfVirtIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfStatus (u4FsMIStdOspfVirtIfAreaId,
                                       u4FsMIStdOspfVirtIfNeighbor,
                                       pi4RetValFsMIStdOspfVirtIfStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtIfAuthType
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                retValFsMIStdOspfVirtIfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtIfAuthType (INT4 i4FsMIStdOspfVirtIfContextId,
                                 UINT4 u4FsMIStdOspfVirtIfAreaId,
                                 UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                 INT4 *pi4RetValFsMIStdOspfVirtIfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtIfAuthType (u4FsMIStdOspfVirtIfAreaId,
                                         u4FsMIStdOspfVirtIfNeighbor,
                                         pi4RetValFsMIStdOspfVirtIfAuthType);
    UtilOspfResetContext ();
    return i1Return;
}

INT1
nmhGetFsMIStdOspfVirtIfCryptoAuthType (INT4 i4FsMIStdOspfVirtIfContextId,
                                       UINT4 u4FsMIStdOspfVirtIfAreaId,
                                       UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                       INT4
                                       *pi4RetValFsMIStdOspfVirtIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfVirtIfContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFutOspfVirtIfCryptoAuthType (u4FsMIStdOspfVirtIfAreaId,
                                                  u4FsMIStdOspfVirtIfNeighbor,
                                                  pi4RetValFsMIStdOspfVirtIfCryptoAuthType);

    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfNbrTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfNbrTable
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfNbrTable (INT4 *pi4FsMIStdOspfNbrContextId,
                                     UINT4 *pu4FsMIStdOspfNbrIpAddr,
                                     INT4 *pi4FsMIStdOspfNbrAddressLessIndex)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfNbrContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfNbrContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIStdOspfNbrContextId = *pi4FsMIStdOspfNbrContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfNbrTable (pu4FsMIStdOspfNbrIpAddr,
                                                 pi4FsMIStdOspfNbrAddressLessIndex);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfNbrContextId,
                                 (UINT4 *) pi4FsMIStdOspfNbrContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfNbrTable
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                nextFsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                nextFsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex
                nextFsMIStdOspfNbrAddressLessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfNbrTable (INT4 i4FsMIStdOspfNbrContextId,
                                    INT4 *pi4NextFsMIStdOspfNbrContextId,
                                    UINT4 u4FsMIStdOspfNbrIpAddr,
                                    UINT4 *pu4NextFsMIStdOspfNbrIpAddr,
                                    INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                    INT4 *pi4NextFsMIStdOspfNbrAddressLessIndex)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfNbrContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfNbrTable (u4FsMIStdOspfNbrIpAddr,
                                                pu4NextFsMIStdOspfNbrIpAddr,
                                                i4FsMIStdOspfNbrAddressLessIndex,
                                                pi4NextFsMIStdOspfNbrAddressLessIndex);

        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfNbrContextId,
                                      (UINT4 *) pi4NextFsMIStdOspfNbrContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfNbrContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfNbrTable (pu4NextFsMIStdOspfNbrIpAddr,
                                              pi4NextFsMIStdOspfNbrAddressLessIndex);

            UtilOspfResetContext ();
            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfNbrContextId = *pi4NextFsMIStdOspfNbrContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfNbrContextId = i4FsMIStdOspfNbrContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrRtrId
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrRtrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrRtrId (INT4 i4FsMIStdOspfNbrContextId,
                           UINT4 u4FsMIStdOspfNbrIpAddr,
                           INT4 i4FsMIStdOspfNbrAddressLessIndex,
                           UINT4 *pu4RetValFsMIStdOspfNbrRtrId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrRtrId (u4FsMIStdOspfNbrIpAddr,
                                   i4FsMIStdOspfNbrAddressLessIndex,
                                   pu4RetValFsMIStdOspfNbrRtrId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrOptions
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrOptions (INT4 i4FsMIStdOspfNbrContextId,
                             UINT4 u4FsMIStdOspfNbrIpAddr,
                             INT4 i4FsMIStdOspfNbrAddressLessIndex,
                             INT4 *pi4RetValFsMIStdOspfNbrOptions)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrOptions (u4FsMIStdOspfNbrIpAddr,
                                     i4FsMIStdOspfNbrAddressLessIndex,
                                     pi4RetValFsMIStdOspfNbrOptions);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrPriority
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrPriority (INT4 i4FsMIStdOspfNbrContextId,
                              UINT4 u4FsMIStdOspfNbrIpAddr,
                              INT4 i4FsMIStdOspfNbrAddressLessIndex,
                              INT4 *pi4RetValFsMIStdOspfNbrPriority)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrPriority (u4FsMIStdOspfNbrIpAddr,
                                      i4FsMIStdOspfNbrAddressLessIndex,
                                      pi4RetValFsMIStdOspfNbrPriority);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrState
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrState (INT4 i4FsMIStdOspfNbrContextId,
                           UINT4 u4FsMIStdOspfNbrIpAddr,
                           INT4 i4FsMIStdOspfNbrAddressLessIndex,
                           INT4 *pi4RetValFsMIStdOspfNbrState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrState (u4FsMIStdOspfNbrIpAddr,
                                   i4FsMIStdOspfNbrAddressLessIndex,
                                   pi4RetValFsMIStdOspfNbrState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrEvents
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrEvents (INT4 i4FsMIStdOspfNbrContextId,
                            UINT4 u4FsMIStdOspfNbrIpAddr,
                            INT4 i4FsMIStdOspfNbrAddressLessIndex,
                            UINT4 *pu4RetValFsMIStdOspfNbrEvents)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrEvents (u4FsMIStdOspfNbrIpAddr,
                                    i4FsMIStdOspfNbrAddressLessIndex,
                                    pu4RetValFsMIStdOspfNbrEvents);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrLsRetransQLen
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrLsRetransQLen (INT4 i4FsMIStdOspfNbrContextId,
                                   UINT4 u4FsMIStdOspfNbrIpAddr,
                                   INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                   UINT4 *pu4RetValFsMIStdOspfNbrLsRetransQLen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrLsRetransQLen (u4FsMIStdOspfNbrIpAddr,
                                           i4FsMIStdOspfNbrAddressLessIndex,
                                           pu4RetValFsMIStdOspfNbrLsRetransQLen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbmaNbrStatus
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbmaNbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbmaNbrStatus (INT4 i4FsMIStdOspfNbrContextId,
                                UINT4 u4FsMIStdOspfNbrIpAddr,
                                INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                INT4 *pi4RetValFsMIStdOspfNbmaNbrStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbmaNbrStatus (u4FsMIStdOspfNbrIpAddr,
                                        i4FsMIStdOspfNbrAddressLessIndex,
                                        pi4RetValFsMIStdOspfNbmaNbrStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbmaNbrPermanence
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbmaNbrPermanence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbmaNbrPermanence (INT4 i4FsMIStdOspfNbrContextId,
                                    UINT4 u4FsMIStdOspfNbrIpAddr,
                                    INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                    INT4 *pi4RetValFsMIStdOspfNbmaNbrPermanence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbmaNbrPermanence (u4FsMIStdOspfNbrIpAddr,
                                            i4FsMIStdOspfNbrAddressLessIndex,
                                            pi4RetValFsMIStdOspfNbmaNbrPermanence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfNbrHelloSuppressed
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                retValFsMIStdOspfNbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfNbrHelloSuppressed (INT4 i4FsMIStdOspfNbrContextId,
                                     UINT4 u4FsMIStdOspfNbrIpAddr,
                                     INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                     INT4
                                     *pi4RetValFsMIStdOspfNbrHelloSuppressed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfNbrHelloSuppressed (u4FsMIStdOspfNbrIpAddr,
                                             i4FsMIStdOspfNbrAddressLessIndex,
                                             pi4RetValFsMIStdOspfNbrHelloSuppressed);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfVirtNbrTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfVirtNbrTable
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfVirtNbrTable (INT4 *pi4FsMIStdOspfVirtNbrContextId,
                                         UINT4 *pu4FsMIStdOspfVirtNbrArea,
                                         UINT4 *pu4FsMIStdOspfVirtNbrRtrId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfVirtNbrContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfVirtNbrContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIStdOspfVirtNbrContextId = *pi4FsMIStdOspfVirtNbrContextId;
        if (UtilOspfSetContext (*pi4FsMIStdOspfVirtNbrContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfVirtNbrTable (pu4FsMIStdOspfVirtNbrArea,
                                                     pu4FsMIStdOspfVirtNbrRtrId);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfVirtNbrContextId,
                                 (UINT4 *) pi4FsMIStdOspfVirtNbrContextId)
           != OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfVirtNbrTable
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                nextFsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                nextFsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId
                nextFsMIStdOspfVirtNbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfVirtNbrTable (INT4 i4FsMIStdOspfVirtNbrContextId,
                                        INT4
                                        *pi4NextFsMIStdOspfVirtNbrContextId,
                                        UINT4 u4FsMIStdOspfVirtNbrArea,
                                        UINT4 *pu4NextFsMIStdOspfVirtNbrArea,
                                        UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                        UINT4 *pu4NextFsMIStdOspfVirtNbrRtrId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfVirtNbrContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfVirtNbrTable (u4FsMIStdOspfVirtNbrArea,
                                                    pu4NextFsMIStdOspfVirtNbrArea,
                                                    u4FsMIStdOspfVirtNbrRtrId,
                                                    pu4NextFsMIStdOspfVirtNbrRtrId);

        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfVirtNbrContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfVirtNbrContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfVirtNbrContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfVirtNbrTable (pu4NextFsMIStdOspfVirtNbrArea,
                                                  pu4NextFsMIStdOspfVirtNbrRtrId);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfVirtNbrContextId = *pi4NextFsMIStdOspfVirtNbrContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfVirtNbrContextId = i4FsMIStdOspfVirtNbrContextId;
    }
    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtNbrIpAddr
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId

                The Object 
                retValFsMIStdOspfVirtNbrIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtNbrIpAddr (INT4 i4FsMIStdOspfVirtNbrContextId,
                                UINT4 u4FsMIStdOspfVirtNbrArea,
                                UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                UINT4 *pu4RetValFsMIStdOspfVirtNbrIpAddr)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtNbrIpAddr (u4FsMIStdOspfVirtNbrArea,
                                        u4FsMIStdOspfVirtNbrRtrId,
                                        pu4RetValFsMIStdOspfVirtNbrIpAddr);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtNbrOptions
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId

                The Object 
                retValFsMIStdOspfVirtNbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtNbrOptions (INT4 i4FsMIStdOspfVirtNbrContextId,
                                 UINT4 u4FsMIStdOspfVirtNbrArea,
                                 UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                 INT4 *pi4RetValFsMIStdOspfVirtNbrOptions)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtNbrOptions (u4FsMIStdOspfVirtNbrArea,
                                         u4FsMIStdOspfVirtNbrRtrId,
                                         pi4RetValFsMIStdOspfVirtNbrOptions);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtNbrState
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId

                The Object 
                retValFsMIStdOspfVirtNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtNbrState (INT4 i4FsMIStdOspfVirtNbrContextId,
                               UINT4 u4FsMIStdOspfVirtNbrArea,
                               UINT4 u4FsMIStdOspfVirtNbrRtrId,
                               INT4 *pi4RetValFsMIStdOspfVirtNbrState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtNbrState (u4FsMIStdOspfVirtNbrArea,
                                       u4FsMIStdOspfVirtNbrRtrId,
                                       pi4RetValFsMIStdOspfVirtNbrState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtNbrEvents
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId

                The Object 
                retValFsMIStdOspfVirtNbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtNbrEvents (INT4 i4FsMIStdOspfVirtNbrContextId,
                                UINT4 u4FsMIStdOspfVirtNbrArea,
                                UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                UINT4 *pu4RetValFsMIStdOspfVirtNbrEvents)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtNbrEvents (u4FsMIStdOspfVirtNbrArea,
                                        u4FsMIStdOspfVirtNbrRtrId,
                                        pu4RetValFsMIStdOspfVirtNbrEvents);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtNbrLsRetransQLen
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId

                The Object 
                retValFsMIStdOspfVirtNbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtNbrLsRetransQLen (INT4 i4FsMIStdOspfVirtNbrContextId,
                                       UINT4 u4FsMIStdOspfVirtNbrArea,
                                       UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                       UINT4
                                       *pu4RetValFsMIStdOspfVirtNbrLsRetransQLen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtNbrLsRetransQLen (u4FsMIStdOspfVirtNbrArea,
                                               u4FsMIStdOspfVirtNbrRtrId,
                                               pu4RetValFsMIStdOspfVirtNbrLsRetransQLen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfVirtNbrHelloSuppressed
 Input       :  The Indices
                FsMIStdOspfVirtNbrContextId
                FsMIStdOspfVirtNbrArea
                FsMIStdOspfVirtNbrRtrId

                The Object 
                retValFsMIStdOspfVirtNbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfVirtNbrHelloSuppressed (INT4 i4FsMIStdOspfVirtNbrContextId,
                                         UINT4 u4FsMIStdOspfVirtNbrArea,
                                         UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                         INT4
                                         *pi4RetValFsMIStdOspfVirtNbrHelloSuppressed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfVirtNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfVirtNbrHelloSuppressed (u4FsMIStdOspfVirtNbrArea,
                                                 u4FsMIStdOspfVirtNbrRtrId,
                                                 pi4RetValFsMIStdOspfVirtNbrHelloSuppressed);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfExtLsdbTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfExtLsdbTable
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfExtLsdbTable (INT4 *pi4FsMIStdOspfExtLsdbContextId,
                                         INT4 *pi4FsMIStdOspfExtLsdbType,
                                         UINT4 *pu4FsMIStdOspfExtLsdbLsid,
                                         UINT4 *pu4FsMIStdOspfExtLsdbRouterId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfExtLsdbContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfExtLsdbContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIStdOspfExtLsdbContextId = *pi4FsMIStdOspfExtLsdbContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfExtLsdbContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetFirstIndexOspfExtLsdbTable (pi4FsMIStdOspfExtLsdbType,
                                                     pu4FsMIStdOspfExtLsdbLsid,
                                                     pu4FsMIStdOspfExtLsdbRouterId);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfExtLsdbContextId,
                                 (UINT4 *) pi4FsMIStdOspfExtLsdbContextId)
           != OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfExtLsdbTable
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                nextFsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                nextFsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                nextFsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId
                nextFsMIStdOspfExtLsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfExtLsdbTable (INT4 i4FsMIStdOspfExtLsdbContextId,
                                        INT4
                                        *pi4NextFsMIStdOspfExtLsdbContextId,
                                        INT4 i4FsMIStdOspfExtLsdbType,
                                        INT4 *pi4NextFsMIStdOspfExtLsdbType,
                                        UINT4 u4FsMIStdOspfExtLsdbLsid,
                                        UINT4 *pu4NextFsMIStdOspfExtLsdbLsid,
                                        UINT4 u4FsMIStdOspfExtLsdbRouterId,
                                        UINT4
                                        *pu4NextFsMIStdOspfExtLsdbRouterId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfExtLsdbContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfExtLsdbContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal = nmhGetNextIndexOspfExtLsdbTable (i4FsMIStdOspfExtLsdbType,
                                                    pi4NextFsMIStdOspfExtLsdbType,
                                                    u4FsMIStdOspfExtLsdbLsid,
                                                    pu4NextFsMIStdOspfExtLsdbLsid,
                                                    u4FsMIStdOspfExtLsdbRouterId,
                                                    pu4NextFsMIStdOspfExtLsdbRouterId);

        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfExtLsdbContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfExtLsdbContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfExtLsdbContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfExtLsdbTable (pi4NextFsMIStdOspfExtLsdbType,
                                                  pu4NextFsMIStdOspfExtLsdbLsid,
                                                  pu4NextFsMIStdOspfExtLsdbRouterId);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfExtLsdbContextId = *pi4NextFsMIStdOspfExtLsdbContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfExtLsdbContextId = i4FsMIStdOspfExtLsdbContextId;
    }

    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExtLsdbSequence
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId

                The Object 
                retValFsMIStdOspfExtLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExtLsdbSequence (INT4 i4FsMIStdOspfExtLsdbContextId,
                                  INT4 i4FsMIStdOspfExtLsdbType,
                                  UINT4 u4FsMIStdOspfExtLsdbLsid,
                                  UINT4 u4FsMIStdOspfExtLsdbRouterId,
                                  INT4 *pi4RetValFsMIStdOspfExtLsdbSequence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfExtLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfExtLsdbSequence (i4FsMIStdOspfExtLsdbType,
                                          u4FsMIStdOspfExtLsdbLsid,
                                          u4FsMIStdOspfExtLsdbRouterId,
                                          pi4RetValFsMIStdOspfExtLsdbSequence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExtLsdbAge
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId

                The Object 
                retValFsMIStdOspfExtLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExtLsdbAge (INT4 i4FsMIStdOspfExtLsdbContextId,
                             INT4 i4FsMIStdOspfExtLsdbType,
                             UINT4 u4FsMIStdOspfExtLsdbLsid,
                             UINT4 u4FsMIStdOspfExtLsdbRouterId,
                             INT4 *pi4RetValFsMIStdOspfExtLsdbAge)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfExtLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfExtLsdbAge (i4FsMIStdOspfExtLsdbType,
                                     u4FsMIStdOspfExtLsdbLsid,
                                     u4FsMIStdOspfExtLsdbRouterId,
                                     pi4RetValFsMIStdOspfExtLsdbAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExtLsdbChecksum
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId

                The Object 
                retValFsMIStdOspfExtLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExtLsdbChecksum (INT4 i4FsMIStdOspfExtLsdbContextId,
                                  INT4 i4FsMIStdOspfExtLsdbType,
                                  UINT4 u4FsMIStdOspfExtLsdbLsid,
                                  UINT4 u4FsMIStdOspfExtLsdbRouterId,
                                  INT4 *pi4RetValFsMIStdOspfExtLsdbChecksum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfExtLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfExtLsdbChecksum (i4FsMIStdOspfExtLsdbType,
                                          u4FsMIStdOspfExtLsdbLsid,
                                          u4FsMIStdOspfExtLsdbRouterId,
                                          pi4RetValFsMIStdOspfExtLsdbChecksum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfExtLsdbAdvertisement
 Input       :  The Indices
                FsMIStdOspfExtLsdbContextId
                FsMIStdOspfExtLsdbType
                FsMIStdOspfExtLsdbLsid
                FsMIStdOspfExtLsdbRouterId

                The Object 
                retValFsMIStdOspfExtLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfExtLsdbAdvertisement (INT4 i4FsMIStdOspfExtLsdbContextId,
                                       INT4 i4FsMIStdOspfExtLsdbType,
                                       UINT4 u4FsMIStdOspfExtLsdbLsid,
                                       UINT4 u4FsMIStdOspfExtLsdbRouterId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIStdOspfExtLsdbAdvertisement)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfExtLsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfExtLsdbAdvertisement (i4FsMIStdOspfExtLsdbType,
                                               u4FsMIStdOspfExtLsdbLsid,
                                               u4FsMIStdOspfExtLsdbRouterId,
                                               pRetValFsMIStdOspfExtLsdbAdvertisement);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Get Routines for Table : FsMIStdOspfAreaAggregateTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfAreaAggregateTable
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfAreaAggregateTable (INT4
                                               *pi4FsMIStdOspfAreaAggregateContextId,
                                               UINT4
                                               *pu4FsMIStdOspfAreaAggregateAreaID,
                                               INT4
                                               *pi4FsMIStdOspfAreaAggregateLsdbType,
                                               UINT4
                                               *pu4FsMIStdOspfAreaAggregateNet,
                                               UINT4
                                               *pu4FsMIStdOspfAreaAggregateMask)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfAreaAggregateContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfAreaAggregateContextId)
        == OSPF_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIStdOspfAreaAggregateContextId =
            *pi4FsMIStdOspfAreaAggregateContextId;
        if (UtilOspfSetContext (*pi4FsMIStdOspfAreaAggregateContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal =
            nmhGetFirstIndexOspfAreaAggregateTable
            (pu4FsMIStdOspfAreaAggregateAreaID,
             pi4FsMIStdOspfAreaAggregateLsdbType,
             pu4FsMIStdOspfAreaAggregateNet, pu4FsMIStdOspfAreaAggregateMask);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfAreaAggregateContextId,
                                 (UINT4 *) pi4FsMIStdOspfAreaAggregateContextId)
           != OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfAreaAggregateTable
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                nextFsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                nextFsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                nextFsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                nextFsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask
                nextFsMIStdOspfAreaAggregateMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfAreaAggregateTable (INT4
                                              i4FsMIStdOspfAreaAggregateContextId,
                                              INT4
                                              *pi4NextFsMIStdOspfAreaAggregateContextId,
                                              UINT4
                                              u4FsMIStdOspfAreaAggregateAreaID,
                                              UINT4
                                              *pu4NextFsMIStdOspfAreaAggregateAreaID,
                                              INT4
                                              i4FsMIStdOspfAreaAggregateLsdbType,
                                              INT4
                                              *pi4NextFsMIStdOspfAreaAggregateLsdbType,
                                              UINT4
                                              u4FsMIStdOspfAreaAggregateNet,
                                              UINT4
                                              *pu4NextFsMIStdOspfAreaAggregateNet,
                                              UINT4
                                              u4FsMIStdOspfAreaAggregateMask,
                                              UINT4
                                              *pu4NextFsMIStdOspfAreaAggregateMask)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfAreaAggregateContextId) ==
        OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfAreaAggregateContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }

        i4RetVal =
            nmhGetNextIndexOspfAreaAggregateTable
            (u4FsMIStdOspfAreaAggregateAreaID,
             pu4NextFsMIStdOspfAreaAggregateAreaID,
             i4FsMIStdOspfAreaAggregateLsdbType,
             pi4NextFsMIStdOspfAreaAggregateLsdbType,
             u4FsMIStdOspfAreaAggregateNet, pu4NextFsMIStdOspfAreaAggregateNet,
             u4FsMIStdOspfAreaAggregateMask,
             pu4NextFsMIStdOspfAreaAggregateMask);

        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId
                ((UINT4) i4FsMIStdOspfAreaAggregateContextId,
                 (UINT4 *) pi4NextFsMIStdOspfAreaAggregateContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfAreaAggregateContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexOspfAreaAggregateTable
                (pu4NextFsMIStdOspfAreaAggregateAreaID,
                 pi4NextFsMIStdOspfAreaAggregateLsdbType,
                 pu4NextFsMIStdOspfAreaAggregateNet,
                 pu4NextFsMIStdOspfAreaAggregateMask);

            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfAreaAggregateContextId =
                *pi4NextFsMIStdOspfAreaAggregateContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfAreaAggregateContextId =
            i4FsMIStdOspfAreaAggregateContextId;
    }
    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaAggregateStatus
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask

                The Object 
                retValFsMIStdOspfAreaAggregateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaAggregateStatus (INT4 i4FsMIStdOspfAreaAggregateContextId,
                                      UINT4 u4FsMIStdOspfAreaAggregateAreaID,
                                      INT4 i4FsMIStdOspfAreaAggregateLsdbType,
                                      UINT4 u4FsMIStdOspfAreaAggregateNet,
                                      UINT4 u4FsMIStdOspfAreaAggregateMask,
                                      INT4
                                      *pi4RetValFsMIStdOspfAreaAggregateStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaAggregateStatus (u4FsMIStdOspfAreaAggregateAreaID,
                                              i4FsMIStdOspfAreaAggregateLsdbType,
                                              u4FsMIStdOspfAreaAggregateNet,
                                              u4FsMIStdOspfAreaAggregateMask,
                                              pi4RetValFsMIStdOspfAreaAggregateStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfAreaAggregateEffect
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask

                The Object 
                retValFsMIStdOspfAreaAggregateEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfAreaAggregateEffect (INT4 i4FsMIStdOspfAreaAggregateContextId,
                                      UINT4 u4FsMIStdOspfAreaAggregateAreaID,
                                      INT4 i4FsMIStdOspfAreaAggregateLsdbType,
                                      UINT4 u4FsMIStdOspfAreaAggregateNet,
                                      UINT4 u4FsMIStdOspfAreaAggregateMask,
                                      INT4
                                      *pi4RetValFsMIStdOspfAreaAggregateEffect)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfAreaAggregateEffect (u4FsMIStdOspfAreaAggregateAreaID,
                                              i4FsMIStdOspfAreaAggregateLsdbType,
                                              u4FsMIStdOspfAreaAggregateNet,
                                              u4FsMIStdOspfAreaAggregateMask,
                                              pi4RetValFsMIStdOspfAreaAggregateEffect);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfGlobalTraceLevel
 Input       :  The Indices

                The Object
                retValFsMIOspfGlobalTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfGlobalTraceLevel (INT4 *pi4RetValFsMIOspfGlobalTraceLevel)
{
    *pi4RetValFsMIOspfGlobalTraceLevel = gOsRtr.u4OspfGblTrace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVrfSpfInterval
 Input       :  The Indices

                The Object
                retValFsMIOspfVrfSpfInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVrfSpfInterval (INT4 *pi4RetValFsMIOspfVrfSpfInterval)
{
    *pi4RetValFsMIOspfVrfSpfInterval = (INT4) gOsRtr.u4VrfSpfInterval;
    return SNMP_SUCCESS;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfTable
 Input       :  The Indices
                FsMIOspfContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMIOspfTable (INT4 *pi4FsMIOspfContextId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfContextId) == OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfTable
 Input       :  The Indices
                FsMIOspfContextId
                nextFsMIOspfContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfTable (INT4 i4FsMIOspfContextId,
                              INT4 *pi4NextFsMIOspfContextId)
{
    if (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfContextId,
                              (UINT4 *) pi4NextFsMIOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfOverFlowState
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfOverFlowState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfOverFlowState (INT4 i4FsMIOspfContextId,
                             INT4 *pi4RetValFsMIOspfOverFlowState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfOverFlowState (pi4RetValFsMIOspfOverFlowState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfPktsRcvd
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfPktsRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfPktsRcvd (INT4 i4FsMIOspfContextId,
                        UINT4 *pu4RetValFsMIOspfPktsRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfPktsRcvd (pu4RetValFsMIOspfPktsRcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfPktsTxed
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfPktsTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfPktsTxed (INT4 i4FsMIOspfContextId,
                        UINT4 *pu4RetValFsMIOspfPktsTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfPktsTxed (pu4RetValFsMIOspfPktsTxed);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfPktsDisd
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfPktsDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfPktsDisd (INT4 i4FsMIOspfContextId,
                        UINT4 *pu4RetValFsMIOspfPktsDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfPktsDisd (pu4RetValFsMIOspfPktsDisd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRFC1583Compatibility
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfRFC1583Compatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRFC1583Compatibility (INT4 i4FsMIOspfContextId,
                                    INT4 *pi4RetValFsMIOspfRFC1583Compatibility)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfRFC1583Compatibility
        (pi4RetValFsMIOspfRFC1583Compatibility);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfTraceLevel
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfTraceLevel (INT4 i4FsMIOspfContextId,
                          INT4 *pi4RetValFsMIOspfTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfTraceLevel (pi4RetValFsMIOspfTraceLevel);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfMinLsaInterval
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfMinLsaInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfMinLsaInterval (INT4 i4FsMIOspfContextId,
                              INT4 *pi4RetValFsMIOspfMinLsaInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfMinLsaInterval (pi4RetValFsMIOspfMinLsaInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfABRType
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfABRType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfABRType (INT4 i4FsMIOspfContextId, INT4 *pi4RetValFsMIOspfABRType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfABRType (pi4RetValFsMIOspfABRType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfNssaAsbrDefRtTrans
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfNssaAsbrDefRtTrans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfNssaAsbrDefRtTrans (INT4 i4FsMIOspfContextId,
                                  INT4 *pi4RetValFsMIOspfNssaAsbrDefRtTrans)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfNssaAsbrDefRtTrans (pi4RetValFsMIOspfNssaAsbrDefRtTrans);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfDefaultPassiveInterface
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                retValFsMIOspfDefaultPassiveInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfDefaultPassiveInterface (INT4 i4FsMIOspfContextId,
                                       INT4
                                       *pi4RetValFsMIOspfDefaultPassiveInterface)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfDefaultPassiveInterface
        (pi4RetValFsMIOspfDefaultPassiveInterface);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfSpfHoldtime
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfSpfHoldtime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfSpfHoldtime (INT4 i4FsMIStdOspfContextId,
                           INT4 *pi4RetValFsMIOspfSpfHoldtime)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfSpfHoldtime (pi4RetValFsMIOspfSpfHoldtime);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfSpfDelay
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfSpfDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfSpfDelay (INT4 i4FsMIStdOspfContextId,
                        INT4 *pi4RetValFsMIOspfSpfDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfSpfDelay (pi4RetValFsMIOspfSpfDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRestartSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRestartSupport (INT4 i4FsMIStdOspfContextId,
                              INT4 *pi4RetValFsMIOspfRestartSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartSupport (pi4RetValFsMIOspfRestartSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRestartInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsMIOspfRestartInterval (INT4 i4FsMIStdOspfContextId,
                               INT4 *pi4RetValFsMIOspfRestartInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartInterval (pi4RetValFsMIOspfRestartInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartStrictLsaChecking
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRestartStrictLsaChecking
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIOspfRestartStrictLsaChecking
    (INT4 i4FsMIStdOspfContextId,
     INT4 *pi4RetValFsMIOspfRestartStrictLsaChecking)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartStrictLsaChecking
        (pi4RetValFsMIOspfRestartStrictLsaChecking);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRestartStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRestartStatus (INT4 i4FsMIStdOspfContextId,
                             INT4 *pi4RetValFsMIOspfRestartStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartStatus (pi4RetValFsMIOspfRestartStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartAge
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRestartAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRestartAge (INT4 i4FsMIStdOspfContextId,
                          UINT4 *pu4RetValFsMIOspfRestartAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartAge (pu4RetValFsMIOspfRestartAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartExitReason
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRestartExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRestartExitReason (INT4 i4FsMIStdOspfContextId,
                                 INT4 *pi4RetValFsMIOspfRestartExitReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartExitReason
        (pi4RetValFsMIOspfRestartExitReason);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfHelperSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfHelperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfHelperSupport (INT4 i4FsMIStdOspfContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIOspfHelperSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfHelperSupport (pRetValFsMIOspfHelperSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtTraceLevel
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfExtTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtTraceLevel (INT4 i4FsMIStdOspfContextId,
                             INT4 *pi4RetValFsMIOspfExtTraceLevel)
{

    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtTraceLevel (pi4RetValFsMIOspfExtTraceLevel);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfHelperGraceTimeLimit
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfHelperGraceTimeLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfHelperGraceTimeLimit (INT4 i4FsMIStdOspfContextId,
                                    INT4 *pi4RetValFsMIOspfHelperGraceTimeLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfHelperGraceTimeLimit
        (pi4RetValFsMIOspfHelperGraceTimeLimit);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartAckState
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfRestartAckState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRestartAckState (INT4 i4FsMIStdOspfContextId,
                               INT4 *pi4RetValFsMIOspfRestartAckState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartAckState (pi4RetValFsMIOspfRestartAckState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfGraceLsaRetransmitCount
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfGraceLsaRetransmitCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfGraceLsaRetransmitCount (INT4 i4FsMIStdOspfContextId,
                                       INT4
                                       *pi4RetValFsMIOspfGraceLsaRetransmitCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfGraceLsaRetransmitCount
        (pi4RetValFsMIOspfGraceLsaRetransmitCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRestartReason
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfRestartReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRestartReason (INT4 i4FsMIStdOspfContextId,
                             INT4 *pi4RetValFsMIOspfRestartReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRestartReason (pi4RetValFsMIOspfRestartReason);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfGlobalExtTraceLevel
 Input       :  The Indices

                The Object
                retValFsMIOspfGlobalExtTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfGlobalExtTraceLevel (INT4 *pi4RetValFsMIOspfGlobalExtTraceLevel)
{

    *pi4RetValFsMIOspfGlobalExtTraceLevel = gOsRtr.u4OspfGblExtTrace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRTStaggeringInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfRTStaggeringInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRTStaggeringInterval (INT4 i4FsMIStdOspfContextId,
                                    UINT4
                                    *pu4RetValFsMIOspfRTStaggeringInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfRTStaggeringInterval ((UINT4 *)
                                           pu4RetValFsMIOspfRTStaggeringInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************                      Function    :  nmhGetFsMIOspfRouterIdPermanence
  Input       :  The Indices
                 FsMIStdOspfContextId
                 The Object
                 retValFsMIOspfRouterIdPermanence
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE                                                     ****************************************************************************/
INT1
nmhGetFsMIOspfRouterIdPermanence (INT4 i4FsMIStdOspfContextId,
                                  INT4 *pi4RetValFsMIOspfRouterIdPermanence)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhGetFutospfRouterIdPermanence (pi4RetValFsMIOspfRouterIdPermanence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfBfdStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                The Object
                retValFsMIOspfBfdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMIOspfBfdStatus (INT4 i4FsMIStdOspfContextId,
                         INT4 *pi4RetValFsMIOspfBfdStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFutOspfBfdStatus (pi4RetValFsMIOspfBfdStatus);
    UtilOspfResetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfBfdAllIfState
 Input       :  The Indices
                FsMIStdOspfContextId
                The Object
                retValFsMIOspfBfdAllIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMIOspfBfdAllIfState (INT4 i4FsMIStdOspfContextId,
                             INT4 *pi4RetValFsMIOspfBfdAllIfState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFutOspfBfdAllIfState (pi4RetValFsMIOspfBfdAllIfState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRTStaggeringStatus
 Input       :  The Indices

                The Object
                retValFsMIOspfRTStaggeringStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRTStaggeringStatus (INT4 *pi4RetValFsMIOspfRTStaggeringStatus)
{
    INT1                i4RetVal = SNMP_FAILURE;

    i4RetVal =
        nmhGetFutOspfRTStaggeringStatus (pi4RetValFsMIOspfRTStaggeringStatus);
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfHotStandbyAdminStatus
 Input       :  The Indices

                The Object
                retValFsMIOspfHotStandbyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfHotStandbyAdminStatus (INT4
                                     *pi4RetValFsMIOspfHotStandbyAdminStatus)
{
    INT1                i4RetVal = SNMP_FAILURE;

    i4RetVal =
        nmhGetFutOspfHotStandbyAdminStatus
        (pi4RetValFsMIOspfHotStandbyAdminStatus);
    return i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfHotStandbyState
 Input       :  The Indices

                The Object
                retValFsMIOspfHotStandbyState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfHotStandbyState (INT4 *pi4RetValFsMIOspfHotStandbyState)
{
    INT1                i4RetVal = SNMP_FAILURE;

    i4RetVal = nmhGetFutOspfHotStandbyState (pi4RetValFsMIOspfHotStandbyState);
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfDynamicBulkUpdStatus
 Input       :  The Indices

                The Object
                retValFsMIOspfDynamicBulkUpdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfDynamicBulkUpdStatus (INT4 *pi4RetValFsMIOspfDynamicBulkUpdStatus)
{
    INT1                i4RetVal = SNMP_FAILURE;

    i4RetVal =
        nmhGetFutOspfDynamicBulkUpdStatus
        (pi4RetValFsMIOspfDynamicBulkUpdStatus);
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfStanbyHelloSyncCount
 Input       :  The Indices

                The Object
                retValFsMIOspfStanbyHelloSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfStanbyHelloSyncCount (UINT4
                                    *pu4RetValFsMIOspfStanbyHelloSyncCount)
{
    INT1                i4RetVal = SNMP_FAILURE;

    i4RetVal =
        nmhGetFutOspfStanbyHelloSyncCount
        (pu4RetValFsMIOspfStanbyHelloSyncCount);
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfStanbyLsaSyncCount
 Input       :  The Indices

                The Object
                retValFsMIOspfStanbyLsaSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfStanbyLsaSyncCount (UINT4 *pu4RetValFsMIOspfStanbyLsaSyncCount)
{
    INT1                i4RetVal = SNMP_FAILURE;

    i4RetVal =
        nmhGetFutOspfStanbyLsaSyncCount (pu4RetValFsMIOspfStanbyLsaSyncCount);
    return i4RetVal;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfAreaTable
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMIOspfAreaTable (INT4 *pi4FsMIOspfAreaContextId,
                                   UINT4 *pu4FsMIOspfAreaId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfAreaContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    if (UtilOspfSetContext (*pi4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i4RetVal;
    }
    i4RetVal = nmhGetFirstIndexFutOspfAreaTable (pu4FsMIOspfAreaId);
    UtilOspfResetContext ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfAreaTable
 Input       :  The Indices
                FsMIOspfAreaContextId
                nextFsMIOspfAreaContextId
                FsMIOspfAreaId
                nextFsMIOspfAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfAreaTable (INT4 i4FsMIOspfAreaContextId,
                                  INT4 *pi4NextFsMIOspfAreaContextId,
                                  UINT4 u4FsMIOspfAreaId,
                                  UINT4 *pu4NextFsMIOspfAreaId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfAreaContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal = nmhGetNextIndexFutOspfAreaTable (u4FsMIOspfAreaId,
                                                    pu4NextFsMIOspfAreaId);
        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfAreaContextId,
                                      (UINT4 *) pi4NextFsMIOspfAreaContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfAreaContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal = nmhGetFirstIndexFutOspfAreaTable (pu4NextFsMIOspfAreaId);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfAreaContextId = *pi4NextFsMIOspfAreaContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfAreaContextId = i4FsMIOspfAreaContextId;
    }
    return i4RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaIfCount
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaIfCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaIfCount (INT4 i4FsMIOspfAreaContextId,
                           UINT4 u4FsMIOspfAreaId,
                           UINT4 *pu4RetValFsMIOspfAreaIfCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaIfCount (u4FsMIOspfAreaId,
                                         pu4RetValFsMIOspfAreaIfCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaNetCount
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaNetCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaNetCount (INT4 i4FsMIOspfAreaContextId,
                            UINT4 u4FsMIOspfAreaId,
                            UINT4 *pu4RetValFsMIOspfAreaNetCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaNetCount (u4FsMIOspfAreaId,
                                          pu4RetValFsMIOspfAreaNetCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaRtrCount
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaRtrCount (INT4 i4FsMIOspfAreaContextId,
                            UINT4 u4FsMIOspfAreaId,
                            UINT4 *pu4RetValFsMIOspfAreaRtrCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaRtrCount (u4FsMIOspfAreaId,
                                          pu4RetValFsMIOspfAreaRtrCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaNSSATranslatorRole
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaNSSATranslatorRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaNSSATranslatorRole (INT4 i4FsMIOspfAreaContextId,
                                      UINT4 u4FsMIOspfAreaId,
                                      INT4
                                      *pi4RetValFsMIOspfAreaNSSATranslatorRole)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaNSSATranslatorRole (u4FsMIOspfAreaId,
                                                    pi4RetValFsMIOspfAreaNSSATranslatorRole);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaNSSATranslatorState
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaNSSATranslatorState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaNSSATranslatorState (INT4 i4FsMIOspfAreaContextId,
                                       UINT4 u4FsMIOspfAreaId,
                                       INT4
                                       *pi4RetValFsMIOspfAreaNSSATranslatorState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaNSSATranslatorState (u4FsMIOspfAreaId,
                                                     pi4RetValFsMIOspfAreaNSSATranslatorState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaNSSATranslatorStabilityInterval
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaNSSATranslatorStabilityInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaNSSATranslatorStabilityInterval (INT4 i4FsMIOspfAreaContextId,
                                                   UINT4 u4FsMIOspfAreaId,
                                                   INT4
                                                   *pi4RetValFsMIOspfAreaNSSATranslatorStabilityInterval)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfAreaNSSATranslatorStabilityInterval (u4FsMIOspfAreaId,
                                                          pi4RetValFsMIOspfAreaNSSATranslatorStabilityInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaNSSATranslatorEvents
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                retValFsMIOspfAreaNSSATranslatorEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaNSSATranslatorEvents (INT4 i4FsMIOspfAreaContextId,
                                        UINT4 u4FsMIOspfAreaId,
                                        UINT4
                                        *pu4RetValFsMIOspfAreaNSSATranslatorEvents)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaNSSATranslatorEvents (u4FsMIOspfAreaId,
                                                      pu4RetValFsMIOspfAreaNSSATranslatorEvents);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaDfInfOriginate
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object
                retValFsMIOspfAreaDfInfOriginate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaDfInfOriginate (INT4 i4FsMIOspfAreaContextId,
                                  UINT4 u4FsMIOspfAreaId,
                                  INT4 *pi4RetValFsMIOspfAreaDfInfOriginate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaDfInfOriginate (u4FsMIOspfAreaId,
                                                pi4RetValFsMIOspfAreaDfInfOriginate);
    UtilOspfResetContext ();
    return i1Return;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfHostTable
 Input       :  The Indices
                FsMIOspfHostContextId
                FsMIOspfHostIpAddress
                FsMIOspfHostTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMIOspfHostTable (INT4 *pi4FsMIOspfHostContextId,
                                   UINT4 *pu4FsMIOspfHostIpAddress,
                                   INT4 *pi4FsMIOspfHostTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfHostContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfHostContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIOspfHostContextId = *pi4FsMIOspfHostContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfHostContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal = nmhGetFirstIndexFutOspfHostTable (pu4FsMIOspfHostIpAddress,
                                                     pi4FsMIOspfHostTOS);
        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfHostContextId,
                                 (UINT4 *) pi4FsMIOspfHostContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfHostTable
 Input       :  The Indices
                FsMIOspfHostContextId
                nextFsMIOspfHostContextId
                FsMIOspfHostIpAddress
                nextFsMIOspfHostIpAddress
                FsMIOspfHostTOS
                nextFsMIOspfHostTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfHostTable (INT4 i4FsMIOspfHostContextId,
                                  INT4 *pi4NextFsMIOspfHostContextId,
                                  UINT4 u4FsMIOspfHostIpAddress,
                                  UINT4 *pu4NextFsMIOspfHostIpAddress,
                                  INT4 i4FsMIOspfHostTOS,
                                  INT4 *pi4NextFsMIOspfHostTOS)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfHostContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfHostContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal = nmhGetNextIndexFutOspfHostTable (u4FsMIOspfHostIpAddress,
                                                    pu4NextFsMIOspfHostIpAddress,
                                                    i4FsMIOspfHostTOS,
                                                    pi4NextFsMIOspfHostTOS);
        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfHostContextId,
                                      (UINT4 *) pi4NextFsMIOspfHostContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfHostContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i4RetVal =
                nmhGetFirstIndexFutOspfHostTable (pu4NextFsMIOspfHostIpAddress,
                                                  pi4NextFsMIOspfHostTOS);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfHostContextId = *pi4NextFsMIOspfHostContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfHostContextId = i4FsMIOspfHostContextId;
    }
    return i4RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfHostRouteIfIndex
 Input       :  The Indices
                FsMIOspfHostContextId
                FsMIOspfHostIpAddress
                FsMIOspfHostTOS

                The Object 
                retValFsMIOspfHostRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfHostRouteIfIndex (INT4 i4FsMIOspfHostContextId,
                                UINT4 u4FsMIOspfHostIpAddress,
                                INT4 i4FsMIOspfHostTOS,
                                INT4 *pi4RetValFsMIOspfHostRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfHostRouteIfIndex (u4FsMIOspfHostIpAddress,
                                              i4FsMIOspfHostTOS,
                                              pi4RetValFsMIOspfHostRouteIfIndex);
    UtilOspfResetContext ();
    return i1Return;
}

/* GET_FIRST Routine. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfIfTable
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMIOspfIfTable (INT4 *pi4FsMIOspfIfContextId,
                                 UINT4 *pu4FsMIOspfIfIpAddress,
                                 INT4 *pi4FsMIOspfAddressLessIf)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfIfContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfIfContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        i4FsMIOspfIfContextId = *pi4FsMIOspfIfContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfIfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal = nmhGetFirstIndexFutOspfIfTable (pu4FsMIOspfIfIpAddress,
                                                   pi4FsMIOspfAddressLessIf);
        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfContextId,
                                 (UINT4 *) pi4FsMIOspfIfContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfIfTable
 Input       :  The Indices
                FsMIOspfIfContextId
                nextFsMIOspfIfContextId
                FsMIOspfIfIpAddress
                nextFsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
                nextFsMIOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfIfTable (INT4 i4FsMIOspfIfContextId,
                                INT4 *pi4NextFsMIOspfIfContextId,
                                UINT4 u4FsMIOspfIfIpAddress,
                                UINT4 *pu4NextFsMIOspfIfIpAddress,
                                INT4 i4FsMIOspfAddressLessIf,
                                INT4 *pi4NextFsMIOspfAddressLessIf)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfIfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal = nmhGetNextIndexFutOspfIfTable (u4FsMIOspfIfIpAddress,
                                                  pu4NextFsMIOspfIfIpAddress,
                                                  i4FsMIOspfAddressLessIf,
                                                  pi4NextFsMIOspfAddressLessIf);
        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfContextId,
                                      (UINT4 *) pi4NextFsMIOspfIfContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfIfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal =
                nmhGetFirstIndexFutOspfIfTable (pu4NextFsMIOspfIfIpAddress,
                                                pi4NextFsMIOspfAddressLessIf);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfIfContextId = *pi4NextFsMIOspfIfContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfIfContextId = i4FsMIOspfIfContextId;
    }
    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfOperState
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfOperState (INT4 i4FsMIOspfIfContextId,
                           UINT4 u4FsMIOspfIfIpAddress,
                           INT4 i4FsMIOspfAddressLessIf,
                           INT4 *pi4RetValFsMIOspfIfOperState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfOperState (u4FsMIOspfIfIpAddress,
                                         i4FsMIOspfAddressLessIf,
                                         pi4RetValFsMIOspfIfOperState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfPassive
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfPassive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfPassive (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         INT4 *pi4RetValFsMIOspfIfPassive)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfPassive (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pi4RetValFsMIOspfIfPassive);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfNbrCount
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfNbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfNbrCount (INT4 i4FsMIOspfIfContextId,
                          UINT4 u4FsMIOspfIfIpAddress,
                          INT4 i4FsMIOspfAddressLessIf,
                          UINT4 *pu4RetValFsMIOspfIfNbrCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfNbrCount (u4FsMIOspfIfIpAddress,
                                        i4FsMIOspfAddressLessIf,
                                        pu4RetValFsMIOspfIfNbrCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAdjCount
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfAdjCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAdjCount (INT4 i4FsMIOspfIfContextId,
                          UINT4 u4FsMIOspfIfIpAddress,
                          INT4 i4FsMIOspfAddressLessIf,
                          UINT4 *pu4RetValFsMIOspfIfAdjCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfAdjCount (u4FsMIOspfIfIpAddress,
                                        i4FsMIOspfAddressLessIf,
                                        pu4RetValFsMIOspfIfAdjCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfHelloRcvd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfHelloRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfHelloRcvd (INT4 i4FsMIOspfIfContextId,
                           UINT4 u4FsMIOspfIfIpAddress,
                           INT4 i4FsMIOspfAddressLessIf,
                           UINT4 *pu4RetValFsMIOspfIfHelloRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfHelloRcvd (u4FsMIOspfIfIpAddress,
                                         i4FsMIOspfAddressLessIf,
                                         pu4RetValFsMIOspfIfHelloRcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfHelloTxed
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfHelloTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfHelloTxed (INT4 i4FsMIOspfIfContextId,
                           UINT4 u4FsMIOspfIfIpAddress,
                           INT4 i4FsMIOspfAddressLessIf,
                           UINT4 *pu4RetValFsMIOspfIfHelloTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfHelloTxed (u4FsMIOspfIfIpAddress,
                                         i4FsMIOspfAddressLessIf,
                                         pu4RetValFsMIOspfIfHelloTxed);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfHelloDisd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfHelloDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfHelloDisd (INT4 i4FsMIOspfIfContextId,
                           UINT4 u4FsMIOspfIfIpAddress,
                           INT4 i4FsMIOspfAddressLessIf,
                           UINT4 *pu4RetValFsMIOspfIfHelloDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfHelloDisd (u4FsMIOspfIfIpAddress,
                                         i4FsMIOspfAddressLessIf,
                                         pu4RetValFsMIOspfIfHelloDisd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfDdpRcvd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfDdpRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfDdpRcvd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfDdpRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfDdpRcvd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfDdpRcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfDdpTxed
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfDdpTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfDdpTxed (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfDdpTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfDdpTxed (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfDdpTxed);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfDdpDisd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfDdpDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfDdpDisd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfDdpDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfDdpDisd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfDdpDisd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLrqRcvd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLrqRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLrqRcvd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLrqRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLrqRcvd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLrqRcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLrqTxed
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLrqTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLrqTxed (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLrqTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLrqTxed (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLrqTxed);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLrqDisd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLrqDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLrqDisd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLrqDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLrqDisd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLrqDisd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLsuRcvd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLsuRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLsuRcvd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLsuRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLsuRcvd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLsuRcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLsuTxed
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLsuTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLsuTxed (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLsuTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLsuTxed (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLsuTxed);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLsuDisd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLsuDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLsuDisd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLsuDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLsuDisd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLsuDisd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLakRcvd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLakRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLakRcvd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLakRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLakRcvd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLakRcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLakTxed
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLakTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLakTxed (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLakTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLakTxed (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLakTxed);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfLakDisd
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfIfLakDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfLakDisd (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         UINT4 *pu4RetValFsMIOspfIfLakDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfLakDisd (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       pu4RetValFsMIOspfIfLakDisd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 *  Function    :  nmhGetFsMIOspfIfBfdState
 *  Input       :  The Indices
 *                 FsMIStdOspfContextId
 *                 FsMIOspfIfIpAddress
 *                 FsMIOspfAddressLessIf
 *                 The Object
 *                 retValFsMIOspfIfBfdState
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIOspfIfBfdState (INT4 i4FsMIStdOspfContextId,
                          UINT4 u4FsMIOspfIfIpAddress,
                          INT4 i4FsMIOspfAddressLessIf,
                          INT4 *pi4RetValFsMIOspfIfBfdState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfBfdState (u4FsMIOspfIfIpAddress,
                                        i4FsMIOspfAddressLessIf,
                                        pi4RetValFsMIOspfIfBfdState);
    UtilOspfResetContext ();
    return i1Return;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfIfMD5AuthTable
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMIOspfIfMD5AuthTable (INT4 *pi4FsMIOspfIfMD5AuthContextId,
                                        UINT4 *pu4FsMIOspfIfMD5AuthIpAddress,
                                        INT4 *pi4FsMIOspfIfMD5AuthAddressLessIf,
                                        INT4 *pi4FsMIOspfIfMD5AuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfIfMD5AuthContextId;
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfIfMD5AuthContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIOspfIfMD5AuthContextId = *pi4FsMIOspfIfMD5AuthContextId;
        if (UtilOspfSetContext (*pi4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfIfMD5AuthTable
            (pu4FsMIOspfIfMD5AuthIpAddress, pi4FsMIOspfIfMD5AuthAddressLessIf,
             pi4FsMIOspfIfMD5AuthKeyId);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfMD5AuthContextId,
                                 (UINT4 *) pi4FsMIOspfIfMD5AuthContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetFirstIndexFsMIOspfIfAuthTable
 *  Input       :  The Indices
 *                 FsMIStdOspfContextId
 *                 FsMIOspfIfAuthIpAddress
 *                 FsMIOspfIfAuthAddressLessIf
 *                 FsMIOspfIfAuthKeyId
 *  Output      :  The Get First Routines gets the Lexicographicaly
 *                 First Entry from the Table.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfIfAuthTable (INT4 *pi4FsMIStdOspfContextId,
                                     UINT4 *pu4FsMIOspfIfAuthIpAddress,
                                     INT4 *pi4FsMIOspfIfAuthAddressLessIf,
                                     INT4 *pi4FsMIOspfIfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfIfAuthContextId;
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIOspfIfAuthContextId = *pi4FsMIStdOspfContextId;
        if (UtilOspfSetContext ((UINT4) *pi4FsMIStdOspfContextId) ==
            SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfIfAuthTable
            (pu4FsMIOspfIfAuthIpAddress, pi4FsMIOspfIfAuthAddressLessIf,
             pi4FsMIOspfIfAuthKeyId);

        UtilOspfResetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfAuthContextId,
                                 (UINT4 *) pi4FsMIStdOspfContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfIfMD5AuthTable
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                nextFsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                nextFsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                nextFsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId
                nextFsMIOspfIfMD5AuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfIfMD5AuthTable (INT4 i4FsMIOspfIfMD5AuthContextId,
                                       INT4 *pi4NextFsMIOspfIfMD5AuthContextId,
                                       UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                       UINT4 *pu4NextFsMIOspfIfMD5AuthIpAddress,
                                       INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                       INT4
                                       *pi4NextFsMIOspfIfMD5AuthAddressLessIf,
                                       INT4 i4FsMIOspfIfMD5AuthKeyId,
                                       INT4 *pi4NextFsMIOspfIfMD5AuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    if (UtilOspfIsValidCxtId (i4FsMIOspfIfMD5AuthContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfIfMD5AuthTable (u4FsMIOspfIfMD5AuthIpAddress,
                                                  pu4NextFsMIOspfIfMD5AuthIpAddress,
                                                  i4FsMIOspfIfMD5AuthAddressLessIf,
                                                  pi4NextFsMIOspfIfMD5AuthAddressLessIf,
                                                  i4FsMIOspfIfMD5AuthKeyId,
                                                  pi4NextFsMIOspfIfMD5AuthKeyId);
        UtilOspfResetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfMD5AuthContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfIfMD5AuthContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfIfMD5AuthContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal =
                nmhGetFirstIndexFutOspfIfMD5AuthTable
                (pu4NextFsMIOspfIfMD5AuthIpAddress,
                 pi4NextFsMIOspfIfMD5AuthAddressLessIf,
                 pi4NextFsMIOspfIfMD5AuthKeyId);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfIfMD5AuthContextId = *pi4NextFsMIOspfIfMD5AuthContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfIfMD5AuthContextId = i4FsMIOspfIfMD5AuthContextId;
    }
    return i4RetVal;

}

INT1
nmhGetNextIndexFsMIOspfIfAuthTable (INT4 i4FsMIStdOspfContextId,
                                    INT4 *pi4NextFsMIStdOspfContextId,
                                    UINT4 u4FsMIOspfIfAuthIpAddress,
                                    UINT4 *pu4NextFsMIOspfIfAuthIpAddress,
                                    INT4 i4FsMIOspfIfAuthAddressLessIf,
                                    INT4 *pi4NextFsMIOspfIfAuthAddressLessIf,
                                    INT4 i4FsMIOspfIfAuthKeyId,
                                    INT4 *pi4NextFsMIOspfIfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    if (UtilOspfIsValidCxtId ((UINT4) i4FsMIStdOspfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfIfAuthTable (u4FsMIOspfIfAuthIpAddress,
                                               pu4NextFsMIOspfIfAuthIpAddress,
                                               i4FsMIOspfIfAuthAddressLessIf,
                                               pi4NextFsMIOspfIfAuthAddressLessIf,
                                               i4FsMIOspfIfAuthKeyId,
                                               pi4NextFsMIOspfIfAuthKeyId);
        UtilOspfResetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext ((UINT4) *pi4NextFsMIStdOspfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal =
                nmhGetFirstIndexFutOspfIfAuthTable
                (pu4NextFsMIOspfIfAuthIpAddress,
                 pi4NextFsMIOspfIfAuthAddressLessIf,
                 pi4NextFsMIOspfIfAuthKeyId);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfContextId = *pi4NextFsMIStdOspfContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfContextId = i4FsMIStdOspfContextId;
    }
    return i4RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfMD5AuthKey
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                retValFsMIOspfIfMD5AuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfMD5AuthKey (INT4 i4FsMIOspfIfMD5AuthContextId,
                            UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                            INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                            INT4 i4FsMIOspfIfMD5AuthKeyId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIOspfIfMD5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfMD5AuthKey (u4FsMIOspfIfMD5AuthIpAddress,
                                          i4FsMIOspfIfMD5AuthAddressLessIf,
                                          i4FsMIOspfIfMD5AuthKeyId,
                                          pRetValFsMIOspfIfMD5AuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAuthKey
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                retValFsMIOspfIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAuthKey (INT4 i4FsMIStdOspfContextId,
                         UINT4 u4FsMIOspfIfAuthIpAddress,
                         INT4 i4FsMIOspfIfAuthAddressLessIf,
                         INT4 i4FsMIOspfIfAuthKeyId,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsMIOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfAuthKey (u4FsMIOspfIfAuthIpAddress,
                                       i4FsMIOspfIfAuthAddressLessIf,
                                       i4FsMIOspfIfAuthKeyId,
                                       pRetValFsMIOspfIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                retValFsMIOspfIfMD5AuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfMD5AuthKeyStartAccept (INT4 i4FsMIOspfIfMD5AuthContextId,
                                       UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                       INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                       INT4 i4FsMIOspfIfMD5AuthKeyId,
                                       INT4
                                       *pi4RetValFsMIOspfIfMD5AuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfIfMD5AuthKeyStartAccept (u4FsMIOspfIfMD5AuthIpAddress,
                                              i4FsMIOspfIfMD5AuthAddressLessIf,
                                              i4FsMIOspfIfMD5AuthKeyId,
                                              pi4RetValFsMIOspfIfMD5AuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                retValFsMIOspfIfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAuthKeyStartAccept (INT4 i4FsMIStdOspfContextId,
                                    UINT4 u4FsMIOspfIfAuthIpAddress,
                                    INT4 i4FsMIOspfIfAuthAddressLessIf,
                                    INT4 i4FsMIOspfIfAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMIOspfIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfIfAuthKeyStartAccept (u4FsMIOspfIfAuthIpAddress,
                                           i4FsMIOspfIfAuthAddressLessIf,
                                           i4FsMIOspfIfAuthKeyId,
                                           pRetValFsMIOspfIfAuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                retValFsMIOspfIfMD5AuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfMD5AuthKeyStartGenerate (INT4 i4FsMIOspfIfMD5AuthContextId,
                                         UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                         INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                         INT4 i4FsMIOspfIfMD5AuthKeyId,
                                         INT4
                                         *pi4RetValFsMIOspfIfMD5AuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfIfMD5AuthKeyStartGenerate (u4FsMIOspfIfMD5AuthIpAddress,
                                                i4FsMIOspfIfMD5AuthAddressLessIf,
                                                i4FsMIOspfIfMD5AuthKeyId,
                                                pi4RetValFsMIOspfIfMD5AuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                retValFsMIOspfIfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAuthKeyStartGenerate (INT4 i4FsMIStdOspfContextId,
                                      UINT4 u4FsMIOspfIfAuthIpAddress,
                                      INT4 i4FsMIOspfIfAuthAddressLessIf,
                                      INT4 i4FsMIOspfIfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIOspfIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfAuthKeyStartGenerate (u4FsMIOspfIfAuthIpAddress,
                                                    i4FsMIOspfIfAuthAddressLessIf,
                                                    i4FsMIOspfIfAuthKeyId,
                                                    pRetValFsMIOspfIfAuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                retValFsMIOspfIfMD5AuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfMD5AuthKeyStopGenerate (INT4 i4FsMIOspfIfMD5AuthContextId,
                                        UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                        INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                        INT4 i4FsMIOspfIfMD5AuthKeyId,
                                        INT4
                                        *pi4RetValFsMIOspfIfMD5AuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfIfMD5AuthKeyStopGenerate (u4FsMIOspfIfMD5AuthIpAddress,
                                               i4FsMIOspfIfMD5AuthAddressLessIf,
                                               i4FsMIOspfIfMD5AuthKeyId,
                                               pi4RetValFsMIOspfIfMD5AuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                retValFsMIOspfIfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAuthKeyStopGenerate (INT4 i4FsMIStdOspfContextId,
                                     UINT4 u4FsMIOspfIfAuthIpAddress,
                                     INT4 i4FsMIOspfIfAuthAddressLessIf,
                                     INT4 i4FsMIOspfIfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIOspfIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhGetFutOspfIfAuthKeyStopGenerate (u4FsMIOspfIfAuthIpAddress,
                                            i4FsMIOspfIfAuthAddressLessIf,
                                            i4FsMIOspfIfAuthKeyId,
                                            pRetValFsMIOspfIfAuthKeyStopGenerate);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                retValFsMIOspfIfMD5AuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfMD5AuthKeyStopAccept (INT4 i4FsMIOspfIfMD5AuthContextId,
                                      UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                      INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                      INT4 i4FsMIOspfIfMD5AuthKeyId,
                                      INT4
                                      *pi4RetValFsMIOspfIfMD5AuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfIfMD5AuthKeyStopAccept (u4FsMIOspfIfMD5AuthIpAddress,
                                             i4FsMIOspfIfMD5AuthAddressLessIf,
                                             i4FsMIOspfIfMD5AuthKeyId,
                                             pi4RetValFsMIOspfIfMD5AuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                retValFsMIOspfIfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAuthKeyStopAccept (INT4 i4FsMIStdOspfContextId,
                                   UINT4 u4FsMIOspfIfAuthIpAddress,
                                   INT4 i4FsMIOspfIfAuthAddressLessIf,
                                   INT4 i4FsMIOspfIfAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIOspfIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhGetFutOspfIfAuthKeyStopAccept (u4FsMIOspfIfAuthIpAddress,
                                          i4FsMIOspfIfAuthAddressLessIf,
                                          i4FsMIOspfIfAuthKeyId,
                                          pRetValFsMIOspfIfAuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfMD5AuthKeyStatus
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                retValFsMIOspfIfMD5AuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfMD5AuthKeyStatus (INT4 i4FsMIOspfIfMD5AuthContextId,
                                  UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                  INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                  INT4 i4FsMIOspfIfMD5AuthKeyId,
                                  INT4 *pi4RetValFsMIOspfIfMD5AuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfMD5AuthKeyStatus (u4FsMIOspfIfMD5AuthIpAddress,
                                                i4FsMIOspfIfMD5AuthAddressLessIf,
                                                i4FsMIOspfIfMD5AuthKeyId,
                                                pi4RetValFsMIOspfIfMD5AuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                retValFsMIOspfIfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfIfAuthKeyStatus (INT4 i4FsMIStdOspfContextId,
                               UINT4 u4FsMIOspfIfAuthIpAddress,
                               INT4 i4FsMIOspfIfAuthAddressLessIf,
                               INT4 i4FsMIOspfIfAuthKeyId,
                               INT4 *pi4RetValFsMIOspfIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfIfAuthKeyStatus (u4FsMIOspfIfAuthIpAddress,
                                             i4FsMIOspfIfAuthAddressLessIf,
                                             i4FsMIOspfIfAuthKeyId,
                                             pi4RetValFsMIOspfIfAuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfVirtIfMD5AuthTable
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfVirtIfMD5AuthTable (INT4
                                            *pi4FsMIOspfVirtIfMD5AuthContextId,
                                            UINT4
                                            *pu4FsMIOspfVirtIfMD5AuthAreaId,
                                            UINT4
                                            *pu4FsMIOspfVirtIfMD5AuthNeighbor,
                                            INT4 *pi4FsMIOspfVirtIfMD5AuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfVirtIfMD5AuthContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        if (UtilOspfSetContext (*pi4FsMIOspfVirtIfMD5AuthContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfVirtIfMD5AuthTable
            (pu4FsMIOspfVirtIfMD5AuthAreaId, pu4FsMIOspfVirtIfMD5AuthNeighbor,
             pi4FsMIOspfVirtIfMD5AuthKeyId);
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        UtilOspfResetContext ();
    }
    while (UtilOspfGetNextCxtId ((UINT4) *pi4FsMIOspfVirtIfMD5AuthContextId,
                                 (UINT4 *) pi4FsMIOspfVirtIfMD5AuthContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfVirtIfAuthTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfVirtIfAuthTable (INT4 *pi4FsMIStdOspfContextId,
                                         UINT4 *pu4FsMIOspfVirtIfAuthAreaId,
                                         UINT4 *pu4FsMIOspfVirtIfAuthNeighbor,
                                         INT4 *pi4FsMIOspfVirtIfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        if (UtilOspfSetContext ((UINT4) *pi4FsMIStdOspfContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfVirtIfAuthTable
            (pu4FsMIOspfVirtIfAuthAreaId, pu4FsMIOspfVirtIfAuthNeighbor,
             pi4FsMIOspfVirtIfAuthKeyId);
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        UtilOspfResetContext ();
    }
    while (UtilOspfGetNextCxtId ((UINT4) *pi4FsMIStdOspfContextId,
                                 (UINT4 *) pi4FsMIStdOspfContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfVirtIfMD5AuthTable
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                nextFsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                nextFsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                nextFsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId
                nextFsMIOspfVirtIfMD5AuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfVirtIfMD5AuthTable (INT4
                                           i4FsMIOspfVirtIfMD5AuthContextId,
                                           INT4
                                           *pi4NextFsMIOspfVirtIfMD5AuthContextId,
                                           UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           *pu4NextFsMIOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           u4FsMIOspfVirtIfMD5AuthNeighbor,
                                           UINT4
                                           *pu4NextFsMIOspfVirtIfMD5AuthNeighbor,
                                           INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                           INT4
                                           *pi4NextFsMIOspfVirtIfMD5AuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfVirtIfMD5AuthContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) ==
            SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfVirtIfMD5AuthTable
            (u4FsMIOspfVirtIfMD5AuthAreaId, pu4NextFsMIOspfVirtIfMD5AuthAreaId,
             u4FsMIOspfVirtIfMD5AuthNeighbor,
             pu4NextFsMIOspfVirtIfMD5AuthNeighbor, i4FsMIOspfVirtIfMD5AuthKeyId,
             pi4NextFsMIOspfVirtIfMD5AuthKeyId);
        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfVirtIfMD5AuthContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfVirtIfMD5AuthContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfVirtIfMD5AuthContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal =
                nmhGetFirstIndexFutOspfVirtIfMD5AuthTable
                (pu4NextFsMIOspfVirtIfMD5AuthAreaId,
                 pu4NextFsMIOspfVirtIfMD5AuthNeighbor,
                 pi4NextFsMIOspfVirtIfMD5AuthKeyId);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfVirtIfMD5AuthContextId =
                *pi4NextFsMIOspfVirtIfMD5AuthContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfVirtIfMD5AuthContextId
            = i4FsMIOspfVirtIfMD5AuthContextId;
    }
    return i4RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfVirtIfAuthTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                nextFsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                nextFsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId
                nextFsMIOspfVirtIfAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfVirtIfAuthTable (INT4 i4FsMIStdOspfContextId,
                                        INT4 *pi4NextFsMIStdOspfContextId,
                                        UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                        UINT4 *pu4NextFsMIOspfVirtIfAuthAreaId,
                                        UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                        UINT4
                                        *pu4NextFsMIOspfVirtIfAuthNeighbor,
                                        INT4 i4FsMIOspfVirtIfAuthKeyId,
                                        INT4 *pi4NextFsMIOspfVirtIfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId ((UINT4) i4FsMIStdOspfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfVirtIfAuthTable
            (u4FsMIOspfVirtIfAuthAreaId, pu4NextFsMIOspfVirtIfAuthAreaId,
             u4FsMIOspfVirtIfAuthNeighbor,
             pu4NextFsMIOspfVirtIfAuthNeighbor, i4FsMIOspfVirtIfAuthKeyId,
             pi4NextFsMIOspfVirtIfAuthKeyId);
        UtilOspfResetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext ((UINT4) *pi4NextFsMIStdOspfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal =
                nmhGetFirstIndexFutOspfVirtIfAuthTable
                (pu4NextFsMIOspfVirtIfAuthAreaId,
                 pu4NextFsMIOspfVirtIfAuthNeighbor,
                 pi4NextFsMIOspfVirtIfAuthKeyId);
            UtilOspfResetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfContextId = *pi4NextFsMIStdOspfContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfContextId = i4FsMIStdOspfContextId;
    }
    return i4RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfMD5AuthKey
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                retValFsMIOspfVirtIfMD5AuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfMD5AuthKey (INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIOspfVirtIfMD5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfVirtIfMD5AuthKey (u4FsMIOspfVirtIfMD5AuthAreaId,
                                              u4FsMIOspfVirtIfMD5AuthNeighbor,
                                              i4FsMIOspfVirtIfMD5AuthKeyId,
                                              pRetValFsMIOspfVirtIfMD5AuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                retValFsMIOspfVirtIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfAuthKey (INT4 i4FsMIStdOspfContextId,
                             UINT4 u4FsMIOspfVirtIfAuthAreaId,
                             UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                             INT4 i4FsMIOspfVirtIfAuthKeyId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfVirtIfAuthKey (u4FsMIOspfVirtIfAuthAreaId,
                                           u4FsMIOspfVirtIfAuthNeighbor,
                                           i4FsMIOspfVirtIfAuthKeyId,
                                           pRetValFsMIOspfVirtIfAuthKey);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                retValFsMIOspfVirtIfMD5AuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStartAccept (INT4
                                           i4FsMIOspfVirtIfMD5AuthContextId,
                                           UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           u4FsMIOspfVirtIfMD5AuthNeighbor,
                                           INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                           INT4
                                           *pi4RetValFsMIOspfVirtIfMD5AuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfMD5AuthKeyStartAccept (u4FsMIOspfVirtIfMD5AuthAreaId,
                                                  u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                  i4FsMIOspfVirtIfMD5AuthKeyId,
                                                  pi4RetValFsMIOspfVirtIfMD5AuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                retValFsMIOspfVirtIfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfAuthKeyStartAccept (INT4 i4FsMIStdOspfContextId,
                                        UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                        UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                        INT4 i4FsMIOspfVirtIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValFsMIOspfVirtIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfAuthKeyStartAccept (u4FsMIOspfVirtIfAuthAreaId,
                                               u4FsMIOspfVirtIfAuthNeighbor,
                                               i4FsMIOspfVirtIfAuthKeyId,
                                               pRetValFsMIOspfVirtIfAuthKeyStartAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                retValFsMIOspfVirtIfMD5AuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStartGenerate (INT4
                                             i4FsMIOspfVirtIfMD5AuthContextId,
                                             UINT4
                                             u4FsMIOspfVirtIfMD5AuthAreaId,
                                             UINT4
                                             u4FsMIOspfVirtIfMD5AuthNeighbor,
                                             INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                             INT4
                                             *pi4RetValFsMIOspfVirtIfMD5AuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfMD5AuthKeyStartGenerate
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId,
         pi4RetValFsMIOspfVirtIfMD5AuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                retValFsMIOspfVirtIfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfAuthKeyStartGenerate (INT4 i4FsMIStdOspfContextId,
                                          UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                          UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                          INT4 i4FsMIOspfVirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pRetValFsMIOspfVirtIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfAuthKeyStartGenerate
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId, pRetValFsMIOspfVirtIfAuthKeyStartGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                retValFsMIOspfVirtIfMD5AuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStopGenerate (INT4
                                            i4FsMIOspfVirtIfMD5AuthContextId,
                                            UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                            UINT4
                                            u4FsMIOspfVirtIfMD5AuthNeighbor,
                                            INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                            INT4
                                            *pi4RetValFsMIOspfVirtIfMD5AuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfMD5AuthKeyStopGenerate
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId,
         pi4RetValFsMIOspfVirtIfMD5AuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                retValFsMIOspfVirtIfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfAuthKeyStopGenerate (INT4 i4FsMIStdOspfContextId,
                                         UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                         UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                         INT4 i4FsMIOspfVirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pRetValFsMIOspfVirtIfAuthKeyStopGenerate)
{

    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfAuthKeyStopGenerate
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId, pRetValFsMIOspfVirtIfAuthKeyStopGenerate);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                retValFsMIOspfVirtIfMD5AuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStopAccept (INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                          UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                          UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                          INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                          INT4
                                          *pi4RetValFsMIOspfVirtIfMD5AuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfMD5AuthKeyStopAccept (u4FsMIOspfVirtIfMD5AuthAreaId,
                                                 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                                 i4FsMIOspfVirtIfMD5AuthKeyId,
                                                 pi4RetValFsMIOspfVirtIfMD5AuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                retValFsMIOspfVirtIfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfAuthKeyStopAccept (INT4 i4FsMIStdOspfContextId,
                                       UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                       UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                       INT4 i4FsMIOspfVirtIfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValFsMIOspfVirtIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfAuthKeyStopAccept (u4FsMIOspfVirtIfAuthAreaId,
                                              u4FsMIOspfVirtIfAuthNeighbor,
                                              i4FsMIOspfVirtIfAuthKeyId,
                                              pRetValFsMIOspfVirtIfAuthKeyStopAccept);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfMD5AuthKeyStatus
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                retValFsMIOspfVirtIfMD5AuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfMD5AuthKeyStatus (INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                      UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                      UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                      INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                      INT4
                                      *pi4RetValFsMIOspfVirtIfMD5AuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfMD5AuthKeyStatus (u4FsMIOspfVirtIfMD5AuthAreaId,
                                             u4FsMIOspfVirtIfMD5AuthNeighbor,
                                             i4FsMIOspfVirtIfMD5AuthKeyId,
                                             pi4RetValFsMIOspfVirtIfMD5AuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                retValFsMIOspfVirtIfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfVirtIfAuthKeyStatus (INT4 i4FsMIStdOspfContextId,
                                   UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                   UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                   INT4 i4FsMIOspfVirtIfAuthKeyId,
                                   INT4 *pi4RetValFsMIOspfVirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfVirtIfAuthKeyStatus (u4FsMIOspfVirtIfAuthAreaId,
                                          u4FsMIOspfVirtIfAuthNeighbor,
                                          i4FsMIOspfVirtIfAuthKeyId,
                                          pi4RetValFsMIOspfVirtIfAuthKeyStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfNbrTable
 Input       :  The Indices
                FsMIOspfNbrContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfNbrTable (INT4 *pi4FsMIOspfNbrContextId,
                                  UINT4 *pu4FsMIOspfNbrIpAddr,
                                  INT4 *pi4FsMIOspfNbrAddressLessIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfNbrContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfNbrContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfNbrContextId = *pi4FsMIOspfNbrContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfNbrContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetFirstIndexFutOspfNbrTable (pu4FsMIOspfNbrIpAddr,
                                                    pi4FsMIOspfNbrAddressLessIndex);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfNbrContextId,
                                 (UINT4 *) pi4FsMIOspfNbrContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfNbrTable
 Input       :  The Indices
                FsMIOspfNbrContextId
                nextFsMIOspfNbrContextId
                FsMIOspfNbrIpAddr
                nextFsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex
                nextFsMIOspfNbrAddressLessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfNbrTable (INT4 i4FsMIOspfNbrContextId,
                                 INT4 *pi4NextFsMIOspfNbrContextId,
                                 UINT4 u4FsMIOspfNbrIpAddr,
                                 UINT4 *pu4NextFsMIOspfNbrIpAddr,
                                 INT4 i4FsMIOspfNbrAddressLessIndex,
                                 INT4 *pi4NextFsMIOspfNbrAddressLessIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (UtilOspfIsValidCxtId (i4FsMIOspfNbrContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfNbrContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetNextIndexFutOspfNbrTable (u4FsMIOspfNbrIpAddr,
                                                   pu4NextFsMIOspfNbrIpAddr,
                                                   i4FsMIOspfNbrAddressLessIndex,
                                                   pi4NextFsMIOspfNbrAddressLessIndex);
        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfNbrContextId,
                                      (UINT4 *) pi4NextFsMIOspfNbrContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfNbrContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfNbrTable (pu4NextFsMIOspfNbrIpAddr,
                                                 pi4NextFsMIOspfNbrAddressLessIndex);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfNbrContextId = *pi4NextFsMIOspfNbrContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfNbrContextId = i4FsMIOspfNbrContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfNbrDBSummaryQLen
 Input       :  The Indices
                FsMIOspfNbrContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex

                The Object 
                retValFsMIOspfNbrDBSummaryQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfNbrDBSummaryQLen (INT4 i4FsMIOspfNbrContextId,
                                UINT4 u4FsMIOspfNbrIpAddr,
                                INT4 i4FsMIOspfNbrAddressLessIndex,
                                UINT4 *pu4RetValFsMIOspfNbrDBSummaryQLen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfNbrDBSummaryQLen (u4FsMIOspfNbrIpAddr,
                                              i4FsMIOspfNbrAddressLessIndex,
                                              pu4RetValFsMIOspfNbrDBSummaryQLen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfNbrLSReqQLen
 Input       :  The Indices
                FsMIOspfNbrContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex

                The Object 
                retValFsMIOspfNbrLSReqQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfNbrLSReqQLen (INT4 i4FsMIOspfNbrContextId,
                            UINT4 u4FsMIOspfNbrIpAddr,
                            INT4 i4FsMIOspfNbrAddressLessIndex,
                            UINT4 *pu4RetValFsMIOspfNbrLSReqQLen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfNbrLSReqQLen (u4FsMIOspfNbrIpAddr,
                                          i4FsMIOspfNbrAddressLessIndex,
                                          pu4RetValFsMIOspfNbrLSReqQLen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfNbrRestartHelperStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex

                The Object
                retValFsMIOspfNbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIOspfNbrRestartHelperStatus
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIOspfNbrIpAddr,
     INT4 i4FsMIOspfNbrAddressLessIndex,
     INT4 *pi4RetValFsMIOspfNbrRestartHelperStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfNbrRestartHelperStatus
        (u4FsMIOspfNbrIpAddr,
         i4FsMIOspfNbrAddressLessIndex,
         pi4RetValFsMIOspfNbrRestartHelperStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfNbrRestartHelperAge
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex

                The Object
                retValFsMIOspfNbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1                nmhGetFsMIOspfNbrRestartHelperAge
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIOspfNbrIpAddr,
     INT4 i4FsMIOspfNbrAddressLessIndex,
     UINT4 *pu4RetValFsMIOspfNbrRestartHelperAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfNbrRestartHelperAge
        (u4FsMIOspfNbrIpAddr,
         i4FsMIOspfNbrAddressLessIndex, pu4RetValFsMIOspfNbrRestartHelperAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfNbrRestartHelperExitReason
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfNbrIpAddr
                FsMIOspfNbrAddressLessIndex

                The Object
                retValFsMIOspfNbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIOspfNbrRestartHelperExitReason
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIOspfNbrIpAddr,
     INT4 i4FsMIOspfNbrAddressLessIndex,
     INT4 *pi4RetValFsMIOspfNbrRestartHelperExitReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfNbrRestartHelperExitReason
        (u4FsMIOspfNbrIpAddr,
         i4FsMIOspfNbrAddressLessIndex,
         pi4RetValFsMIOspfNbrRestartHelperExitReason);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 *  Function    :  nmhGetFsMIOspfNbrBfdState
 *  Input       :  The Indices
 *                 FsMIStdOspfContextId
 *                 FsMIOspfNbrIpAddr
 *                 FsMIOspfNbrAddressLessIndex
 *                 The Object
 *                 retValFsMIOspfNbrBfdState
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIOspfNbrBfdState (INT4 i4FsMIStdOspfContextId,
                           UINT4 u4FsMIOspfNbrIpAddr,
                           INT4 i4FsMIOspfNbrAddressLessIndex,
                           INT4 *pi4RetValFsMIOspfNbrBfdState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfNbrBfdState (u4FsMIOspfNbrIpAddr,
                                         i4FsMIOspfNbrAddressLessIndex,
                                         pi4RetValFsMIOspfNbrBfdState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfRoutingTable
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfRoutingTable (INT4 *pi4FsMIOspfRouteContextId,
                                      UINT4 *pu4FsMIOspfRouteIpAddr,
                                      UINT4 *pu4FsMIOspfRouteIpAddrMask,
                                      INT4 *pi4FsMIOspfRouteIpTos,
                                      UINT4 *pu4FsMIOspfRouteIpNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfRouteContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfRouteContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfRouteContextId = *pi4FsMIOspfRouteContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfRouteContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFutOspfRoutingTable (pu4FsMIOspfRouteIpAddr,
                                                        pu4FsMIOspfRouteIpAddrMask,
                                                        pi4FsMIOspfRouteIpTos,
                                                        pu4FsMIOspfRouteIpNextHop);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfRouteContextId,
                                 (UINT4 *) pi4FsMIOspfRouteContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfRoutingTable
 Input       :  The Indices
                FsMIOspfRouteContextId
                nextFsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                nextFsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                nextFsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                nextFsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop
                nextFsMIOspfRouteIpNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfRoutingTable (INT4 i4FsMIOspfRouteContextId,
                                     INT4 *pi4NextFsMIOspfRouteContextId,
                                     UINT4 u4FsMIOspfRouteIpAddr,
                                     UINT4 *pu4NextFsMIOspfRouteIpAddr,
                                     UINT4 u4FsMIOspfRouteIpAddrMask,
                                     UINT4 *pu4NextFsMIOspfRouteIpAddrMask,
                                     INT4 i4FsMIOspfRouteIpTos,
                                     INT4 *pi4NextFsMIOspfRouteIpTos,
                                     UINT4 u4FsMIOspfRouteIpNextHop,
                                     UINT4 *pu4NextFsMIOspfRouteIpNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (UtilOspfIsValidCxtId (i4FsMIOspfRouteContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetNextIndexFutOspfRoutingTable (u4FsMIOspfRouteIpAddr,
                                                       pu4NextFsMIOspfRouteIpAddr,
                                                       u4FsMIOspfRouteIpAddrMask,
                                                       pu4NextFsMIOspfRouteIpAddrMask,
                                                       i4FsMIOspfRouteIpTos,
                                                       pi4NextFsMIOspfRouteIpTos,
                                                       u4FsMIOspfRouteIpNextHop,
                                                       pu4NextFsMIOspfRouteIpNextHop);
        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfRouteContextId,
                                      (UINT4 *) pi4NextFsMIOspfRouteContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfRouteContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfRoutingTable (pu4NextFsMIOspfRouteIpAddr,
                                                     pu4NextFsMIOspfRouteIpAddrMask,
                                                     pi4NextFsMIOspfRouteIpTos,
                                                     pu4NextFsMIOspfRouteIpNextHop);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfRouteContextId = *pi4NextFsMIOspfRouteContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfRouteContextId = i4FsMIOspfRouteContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfRouteType
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop

                The Object 
                retValFsMIOspfRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRouteType (INT4 i4FsMIOspfRouteContextId,
                         UINT4 u4FsMIOspfRouteIpAddr,
                         UINT4 u4FsMIOspfRouteIpAddrMask,
                         INT4 i4FsMIOspfRouteIpTos,
                         UINT4 u4FsMIOspfRouteIpNextHop,
                         INT4 *pi4RetValFsMIOspfRouteType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRouteType (u4FsMIOspfRouteIpAddr,
                                       u4FsMIOspfRouteIpAddrMask,
                                       i4FsMIOspfRouteIpTos,
                                       u4FsMIOspfRouteIpNextHop,
                                       pi4RetValFsMIOspfRouteType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRouteAreaId
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop

                The Object 
                retValFsMIOspfRouteAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRouteAreaId (INT4 i4FsMIOspfRouteContextId,
                           UINT4 u4FsMIOspfRouteIpAddr,
                           UINT4 u4FsMIOspfRouteIpAddrMask,
                           INT4 i4FsMIOspfRouteIpTos,
                           UINT4 u4FsMIOspfRouteIpNextHop,
                           UINT4 *pu4RetValFsMIOspfRouteAreaId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRouteAreaId (u4FsMIOspfRouteIpAddr,
                                         u4FsMIOspfRouteIpAddrMask,
                                         i4FsMIOspfRouteIpTos,
                                         u4FsMIOspfRouteIpNextHop,
                                         pu4RetValFsMIOspfRouteAreaId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRouteCost
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop

                The Object 
                retValFsMIOspfRouteCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRouteCost (INT4 i4FsMIOspfRouteContextId,
                         UINT4 u4FsMIOspfRouteIpAddr,
                         UINT4 u4FsMIOspfRouteIpAddrMask,
                         INT4 i4FsMIOspfRouteIpTos,
                         UINT4 u4FsMIOspfRouteIpNextHop,
                         INT4 *pi4RetValFsMIOspfRouteCost)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRouteCost (u4FsMIOspfRouteIpAddr,
                                       u4FsMIOspfRouteIpAddrMask,
                                       i4FsMIOspfRouteIpTos,
                                       u4FsMIOspfRouteIpNextHop,
                                       pi4RetValFsMIOspfRouteCost);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRouteType2Cost
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop

                The Object 
                retValFsMIOspfRouteType2Cost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRouteType2Cost (INT4 i4FsMIOspfRouteContextId,
                              UINT4 u4FsMIOspfRouteIpAddr,
                              UINT4 u4FsMIOspfRouteIpAddrMask,
                              INT4 i4FsMIOspfRouteIpTos,
                              UINT4 u4FsMIOspfRouteIpNextHop,
                              INT4 *pi4RetValFsMIOspfRouteType2Cost)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRouteType2Cost (u4FsMIOspfRouteIpAddr,
                                            u4FsMIOspfRouteIpAddrMask,
                                            i4FsMIOspfRouteIpTos,
                                            u4FsMIOspfRouteIpNextHop,
                                            pi4RetValFsMIOspfRouteType2Cost);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRouteInterfaceIndex
 Input       :  The Indices
                FsMIOspfRouteContextId
                FsMIOspfRouteIpAddr
                FsMIOspfRouteIpAddrMask
                FsMIOspfRouteIpTos
                FsMIOspfRouteIpNextHop

                The Object 
                retValFsMIOspfRouteInterfaceIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRouteInterfaceIndex (INT4 i4FsMIOspfRouteContextId,
                                   UINT4 u4FsMIOspfRouteIpAddr,
                                   UINT4 u4FsMIOspfRouteIpAddrMask,
                                   INT4 i4FsMIOspfRouteIpTos,
                                   UINT4 u4FsMIOspfRouteIpNextHop,
                                   INT4 *pi4RetValFsMIOspfRouteInterfaceIndex)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRouteInterfaceIndex (u4FsMIOspfRouteIpAddr,
                                                 u4FsMIOspfRouteIpAddrMask,
                                                 i4FsMIOspfRouteIpTos,
                                                 u4FsMIOspfRouteIpNextHop,
                                                 pi4RetValFsMIOspfRouteInterfaceIndex);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfSecIfTable
 Input       :  The Indices
                FsMIOspfSecIfContextId
                FsMIOspfPrimIpAddr
                FsMIOspfPrimAddresslessIf
                FsMIOspfSecIpAddr
                FsMIOspfSecIpAddrMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfSecIfTable (INT4 *pi4FsMIOspfSecIfContextId,
                                    UINT4 *pu4FsMIOspfPrimIpAddr,
                                    INT4 *pi4FsMIOspfPrimAddresslessIf,
                                    UINT4 *pu4FsMIOspfSecIpAddr,
                                    UINT4 *pu4FsMIOspfSecIpAddrMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfSecIfContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfSecIfContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfSecIfContextId = *pi4FsMIOspfSecIfContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfSecIfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetFirstIndexFutOspfSecIfTable (pu4FsMIOspfPrimIpAddr,
                                                      pi4FsMIOspfPrimAddresslessIf,
                                                      pu4FsMIOspfSecIpAddr,
                                                      pu4FsMIOspfSecIpAddrMask);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfSecIfContextId,
                                 (UINT4 *) pi4FsMIOspfSecIfContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfSecIfTable
 Input       :  The Indices
                FsMIOspfSecIfContextId
                nextFsMIOspfSecIfContextId
                FsMIOspfPrimIpAddr
                nextFsMIOspfPrimIpAddr
                FsMIOspfPrimAddresslessIf
                nextFsMIOspfPrimAddresslessIf
                FsMIOspfSecIpAddr
                nextFsMIOspfSecIpAddr
                FsMIOspfSecIpAddrMask
                nextFsMIOspfSecIpAddrMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfSecIfTable (INT4 i4FsMIOspfSecIfContextId,
                                   INT4 *pi4NextFsMIOspfSecIfContextId,
                                   UINT4 u4FsMIOspfPrimIpAddr,
                                   UINT4 *pu4NextFsMIOspfPrimIpAddr,
                                   INT4 i4FsMIOspfPrimAddresslessIf,
                                   INT4 *pi4NextFsMIOspfPrimAddresslessIf,
                                   UINT4 u4FsMIOspfSecIpAddr,
                                   UINT4 *pu4NextFsMIOspfSecIpAddr,
                                   UINT4 u4FsMIOspfSecIpAddrMask,
                                   UINT4 *pu4NextFsMIOspfSecIpAddrMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (UtilOspfIsValidCxtId (i4FsMIOspfSecIfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfSecIfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetNextIndexFutOspfSecIfTable (u4FsMIOspfPrimIpAddr,
                                                     pu4NextFsMIOspfPrimIpAddr,
                                                     i4FsMIOspfPrimAddresslessIf,
                                                     pi4NextFsMIOspfPrimAddresslessIf,
                                                     u4FsMIOspfSecIpAddr,
                                                     pu4NextFsMIOspfSecIpAddr,
                                                     u4FsMIOspfSecIpAddrMask,
                                                     pu4NextFsMIOspfSecIpAddrMask);
        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfSecIfContextId,
                                      (UINT4 *) pi4NextFsMIOspfSecIfContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfSecIfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfSecIfTable (pu4NextFsMIOspfPrimIpAddr,
                                                   pi4NextFsMIOspfPrimAddresslessIf,
                                                   pu4NextFsMIOspfSecIpAddr,
                                                   pu4NextFsMIOspfSecIpAddrMask);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfSecIfContextId = *pi4NextFsMIOspfSecIfContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfSecIfContextId = i4FsMIOspfSecIfContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfSecIfStatus
 Input       :  The Indices
                FsMIOspfSecIfContextId
                FsMIOspfPrimIpAddr
                FsMIOspfPrimAddresslessIf
                FsMIOspfSecIpAddr
                FsMIOspfSecIpAddrMask

                The Object 
                retValFsMIOspfSecIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfSecIfStatus (INT4 i4FsMIOspfSecIfContextId,
                           UINT4 u4FsMIOspfPrimIpAddr,
                           INT4 i4FsMIOspfPrimAddresslessIf,
                           UINT4 u4FsMIOspfSecIpAddr,
                           UINT4 u4FsMIOspfSecIpAddrMask,
                           INT4 *pi4RetValFsMIOspfSecIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfSecIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfSecIfStatus (u4FsMIOspfPrimIpAddr,
                                         i4FsMIOspfPrimAddresslessIf,
                                         u4FsMIOspfSecIpAddr,
                                         u4FsMIOspfSecIpAddrMask,
                                         pi4RetValFsMIOspfSecIfStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfAreaAggregateTable
 Input       :  The Indices
                FsMIOspfAreaAggregateContextId
                FsMIOspfAreaAggregateAreaID
                FsMIOspfAreaAggregateLsdbType
                FsMIOspfAreaAggregateNet
                FsMIOspfAreaAggregateMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfAreaAggregateTable (INT4
                                            *pi4FsMIOspfAreaAggregateContextId,
                                            UINT4
                                            *pu4FsMIOspfAreaAggregateAreaID,
                                            INT4
                                            *pi4FsMIOspfAreaAggregateLsdbType,
                                            UINT4 *pu4FsMIOspfAreaAggregateNet,
                                            UINT4 *pu4FsMIOspfAreaAggregateMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfAreaAggregateContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfAreaAggregateContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfAreaAggregateContextId = *pi4FsMIOspfAreaAggregateContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfAreaAggregateContextId)
            == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfAreaAggregateTable
            (pu4FsMIOspfAreaAggregateAreaID, pi4FsMIOspfAreaAggregateLsdbType,
             pu4FsMIOspfAreaAggregateNet, pu4FsMIOspfAreaAggregateMask);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfAreaAggregateContextId,
                                 (UINT4 *) pi4FsMIOspfAreaAggregateContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfAreaAggregateTable
 Input       :  The Indices
                FsMIOspfAreaAggregateContextId
                nextFsMIOspfAreaAggregateContextId
                FsMIOspfAreaAggregateAreaID
                nextFsMIOspfAreaAggregateAreaID
                FsMIOspfAreaAggregateLsdbType
                nextFsMIOspfAreaAggregateLsdbType
                FsMIOspfAreaAggregateNet
                nextFsMIOspfAreaAggregateNet
                FsMIOspfAreaAggregateMask
                nextFsMIOspfAreaAggregateMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfAreaAggregateTable (INT4
                                           i4FsMIOspfAreaAggregateContextId,
                                           INT4
                                           *pi4NextFsMIOspfAreaAggregateContextId,
                                           UINT4 u4FsMIOspfAreaAggregateAreaID,
                                           UINT4
                                           *pu4NextFsMIOspfAreaAggregateAreaID,
                                           INT4 i4FsMIOspfAreaAggregateLsdbType,
                                           INT4
                                           *pi4NextFsMIOspfAreaAggregateLsdbType,
                                           UINT4 u4FsMIOspfAreaAggregateNet,
                                           UINT4
                                           *pu4NextFsMIOspfAreaAggregateNet,
                                           UINT4 u4FsMIOspfAreaAggregateMask,
                                           UINT4
                                           *pu4NextFsMIOspfAreaAggregateMask)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfAreaAggregateContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfAreaAggregateContextId) ==
            SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfAreaAggregateTable
            (u4FsMIOspfAreaAggregateAreaID, pu4NextFsMIOspfAreaAggregateAreaID,
             i4FsMIOspfAreaAggregateLsdbType,
             pi4NextFsMIOspfAreaAggregateLsdbType, u4FsMIOspfAreaAggregateNet,
             pu4NextFsMIOspfAreaAggregateNet, u4FsMIOspfAreaAggregateMask,
             pu4NextFsMIOspfAreaAggregateMask);
        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfAreaAggregateContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfAreaAggregateContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfAreaAggregateContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfAreaAggregateTable
                (pu4NextFsMIOspfAreaAggregateAreaID,
                 pi4NextFsMIOspfAreaAggregateLsdbType,
                 pu4NextFsMIOspfAreaAggregateNet,
                 pu4NextFsMIOspfAreaAggregateMask);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfAreaAggregateContextId =
                *pi4NextFsMIOspfAreaAggregateContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfAreaAggregateContextId
            = i4FsMIOspfAreaAggregateContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaAggregateExternalTag
 Input       :  The Indices
                FsMIOspfAreaAggregateContextId
                FsMIOspfAreaAggregateAreaID
                FsMIOspfAreaAggregateLsdbType
                FsMIOspfAreaAggregateNet
                FsMIOspfAreaAggregateMask

                The Object 
                retValFsMIOspfAreaAggregateExternalTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaAggregateExternalTag (INT4 i4FsMIOspfAreaAggregateContextId,
                                        UINT4 u4FsMIOspfAreaAggregateAreaID,
                                        INT4 i4FsMIOspfAreaAggregateLsdbType,
                                        UINT4 u4FsMIOspfAreaAggregateNet,
                                        UINT4 u4FsMIOspfAreaAggregateMask,
                                        INT4
                                        *pi4RetValFsMIOspfAreaAggregateExternalTag)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAreaAggregateContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfAreaAggregateExternalTag (u4FsMIOspfAreaAggregateAreaID,
                                               i4FsMIOspfAreaAggregateLsdbType,
                                               u4FsMIOspfAreaAggregateNet,
                                               u4FsMIOspfAreaAggregateMask,
                                               pi4RetValFsMIOspfAreaAggregateExternalTag);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfAsExternalAggregationTable
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfAsExternalAggregationTable (INT4
                                                    *pi4FsMIOspfAsExternalAggregationContextId,
                                                    UINT4
                                                    *pu4FsMIOspfAsExternalAggregationNet,
                                                    UINT4
                                                    *pu4FsMIOspfAsExternalAggregationMask,
                                                    UINT4
                                                    *pu4FsMIOspfAsExternalAggregationAreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfAsExternalAggregationContextId;

    if (UtilOspfGetFirstCxtId
        ((UINT4 *) pi4FsMIOspfAsExternalAggregationContextId) == OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfAsExternalAggregationContextId =
            *pi4FsMIOspfAsExternalAggregationContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfAsExternalAggregationContextId)
            == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfAsExternalAggregationTable
            (pu4FsMIOspfAsExternalAggregationNet,
             pu4FsMIOspfAsExternalAggregationMask,
             pu4FsMIOspfAsExternalAggregationAreaId);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId
           ((UINT4) i4FsMIOspfAsExternalAggregationContextId,
            (UINT4 *) pi4FsMIOspfAsExternalAggregationContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfAsExternalAggregationTable
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                nextFsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                nextFsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                nextFsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId
                nextFsMIOspfAsExternalAggregationAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfAsExternalAggregationTable (INT4
                                                   i4FsMIOspfAsExternalAggregationContextId,
                                                   INT4
                                                   *pi4NextFsMIOspfAsExternalAggregationContextId,
                                                   UINT4
                                                   u4FsMIOspfAsExternalAggregationNet,
                                                   UINT4
                                                   *pu4NextFsMIOspfAsExternalAggregationNet,
                                                   UINT4
                                                   u4FsMIOspfAsExternalAggregationMask,
                                                   UINT4
                                                   *pu4NextFsMIOspfAsExternalAggregationMask,
                                                   UINT4
                                                   u4FsMIOspfAsExternalAggregationAreaId,
                                                   UINT4
                                                   *pu4NextFsMIOspfAsExternalAggregationAreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfAsExternalAggregationContextId)
        == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId)
            == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfAsExternalAggregationTable
            (u4FsMIOspfAsExternalAggregationNet,
             pu4NextFsMIOspfAsExternalAggregationNet,
             u4FsMIOspfAsExternalAggregationMask,
             pu4NextFsMIOspfAsExternalAggregationMask,
             u4FsMIOspfAsExternalAggregationAreaId,
             pu4NextFsMIOspfAsExternalAggregationAreaId);
        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId
                ((UINT4) i4FsMIOspfAsExternalAggregationContextId,
                 (UINT4 *) pi4NextFsMIOspfAsExternalAggregationContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext
                (*pi4NextFsMIOspfAsExternalAggregationContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfAsExternalAggregationTable
                (pu4NextFsMIOspfAsExternalAggregationNet,
                 pu4NextFsMIOspfAsExternalAggregationMask,
                 pu4NextFsMIOspfAsExternalAggregationAreaId);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfAsExternalAggregationContextId
                = *pi4NextFsMIOspfAsExternalAggregationContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfAsExternalAggregationContextId
            = i4FsMIOspfAsExternalAggregationContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfAsExternalAggregationEffect
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                retValFsMIOspfAsExternalAggregationEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAsExternalAggregationEffect (INT4
                                           i4FsMIOspfAsExternalAggregationContextId,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationNet,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationMask,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationAreaId,
                                           INT4
                                           *pi4RetValFsMIOspfAsExternalAggregationEffect)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfAsExternalAggregationEffect
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId,
         pi4RetValFsMIOspfAsExternalAggregationEffect);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAsExternalAggregationTranslation
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                retValFsMIOspfAsExternalAggregationTranslation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAsExternalAggregationTranslation (INT4
                                                i4FsMIOspfAsExternalAggregationContextId,
                                                UINT4
                                                u4FsMIOspfAsExternalAggregationNet,
                                                UINT4
                                                u4FsMIOspfAsExternalAggregationMask,
                                                UINT4
                                                u4FsMIOspfAsExternalAggregationAreaId,
                                                INT4
                                                *pi4RetValFsMIOspfAsExternalAggregationTranslation)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfAsExternalAggregationTranslation
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId,
         pi4RetValFsMIOspfAsExternalAggregationTranslation);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAsExternalAggregationStatus
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                retValFsMIOspfAsExternalAggregationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAsExternalAggregationStatus (INT4
                                           i4FsMIOspfAsExternalAggregationContextId,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationNet,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationMask,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationAreaId,
                                           INT4
                                           *pi4RetValFsMIOspfAsExternalAggregationStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfAsExternalAggregationStatus
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId,
         pi4RetValFsMIOspfAsExternalAggregationStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfTrapTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfTrapTable (INT4 *pi4FsMIStdOspfContextId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfTrapTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfTrapTable (INT4 i4FsMIStdOspfContextId,
                                     INT4 *pi4NextFsMIStdOspfContextId)
{
    if (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                              (UINT4 *) pi4NextFsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfSetTrap
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIStdOspfSetTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfSetTrap (INT4 i4FsMIStdOspfContextId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMIStdOspfSetTrap)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfSetTrap (pRetValFsMIStdOspfSetTrap);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfConfigErrorType
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIStdOspfConfigErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfConfigErrorType (INT4 i4FsMIStdOspfContextId,
                                  INT4 *pi4RetValFsMIStdOspfConfigErrorType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfConfigErrorType (pi4RetValFsMIStdOspfConfigErrorType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfPacketType
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIStdOspfPacketType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfPacketType (INT4 i4FsMIStdOspfContextId,
                             INT4 *pi4RetValFsMIStdOspfPacketType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfPacketType (pi4RetValFsMIStdOspfPacketType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfPacketSrc
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIStdOspfPacketSrc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfPacketSrc (INT4 i4FsMIStdOspfContextId,
                            UINT4 *pu4RetValFsMIStdOspfPacketSrc)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfPacketSrc (pu4RetValFsMIStdOspfPacketSrc);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfOpaqueTable
 Input       :  The Indices
                FsMIOspfOpaqueContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfOpaqueTable (INT4 *pi4FsMIOspfOpaqueContextId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfOpaqueContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfOpaqueTable
 Input       :  The Indices
                FsMIOspfOpaqueContextId
                nextFsMIOspfOpaqueContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfOpaqueTable (INT4 i4FsMIOspfOpaqueContextId,
                                    INT4 *pi4NextFsMIOspfOpaqueContextId)
{
    if (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfOpaqueContextId,
                              (UINT4 *) pi4NextFsMIOspfOpaqueContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfOpaqueOption
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                retValFsMIOspfOpaqueOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfOpaqueOption (INT4 i4FsMIOspfOpaqueContextId,
                            INT4 *pi4RetValFsMIOspfOpaqueOption)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfOpaqueOption (pi4RetValFsMIOspfOpaqueOption);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType11LsaCount
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                retValFsMIOspfType11LsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType11LsaCount (INT4 i4FsMIOspfOpaqueContextId,
                              UINT4 *pu4RetValFsMIOspfType11LsaCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType11LsaCount (pu4RetValFsMIOspfType11LsaCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType11LsaCksumSum
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                retValFsMIOspfType11LsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType11LsaCksumSum (INT4 i4FsMIOspfOpaqueContextId,
                                 INT4 *pi4RetValFsMIOspfType11LsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfType11LsaCksumSum (pi4RetValFsMIOspfType11LsaCksumSum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAreaIDValid
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                retValFsMIOspfAreaIDValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAreaIDValid (INT4 i4FsMIOspfOpaqueContextId,
                           INT4 *pi4RetValFsMIOspfAreaIDValid)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAreaIDValid (pi4RetValFsMIOspfAreaIDValid);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfOpaqueInterfaceTable
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfOpaqueInterfaceTable (INT4 *pi4FsMIOspfIfContextId,
                                              UINT4 *pu4FsMIOspfIfIpAddress,
                                              INT4 *pi4FsMIOspfAddressLessIf)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfIfContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfIfContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIOspfIfContextId = *pi4FsMIOspfIfContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfIfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfOpaqueInterfaceTable (pu4FsMIOspfIfIpAddress,
                                                         pi4FsMIOspfAddressLessIf);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfContextId,
                                 (UINT4 *) pi4FsMIOspfIfContextId) !=
           OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfOpaqueInterfaceTable
 Input       :  The Indices
                FsMIOspfIfContextId
                nextFsMIOspfIfContextId
                FsMIOspfIfIpAddress
                nextFsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
                nextFsMIOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfOpaqueInterfaceTable (INT4 i4FsMIOspfIfContextId,
                                             INT4 *pi4NextFsMIOspfIfContextId,
                                             UINT4 u4FsMIOspfIfIpAddress,
                                             UINT4 *pu4NextFsMIOspfIfIpAddress,
                                             INT4 i4FsMIOspfAddressLessIf,
                                             INT4 *pi4NextFsMIOspfAddressLessIf)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfIfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfOpaqueInterfaceTable (u4FsMIOspfIfIpAddress,
                                                        pu4NextFsMIOspfIfIpAddress,
                                                        i4FsMIOspfAddressLessIf,
                                                        pi4NextFsMIOspfAddressLessIf);
        UtilOspfResetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfContextId,
                                      (UINT4 *) pi4NextFsMIOspfIfContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfIfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfOpaqueInterfaceTable
                (pu4NextFsMIOspfIfIpAddress, pi4NextFsMIOspfAddressLessIf);

            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfIfContextId = *pi4NextFsMIOspfIfContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfIfContextId = i4FsMIOspfIfContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfOpaqueType9LsaCount
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfOpaqueType9LsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfOpaqueType9LsaCount (INT4 i4FsMIOspfIfContextId,
                                   UINT4 u4FsMIOspfIfIpAddress,
                                   INT4 i4FsMIOspfAddressLessIf,
                                   UINT4 *pu4RetValFsMIOspfOpaqueType9LsaCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfOpaqueType9LsaCount (u4FsMIOspfIfIpAddress,
                                                 i4FsMIOspfAddressLessIf,
                                                 pu4RetValFsMIOspfOpaqueType9LsaCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfOpaqueType9LsaCksumSum
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                retValFsMIOspfOpaqueType9LsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfOpaqueType9LsaCksumSum (INT4 i4FsMIOspfIfContextId,
                                      UINT4 u4FsMIOspfIfIpAddress,
                                      INT4 i4FsMIOspfAddressLessIf,
                                      INT4
                                      *pi4RetValFsMIOspfOpaqueType9LsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfOpaqueType9LsaCksumSum (u4FsMIOspfIfIpAddress,
                                                    i4FsMIOspfAddressLessIf,
                                                    pi4RetValFsMIOspfOpaqueType9LsaCksumSum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfType9LsdbTable
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfType9LsdbTable (INT4 *pi4FsMIOspfType9LsdbContextId,
                                        UINT4 *pu4FsMIOspfType9LsdbIfIpAddress,
                                        INT4 *pi4FsMIOspfType9LsdbOpaqueType,
                                        UINT4 *pu4FsMIOspfType9LsdbLsid,
                                        UINT4 *pu4FsMIOspfType9LsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfType9LsdbContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfType9LsdbContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfType9LsdbContextId = *pi4FsMIOspfType9LsdbContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfType9LsdbTable
            (pu4FsMIOspfType9LsdbIfIpAddress, pi4FsMIOspfType9LsdbOpaqueType,
             pu4FsMIOspfType9LsdbLsid, pu4FsMIOspfType9LsdbRouterId);
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        UtilOspfResetContext ();
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfType9LsdbContextId,
                                 (UINT4 *) pi4FsMIOspfType9LsdbContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfType9LsdbTable
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                nextFsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                nextFsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                nextFsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                nextFsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId
                nextFsMIOspfType9LsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfType9LsdbTable (INT4 i4FsMIOspfType9LsdbContextId,
                                       INT4 *pi4NextFsMIOspfType9LsdbContextId,
                                       UINT4 u4FsMIOspfType9LsdbIfIpAddress,
                                       UINT4
                                       *pu4NextFsMIOspfType9LsdbIfIpAddress,
                                       INT4 i4FsMIOspfType9LsdbOpaqueType,
                                       INT4 *pi4NextFsMIOspfType9LsdbOpaqueType,
                                       UINT4 u4FsMIOspfType9LsdbLsid,
                                       UINT4 *pu4NextFsMIOspfType9LsdbLsid,
                                       UINT4 u4FsMIOspfType9LsdbRouterId,
                                       UINT4 *pu4NextFsMIOspfType9LsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfType9LsdbContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfType9LsdbTable
            (u4FsMIOspfType9LsdbIfIpAddress,
             pu4NextFsMIOspfType9LsdbIfIpAddress, i4FsMIOspfType9LsdbOpaqueType,
             pi4NextFsMIOspfType9LsdbOpaqueType, u4FsMIOspfType9LsdbLsid,
             pu4NextFsMIOspfType9LsdbLsid, u4FsMIOspfType9LsdbRouterId,
             pu4NextFsMIOspfType9LsdbRouterId);
        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfType9LsdbContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfType9LsdbContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfType9LsdbContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfType9LsdbTable
                (pu4NextFsMIOspfType9LsdbIfIpAddress,
                 pi4NextFsMIOspfType9LsdbOpaqueType,
                 pu4NextFsMIOspfType9LsdbLsid,
                 pu4NextFsMIOspfType9LsdbRouterId);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfType9LsdbContextId = *pi4NextFsMIOspfType9LsdbContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfType9LsdbContextId = i4FsMIOspfType9LsdbContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfType9LsdbSequence
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId

                The Object 
                retValFsMIOspfType9LsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType9LsdbSequence (INT4 i4FsMIOspfType9LsdbContextId,
                                 UINT4 u4FsMIOspfType9LsdbIfIpAddress,
                                 INT4 i4FsMIOspfType9LsdbOpaqueType,
                                 UINT4 u4FsMIOspfType9LsdbLsid,
                                 UINT4 u4FsMIOspfType9LsdbRouterId,
                                 INT4 *pi4RetValFsMIOspfType9LsdbSequence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType9LsdbSequence (u4FsMIOspfType9LsdbIfIpAddress,
                                               i4FsMIOspfType9LsdbOpaqueType,
                                               u4FsMIOspfType9LsdbLsid,
                                               u4FsMIOspfType9LsdbRouterId,
                                               pi4RetValFsMIOspfType9LsdbSequence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType9LsdbAge
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId

                The Object 
                retValFsMIOspfType9LsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType9LsdbAge (INT4 i4FsMIOspfType9LsdbContextId,
                            UINT4 u4FsMIOspfType9LsdbIfIpAddress,
                            INT4 i4FsMIOspfType9LsdbOpaqueType,
                            UINT4 u4FsMIOspfType9LsdbLsid,
                            UINT4 u4FsMIOspfType9LsdbRouterId,
                            INT4 *pi4RetValFsMIOspfType9LsdbAge)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType9LsdbAge (u4FsMIOspfType9LsdbIfIpAddress,
                                          i4FsMIOspfType9LsdbOpaqueType,
                                          u4FsMIOspfType9LsdbLsid,
                                          u4FsMIOspfType9LsdbRouterId,
                                          pi4RetValFsMIOspfType9LsdbAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType9LsdbChecksum
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId

                The Object 
                retValFsMIOspfType9LsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType9LsdbChecksum (INT4 i4FsMIOspfType9LsdbContextId,
                                 UINT4 u4FsMIOspfType9LsdbIfIpAddress,
                                 INT4 i4FsMIOspfType9LsdbOpaqueType,
                                 UINT4 u4FsMIOspfType9LsdbLsid,
                                 UINT4 u4FsMIOspfType9LsdbRouterId,
                                 INT4 *pi4RetValFsMIOspfType9LsdbChecksum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType9LsdbChecksum (u4FsMIOspfType9LsdbIfIpAddress,
                                               i4FsMIOspfType9LsdbOpaqueType,
                                               u4FsMIOspfType9LsdbLsid,
                                               u4FsMIOspfType9LsdbRouterId,
                                               pi4RetValFsMIOspfType9LsdbChecksum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType9LsdbAdvertisement
 Input       :  The Indices
                FsMIOspfType9LsdbContextId
                FsMIOspfType9LsdbIfIpAddress
                FsMIOspfType9LsdbOpaqueType
                FsMIOspfType9LsdbLsid
                FsMIOspfType9LsdbRouterId

                The Object 
                retValFsMIOspfType9LsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType9LsdbAdvertisement (INT4 i4FsMIOspfType9LsdbContextId,
                                      UINT4 u4FsMIOspfType9LsdbIfIpAddress,
                                      INT4 i4FsMIOspfType9LsdbOpaqueType,
                                      UINT4 u4FsMIOspfType9LsdbLsid,
                                      UINT4 u4FsMIOspfType9LsdbRouterId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIOspfType9LsdbAdvertisement)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType9LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfType9LsdbAdvertisement (u4FsMIOspfType9LsdbIfIpAddress,
                                             i4FsMIOspfType9LsdbOpaqueType,
                                             u4FsMIOspfType9LsdbLsid,
                                             u4FsMIOspfType9LsdbRouterId,
                                             pRetValFsMIOspfType9LsdbAdvertisement);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfType11LsdbTable
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfType11LsdbTable (INT4 *pi4FsMIOspfType11LsdbContextId,
                                         INT4 *pi4FsMIOspfType11LsdbOpaqueType,
                                         UINT4 *pu4FsMIOspfType11LsdbLsid,
                                         UINT4 *pu4FsMIOspfType11LsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfType11LsdbContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfType11LsdbContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfType11LsdbContextId = *pi4FsMIOspfType11LsdbContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfType11LsdbContextId)
            == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfType11LsdbTable
            (pi4FsMIOspfType11LsdbOpaqueType, pu4FsMIOspfType11LsdbLsid,
             pu4FsMIOspfType11LsdbRouterId);
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        UtilOspfResetContext ();
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfType11LsdbContextId,
                                 (UINT4 *) pi4FsMIOspfType11LsdbContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfType11LsdbTable
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                nextFsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                nextFsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                nextFsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId
                nextFsMIOspfType11LsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfType11LsdbTable (INT4 i4FsMIOspfType11LsdbContextId,
                                        INT4
                                        *pi4NextFsMIOspfType11LsdbContextId,
                                        INT4 i4FsMIOspfType11LsdbOpaqueType,
                                        INT4
                                        *pi4NextFsMIOspfType11LsdbOpaqueType,
                                        UINT4 u4FsMIOspfType11LsdbLsid,
                                        UINT4 *pu4NextFsMIOspfType11LsdbLsid,
                                        UINT4 u4FsMIOspfType11LsdbRouterId,
                                        UINT4
                                        *pu4NextFsMIOspfType11LsdbRouterId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfType11LsdbContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfType11LsdbContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfType11LsdbTable
            (i4FsMIOspfType11LsdbOpaqueType,
             pi4NextFsMIOspfType11LsdbOpaqueType,
             u4FsMIOspfType11LsdbLsid,
             pu4NextFsMIOspfType11LsdbLsid,
             u4FsMIOspfType11LsdbRouterId, pu4NextFsMIOspfType11LsdbRouterId);
        UtilOspfResetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfType11LsdbContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfType11LsdbContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfType11LsdbContextId)
                == SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfType11LsdbTable
                (pi4NextFsMIOspfType11LsdbOpaqueType,
                 pu4NextFsMIOspfType11LsdbLsid,
                 pu4NextFsMIOspfType11LsdbRouterId);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfType11LsdbContextId = *pi4NextFsMIOspfType11LsdbContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfType11LsdbContextId = i4FsMIOspfType11LsdbContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfType11LsdbSequence
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId

                The Object 
                retValFsMIOspfType11LsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType11LsdbSequence (INT4 i4FsMIOspfType11LsdbContextId,
                                  INT4 i4FsMIOspfType11LsdbOpaqueType,
                                  UINT4 u4FsMIOspfType11LsdbLsid,
                                  UINT4 u4FsMIOspfType11LsdbRouterId,
                                  INT4 *pi4RetValFsMIOspfType11LsdbSequence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType11LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType11LsdbSequence (i4FsMIOspfType11LsdbOpaqueType,
                                                u4FsMIOspfType11LsdbLsid,
                                                u4FsMIOspfType11LsdbRouterId,
                                                pi4RetValFsMIOspfType11LsdbSequence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType11LsdbAge
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId

                The Object 
                retValFsMIOspfType11LsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType11LsdbAge (INT4 i4FsMIOspfType11LsdbContextId,
                             INT4 i4FsMIOspfType11LsdbOpaqueType,
                             UINT4 u4FsMIOspfType11LsdbLsid,
                             UINT4 u4FsMIOspfType11LsdbRouterId,
                             INT4 *pi4RetValFsMIOspfType11LsdbAge)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType11LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType11LsdbAge (i4FsMIOspfType11LsdbOpaqueType,
                                           u4FsMIOspfType11LsdbLsid,
                                           u4FsMIOspfType11LsdbRouterId,
                                           pi4RetValFsMIOspfType11LsdbAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType11LsdbChecksum
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId

                The Object 
                retValFsMIOspfType11LsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType11LsdbChecksum (INT4 i4FsMIOspfType11LsdbContextId,
                                  INT4 i4FsMIOspfType11LsdbOpaqueType,
                                  UINT4 u4FsMIOspfType11LsdbLsid,
                                  UINT4 u4FsMIOspfType11LsdbRouterId,
                                  INT4 *pi4RetValFsMIOspfType11LsdbChecksum)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType11LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfType11LsdbChecksum (i4FsMIOspfType11LsdbOpaqueType,
                                                u4FsMIOspfType11LsdbLsid,
                                                u4FsMIOspfType11LsdbRouterId,
                                                pi4RetValFsMIOspfType11LsdbChecksum);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfType11LsdbAdvertisement
 Input       :  The Indices
                FsMIOspfType11LsdbContextId
                FsMIOspfType11LsdbOpaqueType
                FsMIOspfType11LsdbLsid
                FsMIOspfType11LsdbRouterId

                The Object 
                retValFsMIOspfType11LsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfType11LsdbAdvertisement (INT4 i4FsMIOspfType11LsdbContextId,
                                       INT4 i4FsMIOspfType11LsdbOpaqueType,
                                       UINT4 u4FsMIOspfType11LsdbLsid,
                                       UINT4 u4FsMIOspfType11LsdbRouterId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIOspfType11LsdbAdvertisement)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfType11LsdbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfType11LsdbAdvertisement (i4FsMIOspfType11LsdbOpaqueType,
                                              u4FsMIOspfType11LsdbLsid,
                                              u4FsMIOspfType11LsdbRouterId,
                                              pRetValFsMIOspfType11LsdbAdvertisement);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfAppInfoDbTable
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfAppInfoDbTable (INT4 *pi4FsMIOspfAppInfoDbContextId,
                                        INT4 *pi4FsMIOspfAppInfoDbAppid)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfAppInfoDbContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfAppInfoDbContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfAppInfoDbContextId = *pi4FsMIOspfAppInfoDbContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfAppInfoDbTable (pi4FsMIOspfAppInfoDbAppid);
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        UtilOspfResetContext ();
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfAppInfoDbContextId,
                                 (UINT4 *) pi4FsMIOspfAppInfoDbContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfAppInfoDbTable
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                nextFsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid
                nextFsMIOspfAppInfoDbAppid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfAppInfoDbTable (INT4 i4FsMIOspfAppInfoDbContextId,
                                       INT4 *pi4NextFsMIOspfAppInfoDbContextId,
                                       INT4 i4FsMIOspfAppInfoDbAppid,
                                       INT4 *pi4NextFsMIOspfAppInfoDbAppid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfAppInfoDbContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfAppInfoDbTable (i4FsMIOspfAppInfoDbAppid,
                                                  pi4NextFsMIOspfAppInfoDbAppid);
        UtilOspfResetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfAppInfoDbContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfAppInfoDbContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfAppInfoDbContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfAppInfoDbTable
                (pi4NextFsMIOspfAppInfoDbAppid);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfAppInfoDbContextId = *pi4NextFsMIOspfAppInfoDbContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfAppInfoDbContextId = i4FsMIOspfAppInfoDbContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbOpaqueType
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbOpaqueType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbOpaqueType (INT4 i4FsMIOspfAppInfoDbContextId,
                                   INT4 i4FsMIOspfAppInfoDbAppid,
                                   INT4 *pi4RetValFsMIOspfAppInfoDbOpaqueType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbOpaqueType (i4FsMIOspfAppInfoDbAppid,
                                                 pi4RetValFsMIOspfAppInfoDbOpaqueType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbLsaTypesSupported
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbLsaTypesSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbLsaTypesSupported (INT4 i4FsMIOspfAppInfoDbContextId,
                                          INT4 i4FsMIOspfAppInfoDbAppid,
                                          INT4
                                          *pi4RetValFsMIOspfAppInfoDbLsaTypesSupported)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfAppInfoDbLsaTypesSupported (i4FsMIOspfAppInfoDbAppid,
                                                 pi4RetValFsMIOspfAppInfoDbLsaTypesSupported);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbType9Gen
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbType9Gen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbType9Gen (INT4 i4FsMIOspfAppInfoDbContextId,
                                 INT4 i4FsMIOspfAppInfoDbAppid,
                                 UINT4 *pu4RetValFsMIOspfAppInfoDbType9Gen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbType9Gen (i4FsMIOspfAppInfoDbAppid,
                                               pu4RetValFsMIOspfAppInfoDbType9Gen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbType9Rcvd
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbType9Rcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbType9Rcvd (INT4 i4FsMIOspfAppInfoDbContextId,
                                  INT4 i4FsMIOspfAppInfoDbAppid,
                                  UINT4 *pu4RetValFsMIOspfAppInfoDbType9Rcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbType9Rcvd (i4FsMIOspfAppInfoDbAppid,
                                                pu4RetValFsMIOspfAppInfoDbType9Rcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbType10Gen
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbType10Gen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbType10Gen (INT4 i4FsMIOspfAppInfoDbContextId,
                                  INT4 i4FsMIOspfAppInfoDbAppid,
                                  UINT4 *pu4RetValFsMIOspfAppInfoDbType10Gen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbType10Gen (i4FsMIOspfAppInfoDbAppid,
                                                pu4RetValFsMIOspfAppInfoDbType10Gen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbType10Rcvd
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbType10Rcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbType10Rcvd (INT4 i4FsMIOspfAppInfoDbContextId,
                                   INT4 i4FsMIOspfAppInfoDbAppid,
                                   UINT4 *pu4RetValFsMIOspfAppInfoDbType10Rcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbType10Rcvd (i4FsMIOspfAppInfoDbAppid,
                                                 pu4RetValFsMIOspfAppInfoDbType10Rcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbType11Gen
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbType11Gen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbType11Gen (INT4 i4FsMIOspfAppInfoDbContextId,
                                  INT4 i4FsMIOspfAppInfoDbAppid,
                                  UINT4 *pu4RetValFsMIOspfAppInfoDbType11Gen)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbType11Gen (i4FsMIOspfAppInfoDbAppid,
                                                pu4RetValFsMIOspfAppInfoDbType11Gen);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfAppInfoDbType11Rcvd
 Input       :  The Indices
                FsMIOspfAppInfoDbContextId
                FsMIOspfAppInfoDbAppid

                The Object 
                retValFsMIOspfAppInfoDbType11Rcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfAppInfoDbType11Rcvd (INT4 i4FsMIOspfAppInfoDbContextId,
                                   INT4 i4FsMIOspfAppInfoDbAppid,
                                   UINT4 *pu4RetValFsMIOspfAppInfoDbType11Rcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfAppInfoDbContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfAppInfoDbType11Rcvd (i4FsMIOspfAppInfoDbAppid,
                                                 pu4RetValFsMIOspfAppInfoDbType11Rcvd);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfRRDRouteTable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfRRDRouteTable (INT4 *pi4FsMIOspfRRDRouteContextId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfRRDRouteContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfRRDRouteTable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId
                nextFsMIOspfRRDRouteContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfRRDRouteTable (INT4 i4FsMIOspfRRDRouteContextId,
                                      INT4 *pi4NextFsMIOspfRRDRouteContextId)
{
    if (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfRRDRouteContextId,
                              (UINT4 *) pi4NextFsMIOspfRRDRouteContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDStatus
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                retValFsMIOspfRRDStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDStatus (INT4 i4FsMIOspfRRDRouteContextId,
                         INT4 *pi4RetValFsMIOspfRRDStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDStatus (pi4RetValFsMIOspfRRDStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDSrcProtoMaskEnable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                retValFsMIOspfRRDSrcProtoMaskEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDSrcProtoMaskEnable (INT4 i4FsMIOspfRRDRouteContextId,
                                     INT4
                                     *pi4RetValFsMIOspfRRDSrcProtoMaskEnable)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfRRDSrcProtoMaskEnable
        (pi4RetValFsMIOspfRRDSrcProtoMaskEnable);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDSrcProtoMaskDisable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                retValFsMIOspfRRDSrcProtoMaskDisable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDSrcProtoMaskDisable (INT4 i4FsMIOspfRRDRouteContextId,
                                      INT4
                                      *pi4RetValFsMIOspfRRDSrcProtoMaskDisable)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfRRDSrcProtoMaskDisable
        (pi4RetValFsMIOspfRRDSrcProtoMaskDisable);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDRouteMapEnable
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfRRDRouteMapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDRouteMapEnable (INT4 i4FsMIStdOspfContextId,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValFsMIOspfRRDRouteMapEnable)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDRouteMapEnable
        (pRetValFsMIOspfRRDRouteMapEnable);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfRRDRouteConfigTable
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfRRDRouteConfigTable (INT4
                                             *pi4FsMIOspfRRDRouteConfigContextId,
                                             UINT4 *pu4FsMIOspfRRDRouteDest,
                                             UINT4 *pu4FsMIOspfRRDRouteMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfRRDRouteConfigContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfRRDRouteConfigContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIOspfRRDRouteConfigContextId = *pi4FsMIOspfRRDRouteConfigContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfRRDRouteConfigContextId) ==
            SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFutOspfRRDRouteConfigTable (pu4FsMIOspfRRDRouteDest,
                                                        pu4FsMIOspfRRDRouteMask);
        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfRRDRouteConfigContextId,
                                 (UINT4 *) pi4FsMIOspfRRDRouteConfigContextId)
           != OSPF_FAILURE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfRRDRouteConfigTable
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                nextFsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                nextFsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask
                nextFsMIOspfRRDRouteMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfRRDRouteConfigTable (INT4
                                            i4FsMIOspfRRDRouteConfigContextId,
                                            INT4
                                            *pi4NextFsMIOspfRRDRouteConfigContextId,
                                            UINT4 u4FsMIOspfRRDRouteDest,
                                            UINT4 *pu4NextFsMIOspfRRDRouteDest,
                                            UINT4 u4FsMIOspfRRDRouteMask,
                                            UINT4 *pu4NextFsMIOspfRRDRouteMask)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfRRDRouteConfigContextId) ==
        OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) ==
            SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFutOspfRRDRouteConfigTable (u4FsMIOspfRRDRouteDest,
                                                       pu4NextFsMIOspfRRDRouteDest,
                                                       u4FsMIOspfRRDRouteMask,
                                                       pu4NextFsMIOspfRRDRouteMask);
        UtilOspfResetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfRRDRouteConfigContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfRRDRouteConfigContextId))
               == OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfRRDRouteConfigContextId)
                == SNMP_FAILURE)
            {
                break;
            }
            i1RetVal =
                nmhGetFirstIndexFutOspfRRDRouteConfigTable
                (pu4NextFsMIOspfRRDRouteDest, pu4NextFsMIOspfRRDRouteMask);
            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfRRDRouteConfigContextId =
                *pi4NextFsMIOspfRRDRouteConfigContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfRRDRouteConfigContextId
            = i4FsMIOspfRRDRouteConfigContextId;
    }
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDRouteMetric
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                retValFsMIOspfRRDRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDRouteMetric (INT4 i4FsMIOspfRRDRouteConfigContextId,
                              UINT4 u4FsMIOspfRRDRouteDest,
                              UINT4 u4FsMIOspfRRDRouteMask,
                              INT4 *pi4RetValFsMIOspfRRDRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDRouteMetric (u4FsMIOspfRRDRouteDest,
                                            u4FsMIOspfRRDRouteMask,
                                            pi4RetValFsMIOspfRRDRouteMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDRouteMetricType
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                retValFsMIOspfRRDRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDRouteMetricType (INT4 i4FsMIOspfRRDRouteConfigContextId,
                                  UINT4 u4FsMIOspfRRDRouteDest,
                                  UINT4 u4FsMIOspfRRDRouteMask,
                                  INT4 *pi4RetValFsMIOspfRRDRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDRouteMetricType (u4FsMIOspfRRDRouteDest,
                                                u4FsMIOspfRRDRouteMask,
                                                pi4RetValFsMIOspfRRDRouteMetricType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDRouteTagType
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                retValFsMIOspfRRDRouteTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDRouteTagType (INT4 i4FsMIOspfRRDRouteConfigContextId,
                               UINT4 u4FsMIOspfRRDRouteDest,
                               UINT4 u4FsMIOspfRRDRouteMask,
                               INT4 *pi4RetValFsMIOspfRRDRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDRouteTagType (u4FsMIOspfRRDRouteDest,
                                             u4FsMIOspfRRDRouteMask,
                                             pi4RetValFsMIOspfRRDRouteTagType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDRouteTag
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                retValFsMIOspfRRDRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDRouteTag (INT4 i4FsMIOspfRRDRouteConfigContextId,
                           UINT4 u4FsMIOspfRRDRouteDest,
                           UINT4 u4FsMIOspfRRDRouteMask,
                           UINT4 *pu4RetValFsMIOspfRRDRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDRouteTag (u4FsMIOspfRRDRouteDest,
                                         u4FsMIOspfRRDRouteMask,
                                         pu4RetValFsMIOspfRRDRouteTag);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDRouteStatus
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                retValFsMIOspfRRDRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDRouteStatus (INT4 i4FsMIOspfRRDRouteConfigContextId,
                              UINT4 u4FsMIOspfRRDRouteDest,
                              UINT4 u4FsMIOspfRRDRouteMask,
                              INT4 *pi4RetValFsMIOspfRRDRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfRRDRouteStatus (u4FsMIOspfRRDRouteDest,
                                            u4FsMIOspfRRDRouteMask,
                                            pi4RetValFsMIOspfRRDRouteStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfBRRouteTable
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfBRRouteTable (INT4 *pi4FsMIOspfBRRouteContextId,
                                      UINT4 *pu4FsMIOspfBRRouteIpAddr,
                                      UINT4 *pu4FsMIOspfBRRouteIpAddrMask,
                                      UINT4 *pu4FsMIOspfBRRouteIpTos,
                                      UINT4 *pu4FsMIOspfBRRouteIpNextHop,
                                      INT4 *pi4FsMIOspfBRRouteDestType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfBRRouteContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfBRRouteContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfBRRouteContextId = *pi4FsMIOspfBRRouteContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFutOspfBRRouteTable (pu4FsMIOspfBRRouteIpAddr,
                                                 pu4FsMIOspfBRRouteIpAddrMask,
                                                 pu4FsMIOspfBRRouteIpTos,
                                                 pu4FsMIOspfBRRouteIpNextHop,
                                                 pi4FsMIOspfBRRouteDestType);

        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfBRRouteContextId,
                                 (UINT4 *) pi4FsMIOspfBRRouteContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfBRRouteTable
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                nextFsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                nextFsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                nextFsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                nextFsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                nextFsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType
                nextFsMIOspfBRRouteDestType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfBRRouteTable (INT4 i4FsMIOspfBRRouteContextId,
                                     INT4 *pi4NextFsMIOspfBRRouteContextId,
                                     UINT4 u4FsMIOspfBRRouteIpAddr,
                                     UINT4 *pu4NextFsMIOspfBRRouteIpAddr,
                                     UINT4 u4FsMIOspfBRRouteIpAddrMask,
                                     UINT4 *pu4NextFsMIOspfBRRouteIpAddrMask,
                                     UINT4 u4FsMIOspfBRRouteIpTos,
                                     UINT4 *pu4NextFsMIOspfBRRouteIpTos,
                                     UINT4 u4FsMIOspfBRRouteIpNextHop,
                                     UINT4 *pu4NextFsMIOspfBRRouteIpNextHop,
                                     INT4 i4FsMIOspfBRRouteDestType,
                                     INT4 *pi4NextFsMIOspfBRRouteDestType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfBRRouteContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexFutOspfBRRouteTable (u4FsMIOspfBRRouteIpAddr,
                                                       pu4NextFsMIOspfBRRouteIpAddr,
                                                       u4FsMIOspfBRRouteIpAddrMask,
                                                       pu4NextFsMIOspfBRRouteIpAddrMask,
                                                       u4FsMIOspfBRRouteIpTos,
                                                       pu4NextFsMIOspfBRRouteIpTos,
                                                       u4FsMIOspfBRRouteIpNextHop,
                                                       pu4NextFsMIOspfBRRouteIpNextHop,
                                                       i4FsMIOspfBRRouteDestType,
                                                       pi4NextFsMIOspfBRRouteDestType);

        UtilOspfResetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfBRRouteContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfBRRouteContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfBRRouteContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfBRRouteTable
                (pu4NextFsMIOspfBRRouteIpAddr, pu4NextFsMIOspfBRRouteIpAddrMask,
                 pu4NextFsMIOspfBRRouteIpTos, pu4NextFsMIOspfBRRouteIpNextHop,
                 pi4NextFsMIOspfBRRouteDestType);

            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfBRRouteContextId = *pi4NextFsMIOspfBRRouteContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfBRRouteContextId = i4FsMIOspfBRRouteContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfBRRouteType
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType

                The Object 
                retValFsMIOspfBRRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfBRRouteType (INT4 i4FsMIOspfBRRouteContextId,
                           UINT4 u4FsMIOspfBRRouteIpAddr,
                           UINT4 u4FsMIOspfBRRouteIpAddrMask,
                           UINT4 u4FsMIOspfBRRouteIpTos,
                           UINT4 u4FsMIOspfBRRouteIpNextHop,
                           INT4 i4FsMIOspfBRRouteDestType,
                           INT4 *pi4RetValFsMIOspfBRRouteType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfBRRouteType (u4FsMIOspfBRRouteIpAddr,
                                         u4FsMIOspfBRRouteIpAddrMask,
                                         u4FsMIOspfBRRouteIpTos,
                                         u4FsMIOspfBRRouteIpNextHop,
                                         i4FsMIOspfBRRouteDestType,
                                         pi4RetValFsMIOspfBRRouteType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfBRRouteAreaId
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType

                The Object 
                retValFsMIOspfBRRouteAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfBRRouteAreaId (INT4 i4FsMIOspfBRRouteContextId,
                             UINT4 u4FsMIOspfBRRouteIpAddr,
                             UINT4 u4FsMIOspfBRRouteIpAddrMask,
                             UINT4 u4FsMIOspfBRRouteIpTos,
                             UINT4 u4FsMIOspfBRRouteIpNextHop,
                             INT4 i4FsMIOspfBRRouteDestType,
                             UINT4 *pu4RetValFsMIOspfBRRouteAreaId)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfBRRouteAreaId (u4FsMIOspfBRRouteIpAddr,
                                           u4FsMIOspfBRRouteIpAddrMask,
                                           u4FsMIOspfBRRouteIpTos,
                                           u4FsMIOspfBRRouteIpNextHop,
                                           i4FsMIOspfBRRouteDestType,
                                           pu4RetValFsMIOspfBRRouteAreaId);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfBRRouteCost
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType

                The Object 
                retValFsMIOspfBRRouteCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfBRRouteCost (INT4 i4FsMIOspfBRRouteContextId,
                           UINT4 u4FsMIOspfBRRouteIpAddr,
                           UINT4 u4FsMIOspfBRRouteIpAddrMask,
                           UINT4 u4FsMIOspfBRRouteIpTos,
                           UINT4 u4FsMIOspfBRRouteIpNextHop,
                           INT4 i4FsMIOspfBRRouteDestType,
                           INT4 *pi4RetValFsMIOspfBRRouteCost)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfBRRouteCost (u4FsMIOspfBRRouteIpAddr,
                                         u4FsMIOspfBRRouteIpAddrMask,
                                         u4FsMIOspfBRRouteIpTos,
                                         u4FsMIOspfBRRouteIpNextHop,
                                         i4FsMIOspfBRRouteDestType,
                                         pi4RetValFsMIOspfBRRouteCost);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfBRRouteInterfaceIndex
 Input       :  The Indices
                FsMIOspfBRRouteContextId
                FsMIOspfBRRouteIpAddr
                FsMIOspfBRRouteIpAddrMask
                FsMIOspfBRRouteIpTos
                FsMIOspfBRRouteIpNextHop
                FsMIOspfBRRouteDestType

                The Object 
                retValFsMIOspfBRRouteInterfaceIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfBRRouteInterfaceIndex (INT4 i4FsMIOspfBRRouteContextId,
                                     UINT4 u4FsMIOspfBRRouteIpAddr,
                                     UINT4 u4FsMIOspfBRRouteIpAddrMask,
                                     UINT4 u4FsMIOspfBRRouteIpTos,
                                     UINT4 u4FsMIOspfBRRouteIpNextHop,
                                     INT4 i4FsMIOspfBRRouteDestType,
                                     INT4
                                     *pi4RetValFsMIOspfBRRouteInterfaceIndex)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfBRRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfBRRouteInterfaceIndex (u4FsMIOspfBRRouteIpAddr,
                                                   u4FsMIOspfBRRouteIpAddrMask,
                                                   u4FsMIOspfBRRouteIpTos,
                                                   u4FsMIOspfBRRouteIpNextHop,
                                                   i4FsMIOspfBRRouteDestType,
                                                   pi4RetValFsMIOspfBRRouteInterfaceIndex);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfExtRouteTable
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfExtRouteTable (INT4 *pi4FsMIOspfExtRouteContextId,
                                       UINT4 *pu4FsMIOspfExtRouteDest,
                                       UINT4 *pu4FsMIOspfExtRouteMask,
                                       INT4 *pi4FsMIOspfExtRouteTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfExtRouteContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIOspfExtRouteContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfExtRouteContextId = *pi4FsMIOspfExtRouteContextId;

        if (UtilOspfSetContext (*pi4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFutOspfExtRouteTable (pu4FsMIOspfExtRouteDest,
                                                  pu4FsMIOspfExtRouteMask,
                                                  pi4FsMIOspfExtRouteTOS);

        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfExtRouteContextId,
                                 (UINT4 *) pi4FsMIOspfExtRouteContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfExtRouteTable
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                nextFsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                nextFsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                nextFsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS
                nextFsMIOspfExtRouteTOS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfExtRouteTable (INT4 i4FsMIOspfExtRouteContextId,
                                      INT4 *pi4NextFsMIOspfExtRouteContextId,
                                      UINT4 u4FsMIOspfExtRouteDest,
                                      UINT4 *pu4NextFsMIOspfExtRouteDest,
                                      UINT4 u4FsMIOspfExtRouteMask,
                                      UINT4 *pu4NextFsMIOspfExtRouteMask,
                                      INT4 i4FsMIOspfExtRouteTOS,
                                      INT4 *pi4NextFsMIOspfExtRouteTOS)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIOspfExtRouteContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexFutOspfExtRouteTable (u4FsMIOspfExtRouteDest,
                                                        pu4NextFsMIOspfExtRouteDest,
                                                        u4FsMIOspfExtRouteMask,
                                                        pu4NextFsMIOspfExtRouteMask,
                                                        i4FsMIOspfExtRouteTOS,
                                                        pi4NextFsMIOspfExtRouteTOS);

        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfExtRouteContextId,
                                      (UINT4 *)
                                      pi4NextFsMIOspfExtRouteContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIOspfExtRouteContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfExtRouteTable
                (pu4NextFsMIOspfExtRouteDest, pu4NextFsMIOspfExtRouteMask,
                 pi4NextFsMIOspfExtRouteTOS);

            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIOspfExtRouteContextId = *pi4NextFsMIOspfExtRouteContextId;
        }
    }
    else
    {
        *pi4NextFsMIOspfExtRouteContextId = i4FsMIOspfExtRouteContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteMetric
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteMetric (INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              INT4 *pi4RetValFsMIOspfExtRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteMetric (u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            pi4RetValFsMIOspfExtRouteMetric);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteMetricType
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteMetricType (INT4 i4FsMIOspfExtRouteContextId,
                                  UINT4 u4FsMIOspfExtRouteDest,
                                  UINT4 u4FsMIOspfExtRouteMask,
                                  INT4 i4FsMIOspfExtRouteTOS,
                                  INT4 *pi4RetValFsMIOspfExtRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteMetricType (u4FsMIOspfExtRouteDest,
                                                u4FsMIOspfExtRouteMask,
                                                i4FsMIOspfExtRouteTOS,
                                                pi4RetValFsMIOspfExtRouteMetricType);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteTag
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteTag (INT4 i4FsMIOspfExtRouteContextId,
                           UINT4 u4FsMIOspfExtRouteDest,
                           UINT4 u4FsMIOspfExtRouteMask,
                           INT4 i4FsMIOspfExtRouteTOS,
                           INT4 *pi4RetValFsMIOspfExtRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteTag (u4FsMIOspfExtRouteDest,
                                         u4FsMIOspfExtRouteMask,
                                         i4FsMIOspfExtRouteTOS,
                                         pi4RetValFsMIOspfExtRouteTag);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteFwdAdr
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteFwdAdr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteFwdAdr (INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              UINT4 *pu4RetValFsMIOspfExtRouteFwdAdr)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteFwdAdr (u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            pu4RetValFsMIOspfExtRouteFwdAdr);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteIfIndex
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteIfIndex (INT4 i4FsMIOspfExtRouteContextId,
                               UINT4 u4FsMIOspfExtRouteDest,
                               UINT4 u4FsMIOspfExtRouteMask,
                               INT4 i4FsMIOspfExtRouteTOS,
                               INT4 *pi4RetValFsMIOspfExtRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteIfIndex (u4FsMIOspfExtRouteDest,
                                             u4FsMIOspfExtRouteMask,
                                             i4FsMIOspfExtRouteTOS,
                                             pi4RetValFsMIOspfExtRouteIfIndex);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteNextHop
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteNextHop (INT4 i4FsMIOspfExtRouteContextId,
                               UINT4 u4FsMIOspfExtRouteDest,
                               UINT4 u4FsMIOspfExtRouteMask,
                               INT4 i4FsMIOspfExtRouteTOS,
                               UINT4 *pu4RetValFsMIOspfExtRouteNextHop)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteNextHop (u4FsMIOspfExtRouteDest,
                                             u4FsMIOspfExtRouteMask,
                                             i4FsMIOspfExtRouteTOS,
                                             pu4RetValFsMIOspfExtRouteNextHop);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfExtRouteStatus
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                retValFsMIOspfExtRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfExtRouteStatus (INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              INT4 *pi4RetValFsMIOspfExtRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfExtRouteStatus (u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            pi4RetValFsMIOspfExtRouteStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfVirtNbrTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfVirtNbrTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtNbrArea
                FsMIOspfVirtNbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMIOspfVirtNbrTable (INT4 *pi4FsMIStdOspfContextId,
                                      UINT4 *pu4FsMIStdOspfVirtNbrArea,
                                      UINT4 *pu4FsMIStdOspfVirtNbrRtrId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfCxtId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIOspfCxtId = *pi4FsMIStdOspfContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFutOspfVirtNbrTable (pu4FsMIStdOspfVirtNbrArea,
                                                 pu4FsMIStdOspfVirtNbrRtrId);

        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfCxtId,
                                 (UINT4 *) pi4FsMIStdOspfContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfVirtNbrTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
                FsMIOspfVirtNbrArea
                nextFsMIOspfVirtNbrArea
                FsMIOspfVirtNbrRtrId
                nextFsMIOspfVirtNbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfVirtNbrTable (INT4 i4FsMIStdOspfContextId,
                                     INT4 *pi4NextFsMIStdOspfContextId,
                                     UINT4 u4FsMIStdOspfVirtNbrArea,
                                     UINT4 *pu4NextFsMIStdOspfVirtNbrArea,
                                     UINT4 u4FsMIStdOspfVirtNbrRtrId,
                                     UINT4 *pu4NextFsMIStdOspfVirtNbrRtrId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_SUCCESS)
    {
        if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexFutOspfVirtNbrTable
            (u4FsMIStdOspfVirtNbrArea,
             pu4NextFsMIStdOspfVirtNbrArea,
             u4FsMIStdOspfVirtNbrRtrId, pu4NextFsMIStdOspfVirtNbrRtrId);

        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfContextId)
                == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfVirtNbrTable
                (pu4NextFsMIStdOspfVirtNbrArea, pu4NextFsMIStdOspfVirtNbrRtrId);

            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfContextId = *pi4NextFsMIStdOspfContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfContextId = i4FsMIStdOspfContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtNbrRestartHelperStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtNbrArea
                FsMIOspfVirtNbrRtrId

                The Object
                retValFsMIOspfVirtNbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIOspfVirtNbrRestartHelperStatus
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIStdOspfVirtNbrArea,
     UINT4 u4FsMIStdOspfVirtNbrRtrId,
     INT4 *pi4RetValFsMIOspfVirtNbrRestartHelperStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfVirtNbrRestartHelperStatus
        (u4FsMIStdOspfVirtNbrArea,
         u4FsMIStdOspfVirtNbrRtrId,
         pi4RetValFsMIOspfVirtNbrRestartHelperStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtNbrRestartHelperAge
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtNbrArea
                FsMIOspfVirtNbrRtrId

                The Object
                retValFsMIOspfVirtNbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIOspfVirtNbrRestartHelperAge
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIStdOspfVirtNbrArea,
     UINT4 u4FsMIStdOspfVirtNbrRtrId,
     UINT4 *pu4RetValFsMIOspfVirtNbrRestartHelperAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfVirtNbrRestartHelperAge
        (u4FsMIStdOspfVirtNbrArea,
         u4FsMIStdOspfVirtNbrRtrId, pu4RetValFsMIOspfVirtNbrRestartHelperAge);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfVirtNbrRestartHelperExitReason
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfVirtNbrArea
                FsMIOspfVirtNbrRtrId

                The Object
                retValFsMIOspfVirtNbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIOspfVirtNbrRestartHelperExitReason
    (INT4 i4FsMIStdOspfContextId,
     UINT4 u4FsMIStdOspfVirtNbrArea,
     UINT4 u4FsMIStdOspfVirtNbrRtrId,
     INT4 *pi4RetValFsMIOspfVirtNbrRestartHelperExitReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfVirtNbrRestartHelperExitReason
        (u4FsMIStdOspfVirtNbrArea,
         u4FsMIStdOspfVirtNbrRtrId,
         pi4RetValFsMIOspfVirtNbrRestartHelperExitReason);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfGrTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfGrTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfGrTable (INT4 *pi4FsMIStdOspfContextId)
{
    return nmhGetFirstIndexFsMIStdOspfTable (pi4FsMIStdOspfContextId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfGrTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfGrTable (INT4 i4FsMIStdOspfContextId,
                                INT4 *pi4NextFsMIStdOspfContextId)
{
    return nmhGetNextIndexFsMIStdOspfTable (i4FsMIStdOspfContextId,
                                            pi4NextFsMIStdOspfContextId);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfGrShutdown
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                retValFsMIOspfGrShutdown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfGrShutdown (INT4 i4FsMIStdOspfContextId,
                          INT4 *pi4RetValFsMIOspfGrShutdown)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfGrShutdown (pi4RetValFsMIOspfGrShutdown);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfDistInOutRouteMapTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfDistInOutRouteMapTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfDistInOutRouteMapTable (INT4 *pi4FsMIStdOspfContextId,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIOspfDistInOutRouteMapName,
                                                INT4
                                                *pi4FsMIOspfDistInOutRouteMapType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdOspfAreaContextId;

    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIStdOspfAreaContextId = *pi4FsMIStdOspfContextId;

        if (UtilOspfSetContext (*pi4FsMIStdOspfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFutOspfDistInOutRouteMapTable
            (pFsMIOspfDistInOutRouteMapName, pi4FsMIOspfDistInOutRouteMapType);

        UtilOspfResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfAreaContextId,
                                 (UINT4 *) pi4FsMIStdOspfContextId) !=
           OSPF_FAILURE);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfDistInOutRouteMapTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                nextFsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType
                nextFsMIOspfDistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfDistInOutRouteMapTable (INT4 i4FsMIStdOspfContextId,
                                               INT4
                                               *pi4NextFsMIStdOspfContextId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsMIOspfDistInOutRouteMapName,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsMIOspfDistInOutRouteMapName,
                                               INT4
                                               i4FsMIOspfDistInOutRouteMapType,
                                               INT4
                                               *pi4NextFsMIOspfDistInOutRouteMapType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_SUCCESS)
    {

        if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexFutOspfDistInOutRouteMapTable
            (pFsMIOspfDistInOutRouteMapName,
             pNextFsMIOspfDistInOutRouteMapName,
             i4FsMIOspfDistInOutRouteMapType,
             pi4NextFsMIOspfDistInOutRouteMapType);

        UtilOspfResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                                      (UINT4 *)
                                      pi4NextFsMIStdOspfContextId)) ==
               OSPF_SUCCESS)
        {
            if (UtilOspfSetContext (*pi4NextFsMIStdOspfContextId) ==
                SNMP_FAILURE)
            {
                break;
            }

            i1RetVal = nmhGetFirstIndexFutOspfDistInOutRouteMapTable
                (pNextFsMIOspfDistInOutRouteMapName,
                 pi4NextFsMIOspfDistInOutRouteMapType);

            UtilOspfResetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfContextId = *pi4NextFsMIStdOspfContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfContextId = i4FsMIStdOspfContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfDistInOutRouteMapValue
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType

                The Object 
                retValFsMIOspfDistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfDistInOutRouteMapValue (INT4 i4FsMIStdOspfContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIOspfDistInOutRouteMapName,
                                      INT4 i4FsMIOspfDistInOutRouteMapType,
                                      INT4
                                      *pi4RetValFsMIOspfDistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfDistInOutRouteMapValue
        (pFsMIOspfDistInOutRouteMapName,
         i4FsMIOspfDistInOutRouteMapType,
         pi4RetValFsMIOspfDistInOutRouteMapValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType

                The Object 
                retValFsMIOspfDistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfDistInOutRouteMapRowStatus (INT4 i4FsMIStdOspfContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIOspfDistInOutRouteMapName,
                                          INT4 i4FsMIOspfDistInOutRouteMapType,
                                          INT4
                                          *pi4RetValFsMIOspfDistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfDistInOutRouteMapRowStatus
        (pFsMIOspfDistInOutRouteMapName,
         i4FsMIOspfDistInOutRouteMapType,
         pi4RetValFsMIOspfDistInOutRouteMapRowStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfPreferenceTable
 Input       :  The Indices
                FsMIStdOspfContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfPreferenceTable (INT4 *pi4FsMIStdOspfContextId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfPreferenceTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfPreferenceTable (INT4 i4FsMIStdOspfContextId,
                                        INT4 *pi4NextFsMIStdOspfContextId)
{
    if (UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                              (UINT4 *) pi4NextFsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfPreferenceValue
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                retValFsMIOspfPreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfPreferenceValue (INT4 i4FsMIStdOspfContextId,
                               INT4 *pi4RetValFsMIOspfPreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfPreferenceValue (pi4RetValFsMIOspfPreferenceValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfRRDMetricTable
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfRRDMetricTable (INT4 *pi4FsMIStdOspfContextId,
                                        INT4 *pi4FsMIOspfRRDProtocolId)
{
    if (UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfContextId) ==
        OSPF_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4FsMIOspfRRDProtocolId = REDISTRUTE_ARR_BGP;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfRRDMetricTable
 Input       :  The Indices
                FsMIStdOspfContextId
                nextFsMIStdOspfContextId
                FsMIOspfRRDProtocolId
                nextFsMIOspfRRDProtocolId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfRRDMetricTable (INT4 i4FsMIStdOspfContextId,
                                       INT4 *pi4NextFsMIStdOspfContextId,
                                       INT4 i4FsMIOspfRRDProtocolId,
                                       INT4 *pi4NextFsMIOspfRRDProtocolId)
{
    INT4                i4Count = i4FsMIOspfRRDProtocolId;
    INT4                i4ProtocolId = -1;

    if (UtilOspfIsValidCxtId (i4FsMIStdOspfContextId) == OSPF_SUCCESS)
    {
        if (i4Count < MAX_PROTO_REDISTRUTE_SIZE)
        {
            i4ProtocolId = i4Count + 1;
            *pi4NextFsMIOspfRRDProtocolId = i4ProtocolId;
            return SNMP_SUCCESS;
        }
    }
    while ((UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfContextId,
                                  (UINT4 *)
                                  pi4NextFsMIStdOspfContextId)) == OSPF_SUCCESS)
    {
        *pi4NextFsMIOspfRRDProtocolId = REDISTRUTE_ARR_BGP;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDMetricValue
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId

                The Object 
                retValFsMIOspfRRDMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDMetricValue (INT4 i4FsMIStdOspfContextId,
                              INT4 i4FsMIOspfRRDProtocolId,
                              INT4 *pi4RetValFsMIOspfRRDMetricValue)
{
    tOspfCxt           *pOspfCxt = NULL;
    if ((pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIOspfRRDMetricValue =
        pOspfCxt->au4MetricValue[i4FsMIOspfRRDProtocolId - 1];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfRRDMetricType
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId

                The Object 
                retValFsMIOspfRRDMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfRRDMetricType (INT4 i4FsMIStdOspfContextId,
                             INT4 i4FsMIOspfRRDProtocolId,
                             INT4 *pi4RetValFsMIOspfRRDMetricType)
{
    tOspfCxt           *pOspfCxt = NULL;
    if ((pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMIOspfRRDMetricType =
        pOspfCxt->au1MetricType[i4FsMIOspfRRDProtocolId - 1];
    return SNMP_SUCCESS;
}
