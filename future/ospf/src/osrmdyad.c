/************************************************************/
/*$Id: osrmdyad.c,v 1.5 2014/12/30 12:42:19 siva Exp $   */
/*                                                          */
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved     */
/*                                                          */
/*  FILE NAME             : osreddyn.c                      */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : OSPF                            */
/*  LANGUAGE              : C                               */
/*  FUNCTIONS DEFINED     : OspfRmProcessIfState           */
/*                          OspfRmProcessNbrStateChange,   */
/*                          OspfRmSendHello,               */
/*                          OspfRmSendNbrStateChange,      */
/*  DESCRIPTION           : This file contains definitions  */
/*                          related to dynamic update for   */
/*                          High Availability               */
/************************************************************/
/*                                                          */
/*  Change History                                          */
/*  Version               :                                 */
/*  Date(DD/MM/YYYY)      :                                 */
/*  Modified by           :                                 */
/*  Description of change :                                 */
/************************************************************/

#ifndef _OSREDDYN_
#define _OSREDDYN_

#include "osinc.h"

/***************************************************************/
/*  Function Name   :OspfRmSendNbrStateChange                 */
/*  Description     :This functions is invoked from Nsm2wayRcvd*/
/*                   or NsmRestartAdj or NsmExchgDone or       */
/*                   NsmLdngDone or NsmLlDown. This function   */
/*                   will fill the CRU buff with context Id,   */
/*                   neighbor Id, and event and sends the      */
/*                   message to standby by calling             */
/*                   OspfSendMsgToRm                           */
/*  Input(s)        :u4OspfCxtId  - Context Id                 */
/*                                                             */
/*                   nbrId        - neighbor Id                */
/*                                                             */
/*                   u1Event      - Event to identify the      */
/*                                  change in neighbor         */
/*                                                             */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :None                                      */
/***************************************************************/
VOID
OspfRmSendNbrStateChange (UINT4 u4OspfCxtId,
                          tNeighbor * pNbr, UINT1 u1Event, UINT4 u4GracePeriod)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ZERO;
    UINT2               u2MsgLen = OSPF_RED_SYN_NBR_STATE_SIZE +
        OSPF_RED_DYN_SYN_HDR_SIZE;

    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }

    /* Check If standby exist if not return */
    if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "No Standby is existing ");
        return;
    }

    if (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
    {
        u2MsgLen += MAX_IP_ADDR_LEN;
    }
    if (u1Event == OSPF_RED_SYNC_NBR_STATE_HELPER)
    {
        /* For storing the grace period */
        u2MsgLen += sizeof (UINT4);
    }

    /* Allocate the CRU buffer */
    if ((pMsg = UtilOsMsgAlloc (u2MsgLen)) == NULL)
    {

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Nbr State change Syn Alloc \n");
        return;
    }

    /* Construct the CRU buffer */

    /* Fill the Sub Message  Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset,
                                OSPF_RED_SYNC_NBR_STATE_CHNG_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Fill the Message  Length */
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pMsg, u4Offset, u2MsgLen);
    u4Offset = u4Offset + sizeof (UINT2);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, u1Event);
    u4Offset = u4Offset + sizeof (UINT1);
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, u4OspfCxtId);
    u4Offset = u4Offset + sizeof (UINT4);
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pNbr->pInterface->u4AddrlessIf);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->pInterface->ifIpAddr,
                                u4Offset, MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pNbr->nbrOptions);
    u4Offset = u4Offset + sizeof (UINT1);

    if ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->nbrIpAddr, u4Offset,
                                    MAX_IP_ADDR_LEN);
    }
    else
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->nbrId, u4Offset,
                                    MAX_IP_ADDR_LEN);
    }

    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset,
                                pNbr->pInterface->u1NetworkType);
    u4Offset = u4Offset + sizeof (UINT1);

    if (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->pInterface->transitAreaId,
                                    u4Offset, MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    }

    if (u1Event == OSPF_RED_SYNC_NBR_STATE_HELPER)
    {
        OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, u4GracePeriod);
        u4Offset = u4Offset + sizeof (UINT4);
    }

    /* Call RM Interface to send the Message */
    OspfRmSendMsgToRm (pMsg, u2MsgLen);
    return;
}

/***************************************************************/
/*  Function Name   :OspfRmProcessNbrStateChange              */
/*  Description     :This function is invoked at standby node  */
/*                   when it gets Neighbor state change event  */
/*                   from active through RM. This function is  */
/*                   invoked from from OspfRmHandleRMMessage. */
/*                   This function will do the following       */
/*                    1.Find out the Interface using contextId,*/
/*                        interface index                      */
/*                    2.Find out the nbr using interface,      */
/*                        nbr Ipaddress, and nbr Id            */
/*                    3.call Nsm2wayRcvd (tNeighbor * pNbr)    */
/*                        if the message type is               */
/*                        OSPF_SYNC_NBR_STATE_TWO_WAY_RECVD    */
/*                    4.call NsmRestartAdj (tNeighbor * pNbr)  */
/*                        if the message type is               */
/*                        OSPF_SYNC_NBR_STATE_RESTART_ADJ      */
/*                    5.call NbrUpdateState if the message type*/
/*                        is OSPF_SYNC_NBR_STATE_FULL          */
/*                    6.call NsmLlDown (tNeighbor * pNbr) if   */
/*                        the message type is                  */
/*                        OSPF_SYNC_NBR_STATE_DOWN.            */
/*                    7.update the pNbr->u1NbrHelperStatus and */
/*                        start the pNbr->helperGraceTimer     */
/*                        for the grace period if the message  */
/*                        type is OSPF_SYNC_NBR_STATE_HELPER   */
/*  Input(s)        :pRmMesg - The message received from RM    */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         : NONE                                     */
/***************************************************************/
VOID
OspfRmProcessNbrStateChange (tRmMsg * pRmMesg)
{
    tIPADDR             nbrIpAddr;
    tIPADDR             IfIpAddr;
    tOspfCxt           *pOspfCxt = NULL;
    tNeighbor          *pNbr = NULL;
    tInterface         *pInterface = NULL;
    tAreaId             transitAreaId;
    tOPTIONS            nbrOptions;
    UINT4               u4ContextId = OSPF_INVALID_CXT_ID;
    UINT4               u4IfAddlessIf = 0;
    UINT4               u4GracePeriod = ZERO;
    UINT1               u1Event = ZERO;
    UINT4               u4Offset = OSPF_RED_MSG_HDR_SIZE;
    UINT1               u1NetworkType = 0;

    if (gOsRtr.osRedInfo.u4OsRmState != OSPF_RED_STANDBY)
    {
        /* Only Standby node can process sync messages */
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is active");
        return;
    }

    /* Parse Context Id, Neighbor Id, and event from CRU Buffer */
    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, u1Event);
    u4Offset += sizeof (UINT1);

    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4ContextId);
    u4Offset += sizeof (UINT4);

    OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4IfAddlessIf);
    u4Offset += sizeof (UINT4);

    OSPF_CRU_BMC_GET_STRING (pRmMesg, IfIpAddr, u4Offset, MAX_IP_ADDR_LEN)
        u4Offset += MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, nbrOptions);
    u4Offset += sizeof (UINT1);
    OSPF_CRU_BMC_GET_STRING (pRmMesg, nbrIpAddr, u4Offset, MAX_IP_ADDR_LEN)
        u4Offset += MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_GET_1_BYTE (pRmMesg, u4Offset, u1NetworkType);
    u4Offset += sizeof (UINT1);

    if (u1NetworkType == IF_VIRTUAL)
    {
        OSPF_CRU_BMC_GET_STRING (pRmMesg, transitAreaId,
                                 u4Offset, MAX_IP_ADDR_LEN);
        u4Offset += MAX_IP_ADDR_LEN;
    }

    if (u1Event == OSPF_RED_SYNC_NBR_STATE_HELPER)
    {
        OSPF_CRU_BMC_GET_4_BYTE (pRmMesg, u4Offset, u4GracePeriod);
        u4Offset += sizeof (UINT4);
    }

    /* Get the neighbor structure using the context Id and Neighbor Id */
    if ((pOspfCxt = UtilOspfGetCxt (u4ContextId)) == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_INTERFACE_TRC, u4ContextId,
                  "Process Nbr State Sync, Associated Ospf Context "
                  "Not Found\n");
        return;
    }
    if (u1NetworkType == IF_VIRTUAL)
    {
        pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrIpAddr);
        if (pInterface == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Interface Not Found \n");
            return;
        }
    }
    else
    {

        if ((pInterface = GetFindIfInCxt (pOspfCxt, IfIpAddr,
                                          u4IfAddlessIf)) == NULL)
        {
            OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                              "Interface Not Found \n");
            return;

        }
    }

    /* Get the Nbr from the global Nbr List */
    pNbr = HpSearchNbrLst (&nbrIpAddr, pInterface);

    if (pNbr != NULL)
    {
        /* update the neighbor options */
        pNbr->nbrOptions = nbrOptions;

        switch (u1Event)
        {
            case OSPF_RED_SYNC_NBR_STATE_TWO_WAY_RECVD:
                Nsm2wayRcvd (pNbr);
                break;
            case OSPF_RED_SYNC_NBR_STATE_RESTART_ADJ:
                NsmRestartAdj (pNbr);
                break;
            case OSPF_RED_SYNC_NBR_STATE_FULL:
                NbrUpdateState (pNbr, NBRS_FULL);
                RtcSetRtTimer (pOspfCxt);
                break;
            case OSPF_RED_SYNC_NBR_STATE_DOWN:
                NsmDown (pNbr);
                break;
            case OSPF_RED_SYNC_NBR_STATE_LL_DOWN:
                NsmLlDown (pNbr);
                break;
            case OSPF_RED_SYNC_NBR_STATE_HELPER:
                pNbr->u1NbrHelperStatus = OSPF_GR_HELPING;
                /* Update u1GRSupportFlag in pInterface, this 
                 * flag is required in helper mode to know 
                 * atlease on GR router present in this 
                 * interface as a neighbor */
                pNbr->pInterface->u1GRSupportFlag = OSPF_TRUE;

                /* Start the Grace Timer for GR Neighbor */
                TmrSetTimer (&(pNbr->helperGraceTimer),
                             HELPER_GRACE_TIMER,
                             (NO_OF_TICKS_PER_SEC * u4GracePeriod));
                break;
            default:
                OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                                  OSPF_INVALID_CXT_ID,
                                  "Invalid State Change Event \n");
                break;
        }
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Neighbor state is successfully updated ");
        return;
    }
    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                      "Failed to update the neighbor state");
    return;
}

/***************************************************************/
/*  Function Name   :OspfRmSendHello                          */
/*  Description     :This function will be invoked from        */
/*                   HpRcvHello  in active node when it gets   */
/*                   the hello packet and it finds that the    */
/*                   hello packet is latest than the last sent */
/*                   hello then it will store this hello in    */
/*                   last received hello and send this hello   */
/*                   to standby by calling OspfSendMsgToRm     */
/*  Input(s)        :pHelloPkt  - The received Hello Packet    */
/*                                                             */
/*                   u2PktLen   - The hello packet length      */
/*                                                             */
/*                   pInterface - The interface on which       */
/*                                the hello is received        */
/*                                                             */
/*                   pSrcIpAddr - The source Ip address        */
/*                                                             */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :None                                      */
/***************************************************************/
VOID
OspfRmSendHello (tCRU_BUF_CHAIN_HEADER * pHelloPkt,
                 UINT2 u2PktLen,
                 tInterface * pInterface,
                 tNeighbor * pNbr, tIPADDR * pSrcIpAddr)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = OSPF_ZERO;
    UINT2               u2OldChkSum = OSPF_ZERO;
    UINT2               u2NewChkSum = OSPF_ZERO;
    UINT2               u2RmMsgLength = (UINT2) (OSPF_RED_SYN_HELLO_HDR_SIZE +
                                                 OSPF_RED_DYN_SYN_HDR_SIZE +
                                                 OSPF_RED_NBR_CNT_SIZE);
    /* Get the Check sum from the recvd Hello Packet */
    OSPF_CRU_BMC_GET_2_BYTE (pHelloPkt, CHKSUM_OFFSET_IN_HEADER, u2NewChkSum);
    /* Get the Check sum from the last stored hello Packet */
    OSPF_BUFFER_EXTRACT_2_BYTE (&
                                (pNbr->lastRcvHello.
                                 au1Pkt[VERSION_NO_OFFSET_IN_HEADER]),
                                CHKSUM_OFFSET_IN_HEADER, u2OldChkSum);

    if ((pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen == u2PktLen) &&
        (u2NewChkSum == u2OldChkSum))
    {
        /* no change in hello packet contents so return */
        return;
    }

    /* Store the received hello packet information */
    MEMSET (&(pNbr->lastRcvHello.au1Pkt), 0, MAX_HELLO_SIZE);
    OSPF_CRU_BMC_GET_STRING (pHelloPkt, pNbr->lastRcvHello.au1Pkt,
                             VERSION_NO_OFFSET_IN_HEADER, u2PktLen);
    pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen = u2PktLen;
    IP_ADDR_COPY (pNbr->lastRcvHello.helloPktHdr.SrcIpAddr, *pSrcIpAddr);
    IP_ADDR_COPY (pNbr->lastRcvHello.helloPktHdr.IfIpAddr,
                  pInterface->ifIpAddr);
    pNbr->lastRcvHello.helloPktHdr.u4AddrlessIf = pInterface->u4AddrlessIf;
    pNbr->lastRcvHello.helloPktHdr.u4ContextId =
        pInterface->pArea->pOspfCxt->u4OspfCxtId;

    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }

    /* Check If istandby exist if not return */
    if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "No Standby is existing ");
        return;
    }

    /* Construct the CRU Buffer */
    /* 4 is to fill the neighbor count */
    u2RmMsgLength = (UINT2) (u2RmMsgLength + u2PktLen);
    if ((pMsg = UtilOsMsgAlloc (u2RmMsgLength)) == NULL)
    {

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                          OSPF_INVALID_CXT_ID, "Hello Syn Alloc Failed \n");
        return;
    }
    /* Fill the Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, OSPF_RED_SYNC_HELLO_PDU_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Fill the Message  Length */
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);

    OspfRmConstructHello (pMsg, &u4Offset, pNbr);
    /* Call RM Interface to send the Message */
    OspfRmSendMsgToRm (pMsg, u2RmMsgLength);
    gOsRtr.osRedInfo.u4HelloSyncCount++;

    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                      OSPF_INVALID_CXT_ID, "Hello Syn sent to Standby \n");
    return;
}

/***************************************************************/
/*  Function Name   :OspfRmSendIfState                         */
/*  Description     :This function will be invoked in active   */
/*                   node when interface state is changed,     */
/*                   DR and BDR on the interface is changed    */
/*  Input(s)        :                                          */
/*                   pInterface - The interface for which      */
/*                                the interface state is sent  */
/*                   u4BitMask  - Bit Mask to identify whether */
/*                                interface needs to be deleted*/
/*                                                             */
/*  Output(s)       :None                                      */
/*  Returns         :None                                      */
/***************************************************************/
VOID
OspfRmSendIfState (tInterface * pInterface, UINT4 u4BitMask)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = OSPF_ZERO;
    UINT2               u2RmMsgLength = (UINT2) (OSPF_RED_SYN_IF_STATE_SIZE +
                                                 OSPF_RED_DYN_SYN_HDR_SIZE);

    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }

    /* Check If istandby exist if not return */
    if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "No Standby is existing ");
        return;
    }

    /* Construct the CRU Buffer */
    if ((pMsg = UtilOsMsgAlloc (u2RmMsgLength)) == NULL)
    {

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                          OSPF_INVALID_CXT_ID,
                          "Interface State Syn Alloc Failed \n");
        return;
    }
    /* Fill the Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, OSPF_RED_SYNC_IFSTATE_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Fill the Message  Length */
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);

    OspfRmConstructIfState (pMsg, &u4Offset, pInterface, u4BitMask);
    /* Call RM Interface to send the Message */

    OspfRmSendMsgToRm (pMsg, u2RmMsgLength);

    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                      OSPF_INVALID_CXT_ID,
                      "Interface state sync to Standby \n");
    return;
}



/***************************************************************/
/*  Function Name   :OspfRmSendMd5SeqNo                         */
/*  Description     :This function will be invoked in active    */
/*                   node when md5 sequence number is changed   */
/*  Input(s)        :                                           */
/*                   u4CryptSeqNum - md5 seq no of the interface*/ 
/*                   pNbr      -                                */
/*                                                              */
/*                                                              */
/*  Output(s)       :None                                       */
/*  Returns         :None                                       */
/***************************************************************/
VOID
OspfRmSendMd5SeqNo (tInterface * pInterface)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = OSPF_ZERO;
    UINT2               u2RmMsgLength = (UINT2) (OSPF_RED_DYN_SYN_SEQNO_SIZE +
		                                 OSPF_RED_DYN_SYN_HDR_SIZE);
                                                  
    /* Check If the node is active if not return */
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Node is Standby");
        return;
    }
    /* Check If istandby exist if not return */
    if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
    {
        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "No Standby is existing ");
        return;
    }

    /* Construct the CRU Buffer */
    if ((pMsg = UtilOsMsgAlloc (u2RmMsgLength)) == NULL)
    {

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                          OSPF_INVALID_CXT_ID,
                          "Interface State Syn Alloc Failed \n");
        return;
    }
    /* Fill the Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset,OSPF_RED_SYNC_MD5_SEQ_NO_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Fill the Message  Length */
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);

    /*fill the context id */
     OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                 pInterface->pArea->pOspfCxt->u4OspfCxtId);         
		    

     u4Offset = u4Offset + sizeof (UINT4);
   
     /*fill the Ip address */
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
		                 pInterface->u4AddrlessIf);
                               
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg,
		                pInterface->ifIpAddr,  
                                u4Offset, MAX_IP_ADDR_LEN);

    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    
   /*fill the md5 seq no */
     OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                 pInterface->u4CryptSeqNum);

     u4Offset = u4Offset + sizeof (UINT4);

     /* Call RM Interface to send the Message */
    OspfRmSendMsgToRm (pMsg, u2RmMsgLength);

    OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC,
                      OSPF_INVALID_CXT_ID,
                      "Interface Md5 seq no sent \n");
    return;
}
#endif /* _OSREDDYN_ */
