/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ossz.c,v 1.6 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _OSPFSZ_C
#include "osinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
OspfSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSPF_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsOSPFSizingParams[i4SizingId].u4StructSize,
                              FsOSPFSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(OSPFMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            OspfSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
OspfSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsOSPFSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, OSPFMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
OspfSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSPF_MAX_SIZING_ID; i4SizingId++)
    {
        if (OSPFMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (OSPFMemPoolIds[i4SizingId]);
            OSPFMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
