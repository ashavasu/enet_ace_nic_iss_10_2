/*$Id: osrbutl.c,v 1.6 2011/12/05 14:19:36 siva Exp $*/
#include "osinc.h"
#ifdef HIGH_PERF_RXMT_LST_WANTED
PRIVATE INT4        RbRxmtNodeCompareLsa
PROTO ((tLsaInfo * pFstLsaInfo, tLsaInfo * pScndLsaInfo));
PRIVATE INT4        RbRxmtNodeCompareNbr
PROTO ((tNeighbor * pFstNbr, tNeighbor * pScndNbr));
#endif
/*****************************************************************************/
/* Function     : RbCompareLsa                                               */
/*                                                                           */
/* Description  : Compare function for the LSAs                              */
/*                                                                           */
/* Input        : e1        Pointer to LsaInfo node1                         */
/*                e2        Pointer to LsaInfo node2                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPF_EQUAL if keys of both the elements are same.       */
/*                2. OSPF_LESS key of first element is less than second      */
/*                element key.                                               */
/*                3. OSPF_GREATER if key of first element is greater than    */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
RbCompareLsa (tRBElem * e1, tRBElem * e2)
{
    tLsaInfo           *pFstLsaInfo = e1;
    tLsaInfo           *pScndLsaInfo = e2;
    INT4                i4RetVal;

    i4RetVal = UtilLsaIdComp (pFstLsaInfo->lsaId.u1LsaType,
                              pFstLsaInfo->lsaId.linkStateId,
                              pFstLsaInfo->lsaId.advRtrId,
                              pScndLsaInfo->lsaId.u1LsaType,
                              pScndLsaInfo->lsaId.linkStateId,
                              pScndLsaInfo->lsaId.advRtrId);

    if (i4RetVal == OSPF_EQUAL)
    {
        return OSPF_RB_EQUAL;
    }
    else if (i4RetVal == OSPF_GREATER)
    {
        return OSPF_RB_GREATER;
    }
    return OSPF_RB_LESS;
}

/*****************************************************************************/
/* Function     : RbCompareNbr                                               */
/*                                                                           */
/* Description  : Compare function for the Neighbors                         */
/*                                                                           */
/* Input        : e1        Pointer to tNeighbor node1                       */
/*                e2        Pointer to tNeighbor node2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPF_EQUAL if keys of both the elements are same.       */
/*                2. OSPF_LESS key of first element is less than second      */
/*                element key.                                               */
/*                3. OSPF_GREATER if key of first element is greater than    */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
RbCompareNbr (tRBElem * e1, tRBElem * e2)
{
    tNeighbor          *pFstNbr = e1;
    tNeighbor          *pScndNbr = e2;
    tIPADDR             FstNbrSearchField;
    tIPADDR             ScndNbrSearchField;
    INT4                i4RetVal;

    if ((pFstNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pFstNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pFstNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        IP_ADDR_COPY (FstNbrSearchField, pFstNbr->nbrIpAddr);
    }
    else
    {
        IP_ADDR_COPY (FstNbrSearchField, pFstNbr->nbrId);
    }

    if ((pScndNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pScndNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pScndNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        IP_ADDR_COPY (ScndNbrSearchField, pScndNbr->nbrIpAddr);
    }
    else
    {
        IP_ADDR_COPY (ScndNbrSearchField, pScndNbr->nbrId);
    }

    i4RetVal = UtilIpAddrComp (FstNbrSearchField, ScndNbrSearchField);

    if (i4RetVal == OSPF_LESS)
    {
        return OSPF_RB_LESS;
    }
    else if (i4RetVal == OSPF_GREATER)
    {
        return OSPF_RB_GREATER;
    }
    else
    {
        /* This check is valid only for point to point networks */
        if (pFstNbr->pInterface->u4IfIndex > pScndNbr->pInterface->u4IfIndex)
        {
            return OSPF_RB_GREATER;
        }
        else if (pFstNbr->pInterface->u4IfIndex <
                 pScndNbr->pInterface->u4IfIndex)
        {
            return OSPF_RB_LESS;
        }
        else
        {
            if (pFstNbr->pInterface->u1NetworkType >
                pScndNbr->pInterface->u1NetworkType)
            {
                return OSPF_RB_GREATER;
            }
            else if (pFstNbr->pInterface->u1NetworkType <
                     pScndNbr->pInterface->u1NetworkType)
            {
                return OSPF_RB_LESS;
            }
        }
    }
    return OSPF_RB_EQUAL;
}

/*****************************************************************************/
/* Function     : RbWalkHandleLsuSplAgeTmrAction                             */
/*                                                                           */
/* Description  : Starts/Stops the DNA_LSA_SPL_AGING_TIMER for DNA SummaryLsa*/
/*                                                                           */
/* Input        : e1        Pointer to LsaInfo                               */
/*              : arg  -    RouterId                                         */
/*              : out  -    Action (START or STOP Timer)                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RB_WALK_CONT - to continue the RB Walk                     */
/*****************************************************************************/
INT4
RbWalkHandleLsuSplAgeTmrAction (tRBElem * e, eRBVisit visit, UINT4 level,
                                void *arg, void *out)
{
    tLsaInfo           *pLsaInfo = (tLsaInfo *) e;
    tRouterId          *pRtrId = (tRouterId *) arg;

    UNUSED_PARAM (level);

    if ((visit == leaf) || (visit == postorder))
    {
        if (IS_DNA_LSA (pLsaInfo) &&
            (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, *(tIPADDR *) pRtrId) ==
             OSPF_EQUAL))
        {
            if (*(UINT1 *) out == START)
            {
                TmrRestartTimer (&(pLsaInfo->dnaLsaSplAgingTimer),
                                 DNA_LSA_SPL_AGING_TIMER,
                                 NO_OF_TICKS_PER_SEC * MAX_AGE);
            }
            else
            {
                /* action is to STOP timer */
                TmrDeleteTimer (&(pLsaInfo->dnaLsaSplAgingTimer));
            }
        }
    }

    return (RB_WALK_CONT);
}

/*****************************************************************************/
/* Function     : RbWalkIndicationLsa                                        */
/*                                                                           */
/* Description  : Return OSPF_TRUE if self originated Indication LSA is found*/
/*                                                                           */
/* Input        : e1        Pointer to LsaInfo                               */
/*                                                                           */
/* Output       : out - Returns OSPF_TRUE/OSPF_FALSE                         */
/*                                                                           */
/* Returns      : RB_WALK_CONT - to continue the RB Walk                     */
/*              : RB_WALK_BREAK- to break the RB Walk                        */
/*****************************************************************************/
INT4
RbWalkIndicationLsa (tRBElem * e, eRBVisit visit, UINT4 level, void *arg,
                     void *out)
{
    tLsaInfo           *pLsaInfo = (tLsaInfo *) e;

    *(UINT4 *) out = OSPF_FALSE;

    UNUSED_PARAM (level);
    UNUSED_PARAM (arg);

    if ((visit == leaf) || (visit == postorder))
    {
        /* area has indication lsa with advertising routerid greater
         * than the routerid
         **/

        if (IS_INDICATION_LSA (pLsaInfo) &&
            (UtilIpAddrComp
             (pLsaInfo->lsaId.advRtrId,
              pLsaInfo->pArea->pOspfCxt->rtrId) == OSPF_GREATER))
        {
            *(UINT1 *) out = OSPF_TRUE;
            return (RB_WALK_BREAK);
        }
    }

    return (RB_WALK_CONT);
}

/*****************************************************************************/
/* Function     : RbWalkAndRestartIndicationLsa                              */
/*                                                                           */
/* Description  : Restarts the LSA Aging timer for self originated           */
/*                Indication summary LSA                                     */
/*                                                                           */
/* Input        : e1        Pointer to LsaInfo                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPF_EQUAL if keys of both the elements are same.       */
/*                2. OSPF_LESS key of first element is less than second      */
/*                element key.                                               */
/*                3. OSPF_GREATER if key of first element is greater than    */
/*                second element key                                         */
/* Returns      : RB_WALK_CONT - to continue the RB Walk                     */
/*****************************************************************************/
INT4
RbWalkAndRestartIndicationLsa (tRBElem * e, eRBVisit visit, UINT4 level,
                               void *arg, void *out)
{
    tLsaInfo           *pLsaInfo = (tLsaInfo *) e;

    UNUSED_PARAM (level);
    UNUSED_PARAM (arg);
    UNUSED_PARAM (out);

    if ((visit == leaf) || (visit == postorder))
    {
        if (IS_INDICATION_LSA (pLsaInfo) &&
            IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))
        {
            pLsaInfo->pArea->bIndicationLsaPresence = OSPF_FALSE;
            TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                             LSA_PREMATURE_AGING_TIMER,
                             NO_OF_TICKS_PER_SEC * 1);
        }
    }

    return (RB_WALK_CONT);
}

/*****************************************************************************/
/* Function     : RbWalkAndRestartSummaryDnaLsa                              */
/*                                                                           */
/* Description  : Restarts the LSA Aging timer for DNA summary Lsa           */
/*                                                                           */
/* Input        : e1        Pointer to LsaInfo                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RB_WALK_CONT - to continue the RB Walk                     */
/*****************************************************************************/
INT4
RbWalkAndRestartSummaryDnaLsa (tRBElem * e, eRBVisit visit, UINT4 level,
                               void *arg, void *out)
{
    tLsaInfo           *pLsaInfo = (tLsaInfo *) e;

    UNUSED_PARAM (level);
    UNUSED_PARAM (arg);
    UNUSED_PARAM (out);

    if ((visit == leaf) || (visit == postorder))
    {
        /* if it is a dna lsa flush it from the routing domain */
        if (IS_DNA_LSA (pLsaInfo))
        {
            TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                             LSA_PREMATURE_AGING_TIMER,
                             NO_OF_TICKS_PER_SEC * 1);

        }
    }

    return (RB_WALK_CONT);
}

#ifdef HIGH_PERF_RXMT_LST_WANTED
PUBLIC INT4
RbCompareRxmtInfo (tRBElem * e1, tRBElem * e2)
{
    tRxmtNode          *pFstRxmtNode = e1;
    tRxmtNode          *pScndRxmtNode = e2;
    INT4                i4RetVal;

    if (pFstRxmtNode->pLsaInfo->pOspfCxt->u4OspfCxtId >
        pScndRxmtNode->pLsaInfo->pOspfCxt->u4OspfCxtId)
    {
        i4RetVal = OSPF_RB_GREATER;
    }
    else if (pFstRxmtNode->pLsaInfo->pOspfCxt->u4OspfCxtId <
             pScndRxmtNode->pLsaInfo->pOspfCxt->u4OspfCxtId)
    {
        i4RetVal = OSPF_RB_LESS;
    }
    else
    {
        i4RetVal = RbRxmtNodeCompareLsa (pFstRxmtNode->pLsaInfo,
                                         pScndRxmtNode->pLsaInfo);
        if (i4RetVal == OSPF_RB_EQUAL)
        {
            i4RetVal = RbRxmtNodeCompareNbr (pFstRxmtNode->pNbr,
                                             pScndRxmtNode->pNbr);
        }
    }
    return i4RetVal;
}

PRIVATE INT4
RbRxmtNodeCompareLsa (tLsaInfo * pFstLsaInfo, tLsaInfo * pScndLsaInfo)
{
    INT4                i4RetVal;

    i4RetVal = UtilLsaIdComp (pFstLsaInfo->lsaId.u1LsaType,
                              pFstLsaInfo->lsaId.linkStateId,
                              pFstLsaInfo->lsaId.advRtrId,
                              pScndLsaInfo->lsaId.u1LsaType,
                              pScndLsaInfo->lsaId.linkStateId,
                              pScndLsaInfo->lsaId.advRtrId);

    if (i4RetVal == OSPF_EQUAL)
    {
        return OSPF_RB_EQUAL;
    }
    else if (i4RetVal == OSPF_GREATER)
    {
        return OSPF_RB_GREATER;
    }
    return OSPF_RB_LESS;
}

PRIVATE INT4
RbRxmtNodeCompareNbr (tNeighbor * pFstNbr, tNeighbor * pScndNbr)
{
    tIPADDR             FstNbrSearchField;
    tIPADDR             ScndNbrSearchField;
    INT4                i4RetVal;

    if ((pFstNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pFstNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pFstNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        IP_ADDR_COPY (FstNbrSearchField, pFstNbr->nbrIpAddr);
    }
    else
    {
        IP_ADDR_COPY (FstNbrSearchField, pFstNbr->nbrId);
    }

    if ((pScndNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pScndNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pScndNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        IP_ADDR_COPY (ScndNbrSearchField, pScndNbr->nbrIpAddr);
    }
    else
    {
        IP_ADDR_COPY (ScndNbrSearchField, pScndNbr->nbrId);
    }

    i4RetVal = UtilIpAddrComp (FstNbrSearchField, ScndNbrSearchField);

    if (i4RetVal == OSPF_LESS)
    {
        return OSPF_RB_LESS;
    }
    else if (i4RetVal == OSPF_GREATER)
    {
        return OSPF_RB_GREATER;
    }
    else
    {
        /* This check is valid only for point to point networks */
        if (pFstNbr->pInterface->u4IfIndex > pScndNbr->pInterface->u4IfIndex)
        {
            return OSPF_RB_GREATER;
        }
        else if (pFstNbr->pInterface->u4IfIndex <
                 pScndNbr->pInterface->u4IfIndex)
        {
            return OSPF_RB_LESS;
        }
        else
        {
            if (pFstNbr->pInterface->u1NetworkType >
                pScndNbr->pInterface->u1NetworkType)
            {
                return OSPF_RB_GREATER;
            }
            else if (pFstNbr->pInterface->u1NetworkType <
                     pScndNbr->pInterface->u1NetworkType)
            {
                return OSPF_RB_LESS;
            }
        }
    }
    return OSPF_RB_EQUAL;
}
#endif /* HIGH_PERF_RXMT_LST_WANTED */
