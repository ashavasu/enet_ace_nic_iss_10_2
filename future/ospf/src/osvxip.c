/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osvxip.c,v 1.14 2017/09/21 13:48:47 siva Exp $
 *
 * Description:This file contains procedures related to
 *             OSPF & VxWork IP Integration 
 *
 *******************************************************************/

#include "osinc.h"
#ifdef RAWSOCK_WANTED
#ifdef VxIP_WANTED

#include "snmcdefn.h"
#include "snmctdfs.h"

PRIVATE UINT1       gau1TxPktBuf[MAX_OSPF_PKT_LEN];
PRIVATE UINT1       gau1RxPktBuf[MAX_OSPF_PKT_LEN];
PRIVATE tInterface *GetIfFromPkt
PROTO ((tIPADDR srcIpAddr, tIPADDR destIpAddr));

/*****************************************************************************/
/*                                                                                   */
/* Function     : IpifSpawnOspfTask                                           */
/*                                                                           */
/* Description  : This procedure spawns the Ospf Task            */
/*                                                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if OSPF Task is successfully spawned              */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IpifSpawnOspfTask (void)
{
    tOsixTaskId         u4IpOspfTaskId;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC:IpifSpawnOspfTask\n");

    /* task initializations */
    if (OspfTaskInit () == OSPF_FAILURE)
    {

        return OSPF_FAILURE;
    }

    /* spawn ospf task */

    if (OsixCreateTask ((const UINT1 *) "OSPF", OSPF_ROUTING_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        OSPFTaskMain, NULL, OSIX_DEFAULT_TASK_MODE,
                        (tOsixTaskId *) & (u4IpOspfTaskId)) != OSIX_SUCCESS)
    {
    }

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "EXIT:IpifSpawnOspfTask\n");
    return SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IpifSendPktInCxt                                    */
/*                                                                         */
/*     Description   : This function sends the OSPF pkt to VxWorkIP        */
/*                                                                                                    */
/*                                                                         */
/*     Input(s)      :  pPkt          : Pointer to the buffer.             */
/*                      pSrcIpAddr  : Source IP Addr.                      */
/*                      pDestIpAddr : Destination IP Addr.                 */
/*                      u2Len         : Length of the OSPF pdu.            */
/*                      u1TimeToLive: Time to Live.                        */
/*                      u4OspfCxtId: OSPF Context ID                       */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  SUCCESS/FAILURE.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
IpifSendPktInCxt (UINT4 u4OspfCxtId, tCRU_BUF_CHAIN_HEADER * pPkt,
                  tIPADDR * pSrcIpAddr,
                  tIPADDR * pDestIpAddr, UINT2 u2Len, UINT1 u1TimeToLive)
{

    UINT1              *pRawPkt;
    struct sockaddr_in  DestAddr;
    INT4                i4SendBytes;
    UNUSED_PARAM (u4OspfCxtId);
    /* struct in_addr      IfAddr; */

/* ChangeForPorting - Not Constructing IP Header */
    OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    pRawPkt = gau1TxPktBuf;

    if (CRU_BUF_Copy_FromBufChain (pPkt, pRawPkt, 0, u2Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    OS_MEM_SET (&(DestAddr), 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = *((UINT4 *) (pDestIpAddr));
    DestAddr.sin_port = 0;

    if ((i4SendBytes = sendto ((gOsRtr.ai4SockId[u4OspfCxtId]), pRawPkt,
                               u2Len, 0, ((struct sockaddr *) &DestAddr),
                               (sizeof (struct sockaddr_in)))) != u2Len)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    CRU_BUF_Release_MsgBufChain (pPkt, 0);
    OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : OspfIpCalcCheckSum                                     */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
EXPORT UINT2
OspfIpCalcCheckSum (UINT1 *pBuf, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifJoinMcastGroup                                     */
/*                                                                           */
/* Description  : This procedure calls the IP module to join the specified   */
/*                multicast group in  the specified interface                */
/*                                                                           */
/* Input        :  pMcastGroupAddr : multicast group address                 */
/*                 pInterface       : pointer to the interface               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IpifJoinMcastGroup (tIPADDR * pMcastGroupAddr, tInterface * pInterface)
{

    struct ip_mreq      McastReq;
    UINT4               u4Addr;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: IpifJoinMcastGroup\n");

    MEMSET (&McastReq, 0, sizeof (struct ip_mreq));

    u4Addr = *(UINT4 *) (*pMcastGroupAddr);
    McastReq.imr_multiaddr.s_addr = (u4Addr);
    u4Addr = *(UINT4 *) (pInterface->ifIpAddr);
    McastReq.imr_interface.s_addr = (u4Addr);

    if (setsockopt
        (gOsRtr.ai4SockId[pInterface->pArea->pOspfCxt->u4OspfCxtId], IPPROTO_IP,
         IP_ADD_MEMBERSHIP, (void *) &McastReq, sizeof (McastReq)) != 0)
    {
        OSPF_TRC (OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Join multicast group failed\n");
    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: IpifJoinMcastGroup\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifLeaveMcastGroup                       */
/*                                                                           */
/* Description  : This procedure calls the IP module to leave the specified  */
/*                multicast group in  the specified interface                */
/*                                                                           */
/* Input        :  pMcastGroupAddr : multicast group address                 */
/*                 pInterface      : pointer to the interface                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IpifLeaveMcastGroup (tIPADDR * pMcastGroupAddr, tInterface * pInterface)
{

    struct ip_mreq      McastReq;
    UINT4               u4Addr;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: IpifLeaveMcastGroup\n");
    MEMSET (&McastReq, 0, sizeof (struct ip_mreq));

    u4Addr = *(UINT4 *) (*pMcastGroupAddr);
    McastReq.imr_multiaddr.s_addr = u4Addr;
    u4Addr = *(UINT4 *) (pInterface->ifIpAddr);
    McastReq.imr_interface.s_addr = u4Addr;

    if (setsockopt
        (gOsRtr.ai4SockId[pInterface->pArea->pOspfCxt->u4OspfCxtId], IPPROTO_IP,
         IP_DROP_MEMBERSHIP, (void *) &McastReq, sizeof (McastReq)) != 0)
    {
        OSPF_TRC (OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Join multicast group failed\n");
    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: IpifLeaveMcastGroup\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifGetIfParamsInCxt                                    */
/*                                                                           */
/* Description  : VxWork IP Stack stores interface info in global               */
/*                     interface structure.This func traverses the  list               */
/*                     and retrieves interface info                                            */
/*                                                                           */
/* Input        : pIpAddr          : IP address                            */
/*                * pAddrlessIf    : address less index                    */
/*                                                                           */
/* Output       : * pIfIndex       : IP handle                             */
/*                * pIfIpAddrMask: IP address mask                       */
/*                * pIfMtuSize    : size of mtu                           */
/*                * pIfSpeed       : metric cost                           */
/*                * p_if_oper_stat   : operational status of Iface           */
/*                  u4OspfCxtId: OSPF Context ID                             */
/*                                                                           */
/* Returns      :     OSPF_SUCCESS, if packet is successfully transferred             */
/*                       OSPF_FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IpifGetIfParams (tIPADDR pIpAddr,
                 UINT4 u4AddrlessIf,
                 UINT2 *pIfIndex,
                 tIPADDRMASK pIfIpAddrMask,
                 UINT4 *pIfMtuSize,
                 UINT4 *pIfSpeed, UINT1 *pu1IfType, UINT1 *pu1IfOperStat)
{

    struct ifnet       *pIf = NULL;
    unsigned long       uNetAddress, uNetMask;
    struct ifaddr      *ifa;
    struct sockaddr_in *Addr;
    tIPADDR             CmpIfIpAddr;
    UNUSED_PARAM (u4OspfCxtId);

    for (pIf = ifnet; pIf != NULL; pIf = pIf->if_next)
    {
        for (ifa = pIf->if_addrlist; ifa != NULL; ifa = ifa->ifa_next)
        {
            if (ifa->ifa_addr->sa_family == AF_INET)
            {
                Addr = (struct sockaddr_in *) ifa->ifa_addr;
                uNetAddress = Addr->sin_addr.s_addr;

                /* As ifnet structure itself stores Address & mask in 
                 * network order, no conversion is required here
                 */
                *(UINT4 *) CmpIfIpAddr = uNetAddress;

                if (UtilIpAddrComp (pIpAddr, CmpIfIpAddr) == OSPF_EQUAL)
                {
                    Addr = (struct sockaddr_in *) (ifa->ifa_netmask);
                    uNetMask = Addr->sin_addr.s_addr;
                    *pIfIndex = pIf->if_index;
                    *pIfMtuSize = pIf->if_mtu;
                    *pIfSpeed = pIf->if_baudrate;
                    *pu1IfOperStat = 1;
                    *(UINT4 *) pIfIpAddrMask = uNetMask;
                    *pu1IfType = IF_BROADCAST;
                    if ((pIf->if_flags) & IFF_POINTOPOINT)
                    {
                        *pu1IfType = IF_PTOP;
                    }
                    return OSPF_SUCCESS;

                }
            }
        }
    }

    return OSPF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : ValidIpInterfaceInCxt                                         */
/*                                                                           */
/* Description  : VxWork IP Stack stores interface info in global               */
/*                 interface structure.This func traverses the  list               */
/*                 and checks if the interface is valid at IP  level   */
/*                                                                           */
/* Input        : pIpAddr          : IP address                            */
/*                u4AddrlessIf    : address less index                    */
/*                      u4OspfCxtId: OSPF Context ID                       */
/*                                                                           */
/* Output       : None                             */
/*                                                                           */
/* Returns      :     SNMP_SUCCESS             */
/*                       SNMP_FAILURE, otherwise                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ValidIpInterfaceInCxt (UINT4 u4OspfCxtId, tIPADDR pIpAddr, UINT4 u4AddrlessIf)
{
    struct ifnet       *pIf = NULL;
    unsigned long       uNetAddress;
    struct ifaddr      *ifa;
    struct sockaddr_in *Addr;
    tIPADDR             CmpIfIpAddr;
    UNUSED_PARAM (u4OspfCxtId);

    for (pIf = ifnet; pIf != NULL; pIf = pIf->if_next)
    {
        for (ifa = pIf->if_addrlist; ifa != NULL; ifa = ifa->ifa_next)
        {
            if (ifa->ifa_addr->sa_family == AF_INET)
            {
                Addr = (struct sockaddr_in *) ifa->ifa_addr;
                uNetAddress = Addr->sin_addr.s_addr;

                /* As ifnet structure itself stores Address & mask in 
                 * network order, no conversion is required here
                 */
                *(UINT4 *) CmpIfIpAddr = uNetAddress;

                if (UtilIpAddrComp (pIpAddr, CmpIfIpAddr) == OSPF_EQUAL)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    return SNMP_FAILURE;
}

/* ChangeForPorting - To be used with FutureIP */
#ifdef IP_WANTED
#ifdef DEBUG_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFGetRoute                                             */
/*                                                                           */
/* Description  : Finds the route based on destination IP address and type   */
/*                of service.                                                */
/*                                                                           */
/* Input        : * UINT4                : destination IP address            */
/*                  UINT1                : TOS                               */
/*                                                                           */
/* Output       : * (tInterfaceId *)   : next hop interface                */
/*                * (UINT4 *)            : next hop IP address               */
/*                                                                           */
/* Returns      : SUCCESS, if route found                                    */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
OSPFGetRoute (UINT4 u4DestIpAddr, UINT1 u1Tos,
              tInterfaceId * pNextHopIf, UINT4 *pNextHopIpAddr)
{

    tIPADDR             destIpAddr;
    tRouteNextHop      *pRtNextHop;
    tInterfaceId        ifId;
    tPath              *pPath;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID, "FUNC:OSPFGetRoute \n");
    OSPF_CRU_BMC_DWTOPDU (&destIpAddr, u4DestIpAddr);
    pPath =
        RtlRtLookup ((tOspfRt *) gOsRtr.pOspfRt,
                     (tIPADDR *) destIpAddr, (INT1) u1Tos);
    if (pPath != NULL)
    {
        if (pPath->u1HopCount != 0)
        {
            pPath->u1NextHopIndex++;
            pPath->u1NextHopIndex %= pPath->u1HopCount;
            if ((pRtNextHop =
                 (&(pPath->aNextHops[pPath->u1NextHopIndex]))) != NULL)
            {
                IpGetIfIdFromIfIndex ((UINT4) pRtNextHop->u2IfIndex, &ifId);
                OS_MEM_CPY (pNextHopIf, &(ifId), sizeof (tInterfaceId));
                IP_ADDR_COPY (pNextHopIpAddr, &(pRtNextHop->nbrIpAddr));
                *pNextHopIpAddr =
                    OSPF_CRU_BMC_DWFROMPDU (pRtNextHop->nbrIpAddr);
                return SUCCESS;
            }
        }
    }
    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID, "EXIT: OSPFGetRoute \n");
    return OSPF_FAILURE;

}
#endif /* DEBUG_WANTED */
#endif /* IP_WANTED    */

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifGetPkt                                                 */
/*                                                                           */
/* Description  : This procedure gets the pkt from socket and calls          */
/*                PppRcvPkt which processes the packet received from IP.     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpifGetPkt (INT4 i4SockId)
{

    INT4                i4fdCount;
    struct timeval      TimeVal;
    fd_set              gReadFds;
    fd_set              gReadCpyFds;
    fd_set              gWriteFds;
    fd_set              gExceptFds;
    INT4                i4RecvNodeLen;
    INT4                i4RecvBytes;
    tCRU_BUF_CHAIN_HEADER *pPkt;
    UINT1              *pRecvPkt;
    UINT4               ipAddr;
    tInterface         *pInterface;
    tIPADDR             srcIpAddr;
    tIPADDR             destIpAddr;
    UINT1               u1Hlen = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2Datalen = 0;
    struct sockaddr_in  Recv_Node;

    /* Set time-to-wait as 0 for non-blocking select() call */
    TimeVal.tv_sec = 0;
    TimeVal.tv_usec = 0;
    FD_ZERO ((fd_set *) & (gReadFds));
    FD_ZERO ((fd_set *) & (gReadCpyFds));
    FD_ZERO ((fd_set *) & (gWriteFds));
    FD_ZERO ((fd_set *) & (gExceptFds));

    FD_SET (i4SockId, (fd_set *) & (gReadFds));

    /* Continuously poll sockets for data. The select call is
     * made as a non-blocking call. Only when there is no data
     * in any of the sockets, this loop finishes.
     */

    for (;;)
    {
        MEMCPY ((char *) &(gReadCpyFds), (char *) &(gReadFds), sizeof (fd_set));

        i4fdCount = select ((i4SockId) + 1,
                            (fd_set *) & (gReadCpyFds),
                            (fd_set *) & (gWriteFds),
                            (fd_set *) & (gExceptFds),
                            (struct timeval *) &TimeVal);
        if (i4fdCount < 0)
        {
            OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                          "EXIT: Select call failed");
            return;
        }

        if (i4fdCount == 0)
        {
            OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                          "EXIT: No packets to read in sockets");
            return;
        }

        if (FD_ISSET (i4SockId, (fd_set *) & (gReadCpyFds)))
        {
            i4RecvNodeLen = sizeof (Recv_Node);
            OS_MEM_SET (&Recv_Node, 0, i4RecvNodeLen);

            OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
            pRecvPkt = gau1RxPktBuf;

            /* Call Recvfrom to recv pkts */
            if ((i4RecvBytes = recvfrom ((i4SockId), pRecvPkt,
                                         (gOsRtr.u4MaxMtuSize),
                                         0,
                                         ((struct sockaddr *) &Recv_Node),
                                         (int *) &i4RecvNodeLen)) > 0)
            {
                /* Copy the recvd linear buf to cru buf */
                if ((pPkt =
                     CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
                {
                    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                                  "EXIT : Unable to allocate buf\n");
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }
                MEMSET (pPkt->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
                CRU_BUF_UPDATE_MODULE_INFO (pPkt, "OSPFIpGetPkt");
                CRU_BUF_Copy_OverBufChain (pPkt, pRecvPkt, 0, i4RecvBytes);

                OSPF_IP_PKT_GET_SRC (pPkt, ipAddr);
                OSPF_CRU_BMC_DWTOPDU (srcIpAddr, ipAddr);
                IP_PKT_GET_DEST (pPkt, ipAddr);
                OSPF_CRU_BMC_DWTOPDU (destIpAddr, ipAddr);

                if ((pInterface = GetIfFromPkt (srcIpAddr, destIpAddr)) == NULL)
                {
                    OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                                  "Pkt Disc: dropping unwanted packets\n ");
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                if (UtilIpAddrComp (srcIpAddr, pInterface->ifIpAddr) ==
                    OSPF_EQUAL)
                {
                    OSPF_TRC (CONTROL_PLANE_TRC,
                              pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              "Pkt Disc: dropping self-originated PDU\n ");
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                /* If the interface is Passive interface 
                 * No need to process any OSPF protocol packet */
                if (pInterface->bPassive == OSPF_TRUE)
                {
                    OSPF_TRC (CONTROL_PLANE_TRC,
                              pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              "Pkt Disc: dropping Packets received on passive interfaece \n ");
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                OSPF_CRU_BMC_GET_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1Hlen);
                OSPF_CRU_BMC_GET_2_BYTE (pPkt, IP_PKT_OFF_LEN, u2PktLen);

                /* ChangeForPorting FutureOspf to VxWorks IP Stack                  */
                /* In case of FutureIP being used we have to subtract IP header     */
                /* length from the total packet length , as it stores pkt len as        */
                /* sum of header + data                                                             */

#ifdef IP_WANTED
                u2Datalen = u2PktLen - ((u1Hlen & 0x0f) * 4);
#else
                u2Datalen = u2PktLen;
#endif

                OSPF_IP_PKT_MOVE_TO_DATA (pPkt, ((u1Hlen & 0x0f) * 4));

                PppRcvPkt (pPkt, u2Datalen, pInterface, &srcIpAddr,
                           &destIpAddr);
            }

            OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);

        }

    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: Socket read completed");
    return;
}

#ifndef RRD_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRTAddRouteToCommIpRtTbl                                */
/*                                                                           */
/* Description  This routine is used to add the specified route entry to   */
/*                the VxIP Stack routing table.                              */
/*                                                                           */
/* Input        : pRtEntry      : Route entry that has to be added.          */
/*                                                                           */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFRTAddRouteToCommIpRtTbl (tRtEntry * pRtEntry)
{
    long                destIp, gateIp, ipMask;
    int                 iTos, iFlags, iProto;
    tPath              *pCurrPath;
    UINT1               u1HopInd = 0;

    if (pRtEntry->u1DestType == DEST_NETWORK)
    {
        if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
        {
            return;
        }

        destIp = *(UINT4 *) (pRtEntry->destId);

        /* Address is stored in ospf routing data structure in network
         * byte order and mask is stored in host order
         */

        ipMask = OSIX_HTONL (*(UINT4 *) &(pRtEntry->u4IpAddrMask));

        for (u1HopInd = 0; u1HopInd < pCurrPath->u1HopCount; u1HopInd++)
        {
            iTos = TOS_0;
            iFlags = 0;
            iProto = FS_OSPF_PROTO;
            gateIp = *(UINT4 *) (pCurrPath->aNextHops[u1HopInd].nbrIpAddr);

            /* Updation of Route Entries with Next Hop 0.0.0.0 to
             * the VxIP Routing Table is not required. */
            if (UtilIpAddrComp (pCurrPath->aNextHops[u1HopInd].nbrIpAddr,
                                gNullIpAddr) == OSPF_EQUAL)
            {
                continue;
            }

            if (mRouteEntryAdd (destIp, gateIp, ipMask, iTos, iFlags,
                                iProto) != 0)
            {
                OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                              OSPF_INVALID_CXT_ID, "Failed to Add Route\n");
                return;
            }
            else
            {
                OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                              "Success Add Route\n");
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRTAddNextHopToCommIpRtTbl                              */
/*                                                                           */
/* Description  : This routine is used to add the specified route entry to   */
/*                the Common routing(TRIE) table for the IP forwarding.      */
/*                                                                           */
/* Input        : pRtEntry      : Route entry that has to be added.          */
/*              : u1HopIndex     : Index to which NextHop path is added      */
/*                                                                           */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFRTAddNextHopToCommIpRtTbl (tRtEntry * pRtEntry, tPath * pCurrPath,
                               UINT1 u1HopIndex)
{
    long                destIp, gateIp, ipMask;
    int                 iTos, iFlags, iProto;

    if (pRtEntry->u1DestType == DEST_NETWORK)
    {
        /* Address is stored in ospf routing data structure in network
         * byte order and mask is stored in host order
         */
        ipMask = OSIX_HTONL (*(UINT4 *) &(pRtEntry->u4IpAddrMask));

        destIp = *(UINT4 *) (pRtEntry->destId);

        iTos = TOS_0;
        iFlags = 0;
        iProto = FS_OSPF_PROTO;
        gateIp = *(UINT4 *) (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr);

        /* Updation of Route Entries with Next Hop 0.0.0.0 to
         * the VxIP Routing Table is not required. */
        if (UtilIpAddrComp (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr,
                            gNullIpAddr) == OSPF_EQUAL)
        {
            return;
        }

        if (mRouteEntryAdd (destIp, gateIp, ipMask, iTos, iFlags, iProto) != 0)
        {
            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                      pCurrPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "Failed to Add Route\n");
            return;
        }
        else
        {
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pCurrPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "Success Add Route\n");
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRTUpdateRouteToCommIpRtTbl                            */
/*                                                                           */
/* Description  : This routine is used to update the specified route entry   */
/*                                                                           */
/*                                                                           */
/* Input        : pRtEntry      : Route entry that has to be Updated.        */
/*              : pCurrPath     : Pointer to the path associated to          */
/*                                    this Route Entry                       */
/*                                                                           */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFRTUpdateRouteToCommIpRtTbl (tRtEntry * pRtEntry, tPath * pCurrPath,
                                UINT1 u1HopInd)
{

    /* This function was used in the baseline stack to modify only */
    /* cost associated with a route since FutureIP supports cost   */
    /* association with routes. VxWorks IP stack does not support  */
    /* cost association, so this function body does'nt do anything */
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRTDeleteRouteFromCommIpRtTbl                         */
/*                                                                           */
/* Description  : This routine is used to Delete the specified route entry   */
/*                from the VxWork IP routing table.                          */
/*                                                                           */
/* Input        : pRtEntry      : Route entry that has to be Deleted.      */
/*                                                                           */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFRTDeleteRouteFromCommIpRtTbl (tRtEntry * pRtEntry)
{
    long                destIp, gateIp, ipMask;
    int                 iTos, iFlags, iProto;
    tPath              *pCurrPath;
    UINT1               u1HopInd = 0;

    if (pRtEntry->u1DestType == DEST_NETWORK)
    {
        if ((pCurrPath = GET_TOS_PATH (pRtEntry, TOS_0)) == NULL)
        {
            return;
        }

        destIp = *(UINT4 *) (pRtEntry->destId);
        /* Address is stored in ospf routing data structure in network
         * byte order and mask is stored in host order
         */
        ipMask = OSIX_HTONL (*(UINT4 *) &(pRtEntry->u4IpAddrMask));

        for (u1HopInd = 0; u1HopInd < pCurrPath->u1HopCount; u1HopInd++)
        {
            iTos = TOS_0;
            iFlags = 0;
            iProto = FS_OSPF_PROTO;
            gateIp = *(UINT4 *) (pCurrPath->aNextHops[u1HopInd].nbrIpAddr);

            /* Deletion of Route Entries with Next Hop 0.0.0.0 from 
             * the VxIP Routing Table is not required. */
            if (UtilIpAddrComp (pCurrPath->aNextHops[u1HopInd].nbrIpAddr,
                                gNullIpAddr) == OSPF_EQUAL)
            {
                continue;
            }

            if (mRouteEntryDelete (destIp, gateIp, ipMask, iTos, iFlags,
                                   iProto) != OK)
            {
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                          pCurrPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
                          "Failed to Delete Route \n");
                return;
            }
            else
            {
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pCurrPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
                          "Success in Deleting Route\n");
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFRTDeleteNextHopFromCommIpRtTbl                         */
/*                                                                           */
/* Description  : This routine is used to delete the specified route entry   */
/*                form the Common routing(TRIE) table for the IP forwarding. */
/*                                                                           */
/* Input        : pRtEntry      : Route entry that has to be deleted.        */
/*              : u1HopIndex     : Index to which NextHop path is deleted    */
/*                                                                           */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFRTDeleteNextHopFromCommIpRtTbl (tRtEntry * pRtEntry, tPath * pCurrPath,
                                    UINT1 u1HopIndex)
{

    long                destIp, gateIp, ipMask;
    int                 iTos, iFlags, iProto;

    if (pRtEntry->u1DestType == DEST_NETWORK)
    {
        destIp = *(UINT4 *) (pRtEntry->destId);

        /* Address is stored in ospf routing data structure in network
         * byte order and mask is stored in host order
         */
        ipMask = OSIX_HTONL (*(UINT4 *) &(pRtEntry->u4IpAddrMask));

        iTos = TOS_0;
        iFlags = 0;
        iProto = FS_OSPF_PROTO;
        gateIp = *(UINT4 *) (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr);

        /* Deletion of Route Entries with Next Hop 0.0.0.0 from 
         * the VxIP Routing Table is not required. */
        if (UtilIpAddrComp (pCurrPath->aNextHops[u1HopIndex].nbrIpAddr,
                            gNullIpAddr) == OSPF_EQUAL)
        {
            return;
        }

        if (mRouteEntryDelete (destIp, gateIp, ipMask, iTos, iFlags,
                               iProto) != OK)
        {
            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                      pCurrPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "Failed to Delete Route \n");
            return;
        }
        else
        {
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pCurrPath->pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "Success in Deleting Route\n");
        }
    }
    return;
}
#endif /* RRD_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfSetIfSockOptions                                      */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfSetIfSockOptions (tInterface * pInterface)
{

    UINT4               u4Addr;
    struct in_addr      MyAddr;

    /* The VxWorks IP stack does not support unnumbered interfaces                 */
    /* So, we assume that any created interface will have an associated IP address */
    MEMSET (&MyAddr, 0, sizeof (struct in_addr));
    u4Addr = *((UINT4 *) (pInterface->ifIpAddr));
    MyAddr.s_addr = u4Addr;
    if (setsockopt
        (gOsRtr.ai4SockId[pInterface->pArea->pOspfCxt->u4OspfCxtId], IPPROTO_IP,
         IP_MULTICAST_IF, (void *) &MyAddr, sizeof (MyAddr)) != 0)
    {
        OSPF_TRC (OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Setting receiving interface for socket failed \n");
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfSetRawSockOptions                                      */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : pInterface      : interface                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfSetRawSockOptions (UINT4 u4OspfCxtId)
{
    /* In baseline code this routine would use setsockopt calls */
    /* to include the ip header in the pkt and then give it to     */
    /* FutureIP                                                                    */
    /* In VxWork case only ospf pkt is passed to IP Layer       */
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCreateSocket                                           */
/*                                                                           */
/* Description  : Create Socket                                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfCreateSocket (UINT4 u4OspfCxtId)
{
    INT4                i4SockFd;
    UINT1               u1Ttl;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4SockFd = LnxVrfGetSocketFd (u4OspfCxtId, AF_INET, SOCK_RAW,
                                  FS_OSPF_PROTO, LNX_VRF_OPEN_SOCKET);

#else
    i4SockFd = socket (AF_INET, SOCK_RAW, FS_OSPF_PROTO);
#endif

    if (i4SockFd == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }

    gOsRtr.ai4SockId[u4OspfCxtId] = i4SockFd;

    /* Ospf packets should not be forwarded beyond this subnet      */
    /* Set the TTL for multicast packets to 1 to prevent forwarding */
    u1Ttl = 1;
    if (setsockopt
        (gOsRtr.ai4SockId[u4OspfCxtId], IPPROTO_IP, IP_MULTICAST_TTL, &u1Ttl,
         sizeof (u1Ttl)) != 0)
    {
        OSPF_GBL_TRC (OSPF_CONFIGURATION_TRC, OSPF_INVALID_CXT_ID,
                      "Setting MULTICAST_TTL for socket failed \n");
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCloseSocket                                            */
/*                                                                           */
/* Description  : Close Socket                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfCloseSocket (u4OspfCxtId)
{

    close (gOsRtr.ai4SockId[u4OspfCxtId]);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetIfFromPkt                                               */
/*                                                                           */
/* Description  : Get the Ospf Interface from source and destination         */
/*                address of the packet                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pInterface                                                 */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PRIVATE tInterface *
GetIfFromPkt (tIPADDR srcIpAddr, tIPADDR destIpAddr)
{
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((UtilIpAddrComp (destIpAddr, gAllSpfRtrs) == OSPF_EQUAL)
        || (UtilIpAddrComp (destIpAddr, gAllDRtrs) == OSPF_EQUAL))
    {
        /* Getting the interface from List */
        TMO_SLL_Scan (&(gOsRtr.sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            if (UtilIpAddrMaskComp (pInterface->ifIpAddr, srcIpAddr,
                                    pInterface->ifIpAddrMask) == OSPF_EQUAL)
            {
                return pInterface;
            }
        }
    }
    else
    {
        return (GetFindIf (destIpAddr, 0));
    }
    return NULL;
}

#endif /* #ifdef VxIP_WANTED */
#endif /* #ifdef RAWSOCK_WANTED */
