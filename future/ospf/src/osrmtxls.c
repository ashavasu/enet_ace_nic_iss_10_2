/************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved     */
/*                                                          */
/*  FILE NAME             : osredblk.c                      */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : OSPF                            */
/*  LANGUAGE              : C                               */
/*  FUNCTIONS DEFINED     : OspfRmProcessHello,            */
/*                          OspfRmSendBulkHello,           */
/*  DESCRIPTION           : This file contains definitions  */
/*                          related to bulk update for      */
/*                          High Availability               */
/* $Id: osrmtxls.c,v 1.12 2014/03/01 11:40:49 siva Exp $      */
/************************************************************/
/*                                                          */
/*  Change History                                          */
/*  Version               :                                 */
/*  Date(DD/MM/YYYY)      :                                 */
/*  Modified by           :                                 */
/*  Description of change :                                 */
/************************************************************/
#ifndef _OSREDBLK_
#define _OSREDBLK_

#include "osinc.h"

PRIVATE UINT2       OspfRmGetRequiredLenForLsa (tLsaInfo *);
PRIVATE tLsaInfo   *OsRmTxLsSendBulkUpdatesInCxt
PROTO ((tOspfCxt * pOspfCxt, tAreaId areaId, tLsaId lsaId));
PRIVATE tLsaInfo   *OsRmTxLsSendIntfBulkUpdatesInCxt
PROTO ((tArea * pArea, tRmMsg ** ppRmMsg, UINT2 *pu2Len));
PRIVATE tLsaInfo   *OsRmTxLsSendAreaBulkUpdatesInCxt
PROTO ((tArea * pArea, tRmMsg ** ppRmMsg, UINT2 *pu2Len));
PRIVATE tLsaInfo   *OsRmTxLsSendVirtBulkUpdatesInCxt
PROTO ((tOspfCxt * pOspfCxt, tRmMsg ** ppRmMsg, UINT2 *pu2Len));
PRIVATE tLsaInfo   *OsRmTxLsSendAsBulkUpdatesInCxt
PROTO ((tOspfCxt * pOspfCxt, tRmMsg ** ppRmMsg, UINT2 *pu2Len));
PRIVATE INT4        OsRmTxLsConstructAndSendLsu
PROTO ((tLsaInfo * pLsaInfo, tRmMsg ** ppRmMsg, UINT2 *pu2Len));

/****************************************************************************/
/*                                                                          */
/* Function     : OspfRmConstructLsaMsg                                     */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                or dynamic LSA sync up by the active node.                */
/*                This function constructs the bulk                         */
/*                update packet based on the LSA info passed                */
/*                                                                          */
/* Format of LSU bulk update message                                        */
/* Field              Content                                               */
/* Length             Length of LSA                                         */
/* LSA                LSA Content                                           */
/* Ack flag           OSPF_RED_LSACK_RCVD /OSPF_RED_LSACK_NOT_RCVD          */
/* Age                LSA Age                                               */
/* rxmit time         LSA Rxmit time                                        */
/* Context-ID         Context ID                                            */
/* LSA scope-         Area ID(areaId) in the context for                    */
/* identifier         ROUTER_LSA, NETWORK_LSA, NETWORK_SUM_LSA              */
/*                    ASBR_SUM_LSA, NSSA_LSA, TYPE10_OPQ_LSA.               */
/*                    Interface ID (ifAddr,IfaddrlessIndex) in the          */
/*                    context for TYPE9_OPQ_LSA.                            */
/* AssoPrimitive      For self LSA only.                                    */
/*                                                                          */
/* Input        : pLsaInfo    -     pointer to LSA Info                     */
/*                                                                          */
/* Output       : pRmMsg      -     pointer to RM Message                   */
/*                pu2Len      -     Length of the message                   */
/*                                                                          */
/* Returns      : OSPF_TRUE / OSPF_FALSE                                    */
/*                                                                          */
/****************************************************************************/
PUBLIC              BOOL1
OspfRmConstructLsaMsg (tLsaInfo * pLsaInfo, tRmMsg ** pPMsg, UINT2 *pu2MsgLen)
{
    tIPADDR             ipNetNum;
    tIPADDR             ipAddrMask;
    tIPADDR             asNetNum;
    tIPADDRMASK         asAddrMask;
    tAreaId             areaId;
    tMetric             aMetric[OSPF_MAX_METRIC];
    tArea              *pAddrRangeArea = NULL;
    tExtRoute          *pExtRoute = NULL;
    tAddrRange         *pAddrRange = NULL;
    tOpqLSAInfo        *pOpqLsaInfo = NULL;
    tInterface         *pInterface = NULL;
    tAsExtAddrRange    *pAsExtRange = NULL;
    UINT4               u4RemainingTime = 0;
    UINT4               u4AreaOffSet = 0;
    INT4                i4RetVal = OSPF_FAILURE;
    UINT2               u2Len = 0;
    UINT2               u2MaxLen = OSPF_RED_IFACE_MTU;
    UINT1               u1LsaAck = OSPF_RED_LSACK_NOT_RCVD;
    UINT1               u1InternalLsaType = 0;

    /* Check whether we can accommodate LSA in the packet or not */
    u2Len = OspfRmGetRequiredLenForLsa (pLsaInfo);

    if ((u2Len + *pu2MsgLen) > u2MaxLen)
    {
        return OSPF_TRUE;
    }

    OSPF_RED_PUT_2_BYTE (*pPMsg, *pu2MsgLen, pLsaInfo->u2LsaLen);
    OSPF_RED_PUT_1_BYTE (*pPMsg, *pu2MsgLen, pLsaInfo->lsaId.u1LsaType);
    OSPF_RED_PUT_N_BYTE (*pPMsg, pLsaInfo->pLsa,
                         *pu2MsgLen, pLsaInfo->u2LsaLen);

    if (pLsaInfo->u4RxmtCount == 0)
    {
        u1LsaAck = OSPF_RED_LSACK_RCVD;
    }
    else
    {
        u1LsaAck = OSPF_RED_LSACK_NOT_RCVD;
    }

    OSPF_RED_PUT_1_BYTE (*pPMsg, *pu2MsgLen, u1LsaAck);
    OSPF_RED_PUT_2_BYTE (*pPMsg, *pu2MsgLen, pLsaInfo->u2LsaAge);

    TmrGetRemainingTime (gTimerLst,
                         (tTmrAppTimer *) & (pLsaInfo->lsaAgingTimer.timerNode),
                         (UINT4 *) &(u4RemainingTime));

    OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen, u4RemainingTime);
    OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen, pLsaInfo->pOspfCxt->u4OspfCxtId);

    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case NSSA_LSA:
        case TYPE10_OPQ_LSA:
            OSPF_RED_PUT_N_BYTE (*pPMsg, pLsaInfo->pArea->areaId, *pu2MsgLen,
                                 sizeof (tAreaId));
            break;

        case TYPE9_OPQ_LSA:
            OSPF_RED_PUT_N_BYTE (*pPMsg, pLsaInfo->pInterface->ifIpAddr,
                                 *pu2MsgLen, sizeof (tAreaId));

            OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen,
                                 pLsaInfo->pInterface->u4AddrlessIf);
            break;
        default:
            break;
    }

    i4RetVal = UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                               pLsaInfo->pOspfCxt->rtrId);

    if (i4RetVal == OSPF_EQUAL)
    {
        u1InternalLsaType = 0;
        if (pLsaInfo->pLsaDesc != NULL)
        {
            u1InternalLsaType = pLsaInfo->pLsaDesc->u1InternalLsaType;
        }

        OSPF_RED_PUT_1_BYTE (*pPMsg, *pu2MsgLen, u1InternalLsaType);

        switch (u1InternalLsaType)
        {
            case NETWORK_SUM_LSA:
            case ASBR_SUM_LSA:
            case INDICATION_LSA:
                OSPF_RED_PUT_N_BYTE (*pPMsg, pLsaInfo->pLsaDesc->pAssoPrimitive,
                                     *pu2MsgLen, sizeof (tSummaryParam));
                break;

            case NETWORK_LSA:

                pInterface =
                    (tInterface *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;

                OSPF_RED_PUT_N_BYTE (*pPMsg, pInterface->ifIpAddr, *pu2MsgLen,
                                     sizeof (tIPADDR));

                OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen,
                                     pInterface->u4AddrlessIf);
                break;

            case AS_EXT_LSA:
            case NSSA_LSA:
                OSPF_RED_PUT_1_BYTE (*pPMsg, *pu2MsgLen,
                                     pLsaInfo->u1TrnsltType5);
                MEMSET (&ipNetNum, 0xff, sizeof (tIPADDR));
                MEMSET (&ipAddrMask, 0xff, sizeof (tIPADDR));
                pExtRoute =
                    (tExtRoute *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;
                if (pExtRoute != NULL)
                {
                    MEMCPY (&ipNetNum, pExtRoute->ipNetNum, sizeof (tIPADDR));
                    MEMCPY (&ipAddrMask, pExtRoute->ipAddrMask,
                            sizeof (tIPADDR));
                }
                OSPF_RED_PUT_N_BYTE (*pPMsg, &ipNetNum,
                                     *pu2MsgLen, sizeof (tIPADDR));
                OSPF_RED_PUT_N_BYTE (*pPMsg, &ipAddrMask,
                                     *pu2MsgLen, sizeof (tIPADDR));
                break;
            case COND_AS_EXT_LSA:
            case COND_NSSA_LSA:
                OSPF_RED_PUT_1_BYTE (*pPMsg, *pu2MsgLen,
                                     pLsaInfo->u1TrnsltType5);

                MEMSET (&asNetNum, 0xff, sizeof (tIPADDR));
                MEMSET (&asAddrMask, 0xff, sizeof (tIPADDRMASK));
                MEMSET (&areaId, 0xff, sizeof (tAreaId));
                MEMSET (&aMetric, 0xff, (sizeof (tMetric) * OSPF_MAX_METRIC));
                pAsExtRange =
                    (tAsExtAddrRange *) (VOID *) pLsaInfo->pLsaDesc->
                    pAssoPrimitive;
                if (pAsExtRange != NULL)
                {
                    MEMCPY (&asNetNum, pAsExtRange->ipAddr, sizeof (tIPADDR));
                    MEMCPY (&asAddrMask, pAsExtRange->ipAddrMask,
                            sizeof (tIPADDRMASK));
                    MEMCPY (&areaId, pAsExtRange->areaId, sizeof (tAreaId));
                    MEMCPY (&aMetric, &(pAsExtRange->aMetric),
                            (sizeof (tMetric) * OSPF_MAX_METRIC));
                }
                OSPF_RED_PUT_N_BYTE (*pPMsg, &asNetNum,
                                     *pu2MsgLen, sizeof (tIPADDR));
                OSPF_RED_PUT_N_BYTE (*pPMsg, &asAddrMask,
                                     *pu2MsgLen, sizeof (tIPADDRMASK));
                OSPF_RED_PUT_N_BYTE (*pPMsg, &areaId,
                                     *pu2MsgLen, sizeof (tAreaId));
                OSPF_RED_PUT_N_BYTE (*pPMsg, &aMetric,
                                     *pu2MsgLen,
                                     (sizeof (tMetric) * OSPF_MAX_METRIC));
                break;

            case COND_NETWORK_SUM_LSA:
            case AS_TRNSLTD_EXT_RNG_LSA:
                pAddrRange =
                    (tAddrRange *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;
                u4AreaOffSet =
                    PTR_TO_U4 (&
                               (((tArea *) 0)->
                                aAddrRange[pAddrRange->u1AddrRangeIndex]));
                pAddrRangeArea = (tArea *) (pAddrRange - u4AreaOffSet);

                OSPF_RED_PUT_N_BYTE (*pPMsg, pAddrRangeArea->areaId,
                                     *pu2MsgLen, sizeof (tAreaId));
                OSPF_RED_PUT_N_BYTE (*pPMsg, pAddrRange->ipAddr,
                                     *pu2MsgLen, sizeof (tIPADDR));
                OSPF_RED_PUT_N_BYTE (*pPMsg, pAddrRange->ipAddrMask,
                                     *pu2MsgLen, sizeof (tIPADDRMASK));
                OSPF_RED_PUT_N_BYTE (*pPMsg, &(pAddrRange->aMetric),
                                     *pu2MsgLen,
                                     (sizeof (tMetric) * OSPF_MAX_METRIC));

                break;

            case TYPE9_OPQ_LSA:
            case TYPE10_OPQ_LSA:
            case TYPE11_OPQ_LSA:
                pOpqLsaInfo = (tOpqLSAInfo *) (VOID *)
                    pLsaInfo->pLsaDesc->pAssoPrimitive;

                OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen,
                                     pOpqLsaInfo->u4IfaceID);
                OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen,
                                     pOpqLsaInfo->u4AddrlessIf);
                OSPF_RED_PUT_4_BYTE (*pPMsg, *pu2MsgLen, pOpqLsaInfo->u4AreaID);
                OSPF_RED_PUT_2_BYTE (*pPMsg, *pu2MsgLen, pOpqLsaInfo->u2LSALen);
                OSPF_RED_PUT_1_BYTE (*pPMsg, *pu2MsgLen,
                                     pOpqLsaInfo->u1LSAStatus);
                break;
            default:
                break;
        }
    }

    gOsRtr.osRedInfo.lsaId.u1LsaType = pLsaInfo->lsaId.u1LsaType;
    IP_ADDR_COPY (gOsRtr.osRedInfo.lsaId.linkStateId,
                  pLsaInfo->lsaId.linkStateId);
    IP_ADDR_COPY (gOsRtr.osRedInfo.lsaId.advRtrId, pLsaInfo->lsaId.advRtrId);
    gOsRtr.osRedInfo.u4LsaSyncCount++;
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmGetRequiredLenForLsa                                */
/*                                                                           */
/* Description  : This function calculate the message length required to     */
/*                construct the bulk LSA message for the the LSA             */
/*                                                                           */
/* Input        : pLsaInfo - pionter to the LSA Info                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Length to construct the bulk LSA.                          */
/*                                                                           */
/*****************************************************************************/
UINT2
OspfRmGetRequiredLenForLsa (tLsaInfo * pLsaInfo)
{
    UINT2               u2Len = 0;
    INT4                i4RetVal = OSPF_FAILURE;

    u2Len = (UINT2) (sizeof (UINT2) + pLsaInfo->u2LsaLen + sizeof (UINT1) +
                     sizeof (UINT2) + sizeof (UINT4) + sizeof (UINT4));

    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case NSSA_LSA:
        case TYPE10_OPQ_LSA:
            u2Len = (UINT2) (u2Len + sizeof (tAreaId));
            break;

        case TYPE9_OPQ_LSA:
            u2Len = (UINT2) (u2Len + sizeof (tIPADDR) + sizeof (UINT4));
            break;
        default:
            break;
    }

    /* Self Originated LSA */
    i4RetVal = UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                               pLsaInfo->pOspfCxt->rtrId);

    if (i4RetVal == OSPF_EQUAL)
    {
        /* 1 byte to store the internal LSA Type */
        u2Len = (UINT2) (u2Len + sizeof (UINT1));
        switch (pLsaInfo->pLsaDesc->u1InternalLsaType)
        {
            case NETWORK_SUM_LSA:
            case ASBR_SUM_LSA:
            case INDICATION_LSA:
                u2Len = (UINT2) (u2Len + sizeof (tSummaryParam));
                break;

            case AS_EXT_LSA:
            case NSSA_LSA:
                u2Len = (UINT2) (u2Len + sizeof (UINT1));
                u2Len = (UINT2) (u2Len + sizeof (tIPADDR) + sizeof (tIPADDR));
                break;

            case COND_NETWORK_SUM_LSA:
            case AS_TRNSLTD_EXT_RNG_LSA:
                u2Len = (UINT2) (u2Len + sizeof (tAreaId) + sizeof (tIPADDR)
                                 + sizeof (tIPADDRMASK)
                                 + (sizeof (tMetric) * OSPF_MAX_METRIC));
                break;

            case COND_AS_EXT_LSA:
            case COND_NSSA_LSA:
                u2Len = (UINT2) (u2Len + sizeof (UINT1) + sizeof (tIPADDR)
                                 + sizeof (tIPADDRMASK) + sizeof (tAreaId)
                                 + (sizeof (tMetric) * OSPF_MAX_METRIC));
                break;
            case TYPE9_OPQ_LSA:
            case TYPE10_OPQ_LSA:
            case TYPE11_OPQ_LSA:
                u2Len = (UINT2) (u2Len + sizeof (UINT4) + sizeof (UINT4) +
                                 sizeof (UINT4) + sizeof (UINT2) +
                                 sizeof (UINT1));
                break;
            default:
                break;
        }
    }
    return u2Len;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OspfRmSendBulkLsaInfo                                    */
/*                                                                          */
/* Description  : This function is called when active node get the bulk     */
/*                request message or sub bulk update event. This function   */
/*                gets the next LSA corresponding to the one stored in the  */
/*                temporary data and sends to the standby node.             */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/
VOID
OspfRmSendBulkLsaInfo ()
{
    tOspfCxt           *pOspfCxt = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    UINT4               u4ContextId;
    UINT4               u4CurrentCxtId = OSPF_DEFAULT_CXT_ID;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (gOsRtr.osRedInfo.lsaId.u1LsaType == 0)
    {
        gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_LSDB_MOD;

        OSPF_EXT_TRC (OSPF_REDUNDANCY_TRC, OSPF_DEFAULT_CXT_ID,
                      "OspfRmSendBulkLsaInfo : "
                      "Bulk update started for LSU\r\n");

    }
    if (gOsRtr.osRedInfo.u4LastSynCxtId != OSPF_INVALID_CXT_ID)
    {
        u4CurrentCxtId = gOsRtr.osRedInfo.u4LastSynCxtId;
    }

    for (u4ContextId = u4CurrentCxtId; u4ContextId < u4MaxCxt; u4ContextId++)
    {
        pOspfCxt = UtilOspfGetCxt (u4ContextId);

        if (pOspfCxt == NULL)
        {
            /* Get the next context */
            OSPF_EXT_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_DEFAULT_CXT_ID,
                           "OspfRmSendBulkLsaInfo : "
                           "Context id : %u not available\r\n", u4ContextId);
            continue;
        }

        if (u4ContextId != gOsRtr.osRedInfo.u4LastSynCxtId)
        {
            /* Scanning the new context, so clear the last sync
             * LSA Id
             */
            MEMSET (gOsRtr.osRedInfo.lastSynAreaId, 0, sizeof (tAreaId));
            MEMSET (&(gOsRtr.osRedInfo.lsaId), 0, sizeof (tLsaId));
        }

        pLsaInfo =
            OsRmTxLsSendBulkUpdatesInCxt (pOspfCxt,
                                          gOsRtr.osRedInfo.lastSynAreaId,
                                          gOsRtr.osRedInfo.lsaId);
    }

    if (pLsaInfo != NULL)
    {
        /* Since pLsaInfo is not NULL, there is still more LSU to be synced to
         * the standby node. Update the global variables
         */
        gOsRtr.osRedInfo.u4LastSynCxtId = u4ContextId;
        MEMCPY (gOsRtr.osRedInfo.lastSynAreaId, pLsaInfo->pArea->areaId,
                sizeof (tAreaId));
        MEMCPY (&(gOsRtr.osRedInfo.lsaId), &(pLsaInfo->lsaId), sizeof (tLsaId));
    }
    else
    {
        /* Reinitialize the last update flags */
        OspfRmInitLsdbBulkUpdateFlags ();
        /* Update the next bulk update should be sent for Timers */
        gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_TMR_MOD;
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;
    }
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmTxLsSendBulkUpdatesInCxt                              */
/*                                                                          */
/* Description  : This function creates and send the LSU bulk updates in    */
/*                context                                                   */
/*                                                                          */
/* Input        : pOspfCxt   -   Pointer to context                         */
/*                areaId     -   Last tx area                               */
/*                lsaId      -   Last tx LSA identifier                     */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : pointer to Lsa Info / NULL                                */
/*                                                                          */
/****************************************************************************/

PRIVATE tLsaInfo   *
OsRmTxLsSendBulkUpdatesInCxt (tOspfCxt * pOspfCxt, tAreaId areaId, tLsaId lsaId)
{
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pAreaNode = NULL;
    tArea              *pArea = NULL;
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2PktLen = 0;
    INT1                i1AreaComp = 0;

    /* The following order is used to LSU bulk update transmission
     *
     * 1. Scan all the areas in the context.
     * 2. In each area, scan all the interfaces and send the LSA within
     *    each interface.
     * 3. LSA in all the interfaces in a single area will be sent at a stretch
     *    without relinquishing.
     * 4. Scan all the LSA within an area and send to the standby node. LSU
     *    within an area will be sent at a stretch without relinquishing.
     * 5. After scanning all the areas, send LSU within virtual links present
     *    in context.
     * 6. Finally scann the external LSA and send to the standby node.
     */

    if ((lsaId.u1LsaType == AS_EXT_LSA) || (lsaId.u1LsaType == TYPE11_OPQ_LSA))
    {
        pLsaInfo = OsRmTxLsSendAsBulkUpdatesInCxt (pOspfCxt, &pRmMsg,
                                                   &u2PktLen);
        if (pRmMsg != NULL)
        {
            if (OspfRmSendMsgToRm (pRmMsg, u2PktLen) == OSPF_FAILURE)
            {
                OspfRmSendBulkAbort (RM_SENDTO_FAIL);
                gOsRtr.osRedInfo.u4DynBulkUpdatStatus =
                    OSPF_RED_BLKUPDT_ABORTED;
                return NULL;
            }
        }
        return pLsaInfo;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pAreaNode, tTMO_SLL_NODE *)
    {
        pArea = (tArea *) pAreaNode;

        i1AreaComp = UtilIpAddrComp (areaId, pArea->areaId);

        if (i1AreaComp == OSPF_GREATER)
        {
            continue;
        }
        else if (i1AreaComp == OSPF_EQUAL)
        {
            /* Relinquish happens after scanning all the LSU in all the
             * interfaces in an area or after scanning all the LSU in an
             * area
             */
            if (lsaId.u1LsaType == TYPE9_OPQ_LSA)
            {
                /* Interfacce specfic LSU are transmitted to the stanby node
                 * Therefore, scan area specific LSU
                 */
                lsaId.u1LsaType = ROUTER_LSA;
            }
            else
            {
                if (lsaId.u1LsaType == 0)
                {
                    /* No LSU has been transmitted to the standby from this context
                     * Scann all the interfaces in this area and send all LSU
                     * in each interface
                     */
                    lsaId.u1LsaType = TYPE9_OPQ_LSA;
                }
                else
                {
                    lsaId.u1LsaType = ROUTER_LSA;
                }
            }
        }
        else
        {
            /* New area. So, transmit the interface specific LSA */
            lsaId.u1LsaType = TYPE9_OPQ_LSA;
        }

        /* Scan ans send all the LSA specific to interface or area */
        if (lsaId.u1LsaType == TYPE9_OPQ_LSA)
        {
            pLsaInfo = OsRmTxLsSendIntfBulkUpdatesInCxt (pArea, &pRmMsg,
                                                         &u2PktLen);

            /* Relinquish */
            if (OsRmBulkUpdtRelinquish () == OSIX_SUCCESS)
            {
                if (pRmMsg != NULL)
                {
                    RM_DATA_ASSIGN_2_BYTE (pRmMsg,
                                           OSPF_RED_MESSAGE_LENGTH_OFFSET,
                                           u2PktLen);
                    if (OspfRmSendMsgToRm (pRmMsg, u2PktLen) == OSPF_FAILURE)
                    {
                        return NULL;
                    }

                    pRmMsg = NULL;
                    u2PktLen = 0;
                }
                return pLsaInfo;
            }
            if (gOsRtr.osRedInfo.u4DynBulkUpdatStatus ==
                OSPF_RED_BLKUPDT_ABORTED)
            {
                return NULL;
            }

        }

        pLsaInfo = OsRmTxLsSendAreaBulkUpdatesInCxt (pArea, &pRmMsg, &u2PktLen);

        /* Relinquish */
        if (OsRmBulkUpdtRelinquish () == OSIX_SUCCESS)
        {
            if (pRmMsg != NULL)
            {
                RM_DATA_ASSIGN_2_BYTE (pRmMsg, OSPF_RED_MESSAGE_LENGTH_OFFSET,
                                       u2PktLen);
                if (OspfRmSendMsgToRm (pRmMsg, u2PktLen) == OSPF_FAILURE)
                {
                    return NULL;
                }

                pRmMsg = NULL;
                u2PktLen = 0;
            }
            return pLsaInfo;
        }
    }

    /* Scan the virtual links */
    if (OsRmTxLsSendVirtBulkUpdatesInCxt (pOspfCxt, &pRmMsg, &u2PktLen) == NULL)
    {
        OSPF_EXT_TRC (OSPF_REDUNDANCY_TRC, pOspfCxt->u4OspfCxtId,
                      "OsRmTxLsSendVirtBulkUpdatesInCxt : "
                      "has returned NULL\r\n");
    }
    /* Scan the external LSA */
    pLsaInfo = OsRmTxLsSendAsBulkUpdatesInCxt (pOspfCxt, &pRmMsg, &u2PktLen);

    if (pRmMsg != NULL)
    {
        RM_DATA_ASSIGN_2_BYTE (pRmMsg, OSPF_RED_MESSAGE_LENGTH_OFFSET,
                               u2PktLen);
        if (OspfRmSendMsgToRm (pRmMsg, u2PktLen) == OSPF_FAILURE)
        {
            return NULL;
        }

        pRmMsg = NULL;
        u2PktLen = 0;
    }
    return pLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmTxLsSendIntfBulkUpdatesInCxt                          */
/*                                                                          */
/* Description  : This function creates and send the LSU bulk updates in    */
/*                all the interfaces in a particular area                   */
/*                                                                          */
/* Input        : pArea         -  Pointer to Area structure                */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to Lsa Info / NULL                                */
/*                                                                          */
/****************************************************************************/

PRIVATE tLsaInfo   *
OsRmTxLsSendIntfBulkUpdatesInCxt (tArea * pArea, tRmMsg ** ppRmMsg,
                                  UINT2 *pu2Len)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pIntfNode = NULL;
    tInterface         *pInterface = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pTmpLsaInfo = NULL;    /* Any LSA to be returned */
    INT4                i4RetVal = OSIX_FAILURE;

    TMO_SLL_Scan (&(pArea->ifsInArea), pIntfNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pIntfNode);

        TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pNode);

            if (pTmpLsaInfo == NULL)
            {
                pTmpLsaInfo = pLsaInfo;
            }

            i4RetVal = OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg, pu2Len);

            if (i4RetVal == OSIX_FAILURE)
            {
                /* Error in creation or transmission of LSU bulk update */
                return NULL;
            }

            if (*ppRmMsg == NULL)
            {
                /* In this case, the pkt has been sent to the standby and
                 * the current LSA is not sent to the standby. Do not get
                 * the next LSA until transmitting this LSA
                 */
                if (OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg,
                                                 pu2Len) == OSIX_FAILURE)
                {
                    return NULL;
                }
            }

            pLsaInfo = NULL;
        }
    }

    return pTmpLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmTxLsSendAreaBulkUpdatesInCxt                          */
/*                                                                          */
/* Description  : This function creates and send the LSU bulk updates in    */
/*                a particular area                                         */
/*                                                                          */
/* Input        : pArea         -  Pointer to Area structure                */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to Lsa Info / NULL                                */
/*                                                                          */
/****************************************************************************/

PRIVATE tLsaInfo   *
OsRmTxLsSendAreaBulkUpdatesInCxt (tArea * pArea, tRmMsg ** ppRmMsg,
                                  UINT2 *pu2Len)
{
    tTMO_SLL           *pLsaLst = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pTmpLsaInfo = NULL;    /* Any LSA to be returned */
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1LsaType = 0;

    for (u1LsaType = ROUTER_LSA; u1LsaType <= TYPE10_OPQ_LSA; u1LsaType++)
    {
        if (u1LsaType == NETWORK_SUM_LSA)
        {
            pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pArea->pSummaryLsaRoot);
            while (pLsaInfo != NULL)
            {
                if (pTmpLsaInfo == NULL)
                {
                    pTmpLsaInfo = pLsaInfo;
                }

                i4RetVal =
                    OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg, pu2Len);

                if (i4RetVal == OSIX_FAILURE)
                {
                    /* Error in creation or transmission of LSU bulk update */
                    return NULL;
                }

                if (*ppRmMsg == NULL)
                {
                    /* In this case, the pkt has been sent to the standby and
                     * the current LSA is not sent to the standby. Do not get
                     * the next LSA until transmitting this LSA
                     */
                    if (OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg,
                                                     pu2Len) == OSIX_FAILURE)
                    {
                        return NULL;
                    }
                }

                pLsaInfo =
                    (tLsaInfo *) RBTreeGetNext (pArea->pSummaryLsaRoot,
                                                pLsaInfo, NULL);
            }
        }
        else
        {
            switch (u1LsaType)
            {
                case ROUTER_LSA:
                    pLsaLst = &(pArea->rtrLsaLst);
                    break;

                case NETWORK_LSA:
                    pLsaLst = &(pArea->networkLsaLst);
                    break;

                case NSSA_LSA:
                    pLsaLst = &(pArea->nssaLSALst);
                    break;

                case TYPE10_OPQ_LSA:
                    pLsaLst = &(pArea->Type10OpqLSALst);
                    break;

                default:
                    pLsaLst = NULL;
                    continue;
            }

            TMO_SLL_Scan (pLsaLst, pNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pNode);

                if (pTmpLsaInfo == NULL)
                {
                    pTmpLsaInfo = pLsaInfo;
                }

                i4RetVal =
                    OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg, pu2Len);

                if (i4RetVal == OSIX_FAILURE)
                {
                    /* Error in creation or transmission of LSU bulk update */
                    return NULL;
                }

                if (*ppRmMsg == NULL)
                {
                    /* In this case, the pkt has been sent to the standby and
                     * the current LSA is not sent to the standby. Do not get
                     * the next LSA until transmitting this LSA
                     */
                    if (OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg,
                                                     pu2Len) == OSIX_FAILURE)
                    {
                        return NULL;
                    }
                }

                pLsaInfo = NULL;
            }
        }
    }

    return pTmpLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmTxLsSendVirtBulkUpdatesInCxt                          */
/*                                                                          */
/* Description  : This function creates and send the LSU bulk updates in    */
/*                all the virtual interfaces in a particular area           */
/*                                                                          */
/* Input        : pOspfCxt      -  Pointer to context structure             */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to Lsa Info / NULL                                */
/*                                                                          */
/****************************************************************************/

PRIVATE tLsaInfo   *
OsRmTxLsSendVirtBulkUpdatesInCxt (tOspfCxt * pOspfCxt, tRmMsg ** ppRmMsg,
                                  UINT2 *pu2Len)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pIntfNode = NULL;
    tInterface         *pInterface = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pTmpLsaInfo = NULL;    /* Any LSA to be returned */
    INT4                i4RetVal = OSIX_FAILURE;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pIntfNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_LST (pIntfNode);

        TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pNode);

            if (pTmpLsaInfo == NULL)
            {
                pTmpLsaInfo = pLsaInfo;
            }

            i4RetVal = OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg, pu2Len);

            if (i4RetVal == OSIX_FAILURE)
            {
                /* Error in creation or transmission of LSU bulk update */
                return NULL;
            }

            if (*ppRmMsg == NULL)
            {
                /* In this case, the pkt has been sent to the standby and
                 * the current LSA is not sent to the standby. Do not get
                 * the next LSA until transmitting this LSA
                 */
                if (OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg,
                                                 pu2Len) == OSIX_FAILURE)
                {
                    return NULL;
                }
            }

            pLsaInfo = NULL;
        }
    }

    return pTmpLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmTxLsSendAsBulkUpdatesInCxt                            */
/*                                                                          */
/* Description  : This function creates and send the external LSU bulk      */
/*                updates in context                                        */
/*                                                                          */
/* Input        : pOspfCxt      -  Pointer to context structure             */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to Lsa Info / NULL                                */
/*                                                                          */
/****************************************************************************/

PRIVATE tLsaInfo   *
OsRmTxLsSendAsBulkUpdatesInCxt (tOspfCxt * pOspfCxt, tRmMsg ** ppRmMsg,
                                UINT2 *pu2Len)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pPrevLsaInfo = NULL;
    tLsaInfo            lsInfo;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1LsaCount = 0;

    /* Since the number of Type 11 opaque LSA will be minimum, send all
     * the opaque LSA at a stretch
     */
    MEMSET (&lsInfo, 0, sizeof (tLsaInfo));
    TMO_SLL_Scan (&(pOspfCxt->Type11OpqLSALst), pNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pNode);

        i4RetVal = OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg, pu2Len);

        if (i4RetVal == OSIX_FAILURE)
        {
            /* Error in creation or transmission of LSU bulk update */
            return NULL;
        }

        if (*ppRmMsg == NULL)
        {
            /* In this case, the pkt has been sent to the standby and
             * the current LSA is not sent to the standby. Do not get
             * the next LSA until transmitting this LSA
             */
            if (OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg,
                                             pu2Len) == OSIX_FAILURE)
            {
                return NULL;
            }
        }

        pLsaInfo = NULL;
    }
    MEMCPY (&(lsInfo.lsaId), &(gOsRtr.osRedInfo.lsaId), sizeof (tLsaId));

    while ((pLsaInfo = (tLsaInfo *) RBTreeGetNext
            (pOspfCxt->pAsExtLsaRBRoot, (tRBElem *) & (lsInfo), NULL)) != NULL)
    {
        i4RetVal = OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg, pu2Len);

        if (i4RetVal == OSIX_FAILURE)
        {
            /* Error in creation or transmission of LSU bulk update */
            return NULL;
        }
        if (*ppRmMsg == NULL)
        {
            /* In this case, the pkt has been sent to the standby and
             * the current LSA is not sent to the standby. Do not get
             * the next LSA until transmitting this LSA
             */
            if (OsRmTxLsConstructAndSendLsu (pLsaInfo, ppRmMsg,
                                             pu2Len) == OSIX_FAILURE)
            {
                return NULL;
            }
        }

        pPrevLsaInfo = pLsaInfo;
        MEMCPY (&(lsInfo.lsaId), &(pLsaInfo->lsaId), sizeof (tLsaId));
        u1LsaCount++;

        if (u1LsaCount == OSPF_RED_MAX_LSA_PER_BULK_EVENT)
        {
            /* If max LSA count is reached but current bulk LSA packet 
             * is not sent, then send it here if we need to relinquish*/
            if ((OsRmBulkUpdtRelinquish () == OSIX_SUCCESS) &&
                (*ppRmMsg != NULL))
            {
                RM_DATA_ASSIGN_2_BYTE (*ppRmMsg, OSPF_RED_MESSAGE_LENGTH_OFFSET,
                                       *pu2Len);

                i4RetVal = OspfRmSendMsgToRm (*ppRmMsg, *pu2Len);

                *ppRmMsg = NULL;
                *pu2Len = 0;
                if (i4RetVal == OSPF_FAILURE)
                {
                    OspfRmSendBulkAbort (RM_SENDTO_FAIL);
                    gOsRtr.osRedInfo.u4DynBulkUpdatStatus =
                        OSPF_RED_BLKUPDT_ABORTED;
                    return NULL;
                }

                return pPrevLsaInfo;
            }

            u1LsaCount = 0;
        }

        pLsaInfo = NULL;
    }

    UNUSED_PARAM (pPrevLsaInfo);
    return NULL;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OsRmTxLsConstructAndSendLsu                               */
/*                                                                          */
/* Description  : This function contructs and send the LSU bulk updates in  */
/*                context                                                   */
/*                                                                          */
/* Input        : pLsaInfo      -  Pointer to LSU structure                 */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PRIVATE INT4
OsRmTxLsConstructAndSendLsu (tLsaInfo * pLsaInfo, tRmMsg ** ppRmMsg,
                             UINT2 *pu2Len)
{
    INT4                i4RetVal = 0;
    UINT1               u1MsgFull = OSPF_TRUE;

    if ((IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo)) &&
        (pLsaInfo->pLsaDesc == NULL))
    {
        /* This condition occurs when the self orginated LSA needs to be 
         *  flushed and deleted from the database. so skip the LSA in bulk update.
         */
        return OSIX_SUCCESS;
    }
    if (*ppRmMsg == NULL)
    {
        *ppRmMsg = OspfRmAllocForRmMsg (OSPF_RED_IFACE_MTU);

        if (*ppRmMsg == NULL)
        {
            OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
            gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
            return OSIX_FAILURE;
        }

        OSPF_RED_PUT_1_BYTE (*ppRmMsg, *pu2Len, OSPF_RED_BULK_UPDATE_MSG);
        OSPF_RED_PUT_2_BYTE (*ppRmMsg, *pu2Len, 0);
        OSPF_RED_PUT_1_BYTE (*ppRmMsg, *pu2Len, OSPF_RED_SYNC_LSU_MSG);
    }

    u1MsgFull = OspfRmConstructLsaMsg (pLsaInfo, ppRmMsg, pu2Len);

    if (u1MsgFull == OSPF_TRUE)
    {
        RM_DATA_ASSIGN_2_BYTE (*ppRmMsg, OSPF_RED_MESSAGE_LENGTH_OFFSET,
                               *pu2Len);
        i4RetVal = OspfRmSendMsgToRm (*ppRmMsg, *pu2Len);

        *ppRmMsg = NULL;
        *pu2Len = 0;
        if (i4RetVal == OSPF_FAILURE)
        {
            OspfRmSendBulkAbort (RM_SENDTO_FAIL);
            gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
            return OSIX_FAILURE;
        }

    }

    return OSIX_SUCCESS;
}
#endif /* _OSREDBLK_ */
