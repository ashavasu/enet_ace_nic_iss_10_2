/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osagd.c,v 1.13 2017/09/21 13:48:45 siva Exp $
 *
 * Description:This file contains procedures related to aging
 *             the database.
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : AgdFlushOut                                              */
/*                                                                           */
/* Description  : The advertisements age is set to MAX_AGE and it is flushed */
/*                out.                                                       */
/*                                                                           */
/* Input        : pLsaInfo    : pointer to the advertisement               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
AgdFlushOut (tLsaInfo * pLsaInfo)
{
    tLsaTrapInfo        trapMaxAgeLsa;
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;

    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC : AgdFlushOut\n");

    OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_LSDB_TRC, u4OspfCxtId,
               "Flushing LSA LSType %d LSId %x ADV_RTR_ID %x\n",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    /* set the age field to MAX_AGE */

    pLsaInfo->u2LsaAge = MAX_AGE;
    pLsaInfo->u1LsaFlushed = OSPF_TRUE;
    OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, u4OspfCxtId,
               "FUNC :AgdFlushOut Flushing LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               pLsaInfo->u2LsaAge, pLsaInfo->lsaSeqNum);

    OSPF_CRU_BMC_WTOPDU ((pLsaInfo->pLsa + AGE_OFFSET_IN_LS_HEADER), MAX_AGE);

    if (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))
    {
        if ((pLsaInfo->pLsaDesc != NULL) &&
            (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired != OSPF_TRUE))
        {
            TmrDeleteTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer));
            pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
        }
    }

    if (IS_TRAP_ENABLED_IN_CXT (MAX_AGE_LSA_TRAP, pLsaInfo->pOspfCxt)
        && !(pLsaInfo->pOspfCxt->u1SwPeriodsElapsed < 16))
    {
        if (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA)
        {
            SET_NULL_IP_ADDR (trapMaxAgeLsa.lsdbAreaId);
        }
        else
        {
            IP_ADDR_COPY (trapMaxAgeLsa.lsdbAreaId, pLsaInfo->pArea->areaId);
        }
        trapMaxAgeLsa.u1LsdbType = pLsaInfo->lsaId.u1LsaType;
        IP_ADDR_COPY (trapMaxAgeLsa.lsdbLsid, pLsaInfo->lsaId.linkStateId);
        IP_ADDR_COPY (trapMaxAgeLsa.lsdbRtrId, pLsaInfo->lsaId.advRtrId);
        IP_ADDR_COPY (trapMaxAgeLsa.rtrId, pLsaInfo->pOspfCxt->rtrId);
        SnmpifSendTrapInCxt (pLsaInfo->pOspfCxt, MAX_AGE_LSA_TRAP,
                             &trapMaxAgeLsa);
    }

    /* 
     * In the following function call the parameter NULL refers to the
     * nbr from whom the lsa was received. This is set so because the lsa is
     * to be flooded to all neighbors.
     */

    if (pLsaInfo->u1FloodFlag == OSPF_FALSE)
    {
        LsuFloodOut (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                     NULL, pLsaInfo->pArea, OSPF_TRUE, pLsaInfo->pInterface);

        LsuSendAllFloodUpdatesInCxt (pLsaInfo->pOspfCxt);
    }

    OSPF_TRC3 (CONTROL_PLANE_TRC, u4OspfCxtId,
               " LSA LSType %d LSId %x ADV_RTR_ID %x Flooded Out\n",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    if (pLsaInfo->u4RxmtCount == 0)
    {
        LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
    }
    else
    {
        if (OsRmDynLsSendLsa (pLsaInfo) == OSIX_FAILURE)
        {
            OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                           pLsaInfo->pOspfCxt->u4OspfCxtId,
                           "Failed to sync the "
                           "originated LSU to the standby node "
                           "LSA Type : %d, Link state id : %x "
                           "Advertising router id : %x\r\n",
                           pLsaInfo->lsaId.u1LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->
                                                   lsaId.linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT : AgdFlushOut\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : agd_check_sum                                              */
/*                                                                           */
/* Description  : The checksum of the specified advertisement is verified. If*/
/*                it is found to be incorrect then the OSPF is  restarted.   */
/*                                                                           */
/* Input        : pLsaInfo    : pointer to the advertisement whose         */
/*                                      checksum is to be verified           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if checksum is correct                            */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
AgdCheckChksum (tLsaInfo * pLsaInfo)
{
    tOspfCxt           *pOspfCxt = NULL;

    /* Store the context pointer in pLsaInfo to pOspfCxt.
       pLsaInfo will be cleared while the disabling the context
       and hence, the pointer will be lost */
    pOspfCxt = pLsaInfo->pOspfCxt;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC : agd_check_sum\n");

    OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_LSDB_TRC,
               pOspfCxt->u4OspfCxtId,
               "LSA ChkSum Verification LSType %d LSId %x ADV_RTR_ID %x\n",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    if (UtilVerifyLsaFletChksum (pLsaInfo->pLsa, pLsaInfo->u2LsaLen)
        == OSPF_FALSE)
    {

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "ChkSum Failure OSPF Restarted\n");
        if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            RtrDisableInCxt (pOspfCxt);
        }
        else if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
        {
            GrDisableInCxt (pOspfCxt);
        }
        else
        {
            /* OSPF Process is undergoing GR shutdown */
            return OSPF_FAILURE;
        }
        RtrEnableInCxt (pOspfCxt);

        return OSPF_FAILURE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC, pOspfCxt->u4OspfCxtId,
              "ChkSum SUCCESS \n");

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "EXIT : agd_check_sum\n");
    return OSPF_SUCCESS;
}

/* ------------------------------------------------------------------------*/
/*                         End of the file  osagd.c                        */
/*-------------------------------------------------------------------------*/
