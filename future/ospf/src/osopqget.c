/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osopqget.c,v 1.16 2014/10/29 12:51:00 siva Exp $
 *
 * Description:This file contains the Snmp low level get routines for the 
 *             Opqaue LSA support related objects in the FutureOSPF 
 *             Enterprises MIB.  
 *             
 *
 *****************************************************************************/
#include "osinc.h"
#include "stdoslow.h"

/* Low Level GET Routine for All Opaque support related scalar Objects  */
/****************************************************************************
 Function    :  nmhGetFutOspfOpaqueOption
 Input       :  The Indices

                The Object 
                retValFutOspfOpaqueOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfOpaqueOption (INT4 *pi4RetValFutOspfOpaqueOption)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    (*pi4RetValFutOspfOpaqueOption) = (INT4) pOspfCxt->u1OpqCapableRtr;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType11LsaCount
 Input       :  The Indices

                The Object 
                retValFutOspfType11LsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType11LsaCount (UINT4 *pu4RetValFutOspfType11LsaCount)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    (*pu4RetValFutOspfType11LsaCount)
        = (UINT4) TMO_SLL_Count ((tTMO_SLL *) (&(pOspfCxt->Type11OpqLSALst)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType11LsaCksumSum
 Input       :  The Indices

                The Object 
                retValFutOspfType11LsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType11LsaCksumSum (INT4 *pi4RetValFutOspfType11LsaCksumSum)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    (*pi4RetValFutOspfType11LsaCksumSum)
        = (INT4) pOspfCxt->u4Type11OpqLSAChksumSum;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfOpaqueInterfaceTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfOpaqueInterfaceTable
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFutOspfOpaqueInterfaceTable (UINT4 u4FutOspfIfIpAddress,
                                                     INT4
                                                     i4FutOspfAddressLessIf)
{
    /* 
     * FutOspfOpaqueInterfaceTable augments the FutOspfIfTable. Hence the
     * validate index of FutOspfOpaqueInterfaceTable is equivalent to a 
     * validate index of FutOspfIfTable. 
     */
    return (nmhValidateIndexInstanceFutOspfIfTable
            (u4FutOspfIfIpAddress, i4FutOspfAddressLessIf));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfOpaqueInterfaceTable
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfOpaqueInterfaceTable (UINT4 *pu4FutOspfIfIpAddress,
                                             INT4 *pi4FutOspfAddressLessIf)
{
    /* 
     * FutOspfOpaqueInterfaceTable augments the FutOspfIfTable. Hence the
     * Get First index of FutOspfOpaqueInterfaceTable is equivalent to a 
     * Get First index of FutOspfIfTable. 
     */
    return (nmhGetFirstIndexOspfIfTable (pu4FutOspfIfIpAddress,
                                         pi4FutOspfAddressLessIf));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfOpaqueInterfaceTable
 Input       :  The Indices
                FutOspfIfIpAddress
                nextFutOspfIfIpAddress
                FutOspfAddressLessIf
                nextFutOspfAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfOpaqueInterfaceTable (UINT4 u4FutOspfIfIpAddress,
                                            UINT4 *pu4NextFutOspfIfIpAddress,
                                            INT4 i4FutOspfAddressLessIf,
                                            INT4 *pi4NextFutOspfAddressLessIf)
{
    /* 
     * FutOspfOpaqueInterfaceTable augments the FutOspfIfTable. Hence the
     * Get Next index of FutOspfOpaqueInterfaceTable is equivalent to a 
     * Get Next index of FutOspfIfTable. 
     */
    return (nmhGetNextIndexOspfIfTable
            (u4FutOspfIfIpAddress, pu4NextFutOspfIfIpAddress,
             i4FutOspfAddressLessIf, pi4NextFutOspfAddressLessIf));
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFutOspfOpaqueType9LsaCount
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfOpaqueType9LsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfOpaqueType9LsaCount (UINT4 u4FutOspfIfIpAddress,
                                  INT4 i4FutOspfAddressLessIf,
                                  UINT4 *pu4RetValFutOspfOpaqueType9LsaCount)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt,
                                      ifIpAddr, (UINT4) i4FutOspfAddressLessIf))
        != NULL)
    {
        *pu4RetValFutOspfOpaqueType9LsaCount
            =
            (UINT4)
            TMO_SLL_Count ((tTMO_SLL *) (&(pInterface->Type9OpqLSALst)));
        return SNMP_SUCCESS;
    }
    *pu4RetValFutOspfOpaqueType9LsaCount = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfOpaqueType9LsaCksumSum
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object 
                retValFutOspfOpaqueType9LsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfOpaqueType9LsaCksumSum (UINT4 u4FutOspfIfIpAddress,
                                     INT4 i4FutOspfAddressLessIf,
                                     INT4
                                     *pi4RetValFutOspfOpaqueType9LsaCksumSum)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt,
                                      ifIpAddr, (UINT4) i4FutOspfAddressLessIf))
        != NULL)
    {
        *pi4RetValFutOspfOpaqueType9LsaCksumSum
            = (INT4) pInterface->u4Type9OpqLSAChksumSum;
        return SNMP_SUCCESS;
    }
    *pi4RetValFutOspfOpaqueType9LsaCksumSum = 0;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfType9LsdbTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfType9LsdbTable
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfType9LsdbTable (UINT4
                                               u4FutOspfType9LsdbIfIpAddress,
                                               INT4
                                               i4FutOspfType9LsdbOpaqueType,
                                               UINT4 u4FutOspfType9LsdbLsid,
                                               UINT4 u4FutOspfType9LsdbRouterId)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfType9LsdbOpaqueType);
    /*
     * This routine checks the existence of a Type 9 LSA for the given set of 
     * indices. Success is returned in case of existence of a Type 9 LSA 
     * otherwise Failure is returned.
     */
    if (olsGetType9OpqLsaInCxt (pOspfCxt,
                                u4FutOspfType9LsdbIfIpAddress,
                                0,
                                u4FutOspfType9LsdbLsid,
                                u4FutOspfType9LsdbRouterId) != NULL)
    {
        /* Type 9 LSA for the given index combination exist */
        return SNMP_SUCCESS;
    }
    /* Type 9 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfType9LsdbTable
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfType9LsdbTable (UINT4 *pu4FutOspfType9LsdbIfIpAddress,
                                       INT4 *pi4FutOspfType9LsdbOpaqueType,
                                       UINT4 *pu4FutOspfType9LsdbLsid,
                                       UINT4 *pu4FutOspfType9LsdbRouterId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pIfNode;
    tTMO_SLL_NODE      *pLsaNode;
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfNode);
        pLsaNode = TMO_SLL_First (&(pInterface->Type9OpqLSALst));
        if (pLsaNode != NULL)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
            *pu4FutOspfType9LsdbIfIpAddress =
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pInterface->ifIpAddr);
            *pi4FutOspfType9LsdbOpaqueType =
                (INT4) pLsaInfo->lsaId.linkStateId[0];
            *pu4FutOspfType9LsdbLsid =
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
            *pu4FutOspfType9LsdbRouterId =
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfType9LsdbTable
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                nextFutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                nextFutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                nextFutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId
                nextFutOspfType9LsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfType9LsdbTable (UINT4 u4FutOspfType9LsdbIfIpAddress,
                                      UINT4 *pu4NextFutOspfType9LsdbIfIpAddress,
                                      INT4 i4FutOspfType9LsdbOpaqueType,
                                      INT4 *pi4NextFutOspfType9LsdbOpaqueType,
                                      UINT4 u4FutOspfType9LsdbLsid,
                                      UINT4 *pu4NextFutOspfType9LsdbLsid,
                                      UINT4 u4FutOspfType9LsdbRouterId,
                                      UINT4 *pu4NextFutOspfType9LsdbRouterId)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pIfNode;
    tTMO_SLL_NODE      *pOpqNode = NULL;
    tTMO_SLL_NODE      *pNextOpqNode = NULL;
    tLsaInfo           *pLsaInfo;
    tLsaInfo           *pNextLsaInfo = NULL;
    tIPADDR             currInterfaceAddr;
    tLINKSTATEID        currLinkStateId;
    tRouterId           currAdvRtrId;

    UINT1               u1Flag = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfType9LsdbOpaqueType);

    OSPF_CRU_BMC_DWTOPDU (currInterfaceAddr, u4FutOspfType9LsdbIfIpAddress);
    OSPF_CRU_BMC_DWTOPDU (currLinkStateId, u4FutOspfType9LsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (currAdvRtrId, u4FutOspfType9LsdbRouterId);

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfNode);

        if (TMO_SLL_Count (&(pInterface->Type9OpqLSALst)) == 0)
        {
            /* This Interface is not containing any Type9 Lsa's */
            continue;
        }
        if (u1Flag == OSPF_TRUE)
        {
            pNextOpqNode = TMO_SLL_First (&(pInterface->Type9OpqLSALst));
            break;
        }

        switch (UtilIpAddrIndComp (pInterface->ifIpAddr,
                                   pInterface->u4AddrlessIf,
                                   currInterfaceAddr, 0))

        {
            case OSPF_EQUAL:
            {
                TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pOpqNode,
                              tTMO_SLL_NODE *)
                {
                    pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pOpqNode);
                    switch (UtilLsaIdComp
                            (pLsaInfo->lsaId.u1LsaType,
                             pLsaInfo->lsaId.linkStateId,
                             pLsaInfo->lsaId.advRtrId,
                             TYPE9_OPQ_LSA, currLinkStateId, currAdvRtrId))
                    {
                        case OSPF_EQUAL:
                        {
                            u1Flag = OSPF_TRUE;
                            pNextOpqNode = TMO_SLL_Next
                                (&(pInterface->Type9OpqLSALst), pOpqNode);
                            break;
                        }
                        case OSPF_GREATER:
                        {
                            pNextOpqNode = pOpqNode;
                            u1Flag = OSPF_TRUE;
                            break;
                        }
                    }
                    if (u1Flag == OSPF_TRUE)
                    {
                        break;
                    }
                }
                break;
            }
            case OSPF_GREATER:
            {
                pNextOpqNode = TMO_SLL_First (&(pInterface->Type9OpqLSALst));
                u1Flag = OSPF_TRUE;
                break;
            }
        }

        if ((pNextOpqNode == NULL) && (u1Flag == OSPF_TRUE))
        {
            continue;
        }
        else if (pNextOpqNode != NULL)
        {
            break;
        }
    }
    if ((pNextOpqNode == NULL) || (u1Flag == OSPF_FALSE))
    {
        return SNMP_FAILURE;
    }
    pNextLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pNextOpqNode);
    *pu4NextFutOspfType9LsdbIfIpAddress =
        OSPF_CRU_BMC_DWFROMPDU (pNextLsaInfo->pInterface->ifIpAddr);
    *pi4NextFutOspfType9LsdbOpaqueType =
        (INT4) pNextLsaInfo->lsaId.linkStateId[0];
    *pu4NextFutOspfType9LsdbLsid =
        OSPF_CRU_BMC_DWFROMPDU (pNextLsaInfo->lsaId.linkStateId);
    *pu4NextFutOspfType9LsdbRouterId =
        OSPF_CRU_BMC_DWFROMPDU (pNextLsaInfo->lsaId.advRtrId);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfType9LsdbSequence
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId

                The Object 
                retValFutOspfType9LsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType9LsdbSequence (UINT4 u4FutOspfType9LsdbIfIpAddress,
                                INT4 i4FutOspfType9LsdbOpaqueType,
                                UINT4 u4FutOspfType9LsdbLsid,
                                UINT4 u4FutOspfType9LsdbRouterId,
                                INT4 *pi4RetValFutOspfType9LsdbSequence)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfType9LsdbOpaqueType);

    if ((pLsaInfo = olsGetType9OpqLsaInCxt (pOspfCxt,
                                            u4FutOspfType9LsdbIfIpAddress,
                                            0,
                                            u4FutOspfType9LsdbLsid,
                                            u4FutOspfType9LsdbRouterId)) !=
        NULL)
    {
        /* Type 9 LSA for the given index combination exist */
        *pi4RetValFutOspfType9LsdbSequence = (INT4) pLsaInfo->lsaSeqNum;
        return SNMP_SUCCESS;
    }
    /* Type 9 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType9LsdbAge
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId

                The Object 
                retValFutOspfType9LsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType9LsdbAge (UINT4 u4FutOspfType9LsdbIfIpAddress,
                           INT4 i4FutOspfType9LsdbOpaqueType,
                           UINT4 u4FutOspfType9LsdbLsid,
                           UINT4 u4FutOspfType9LsdbRouterId,
                           INT4 *pi4RetValFutOspfType9LsdbAge)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfType9LsdbOpaqueType);

    if ((pLsaInfo = olsGetType9OpqLsaInCxt (pOspfCxt,
                                            u4FutOspfType9LsdbIfIpAddress,
                                            0,
                                            u4FutOspfType9LsdbLsid,
                                            u4FutOspfType9LsdbRouterId)) !=
        NULL)
    {
        /* Type 9 LSA for the given index combination exist */
        *pi4RetValFutOspfType9LsdbAge = (INT4) (pLsaInfo->u2LsaAge);
        return SNMP_SUCCESS;
    }
    /* Type 9 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType9LsdbChecksum
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId

                The Object 
                retValFutOspfType9LsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType9LsdbChecksum (UINT4 u4FutOspfType9LsdbIfIpAddress,
                                INT4 i4FutOspfType9LsdbOpaqueType,
                                UINT4 u4FutOspfType9LsdbLsid,
                                UINT4 u4FutOspfType9LsdbRouterId,
                                INT4 *pi4RetValFutOspfType9LsdbChecksum)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfType9LsdbOpaqueType);

    if ((pLsaInfo = olsGetType9OpqLsaInCxt (pOspfCxt,
                                            u4FutOspfType9LsdbIfIpAddress,
                                            0,
                                            u4FutOspfType9LsdbLsid,
                                            u4FutOspfType9LsdbRouterId)) !=
        NULL)
    {
        /* Type 9 LSA for the given index combination exist */
        *pi4RetValFutOspfType9LsdbChecksum = (INT4) pLsaInfo->u2LsaChksum;
        return SNMP_SUCCESS;
    }
    /* Type 9 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType9LsdbAdvertisement
 Input       :  The Indices
                FutOspfType9LsdbIfIpAddress
                FutOspfType9LsdbOpaqueType
                FutOspfType9LsdbLsid
                FutOspfType9LsdbRouterId

                The Object 
                retValFutOspfType9LsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType9LsdbAdvertisement (UINT4 u4FutOspfType9LsdbIfIpAddress,
                                     INT4 i4FutOspfType9LsdbOpaqueType,
                                     UINT4 u4FutOspfType9LsdbLsid,
                                     UINT4 u4FutOspfType9LsdbRouterId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFutOspfType9LsdbAdvertisement)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfType9LsdbOpaqueType);

    if ((pLsaInfo = olsGetType9OpqLsaInCxt (pOspfCxt,
                                            u4FutOspfType9LsdbIfIpAddress,
                                            0,
                                            u4FutOspfType9LsdbLsid,
                                            u4FutOspfType9LsdbRouterId)) !=
        NULL)
    {
        /* As the SNMP implemenation supports only 
         * maximum of SNMP_MAX_OCTETSTRING_SIZE
         * We fill only upto SNMP_MAX_OCTETSTRING_SIZE*/
        pRetValFutOspfType9LsdbAdvertisement->i4_Length =
            ((pLsaInfo->u2LsaLen <= SNMP_MAX_OCTETSTRING_SIZE) ?
             pLsaInfo->u2LsaLen : SNMP_MAX_OCTETSTRING_SIZE);
        CPY_TO_SNMP (pRetValFutOspfType9LsdbAdvertisement,
                     pLsaInfo->pLsa,
                     pRetValFutOspfType9LsdbAdvertisement->i4_Length);
        return SNMP_SUCCESS;
    }
    /* Type 9 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfType11LsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfType11LsdbTable
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfType11LsdbTable (INT4
                                                i4FutOspfType11LsdbOpaqueType,
                                                UINT4 u4FutOspfType11LsdbLsid,
                                                UINT4
                                                u4FutOspfType11LsdbRouterId)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * This routine checks the existence of a Type 9 LSA for the given set of 
     * indices. Success is returned in case of existence of a Type 9 LSA 
     * otherwise Failure is returned.
     */
    if (olsGetType11OpqLsaInCxt (pOspfCxt, i4FutOspfType11LsdbOpaqueType,
                                 u4FutOspfType11LsdbLsid,
                                 u4FutOspfType11LsdbRouterId) != NULL)
    {
        /* Type 11 LSA for the given index combination exist */
        return SNMP_SUCCESS;
    }
    /* Type 11 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfType11LsdbTable
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfType11LsdbTable (INT4 *pi4FutOspfType11LsdbOpaqueType,
                                        UINT4 *pu4FutOspfType11LsdbLsid,
                                        UINT4 *pu4FutOspfType11LsdbRouterId)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLstNode = TMO_SLL_First (&pOspfCxt->Type11OpqLSALst)) != NULL)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);

        IP_ADDR_COPY (linkStateId, pLsaInfo->lsaId.linkStateId);
        IP_ADDR_COPY (advRtrId, pLsaInfo->lsaId.advRtrId);
        *pu4FutOspfType11LsdbLsid = OSPF_CRU_BMC_DWFROMPDU (linkStateId);
        *pu4FutOspfType11LsdbRouterId = OSPF_CRU_BMC_DWFROMPDU (advRtrId);
        *pi4FutOspfType11LsdbOpaqueType = pLsaInfo->lsaId.linkStateId[0];
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfType11LsdbTable
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                nextFutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                nextFutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId
                nextFutOspfType11LsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfType11LsdbTable (INT4 i4FutOspfType11LsdbOpaqueType,
                                       INT4 *pi4NextFutOspfType11LsdbOpaqueType,
                                       UINT4 u4FutOspfType11LsdbLsid,
                                       UINT4 *pu4NextFutOspfType11LsdbLsid,
                                       UINT4 u4FutOspfType11LsdbRouterId,
                                       UINT4 *pu4NextFutOspfType11LsdbRouterId)
{

    tLINKSTATEID        currLinkStateId;
    tRouterId           currAdvRtrId;
    tLINKSTATEID        nextLinkStateId;
    tRouterId           nextAdvRtrId;
    tTMO_SLL           *pLsaLst;
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLsaNode;
    UINT1               u1LsIdLen = MAX_IP_ADDR_LEN;
    UINT1               u1AdvRtrIdLen = MAX_IP_ADDR_LEN;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    /* The following assignemnt is to avoid compilation warning.
       The value of i4FutOspfType11LsdbOpaqueType is part of the
       u4FutOspfType11LsdbLsid, and hence it is not required to be
       used explicitly and not used in this routine. */
    UNUSED_PARAM(i4FutOspfType11LsdbOpaqueType);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (currLinkStateId, u4FutOspfType11LsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (currAdvRtrId, u4FutOspfType11LsdbRouterId);

    pLsaLst = &(pOspfCxt->Type11OpqLSALst);

    TMO_SLL_Scan (pLsaLst, pLsaNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

        if (GetIsNextType11LsaTableIndex (&currLinkStateId,
                                          u1LsIdLen, &currAdvRtrId,
                                          u1AdvRtrIdLen, pLsaInfo) == OSPF_TRUE)
        {
            *pi4NextFutOspfType11LsdbOpaqueType =
                pLsaInfo->lsaId.linkStateId[0];
            IP_ADDR_COPY (nextLinkStateId, pLsaInfo->lsaId.linkStateId);
            IP_ADDR_COPY (nextAdvRtrId, pLsaInfo->lsaId.advRtrId);
            *pu4NextFutOspfType11LsdbLsid =
                OSPF_CRU_BMC_DWFROMPDU (nextLinkStateId);
            *pu4NextFutOspfType11LsdbRouterId =
                OSPF_CRU_BMC_DWFROMPDU (nextAdvRtrId);

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFutOspfType11LsdbSequence
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId

                The Object 
                retValFutOspfType11LsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType11LsdbSequence (INT4 i4FutOspfType11LsdbOpaqueType,
                                 UINT4 u4FutOspfType11LsdbLsid,
                                 UINT4 u4FutOspfType11LsdbRouterId,
                                 INT4 *pi4RetValFutOspfType11LsdbSequence)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * This routine checks the existence of a Type 9 LSA for the given set of 
     * indices. Success is returned in case of existence of a Type 9 LSA 
     * otherwise Failure is returned.
     */
    if ((pLsaInfo = olsGetType11OpqLsaInCxt (pOspfCxt,
                                             i4FutOspfType11LsdbOpaqueType,
                                             u4FutOspfType11LsdbLsid,
                                             u4FutOspfType11LsdbRouterId)) !=
        NULL)
    {
        /* Type 11 LSA for the given index combination exist */
        *pi4RetValFutOspfType11LsdbSequence = (INT4) pLsaInfo->lsaSeqNum;

        return SNMP_SUCCESS;
    }
    /* Type 11 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType11LsdbAge
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId

                The Object 
                retValFutOspfType11LsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType11LsdbAge (INT4 i4FutOspfType11LsdbOpaqueType,
                            UINT4 u4FutOspfType11LsdbLsid,
                            UINT4 u4FutOspfType11LsdbRouterId,
                            INT4 *pi4RetValFutOspfType11LsdbAge)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * This routine checks the existence of a Type 9 LSA for the given set of 
     * indices. Success is returned in case of existence of a Type 9 LSA 
     * otherwise Failure is returned.
     */
    if ((pLsaInfo = olsGetType11OpqLsaInCxt (pOspfCxt,
                                             i4FutOspfType11LsdbOpaqueType,
                                             u4FutOspfType11LsdbLsid,
                                             u4FutOspfType11LsdbRouterId)) !=
        NULL)
    {
        /* Type 11 LSA for the given index combination exist */
        *pi4RetValFutOspfType11LsdbAge = (INT4) (pLsaInfo->u2LsaAge);

        return SNMP_SUCCESS;
    }
    /* Type 11 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType11LsdbChecksum
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId

                The Object 
                retValFutOspfType11LsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType11LsdbChecksum (INT4 i4FutOspfType11LsdbOpaqueType,
                                 UINT4 u4FutOspfType11LsdbLsid,
                                 UINT4 u4FutOspfType11LsdbRouterId,
                                 INT4 *pi4RetValFutOspfType11LsdbChecksum)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * This routine checks the existence of a Type 9 LSA for the given set of 
     * indices. Success is returned in case of existence of a Type 9 LSA 
     * otherwise Failure is returned.
     */
    if ((pLsaInfo = olsGetType11OpqLsaInCxt (pOspfCxt,
                                             i4FutOspfType11LsdbOpaqueType,
                                             u4FutOspfType11LsdbLsid,
                                             u4FutOspfType11LsdbRouterId)) !=
        NULL)
    {
        /* Type 11 LSA for the given index combination exist */
        *pi4RetValFutOspfType11LsdbChecksum = (INT4) (pLsaInfo->u2LsaChksum);

        return SNMP_SUCCESS;
    }
    /* Type 11 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfType11LsdbAdvertisement
 Input       :  The Indices
                FutOspfType11LsdbOpaqueType
                FutOspfType11LsdbLsid
                FutOspfType11LsdbRouterId

                The Object 
                retValFutOspfType11LsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfType11LsdbAdvertisement (INT4 i4FutOspfType11LsdbOpaqueType,
                                      UINT4 u4FutOspfType11LsdbLsid,
                                      UINT4 u4FutOspfType11LsdbRouterId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFutOspfType11LsdbAdvertisement)
{
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * This routine checks the existence of a Type 9 LSA for the given set of 
     * indices. Success is returned in case of existence of a Type 9 LSA 
     * otherwise Failure is returned.
     */
    if ((pLsaInfo = olsGetType11OpqLsaInCxt (pOspfCxt,
                                             i4FutOspfType11LsdbOpaqueType,
                                             u4FutOspfType11LsdbLsid,
                                             u4FutOspfType11LsdbRouterId)) !=
        NULL)
    {
        /* As the SNMP implemenation supports only 
         * maximum of SNMP_MAX_OCTETSTRING_SIZE 
         * We fill only upto SNMP_MAX_OCTETSTRING_SIZE*/
        pRetValFutOspfType11LsdbAdvertisement->i4_Length =
            ((pLsaInfo->u2LsaLen <= SNMP_MAX_OCTETSTRING_SIZE) ?
             pLsaInfo->u2LsaLen : SNMP_MAX_OCTETSTRING_SIZE);
        CPY_TO_SNMP (pRetValFutOspfType11LsdbAdvertisement,
                     pLsaInfo->pLsa,
                     pRetValFutOspfType11LsdbAdvertisement->i4_Length);
        return SNMP_SUCCESS;
    }

    /* Type 11 LSA for the given index combination does not exist */
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfAppInfoDbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfAppInfoDbTable
 Input       :  The Indices
                FutOspfAppInfoDbAppid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFutOspfAppInfoDbTable (INT4 i4FutOspfAppInfoDbAppid)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }

    if (pAppInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else if (pAppInfo->u1Status == OSPF_VALID)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfAppInfoDbTable
 Input       :  The Indices
                FutOspfAppInfoDbAppid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfAppInfoDbTable (INT4 *pi4FutOspfAppInfoDbAppid)
{
    UINT1               u1Count;
    tAppInfo           *pAppInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1Count = 0; u1Count < MAX_APP_REG; u1Count++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1Count);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u1Status == OSPF_VALID)
            {
                *pi4FutOspfAppInfoDbAppid = pAppInfo->u4AppId;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfAppInfoDbTable
 Input       :  The Indices
                FutOspfAppInfoDbAppid
                nextFutOspfAppInfoDbAppid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfAppInfoDbTable (INT4 i4FutOspfAppInfoDbAppid,
                                      INT4 *pi4NextFutOspfAppInfoDbAppid)
{
    UINT1               u1Count;
    tAppInfo           *pAppInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1Count = (UINT1) i4FutOspfAppInfoDbAppid; u1Count < (MAX_APP_REG - 1);
         u1Count++)
    {

        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, (u1Count + 1));
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u1Status == OSPF_VALID)
            {
                *pi4NextFutOspfAppInfoDbAppid = pAppInfo->u4AppId;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbOpaqueType
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbOpaqueType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbOpaqueType (INT4 i4FutOspfAppInfoDbAppid,
                                  INT4 *pi4RetValFutOspfAppInfoDbOpaqueType)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }

    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pi4RetValFutOspfAppInfoDbOpaqueType = (INT4) (pAppInfo->u1OpqType);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbLsaTypesSupported
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbLsaTypesSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbLsaTypesSupported (INT4 i4FutOspfAppInfoDbAppid,
                                         INT4
                                         *pi4RetValFutOspfAppInfoDbLsaTypesSupported)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }
    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pi4RetValFutOspfAppInfoDbLsaTypesSupported
            = (INT4) (pAppInfo->u1LSATypesRegistered);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbType9Gen
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbType9Gen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbType9Gen (INT4 i4FutOspfAppInfoDbAppid,
                                UINT4 *pu4RetValFutOspfAppInfoDbType9Gen)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }
    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pu4RetValFutOspfAppInfoDbType9Gen = (UINT4) (pAppInfo->Type9LSAGen);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbType9Rcvd
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbType9Rcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbType9Rcvd (INT4 i4FutOspfAppInfoDbAppid,
                                 UINT4 *pu4RetValFutOspfAppInfoDbType9Rcvd)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }
    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pu4RetValFutOspfAppInfoDbType9Rcvd = (UINT4) (pAppInfo->Type9LSARcvd);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbType10Gen
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbType10Gen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbType10Gen (INT4 i4FutOspfAppInfoDbAppid,
                                 UINT4 *pu4RetValFutOspfAppInfoDbType10Gen)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }
    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pu4RetValFutOspfAppInfoDbType10Gen = (UINT4) (pAppInfo->Type10LSAGen);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbType10Rcvd
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbType10Rcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbType10Rcvd (INT4 i4FutOspfAppInfoDbAppid,
                                  UINT4 *pu4RetValFutOspfAppInfoDbType10Rcvd)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }

    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pu4RetValFutOspfAppInfoDbType10Rcvd =
            (UINT4) (pAppInfo->Type10LSARcvd);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbType11Gen
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbType11Gen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbType11Gen (INT4 i4FutOspfAppInfoDbAppid,
                                 UINT4 *pu4RetValFutOspfAppInfoDbType11Gen)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }
    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pu4RetValFutOspfAppInfoDbType11Gen = (UINT4) (pAppInfo->Type11LSAGen);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfAppInfoDbType11Rcvd
 Input       :  The Indices
                FutOspfAppInfoDbAppid

                The Object 
                retValFutOspfAppInfoDbType11Rcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAppInfoDbType11Rcvd (INT4 i4FutOspfAppInfoDbAppid,
                                  UINT4 *pu4RetValFutOspfAppInfoDbType11Rcvd)
{
    UINT1               u1AppIndx;
    tAppInfo           *pAppInfo = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u1AppIndx = 0; u1AppIndx < MAX_APP_REG; u1AppIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u4AppId == (UINT4) i4FutOspfAppInfoDbAppid)
            {
                break;
            }
        }
    }
    /* Value is reduced by 1, since the information is stored in  an array.  */
    if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
    {

        /* Application with the given ID is available and is valid. */
        *pu4RetValFutOspfAppInfoDbType11Rcvd =
            (UINT4) (pAppInfo->Type11LSARcvd);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* ChangeForPorting - Application is always expected to provide Area Id for */
/* Type10 Opq Lsa. So OSPF_TRUE is returned always. Function is stub.       */
/****************************************************************************
 Function    :  nmhGetFutOspfAreaIDValid
 Input       :  The Indices

                The Object 
                retValFutOspfAreaIDValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfAreaIDValid (INT4 *pi4RetValFutOspfAreaIDValid)
{
    (*pi4RetValFutOspfAreaIDValid) = OSPF_TRUE;
    return SNMP_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                          End of file osopqget.c                            */
/*----------------------------------------------------------------------------*/
