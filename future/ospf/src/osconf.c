/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osconf.c,v 1.110 2018/02/02 09:47:36 siva Exp $
 *
 * Description:This file contains the Snmp low level set 
 *             routines
 *
 *******************************************************************/

#include "osinc.h"
#include "ospfcli.h"
#include "fsmioscli.h"
#include "fsosmicli.h"
#include "fsstdmcli.h"
#include "fsmistdospfcli.h"
#include "stdoslow.h"
#include "fsostlow.h"
#include "ospftlow.h"

#if defined (LNXIP4_WANTED) && defined (L3_SWITCHING_WANTED)
#include "ipnp.h"
#endif

/****************************************************************************
 Function    :  nmhSetOspfRouterId
 Input       :  The Indices

                The Object 
                setValOspfRouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfRouterId (UINT4 u4SetValOspfRouterId)
{
    tRouterId           rtrId;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4OspfCxtId = gOsRtr.u4OspfCxtId;
    UINT4               u4SeqNum = 0;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC : nmhSetOspfRouterId\n");

    OSPF_CRU_BMC_DWTOPDU (rtrId, u4SetValOspfRouterId);

    if ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL)
    {
        return SNMP_FAILURE;
    }
    /*
     * If the router id of a router changes the protocol is brought down and
     * restarted after setting the new router id.
     */
    if (UtilIpAddrComp (pOspfCxt->rtrId, rtrId) != OSPF_EQUAL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfRouterId;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfRouterId) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        if (pOspfCxt->admnStat == OSPF_ENABLED)
        {
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {

                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", gOsRtr.u4OspfCxtId,
                                  u4SetValOspfRouterId));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_CHNG_ROUTERID;
            IP_ADDR_COPY (&(pOspfIfParam->param.rtrId_param.rtrId), &(rtrId));
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", gOsRtr.u4OspfCxtId,
                                  u4SetValOspfRouterId));
#endif
                return SNMP_FAILURE;
            }
        }
        else
        {
            IP_ADDR_COPY (&(pOspfCxt->rtrId), &(rtrId));
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", gOsRtr.u4OspfCxtId,
                          u4SetValOspfRouterId));
#endif
    }
#ifdef RM_WANTED
    if ((RmGetNodeState () == RM_ACTIVE) ||
        (RM_STATIC_CONFIG_IN_PROGRESS != RmGetStaticConfigStatus ()))
    {
#endif
        /* Previously router-id is selected dynamically 
         * now routerid status is updated as permanent */
        if ((pOspfCxt->i4RouterIdStatus != ROUTERID_PERMANENT) &&
            (u4SetValOspfRouterId != ZERO))
        {
            /* update routerid status and notify to snmp */
            pOspfCxt->i4RouterIdStatus = ROUTERID_PERMANENT;
#ifdef SNMP_2_WANTED
            MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIOspfRouterIdPermanence;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIOspfRouterIdPermanence) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 1;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.i2Rsvd = 0;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              gOsRtr.u4OspfCxtId, ROUTERID_PERMANENT));
#endif
        }
#ifdef RM_WANTED
    }
#endif
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : nmhSetOspfRouterId : SNMP_SUCCESS\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfAdminStat
 Input       :  The Indices

                The Object 
                setValOspfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAdminStat (INT4 i4SetValOspfAdminStat)
{
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = NULL;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tRouterId           rtrId;

    OSPF_GBL_TRC1 (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                   "FUNC : nmhSetOspfAdminStat OspfAdminStat :%d\n",
                   i4SetValOspfAdminStat);
    OSPF_CXT_LOCK ();

    pOspfCxt = gOsRtr.pOspfCxt;
    if (pOspfCxt == NULL)
    {
        OSPF_CXT_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pOspfCxt->admnStat != (UINT1) i4SetValOspfAdminStat)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {

            OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
            OSPF_CXT_UNLOCK ();
            return SNMP_FAILURE;
        }
        if (pOspfCxt->u1RestartExitReason != OSPF_RESTART_INPROGRESS)
        {
            if (pOspfCxt->i4RouterIdStatus == ROUTERID_DYNAMIC)
            {
                MEMSET (&rtrId, 0, sizeof (tRouterId));
                if (UtilSelectOspfRouterId (gOsRtr.u4OspfCxtId,
                                            &rtrId) == OSPF_SUCCESS)
                {
                    IP_ADDR_COPY (&(pOspfCxt->rtrId), &(rtrId));
                }
            }
        }
        pOspfIfParam->u1OpCode = OSPF_SET_PROTOCOL_STATUS;
        pOspfIfParam->param.admnStatus.bOspfStatus =
            (UINT1) i4SetValOspfAdminStat;
        pOspfIfParam->pOspfCxt = pOspfCxt;
        pOspfIfParam->u4OspfCxtId = gOsRtr.u4OspfCxtId;

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfAdminStat;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfAdminStat) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValOspfAdminStat));
#endif
            OSPF_CXT_UNLOCK ();
            return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValOspfAdminStat));
#endif

        if (i4SetValOspfAdminStat == OSPF_ENABLED &&
            (gOsRtr.osRedInfo.u4OsRmState == OSPF_RED_INIT))
        {
            RtrEnableInCxt (pOspfCxt);
        }

    }

    OSPF_CXT_UNLOCK ();
    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC : nmhSetOspfAdminStat: SNMP_SUCCESS \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfASBdrRtrStatus
 Input       :  The Indices

                The Object 
                setValOspfASBdrRtrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfASBdrRtrStatus (INT4 i4SetValOspfASBdrRtrStatus)
{
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC1 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "FUNC :nmhSetOspfASBdrRtrStatus OspfASBdrRtrStatus :%d\n",
               i4SetValOspfASBdrRtrStatus);

    if (pOspfCxt->bAsBdrRtr != (UINT1) i4SetValOspfASBdrRtrStatus)
    {
        if (((UINT1) i4SetValOspfASBdrRtrStatus == OSPF_TRUE)
            && (IS_INTERNAL_RTR_IN_STUB_AREA_IN_CXT (pOspfCxt)))
        {
            return SNMP_FAILURE;
        }
        else
        {
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {

                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSSPF_SNMP_IF_MSG_ALLOC Failure\n");

                return SNMP_FAILURE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfASBdrRtrStatus;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfASBdrRtrStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 1;
#endif
            pOspfIfParam->u1OpCode = OSPF_SET_AS_BDR_RTR_STATUS;
            pOspfIfParam->param.asbr_status.bStatus =
                (UINT1) i4SetValOspfASBdrRtrStatus;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
                i1RetVal = SNMP_FAILURE;
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = i1RetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValOspfASBdrRtrStatus));
#endif
        }
    }

    OSPF_TRC1 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "EXIT :nmhSetOspfASBdrRtrStatus : %d \n", i1RetVal);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetOspfTOSSupport
 Input       :  The Indices

                The Object 
                setValOspfTOSSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfTOSSupport (INT4 i4SetValOspfTOSSupport)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

#ifndef TOS_SUPPORT

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC1 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "FUNC :nmhSetOspfTOSSupport OspfTOSSupport:%d\n",
               i4SetValOspfTOSSupport);

    if (i4SetValOspfTOSSupport != OSPF_FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->bTosCapability != (UINT1) i4SetValOspfTOSSupport)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfTOSSupport;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfTOSSupport) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        pOspfIfParam->u1OpCode = OSPF_SET_TOS_SUPPORT;
        pOspfIfParam->param.tos_support.bTosStatus =
            (UINT1) i4SetValOspfTOSSupport;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValOspfTOSSupport));
#endif
            return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValOspfTOSSupport));
#endif
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "EXIT :nmhSetOspfTOSSupport: SNMP_SUCCESS \n");
    return SNMP_SUCCESS;
#endif /*   TOS Support  */
}

/****************************************************************************
 Function    :  nmhSetOspfExtLsdbLimit
 Input       :  The Indices

                The Object 
                setValOspfExtLsdbLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfExtLsdbLimit (INT4 i4SetValOspfExtLsdbLimit)
{

    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->i4ExtLsdbLimit != i4SetValOspfExtLsdbLimit)
    {
        pOspfCxt->i4ExtLsdbLimit = i4SetValOspfExtLsdbLimit;

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfExtLsdbLimit;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfExtLsdbLimit) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        pOspfIfParam->u1OpCode = OSPF_SET_EXT_LSDB_LIMIT;
        pOspfIfParam->pOspfCxt = pOspfCxt;
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            i1RetVal = SNMP_FAILURE;
        }

#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = i1RetVal;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValOspfExtLsdbLimit));
#endif
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetOspfMulticastExtensions
 Input       :  The Indices

                The Object 
                setValOspfMulticastExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfMulticastExtensions (INT4 i4SetValOspfMulticastExtensions)
{
    UNUSED_PARAM (i4SetValOspfMulticastExtensions);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfExitOverflowInterval
 Input       :  The Indices

                The Object 
                setValOspfExitOverflowInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfExitOverflowInterval (INT4 i4SetValOspfExitOverflowInterval)
{

    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u4ExitOverflowInterval !=
        (UINT4) i4SetValOspfExitOverflowInterval)
    {
        pOspfCxt->u4ExitOverflowInterval =
            (UINT4) i4SetValOspfExitOverflowInterval;
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfExitOverflowInterval;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfExitOverflowInterval) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        pOspfIfParam->u1OpCode = OSPF_SET_EXIT_OVERFLOW_INT;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValOspfExitOverflowInterval));
#endif
            return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValOspfExitOverflowInterval));
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfDemandExtensions
 Input       :  The Indices

                The Object 
                setValOspfDemandExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfDemandExtensions (INT4 i4SetValOspfDemandExtensions)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfDemandExtensions;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIStdOspfDemandExtensions) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt->bDemandExtension = (UINT1) i4SetValOspfDemandExtensions;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValOspfDemandExtensions));
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetOspfAuthType
 Input       :  The Indices
                OspfAreaId

                The Object 
                setValOspfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAuthType (UINT4 u4OspfAreaId, INT4 u4SetValOspfAuthType)
{

    UNUSED_PARAM (u4OspfAreaId);
    UNUSED_PARAM (u4SetValOspfAuthType);
/*  Authentication not supported */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfImportAsExtern
 Input       :  The Indices
                OspfAreaId

                The Object 
                setValOspfImportAsExtern
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfImportAsExtern (UINT4 u4OspfAreaId, INT4 i4SetValOspfImportAsExtern)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfQMsg          *pMessage;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC2 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "FUNC:nmhSetOspfImportAsExtern OspfAreaId :%u "
               "OspfImportAsExtern %d\n",
               u4OspfAreaId, i4SetValOspfImportAsExtern);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
    if ((UtilIpAddrComp (areaId, gNullIpAddr) == OSPF_EQUAL) &&
        (i4SetValOspfImportAsExtern != NORMAL_AREA))
    {
        CLI_SET_ERR (CLI_OSPF_BACKBONE_AREA_ERR);
        return SNMP_FAILURE;
    }

    if (UtilIpAddrComp (areaId, gNullIpAddr) != OSPF_EQUAL)
    {
        pArea = GetFindAreaInCxt (pOspfCxt, &(areaId));
        if (pArea == NULL)
            return SNMP_FAILURE;

        if (pArea->u4AreaType == (UINT4) i4SetValOspfImportAsExtern)
            return SNMP_SUCCESS;
        if ((pArea->u4AreaType == NORMAL_AREA) &&
            ((i4SetValOspfImportAsExtern == STUB_AREA) ||
             (i4SetValOspfImportAsExtern == NSSA_AREA)))
        {
            if (UtilFindIsTransitArea (pArea) == OSPF_TRUE)
            {
                return SNMP_FAILURE;
            }
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfImportAsExtern;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfImportAsExtern) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 2;
#endif
        /* Since no interface is attached, just set the area Type and return */
        pArea->u4AreaType = i4SetValOspfImportAsExtern;

        if (pArea->u4AreaType == NSSA_AREA)
        {
            pArea->u4NssaTrnsltrStbltyInterval = NSSA_TRNSLTR_STBLTY_INTRVL;
            pArea->u1NssaTrnsltrRole = TRNSLTR_ROLE_CANDIDATE;
            pArea->u4DfInfOriginate = DEFAULT_INFO_ORIGINATE;
            pArea->areaOptions = N_BIT_MASK;
            pOspfCxt->u4NssaAreaCount++;
        }
        else if (pArea->u4AreaType == NORMAL_AREA)
            pArea->areaOptions = E_BIT_MASK;
        else
            pArea->areaOptions = RAG_NO_CHNG;

        /* Invoke AreaChangeAreaType */
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4OspfAreaId, i4SetValOspfImportAsExtern));
#endif
            return SNMP_FAILURE;
        }

        pOspfIfParam->u1OpCode = OSPF_CHNG_AREA_TYPE;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
        pOspfIfParam->param.sig_param.u1Type =
            (UINT1) i4SetValOspfImportAsExtern;
        pOspfIfParam->pOspfCxt = pOspfCxt;
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4OspfAreaId, i4SetValOspfImportAsExtern));
#endif
            return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                          u4OspfAreaId, i4SetValOspfImportAsExtern));
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfAreaSummary
 Input       :  The Indices
                OspfAreaId

                The Object 
                setValOspfAreaSummary
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaSummary (UINT4 u4OspfAreaId, INT4 i4SetValOspfAreaSummary)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);

    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);

    if (pArea == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not Present\n");
        return SNMP_FAILURE;
    }

    if (pArea->u1SummaryFunctionality == (UINT1) i4SetValOspfAreaSummary)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfAreaSummary;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfAreaSummary) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
#endif
    pOspfIfParam->u1OpCode = OSPF_CHANGE_AREA_SUMMARY;
    pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    pArea->u1SummaryFunctionality = (UINT1) i4SetValOspfAreaSummary;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        i1RetVal = SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                      u4OspfAreaId, i4SetValOspfAreaSummary));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetOspfAreaStatus
 Input       :  The Indices
                OspfAreaId

                The Object 
                setValOspfAreaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaStatus (UINT4 u4OspfAreaId, INT4 i4SetValOspfAreaStatus)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfAreaStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfAreaStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC2 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "FUNC:nmhSetOspfAreaStatus OspfAreaId :%u OspfAreaStatus: %d\n",
               u4OspfAreaId, i4SetValOspfAreaStatus);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaId);
    switch ((UINT1) i4SetValOspfAreaStatus)
    {

        case CREATE_AND_GO:
            /*
             * Since this table do not have any critical variables it can Create
             * the row and set the i4SetValOspfAreaStatus to ACTIVE
             */
            if ((pArea = AreaCreateInCxt (pOspfCxt, &areaId)) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "Area Create Failed\n");
                return SNMP_FAILURE;
            }
            pArea->areaStatus = ACTIVE;

            if (pOspfCxt->admnStat == OSPF_ENABLED)
            {

                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                    KW_FALSEPOSITIVE_FIX (pArea);
                    KW_FALSEPOSITIVE_FIX1 (pArea->pNbrTbl->SemId);
                    KW_FALSEPOSITIVE_FIX2 (pArea->pSummaryLsaRoot->SemId);
                    AreaDelete (pArea);
                    return SNMP_FAILURE;
                }

                pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
                pOspfIfParam->param.sig_param.u1Type = SIG_NEW_AREA_ATTACHED;
                pOspfIfParam->pOspfCxt = pOspfCxt;
                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
                    KW_FALSEPOSITIVE_FIX (pArea);
                    KW_FALSEPOSITIVE_FIX1 (pArea->pNbrTbl->SemId);
                    KW_FALSEPOSITIVE_FIX2 (pArea->pSummaryLsaRoot->SemId);
                    AreaDelete (pArea);
                    return SNMP_FAILURE;
                }
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfAreaId, i4SetValOspfAreaStatus));
#endif
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfAreaId, ACTIVE));
#endif
            KW_FALSEPOSITIVE_FIX (pArea);
            KW_FALSEPOSITIVE_FIX1 (pArea->pNbrTbl->SemId);
            KW_FALSEPOSITIVE_FIX2 (pArea->pSummaryLsaRoot->SemId);
            return SNMP_SUCCESS;

        case CREATE_AND_WAIT:
            if ((pArea = AreaCreateInCxt (pOspfCxt, &areaId)) != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
                pArea->areaStatus = NOT_IN_SERVICE;

                KW_FALSEPOSITIVE_FIX (pArea);
                KW_FALSEPOSITIVE_FIX1 (pArea->pNbrTbl->SemId);
                KW_FALSEPOSITIVE_FIX2 (pArea->pSummaryLsaRoot->SemId);
                break;
            }
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "Area Create Failure\n");
            CLI_SET_ERR (CLI_OSPF_NO_FREE_ENTRY);
            return SNMP_FAILURE;

        case ACTIVE:
            pArea = GetFindAreaInCxt (pOspfCxt, &(areaId));
            if (pArea == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pArea->areaStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pArea->areaStatus = ACTIVE;

            if (pOspfCxt->admnStat == OSPF_ENABLED)
            {
                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                    return SNMP_FAILURE;
                }
                pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
                pOspfIfParam->param.sig_param.u1Type = SIG_NEW_AREA_ATTACHED;
                pOspfIfParam->pOspfCxt = pOspfCxt;

                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
                    return SNMP_FAILURE;
                }
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            break;

        case DESTROY:
            pArea = GetFindAreaInCxt (pOspfCxt, &(areaId));
            if (pArea == NULL)
            {
                return SNMP_SUCCESS;
            }
            if ((TMO_SLL_Count (&(pArea->ifsInArea)) == 0)
                && (UtilFindIsTransitArea (pArea) == OSPF_FALSE))
            {
                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                    return SNMP_FAILURE;
                }

                RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);

                pOspfIfParam->u1OpCode = OSPF_DELETE_AREA;
                pOspfIfParam->param.area_param.pArea = pArea;
                pOspfIfParam->pOspfCxt = pOspfCxt;

                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfAreaId, i4SetValOspfAreaStatus));
#endif
                    return SNMP_FAILURE;
                }

                break;
            }
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                      u4OspfAreaId, i4SetValOspfAreaStatus));
#endif
    KW_FALSEPOSITIVE_FIX1 (pArea->pNbrTbl->SemId);
    KW_FALSEPOSITIVE_FIX2 (pArea->pSummaryLsaRoot->SemId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfStubMetric
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                setValOspfStubMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfStubMetric (UINT4 u4OspfStubAreaId, INT4 i4OspfStubTOS,
                      INT4 i4SetValOspfStubMetric)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfStubMetric\n");

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);
    if (UtilIpAddrComp (areaId, gNullIpAddr) != OSPF_EQUAL)
    {
        if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId, "Area Not Found \n");
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfStubMetric;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfStubMetric) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
            u4Value = (UINT4) i4SetValOspfStubMetric;

        if (pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
            rowStatus == ACTIVE)
        {
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfStubAreaId, i4OspfStubTOS,
                                  i4SetValOspfStubMetric));
#endif
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
            pOspfIfParam->param.sig_param.u1Type = DEFAULT_NETWORK_SUM_LSA;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfStubAreaId, i4OspfStubTOS,
                                  i4SetValOspfStubMetric));
#endif
                return SNMP_FAILURE;
            }

        }
        else
        {
            pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                rowMask =
                pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                rowMask | STUB_METRIC_MASK;
            if (pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                rowMask == STUB_METRIC_MASK)
            {
                pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus =
                    NOT_IN_SERVICE;
            }
            else
            {
                pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus =
                    NOT_READY;
            }
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfStubAreaId, i4OspfStubTOS,
                          i4SetValOspfStubMetric));
#endif
        return SNMP_SUCCESS;
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetOspfStubMetric : SNMP_FAILURE \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfStubStatus
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                setValOspfStubStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfStubStatus (UINT4 u4OspfStubAreaId, INT4 i4OspfStubTOS,
                      INT4 i4SetValOspfStubStatus)
{
    tAreaId             areaId;
    tArea              *pArea;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfStubStatus\n");

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);

    if (UtilIpAddrComp (areaId, gNullIpAddr) != OSPF_EQUAL)
    {
        if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
        {
            if (i4SetValOspfStubStatus == DESTROY)
            {
                return SNMP_SUCCESS;
            }
            else
            {

                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, "Area Not Found \n");
                return SNMP_FAILURE;
            }
        }

        if ((pArea->u4AreaType != NSSA_AREA) &&
            (pArea->u4AreaType != STUB_AREA))
        {
            return SNMP_FAILURE;
        }

        if ((i4SetValOspfStubStatus == DESTROY) &&
            (!(IS_VALID_TOS_VALUE (i4OspfStubTOS))))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfStubStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfStubStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        switch ((UINT1) i4SetValOspfStubStatus)
        {

            case CREATE_AND_GO:
                if ((pInterface =
                     IfGetMinCostIfInArea ((UINT1)
                                           DECODE_TOS ((INT1) i4OspfStubTOS),
                                           pArea)) == NULL)
                {
                    OSPF_TRC (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId,
                              "No If configured to the Area\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }

                if (gu1DefCostCiscoCompat == OSPF_FALSE)
                {

                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].u4Value =
                        pInterface->aIfOpCost[DECODE_TOS
                                              ((INT1) i4OspfStubTOS)].u4Value;
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].rowStatus =
                        ACTIVE;
                }
                else
                {
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].u4Value =
                        STUB_NSSA_DEFAULT_COST;
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].rowStatus =
                        ACTIVE;
                }

                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {

                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }

                pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
                pOspfIfParam->param.sig_param.u1Type = DEFAULT_NETWORK_SUM_LSA;
                pOspfIfParam->pOspfCxt = pOspfCxt;
                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }

                break;

            case CREATE_AND_WAIT:
                if ((pInterface =
                     IfGetMinCostIfInArea ((UINT1)
                                           DECODE_TOS ((INT1) i4OspfStubTOS),
                                           pArea)) == NULL)
                {
                    pArea->aStubDefaultCost[DECODE_TOS ((INT1)
                                                        i4OspfStubTOS)].
                        rowMask = 0;
                    pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                        rowStatus = NOT_READY;
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_SUCCESS;
                }

                if (gu1DefCostCiscoCompat == OSPF_FALSE)
                {
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].u4Value =
                        pInterface->aIfOpCost[DECODE_TOS
                                              ((INT1) i4OspfStubTOS)].u4Value;
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].rowStatus =
                        NOT_READY;
                }
                else
                {
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].u4Value =
                        STUB_NSSA_DEFAULT_COST;
                    pArea->aStubDefaultCost[DECODE_TOS
                                            ((INT1) i4OspfStubTOS)].rowStatus =
                        NOT_READY;

                }
                break;

            case ACTIVE:

                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {

                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }

                pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
                pOspfIfParam->param.sig_param.u1Type = DEFAULT_NETWORK_SUM_LSA;
                pOspfIfParam->pOspfCxt = pOspfCxt;
                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }
                pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus =
                    ACTIVE;

                break;

            case DESTROY:

                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {

                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }

                pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
                pOspfIfParam->param.sig_param.u1Type = DEFAULT_NETWORK_SUM_LSA;
                pOspfIfParam->pOspfCxt = pOspfCxt;
                pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus =
                    INVALID;

                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfStubAreaId, i4OspfStubTOS,
                                      i4SetValOspfStubStatus));
#endif
                    return SNMP_FAILURE;
                }

                break;

            default:
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfStubAreaId, i4OspfStubTOS,
                                  i4SetValOspfStubStatus));
#endif
                return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfStubAreaId, i4OspfStubTOS,
                          i4SetValOspfStubStatus));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/**************************************************************
 Function    :  nmhSetOspfStubMetricType
 Input       :  The Indices
                OspfStubAreaId
                OspfStubTOS

                The Object 
                setValOspfStubMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************/
INT1
nmhSetOspfStubMetricType (UINT4 u4OspfStubAreaId, INT4 i4OspfStubTOS,
                          INT4 i4SetValOspfStubMetricType)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfStubMetricType\n");

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfStubAreaId);

    if (UtilIpAddrComp (areaId, gNullIpAddr) != OSPF_EQUAL)
    {
        if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId, "Area Not Found \n");
            return SNMP_FAILURE;
        }

        if ((pArea->u4AreaType != NSSA_AREA) &&
            (pArea->u4AreaType != STUB_AREA))
        {
            return SNMP_FAILURE;
        }

        if ((pArea->u4AreaType == STUB_AREA) &&
            ((i4SetValOspfStubMetricType == TYPE1EXT_METRIC) ||
             (i4SetValOspfStubMetricType == TYPE2EXT_METRIC)))
            return SNMP_FAILURE;

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfStubMetricType;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfStubMetricType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].u4MetricType
            = (UINT4) i4SetValOspfStubMetricType;

        if (pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
            rowStatus == ACTIVE)
        {
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfStubAreaId, i4OspfStubTOS,
                                  i4SetValOspfStubMetricType));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
            pOspfIfParam->param.sig_param.u1Type = DEFAULT_NETWORK_SUM_LSA;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfStubAreaId, i4OspfStubTOS,
                                  i4SetValOspfStubMetricType));
#endif
                return SNMP_FAILURE;
            }

        }
        else
        {
            pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                rowMask =
                pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                rowMask | STUB_METRIC_TYPE_MASK;
            if (pArea->aStubDefaultCost[DECODE_TOS ((INT1) i4OspfStubTOS)].
                rowMask == STUB_METRIC_TYPE_MASK)
            {
                pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus =
                    NOT_IN_SERVICE;
            }
            else
            {
                pArea->aStubDefaultCost[DECODE_TOS
                                        ((INT1) i4OspfStubTOS)].rowStatus =
                    NOT_READY;
            }
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfStubAreaId, i4OspfStubTOS,
                          i4SetValOspfStubMetricType));
#endif
        return SNMP_SUCCESS;
    }
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfStubMetricType\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfAreaRangeMask
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                setValOspfAreaRangeMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaRangeMask (UINT4 u4OspfAreaRangeAreaId,
                         UINT4 u4OspfAreaRangeNet,
                         UINT4 u4SetValOspfAreaRangeMask)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (u4SetValOspfAreaRangeMask);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfAreaRangeStatus
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                setValOspfAreaRangeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaRangeStatus (UINT4 u4OspfAreaRangeAreaId,
                           UINT4 u4OspfAreaRangeNet,
                           INT4 i4SetValOspfAreaRangeStatus)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (i4SetValOspfAreaRangeStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfAreaRangeEffect
 Input       :  The Indices
                OspfAreaRangeAreaId
                OspfAreaRangeNet

                The Object 
                setValOspfAreaRangeEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaRangeEffect (UINT4 u4OspfAreaRangeAreaId,
                           UINT4 u4OspfAreaRangeNet,
                           INT4 i4SetValOspfAreaRangeEffect)
{
    UNUSED_PARAM (u4OspfAreaRangeAreaId);
    UNUSED_PARAM (u4OspfAreaRangeNet);
    UNUSED_PARAM (i4SetValOspfAreaRangeEffect);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfHostMetric
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                setValOspfHostMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfHostMetric (UINT4 u4OspfHostIpAddress, INT4 i4OspfHostTOS,
                      INT4 i4SetValOspfHostMetric)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfHostMetric\n");

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4OspfHostIpAddress);

    pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr);
    if (pHost == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "No Host Exists\n");
        return SNMP_FAILURE;
    }

    if (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].u4Value
        == (UINT4) i4SetValOspfHostMetric
        && (i4SetValOspfHostMetric != DEFAULT_HOST_METRIC))
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfHostMetric;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfHostMetric) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].u4Value
        = i4SetValOspfHostMetric;
    if (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus != ACTIVE)
    {
        pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowMask =
            pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowMask
            | HOST_METRIC_MASK;
        if (pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowMask
            == HOST_METRIC_MASK)
        {
            pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus =
                NOT_IN_SERVICE;
        }
        else
        {
            pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus =
                NOT_READY;
        }
    }
    else
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4OspfHostIpAddress, i4OspfHostTOS,
                              i4SetValOspfHostMetric));
#endif
            return SNMP_FAILURE;
        }

        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pHost->pArea;
        pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4OspfHostIpAddress, i4OspfHostTOS,
                              i4SetValOspfHostMetric));
#endif
            return SNMP_FAILURE;
        }

    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4OspfHostIpAddress, i4OspfHostTOS,
                      i4SetValOspfHostMetric));
#endif
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetOspfHostMetric\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfHostStatus
 Input       :  The Indices
                OspfHostIpAddress
                OspfHostTOS

                The Object 
                setValOspfHostStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfHostStatus (UINT4 u4OspfHostIpAddress, INT4 i4OspfHostTOS,
                      INT4 i4SetValOspfHostStatus)
{
    tIPADDR             hostAddr;
    tHost              *pHost;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: nmhSetOspfHostStatus\n");

    OSPF_CRU_BMC_DWTOPDU (hostAddr, u4OspfHostIpAddress);

    pHost = GetFindHostInCxt (pOspfCxt, &(hostAddr));
    if ((pHost == NULL) && (i4SetValOspfHostStatus == DESTROY))
    {
        return SNMP_SUCCESS;
    }

    if ((i4SetValOspfHostStatus == DESTROY) &&
        (!(IS_VALID_TOS_VALUE (i4OspfHostTOS))))
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfHostStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfHostStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    switch ((UINT1) i4SetValOspfHostStatus)
    {
        case CREATE_AND_GO:
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "No Necessary Info Regarding Metric And"
                      "HostRouteIfIndex Available\n");
            i1RetVal = SNMP_FAILURE;
            break;

        case CREATE_AND_WAIT:
            /* At this place SetFindHost will return valid pointer, 
             * because the validity would have been checked 
             * in test routine */
            if ((pHost = SetFindHostInCxt (pOspfCxt, &(hostAddr))) != NULL)
            {
                pHost->aHostCost[DECODE_TOS
                                 ((INT1) i4OspfHostTOS)].rowStatus = NOT_READY;
                OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                          "EXIT:nmhSetOspfHostStatus\n");
                i1RetVal = SNMP_SUCCESS;
                break;
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "Host Alloc Failed\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

        case ACTIVE:
            if (pHost == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "Host Not Found\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }
            pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus =
                ACTIVE;
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }
            pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pHost->pArea;
            pOspfIfParam->param.sig_param.u1Type = SIG_HOST_ATTACHED;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "EXIT:nmhSetOspfHostStatus\n");
            i1RetVal = SNMP_SUCCESS;
            break;

        case DESTROY:
            pHost->aHostCost[DECODE_TOS ((INT1) i4OspfHostTOS)].rowStatus =
                INVALID;
            pArea = pHost->pArea;
            /* Delete host if all TOS entries are invalid */
            if (HostCheckHostStatus (pHost) == OSPF_INVALID)
            {
                HostDelete (pHost);
            }
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }
            pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
            pOspfIfParam->param.sig_param.u1Type = SIG_HOST_DETACHED;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "EXIT:nmhSetOspfHostStatus\n");
            i1RetVal = SNMP_SUCCESS;
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
            i1RetVal = SNMP_FAILURE;
            break;
        default:
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "SNMP Failure\n");
            i1RetVal = SNMP_FAILURE;
            break;
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetOspfHostStatus\n");
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4OspfHostIpAddress, i4OspfHostTOS,
                      i4SetValOspfHostStatus));
#endif
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetOspfIfAreaId
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfAreaId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfAreaId (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    UINT4 u4SetValOspfIfAreaId)
{
    tIPADDR             ifIpAddr;
    tAreaId             areaId;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfIfAreaId\n");

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4SetValOspfIfAreaId);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "If Does Not Exist\n");
        return SNMP_FAILURE;
    }
    if ((pInterface->pArea != NULL) &&
        (UtilIpAddrComp (pInterface->pArea->areaId, areaId)) == OSPF_EQUAL)
    {
        return SNMP_SUCCESS;
    }
    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        /* virtual ifs belong only to the backbone */
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Virtual If\n");
        return SNMP_FAILURE;
    }
    if (GetFindAreaInCxt (pOspfCxt, &areaId) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfAreaId;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfIfAreaId) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    pOspfIfParam->u1OpCode = OSPF_SET_IF_AREA_ID;
    pOspfIfParam->param.if_param.pInterface = pInterface;
    pOspfIfParam->param.if_param.u4AreaId = u4SetValOspfIfAreaId;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          u4SetValOspfIfAreaId));
#endif
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p", gOsRtr.u4OspfCxtId,
                      u4OspfIfIpAddress, i4OspfAddressLessIf,
                      u4SetValOspfIfAreaId));
#endif
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetOspfIfAreaId : SNMP_SUCCESS\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfIfType
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfType (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                  INT4 i4SetValOspfIfType)
{

    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        if (pInterface->u1NetworkType == (UINT1) i4SetValOspfIfType)
        {
            return SNMP_SUCCESS;
        }

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

            if (pNbr->u1ConfigStatus == CONFIGURED_NBR)
            {
                OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                          "Configured Neighbours present on this interface, If Type not set \n");
                return SNMP_FAILURE;
            }
        }

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfType;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfIfType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pOspfIfParam->u1OpCode = OSPF_SET_IF_TYPE;
        pOspfIfParam->param.if_param.pInterface = pInterface;
        pOspfIfParam->param.if_param.u1Type = (UINT1) i4SetValOspfIfType;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4OspfIfIpAddress, i4OspfAddressLessIf,
                              i4SetValOspfIfType));
#endif
            return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfType));
#endif
        return SNMP_SUCCESS;

    }
    OSPF_TRC1 (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
               "Unable to set If type for the interface %x\n",
               u4OspfIfIpAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfAdminStat
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfAdminStat (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                       INT4 i4SetValOspfIfAdminStat)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfIfAdminStat\n");

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        if (pInterface->admnStatus == (UINT1) i4SetValOspfIfAdminStat)
        {
            return SNMP_SUCCESS;
        }
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {

            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");

            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfAdminStat;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfIfAdminStat) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pOspfIfParam->u1OpCode = OSPF_SET_ADMN_STATUS;
        pOspfIfParam->param.admn_param.pInterface = pInterface;
        pOspfIfParam->param.admn_param.u1Status =
            (UINT1) i4SetValOspfIfAdminStat;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
            i1RetVal = SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = i1RetVal;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfAdminStat));
#endif
        return i1RetVal;
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetOspfIfAdminStat\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfRtrPriority
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfRtrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfRtrPriority (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                         INT4 i4SetValOspfIfRtrPriority)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        if (pInterface->u1RtrPriority != (UINT1) i4SetValOspfIfRtrPriority)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfRtrPriority;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfIfRtrPriority) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            pInterface->u1RtrPriority = (UINT1) i4SetValOspfIfRtrPriority;
            IsmSchedule (pInterface, IFE_NBR_CHANGE);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4OspfIfIpAddress, i4OspfAddressLessIf,
                              i4SetValOspfIfRtrPriority));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfTransitDelay
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfTransitDelay (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                          INT4 i4SetValOspfIfTransitDelay)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfTransitDelay;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfIfTransitDelay) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->u2IfTransDelay = (UINT2) i4SetValOspfIfTransitDelay;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfTransitDelay));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfRetransInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfRetransInterval (UINT4 u4OspfIfIpAddress,
                             INT4 i4OspfAddressLessIf,
                             INT4 i4SetValOspfIfRetransInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfRetransInterval;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfIfRetransInterval) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->u2RxmtInterval = (UINT2) i4SetValOspfIfRetransInterval;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfRetransInterval));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfHelloInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfHelloInterval (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                           INT4 i4SetValOspfIfHelloInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfHelloInterval;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfIfHelloInterval) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->u2HelloInterval = (UINT2) i4SetValOspfIfHelloInterval;
        TmrRestartTimer (&(pInterface->helloTimer), HELLO_TIMER,
                         NO_OF_TICKS_PER_SEC * (pInterface->u2HelloInterval));
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfHelloInterval));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfRtrDeadInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfRtrDeadInterval (UINT4 u4OspfIfIpAddress,
                             INT4 i4OspfAddressLessIf,
                             INT4 i4SetValOspfIfRtrDeadInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfRtrDeadInterval;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfIfRtrDeadInterval) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->i4RtrDeadInterval = i4SetValOspfIfRtrDeadInterval;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfRtrDeadInterval));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfPollInterval
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfPollInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfPollInterval (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                          INT4 i4SetValOspfIfPollInterval)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfPollInterval;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfIfPollInterval) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->i4PollInterval = i4SetValOspfIfPollInterval;
        TmrRestartTimer (&(pInterface->pollTimer), POLL_TIMER,
                         NO_OF_TICKS_PER_SEC * (pInterface->i4PollInterval));
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfPollInterval));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfAuthType
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfAuthType (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                      INT4 i4SetValOspfIfAuthType)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfAuthType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfIfAuthType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        if (pInterface->u2AuthType != i4SetValOspfIfAuthType)
        {
            pInterface->u2AuthType = (UINT2) i4SetValOspfIfAuthType;
            pInterface->pLastAuthkey = NULL;
            if ((pInterface->u2AuthType == 2)
                && (pInterface->u4CryptoAuthType == 0))
            {
                pInterface->u4CryptoAuthType = OSPF_AUTH_MD5;
            }
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfAuthType));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfAuthKey
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfAuthKey (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                     tSNMP_OCTET_STRING_TYPE * pSetValOspfIfAuthKey)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    UINT1              *pAuthKey = NULL;
    UINT1               u1Len = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pSetValOspfIfAuthKey == NULL)
    {
        return SNMP_FAILURE;
    }
    pAuthKey = pSetValOspfIfAuthKey->pu1_OctetList;
    u1Len = (UINT1) pSetValOspfIfAuthKey->i4_Length;
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if (((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                       (UINT4) i4OspfAddressLessIf)) != NULL)
        && (u1Len <= AUTH_KEY_SIZE))
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfAuthKey;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfIfAuthKey) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        OS_MEM_CPY (pInterface->authKey, pAuthKey, u1Len);

        if (u1Len < AUTH_KEY_SIZE)
        {

            /*
             * if auth key len is shorter than AUTH_KEY_SIZE
             * then it is padded with 0s.
             */

            OS_MEM_SET ((UINT1 *) (pInterface->authKey + u1Len),
                        0, (AUTH_KEY_SIZE - u1Len));
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %s", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          pSetValOspfIfAuthKey));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfStatus
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfStatus (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    INT4 i4SetValOspfIfStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tIPADDR             ifIpAddrMask;
    UINT4               u4IfIndex;
    UINT4               u4IfMtuSize;
    UINT4               u4IfSpeed;
    UINT4               u4SeqNum = 0;
    UINT1               u1IfType;
    UINT1               u1OperStat;
    UINT4               u4AreaId;
#if defined (LNXIP4_WANTED) && defined (L3_SWITCHING_WANTED)
    UINT4               u4Port = OSPF_ZERO;
#endif

    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage = (tOspfQMsg *) NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfIfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfIfStatus\n");

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    switch ((UINT1) i4SetValOspfIfStatus)
    {
        case CREATE_AND_GO:
            OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: IpifGetIfParamsInCxt\n");

            if (IpifGetIfParamsInCxt (pOspfCxt->u4OspfCxtId,
                                      ifIpAddr, (UINT4) i4OspfAddressLessIf,
                                      &u4IfIndex, ifIpAddrMask, &u4IfMtuSize,
                                      &u4IfSpeed, &u1IfType,
                                      &u1OperStat) == OSPF_FAILURE)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "IpifGetIfParamsInCxt Failure \n");
                return SNMP_FAILURE;
            }
            else
            {
                OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                          "EXIT: IpifGetIfParamsInCxt\n");
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((pInterface = IfCreateInCxt (pOspfCxt, ifIpAddr,
                                             ifIpAddrMask,
                                             (UINT4) i4OspfAddressLessIf,
                                             u4IfIndex, u4IfMtuSize,
                                             u4IfSpeed, u1IfType,
                                             u1OperStat)) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "if create Failure \n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }
            pInterface->ifStatus = ACTIVE;

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_IF_ACTIVATE;
            pOspfIfParam->param.if_param.pInterface = pInterface;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }

            u4AreaId = OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId);

            if (u4AreaId == 0)
            {
                pOspfCxt->u2AreaZeroInterfaceCnt =
                    ((pOspfCxt->u2AreaZeroInterfaceCnt) + 1);
            }

            break;

        case CREATE_AND_WAIT:
            OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: IpifGetIfParamsInCxt\n");

            if (IpifGetIfParamsInCxt (pOspfCxt->u4OspfCxtId,
                                      ifIpAddr, (UINT4) i4OspfAddressLessIf,
                                      &u4IfIndex, ifIpAddrMask,
                                      &u4IfMtuSize, &u4IfSpeed, &u1IfType,
                                      &u1OperStat) == OSPF_FAILURE)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "IpifGetIfParamsInCxt Failure\n");
                CLI_SET_ERR (CLI_OSPF_INTERFACE_NOT_EXISTS);
                return SNMP_FAILURE;
            }
            else
            {
                OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                          "EXIT: IpifGetIfParamsInCxt\n");
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((pInterface = IfCreateInCxt (pOspfCxt, ifIpAddr, ifIpAddrMask,
                                             (UINT4) i4OspfAddressLessIf,
                                             u4IfIndex, u4IfMtuSize,
                                             u4IfSpeed, u1IfType,
                                             u1OperStat)) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "if create Failure \n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }
            else
            {

                if (pInterface->rowMask == IF_TABLE_STD_MASK)
                {
                    pInterface->ifStatus = NOT_IN_SERVICE;
                    IfSetAdmnStat (pInterface, OSPF_DISABLED);
                }
                else
                {
                    pInterface->ifStatus = NOT_READY;
                }
                break;
            }

        case ACTIVE:
            pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                         (UINT4) i4OspfAddressLessIf);
            if (pInterface == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Interface Entry Does not exist \n");
                return SNMP_FAILURE;
            }

            if ((pOspfCxt->u1BfdStatusInAllIf == OSPF_BFD_ENABLED) &&
                (pOspfCxt->u1BfdAdminStatus == OSPF_BFD_ENABLED))
            {
                pInterface->u1BfdIfStatus = OSPF_BFD_ENABLED;
            }
            if (pInterface->ifStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pInterface->ifStatus = ACTIVE;
            IfSetAdmnStat (pInterface, OSPF_ENABLED);
            /* In case of demand circuit hello packet has to be transmitted for intial session formation so joining Mcast group */
            IpifJoinMcastGroup (&gAllSpfRtrs, pInterface);

            /* Do the hardware initialisation to receive OSPF packets */

#if defined (LNXIP4_WANTED) && defined (L3_SWITCHING_WANTED)
            if (ISS_HW_SUPPORTED ==
                IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
            {
                NetIpv4GetIfIndexFromAddrInCxt (pOspfCxt->u4OspfCxtId,
                                                u4OspfIfIpAddress, &u4Port);
                NetIpv4GetCfaIfIndexFromPort (u4Port, &u4IfIndex);
                if (FsNpOspfCreateAndDeleteFilter (u4IfIndex, ACTIVE) ==
                    FNP_FAILURE)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF Filter Creation Failed\n");
                    return SNMP_FAILURE;

                }
            }
#endif
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_IF_ACTIVATE;
            pOspfIfParam->param.if_param.pInterface = pInterface;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }

            u4AreaId = OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId);

            if (u4AreaId == 0)
            {
                pOspfCxt->u2AreaZeroInterfaceCnt =
                    ((pOspfCxt->u2AreaZeroInterfaceCnt) + 1);
            }

            break;

        case DESTROY:
            pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                         (UINT4) i4OspfAddressLessIf);
            if (pInterface == NULL)
            {
                return SNMP_SUCCESS;
            }

#if defined (LNXIP4_WANTED) && defined (L3_SWITCHING_WANTED)
            if (ISS_HW_SUPPORTED ==
                IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
            {
                NetIpv4GetIfIndexFromAddrInCxt (pOspfCxt->u4OspfCxtId,
                                                u4OspfIfIpAddress, &u4Port);
                NetIpv4GetCfaIfIndexFromPort (u4Port, &u4IfIndex);
                if (FsNpOspfCreateAndDeleteFilter (u4IfIndex, DESTROY) ==
                    FNP_FAILURE)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF Filter Deletion Failed\n");
                    return SNMP_FAILURE;
                }
            }
#endif
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_DELETE_IF;
            pOspfIfParam->param.if_param.pInterface = pInterface;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }
            u4AreaId = OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId);

            if (u4AreaId == 0)
            {
                pOspfCxt->u2AreaZeroInterfaceCnt =
                    ((pOspfCxt->u2AreaZeroInterfaceCnt) - 1);
            }

            break;

        case NOT_IN_SERVICE:

            pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                         (UINT4) i4OspfAddressLessIf);
            if (pInterface == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          "Interface Entry Does not exist \n");
                return SNMP_FAILURE;
            }

            if (pInterface->ifStatus == NOT_IN_SERVICE)
            {
                return SNMP_SUCCESS;
            }
            pInterface->ifStatus = NOT_IN_SERVICE;
            IfSetAdmnStat (pInterface, OSPF_ENABLED);

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_IF_INACTIVATE;
            pOspfIfParam->param.if_param.pInterface = pInterface;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfIpAddress, i4OspfAddressLessIf,
                                  i4SetValOspfIfStatus));
#endif
                return SNMP_FAILURE;
            }

            u4AreaId = OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId);

            if (u4AreaId == 0)
            {
                pOspfCxt->u2AreaZeroInterfaceCnt =
                    ((pOspfCxt->u2AreaZeroInterfaceCnt) - 1);
            }
            if (pInterface->u1NetworkType != IF_VIRTUAL)
            {
                UtilOspfClearAreaDb (pInterface->pArea);
            }

            break;

            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4OspfIfIpAddress, i4OspfAddressLessIf,
                      i4SetValOspfIfStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfIfMulticastForwarding
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfMulticastForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfMulticastForwarding (UINT4 u4OspfIfIpAddress,
                                 INT4 i4OspfAddressLessIf,
                                 INT4 i4SetValOspfIfMulticastForwarding)
{
    UNUSED_PARAM (u4OspfIfIpAddress);
    UNUSED_PARAM (i4OspfAddressLessIf);
    UNUSED_PARAM (i4SetValOspfIfMulticastForwarding);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfIfDemand
 Input       :  The Indices
                OspfIfIpAddress
                OspfAddressLessIf

                The Object 
                setValOspfIfDemand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfDemand (UINT4 u4OspfIfIpAddress, INT4 i4OspfAddressLessIf,
                    INT4 i4SetValOspfIfDemand)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4OspfAddressLessIf)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "If Does Not Exist\n");
        return SNMP_FAILURE;
    }
    if (pInterface->bDcEndpt == (UINT1) i4SetValOspfIfDemand)
    {
        return SNMP_SUCCESS;
    }
    else if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfDemand;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfIfDemand) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    pOspfIfParam->u1OpCode = OSPF_SET_IF_DEMAND;
    pOspfIfParam->param.if_param.pInterface = pInterface;
    pOspfIfParam->param.if_param.u1Demand = (UINT1) i4SetValOspfIfDemand;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, i4OspfAddressLessIf,
                          i4SetValOspfIfDemand));
#endif
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4OspfIfIpAddress, i4OspfAddressLessIf,
                      i4SetValOspfIfDemand));
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetOspfIfMetricValue
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS

                The Object 
                setValOspfIfMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfMetricValue (UINT4 u4OspfIfMetricIpAddress,
                         INT4 i4OspfIfMetricAddressLessIf,
                         INT4 i4OspfIfMetricTOS, INT4 i4SetValOspfIfMetricValue)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfIfMetricValue\n");

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);

    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                 (UINT4) i4OspfIfMetricAddressLessIf);

    if (pInterface == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Invalid Interface \n");
        return SNMP_FAILURE;
    }

    if (pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
        u4Value != (UINT4) i4SetValOspfIfMetricValue)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfMetricValue;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfIfMetricValue) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
            u4Value = (UINT4) i4SetValOspfIfMetricValue;
        if (pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
            rowStatus == ACTIVE)
        {
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricValue));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pInterface->pArea;
            pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricValue));
#endif
                return SNMP_FAILURE;
            }

        }
        else
        {
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                rowStatus = NOT_IN_SERVICE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfMetricIpAddress, i4OspfIfMetricAddressLessIf,
                          i4OspfIfMetricTOS, i4SetValOspfIfMetricValue));
#endif
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetOspfIfMetricValue\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfIfMetricStatus
 Input       :  The Indices
                OspfIfMetricIpAddress
                OspfIfMetricAddressLessIf
                OspfIfMetricTOS

                The Object 
                setValOspfIfMetricStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfIfMetricStatus (UINT4 u4OspfIfMetricIpAddress,
                          INT4 i4OspfIfMetricAddressLessIf,
                          INT4 i4OspfIfMetricTOS,
                          INT4 i4SetValOspfIfMetricStatus)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC:nmhSetOspfIfMetricStatus\n");

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfMetricIpAddress);

    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                 (UINT4) i4OspfIfMetricAddressLessIf);

    if (pInterface == NULL)
    {
        if (i4SetValOspfIfMetricStatus == DESTROY)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId,
                      "Interface Entry Does not exist \n");
            return SNMP_FAILURE;
        }
    }

    if ((i4SetValOspfIfMetricStatus == DESTROY) &&
        (!(IS_VALID_TOS_VALUE (i4OspfIfMetricTOS))))
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfMetricStatus;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIStdOspfIfMetricStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    switch ((UINT1) i4SetValOspfIfMetricStatus)
    {
        case CREATE_AND_GO:
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                u4Value = pInterface->aIfOpCost[DECODE_TOS (TOS_0)].u4Value;
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                rowStatus = ACTIVE;
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pInterface;
            pOspfIfParam->param.sig_param.u1Type = SIG_IF_METRIC_PARAM_CHANGE;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case CREATE_AND_WAIT:
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                u4Value = pInterface->aIfOpCost[DECODE_TOS (TOS_0)].u4Value;
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                rowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                rowStatus = ACTIVE;
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pInterface;
            pOspfIfParam->param.sig_param.u1Type = SIG_IF_METRIC_PARAM_CHANGE;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case DESTROY:

            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                rowStatus = INVALID;
            pInterface->aIfOpCost[DECODE_TOS ((INT1) i4OspfIfMetricTOS)].
                u4Value = LS_INFINITY_16BIT;
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_DELETE_IF_METRIC;
            pOspfIfParam->param.if_param.pInterface = pInterface;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfIfMetricIpAddress,
                                  i4OspfIfMetricAddressLessIf,
                                  i4OspfIfMetricTOS,
                                  i4SetValOspfIfMetricStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4OspfIfMetricIpAddress,
                              i4OspfIfMetricAddressLessIf,
                              i4OspfIfMetricTOS, i4SetValOspfIfMetricStatus));
#endif
            return SNMP_FAILURE;
            /*end switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                      u4OspfIfMetricIpAddress, i4OspfIfMetricAddressLessIf,
                      i4OspfIfMetricTOS, i4SetValOspfIfMetricStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfTransitDelay
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfTransitDelay (UINT4 u4OspfVirtIfAreaId,
                              UINT4 u4OspfVirtIfNeighbor,
                              INT4 i4SetValOspfVirtIfTransitDelay)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        if (pInterface->u2IfTransDelay !=
            (UINT2) i4SetValOspfVirtIfTransitDelay)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfTransitDelay;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfVirtIfTransitDelay) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            pInterface->u2IfTransDelay = (UINT2) i4SetValOspfVirtIfTransitDelay;
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                              i4SetValOspfVirtIfTransitDelay));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfRetransInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfRetransInterval (UINT4 u4OspfVirtIfAreaId,
                                 UINT4 u4OspfVirtIfNeighbor,
                                 INT4 i4SetValOspfVirtIfRetransInterval)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        if (pInterface->u2RxmtInterval !=
            (UINT2) i4SetValOspfVirtIfRetransInterval)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfRetransInterval;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfVirtIfRetransInterval) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            pInterface->u2RxmtInterval =
                (UINT2) i4SetValOspfVirtIfRetransInterval;
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                              i4SetValOspfVirtIfRetransInterval));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfHelloInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfHelloInterval (UINT4 u4OspfVirtIfAreaId,
                               UINT4 u4OspfVirtIfNeighbor,
                               INT4 i4SetValOspfVirtIfHelloInterval)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        if (pInterface->u2HelloInterval !=
            (UINT2) i4SetValOspfVirtIfHelloInterval)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfHelloInterval;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfVirtIfHelloInterval) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            pInterface->u2HelloInterval =
                (UINT2) i4SetValOspfVirtIfHelloInterval;
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                              i4SetValOspfVirtIfHelloInterval));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfRtrDeadInterval
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfRtrDeadInterval (UINT4 u4OspfVirtIfAreaId,
                                 UINT4 u4OspfVirtIfNeighbor,
                                 INT4 i4SetValOspfVirtIfRtrDeadInterval)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        if (pInterface->i4RtrDeadInterval != i4SetValOspfVirtIfRtrDeadInterval)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfRtrDeadInterval;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfVirtIfRtrDeadInterval) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            pInterface->i4RtrDeadInterval = i4SetValOspfVirtIfRtrDeadInterval;
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                              i4SetValOspfVirtIfRtrDeadInterval));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfAuthType
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfAuthType (UINT4 u4OspfVirtIfAreaId,
                          UINT4 u4OspfVirtIfNeighbor,
                          INT4 i4SetValOspfVirtIfAuthType)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {
        if (pInterface->u2AuthType != (UINT2) i4SetValOspfVirtIfAuthType)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfAuthType;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfVirtIfAuthType) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            if (pInterface->u2AuthType != i4SetValOspfVirtIfAuthType)
            {
                pInterface->u2AuthType = (UINT2) i4SetValOspfVirtIfAuthType;
                pInterface->pLastAuthkey = NULL;
                if ((pInterface->u2AuthType == 2)
                    && (pInterface->u4CryptoAuthType == 0))
                {
                    pInterface->u4CryptoAuthType = OSPF_AUTH_MD5;
                }
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                              u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                              i4SetValOspfVirtIfAuthType));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfAuthKey
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfAuthKey (UINT4 u4OspfVirtIfAreaId, UINT4 u4OspfVirtIfNeighbor,
                         tSNMP_OCTET_STRING_TYPE * pSetValOspfVirtIfAuthKey)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    UINT1              *pAuthKey;
    UINT1               u1Len = (UINT1) (pSetValOspfVirtIfAuthKey->i4_Length);
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    pAuthKey = pSetValOspfVirtIfAuthKey->pu1_OctetList;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if (((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
        && (u1Len <= AUTH_KEY_SIZE))
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfAuthKey;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfVirtIfAuthKey) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        OS_MEM_CPY (pInterface->authKey, pAuthKey, u1Len);

        if (u1Len < AUTH_KEY_SIZE)
        {

            /*
             * if auth key len is shorter than AUTH_KEY_SIZE
             * then it is padded with 0s.
             */

            OS_MEM_SET ((UINT1 *) (pInterface->authKey + u1Len),
                        0, (AUTH_KEY_SIZE - u1Len));
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %s", gOsRtr.u4OspfCxtId,
                          u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                          pSetValOspfVirtIfAuthKey));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfVirtIfStatus
 Input       :  The Indices
                OspfVirtIfAreaId
                OspfVirtIfNeighbor

                The Object 
                setValOspfVirtIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfVirtIfStatus (UINT4 u4OspfVirtIfAreaId, UINT4 u4OspfVirtIfNeighbor,
                        INT4 i4SetValOspfVirtIfStatus)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tArea              *pArea = (tArea *) NULL;
    tInterface         *pInterface = NULL;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage = (tOspfQMsg *) NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfVirtIfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC nmhSetOspfVirtIfStatus:\n");

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    switch ((UINT1) i4SetValOspfVirtIfStatus)
    {
        case CREATE_AND_GO:
            if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, "Area Not Found\n");
                return SNMP_FAILURE;
            }

            if (pArea->u4AreaType != NORMAL_AREA)
                return SNMP_FAILURE;

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_CREATE_VIRT_IF;
            IP_ADDR_COPY (pOspfIfParam->param.vif_param.nbrId, &nbrId);
            IP_ADDR_COPY (pOspfIfParam->param.vif_param.areaId, &areaId);
            pOspfIfParam->pOspfCxt = pOspfCxt;

            pOspfIfParam->param.vif_param.rowStatus = ACTIVE;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                                  i4SetValOspfVirtIfStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case CREATE_AND_WAIT:
            if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, " Area Not Found\n");
                return SNMP_FAILURE;
            }

            if (pArea->u4AreaType != NORMAL_AREA)
                return SNMP_FAILURE;

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_CREATE_VIRT_IF;
            IP_ADDR_COPY (pOspfIfParam->param.vif_param.nbrId, &nbrId);
            IP_ADDR_COPY (pOspfIfParam->param.vif_param.areaId, &areaId);
            pOspfIfParam->param.vif_param.rowStatus = NOT_IN_SERVICE;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                                  i4SetValOspfVirtIfStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_ACTIVATE_VIRT_IF;
            IP_ADDR_COPY (pOspfIfParam->param.vif_param.nbrId, &nbrId);
            IP_ADDR_COPY (pOspfIfParam->param.vif_param.areaId, &areaId);
            pOspfIfParam->param.vif_param.rowStatus = ACTIVE;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                                  i4SetValOspfVirtIfStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case DESTROY:
            pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);

            if (pInterface == NULL)
            {
                return SNMP_SUCCESS;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfIfParam->u1OpCode = OSPF_DELETE_IF;
            pOspfIfParam->param.if_param.pInterface = pInterface;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                                  i4SetValOspfVirtIfStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                      i4SetValOspfVirtIfStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfNbrPriority
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                setValOspfNbrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfNbrPriority (UINT4 u4OspfNbrIpAddr, INT4 i4OspfNbrAddressLessIndex,
                       INT4 i4SetValOspfNbrPriority)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    INT4                i4IsmSchedFlag;
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);
    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                 (UINT4) i4OspfNbrAddressLessIndex)) != NULL)
    {
        if (pNbr->u1NbrRtrPriority != (UINT1) i4SetValOspfNbrPriority)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfNbrPriority;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIStdOspfNbrPriority) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 3;
#endif
            if ((i4IsmSchedFlag =
                 NbrProcessPriorityChange (pNbr,
                                           (UINT1) i4SetValOspfNbrPriority)) ==
                ISM_SCHEDULED)
            {
                IsmSchedule (pNbr->pInterface, IFE_NBR_CHANGE);
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4OspfNbrIpAddr, i4OspfNbrAddressLessIndex,
                              i4SetValOspfNbrPriority));
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfNbmaNbrStatus
 Input       :  The Indices
                OspfNbrIpAddr
                OspfNbrAddressLessIndex

                The Object 
                setValOspfNbmaNbrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfNbmaNbrStatus (UINT4 u4OspfNbrIpAddr,
                         INT4 i4OspfNbrAddressLessIndex,
                         INT4 i4SetValOspfNbmaNbrStatus)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr = NULL;
    tInterface         *pInterface;
    tRouterId           defNbrId;
    tTMO_SLL_NODE      *pLstNode;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC nmhSetOspfNbmaNbrStatus:\n");

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfNbmaNbrStatus;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIStdOspfNbmaNbrStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    switch ((UINT1) i4SetValOspfNbmaNbrStatus)
    {
        case CREATE_AND_GO:
            /* find the interface on which new neighbor is to be created */
            TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if ((pInterface->u1NetworkType != IF_NBMA) &&
                (pInterface->u1NetworkType != IF_PTOMP))
            {
                /* Nbr can be configured only in NBMA intrfaces */
                continue;
            }

            if (pInterface->pArea == NULL)
            {
                continue;
            }

            if (UtilIpAddrMaskComp (pInterface->ifIpAddr,
                                    nbrIpAddr,
                                    pInterface->ifIpAddrMask) == OSPF_EQUAL)
            {
                OSPF_CRU_BMC_DWTOPDU (defNbrId, 0);
                pNbr = NbrCreate (&(nbrIpAddr), &defNbrId,
                                  (UINT4) i4OspfNbrAddressLessIndex,
                                  pInterface, CONFIGURED_NBR);
                if (pNbr == NULL)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "Nbr Create Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_FAILURE;
                }
                pNbr->nbrStatus = ACTIVE;

                /* If the interface is Passive interface 
                 * no need to Activate the neighbor */
                if (pInterface->bPassive == OSPF_TRUE)
                {
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_SUCCESS;
                }

                /* pose an event for activating */
                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_FAILURE;
                }
                pOspfIfParam->u1OpCode = OSPF_ACTIVATE_NBR;
                pOspfIfParam->param.nbr_param.pNbr = pNbr;
                pOspfIfParam->pOspfCxt = pOspfCxt;
                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_FAILURE;
                }

            }
        }

            break;

        case CREATE_AND_WAIT:
            /* find the interface on which new neighbor is to be created */
            TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if ((pInterface->u1NetworkType != IF_NBMA) &&
                (pInterface->u1NetworkType != IF_PTOMP))
            {
                /* Nbr can be configured only in NBMA intrfaces */
                continue;
            }

            if (pInterface->pArea == NULL)
            {
                continue;
            }

            if (UtilIpAddrMaskComp (pInterface->ifIpAddr,
                                    nbrIpAddr,
                                    pInterface->ifIpAddrMask) == OSPF_EQUAL)
            {

                OSPF_CRU_BMC_DWTOPDU (defNbrId, 0);
                pNbr = NbrCreate (&(nbrIpAddr), &defNbrId,
                                  (UINT4) i4OspfNbrAddressLessIndex,
                                  pInterface, CONFIGURED_NBR);
                if (pNbr == NULL)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "Nbr Create Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_FAILURE;
                }
                pNbr->nbrStatus = NOT_IN_SERVICE;
            }
        }
            break;

        case ACTIVE:
            if ((pNbr = GetFindNbrInCxt (pOspfCxt, &(nbrIpAddr),
                                         (UINT4) i4OspfNbrAddressLessIndex)) ==
                NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, "Nbr Not Found\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfNbrIpAddr,
                                  i4OspfNbrAddressLessIndex,
                                  i4SetValOspfNbmaNbrStatus));
#endif
                return SNMP_FAILURE;
            }
            pNbr->nbrStatus = ACTIVE;

            /* If the interface is Passive interface 
             * no need to Activate the neighbor */
            if (pNbr->pInterface->bPassive == OSPF_TRUE)
            {
                break;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          " OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfNbrIpAddr,
                                  i4OspfNbrAddressLessIndex,
                                  i4SetValOspfNbmaNbrStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_ACTIVATE_NBR;
            pOspfIfParam->param.nbr_param.pNbr = pNbr;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfNbrIpAddr,
                                  i4OspfNbrAddressLessIndex,
                                  i4SetValOspfNbmaNbrStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case DESTROY:
            pNbr = GetFindNbrInCxt (pOspfCxt, &nbrIpAddr,
                                    (UINT4) i4OspfNbrAddressLessIndex);
            if (pNbr == NULL)
            {
                /* As sequence number has been reserved, a dummy message
                 * needs to be send.*/
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfNbrIpAddr,
                                  i4OspfNbrAddressLessIndex,
                                  i4SetValOspfNbmaNbrStatus));
#endif
                return SNMP_SUCCESS;
            }

            if ((pNbr->pInterface->u1NetworkType == IF_NBMA)
                || (pNbr->pInterface->u1NetworkType == IF_PTOMP))
            {
                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
                {
                    pOspfIfParam = (tOspfSnmpIfParam *)
                        OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
                }
                else
                {

                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_FAILURE;
                }
                pOspfIfParam->u1OpCode = OSPF_DELETE_NBR;
                pOspfIfParam->param.nbr_param.pNbr = pNbr;
                pOspfIfParam->pOspfCxt = pOspfCxt;
                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                      gOsRtr.u4OspfCxtId,
                                      u4OspfNbrIpAddr,
                                      i4OspfNbrAddressLessIndex,
                                      i4SetValOspfNbmaNbrStatus));
#endif
                    return SNMP_FAILURE;
                }

                break;
            }
            else
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfNbrIpAddr,
                                  i4OspfNbrAddressLessIndex,
                                  i4SetValOspfNbmaNbrStatus));
#endif
                return SNMP_FAILURE;
            }

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4OspfNbrIpAddr,
                              i4OspfNbrAddressLessIndex,
                              i4SetValOspfNbmaNbrStatus));
#endif
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4OspfNbrIpAddr, i4OspfNbrAddressLessIndex,
                      i4SetValOspfNbmaNbrStatus));
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetOspfAreaAggregateStatus
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask

                The Object 
                setValOspfAreaAggregateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaAggregateStatus (UINT4 u4OspfAreaAggregateAreaID,
                               INT4 i4OspfAreaAggregateLsdbType,
                               UINT4 u4OspfAreaAggregateNet,
                               UINT4 u4OspfAreaAggregateMask,
                               INT4 i4SetValOspfAreaAggregateStatus)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea;
    INT1                i1Index;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfAreaAggregateStatus;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIStdOspfAreaAggregateStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 5;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);
    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not found, Failure\n");
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4SetValOspfAreaAggregateStatus)
    {
        case CREATE_AND_GO:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((AreaCreateAddrRange (pArea, &ipAddr, &addrMask,
                                      (UINT1) i4OspfAreaAggregateLsdbType,
                                      ACTIVE, &i1Index)) == OSPF_FAILURE)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "AreaCreateAddrRange ACTIVE Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateStatus));
#endif
                return SNMP_FAILURE;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_ACTIVE_AREA_ADDR_RANGE;
            pOspfIfParam->param.area_param.pArea = pArea;
            pOspfIfParam->param.area_param.u1AggregateIndex = (UINT1) i1Index;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case CREATE_AND_WAIT:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((AreaCreateAddrRange (pArea, &ipAddr, &addrMask,
                                      (UINT1) i4OspfAreaAggregateLsdbType,
                                      NOT_IN_SERVICE,
                                      &i1Index)) == OSPF_FAILURE)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "AreaCreateAddrRange NOT_IN_SERVICE Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateStatus));
#endif
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                      (UINT1)
                                      i4OspfAreaAggregateLsdbType,
                                      &i1Index) != OSPF_FAILURE)
            {
                if (pArea->aAddrRange[i1Index].areaAggStatus == ACTIVE)
                    return SNMP_SUCCESS;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_ACTIVE_AREA_ADDR_RANGE;
            pOspfIfParam->param.area_param.pArea = pArea;
            pOspfIfParam->param.area_param.u1AggregateIndex = (UINT1) i1Index;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            pArea->aAddrRange[i1Index].areaAggStatus = ACTIVE;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case DESTROY:
            if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                                      (UINT1)
                                      i4OspfAreaAggregateLsdbType,
                                      &i1Index) == OSPF_FAILURE)
            {
                break;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_DELETE_AREA_ADDR_RANGE;
            pOspfIfParam->param.area_param.pArea = pArea;
            pOspfIfParam->param.area_param.u1AggregateIndex = i1Index;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end of switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4OspfAreaAggregateAreaID, i4OspfAreaAggregateLsdbType,
                      u4OspfAreaAggregateNet, u4OspfAreaAggregateMask,
                      i4SetValOspfAreaAggregateStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfAreaAggregateEffect
 Input       :  The Indices
                OspfAreaAggregateAreaID
                OspfAreaAggregateLsdbType
                OspfAreaAggregateNet
                OspfAreaAggregateMask

                The Object 
                setValOspfAreaAggregateEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfAreaAggregateEffect (UINT4 u4OspfAreaAggregateAreaID,
                               INT4 i4OspfAreaAggregateLsdbType,
                               UINT4 u4OspfAreaAggregateNet,
                               UINT4 u4OspfAreaAggregateMask,
                               INT4 i4SetValOspfAreaAggregateEffect)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea = (tArea *) NULL;
    INT1                i1Index;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4OspfAreaAggregateNet);
    OSPF_CRU_BMC_DWTOPDU (addrMask, u4OspfAreaAggregateMask);

    if ((pArea = GetFindAreaInCxt (pOspfCxt, (&areaId))) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not Found\n");
        return SNMP_FAILURE;
    }

    if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                              (UINT1) i4OspfAreaAggregateLsdbType,
                              &i1Index) != OSPF_FAILURE)
    {
        if (pArea->aAddrRange[i1Index].u1AdvertiseStatus ==
            i4SetValOspfAreaAggregateEffect)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfAreaAggregateEffect;
            SnmpNotifyInfo.u4OidLen
                = sizeof (FsMIStdOspfAreaAggregateEffect) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = OspfLock;
            SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
            SnmpNotifyInfo.u4Indices = 5;
#endif
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateEffect));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = SET_AREA_AGGR_EFFECT;
            pOspfIfParam->param.area_param.pArea = pArea;
            pOspfIfParam->param.area_param.u1AggregateIndex = (UINT1) i1Index;
            pOspfIfParam->param.area_param.u1AggregateEffect =
                (UINT1) i4SetValOspfAreaAggregateEffect;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4OspfAreaAggregateAreaID,
                                  i4OspfAreaAggregateLsdbType,
                                  u4OspfAreaAggregateNet,
                                  u4OspfAreaAggregateMask,
                                  i4SetValOspfAreaAggregateEffect));
#endif
                return SNMP_FAILURE;
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4OspfAreaAggregateAreaID,
                              i4OspfAreaAggregateLsdbType,
                              u4OspfAreaAggregateNet, u4OspfAreaAggregateMask,
                              i4SetValOspfAreaAggregateEffect));
#endif
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfMaxLsaSize
 Input       :  The Indices

                The Object 
                setValFutOspfMaxLsaSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMaxLsaSize (INT4 i4SetValFutOspfMaxLsaSize)
{

    UNUSED_PARAM (i4SetValFutOspfMaxLsaSize);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfMaxAreas
 Input       :  The Indices

                The Object 
                setValFutOspfMaxAreas
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMaxAreas (INT4 i4SetValFutOspfMaxAreas)
{

    UNUSED_PARAM (i4SetValFutOspfMaxAreas);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfMaxLSAperArea
 Input       :  The Indices

                The Object 
                setValFutOspfMaxLSAperArea
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMaxLSAperArea (INT4 i4SetValFutOspfMaxLSAperArea)
{

    UNUSED_PARAM (i4SetValFutOspfMaxLSAperArea);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfMaxExtLSAs
 Input       :  The Indices

                The Object 
                setValFutOspfMaxExtLSAs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMaxExtLSAs (INT4 i4SetValFutOspfMaxExtLSAs)
{

    UNUSED_PARAM (i4SetValFutOspfMaxExtLSAs);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfMaxSelfOrgLSAs
 Input       :  The Indices

                The Object 
                setValFutOspfMaxSelfOrgLSAs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMaxSelfOrgLSAs (INT4 i4SetValFutOspfMaxSelfOrgLSAs)
{

    UNUSED_PARAM (i4SetValFutOspfMaxSelfOrgLSAs);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfMaxRoutes
 Input       :  The Indices

                The Object 
                setValFutOspfMaxRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMaxRoutes (INT4 i4SetValFutOspfMaxRoutes)
{

    UNUSED_PARAM (i4SetValFutOspfMaxRoutes);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfTraceLevel
 Input       :  The Indices

                The Object 
                setValFutOspfTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfTraceLevel (INT4 i4SetValFutOspfTraceLevel)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef TRACE_WANTED
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4SetValFutOspfTraceLevel);
#ifdef TRACE_WANTED
    RM_GET_SEQ_NUM (&u4SeqNum);

    pOspfCxt->u4OspfTrace = i4SetValFutOspfTraceLevel;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfTraceLevel;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfTraceLevel) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfTraceLevel));
#endif
    return SNMP_SUCCESS;
#else
    return SNMP_FAILURE;
#endif /* TRACE_WANTED */
}

/****************************************************************************
 Function    :  nmhSetFutOspfMinLsaInterval
 Input       :  The Indices

                The Object 
                setValFutOspfMinLsaInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfMinLsaInterval (INT4 i4SetValFutOspfMinLsaInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    pOspfCxt->u2MinLsaInterval = (UINT2) i4SetValFutOspfMinLsaInterval;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfMinLsaInterval;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfMinLsaInterval) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfMinLsaInterval));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfABRType
 Input       :  The Indices

                The Object
                setValFutOspfABRType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfABRType (INT4 i4SetValFutOspfABRType)
{
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If setting value is same as the present value 
     * no need to post any message. */
    if (pOspfCxt->u4ABRType == (UINT1) i4SetValFutOspfABRType)
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfABRType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfABRType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    /* If the router is disabled no need to post the 
     * messgage. Just set the value. */
    if (pOspfCxt->admnStat == OSPF_DISABLED)
    {
        pOspfCxt->u4ABRType = i4SetValFutOspfABRType;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfABRType));
#endif
        return SNMP_SUCCESS;
    }
    /* Post the message to indicate some action 
     * needs to be taken when the ABR type is changed */
    else
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValFutOspfABRType));
#endif

            return SNMP_FAILURE;
        }

        pOspfIfParam->u1OpCode = OSPF_SET_ABR_TYPE;
        pOspfIfParam->param.abrtype_param.u4ABRType =
            (UINT4) i4SetValFutOspfABRType;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValFutOspfABRType));
#endif
            return SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfABRType));
#endif
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetFutOspfNssaAsbrDefRtTrans
 Input       :  The Indices

                The Object 
                setValFutOspfNssaAsbrDefRtTrans
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfNssaAsbrDefRtTrans (INT4 i4SetValFutOspfNssaAsbrDefRtTrans)
{

    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfNssaAsbrDefRtTrans;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfNssaAsbrDefRtTrans) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt->bNssaAsbrDefRtTrans = (UINT4) i4SetValFutOspfNssaAsbrDefRtTrans;
    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfNssaAsbrDefRtTrans));
#endif
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_SET_ASBR_TRANS_STATUS;
    pOspfIfParam->pOspfCxt = pOspfCxt;
    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfNssaAsbrDefRtTrans));
#endif
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfNssaAsbrDefRtTrans));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfDefaultPassiveInterface
 Input       :  The Indices

                The Object
                setValFutOspfDefaultPassiveInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfDefaultPassiveInterface (INT4
                                      i4SetValFutOspfDefaultPassiveInterface)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfDefaultPassiveInterface;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfDefaultPassiveInterface) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt->bDefaultPassiveInterface =
        (UINT4) i4SetValFutOspfDefaultPassiveInterface;
    pOspfIfParam->u1OpCode = OSPF_SET_ALL_IF_PASSIVE;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfDefaultPassiveInterface));
#endif
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfDefaultPassiveInterface));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfSpfHoldtime
 Input       :  The Indices

                The Object
                setValFutOspfSpfHoldtime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfSpfHoldtime (INT4 i4SetValFutOspfSpfHoldtime)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfSpfHoldtime;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfSpfHoldtime) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt->u4SpfHoldTimeInterval = (UINT4) i4SetValFutOspfSpfHoldtime;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfSpfHoldtime));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfSpfDelay
 Input       :  The Indices

                The Object
                setValFutOspfSpfDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfSpfDelay (INT4 i4SetValFutOspfSpfDelay)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfSpfDelay;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfSpfDelay) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt->u4SpfInterval = (UINT4) i4SetValFutOspfSpfDelay;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfSpfDelay));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRestartSupport
 Input       :  The Indices

                The Object
                setValFutOspfRestartSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRestartSupport (INT4 i4SetValFutOspfRestartSupport)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRestartSupport;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRestartSupport) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u1RestartSupport == i4SetValFutOspfRestartSupport)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_SET_RESTART_SUPPORT;
    pOspfIfParam->param.restartSupportParam.u1RestartSupport =
        (UINT1) i4SetValFutOspfRestartSupport;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfRestartSupport));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRestartInterval
 Input       :  The Indices

                The Object
                setValFutOspfRestartInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRestartInterval (INT4 i4SetValFutOspfRestartInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRestartInterval;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRestartInterval) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u4GracePeriod == (UINT4) i4SetValFutOspfRestartInterval)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_SET_GRACE_PERIOD;
    pOspfIfParam->param.gracePeriodParam.u4GracePeriod =
        (UINT4) i4SetValFutOspfRestartInterval;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfRestartInterval));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRestartStrictLsaChecking
 Input       :  The Indices

                The Object
                setValFutOspfRestartStrictLsaChecking
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfRestartStrictLsaChecking
    (INT4 i4SetValFutOspfRestartStrictLsaChecking)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRestartStrictLsaChecking;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIOspfRestartStrictLsaChecking) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u1StrictLsaCheck == i4SetValFutOspfRestartStrictLsaChecking)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }
    pOspfIfParam->u1OpCode = OSPF_GR_SET_STRICT_LSA_CHECK;
    pOspfIfParam->param.helper_param.u1StrictLsaCheck =
        (UINT1) i4SetValFutOspfRestartStrictLsaChecking;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfRestartStrictLsaChecking));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfHelperSupport
 Input       :  The Indices

                The Object
                setValFutOspfHelperSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfHelperSupport (tSNMP_OCTET_STRING_TYPE *
                            pSetValFutOspfHelperSupport)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfHelperSupport;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfHelperSupport) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pOspfCxt->u1HelperSupport ==
        ((UINT1) *pSetValFutOspfHelperSupport->pu1_OctetList))
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_SET_HELPER_SUPPORT;
    pOspfIfParam->param.helper_param.u1HelperSupport =
        (UINT1) (*pSetValFutOspfHelperSupport->pu1_OctetList);

    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", gOsRtr.u4OspfCxtId,
                      pSetValFutOspfHelperSupport));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfExtTraceLevel
 Input       :  The Indices

                The Object
                setValFutOspfExtTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtTraceLevel (INT4 i4SetValFutOspfExtTraceLevel)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtTraceLevel;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtTraceLevel) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pOspfCxt->u4OspfExtTrace = i4SetValFutOspfExtTraceLevel;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfExtTraceLevel));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhSetFutospfRouterIdPermanence
  Input       :  The Indices
                 The Object
                 setValFutospfRouterIdPermanence
  Output      :  The Set Low Lev Routine Take the Indices &                                                       Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutospfRouterIdPermanence (INT4 i4SetValFutospfRouterIdPermanence)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum;
    /* update routerid status and notify to snmp */
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRouterIdPermanence;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIOspfRouterIdPermanence) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt->i4RouterIdStatus = i4SetValFutospfRouterIdPermanence;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutospfRouterIdPermanence));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfHelperGraceTimeLimit
 Input       :  The Indices

                The Object 
                setValFutOspfHelperGraceTimeLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfHelperGraceTimeLimit (INT4 i4SetValFutOspfHelperGraceTimeLimit)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfHelperGraceTimeLimit;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIOspfHelperGraceTimeLimit) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u4HelperGrTimeLimit ==
        ((UINT4) i4SetValFutOspfHelperGraceTimeLimit))
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }
    pOspfIfParam->u1OpCode = OSPF_GR_SET_HELPER_GRTIMELIMIT;
    pOspfIfParam->param.helper_param.u4HelperGrTimeLimit =
        (UINT4) i4SetValFutOspfHelperGraceTimeLimit;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfHelperGraceTimeLimit));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfRestartAckState
 Input       :  The Indices

                The Object 
                setValFutOspfRestartAckState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRestartAckState (INT4 i4SetValFutOspfRestartAckState)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRestartAckState;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRestartAckState) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u4GraceAckState == (UINT4) i4SetValFutOspfRestartAckState)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_SET_ACK_STATE;
    pOspfIfParam->param.restart_param.u4GraceAckState =
        (UINT4) i4SetValFutOspfRestartAckState;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfRestartAckState));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfGraceLsaRetransmitCount
 Input       :  The Indices

                The Object 
                setValFutOspfGraceLsaRetransmitCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfGraceLsaRetransmitCount (INT4
                                      i4SetValFutOspfGraceLsaRetransmitCount)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfGraceLsaRetransmitCount;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIOspfGraceLsaRetransmitCount) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pOspfCxt->u1GrLsaMaxTxCount == i4SetValFutOspfGraceLsaRetransmitCount)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_SET_GRACELSA_MAXCOUNT;
    pOspfIfParam->param.restart_param.u1GrLsaMaxTxCount =
        (UINT1) i4SetValFutOspfGraceLsaRetransmitCount;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfGraceLsaRetransmitCount));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfRestartReason
 Input       :  The Indices

                The Object 
                setValFutOspfRestartReason
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRestartReason (INT4 i4SetValFutOspfRestartReason)
{

    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRestartReason;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRestartReason) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pOspfCxt->u1RestartReason == i4SetValFutOspfRestartReason)
    {
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_SET_GR_RESTARTREASON;
    pOspfIfParam->param.restart_param.u1RestartReason =
        (UINT1) i4SetValFutOspfRestartReason;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfRestartReason));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfGrShutdown
 Input       :  The Indices

                The Object
                setValFutOspfGrShutdown
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfGrShutdown (INT4 i4SetValFutOspfGrShutdown)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_GR_PERFORM_RESTART;
    if (i4SetValFutOspfGrShutdown == OSPF_GR_UNPLANNED_SHUTDOWN)
    {
        pOspfIfParam->param.restart_param.u1RestartReason = OSPF_GR_UNKNOWN;
    }
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
        return SNMP_FAILURE;
    }
    IssSetModuleSystemControl (OSPF_MODULE_ID, MODULE_SHUTDOWN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRTStaggeringInterval
 Input       :  The Indices

                The Object 
               setValFutOspfRTStaggeringInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRTStaggeringInterval (UINT4 u4SetValFutOspfRTStaggeringInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRTStaggeringInterval;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRTStaggeringInterval)
        / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pOspfCxt->u4RTStaggeringInterval =
        (UINT4) (u4SetValFutOspfRTStaggeringInterval);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;    /* KlocWorks warning removal */
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u", gOsRtr.u4OspfCxtId,
                      u4SetValFutOspfRTStaggeringInterval));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfBfdStatus
 Input       :  The Indices
                The Object
                setValFutOspfBfdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFutOspfBfdStatus (INT4 i4SetValFutOspfBfdStatus)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfBfdStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfBfdStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pOspfCxt->u1BfdAdminStatus = (UINT1) i4SetValFutOspfBfdStatus;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            if (pNbr->u1NbrBfdState == OSPF_BFD_ENABLED &&
                pOspfCxt->u1BfdAdminStatus == OSPF_BFD_DISABLED)
            {
                pInterface->u1BfdIfStatus = OSPF_BFD_DISABLED;
                pOspfCxt->u1BfdStatusInAllIf = OSPF_BFD_DISABLED;
                IfOspfBfdDeRegister (pNbr, OSIX_FALSE);
            }
            if (pOspfCxt->u1BfdAdminStatus == OSPF_BFD_ENABLED)
            {
                if ((pNbr->pInterface->u1NetworkType == IF_NBMA) ||
                    (pNbr->pInterface->u1NetworkType == IF_BROADCAST))
                {
                    if ((pNbr->pInterface->u1IsmState == IFS_DR) ||
                        (pNbr->pInterface->u1IsmState == IFS_BACKUP) ||
                        (pNbr->pInterface->u1IsmState == IFS_DR_OTHER))
                    {
                        if ((((pNbr->pInterface->u1IsmState == IFS_DR) ||
                              (pNbr->pInterface->u1IsmState == IFS_BACKUP)) &&
                             (pNbr->u1NsmState == NBRS_FULL)) ||
                            (((pNbr->pInterface->u1IsmState != IFS_DR) &&
                              (pNbr->pInterface->u1IsmState != IFS_BACKUP)) &&
                             (pNbr->u1NsmState == NBRS_2WAY)))
                        {
                            IfOspfBfdRegister (pNbr);
                        }
                    }
                }
                else if ((pNbr->pInterface->u1NetworkType == IF_PTOP)
                         || (pNbr->pInterface->u1NetworkType == IF_PTOMP))
                {
                    if (pNbr->u1NsmState == NBRS_FULL)
                    {
                        IfOspfBfdRegister (pNbr);
                    }
                }
            }
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfBfdStatus));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfBfdAllIfState
 Input       :  The Indices

                The Object
                setValFutOspfBfdAllIfState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFutOspfBfdAllIfState (INT4 i4SetValFutOspfBfdAllIfState)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Checking for Global BFD Admin status */
    if (pOspfCxt->u1BfdAdminStatus == OSPF_BFD_DISABLED)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfBfdAllIfState;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfBfdAllIfState) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
#endif
    pOspfCxt->u1BfdStatusInAllIf = (UINT1) i4SetValFutOspfBfdAllIfState;
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pOspfCxt->u1BfdStatusInAllIf == OSPF_BFD_ENABLED)
        {
            pInterface->u1BfdIfStatus = OSPF_BFD_ENABLED;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if ((pNbr->pInterface->u1NetworkType == IF_NBMA) ||
                    (pNbr->pInterface->u1NetworkType == IF_BROADCAST))
                {
                    if ((pNbr->pInterface->u1IsmState == IFS_DR) ||
                        (pNbr->pInterface->u1IsmState == IFS_BACKUP) ||
                        (pNbr->pInterface->u1IsmState == IFS_DR_OTHER))
                    {
                        if ((((pNbr->pInterface->u1IsmState == IFS_DR) ||
                              (pNbr->pInterface->u1IsmState == IFS_BACKUP)) &&
                             (pNbr->u1NsmState == NBRS_FULL)) ||
                            (((pNbr->pInterface->u1IsmState != IFS_DR) &&
                              (pNbr->pInterface->u1IsmState != IFS_BACKUP)) &&
                             (pNbr->u1NsmState == NBRS_2WAY)))
                        {
                            IfOspfBfdRegister (pNbr);
                        }
                    }
                }
                else if ((pNbr->pInterface->u1NetworkType == IF_PTOP)
                         || (pNbr->pInterface->u1NetworkType == IF_PTOMP))
                {
                    if (pNbr->u1NsmState == NBRS_FULL)
                    {
                        IfOspfBfdRegister (pNbr);
                    }
                }
            }
        }
        else
        {
            pInterface->u1BfdIfStatus = OSPF_BFD_DISABLED;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                if (pNbr->u1NbrBfdState == OSPF_BFD_ENABLED)
                {
                    IfOspfBfdDeRegister (pNbr, OSIX_FALSE);
                }
            }
        }

    }
#ifdef SNMP_2_WANTED
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfBfdAllIfState));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRTStaggeringStatus
 Input       :  The Indices

                The Object 
                setValFutOspfRTStaggeringStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRTStaggeringStatus (INT4 i4SetValFutOspfRTStaggeringStatus)
{
    UINT4               u4SeqNum = 0;
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRTStaggeringStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRTStaggeringStatus)
        / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
#endif
    gOsRtr.u4RTStaggeringStatus = (UINT4) i4SetValFutOspfRTStaggeringStatus;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;    /* KlocWorks warning removal */
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFutOspfRTStaggeringStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfAreaNSSATranslatorRole
 Input       :  The Indices
                OspfAreaId

                The Object 
                setValOspfAreaNSSATranslatorRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfAreaNSSATranslatorRole (UINT4 u4FutOspfAreaId,
                                     INT4 i4SetValFutOspfAreaNSSATranslatorRole)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);

    if (pArea == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not Present\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfAreaNSSATranslatorRole;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfAreaNSSATranslatorRole) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
#endif
    pArea->u1NssaTrnsltrRole = (UINT1) i4SetValFutOspfAreaNSSATranslatorRole;
    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfAreaId,
                          i4SetValFutOspfAreaNSSATranslatorRole));
#endif
        return SNMP_FAILURE;
    }

    pOspfIfParam->u1OpCode = OSPF_TRNSLTR_ROLE_CHANGE;
    pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfAreaId,
                          i4SetValFutOspfAreaNSSATranslatorRole));
#endif
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfAreaId, i4SetValFutOspfAreaNSSATranslatorRole));
#endif
    return SNMP_SUCCESS;
}

/*************************************************************
 Function    :  nmhFutSetOspfAreaNSSATranslatorStabilityInterval
 Input       :  The Indices
                OspfAreaId

                The Object 
                setValOspfAreaNSSATranslatorStabilityInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**************************************************************/
INT1
nmhSetFutOspfAreaNSSATranslatorStabilityInterval (UINT4 u4FutOspfAreaId,
                                                  INT4
                                                  i4SetValFutOspfAreaNSSATanslatorStabilityInterval)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);

    pArea = GetFindAreaInCxt (pOspfCxt, &areaId);

    if (pArea == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not Present\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfAreaNSSATranslatorStabilityInterval;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfAreaNSSATranslatorStabilityInterval) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
#endif
    pArea->u4NssaTrnsltrStbltyInterval =
        (UINT4) i4SetValFutOspfAreaNSSATanslatorStabilityInterval;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfAreaId,
                      i4SetValFutOspfAreaNSSATanslatorStabilityInterval));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFutOspfAreaDfInfOriginate
 *  Input       :  The Indices
 *                 FutOspfAreaId
 *                 The Object
 *                 setValFutOspfAreaDfInfOriginate
 *  Output      :  The Set Low Lev Routine Take the Indices & Sets the
 *                 Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFutOspfAreaDfInfOriginate (UINT4 u4FutOspfAreaId,
                                 INT4 i4SetValFutOspfAreaDfInfOriginate)
{
    tAreaId             areaId;
    tArea              *pArea;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    UINT1               u1Index;
    tLsaInfo           *pLsaInfo;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaId);
    if (UtilIpAddrComp (areaId, gNullIpAddr) != OSPF_EQUAL)
    {
        if ((pArea = GetFindAreaInCxt (pOspfCxt, &areaId)) == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId, "Area Not Present\n");
            return SNMP_FAILURE;
        }

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfAreaDfInfOriginate;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfAreaDfInfOriginate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 2;
#endif
        if (i4SetValFutOspfAreaDfInfOriginate == DEFAULT_INFO_ORIGINATE)
        {

            pOspfIfParam->u1OpCode = OSPF_CHANGE_AREA_SUMMARY;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pArea;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            pArea->u4DfInfOriginate = (UINT4) i4SetValFutOspfAreaDfInfOriginate;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
                return SNMP_FAILURE;
            }
        }
        else
        {
            pArea->u4DfInfOriginate = (UINT4) i4SetValFutOspfAreaDfInfOriginate;

            if (pArea->u4AreaType == NSSA_AREA)
            {
                pLsaInfo = LsuSearchDatabase (NSSA_LSA,
                                              &(gDefaultDest),
                                              &(pArea->pOspfCxt->rtrId), NULL,
                                              (UINT1 *) pArea);
                if (pLsaInfo != NULL)
                {
                    AgdFlushOut (pLsaInfo);
                }

                for (u1Index = 0; u1Index < OSPF_MAX_METRIC; u1Index++)
                {
                    if (pArea->aStubDefaultCost[u1Index].u4MetricType
                        != OSPF_METRIC)
                    {
                        pArea->aStubDefaultCost[u1Index].u4MetricType =
                            OSPF_METRIC;
                    }
                }
            }
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfAreaId, i4SetValFutOspfAreaDfInfOriginate));
#endif
        KW_FALSEPOSITIVE_FIX (pMessage);
        return SNMP_SUCCESS;
    }
    OSPF_TRC (OSPF_FN_EXIT | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
              "EXIT:nmhSetFutOspfAreaDfInfOriginate : SNMP_FAILURE \n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfHostRouteIfIndex
 Input       :  The Indices
                FutOspfHostIpAddress
                FutOspfHostTOS

                The Object 
                setValFutOspfHostRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfHostRouteIfIndex (UINT4 u4FutOspfHostIpAddress,
                               INT4 i4FutOspfHostTOS,
                               INT4 i4SetValFutOspfHostRouteIfIndex)
{
    tIPADDR             hostIpAddr;
    tHost              *pHost;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tInterface         *pInterface;
    INT1                i1TmpTos;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC :nmhSetFutOspfHostRouteIfIndex\n");

    OSPF_CRU_BMC_DWTOPDU (hostIpAddr, u4FutOspfHostIpAddress);

    if ((pHost = GetFindHostInCxt (pOspfCxt, &hostIpAddr)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Host Not Found\n");
        return SNMP_FAILURE;
    }

    if (pHost->u4FwdIfIndex == (UINT4) i4SetValFutOspfHostRouteIfIndex - 1)
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfHostRouteIfIndex;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfHostRouteIfIndex) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    /* If the forwarding interface changes for a host regenerate the
     * router lsa if it was generated to previous area.
     */
    pInterface = UtilFindIfInCxt (pOspfCxt, pHost->u4FwdIfIndex);
    if ((pInterface != NULL) && (pInterface->u1IsmState > IFS_DOWN))
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4FutOspfHostIpAddress, i4FutOspfHostTOS,
                              i4SetValFutOspfHostRouteIfIndex));
#endif
            return SNMP_FAILURE;
        }
        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pHost->pArea;
        pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
        pOspfIfParam->pOspfCxt = pOspfCxt;
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                              u4FutOspfHostIpAddress, i4FutOspfHostTOS,
                              i4SetValFutOspfHostRouteIfIndex));
#endif
            return SNMP_FAILURE;
        }

    }
    /* Assign the new fwd if index & check whether the router lsa has to be
     * generated for the new area.
     */
    pHost->u4FwdIfIndex = (UINT4) i4SetValFutOspfHostRouteIfIndex - 1;

    /* Set the HOST_IF_INDEX_MASK for all the TOS for this host */
    for (i1TmpTos = 0; i1TmpTos < OSPF_MAX_METRIC; i1TmpTos += 2)
    {
        pHost->aHostCost[DECODE_TOS (i1TmpTos)].rowMask =
            pHost->aHostCost[DECODE_TOS (i1TmpTos)].rowMask |
            HOST_IF_INDEX_MASK;
    }

    pInterface = UtilFindIfInCxt (pOspfCxt, pHost->u4FwdIfIndex);
    if (pInterface != NULL)
    {
        pHost->pArea = pInterface->pArea;
    }
    if (pHost->aHostCost[DECODE_TOS ((INT1) i4FutOspfHostTOS)].rowStatus !=
        ACTIVE)
    {
        if (pHost->aHostCost[DECODE_TOS ((INT1) i4FutOspfHostTOS)].rowMask
            == HOST_METRIC_MASK)
        {
            pHost->aHostCost[DECODE_TOS ((INT1) i4FutOspfHostTOS)].
                rowStatus = NOT_IN_SERVICE;
        }
        else
        {
            pHost->aHostCost[DECODE_TOS ((INT1) i4FutOspfHostTOS)].
                rowStatus = NOT_READY;
        }
    }
    else
    {
        pInterface = UtilFindIfInCxt (pOspfCxt, pHost->u4FwdIfIndex);
        if ((pInterface != NULL) && (pInterface->u1IsmState > IFS_DOWN))
        {
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfHostIpAddress, i4FutOspfHostTOS,
                                  i4SetValFutOspfHostRouteIfIndex));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pHost->pArea;
            pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfHostIpAddress, i4FutOspfHostTOS,
                                  i4SetValFutOspfHostRouteIfIndex));
#endif
                return SNMP_FAILURE;
            }

        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfHostIpAddress, i4FutOspfHostTOS,
                      i4SetValFutOspfHostRouteIfIndex));
#endif
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT :nmhSetFutOspfHostRouteIfIndex\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfPassive
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf

                The Object
                setValFutOspfIfPassive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfPassive (UINT4 u4FutOspfIfIpAddress,
                        INT4 i4FutOspfAddressLessIf,
                        INT4 i4SetValFutOspfIfPassive)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4FutOspfAddressLessIf)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Find If Failure\n");
        return SNMP_FAILURE;
    }

    if (pInterface->bPassive == (UINT1) i4SetValFutOspfIfPassive)
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfPassive;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfIfPassive) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    if ((pInterface->ifStatus != ACTIVE) ||
        (pInterface->admnStatus != OSPF_ENABLED))
    {
        pInterface->bPassive = i4SetValFutOspfIfPassive;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfIpAddress, i4FutOspfAddressLessIf,
                          i4SetValFutOspfIfPassive));
#endif
        return SNMP_SUCCESS;
    }

    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfIpAddress, i4FutOspfAddressLessIf,
                          i4SetValFutOspfIfPassive));
#endif
        return SNMP_FAILURE;
    }

    pInterface->bPassive = (UINT1) i4SetValFutOspfIfPassive;

    pOspfIfParam->u1OpCode = OSPF_SET_IF_PASSIVE;
    pOspfIfParam->param.admn_param.pInterface = pInterface;
    pOspfIfParam->pOspfCxt = pOspfCxt;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfIpAddress, i4FutOspfAddressLessIf,
                          i4SetValFutOspfIfPassive));
#endif
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfIfIpAddress, i4FutOspfAddressLessIf,
                      i4SetValFutOspfIfPassive));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfBfdState
 Input       :  The Indices
                FutOspfIfIpAddress
                FutOspfAddressLessIf
 
                The Object
                setValFutOspfIfBfdState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFutOspfIfBfdState (UINT4 u4FutOspfIfIpAddress,
                         INT4 i4FutOspfAddressLessIf,
                         INT4 i4SetValFutOspfIfBfdState)
{
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfIpAddress);

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4FutOspfAddressLessIf)) != NULL)
    {

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfBfdState;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfIfBfdState) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SnmpNotifyInfo.i2Rsvd = 0;
#endif
        pInterface->u1BfdIfStatus = (UINT1) i4SetValFutOspfIfBfdState;
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            if (pInterface->u1BfdIfStatus == OSPF_BFD_ENABLED)
            {
                if ((pNbr->pInterface->u1NetworkType == IF_NBMA) ||
                    (pNbr->pInterface->u1NetworkType == IF_BROADCAST))
                {
                    if ((pNbr->pInterface->u1IsmState == IFS_DR) ||
                        (pNbr->pInterface->u1IsmState == IFS_BACKUP) ||
                        (pNbr->pInterface->u1IsmState == IFS_DR_OTHER))
                    {
                        if ((((pNbr->pInterface->u1IsmState == IFS_DR) ||
                              (pNbr->pInterface->u1IsmState == IFS_BACKUP)) &&
                             (pNbr->u1NsmState == NBRS_FULL)) ||
                            (((pNbr->pInterface->u1IsmState != IFS_DR) &&
                              (pNbr->pInterface->u1IsmState != IFS_BACKUP)) &&
                             (pNbr->u1NsmState == NBRS_2WAY)))
                        {
                            IfOspfBfdRegister (pNbr);
                        }
                    }
                }
                else if ((pNbr->pInterface->u1NetworkType == IF_PTOP)
                         || (pNbr->pInterface->u1NetworkType == IF_PTOMP))
                {
                    if (pNbr->u1NsmState == NBRS_FULL)
                    {
                        IfOspfBfdRegister (pNbr);
                    }
                }
            }
            if (pInterface->u1BfdIfStatus != OSPF_BFD_ENABLED &&
                pNbr->u1NbrBfdState == OSPF_BFD_ENABLED)
            {
                IfOspfBfdDeRegister (pNbr, OSIX_FALSE);
            }
        }
#ifdef SNMP_2_WANTED
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfIpAddress, i4FutOspfAddressLessIf,
                          i4SetValFutOspfIfBfdState));
#endif
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteMetric
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteMetric (UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             INT4 i4SetValFutOspfExtRouteMetric)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest, &extRouteMask);

    if (pExtRoute == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Entry Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].u4Value
        == (UINT4) i4SetValFutOspfExtRouteMetric)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Metric - No Change \n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteMetric;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtRouteMetric) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].u4Value =
        (UINT4) i4SetValFutOspfExtRouteMetric;

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        rowStatus == ACTIVE)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteMetric));
#endif
            return SNMP_FAILURE;
        }
        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteMetric));
#endif
            return SNMP_FAILURE;
        }

    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, i4SetValFutOspfExtRouteMetric));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteMetricType
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteMetricType (UINT4 u4FutOspfExtRouteDest,
                                 UINT4 u4FutOspfExtRouteMask,
                                 INT4 i4FutOspfExtRouteTOS,
                                 INT4 i4SetValFutOspfExtRouteMetricType)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest, &extRouteMask);

    if (pExtRoute == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Entry Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if (pExtRoute->extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        u1MetricType == (UINT1) i4SetValFutOspfExtRouteMetricType)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId,
                  " Ext Route Metric Type - No Change \n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteMetricType;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfExtRouteMetricType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    pExtRoute->extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        u1MetricType = (UINT1) i4SetValFutOspfExtRouteMetricType;

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        rowStatus == ACTIVE)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteMetricType));
#endif
            return SNMP_FAILURE;
        }
        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteMetricType));
#endif
            return SNMP_FAILURE;
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, i4SetValFutOspfExtRouteMetricType));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteTag
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteTag (UINT4 u4FutOspfExtRouteDest,
                          UINT4 u4FutOspfExtRouteMask,
                          INT4 i4FutOspfExtRouteTOS,
                          INT4 i4SetValFutOspfExtRouteTag)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest, &extRouteMask);
    if (pExtRoute == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Entry Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if (pExtRoute->extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        u4ExtTag == (UINT4) i4SetValFutOspfExtRouteTag)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Tag - No Change \n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteTag;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtRouteTag) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    pExtRoute->extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        u4ExtTag = (UINT4) i4SetValFutOspfExtRouteTag;

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        rowStatus == ACTIVE)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteTag));
#endif
            return SNMP_FAILURE;
        }

        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteTag));
#endif
            return SNMP_FAILURE;
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, i4SetValFutOspfExtRouteTag));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteFwdAdr
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteFwdAdr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteFwdAdr (UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             UINT4 u4SetValFutOspfExtRouteFwdAdr)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tIPADDR             fwdAdr;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);
    OSPF_CRU_BMC_DWTOPDU (fwdAdr, u4SetValFutOspfExtRouteFwdAdr);

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &(extRouteDest),
                                      &(extRouteMask));
    if (pExtRoute == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Entry Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if (UtilIpAddrComp (pExtRoute->extrtParam[DECODE_TOS
                                              ((INT1) i4FutOspfExtRouteTOS)].
                        fwdAddr, fwdAdr) == OSPF_EQUAL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route fwdAdr - No Change \n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteFwdAdr;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtRouteFwdAdr) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    IP_ADDR_COPY (pExtRoute->extrtParam
                  [DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].fwdAddr, fwdAdr);

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        rowStatus == ACTIVE)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              u4SetValFutOspfExtRouteFwdAdr));
#endif
            return SNMP_FAILURE;
        }

        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              u4SetValFutOspfExtRouteFwdAdr));
#endif
            return SNMP_FAILURE;
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, u4SetValFutOspfExtRouteFwdAdr));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteIfIndex
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteIfIndex (UINT4 u4FutOspfExtRouteDest,
                              UINT4 u4FutOspfExtRouteMask,
                              INT4 i4FutOspfExtRouteTOS,
                              INT4 i4SetValFutOspfExtRouteIfIndex)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfExtRouteIfIndex\n");

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest, &extRouteMask);

    if (pExtRoute == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Entry Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if (pExtRoute->extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        u4FwdIfIndex == (UINT4) i4SetValFutOspfExtRouteIfIndex)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId,
                  " Ext Route fwdIfIndex - No Change \n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteIfIndex;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtRouteIfIndex) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    pExtRoute->extrtParam[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        u4FwdIfIndex = (UINT4) i4SetValFutOspfExtRouteIfIndex;

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        rowStatus == ACTIVE)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteIfIndex));
#endif
            return SNMP_FAILURE;
        }
        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteIfIndex));
#endif
            return SNMP_FAILURE;
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, i4SetValFutOspfExtRouteIfIndex));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteNextHop
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteNextHop
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteNextHop (UINT4 u4FutOspfExtRouteDest,
                              UINT4 u4FutOspfExtRouteMask,
                              INT4 i4FutOspfExtRouteTOS,
                              UINT4 u4SetValFutOspfExtRouteNextHop)
{
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tIPADDR             nextHop;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);
    OSPF_CRU_BMC_DWTOPDU (nextHop, u4SetValFutOspfExtRouteNextHop);

    pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest, &extRouteMask);

    if (pExtRoute == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " Ext Route Entry Does Not Exist\n");
        return SNMP_FAILURE;
    }

    if (UtilIpAddrComp (pExtRoute->extrtParam[DECODE_TOS
                                              ((INT1) i4FutOspfExtRouteTOS)].
                        fwdNextHop, nextHop) == OSPF_EQUAL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId,
                  " Ext Route fwd Next Hop - No Change \n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteNextHop;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtRouteNextHop) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    IP_ADDR_COPY (pExtRoute->extrtParam
                  [DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].fwdNextHop,
                  nextHop);

    if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
        rowStatus == ACTIVE)
    {
        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              u4SetValFutOspfExtRouteNextHop));
#endif
            return SNMP_FAILURE;
        }

        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
        pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              u4SetValFutOspfExtRouteNextHop));
#endif
            return SNMP_FAILURE;
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, u4SetValFutOspfExtRouteNextHop));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfExtRouteStatus
 Input       :  The Indices
                FutOspfExtRouteDest
                FutOspfExtRouteMask
                FutOspfExtRouteTOS

                The Object 
                setValFutOspfExtRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfExtRouteStatus (UINT4 u4FutOspfExtRouteDest,
                             UINT4 u4FutOspfExtRouteMask,
                             INT4 i4FutOspfExtRouteTOS,
                             INT4 i4SetValFutOspfExtRouteStatus)
{
    tIPADDR             extRouteDest;
    tIPADDRMASK         extRouteMask;
    tExtRoute          *pExtRoute;
    tOspfSnmpIfParam   *pOspfIfParam = NULL;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    UINT1               u1NewExtRtFlag = OSPF_FALSE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfExtRouteStatus\n");

    OSPF_CRU_BMC_DWTOPDU (extRouteDest, u4FutOspfExtRouteDest);
    OSPF_CRU_BMC_DWTOPDU (extRouteMask, u4FutOspfExtRouteMask);

    if ((i4SetValFutOspfExtRouteStatus == DESTROY) &&
        (!(IS_VALID_TOS_VALUE (i4FutOspfExtRouteTOS))))
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfExtRouteStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfExtRouteStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    switch ((UINT1) i4SetValFutOspfExtRouteStatus)
    {
        case CREATE_AND_GO:
            pExtRoute = ExtrtRtAddDefaultValuesInCxt (pOspfCxt, extRouteDest,
                                                      extRouteMask,
                                                      (INT1) DECODE_TOS
                                                      (i4FutOspfExtRouteTOS));
            if (pExtRoute == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          " Ext Route Creation Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            pExtRoute->u1ExtRtIsStatic = OSIX_TRUE;
            pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
                rowStatus = ACTIVE;

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = OSPF_EXTRT_ADD;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case CREATE_AND_WAIT:
            if ((pExtRoute = ExtrtAddInCxt (pOspfCxt, extRouteDest,
                                            extRouteMask,
                                            (INT1) DECODE_TOS ((INT1)
                                                               (i4FutOspfExtRouteTOS)),
                                            (UINT4) MIN_METRIC,
                                            (UINT1) DEFAULT_METRIC_TYPE,
                                            (UINT4) DEFAULT_EXTRT_TAG,
                                            gNullIpAddr,
                                            (UINT2) DEFAULT_FWD_IF_INDEX,
                                            gNullIpAddr,
                                            (UINT2) STATIC_RT,
                                            (UINT4) INVALID,
                                            (UINT1) INVALID)) == NULL)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            pExtRoute->aMetric[DECODE_TOS
                               ((INT1) i4FutOspfExtRouteTOS)].rowStatus =
                NOT_READY;
            pExtRoute->u1ExtRtIsStatic = OSIX_TRUE;

            break;

        case NOT_IN_SERVICE:
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteStatus));
#endif
            return SNMP_FAILURE;

        case ACTIVE:
            pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                              &extRouteMask);
            if (pExtRoute == NULL)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            if (pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
                rowStatus == NOT_READY)
            {
                u1NewExtRtFlag = OSPF_TRUE;
            }

            pExtRoute->u1ExtRtIsStatic = OSIX_TRUE;
            pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
                rowStatus = ACTIVE;

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            if (u1NewExtRtFlag == OSPF_TRUE)
            {
                pOspfIfParam->u1OpCode = OSPF_EXTRT_ADD;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;

            }
            else
            {
                pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                pOspfIfParam->param.sig_param.u1Type = AS_EXT_LSA;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
            }
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case DESTROY:
            pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                              &extRouteMask);
            if (pExtRoute == NULL)
            {
                /* Dummy syncup message has to be send */
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_SUCCESS;
            }
            pExtRoute->aMetric[DECODE_TOS ((INT1) i4FutOspfExtRouteTOS)].
                rowStatus = INVALID;
            /* delete Route if all TOS entries are invalid */
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            pOspfIfParam->u1OpCode = OSPF_EXTRT_DELETE;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pExtRoute;
            pOspfIfParam->pOspfCxt = pOspfCxt;
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                                  i4FutOspfExtRouteTOS,
                                  i4SetValFutOspfExtRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            break;

        default:
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                              i4FutOspfExtRouteTOS,
                              i4SetValFutOspfExtRouteStatus));
#endif
            return SNMP_FAILURE;

            /* end switch */
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfExtRouteDest, u4FutOspfExtRouteMask,
                      i4FutOspfExtRouteTOS, i4SetValFutOspfExtRouteStatus));
#endif
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : nmhSetFutOspfExtRouteStatus\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfSecIfStatus
 Input       :  The Indices
                FutOspfPrimIpAddr
                FutOspfPrimAddresslessIf
                FutOspfSecIpAddr
                FutOspfSecIpAddrMask

                The Object 
                setValFutOspfSecIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfSecIfStatus (UINT4 u4FutOspfPrimIpAddr,
                          INT4 i4FutOspfPrimAddresslessIf,
                          UINT4 u4FutOspfSecIpAddr,
                          UINT4 u4FutOspfSecIpAddrMask,
                          INT4 i4SetValFutOspfSecIfStatus)
{
    tInterface         *pInterface = NULL;
    tIPADDR             primIpAddr;
    tIPADDR             secIpAddr;
    tIPADDRMASK         secIpAddrMask;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage = (tOspfQMsg *) NULL;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT2                i2SecCount = 0;
    INT1                i1ExitStat = SNMP_SUCCESS + SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfSecIfStatus\n");

    OSPF_CRU_BMC_DWTOPDU (primIpAddr, u4FutOspfPrimIpAddr);
    OSPF_CRU_BMC_DWTOPDU (secIpAddr, u4FutOspfSecIpAddr);
    OSPF_CRU_BMC_DWTOPDU (secIpAddrMask, u4FutOspfSecIpAddrMask);

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfSecIfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfSecIfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 5;
#endif
    /*If interface is not available, can't proceds further */
    if ((pInterface = GetFindIfInCxt (pOspfCxt, primIpAddr,
                                      (UINT4) i4FutOspfPrimAddresslessIf))
        != NULL)
    {
        switch ((UINT1) i4SetValFutOspfSecIfStatus)
        {
            case CREATE_AND_GO:

                while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
                {
                    if (UtilAddrMaskCmpe
                        (OSPF_CRU_BMC_DWFROMPDU
                         (pInterface->secondaryIP.ifSecIp[i2SecCount].
                          ifSecIpAddr),
                         OSPF_CRU_BMC_DWFROMPDU (pInterface->secondaryIP.
                                                 ifSecIp[i2SecCount].
                                                 ifSecIpAddrMask),
                         u4FutOspfSecIpAddr,
                         u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
                    {
                        /* Since the row already exists, no need to create it 
                           again */
                        i1ExitStat = SNMP_FAILURE;
                        break;
                    }
                    i2SecCount++;
                }                /* end while */

                if (i1ExitStat == SNMP_FAILURE)
                    break;

                /* Primary Interface exists, Sec needs to be created */
                i2SecCount = (pInterface->secondaryIP.i1SecIpCount)++;
                IP_ADDR_COPY (pInterface->secondaryIP.
                              ifSecIp[i2SecCount].ifSecIpAddr, secIpAddr);
                IP_ADDR_COPY (pInterface->secondaryIP.
                              ifSecIp[i2SecCount].ifSecIpAddrMask,
                              secIpAddrMask);
                pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status = ACTIVE;

                if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
                {
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                    i1ExitStat = SNMP_FAILURE;
                    break;
                }

                pOspfIfParam
                    =
                    (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);

                pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                pOspfIfParam->param.sig_param.pStruct =
                    (UINT1 *) pInterface->pArea;
                pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
                pOspfIfParam->pOspfCxt = pOspfCxt;

                if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                {
                    OSPF_SNMP_IF_MSG_FREE (pMessage);
                    OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                              OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                              "OSPF_SNMP_IF_MSG_SEND Failure\n");
                    i1ExitStat = SNMP_FAILURE;
                    break;
                }

                i1ExitStat = SNMP_SUCCESS;
                break;

            case CREATE_AND_WAIT:
                /* Same as CREATE_AND_GO with status assigned to NOT_READY. */

                while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
                {
                    if (UtilAddrMaskCmpe
                        (OSPF_CRU_BMC_DWFROMPDU
                         (pInterface->secondaryIP.ifSecIp[i2SecCount].
                          ifSecIpAddr),
                         OSPF_CRU_BMC_DWFROMPDU (pInterface->secondaryIP.
                                                 ifSecIp[i2SecCount].
                                                 ifSecIpAddrMask),
                         u4FutOspfSecIpAddr,
                         u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
                    {
                        /* Since the row already exists, no need to create it 
                           again */
                        i1ExitStat = SNMP_FAILURE;
                        break;
                    }
                    i2SecCount++;
                }                /* end while */

                if (i1ExitStat == SNMP_FAILURE)
                    break;

                /* Primary Interface exists, Sec needs to be created */
                i2SecCount = (pInterface->secondaryIP.i1SecIpCount)++;
                IP_ADDR_COPY (pInterface->secondaryIP.
                              ifSecIp[i2SecCount].ifSecIpAddr, secIpAddr);
                IP_ADDR_COPY (pInterface->secondaryIP.
                              ifSecIp[i2SecCount].ifSecIpAddrMask,
                              secIpAddrMask);
                pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status =
                    NOT_READY;

                i1ExitStat = SNMP_SUCCESS;
                break;

            case ACTIVE:

                while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
                {
                    if (UtilAddrMaskCmpe
                        (OSPF_CRU_BMC_DWFROMPDU
                         (pInterface->secondaryIP.ifSecIp[i2SecCount].
                          ifSecIpAddr),
                         OSPF_CRU_BMC_DWFROMPDU (pInterface->secondaryIP.
                                                 ifSecIp[i2SecCount].
                                                 ifSecIpAddrMask),
                         u4FutOspfSecIpAddr,
                         u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
                    {
                        if (pInterface->secondaryIP.ifSecIp[i2SecCount].u1Status
                            == ACTIVE)
                        {
                            i1ExitStat = SNMP_SUCCESS;
                            break;
                        }

                        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
                        {
                            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                            i1ExitStat = SNMP_FAILURE;
                            break;
                        }

                        pOspfIfParam = (tOspfSnmpIfParam *)
                            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);

                        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                        pOspfIfParam->param.sig_param.pStruct =
                            (UINT1 *) pInterface->pArea;
                        pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
                        pOspfIfParam->pOspfCxt = pOspfCxt;

                        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                        {
                            OSPF_SNMP_IF_MSG_FREE (pMessage);
                            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
                            i1ExitStat = SNMP_FAILURE;
                            break;
                        }
                        /* Both prim and sec ifaces exist. */
                        pInterface->secondaryIP.ifSecIp[i2SecCount].
                            u1Status = ACTIVE;

                        i1ExitStat = SNMP_SUCCESS;
                        break;
                    }
                    i2SecCount++;
                }                /* End of While */

                if (!(i1ExitStat == SNMP_SUCCESS))
                    i1ExitStat = SNMP_FAILURE;

                break;            /* ACTIVE */

            case DESTROY:

                while (i2SecCount < pInterface->secondaryIP.i1SecIpCount)
                {
                    if (UtilAddrMaskCmpe
                        (OSPF_CRU_BMC_DWFROMPDU
                         (pInterface->secondaryIP.ifSecIp[i2SecCount].
                          ifSecIpAddr),
                         OSPF_CRU_BMC_DWFROMPDU (pInterface->secondaryIP.
                                                 ifSecIp[i2SecCount].
                                                 ifSecIpAddrMask),
                         u4FutOspfSecIpAddr,
                         u4FutOspfSecIpAddrMask) == OSPF_EQUAL)
                    {

                        /* shifting the entries following the one found above one   
                           way up */
                        while (i2SecCount < (pInterface->secondaryIP.
                                             i1SecIpCount - 1))
                        {
                            if (i2SecCount < (MAX_SEC_INTERFACES - 1))
                            {
                                IP_ADDR_COPY (pInterface->secondaryIP.ifSecIp
                                              [i2SecCount].ifSecIpAddr,
                                              pInterface->secondaryIP.
                                              ifSecIp[i2SecCount +
                                                      1].ifSecIpAddr);
                                IP_ADDR_COPY (pInterface->secondaryIP.
                                              ifSecIp[i2SecCount].
                                              ifSecIpAddrMask,
                                              pInterface->secondaryIP.
                                              ifSecIp[i2SecCount +
                                                      1].ifSecIpAddrMask);

                                pInterface->secondaryIP.ifSecIp[i2SecCount].
                                    u1Status =
                                    pInterface->secondaryIP.ifSecIp[i2SecCount +
                                                                    1].u1Status;
                            }
                            i2SecCount++;
                        }        /* end while */
                        pInterface->secondaryIP.i1SecIpCount--;

                        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
                        {
                            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                            i1ExitStat = SNMP_FAILURE;
                            break;
                        }

                        pOspfIfParam = (tOspfSnmpIfParam *)
                            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);

                        pOspfIfParam->u1OpCode = OSPF_REGENERATE_LSA;
                        pOspfIfParam->param.sig_param.pStruct =
                            (UINT1 *) pInterface->pArea;
                        pOspfIfParam->param.sig_param.u1Type = ROUTER_LSA;
                        pOspfIfParam->pOspfCxt = pOspfCxt;

                        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
                        {
                            OSPF_SNMP_IF_MSG_FREE (pMessage);
                            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
                            i1ExitStat = SNMP_FAILURE;
                            break;
                        }

                        i1ExitStat = SNMP_SUCCESS;
                        break;
                    }
                    i2SecCount++;
                }                /* End of While */

                if (!(i1ExitStat == SNMP_SUCCESS))
                    i1ExitStat = SNMP_FAILURE;

                break;

            case NOT_IN_SERVICE:
                /* NOT_IN_SERVICE is not supported
                 */
                /* CASE falls through
                 */
            default:
                i1ExitStat = SNMP_FAILURE;
                break;
        }                        /* end switch */
    }
    else
    {
        if (i4SetValFutOspfSecIfStatus == DESTROY)
        {
            i1ExitStat = SNMP_SUCCESS;
        }
        else
        {
            i1ExitStat = SNMP_FAILURE;
        }
    }
    if (!(i1ExitStat == SNMP_FAILURE))
        i1ExitStat = SNMP_SUCCESS;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT : nmhSetFutOspfSecIfStatus\n");
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1ExitStat;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfPrimIpAddr, i4FutOspfPrimAddresslessIf,
                      u4FutOspfSecIpAddr, u4FutOspfSecIpAddrMask,
                      i4SetValFutOspfSecIfStatus));
#endif
    return (i1ExitStat);
}

/****************************************************************************
 Function    :  nmhSetFutOspfAreaAggregateExternalTag
 Input       :  The Indices
                FutOspfAreaAggregateAreaID
                FutOspfAreaAggregateLsdbType
                FutOspfAreaAggregateNet
                FutOspfAreaAggregateMask

                The Object 
                setValFutOspfAreaAggregateEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfAreaAggregateExternalTag (UINT4 u4FutOspfAreaAggregateAreaID,
                                       INT4 i4FutOspfAreaAggregateLsdbType,
                                       UINT4 u4FutOspfAreaAggregateNet,
                                       UINT4 u4FutOspfAreaAggregateMask,
                                       INT4
                                       i4SetValFutOspfAreaAggregateExternalTag)
{
    tAreaId             areaId;
    tIPADDR             ipAddr;
    tIPADDR             addrMask;
    tArea              *pArea = (tArea *) NULL;
    INT1                i1Index;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAreaAggregateAreaID);
    OSPF_CRU_BMC_DWTOPDU (ipAddr, u4FutOspfAreaAggregateNet);
    OSPF_CRU_BMC_DWTOPDU (addrMask, u4FutOspfAreaAggregateMask);

    if ((pArea = (GetFindAreaInCxt (pOspfCxt, (&areaId)))) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, "Area Not Found \n");
        return SNMP_FAILURE;
    }

    if (GetFindAreaAggrIndex (pArea, &ipAddr, &addrMask,
                              (UINT1)
                              i4FutOspfAreaAggregateLsdbType,
                              &i1Index) != OSPF_FAILURE)
    {
        if (pArea->aAddrRange[i1Index].u4ExtRtTag ==
            (UINT4) i4SetValFutOspfAreaAggregateExternalTag)
            return SNMP_SUCCESS;

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
            return SNMP_FAILURE;
        }
        pOspfIfParam->u1OpCode = SET_AREA_AGGR_TAG;
        pOspfIfParam->param.area_param.pArea = pArea;
        pOspfIfParam->param.area_param.u1AggregateIndex = (UINT1) i1Index;
        pArea->aAddrRange[i1Index].u4ExtRtTag =
            (UINT4) i4SetValFutOspfAreaAggregateExternalTag;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfAreaAggregateExternalTag;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfAreaAggregateExternalTag) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 5;
#endif
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
            i1RetVal = SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = i1RetVal;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %p %p %i",
                          gOsRtr.u4OspfCxtId,
                          u4FutOspfAreaAggregateAreaID,
                          i4FutOspfAreaAggregateLsdbType,
                          u4FutOspfAreaAggregateNet, u4FutOspfAreaAggregateMask,
                          i4SetValFutOspfAreaAggregateExternalTag));
#endif
        return i1RetVal;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfAsExternalAggregationEffect
 Input       :  The Indices
                FutOspfAsExternalAggregationNet
                FutOspfAsExternalAggregationMask
                FutOspfAsExternalAggregationAreaId

                The Object 
                setValFutOspfAsExternalAggregationEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfAsExternalAggregationEffect (UINT4
                                          u4FutOspfAsExternalAggregationNet,
                                          UINT4
                                          u4FutOspfAsExternalAggregationMask,
                                          UINT4
                                          u4FutOspfAsExternalAggregationAreaId,
                                          INT4
                                          i4SetValFutOspfAsExternalAggregationEffect)
{

    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfAsExternalAggregationEffect\n");
    u4FutOspfAsExternalAggregationNet =
        (u4FutOspfAsExternalAggregationNet &
         u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);
    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);
    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);

    if (pAsExtAddrRng == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG Creation Failure\n");
        return SNMP_FAILURE;
    }

    if (pAsExtAddrRng->u1RangeEffect ==
        (UINT1) i4SetValFutOspfAsExternalAggregationEffect)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG - No Change \n");

        return SNMP_SUCCESS;
    }

    if (pAsExtAddrRng->rangeStatus == ACTIVE)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId,
                  " AS Ext AGG Active - Make not in Service\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfAsExternalAggregationEffect;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfAsExternalAggregationEffect) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    pAsExtAddrRng->u1RangeEffect =
        (UINT1) i4SetValFutOspfAsExternalAggregationEffect;

    switch ((UINT1) i4SetValFutOspfAsExternalAggregationEffect)
    {
        case RAG_ADVERTISE:
            pAsExtAddrRng->u1AttrMask =
                pAsExtAddrRng->u1AttrMask | RAG_ADV_MASK;
            break;

        case RAG_DO_NOT_ADVERTISE:
            pAsExtAddrRng->u1AttrMask =
                pAsExtAddrRng->u1AttrMask | RAG_DONOTADV_MASK;
            break;

        case RAG_ALLOW_ALL:
            pAsExtAddrRng->u1AttrMask =
                pAsExtAddrRng->u1AttrMask | RAG_ALLOW_MASK;
            break;

        case RAG_DENY_ALL:
            pAsExtAddrRng->u1AttrMask =
                pAsExtAddrRng->u1AttrMask | RAG_DENY_MASK;
            break;

        default:
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId, "Invalid AS EXT AGG Effect\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfAsExternalAggregationNet,
                              u4FutOspfAsExternalAggregationMask,
                              u4FutOspfAsExternalAggregationAreaId,
                              i4SetValFutOspfAsExternalAggregationEffect));
#endif
            return SNMP_FAILURE;

    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfAsExternalAggregationEffect\n");
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfAsExternalAggregationNet,
                      u4FutOspfAsExternalAggregationMask,
                      u4FutOspfAsExternalAggregationAreaId,
                      i4SetValFutOspfAsExternalAggregationEffect));
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfAsExternalAggregationTranslation
 Input       :  The Indices
                FutOspfAsExternalAggregationNet
                FutOspfAsExternalAggregationMask
                FutOspfAsExternalAggregationAreaId

                The Object 
                setValFutOspfAsExternalAggregationTranslation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfAsExternalAggregationTranslation (UINT4
                                               u4FutOspfAsExternalAggregationNet,
                                               UINT4
                                               u4FutOspfAsExternalAggregationMask,
                                               UINT4
                                               u4FutOspfAsExternalAggregationAreaId,
                                               INT4
                                               i4SetValFutOspfAsExternalAggregationTranslation)
{

    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfAsExternalAggregationTranslation\n");
    u4FutOspfAsExternalAggregationNet =
        (u4FutOspfAsExternalAggregationNet &
         u4FutOspfAsExternalAggregationMask);

    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);
    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);
    pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                          areaId);

    if (pAsExtAddrRng == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG Creation Failure\n");
        return SNMP_FAILURE;
    }

    if (pAsExtAddrRng->u1AggTranslation ==
        (UINT1) i4SetValFutOspfAsExternalAggregationTranslation)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId, " AS Ext AGG - No Change \n");
        return SNMP_SUCCESS;
    }

    if (pAsExtAddrRng->rangeStatus == ACTIVE)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                  pOspfCxt->u4OspfCxtId,
                  " AS Ext AGG Active - Make not in Service\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfAsExternalAggregationTranslation;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfAsExternalAggregationTranslation) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    pAsExtAddrRng->u1AggTranslation =
        (UINT1) i4SetValFutOspfAsExternalAggregationTranslation;

    pAsExtAddrRng->u1AttrMask = pAsExtAddrRng->u1AttrMask | RAG_TRANS_MASK;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfAsExternalAggregationTranslation\n");
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfAsExternalAggregationNet,
                      u4FutOspfAsExternalAggregationMask,
                      u4FutOspfAsExternalAggregationAreaId,
                      i4SetValFutOspfAsExternalAggregationTranslation));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfAsExternalAggregationStatus
 Input       :  The Indices
                FutOspfAsExternalAggregationNet
                FutOspfAsExternalAggregationMask
                FutOspfAsExternalAggregationAreaId

                The Object 
                setValFutOspfAsExternalAggregationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfAsExternalAggregationStatus (UINT4
                                          u4FutOspfAsExternalAggregationNet,
                                          UINT4
                                          u4FutOspfAsExternalAggregationMask,
                                          UINT4
                                          u4FutOspfAsExternalAggregationAreaId,
                                          INT4
                                          i4SetValFutOspfAsExternalAggregationStatus)
{

    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tAreaId             areaId;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tAsExtAddrRange    *pAsExtAddrRng;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfAsExternalAggregationStatus;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfAsExternalAggregationStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
    u4FutOspfAsExternalAggregationNet =
        (u4FutOspfAsExternalAggregationNet &
         u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (asExtNet, u4FutOspfAsExternalAggregationNet);
    OSPF_CRU_BMC_DWTOPDU (asExtMask, u4FutOspfAsExternalAggregationMask);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfAsExternalAggregationAreaId);

    switch ((UINT1) i4SetValFutOspfAsExternalAggregationStatus)
    {

        case CREATE_AND_GO:

            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                                  areaId);
            if (pAsExtAddrRng != NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          " AS Ext AGG already existing\n");
                return SNMP_FAILURE;
            }

            OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagAsExtAddDefaultValues\n");

            pAsExtAddrRng =
                RagAsExtAddDefaultValues (u4FutOspfAsExternalAggregationNet,
                                          u4FutOspfAsExternalAggregationMask,
                                          u4FutOspfAsExternalAggregationAreaId);

            OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Existing FUNC: RagAsExtAddDefaultValues\n");

            if (pAsExtAddrRng == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          " AS Ext AGG Creation Failure\n");
                return SNMP_FAILURE;
            }

            pAsExtAddrRng->rangeStatus = ACTIVE;
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                AS_EXT_AGG_FREE (pAsExtAddrRng);
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = OSPF_AS_EXT_AGG;
            pOspfIfParam->param.sig_param.u1Type = CREATE_AND_GO;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pAsExtAddrRng;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
                AS_EXT_AGG_FREE (pAsExtAddrRng);
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfAsExternalAggregationNet,
                                  u4FutOspfAsExternalAggregationMask,
                                  u4FutOspfAsExternalAggregationAreaId,
                                  i4SetValFutOspfAsExternalAggregationStatus));
#endif
                return SNMP_FAILURE;
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfAsExternalAggregationNet,
                              u4FutOspfAsExternalAggregationMask,
                              u4FutOspfAsExternalAggregationAreaId,
                              i4SetValFutOspfAsExternalAggregationStatus));
#endif
            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
            KW_FALSEPOSITIVE_FIX (pAsExtAddrRng);
            return SNMP_SUCCESS;

        case CREATE_AND_WAIT:

            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet,
                                                  asExtMask, areaId);
            if (pAsExtAddrRng != NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId,
                          " AS Ext AGG already existing\n");
                return SNMP_FAILURE;
            }

            OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagAsExtAddDefaultValues\n");

            pAsExtAddrRng =
                RagAsExtAddDefaultValues (u4FutOspfAsExternalAggregationNet,
                                          u4FutOspfAsExternalAggregationMask,
                                          u4FutOspfAsExternalAggregationAreaId);

            OSPF_TRC (OSPF_CONFIGURATION_TRC, pOspfCxt->u4OspfCxtId,
                      "Existing FUNC: RagAsExtAddDefaultValues\n");

            if (pAsExtAddrRng == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          " AS Ext AGG Creation Failure\n");
                return SNMP_FAILURE;
            }

            pAsExtAddrRng->rangeStatus = NOT_READY;
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                AS_EXT_AGG_FREE (pAsExtAddrRng);
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = OSPF_AS_EXT_AGG;
            pOspfIfParam->param.sig_param.u1Type = CREATE_AND_WAIT;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pAsExtAddrRng;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
                AS_EXT_AGG_FREE (pAsExtAddrRng);
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfAsExternalAggregationNet,
                                  u4FutOspfAsExternalAggregationMask,
                                  u4FutOspfAsExternalAggregationAreaId,
                                  i4SetValFutOspfAsExternalAggregationStatus));
#endif
                return SNMP_FAILURE;
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfAsExternalAggregationNet,
                              u4FutOspfAsExternalAggregationMask,
                              u4FutOspfAsExternalAggregationAreaId,
                              i4SetValFutOspfAsExternalAggregationStatus));
#endif
            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
            KW_FALSEPOSITIVE_FIX (pAsExtAddrRng);
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:
            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet,
                                                  asExtMask, areaId);
            if (pAsExtAddrRng == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, " AS Ext AGG not existing\n");
                return SNMP_FAILURE;
            }

            if (pAsExtAddrRng->rangeStatus == NOT_IN_SERVICE)
            {
                OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                          "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
                return SNMP_SUCCESS;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pAsExtAddrRng->rangeStatus = NOT_IN_SERVICE;
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfAsExternalAggregationNet,
                              u4FutOspfAsExternalAggregationMask,
                              u4FutOspfAsExternalAggregationAreaId,
                              i4SetValFutOspfAsExternalAggregationStatus));
#endif
            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
            return SNMP_SUCCESS;

        case ACTIVE:

            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet,
                                                  asExtMask, areaId);
            if (pAsExtAddrRng == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, " AS Ext AGG not existing\n");
                return SNMP_FAILURE;
            }

            if (pAsExtAddrRng->rangeStatus == ACTIVE)
            {
                OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                          "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
                return SNMP_SUCCESS;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }

            if (pAsExtAddrRng->rangeStatus == NOT_IN_SERVICE)
            {
                pOspfIfParam->u1OpCode = OSPF_AS_EXT_AGG;
                pOspfIfParam->param.sig_param.u1Type = NOT_IN_SERVICE;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pAsExtAddrRng;
            }
            else
            {
                pOspfIfParam->u1OpCode = OSPF_AS_EXT_AGG;
                pOspfIfParam->param.sig_param.u1Type = ACTIVE;
                pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pAsExtAddrRng;
            }
            pAsExtAddrRng->rangeStatus = ACTIVE;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfAsExternalAggregationNet,
                                  u4FutOspfAsExternalAggregationMask,
                                  u4FutOspfAsExternalAggregationAreaId,
                                  i4SetValFutOspfAsExternalAggregationStatus));
#endif
                return SNMP_FAILURE;
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfAsExternalAggregationNet,
                              u4FutOspfAsExternalAggregationMask,
                              u4FutOspfAsExternalAggregationAreaId,
                              i4SetValFutOspfAsExternalAggregationStatus));
#endif
            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
            return SNMP_SUCCESS;

        case DESTROY:
            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet,
                                                  asExtMask, areaId);
            if (pAsExtAddrRng == NULL)
            {
                return SNMP_SUCCESS;
            }
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = OSPF_AS_EXT_AGG;
            pOspfIfParam->param.sig_param.u1Type = DESTROY;
            pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pAsExtAddrRng;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfAsExternalAggregationNet,
                                  u4FutOspfAsExternalAggregationMask,
                                  u4FutOspfAsExternalAggregationAreaId,
                                  i4SetValFutOspfAsExternalAggregationStatus));
#endif
                return SNMP_FAILURE;
            }
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p %i",
                              gOsRtr.u4OspfCxtId,
                              u4FutOspfAsExternalAggregationNet,
                              u4FutOspfAsExternalAggregationMask,
                              u4FutOspfAsExternalAggregationAreaId,
                              i4SetValFutOspfAsExternalAggregationStatus));
#endif
            OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
                      "FUNC : nmhSetFutOspfAsExternalAggregationStatus\n");
            return SNMP_SUCCESS;

        default:
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                      pOspfCxt->u4OspfCxtId, "Invalid Row Status\n");
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfSetTrap
 Input       :  The Indices

                The Object 
                setValOspfSetTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfSetTrap (tSNMP_OCTET_STRING_TYPE * pSetValOspfSetTrap)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4TempVal = 0;
    UINT4               u4Val;
    UINT4               u4SeqNum = 0;
    UINT1               u1Index;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfSetTrap;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfSetTrap) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    for (u1Index = 0; u1Index < pSetValOspfSetTrap->i4_Length; u1Index++)
    {
        u4Val = pSetValOspfSetTrap->pu1_OctetList[u1Index] << 8 * (3 - u1Index);
        pOspfCxt->u4TrapControl = u4Val | u4TempVal;
        u4TempVal = pOspfCxt->u4TrapControl;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                      gOsRtr.u4OspfCxtId, pSetValOspfSetTrap));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRFC1583Compatibility
 Input       :  The Indices

                The Object
                setValOspfRFC1583Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRFC1583Compatibility (INT4 i4SetValFutOspfRFC1583Compatibility)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->rfc1583Compatibility !=
        (UINT4) i4SetValFutOspfRFC1583Compatibility)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfRFC1583Compatibility;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfRFC1583Compatibility) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        pOspfCxt->rfc1583Compatibility = i4SetValFutOspfRFC1583Compatibility;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfRFC1583Compatibility));
#endif
        if (pOspfCxt->u1RtrNetworkLsaChanged != OSPF_TRUE)
        {
            pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
            RtcSetRtTimer (pOspfCxt);
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfIfMD5AuthKey
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                setValFutOspfIfMD5AuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfMD5AuthKey (UINT4 u4FutOspfIfMD5AuthIpAddress,
                           INT4 i4FutOspfIfMD5AuthAddressLessIf,
                           INT4 i4FutOspfIfMD5AuthKeyId,
                           tSNMP_OCTET_STRING_TYPE * pSetValFutOspfIfMD5AuthKey)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfMD5AuthKey;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfIfMD5AuthKey) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        /* The entry already exists, The Authentication key is changed */
        AuthKeyCopy (pAuthkeyInfo->authKey,
                     pSetValFutOspfIfMD5AuthKey->pu1_OctetList,
                     pSetValFutOspfIfMD5AuthKey->i4_Length);
        /*The length is used only in case of SHA-1 and other SHA authentication */

        pAuthkeyInfo->u1KeyLen = pSetValFutOspfIfMD5AuthKey->i4_Length;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %s", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId, pSetValFutOspfIfMD5AuthKey));
#endif
        return SNMP_SUCCESS;
    }

    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) i4FutOspfIfMD5AuthAddressLessIf))
        == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "If find Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %s", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId, pSetValFutOspfIfMD5AuthKey));
#endif
        return SNMP_FAILURE;
    }
    /*    A new entry needs to be created , allocate memory , assign def vals */
    if ((pAuthkeyInfo = Md5AuthKeyInfoCreate ()) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "MD5 AuthKeyInfo Allocation Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %s", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId, pSetValFutOspfIfMD5AuthKey));
#endif
        return SNMP_FAILURE;
    }
    pAuthkeyInfo->u1AuthkeyId = (UINT1) i4FutOspfIfMD5AuthKeyId;
    AuthKeyCopy (pAuthkeyInfo->authKey,
                 pSetValFutOspfIfMD5AuthKey->pu1_OctetList,
                 pSetValFutOspfIfMD5AuthKey->i4_Length);

    SortInsertMd5AuthKeyInfo (&(pInterface->sortMd5authkeyLst), pAuthkeyInfo);
    /*The length is used only in case of SHA-1 and other SHA authentication */
    pAuthkeyInfo->u1KeyLen = pSetValFutOspfIfMD5AuthKey->i4_Length;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %s", gOsRtr.u4OspfCxtId,
                      u4FutOspfIfMD5AuthIpAddress,
                      i4FutOspfIfMD5AuthAddressLessIf, i4FutOspfIfMD5AuthKeyId,
                      pSetValFutOspfIfMD5AuthKey));
#endif
    KW_FALSEPOSITIVE_FIX (pAuthkeyInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfAuthKey
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                setValFutOspfIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfAuthKey (UINT4 u4FutOspfIfAuthIpAddress,
                        INT4 i4FutOspfIfAuthAddressLessIf,
                        INT4 i4FutOspfIfAuthKeyId,
                        tSNMP_OCTET_STRING_TYPE * pSetValFutOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhSetFutOspfIfMD5AuthKey (u4FutOspfIfAuthIpAddress,
                                          i4FutOspfIfAuthAddressLessIf,
                                          i4FutOspfIfAuthKeyId,
                                          pSetValFutOspfIfAuthKey);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFutOspfIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                setValFutOspfIfMD5AuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfMD5AuthKeyStartAccept (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                      INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                      INT4 i4FutOspfIfMD5AuthKeyId,
                                      INT4
                                      i4SetValFutOspfIfMD5AuthKeyStartAccept)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfMD5AuthKeyStartAccept;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfIfMD5AuthKeyStartAccept) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        if (MsrGetRestorationStatus () == ISS_TRUE)
        {
            pAuthkeyInfo->u4KeyStartAccept =
                (UINT4) i4SetValFutOspfIfMD5AuthKeyStartAccept;
        }
        else
        {
            pAuthkeyInfo->u4KeyStartAccept =
                ((UINT4) i4SetValFutOspfIfMD5AuthKeyStartAccept);
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId,
                          i4SetValFutOspfIfMD5AuthKeyStartAccept));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                setValFutOspfIfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfAuthKeyStartAccept (UINT4 u4FutOspfIfAuthIpAddress,
                                   INT4 i4FutOspfIfAuthAddressLessIf,
                                   INT4 i4FutOspfIfAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFutOspfIfAuthKeyStartAccept)
{
    INT4                i4AuthKeyStartAccept = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    OspfConvertTimeForSnmp (pSetValFutOspfIfAuthKeyStartAccept->pu1_OctetList,
                            &tm);
    i4AuthKeyStartAccept = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStartAccept (u4FutOspfIfAuthIpAddress,
                                              i4FutOspfIfAuthAddressLessIf,
                                              i4FutOspfIfAuthKeyId,
                                              i4AuthKeyStartAccept);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                setValFutOspfIfMD5AuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfMD5AuthKeyStartGenerate (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                        INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                        INT4 i4FutOspfIfMD5AuthKeyId,
                                        INT4
                                        i4SetValFutOspfIfMD5AuthKeyStartGenerate)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfMD5AuthKeyStartGenerate;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfIfMD5AuthKeyStartGenerate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        if (MsrGetRestorationStatus () == ISS_TRUE)
        {
            pAuthkeyInfo->u4KeyStartGenerate =
                (UINT4) i4SetValFutOspfIfMD5AuthKeyStartGenerate;
        }
        else
        {
            pAuthkeyInfo->u4KeyStartGenerate =
                ((UINT4) i4SetValFutOspfIfMD5AuthKeyStartGenerate);
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId,
                          i4SetValFutOspfIfMD5AuthKeyStartGenerate));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                *pSetValFutOspfIfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfAuthKeyStartGenerate (UINT4 u4FutOspfIfAuthIpAddress,
                                     INT4 i4FutOspfIfAuthAddressLessIf,
                                     INT4 i4FutOspfIfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFutOspfIfAuthKeyStartGenerate)
{
    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfIfAuthKeyStartGenerate->pu1_OctetList,
                            &tm);
    i4AuthKeyStartGenerate = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStartGenerate (u4FutOspfIfAuthIpAddress,
                                                i4FutOspfIfAuthAddressLessIf,
                                                i4FutOspfIfAuthKeyId,
                                                i4AuthKeyStartGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                setValFutOspfIfMD5AuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfMD5AuthKeyStopGenerate (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                       INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                       INT4 i4FutOspfIfMD5AuthKeyId,
                                       INT4
                                       i4SetValFutOspfIfMD5AuthKeyStopGenerate)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfMD5AuthKeyStopGenerate;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfIfMD5AuthKeyStopGenerate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        if ((i4SetValFutOspfIfMD5AuthKeyStopGenerate == 0) ||
            (MsrGetRestorationStatus () == ISS_TRUE))
        {
            pAuthkeyInfo->u4KeyStopGenerate =
                (UINT4) i4SetValFutOspfIfMD5AuthKeyStopGenerate;
        }
        else
        {
            pAuthkeyInfo->u4KeyStopGenerate =
                ((UINT4) i4SetValFutOspfIfMD5AuthKeyStopGenerate);
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId,
                          i4SetValFutOspfIfMD5AuthKeyStopGenerate));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                setValFutOspfIfMD5AuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfMD5AuthKeyStopAccept (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                     INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                     INT4 i4FutOspfIfMD5AuthKeyId,
                                     INT4 i4SetValFutOspfIfMD5AuthKeyStopAccept)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) !=
        NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfMD5AuthKeyStopAccept;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfIfMD5AuthKeyStopAccept) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        if ((i4SetValFutOspfIfMD5AuthKeyStopAccept == 0) ||
            (MsrGetRestorationStatus () == ISS_TRUE))
        {
            pAuthkeyInfo->u4KeyStopAccept =
                (UINT4) i4SetValFutOspfIfMD5AuthKeyStopAccept;
        }
        else
        {
            pAuthkeyInfo->u4KeyStopAccept =
                ((UINT4) i4SetValFutOspfIfMD5AuthKeyStopAccept);
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfIfMD5AuthIpAddress,
                          i4FutOspfIfMD5AuthAddressLessIf,
                          i4FutOspfIfMD5AuthKeyId,
                          i4SetValFutOspfIfMD5AuthKeyStopAccept));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfMD5AuthKeyStatus
 Input       :  The Indices
                FutOspfIfMD5AuthIpAddress
                FutOspfIfMD5AuthAddressLessIf
                FutOspfIfMD5AuthKeyId

                The Object 
                setValFutOspfIfMD5AuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfMD5AuthKeyStatus (UINT4 u4FutOspfIfMD5AuthIpAddress,
                                 INT4 i4FutOspfIfMD5AuthAddressLessIf,
                                 INT4 i4FutOspfIfMD5AuthKeyId,
                                 INT4 i4SetValFutOspfIfMD5AuthKeyStatus)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfIfMD5AuthIpAddress);
    if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                   ifIpAddr,
                                                   (UINT4)
                                                   i4FutOspfIfMD5AuthAddressLessIf,
                                                   (UINT1)
                                                   i4FutOspfIfMD5AuthKeyId)) ==
        NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Auth Key Find Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfIfMD5AuthKeyStatus;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfIfMD5AuthKeyStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    switch (i4SetValFutOspfIfMD5AuthKeyStatus)
    {

        case AUTHKEY_STATUS_VALID:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pAuthkeyInfo->u1AuthkeyStatus =
                (UINT1) i4SetValFutOspfIfMD5AuthKeyStatus;
            break;
        case AUTHKEY_STATUS_DELETE:
            if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                              (UINT4)
                                              i4FutOspfIfMD5AuthAddressLessIf))
                == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, "Interface Not Found\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (pInterface->pLastAuthkey == pAuthkeyInfo)
                pInterface->pLastAuthkey = NULL;
            TMO_SLL_Delete (&(pInterface->sortMd5authkeyLst),
                            &(pAuthkeyInfo->nextSortKey));
            MD5AUTH_FREE (pAuthkeyInfo);
            break;
        default:
            /*   It will never happen   */
            return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfIfMD5AuthIpAddress,
                      i4FutOspfIfMD5AuthAddressLessIf, i4FutOspfIfMD5AuthKeyId,
                      i4SetValFutOspfIfMD5AuthKeyStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                setValFutOspfIfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfAuthKeyStopGenerate (UINT4 u4FutOspfIfAuthIpAddress,
                                    INT4 i4FutOspfIfAuthAddressLessIf,
                                    INT4 i4FutOspfIfAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFutOspfIfAuthKeyStopGenerate)
{
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfIfAuthKeyStopGenerate->pu1_OctetList,
                            &tm);
    i4AuthKeyStopGenerate = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStopGenerate (u4FutOspfIfAuthIpAddress,
                                               i4FutOspfIfAuthAddressLessIf,
                                               i4FutOspfIfAuthKeyId,
                                               i4AuthKeyStopGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                setValFutOspfIfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfAuthKeyStopAccept (UINT4 u4FutOspfIfAuthIpAddress,
                                  INT4 i4FutOspfIfAuthAddressLessIf,
                                  INT4 i4FutOspfIfAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFutOspfIfAuthKeyStopAccept)
{
    INT4                i4AuthKeyStopAccept = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfIfAuthKeyStopAccept->pu1_OctetList,
                            &tm);
    i4AuthKeyStopAccept = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStopAccept (u4FutOspfIfAuthIpAddress,
                                             i4FutOspfIfAuthAddressLessIf,
                                             i4FutOspfIfAuthKeyId,
                                             i4AuthKeyStopAccept);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfIfAuthKeyStatus
 Input       :  The Indices
                FutOspfIfAuthIpAddress
                FutOspfIfAuthAddressLessIf
                FutOspfIfAuthKeyId

                The Object 
                setValFutOspfIfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfIfAuthKeyStatus (UINT4 u4FutOspfIfAuthIpAddress,
                              INT4 i4FutOspfIfAuthAddressLessIf,
                              INT4 i4FutOspfIfAuthKeyId,
                              INT4 i4SetValFutOspfIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhSetFutOspfIfMD5AuthKeyStatus (u4FutOspfIfAuthIpAddress,
                                                i4FutOspfIfAuthAddressLessIf,
                                                i4FutOspfIfAuthKeyId,
                                                i4SetValFutOspfIfAuthKeyStatus);

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfMD5AuthKey
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                setValFutOspfVirtIfMD5AuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfMD5AuthKey (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                               UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                               INT4 i4FutOspfVirtIfMD5AuthKeyId,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFutOspfVirtIfMD5AuthKey)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tMd5AuthkeyInfo    *pCurrentAuthkeyInfo;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pSetValFutOspfVirtIfMD5AuthKey == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfVirtIfMD5AuthKey;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfVirtIfMD5AuthKey) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pAuthkeyInfo = GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                       &nbrId, (UINT1)
                                                       i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        /* The entry already exists, The Authentication key is changed */
        AuthKeyCopy (pAuthkeyInfo->authKey,
                     pSetValFutOspfVirtIfMD5AuthKey->pu1_OctetList,
                     pSetValFutOspfVirtIfMD5AuthKey->i4_Length);

        pAuthkeyInfo->u1KeyLen = pSetValFutOspfVirtIfMD5AuthKey->i4_Length;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          pSetValFutOspfVirtIfMD5AuthKey));
#endif
        if ((pInterface =
             GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
        {
            /*If the current key in use is changed, make the virtual interface down and up if it is already up */
            pCurrentAuthkeyInfo = GetAuthkeyTouse (pInterface);

            if ((pCurrentAuthkeyInfo == pAuthkeyInfo)
                && (pInterface->u1IsmState != IFS_DOWN))
            {
                OspfIfDown (pInterface);
                OspfIfUp (pInterface);

            }
        }
        return SNMP_SUCCESS;
    }

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId, "Virtual If Find Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          pSetValFutOspfVirtIfMD5AuthKey));
#endif
        return SNMP_FAILURE;
    }
    /*    A new entry needs to be created , allocate memory , assign def vals */
    if ((pAuthkeyInfo = Md5AuthKeyInfoCreate ()) == NULL)
    {
        /* something seriously wrong, Atleast inform      
           the guy who is looking at console */

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "MD5 AuthKeyInfo Allocation Failure\n");
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          pSetValFutOspfVirtIfMD5AuthKey));
#endif
        return SNMP_FAILURE;
    }
    pAuthkeyInfo->u1AuthkeyId = (UINT1) i4FutOspfVirtIfMD5AuthKeyId;
    pAuthkeyInfo->u1KeyLen = pSetValFutOspfVirtIfMD5AuthKey->i4_Length;
    AuthKeyCopy (pAuthkeyInfo->authKey,
                 pSetValFutOspfVirtIfMD5AuthKey->pu1_OctetList,
                 pSetValFutOspfVirtIfMD5AuthKey->i4_Length);
    SortInsertMd5AuthKeyInfo ((tTMO_SLL *) & (pInterface->sortMd5authkeyLst),
                              pAuthkeyInfo);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %s", gOsRtr.u4OspfCxtId,
                      u4FutOspfVirtIfMD5AuthAreaId,
                      u4FutOspfVirtIfMD5AuthNeighbor,
                      i4FutOspfVirtIfMD5AuthKeyId,
                      pSetValFutOspfVirtIfMD5AuthKey));
#endif
    KW_FALSEPOSITIVE_FIX (pAuthkeyInfo);

    /*If the current key in use is changed, make the virtual interface down and up if it is already up */
    pCurrentAuthkeyInfo = GetAuthkeyTouse (pInterface);

    if ((pCurrentAuthkeyInfo == pAuthkeyInfo)
        && (pInterface->u1IsmState != IFS_DOWN))
    {
        OspfIfDown (pInterface);
        OspfIfUp (pInterface);

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfAuthKey
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                setValFutOspfVirtIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfAuthKey (UINT4 u4FutOspfVirtIfAuthAreaId,
                            UINT4 u4FutOspfVirtIfAuthNeighbor,
                            INT4 i4FutOspfVirtIfAuthKeyId,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFutOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhSetFutOspfVirtIfMD5AuthKey (u4FutOspfVirtIfAuthAreaId,
                                              u4FutOspfVirtIfAuthNeighbor,
                                              i4FutOspfVirtIfAuthKeyId,
                                              pSetValFutOspfVirtIfAuthKey);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                setValFutOspfVirtIfMD5AuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfMD5AuthKeyStartAccept (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                          UINT4
                                          u4FutOspfVirtIfMD5AuthNeighbor,
                                          INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                          INT4
                                          i4SetValFutOspfVirtIfMD5AuthKeyStartAccept)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo = GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                                          &(areaId),
                                                          &(nbrId),
                                                          (UINT1)
                                                          i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfVirtIfMD5AuthKeyStartAccept;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfVirtIfMD5AuthKeyStartAccept) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        pMd5AuthkeyInfo->u4KeyStartAccept =
            ((UINT4) i4SetValFutOspfVirtIfMD5AuthKeyStartAccept);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          i4SetValFutOspfVirtIfMD5AuthKeyStartAccept));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                setValFutOspfVirtIfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfAuthKeyStartAccept (UINT4 u4FutOspfVirtIfAuthAreaId,
                                       UINT4
                                       u4FutOspfVirtIfAuthNeighbor,
                                       INT4 i4FutOspfVirtIfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFutOspfVirtIfAuthKeyStartAccept)
{
    INT4                i4AuthKeyStartAccept = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfVirtIfAuthKeyStartAccept->
                            pu1_OctetList, &tm);
    i4AuthKeyStartAccept = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStartAccept (u4FutOspfVirtIfAuthAreaId,
                                                  u4FutOspfVirtIfAuthNeighbor,
                                                  i4FutOspfVirtIfAuthKeyId,
                                                  i4AuthKeyStartAccept);

    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                setValFutOspfVirtIfMD5AuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfMD5AuthKeyStartGenerate (UINT4
                                            u4FutOspfVirtIfMD5AuthAreaId,
                                            UINT4
                                            u4FutOspfVirtIfMD5AuthNeighbor,
                                            INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                            INT4
                                            i4SetValFutOspfVirtIfMD5AuthKeyStartGenerate)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo = GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                                          &areaId,
                                                          &(nbrId),
                                                          (UINT1)
                                                          i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfVirtIfMD5AuthKeyStartGenerate;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfVirtIfMD5AuthKeyStartGenerate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        pMd5AuthkeyInfo->u4KeyStartGenerate =
            ((UINT4) i4SetValFutOspfVirtIfMD5AuthKeyStartGenerate);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          i4SetValFutOspfVirtIfMD5AuthKeyStartGenerate));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                setValFutOspfVirtIfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfAuthKeyStartGenerate (UINT4
                                         u4FutOspfVirtIfAuthAreaId,
                                         UINT4
                                         u4FutOspfVirtIfAuthNeighbor,
                                         INT4 i4FutOspfVirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValFutOspfVirtIfAuthKeyStartGenerate)
{
    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfVirtIfAuthKeyStartGenerate->
                            pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) GrGetSecondsSinceBase (tm);

    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStartGenerate (u4FutOspfVirtIfAuthAreaId,
                                                    u4FutOspfVirtIfAuthNeighbor,
                                                    i4FutOspfVirtIfAuthKeyId,
                                                    i4AuthKeyStartGenerate);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                setValFutOspfVirtIfMD5AuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfMD5AuthKeyStopGenerate (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           u4FutOspfVirtIfMD5AuthNeighbor,
                                           INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                           INT4
                                           i4SetValFutOspfVirtIfMD5AuthKeyStopGenerate)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo = GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                                          &areaId,
                                                          &(nbrId),
                                                          (UINT1)
                                                          i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfVirtIfMD5AuthKeyStopGenerate;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfVirtIfMD5AuthKeyStopGenerate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        if (i4SetValFutOspfVirtIfMD5AuthKeyStopGenerate != 0)
        {
            pMd5AuthkeyInfo->u4KeyStopGenerate =
                ((UINT4) i4SetValFutOspfVirtIfMD5AuthKeyStopGenerate);
        }
        else
        {
            pMd5AuthkeyInfo->u4KeyStopGenerate =
                (UINT4) i4SetValFutOspfVirtIfMD5AuthKeyStopGenerate;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          i4SetValFutOspfVirtIfMD5AuthKeyStopGenerate));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                setValFutOspfVirtIfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfAuthKeyStopGenerate (UINT4 u4FutOspfVirtIfAuthAreaId,
                                        UINT4
                                        u4FutOspfVirtIfAuthNeighbor,
                                        INT4 i4FutOspfVirtIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFutOspfVirtIfAuthKeyStopGenerate)
{
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfVirtIfAuthKeyStopGenerate->
                            pu1_OctetList, &tm);
    i4AuthKeyStopGenerate = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStopGenerate (u4FutOspfVirtIfAuthAreaId,
                                                   u4FutOspfVirtIfAuthNeighbor,
                                                   i4FutOspfVirtIfAuthKeyId,
                                                   i4AuthKeyStopGenerate);

    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                setValFutOspfVirtIfMD5AuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfMD5AuthKeyStopAccept (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                         UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                                         INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                         INT4
                                         i4SetValFutOspfVirtIfMD5AuthKeyStopAccept)
{
    tAreaId             areaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pMd5AuthkeyInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pMd5AuthkeyInfo = GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                                          &areaId,
                                                          &(nbrId),
                                                          (UINT1)
                                                          i4FutOspfVirtIfMD5AuthKeyId))
        != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfVirtIfMD5AuthKeyStopAccept;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfVirtIfMD5AuthKeyStopAccept) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 4;
#endif
        if (i4SetValFutOspfVirtIfMD5AuthKeyStopAccept != 0)
        {
            pMd5AuthkeyInfo->u4KeyStopAccept =
                ((UINT4) i4SetValFutOspfVirtIfMD5AuthKeyStopAccept);
        }
        else
        {
            pMd5AuthkeyInfo->u4KeyStopAccept =
                (UINT4) i4SetValFutOspfVirtIfMD5AuthKeyStopAccept;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                          u4FutOspfVirtIfMD5AuthAreaId,
                          u4FutOspfVirtIfMD5AuthNeighbor,
                          i4FutOspfVirtIfMD5AuthKeyId,
                          i4SetValFutOspfVirtIfMD5AuthKeyStopAccept));
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                setValFutOspfVirtIfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfAuthKeyStopAccept (UINT4 u4FutOspfVirtIfAuthAreaId,
                                      UINT4 u4FutOspfVirtIfAuthNeighbor,
                                      INT4 i4FutOspfVirtIfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFutOspfVirtIfAuthKeyStopAccept)
{
    INT4                i4AuthKeyStopAccept = 0;
    tUtlTm              tm;
    INT1                i1Return = SNMP_FAILURE;
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    OspfConvertTimeForSnmp (pSetValFutOspfVirtIfAuthKeyStopAccept->
                            pu1_OctetList, &tm);
    i4AuthKeyStopAccept = (INT4) GrGetSecondsSinceBase (tm);
    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStopAccept (u4FutOspfVirtIfAuthAreaId,
                                                 u4FutOspfVirtIfAuthNeighbor,
                                                 i4FutOspfVirtIfAuthKeyId,
                                                 i4AuthKeyStopAccept);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfMD5AuthKeyStatus
 Input       :  The Indices
                FutOspfVirtIfMD5AuthAreaId
                FutOspfVirtIfMD5AuthNeighbor
                FutOspfVirtIfMD5AuthKeyId

                The Object 
                setValFutOspfVirtIfMD5AuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfMD5AuthKeyStatus (UINT4 u4FutOspfVirtIfMD5AuthAreaId,
                                     UINT4 u4FutOspfVirtIfMD5AuthNeighbor,
                                     INT4 i4FutOspfVirtIfMD5AuthKeyId,
                                     INT4 i4SetValFutOspfVirtIfMD5AuthKeyStatus)
{
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4FutOspfVirtIfMD5AuthAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4FutOspfVirtIfMD5AuthNeighbor);
    if ((pAuthkeyInfo = GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                                       &transitAreaId,
                                                       &(nbrId),
                                                       (UINT1)
                                                       i4FutOspfVirtIfMD5AuthKeyId))
        == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC | OSPF_CRITICAL_TRC,
                  pOspfCxt->u4OspfCxtId,
                  "Virtual If Auth Key Info Find Failure\n");
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfVirtIfMD5AuthKeyStatus;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfVirtIfMD5AuthKeyStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
#endif
    switch (i4SetValFutOspfVirtIfMD5AuthKeyStatus)
    {

        case AUTHKEY_STATUS_VALID:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pAuthkeyInfo->u1AuthkeyStatus =
                (UINT1) i4SetValFutOspfVirtIfMD5AuthKeyStatus;
            break;
        case AUTHKEY_STATUS_DELETE:
            pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId,
                                             &(nbrId));
            if (pInterface == NULL)
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC,
                          pOspfCxt->u4OspfCxtId, "Interface Not Found \n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (pInterface->pLastAuthkey == pAuthkeyInfo)
                pInterface->pLastAuthkey = NULL;
            TMO_SLL_Delete (&(pInterface->sortMd5authkeyLst),
                            &(pAuthkeyInfo->nextSortKey));
            MD5AUTH_FREE (pAuthkeyInfo);
            break;
        default:
            /*   It will never happen   */
            return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfVirtIfMD5AuthAreaId,
                      u4FutOspfVirtIfMD5AuthNeighbor,
                      i4FutOspfVirtIfMD5AuthKeyId,
                      i4SetValFutOspfVirtIfMD5AuthKeyStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfAuthKeyStatus
 Input       :  The Indices
                FutOspfVirtIfAuthAreaId
                FutOspfVirtIfAuthNeighbor
                FutOspfVirtIfAuthKeyId

                The Object 
                setValFutOspfVirtIfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfAuthKeyStatus (UINT4 u4FutOspfVirtIfAuthAreaId,
                                  UINT4 u4FutOspfVirtIfAuthNeighbor,
                                  INT4 i4FutOspfVirtIfAuthKeyId,
                                  INT4 i4SetValFutOspfVirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    i1Return = nmhSetFutOspfVirtIfMD5AuthKeyStatus (u4FutOspfVirtIfAuthAreaId,
                                                    u4FutOspfVirtIfAuthNeighbor,
                                                    i4FutOspfVirtIfAuthKeyId,
                                                    i4SetValFutOspfVirtIfAuthKeyStatus);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDRouteMetric
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                setValFutOspfRRDRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDRouteMetric (UINT4 u4FutOspfRRDRouteDest,
                             UINT4 u4FutOspfRRDRouteMask,
                             INT4 i4SetValFutOspfRRDRouteMetric)
{
    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDRouteMetric;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDRouteMetric) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        pRrdConfRtInfo->u4RrdRouteMetricValue = i4SetValFutOspfRRDRouteMetric;
        i1RetVal = SNMP_SUCCESS;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                      i4SetValFutOspfRRDRouteMetric));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDRouteMetricType
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                setValFutOspfRRDRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDRouteMetricType (UINT4 u4FutOspfRRDRouteDest,
                                 UINT4 u4FutOspfRRDRouteMask,
                                 INT4 i4SetValFutOspfRRDRouteMetricType)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDRouteMetricType;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfRRDRouteMetricType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        pRrdConfRtInfo->u1RrdRouteMetricType = (UINT1)
            i4SetValFutOspfRRDRouteMetricType;
        i1RetVal = SNMP_SUCCESS;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                      i4SetValFutOspfRRDRouteMetricType));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDRouteTagType
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                setValFutOspfRRDRouteTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDRouteTagType (UINT4 u4FutOspfRRDRouteDest,
                              UINT4 u4FutOspfRRDRouteMask,
                              INT4 i4SetValFutOspfRRDRouteTagType)
{
    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDRouteTagType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDRouteTagType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        pRrdConfRtInfo->u1RedistrTagType =
            (UINT1) i4SetValFutOspfRRDRouteTagType;
        i1RetVal = SNMP_SUCCESS;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                      i4SetValFutOspfRRDRouteTagType));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDRouteTag
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask
 
                The Object 
                setValFutOspfRRDRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDRouteTag (UINT4 u4FutOspfRRDRouteDest,
                          UINT4 u4FutOspfRRDRouteMask,
                          UINT4 u4SetValFutOspfRRDRouteTag)
{

    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDRouteTag;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDRouteTag) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);

    if ((pRrdConfRtInfo =
         GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                    &RrdDestAddrMask)) != NULL)
    {
        pRrdConfRtInfo->u4ManualTagValue = u4SetValFutOspfRRDRouteTag;
        i1RetVal = SNMP_SUCCESS;
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %p", gOsRtr.u4OspfCxtId,
                      u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                      u4SetValFutOspfRRDRouteTag));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDRouteStatus
 Input       :  The Indices
                FutOspfRRDRouteDest
                FutOspfRRDRouteMask

                The Object 
                setValFutOspfRRDRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDRouteStatus (UINT4 u4FutOspfRRDRouteDest,
                             UINT4 u4FutOspfRRDRouteMask,
                             INT4 i4SetValFutOspfRRDRouteStatus)
{
    tIPADDR             RrdDestIPAddr;
    tIPADDRMASK         RrdDestAddrMask;
    tRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (RrdDestIPAddr, u4FutOspfRRDRouteDest);
    OSPF_CRU_BMC_DWTOPDU (RrdDestAddrMask, u4FutOspfRRDRouteMask);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDRouteStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDRouteStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
#endif
    switch ((UINT1) i4SetValFutOspfRRDRouteStatus)
    {
        case CREATE_AND_GO:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((pRrdConfRtInfo =
                 RrdConfRtInfoCreateInCxt (pOspfCxt, &RrdDestIPAddr,
                                           &RrdDestAddrMask)) == NULL)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = SET_RRD_CONFIG_RECORD;
            IP_ADDR_COPY (pOspfIfParam->param.RRDConfigRecParam.
                          rrdDestIPAddr, pRrdConfRtInfo->rrdDestIPAddr);
            IP_ADDR_COPY (pOspfIfParam->param.RRDConfigRecParam.
                          rrdDestAddrMask, pRrdConfRtInfo->rrdDestAddrMask);
            pRrdConfRtInfo->rrdRtInfoStatus = ACTIVE;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case CREATE_AND_WAIT:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((pRrdConfRtInfo =
                 RrdConfRtInfoCreateInCxt (pOspfCxt, &RrdDestIPAddr,
                                           &RrdDestAddrMask)) == NULL)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            pRrdConfRtInfo->rrdRtInfoStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            if ((pRrdConfRtInfo = GetFindRrdConfRtInfoInCxt (pOspfCxt,
                                                             &RrdDestIPAddr,
                                                             &RrdDestAddrMask))
                == NULL)
            {

                return SNMP_FAILURE;
            }
            if (pRrdConfRtInfo->rrdRtInfoStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = SET_RRD_CONFIG_RECORD;
            IP_ADDR_COPY (pOspfIfParam->param.RRDConfigRecParam.
                          rrdDestIPAddr, pRrdConfRtInfo->rrdDestIPAddr);
            IP_ADDR_COPY (pOspfIfParam->param.RRDConfigRecParam.
                          rrdDestAddrMask, pRrdConfRtInfo->rrdDestAddrMask);
            pRrdConfRtInfo->rrdRtInfoStatus = ACTIVE;
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }

            break;

        case NOT_IN_SERVICE:
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if ((pRrdConfRtInfo = GetFindRrdConfRtInfoInCxt (pOspfCxt,
                                                             &RrdDestIPAddr,
                                                             &RrdDestAddrMask))
                == NULL)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            pRrdConfRtInfo->rrdRtInfoStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (GetFindRrdConfRtInfoInCxt (pOspfCxt, &RrdDestIPAddr,
                                           &RrdDestAddrMask) == NULL)
            {
                return SNMP_SUCCESS;
            }

            if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
            {
                pOspfIfParam = (tOspfSnmpIfParam *)
                    OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
            }
            else
            {
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
                return SNMP_FAILURE;
            }

            pOspfIfParam->u1OpCode = DELETE_RRD_CONFIG_RECORD;
            IP_ADDR_COPY (pOspfIfParam->param.RRDConfigRecParam.
                          rrdDestIPAddr, RrdDestIPAddr);
            IP_ADDR_COPY (pOspfIfParam->param.RRDConfigRecParam.
                          rrdDestAddrMask, RrdDestAddrMask);
            pOspfIfParam->pOspfCxt = pOspfCxt;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
            {
                OSPF_SNMP_IF_MSG_FREE (pMessage);
                OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                          OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                          "OSPF_SNMP_IF_MSG_SEND Failure\n");
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                                  gOsRtr.u4OspfCxtId,
                                  u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                                  i4SetValFutOspfRRDRouteStatus));
#endif
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }                            /* switch */
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                      u4FutOspfRRDRouteDest, u4FutOspfRRDRouteMask,
                      i4SetValFutOspfRRDRouteStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDStatus
 Input       :  The Indices

                The Object 
                setValFutOspfRRDStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDStatus (INT4 i4SetValFutOspfRRDStatus)
{

    tOspfQMsg          *pMessage;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    UINT4               u4ErrorCode = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFutOspfRRDStatus == ((INT4) pOspfCxt->redistrAdmnStatus))
    {
        return SNMP_SUCCESS;
    }
    if ((i4SetValFutOspfRRDStatus == OSPF_TRUE) &&
        (pOspfCxt->bAsBdrRtr != OSPF_TRUE))
    {
        if (nmhTestv2OspfASBdrRtrStatus (&u4ErrorCode, OSPF_TRUE) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetOspfASBdrRtrStatus (OSPF_TRUE) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        pOspfCxt->u1AsbrFlag = OSPF_TRUE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (i4SetValFutOspfRRDStatus == OSPF_ENABLED)
    {
        /* RedrEnab message is sent on setting futOspfRRDSrcProtoMask Enable */
        pOspfCxt->redistrAdmnStatus = OSPF_ENABLED;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfRRDStatus));
#endif
        return SNMP_SUCCESS;
    }

    /*
     * EnQ SNMP message for redistribution disable with SrcProtoBitMask 
     * indicating all protocols (including static & local routes)
     */

    if (pOspfCxt->u4RrdSrcProtoBitMask)
    {

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValFutOspfRRDStatus));
#endif
            return SNMP_FAILURE;
        }

        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);

        pOspfIfParam->u1OpCode = OSPF_DISABLE_SRC_PROTO;
        pOspfIfParam->param.srcProtoDisParam.u4SrcProtoDisMask =
            RRD_SRC_PROTO_BIT_MASK;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValFutOspfRRDStatus));
#endif
            return SNMP_FAILURE;
        }

    }                            /* if */

    if ((i4SetValFutOspfRRDStatus == OSPF_FALSE)
        && (pOspfCxt->u1AsbrFlag == OSPF_TRUE))
    {
        if (nmhTestv2OspfASBdrRtrStatus (&u4ErrorCode, OSPF_FALSE) ==
            SNMP_FAILURE)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "ASBR Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValFutOspfRRDStatus));
#endif
            return SNMP_FAILURE;
        }
        if (nmhSetOspfASBdrRtrStatus (OSPF_FALSE) == SNMP_FAILURE)
        {
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "ASBR Failure\n");
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                              i4SetValFutOspfRRDStatus));
#endif
            return SNMP_FAILURE;
        }
        pOspfCxt->u1AsbrFlag = OSPF_FALSE;
    }

    pOspfCxt->redistrAdmnStatus = OSPF_DISABLED;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfRRDStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDSrcProtoMaskEnable
 Input       :  The Indices

                The Object 
                setValFutOspfRRDSrcProtoMaskEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDSrcProtoMaskEnable (INT4 i4SetValFutOspfRRDSrcProtoMaskEnable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4NewSrcProtoMaskEnable;
    UINT4               u4SeqNum = 0;
    UINT4               u4ErrorCode = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4NewSrcProtoMaskEnable = 0;
    if (i4SetValFutOspfRRDSrcProtoMaskEnable ==
        (INT4) pOspfCxt->u4RrdSrcProtoBitMask)
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDSrcProtoMaskEnable;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfRRDSrcProtoMaskEnable) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    /*
     * transition in bit value from 0->1 means that redistribution from the 
     * corresponding protocol is to be enabled
     */

    if (!(pOspfCxt->u4RrdSrcProtoBitMask & LOCAL_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskEnable & LOCAL_RT_MASK))
    {
        u4NewSrcProtoMaskEnable |= LOCAL_RT_MASK;
    }

    if (!(pOspfCxt->u4RrdSrcProtoBitMask & STATIC_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskEnable & STATIC_RT_MASK))
    {
        u4NewSrcProtoMaskEnable |= STATIC_RT_MASK;
    }

    if (!(pOspfCxt->u4RrdSrcProtoBitMask & RIP_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskEnable & RIP_RT_MASK))
    {
        u4NewSrcProtoMaskEnable |= RIP_RT_MASK;
    }

    if (!(pOspfCxt->u4RrdSrcProtoBitMask & BGP_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskEnable & BGP_RT_MASK))
    {
        u4NewSrcProtoMaskEnable |= BGP_RT_MASK;
    }
    if (!(pOspfCxt->u4RrdSrcProtoBitMask & ISISL1_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskEnable & ISISL1_RT_MASK))
    {
        u4NewSrcProtoMaskEnable |= ISISL1_RT_MASK;
    }
    if (!(pOspfCxt->u4RrdSrcProtoBitMask & ISISL2_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskEnable & ISISL2_RT_MASK))
    {
        u4NewSrcProtoMaskEnable |= ISISL2_RT_MASK;
    }

    if (u4NewSrcProtoMaskEnable)
    {
        if (pOspfCxt->bAsBdrRtr != OSPF_TRUE)
        {
            if (nmhTestv2OspfASBdrRtrStatus (&u4ErrorCode, OSPF_TRUE) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (nmhSetOspfASBdrRtrStatus (OSPF_TRUE) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            pOspfCxt->u1AsbrFlag = OSPF_TRUE;
        }
        /* Update the SrcProtoBitMask */
        pOspfCxt->u4RrdSrcProtoBitMask |= u4NewSrcProtoMaskEnable;

        /* EnQ redis enable message to RTM */
        RtmTxRedistributeEnableInCxt (pOspfCxt, u4NewSrcProtoMaskEnable,
                                      pOspfCxt->au1RMapName);

    }
    if (i4SetValFutOspfRRDSrcProtoMaskEnable == 0)
    {
        if (pOspfCxt->u1AsbrFlag == OSPF_TRUE)
        {
            if (nmhTestv2OspfASBdrRtrStatus (&u4ErrorCode, OSPF_FALSE) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (nmhSetOspfASBdrRtrStatus (OSPF_FALSE) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            pOspfCxt->u1AsbrFlag = OSPF_FALSE;
        }

        RtmSrcProtoDisableInCxt (pOspfCxt, RRD_SRC_PROTO_BIT_MASK);
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                      pOspfCxt->u4RrdSrcProtoBitMask));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDSrcProtoMaskDisable
 Input       :  The Indices

                The Object 
                setValFutOspfRRDSrcProtoMaskDisable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDSrcProtoMaskDisable (INT4 i4SetValFutOspfRRDSrcProtoMaskDisable)
{

    tOspfQMsg          *pSnmpMsg;
    tOspfSnmpIfParam   *pOspfIfParam;
    UINT4               u4NewSrcProtoMaskDisable;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4NewSrcProtoMaskDisable = 0;

    if (i4SetValFutOspfRRDSrcProtoMaskDisable ==
        (INT4) (~(pOspfCxt->u4RrdSrcProtoBitMask) & (RRD_SRC_PROTO_BIT_MASK)))
    {
        return SNMP_SUCCESS;
    }

    /*
     * no transition in bit value (1->1) means that redistribution from the 
     * corresponding protocol was previously enabled & is to be disabled now
     */

    if ((pOspfCxt->u4RrdSrcProtoBitMask & LOCAL_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskDisable & LOCAL_RT_MASK))
    {
        u4NewSrcProtoMaskDisable |= LOCAL_RT_MASK;
    }

    if ((pOspfCxt->u4RrdSrcProtoBitMask & STATIC_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskDisable & STATIC_RT_MASK))
    {
        u4NewSrcProtoMaskDisable |= STATIC_RT_MASK;
    }

    if ((pOspfCxt->u4RrdSrcProtoBitMask & RIP_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskDisable & RIP_RT_MASK))
    {
        u4NewSrcProtoMaskDisable |= RIP_RT_MASK;
    }

    if ((pOspfCxt->u4RrdSrcProtoBitMask & BGP_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskDisable & BGP_RT_MASK))
    {
        u4NewSrcProtoMaskDisable |= BGP_RT_MASK;
    }
    if ((pOspfCxt->u4RrdSrcProtoBitMask & ISISL1_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskDisable & ISISL1_RT_MASK))
    {
        u4NewSrcProtoMaskDisable |= ISISL1_RT_MASK;
    }
    if ((pOspfCxt->u4RrdSrcProtoBitMask & ISISL2_RT_MASK) &&
        (i4SetValFutOspfRRDSrcProtoMaskDisable & ISISL2_RT_MASK))
    {
        u4NewSrcProtoMaskDisable |= ISISL2_RT_MASK;
    }

    if (u4NewSrcProtoMaskDisable)
    {

        pOspfCxt->u4RrdSrcProtoBitMask ^= u4NewSrcProtoMaskDisable;
        /*
         * EnQ SNMP message for redistribution disable with SrcProtoBitMask 
         * as constructed above
         */

        if ((pSnmpMsg = OSPF_SNMP_IF_MSG_ALLOC ()) == NULL)
        {
            return SNMP_FAILURE;
        }

        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pSnmpMsg);

        pOspfIfParam->u1OpCode = OSPF_DISABLE_SRC_PROTO;
        pOspfIfParam->param.srcProtoDisParam.u4SrcProtoDisMask =
            u4NewSrcProtoMaskDisable;
        pOspfIfParam->pOspfCxt = pOspfCxt;

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDSrcProtoMaskDisable;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIOspfRRDSrcProtoMaskDisable) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        if (OSPF_SNMP_IF_MSG_SEND (pSnmpMsg) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pSnmpMsg);
            i1RetVal = SNMP_FAILURE;
        }
        if (pOspfCxt->u4RrdSrcProtoBitMask == 0)
        {
            if (pOspfCxt->u1AsbrFlag == OSPF_TRUE)
            {
                if (nmhSetOspfASBdrRtrStatus (OSPF_FALSE) == SNMP_FAILURE)
                {
                    i1RetVal = SNMP_FAILURE;
                }
            }
            pOspfCxt->u1AsbrFlag = OSPF_FALSE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = i1RetVal;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          ~(pOspfCxt->u4RrdSrcProtoBitMask)));
#endif
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFutOspfRRDRouteMapEnable
 Input       :  The Indices

                The Object 
                setValFutOspfRRDRouteMapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfRRDRouteMapEnable (tSNMP_OCTET_STRING_TYPE
                                * pSetValFutOspfRRDRouteMapEnable)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFutOspfRRDRouteMapEnable->i4_Length > RMAP_MAX_NAME_LEN + 1)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->bAsBdrRtr != OSPF_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->redistrAdmnStatus != OSPF_ENABLED)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDRouteMapEnable;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfRRDRouteMapEnable) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    if (pSetValFutOspfRRDRouteMapEnable->i4_Length != OSPF_ZERO)
    {
        if (STRLEN (pOspfCxt->au1RMapName) == OSPF_ZERO)
        {
            MEMCPY (pOspfCxt->au1RMapName,
                    pSetValFutOspfRRDRouteMapEnable->pu1_OctetList,
                    pSetValFutOspfRRDRouteMapEnable->i4_Length);
            pOspfCxt->au1RMapName[pSetValFutOspfRRDRouteMapEnable->i4_Length] =
                '\0';
        }
        else if (MEMCMP (pOspfCxt->au1RMapName,
                         pSetValFutOspfRRDRouteMapEnable->pu1_OctetList,
                         pSetValFutOspfRRDRouteMapEnable->i4_Length) !=
                 OSPF_ZERO)
        {
            /* Check for existence of Route Map with different Name. 
             * If So, return failure, */
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", gOsRtr.u4OspfCxtId,
                              pSetValFutOspfRRDRouteMapEnable));
#endif
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Resetting the Route Map Name Associated */
        MEMSET (pOspfCxt->au1RMapName, OSPF_ZERO, RMAP_MAX_NAME_LEN);
    }
    /* EnQ redis enable message to RTM */
    RtmTxRedistributeEnableInCxt (pOspfCxt, pOspfCxt->u4RrdSrcProtoBitMask,
                                  pOspfCxt->au1RMapName);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", gOsRtr.u4OspfCxtId,
                      pSetValFutOspfRRDRouteMapEnable));
#endif
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfDistInOutRouteMapTable. */
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfDistInOutRouteMapValue
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType

                The Object
                setValFutOspfDistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfDistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                     pFutOspfDistInOutRouteMapName,
                                     INT4 i4FutOspfDistInOutRouteMapType,
                                     INT4 i4SetValFutOspfDistInOutRouteMapValue)
{
    UINT4               u4ErrCode = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pFutOspfDistInOutRouteMapName == NULL
        || pFutOspfDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (nmhTestv2FutOspfDistInOutRouteMapValue (&u4ErrCode,
                                                pFutOspfDistInOutRouteMapName,
                                                i4FutOspfDistInOutRouteMapType,
                                                i4SetValFutOspfDistInOutRouteMapValue)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        pOspfCxt->pDistanceFilterRMap->u1RMapOldDistance =
            pOspfCxt->pDistanceFilterRMap->u1RMapDistance;

        pOspfCxt->pDistanceFilterRMap->u1RMapDistance =
            (UINT1) i4SetValFutOspfDistInOutRouteMapValue;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIOspfDistInOutRouteMapValue,
                          u4SeqNum, TRUE, OspfLock, OspfUnLock,
                          3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", (INT4) gOsRtr.u4OspfCxtId,
                      pFutOspfDistInOutRouteMapName,
                      i4FutOspfDistInOutRouteMapType,
                      i4SetValFutOspfDistInOutRouteMapValue));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfDistInOutRouteMapRowStatus
 Input       :  The Indices
                FutOspfDistInOutRouteMapName
                FutOspfDistInOutRouteMapType

                The Object
                setValFutOspfDistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfDistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFutOspfDistInOutRouteMapName,
                                         INT4 i4FutOspfDistInOutRouteMapType,
                                         INT4
                                         i4SetValFutOspfDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    UINT4               u4ErrCode = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (nmhTestv2FutOspfDistInOutRouteMapRowStatus (&u4ErrCode,
                                                    pFutOspfDistInOutRouteMapName,
                                                    i4FutOspfDistInOutRouteMapType,
                                                    i4SetValFutOspfDistInOutRouteMapRowStatus)
        == SNMP_SUCCESS)
    {

        switch (i4SetValFutOspfDistInOutRouteMapRowStatus)
        {
            case CREATE_AND_WAIT:
            {
                tFilteringRMap     *pFilter = NULL;
                UINT4               u4Status = FILTERNIG_STAT_DEFAULT;

                RMAP_FILTER_ALLOC (pFilter);

                if (pFilter != NULL)
                {
                    OS_MEM_SET (pFilter, 0, sizeof (tFilteringRMap));
                    pFilter->u1RowStatus =
                        i4SetValFutOspfDistInOutRouteMapRowStatus;
                    if (pFutOspfDistInOutRouteMapName->i4_Length >
                        RMAP_MAX_NAME_LEN)
                    {
                        RMAP_FILTER_FREE (pFilter);
                        return SNMP_FAILURE;
                    }
                    OS_MEM_CPY (pFilter->au1DistInOutFilterRMapName,
                                pFutOspfDistInOutRouteMapName->pu1_OctetList,
                                MEM_MAX_BYTES (RMAP_MAX_NAME_LEN,
                                               (UINT4)
                                               pFutOspfDistInOutRouteMapName->
                                               i4_Length));

                    u4Status =
                        RMapGetInitialMapStatus (pFutOspfDistInOutRouteMapName->
                                                 pu1_OctetList);
                    if (u4Status != 0)
                    {
                        pFilter->u1Status = FILTERNIG_STAT_ENABLE;
                    }
                    else
                    {
                        pFilter->u1Status = FILTERNIG_STAT_DISABLE;
                    }

                    if (i4FutOspfDistInOutRouteMapType ==
                        FILTERING_TYPE_DISTANCE)
                    {
                        pOspfCxt->pDistanceFilterRMap = pFilter;
                    }
                    else if (i4FutOspfDistInOutRouteMapType ==
                             FILTERING_TYPE_DISTRIB_IN)
                    {
                        pOspfCxt->pDistributeInFilterRMap = pFilter;
                    }
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
                break;
            case DESTROY:
                if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    RMAP_FILTER_FREE (pOspfCxt->pDistanceFilterRMap);
                    pOspfCxt->pDistanceFilterRMap = NULL;
                }
                else if (i4FutOspfDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    RMAP_FILTER_FREE (pOspfCxt->pDistributeInFilterRMap);
                    pOspfCxt->pDistributeInFilterRMap = NULL;
                }
                break;
            default:
                if (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    pOspfCxt->pDistanceFilterRMap->u1RowStatus =
                        i4SetValFutOspfDistInOutRouteMapRowStatus;
                }
                else if (i4FutOspfDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    pOspfCxt->pDistributeInFilterRMap->u1RowStatus =
                        i4SetValFutOspfDistInOutRouteMapRowStatus;
                }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIOspfDistInOutRouteMapRowStatus,
                          u4SeqNum, TRUE, OspfLock, OspfUnLock,
                          3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", (INT4) gOsRtr.u4OspfCxtId,
                      pFutOspfDistInOutRouteMapName,
                      i4FutOspfDistInOutRouteMapType,
                      i4SetValFutOspfDistInOutRouteMapRowStatus));
#endif
    if ((i4SetValFutOspfDistInOutRouteMapRowStatus == ACTIVE) &&
        (i4FutOspfDistInOutRouteMapType == FILTERING_TYPE_DISTANCE))
    {
        if (pOspfCxt->pDistanceFilterRMap->u1RMapDistance !=
            pOspfCxt->pDistanceFilterRMap->u1RMapOldDistance)
        {
            /* Restart distance timer for update configured distance value for
             *              *              * existing routes */
            TmrRestartTimer (&(gOsRtr.pOspfCxt->distanceTimer),
                             DISTANCE_TIMER, OSPF_DISTANCE_TIME_INTERVAL);
            pOspfCxt->pLastUpdateRtLst = NULL;
        }
    }

#else
    UNUSED_PARAM (pFutOspfDistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfDistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFutOspfDistInOutRouteMapRowStatus);
#endif /*ROUTEMAP_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  nmhSetFutOspfPreferenceValue
  Input       :  The Indices

                 The Object 
                 setValFutOspfPreferenceValue
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfPreferenceValue (INT4 i4SetValFutOspfPreferenceValue)
{
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (i4SetValFutOspfPreferenceValue == 0)
    {
        gOsRtr.pOspfCxt->u1Distance = OSPF_DEFAULT_PREFERENCE;
    }
    else
    {
        gOsRtr.pOspfCxt->u1Distance = (UINT1) i4SetValFutOspfPreferenceValue;
    }

    /* Restart distance timer for update configured distance value for
       existing routes */
    TmrRestartTimer (&(gOsRtr.pOspfCxt->distanceTimer),
                     DISTANCE_TIMER, OSPF_DISTANCE_TIME_INTERVAL);
    gOsRtr.pOspfCxt->pLastUpdateRtLst = NULL;

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIOspfPreferenceValue,
                          u4SeqNum, FALSE, OspfLock, OspfUnLock,
                          1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", (INT4) gOsRtr.u4OspfCxtId,
                      i4SetValFutOspfPreferenceValue));
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfIfCryptoAuthType 
 Input       :  The Indices
                u4OspfIfIpAddress
                u4OspfAddressLessIf

                The Object 
                i4SetValFutOspfIfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFutOspfIfCryptoAuthType (UINT4 u4OspfIfIpAddress,
                               UINT4 u4OspfAddressLessIf,
                               INT4 i4SetValFutOspfIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    if (pOspfCxt == NULL)
    {
        return i1Return;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4OspfIfIpAddress);
    if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                      (UINT4) u4OspfAddressLessIf)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfIfCryptoAuthType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdOspfIfCryptoAuthType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->u4CryptoAuthType = (UINT4) i4SetValFutOspfIfCryptoAuthType;
        pInterface->pLastAuthkey = NULL;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i", gOsRtr.u4OspfCxtId,
                          u4OspfIfIpAddress, u4OspfAddressLessIf,
                          i4SetValFutOspfIfCryptoAuthType));
#endif
        i1Return = SNMP_SUCCESS;
    }

    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFutOspfVirtIfCryptoAuthType 
 Input       :  The Indices
                u4OspfVirtIfAreaId
                u4OspfVirtIfNeighbor 

                The Object 
                i4SetValFutOspfVirtIfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfVirtIfCryptoAuthType (UINT4 u4OspfVirtIfAreaId,
                                   UINT4 u4OspfVirtIfNeighbor,
                                   INT4 i4SetValFutOspfVirtIfCryptoAuthType)
{

    INT1                i1Return = SNMP_FAILURE;
    tAreaId             areaId;
    tRouterId           nbrId;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, u4OspfVirtIfAreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVirtIfNeighbor);

    if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) != NULL)
    {

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfVirtIfCryptoAuthType;
        SnmpNotifyInfo.u4OidLen
            = sizeof (FsMIStdOspfVirtIfCryptoAuthType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 3;
#endif
        pInterface->u4CryptoAuthType =
            (UINT4) i4SetValFutOspfVirtIfCryptoAuthType;
        pInterface->pLastAuthkey = NULL;
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", gOsRtr.u4OspfCxtId,
                          u4OspfVirtIfAreaId, u4OspfVirtIfNeighbor,
                          i4SetValFutOspfVirtIfCryptoAuthType));
#endif
        i1Return = SNMP_SUCCESS;
    }
    return i1Return;

}
