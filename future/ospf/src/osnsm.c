/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osnsm.c,v 1.18 2013/09/20 17:07:34 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             neighbor state machine.
 *
 *******************************************************************/

#include "osinc.h"
/* Contains Static definition */
#include "osnsm.h"

/* Proto types of the functions private to this file only */

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmRunNsm                                                */
/*                                                                           */
/* Description  : This procedure indexes into the state event table using the*/
/*                nbr event and the neighbor state to get an integer. This   */
/*                integer when indexed into an array of function pointers    */
/*                gives the actual action routine to be called.              */
/*                                                                           */
/* Input        : pNbr             : neighbour                              */
/*                u1NbrEvent      : event                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
NsmRunNsm (tNeighbor * pNbr, UINT1 u1NbrEvent)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmRunNsm\n");

    if (!((pNbr->u1NsmState < MAX_NBR_STATE) && (u1NbrEvent < MAX_NBR_EVENT)))
    {
        return OSPF_FAILURE;
    }

    if (pNbr->pInterface->u1NetworkType < MAX_IF_TYPE)
    {
        OSPF_NBR_TRC5 (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NSM Nbr %x.%d N/W Type %s Current State: %s Evt: %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf,
                       au1DbgIfType[pNbr->pInterface->u1NetworkType],
                       au1DbgNbrState[pNbr->u1NsmState],
                       au1DbgNbrEvent[u1NbrEvent]);
    }

    /* SEM Function is executed now */
    (*a_nsm_func[au1_nsm_table[u1NbrEvent][pNbr->u1NsmState]]) (pNbr);

    if (pNbr->u1NsmState < MAX_NBR_STATE)
    {
        OSPF_NBR_TRC3 (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NSM Nbr %x.%d Next State: %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf, au1DbgNbrState[pNbr->u1NsmState]);
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmInvalid                                                */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure  is invoked when an invalid event occurs.   */
/*                This procedure generates an appropriate error message.     */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmInvalid (tNeighbor * pNbr)
{
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmInvalid\n");

    COUNTER_OP (pNbr->u4NbrEvents, 1);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmLlDown                                                */
/*                                                                           */
/* Description  : Reference : RFC-1793 section 3.2.2                         */
/*                The link state request list, link state retransmission list*/
/*                and the database summary list are cleared and the          */
/*                inactivity timer is disabled.                              */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmLlDown (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmLlDown\n");

    NsmResetVariables (pNbr);
    TmrDeleteTimer (&pNbr->inactivityTimer);

    if (IS_DC_EXT_APPLICABLE_PTOP_IF (pNbr->pInterface) &&
        (pNbr->u1NsmState >= NBRS_INIT) &&
        (pNbr->pInterface->u1IsmState != IFS_DOWN))
    {
        GENERATE_IF_EVENT (pNbr->pInterface, IFE_DOWN);
    }

    NbrUpdateState (pNbr, NBRS_DOWN);
    pNbr->u4NbrCryptSeqNum = 0;

    OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              pNbr, OSPF_RED_SYNC_NBR_STATE_LL_DOWN, 0);
    if (IS_DC_EXT_APPLICABLE_PTOP_IF (pNbr->pInterface))
    {
        TmrRestartTimer (&(pNbr->pInterface->pollTimer), POLL_TIMER,
                         NO_OF_TICKS_PER_SEC *
                         (pNbr->pInterface->i4PollInterval));
    }
    OSPF_NBR_TRC (OSPF_NSM_TRC, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Nbr Brought DN\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmDown                                                   */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                The link state request list, link state retransmission list*/
/*                and the database summary list are cleared and the          */
/*                inactivity timer is disabled.                              */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmDown (tNeighbor * pNbr)
{

    UINT4               u4OspfCxtId;

    u4OspfCxtId = pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId;
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr, u4OspfCxtId, "FUNC: NsmDown\n");
    NsmResetVariables (pNbr);
    TmrDeleteTimer (&pNbr->inactivityTimer);
    NbrUpdateState (pNbr, NBRS_DOWN);
    OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              pNbr, OSPF_RED_SYNC_NBR_STATE_DOWN, 0);
    if (pNbr->u1ConfigStatus != CONFIGURED_NBR)
    {
        pNbr->bIsBfdDisable = OSIX_TRUE;
        NbrDelete (pNbr);
    }

    OSPF_NBR_TRC (OSPF_NSM_TRC, pNbr, u4OspfCxtId, "Nbr Deleted\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Nsm1wayRcvd                                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                The link state request list, link state retransmission list*/
/*                and the database summary list are cleared. The next state  */
/*                is init.                                                   */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
Nsm1wayRcvd (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: Nsm1wayRcvd\n");

    NsmResetVariables (pNbr);
    NbrUpdateState (pNbr, NBRS_INIT);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmRestartAdj                                            */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure clears the link state request list, link    */
/*                state retransmission list and the database summary list.   */
/*                The next state is exstart and the process of sending empty */
/*                DDP with I,M,MS bits set is initiated.                     */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmRestartAdj (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmRestartAdj\n");
    NsmResetVariables (pNbr);
    NbrUpdateState (pNbr, NBRS_EXSTART);
    OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              pNbr, OSPF_RED_SYNC_NBR_STATE_RESTART_ADJ, 0);
    pNbr->dbSummary.seqNum++;
    pNbr->dbSummary.bMaster = OSPF_FALSE;
    DdpSendDdp (pNbr);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmCheckAdj                                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure determines whether the neighbor should      */
/*                remain adjacent. If yes there is no state change and no    */
/*                action is necessary. Otherwise the link state retransmiss- */
/*                ion list, link state request list and the summary list are */
/*                cleared and the next state is 2-way.                       */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmCheckAdj (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmCheckAdj\n");
    if (NsmToBecomeAdj (pNbr) == OSPF_FALSE)
    {

        NbrUpdateState (pNbr, NBRS_2WAY);
        NsmResetVariables (pNbr);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmProcessAdj                                            */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure determines whether the neighbor should      */
/*                become adjacent. If yes no there is no state change and no */
/*                action is necessary. Otherwise the next state is exstart   */
/*                and the process of sending empty DDPs is initiated.        */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmProcessAdj (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmProcessAdj\n");
    if (NsmToBecomeAdj (pNbr) == OSPF_TRUE)
    {

        NbrUpdateState (pNbr, NBRS_EXSTART);
        OsixGetSysTime ((tOsixSysTime *) & (pNbr->dbSummary.seqNum));
        pNbr->dbSummary.bMaster = OSPF_FALSE;
        DdpSendDdp (pNbr);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmExchgDone                                             */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                If the link state request list is empty then next state is */
/*                full. Otherwise the next state is loading and the process  */
/*               of sending link state request packets is initiated.         */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmExchgDone (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmExchgDone\n");
    if (LrqIsEmptyLsaReqLst (pNbr) == OSPF_TRUE)
    {
        NbrUpdateState (pNbr, NBRS_FULL);
        OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->
                                  u4OspfCxtId, pNbr,
                                  OSPF_RED_SYNC_NBR_STATE_FULL, 0);

        if (pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState
            == OSPF_GR_RESTART)
        {
            GrCheckAndExitGracefulRestart (pNbr->pInterface->pArea->pOspfCxt);
        }
    }
    else
    {

        NbrUpdateState (pNbr, NBRS_LOADING);
        if (pNbr->lsaReqDesc.bLsaReqOutstanding == OSPF_FALSE)
        {
            LrqSendLsaReq (pNbr);
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmNegDone                                               */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                Next state is exstart. Process of sending DDPs is          */
/*                initiated. If the router is the bMaster then enable the   */
/*                retransmission timer for retransmission of DDPs from the   */
/*                bMaster.                                                  */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmNegDone (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmNegDone\n");
    DdpBuildSummary (pNbr);
    NbrUpdateState (pNbr, NBRS_EXCHANGE);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : Nsm2wayRcvd                                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure first determines whether the neighbor should*/
/*                be adjacent. If yes the next state is exstart. The sequence*/
/*                number field is set and the process of sending empty DDPs  */
/*                with  I,M,MS bits set is initiated.                        */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
Nsm2wayRcvd (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: Nsm2wayRcvd\n");

    if (NsmToBecomeAdj (pNbr) == OSPF_TRUE)
    {

        NbrUpdateState (pNbr, NBRS_EXSTART);
        OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->
                                  u4OspfCxtId, pNbr,
                                  OSPF_RED_SYNC_NBR_STATE_TWO_WAY_RECVD, 0);
        if (pNbr->dbSummary.seqNum == 0)
        {
            OsixGetSysTime ((tOsixSysTime *) & (pNbr->dbSummary.seqNum));
        }
        else
        {
            pNbr->dbSummary.seqNum++;
        }
        pNbr->dbSummary.bMaster = OSPF_FALSE;
        DdpSendDdp (pNbr);
    }
    else
    {
        NbrUpdateState (pNbr, NBRS_2WAY);
        OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->
                                  u4OspfCxtId, pNbr,
                                  OSPF_RED_SYNC_NBR_STATE_TWO_WAY_RECVD, 0);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmStart                                                  */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure sends hello packet to the neighbor. The next*/
/*                state is attempt The inactivity timer for the neighbor is  */
/*                started.                                                   */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmStart (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmStart\n");
    /* At the time of Restart, neighbors must be learned only through 
     *  the received hello packet*/
    if (pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState
        != OSPF_GR_RESTART)
    {
        HpSendHello (pNbr->pInterface, pNbr, HELLO_TIMER);
        TmrSetTimer (&pNbr->inactivityTimer, INACTIVITY_TIMER,
                     NO_OF_TICKS_PER_SEC *
                     (pNbr->pInterface->i4RtrDeadInterval));
        NbrUpdateState (pNbr, NBRS_ATTEMPT);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmStartInactTimer                                      */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure starts the inactivity timer for the         */
/*                neighbor. The next state is init.                          */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmStartInactTimer (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmStartInactTimer\n");

    TmrSetTimer (&pNbr->inactivityTimer, INACTIVITY_TIMER,
                 NO_OF_TICKS_PER_SEC * (pNbr->pInterface->i4RtrDeadInterval));
    NbrUpdateState (pNbr, NBRS_INIT);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmRestartInactTimer                                    */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure restarts the inactivity timer. There is no  */
/*                state change.                                              */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmRestartInactTimer (tNeighbor * pNbr)
{
    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can restart inactivity timer */
        return;
    }

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmRestartInactTimer\n");

    TmrRestartTimer (&pNbr->inactivityTimer, INACTIVITY_TIMER,
                     NO_OF_TICKS_PER_SEC *
                     (pNbr->pInterface->i4RtrDeadInterval));

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmAttHelloRcvd                                         */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure restarts the inactivity timer. The next     */
/*                state is init.                                             */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmAttHelloRcvd (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmAttHelloRcvd\n");

    TmrRestartTimer (&pNbr->inactivityTimer, INACTIVITY_TIMER,
                     NO_OF_TICKS_PER_SEC *
                     (pNbr->pInterface->i4RtrDeadInterval));
    NbrUpdateState (pNbr, NBRS_INIT);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmLdngDone                                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure does not perform any action. The next state */
/*                is full.                                                   */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC void
NsmLdngDone (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmLdngDone\n");
    NbrUpdateState (pNbr, NBRS_FULL);
    OspfRmSendNbrStateChange (pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              pNbr, OSPF_RED_SYNC_NBR_STATE_FULL, 0);

    if (pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState
        == OSPF_GR_RESTART)
    {
        GrCheckAndExitGracefulRestart (pNbr->pInterface->pArea->pOspfCxt);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmNoAction                                              */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure does not perform any action or state change.*/
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmNoAction (tNeighbor * pNbr)
{
    UNUSED_PARAM (pNbr);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmToBecomeAdj                                          */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.4                          */
/*                This procedure determines whether this adjacency should be */
/*                maintained with this neighbor.                             */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if adjacency should be formed with this neighbor*/
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
NsmToBecomeAdj (tNeighbor * pNbr)
{

    tInterface         *pInterface;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmToBecomeAdj\n");
    pInterface = pNbr->pInterface;
    if ((pInterface->u1NetworkType == IF_PTOP) ||
        (pInterface->u1NetworkType == IF_VIRTUAL) ||
        (pInterface->u1NetworkType == IF_PTOMP) ||
        (pInterface->u1IsmState == IFS_DR) ||
        (pInterface->u1IsmState == IFS_BACKUP) ||
        (UtilIpAddrComp (pNbr->nbrIpAddr,
                         GET_DR (pInterface)) == OSPF_EQUAL) ||
        (UtilIpAddrComp (pNbr->nbrIpAddr, GET_BDR (pInterface)) == OSPF_EQUAL))
    {

        OSPF_NBR_TRC (OSPF_NSM_TRC, pNbr,
                      pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Should be adjacent\n");
        return (OSPF_TRUE);
    }
    else
    {

        OSPF_NBR_TRC (OSPF_NSM_TRC, pNbr,
                      pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Need not be adjacent\n");

        return OSPF_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NsmResetVariables                                        */
/*                                                                           */
/* Description  : Resets all the variables associated with the neighbour.    */
/*                                                                           */
/* Input        : pNbr            : neighbour                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NsmResetVariables (tNeighbor * pNbr)
{

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NsmResetVariables\n");

    LsuClearRxmtLst (pNbr);
    LrqClearLsaReqLst (pNbr);
    DdpClearSummary (pNbr);

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osnsm.c                        */
/*-----------------------------------------------------------------------*/
