
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osvxtask.c,v 1.5 2007/02/01 15:00:43 iss Exp $
 *
 * Description:This file contain procedure to initialize memory
 *             and spawn ospf task for VxWorks Environment
 *
 *******************************************************************/

/* Change for porting FutureOspf to VxWorks IP stack           */
/* If FutureOspf is used with FutureIp, the Cfa component does */
/* all the initialization. In its absence, we do inits here    */

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfMain                                                   */
/*                                                                           */
/* Description  : This function initializes memory buffer and spawns the     */
/*                ospf task                                                  */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

tOsixCfg            gOsixCfg;
tTimerCfg           gTimerCfg;
tMemPoolCfg         gMemPoolCfg;
tBufConfig          gBufConfig;

int
OspfMain ()
{

/* Initialize Osix */
    gOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    gOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    gOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    gOsixCfg.u4MyNodeId = SELF;
    gOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;

    gOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (OsixInit (&gOsixCfg) != OSIX_SUCCESS)
    {
        PRINTF ("OsixInit Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize MemPool manager */
    gMemPoolCfg.u4MaxMemPools = SYS_MAX_MEMPOOLS;
    gMemPoolCfg.u4NumberOfMemTypes = 0;
    gMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    gMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    gMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    gMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&gMemPoolCfg) != MEM_SUCCESS)
    {
        PRINTF ("MemInitMemPool Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize Buffer manager */
    gBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    gBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    gBufConfig.u4MaxDataBlockCfg = 1;
    gBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    gBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&gBufConfig) != CRU_SUCCESS)
    {
        PRINTF ("BufInitManager Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize the Timer Manager */
    gTimerCfg.u4MaxTimerLists = SYS_MAX_NUM_OF_TIMER_LISTS;
    if (TmrTimerInit (&gTimerCfg) != TMR_SUCCESS)
    {
        PRINTF ("TmrTimerInit Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

    if (IpifSpawnOspfTask () != OSPF_SUCCESS)
    {
        printf ("\n OSPFSpawn is failed \n");
        return (0);
    }

    Fsap2Start ();

    printf ("OSPF Spawn Success\n");
    return (0);
}

/*------------------------------------------------------------------------*/
/*                        End of the file  osvxtask.c                       */
/*------------------------------------------------------------------------*/
