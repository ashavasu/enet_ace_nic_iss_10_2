/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osragext.c,v 1.17 2017/09/21 13:48:47 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *                   route aggregation feature
 *
 *******************************************************************/
#include "osinc.h"

/************************************************************************/
/*                                       */
/* Function         : RagAddRouteInAggAddrRange                  */
/*                                     */
/* Description             : This procedure adds the newly added         */
/*              external route in the address range         */
/*              it falls in                        */
/*                                                                        */
/* Input            : pExtRoute -Pointer to Ext Rt             */
/*                                                                      */
/* Output           : None                                             */
/*                                                                      */
/* Returns          : VOID                                             */
/*                                                                         */
/************************************************************************/
PUBLIC VOID
RagAddRouteInAggAddrRangeInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{
    tAsExtAddrRange    *pBkBoneRng = NULL;
    tAsExtAddrRange    *pTmpNssaRng = NULL;
    tAsExtAddrRange    *pType7Rng = NULL;
    tArea              *pArea = NULL;
    UINT1               u1ChngFlag;
    UINT1               u1Tos = TOS_0;
    tMetric             aTempMetric[OSPF_MAX_METRIC];

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddRouteInAggAddrRange\n");

    if (pOspfCxt->pBackbone != NULL)
    {

        /* Find the most specific bkbone address range  
           that subsumes the route                      
         */

        pBkBoneRng = RagFindMatChngBkBoneRangeInCxt (pOspfCxt, pExtRoute);
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }

        /* Find the most specific NSSA range that subsumes the 
           route    
         */
        pTmpNssaRng = RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);
        if (pTmpNssaRng == NULL)
        {

            /* If no NSSA range subsumes the added Ext Rt,    
               Bkbone range that subsumes the Ext Rt is used
               for aggregation in NSSA                    
             */

            pType7Rng = pBkBoneRng;
            if (pType7Rng != NULL)
            {
                RagProcessNssaRange (pType7Rng, pArea);
            }
        }
        else
        {
            pType7Rng = pTmpNssaRng;
            OSPF_TRC3 (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                       "Route: %x Falls in Rng: %x  AreaId : %x \n",
                       OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum),
                       OSPF_CRU_BMC_DWFROMPDU (pType7Rng->ipAddr),
                       OSPF_CRU_BMC_DWFROMPDU (pType7Rng->areaId));

            for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
            {
                aTempMetric[u1Tos].u4Value = pType7Rng->aMetric[u1Tos].u4Value;
                aTempMetric[u1Tos].u4MetricType =
                    pType7Rng->aMetric[u1Tos].u4MetricType;
                aTempMetric[u1Tos].bStatus = pType7Rng->aMetric[u1Tos].bStatus;
            }

            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "FUNC: RagUpdatCostTypeForRange\n");

            u1ChngFlag = RagUpdatCostTypeForRange (pType7Rng, pExtRoute);
            UNUSED_PARAM (u1ChngFlag);

            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                      "Exit FUNC: RagUpdatCostTypeForRange\n");

            if (RagIsChangeInMetric (aTempMetric, pType7Rng->aMetric)
                == OSPF_TRUE)
            {
                /* NSSA range, if affected (cost,type) by new Ext Rt is used 
                 * for aggregation in NSSA */
                RagProcessNssaRange (pType7Rng, pArea);
            }
        }

        if (pType7Rng == NULL)

            /* If Ext Rt is not subsumed by any range   
               (both bkbone and NSSA), originate non-agg    
               Type 7 in NSSA                       
             */

            RagOriginateNonAggLsaInCxt (pOspfCxt, pExtRoute, pArea);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddRouteInAggAddrRange\n");
}

/************************************************************************/
/*                                                                   */
/* Function        : RagDelRouteFromAggAddrRange                    */
/*                                                                         */
/* Description     : This procedure deletes the Ext Rt             */
/*          from any address range it falls in             */
/*                                                                      */
/* Input          : pExtRoute -Pointer to Ext Rt                 */
/*                                                                      */
/* Output          : None                                              */
/*                                                                      */
/* Returns         : VOID                                              */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagDelRouteFromAggAddrRangeInCxt (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute)
{

    tLsaInfo           *pType5LsaInfo = NULL;
    tArea              *pArea = NULL;
    tAsExtAddrRange    *pTmpNssaRng = NULL;
    tLsaInfo           *pType7LsaInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagDelRouteFromAggAddrRange\n");

    /* Check if the deleted Ext Rt is subsumed by any Bkbone Range */
    if (pExtRoute->pAsExtAddrRange != NULL)
    {

        /* If YES, removes the Ext Rt from Bkbone range 
           and takes necessary measures 
         */
        RagFlushBkBoneRangeInCxt (pOspfCxt, pExtRoute->pAsExtAddrRange,
                                  pExtRoute);

    }
    else
    {
        /* If NO, check if this Ext Rt has caused non-aggregated Type 5 
           LSA generation
         */
        pType5LsaInfo =
            LsuDatabaseLookUp (AS_EXT_LSA, &(pExtRoute->ipNetNum),
                               &(pExtRoute->ipAddrMask),
                               &(pOspfCxt->rtrId), NULL,
                               (UINT1 *) pOspfCxt->pBackbone);

        /* Flush the non-aggregated Type 5 LSA  */
        if ((pType5LsaInfo != NULL) &&
            (pType5LsaInfo->u1TrnsltType5 == REDISTRIBUTED_TYPE5))
        {
            if (pType5LsaInfo->pLsaDesc != NULL)
            {
                pType5LsaInfo->pLsaDesc->pAssoPrimitive = NULL;
            }
            LsuDeleteNodeFromAllRxmtLst (pType5LsaInfo);
            AgdFlushOut (pType5LsaInfo);
        }
    }

    /* Check if any of the NSSA area of the router 
       has Type 7 LSAs because of this Ext Rt
     */

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }
        /* Find the range in NSSA area that best 
           subsumes this Ext Rt     
         */
        pTmpNssaRng = RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);

        if (pTmpNssaRng != NULL)
        {
            /* If range is found, take necessary measures by invoking 
               below function
             */
            RagFlushNssaRange (pTmpNssaRng, pExtRoute, pArea);
        }
        else
        {
            /* If there is no NSSA range that subsumes this Ext Rt,
               aggregation in area might have been done on the 
               basis of Bkbone range that subsumes the Ext Rt
             */
            if (pExtRoute->pAsExtAddrRange != NULL)
            {
                /* If YES, take necessary measure by invoking below
                   function
                 */
                RagFlushNssaRange (pExtRoute->pAsExtAddrRange,
                                   pExtRoute, pArea);
            }
            else
            {

                /* This Ext Rt must have been originated as non- aggregated 
                   Type 7 LSA
                 */
                pType7LsaInfo =
                    LsuDatabaseLookUp (NSSA_LSA, &(pExtRoute->ipNetNum),
                                       &(pExtRoute->ipAddrMask),
                                       &(pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea);

                if (pType7LsaInfo != NULL)
                {
                    /* If so, flush the Type 7 LSA   */

                    if (pType7LsaInfo->pLsaDesc != NULL)
                    {
                        pType7LsaInfo->pLsaDesc->pAssoPrimitive = NULL;
                    }
                    AgdFlushOut (pType7LsaInfo);
                }
            }
        }
    }
    pExtRoute->pAsExtAddrRange = NULL;

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagDelRouteFromAggAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                    */
/* Function     : RagAddrRangeAttrChng                        */
/*                                                                         */
/* Description     : Handles attribute change for configured          */
/*          range                            */
/*                                                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
RagAddrRangeAttrChngInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddrRangeAttrChng\n");
    if (pAsExtRange->u1AttrMask == RAG_NO_CHNG)
    {
        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                  " NO Change in Attributes \n");
        return;
    }
    /* Add the range again  */
    RagAddNewAddrRangeInCxt (pOspfCxt, pAsExtRange);

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddrRangeAttrChng\n");
    return;
}

/************************************************************************/
/*                                                               */
/* Function        : RagOriginateNonAggLsa                 */
/*                                                              */
/* Description     : Originates non-aggregated Type 5              */
/*          or Type 7 LSA for the Ext Rt passed             */
/*                                      */
/* Input           : pExtRoute -Pointer to Ext Rt              */
/*        : pArea - Pointer to Area                */
/*                                      */
/* Output          : None                           */
/*                                                                    */
/* Returns         : VOID                             */
/*                                                       */
/************************************************************************/
PUBLIC VOID
RagOriginateNonAggLsaInCxt (tOspfCxt * pOspfCxt,
                            tExtRoute * pExtRoute, tArea * pArea)
{
    tLsaInfo           *pLsaInfo = NULL;
    tIPADDR             lsId;
    tExtLsdbOverflowTrapInfo trapElsdbOverflow;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagOriginateNonAggLsa\n");

    /* Area passed is NULL - Originate Type 5 LSA   */
    if (pArea == NULL)
    {

        if ((pLsaInfo =
             LsuDatabaseLookUp (AS_EXT_LSA, &(pExtRoute->ipNetNum),
                                &(pExtRoute->ipAddrMask), &(pOspfCxt->rtrId),
                                NULL, (UINT1 *) pOspfCxt->pBackbone)) != NULL)
        {
            if (pLsaInfo->pLsaDesc != NULL)
            {
                pLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pExtRoute;
                pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
            }
            pLsaInfo->u1LsaFlushed = OSPF_FALSE;
        }

        /* Originate new Type 5 LSA */
        if (RagGenExtLsaInCxt (pOspfCxt, AS_EXT_LSA, (UINT1 *) pExtRoute) ==
            OSPF_TRUE)
        {
            IP_ADDR_COPY (lsId, pExtRoute->ipNetNum);

            OlsModifyLsId (pOspfCxt->pBackbone, AS_EXT_LSA, &(lsId),
                           (UINT1 *) pExtRoute);
            OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
                       "FUNC:RagOriginateNonAggLsaInCxt,origin of new ext LSA  linkstateId  = %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (lsId));
            OlsGenerateLsa (pOspfCxt->pBackbone, AS_EXT_LSA, &(lsId),
                            (UINT1 *) pExtRoute);
        }

        if (IS_TRAP_ENABLED_IN_CXT (LSDB_APPROACHING_OVERFLOW_TRAP, pOspfCxt)
            && pOspfCxt->bLsdbApproachingOvflTrapGen == OSPF_FALSE
            && (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT (pOspfCxt) >=
                OVERFLOW_APPROACHING_LIMIT_IN_CXT (pOspfCxt))
            && (pOspfCxt->i4ExtLsdbLimit != NO_LIMIT))
        {
            trapElsdbOverflow.i4ExtLsdbLimit = pOspfCxt->i4ExtLsdbLimit;
            IP_ADDR_COPY (trapElsdbOverflow.rtrId, pOspfCxt->rtrId);
            SnmpifSendTrapInCxt (pOspfCxt, LSDB_APPROACHING_OVERFLOW_TRAP,
                                 &trapElsdbOverflow);
            pOspfCxt->bLsdbApproachingOvflTrapGen = OSPF_TRUE;
        }
    }
    /* Area passed is NSSA - Originate Type 7       */
    else if (pArea->u4AreaType == NSSA_AREA)
    {
        if (pOspfCxt->bOverflowState == OSPF_TRUE)
        {
            OSPF_TRC2 (OS_RESOURCE_TRC, pOspfCxt->u4OspfCxtId,
                       "Already in LSDB overflow state. Type 7 LSA for %x %x "
                       "not generated\n",
                       OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum),
                       OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask));
            return;
        }

        if ((UtilIpAddrComp (pExtRoute->ipNetNum, gNullIpAddr) == OSPF_EQUAL)
            && (UtilIpAddrComp (pExtRoute->ipAddrMask, gNullIpAddr) ==
                OSPF_EQUAL))
        {
            IP_ADDR_COPY (lsId, pExtRoute->ipNetNum);
            OlsGenerateLsa (pArea, DEFAULT_NSSA_LSA, &(lsId),
                            (UINT1 *) pExtRoute);
        }
        else
        {
            IP_ADDR_COPY (lsId, pExtRoute->ipNetNum);

            OlsModifyLsId (pArea, NSSA_LSA, &(lsId), (UINT1 *) pExtRoute);
            OlsGenerateLsa (pArea, NSSA_LSA, &(lsId), (UINT1 *) pExtRoute);
        }

    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "FUNC: RagOriginateNonAggLsa\n");
}

/************************************************************************/
/*                                                               */
/* Function        : RagAsExtAddDefaultValues                 */
/*                                                               */
/* Description     : Creates As Ext Entry with default              */
/*          values                         */
/*                                      */
/*                                       */
/*                                                                 */
/* Input           : u4AsExtNet -Range's Net                  */
/*        : u4AsExtMask - Range Mask                 */
/*          u4AreaId - Area Id associated with Range          */
/*                                       */
/*                                      */
/* Output          : None                           */
/*                                      */
/*                                                                 */
/* Returns         : As Ext range OR NULL                     */
/*                                       */
/*                                      */
/*                                                       */
/************************************************************************/
PUBLIC tAsExtAddrRange *
RagAsExtAddDefaultValues (UINT4 u4AsExtNet, UINT4 u4AsExtMask, UINT4 u4AreaId)
{
    tAsExtAddrRange    *pAsExtAddrRng = NULL;
    UINT1               u1Tos;

    AS_EXT_AGG_ALLOC (pAsExtAddrRng);

    if (pAsExtAddrRng == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "AS EXT AGG Alloc Failed\n");
        return NULL;
    }
    /*Summary address should be the network address */
    u4AsExtNet = (u4AsExtNet & u4AsExtMask);

    OSPF_CRU_BMC_DWTOPDU (pAsExtAddrRng->ipAddr, u4AsExtNet);
    OSPF_CRU_BMC_DWTOPDU (pAsExtAddrRng->ipAddrMask, u4AsExtMask);
    OSPF_CRU_BMC_DWTOPDU (pAsExtAddrRng->areaId, u4AreaId);
    pAsExtAddrRng->u1AggTranslation = OSPF_TRUE;
    pAsExtAddrRng->u1RangeEffect = RAG_ADVERTISE;
    pAsExtAddrRng->rangeStatus = CREATE_AND_WAIT;
    pAsExtAddrRng->u1AttrMask = RAG_NO_CHNG;

    for (u1Tos = TOS_0; u1Tos < OSPF_MAX_METRIC; u1Tos++)
    {
        pAsExtAddrRng->aMetric[u1Tos].u4MetricType = TYPE_1_METRIC;
        pAsExtAddrRng->aMetric[u1Tos].u4Value = LS_INFINITY_24BIT;
        pAsExtAddrRng->aMetric[u1Tos].bStatus = OSPF_INVALID;
    }
    TMO_SLL_Init (&(pAsExtAddrRng->extRtLst));
    TMO_SLL_Init_Node (&(pAsExtAddrRng->nextAsExtAddrRngInRtr));

    return pAsExtAddrRng;
}

/************************************************************************/
/*                                                                */
/* Function        : RagHandleAsExtAgg                     */
/*                                                                */
/* Description     : Handles creation of AS Ext Entry              */
/*                                      */
/* Input           : pAsExtRange - Pointer to range              */
/*        : u1Type - Action to be taken                 */
/*                                      */
/* Output          : None                           */
/*                                                                 */
/* Returns         : As Ext range OR NULL                     */
/*                                                       */
/************************************************************************/
PUBLIC INT4
RagHandleAsExtAggInCxt (tOspfCxt * pOspfCxt,
                        tAsExtAddrRange * pAsExtRange, UINT1 u1Type)
{

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagHandleAsExtAgg\n");
    switch (u1Type)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
        case ACTIVE:
            RagAddNewAddrRangeInCxt (pOspfCxt, pAsExtRange);
            break;

        case NOT_IN_SERVICE:
            RagAddrRangeAttrChngInCxt (pOspfCxt, pAsExtRange);
            break;

        case DESTROY:
            RagDelAddrRangeInCxt (pOspfCxt, pAsExtRange, OSPF_TRUE);

            break;

        default:
            OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId, "Invalid Action\n");
            return OSPF_FAILURE;
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "FUNC: RagHandleAsExtAgg\n");
    return OSPF_SUCCESS;
}

/************************************************************************/
/*                                                                */
/* Function        : RagAddRangeInRtLst                     */
/*                                                                 */
/* Description     : Inserts range in global Rtr Lst in             */
/*          ascending order                     */
/*                                      */
/* Input           : pAsExtRange -Pointer to Range               */
/*                                      */
/* Output          : None                           */
/*                                                                */
/* Returns         : OSPF_TRUE - If range added in Lst             */
/*          OSPF_FALSE - If addition fails              */
/*                                                       */
/************************************************************************/
PUBLIC UINT1
RagAddRangeInRtLstInCxt (tOspfCxt * pOspfCxt, tAsExtAddrRange * pAsExtRange)
{

    tArea              *pArea = NULL;
    tAsExtAddrRange    *pScanAsExtRng = NULL;
    UINT1               u1RetVal;
    tIPADDR             newRngNet;
    tIPADDR             tmpRngNet;
    tTMO_SLL_NODE      *pPrev = NULL;
    tTMO_SLL_NODE      *pScanAsExtNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: RagAddRangeInRtLst\n");

    /* If associated area does not exist - return */
    pArea = GetFindAreaInCxt (pOspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        return OSPF_FALSE;
    }

    if ((UtilIpAddrComp (pAsExtRange->areaId, gNullIpAddr)
         != OSPF_EQUAL) && (pArea->u4AreaType != NSSA_AREA))
    {
        return OSPF_FALSE;
    }
    /* reset pointer to the cached node */
    pOspfCxt->pLastExtAgg = NULL;

    /* If first node in list  - insert */
    if (TMO_SLL_Count (&(pOspfCxt->asExtAddrRangeLstInRtr)) == RAG_NO_CHNG)
    {
        TMO_SLL_Add (&(pOspfCxt->asExtAddrRangeLstInRtr),
                     &(pAsExtRange->nextAsExtAddrRngInRtr));
        return OSPF_TRUE;
    }

    UtilIpAddrMaskCopy (newRngNet, pAsExtRange->ipAddr,
                        pAsExtRange->ipAddrMask);

    /* List is in ascending order :
       Starting with lowest area Id range, all nodes
       are arranged in ascending order of net/mask
     */
    TMO_SLL_Scan (&(pOspfCxt->asExtAddrRangeLstInRtr),
                  pScanAsExtNode, tTMO_SLL_NODE *)
    {
        pScanAsExtRng = GET_ASEXTRNG_FROM_RTRLST (pScanAsExtNode);
        u1RetVal = UtilIpAddrComp (pAsExtRange->areaId, pScanAsExtRng->areaId);

        if (u1RetVal == OSPF_GREATER)
        {
            /* Check if the next node  is not NULL 
               If next node is NULL,, means we have reached 
               the end of list and this node should be inserted as 
               last node
             */
            pNext = TMO_SLL_Next (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                  &(pScanAsExtRng->nextAsExtAddrRngInRtr));
            if (pNext == NULL)
            {
                TMO_SLL_Insert (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                &(pScanAsExtRng->nextAsExtAddrRngInRtr),
                                &(pAsExtRange->nextAsExtAddrRngInRtr));
                return OSPF_TRUE;
            }
            else
            {
                continue;
            }
        }
        else if (u1RetVal == OSPF_LESS)
        {
            /* Insert before this node */
            pPrev = TMO_SLL_Previous (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                      &(pScanAsExtRng->nextAsExtAddrRngInRtr));
            TMO_SLL_Insert (&(pOspfCxt->asExtAddrRangeLstInRtr),
                            pPrev, &(pAsExtRange->nextAsExtAddrRngInRtr));

            return OSPF_TRUE;
        }
        else
        {
            /* There exist range having same area Id:
               So now inserted in ascending order of 
               net/mask
             */
            UtilIpAddrMaskCopy (tmpRngNet, pScanAsExtRng->ipAddr,
                                pScanAsExtRng->ipAddrMask);
            if (UtilIpAddrComp (tmpRngNet, newRngNet) == OSPF_LESS)
            {
                pNext = TMO_SLL_Next (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                      &(pScanAsExtRng->nextAsExtAddrRngInRtr));
                if (pNext == NULL)
                {
                    TMO_SLL_Insert (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                    &(pScanAsExtRng->nextAsExtAddrRngInRtr),
                                    &(pAsExtRange->nextAsExtAddrRngInRtr));
                    return OSPF_TRUE;
                }
                else
                {
                    continue;
                }
            }
            else if (UtilIpAddrComp (tmpRngNet, newRngNet) == OSPF_GREATER)
            {
                /* Insert before this node */
                pPrev = TMO_SLL_Previous
                    (&(pOspfCxt->asExtAddrRangeLstInRtr),
                     &(pScanAsExtRng->nextAsExtAddrRngInRtr));

                TMO_SLL_Insert (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                pPrev, &(pAsExtRange->nextAsExtAddrRngInRtr));
                return OSPF_TRUE;
            }
            else
            {
                if (UtilIpAddrComp (pAsExtRange->ipAddrMask,
                                    pScanAsExtRng->ipAddrMask) == OSPF_GREATER)
                {
                    pNext = TMO_SLL_Next
                        (&(pOspfCxt->asExtAddrRangeLstInRtr),
                         &(pScanAsExtRng->nextAsExtAddrRngInRtr));

                    if (pNext == NULL)
                    {
                        /* Insert after this node */
                        TMO_SLL_Insert (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                        &(pScanAsExtRng->nextAsExtAddrRngInRtr),
                                        &(pAsExtRange->nextAsExtAddrRngInRtr));

                        return OSPF_TRUE;
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (UtilIpAddrComp (pAsExtRange->ipAddrMask,
                                         pScanAsExtRng->ipAddrMask) ==
                         OSPF_LESS)
                {
                    /* Insert before this node */
                    pPrev =
                        TMO_SLL_Previous
                        (&(pOspfCxt->asExtAddrRangeLstInRtr),
                         &(pScanAsExtRng->nextAsExtAddrRngInRtr));

                    TMO_SLL_Insert (&(pOspfCxt->asExtAddrRangeLstInRtr),
                                    pPrev,
                                    &(pAsExtRange->nextAsExtAddrRngInRtr));

                    return OSPF_TRUE;
                }
                else
                {
                    if (UtilIpAddrComp (pAsExtRange->ipAddr,
                                        pScanAsExtRng->ipAddr) != OSPF_EQUAL)
                    {
                        OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                                  " Invalid Config \n");
                        return OSPF_FALSE;
                    }

                    OSPF_TRC (OSPF_RAG_TRC, pOspfCxt->u4OspfCxtId,
                              " Entry exist - Must be made ACTIVE\n");
                    return OSPF_TRUE;
                }
            }
        }
    }                            /* List Scan */
    return OSPF_FALSE;
}

/************************************************************************/
/*                                                                    */
/* Function     : RagInsertRngInAreaLst                    */
/*                                                                         */
/* Description     : Adds the range in area Lst in             */
/*            sorted order                         */
/*                                                                      */
/* Input          : pAsExtRange -Pointer to new Range               */
/*                  pArea - Area pointer */
/*                                                                      */
/* Output          : None                                              */
/*                                                                      */
/* Returns         :  OSPF_SUCCESS                                       */
/*                    OSPF_FAILURE                                        */
/************************************************************************/
PUBLIC INT1
RagInsertRngInAreaLst (tAsExtAddrRange * pAsExtRange, tArea * pArea)
{
    tIPADDR             newRngNet;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               au4ExtIndex[2];

    OSPF_TRC (OSPF_FN_ENTRY, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagInsertRngInAreaLst\n");
    UtilIpAddrMaskCopy (newRngNet, pAsExtRange->ipAddr,
                        pAsExtRange->ipAddrMask);

    inParams.pRoot = pArea->pAsExtAddrRangeTrie;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = UtilFindMaskLen (OSPF_CRU_BMC_DWFROMPDU
                                            (pAsExtRange->ipAddrMask));
    au4ExtIndex[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU (pAsExtRange->ipAddr));
    au4ExtIndex[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                 (pAsExtRange->ipAddrMask));
    inParams.Key.pKey = (UINT1 *) au4ExtIndex;
    OSPF_TRC2 (CONTROL_PLANE_TRC, pArea->pOspfCxt->u4OspfCxtId,
               "ipAddr = %x, ipAddrMask = %x\n",
               pAsExtRange->ipAddr, pAsExtRange->ipAddrMask);
    if (TrieAdd (&inParams, (VOID *) pAsExtRange, &outParams) == TRIE_SUCCESS)
    {
        gOsRtr.u4AsExtRouteAggrCnt++;
        return OSPF_SUCCESS;
    }

    OSPF_TRC (OSPF_FN_EXIT, pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: RagInsertRngInAreaLst\n");
    return OSPF_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osragext.c                        */
/*-----------------------------------------------------------------------*/
