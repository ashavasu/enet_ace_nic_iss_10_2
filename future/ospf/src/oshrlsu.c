/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oshrlsu.c,v 1.11 2017/09/21 13:48:46 siva Exp $
 *
 * Description: This file contains procedures for 
 *              maintaining the High performance LSA
 *              retransmission list
 *
 *******************************************************************/

#include "osinc.h"

PRIVATE             tRxmtNode
    * HrLsuGetRxmtNode (tNeighbor * pNbr, tLsaInfo * pLsaInfo);
PRIVATE VOID
 
 
 
 HrLsuDeleteFromRxmtNode (tNeighbor * pNbr, tLsaInfo * pLsaInfo,
                          tRxmtNode * pRxmtNode);
PRIVATE VOID
 
 
 
 HrLsuNbrRxmtIndexDllAdd (tNeighbor * pNbr, tRxmtNode * pRxmtNode,
                          UINT4 u4InsertRxmtIndex);
PRIVATE UINT4       HrLsuNbrRxmtIndexDllGet (tNeighbor * pNbr);
PRIVATE VOID
 
 
 
 HrLsuLsaRxmtIndexDllAdd (tLsaInfo * pLsaInfo, tRxmtNode * pRxmtNode,
                          UINT4 u4InsertRxmtIndex);
PRIVATE UINT4       HrLsuLsaRxmtIndexDllGet (tLsaInfo * pLsaInfo);
PRIVATE VOID        HrLsuSendRxmtNodeAllocFailureEvent (tOspfCxt * pOspfCxt);
/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuAddToRxmtLst                                          */
/*                                                                           */
/* Description  : The specified LSA is added to the link state retransmission*/
/*                list of this neighbor.                                     */
/*                                                                           */
/* Input        : pNbr             : neighbour to which the LSA is to be     */
/*                                    added                                  */
/*                pLsaInfo        : the LSA to be added                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuAddToRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    UINT4               u4TmrInterval;
    UINT4               u4RxmtNodeIndex;
    tRxmtNode          *pRxmtNode = NULL;
    tRxmtNode           RxmtNode;
    tRxmtNode          *pSearchRxmtNode = NULL;

    OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "LSA to be added to rxmt list of Nbr %x.%d \n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr), pNbr->u4NbrAddrlessIf);

    pSearchRxmtNode = &RxmtNode;
    OS_MEM_SET (pSearchRxmtNode, 0, sizeof (tRxmtNode));
    pSearchRxmtNode->pNbr = pNbr;
    pSearchRxmtNode->pLsaInfo = pLsaInfo;

    if ((pRxmtNode = RBTreeGet (gOsRtr.pRxmtLstRBRoot,
                                pSearchRxmtNode)) != NULL)
    {
        /* pLsaInfo is already added in the Rxmt Lst for the Neighbor "pNbr" */
        return;
    }

    /* Allocating Memory for pRxmtNode from the list of Free Memory */
    if ((pRxmtNode =
         (tRxmtNode *) TMO_DLL_Get (&(gOsRtr.rxmtNodeFreeMemLst))) == NULL)
    {
        /* No Free Memory available for adding LSA to Rxmt Lst */
        OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC | OSPF_CRITICAL_TRC,
                   pLsaInfo->pOspfCxt->u4OspfCxtId,
                   "Rxmt node allocation failure for the neighbor  %x.%d \n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);
        /* Send a failure event to OSPF Task to restart OSPF Adjacency */
        HrLsuSendRxmtNodeAllocFailureEvent (pLsaInfo->pOspfCxt);
        return;
    }
    OS_MEM_SET (pRxmtNode, 0, sizeof (tRxmtNode));
    u4RxmtNodeIndex = GET_RXMT_NODE_INDEX (pRxmtNode);
    HrLsuNbrRxmtIndexDllAdd (pNbr, pRxmtNode, u4RxmtNodeIndex);
    HrLsuLsaRxmtIndexDllAdd (pLsaInfo, pRxmtNode, u4RxmtNodeIndex);
    pRxmtNode->pNbr = pNbr;
    pRxmtNode->pLsaInfo = pLsaInfo;

    /* Stores the time (in seconds) at which this LSA was sent out */
    pRxmtNode->u4LastSentTime = OsixGetSysUpTime ();

    if (RBTreeAdd (gOsRtr.pRxmtLstRBRoot, pRxmtNode) == RB_FAILURE)
    {
        HrLsuRxmtNodeMemFree (pRxmtNode);
        OSPF_TRC (OSPF_CRITICAL_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "LSA Addtion to Rxmt List Failure\n");
        return;
    }
    pLsaInfo->u4RxmtCount++;
    pNbr->lsaRxmtDesc.u4LsaRxmtCount++;

    /* The Rxmt timer of neighbohr is being 
       started only if LSA is added on to Rxmt list */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 1)
    {
        u4TmrInterval = NO_OF_TICKS_PER_SEC *
            (pNbr->pInterface->u2RxmtInterval);
        TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer), LSA_RXMT_TIMER,
                     ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ?
                      UtilJitter (u4TmrInterval, OSPF_JITTER) : u4TmrInterval));
    }

    OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "LSA Added To Rxmt Lst Of Nbr %x\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuRxmtLsu                                               */
/*                                                                           */
/* Description  : This procedure is called when the rxmt timer fires for a   */
/*                particular neighbor. This procedure constructs a ls update */
/*                packet from the top of the retransmission list and         */
/*                transmits it to the neighbor.                              */
/*                                                                           */
/* Input        : pNbr            : neighbour to whom the LS update packet   */
/*                                   is to be retransmitted                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuRxmtLsu (tNeighbor * pNbr)
{

    tLsUpdate           lsUpdate;
    tLsaInfo           *pLsaInfo;
    tLsHeader          *pLsHeader;
    tRxmtNode          *pRxmtNode = NULL;
    tLsaInfo           *pGrLsaInfo = NULL;    /* pointer to Grace LSA
                                             * info structure */
    UINT4               u4RxmtIndex;
    UINT4               u4NextRxmtIndex;
    UINT4               u4CurrentTime;
    INT4                i4Counter = 0;

    /* check rxmt count for neighbor */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        return;
    }

    OSPF_NBR_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Retransmission to be done to Nbr %x Address %x\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    lsUpdate.pLsuPkt = NULL;
    LsuClearUpdate (&lsUpdate);
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX, u4RxmtIndex);

    if (u4RxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        return;
    }
    /* Get the current sys time in seconds */
    u4CurrentTime = OsixGetSysUpTime ();
    do
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        OSPF_GET_DLL_INDEX_IN_RXMT_NODE
            (pRxmtNode->au1RxmtInfo, NBR_NEXT_DLL_INDEX, u4NextRxmtIndex);
        pLsaInfo = pRxmtNode->pLsaInfo;

        /* Grace LSA should be transmitted twice to each neighbor.
         * If no ack has been recieved for this LSA, the process
         * switched to graceful restart mode */
        if ((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
            (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
        {
            if ((pNbr->pInterface->pArea->pOspfCxt->u1RestartStatus
                 == OSPF_RESTART_UNPLANNED) && (pNbr->u1GraceLsaTxCount > 0))
            {
                pGrLsaInfo = pLsaInfo;
                /* This LSA has been re-tranmitted once */
                continue;
            }
            else if (pNbr->u1GraceLsaTxCount >=
                     pNbr->pInterface->pArea->pOspfCxt->u1GrLsaMaxTxCount)
            {
                pGrLsaInfo = pLsaInfo;
                continue;
            }
            pNbr->u1GraceLsaTxCount++;
        }

        if (lsUpdate.pLsuPkt == NULL)
        {
            pLsHeader = (tLsHeader *) (pLsaInfo->pLsa);
            if (IS_VIRTUAL_IFACE (pNbr->pInterface))
            {
                VifSendRxmtTrap (pNbr, pLsHeader, LS_UPDATE_PKT);
            }
            else
            {
                IfSendRxmtTrap (pNbr, pLsHeader, LS_UPDATE_PKT);
            }
        }

        if ((u4CurrentTime - pRxmtNode->u4LastSentTime) >
            (UINT4) (pNbr->pInterface->u2RxmtInterval / 2))
        {
            if (LsuAddToLsu (pNbr, pLsaInfo, &lsUpdate, TX_ONCE) == LSU_TXED)
            {
                i4Counter++;
                if (i4Counter == MAX_OSPF_TX_RXMT_LS_UPDATES)
                {
                    break;
                }
            }
            /* update the LSA retransmitted time */
            pRxmtNode->u4LastSentTime = OsixGetSysUpTime ();
        }
        u4RxmtIndex = u4NextRxmtIndex;
    }
    while (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX);

    /* The above loop might end without the update packet being transmitted */
    LsuSendLsu (pNbr, &lsUpdate);

    /* If pGrLsaInfo is not NULL, then the grace LSA to be re-transmitted
     * to this neighbor has reached the threshold */
    if (pGrLsaInfo != NULL)
    {
        GrDeleteRxmtLst (pNbr, pGrLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuDeleteFromRxmtLst                                     */
/*                                                                           */
/* Description  : The specified LSA is removed from the neighbor's           */
/*                retransmission list. If the LSA is no longer present in any*/
/*                retransmission list then it is removed from the doubly     */
/*                linked list containing all LSAs to be retransmitted. The   */
/*                rxmt_count field is updated and seq_num_wrap_around is     */
/*                verified.                                                  */
/*                                                                           */
/* Input        : pNbr             : neighbour from which the LSA is to be   */
/*                                    deleted                                */
/*                pLsaInfo        : the LSA to be deleted                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuDeleteFromRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    UINT4               u4OspfCxtId;
    tRxmtNode          *pRxmtNode = NULL;
    tRxmtNode           RxmtNode;
    tRxmtNode          *pSearchRxmtNode = NULL;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;

    OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, u4OspfCxtId,
               "LSA To Be Deleted From Rxmt Lst Of Nbr %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr), pNbr->u4NbrAddrlessIf);

    if (pLsaInfo->u4RxmtCount == 0)
    {
        return;
    }
    pSearchRxmtNode = &RxmtNode;
    OS_MEM_SET (pSearchRxmtNode, 0, sizeof (tRxmtNode));
    pSearchRxmtNode->pNbr = pNbr;
    pSearchRxmtNode->pLsaInfo = pLsaInfo;
    if ((pRxmtNode = RBTreeGet (gOsRtr.pRxmtLstRBRoot,
                                pSearchRxmtNode)) != NULL)
    {
        HrLsuLsaRxmtIndexDllDel (pLsaInfo, pRxmtNode);
        HrLsuNbrRxmtIndexDllDel (pNbr, pRxmtNode);
        /* Remove the rxmt node from the RB Tree */

        RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);
        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;

        /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
        {
            TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        }
        if (--pLsaInfo->u4RxmtCount == 0)
        {
            OspfRmSendLsAckInfo (pLsaInfo);
            /* if advt age is MAX_AGE remove it from database */
            if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
            {
                LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
            }
        }
        /* if lsa is not present in any rxmt list then delete from linked list */
        HrLsuRxmtNodeMemFree (pRxmtNode);
    }
    else
    {
        OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                   u4OspfCxtId,
                   "LSA Not Present In Rxmt Lst Of Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

        /* pLsaInfo is not there in the rxmt list for this neighbor. */
        return;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuDeleteNodeFromAllRxmtLst                              */
/*                                                                           */
/* Description  : The specified LSA is removed from all neighbor's rxmt      */
/*                lists. The LSA is also removed from the doubly linked list */
/*                containing LSAs to be retransmitted.                       */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
HrLsuDeleteNodeFromAllRxmtLst (tLsaInfo * pLsaInfo)
{

    tNeighbor          *pNbr;
    tRxmtNode          *pRxmtNode = NULL;
    UINT4               u4RxmtIndex;

    OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "Delete LSA Type %s linkStateId %x adv rtr id  %x area id  %x\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType],
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
               (pLsaInfo->pArea == NULL ? 0 :
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->areaId)));

    while ((u4RxmtIndex = HrLsuLsaRxmtIndexDllGet (pLsaInfo))
           != OSPF_INVALID_RXMT_INDEX)
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        pNbr = pRxmtNode->pNbr;

        HrLsuNbrRxmtIndexDllDel (pNbr, pRxmtNode);
        pLsaInfo->u4RxmtCount--;
        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;
        /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
        {
            TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        }
        RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);

        HrLsuRxmtNodeMemFree (pRxmtNode);
    }
    pLsaInfo->u4RxmtCount = 0;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuClearRxmtLst                                          */
/*                                                                           */
/* Description  : This procedure clears the retransmission list associated   */
/*                with this neighbor.                                        */
/*                                                                           */
/* Input        : pNbr            : neighbour whose rxmt list has to be      */
/*                                   cleared                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuClearRxmtLst (tNeighbor * pNbr)
{
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_DLL_NODE      *pLsaNode = NULL;
    UINT4               u4OspfCxtId;
    UINT4               u4RxmtIndex;
    tRxmtNode          *pRxmtNode = NULL;

    u4OspfCxtId = pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId;

    OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, u4OspfCxtId,
               "Clearing the Rxmt List for the  Nbr %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr), pNbr->u4NbrAddrlessIf);

    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        return;
    }

    while ((u4RxmtIndex = HrLsuNbrRxmtIndexDllGet (pNbr))
           != OSPF_INVALID_RXMT_INDEX)
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        pLsaInfo = pRxmtNode->pLsaInfo;
        HrLsuLsaRxmtIndexDllDel (pLsaInfo, pRxmtNode);
        RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);
        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;
        if (--pLsaInfo->u4RxmtCount == 0)
        {
            /* if advt age is MAX_AGE remove it from database */
            if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
            {
                LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
            }
        }
        HrLsuRxmtNodeMemFree (pRxmtNode);
        pLsaNode = NULL;
    }
    pNbr->lsaRxmtDesc.u4LsaRxmtCount = 0;
    TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuDeleteFromAllRxmtLst                                  */
/*                                                                           */
/* Description  : The specified LSA is removed from all neighbor's rxmt      */
/*                lists. The LSA is also removed from the doubly linked list */
/*                containing LSAs to be retransmitted.                       */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuDeleteFromAllRxmtLst (tLsaInfo * pLsaInfo)
{

    tOspfCxt           *pOspfCxt = pLsaInfo->pOspfCxt;
    tNeighbor          *pNbr = NULL;
    tRxmtNode          *pRxmtNode = NULL;
    UINT4               u4RxmtIndex;

    OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pOspfCxt->u4OspfCxtId,
               "Delete LSA Type %s linkStateId %x adv rtr id  %x area id  %x\n",
               au1DbgLsaType[pLsaInfo->lsaId.u1LsaType],
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
               (pLsaInfo->pArea == NULL ? 0 :
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->areaId)));

    while ((u4RxmtIndex = HrLsuLsaRxmtIndexDllGet (pLsaInfo))
           != OSPF_INVALID_RXMT_INDEX)
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        pNbr = pRxmtNode->pNbr;
        HrLsuNbrRxmtIndexDllDel (pNbr, pRxmtNode);
        RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);
        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;
        /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
        {
            TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        }

        HrLsuRxmtNodeMemFree (pRxmtNode);

        if (--pLsaInfo->u4RxmtCount == 0)
        {
            /* if advt age is MAX_AGE remove it from database */
            if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
            {
                LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
                return;
            }
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuGetRxmtNode                                           */
/*                                                                           */
/* Description  : This funnction retrieves the retransmission node           */
/*                containing the specified LSA to the specified neighnor     */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                pNbr              : pointer to the Neighbor                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to tRxmtNode                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE tRxmtNode  *
HrLsuGetRxmtNode (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    UINT4               u4OspfCxtId;
    tRxmtNode          *pRxmtNode = NULL;
    tRxmtNode           RxmtNode;
    tRxmtNode          *pSearchRxmtNode = NULL;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;

    OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, u4OspfCxtId,
               "LSA Get the RxmtNode for Nbr %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr), pNbr->u4NbrAddrlessIf);

    pSearchRxmtNode = &RxmtNode;
    OS_MEM_SET (pSearchRxmtNode, 0, sizeof (tRxmtNode));
    pSearchRxmtNode->pNbr = pNbr;
    pSearchRxmtNode->pLsaInfo = pLsaInfo;
    pRxmtNode = RBTreeGet (gOsRtr.pRxmtLstRBRoot, pSearchRxmtNode);
    return pRxmtNode;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuDeleteFromRxmtNode                                    */
/*                                                                           */
/* Description  : This funnction deletes the retransmission node             */
/*                from the neighbor DLL, LSA DLL and RBTree. The node is     */
/*                added to the free retransmission DLL                       */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*                pLsaInfo          : pointer to the advertisement           */
/*                pRxmtNode         : Retransmission node to be deleted      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HrLsuDeleteFromRxmtNode (tNeighbor * pNbr, tLsaInfo * pLsaInfo,
                         tRxmtNode * pRxmtNode)
{
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;

    OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, u4OspfCxtId,
               "LSA To Be Deleted From Rxmt Lst Of Nbr %x.%d\n",
               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr), pNbr->u4NbrAddrlessIf);

    HrLsuLsaRxmtIndexDllDel (pLsaInfo, pRxmtNode);
    HrLsuNbrRxmtIndexDllDel (pNbr, pRxmtNode);
    RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);
    pNbr->lsaRxmtDesc.u4LsaRxmtCount--;

    /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
    }
    if (--pLsaInfo->u4RxmtCount == 0)
    {
        /* if advt age is MAX_AGE remove it from database */
        if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
        {
            LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
        }
    }
    /* if lsa is not present in any rxmt list then delete from linked list */

    HrLsuRxmtNodeMemFree (pRxmtNode);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuFindAndProcessAckFlag                                 */
/*                                                                           */
/* Description  : This funnction processes the LSA ack. If the node is       */
/*                for the LSA and Nbr is present in the retransmission       */
/*                list, it is removed from all the lists.                    */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*                pLsaInfo          : pointer to the advertisement           */
/*                pLsa              : pointer to the LSA packet              */
/*                pRcvdLsHeader     : receiver LSA header                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to tRxmtNode                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuFindAndProcessAckFlag (tNeighbor * pNbr, tLsaInfo * pLsaInfo,
                            UINT1 *pLsa, tLsHeader * pRcvdLsHeader)
{
    tRxmtNode          *pRxmtNode = NULL;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LsuFindAndProcessAckFlag\n");

    if ((pRxmtNode = HrLsuGetRxmtNode (pNbr, pLsaInfo)) != NULL)
    {
        OSPF_TRC3 (OSPF_LSU_TRC,
                   pNbr->pInterface->pArea->pOspfCxt->
                   u4OspfCxtId,
                   "Dup LSA Rcvd And Treated As Implied ACK"
                   "Nbr %x LSType %s LSId %x \n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                   au1DbgLsaType[pRcvdLsHeader->u1LsaType],
                   OSPF_CRU_BMC_DWFROMPDU (pRcvdLsHeader->linkStateId));

        HrLsuDeleteFromRxmtNode (pNbr, pLsaInfo, pRxmtNode);

        LsuProcessAckFlag (pNbr, pLsa, IMPLIED_ACK);
    }
    else
    {
        OSPF_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                   pNbr->pInterface->pArea->pOspfCxt->
                   u4OspfCxtId,
                   "Dup LSA Rcvd And Not Treated As Implied ACK"
                   "Nbr %x LSType %s LSId %x \n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                   au1DbgLsaType[pRcvdLsHeader->u1LsaType],
                   OSPF_CRU_BMC_DWFROMPDU (pRcvdLsHeader->linkStateId));

        LsuProcessAckFlag (pNbr, pLsa, NO_IMPLIED_ACK);
    }
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LsuFindAndProcessAckFlag\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuNbrRxmtIndexDllAdd                                    */
/*                                                                           */
/* Description  : This funnction adds the retransimission node in            */
/*                neighbor DLL.                                              */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*                pRxmtNode         : Node to be added                       */
/*                u4InsertRxmtIndex : Index of the global rxmt Node array    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HrLsuNbrRxmtIndexDllAdd (tNeighbor * pNbr, tRxmtNode * pRxmtNode,
                         UINT4 u4InsertRxmtIndex)
{
    UINT4               u4NextRxmtIndex;
    UINT4               u4PrevRxmtIndex;
    tRxmtNode          *pPrevRxmtNode = NULL;

    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX, u4NextRxmtIndex);
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pNbr->au1RxmtInfo, NBR_HEAD_PREV_DLL_INDEX, u4PrevRxmtIndex);
    if (u4NextRxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        /* This is the first Rxmt Node added to Nbr List */
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX, u4InsertRxmtIndex);
    }
    else
    {
        pPrevRxmtNode = &(gOsRtr.aRxmtNode[u4PrevRxmtIndex]);
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pPrevRxmtNode->au1RxmtInfo, NBR_NEXT_DLL_INDEX, u4InsertRxmtIndex);
    }
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE
        (pNbr->au1RxmtInfo, NBR_HEAD_PREV_DLL_INDEX, u4InsertRxmtIndex);
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, NBR_NEXT_DLL_INDEX, OSPF_INVALID_RXMT_INDEX);
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, NBR_PREV_DLL_INDEX, u4PrevRxmtIndex);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuNbrRxmtIndexDllDel                                    */
/*                                                                           */
/* Description  : This funnction deleted the retarnsmission node from        */
/*                the neighbor DLL                                           */
/*                                                                           */
/* Input       : pNbr              : pointer to the Neighbor                 */
/*                pRxmtNode         : Node to be deleted                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to tRxmtNode                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuNbrRxmtIndexDllDel (tNeighbor * pNbr, tRxmtNode * pRxmtNode)
{
    UINT4               u4NextRxmtIndex;
    UINT4               u4PrevRxmtIndex;
    tRxmtNode          *pPrevRxmtNode = NULL;
    tRxmtNode          *pNextRxmtNode = NULL;

    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, NBR_NEXT_DLL_INDEX, u4NextRxmtIndex);
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, NBR_PREV_DLL_INDEX, u4PrevRxmtIndex);

    if (u4PrevRxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        /* This is the first Rxmt Node in Nbr List
         * so update the Start Node index in pNbr
         */
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX, u4NextRxmtIndex);
    }
    else
    {
        pPrevRxmtNode = &(gOsRtr.aRxmtNode[u4PrevRxmtIndex]);
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pPrevRxmtNode->au1RxmtInfo, NBR_NEXT_DLL_INDEX, u4NextRxmtIndex);
    }

    if (u4NextRxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        /* End of List so update the Last node index in pNbr */
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pNbr->au1RxmtInfo, NBR_HEAD_PREV_DLL_INDEX, u4PrevRxmtIndex);

    }
    else
    {
        pNextRxmtNode = &(gOsRtr.aRxmtNode[u4NextRxmtIndex]);
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pNextRxmtNode->au1RxmtInfo, NBR_PREV_DLL_INDEX, u4PrevRxmtIndex);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuNbrRxmtIndexDllGet                                    */
/*                                                                           */
/* Description  : This funnction deleted the first retransmission node       */
/*                from the neighbor DLL and returns the Index.               */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Array Index of the global rxmt array                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
HrLsuNbrRxmtIndexDllGet (tNeighbor * pNbr)
{
    UINT4               u4RxmtIndex;
    tRxmtNode          *pRxmtNode = NULL;

    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX, u4RxmtIndex);
    if (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX)
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        /* Delete u4RxmtIndex fromthe Nbr DLL List */
        HrLsuNbrRxmtIndexDllDel (pNbr, pRxmtNode);

    }
    return (u4RxmtIndex);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuLsaRxmtIndexDllAdd                                    */
/*                                                                           */
/* Description  : This funnction adds the retransimission node in            */
/*                LsaInfo DLL.                                               */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                pRxmtNode         : Node to be added                       */
/*                u4InsertRxmtIndex : Index of the global rxmt Node array    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
HrLsuLsaRxmtIndexDllAdd (tLsaInfo * pLsaInfo, tRxmtNode * pRxmtNode,
                         UINT4 u4InsertRxmtIndex)
{
    UINT4               u4NextRxmtIndex;
    UINT4               u4PrevRxmtIndex;
    tRxmtNode          *pPrevRxmtNode = NULL;

    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pLsaInfo->au1RxmtInfo, LSA_HEAD_NEXT_DLL_INDEX, u4NextRxmtIndex);
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pLsaInfo->au1RxmtInfo, LSA_HEAD_PREV_DLL_INDEX, u4PrevRxmtIndex);
    if (u4NextRxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        /* This is the first Rxmt Node added to LsaInfo List */
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pLsaInfo->au1RxmtInfo, LSA_HEAD_NEXT_DLL_INDEX, u4InsertRxmtIndex);
    }
    else
    {
        pPrevRxmtNode = &(gOsRtr.aRxmtNode[u4PrevRxmtIndex]);
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pPrevRxmtNode->au1RxmtInfo, LSA_NEXT_DLL_INDEX, u4InsertRxmtIndex);
    }
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE
        (pLsaInfo->au1RxmtInfo, LSA_HEAD_PREV_DLL_INDEX, u4InsertRxmtIndex);
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, LSA_NEXT_DLL_INDEX, OSPF_INVALID_RXMT_INDEX);
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, LSA_PREV_DLL_INDEX, u4PrevRxmtIndex);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuLsaRxmtIndexDllDel                                    */
/*                                                                           */
/* Description  : This funnction deletes the retransimission node in         */
/*                LsaInfo DLL.                                               */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                pRxmtNode         : Node to be added                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuLsaRxmtIndexDllDel (tLsaInfo * pLsaInfo, tRxmtNode * pRxmtNode)
{
    UINT4               u4NextRxmtIndex;
    UINT4               u4PrevRxmtIndex;
    tRxmtNode          *pPrevRxmtNode = NULL;
    tRxmtNode          *pNextRxmtNode = NULL;

    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, LSA_NEXT_DLL_INDEX, u4NextRxmtIndex);
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pRxmtNode->au1RxmtInfo, LSA_PREV_DLL_INDEX, u4PrevRxmtIndex);

    if (u4PrevRxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        /* This is the first Rxmt Node in Nbr List
         * so update the Start Node index in pNbr
         */
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pLsaInfo->au1RxmtInfo, LSA_HEAD_NEXT_DLL_INDEX, u4NextRxmtIndex);
    }
    else
    {
        pPrevRxmtNode = &(gOsRtr.aRxmtNode[u4PrevRxmtIndex]);
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pPrevRxmtNode->au1RxmtInfo, LSA_NEXT_DLL_INDEX, u4NextRxmtIndex);
    }

    if (u4NextRxmtIndex == OSPF_INVALID_RXMT_INDEX)
    {
        /* End of List so update the Last node index in pNbr */
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pLsaInfo->au1RxmtInfo, LSA_HEAD_PREV_DLL_INDEX, u4PrevRxmtIndex);

    }
    else
    {
        pNextRxmtNode = &(gOsRtr.aRxmtNode[u4NextRxmtIndex]);
        OSPF_SET_DLL_INDEX_IN_RXMT_NODE
            (pNextRxmtNode->au1RxmtInfo, LSA_PREV_DLL_INDEX, u4PrevRxmtIndex);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuLsaRxmtIndexDllGet                                   */
/*                                                                           */
/* Description  : This funnction deleted the first retransmission node       */
/*                from the LsaINfo DLL and returns the Index.                */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Array Index of the global rxmt array                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
HrLsuLsaRxmtIndexDllGet (tLsaInfo * pLsaInfo)
{
    UINT4               u4RxmtIndex;
    tRxmtNode          *pRxmtNode = NULL;

    OSPF_GET_DLL_INDEX_IN_RXMT_NODE
        (pLsaInfo->au1RxmtInfo, LSA_HEAD_NEXT_DLL_INDEX, u4RxmtIndex);
    if (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX)
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        /* Delete u4RxmtIndex fromthe LsaInfo DLL List */
        HrLsuLsaRxmtIndexDllDel (pLsaInfo, pRxmtNode);

    }
    return (u4RxmtIndex);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuRxmtNodeMemFree                                       */
/*                                                                           */
/* Description  : This funnction clears the retransmission node and          */
/*                adds it to the global free rxmt node list                  */
/*                                                                           */
/* Input        : pRxmtNode         : pointer to the rxmt node               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuRxmtNodeMemFree (tRxmtNode * pRxmtNode)
{
    OS_MEM_SET (pRxmtNode, 0, sizeof (tRxmtNode));
    TMO_DLL_Add (&(gOsRtr.rxmtNodeFreeMemLst), (tTMO_DLL_NODE *) pRxmtNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuCheckAndDelLsaFromRxmtLst                               */
/*                                                                           */
/* Description  : This funnction clears the retransmission node if LSA Ack   */
/*                is pending for the LSA pLsaInfo from the neighbor pNbr     */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*              : pLsaInfo          : pointer to the LsaInfo                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
HrLsuCheckAndDelLsaFromRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    LsuDeleteFromRxmtLst (pNbr, pLsaInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuCheckInRxmtLst                                        */
/*                                                                           */
/* Description  : This funnction find the LSA is there in RXMT list or not   */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*              : pLsaInfo          : pointer to the LsaInfo                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
HrLsuCheckInRxmtLst (tNeighbor * pNbr, tLsaInfo * pLsaInfo)
{
    UINT4               u4OspfCxtId = OSPF_ZERO;
    tRxmtNode          *pRxmtNode = NULL;
    tRxmtNode          *pSearchRxmtNode = NULL;
    tRxmtNode           RxmtNode;

    u4OspfCxtId = pLsaInfo->pOspfCxt->u4OspfCxtId;

    if (pLsaInfo->u4RxmtCount == 0)
    {
        return OSPF_ZERO;
    }

    pSearchRxmtNode = &RxmtNode;
    OS_MEM_SET (pSearchRxmtNode, 0, sizeof (tRxmtNode));
    pSearchRxmtNode->pNbr = pNbr;
    pSearchRxmtNode->pLsaInfo = pLsaInfo;

    if ((pRxmtNode = RBTreeGet (gOsRtr.pRxmtLstRBRoot,
                                pSearchRxmtNode)) != NULL)
    {
        return OSPF_ONE;
    }
    return OSPF_ZERO;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : HrLsuCheckAndDelLsaFromRxmtLst                             */
/*                                                                           */
/* Description  : This funnction Initialises the NBR retransmission DLL Head */
/*                LSA retransmission bit map                                 */
/*                                                                           */
/* Input        : pNbr              : pointer to the Neighbor                */
/*              : pLsaInfo          : pointer to the LsaInfo                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
HrLsuInitialiseLsaRxmtInfo (tLsaInfo * pLsaInfo)
{

    OSPF_SET_DLL_INDEX_IN_RXMT_NODE (pLsaInfo->au1RxmtInfo,
                                     LSA_HEAD_PREV_DLL_INDEX,
                                     OSPF_INVALID_RXMT_INDEX);
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE (pLsaInfo->au1RxmtInfo,
                                     LSA_HEAD_NEXT_DLL_INDEX,
                                     OSPF_INVALID_RXMT_INDEX);
}

PRIVATE VOID
HrLsuSendRxmtNodeAllocFailureEvent (tOspfCxt * pOspfCxt)
{
    tOspfQMsg          *pOspfQMsg = NULL;
    UINT4               u4CurrentTime = 0;

    /* Get the current sys time in seconds */
    u4CurrentTime = OsixGetSysUpTime ();
    if ((u4CurrentTime - pOspfCxt->u4LastRxmtFailEventTime) <
        OSPF_RXMT_FAIL_INTERVAL)
    {
        return;
    }
    pOspfCxt->u4LastRxmtFailEventTime = u4CurrentTime;
    OSPF_TRC (OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
              "Sending RxmtNode memory alloc failure MSG to OSPF Task\n");

    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        OSPF_TRC (OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Memory Allocation for pOspfQMsg Failure\n");
        return;
    }

    pOspfQMsg->u4MsgType = OSPF_RXMT_NODE_ALLOC_FAIL_MSG;
    pOspfQMsg->u4OspfCxtId = pOspfCxt->u4OspfCxtId;

    if (OsixSendToQ ((UINT4) NULL, (const UINT1 *) "OSPQ",
                     (tCRU_BUF_CHAIN_HEADER *) pOspfQMsg, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        OSPF_TRC (OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Ospf Sendto Q Failure\n");
        QMSG_FREE (pOspfQMsg);
        return;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_TRC (OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "Ospf Sendto Evt Failure\n");
    }
}
