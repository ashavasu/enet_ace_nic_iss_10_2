/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oslsu.c,v 1.54 2018/01/09 11:02:36 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             flooding process i.e., sending and receiving of
 *             link state update pkts. This file also contains
 *             procedures for maintaining the rxmt list.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID LsuSendFloodUpdate PROTO ((tInterface * pInterface));

PRIVATE VOID LsuCheckAndFlushIndicationLsa PROTO ((tArea * pArea));

PRIVATE UINT1 LsuFindRecentLsa PROTO ((tNeighbor * pNbr, tLsaInfo * pLsaInfo,
                                       tLSASEQNUM seqNum1, UINT2 age1,
                                       UINT2 chksum1, tLSASEQNUM seqNum2,
                                       UINT2 age2, UINT2 chksum2));

PRIVATE UINT1 LsuCompLsaContents PROTO ((tLsaInfo * pLsaInfo, UINT1 *pRcvLsa));

PRIVATE UINT4 LsuLsaHashFunc PROTO ((UINT1 u1LsaType,
                                     tLINKSTATEID * pLinkStateId,
                                     tRouterId * pAdvRtrId));
PRIVATE VOID LsuAddToSortLsaLst PROTO ((tLsaInfo * pLsaInfo));
PRIVATE VOID        LsuAddToFloodUpdate
PROTO ((tInterface * pInterface, UINT1 *pLsa, UINT2 u2LsaLen));
PRIVATE VOID        LsuFillLsaInfoToOpqApp (tLsaInfo * pLsaInfo,
                                            tAppInfo * pAppInfo,
                                            UINT1 u1LsaStatus);

PRIVATE UINT1
      LsuAddOrDelLsaInRBTree (tLsaInfo * pLsaInfo, UINT1 u1AddDelFlag);

PRIVATE VOID
 
 
 
 LsuDeletePrefixLsaInHashLst (tTMO_HASH_TABLE * pLsaHashTable,
                              tLsaInfo * pLsaInfo);

PRIVATE VOID        LsuAddPrefixLsaInHashLst (tTMO_HASH_TABLE * pLsaHashTable,
                                              tLsaInfo * pLsaInfo, UINT1 *pLsa);
/* 
 * The Macro LSU_FIND_RECENT_HDR calls the function LsuFindRecentLsa
 * for finding the most recent header .
 */
#define LSU_FIND_RECENT_HDR(pNbr,pLsaInfo,p_hdr1,p_hdr2) \
        LsuFindRecentLsa (pNbr,pLsaInfo,(p_hdr1)->lsaSeqNum, (p_hdr1)->u2LsaAge, \
                            (p_hdr1)->u2LsaChksum, (p_hdr2)->lsaSeqNum, \
                            (p_hdr2)->u2LsaAge, (p_hdr2)->u2LsaChksum)

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuRcvLsUpdate                                          */
/*                                                                           */
/* Description  : This procedure takes care of processing a received LSU     */
/*                packet. For each LSA in the packet its checksum and type   */
/*                are validated. If this LSA is a newer than that in the     */
/*                database then it is installed in the database and flooded  */
/*                out on certain interfaces.                                 */
/*                                                                           */
/* Input        : pLsUpdate : the LSA update packet                        */
/*                pNbr       : neighbour from whom the LSA packet is        */
/*                              received                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuRcvLsUpdate (tCRU_BUF_CHAIN_HEADER * pLsUpdate, tNeighbor * pNbr)
{
    INT1                i1RetCode;
    UINT1              *pLsa;
    UINT2               u2LsaLen;
    UINT2               u2NextLsaStart = 0;
    UINT4               u4LsaCount;
    UINT1               u1LsaType = 0;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LsuRcvLsUpdate\n");
    if (pNbr->u1NsmState < MAX_NBR_STATE)
    {
        OSPF_NBR_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "LSU Rcvd Nbr %x.%d NSM State %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf, au1DbgNbrState[pNbr->u1NsmState]);
    }

    if (pNbr->u1NsmState < NBRS_EXCHANGE)
    {

        OSPF_NBR_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "LSU Disc Nbr %x.%d NSM State %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf, au1DbgNbrState[pNbr->u1NsmState]);

        INC_DISCARD_LSU_CNT (pNbr->pInterface);
        return;
    }

    /* skip OSPF header */

    u2NextLsaStart = OS_HEADER_SIZE + LSU_FIXED_PORTION_SIZE;
    OSPF_CRU_BMC_GET_4_BYTE (pLsUpdate, OS_HEADER_SIZE, u4LsaCount);

    while (u4LsaCount--)
    {

        /* get length of next lsa */

        OSPF_CRU_BMC_GET_2_BYTE (pLsUpdate, (u2NextLsaStart +
                                             LENGTH_OFFSET_IN_LS_HEADER),
                                 u2LsaLen);
        OSPF_CRU_BMC_GET_1_BYTE (pLsUpdate, (u2NextLsaStart +
                                             TYPE_OFFSET_IN_LS_HEADER),
                                 u1LsaType);
        /* extract lsa */
        if (u1LsaType == ROUTER_LSA)
        {
            LSA_ALLOC (u1LsaType, pLsa, u2LsaLen);
        }
        else
        {
            if (u2LsaLen > MAX_LSA_SIZE)
            {
                OSPF_TRC1 (OS_RESOURCE_TRC | OSPF_ADJACENCY_TRC,
                           pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "LSA Alloc Failure for Size %d\n", u2LsaLen);
                break;
            }
            LSA_ALLOC (u1LsaType, pLsa, u2LsaLen);
        }
        if (pLsa == NULL)
        {
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gi4OspfSysLogId,
                          "LSA Alloc Failure"));

            OSPF_NBR_TRC1 (OSPF_CRITICAL_TRC | OS_RESOURCE_TRC |
                           OSPF_ADJACENCY_TRC, pNbr,
                           pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "LSA Alloc Failure for Size %d\n", u2LsaLen);
            break;
        }
        OSPF_CRU_BMC_GET_STRING (pLsUpdate, pLsa, u2NextLsaStart, u2LsaLen);

        /* process lsa contents */

        if ((i1RetCode = LsuProcessLsa (pLsa, u2LsaLen, pNbr)) == OSPF_FAILURE)
        {

            LSA_FREE (u1LsaType, pLsa);

            OSPF_NBR_TRC (ALL_FAILURE_TRC | OSPF_ADJACENCY_TRC,
                          pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "LSU Processing Stopped\n");

            break;
        }
        if (i1RetCode == DISCARDED)
        {
            LSA_FREE (u1LsaType, pLsa);
        }

        u2NextLsaStart += u2LsaLen;
    }

    /* 
     * transmit the delayed ack constructed during the processing of the
     * received ls update packet 
     */
    LakSendDelayedAck (pNbr->pInterface);

    /* send the ls update pkts constructed on various interfaces */
    LsuSendAllFloodUpdatesInCxt (pNbr->pInterface->pArea->pOspfCxt);

    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: LsuRcvLsUpdate\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuSendAllFloodUpdates                                 */
/*                                                                           */
/* Description  : This routine floods all the LS update packets.             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuSendAllFloodUpdatesInCxt (tOspfCxt * pOspfCxt)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;
    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {

        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {

            pInterface = GET_IF_PTR_FROM_LST (pLstNode);

            LsuSendFloodUpdate (pInterface);
        }
    }

    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
              "LSU Txmission Over\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuProcessLsa                                            */
/*                                                                           */
/* Description  : Reference : RFC-1247 section 13.                           */
/*                This procedure takes care of the processing of a lsa       */
/*                received in a link state update packet. This Process       */
/*                returns FAILURE when processing of the lsu packet is to be */
/*                stopped.                                                   */
/*                                                                           */
/* Input        : pLsa             : the LSA packet to be processed         */
/*                u2Len            : length of the packet                   */
/*                pNbr             : neighbour who sent this LSA            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if successfully processed                         */
/*                DISCARDED, otherwise                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
LsuProcessLsa (UINT1 *pLsa, UINT2 u2Len, tNeighbor * pNbr)
{
    tOsixSysTime        u4SysTime;
    tExtLsdbOverflowTrapInfo trapElsdbOverflow;
    tLsHeader           lsHeader;
    UINT1               ackFlag = UNUSED_ACK_FLAG;
    tLsaInfo           *pLsaInfo;
    tLsaInfo           *pNewLsaInfo = NULL;
    tExtLsaLink         extLsaLink;
    tExtLsaLink         oldExtLsaLink;
    tLsaReqNode        *pLsaReqNode;
    tLsUpdate           lsUpdate;
    UINT1               u1LsaChngdFlag = OSPF_TRUE;
    tLsaInfo           *pLsaInfoAux;
    UINT4               lsaMask;
    tIPADDR             linkMask;
    INT1                i1RetVal = SUCCESS;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tInterface         *pInterface = NULL;

    tNeighbor          *pNeighbor = NULL;
    UINT2               u2LsaAge = OSPF_ZERO;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LsuProcessLsa\n");
    if ((pNbr->u1NsmState < MAX_NBR_STATE)
        && (pNbr->pInterface->u1IsmState < MAX_IF_STATE))
    {
        OSPF_NBR_TRC5 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "Processing LSA Len %d Nbr %x.%d NSM State %s If State %s\n",
                       u2Len,
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf,
                       au1DbgNbrState[pNbr->u1NsmState],
                       au1DbgIfState[pNbr->pInterface->u1IsmState]);
    }

    /* If the instance is in graceful shutdown process, do not process
     * the LSA */
    if ((pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState
         == OSPF_GR_SHUTDOWN) || (pLsa == NULL))
    {
        return OSPF_FAILURE;
    }
    /* extract ls header info */

    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    /* 
     * is lsa is dna lsa & area is not able to process dna lsas, 
     * discard the lsa 
     */
    if ((lsHeader.u2LsaAge & DO_NOT_AGE) &&
        !IS_DNA_LSA_PROCESS_CAPABLE (pNbr->pInterface->pArea))
    {

        OSPF_NBR_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Not Able To Process DNA LSAs\n");

        return DISCARDED;
    }

    /* validate ls type (step.1) */
    if ((lsHeader.u1LsaType < 1) || (lsHeader.u1LsaType > MAX_LSA_TYPE))
    {

        OSPF_NBR_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Invalid LSA Type\n");

        return DISCARDED;
    }

    /* validate checksum (step.2) */
    if (UtilVerifyLsaFletChksum (pLsa, u2Len) == OSPF_FALSE)
    {

        OSPF_NBR_TRC5 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "LSA Disc ChkSum Failure Nbr %x.%d LSType %s LSId %x"
                       " AdvRtrId %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf,
                       au1DbgLsaType[lsHeader.u1LsaType],
                       OSPF_CRU_BMC_DWFROMPDU (lsHeader.linkStateId),
                       OSPF_CRU_BMC_DWFROMPDU (lsHeader.advRtrId));

        return DISCARDED;
    }

    if ((IS_OPQ_LSA (lsHeader.u1LsaType))
        && (pNbr->pInterface->pArea->pOspfCxt->u1OpqCapableRtr == OSPF_FALSE))
    {
        return DISCARDED;
    }

    /* validate ls type (step.3) */
    if (((lsHeader.u1LsaType == AS_EXT_LSA)
         || (lsHeader.u1LsaType == TYPE11_OPQ_LSA))
        && (pNbr->pInterface->pArea->u4AreaType != NORMAL_AREA))
    {

        OSPF_NBR_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Ext LSA OR Type 11 LSA Into Stub Area\n");

        return DISCARDED;
    }

    /* A type 7 LSA is not to be received in a Stub area, and in a regular area */
    if ((lsHeader.u1LsaType == NSSA_LSA) &&
        ((pNbr->pInterface->pArea->u4AreaType == STUB_AREA) ||
         (pNbr->pInterface->pArea->u4AreaType == NORMAL_AREA)))
    {

        OSPF_NBR_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Err: NSSA LSA Into Stub Area or Regular area\n");

        return DISCARDED;
    }
    if ((lsHeader.u1LsaType == ROUTER_LSA)
        && (UtilIpAddrComp (lsHeader.linkStateId, lsHeader.advRtrId) != 0))
    {
        OSPF_NBR_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Err: Link State Id is not equal to Advt Router Id\n");
        return DISCARDED;
    }
    /*LSA with Reserved seq number cannot be processed */
    if (lsHeader.lsaSeqNum == (INT4) RESERVED_SEQ_NUM)
    {

        OSPF_NBR_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Reserved LSA seq Number Received\n");

        return DISCARDED;
    }

    pLsaInfo = LsuSearchDatabase (lsHeader.u1LsaType,
                                  &(lsHeader.linkStateId),
                                  &(lsHeader.advRtrId),
                                  (UINT1 *) pNbr->pInterface,
                                  (UINT1 *) pNbr->pInterface->pArea);

    /*If topology change occur during router is in helping state, it should come out of helping */
    if ((pLsaInfo != (tLsaInfo *) NULL) && (pLsaInfo->pLsa != NULL))
    {
        if ((pNbr->pInterface->pArea->pOspfCxt->u1HelperStatus
             == OSPF_GR_HELPING) &&
            (pNbr->pInterface->pArea->pOspfCxt->u1StrictLsaCheck
             == OSIX_ENABLED) &&
            (GrFindTopologyChangeLSA (pLsaInfo, pLsa) == OSPF_TOPOLOGY_CHANGE))
        {
            pLsaInfo->u1LsaRefresh = OSPF_TRUE;
        }
    }

    /* Check for Max_age adveritsement (step.4) */

    if (IS_MAX_AGE (lsHeader.u2LsaAge))
    {

        if (pLsaInfo == NULL)
        {
            UINT1               u1DiscardFlag = OSPF_TRUE;
            tTMO_SLL_NODE      *pLstNode;
            tNeighbor          *pLstNbr;

            COUNTER_OP (pNbr->pInterface->pArea->pOspfCxt->u4RcvNewLsa, 1);

            /*
             * If none of the nbrs are in state NBRS_EXCHANGE || NBRS_LOADING
             * the ack the sending nbr & discard the LSA
             */

            TMO_SLL_Scan (&(pNbr->pInterface->pArea->pOspfCxt->sortNbrLst),
                          pLstNode, tTMO_SLL_NODE *)
            {
                pLstNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                if ((pLstNbr->u1NsmState == NBRS_EXCHANGE) ||
                    (pLstNbr->u1NsmState == NBRS_LOADING))
                {

                    u1DiscardFlag = OSPF_FALSE;
                    break;
                }
            }

            if (u1DiscardFlag == OSPF_TRUE)
            {

                /* send direct acknowledge */
                LakSendDirect (pNbr, pLsa);
                /* remove from nbr's request list */
                if ((pLsaReqNode = LrqSearchLsaReqLst
                     (pNbr, &lsHeader)) != NULL)
                {
                    LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                }

                return DISCARDED;
            }

        }
        else
        {

            /* In case of DC_SUPPORT since the rule " Only Self Originated Lsas
             * can be Flushed by a Router " is relaxed it can happen that router
             * can receive a self originated lsa with Max age set when it is 
             * being flushed off from other Neighbours. An additional check 
             * condition is made & if it is so the lsa should be reoriginated 
             * & flooded to other neighbours.  */

            if (LsuIsSelfOriginatedLsaInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                             &lsHeader) == OSPF_TRUE)

            {
                /* Modification to LSA processing rule for receiving self orginated LSA
                 * during graceful restart */
                /* XXX */
                if ((pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_RESTART)
                    && (LsuIsSelfOriginatedLsaInCxt
                        (pNbr->pInterface->pArea->pOspfCxt,
                         &lsHeader) == OSPF_TRUE))
                {
                    i1RetVal = GrLsuProcessRcvdSelfOrgLsa (&lsHeader, pLsaInfo,
                                                           pLsa, u2Len, pNbr);
                    return i1RetVal;
                }
                if (LsuCompLsaInstance (pNbr, pLsaInfo, &lsHeader) ==
                    OSPF_EQUAL)
                {
                    LsuFindAndProcessAckFlag (pNbr, pLsaInfo, pLsa, &lsHeader);

                    return DISCARDED;
                }

                /* send direct acknowledge */
                LakSendDirect (pNbr, pLsa);

                /* remove from nbr's request list */
                if ((pLsaReqNode = LrqSearchLsaReqLst (pNbr,
                                                       &lsHeader)) != NULL)
                {

                    if (LSU_FIND_RECENT_HDR (pNbr, pLsaInfo, &lsHeader,
                                             &(pLsaReqNode->lsHeader)) != LSA2)
                    {
                        LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                    }
                }

                if (pLsaInfo->pLsaDesc)
                {
                    pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
                }

                LsuProcessRcvdSelfOrgLsa (pLsaInfo, lsHeader.lsaSeqNum);

                return DISCARDED;
            }
            /* If Max Aged Grace LSA is received (GR Router successfully restarted). 
               Exit the helper, if this router is Helper for the GR Router */
            if ((pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) &&
                (pLsaInfo->lsaId.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
            {

                pInterface = pNbr->pInterface;
                TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                    if (UtilIpAddrComp (pNeighbor->nbrId, lsHeader.advRtrId)
                        == OSPF_EQUAL)
                    {
                        break;
                    }
                }
                if (pNeighbor == NULL)
                {
                    return DISCARDED;
                }
                if (pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING)
                {
                    OSPF_NBR_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                                   pNbr,
                                   pNbr->pInterface->pArea->pOspfCxt->
                                   u4OspfCxtId,
                                   "Exit the helper process for the neighbor %x \n",
                                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
                    pNeighbor->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
                    if (IS_NBR_FULL (pNbr)
                        || ((pNbr->u1NsmState == NBRS_2WAY)
                            && (pNbr->pInterface->u1IsmState == IFS_DR_OTHER)))
                    {
                        GrExitHelper (OSPF_HELPER_COMPLETED, pNeighbor);
                    }
                    else
                    {
                        GrExitHelper (OSPF_HELPER_GRACE_TIMEDOUT, pNeighbor);
                    }
                }
            }

        }
    }

    /* compare rcvd lsa with database copy  (step.5) */
    if ((pLsaInfo == (tLsaInfo *) NULL) ||
        (LsuCompLsaInstance (pNbr, pLsaInfo, &lsHeader) == RCVD_LSA))
    {

        OSPF_NBR_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "LSA Rcvd Nbr %x LSType %s LSId %x AdvRtrId %x\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                       au1DbgLsaType[lsHeader.u1LsaType],
                       OSPF_CRU_BMC_DWFROMPDU (lsHeader.linkStateId),
                       OSPF_CRU_BMC_DWFROMPDU (lsHeader.advRtrId));

        /* check if min_ls_arrival seconds have elapsed */
        if (pLsaInfo != (tLsaInfo *) NULL)
        {
            GET_LSA_AGE (pLsaInfo, &(u2LsaAge));

            if (!
                ((IS_MAX_AGE (u2LsaAge))
                 && (LsuCheckInRxmtLst (pNbr, pLsaInfo) != OSPF_ZERO)))
            {
                OsixGetSysTime ((tOsixSysTime *) & (u4SysTime));
                if ((pLsaInfo != (tLsaInfo *) NULL) &&
                    (((u4SysTime - pLsaInfo->u4LsaArrivalTime) <
                      MIN_LSA_ARRIVAL))
                    && !(IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo)))
                {
                    /* lsa discarded since min_ls_arrival has not expired */
                    OSPF_NBR_TRC (OSPF_LSU_TRC,
                                  pNbr,
                                  pNbr->pInterface->pArea->pOspfCxt->
                                  u4OspfCxtId,
                                  "Lsa discarded since min_ls_arrival has not expired\n");
                    return DISCARDED;
                }
            }
        }

        if ((pNbr->pInterface->pArea->pOspfCxt->bOverflowState == OSPF_TRUE) &&
            (pLsaInfo == (tLsaInfo *) NULL) &&
            IS_NON_DEFAULT_ASE_LSA (lsHeader) &&
            (pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
            (GET_ASE_LSA_COUNT_IN_CXT (pNbr->pInterface->pArea->pOspfCxt) >=
             pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit))
        {
            OSPF_NBR_TRC2 (OSPF_LSU_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "LSA LSID: %x ADVRTR: %x discarded\n",
                           OSPF_CRU_BMC_DWFROMPDU (lsHeader.linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (lsHeader.advRtrId));
            return DISCARDED;
        }
        if (pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState ==
            OSPF_GR_RESTART)
        {
            /* The received LSA is to be added to the LSDB. Check for
             * LSA inconsistency if the instance in Graceful restart mode */
            if ((GrLsuCheckLsaConsistency (pNbr->pInterface->pArea->pOspfCxt,
                                           pNbr->pInterface->pArea, &lsHeader,
                                           pLsa)) == OSPF_TRUE)
            {
                GrExitGracefultRestart (pNbr->pInterface->pArea->pOspfCxt,
                                        OSPF_RESTART_TOP_CHG);
                return DISCARDED;
            }
            else
            {
                /* modification to step 5.f rule for receiving self orginated LSA
                 * during graceful restart */
                if (LsuIsSelfOriginatedLsaInCxt
                    (pNbr->pInterface->pArea->pOspfCxt, &lsHeader) == OSPF_TRUE)
                {
                    i1RetVal = GrLsuProcessRcvdSelfOrgLsa (&lsHeader, pLsaInfo,
                                                           pLsa, u2Len, pNbr);
                    return i1RetVal;
                }
            }
        }
        if ((pLsaInfo != NULL) &&
            (LsuIsSelfOriginatedLsaInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                          &lsHeader) == OSPF_TRUE))
        {
            LsuProcessRcvdSelfOrgLsa (pLsaInfo, lsHeader.lsaSeqNum);
            if (lsHeader.lsaSeqNum == MAX_SEQ_NUM)
            {
                return DISCARDED;
            }
        }

        if ((pLsaInfo != (tLsaInfo *) NULL) && (pLsaInfo->pLsa != NULL))
        {
            /* Set the u1LsaChngdFlag appropriately */
            u1LsaChngdFlag =
                (UINT1) LsuCheckActualChange (pLsaInfo->pLsa, pLsa);

        }

        /* If the lsa is received for first time, Add the New lsa to the 
         * Data base */
        if (pLsaInfo == NULL)
        {
            if (lsHeader.u1LsaType == AS_EXT_LSA)
            {
                lsaMask = OSPF_CRU_BMC_DWFROMPDU (pLsa + LS_HEADER_SIZE);
                OSPF_CRU_BMC_DWTOPDU (linkMask, lsaMask);
                pLsaInfo = LsuDatabaseLookUp (lsHeader.u1LsaType,
                                              &(lsHeader.linkStateId),
                                              &(linkMask),
                                              &(pNbr->pInterface->pArea->
                                                pOspfCxt->rtrId),
                                              (UINT1 *) pNbr->pInterface,
                                              (UINT1 *) pNbr->pInterface->
                                              pArea);

                if (pLsaInfo != NULL)
                {
                    OS_MEM_CPY ((UINT1 *) &(extLsaLink.fwdAddr),
                                (UINT1 *) (pLsa + LS_HEADER_SIZE +
                                           FWD_ADDR_OFFSET_IN_EXT_LSA),
                                MAX_IP_ADDR_LEN);

                    OS_MEM_CPY ((UINT1 *) &(extLsaLink.u4Cost),
                                (UINT1 *) (pLsa + LS_HEADER_SIZE +
                                           MAX_IP_ADDR_LEN),
                                METRIC_TYPE_SIZE_IN_EXT_LSA +
                                METRIC_SIZE_IN_EXT_LSA);

                    OS_MEM_CPY ((UINT1 *) &(oldExtLsaLink.fwdAddr),
                                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE +
                                           FWD_ADDR_OFFSET_IN_EXT_LSA),
                                MAX_IP_ADDR_LEN);

                    OS_MEM_CPY ((UINT1 *) &(oldExtLsaLink.u4Cost),
                                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE +
                                           MAX_IP_ADDR_LEN),
                                METRIC_TYPE_SIZE_IN_EXT_LSA +
                                METRIC_SIZE_IN_EXT_LSA);

                    if ((UtilIpAddrComp
                         (extLsaLink.fwdAddr,
                          oldExtLsaLink.fwdAddr) == OSPF_EQUAL)
                        && (extLsaLink.u4Cost == oldExtLsaLink.u4Cost)
                        && (UtilIpAddrComp
                            (extLsaLink.fwdAddr, gNullIpAddr) != OSPF_EQUAL)
                        && (UtilIpAddrComp
                            (oldExtLsaLink.fwdAddr, gNullIpAddr) != OSPF_EQUAL))
                    {

                        if (UtilIpAddrComp
                            (lsHeader.advRtrId,
                             pLsaInfo->lsaId.advRtrId) == OSPF_GREATER)
                        {
                            AgdFlushOut (pLsaInfo);
                        }
                    }
                    pLsaInfo = NULL;

                }
            }

            /* Handling Functionally Equv Type 7 LSAs       */
            if (lsHeader.u1LsaType == NSSA_LSA)
                OlsHandleFuncEqvType7 (pLsa, pNbr);

            /*Check for Grace LSA */
            if ((lsHeader.u1LsaType == TYPE9_OPQ_LSA)
                && (lsHeader.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
            {
                if (UtilIpAddrComp (lsHeader.advRtrId,
                                    pNbr->pInterface->pArea->pOspfCxt->rtrId) ==
                    OSPF_EQUAL)
                {
                    /* send direct acknowledge */
                    LakSendDirect (pNbr, pLsa);
                    /* remove from nbr's request list */
                    if ((pLsaReqNode = LrqSearchLsaReqLst
                         (pNbr, &lsHeader)) != NULL)
                    {
                        LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                    }
                    return OSPF_SUCCESS;
                }
            }

            if ((pNewLsaInfo = LsuAddToLsdb (pNbr->pInterface->pArea,
                                             lsHeader.u1LsaType,
                                             &(lsHeader.linkStateId),
                                             &(lsHeader.advRtrId),
                                             pNbr->pInterface,
                                             NEW_LSA, pLsa)) == NULL)
            {

                return DISCARDED;
            }

            /* Install the New Lsa in the Link state database */
            if (LsuInstallLsa (pLsa, pNewLsaInfo) == OSPF_FAILURE)
            {
                return DISCARDED;
            }

        }
        else
        {
            /* remove the old instance of lsa from all rxmt lists */
            LsuDeleteFromAllRxmtLst (pLsaInfo);

            pLsaInfoAux = LsuSearchDatabase (lsHeader.u1LsaType,
                                             &(lsHeader.linkStateId),
                                             &(lsHeader.advRtrId),
                                             (UINT1 *) pNbr->pInterface,
                                             (UINT1 *) pNbr->pInterface->pArea);

            if (pLsaInfoAux == (tLsaInfo *) NULL)
            {

                if ((pLsaInfo = LsuAddToLsdb (pNbr->pInterface->pArea,
                                              lsHeader.u1LsaType,
                                              &(lsHeader.linkStateId),
                                              &(lsHeader.advRtrId),
                                              pNbr->pInterface,
                                              NEW_LSA_INSTANCE, pLsa)) == NULL)
                {

                    return (DISCARDED);
                }
            }

            /* Update the Old Lsa_info in the Data base  with new one */
            if (LsuInstallLsa (pLsa, pLsaInfo) == OSPF_FAILURE)
            {
                return DISCARDED;
            }

        }
        /*Check for Grace LSA */
        if ((lsHeader.u1LsaType == TYPE9_OPQ_LSA)
            && (lsHeader.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
        {

            pInterface = pNbr->pInterface;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbor = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                if (UtilIpAddrComp (pNeighbor->nbrId, lsHeader.advRtrId)
                    == OSPF_EQUAL)
                {
                    break;
                }
            }
            if (pNeighbor != NULL)
            {
                if ((pNbr->pInterface->pArea->pOspfCxt->
                     u1HelperSupport & OSPF_GRACE_HELPER_ALL) &&
                    (pNbr->pInterface->pArea->pOspfCxt->u1RestartStatus ==
                     OSPF_RESTART_NONE))
                {
                    if (GrHelperProcessGraceLSA (pNeighbor, &lsHeader, pLsa) ==
                        OSPF_SUCCESS)
                    {
                        OSPF_NBR_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                                       pNbr, pNbr->pInterface->pArea->pOspfCxt->
                                       u4OspfCxtId,
                                       "Successfully Processed the Grace LSA from %x \n",
                                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
                    }
                    else
                    {

                        OSPF_NBR_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                                       pNbr, pNbr->pInterface->pArea->pOspfCxt->
                                       u4OspfCxtId,
                                       "Couldn't Process the Grace LSA from %x \n",
                                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
                    }
                }
                else
                {
                    pNeighbor->u1NbrHelperExitReason = OSPF_HELPER_NONE;
                }
            }
        }

        /*
         * The flood procedure returns FLOOD_BACK or NO_FLOOD_BACK based on 
         * whether the lsa has been flooded out on the receiving interface 
         * or not 
         */

        ackFlag = LsuFloodOut (pLsa, u2Len, pNbr,
                               pNbr->pInterface->pArea,
                               u1LsaChngdFlag, pNbr->pInterface);

        LsuProcessAckFlag (pNbr, pLsa, ackFlag);

        if ((lsHeader.u2LsaAge == MAX_AGE) && (pLsaInfo != NULL)
            && (pLsaInfo->u4RxmtCount == 0))
        {
            LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE);
            return OSPF_SUCCESS;
        }

        /* if self originated generate new instance */

        if (LsuIsSelfOriginatedLsaInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                         &lsHeader) == OSPF_TRUE)
        {

            OSPF_NBR_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "New Instance Of Self Originated LSA Rcvd Nbr %x"
                           " LSType %s LSId %x\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                           au1DbgLsaType[lsHeader.u1LsaType],
                           OSPF_CRU_BMC_DWFROMPDU (lsHeader.linkStateId));
            if (pLsaInfo == NULL)
            {
                LsuProcessRcvdSelfOrgLsa (pNewLsaInfo, lsHeader.lsaSeqNum);
            }
            else
            {
                LsuProcessRcvdSelfOrgLsa (pLsaInfo, lsHeader.lsaSeqNum);
            }
        }

        COUNTER_OP (pNbr->pInterface->pArea->pOspfCxt->u4RcvNewLsa, 1);

        if (!(pNbr->pInterface->pArea->pOspfCxt->bOverflowState == OSPF_TRUE) &&
            (pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
            (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT
             (pNbr->pInterface->pArea->pOspfCxt) ==
             pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit))
        {
            OSPF_NBR_TRC2 (OS_RESOURCE_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "LSA LSID: %x ADVRTR: %x makes router enter overflow "
                           "state\n",
                           OSPF_CRU_BMC_DWFROMPDU (lsHeader.linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (lsHeader.advRtrId));

            RtrEnterOverflowStateInCxt (pNbr->pInterface->pArea->pOspfCxt);
        }

        if (IS_TRAP_ENABLED_IN_CXT (LSDB_APPROACHING_OVERFLOW_TRAP,
                                    pNbr->pInterface->pArea->pOspfCxt) &&
            (pNbr->pInterface->pArea->pOspfCxt->bLsdbApproachingOvflTrapGen
             == OSPF_FALSE) &&
            (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT
             (pNbr->pInterface->pArea->pOspfCxt) >=
             OVERFLOW_APPROACHING_LIMIT_IN_CXT
             (pNbr->pInterface->pArea->pOspfCxt)) &&
            (pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit != NO_LIMIT))
        {
            trapElsdbOverflow.i4ExtLsdbLimit =
                pNbr->pInterface->pArea->pOspfCxt->i4ExtLsdbLimit;
            IP_ADDR_COPY (trapElsdbOverflow.rtrId,
                          pNbr->pInterface->pArea->pOspfCxt->rtrId);
            SnmpifSendTrapInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                 LSDB_APPROACHING_OVERFLOW_TRAP,
                                 &trapElsdbOverflow);
            pNbr->pInterface->pArea->pOspfCxt->bLsdbApproachingOvflTrapGen =
                OSPF_TRUE;
        }
    }

    /* check if there as an instance of advt in req lst (step.6) */

    else if (LrqSearchLsaReqLst (pNbr, &lsHeader) != (tLsaReqNode *) NULL)
    {

        GENERATE_NBR_EVENT (pNbr, NBRE_BAD_LS_REQ);

        /* stop processing of lsu packet */

        return OSPF_FAILURE;
    }

    /* check if the rcvd lsa and database copy are of same instance (step.7) */
    else if (LsuCompLsaInstance (pNbr, pLsaInfo, &lsHeader) == OSPF_EQUAL)
    {

        /* check for implied ack */
        LsuFindAndProcessAckFlag (pNbr, pLsaInfo, pLsa, &lsHeader);
        return DISCARDED;
    }
    else
    {
        /* database copy is more recent (step.8) */

        if (IS_MAX_AGE (pLsaInfo->u2LsaAge) &&
            pLsaInfo->lsaSeqNum == MAX_SEQ_NUM)
        {
        }
        else
        {
            lsUpdate.pLsuPkt = NULL;
            LsuClearUpdate (&lsUpdate);
            LsuAddToLsu (pNbr, pLsaInfo, &lsUpdate, TX_ONCE);
            LsuSendLsu (pNbr, &lsUpdate);
        }
        return DISCARDED;

    }

    OSPF_NBR_TRC (OSPF_LSU_TRC, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "LSA Processing Over\n");
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: LsuProcessLsa\n");

    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFloodOut                                              */
/*                                                                           */
/* Description  : Reference : RFC-1247 Section 13.3.                         */
/*                This procedure takes care of the flooding of a received    */
/*                LSA. The LSA is flooded out on certain interfaces in this  */
/*                area. The pNbr refers to the neighbor from whom this LSA  */
/*                was received. This paramater is 0 for self-originated LSAs.*/
/*                This procedure returns FLOOD_BACK or NO_FLOOD_BACK based   */
/*                on whether the lsa has been flooded out on the receiving   */
/*                interface or not.                                          */
/*                                                                           */
/* Input        : pLsa            : the LSA to be flooded                   */
/*                u2Len           : length of LSA                           */
/*                pRcvNbr        : nbr from whom the lsa was received.     */
/*                                   NULL for self originated lsa            */
/*                pFloodArea     : the area into which the lsa is to be    */
/*                                   flooded                                 */
/*                pIface           : the Interface into which the lsa is to  */
/*                                   be flooded (this field is valid only    */
/*                                   for TYPE9_OPQ_LSA                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ackFlag.                                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
LsuFloodOut (UINT1 *pLsa,
             UINT2 u2Len,
             tNeighbor * pRcvNbr,
             tArea * pFloodArea, UINT1 u1LsaChngdFlag, tInterface * pIface)
{
    UINT1               ackFlag = UNUSED_ACK_FLAG;
    UINT1               floodFlag = 0;
    UINT1               u1NbrExited = OSPF_FALSE;
    tArea              *pArea;
    tTMO_SLL_NODE      *pLstNbr;
    tTMO_SLL_NODE      *pLstIf;
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pRcvInterface;
    tInterface         *pInterface;
    tNeighbor          *pNbr = NULL;
    tNeighbor          *pNeighbor = NULL;
    tLsHeader           lsHeader;
    tLsaReqNode        *pLsaReqNode;
    tLsaInfo           *pLsaInfo;

    UNUSED_PARAM (u1LsaChngdFlag);

    OSPF_TRC (OSPF_FN_ENTRY, pFloodArea->pOspfCxt->u4OspfCxtId,
              "FUNC: LsuFloodOut\n");
    OSPF_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pFloodArea->pOspfCxt->u4OspfCxtId,
               "Flooding LSA %x Area %x Len %d \n", pLsa, pFloodArea, u2Len);

    pRcvInterface = (pRcvNbr == NULL) ? NULL : pRcvNbr->pInterface;

    /* extract ls header info */

    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    pLsaInfo = LsuSearchDatabase (lsHeader.u1LsaType,
                                  &(lsHeader.linkStateId),
                                  &(lsHeader.advRtrId), (UINT1 *) pIface,
                                  (UINT1 *) pFloodArea);
    if (pLsaInfo == NULL)
    {

        return ackFlag;
    }

    TMO_SLL_Scan (&(pFloodArea->pOspfCxt->areasLst), pArea, tArea *)
    {

        /* LSAs except ext_lsa are flooded only to the specified area */

        if (lsHeader.u1LsaType != TYPE9_OPQ_LSA)
        {
            if (((lsHeader.u1LsaType != AS_EXT_LSA) &&
                 (lsHeader.u1LsaType != TYPE11_OPQ_LSA))
                && (pArea != pFloodArea))
            {
                continue;
            }
        }
        else
        {
            if (pIface->pArea != pArea)
            {
                continue;
            }
        }

        /* ext_lsa are flooded only to non-stub areas */
        if (((lsHeader.u1LsaType == AS_EXT_LSA) ||
             (lsHeader.u1LsaType == TYPE11_OPQ_LSA)) &&
            (pArea->u4AreaType != NORMAL_AREA))
        {

            continue;
        }

        TMO_SLL_Scan (&(pArea->ifsInArea), pLstIf, tTMO_SLL_NODE *)
        {

            pInterface = GET_IF_PTR_FROM_LST (pLstIf);
            if ((lsHeader.u1LsaType == TYPE9_OPQ_LSA) && (pInterface != pIface))
            {
                continue;
            }
            if (IS_VIRTUAL_IFACE (pInterface) &&
                (lsHeader.u1LsaType == AS_EXT_LSA))
            {
                continue;
            }                    /* End */

            /* Only if lsa is changed flood it over the interface */
            if ((IS_DC_EXT_APPLICABLE_IF (pInterface)) &&
                (IS_DNA_LSA_PROCESS_CAPABLE (pArea)) &&
                (u1LsaChngdFlag == OSPF_FALSE))
            {
                continue;
            }

            floodFlag = OSPF_FALSE;

            /* step 1 */

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNbr, tTMO_SLL_NODE *)
            {

                pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNbr);

                /* step 1.a */

                if (pNbr->u1NsmState < NBRS_EXCHANGE)
                {
                    continue;

                    /* step 1.b */

                }
                else if ((IS_OPQ_LSA (lsHeader.u1LsaType)) &&
                         (!(pNbr->nbrOptions & OPQ_BIT_MASK)))
                {
                    continue;
                }
                else if (pNbr->u1NsmState < NBRS_FULL && pRcvNbr != NULL)
                {

                    if ((pLsaReqNode =
                         LrqSearchLsaReqLst (pNbr, &lsHeader)) != NULL)
                    {

                        switch (LSU_FIND_RECENT_HDR (pNbr, pLsaInfo,
                                                     &lsHeader,
                                                     &(pLsaReqNode->lsHeader)))
                        {
                            case LSA1:

                                /* rcv ad is more recent */
                                LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                                break;

                            case LSA2:

                                /* nbr copy is more recent, try next nbr */
                                continue;

                            case LSA_EQUAL:

                                /* both are same instance */
                                LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                                continue;
                            default:
                                break;
                        }
                    }
                }

                /* step 1.c */

                if (pNbr == pRcvNbr)
                {

                    /* try next neighbor */
                    continue;
                }

                /*If topology change during helping, come out of helper */
                if ((pNbr->pInterface->pArea->pOspfCxt->u1StrictLsaCheck ==
                     OSPF_TRUE) && (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING))
                {

                    TMO_SLL_Scan (&
                                  (pNbr->pInterface->pArea->pOspfCxt->
                                   sortNbrLst), pLstNode, tTMO_SLL_NODE *)
                    {
                        pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
                        if ((pLsaInfo->u1LsaRefresh == OSPF_TRUE) &&
                            (pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING))
                        {
                            if (UtilIpAddrComp (pNeighbor->nbrId,
                                                pNbr->nbrId) == OSPF_EQUAL)
                            {
                                u1NbrExited = OSPF_TRUE;
                            }
                            GrExitHelper (OSPF_HELPER_TOP_CHG, pNeighbor);
                        }
                    }
                }
                if (u1NbrExited == OSPF_FALSE)
                {
                    /* step 1.d */
                    LsuAddToRxmtLst (pNbr, pLsaInfo);
                    floodFlag = OSPF_TRUE;
                }
            }

            /* step 2 */

            if (floodFlag == OSPF_FALSE)
            {

                if (pInterface == pRcvInterface)
                {
                    ackFlag = NO_FLOOD_BACK;
                }
                continue;
            }
            if (pInterface == pRcvInterface)
            {

                /* step 3 */

                if ((UtilIpAddrComp
                     (GET_DR (pInterface),
                      pRcvNbr->nbrIpAddr) == OSPF_EQUAL)
                    ||
                    (UtilIpAddrComp
                     (GET_BDR (pInterface), pRcvNbr->nbrIpAddr) == OSPF_EQUAL))
                {

                    ackFlag = NO_FLOOD_BACK;
                    continue;
                }

                /* step 4 */

                if (pInterface->u1IsmState == IFS_BACKUP)
                {

                    ackFlag = NO_FLOOD_BACK;
                    continue;
                }

                if ((pInterface->u1IsmState == IFS_DR) &&
                    (UtilIpAddrComp
                     (pInterface->backupDesgRtr,
                      pRcvNbr->nbrIpAddr) != OSPF_EQUAL))
                {
                    ackFlag = FLOOD_BACK;
                }
            }

            /* step 5 */

            LsuAddToFloodUpdate (pInterface, pLsa, u2Len);

        }                        /* TMO_SLL_Scan for ifsInArea */

    }                            /* TMO_SLL_Scan for areas */

    OSPF_TRC (OSPF_FN_EXIT, pFloodArea->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuFloodOut\n");

    return ackFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuSendFloodUpdate                                      */
/*                                                                           */
/* Description  : This procedure is invoked to send a link state update      */
/*                packet that is being constructed for this interface. This  */
/*                procedure is invoked when the packet is full or the        */
/*                process(flooding) which was adding lsa to the pkt is       */
/*                complete.                                                  */
/*                                                                           */
/* Input        : pInterface        : interface on which lsu is to be sent  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
LsuSendFloodUpdate (tInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr;
    UINT2               u2DigestLen;

    OSPF_TRC (OSPF_FN_ENTRY | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: LsuSendFloodUpdate\n");

    u2DigestLen = DIGEST_LEN_IF_CRYPT (pInterface);
    if ((pInterface->lsUpdate.pLsuPkt == NULL) ||
        (pInterface->lsUpdate.u2CurrentLen == 0))
    {
        return;
    }

    /* set #lsa in lsu packet */

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pInterface->lsUpdate.pLsuPkt, OS_HEADER_SIZE,
                                pInterface->lsUpdate.u4LsaCount);
    UtilConstructHdr (pInterface, pInterface->lsUpdate.pLsuPkt,
                      LS_UPDATE_PKT, pInterface->lsUpdate.u2CurrentLen);

    /* Timestamp database copy for each LSA transmitted in LSU packet */
    LsuPutTxTime (pInterface->lsUpdate.pLsuPkt, pInterface);

    if (pInterface->u1NetworkType == IF_BROADCAST)
    {

        if ((pInterface->u1IsmState == IFS_DR) ||
            (pInterface->u1IsmState == IFS_BACKUP))
        {
            PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                        (UINT2) (pInterface->lsUpdate.u2CurrentLen +
                                 u2DigestLen), pInterface, &gAllSpfRtrs,
                        RELEASE);
        }
        else
        {
            PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                        (UINT2) (pInterface->lsUpdate.u2CurrentLen +
                                 u2DigestLen), pInterface, &gAllDRtrs, RELEASE);
        }
    }
    else
    {

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {

            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            if (pNbr->u1NsmState >= NBRS_EXCHANGE)
            {
                PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                            (UINT2) (pInterface->lsUpdate.u2CurrentLen
                                     + u2DigestLen),
                            pInterface, &(pNbr->nbrIpAddr), NO_RELEASE);
            }
        }
        UtilOsMsgFree (pInterface->lsUpdate.pLsuPkt, NORMAL_RELEASE);
    }
    pInterface->lsUpdate.pLsuPkt = NULL;
    LsuClearUpdate (&pInterface->lsUpdate);

    OSPF_TRC (OSPF_FN_EXIT | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuSendFloodUpdate\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuSplAgeTmrAction                                     */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                Starts or Stops the dna_lsa_spl_aging timer for the lsa's  */
/*                in the lsa_list originated by the routerId.               */
/*                                                                           */
/* Input        : pLsaLst  : List of lsa in which lsa has to be searched.  */
/*                route_id   : Router id is the key for searching lsas.      */
/*                u1Action  : Indicates the START or STOP action of timer.  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuSplAgeTmrAction (tTMO_SLL * pLsaLst, tRouterId routerId, UINT1 u1Action)
{
    tTMO_SLL_NODE      *pLstNode;
    tLsaInfo           *pLsaInfo;

    TMO_SLL_Scan (pLsaLst, pLstNode, tTMO_SLL_NODE *)
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
        if (IS_DNA_LSA (pLsaInfo) &&
            (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, routerId) == OSPF_EQUAL))
        {
            if (u1Action == START)
            {
                TmrRestartTimer (&(pLsaInfo->dnaLsaSplAgingTimer),
                                 DNA_LSA_SPL_AGING_TIMER,
                                 NO_OF_TICKS_PER_SEC * MAX_AGE);
            }
            else
            {
                /* action is to STOP timer */
                TmrDeleteTimer (&(pLsaInfo->dnaLsaSplAgingTimer));
            }
        }
    }                            /* End of TMO_SLL_Scan */
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  LsuStartorStopSplAgingTmr                              */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                Starts or Stops the dna_lsa_spl_aging timer for the   */
/*                Ext Lsa if ASBR entry is reachable or not                */
/*                                                                           */
/* Input        : pRtEntry  : Routing Entry  */
/*                u1Action  : Indicates the START or STOP action of timer.  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuStartorStopSplAgingTmrInCxt (tOspfCxt * pOspfCxt, tRtEntry * pRtEntry,
                                UINT1 u1Action)
{
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pExtLstNode;
    tLsaInfo           *pExtLsaInfo;
    tOsDbNode          *pOsDbNode = NULL;

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pExtLstNode, tTMO_SLL_NODE *)
            {
                pExtLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo,
                                                 nextLsaInfo, pExtLstNode);
                if ((IS_DNA_LSA (pExtLsaInfo)) &&
                    (UtilIpAddrComp (pExtLsaInfo->lsaId.advRtrId,
                                     pRtEntry->destId) == OSPF_EQUAL))
                {
                    if (u1Action == START)
                    {
                        TmrRestartTimer (&(pExtLsaInfo->dnaLsaSplAgingTimer),
                                         DNA_LSA_SPL_AGING_TIMER,
                                         NO_OF_TICKS_PER_SEC * MAX_AGE);

                        OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                                   pOspfCxt->u4OspfCxtId,
                                   "Timer Started for Ext Lsa %08x \n",
                                   OSPF_CRU_BMC_DWFROMPDU (pExtLsaInfo->lsaId.
                                                           linkStateId));
                    }
                    else
                    {
                        /* action is to STOP timer */
                        TmrDeleteTimer (&(pExtLsaInfo->dnaLsaSplAgingTimer));

                        OSPF_TRC1 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                                   pOspfCxt->u4OspfCxtId,
                                   "Timer Stoped for Ext Lsa %08x \n",
                                   OSPF_CRU_BMC_DWFROMPDU (pExtLsaInfo->lsaId.
                                                           linkStateId));
                    }
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuCheckActualChange                                    */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                This procedure compares the database copy with the         */
/*                received lsa & returns OSPF_TRUE if there is any change    */
/*                else returns OSPF_FALSE                                    */
/*                                                                           */
/* Input        : pDbLsa   : Lsa present in the database.                  */
/*                pRcvdLsa : Received LSA                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE : If changed.                                    */
/*                OSPF_FALSE : If not changed                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
LsuCheckActualChange (UINT1 *pDbLsa, UINT1 *pRcvdLsa)
{
    tLsHeader           dbLsHeader;
    tLsHeader           rcvdLsHeader;
    UINT4               u4LsaBodyLen;

    UtilExtractLsHeaderFromLbuf (pDbLsa, &dbLsHeader);
    UtilExtractLsHeaderFromLbuf (pRcvdLsa, &rcvdLsHeader);

    /* check for any change in lsas option field */
    if (dbLsHeader.lsaOptions != rcvdLsHeader.lsaOptions)
    {
        return OSPF_TRUE;
    }

    /* If any one of the lsas age is set to Max age */
    if ((IS_MAX_AGE (dbLsHeader.u2LsaAge)) ||
        (IS_MAX_AGE (rcvdLsHeader.u2LsaAge)))
    {
        return OSPF_TRUE;
    }

    /* check for change in the length field of ls header */
    if (dbLsHeader.u2LsaLen != rcvdLsHeader.u2LsaLen)
    {
        return OSPF_TRUE;
    }

    u4LsaBodyLen = dbLsHeader.u2LsaLen - LS_HEADER_SIZE;

    /* check for any change in the body of the lsa */
    if ((OS_MEM_CMP (((pDbLsa) + LS_HEADER_SIZE),
                     ((pRcvdLsa) + LS_HEADER_SIZE),
                     u4LsaBodyLen)) != OSPF_EQUAL)
    {
        return OSPF_TRUE;
    }
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuAddToFloodUpdate                                    */
/*                                                                           */
/* Description  : This procedure adds the specified lsa to the lsUpdate pkt */
/*                being constructed. If the pkt becomes full it is           */
/*                transmitted.                                               */
/*                                                                           */
/* Input        : pInterface       : interface on which the pkt is to be    */
/*                                    sent                                   */
/*                pLsa             : the lsa(in linear au1Buf) to be added to  */
/*                                    the pkt.                               */
/*                u2LsaLen        : the length of the lsa                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
LsuAddToFloodUpdate (tInterface * pInterface, UINT1 *pLsa, UINT2 u2LsaLen)
{

    tCRU_BUF_CHAIN_HEADER *pLsuPkt;
    UINT2               u2LsaAge;
    tLsHeader           lsHeader;

    OSPF_TRC (OSPF_FN_ENTRY | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: LsuAddToFloodUpdate\n");

    /* if pkt is full transmit the pkt */

    if ((IFACE_MTU (pInterface) -
         (pInterface->lsUpdate.u2CurrentLen +
          DIGEST_LEN_IF_CRYPT (pInterface))) < u2LsaLen)
    {
        LsuSendFloodUpdate (pInterface);
    }

    /* if new pkt allocate buffer */

    if (pInterface->lsUpdate.u2CurrentLen == 0)
    {

        if ((IFACE_MTU (pInterface) - (pInterface->lsUpdate.u2CurrentLen
                                       + DIGEST_LEN_IF_CRYPT (pInterface))) >=
            u2LsaLen)
        {
            if ((pInterface->lsUpdate.pLsuPkt =
                 UtilOsMsgAlloc (IFACE_MTU (pInterface))) == NULL)
            {

                OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "ALLOC FAILED for Txmission of LSU on Interface:%x.%d\n",
                           OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                           pInterface->u4AddrlessIf);
                return;
            }
        }
        else
        {
            if ((pInterface->lsUpdate.pLsuPkt =
                 UtilOsMsgAlloc ((u2LsaLen + OS_HEADER_SIZE + IP_HEADER_SIZE +
                                  LSU_FIXED_PORTION_SIZE +
                                  DIGEST_LEN_IF_CRYPT (pInterface)))) == NULL)
            {

                OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "ALLOC FAILED for Txmission of LSU on Interface:%x.%d\n",
                           OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                           pInterface->u4AddrlessIf);
                return;
            }

        }
        pInterface->lsUpdate.u2CurrentLen =
            OS_HEADER_SIZE + LSU_FIXED_PORTION_SIZE;
        pInterface->lsUpdate.u4LsaCount = 0;
    }

    /* add lsa to the pkt and update age field */

    pLsuPkt = pInterface->lsUpdate.pLsuPkt;
    OSPF_CRU_BMC_ASSIGN_STRING (pLsuPkt, pLsa,
                                pInterface->lsUpdate.u2CurrentLen, u2LsaLen);
    u2LsaAge = OSPF_CRU_BMC_WFROMPDU ((pLsa + AGE_OFFSET_IN_LS_HEADER));
    u2LsaAge = (UINT2) (INC_LSA_AGE (u2LsaAge, pInterface->u2IfTransDelay));
    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    /* Grace LSA should be process as normal LSA. Hence, LSA age should
     * be set as MAX_AGE only */
    if (IS_DC_EXT_APPLICABLE_IF (pInterface) &&
        IS_DNA_LSA_PROCESS_CAPABLE (pInterface->pArea) &&
        IS_MAX_AGE (u2LsaAge) &&
        !((lsHeader.u1LsaType == TYPE9_OPQ_LSA) &&
          (lsHeader.linkStateId[0] == GRACE_LSA_OPQ_TYPE)))
    {

        /*set DO_NOT_AGE in the lsa */
        u2LsaAge = DO_NOT_AGE | MAX_AGE;
    }
    else
    {
        if (IS_MAX_AGE (u2LsaAge))
        {
            u2LsaAge = MAX_AGE;
            OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "FUNC:LsuAddToFloodUpdate, u2LsaAge reach MAX_AGE\n");

        }
    }

    /* Do not age  should not be set for Grace LSA */
    if (IS_DC_EXT_APPLICABLE_IF (pInterface) &&
        IS_DNA_LSA_PROCESS_CAPABLE (pInterface->pArea) &&
        !IS_MAX_AGE (u2LsaAge) &&
        !((lsHeader.u1LsaType == TYPE9_OPQ_LSA) &&
          (lsHeader.linkStateId[0] == GRACE_LSA_OPQ_TYPE)))
    {

        /*set DO_NOT_AGE in the lsa */
        u2LsaAge |= DO_NOT_AGE;
    }

    OSPF_CRU_BMC_ASSIGN_2_BYTE (pLsuPkt,
                                ((UINT4)
                                 (pInterface->lsUpdate.u2CurrentLen +
                                  AGE_OFFSET_IN_LS_HEADER)), u2LsaAge);

    pInterface->lsUpdate.u4LsaCount++;

    if (u2LsaLen >= (IFACE_MTU (pInterface) -
                     (pInterface->lsUpdate.u2CurrentLen
                      + DIGEST_LEN_IF_CRYPT (pInterface))))
    {
        pInterface->lsUpdate.u2CurrentLen += u2LsaLen;
        LsuSendFloodUpdate (pInterface);
    }
    else
    {
        pInterface->lsUpdate.u2CurrentLen += u2LsaLen;
    }

    OSPF_TRC (OSPF_FN_EXIT | OSPF_ADJACENCY_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuAddToFloodUpdate\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuCheckAndFlushIndicationLsa                         */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                This checks & flushes of all indication lsas in the area.  */
/*                                                                           */
/* Input        : pArea     : area from which indication lsa's have         */
/*                             to be flushed.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
LsuCheckAndFlushIndicationLsa (tArea * pArea)
{
    tArea              *pTmpArea;
    UINT1               u1Flag = OSPF_TRUE;

    if (pArea->u4DcBitResetLsaCount == 0)
    {

        /* area has no dc bit reset lsa */
        u1Flag = OSPF_FALSE;
    }
    if (u1Flag == OSPF_TRUE)
    {
        TMO_SLL_Scan (&(pArea->pOspfCxt->areasLst), pTmpArea, tArea *)
        {
            if ((pTmpArea == pArea) || (!IS_INDICATION_LSA_PRESENCE (pTmpArea)))
            {
                continue;
            }
            RBTreeWalk (pTmpArea->pSummaryLsaRoot,
                        RbWalkAndRestartIndicationLsa, NULL, NULL);
            /* end of TMO_SLL_Scan for area */
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuInstallLsa                                            */
/*                                                                           */
/* Description  : This procedure installs the LSA in the specified area. If  */
/*                there is any change appropriate routing table entries are  */
/*                re-calculated.                                             */
/*                                                                           */
/* Input        : pLsa               : poinetr to LSA                       */
/*                pLsaInfo          : pointer to the advertisement         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS or OSPF_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
LsuInstallLsa (UINT1 *pLsa, tLsaInfo * pLsaInfo)
{
    UINT1              *currPtr;
    UINT1               changeFlag = OSPF_FALSE;
    UINT1               u1Instance = NEW_LSA;
    UINT1               u1PrvOptions;

    OSPF_TRC (OSPF_FN_ENTRY | OSPF_ADJACENCY_TRC,
              pLsaInfo->pOspfCxt->u4OspfCxtId, "FUNC: LsuInstallLsa\n");
    OSPF_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "Install LSA Type %d Link_state_ID %x Adv_rtr_ID %x\n",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    currPtr = pLsa;
    if (LsuCompLsaContents (pLsaInfo, pLsa) == NOT_EQUAL)
    {
        changeFlag = OSPF_TRUE;
    }

    if (pLsaInfo->pLsa != NULL)
    {

        /* overwriting the existing dc_bit_reset_lsa */
        if (!IS_DC_BIT_SET_LSA (pLsaInfo) &&
            pLsaInfo->lsaId.u1LsaType != AS_EXT_LSA &&
            pLsaInfo->lsaId.u1LsaType != TYPE9_OPQ_LSA &&
            pLsaInfo->lsaId.u1LsaType != TYPE11_OPQ_LSA)
        {

            if (pLsaInfo->pArea->u4DcBitResetLsaCount == 0)
            {
                return OSPF_FAILURE;
            }
            if (IS_INDICATION_LSA (pLsaInfo))
            {
                pLsaInfo->pArea->u4IndicationLsaCount--;

            }
            if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
            {
                if (pLsaInfo->pArea->u4DcBitResetLsaCount == 1)
                {
                    LsuCheckAndFlushIndicationLsa (pLsaInfo->pArea);
                }
            }
            pLsaInfo->pArea->u4DcBitResetLsaCount--;

        }
        u1Instance = NEW_LSA_INSTANCE;

        switch (pLsaInfo->lsaId.u1LsaType)
        {
            case ROUTER_LSA:
            case NETWORK_LSA:
            case NETWORK_SUM_LSA:
            case ASBR_SUM_LSA:
            case COND_NETWORK_SUM_LSA:
            case DEFAULT_NETWORK_SUM_LSA:
                pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
                break;

            case NSSA_LSA:
                pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
                break;

            case AS_EXT_LSA:
                pLsaInfo->pOspfCxt->u4ExtLsaChksumSum -= pLsaInfo->u2LsaChksum;
                if (MEMCMP (pLsaInfo->pLsa + LS_HEADER_SIZE,
                            pLsa + LS_HEADER_SIZE,
                            MAX_IP_ADDR_LEN) != OSPF_EQUAL)
                {
                    /* If the Mask of the LSA for Installation differs from the
                       existing LSA,Rearrange the LSA in the HashTable */
                    LsuDeletePrefixLsaInHashLst (pLsaInfo->pOspfCxt->
                                                 pExtLsaHashTable, pLsaInfo);
                    LsuAddPrefixLsaInHashLst (pLsaInfo->pOspfCxt->
                                              pExtLsaHashTable, pLsaInfo, pLsa);
                }
                break;

            case TYPE11_OPQ_LSA:
                pLsaInfo->pOspfCxt->u4Type11OpqLSAChksumSum -=
                    pLsaInfo->u2LsaChksum;
                break;

            case TYPE10_OPQ_LSA:
                pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
                pLsaInfo->pArea->u4Type10OpqLSAChksumSum -=
                    pLsaInfo->u2LsaChksum;
                break;

            case TYPE9_OPQ_LSA:
                pLsaInfo->pInterface->u4Type9OpqLSAChksumSum -=
                    pLsaInfo->u2LsaChksum;
                break;
        }

        LSA_FREE (pLsaInfo->lsaId.u1LsaType, pLsaInfo->pLsa);
    }

    pLsaInfo->u2LsaAge = LGET2BYTE (currPtr);
    /* Checking the P-bit option. If there is a change 
     * set the changeFlag to OSPF_TRUE. 
     * This is done to call RtcIncUpdateExtRoute */
    u1PrvOptions = pLsaInfo->lsaOptions;
    pLsaInfo->lsaOptions = LGET1BYTE (currPtr);
    if (u1PrvOptions != pLsaInfo->lsaOptions)
    {
        changeFlag = OSPF_TRUE;
    }
    pLsaInfo->lsaId.u1LsaType = LGET1BYTE (currPtr);
    LGETSTR (currPtr, pLsaInfo->lsaId.linkStateId, MAX_IP_ADDR_LEN);
    LGETSTR (currPtr, pLsaInfo->lsaId.advRtrId, MAX_IP_ADDR_LEN);
    pLsaInfo->lsaSeqNum = LGET4BYTE (currPtr);
    pLsaInfo->u2LsaChksum = LGET2BYTE (currPtr);
    pLsaInfo->u2LsaLen = LGET2BYTE (currPtr);
    pLsaInfo->pLsa = pLsa;
    pLsaInfo->u1TrnsltType5 = OSPF_FALSE;
    pLsaInfo->u1LsaFlushed = OSPF_FALSE;

    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case COND_NETWORK_SUM_LSA:
        case DEFAULT_NETWORK_SUM_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum += pLsaInfo->u2LsaChksum;
            break;

        case NSSA_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum += pLsaInfo->u2LsaChksum;
            break;

        case AS_EXT_LSA:
            pLsaInfo->pOspfCxt->u4ExtLsaChksumSum += pLsaInfo->u2LsaChksum;
            break;

        case TYPE11_OPQ_LSA:
            pLsaInfo->pOspfCxt->u4Type11OpqLSAChksumSum +=
                pLsaInfo->u2LsaChksum;
            break;

        case TYPE10_OPQ_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum += pLsaInfo->u2LsaChksum;
            pLsaInfo->pArea->u4Type10OpqLSAChksumSum += pLsaInfo->u2LsaChksum;
            break;

        case TYPE9_OPQ_LSA:
            pLsaInfo->pInterface->u4Type9OpqLSAChksumSum +=
                pLsaInfo->u2LsaChksum;
            break;
    }

    OsixGetSysTime ((tOsixSysTime *) & (pLsaInfo->u4LsaArrivalTime));

    /* if lsa age is equal to MAX_AGE then the aging timer is set to 1 second */
    if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        if (pLsaInfo->lsaSeqNum < MAX_SEQ_NUM)
        {
            pLsaInfo->u1FloodFlag = OSPF_TRUE;
            TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                             LSA_NORMAL_AGING_TIMER, NO_OF_TICKS_PER_SEC * 1);
        }

        changeFlag = OSPF_TRUE;
    }
    else
    {
        if (pLsaInfo->u2LsaAge & DO_NOT_AGE)
        {
            TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                             LSA_NORMAL_AGING_TIMER,
                             NO_OF_TICKS_PER_SEC * CHECK_AGE);
        }
        else
        {
            TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                             LSA_NORMAL_AGING_TIMER,
                             NO_OF_TICKS_PER_SEC *
                             (CHECK_AGE - (pLsaInfo->u2LsaAge % CHECK_AGE)));
        }
        if ((pLsaInfo->pOspfCxt->u1OpqCapableRtr == OSPF_TRUE) &&
            (((pLsaInfo->lsaId.u1LsaType == ROUTER_LSA) &&
              (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))) ||
             (pLsaInfo->lsaId.u1LsaType == NETWORK_LSA) ||
             (pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) ||
             (pLsaInfo->lsaId.u1LsaType == TYPE10_OPQ_LSA) ||
             (pLsaInfo->lsaId.u1LsaType == TYPE11_OPQ_LSA)))
        {
            LsuSendLsaInfoInfoToOpqApp (pLsaInfo, u1Instance);
        }
    }
    if (pLsaInfo->pLsaDesc != NULL)
    {

        if (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired == OSPF_TRUE)
        {
            pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_FALSE;
            TmrSetTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer),
                         MIN_LSA_INTERVAL_TIMER,
                         (NO_OF_TICKS_PER_SEC *
                          pLsaInfo->pOspfCxt->u2MinLsaInterval));
        }
    }

    /* Newly installed lsa is dc_bit_reset_lsa */
    if (!IS_DC_BIT_SET_LSA (pLsaInfo) &&
        pLsaInfo->lsaId.u1LsaType != AS_EXT_LSA &&
        pLsaInfo->lsaId.u1LsaType != TYPE9_OPQ_LSA &&
        pLsaInfo->lsaId.u1LsaType != TYPE11_OPQ_LSA)
    {
        pLsaInfo->pArea->u4DcBitResetLsaCount++;

        /* check is it an indication lsa */
        if (IS_INDICATION_LSA (pLsaInfo))
        {
            pLsaInfo->pArea->u4IndicationLsaCount++;
        }

        if (pLsaInfo->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        {
            /* check for the generation of indication lsas */
            if ((!BACKBONE_ID (pLsaInfo->pArea->areaId)) &&
                (!IS_INDICATION_LSA (pLsaInfo)))
            {

                /* point 1 of RFC 1793 sec 2.5.1 */
                OlsSignalLsaRegenInCxt (pLsaInfo->pOspfCxt,
                                        SIG_INDICATION,
                                        (UINT1 *) pLsaInfo->pArea);
            }
            else if ((BACKBONE_ID (pLsaInfo->pArea->areaId)) &&
                     (!(IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))) &&
                     (!(IS_INDICATION_LSA (pLsaInfo))))
            {

                /* point 2 of RFC 1793 sec 2.5.1 */
                OlsSignalLsaRegenInCxt (pLsaInfo->pOspfCxt,
                                        SIG_INDICATION,
                                        (UINT1 *) pLsaInfo->pArea);
            }
        }
    }

    if ((pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA) &&
        ((UtilIpAddrComp (pLsaInfo->lsaId.linkStateId,
                          gNullIpAddr)) == OSPF_EQUAL))
    {

        pLsaInfo->pOspfCxt->bDefaultAseLsaPresenc = OSPF_TRUE;
    }

    if (changeFlag == OSPF_TRUE)
    {

        switch (pLsaInfo->lsaId.u1LsaType)
        {

            case ROUTER_LSA:
            case NETWORK_LSA:
                if (pLsaInfo->pOspfCxt->u1RtrNetworkLsaChanged != OSPF_TRUE)
                {
                    pLsaInfo->pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
                    RtcSetRtTimer (pLsaInfo->pOspfCxt);
                }
                break;

            case NETWORK_SUM_LSA:
            case ASBR_SUM_LSA:

                if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                                    pLsaInfo->pOspfCxt->rtrId) != OSPF_EQUAL)
                {
                    RtcIncUpdtRtSmmryLinkLsa (pLsaInfo->pOspfCxt->pOspfRt,
                                              pLsaInfo);
                }
                break;

            case AS_EXT_LSA:
            case NSSA_LSA:
                if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                                    pLsaInfo->pOspfCxt->rtrId) != OSPF_EQUAL)
                {
                    RtcIncUpdateExtRoute (pLsaInfo->pOspfCxt->pOspfRt,
                                          pLsaInfo);
                }
                break;

            default:
                break;
        }
    }

    /* Send the originated LSA to the standby node */
    if (OsRmDynLsSendLsa (pLsaInfo) == OSIX_FAILURE)
    {
        OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "Failed to sync the "
                       "originated LSU to the standby node "
                       "LSA Type : %d, Link state id : %x "
                       "Advertising router id : %x\r\n",
                       pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->
                                               lsaId.linkStateId),
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    }

    OSPF_TRC (OSPF_FN_EXIT | OSPF_ADJACENCY_TRC,
              pLsaInfo->pOspfCxt->u4OspfCxtId, "EXIT: LsuInstallLsa\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuSearchDatabase                                        */
/*                                                                           */
/* Description  : This procedure searches for the specified advertisement in */
/*                the specified area's database. If the advertisement type is*/
/*                AS external then the advertisement is searched in          */
/*                pOspfCxt->asExtLsaLst. If the searched advertisement is not*/
/*                found in the database then this function returns 0.        */
/*                                                                           */
/* Input        : u1LsaType           : type of LSA                        */
/*                pLinkStateId       : pointer to advertisement           */
/*                pAdvRtrId          : router id                          */
/*                                                                           */
/*                pPtr                  : NULL , for TYPE11_OPQ_LSA          */
/*                                                  and AS_EXT_LSAs          */
/*                                        Pointer to Interface to be         */
/*                                             searched for TYPE9_OPQ_LSA    */
/*                                        Pointer  to the Area to be         */
/*                                           searched for other lsa_types    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA info structure, if found                    */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tLsaInfo    *
LsuSearchDatabase (UINT1 u1LsaType,
                   tLINKSTATEID * pLinkStateId,
                   tRouterId * pAdvRtrId, UINT1 *pPtr1, UINT1 *pPtr2)
{
    UINT4               u4HashIndex;
    UINT4               u4LSId;
    UINT4               u4AdvRtrId;
    tLsaInfo           *pLsaInfo;
    tTMO_HASH_TABLE    *pLsaHashTable;
    tInterface         *pIface;
    tArea              *pArea;
    tTMO_SLL_NODE      *pLstNode;
    tLsaInfo            lsaInf;

    /* Scan the Type9SLLLst on the given interface and perform parameter 
       comparisions as done below. If LSA is found return pLsaInfo.
       Otherwise return NULL */
    if (u1LsaType == TYPE9_OPQ_LSA)
    {
        if (pPtr1 == NULL)
        {
            return (tLsaInfo *) NULL;
        }
        pIface = (tInterface *) (VOID *) pPtr1;

        TMO_SLL_Scan (&(pIface->Type9OpqLSALst), pLstNode, tTMO_SLL_NODE *)
        {

            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);

            if (UtilLsaIdComp (u1LsaType, *pLinkStateId, *pAdvRtrId,
                               pLsaInfo->lsaId.u1LsaType,
                               pLsaInfo->lsaId.linkStateId,
                               pLsaInfo->lsaId.advRtrId) == OSPF_EQUAL)
            {
                return pLsaInfo;
            }
        }
        return (tLsaInfo *) NULL;
    }

    if (u1LsaType == TYPE11_OPQ_LSA)
    {
        if (pPtr2 == NULL)
        {
            return (tLsaInfo *) NULL;
        }
        pArea = (tArea *) (VOID *) pPtr2;
        u4LSId = OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pLinkStateId);
        u4AdvRtrId = OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pAdvRtrId);
        pLsaInfo = olsGetType11OpqLsaInCxt (pArea->pOspfCxt,
                                            u1LsaType, u4LSId, u4AdvRtrId);
        return pLsaInfo;
    }

    if (u1LsaType == AS_EXT_LSA)
    {
        if (pPtr2 == NULL)
        {
            return (tLsaInfo *) NULL;
        }
        pArea = (tArea *) (VOID *) pPtr2;
        /* Search in the RBTree Maintained for Type5 LSAs */
        lsaInf.lsaId.u1LsaType = u1LsaType;
        IP_ADDR_COPY (lsaInf.lsaId.linkStateId, pLinkStateId);
        IP_ADDR_COPY (lsaInf.lsaId.advRtrId, pAdvRtrId);

        pLsaInfo = (tLsaInfo *) RBTreeGet (pArea->pOspfCxt->pAsExtLsaRBRoot,
                                           (tRBElem *) & lsaInf);
        return pLsaInfo;
    }
    else
    {
        if (pPtr2 == NULL)
        {
            return (tLsaInfo *) NULL;
        }

        pArea = (tArea *) (VOID *) pPtr2;

        pLsaHashTable = pArea->pLsaHashTable;
    }

    if (pLsaHashTable == NULL)
    {
        return (tLsaInfo *) NULL;
    }
    u4HashIndex = LsuLsaHashFunc (u1LsaType, pLinkStateId, pAdvRtrId);

    TMO_HASH_Scan_Bucket (pLsaHashTable, u4HashIndex, pLsaInfo, tLsaInfo *)
    {

        if (UtilLsaIdComp (u1LsaType, *pLinkStateId, *pAdvRtrId,
                           pLsaInfo->lsaId.u1LsaType,
                           pLsaInfo->lsaId.linkStateId,
                           pLsaInfo->lsaId.advRtrId) == OSPF_EQUAL)
        {

            return pLsaInfo;
        }
    }
    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "LSA Search in Database Failed\n");

    return (tLsaInfo *) NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDatabaseLookUp                                        */
/*                                                                           */
/* Description  : This procedure searches for the specified advertisement in */
/*                the specified area's database using LsuSearchDatabase routine. */
/*                It also takes into account, the host bits set as a result of         */
/*                RFC2328 Appendix E specification                               */
/*                                                                           */
/* Input        : u1LsaType           : type of LSA                        */
/*                pLinkStateId       : pointer to advertisement           */
/*                pLinkMask           : pLinkMask                       */
/*                pAdvRtrId          : router id                          */
/*                                                                           */
/*                pPtr                  : NULL , for TYPE11_OPQ_LSA          */
/*                                                  and AS_EXT_LSAs          */
/*                                        Pointer to Interface to be         */
/*                                             searched for TYPE9_OPQ_LSA    */
/*                                        Pointer  to the Area to be         */
/*                                           searched for other lsa_types    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA info structure, if found                    */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tLsaInfo    *
LsuDatabaseLookUp (UINT1 u1LsaType, tLINKSTATEID * pLinkStateId,
                   tIPADDR * pLinkMask, tRouterId * pAdvRtrId,
                   UINT1 *pPtr1, UINT1 *pPtr2)
{

    tLsaInfo           *pLsaInfo = NULL;
    UINT4               tmpLsId;
    UINT4               netMask;
    UINT4               lsaMask;
    tIPADDR             maskedAddr;

    netMask = OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pLinkMask);
    UtilIpAddrMaskCopy (maskedAddr, *pLinkStateId, *pLinkMask);
    pLsaInfo =
        LsuSearchDatabase (u1LsaType, &maskedAddr, pAdvRtrId, pPtr1, pPtr2);

    if (pLsaInfo == NULL)
    {
        tmpLsId = OSPF_CRU_BMC_DWFROMPDU (maskedAddr);
        tmpLsId |= (~netMask);
        OSPF_CRU_BMC_DWTOPDU (maskedAddr, tmpLsId);
        pLsaInfo = LsuSearchDatabase (u1LsaType, &maskedAddr,
                                      pAdvRtrId, pPtr1, pPtr2);
        if (pLsaInfo != NULL)
        {
            return pLsaInfo;
        }
    }
    else
    {
        lsaMask = OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pLsa + LS_HEADER_SIZE);
        if (lsaMask == netMask)
        {
            return pLsaInfo;
        }
        else
        {
            tmpLsId = OSPF_CRU_BMC_DWFROMPDU (maskedAddr);
            tmpLsId |= (~netMask);
            OSPF_CRU_BMC_DWTOPDU (maskedAddr, tmpLsId);
            pLsaInfo = LsuSearchDatabase (u1LsaType, &maskedAddr,
                                          pAdvRtrId, pPtr1, pPtr2);
            if (pLsaInfo != NULL)
            {
                return pLsaInfo;
            }
        }
    }

    return (tLsaInfo *) NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuCompLsaInstance                                      */
/*                                                                           */
/* Description  : This procedure compares two LSAs to determine which is more*/
/*                recent.                                                    */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement in data */
/*                                      base                                 */
/*                pRcvdLsHeader    : the received advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : DB_LSA if the database copy is more recent                 */
/*                RCVD_LSA if the received copy is more recent               */
/*                OSPF_EQUAL if both are the same instance                        */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
LsuCompLsaInstance (tNeighbor * pNbr, tLsaInfo * pLsaInfo,
                    tLsHeader * pRcvdLsHeader)
{

    UINT1               retval;
    UINT2               u2LsaAge;
    tLSASEQNUM          lsaSeqNum;
    UINT1              *currPtr;

    retval = OSPF_ZERO;

    GET_LSA_AGE (pLsaInfo, &(u2LsaAge));

    currPtr = pLsaInfo->pLsa + OSPF_LSA_HDR_SEQ_NO_OFFSET;
    lsaSeqNum = LGET4BYTE (currPtr);

    switch (LsuFindRecentLsa (pNbr, pLsaInfo,
                              lsaSeqNum,
                              u2LsaAge,
                              pLsaInfo->u2LsaChksum,
                              pRcvdLsHeader->lsaSeqNum,
                              pRcvdLsHeader->u2LsaAge,
                              pRcvdLsHeader->u2LsaChksum))
    {
        case LSA_EQUAL:
            retval = OSPF_EQUAL;
            break;

        case LSA1:
            retval = DB_LSA;
            break;

        case LSA2:
            retval = RCVD_LSA;
            break;
        default:
            break;

    }

    return retval;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFindRecentLsa                                        */
/*                                                                           */
/* Description  : Reference : RFC-1247 Section 13.1.                         */
/*                This procedure returns 1 if first lsa is recent, 2 if the  */
/*                second lsa is recent and 0 if they are the same instance.  */
/*                                                                           */
/* Input        : seqNum1           : sequence number of of first LSA       */
/*                age1               : age of first LSA                      */
/*                chksum1            : checksum of first LSA                 */
/*                seqNum2           : sequence number of of second LSA      */
/*                age2               : age of second LSA                     */
/*                chksum2            : checksum of second LSA                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if both are equal                                   */
/*                LSA1, if LSA1 is recent                                    */
/*                LSA2, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT1
LsuFindRecentLsa (tNeighbor * pNbr, tLsaInfo * pLsaInfo1, tLSASEQNUM seqNum1,
                  UINT2 age1,
                  UINT2 chksum1, tLSASEQNUM seqNum2, UINT2 age2, UINT2 chksum2)
{
    UINT1               retval;

    age1 = age1 & 0x7fff;
    age2 = age2 & 0x7fff;

    if (seqNum1 != seqNum2)
    {
        retval = ((seqNum1 > seqNum2) ? LSA1 : LSA2);
    }
    else if (chksum1 != chksum2)
    {
        retval = ((chksum1 > chksum2) ? LSA1 : LSA2);
    }
    else if ((IS_MAX_AGE (age1)) && !(IS_MAX_AGE (age2)))
    {
        if ((pNbr != NULL) && (pLsaInfo1 != NULL))
        {
            if (LsuCheckInRxmtLst (pNbr, pLsaInfo1) != OSPF_ZERO)
            {
                retval = LSA2;
            }
            else
            {
                retval = LSA1;
            }
        }
        else
        {
            retval = LSA1;
        }
    }
    else if ((IS_MAX_AGE (age2)) && !(IS_MAX_AGE (age1)))
    {
        retval = LSA2;
    }
    else if (DIFF (age1, age2) > MAX_AGE_DIFF)
    {
        retval = ((age1 < age2) ? LSA1 : LSA2);
    }
    else
    {

        return LSA_EQUAL;
    }
    return retval;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuProcessDeletedLsa                                       */
/*                                                                           */
/* Description  : This procedure is called when the Exchange or load neighbor*/
/*                count becomes 0. This procedure scans the entire database  */
/*                of the specified area and deletes the LSAs marked as       */
/*                deleted.                                                   */
/*                                                                           */
/* Input        : pArea        : AREA that is to be updated                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuProcessDeletedLsa (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    tTMO_HASH_NODE     *pPrevNode;
    UINT4               u4HashKey;

    TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashKey)
    {

        pPrevNode = NULL;
        while ((pLsaInfo =
                (tLsaInfo *) (TMO_HASH_Get_Next_Bucket_Node
                              (pArea->pLsaHashTable, u4HashKey,
                               pPrevNode))) != NULL)
        {

            if ((pLsaInfo->u1LsaFlushed == OSPF_TRUE) &&
                (LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_FALSE) == OSPF_TRUE))
            {

                /* The lsa has been deleted from the database */

            }
            else
            {
                pPrevNode = &(pLsaInfo->nextLsaInfoNode);
            }
        }
    }

    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "Deleted LSAs Processed\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuCompLsaContents                                      */
/*                                                                           */
/* Description  : This procedure compares the contents of the two LSAs. This */
/*                function returns OSPF_EQUAL if there is no difference and       */
/*                NOT_EQUAL otherwise. This procedure is used while          */
/*                installing a newer instance of an advertisement.           */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement         */
/*                pRcvLsa           : received LSA                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL     : if two lsa's are equal                         */
/*                NOT_EQUAL : otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT1
LsuCompLsaContents (tLsaInfo * pLsaInfo, UINT1 *pRcvLsa)
{

    UINT2               u2RcvLsaLen;

    if (pLsaInfo->pLsa == NULL)
    {
        return NOT_EQUAL;
    }

    u2RcvLsaLen =
        OSPF_CRU_BMC_WFROMPDU ((pRcvLsa + LENGTH_OFFSET_IN_LS_HEADER));

    if (u2RcvLsaLen == pLsaInfo->u2LsaLen)
    {

        if (OS_MEM_CMP ((pLsaInfo->pLsa + LS_HEADER_SIZE),
                        (pRcvLsa + LS_HEADER_SIZE),
                        (u2RcvLsaLen - LS_HEADER_SIZE)))
        {
            return NOT_EQUAL;
        }
        else
        {
            return OSPF_EQUAL;
        }
    }
    else
    {
        return NOT_EQUAL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuLsaHashFunc                                          */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : u1LsaType      : lsa type                                */
/*                pLinkStateId  : link state id                           */
/*                pAdvRtrId     : Advertiser router id.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
LsuLsaHashFunc (UINT1 u1LsaType,
                tLINKSTATEID * pLinkStateId, tRouterId * pAdvRtrId)
{
    UINT1               au1Buf[LSA_HASH_KEY_SIZE];
    UINT4               u4HashTableSize;

    au1Buf[0] = u1LsaType;
    OS_MEM_CPY ((au1Buf + 1), pLinkStateId, sizeof (tLINKSTATEID));
    OS_MEM_CPY ((au1Buf + 1 + sizeof (tLINKSTATEID)), pAdvRtrId,
                sizeof (tRouterId));

    if ((u1LsaType == AS_EXT_LSA) || (u1LsaType == TYPE11_OPQ_LSA))
    {
        u4HashTableSize = EXT_LSA_HASH_TABLE_SIZE;
    }
    else
    {
        u4HashTableSize = LSA_HASH_TABLE_SIZE;
    }
    return (UtilHashGetValue (u4HashTableSize, au1Buf, LSA_HASH_KEY_SIZE));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFlushoutSelfOrgExtLsa                              */
/*                                                                           */
/* Description  : This procedure scans the AS external lsa list and flushes  */
/*                out the self originated LSAs.                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuFlushoutSelfOrgExtLsaInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4HashKey = 0;
    tLsaInfo           *pLsaInfo;
    tOsDbNode          *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pLstNode;
    tOsDbNode          *pOsNextDbNode = NULL;
    tTMO_SLL_NODE      *pNextLstNode;

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_DYN_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                                  pOsDbNode, pOsNextDbNode, tOsDbNode *)
        {
            OSPF_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pNextLstNode,
                                tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLstNode);
                if ((UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, pOspfCxt->rtrId)
                     == OSPF_EQUAL) &&
                    (pLsaInfo->u1TrnsltType5 == REDISTRIBUTED_TYPE5))
                {

                    /* 
                     * The following procedure does not actually delete the 
                     * lsa from the database. It only starts the aging timer
                     * with a value of one second.
                     */

                    LsuFlushLsaAfterRemovingFromDescLst (pLsaInfo);
                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuAddToLsu                                             */
/*                                                                           */
/* Description  : This procedure is called when a requested advertisement is */
/*                located in the database. The specified LSA is added to the */
/*                update packet. If the packet becomes full it is            */
/*                transmitted.                                               */
/*                                                                           */
/* Input        : pNbr              : neighbour to whom the LSU is being    */
/*                                     transmitted                           */
/*                pLsaInfo         : pointer to advertisement              */
/*                pUpdate           : the update packet                     */
/*                u1TxFlag         : transmit flag                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : LSA_TXED, if LSU txed once                                 */
/*                LSA_ADDED, if LSA is added and not transmitted             */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
LsuAddToLsu (tNeighbor * pNbr,
             tLsaInfo * pLsaInfo, tLsUpdate * pUpdate, UINT1 u1TxFlag)
{

    UINT2               u2LsaLen;
    UINT2               u2LsaAge;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: LsuAddToLsu\n");
    if (pLsaInfo->pLsa == NULL)
    {
        return OSPF_FAILURE;
    }

    u2LsaLen = pLsaInfo->u2LsaLen;
    if (pUpdate->u2CurrentLen > 0 &&
        (u2LsaLen >
         (IFACE_MTU (pNbr->pInterface) - (pUpdate->u2CurrentLen
                                          +
                                          DIGEST_LEN_IF_CRYPT
                                          (pNbr->pInterface)))))
    {

        LsuSendLsu (pNbr, pUpdate);
        if (u1TxFlag == TX_ONCE)
        {
            return LSU_TXED;
        }
    }

    /* The LSA is added to the LSU */

    if (pUpdate->u2CurrentLen == 0)
    {

        if ((pUpdate->pLsuPkt =
             UtilOsMsgAlloc (IFACE_MTU (pNbr->pInterface))) == NULL)
        {

            OSPF_TRC2 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "ALLOC FAILED for Lsu to Nbr %x.%d \n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf);

            return OSPF_FAILURE;
        }
        pUpdate->u2CurrentLen = OS_HEADER_SIZE + LSU_FIXED_PORTION_SIZE;
        pUpdate->u4LsaCount = 0;
    }
    GET_LSA_AGE (pLsaInfo, &(u2LsaAge));
    u2LsaAge =
        (UINT2) (INC_LSA_AGE (u2LsaAge, pNbr->pInterface->u2IfTransDelay));

    if (IS_DC_EXT_APPLICABLE_IF (pNbr->pInterface) &&
        IS_DNA_LSA_PROCESS_CAPABLE (pNbr->pInterface->pArea) &&
        IS_MAX_AGE (u2LsaAge))
    {

        /*set DO_NOT_AGE in the lsa */
        u2LsaAge = DO_NOT_AGE | MAX_AGE;
    }
    else
    {
        if (IS_MAX_AGE (u2LsaAge))
        {
            u2LsaAge = MAX_AGE;
            OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "FUNC:LsuAddToLsu LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                       pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                       pLsaInfo->u2LsaAge, pLsaInfo->lsaSeqNum);

        }
    }

    if (IS_DC_EXT_APPLICABLE_IF (pNbr->pInterface) &&
        IS_DNA_LSA_PROCESS_CAPABLE (pNbr->pInterface->pArea) &&
        !IS_MAX_AGE (u2LsaAge))
    {

        /*set DO_NOT_AGE in the lsa */
        u2LsaAge |= DO_NOT_AGE;
    }

    OSPF_CRU_BMC_ASSIGN_STRING (((tCRU_BUF_CHAIN_HEADER *) (pUpdate->pLsuPkt)),
                                pLsaInfo->pLsa, pUpdate->u2CurrentLen,
                                u2LsaLen);
    OSPF_CRU_BMC_ASSIGN_2_BYTE (((tCRU_BUF_CHAIN_HEADER *) (pUpdate->pLsuPkt)),
                                pUpdate->u2CurrentLen, u2LsaAge);
    pUpdate->u2CurrentLen += u2LsaLen;
    pUpdate->u4LsaCount++;

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuAddToLsu \n");

    return LSA_ADDED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuSendLsu                                               */
/*                                                                           */
/* Description  : This procedure just transmits the ls_update_pkt that was   */
/*                constructed during the processing of a received ls_req_pkt.*/
/*                This procedure is called when the processing of the        */
/*                received ls_req_pkt is over.                               */
/*                                                                           */
/* Input        : pNbr             : neighbour to whom the update packet is */
/*                                    to be sent                             */
/*                pUpdate          : the LSU that is to be sent             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuSendLsu (tNeighbor * pNbr, tLsUpdate * pUpdate)
{
    if (pUpdate->u2CurrentLen != 0)
    {

        /* set #lsa in lsu packet */

        OSPF_CRU_BMC_ASSIGN_4_BYTE (pUpdate->pLsuPkt, OS_HEADER_SIZE,
                                    pUpdate->u4LsaCount);
        UtilConstructHdr (pNbr->pInterface,
                          (tCRU_BUF_CHAIN_HEADER *) (pUpdate->pLsuPkt),
                          LS_UPDATE_PKT, pUpdate->u2CurrentLen);

        /* Timestamp database copy for each LSA transmitted in LSU packet */
        LsuPutTxTime (pUpdate->pLsuPkt, pNbr->pInterface);

        PppSendPkt (pUpdate->pLsuPkt, (UINT2) (pUpdate->u2CurrentLen
                                               +
                                               DIGEST_LEN_IF_CRYPT (pNbr->
                                                                    pInterface)),
                    pNbr->pInterface, &(pNbr->nbrIpAddr), RELEASE);
        pUpdate->pLsuPkt = NULL;
        LsuClearUpdate (pUpdate);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuClearUpdate                                           */
/*                                                                           */
/* Description  : Clears the update packet.                                  */
/*                                                                           */
/* Input        : pUpdate          : the LSU packet that is to be cleared   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuClearUpdate (tLsUpdate * pUpdate)
{
    pUpdate->u2CurrentLen = 0;
    pUpdate->u4LsaCount = 0;

    if (pUpdate->pLsuPkt != NULL)
    {

        UtilOsMsgFree (pUpdate->pLsuPkt, FORCED_RELEASE);
        pUpdate->pLsuPkt = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuProcessAckFlag                                       */
/*                                                                           */
/* Description  : This routine processes the LS ack packet.                  */
/*                                                                           */
/* Input        : pNbr               : neighbour from whom the ack was rcvd */
/*                pLsa               : the LS ack packet                    */
/*                u1AckFlag         : type of ack                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuProcessAckFlag (tNeighbor * pNbr, UINT1 *pLsa, UINT1 u1AckFlag)
{
    tLsaReqNode        *pLsaReqNode;
    tLsHeader           lsHeader;

    /* process ackFlag  (section 13.5 , Table 19) */

    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    switch (u1AckFlag)
    {

        case IMPLIED_ACK:

            /* received advt was duplicate and treated as implied ack */
            if ((pNbr->pInterface->u1IsmState == IFS_BACKUP) &&
                (UtilIpAddrComp (GET_DR (pNbr->pInterface),
                                 pNbr->nbrIpAddr) == OSPF_EQUAL))
            {
                LakAddToDelayedAck (pNbr->pInterface, pLsa);
                /* remove from nbr's request list */
                if ((pLsaReqNode = LrqSearchLsaReqLst
                     (pNbr, &lsHeader)) != NULL)
                {
                    LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                }

            }
            break;

        case NO_IMPLIED_ACK:

            /* received advt was duplicate but not treated as implied ack */
            LakSendDirect (pNbr, pLsa);
            /* remove from nbr's request list */
            if ((pLsaReqNode = LrqSearchLsaReqLst (pNbr, &lsHeader)) != NULL)
            {
                LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
            }

            break;

        case FLOOD_BACK:

            /* The lsa is flooded out on receiving interface */
            break;

        case NO_FLOOD_BACK:

            /* The lsa is not flooded out on receiving interface */
            if ((pNbr->pInterface->u1IsmState != IFS_BACKUP) ||
                ((pNbr->pInterface->u1IsmState == IFS_BACKUP) &&
                 (UtilIpAddrComp (GET_DR (pNbr->pInterface),
                                  pNbr->nbrIpAddr) == OSPF_EQUAL)))
            {
                LakAddToDelayedAck (pNbr->pInterface, pLsa);
                /* remove from nbr's request list */
                if ((pLsaReqNode = LrqSearchLsaReqLst
                     (pNbr, &lsHeader)) != NULL)
                {
                    LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                }

            }
            break;

        default:
            break;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuProcessRcvdSelfOrgLsa                              */
/*                                                                           */
/* Description  : This routine processes the received self originating LSA.  */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement         */
/*                lsaSeqNum         : Sequance number of the lsas.         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
LsuProcessRcvdSelfOrgLsa (tLsaInfo * pLsaInfo, tLSASEQNUM lsaSeqNum)
{
    /*Null check for LsaDesc */
    if (pLsaInfo->pLsaDesc == NULL)
    {
        AgdFlushOut (pLsaInfo);
        return;
    }

    if ((pLsaInfo->pOspfCxt->bOverflowState == OSPF_TRUE)
        && IS_NON_DEFAULT_ASE_LSA (pLsaInfo->lsaId))
    {
        AgdFlushOut (pLsaInfo);
        return;
    }

    pLsaInfo->lsaSeqNum = lsaSeqNum;

    if (pLsaInfo->lsaSeqNum >= MAX_SEQ_NUM)
    {
        LsuDeleteFromAllRxmtLst (pLsaInfo);

        if (pLsaInfo->pLsaDesc != NULL)
        {

            pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
            TmrRestartTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer),
                             MIN_LSA_INTERVAL_TIMER, NO_OF_TICKS_PER_SEC * 1);
        }
        return;
    }                            /* end */

    OlsStartTimerForLsaRegen (pLsaInfo);

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFlushLsaAfterRemovingFromDescLst                 */
/*                                                                           */
/* Description  : Flushes the LSA.                                           */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuFlushLsaAfterRemovingFromDescLst (tLsaInfo * pLsaInfo)
{
    AgdFlushOut (pLsaInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuClearLsaDesc                                         */
/*                                                                           */
/* Description  : Clears the LSA descriptor from the LSA descriptor list.    */
/*                                                                           */
/* Input        : pLsaDesc          : pointer to the LSA descriptor        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuClearLsaDesc (tLsaDesc * pLsaDesc)
{
    UINT1               u1InternalType = pLsaDesc->u1InternalLsaType;
    tOspfCxt           *pOspfCxt = pLsaDesc->pLsaInfo->pOspfCxt;

    if (IS_OPQ_LSA (u1InternalType))
    {
        if (pLsaDesc->pAssoPrimitive != NULL)
        {
            OspfOpqLSAFree ((tOpqLSAInfo *) (VOID *) pLsaDesc->pAssoPrimitive);
        }
    }
    TMO_DLL_Delete (&(pOspfCxt->origLsaDescLst), &(pLsaDesc->lsaDescNode));
    TmrDeleteTimer (&pLsaDesc->minLsaIntervalTimer);
    if (pLsaDesc->pAssoPrimitive != NULL)
    {

        if ((u1InternalType == NETWORK_SUM_LSA) ||
            (u1InternalType == ASBR_SUM_LSA) ||
            (u1InternalType == INDICATION_LSA))
        {
            SUM_PARAM_FREE (pLsaDesc->pAssoPrimitive);
        }

    }
    LSA_DESC_FREE (pLsaDesc);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuClearLsaInfo                                         */
/*                                                                           */
/* Description  : Clears the LSA from LSA list.                              */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuClearLsaInfo (tLsaInfo * pLsaInfo)
{
    tTMO_HASH_TABLE    *pHashTable = NULL;
    tOspfCxt           *pOspfCxt = pLsaInfo->pOspfCxt;

    if ((pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA) ||
        (pLsaInfo->lsaId.u1LsaType == TYPE11_OPQ_LSA))
    {

        /* If the Exterior route defining Default destination is deleted
         * then Reset flag which indicates the presence of default lsa
         * before deleting the lsa.
         */

        if (IS_NULL_IP_ADDR (pLsaInfo->lsaId.linkStateId))
        {
            pOspfCxt->bDefaultAseLsaPresenc = OSPF_FALSE;
        }

        if (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA)
        {
            LsuDeletePrefixLsaInHashLst (pOspfCxt->pExtLsaHashTable, pLsaInfo);

            /*Delete  the Type5 LSAs in RBTree */
            LsuAddOrDelLsaInRBTree (pLsaInfo, OSPF_DELETE);

            TmrDeleteTimer (&pLsaInfo->lsaAgingTimer);
            TmrDeleteTimer (&pLsaInfo->dnaLsaSplAgingTimer);

            LSA_FREE (pLsaInfo->lsaId.u1LsaType, pLsaInfo->pLsa);

            OS_MEM_SET ((char *) pLsaInfo, 0, sizeof (tLsaInfo));
            LSA_INFO_FREE (pLsaInfo);
            return;
        }
    }

    /* delete from hash table */
    if (pLsaInfo->lsaId.u1LsaType != TYPE11_OPQ_LSA)
        /* delete from hash table */
    {
        pHashTable = pLsaInfo->pArea->pLsaHashTable;
        TMO_HASH_Delete_Node (pHashTable, &(pLsaInfo->nextLsaInfoNode),
                              LsuLsaHashFunc (pLsaInfo->lsaId.u1LsaType,
                                              &(pLsaInfo->lsaId.
                                                linkStateId),
                                              &(pLsaInfo->lsaId.advRtrId)));
    }

    /* delete from lsa list */

    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case ROUTER_LSA:
            if (pOspfCxt->pLastLsdbLst == &pLsaInfo->pArea->rtrLsaLst &&
                pOspfCxt->pLastLsdbNode == &pLsaInfo->nextLsaInfo)
            {

                pOspfCxt->pLastLsdbLst = NULL;
                pOspfCxt->pLastLsdbNode = NULL;
            }
            TMO_SLL_Delete (&(pLsaInfo->pArea->rtrLsaLst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        case NETWORK_LSA:
            if (pOspfCxt->pLastLsdbLst == &pLsaInfo->pArea->networkLsaLst &&
                pOspfCxt->pLastLsdbNode == &pLsaInfo->nextLsaInfo)
            {

                pOspfCxt->pLastLsdbLst = NULL;
                pOspfCxt->pLastLsdbNode = NULL;
            }
            TMO_SLL_Delete (&(pLsaInfo->pArea->networkLsaLst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
            RBTreeRemove (pLsaInfo->pArea->pSummaryLsaRoot, pLsaInfo);

            break;

        case NSSA_LSA:
            if (pOspfCxt->pLastLsdbLst == &pLsaInfo->pArea->nssaLSALst &&
                pOspfCxt->pLastLsdbNode == &pLsaInfo->nextLsaInfo)
            {

                pOspfCxt->pLastLsdbLst = NULL;
                pOspfCxt->pLastLsdbNode = NULL;
            }
            TMO_SLL_Delete (&(pLsaInfo->pArea->nssaLSALst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        case TYPE9_OPQ_LSA:
            if (pOspfCxt->pLastLsdbLst == &pLsaInfo->pInterface->Type9OpqLSALst
                && pOspfCxt->pLastLsdbNode == &pLsaInfo->nextLsaInfo)
            {

                pOspfCxt->pLastLsdbLst = NULL;
                pOspfCxt->pLastLsdbNode = NULL;
            }
            TMO_SLL_Delete (&(pLsaInfo->pInterface->Type9OpqLSALst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        case TYPE10_OPQ_LSA:
            if (pOspfCxt->pLastLsdbLst == &pLsaInfo->pArea->Type10OpqLSALst &&
                pOspfCxt->pLastLsdbNode == &pLsaInfo->nextLsaInfo)
            {

                pOspfCxt->pLastLsdbLst = NULL;
                pOspfCxt->pLastLsdbNode = NULL;
            }
            TMO_SLL_Delete (&(pLsaInfo->pArea->Type10OpqLSALst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        case TYPE11_OPQ_LSA:
            if (pOspfCxt->pLastLsdbLst == &pOspfCxt->Type11OpqLSALst &&
                pOspfCxt->pLastLsdbNode == &pLsaInfo->nextLsaInfo)
            {

                pOspfCxt->pLastLsdbLst = NULL;
                pOspfCxt->pLastLsdbNode = NULL;
            }
            TMO_SLL_Delete (&(pOspfCxt->Type11OpqLSALst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        default:
            break;
    }

    TmrDeleteTimer (&pLsaInfo->lsaAgingTimer);
    TmrDeleteTimer (&pLsaInfo->dnaLsaSplAgingTimer);

    LSA_FREE (pLsaInfo->lsaId.u1LsaType, pLsaInfo->pLsa);

    OS_MEM_SET ((char *) pLsaInfo, 0, sizeof (tLsaInfo));
    LSA_INFO_FREE (pLsaInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDeleteLsaFromDatabase                               */
/*                                                                           */
/* Description  : This procedure deletes the specified lsa from the database.*/
/*                The conditions necessary for deleting a lsa from the       */
/*                database should be taken care at the calling place.        */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement         */
/*                u1ForcedRelFlag  : flag                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if the lsa is deleted from the database         */
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
LsuDeleteLsaFromDatabase (tLsaInfo * pLsaInfo, UINT1 u1ForcedRelFlag)
{
    UINT1               u1LsaTypeDeleted;
    tArea              *pArea = NULL;
    tLsaInfo           *pType7LsaInfo = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    /* The type of lsa deleted is copied here. The type is checked
       against Network Summary type in the end of this routine.
       If the LSA is network summary type, then the global variable
       indicating router, network lsa change is set. RT calculation
       will be done as this global variable is set now. This is done
       to force RT calculation after a network summary is flushed out
       from the data base.
     */
    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: LsuDeleteLsaFromDatabase\n");

    u1LsaTypeDeleted = pLsaInfo->lsaId.u1LsaType;

    OSPF_TRC3 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "Delete Lsa Type %d linkStateId %x advRtrId %x ",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    pOspfCxt = pLsaInfo->pOspfCxt;

    if (IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        pLsaInfo->u1LsaFlushed = OSPF_TRUE;
    }

    if (u1ForcedRelFlag == OSPF_FALSE)
    {
        if (((pLsaInfo->u1LsaFlushed != OSPF_TRUE) ||
             (pLsaInfo->u4RxmtCount != 0)) &&
            (pLsaInfo->lsaSeqNum < MAX_SEQ_NUM))
        {

            return OSPF_FALSE;
        }
        if ((pLsaInfo->lsaId.u1LsaType != AS_EXT_LSA) &&
            (pLsaInfo->lsaId.u1LsaType != TYPE11_OPQ_LSA) &&
            (pLsaInfo->lsaId.u1LsaType != TYPE9_OPQ_LSA) &&
            (pLsaInfo->pArea->u4XchgOrLoadNbrCount != 0))
        {

            return OSPF_FALSE;
        }
    }
    pLsaInfo->u1LsaFlushed = OSPF_FALSE;

    /* if seq_num_wrap_around flag is TRUE then originate new advt */

    if (u1ForcedRelFlag == OSPF_FALSE)
    {

        if ((pLsaInfo->pLsaDesc != (tLsaDesc *) NULL) &&
            (pLsaInfo->pLsaDesc->u1SeqNumWrapAround == OSPF_TRUE))
        {

            pLsaInfo->pLsaDesc->u1SeqNumWrapAround = OSPF_FALSE;
            pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
            TmrRestartTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer),
                             MIN_LSA_INTERVAL_TIMER, NO_OF_TICKS_PER_SEC * 1);
            return OSPF_FALSE;
        }
    }

    if (u1ForcedRelFlag == OSPF_TRUE)
    {
        LsuDeleteNodeFromAllRxmtLst (pLsaInfo);
    }

    if (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA &&
        !IS_NULL_IP_ADDR (pLsaInfo->lsaId.linkStateId) &&
        (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT (pOspfCxt) - 1 <=
         OVERFLOW_APPROACHING_LIMIT_IN_CXT (pOspfCxt)))
    {
        pOspfCxt->bLsdbApproachingOvflTrapGen = OSPF_FALSE;
    }

    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case COND_NETWORK_SUM_LSA:
        case DEFAULT_NETWORK_SUM_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case AS_EXT_LSA:
            pOspfCxt->u4ExtLsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case TYPE11_OPQ_LSA:
            pOspfCxt->u4Type11OpqLSAChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case TYPE10_OPQ_LSA:
            pLsaInfo->pArea->u4AreaLsaChksumSum -= pLsaInfo->u2LsaChksum;
            pLsaInfo->pArea->u4Type10OpqLSAChksumSum -= pLsaInfo->u2LsaChksum;
            break;
        case TYPE9_OPQ_LSA:
            pLsaInfo->pInterface->u4Type9OpqLSAChksumSum -=
                pLsaInfo->u2LsaChksum;
            break;
    }

    /* if the lsa to be removed is dc_bit_reset lsa */
    if (!IS_DC_BIT_SET_LSA (pLsaInfo) &&
        pLsaInfo->lsaId.u1LsaType != AS_EXT_LSA &&
        pLsaInfo->lsaId.u1LsaType != TYPE9_OPQ_LSA &&
        pLsaInfo->lsaId.u1LsaType != TYPE11_OPQ_LSA)
    {

        if (pLsaInfo->pArea->u4DcBitResetLsaCount == 0)
        {
            return OSPF_FALSE;
        }
        if (IS_INDICATION_LSA (pLsaInfo))
        {
            pLsaInfo->pArea->u4IndicationLsaCount--;
        }
        if (pLsaInfo->pArea->u4DcBitResetLsaCount == 1)
        {
            LsuCheckAndFlushIndicationLsa (pLsaInfo->pArea);
        }
        pLsaInfo->pArea->u4DcBitResetLsaCount--;
    }

    if ((pLsaInfo->pOspfCxt->u1OpqCapableRtr == OSPF_TRUE) &&
        (((pLsaInfo->lsaId.u1LsaType == ROUTER_LSA) &&
          (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))) ||
         (pLsaInfo->lsaId.u1LsaType == NETWORK_LSA) ||
         (pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA) ||
         (pLsaInfo->lsaId.u1LsaType == TYPE10_OPQ_LSA)))
    {
        LsuSendLsaInfoInfoToOpqApp (pLsaInfo, FLUSHED_LSA);
    }

    if (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA)
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if (pArea->u4AreaType == NSSA_AREA)
            {
                pType7LsaInfo =
                    LsuSearchDatabase (NSSA_LSA,
                                       &(pLsaInfo->lsaId.linkStateId),
                                       &(pOspfCxt->rtrId), NULL,
                                       (UINT1 *) pArea);

                if (pType7LsaInfo != NULL)
                {
                    OlsStartTimerForLsaRegen (pType7LsaInfo);
                }
            }
        }
    }

    if (u1LsaTypeDeleted == AS_EXT_LSA || u1LsaTypeDeleted == NSSA_LSA)
    {
        /* Incremental Update should be only called if LSA is not MAX_AGE
         * and is not Self Originated LSA. */
        if (!((IS_MAX_AGE (pLsaInfo->u2LsaAge)) &&
              (IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo))))
        {
            RtcIncUpdateExtRoute (pOspfCxt->pOspfRt, pLsaInfo);
        }
    }

    /* Send the deleted LSA to the standby node */
    pLsaInfo->u2LsaAge = MAX_AGE;
    OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
               pLsaInfo->pOspfCxt->u4OspfCxtId,
               "FUNC :LsuDeleteLsaFromDatabase  LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
               pLsaInfo->lsaId.u1LsaType,
               OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
               pLsaInfo->u2LsaAge, pLsaInfo->lsaSeqNum);

    if (OsRmDynLsSendLsa (pLsaInfo) == OSIX_FAILURE)
    {
        OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "Failed to sync the "
                       "originated LSU to the standby node "
                       "LSA Type : %d, Link state id : %x "
                       "Advertising router id : %x\r\n",
                       pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->
                                               lsaId.linkStateId),
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    }

    if (pLsaInfo->pLsaDesc != NULL)
    {

        LsuClearLsaDesc (pLsaInfo->pLsaDesc);
        pLsaInfo->pLsaDesc = NULL;
    }

    LsuClearLsaInfo (pLsaInfo);

    if ((u1LsaTypeDeleted == ROUTER_LSA) ||
        (u1LsaTypeDeleted == NETWORK_SUM_LSA) ||
        (u1LsaTypeDeleted == ASBR_SUM_LSA))
    {
        if (pOspfCxt->u1RtrNetworkLsaChanged != OSPF_TRUE)
        {
            pOspfCxt->u1RtrNetworkLsaChanged = OSPF_TRUE;
            RtcSetRtTimer (pOspfCxt);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: LsuDeleteLsaFromDatabase \n");
    return OSPF_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* function     : LsuFlushAllSelfOrgLsas                                        */
/*                                                                           */
/* description  : this routine fluses all the self originated lsas fron     */
/*                the database.                                             */
/*                called only once when the protocol is brought down.        */
/*                                                                           */
/* input        : none                                                       */
/*                                                                           */
/* output       : none                                                       */
/*                                                                           */
/* returns      : void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuFlushAllSelfOrgLsasInCxt (tOspfCxt * pOspfCxt)
{

    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        LsuFlushSelfOrgAreaLsaDatabase (pArea);
    }

    /* While deleting a interface, if no interface is associated with the
     * backbone, then the would have been delinked from the areasLst.
     * Backbone is the last entry in the Global areasList
     */

    if ((pArea =
         (tArea *) TMO_SLL_Last (&(pOspfCxt->areasLst))) != pOspfCxt->pBackbone)
    {
        LsuFlushSelfOrgAreaLsaDatabase (pOspfCxt->pBackbone);
    }
    LsuFlushAllSelfOrgExtLsasInCxt (pOspfCxt);
}

/*****************************************************************************/
/*                                                                           */
/* function     : LsuDeleteAllLsas                                        */
/*                                                                           */
/* description  : this routine deletes all the lsas fron the database.       */
/*                called only once when the protocol is brought down.        */
/*                                                                           */
/* input        : none                                                       */
/*                                                                           */
/* output       : none                                                       */
/*                                                                           */
/* returns      : void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuDeleteAllLsasInCxt (tOspfCxt * pOspfCxt)
{

    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        LsuDeleteAreaLsaDatabase (pArea);
    }

    /* While deleting a interface, if no interface is associated with the
     * backbone, then the would have been delinked from the areasLst.
     * Backbone is the last entry in the Global areasList
     */

    if ((pArea =
         (tArea *) TMO_SLL_Last (&(pOspfCxt->areasLst))) != pOspfCxt->pBackbone)
    {
        LsuDeleteAreaLsaDatabase (pOspfCxt->pBackbone);
    }
    LsuDeleteAllExtLsasInCxt (pOspfCxt);
    LsuDeleteAllType11LsasInCxt (pOspfCxt);
    LsuDeleteAllType9LsasInCxt (pOspfCxt);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFlushSelfOrgAreaLsaDatabase                            */
/*                                                                           */
/* Description  : This procedure is called when an area is being deleted, or */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pArea         : pointer to the AREA that is being deleted */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuFlushSelfOrgAreaLsaDatabase (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    tLsaInfo           *pTempLsaInfo;
    UINT4               u4HashKey;

    TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashKey)
    {
        OSPF_DYNM_SLL_SCAN (&pArea->pLsaHashTable->HashList[u4HashKey],
                            pLsaInfo, pTempLsaInfo, tLsaInfo *)
        {
            if ((UtilIpAddrComp (pLsaInfo->lsaId.advRtrId,
                                 pArea->pOspfCxt->rtrId)
                 == OSPF_EQUAL) &&
                (pLsaInfo->lsaId.u1LsaType != TYPE10_OPQ_LSA))
            {
                AgdFlushOut (pLsaInfo);
            }
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDeleteAreaLsaDatabase                               */
/*                                                                           */
/* Description  : This procedure is called when an area is being deleted, or */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pArea         : pointer to the AREA that is being deleted */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuDeleteAreaLsaDatabase (tArea * pArea)
{
    tLsaInfo           *pLsaInfo;
    UINT4               u4HashKey;

    TMO_HASH_Scan_Table (pArea->pLsaHashTable, u4HashKey)
    {

        while ((pLsaInfo =
                (tLsaInfo *)
                TMO_HASH_Get_First_Bucket_Node (pArea->pLsaHashTable,
                                                u4HashKey)) != NULL)
        {
            LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFlushAllSelfOrgExtLsas                                 */
/*                                                                           */
/* Description  : This routine flushes all the external LSAs fronm the       */
/*                database.                                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuFlushAllSelfOrgExtLsasInCxt (tOspfCxt * pOspfCxt)
{
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pNextLsaInfo = NULL;

    pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pOspfCxt->pAsExtLsaRBRoot,
                                        (tRBElem *) pLsaInfo, NULL);

        if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, pOspfCxt->rtrId) ==
            OSPF_EQUAL)
        {
            AgdFlushOut (pLsaInfo);
        }
        pLsaInfo = pNextLsaInfo;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDeleteAllExtLsas                                    */
/*                                                                           */
/* Description  : This routine deletes all the external LSAs fronm the       */
/*                database.                                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuDeleteAllExtLsasInCxt (tOspfCxt * pOspfCxt)
{
    tLsaInfo           *pLsaInfo;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "Entry: LsuDeleteAllExtLsas \n");
    while ((pLsaInfo = (tLsaInfo *)
            RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot)) != NULL)
    {
        LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: LsuDeleteAllExtLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDeleteAllType11Lsas                                     */
/*                                                                           */
/* Description  : This routine deletes all the Type11 LSAs fronm the         */
/*                database.                                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuDeleteAllType11LsasInCxt (tOspfCxt * pOspfCxt)
{
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLstNode;

    while ((pLstNode = TMO_SLL_First (&(pOspfCxt->Type11OpqLSALst))) != NULL)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);
        LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDeleteAllType9Lsas                                     */
/*                                                                           */
/* Description  : This routine deletes all the Type9 LSAs fronm the         */
/*                database.                                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuDeleteAllType9LsasInCxt (tOspfCxt * pOspfCxt)
{
    tLsaInfo           *pLsaInfo;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pLsaNode;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        while ((pLsaNode =
                TMO_SLL_First (&(pInterface->Type9OpqLSALst))) != NULL)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
            LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
        }

    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuAddToLsdb                                            */
/*                                                                           */
/* Description  : This procedure allocates a new lsa_info structure sets the */
/*                lsaId fields and adds it to the hash table.               */
/*                                                                           */
/* Input        : pArea                 : pointer to AREA                   */
/*                u1LsaType            : type of LSA                       */
/*                pLinkStateId        :  IP address                       */
/*                pAdvRtrId           : advertising router id             */
/*                pIface                 : pointer to the INTERFACE          */
/*                u1LSAStatus            :Indicates if the LSA is a          */
/*                                        NEW_LSA or NEW_LSA_INSTANCE        */
/*                                         or FLUSHED_LSA                    */
/*                pLsa                  : pointer to LSA                    */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA Info, if LSA successfully added             */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tLsaInfo    *
LsuAddToLsdb (tArea * pArea,
              UINT1 u1LsaType,
              tLINKSTATEID * pLinkStateId, tRouterId * pAdvRtrId,
              tInterface * pIface, UINT1 u1LSAStatus, UINT1 *pLsa)
{

    tLsaInfo           *pLsaInfo;

    if (LSA_INFO_ALLOC (pLsaInfo) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ADJACENCY_TRC,
                  pArea->pOspfCxt->u4OspfCxtId, "LSA Info Alloc Failure\n");

        return (NULL);
    }
    OS_MEM_SET ((char *) pLsaInfo, 0, sizeof (tLsaInfo));
    pLsaInfo->u1LsaFlushed = OSPF_FALSE;
    pLsaInfo->pOspfCxt = pArea->pOspfCxt;
    TMO_HASH_INIT_NODE (&(pLsaInfo->nextLsaInfoNode));
    TMO_SLL_Init_Node (&(pLsaInfo->nextLsaInfo));
    pLsaInfo->lsaId.u1LsaType = u1LsaType;
    IP_ADDR_COPY (pLsaInfo->lsaId.linkStateId, pLinkStateId);
    IP_ADDR_COPY (pLsaInfo->lsaId.advRtrId, pAdvRtrId);
    pLsaInfo->pArea = pArea;
    pLsaInfo->pInterface = pIface;
    pLsaInfo->u1LSAStatus = u1LSAStatus;
    pLsaInfo->u1FloodFlag = OSPF_FALSE;
    OsixGetSysTime ((tOsixSysTime *) & (pLsaInfo->u4LsaArrivalTime));
    TmrSetTimer (&(pLsaInfo->lsaAgingTimer), LSA_NORMAL_AGING_TIMER,
                 NO_OF_TICKS_PER_SEC * MAX_AGE);
    LsuInitialiseLsaRxmtInfo (pLsaInfo);

    pLsaInfo->u4RxmtCount = 0;

    if (u1LsaType == AS_EXT_LSA)
    {
        /* Add the Type5 LSA in the HashTable */
        LsuAddPrefixLsaInHashLst (pArea->pOspfCxt->pExtLsaHashTable, pLsaInfo,
                                  pLsa);

        /*Add the Type5 LSAs in RBTree */
        LsuAddOrDelLsaInRBTree (pLsaInfo, OSPF_ADD);
        return (pLsaInfo);
    }
    else
    {
        if (u1LsaType != TYPE11_OPQ_LSA)
        {

            TMO_HASH_Add_Node (pArea->pLsaHashTable,
                               &(pLsaInfo->nextLsaInfoNode),
                               LsuLsaHashFunc (u1LsaType,
                                               pLinkStateId, pAdvRtrId), NULL);
        }
    }

    LsuAddToSortLsaLst (pLsaInfo);

    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pArea->pOspfCxt->u4OspfCxtId,
              "New LSA Info Created\n");

    return (pLsaInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuAddToSortLsaLst                                    */
/*                                                                           */
/* Description  : Adds the given advertisement to the LSA list.              */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
LsuAddToSortLsaLst (tLsaInfo * pLsaInfo)
{

    tTMO_SLL           *pLsaLst;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pPrevNode;
    tLsaInfo           *pLstLsaInfo;

    if ((pLsaInfo->lsaId.u1LsaType == NETWORK_SUM_LSA) ||
        (pLsaInfo->lsaId.u1LsaType == ASBR_SUM_LSA))
    {
        if (RBTreeAdd (pLsaInfo->pArea->pSummaryLsaRoot, pLsaInfo)
            == RB_FAILURE)
        {
            OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                          "RBTreeAdd failed in LsuAddToSortLsaLst\r\n");
        }
        return;
    }

    switch (pLsaInfo->lsaId.u1LsaType)
    {

        case ROUTER_LSA:
            pLsaLst = &(pLsaInfo->pArea->rtrLsaLst);
            break;

        case NETWORK_LSA:
            pLsaLst = &(pLsaInfo->pArea->networkLsaLst);
            break;

        case NSSA_LSA:
            pLsaLst = &(pLsaInfo->pArea->nssaLSALst);
            break;

        case TYPE9_OPQ_LSA:
            pLsaLst = &(pLsaInfo->pInterface->Type9OpqLSALst);
            break;

        case TYPE10_OPQ_LSA:
            pLsaLst = &(pLsaInfo->pArea->Type10OpqLSALst);
            break;

        case TYPE11_OPQ_LSA:
            pLsaLst = &(pLsaInfo->pOspfCxt->Type11OpqLSALst);
            break;

        default:
            pLsaLst = NULL;
            break;
    }

    if (pLsaLst == NULL)
    {
        /* Lsa Type is unknown so return */
        return;
    }
    pPrevNode = NULL;

    TMO_SLL_Scan (pLsaLst, pLstNode, tTMO_SLL_NODE *)
    {

        pLstLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLstNode);

        if (UtilLsaIdComp (pLstLsaInfo->lsaId.u1LsaType,
                           pLstLsaInfo->lsaId.linkStateId,
                           pLstLsaInfo->lsaId.advRtrId,
                           pLsaInfo->lsaId.u1LsaType,
                           pLsaInfo->lsaId.linkStateId,
                           pLsaInfo->lsaId.advRtrId) == OSPF_GREATER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }

    TMO_SLL_Insert (pLsaLst, pPrevNode, &(pLsaInfo->nextLsaInfo));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuIsSelfOriginatedLsaInCxt                                */
/*                                                                           */
/* Description  : This function checks whether the lsa is self org or not.   */
/*                                                                           */
/* Input        : pLsHeader      : pointer to lsa header                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE   : if the lsa is self originated.               */
/*                OSPF_FALSE  : otherwise.                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
LsuIsSelfOriginatedLsaInCxt (tOspfCxt * pOspfCxt, tLsHeader * pLsHeader)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    if (UtilIpAddrComp (pLsHeader->advRtrId, pOspfCxt->rtrId) == OSPF_EQUAL)
    {
        return OSPF_TRUE;
    }
    if (pLsHeader->u1LsaType == NETWORK_LSA)
    {

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {

            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            if (IS_UNNUMBERED_PTOP_IFACE (pInterface) != OSPF_TRUE)
            {
                if (UtilIpAddrComp (pLsHeader->linkStateId,
                                    pInterface->ifIpAddr) == OSPF_EQUAL)
                {
                    return OSPF_TRUE;
                }
            }
        }
    }
    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  LsuFlushAllNonDefaultSelfOrgAseLsaInCxt                */
/*                                                                           */
/* Description  : This function flushes all the Non default self originated  */
/*                AS_external LSA's.                                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuFlushAllNonDefaultSelfOrgAseLsaInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4HashKey = 0;
    tArea              *pArea = NULL;
    tOsDbNode          *pOsDbNode = NULL;
    tLsaInfo           *pLsaInfo;
    tLsaInfo           *pType7LsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pLsaNode = NULL;

    TMO_HASH_Scan_Table (pOspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pOspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tOsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLstNode);
                if ((UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, pOspfCxt->rtrId)
                     == OSPF_EQUAL)
                    && !IS_NULL_IP_ADDR (pLsaInfo->lsaId.linkStateId))
                {

                    /* The following procedure does not actually delete the 
                     * lsa from the database. It only starts the aging timer
                     * with a value of one second.
                     */

                    LsuFlushLsaAfterRemovingFromDescLst (pLsaInfo);
                }
            }
        }
    }

    /* LSA's in NSSA Area will be flushed when Lsdb Limit reached 
     * only if the router is an ABR. */
    if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if (pArea->u4AreaType != NSSA_AREA)
            {
                continue;
            }

            TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
            {
                pType7LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                if ((UtilIpAddrComp (pType7LsaInfo->lsaId.advRtrId,
                                     pOspfCxt->rtrId) != OSPF_EQUAL) ||
                    IS_NULL_IP_ADDR (pType7LsaInfo->lsaId.linkStateId))
                {
                    continue;
                }

                OSPF_TRC2 (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                           "Type 7 LSA %x %x is Flushed\n",
                           OSPF_CRU_BMC_DWFROMPDU (pType7LsaInfo->lsaId.
                                                   linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (pType7LsaInfo->pLsa +
                                                   LS_HEADER_SIZE));

                LsuFlushLsaAfterRemovingFromDescLst (pType7LsaInfo);
            }
        }
    }

    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
              "All Non Default Self Org LSAs Flushed As Router Enters OvrFlw State\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuGenerateNonDefaultAsExtLsas                             */
/*                                                                           */
/* Description  : This function generates the Non default AS_external LSA's. */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsuGenerateNonDefaultAsExtLsasInCxt (tOspfCxt * pOspfCxt)
{
    tExtRoute          *pExtRoute;
    tArea              *pArea = NULL;
    tLsaInfo           *pType7LsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    UINT4               au4Key[2];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;

    /* There are some NSSA LSA's sent from the neighbor that might have been
     * translated into the backbone area. These LSA's might have been flushed
     * when LSDB limit is reached. Here, reoriginate those LSA's. */
    if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
    {
        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            if (pArea->u4AreaType != NSSA_AREA)
            {
                continue;
            }

            TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
            {
                pType7LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                /* If the Type 5 LSA is advertised by other router 
                 * skip the translation here. */
                if ((UtilIpAddrComp (pType7LsaInfo->lsaId.advRtrId,
                                     pOspfCxt->rtrId) == OSPF_EQUAL) ||
                    IS_NULL_IP_ADDR (pType7LsaInfo->lsaId.linkStateId))
                {
                    continue;
                }

                OSPF_TRC2 (OSPF_LSU_TRC, pOspfCxt->u4OspfCxtId,
                           "Flushed Type 5 LSA is regenerated by translating "
                           "Type 7 LSA %x %x to Type 5\n",
                           OSPF_CRU_BMC_DWFROMPDU (pType7LsaInfo->lsaId.
                                                   linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (pType7LsaInfo->pLsa +
                                                   LS_HEADER_SIZE));

                NssaType7LsaTranslation (pArea, pType7LsaInfo);
            }
        }
    }

    au4Key[0] = 0;
    au4Key[1] = 0;
    inParams.Key.pKey = (UINT1 *) au4Key;
    inParams.pRoot = pOspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPF_EXT_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;

    if (TrieGetFirstNode (&inParams,
                          &pAppSpecPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tExtRoute *) pAppSpecPtr;
            if (IS_NULL_IP_ADDR (pExtRoute->ipNetNum))
            {
                au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipNetNum));
                au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4Key;
                pLeafNode = inParams.pLeafNode;
                pExtRoute = NULL;
                continue;
            }

            /* We should originate both Type 5 and Type 7 LSA when leaving
             * overflow state. */
            RagAddRouteInAggAddrRangeInCxt (pOspfCxt, pExtRoute);

            au4Key[0] = OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipNetNum);
            au4Key[1] = OSPF_CRU_BMC_DWFROMPDU (pExtRoute->ipAddrMask);
            inParams.Key.pKey = (UINT1 *) au4Key;
            pLeafNode = inParams.pLeafNode;
            pExtRoute = NULL;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pAppSpecPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }
    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC, pOspfCxt->u4OspfCxtId,
              "Non Default ASExt Self Org LSAs Generated As Router Leaves "
              " OvrFlw State\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuPutTxTime                                               */
/*                                                                           */
/* Description  : This function timestamps the database copy of the LSAs that*/
/*                are transmitted with the current time                      */
/*                                                                           */
/* Input        : pLsuPkt      -  pointer to the LSU packet                  */
/*                pInterface   -  packet length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuPutTxTime (tCRU_BUF_CHAIN_HEADER * pLsuPkt, tInterface * pInterface)
{
    tLsHeader           lsHeader;
    tLsaInfo           *pLsaInfo;
    UINT2               u2NxtLsaStart;
    UINT2               u2LsaLen;
    UINT4               u4LsaCount;
    UINT1              *pLsa;
    UINT1               u1LsaType = 0;

    /* skip OSPF header */
    u2NxtLsaStart = OS_HEADER_SIZE + LSU_FIXED_PORTION_SIZE;

    /* get lsa count */
    OSPF_CRU_BMC_GET_4_BYTE (pLsuPkt, OS_HEADER_SIZE, u4LsaCount);

    while (u4LsaCount--)
    {

        /* get length of lsa */
        OSPF_CRU_BMC_GET_2_BYTE (pLsuPkt,
                                 (u2NxtLsaStart + LENGTH_OFFSET_IN_LS_HEADER),
                                 u2LsaLen);

        OSPF_CRU_BMC_GET_1_BYTE (pLsuPkt, (u2NxtLsaStart +
                                           TYPE_OFFSET_IN_LS_HEADER),
                                 u1LsaType);
        pLsa = (UINT1 *) pLsuPkt + u2NxtLsaStart;
        UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

        pLsaInfo = LsuSearchDatabase (lsHeader.u1LsaType,
                                      &(lsHeader.linkStateId),
                                      &(lsHeader.advRtrId),
                                      (UINT1 *) pInterface,
                                      (UINT1 *) pInterface->pArea);
        if (pLsaInfo != NULL)
        {
            /* timestamp with current time */
            OsixGetSysTime ((tOsixSysTime *) & (pLsaInfo->u4LsaTxTime));
        }

        u2NxtLsaStart += u2LsaLen;

    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuFloodOutLsaToArea                                       */
/*                                                                           */
/* Description  :                                                            */
/*                                                                           */
/* Input        : pFloodArea     : the area into which the lsa is to be      */
/*                                 flooded                                   */
/*                pLsaInfo       : LSA, which has to be flooded              */
/*                u1RxtFlag      : Flag to indicate whether to add to        */
/*                                  Rxmt List or Not                         */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None   .                                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuFloodOutLsaToArea (tArea * pFloodArea, tLsaInfo * pLsaInfo, UINT1 u1RxtFlag)
{
    tTMO_SLL_NODE      *pLstNbr;
    tTMO_SLL_NODE      *pLstIf;
    tInterface         *pInterface;
    tNeighbor          *pNbr = NULL;
    tLsHeader           lsHeader;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: LsuFloodOutASExtLsaToArea\n");

    /* extract ls header info */

    UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);

    TMO_SLL_Scan (&(pFloodArea->ifsInArea), pLstIf, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_LST (pLstIf);

        /* Only if lsa is changed flood it over the interface */
        if ((IS_DC_EXT_APPLICABLE_IF (pInterface)) &&
            (IS_DNA_LSA_PROCESS_CAPABLE (pFloodArea)))
        {
            continue;
        }

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNbr, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNbr);

            if (pNbr->u1NsmState < NBRS_EXCHANGE)
            {
                continue;
            }
            if (u1RxtFlag == OSPF_TRUE)
            {
                LsuAddToRxmtLst (pNbr, pLsaInfo);
            }
        }

        LsuAddToFloodUpdate (pInterface, pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuFloodOutASExtLsaToArea\n");
}

/*****************************************************************************/
/* Function     : LsuSendLsaInfoInfoToOpqApp                                */
/*                                                                           */
/* Description    This function Sends the lsa information to                 */
/*                Opaque application.                                        */
/*                                                                           */
/* Input        : pLsaInfo    : pointer to LSA info                          */
/*                u1LsaStatus : LSA Status                                   */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsuSendLsaInfoInfoToOpqApp (tLsaInfo * pLsaInfo, UINT1 u1LsaStatus)
{
    tAppInfo           *pAppInfo = NULL;
    UINT1               u1AppId;

    if ((pLsaInfo->lsaId.u1LsaType == ROUTER_LSA) ||
        (pLsaInfo->lsaId.u1LsaType == NETWORK_LSA))
    {
        for (u1AppId = 0; u1AppId < MAX_APP_REG; u1AppId++)
        {
            pAppInfo = APP_INFO_IN_CXT (pLsaInfo->pOspfCxt, u1AppId);

            if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
            {
                LsuFillLsaInfoToOpqApp (pLsaInfo, pAppInfo, u1LsaStatus);
            }
        }
    }
    else
    {
        pAppInfo =
            APP_INFO_IN_CXT (pLsaInfo->pOspfCxt,
                             pLsaInfo->lsaId.linkStateId[0]);

        if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
        {
            if (pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA)
            {
                COUNTER_OP (pAppInfo->Type9LSARcvd, 1);
            }
            else if (pLsaInfo->lsaId.u1LsaType == TYPE10_OPQ_LSA)
            {
                COUNTER_OP (pAppInfo->Type10LSARcvd, 1);
            }
            else
            {
                /* TYPE11_OPQ_LSA */
                COUNTER_OP (pAppInfo->Type11LSARcvd, 1);
            }
            LsuFillLsaInfoToOpqApp (pLsaInfo, pAppInfo, u1LsaStatus);
        }
    }
    return;
}

/*****************************************************************************/
/* Function     : LsuFillLsaInfoToOpqApp                                    */
/*                                                                           */
/* Description    This function Sends the neighbor  information to           */
/*                Opaque application.                                        */
/*                                                                           */
/* Input        : pLsaInfo    : Pointer to LSA info                          */
/*                pAppInfo    : Pointer to Opaque Application                */
/*                u1LsaStatus : LSA Status                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
LsuFillLsaInfoToOpqApp (tLsaInfo * pLsaInfo, tAppInfo * pAppInfo,
                        UINT1 u1LsaStatus)
{
    tOsToOpqApp         ospfTeMsg;
    VOID                (*fnptr) (tOsToOpqApp *) = NULL;

    /* If the Opaque application is registered
     * for recieving neighbor information then give it */
    if ((pAppInfo->u4InfoFromOSPF & OSPF_SELF_RTR_LSA_TO_OPQ_APP)
        || (pAppInfo->u4InfoFromOSPF & OSPF_NET_LSA_TO_OPQ_APP)
        || (pAppInfo->u4InfoFromOSPF & OSPF_OPQ_LSA_TO_OPQ_APP))
    {
        ospfTeMsg.u4MsgSubType = OSPF_TE_LSA_INFO;
        if (pLsaInfo->lsaId.u1LsaType == TYPE9_OPQ_LSA)
        {
            /* Fill the interface information */
            ospfTeMsg.u4IfIpAddr =
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pInterface->ifIpAddr);
            ospfTeMsg.u4AddrlessIf = pLsaInfo->pInterface->u4AddrlessIf;
            ospfTeMsg.u4AreaId =
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pInterface->pArea->areaId);
        }
        else
        {
            ospfTeMsg.u4AreaId =
                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->areaId);
        }
        /* Filling the link information */
        ospfTeMsg.lsaInfo.u1LsaStatus = u1LsaStatus;
        ospfTeMsg.lsaInfo.u2LsaLen = pLsaInfo->u2LsaLen;
        ospfTeMsg.lsaInfo.u1LsaType = pLsaInfo->lsaId.u1LsaType;
        ospfTeMsg.lsaInfo.pu1Lsa = pLsaInfo->pLsa;
        ospfTeMsg.lsaInfo.u4LinkStateId =
            OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        ospfTeMsg.lsaInfo.u4AdvRtrId =
            OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId);

        fnptr = (VOID (*)(tOsToOpqApp *)) pAppInfo->OpqAppCallBackFn;

        /* call the call back function to send message to
         * Opaque application */
        if (fnptr != NULL)
        {
            fnptr (&ospfTeMsg);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuPrefixLsaHashFunc                                       */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : LinkStateId  : link state id                               */
/*                ipAddrMask   : IpAddress Mask                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
LsuPrefixLsaHashFunc (tLINKSTATEID LinkStateId, tIPADDRMASK ipAddrMask)
{
    UINT1               au1Buf[2 * MAX_IPV4_ADDR_LEN];
    UINT4               u4HashIndex;

    OS_MEM_CPY (au1Buf, LinkStateId, MAX_IPV4_ADDR_LEN);
    OS_MEM_CPY (au1Buf + 4, ipAddrMask, MAX_IPV4_ADDR_LEN);

    u4HashIndex = UtilHashGetValue (EXT_LSA_HASH_TABLE_SIZE, au1Buf,
                                    2 * MAX_IPV4_ADDR_LEN);
    return u4HashIndex;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuAddPrefixLsaInHashLst                                   */
/*                                                                           */
/* Description  : This function adds External LSA  to a particular Hash Node */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
LsuAddPrefixLsaInHashLst (tTMO_HASH_TABLE * pLsaHashTable,
                          tLsaInfo * pLsaInfo, UINT1 *pLsa)
{
    UINT1               u1DbNodeFound = OSPF_FALSE;
    UINT4               u4HashIndex = 0;
    tIPADDRMASK         destIpAddrMask;
    tIPADDRMASK         DbdestIpAddrMask;
    tOsDbNode          *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tLsaInfo           *pDbLsaInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "ENTRY: LsuAddPrefixLsaInHashLst\n");

    OS_MEM_CPY (destIpAddrMask,
                (UINT1 *) (pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

    u4HashIndex = LsuPrefixLsaHashFunc (pLsaInfo->lsaId.linkStateId,
                                        destIpAddrMask);

    TMO_HASH_Scan_Bucket (pLsaHashTable, u4HashIndex, pOsDbNode, tOsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pDbLstNode);

        OS_MEM_CPY (DbdestIpAddrMask,
                    (UINT1 *) (pDbLsaInfo->pLsa + LS_HEADER_SIZE),
                    MAX_IP_ADDR_LEN);

        if ((MEMCMP (destIpAddrMask, DbdestIpAddrMask, MAX_IPV4_ADDR_LEN)
             == OSPF_EQUAL) &&
            (MEMCMP (pLsaInfo->lsaId.linkStateId,
                     pDbLsaInfo->lsaId.linkStateId, MAX_IPV4_ADDR_LEN)
             == OSPF_EQUAL))
        {
            u1DbNodeFound = OSPF_TRUE;
            break;
        }
    }                            /* End of scan of Hash Bucket */

    /* If Database node is not present, then Create a hash node */
    if (u1DbNodeFound == OSPF_FALSE)
    {
        if (OSPF_DB_NODE_ALLOC (pOsDbNode) == NULL)
        {
            OSPF_TRC (OSPF_CRITICAL_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "DB Node Alloc Failure\n");
            return;
        }

        /* Initialise the Hash node */
        TMO_SLL_Init (&(pOsDbNode->lsaLst));

        /* Adding the Created Hash node to Hash Table */
        TMO_HASH_Add_Node (pLsaHashTable,
                           &(pOsDbNode->NextDbNode), u4HashIndex, NULL);
    }

    TMO_SLL_Add (&(pOsDbNode->lsaLst), &(pLsaInfo->nextLsaInfo));

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuAddPrefixLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuDeletePrefixLsaInHashLst                                */
/*                                                                           */
/* Description  : This function adds External LSA  to a particular Hash Node */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
LsuDeletePrefixLsaInHashLst (tTMO_HASH_TABLE * pLsaHashTable,
                             tLsaInfo * pLsaInfo)
{
    UINT4               u4HashIndex = 0;
    tIPADDRMASK         destIpAddrMask;
    tIPADDRMASK         DbdestIpAddrMask;
    tOsDbNode          *pOsDbNode = NULL;
    tOsDbNode          *pOsNextDbNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pLstNextNode = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tLsaInfo           *pDbLsaInfo = NULL;
    tLsaInfo           *pLstLsaInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "ENTRY: LsuDeletePrefixLsaInHashLst\n");

    OS_MEM_CPY (destIpAddrMask,
                (UINT1 *) (pLsaInfo->pLsa + LS_HEADER_SIZE), MAX_IP_ADDR_LEN);

    u4HashIndex = LsuPrefixLsaHashFunc (pLsaInfo->lsaId.linkStateId,
                                        destIpAddrMask);
    OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
              pLsaInfo->pOspfCxt->u4OspfCxtId,
              "Perform TMO_HASH_DYN_Scan_Bucket\n");

    TMO_HASH_DYN_Scan_Bucket (pLsaHashTable, u4HashIndex, pOsDbNode,
                              pOsNextDbNode, tOsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo = OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pDbLstNode);

        OS_MEM_CPY (DbdestIpAddrMask,
                    (UINT1 *) (pDbLsaInfo->pLsa + LS_HEADER_SIZE),
                    MAX_IP_ADDR_LEN);

        if ((MEMCMP (destIpAddrMask, DbdestIpAddrMask, MAX_IPV4_ADDR_LEN)
             == OSPF_EQUAL) &&
            (MEMCMP (pLsaInfo->lsaId.linkStateId,
                     pDbLsaInfo->lsaId.linkStateId, MAX_IPV4_ADDR_LEN)
             == OSPF_EQUAL))
        {
            OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                      pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "Perform TMO_DYN_SLL_Scan\n");

            TMO_DYN_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, pLstNextNode,
                              tTMO_SLL_NODE *)
            {
                pLstLsaInfo =
                    OSPF_GET_BASE_PTR (tLsaInfo, nextLsaInfo, pLstNode);
                if (pLstLsaInfo == pLsaInfo)
                {
                    TMO_SLL_Delete (&(pOsDbNode->lsaLst),
                                    &(pLstLsaInfo->nextLsaInfo));
                    break;
                }
            }
            /* Check if Hash bucket is empty, if so, delete the bucket */
            if (TMO_SLL_Count (&(pOsDbNode->lsaLst)) == 0)
            {
                /* Delete the hash node and free its memory */
                TMO_HASH_Delete_Node (pLsaHashTable,
                                      &(pOsDbNode->NextDbNode), u4HashIndex);

                OSPF_DB_NODE_FREE (pOsDbNode);
            }
            break;
        }
    }                            /* End of scan of Hash Bucket */

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT: LsuDeletePrefixLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuAddOrDelLsaInRBTree                                     */
/*                                                                           */
/* Description  : This function adds or deletes the Type5/Type 11 lsas       */
/*                 to the RB Tree                                            */
/*                                                                           */
/*                                                                           */
/* Input        : pLsaInfo             : pointer to Lsa Info.                */
/*              : u1AddDelFlag         : OSPFV3_ADD or OSPFV3_DELETE.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if added or deleted successfully            */
/*                OSIX_FAILURE otherwise.                                  */
/*****************************************************************************/
PRIVATE UINT1
LsuAddOrDelLsaInRBTree (tLsaInfo * pLsaInfo, UINT1 u1AddDelFlag)
{
    tRBTree             pRBRoot = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "ENTER: LsuAddOrDelLsaInRBTree\n");

    pRBRoot = pLsaInfo->pOspfCxt->pAsExtLsaRBRoot;

    if (u1AddDelFlag == OSPF_ADD)
    {
        if (RBTreeAdd (pRBRoot, pLsaInfo) == RB_FAILURE)
        {
            OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC |
                      OSPF_CRITICAL_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "RB Tree Add Failure for LSA\n");
            return OSIX_FAILURE;
        }
        OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "LSA Node Added successfully in RB Tree \n");
    }
    else
    {
        if (RBTreeRem (pRBRoot, pLsaInfo) == NULL)
        {
            OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC |
                      OSPF_CRITICAL_TRC, pLsaInfo->pOspfCxt->u4OspfCxtId,
                      "RB Tree delete Failure for LSA\n");
            return OSIX_FAILURE;
        }

        OSPF_TRC (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                  pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "LSA Node deleted successfully from RB Tree \n");
    }

    OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "EXIT: V3LsuAddOrDelLsaInRBTree\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuGetExtLsaCount                                          */
/*                                                                           */
/* Description  : This function returns the Count of External LSAs           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Total No of External Lsa's                                 */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/
UINT4
LsuGetExtLsaCountInCxt (tOspfCxt * pOspfCxt)
{
    UINT4               u4Count = 0;
    RBTreeCount (pOspfCxt->pAsExtLsaRBRoot, &u4Count);
    return u4Count;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsuGetTranslatedLsaCount                                   */
/*                                                                           */
/* Description  : This function returns the Count of Type 7 LSA's that were  */
/*                translated into Type 5 LSA's.                              */
/*                                                                           */
/* Input        : pOspfCxt - Pointer to the OSPF Context                     */
/*                                                                           */
/* Output       : Total No of Type 7 LSA's that were translated into type 5  */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/
INT4
LsuGetTranslatedLsaCount (tOspfCxt * pOspfCxt)
{
    tArea              *pArea = NULL;
    tLsaInfo           *pType7LsaInfo = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    INT4                i4Count = 0;

    if (pOspfCxt->bAreaBdrRtr == OSPF_FALSE)
    {
        return i4Count;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        if (pArea->u4AreaType != NSSA_AREA)
        {
            continue;
        }

        TMO_SLL_Scan (&(pArea->nssaLSALst), pLsaNode, tTMO_SLL_NODE *)
        {
            pType7LsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

            if ((UtilIpAddrComp (pType7LsaInfo->lsaId.advRtrId,
                                 pOspfCxt->rtrId) == OSPF_EQUAL) ||
                IS_NULL_IP_ADDR (pType7LsaInfo->lsaId.linkStateId))
            {
                continue;
            }

            i4Count++;
        }
    }

    return i4Count;
}

/*----------------------------------------------------------------------------*/
/*                           End of the file oslsu.c                          */
/*----------------------------------------------------------------------------*/
