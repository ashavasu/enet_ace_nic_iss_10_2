/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osddp.c,v 1.20 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             database description process.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */
PRIVATE UINT1       gau1LinearBuf[MAX_OSPF_PKT_LEN];    /*This will be used for
                                                         * Processing Ddp Packet.*/
PRIVATE VOID DdpProcessDdp PROTO ((tCRU_BUF_CHAIN_HEADER * pDdPkt,
                                   UINT2 u2Len, tNeighbor * pNbr));
PRIVATE INT1 DdpIsEmptySummary PROTO ((tNeighbor * pNbr));
PRIVATE VOID DdpUpdateSummary PROTO ((tNeighbor * pNbr));
PRIVATE tTRUTHVALUE DdpAddToSummary
PROTO ((tNeighbor * pNbr, tLsaInfo * pLsaInfo, UINT1 *next_ls_header_start));
PRIVATE UINT2       DdpAddSummay (tNeighbor * pNbr);
/*****************************************************************************/
/*                                                                           */
/* Function     : DdpRcvDdp                                                */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.6                          */
/*                This procedure takes care of the processing of a received  */
/*                ddp. The options and sequence number fields are checked to */
/*                identify if the packet is a duplicate or the next in       */
/*                sequence or if there is a mismatch. If it is accepted as   */
/*                the next in sequence then the list of ls_headers in it are */
/*                processed further.                                         */
/*                                                                           */
/* Input        : pDdPkt            : the dd packet                        */
/*                u2Len              : the length of the packet             */
/*                pNbr               : the neighbor from whom the pkt was   */
/*                                      received                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DdpRcvDdp (tCRU_BUF_CHAIN_HEADER * pDdPkt, UINT2 u2Len, tNeighbor * pNbr)
{

    UINT1               u1Options;
    UINT1               u1Flags;
    UINT1               u1NegDone = OSPF_FALSE;
    tLSASEQNUM          ddSeqNum;
    UINT2               u2Ifmtu;
    UINT1               u1DupFlag = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = NULL;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: DdpRcvDdp\n");
    if (pNbr->u1NsmState < MAX_NBR_STATE)
    {
        OSPF_NBR_TRC4 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "Rcvd Pkt Type %s From Nbr %x.%d in state %s\n",
                       au1DbgNbrState[DD_PKT],
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf, au1DbgNbrState[pNbr->u1NsmState]);
    }

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;

    OSPF_CRU_BMC_GET_2_BYTE (pDdPkt, OS_HEADER_SIZE, u2Ifmtu);
    OSPF_CRU_BMC_GET_1_BYTE (pDdPkt, OPTIONS_OFFSET_IN_DDP, u1Options);
    OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pDdPkt, DDP_MASK_OFFSET, u1Flags);
    OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pDdPkt, DDP_SEQ_NUM_OFFSET, ddSeqNum);

    if ((u1Options == pNbr->lastRxDdp.u1Options) &&
        (u1Flags == pNbr->lastRxDdp.u1Flags) &&
        (ddSeqNum == pNbr->lastRxDdp.i4Ddseqno))
    {
        u1DupFlag = OSPF_TRUE;
        OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Duplicate DDP\n");
    }

    if (u2Ifmtu > (UINT2) pNbr->pInterface->u4MtuSize)
    {
        INC_DISCARD_DDP_CNT (pNbr->pInterface);

        OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "DDP Disc MTU Size Mismatch %d\n",
                       pNbr->pInterface->u4MtuSize);
        return;
    }

    if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface))
    {

        /* trying to suppress hellos in demand circuits */
        if (u1Options & DC_BIT_MASK)
        {
            pNbr->bHelloSuppression = OSPF_TRUE;
        }
        else
        {
            pNbr->bHelloSuppression = OSPF_FALSE;
        }
    }

    if ((pNbr->u1NsmState == NBRS_DOWN) || (pNbr->u1NsmState == NBRS_ATTEMPT))
    {

        INC_DISCARD_DDP_CNT (pNbr->pInterface);

        OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "DDP Disc Improper Nbr State %s\n",
                       au1DbgNbrState[pNbr->u1NsmState]);

        return;
    }

    if (pNbr->u1NsmState == NBRS_INIT)
    {

        OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Two way state sync is received for Neighbour with address = %s\n",
                   OspfPrintIpAddr (pNbr->nbrIpAddr));
        GENERATE_NBR_EVENT (pNbr, NBRE_2WAY_RCVD);

        /* continue further in the new state */

    }

    if (pNbr->u1NsmState == NBRS_2WAY)
    {

        OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "DDP Disc Nbr State Already %s\n",
                       au1DbgNbrState[pNbr->u1NsmState]);

        INC_DISCARD_DDP_CNT (pNbr->pInterface);

        return;
    }

    if (pNbr->u1NsmState == NBRS_EXSTART)
    {

        OSPF_NBR_TRC3 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "Flag: %d  , Nbr_ID: %x,  Rtr_ID: %x \n", u1Flags,
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId),
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->pInterface->pArea->
                                               pOspfCxt->rtrId));

        OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "DD seq No: %d,  pNbr.seqNo: %d\n", ddSeqNum,
                       pNbr->dbSummary.seqNum);

        if ((u1Flags & I_BIT_MASK) &&
            (u1Flags & M_BIT_MASK) &&
            (u1Flags & MS_BIT_MASK) &&
            (u2Len == (OS_HEADER_SIZE + DDP_FIXED_PORTION_SIZE)) &&
            (UtilIpAddrComp (pNbr->nbrId,
                             pNbr->pInterface->pArea->pOspfCxt->rtrId)
             == OSPF_GREATER))
        {

            OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Router %x Slave wrt Nbr %x\n",
                           OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId),
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

            pNbr->dbSummary.bMaster = OSPF_FALSE;
            u1NegDone = OSPF_TRUE;

        }
        else if (!(u1Flags & I_BIT_MASK) &&
                 !(u1Flags & MS_BIT_MASK) &&
                 (ddSeqNum == pNbr->dbSummary.seqNum) &&
                 (UtilIpAddrComp (pNbr->nbrId,
                                  pNbr->pInterface->pArea->pOspfCxt->rtrId)
                  == OSPF_LESS))
        {

            OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Router %x Master wrt Nbr %x\n",
                           OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId),
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));

            pNbr->dbSummary.bMaster = OSPF_TRUE;
            u1NegDone = OSPF_TRUE;
        }
        else
        {

            OSPF_NBR_TRC4 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "PktType:%s from nbraddr:%x addrlessif:%x,"
                           "in state:%s discarded.\n",
                           au1DbgPktType[DD_PKT],
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                           pNbr->u4NbrAddrlessIf,
                           au1DbgNbrState[pNbr->u1NsmState]);

            INC_DISCARD_DDP_CNT (pNbr->pInterface);

            OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "DDP Disc Improper Setting Of Flags %d \n", u1Flags);

            return;
        }
        if (u1NegDone == OSPF_TRUE)
        {

            pNbr->nbrOptions = u1Options;
            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Negotiation done for Neighbour with Address = %s\n",
                       OspfPrintIpAddr (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_NEG_DONE);

            /* 
             * if memory can not be allocated for the db_summary_list, NEG_DONE 
             * event will fail without updating nsm state to EXCHANGE. 
             */

            if (pNbr->u1NsmState == NBRS_EXCHANGE)
            {

                TmrDeleteTimer (&pNbr->dbSummary.ddTimer);
                pNbr->lastRxDdp.u1Options = u1Options;
                pNbr->lastRxDdp.u1Flags = u1Flags;
                pNbr->lastRxDdp.i4Ddseqno = ddSeqNum;
                DdpProcessDdp (pDdPkt, u2Len, pNbr);
            }
            else
            {

                pNbr->nbrOptions = 0;
                INC_DISCARD_DDP_CNT (pNbr->pInterface);

                OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                              pNbr,
                              pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              "DDP Disc Due To DB_SUM_LST Alloc Failure\n");

            }
        }
    }
    else if (pNbr->u1NsmState == NBRS_EXCHANGE)
    {
        if ((pNbr->dbSummary.bMaster == OSPF_TRUE) && (u1DupFlag == OSPF_TRUE))
        {
            INC_DISCARD_DDP_CNT (pNbr->pInterface);
            OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "DDP Disc Duplicate Rcvd By Master From Nbr In State %s\n",
                           au1DbgNbrState[pNbr->u1NsmState]);

        }
        else if ((pNbr->dbSummary.bMaster == OSPF_FALSE) &&
                 (u1DupFlag == OSPF_TRUE))
        {

            /* duplicate received by slave */

            OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Dup DDP Rcvd By Salve From Nbr In State %s"
                           " DDP To Be Rxmt \n",
                           au1DbgNbrState[pNbr->u1NsmState]);

            DdpRxmtDdp (pNbr);
        }

        else if (((pNbr->dbSummary.bMaster == OSPF_TRUE) &&
                  (u1Flags & MS_BIT_MASK)) ||
                 ((pNbr->dbSummary.bMaster == OSPF_FALSE) &&
                  !(u1Flags & MS_BIT_MASK)) ||
                 (u1Flags & I_BIT_MASK) || (pNbr->nbrOptions != u1Options))
        {

            OSPF_NBR_TRC3 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC |
                           OSPF_CRITICAL_TRC, pNbr,
                           pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "DDP Disc(OptionsMismatch) OldOptions %d Newoptions %d"
                           " Flags %d \n", pNbr->nbrOptions, u1Options,
                           u1Flags);
            OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Sequence number mismatch\n"
                       "Current Sequence number is %d and expected sequence number is %d or %d\n",
                       ddSeqNum, pNbr->dbSummary.seqNum,
                       pNbr->dbSummary.seqNum + 1);

            GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);
            INC_DISCARD_DDP_CNT (pNbr->pInterface);

        }
        else if (((pNbr->dbSummary.bMaster == OSPF_TRUE) &&
                  (ddSeqNum == pNbr->dbSummary.seqNum)) ||
                 ((pNbr->dbSummary.bMaster == OSPF_FALSE) &&
                  (ddSeqNum == (pNbr->dbSummary.seqNum + 1))))
        {
            pNbr->lastRxDdp.u1Options = u1Options;
            pNbr->lastRxDdp.u1Flags = u1Flags;
            pNbr->lastRxDdp.i4Ddseqno = ddSeqNum;
            DdpProcessDdp (pDdPkt, u2Len, pNbr);
        }
        else
        {
            OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Sequence number mismatch\n"
                       "Current Sequence number is %d and expected sequence number is %d or %d\n",
                       ddSeqNum, pNbr->dbSummary.seqNum,
                       pNbr->dbSummary.seqNum + 1);

            GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);
            INC_DISCARD_DDP_CNT (pNbr->pInterface);

            if (pNbr->u1NsmState < MAX_NBR_STATE)
            {
                OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                               pNbr,
                               pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                               "DDP Disc Duplicate Rcvd By Master From Nbr %x"
                               " in state %s \n",
                               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                               au1DbgNbrState[pNbr->u1NsmState]);
            }

        }
    }
    else if ((pNbr->u1NsmState == NBRS_LOADING) ||
             (pNbr->u1NsmState == NBRS_FULL))
    {

        if (((pNbr->dbSummary.bMaster == OSPF_TRUE) &&
             (u1Flags & MS_BIT_MASK)) ||
            ((pNbr->dbSummary.bMaster == OSPF_FALSE) &&
             !(u1Flags & MS_BIT_MASK)) ||
            (u1Flags & I_BIT_MASK) || (pNbr->nbrOptions != u1Options))
        {

            OSPF_NBR_TRC3 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "DDP Disc(Options mismatch) OldOptions %d Newoptions %d"
                           " Flags %d \n", pNbr->nbrOptions, u1Options,
                           u1Flags);

            OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Sequence number mismatch\n"
                       "Current Sequence number is %d and"
                       "expected sequence number is %d or %d\n",
                       ddSeqNum, pNbr->dbSummary.seqNum,
                       pNbr->dbSummary.seqNum + 1);
            GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);
            INC_DISCARD_DDP_CNT (pNbr->pInterface);

        }
        else if ((pNbr->dbSummary.bMaster == OSPF_TRUE) &&
                 (u1DupFlag == OSPF_TRUE))
        {

            INC_DISCARD_DDP_CNT (pNbr->pInterface);

            OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "DDP Disc Dup Rcvd By Master From Nbr %s in state %s \n",
                           pNbr->nbrIpAddr, au1DbgNbrState[pNbr->u1NsmState]);

        }
        else if ((pNbr->dbSummary.bMaster == OSPF_FALSE) &&
                 (u1DupFlag == OSPF_TRUE) &&
                 (pNbr->dbSummary.dbSummaryPkt != NULL))
        {

            /* duplicate received by slave and lask pkt is not freed */

            OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "DDP To Be Rxmted Dup Rcvd By Slave From Nbr %s"
                           " In State %s \n",
                           pNbr->nbrIpAddr, au1DbgNbrState[pNbr->u1NsmState]);

            DdpRxmtDdp (pNbr);
        }
        else
        {
            OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Sequence number mismatch\n"
                       "Current Sequence number is %d and expected sequence number is %d or %d\n",
                       ddSeqNum, pNbr->dbSummary.seqNum,
                       pNbr->dbSummary.seqNum + 1);

            GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);
            INC_DISCARD_DDP_CNT (pNbr->pInterface);

            OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                          pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "DDP Disc Due To SeqNoMisMatch \n");

        }
    }

    OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Rcvd DDP Processing Over\n");
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpRcvDdp\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpProcessDdp                                            */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.6                          */
/*                This procedure is invoked when the ddp is accepted as next */
/*                in sequence. This procedure considers the ls_headers in the*/
/*                packet and adds them to the link state request list if     */
/*                necessary.                                                 */
/*                                                                           */
/* Input        : pDdPkt            : the dd packet                        */
/*                u2Len              : the length of the packet             */
/*                pNbr               : the neighbor from whom the pkt was   */
/*                                      received                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
DdpProcessDdp (tCRU_BUF_CHAIN_HEADER * pDdPkt, UINT2 u2Len, tNeighbor * pNbr)
{

    UINT2               lsHdrCount;
    tLsaInfo           *pLsaInfo;
    tLsHeader           lsHeader;
    UINT1               u1Flags;
    tLSASEQNUM          ddSeqNum;
    INT4                i4LSACount = 0;
    tOspfCxt           *pOspfCxt = NULL;

    MEMSET (&lsHeader, 0, sizeof (tLsHeader));
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: DdpProcessDdp\n");

    OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "DDP Processing Start\n");

    OSPF_CRU_BMC_GET_1_BYTE (pDdPkt, FLAGS_OFFSET_IN_DDP, u1Flags);
    OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pDdPkt, DDP_SEQ_NUM_OFFSET, ddSeqNum);

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;
    lsHdrCount = (UINT2) ((u2Len - (OS_HEADER_SIZE + DDP_FIXED_PORTION_SIZE))
                          / LS_HEADER_SIZE);
    while (lsHdrCount--)
    {
        UtilExtractLsHeaderFromPkt (pDdPkt, &lsHeader, i4LSACount, 0);

        if (IS_OPQ_LSA (lsHeader.u1LsaType) &&
            (!((pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)
               && (IS_NBR_OPQ_CAPABLE (pNbr)))))
        {
            OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Sequence number mismatch \n"
                       "Current Sequence number is %d and expected sequence number is %d or %d\n",
                       ddSeqNum, pNbr->dbSummary.seqNum,
                       pNbr->dbSummary.seqNum + 1);
            GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);
            return;
        }

        if (!IS_VALID_LS_TYPE (lsHeader.u1LsaType) ||
            (((lsHeader.u1LsaType == AS_EXT_LSA) ||
              (lsHeader.u1LsaType == TYPE11_OPQ_LSA)) &&
             ((pNbr->pInterface->pArea->u4AreaType == STUB_AREA) ||
              (pNbr->pInterface->pArea->u4AreaType == NSSA_AREA))))

        {
            OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE : Sequence number mismatch\n"
                       "Current Sequence number is %d and expected sequence number is %d or %d\n",
                       ddSeqNum, pNbr->dbSummary.seqNum,
                       pNbr->dbSummary.seqNum + 1);
            GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);

            OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                          pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "DDP Processing Stopped Due To LSType Mismatch\n");
            return;

        }
        else
        {
            pLsaInfo = LsuSearchDatabase (lsHeader.u1LsaType,
                                          &(lsHeader.linkStateId),
                                          &(lsHeader.advRtrId),
                                          (UINT1 *) pNbr->pInterface,
                                          (UINT1 *) pNbr->pInterface->pArea);

            if ((pLsaInfo == NULL) ||
                (LsuCompLsaInstance (NULL, pLsaInfo, &lsHeader) == RCVD_LSA))
            {

                if (LrqAddToLsaReqLst (pNbr, &lsHeader) == OSPF_FALSE)
                {
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gi4OspfSysLogId,
                                  "DDP Processing Stopped "
                                  "Due To LRQ Alloc Failure"));
                    OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                               pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                               "NBRE: Sequence number mismatch\n"
                               "Current Sequence number is %d and"
                               "expected sequence number is %d or %d\n",
                               ddSeqNum, pNbr->dbSummary.seqNum,
                               pNbr->dbSummary.seqNum + 1);
                    GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);

                    OSPF_NBR_TRC (OS_RESOURCE_TRC,
                                  pNbr,
                                  pNbr->pInterface->pArea->pOspfCxt->
                                  u4OspfCxtId,
                                  "DDP Processing Stopped "
                                  "Due To LRQ Alloc Failure\n");
                    return;
                }
            }
        }
        /* Processing of received ddp complete */
        i4LSACount++;            /* FSAP2 */
    }

    /* Send ddp in response */

    if (pNbr->dbSummary.bMaster == OSPF_TRUE)
    {

        /* router is bMaster */
        pNbr->dbSummary.seqNum++;
        DdpUpdateSummary (pNbr);
        if ((DdpIsEmptySummary (pNbr) == OSPF_TRUE) &&
            (!(u1Flags & M_BIT_MASK)))
        {

            /* database exchange complete */
            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Exchange done for Neighbour with Address = %s\n",
                       OspfPrintIpAddr (pNbr->nbrIpAddr));
            GENERATE_NBR_EVENT (pNbr, NBRE_EXCHANGE_DONE);
            DdpClearSummary (pNbr);
        }
        else
        {
            /* Since the reception of a DDP packet with same seq num from the 
             * NBR indicates the Ack, the rxmt timer started for that packet is 
             * deleted from the list.
             */
            TmrDeleteTimer (&pNbr->dbSummary.ddTimer);
            DdpSendDdp (pNbr);
        }

    }
    else
    {

        /* router is slave */

        pNbr->dbSummary.seqNum = ddSeqNum;
        DdpUpdateSummary (pNbr);
        DdpSendDdp (pNbr);

        if ((DdpIsEmptySummary (pNbr) == OSPF_TRUE) &&
            (!(u1Flags & M_BIT_MASK)))
        {

            /* database exchange complete */
            OSPF_TRC4 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                       pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "NBRE: Exchange done for Neighbour wit Address = %d.%d.%d.%d\n",
                       pNbr->nbrIpAddr[0], pNbr->nbrIpAddr[1],
                       pNbr->nbrIpAddr[2], pNbr->nbrIpAddr[3]);
            GENERATE_NBR_EVENT (pNbr, NBRE_EXCHANGE_DONE);
            TmrSetTimer (&pNbr->dbSummary.ddTimer,
                         DD_LAST_PKT_LIFE_TIME_TIMER,
                         NO_OF_TICKS_PER_SEC *
                         (pNbr->pInterface->i4RtrDeadInterval));
        }
    }

    OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "DDP Contents Processing Over\n");
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpProcessDdp\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpIsEmptySummary                                       */
/*                                                                           */
/* Description  : This procedure returns OSPF_TRUE if the database summary   */
/*                list of this neighbor is empty and OSPF_FALSE otherwise.   */
/*                                                                           */
/* Input        : pNbr             : the nbr whose summary is to be checked */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if summary list is empty                        */
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT1
DdpIsEmptySummary (tNeighbor * pNbr)
{
    if ((pNbr->dbSummary.pLastLsaTxed == NULL) &&
        (pNbr->dbSummary.u2TxedLsaType == MAX_LSA_TYPE))
    {
        return (OSPF_TRUE);
    }
    else
    {
        return (OSPF_FALSE);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpUpdateSummary                                         */
/*                                                                           */
/* Description  : This procedure updates the top of the summary list so that */
/*                the contents of the previously transmitted ddp are removed */
/*                from the summary list. Also the DD pkt previously transmi- */
/*                tted is freed.                                             */
/*                                                                           */
/* Input        : pNbr             : the nbr whose summary is to be updated */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
DdpUpdateSummary (tNeighbor * pNbr)
{
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: DdpUpdateSummary\n");

    OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Database summary of nbr addr : %x addrless_if : %x.\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    pNbr->dbSummary.u2LastTxedSummaryLen = 0;
    if (pNbr->dbSummary.dbSummaryPkt != NULL)
    {

        UtilOsMsgFree ((tCRU_BUF_CHAIN_HEADER *)
                       (pNbr->dbSummary.dbSummaryPkt), NORMAL_RELEASE);
        pNbr->dbSummary.dbSummaryPkt = NULL;
    }

    OSPF_NBR_TRC (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Database Summary Updated\n");
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpUpdateSummary\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpBuildSummary                                          */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.3                          */
/*                This procedure builds the database summary list for this   */
/*                neighbor. This procedure is invoked whenever the neighbor  */
/*                enters state exchange.                                     */
/*                                                                           */
/* Input        : pNbr             : the nbr whose summary is to be built   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if summary successfully built                   */
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
DdpBuildSummary (tNeighbor * pNbr)
{
    pNbr->dbSummary.pLastLsaTxed = NULL;
    pNbr->dbSummary.u2TxedLsaType = ROUTER_LSA;
    return (OSPF_TRUE);
}

/*****************************************************************************/
/*                                                                          */
/* Function     : DdpAddSummay                                              */
/*                                                                          */
/* Description  : Reference : RFC-2178 section 10.3                         */
/*                This procedure builds the database summary list for the   */
/*                DDP Packet based on the Last trasmitted LSA               */
/*                                                                          */
/* Input        : pNbr         : the nbr whose summary is to be built       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Length of the constructed LSA headers                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT2
DdpAddSummay (tNeighbor * pNbr)
{
    UINT1               u1AddToSummary = OSPF_FALSE;
    UINT2               u2LsatoAdd = 0;
    UINT2               u2DdpLen = 0;
    UINT2               u2DigestLen;
    UINT4               u4MaxTxLen = 0;
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL           *pLsaLst;
    tTMO_SLL_NODE      *pLsaNode;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1              *pNextLsHeaderStart;

    OS_MEM_SET (gau1LinearBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;
    /* If all the Lsas are already transmitted, */
    if ((pNbr->dbSummary.pLastLsaTxed == NULL) &&
        (pNbr->dbSummary.u2TxedLsaType == MAX_LSA_TYPE))
    {
        return (u2DdpLen);
    }

    /* Packet size available for adding LSAs */
    u2DigestLen = DIGEST_LEN_IF_CRYPT (pNbr->pInterface);
    u4MaxTxLen = (IFACE_MTU (pNbr->pInterface)) - (DDP_FIXED_PORTION_SIZE +
                                                   OS_HEADER_SIZE + IP_HDR_LEN +
                                                   u2DigestLen);

    pNextLsHeaderStart = gau1LinearBuf;

    pNbr->dbSummary.dbSummaryLst = pNextLsHeaderStart;

    u2LsatoAdd = pNbr->dbSummary.u2TxedLsaType;
    pLsaInfo = pNbr->dbSummary.pLastLsaTxed;

    /* Add Lsas */
    do
    {
        if (u2LsatoAdd == NETWORK_SUM_LSA)
        {
            if (pLsaInfo == NULL)
            {
                pLsaInfo = (tLsaInfo *)
                    RBTreeGetFirst (pNbr->pInterface->pArea->pSummaryLsaRoot);
            }
            else
            {
                pLsaInfo =
                    (tLsaInfo *) RBTreeGetNext (pNbr->pInterface->pArea->
                                                pSummaryLsaRoot,
                                                (tRBElem *) pLsaInfo, NULL);
            }

            /*Add the Entries from Summary LSA Tree */
            if (pLsaInfo != NULL)
            {
                do
                {
                    if (DdpAddToSummary (pNbr, pLsaInfo,
                                         pNextLsHeaderStart) == OSPF_TRUE)
                    {
                        u2DdpLen += LS_HEADER_SIZE;
                        pNextLsHeaderStart += LS_HEADER_SIZE;
                        u4MaxTxLen -= LS_HEADER_SIZE;
                    }
                    /* If Pkt length is less than header size,
                       then return */
                    if (u4MaxTxLen < LS_HEADER_SIZE)
                    {

                        pNbr->dbSummary.pLastLsaTxed = pLsaInfo;
                        pNbr->dbSummary.u2TxedLsaType = u2LsatoAdd;

                        return (u2DdpLen);
                    }
                }
                while ((pLsaInfo = (tLsaInfo *) RBTreeGetNext
                        (pNbr->pInterface->pArea->pSummaryLsaRoot,
                         (tRBElem *) pLsaInfo, NULL)) != NULL);
            }
            /* If no more Summary LSAs,Go to external LSAs */
            u2LsatoAdd = AS_EXT_LSA;
        }
        else if (u2LsatoAdd == AS_EXT_LSA)
        {
            if ((pNbr->pInterface->pArea->u4AreaType == STUB_AREA) ||
                (pNbr->pInterface->pArea->u4AreaType == NSSA_AREA) ||
                (pNbr->pInterface->u1NetworkType == IF_VIRTUAL))
            {
                u2LsatoAdd = NSSA_LSA;
            }
            else
            {

                if (pLsaInfo == NULL)
                {
                    pLsaInfo = (tLsaInfo *)
                        RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot);
                }
                else
                {
                    pLsaInfo = (tLsaInfo *) RBTreeGetNext
                        (pOspfCxt->pAsExtLsaRBRoot, (tRBElem *) pLsaInfo, NULL);
                }

                /*Add the Entries from External DataBase */
                if (pLsaInfo != NULL)
                {
                    do
                    {
                        if (DdpAddToSummary (pNbr, pLsaInfo,
                                             pNextLsHeaderStart) == OSPF_TRUE)
                        {
                            u2DdpLen += LS_HEADER_SIZE;
                            pNextLsHeaderStart += LS_HEADER_SIZE;
                            u4MaxTxLen -= LS_HEADER_SIZE;
                        }
                        /* If Pkt length is less than header size, 
                         * then return */
                        if (u4MaxTxLen < LS_HEADER_SIZE)
                        {
                            pNbr->dbSummary.pLastLsaTxed = pLsaInfo;
                            pNbr->dbSummary.u2TxedLsaType = u2LsatoAdd;

                            return (u2DdpLen);
                        }
                    }
                    while ((pLsaInfo = (tLsaInfo *) RBTreeGetNext
                            (pOspfCxt->pAsExtLsaRBRoot,
                             (tRBElem *) pLsaInfo, NULL)) != NULL);
                }
                u2LsatoAdd = NSSA_LSA;
            }
        }
        else
        {
            switch (u2LsatoAdd)
            {
                case ROUTER_LSA:
                    pLsaLst = &(pNbr->pInterface->pArea->rtrLsaLst);
                    break;

                case NETWORK_LSA:
                    pLsaLst = &(pNbr->pInterface->pArea->networkLsaLst);
                    break;

                case NSSA_LSA:
                    pLsaLst = &(pNbr->pInterface->pArea->nssaLSALst);
                    break;

                case TYPE9_OPQ_LSA:
                    pLsaLst = &(pNbr->pInterface->Type9OpqLSALst);
                    break;

                case TYPE10_OPQ_LSA:
                    pLsaLst = &(pNbr->pInterface->pArea->Type10OpqLSALst);
                    break;

                case TYPE11_OPQ_LSA:
                    pLsaLst = &(pOspfCxt->Type11OpqLSALst);
                    break;

                default:
                    (u2LsatoAdd)++;
                    continue;

            }

            if (u2LsatoAdd == TYPE11_OPQ_LSA)
            {
                if ((pNbr->pInterface->pArea->u4AreaType == STUB_AREA)
                    || (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
                    || (pNbr->pInterface->pArea->u4AreaType == NSSA_AREA))
                {
                    break;
                }
            }

            u1AddToSummary = OSPF_FALSE;

            TMO_SLL_Scan (pLsaLst, pLsaNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);

                /* Move till the Last Txed LSA is reached */
                if (pLsaInfo == pNbr->dbSummary.pLastLsaTxed)
                {
                    u1AddToSummary = OSPF_TRUE;
                    continue;
                }
                /* If the LSA type is different from the Last
                   transmitted one,Keep adding the LSAs */
                if ((pNbr->dbSummary.pLastLsaTxed != NULL) &&
                    (u2LsatoAdd == pNbr->dbSummary.u2TxedLsaType) &&
                    (u1AddToSummary == OSPF_FALSE))
                {
                    continue;
                }

                if (DdpAddToSummary (pNbr, pLsaInfo,
                                     pNextLsHeaderStart) == OSPF_TRUE)
                {
                    u2DdpLen += LS_HEADER_SIZE;
                    pNextLsHeaderStart += LS_HEADER_SIZE;
                    u4MaxTxLen -= LS_HEADER_SIZE;
                }

                /* If Pkt length is less than header size, then return */
                if (u4MaxTxLen < LS_HEADER_SIZE)
                {
                    break;
                }
            }

            if (pLsaNode == NULL)
            {
                pLsaInfo = NULL;
                if (u2LsatoAdd == NSSA_LSA)
                {
                    if (!
                        (IS_NBR_OPQ_CAPABLE (pNbr)
                         && (pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)))
                    {
                        u2LsatoAdd = MAX_LSA_TYPE;
                        break;
                    }
                }

                if (u2LsatoAdd == MAX_LSA_TYPE)
                {
                    break;
                }
                (u2LsatoAdd)++;
            }
        }

    }
    while ((u4MaxTxLen > LS_HEADER_SIZE) && (u2LsatoAdd <= MAX_LSA_TYPE));

    pNbr->dbSummary.pLastLsaTxed = pLsaInfo;
    pNbr->dbSummary.u2TxedLsaType = u2LsatoAdd;

    return (u2DdpLen);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpAddToSummary                                         */
/*                                                                           */
/* Description  : This routine adds the specified advertisement to the       */
/*                specified neighbour's summary list. If the specified ad is */
/*                of age MAX_AGE then adds it to the rxmt list and returns   */
/*                OSPF_FALSE. Otherwise adds the lsHeader of the advertise- */
/*                ment to the database summary list at point                 */
/*                next_ls_header_start and returns OSPF_TRUE.                */
/*                                                                           */
/* Input        : pNbr                  : nbr whose summary is constructed  */
/*                pLsaInfo             : the advt to be added to summary   */
/*                pNextLsHeaderStart : ptr to the summary being built    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_FALSE, if added to rxmt list                          */
/*                OSPF_TRUE, otherwise                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE             tTRUTHVALUE
DdpAddToSummary (tNeighbor * pNbr,
                 tLsaInfo * pLsaInfo, UINT1 *pNextLsHeaderStart)
{

    UINT2               u2LsaAge;
    UINT1              *pLsaAge;

    OSPF_TRC (OSPF_FN_ENTRY, pLsaInfo->pOspfCxt->u4OspfCxtId,
              "FUNC: DdpAddToSummary\n");

    GET_LSA_AGE (pLsaInfo, &(u2LsaAge));
    if (IS_MAX_AGE (u2LsaAge))
    {

        LsuAddToRxmtLst (pNbr, pLsaInfo);

        OSPF_TRC3 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                   pLsaInfo->pOspfCxt->u4OspfCxtId,
                   "MAX AGE Lsa_Type %d Link_State_Id %x Adv_Rtr_Id "
                   " %x Added To Rxmt Lst\n",
                   pLsaInfo->lsaId.u1LsaType,
                   OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                   OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

        OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpAddToSummary\n");
        return (OSPF_FALSE);
    }
    else
    {

        OS_MEM_CPY (pNextLsHeaderStart, pLsaInfo->pLsa, LS_HEADER_SIZE);

        /* update the age field appropriately in the summary list */

        pLsaAge = (UINT1 *) (pNextLsHeaderStart + AGE_OFFSET_IN_LS_HEADER);
        LADD2BYTE (pLsaAge, u2LsaAge);

        OSPF_TRC3 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                   pLsaInfo->pOspfCxt->u4OspfCxtId,
                   "Lsa_Type %d Link_State_Id %x Adv_Rtr_Id"
                   "%x Added To Database Summary\n",
                   pLsaInfo->lsaId.u1LsaType,
                   OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                   OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
        OSPF_TRC (OSPF_FN_EXIT, pLsaInfo->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpAddToSummary\n");
        return (OSPF_TRUE);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpClearSummary                                          */
/*                                                                           */
/* Description  : This procedure clears the database summary list of the     */
/*                specified neighbour.                                       */
/*                                                                           */
/* Input        : pArg           :  the nbr whose summary is to be cleared  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DdpClearSummary (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: DdpClearSummary\n");

    pNbr->dbSummary.u2LastTxedSummaryLen = 0;
    pNbr->dbSummary.pLastLsaTxed = NULL;
    pNbr->dbSummary.u2TxedLsaType = MAX_LSA_TYPE;

    if (pNbr->dbSummary.dbSummaryPkt != NULL)
    {

        UtilOsMsgFree (pNbr->dbSummary.dbSummaryPkt, NORMAL_RELEASE);
        pNbr->dbSummary.dbSummaryPkt = NULL;
    }

    /* Since the reception of a DDP packet with same seq num from the NBR
     * indicates the Ack, the rxmt timer started for that packet is
     * deleted from the list.
     */
    TmrDeleteTimer (&pNbr->dbSummary.ddTimer);

    OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Database Summary Lst of Nbr %x Addrless_if %x Cleared\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpClearSummary\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpRxmtDdp                                               */
/*                                                                           */
/* Description  : This procedure transmits the previously transmitted ddp to */
/*                the specified neighbor.                                    */
/*                                                                           */
/* Input        : pNbr             : the nbr to whom ddp is to be           */
/*                                    retransmitted                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DdpRxmtDdp (tNeighbor * pNbr)
{
    tLsHeader          *pLsHeader;
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: DdpRxmtDdp\n");

    if (pNbr->dbSummary.dbSummaryPkt == NULL)
    {
        return;
    }

    pLsHeader = (tLsHeader *) (pNbr->dbSummary.dbSummaryPkt +
                               OS_HEADER_SIZE + DDP_FIXED_PORTION_SIZE +
                               pNbr->dbSummary.u2LastTxedSummaryLen);
    if (pNbr->dbSummary.u2LastTxedSummaryLen)
    {
        if (IS_VIRTUAL_IFACE (pNbr->pInterface))
        {
            VifSendRxmtTrap (pNbr, pLsHeader, DD_PKT);
        }
        else
        {
            IfSendRxmtTrap (pNbr, pLsHeader, DD_PKT);
        }
    }

    UtilConstructHdr (pNbr->pInterface,
                      (tCRU_BUF_CHAIN_HEADER *)
                      (pNbr->dbSummary.dbSummaryPkt),
                      DD_PKT, (UINT2) (pNbr->dbSummary.u2LastTxedSummaryLen
                                       + OS_HEADER_SIZE +
                                       DDP_FIXED_PORTION_SIZE));

    PppSendPkt (pNbr->dbSummary.dbSummaryPkt,
                (UINT2) (OS_HEADER_SIZE + DDP_FIXED_PORTION_SIZE +
                         DIGEST_LEN_IF_CRYPT (pNbr->pInterface) +
                         pNbr->dbSummary.u2LastTxedSummaryLen),
                pNbr->pInterface, &(pNbr->nbrIpAddr), NO_RELEASE);

    OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "DDP Rxmt To Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpRxmtDdp\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DdpSendDdp                                               */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.8                          */
/*                This procedure constructs a ddp from the top of the        */
/*                database summary list and transmits it to the neighbor. If */
/*                this procedure is invoked in neighbor state exstart then   */
/*                this procedure transmits empty DDP with I, M, MS bits set. */
/*                This procedure also appropriately updates the previously   */
/*                transmitted ddp. seqNum is properly set before calling    */
/*                this proc for  other than init pkt.                        */
/*                                                                           */
/* Input        : pNbr             : the nbr to whom ddp is to be sent      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DdpSendDdp (tNeighbor * pNbr)
{

    UINT1               u1Flags;
    UINT1               u1Options;
    UINT2               u2Len = 0;
    UINT2               u2DigestLen = 0;
    INT1                i1TimerType = -1;
    UINT4               u4Interval;
    tOspfCxt           *pOspfCxt = NULL;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Ddp should be sent if the node is not active */
        return;
    }

    /* rfc 2178 */
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: DdpSendDdp\n");
    u2DigestLen = DIGEST_LEN_IF_CRYPT (pNbr->pInterface);

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;
    if (pNbr->u1NsmState == NBRS_EXSTART)
    {
        if ((pNbr->dbSummary.dbSummaryPkt =
             UtilOsMsgAlloc (OS_HEADER_SIZE + DDP_FIXED_PORTION_SIZE +
                             MD5_AUTH_DATA_LEN)) == NULL)
        {

            OSPF_NBR_TRC2 (OS_RESOURCE_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Alloc Failure For DDP Tx To Nbr %x.%d\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                           pNbr->u4NbrAddrlessIf);
            return;
        }

        pNbr->dbSummary.u2LastTxedSummaryLen = 0;

        /*  2178  */
        /* 2 bytes  filled with interface MTU  */
        if (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
        {
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                        OS_HEADER_SIZE, 0);
        }
        else
        {
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                        OS_HEADER_SIZE,
                                        (UINT2) pNbr->pInterface->u4MtuSize);
        }

        u1Options = pOspfCxt->rtrOptions | pNbr->pInterface->pArea->areaOptions;

        if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface))
        {
            u1Options |= pNbr->pInterface->ifOptions;
        }
        if (pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)
        {
            u1Options |= OPQ_BIT_MASK;
        }

        OSPF_CRU_BMC_ASSIGN_1_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                    DDP_OPTIONS_OFFSET, u1Options);

        /* To take care of first condition of ddp reception in ex-start
         * RFC 2178 sec - 10.6.
         */
        if (pNbr->dbSummary.bMaster == OSPF_FALSE)
        {
            OSPF_CRU_BMC_ASSIGN_1_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                        DDP_MASK_OFFSET,
                                        (I_BIT_MASK | M_BIT_MASK |
                                         MS_BIT_MASK));
        }
        /* To take care of second condition of ddp reception in ex-start
         * RFC 2178 sec - 10.6.
         */
        else
        {
            OSPF_CRU_BMC_ASSIGN_1_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                        DDP_MASK_OFFSET, (M_BIT_MASK));
        }
        OSPF_CRU_BMC_ASSIGN_4_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                    DDP_SEQ_NUM_OFFSET, pNbr->dbSummary.seqNum);

        i1TimerType = DD_INIT_RXMT_TIMER;

    }
    else if (pNbr->u1NsmState == NBRS_EXCHANGE)
    {
        if ((pNbr->dbSummary.dbSummaryPkt =
             UtilOsMsgAlloc (IFACE_MTU (pNbr->pInterface))) == NULL)
        {
            OSPF_NBR_TRC2 (OS_RESOURCE_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Alloc Failure For DDP Tx To Nbr %x.%d\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                           pNbr->u4NbrAddrlessIf);
            return;
        }

        /* 2178 - 2 bytes  filled with interface MTU  */
        if (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
        {
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                        OS_HEADER_SIZE, 0);
        }
        else
        {
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                        OS_HEADER_SIZE,
                                        (UINT2) pNbr->pInterface->u4MtuSize);
        }

        u1Options = pOspfCxt->rtrOptions | pNbr->pInterface->pArea->areaOptions;

        if (IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface))
        {
            u1Options |= pNbr->pInterface->ifOptions;
        }
        if (pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)
        {
            u1Options |= OPQ_BIT_MASK;
        }

        OSPF_CRU_BMC_ASSIGN_1_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                    DDP_OPTIONS_OFFSET, u1Options);

        OSPF_CRU_BMC_ASSIGN_4_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                    DDP_SEQ_NUM_OFFSET, pNbr->dbSummary.seqNum);
        u2Len = DdpAddSummay (pNbr);

        pNbr->dbSummary.u2LastTxedSummaryLen = u2Len;

        if (DdpIsEmptySummary (pNbr) == OSPF_TRUE)
        {
            u1Flags = ((pNbr->dbSummary.bMaster == OSIX_TRUE)
                       ? MS_BIT_MASK : 0);
        }
        else
        {
            u1Flags = (UINT1) (M_BIT_MASK |
                               ((pNbr->dbSummary.bMaster == OSIX_TRUE) ?
                                MS_BIT_MASK : 0));
        }

        OSPF_CRU_BMC_ASSIGN_1_BYTE (pNbr->dbSummary.dbSummaryPkt,
                                    DDP_MASK_OFFSET, u1Flags);

        OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pNbr->dbSummary.dbSummaryPkt,
                                           pNbr->dbSummary.dbSummaryLst,
                                           DDP_SUMMARY_LST_OFFSET, u2Len);
        if (pNbr->dbSummary.bMaster == OSPF_TRUE)
        {
            /* Since it is the master it starts timer for retransmission of the 
             * DDP Packet.
             */
            i1TimerType = DD_RXMT_TIMER;
        }
    }
    else
    {

        /* neighbor in state other than exchange or exstart */

        if (pNbr->u1NsmState < MAX_NBR_STATE)
        {
            OSPF_NBR_TRC1 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Procedure Invoked in Improper Nbr State %s\n",
                           au1DbgNbrState[pNbr->u1NsmState]);
        }
        return;
    }

    u2Len =
        (UINT2) CRU_BUF_Get_ChainValidByteCount (pNbr->dbSummary.dbSummaryPkt);
    UtilConstructHdr (pNbr->pInterface,
                      (tCRU_BUF_CHAIN_HEADER *) (pNbr->dbSummary.
                                                 dbSummaryPkt), DD_PKT, u2Len);

    u2Len += u2DigestLen;        /* 2178 add the digest len also */

    PppSendPkt (pNbr->dbSummary.dbSummaryPkt,
                (UINT2) (OS_HEADER_SIZE + DDP_FIXED_PORTION_SIZE +
                         pNbr->dbSummary.u2LastTxedSummaryLen +
                         u2DigestLen),
                pNbr->pInterface, &(pNbr->nbrIpAddr), NO_RELEASE);

    if (i1TimerType != -1)
    {

        if (pNbr->pInterface->u1NetworkType == IF_BROADCAST)
        {
            u4Interval = UtilJitter (pNbr->pInterface->u2RxmtInterval,
                                     OSPF_JITTER);
        }
        else
        {
            u4Interval = pNbr->pInterface->u2RxmtInterval;
        }
        TmrSetTimer (&pNbr->dbSummary.ddTimer, i1TimerType,
                     NO_OF_TICKS_PER_SEC * u4Interval);
    }
    OSPF_NBR_TRC2 (OSPF_DDP_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "DDP Txed To Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: DdpSendDdp\n");

}

/* -----------------------------------------------------------------------*/
/*                        End of the file  osddp.c                        */
/*------------------------------------------------------------------------*/
