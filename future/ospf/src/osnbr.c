/*********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osnbr.c,v 1.30 2017/03/10 12:45:35 siva Exp $
 *
 * Description:This file contains structures related to the 
 *             creation and deletion of neighbor structures.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */
#ifndef HIGH_PERF_RXMT_LST_WANTED
PRIVATE INT1 NbrAddToApTable PROTO ((tNeighbor * pNbr));
PRIVATE VOID NbrApMarkFree PROTO ((tNeighbor * pNbr));
#endif
PRIVATE VOID NbrAddToSortNbrLst PROTO ((tNeighbor * pNbr));
PRIVATE VOID NbrSetDefaultValues PROTO ((tNeighbor * pNbr));

PRIVATE VOID        NbrSendStateChgTrap
PROTO ((tNeighbor * pNbr, UINT1 u1PrevState));
PRIVATE VOID NbrGenerateRtrNetworkLsas PROTO ((tNeighbor * pNbr));

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrCreate                                                 */
/*                                                                           */
/* Description  : Creates a neighbour.                                       */
/*                                                                           */
/* Input        : pNbrIpAddr     : IP address of the neighbour            */
/*                pNbrRtrId      : neighbour's router id                  */
/*                u4NbrAddrlessIf   : used for address less interfaces       */
/*                pInterface       : neighbours interface                   */
/*                configStatus     : status flag indicating whether this    */
/*                                    is dynamically discovered or config-   */
/*                                    ured                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the created neighbour, if successfully created  */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tNeighbor   *
NbrCreate (tIPADDR * pNbrIpAddr,
           tRouterId * pNbrRtrId,
           UINT4 u4NbrAddrlessIf, tInterface * pInterface, UINT1 u1ConfigStatus)
{

    tNeighbor          *pNbr;

    if (pNbrIpAddr != NULL)
    {
        OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC : NbrCreate \n");
        OSPF_TRC2 (OSPF_NSM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Create nbr with IP addr  %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pNbrIpAddr),
                   u4NbrAddrlessIf);
    }

    if (NBR_ALLOC (pNbr) == NULL)
    {

        OSPF_TRC (OS_RESOURCE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Nbr Alloc Failure\n");

        return NULL;
    }
    NbrSetDefaultValues (pNbr);
    if (pNbrIpAddr != NULL)
    {
        IP_ADDR_COPY (pNbr->nbrIpAddr, *pNbrIpAddr);
    }
    pNbr->u4NbrAddrlessIf = u4NbrAddrlessIf;
    pNbr->pInterface = pInterface;
    pNbr->u1ConfigStatus = u1ConfigStatus;
    IP_ADDR_COPY (pNbr->nbrId, *pNbrRtrId);

    /* The nbr structure is added to the Neighbor RBTRee Table */
    if (RBTreeAdd (pInterface->pArea->pNbrTbl, pNbr) == RB_FAILURE)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId, "Nbr Tbl Full\n");

        NBR_FREE (pNbr);
        return NULL;
    }
    /* 
     * This nbr is added to the array of nbr pointers and the next free
     * element is updated. 
     */
#ifndef HIGH_PERF_RXMT_LST_WANTED
    if (NbrAddToApTable (pNbr) == OSPF_FAILURE)
    {

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId, "Nbr Tbl Full\n");

        NBR_FREE (pNbr);
        return NULL;
    }
#endif

    /* 
     * add the nbr to the sorted list of nbr maintained for SNMP purpose
     * only nbrs on non-virtual interfaces are added to this lst
     */

    if (pInterface->u1NetworkType != IF_VIRTUAL)
    {
        NbrAddToSortNbrLst (pNbr);
    }

    /* 
     * add the nbr to the list of nbrs in the attatched interface
     * eligible neighbors are inserted at the start of the list
     * ineligible neighbors are added to the end of the list
     */

    if (pNbr->u1NbrRtrPriority > INELIGIBLE_RTR_PRIORITY)
    {
        TMO_SLL_Insert (&(pInterface->nbrsInIf), NULL, &(pNbr->nextNbrInIf));
    }
    else
    {
        TMO_SLL_Add (&(pInterface->nbrsInIf), &(pNbr->nextNbrInIf));
    }

    if (u1ConfigStatus == DISCOVERED_NBR)
    {

        /* 
         * If interface type is NBMA and if interface is already up
         * generate event NBRE_START.
         */

        if ((pInterface->u1NetworkType == IF_NBMA) &&
            (pInterface->u1IsmState > IFS_DOWN))
        {
            GENERATE_NBR_EVENT (pNbr, NBRE_START);
        }
        if ((pInterface->u1NetworkType == IF_PTOP) &&
            (pInterface->pArea->pOspfCxt->u1OpqCapableRtr == OSPF_TRUE))
        {
            NbrSendNbrInfoInfoToOpqApp (pNbr);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT : NbrCreate \n");

    return (pNbr);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrActivate                                               */
/*                                                                           */
/* Description  : This generates the Nbr start event for the configured      */
/*                nbr if the if_state is greater than DOWN.                  */
/*                                                                           */
/* Input        : pNbr            : neighbour to be activated               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
NbrActivate (tNeighbor * pNbr)
{
    if (pNbr->pInterface->u1IsmState > IFS_DOWN)
    {
        GENERATE_NBR_EVENT (pNbr, NBRE_START);
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrInactivate                                             */
/*                                                                           */
/* Description  : This generates the Nbr DOWN  event for the configured nbr. */
/*                                                                           */
/* Input        : pNbr            : neighbour to be inactivated             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
NbrInactivate (tNeighbor * pNbr)
{
    GENERATE_NBR_EVENT (pNbr, NBRE_LL_DOWN);
    return OSPF_SUCCESS;
}

#ifndef HIGH_PERF_RXMT_LST_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : NbrAddToApTable                                        */
/*                                                                           */
/* Description  : Adds the neighbour to the AP table.                        */
/*                                                                           */
/* Input        : pNbr            : neighbour to be added                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if neighbour successfully added                   */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT1
NbrAddToApTable (tNeighbor * pNbr)
{

    UINT2               u2Index;
    tNbrsTable         *p_ap_table;
    UINT4               u4MaxNbrs = 0;

    u4MaxNbrs = OSPF_MIN (MAX_OSPF_NBRS_LIMIT,
                          FsOSPFSizingParams[MAX_OSPF_NBRS_SIZING_ID].
                          u4PreAllocatedUnits);

    p_ap_table = &(pNbr->pInterface->pArea->pOspfCxt->nbrsTable);

    pNbr->i2NbrsTableIndex = p_ap_table->i2NextFreeElement;
    if (pNbr->i2NbrsTableIndex == -1)
    {

        OSPF_NBR_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Nbr Tbl Full\n");

        return OSPF_FAILURE;
    }
    p_ap_table->apNbr[pNbr->i2NbrsTableIndex] = pNbr;

    /* 
     * Update the next_free_element in the ap table. If table is
     * full then the next free element is set to -1.
     */

    p_ap_table->i2NextFreeElement = -1;
    for (u2Index = 0; u2Index < u4MaxNbrs; u2Index++)
    {

        if (p_ap_table->apNbr[u2Index] == (tNeighbor *) NULL)
        {
            p_ap_table->i2NextFreeElement = u2Index;
            break;
        }
    }

    OSPF_NBR_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Nbr Added To Nbr Tbl\n");

    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrApMarkFree                                           */
/*                                                                           */
/* Description  : The entry corresponding the specified nbr is marked free.  */
/*                If the previous next_free_element is -1 then it is set to  */
/*                index.                                                     */
/*                                                                           */
/* Input        : pNbr            : neighbour to be freed                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
NbrApMarkFree (tNeighbor * pNbr)
{
    tOspfCxt           *pOspfCxt = NULL;

    pOspfCxt = pNbr->pInterface->pArea->pOspfCxt;

    pOspfCxt->nbrsTable.apNbr[pNbr->i2NbrsTableIndex] = (tNeighbor *) NULL;
    if (pOspfCxt->nbrsTable.i2NextFreeElement == -1)
    {
        pOspfCxt->nbrsTable.i2NextFreeElement = pNbr->i2NbrsTableIndex;
    }
}

#endif
/*****************************************************************************/
/*                                                                           */
/* Function     : NbrDelete                                                 */
/*                                                                           */
/* Description  : The neighbor is deleted from the hash table and other      */
/*                lists. The associated summary list, request list and       */
/*                retransmission list are cleared. The associated timers are */
/*                disabled. The memory allocated for this neighbor structure */
/*                is freed.                                                  */
/*                                                                           */
/* Input        : pNbr            : neighbour to be deleted                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
NbrDelete (tNeighbor * pNbr)
{
    tIPADDR            *pSearchField;
    UINT4               u4OspfCxtId;

    u4OspfCxtId = pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId;

    if ((pNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        pSearchField = (tIPADDR *) (pNbr->nbrIpAddr);
    }
    else
    {
        pSearchField = (tIPADDR *) (pNbr->nbrId);
    }
    UNUSED_PARAM(pSearchField);
    /* DeRegistering BFD monitoring  */
    IfOspfBfdDeRegister (pNbr, pNbr->bIsBfdDisable);
    pNbr->bIsBfdDisable = OSIX_FALSE;

    RBTreeRem (pNbr->pInterface->pArea->pNbrTbl, pNbr);
    TMO_SLL_Delete (&(pNbr->pInterface->nbrsInIf), &(pNbr->nextNbrInIf));

    /* Virtual neighbors are not present in sort neighbor list */

    if (pNbr->pInterface->u1NetworkType != IF_VIRTUAL)
    {
        TMO_SLL_Delete (&(pNbr->pInterface->pArea->pOspfCxt->sortNbrLst),
                        &(pNbr->nextSortNbr));
    }

    pNbr->pInterface->pArea->pOspfCxt->pLastNbr = NULL;

    /* 
     * mark the entry corresponding to pNbr->u4_nbr_ap_ind 
     * as free in ap table.
     */
#ifndef HIGH_PERF_RXMT_LST_WANTED
    NbrApMarkFree (pNbr);
#endif
    NsmResetVariables (pNbr);
    TmrDeleteTimer (&pNbr->inactivityTimer);
    TmrDeleteTimer (&pNbr->helperGraceTimer);

    NBR_FREE (pNbr);

    OSPF_TRC (CONTROL_PLANE_TRC, u4OspfCxtId, "Nbr Deleted\n");
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrUpdateState                                           */
/*                                                                           */
/* Description  : The state of the neighbor is set to the specified value.   */
/*                                                                           */
/* Input        : pNbr            : the nbr whose state is to be updated    */
/*                u1State         : the new state of the neighbor           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NbrUpdateState (tNeighbor * pNbr, UINT1 u1State)
{
    UINT1               u1PrevState;
    UINT4               u4TmrInterval;

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: NbrUpdateState \n");

    if (u1State == pNbr->u1NsmState)
        return;
    u1PrevState = pNbr->u1NsmState;
    pNbr->u1NsmState = u1State;

    if (IS_DC_EXT_APPLICABLE_PTOP_IF (pNbr->pInterface) &&
        pNbr->u1NsmState >= NBRS_INIT &&
        u1PrevState < NBRS_INIT && pNbr->pInterface->u1IsmState == IFS_DOWN)
    {
        IfUpdateState (pNbr->pInterface, IFS_PTOP);

        TmrRestartTimer (&(pNbr->pInterface->helloTimer), HELLO_TIMER,
                         NO_OF_TICKS_PER_SEC *
                         (pNbr->pInterface->u2HelloInterval));
    }

    COUNTER_OP (pNbr->u4NbrEvents, 1);

    if ((pNbr->pInterface->u1NetworkType < MAX_IF_TYPE) &&
        (u1PrevState < MAX_NBR_STATE) && (pNbr->u1NsmState < MAX_NBR_STATE))
    {
        OSPF_NBR_TRC5 (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                       pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                       "Nbr %x.%d Changes State "
                       "IfType %s Old State %s New State %s\n",
                       OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                       pNbr->u4NbrAddrlessIf,
                       au1DbgIfType[pNbr->pInterface->u1NetworkType],
                       au1DbgNbrState[u1PrevState],
                       au1DbgNbrState[pNbr->u1NsmState]);
    }

    if ((u1PrevState == NBRS_EXCHANGE) || (u1PrevState == NBRS_LOADING))
    {
        pNbr->pInterface->pArea->u4XchgOrLoadNbrCount--;
    }
    if ((pNbr->u1NsmState == NBRS_EXCHANGE) ||
        (pNbr->u1NsmState == NBRS_LOADING))
    {
        pNbr->pInterface->pArea->u4XchgOrLoadNbrCount++;
    }
    if (pNbr->pInterface->pArea->u4XchgOrLoadNbrCount == 0)
    {
        LsuProcessDeletedLsa (pNbr->pInterface->pArea);
    }

    if ((u1PrevState == NBRS_FULL) && (pNbr->u1NsmState != NBRS_FULL))
    {
        pNbr->pInterface->pArea->u4FullNbrCount--;
        pNbr->pInterface->u4NbrFullCount--;
    }
    else if ((u1PrevState != NBRS_FULL) && (pNbr->u1NsmState == NBRS_FULL))
    {
        pNbr->pInterface->pArea->u4FullNbrCount++;
        pNbr->pInterface->u4NbrFullCount++;
    }

    if ((pNbr->pInterface->pArea->pOspfCxt->u1OspfRestartState == OSPF_GR_NONE)
        && (pNbr->u1NbrHelperStatus != OSPF_GR_HELPING))

    {
        if (((u1PrevState == NBRS_FULL) && (pNbr->u1NsmState != NBRS_FULL)) ||
            ((u1PrevState != NBRS_FULL) && (pNbr->u1NsmState == NBRS_FULL)))
        {
            NbrGenerateRtrNetworkLsas (pNbr);
        }
    }

    if (pNbr->u1NsmState == NBRS_EXCHANGE)
    {
        u4TmrInterval = NO_OF_TICKS_PER_SEC *
            (pNbr->pInterface->u2RxmtInterval);

        TmrSetTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer),
                     LSA_REQ_RXMT_TIMER,
                     ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ?
                      UtilJitter (u4TmrInterval, OSPF_JITTER) : u4TmrInterval));
    }
    else if (pNbr->u1NsmState == NBRS_FULL)
    {
        TmrDeleteTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer));
    }
    if ((u1PrevState >= NBRS_EXCHANGE) && (pNbr->u1NsmState < NBRS_EXCHANGE))
    {
        TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
    }
    if (((u1PrevState < NBRS_2WAY) &&
         (pNbr->u1NsmState >= NBRS_2WAY)) ||
        ((u1PrevState >= NBRS_2WAY) && (pNbr->u1NsmState < NBRS_2WAY)))
    {
        IsmSchedule (pNbr->pInterface, IFE_NBR_CHANGE);
    }

    /* BFD registration */
	if (pNbr->pInterface->u1BfdIfStatus == OSPF_BFD_ENABLED)
	{
		if ((pNbr->pInterface->u1NetworkType == IF_NBMA) ||
				(pNbr->pInterface->u1NetworkType == IF_BROADCAST))
		{
			if (pNbr->u1NsmState == NBRS_FULL)
			{
				IfOspfBfdRegister (pNbr);
			}
			else if ((pNbr->pInterface->u1IsmState == IFS_DR_OTHER) &&
					(pNbr->u1NsmState == NBRS_2WAY) &&
					(MEMCMP (pNbr->nbrId, pNbr->desgRtr, sizeof (tIPADDR))) &&
					(MEMCMP (pNbr->nbrId, pNbr->backupDesgRtr, sizeof (tIPADDR))))
			{
				IfOspfBfdRegister (pNbr);
			}
		}
		else
		{
			/* For interface type P2p and P2MP */
			if (pNbr->u1NsmState == NBRS_FULL)
			{
				IfOspfBfdRegister (pNbr);
			}
		}
	}
    /* Deregister when neighbor state is DOWN */
    if (pNbr->u1NsmState == NBRS_DOWN)
    {
	 IfOspfBfdDeRegister (pNbr, OSIX_FALSE);
    }

    NbrSendStateChgTrap (pNbr, u1PrevState);

    OSPF_NBR_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Nbr State Updated\n");
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT : NbrUpdateState \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrAddToSortNbrLst                                    */
/*                                                                           */
/* Description  : Adds the specified to the sortNbrLst.                    */
/*                                                                           */
/* Input        : pNbr             : neighbour to be added                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
NbrAddToSortNbrLst (tNeighbor * pNbr)
{

    tNeighbor          *pLstNbr;
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pPrevLstNode;

    pPrevLstNode = (tTMO_SLL_NODE *) 0;
    TMO_SLL_Scan (&(pNbr->pInterface->pArea->pOspfCxt->sortNbrLst),
                  pLstNode, tTMO_SLL_NODE *)
    {

        pLstNbr = GET_NBR_PTR_FROM_SORT_LST (pLstNode);
        if (UtilIpAddrIndComp (pLstNbr->nbrIpAddr,
                               pLstNbr->u4NbrAddrlessIf,
                               pNbr->nbrIpAddr,
                               pNbr->u4NbrAddrlessIf) == OSPF_GREATER)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pNbr->pInterface->pArea->pOspfCxt->sortNbrLst),
                    pPrevLstNode, &(pNbr->nextSortNbr));

    pNbr->pInterface->pArea->pOspfCxt->pLastNbr = NULL;

    OSPF_NBR_TRC (CONTROL_PLANE_TRC | OSPF_NSM_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Nbr Added To Sorted Nbr Lst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrProcessPriorityChange                                */
/*                                                                           */
/* Description  : This procedure processes the change in priority of a       */
/*                neighbor. It updates the eligible_nbr_lst in the interface */
/*                structure and schedules the ism with event NBR_CHANGE if   */
/*                necessary.                                                 */
/*                                                                           */
/* Input        : pNbr            : neighbour whose priority is to be       */
/*                                   changed                                 */
/*                u1NewPriority  : new priority                            */
/*                                                                           */
/* Output       : ISM_SHEDULED or ISM_NOT_SCHEDULED                          */
/*                                                                           */
/* Returns      : i4Flag                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
NbrProcessPriorityChange (tNeighbor * pNbr, UINT1 u1NewPriority)
{
    INT4                i4Flag;
    i4Flag = ISM_NOT_SCHEDULED;
    if (pNbr->u1NbrRtrPriority != u1NewPriority)
    {

        pNbr->u1NbrRtrPriority = u1NewPriority;

        if ((pNbr->pInterface->u1NetworkType == IF_NBMA) ||
            (pNbr->pInterface->u1NetworkType == IF_BROADCAST))
        {

            TMO_SLL_Delete (&(pNbr->pInterface->nbrsInIf),
                            &(pNbr->nextNbrInIf));

            /* 
             * add the nbr to the list of nbrs in the attatched interface
             * eligible neighbors are inserted at the start of the list
             * ineligible neighbors are added to the end of the list
             */

            if (pNbr->u1NbrRtrPriority > INELIGIBLE_RTR_PRIORITY)
            {
                TMO_SLL_Insert (&(pNbr->pInterface->nbrsInIf), NULL,
                                &(pNbr->nextNbrInIf));
            }
            else
            {
                TMO_SLL_Add (&(pNbr->pInterface->nbrsInIf),
                             &(pNbr->nextNbrInIf));
            }
        }
        i4Flag = ISM_SCHEDULED;
    }
    return i4Flag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrSetDefaultValues                                     */
/*                                                                           */
/* Description  : Initializes the neighbour.                                 */
/*                                                                           */
/* Input        : pNbr            : neighbour to be initialized             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
NbrSetDefaultValues (tNeighbor * pNbr)
{

    OS_MEM_SET (pNbr, 0, sizeof (tNeighbor));
    pNbr->u1NsmState = NBRS_DOWN;
    pNbr->u1NbrRtrPriority = DEF_NBR_PRIORITY;
    pNbr->u4HelloSuppressPtoMp = OSPF_FALSE;
    pNbr->nbrStatus = OSPF_VALID;
    pNbr->lastRxDdp.u1Options = 0;
    pNbr->lastRxDdp.u1Flags = 0;
    pNbr->lastRxDdp.i4Ddseqno = 0;
    pNbr->u4NbrCryptSeqNum = 0;
    pNbr->bHelloSuppression = OSPF_FALSE;
    TMO_SLL_Init_Node (&(pNbr->nextNbrInIf));
    TMO_SLL_Init_Node (&(pNbr->nextSortNbr));
    pNbr->lsaRxmtDesc.u4LsaRxmtCount = 0;
    pNbr->u1NbrHelperStatus = OSPF_GR_NOT_HELPING;
    pNbr->u1NbrHelperExitReason = OSPF_HELPER_NONE;
    MEMSET (&(pNbr->lastRcvHello.au1Pkt), 0, MAX_HELLO_SIZE);
    pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen = 0;
    IP_ADDR_COPY (pNbr->lastRcvHello.helloPktHdr.SrcIpAddr, gNullIpAddr);
    IP_ADDR_COPY (pNbr->lastRcvHello.helloPktHdr.IfIpAddr, gNullIpAddr);
    pNbr->lastRcvHello.helloPktHdr.u4AddrlessIf = 0;
    pNbr->lastRcvHello.helloPktHdr.u4ContextId = OSPF_INVALID_CXT_ID;

    TMO_SLL_Init (&(pNbr->lsaReqDesc.lsaReqLst));
    pNbr->lsaReqDesc.bLsaReqOutstanding = OSPF_FALSE;

#ifdef HIGH_PERF_RXMT_LST_WANTED
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE (pNbr->au1RxmtInfo,
                                     NBR_HEAD_PREV_DLL_INDEX,
                                     OSPF_INVALID_RXMT_INDEX);
    OSPF_SET_DLL_INDEX_IN_RXMT_NODE (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX,
                                     OSPF_INVALID_RXMT_INDEX);
#endif

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrSetArea                                               */
/*                                                                           */
/* Description  : Assigns the neighbours into new area.                      */
/*                                                                           */
/* Input        : pNbr           : pointer to Neighbour                     */
/*                pOldArea      : pointer to old area                      */
/*                pNewArea      : pointer to new area                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NbrSetArea (tNeighbor * pNbr, tArea * pOldArea, tArea * pNewArea)
{

    RBTreeRem (pOldArea->pNbrTbl, pNbr);
    if (RBTreeAdd (pNewArea->pNbrTbl, pNbr) == RB_FAILURE)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      " RBTree Add failed\r\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrCreateNbrsTable                                      */
/*                                                                           */
/* Description  : Initializes the Neighbour AP table which holds an array of */
/*                pointers to neighbour structures.                          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the neighbour AP table, on successful creation  */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NbrCreateNbrsTable (tOspfCxt * pOspfCxt)
{
#ifndef HIGH_PERF_RXMT_LST_WANTED
    UINT4               u4MaxNbrs = 0;
    UINT2               u2Index;

    u4MaxNbrs = OSPF_MIN (MAX_OSPF_NBRS_LIMIT,
                          FsOSPFSizingParams[MAX_OSPF_NBRS_SIZING_ID].
                          u4PreAllocatedUnits);

    pOspfCxt->nbrsTable.i2NextFreeElement = 0;
    for (u2Index = 0; u2Index < u4MaxNbrs; u2Index++)
    {
        pOspfCxt->nbrsTable.apNbr[u2Index] = (tNeighbor *) NULL;
    }
#else
    UNUSED_PARAM (pOspfCxt);
#endif
    OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID, "Nbr Table Created\n");

    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrSendStateChgTrap                                    */
/*                                                                           */
/* Description  : This routine is used to send the trap regarding the        */
/*                 Neighbour state change.                                   */
/*                                                                           */
/* Input        : pNbr         : pointer to the neighbour.                  */
/*                u1PrevState : previous state.                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
NbrSendStateChgTrap (tNeighbor * pNbr, UINT1 u1PrevState)
{
    tNbrStChgTrapInfo   trapNbrStChg;
    tVifOrVnbrStChgTrapInfo trapVnbrStChg;
    UINT1               u1NewState;
    u1NewState = pNbr->u1NsmState;
    if (!IS_INITIAL_IFACE_ACTIVITY_TRAP (pNbr->pInterface) &&
        ((u1PrevState >= NBRS_2WAY && u1NewState < NBRS_2WAY) ||
         (u1NewState == NBRS_2WAY || u1NewState == NBRS_FULL)))
    {

        if (!IS_VIRTUAL_IFACE (pNbr->pInterface) &&
            IS_TRAP_ENABLED_IN_CXT (NBR_STATE_CHANGE_TRAP,
                                    pNbr->pInterface->pArea->pOspfCxt))
        {
            IP_ADDR_COPY (trapNbrStChg.nbrIpAddr, pNbr->nbrIpAddr);
            trapNbrStChg.u4NbrAddrlessIf = pNbr->u4NbrAddrlessIf;
            IP_ADDR_COPY (trapNbrStChg.nbrId, pNbr->nbrId);
            trapNbrStChg.u1State = pNbr->u1NsmState;
            IP_ADDR_COPY (trapNbrStChg.rtrId,
                          pNbr->pInterface->pArea->pOspfCxt->rtrId);
            SnmpifSendTrapInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                 NBR_STATE_CHANGE_TRAP, &trapNbrStChg);
        }
        else
        {
            if (IS_VIRTUAL_IFACE (pNbr->pInterface) &&
                IS_TRAP_ENABLED_IN_CXT (VIRT_NBR_STATE_CHANGE_TRAP,
                                        pNbr->pInterface->pArea->pOspfCxt))
            {
                IP_ADDR_COPY (trapVnbrStChg.tranAreaId,
                              pNbr->pInterface->transitAreaId);
                IP_ADDR_COPY (trapVnbrStChg.virtNbr, pNbr->nbrId);
                trapVnbrStChg.u1State = pNbr->u1NsmState;
                IP_ADDR_COPY (trapVnbrStChg.rtrId,
                              pNbr->pInterface->pArea->pOspfCxt->rtrId);
                SnmpifSendTrapInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                     VIRT_NBR_STATE_CHANGE_TRAP,
                                     &trapVnbrStChg);
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrGenerateRtrNetworkLsas                                  */
/*                                                                           */
/* Description  : This routine is used to generate Router and Network        */
/*                LSAs when the Neighbor is changing to FULL state or        */
/*                changing from FULL state to some other state.              */
/*                RFC 2328 Section: 12.4.1                                   */
/*                                                                           */
/* Input        : pNbr         : pointer to the Neighbour.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
NbrGenerateRtrNetworkLsas (tNeighbor * pNbr)
{
    tArea              *pTransArea = NULL;
    UINT1               u1GenRtrLsaFlag = OSPF_FALSE;
    tLsaInfo           *pLsaInfo = NULL;

    /* RFC - 2328 -- Section: 12.4.1.4  */
    if (IS_PTOP_IFACE (pNbr->pInterface) || IS_PTOMP_IFACE (pNbr->pInterface))
    {
        u1GenRtrLsaFlag = OSPF_TRUE;
    }
    else
    {
        if (IS_VIRTUAL_IFACE (pNbr->pInterface))
        {
            pTransArea =
                GetFindAreaInCxt (pNbr->pInterface->pArea->pOspfCxt,
                                  &(pNbr->pInterface->transitAreaId));
        }
        if (pNbr->u1NsmState == NBRS_FULL)
        {
            if (IS_VIRTUAL_IFACE (pNbr->pInterface) && (pTransArea != NULL))
            {
                pTransArea->u4FullVirtNbrCount++;
                if (pTransArea->u4FullVirtNbrCount == 1)
                {
                    OlsGenerateLsa (pTransArea, ROUTER_LSA,
                                    &(pNbr->pInterface->pArea->pOspfCxt->rtrId),
                                    (UINT1 *) pNbr->pInterface->pArea);
                }
            }

            if (IS_DR (pNbr->pInterface) || IS_VIRTUAL_IFACE (pNbr->pInterface))
            {
                if (pNbr->pInterface->u4NbrFullCount == 1)
                {
                    u1GenRtrLsaFlag = OSPF_TRUE;
                }
            }
            else
            {
                if (UtilIpAddrComp ((pNbr->pInterface->desgRtr),
                                    pNbr->nbrIpAddr) == OSPF_EQUAL)
                {
                    u1GenRtrLsaFlag = OSPF_TRUE;
                }
            }
        }
        else
        {
            if (IS_VIRTUAL_IFACE (pNbr->pInterface) && (pTransArea != NULL))
            {
                pTransArea->u4FullVirtNbrCount--;
                if (pTransArea->u4FullVirtNbrCount == 0)
                {
                    OlsGenerateLsa (pTransArea, ROUTER_LSA,
                                    &(pNbr->pInterface->pArea->pOspfCxt->rtrId),
                                    (UINT1 *) pNbr->pInterface->pArea);
                }
            }

            if (IS_DR (pNbr->pInterface) || IS_VIRTUAL_IFACE (pNbr->pInterface))
            {
                if (pNbr->pInterface->u4NbrFullCount == 0)
                {
                    u1GenRtrLsaFlag = OSPF_TRUE;
                }
            }
            else
            {
                if (UtilIpAddrComp ((pNbr->pInterface->desgRtr),
                                    pNbr->nbrIpAddr) == OSPF_EQUAL)
                {
                    u1GenRtrLsaFlag = OSPF_TRUE;
                }
            }
        }
    }
    if (u1GenRtrLsaFlag == OSPF_TRUE)
    {
	    if(pNbr->pInterface->u1IsmState != IFS_DOWN)
	    {
		    OlsGenerateLsa (pNbr->pInterface->pArea, ROUTER_LSA,
				    &(pNbr->pInterface->pArea->pOspfCxt->rtrId),
				    (UINT1 *) pNbr->pInterface->pArea);
	    }
    }
    if (IS_DR (pNbr->pInterface))
    {
        if (((pLsaInfo = LsuSearchDatabase (NETWORK_LSA,
                                            &(pNbr->pInterface->ifIpAddr),
                                            &(pNbr->pInterface->pArea->
                                              pOspfCxt->rtrId), (UINT1 *) NULL,
                                            (UINT1 *) (pNbr->pInterface->
                                                       pArea))) != NULL)
            && (pLsaInfo->u1LsaFlushed == OSPF_TRUE))
        {
            if (pLsaInfo->pLsaDesc != NULL)
            {
                pLsaInfo->pLsaDesc->u1LsaChanged = OSPF_TRUE;
            }
            LsuDeleteNodeFromAllRxmtLst (pLsaInfo);
            pLsaInfo->u1LsaFlushed = OSPF_FALSE;
        }
        OlsGenerateLsa (pNbr->pInterface->pArea, NETWORK_LSA,
                        &(pNbr->pInterface->ifIpAddr),
                        (UINT1 *) pNbr->pInterface);
    }
}

/*****************************************************************************/
/* Function     : NbrSendNbrInfoInfoToOpqApp                                */
/*                                                                           */
/* Description    This function Sends the neighbor  information to           */
/*                Opaque application.                                        */
/*                                                                           */
/* Input        : pNbr : pointer to Neighbor                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
NbrSendNbrInfoInfoToOpqApp (tNeighbor * pNbr)
{
    tAppInfo           *pAppInfo = NULL;
    tInterface         *pInterface = NULL;
    VOID                (*fnptr) (tOsToOpqApp *) = NULL;
    tOsToOpqApp         ospfTeMsg;
    UINT1               u1AppId;

    pInterface = pNbr->pInterface;
    for (u1AppId = 0; u1AppId < MAX_APP_REG; u1AppId++)
    {
        pAppInfo = pNbr->pInterface->pArea->pOspfCxt->pAppInfo[u1AppId];

        if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
        {
            /* If the Opaque application is registered
             * for recieving neighbor information then give it */
            if (pAppInfo->u4InfoFromOSPF & OSPF_NBR_INFO_TO_OPQ_APP)
            {
                ospfTeMsg.u4MsgSubType = OSPF_TE_NBR_IP_ADDR_INFO;
                ospfTeMsg.u4IfIpAddr =
                    OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
                ospfTeMsg.u4AddrlessIf = pInterface->u4AddrlessIf;
                ospfTeMsg.u4AreaId =
                    OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId);
                ospfTeMsg.nbrInfo.u4NbrIpAddr =
                    OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr);
                fnptr = (VOID (*)(tOsToOpqApp *)) pAppInfo->OpqAppCallBackFn;
                /* call the call back function to send message to
                 * Opaque application */
                if (fnptr != NULL)
                {
                    fnptr (&ospfTeMsg);
                }
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : NbrUpdateNbrTable                                           */
/*                                                                           */
/* Description  : Updates the neighbours associated with a virtual link      */
/*                For a virtual link the interface index changes             */
/*                dynamically based on the route to the ABR                  */
/*                                                                           */
/* Input        : pNbr           : pointer to Neighbour                      */
/*                u4PrevIfIndex   : Old Interface index associated with the   */
/*                                 virtual link                              */
/*                u4CurrIfIndex   : New Interface index associated with the   */
/*                                 virtual link                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
NbrUpdateNbrTable (tNeighbor * pNbr, UINT4 u4PrevIfIndex, UINT4 u4CurrIfIndex)
{

#ifdef HIGH_PERF_RXMT_LST_WANTED
    UINT4               u4RxmtIndex = OSPF_INVALID_RXMT_INDEX;
    UINT4               u4NextRxmtIndex = OSPF_INVALID_RXMT_INDEX;
    tRxmtNode          *pRxmtNode = NULL;
#endif
    pNbr->pInterface->u4IfIndex = u4PrevIfIndex;
    RBTreeRem (pNbr->pInterface->pArea->pNbrTbl, pNbr);
    pNbr->pInterface->u4IfIndex = u4CurrIfIndex;
    if (RBTreeAdd (pNbr->pInterface->pArea->pNbrTbl, pNbr) == RB_FAILURE)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      " RBTree Add failed\r\n");

    }

#ifdef HIGH_PERF_RXMT_LST_WANTED
    /* Interface index of the neighbors formed over a virtual link changes,
     * whenever the route to the virtual neighbor is changed.
     * Interface index associated with a virtual neighbor is one of the keys
     * used to arrange RxmtNodes in the LSA retransmission RBTree.
     * So rearrange the rxmt nodes for this neighbor with new interface index
     */
    OSPF_GET_DLL_INDEX_IN_RXMT_NODE (pNbr->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX,
                                     u4RxmtIndex);

    if ((pNbr->lsaRxmtDesc.u4LsaRxmtCount != 0) &&
        (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX))
    {
        do
        {
            pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
            pNbr->pInterface->u4IfIndex = u4PrevIfIndex;
            /* Remove the Rxmt node added with old Interface index
             * associated with the Neighbor */
            RBTreeRem (gOsRtr.pRxmtLstRBRoot, pRxmtNode);
            pNbr->pInterface->u4IfIndex = u4CurrIfIndex;
            /* Add the Rxmt node with New Interface index
             * associated with the Neighbor */
            if (RBTreeAdd (gOsRtr.pRxmtLstRBRoot, pRxmtNode) == RB_FAILURE)
            {
                OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                              " RBTree Add failed\r\n");
            }
            OSPF_GET_DLL_INDEX_IN_RXMT_NODE (pRxmtNode->au1RxmtInfo,
                                             NBR_NEXT_DLL_INDEX,
                                             u4NextRxmtIndex);
            u4RxmtIndex = u4NextRxmtIndex;
        }
        while (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX);
    }
#endif
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osnbr.c                        */
/*-----------------------------------------------------------------------*/
