
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ospfcli.c,v 1.205 2018/01/18 09:56:26 siva Exp $
 *
 * Description: This file has the handle routines for cli SET/GET
 *              destined for OSPF  Module
 *
 *
 ********************************************************************/

#ifndef __OSPFCLI_C__
#define __OSPFCLI_C__

#include "ospfcli.h"
#include "osinc.h"
#include "cfa.h"
#include "osclipt.h"
#include "snmputil.h"
#include "stdoslow.h"
#include "fsostlow.h"
#include "rrdcli.h"

/* Global data in this file */
UINT1               LS_TYPE;
tIPADDR             LS_ID;
tIPADDR             LS_ADV_ROUTER_ID;
tIPADDR             LS_ROUTER_ID;
static UINT1        u1HeaderFlag = OSIX_FALSE;

PRIVATE VOID        CliPrintSwiCapType (tCliHandle CliHandle, UINT1 u1TlvVal1);
PRIVATE VOID        CliPrintMetricAndMetricType (tCliHandle CliHandle,
                                                 INT4 i4ProtoIndex,
                                                 UINT4 u4OspfCxtId);
PRIVATE INT4        OspfCliCheckIfDefaultValues (UINT4 u4IfIpAddr,
                                                 UINT4 u4OspfAddressLessIf);

/**************************************************************************/
/*  Function Name   : cli_process_ospf_cmd                                */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_ospf_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tAreaStruct         Area;
    tDatabase           Database;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4              *args[OSPF_CLI_MAX_ARGS];
    UINT1               au1AuthKey[MAX_AUTH_STRING_LEN];
    UINT1               au1ContextName[] = "default";
    INT1                argno = 0;
    UINT4               u4ErrCode;
    INT4                i4RespStatus = CLI_SUCCESS;
    INT4                i4MetricValue = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Index = 0;
    UINT4               u4Options = 0;
    UINT4               u4NbrId = 0;
    INT4                i4TagValue = 0;
    UINT4               u4TagValue = 0;
    INT4                i4Key = 0;
    INT4                i4Metric;
    INT4                i4MetricTos;
    INT4                i4MetricType;
    INT4                i4NbrPriority;
    INT4                i4PollInterval;
    INT4                i4SetSummaryTag;
    UINT1               SummaryArea;
    INT4                i4ShowOption = 0;
    INT4                i4ShowAllVlan = FALSE;
    INT4                i4AreaType = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT4               u4NextIfIndex;
    UINT4               u4AreaId;
    INT4                i4StatusVal;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4OspfPrevCxtId = OSPF_INVALID_CXT_ID;
    UINT2               u2ShowCmdFlag = OSPF_FALSE;
    UINT1              *pu1OspfCxtName = NULL;
    UINT1               u1PrintNbrHdr = TRUE;
    UINT1               u1ArgIndex;
    UINT1               u1VlanIdArgIndex;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1               au1TaskName[] = "OSPF";
    UINT1               au1IfName[IF_PREFIX_LEN];
    UINT1              *pu1TaskName;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    INT4                i4AdminStat;
    UINT4               u4OspfCxtIdFromIfIndex = 0;

    UNUSED_PARAM (u4OspfPrevCxtId);
    if (IssGetModuleSystemControl (OSPF_MODULE_ID) == MODULE_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r %% OSPF module is shutdown\r\n");
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, OspfLock, OspfUnLock);
    OspfLock ();

    if (OspfCliGetContext (CliHandle, u4Command, &u4OspfCxtId, &u4IfIndex,
                           &u2ShowCmdFlag) == CLI_FAILURE)
    {
        CLI_SET_CMD_STATUS (CLI_FAILURE);
        OspfUnLock ();
        CliUnRegisterLock (CliHandle);

        return CLI_FAILURE;
    }

    pu1TaskName = au1TaskName;
    va_start (ap, u4Command);

    /* second arguement is always (interface name/index) */
    u4InterfaceIndex = va_arg (ap, UINT4);

    /* If u2ShowCmdFlag is set and for router enable/disable the third 
     * argument is always context name */
    if (u2ShowCmdFlag == OSPF_TRUE || u4Command == OSPF_CLI_NO_ROUTEROSPF
        || u4Command == OSPF_CLI_ROUTEROSPF)
    {
        pu1OspfCxtName = va_arg (ap, UINT1 *);
        if (UtilOspfGetVcmSystemModeExt (OSPF_PROTOCOL_ID) == VCM_SI_MODE)
        {
            pu1OspfCxtName = au1ContextName;
        }
    }

    if (pu1OspfCxtName != NULL)
    {
        if ((i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                    pu1OspfCxtName,
                                                    &u4OspfCxtId)) ==
            CLI_FAILURE)
        {
            OspfUnLock ();
            CliUnRegisterLock (CliHandle);
            va_end (ap);
            return CLI_SUCCESS;
        }
    }

    if (nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat) == SNMP_SUCCESS)
    {
        if ((i4AdminStat == OSPF_DISABLED)
            && (u4Command != OSPF_CLI_ROUTEROSPF))
        {
            CliPrintf (CliHandle,
                       "\r %% OSPF process currently not running\r\n");
            OspfUnLock ();
            CliUnRegisterLock (CliHandle);
            va_end (ap);
            return CLI_SUCCESS;

        }
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store OSPF_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == OSPF_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    if (u2ShowCmdFlag == OSPF_TRUE)
    {
        switch (u4Command)
        {
            case OSPF_CLI_SHOW_INTERFACE:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        OspfCliShowInterfaceInCxt (CliHandle, u4OspfCxtId,
                                                   (INT4) u4InterfaceIndex);
                }
                break;
            }
            case OSPF_CLI_SHOW_VLAN_INTERFACE:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                    if (OspfCliGetCxtIdFromIfIndex
                        (CliHandle, u4InterfaceIndex,
                         &u4OspfCxtIdFromIfIndex) != CLI_FAILURE)
                    {
                        if (u4OspfCxtIdFromIfIndex != u4OspfCxtId)
                        {
                            CliPrintf (CliHandle,
                                       "\r%% Interface Index is not present in the Given Context ID\r\n");
                        }
                    }
                }
                MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          "vlan", *(INT4 *) args[0]);

                i4RespStatus =
                    OspfCliShowVlanInterfaceInCxt (CliHandle, pu1OspfCxtName,
                                                   au1IfName);
                break;
            }
            case OSPF_CLI_SHOW_NBR:
            {
                i4ShowOption = 0;

                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    i4ShowOption |= OSPF_SHOW_NBR_INTF;

                    /* Get the Context id from IfIndex if vrf name 
                     * is not specified */
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                u1ArgIndex = 0;
                /* args [0] ==> nbr Id */
                if (args[u1ArgIndex] != NULL)
                {
                    i4ShowOption |= OSPF_SHOW_NBR_ID;
                    u4NbrId = *args[u1ArgIndex];

                    /* Get the context Id from neighbor if vrf name and
                     * vlan are not specified */
                    if (pu1OspfCxtName == NULL && u4InterfaceIndex == 0)
                    {
                        i4RetVal = OspfCliGetCxtIdFromNeighborId (CliHandle,
                                                                  u4NbrId,
                                                                  &u4OspfCxtId);
                    }
                }
                else
                {
                    u4NbrId = IP_ANY_ADDR;
                }
                u1ArgIndex++;
                /* args [1] ==> nbr detail */
                if (args[u1ArgIndex] != NULL)
                {
                    i4ShowOption |= OSPF_SHOW_NBR_DETAIL;
                }
                u1ArgIndex++;
                /* args [2] ==> Print All the nbr detail
                 * for a given interface alias name */
                if (args[u1ArgIndex] != NULL)
                {
                    i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex]);
                }
                if (u4InterfaceIndex != 0)
                {
                    i4ShowOption |= OSPF_SHOW_NBR_INTF;

                    /* Get the Context id from IfIndex if vrf name 
                     * is not specified */
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }

                    if (i4RetVal != CLI_FAILURE)
                    {
                        i4RespStatus = OspfCliShowNeighborInCxt
                            (CliHandle, u4OspfCxtId,
                             u4InterfaceIndex, u4NbrId, i4ShowOption,
                             u1PrintNbrHdr);
                    }
                }
                else if ((u4InterfaceIndex == 0) && (i4ShowAllVlan == TRUE))
                {
                    i4ShowOption |= OSPF_SHOW_NBR_INTF;
                    u1ArgIndex++;
                    /* Vlan Id Arg Index ==> args [3] */
                    u1VlanIdArgIndex = u1ArgIndex;
                    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                              "vlan", *(INT4 *) args[u1VlanIdArgIndex]);

                    /* Print All the Nbrs on interfaces with
                     * IfAlias as "au1IfName"
                     */
                    u1PrintNbrHdr = TRUE;
                    while (CfaCliGetNextIfIndexFromName
                           (au1IfName, u4InterfaceIndex,
                            &u4NextIfIndex) != OSIX_FAILURE)
                    {
                        u4InterfaceIndex = u4NextIfIndex;
                        /* Get the Context id from IfIndex if vrf name 
                         * is not specified */
                        if (pu1OspfCxtName == NULL)
                        {
                            i4RetVal = OspfCliGetCxtIdFromIfIndex
                                (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                        }
                        i4RespStatus = OspfCliShowNeighborInCxt
                            (CliHandle, u4OspfCxtId, u4InterfaceIndex,
                             u4NbrId, i4ShowOption, u1PrintNbrHdr);
                        u1PrintNbrHdr = FALSE;

                    }

                }
                else
                {
                    /* case : u4InterfaceIndex == 0 && i4ShowAllVlan == FALSE */
                    if (i4ShowOption == 0)
                    {
                        i4ShowOption |= OSPF_SHOW_NBR_ALL;
                    }

                    if (i4RetVal == CLI_FAILURE)
                    {
                        i4RespStatus = CLI_FAILURE;
                    }
                    else if ((u4OspfCxtId != OSPF_INVALID_CXT_ID)
                             && (u4NbrId != 0))
                    {
                        /* Get the first context */
                        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) ==
                            OSPF_FAILURE)
                        {
                            CliUnRegisterAndRemoveLock (CliHandle);
                            return CLI_FAILURE;
                        }
                        do
                        {
                            /* Get the context pointer corresponding to the
                             * context structure */
                            pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
                            if (pOspfCxt == NULL)
                            {
                                break;
                            }
                            if ((nmhGetFsMIStdOspfAdminStat
                                 ((INT4) u4OspfCxtId,
                                  &i4AdminStat) != SNMP_FAILURE)
                                && (i4AdminStat != OSPF_DISABLED))
                            {
                                u1PrintNbrHdr = TRUE;
                                i4RespStatus = OspfCliShowNeighborInCxt
                                    (CliHandle, u4OspfCxtId,
                                     u4InterfaceIndex, u4NbrId, i4ShowOption,
                                     u1PrintNbrHdr);
                            }
                            u4OspfPrevCxtId = u4OspfCxtId;
                        }
                        while (UtilOspfGetNextCxtId
                               (u4OspfPrevCxtId, &u4OspfCxtId) != OSPF_FAILURE);
                    }
                    else
                    {
                        u1PrintNbrHdr = TRUE;
                        i4RespStatus = OspfCliShowNeighborInCxt
                            (CliHandle, u4OspfCxtId,
                             u4InterfaceIndex, u4NbrId, i4ShowOption,
                             u1PrintNbrHdr);
                    }
                }
                break;
            }
            case OSPF_CLI_SHOW_REQ_LSA:
            {
                u1ArgIndex = 0;
                u1ArgIndex++;
                /* args [1] ==> show options */
                i4ShowOption = CLI_PTR_TO_I4 (args[u1ArgIndex++]);

                /* args [2] ==> show all vlan intf */
                if (args[u1ArgIndex] != NULL)
                {
                    i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
                }

                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    /* Get the Context id from IfIndex if vrf name 
                     * is not specified */
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                else if (i4ShowAllVlan == TRUE)
                {
                    /* Vlan Id Arg Index ==> args[3]
                     */
                    u1VlanIdArgIndex = u1ArgIndex;
                    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                              "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                }
                if ((i4ShowOption == OSPF_CLI_SHOW_LSA_NBR) ||
                    (i4ShowOption == OSPF_CLI_SHOW_LSA_IFNBR))
                {
                    u4NbrId = *args[0];

                    /* Get the context Id from neighbor if vrf name and
                     * vlan are not specified */
                    if (pu1OspfCxtName == NULL && u4InterfaceIndex == 0)
                    {
                        i4RetVal = OspfCliGetCxtIdFromNeighborId (CliHandle,
                                                                  u4NbrId,
                                                                  &u4OspfCxtId);
                    }
                }

                if (i4RetVal != CLI_FAILURE)
                {
                    if (i4ShowAllVlan == TRUE)
                    {
                        i4RespStatus = CLI_FAILURE;
                    }
                    else
                    {
                        i4RespStatus = OspfCliShowRequestListInCxt
                            (CliHandle, u4OspfCxtId,
                             u4InterfaceIndex, u4NbrId, i4ShowOption);
                    }
                }
                else
                {
                    i4RespStatus = CLI_FAILURE;
                }
                break;
            }
            case OSPF_CLI_SHOW_TRANS_LSA:
            {
                u1ArgIndex = 0;
                u1ArgIndex++;
                /* args [1] ==> show options */
                i4ShowOption = CLI_PTR_TO_I4 (args[u1ArgIndex++]);

                /* args [2] ==> show all vlan intf */
                if (args[u1ArgIndex] != NULL)
                {
                    i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex++]);
                }

                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    /* Get the Context id from IfIndex if vrf name 
                     * is not specified */
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                else if (i4ShowAllVlan == TRUE)
                {
                    /* Vlan ID Arg Index ==> args[3]
                     */
                    u1VlanIdArgIndex = u1ArgIndex;
                    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                              "vlan", *(INT4 *) args[u1VlanIdArgIndex]);
                }
                if ((i4ShowOption == OSPF_CLI_SHOW_LSA_NBR) ||
                    (i4ShowOption == OSPF_CLI_SHOW_LSA_IFNBR))
                {
                    u4NbrId = *args[0];

                    /* Get the context Id from neighbor if vrf name and
                     * vlan are not specified */
                    if (pu1OspfCxtName == NULL && u4InterfaceIndex == 0)
                    {
                        i4RetVal = OspfCliGetCxtIdFromNeighborId (CliHandle,
                                                                  u4NbrId,
                                                                  &u4OspfCxtId);
                    }
                }

                if (i4RetVal != CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                    if (i4ShowAllVlan == TRUE)
                    {
                        i4RespStatus = OspfCliShowRxmtLtForAllVlanInCxt
                            (CliHandle, u4OspfCxtId,
                             u4InterfaceIndex, u4NbrId, i4ShowOption,
                             au1IfName);
                    }
                    else
                    {
                        i4RespStatus = OspfCliShowRxmtListInCxt
                            (CliHandle, u4OspfCxtId,
                             u4InterfaceIndex, u4NbrId, i4ShowOption);
                    }
                }
                break;
            }
            case OSPF_CLI_SHOW_VIRTUALLINKS:
            {
                /*Get CxtId from Cxtname */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowVirtualIfInCxt (CliHandle,
                                                              u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_SHOW_BORDERROUTERS:
            {
                /*Get CxtId from Cxtname */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowBorderRouterInCxt (CliHandle,
                                                                 u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_SHOW_SUMMARY_ADDRESS:
            {
                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowSummaryAddressInCxt (CliHandle,
                                                                   u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_SHOW_EXT_SUMMARY_ADDR:
            {
                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowExtSummaryAddressInCxt (CliHandle,
                                                                      u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_SHOW_OSPF:
            {
                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowOspfInfoInCxt (CliHandle,
                                                             u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_SHOW_ROUTETABLE:
            {
                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowRoutingTableInCxt (CliHandle,
                                                                 u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_SHOWDATABASE:
            {
                MEMSET (&Database, 0, sizeof (tDatabase));

                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (args[0] != NULL)
                {
                    Database.u4AreaId = *args[0];
                }
                if (args[2] != NULL)
                {
                    Database.u4AdvRouteId = *args[2];
                }
                Database.i4DatabaseOptions = CLI_PTR_TO_I4 (args[1]);

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowDatabaseInCxt (CliHandle,
                                                             Database,
                                                             u4OspfCxtId);
                }

                break;
            }
            case OSPF_CLI_SHOW_LSA_DATABASE:
            {
                MEMSET (&Database, 0, sizeof (tDatabase));

                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }

                if (args[0] != NULL)
                {
                    Database.u4AreaId = *args[0];
                }

                Database.i4LsdbType = CLI_PTR_TO_I4 (args[1]);
                Database.i4DatabaseOptions = CLI_PTR_TO_I4 (args[2]);

                if (args[3] != NULL)
                {
                    Database.u4LsId = *args[3];
                }
                if (args[4] != NULL)
                {
                    Database.u4AdvRouteId = *args[4];
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliShowDatabaseInCxt (CliHandle,
                                                             Database,
                                                             u4OspfCxtId);
                }

                break;
            }
            case OSPF_CLI_SHOW_REDUNDANCY:
            {
                i4RespStatus = OspfCliShowOspfRedInfo (CliHandle);
                break;
            }

#ifdef RM_WANTED
            case OSPF_CLI_SHOW_RED_EXT_RT_MSG:
            {
                i4RespStatus = OspfCliShowOspfRedExtRouteInfo (CliHandle);
                break;
            }
#endif

            case OSPF_CLI_SET_TRACE_LEVEL:
            {
                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                else
                {
                    u4OspfCxtId = OSPF_DEFAULT_CXT_ID;
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliSetTraceLevelInCxt
                        (CliHandle,
                         u4OspfCxtId, CLI_PTR_TO_I4 (args[0]),
                         CLI_PTR_TO_I4 (args[1]), CLI_ENABLE);
                }
                break;
            }
            case OSPF_CLI_RESET_TRACE_LEVEL:
            {
                /* Get the Context Id from the Context name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                else
                {
                    u4OspfCxtId = OSPF_DEFAULT_CXT_ID;
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliSetTraceLevelInCxt
                        (CliHandle,
                         u4OspfCxtId, CLI_PTR_TO_I4 (args[0]),
                         CLI_PTR_TO_I4 (args[1]), CLI_DISABLE);
                }
                break;
            }
            default:
                CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
                i4RespStatus = CLI_FAILURE;
                break;
        }
    }
    else
    {
        if ((u4Command == OSPF_CLI_ROUTEROSPF) ||
            (u4Command == OSPF_CLI_NO_ROUTEROSPF))
        {
            /* Get the Context Id from the Context name */
            if (pu1OspfCxtName != NULL)
            {
                i4RespStatus = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);

            }
            else
            {
                u4OspfCxtId = OSPF_DEFAULT_CXT_ID;
            }
        }

        if (i4RespStatus == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid ospf Context Id\r\n");
            CliUnRegisterLock (CliHandle);
            OspfUnLock ();
            return CLI_FAILURE;
        }

        if (((u4Command != OSPF_CLI_ROUTEROSPF) &&
             (u4Command != OSPF_CLI_NO_ROUTEROSPF)) &&
            (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE))
        {
            CliPrintf (CliHandle, "\r%% Invalid ospf Context Id\r\n");
            CliUnRegisterLock (CliHandle);
            OspfUnLock ();
            return CLI_FAILURE;
        }

        switch (u4Command)
        {
            case OSPF_CLI_ROUTEROSPF:
            {
                i4RespStatus = OspfCliSetAdminStatInCxt (CliHandle,
                                                         OSPF_ENABLED,
                                                         u4OspfCxtId);
                break;
            }
            case OSPF_CLI_NO_ROUTEROSPF:
            {
                i4RespStatus = OspfCliSetAdminStatInCxt (CliHandle,
                                                         OSPF_DISABLED,
                                                         u4OspfCxtId);
                break;
            }
            case OSPF_CLI_ROUTERID:
            {
                i4RespStatus = OspfCliSetRouterId (CliHandle, *args[0]);
                break;
            }
            case OSPF_CLI_NO_ROUTERID:
            {
                i4RespStatus = OspfCliDelRouterId (CliHandle, u4OspfCxtId);
                break;
            }
            case OSPF_CLI_STABILITY_INT:
            {
                i4RespStatus = OspfCliSetStabilityInterval (CliHandle,
                                                            *args[0],
                                                            *(INT4 *) args[1]);
                break;
            }
            case OSPF_CLI_DEF_STABILITY_INT:
            {
                i4RespStatus = OspfCliSetStabilityInterval (CliHandle,
                                                            *args[0],
                                                            NSSA_TRNSLTR_STBLTY_INTRVL);
                break;
            }
            case OSPF_CLI_TRANS_ROLE:
            {
                i4RespStatus = OspfCliSetTransRole (CliHandle,
                                                    *args[0],
                                                    CLI_PTR_TO_I4 (args[1]));
                break;
            }
            case OSPF_CLI_DEF_TRANS_ROLE:
            {
                i4RespStatus = OspfCliSetTransRole (CliHandle,
                                                    *args[0],
                                                    TRNSLTR_ROLE_CANDIDATE);
                break;
            }
            case OSPF_CLI_COMPATRFC1583:
            {
                i4RespStatus =
                    OspfCliSetRFC1583Compatibility (CliHandle, OSPF_ENABLED);
                break;
            }
            case OSPF_CLI_NO_COMPATRFC1583:
            {
                i4RespStatus =
                    OspfCliSetRFC1583Compatibility (CliHandle, OSPF_DISABLED);
                break;
            }
            case OSPF_CLI_SET_ABR_TYPE:
            {
                i4RespStatus =
                    OspfCliSetABRType (CliHandle, CLI_PTR_TO_I4 (args[0]));
                break;
            }
            case OSPF_CLI_NBR:
            {
                if (args[1] != NULL)
                {
                    i4NbrPriority = *(INT4 *) args[1];
                }
                else
                {
                    i4NbrPriority = 0;    /*CLI_OSPF_DEF_NBR_PRIORITY; */
                }
                i4RespStatus =
                    OspfCliSetNbrPriority (CliHandle,
                                           *args[0],
                                           i4NbrPriority,
                                           OSPF_CLI_DEF_ADDRLS_IFINDEX,
                                           OSPF_ENABLED);

                if (args[2] != NULL)
                {
                    i4PollInterval = *(INT4 *) args[2];
                    i4RetVal =
                        OspfCliSetIfPollInterval (CliHandle,
                                                  *args[0],
                                                  i4PollInterval,
                                                  OSPF_CLI_DEF_ADDRLS_IFINDEX,
                                                  OSPF_ENABLED);
                }

                if ((i4RespStatus == CLI_SUCCESS) && (i4RetVal == CLI_SUCCESS))
                {
                    i4RespStatus = CLI_SUCCESS;
                }

                break;
            }
            case OSPF_CLI_NO_NBR:
            {
                i4RespStatus = OspfCliDelNbr (CliHandle,
                                              *args[0],
                                              OSPF_CLI_DEF_ADDRLS_IFINDEX);
                break;
            }
            case OSPF_CLI_DEF_NBR_PARAMS:
            {
                i4NbrPriority = CLI_PTR_TO_I4 (args[1]);
                i4PollInterval = CLI_PTR_TO_I4 (args[2]);
                if (i4NbrPriority == 0)
                    args[1] = NULL;
                if (i4PollInterval == 0)
                    args[2] = NULL;

                if (args[1] != NULL)
                {
                    i4RespStatus =
                        OspfCliSetNbrPriority (CliHandle,
                                               *args[0],
                                               i4NbrPriority,
                                               OSPF_CLI_DEF_ADDRLS_IFINDEX,
                                               OSPF_DISABLED);
                }

                if (args[2] != NULL)
                {
                    i4RetVal =
                        OspfCliSetIfPollInterval (CliHandle,
                                                  *args[0],
                                                  i4PollInterval,
                                                  OSPF_CLI_DEF_ADDRLS_IFINDEX,
                                                  OSPF_DISABLED);
                }
                if ((i4RespStatus == CLI_SUCCESS) && (i4RetVal == CLI_SUCCESS))
                {
                    i4RespStatus = CLI_SUCCESS;
                }

                break;
            }
            case OSPF_CLI_AREA_DEFAULT_COST:
            {
                if (args[2] != NULL)
                {
                    i4MetricTos = *(INT4 *) args[2];
                }
                else
                {
                    i4MetricTos = OSPF_CLI_DEF_TOS;
                }
                i4RespStatus = OspfCliSetStubMetric (CliHandle,
                                                     *args[0],
                                                     *(INT4 *) args[1],
                                                     i4MetricTos, OSPF_FALSE);
                break;
            }
            case OSPF_CLI_DEF_AREACOST:
            {
                if (args[1] != NULL)
                {
                    i4MetricTos = *(INT4 *) args[1];
                    i4RespStatus =
                        OspfCliSetStubMetric (CliHandle,
                                              *args[0],
                                              CLI_OSPF_AS_EXT_DEF_METRIC,
                                              i4MetricTos, OSPF_TRUE);
                }
                else
                {
                    i4MetricTos = OSPF_CLI_DEF_TOS;
                    i4RespStatus =
                        OspfCliSetStubMetric (CliHandle, *args[0],
                                              CLI_OSPF_AS_EXT_DEF_METRIC,
                                              i4MetricTos, OSPF_FALSE);
                }
                break;
            }
            case OSPF_CLI_DEF_AREANSSA:
            {
                if (args[4] == NULL)
                {
                    if (args[1] != NULL)
                    {
                        i4Metric = *(INT4 *) args[1];
                    }
                    else
                    {
                        i4Metric = CLI_OSPF_AS_EXT_DEF_METRIC;
                    }
                    if (args[2] != NULL)
                    {
                        i4MetricType = *(INT4 *) args[2];
                    }
                    else
                    {
                        i4MetricType = OSPF_METRIC;
                    }

                    if (args[3] != NULL)
                    {
                        i4MetricTos = *(INT4 *) args[3];
                    }
                    else
                    {
                        i4MetricTos = OSPF_CLI_DEF_TOS;
                    }

                    i4RespStatus =
                        OspfCliSetStubMetricType (CliHandle,
                                                  *args[0],
                                                  i4Metric, i4MetricType,
                                                  i4MetricTos, OSPF_FALSE);
                }
                else
                {
                    if (args[1] != NULL)
                    {
                        i4Metric = *(INT4 *) args[1];
                    }
                    else
                    {
                        i4Metric = OSPF_ZERO;
                    }
                    if (args[2] != NULL)
                    {
                        i4MetricType = *(INT4 *) args[2];
                    }
                    else
                    {
                        i4MetricType = OSPF_ZERO;
                    }

                    if (args[3] != NULL)
                    {
                        i4MetricTos = *(INT4 *) args[3];
                    }
                    else
                    {
                        i4MetricTos = OSPF_ZERO;
                    }

                    i4RespStatus =
                        OspfCliSetStubMetricType (CliHandle,
                                                  *args[0],
                                                  i4Metric, i4MetricType,
                                                  i4MetricTos, OSPF_TRUE);
                }

                break;
            }
            case OSPF_CLI_AREANSSA:
            {
                if (args[1] != NULL)
                {
                    i4RespStatus =
                        OspfCliSetAreaStub (CliHandle, *args[0],
                                            NSSA_AREA, NO_AREA_SUMMARY);
                }
                else
                {
                    i4RespStatus =
                        OspfCliSetAreaStub (CliHandle, *args[0],
                                            NSSA_AREA, SEND_AREA_SUMMARY);
                }
                break;
            }
            case OSPF_CLI_AREASTUB:
            {
                if (args[1] != NULL)
                {
                    i4RespStatus =
                        OspfCliSetAreaStub (CliHandle, *args[0],
                                            STUB_AREA, NO_AREA_SUMMARY);
                }
                else
                {
                    i4RespStatus =
                        OspfCliSetAreaStub (CliHandle, *args[0],
                                            STUB_AREA, SEND_AREA_SUMMARY);
                }
                break;
            }
            case OSPF_CLI_DEF_AREATYPE:
            {
                tAreaId             areaId;
                tArea              *pArea;
                u4AreaId = *args[0];
                i4AreaType = CLI_PTR_TO_I4 (args[1]);
                pOspfCxt = gOsRtr.pOspfCxt;

                u4AreaId = *args[0];
                if (args[2] != NULL)
                    SummaryArea = NO_AREA_SUMMARY;
                else
                    SummaryArea = SEND_AREA_SUMMARY;
                i4StatusVal = 0;

                if (pOspfCxt == NULL)
                {
                    return CLI_FAILURE;
                }
                OSPF_CRU_BMC_DWTOPDU (areaId, u4AreaId);
                pArea = GetFindAreaInCxt (pOspfCxt, &areaId);
                if (pArea == NULL)
                {
                    return CLI_FAILURE;
                }
                if ((nmhGetOspfAreaStatus (u4AreaId, &i4StatusVal) ==
                     SNMP_FAILURE))

                {
                    CliPrintf (CliHandle, "\r%% Area not configured\r\n");

                    UtilOspfResetContext ();
                    CliUnRegisterLock (CliHandle);
                    OspfUnLock ();
                    return CLI_FAILURE;
                }
                if (args[2] != NULL)
                {
                    nmhGetOspfImportAsExtern (u4AreaId, &i4StatusVal);
                    /*Checking whether the AreaType is valid or not for no-summary */
                    if (i4StatusVal != i4AreaType)
                    {
                        if (i4AreaType == NSSA_AREA)
                            CliPrintf (CliHandle,
                                       "\r%% NSSA area Not Configured\r\n");
                        else if (i4AreaType == STUB_AREA)
                            CliPrintf (CliHandle,
                                       "\r%% Stub area Not Configured\r\n");
                        UtilOspfResetContext ();
                        CliUnRegisterLock (CliHandle);
                        OspfUnLock ();
                        return CLI_FAILURE;
                    }
                    else
                    {
                        if ((i4AreaType == NSSA_AREA)
                            && (SummaryArea != pArea->u1SummaryFunctionality))
                        {
                            CliPrintf (CliHandle,
                                       "\r%% NSSA no-summary area Not Configured\r\n");
                            UtilOspfResetContext ();
                            CliUnRegisterLock (CliHandle);
                            OspfUnLock ();
                            return CLI_FAILURE;
                        }
                        else if ((i4AreaType == STUB_AREA)
                                 && (SummaryArea !=
                                     pArea->u1SummaryFunctionality))
                        {
                            CliPrintf (CliHandle,
                                       "\r%% Stub no-summary area Not Configured\r\n");
                            UtilOspfResetContext ();
                            CliUnRegisterLock (CliHandle);
                            OspfUnLock ();
                            return CLI_FAILURE;
                        }
                        i4RespStatus = OspfCliSetAreaStub (CliHandle, u4AreaId,
                                                           NORMAL_AREA,
                                                           NO_AREA_SUMMARY);
                    }

                }
                else
                {

                    nmhGetOspfImportAsExtern (u4AreaId, &i4StatusVal);

                    /*Checking whether the AreaType is valid or not for no-summary */
                    if (i4StatusVal != i4AreaType)

                    {
                        if (i4AreaType == NSSA_AREA)
                            CliPrintf (CliHandle,
                                       "\r%% NSSA Nrea ot Configured\r\n");
                        else if (i4AreaType == STUB_AREA)
                            CliPrintf (CliHandle,
                                       "\r%% Stub area Not Configured\r\n");

                        UtilOspfResetContext ();
                        CliUnRegisterLock (CliHandle);
                        OspfUnLock ();
                        return CLI_FAILURE;
                    }
                    else
                    {
                        if (i4AreaType == NSSA_AREA
                            && (SummaryArea != pArea->u1SummaryFunctionality))
                        {
                            CliPrintf (CliHandle,
                                       "\r%% NSSA summary area Not Configured\r\n");
                            UtilOspfResetContext ();
                            CliUnRegisterLock (CliHandle);
                            OspfUnLock ();
                            return CLI_FAILURE;
                        }
                        else if (i4AreaType == STUB_AREA
                                 && (SummaryArea !=
                                     pArea->u1SummaryFunctionality))
                        {
                            CliPrintf (CliHandle,
                                       "\r%% Stub summary area Not Configured\r\n");
                            UtilOspfResetContext ();
                            CliUnRegisterLock (CliHandle);
                            OspfUnLock ();
                            return CLI_FAILURE;
                        }

                        i4RespStatus = OspfCliSetAreaStub (CliHandle, u4AreaId,
                                                           NORMAL_AREA,
                                                           SEND_AREA_SUMMARY);
                    }

                }
                break;
            }
            case OSPF_CLI_DEL_AREA:
            {
                i4RespStatus = OspfCliDelArea (CliHandle, *args[0]);
                break;
            }
            case OSPF_CLI_DEFAULT_INFO:
            {
                if (args[0] != NULL)
                {
                    i4Metric = *(INT4 *) args[0];
                }
                else
                {
                    i4Metric = CLI_OSPF_AS_EXT_DEF_METRIC;
                }
                if (args[1] != NULL)
                {
                    i4MetricType = *(INT4 *) args[1];
                }
                else
                {
                    i4MetricType = TYPE_2_METRIC;
                }

                i4RespStatus =
                    OspfCliSetDefaultOriginate (CliHandle, i4Metric,
                                                i4MetricType, IP_ANY_ADDR,
                                                IP_ANY_ADDR, OSPF_CLI_DEF_TOS,
                                                OSPF_ENABLED);
                break;
            }
            case OSPF_CLI_NO_DEFAULT_INFO:
            {
                if ((args[0] == NULL) && (args[1] == NULL))
                {
                    i4RespStatus =
                        OspfCliDelDefaultOriginate (CliHandle, IP_ANY_ADDR,
                                                    IP_ANY_ADDR,
                                                    OSPF_CLI_DEF_TOS);
                }
                else
                {
                    if (args[0] != NULL)
                    {
                        i4Metric = *(INT4 *) args[0];
                    }
                    else
                    {
                        i4Metric = OSPF_CLI_INV_VALUE;
                    }
                    if (args[1] != NULL)
                    {
                        i4MetricType = *(INT4 *) args[1];
                    }
                    else
                    {
                        i4MetricType = OSPF_CLI_INV_VALUE;
                    }

                    i4RespStatus =
                        OspfCliSetDefaultOriginate (CliHandle, i4Metric,
                                                    i4MetricType, IP_ANY_ADDR,
                                                    IP_ANY_ADDR,
                                                    OSPF_CLI_DEF_TOS,
                                                    OSPF_DISABLED);
                }
                break;
            }
            case OSPF_CLI_VIRTUAL_LINK:
            {
                CLI_MEMSET (&Area, 0, sizeof (tAreaStruct));
                i4RespStatus = CLI_SUCCESS;

                Area.u4AreaId = *args[0];
                Area.u4RouterId = *args[1];

                if (nmhGetOspfAreaStatus (Area.u4AreaId, &i4StatusVal) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Virtual link cannot be configured "
                               "since the area does not exists\r\n");

                    UtilOspfResetContext ();
                    CliUnRegisterLock (CliHandle);
                    OspfUnLock ();
                    return CLI_FAILURE;
                }

                u4Options = CLI_PTR_TO_U4 (args[2]);
                Area.i4IfAuthType = CLI_PTR_TO_U4 (args[3]);
                Area.i4IfCryptoAuthType = CLI_PTR_TO_U4 (args[10]);
                Area.u1AreaBitMask = OSPF_VIRT_NO_OPTIONS;

                if ((u4Options & OSPF_VIRT_HELLO_INT) == OSPF_VIRT_HELLO_INT)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_HELLO_INT;
                    Area.i4IfHelloInterval = *(INT4 *) args[4];
                }
                if ((u4Options & OSPF_VIRT_RETRANS_INT) ==
                    OSPF_VIRT_RETRANS_INT)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_RETRANS_INT;
                    Area.i4IfRetransInterval = *(INT4 *) args[5];
                }
                if ((u4Options & OSPF_VIRT_TRANSMIT_DELAY) ==
                    OSPF_VIRT_TRANSMIT_DELAY)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_TRANSMIT_DELAY;
                    Area.i4IfTransitDelay = *(INT4 *) args[6];
                }
                if ((u4Options & OSPF_VIRT_DEAD_INT) == OSPF_VIRT_DEAD_INT)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_DEAD_INT;
                    Area.i4IfRtrDeadInterval = *(INT4 *) args[7];
                }

                if ((u4Options & OSPF_VIRT_AUTH_TYPE) == OSPF_VIRT_AUTH_TYPE)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_AUTH_TYPE;

                }
/*                if ((u4Options & OSPF_VIRT_CRYPTO_TYPE) == OSPF_VIRT_CRYPTO_TYPE)
                {
                    Area.u1ExtnAreaBitMask |= OSPF_VIRT_CRYPTO_TYPE;
                } */

                if (args[8] != NULL)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_AUTH_KEY;

                    if (CLI_STRLEN (args[8]) > MAX_AUTH_STRING_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Key should be less than 9 characters\r\n");
                        i4RespStatus = CLI_FAILURE;
                    }
                    else
                    {
                        CLI_STRCPY (Area.au1AuthKey, args[8]);
                    }
                }
                if (args[9] != NULL)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_MD5_KEY;
                    Area.i4AuthkeyId = *(INT4 *) args[9];

                    if (CLI_STRLEN (args[11]) > MAX_MD5_AUTH_STRING_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Key should be less than 17 characters\r\n");
                        i4RespStatus = CLI_FAILURE;
                    }
                    else
                    {
                        CLI_STRCPY (Area.au1AuthKey, args[11]);
                    }
                }

                if (i4RespStatus != CLI_FAILURE)
                {
                    i4RespStatus = OspfCliSetVirtualLink (CliHandle, &Area);
                }
                break;
            }
            case OSPF_CLI_NO_VIRTUAL_LINK:
            {
                CLI_MEMSET (&Area, 0, sizeof (tAreaStruct));

                Area.u4AreaId = *args[0];
                Area.u4RouterId = *args[1];

                u4Options = CLI_PTR_TO_U4 (args[2]);
                Area.i4IfAuthType = CLI_PTR_TO_I4 (args[3]);

                if ((u4Options & OSPF_VIRT_HELLO_INT) == OSPF_VIRT_HELLO_INT)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_HELLO_INT;
                    Area.i4IfHelloInterval = CLI_OSPF_DEF_HELLO_INTERVAL;
                }
                if ((u4Options & OSPF_VIRT_RETRANS_INT) ==
                    OSPF_VIRT_RETRANS_INT)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_RETRANS_INT;
                    Area.i4IfRetransInterval = CLI_OSPF_DEF_RXMT_INTERVAL;
                }
                if ((u4Options & OSPF_VIRT_TRANSMIT_DELAY) ==
                    OSPF_VIRT_TRANSMIT_DELAY)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_TRANSMIT_DELAY;
                    Area.i4IfTransitDelay = CLI_OSPF_DEF_IF_TRANS_DELAY;
                }
                if ((u4Options & OSPF_VIRT_DEAD_INT) == OSPF_VIRT_DEAD_INT)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_DEAD_INT;
                    Area.i4IfRtrDeadInterval = DEF_VIRT_IF_RTR_DEAD_INTERVAL;
                }

                if ((u4Options & OSPF_VIRT_AUTH_TYPE) == OSPF_VIRT_AUTH_TYPE)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_AUTH_TYPE;
                }

                if ((u4Options & OSPF_VIRT_AUTH_KEY) == OSPF_VIRT_AUTH_KEY)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_AUTH_KEY;
                }
                else if ((u4Options & OSPF_VIRT_MD5_KEY) == OSPF_VIRT_MD5_KEY)
                {
                    Area.u1AreaBitMask |= OSPF_VIRT_MD5_KEY;
                    Area.i4AuthkeyId = *(INT4 *) args[4];
                }
                if (u4Options == OSPF_VIRT_NO_OPTIONS)
                {
                    i4RespStatus = OspfCliDelVirtualLink (CliHandle, &Area);
                }
                else
                {
                    i4RespStatus = OspfCliSetDefVirtualLink (CliHandle, &Area);
                }
                break;
            }
            case OSPF_CLI_ASBR:
            {
                i4RespStatus = OspfCliSetASBRStatus (CliHandle, OSPF_TRUE);
                break;
            }
            case OSPF_CLI_NO_ASBR:
            {
                i4RespStatus = OspfCliSetASBRStatus (CliHandle, OSPF_FALSE);
                break;
            }
            case OSPF_CLI_SUMMARY:
            {
                if (args[5] != NULL)
                {
                    i4TagValue = *(INT4 *) args[5];
                    i4SetSummaryTag = OSPF_TRUE;
                }
                else
                {
                    i4SetSummaryTag = OSPF_FALSE;
                }
                i4RespStatus =
                    OspfCliSetSummary (CliHandle, *args[0],
                                       *args[1], *args[2],
                                       CLI_PTR_TO_I4 (args[3]),
                                       CLI_PTR_TO_I4 (args[4]),
                                       i4TagValue, i4SetSummaryTag);
                break;
            }
            case OSPF_CLI_DEL_SUMM_ADDR:
            {
                i4RespStatus =
                    OspfCliDelSummAddr (CliHandle, *args[0],
                                        *args[1], *args[2],
                                        CLI_PTR_TO_I4 (args[3]));
                break;
            }
            case OSPF_CLI_ADD_EXT_SUMM_ADDR:
            {
                i4RespStatus =
                    OspfCliAddExtSummAddr (CliHandle, *args[0],
                                           *args[1],
                                           *args[2],
                                           CLI_PTR_TO_I4 (args[3]),
                                           CLI_PTR_TO_I4 (args[4]));
                break;
            }
            case OSPF_CLI_DEL_EXT_SUMM_ADDR:
            {
                i4RespStatus = OspfCliDelExtSummAddr (CliHandle,
                                                      *args[0],
                                                      *args[1], *args[2]);
                break;
            }
            case OSPF_CLI_REDISTRIBUTE:
            {
                i4RespStatus = OspfCliSetRedistribute (CliHandle, u4OspfCxtId,
                                                       CLI_PTR_TO_I4 (args[0]),
                                                       OSPF_ENABLED,
                                                       (UINT1 *) args[1],
                                                       *(INT4 *) (args[2]),
                                                       *(INT4 *) (args[3]));
                break;
            }
            case OSPF_CLI_DEF_REDISTRIBUTE:
            {
                i4RespStatus = OspfCliSetRedistribute (CliHandle, u4OspfCxtId,
                                                       CLI_PTR_TO_I4 (args[0]),
                                                       OSPF_DISABLED,
                                                       (UINT1 *) args[1],
                                                       *(INT4 *) (args[2]),
                                                       *(INT4 *) (args[3]));
                break;
            }
            case OSPF_CLI_DISTRIBUTE_LIST:
            {
                i4RespStatus =
                    OspfCliSetDistribute (CliHandle, (UINT1 *) args[0],
                                          (UINT1) CLI_PTR_TO_U4 (args[1]));
                break;
            }
            case OSPF_CLI_REDIST_CONFIG:
            {
                if (args[2] != NULL)
                {
                    i4Metric = *(INT4 *) args[2];
                }
                else
                {
                    i4Metric = OSPF_ZERO;
                }
                i4MetricType = CLI_PTR_TO_I4 (args[3]);
                if (args[4] != NULL)
                {
                    u4TagValue = *args[4];
                }
                else
                {
                    u4TagValue = OSPF_ZERO;
                }
                i4RespStatus = OspfCliSetRRDRoute (CliHandle,
                                                   *args[0],
                                                   *args[1],
                                                   i4Metric, i4MetricType,
                                                   u4TagValue);
                break;
            }
            case OSPF_CLI_DEL_REDIST_CONFIG:
            {
                i4RespStatus = OspfCliDelRRDRoute (CliHandle,
                                                   *args[0], *args[1]);
                break;
            }
            case OSPF_CLI_NETWORK_AREA:
            {
                i4RespStatus = OspfCliSetNetworkArea
                    (CliHandle, u4InterfaceIndex, *args[0], *args[1]);
                break;
            }
            case OSPF_CLI_DEL_NETWORK_AREA:
            {
                i4RespStatus = OspfCliDelNetworkArea
                    (CliHandle, u4InterfaceIndex, *args[0], *args[1]);
                break;
            }
            case OSPF_CLI_NSSA_ASBR_DFRT:
            {
                i4RespStatus = OspfCliSetNssaAsbrDfRt (CliHandle, OSPF_TRUE);
                break;
            }
            case OSPF_CLI_NSSA_ASBR_NO_DFRT:
            {
                i4RespStatus = OspfCliSetNssaAsbrDfRt (CliHandle, OSPF_FALSE);
                break;
            }
            case OSPF_CLI_IF_PASSIVE:
            {
                i4RespStatus = OspfCliSetIfPassive
                    (CliHandle, u4InterfaceIndex, OSPF_TRUE);
                break;
            }
            case OSPF_CLI_IF_NO_PASSIVE:
            {
                i4RespStatus = OspfCliSetIfPassive
                    (CliHandle, u4InterfaceIndex, OSPF_FALSE);
                break;
            }
            case OSPF_CLI_ALL_IF_PASSIVE:
            {
                i4RespStatus = OspfCliSetDefaultPassive (CliHandle, OSPF_TRUE);
                break;
            }
            case OSPF_CLI_ALL_IF_NO_PASSIVE:
            {
                i4RespStatus = OspfCliSetDefaultPassive (CliHandle, OSPF_FALSE);
                break;
            }
            case OSPF_CLI_DEMANDCIRCUIT:
            {
                i4RespStatus = OspfCliSetDemandCircuit (CliHandle, u4IfIndex,
                                                        OSPF_TRUE, OSPF_TRUE);
                break;
            }
            case OSPF_CLI_NO_DEMANDCIRCUIT:
            {
                i4RespStatus = OspfCliSetDemandCircuit (CliHandle, u4IfIndex,
                                                        OSPF_TRUE, OSPF_FALSE);
                break;
            }
            case OSPF_CLI_RETRANSMITINT:
            {
                i4RespStatus = OspfCliSetIfRetransInterval (CliHandle,
                                                            u4IfIndex,
                                                            *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_DEF_RETRANS:
            {
                i4RespStatus =
                    OspfCliSetIfRetransInterval (CliHandle, u4IfIndex,
                                                 CLI_OSPF_DEF_RXMT_INTERVAL);
                break;
            }
            case OSPF_CLI_TRANSMITDELAY:
            {
                i4RespStatus = OspfCliSetIfTransitDelay (CliHandle,
                                                         u4IfIndex,
                                                         *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_DEF_TRANSMITDELAY:
            {
                i4RespStatus = OspfCliSetIfTransitDelay (CliHandle, u4IfIndex,
                                                         CLI_OSPF_DEF_IF_TRANS_DELAY);
                break;
            }
            case OSPF_CLI_PRIORITY:
            {
                i4RespStatus = OspfCliSetIfRtrPriority (CliHandle,
                                                        u4IfIndex,
                                                        *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_DEF_PRIORITY:
            {
                i4RespStatus = OspfCliSetIfRtrPriority (CliHandle, u4IfIndex,
                                                        CLI_OSPF_DEF_RTR_PRIORITY);
                break;
            }
            case OSPF_CLI_HELLOINT:
            {
                i4RespStatus = OspfCliSetIfHelloInterval (CliHandle,
                                                          u4IfIndex,
                                                          *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_DEF_HELLO:
            {
                i4RespStatus = OspfCliSetIfHelloInterval (CliHandle, u4IfIndex,
                                                          CLI_OSPF_DEF_HELLO_INTERVAL);
                break;
            }
            case OSPF_CLI_DEADINT:
            {
                i4RespStatus = OspfCliSetIfDeadInterval (CliHandle,
                                                         u4IfIndex,
                                                         *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_DEF_DEADINT:
            {
                i4RespStatus = OspfCliSetIfDeadInterval (CliHandle, u4IfIndex,
                                                         CLI_OSPF_DEF_RTR_DEAD_INTERVAL);
                break;
            }
            case OSPF_CLI_COST:
            {
                if (args[1] != NULL)
                {
                    i4MetricTos = *(INT4 *) args[1];
                }
                else
                {
                    i4MetricTos = TOS_0;
                }

                if (!(IS_VALID_TOS_VALUE ((INT1) i4MetricTos)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid TOS value specified\r\n");
                    i4RespStatus = CLI_FAILURE;
                }
                if (i4RespStatus != CLI_FAILURE)
                {
                    i4RespStatus =
                        OspfCliSetIfMetricValue (CliHandle, u4IfIndex,
                                                 *(INT4 *) args[0], i4MetricTos,
                                                 OSPF_FALSE);
                }
                break;
            }
            case OSPF_CLI_DEF_COST:
            {

                if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Index) ==
                    NETIPV4_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                    break;

                }

                if (NetIpv4GetIfInfo (u4Index, &NetIpv4IfInfo) ==
                    NETIPV4_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                    break;
                }

                if (NetIpv4IfInfo.u4IfSpeed == 0)
                    i4MetricValue = 0xffffffff;
                else if (NetIpv4IfInfo.u4IfSpeed > TEN_POWER_EIGHT)
                    i4MetricValue = 1;
                else
                    i4MetricValue = TEN_POWER_EIGHT / NetIpv4IfInfo.u4IfSpeed;

                if ((args[0] != NULL) && (*(INT4 *) args[0] != TOS_0))
                {
                    i4MetricTos = *(INT4 *) args[0];

                    if (!(IS_VALID_TOS_VALUE ((INT1) i4MetricTos)))
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid TOS value specified\r\n");
                        i4RespStatus = CLI_FAILURE;
                    }
                    if (i4RespStatus != CLI_FAILURE)
                    {

                        i4RespStatus =
                            OspfCliSetIfMetricValue (CliHandle, u4IfIndex,
                                                     i4MetricValue,
                                                     i4MetricTos, OSPF_TRUE);
                    }
                }
                else
                {
                    i4MetricTos = TOS_0;
                    i4RespStatus =
                        OspfCliSetIfMetricValue (CliHandle, u4IfIndex,
                                                 i4MetricValue,
                                                 i4MetricTos, OSPF_FALSE);
                }
                break;
            }
            case OSPF_CLI_IF_NETWORK_TYPE:
            {
                i4RespStatus =
                    OspfCliSetIfType (CliHandle, u4IfIndex,
                                      CLI_PTR_TO_I4 (args[0]));
                break;
            }
            case OSPF_CLI_DEF_NETWORK:
            {
                i4RespStatus = OspfCliSetIfType (CliHandle, u4IfIndex,
                                                 OSPF_NETWORK_DEFAULT_TYPE);
                break;
            }
            case OSPF_CLI_AUTHKEY:
            {
                if (CLI_STRLEN (args[0]) > MAX_AUTH_STRING_LEN)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        OspfCliSetIfAuthKey (CliHandle, u4IfIndex,
                                             (INT1 *) args[0]);
                }
                break;
            }
            case OSPF_CLI_NO_AUTHKEY:
            {
                STRCPY (au1AuthKey, OSPF_CLI_NULL_STR);
                i4RespStatus = OspfCliSetIfAuthKey (CliHandle, u4IfIndex,
                                                    (INT1 *) au1AuthKey);
                break;
            }
            case OSPF_CLI_AUTHTYPE:
            {
                i4RespStatus =
                    OspfCliSetIfAuthType (CliHandle, u4IfIndex,
                                          CLI_PTR_TO_I4 (args[0]),
                                          CLI_PTR_TO_I4 (args[1]));
                break;
            }
            case OSPF_CLI_DEF_IFAUTH:
            {
                i4RespStatus = OspfCliSetIfAuthType (CliHandle, u4IfIndex,
                                                     NO_AUTHENTICATION, -1);
                break;
            }
            case OSPF_CLI_MESSAGEDIGESTKEY:
            {
                if (CLI_STRLEN (args[2]) > MAX_AUTHKEY_LEN)
                {
                    CliPrintf (CliHandle,
                               "\r%% Key should be less than 17 characters\r\n");
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = OspfCliSetIfMD5AuthKey (CliHandle, u4IfIndex,
                                                           *(INT4 *) args[0],
                                                           (UINT1 *) args[2],
                                                           (UINT4)
                                                           CLI_PTR_TO_I4 (args
                                                                          [1]),
                                                           AUTHKEY_STATUS_VALID);
                }
                break;
            }
            case OSPF_CLI_MD5_KEY_ACCEPT_START:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[0];
                    i4RespStatus =
                        OspfCliSetIfMD5AuthKeyStartAccept (CliHandle, i4Key,
                                                           (UINT1 *) args[1],
                                                           u4OspfCxtId,
                                                           u4IfIndex);
                }
                break;
            }
            case OSPF_CLI_MD5_KEY_GEN_START:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[0];
                    i4RespStatus =
                        OspfCliSetIfMD5AuthKeyStartGenerate (CliHandle, i4Key,
                                                             (UINT1 *) args[1],
                                                             u4OspfCxtId,
                                                             u4IfIndex);
                }
                break;
            }
            case OSPF_CLI_MD5_KEY_GEN_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[0];
                    i4RespStatus =
                        OspfCliSetIfMD5AuthKeyStopGenerate (CliHandle, i4Key,
                                                            (UINT1 *) args[1],
                                                            u4OspfCxtId,
                                                            u4IfIndex);
                }
                break;
            }
            case OSPF_CLI_MD5_KEY_ACCEPT_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[0];
                    i4RespStatus =
                        OspfCliSetIfMD5AuthKeyStopAccept (CliHandle, i4Key,
                                                          (UINT1 *) args[1],
                                                          u4OspfCxtId,
                                                          u4IfIndex);
                }
                break;
            }
            case OSPF_CLI_DEL_MESSAGEDIGESTKEY:
            {
                i4RespStatus = OspfCliSetIfMD5AuthKey (CliHandle, u4IfIndex,
                                                       *(INT4 *) args[0], NULL,
                                                       0,
                                                       AUTHKEY_STATUS_DELETE);
                break;
            }
            case OSPF_CLI_SPF_TIMERS:
            {
                i4RespStatus =
                    OspfCliSetSpfTimers (CliHandle, *(INT4 *) args[0],
                                         *(INT4 *) args[1]);

                break;

            }
            case OSPF_CLI_NO_SPF_TIMERS:
            {
                i4RespStatus = OspfCliResetSpfTimers (CliHandle);

                break;
            }

            case OSPF_CLI_DISTANCE:
            {
                /* args[0] - distance value (1-255)
                 * args[1] - route map name (optional)
                 */
                i4RespStatus =
                    OspfSetRouteDistance (CliHandle, *(INT4 *) args[0],
                                          (UINT1 *) args[1]);
            }
                break;
            case OSPF_CLI_NO_DISTANCE:
            {
                /* args[0] - route map name (optional)
                 */
                i4RespStatus = OspfSetNoRouteDistance (CliHandle, (UINT1 *)
                                                       args[0]);
            }
                break;

            case OSPF_CLI_OPQ:
            {
                i4RespStatus = OspfCliSetOpaqueOption (CliHandle, OSPF_ENABLED);
                break;
            }
            case OSPF_CLI_NO_OPQ:
            {
                i4RespStatus = OspfCliSetOpaqueOption (CliHandle,
                                                       OSPF_DISABLED);
                break;
            }
            case OSPF_CLI_RESTART_SUPPORT:
            {
                i4RespStatus = OspfCliSetRestartSupport
                    (CliHandle, CLI_PTR_TO_I4 (args[0]));
                break;
            }
            case OSPF_CLI_GRACE_PERIOD:
            {
                if (args[0] != NULL)
                {
                    i4RespStatus = OspfCliSetGracePeriod (CliHandle,
                                                          *(INT4 *) (args[0]));
                }
                else
                {
                    i4RespStatus = OspfCliSetGracePeriod (CliHandle,
                                                          OSPF_GR_DEF_INTERVAL);
                }
                break;
            }
            case OSPF_CLI_GR_ACK_STATE:
            {
                i4RespStatus = OspfCliSetGrAckState
                    (CliHandle, CLI_PTR_TO_I4 (args[0]));
                break;

            }
            case OSPF_CLI_GRLSA_RETRANS_COUNT:
            {
                i4RespStatus = OspfCliSetGraceRetransCount
                    (CliHandle, *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_RESTART_REASON:
            {
                i4RespStatus = OspfCliSetRestartReason (CliHandle,
                                                        CLI_PTR_TO_I4 (args
                                                                       [0]));
                break;
            }
            case OSPF_CLI_HELPER_SUPPORT:
            {
                i4RespStatus = OspfCliSetHelperSupport
                    (CliHandle, CLI_PTR_TO_I4 (args[0]), CLI_ENABLE);
                break;
            }
            case OSPF_CLI_NO_HELPER_SUPPORT:
            {
                i4RespStatus = OspfCliSetHelperSupport
                    (CliHandle, CLI_PTR_TO_I4 (args[0]), CLI_DISABLE);
                break;
            }
            case OSPF_CLI_HELPER_GRACE_LIMIT:
            {
                i4RespStatus = OspfCliSetHelperGraceLimitPeriod (CliHandle,
                                                                 *(INT4 *)
                                                                 args[0]);
                break;
            }

            case OSPF_CLI_STRICT_LSA_CHECK:
            {
                i4RespStatus = OspfCliSetStrictLsaCheck
                    (CliHandle, CLI_PTR_TO_I4 (args[0]));
                break;
            }
            case OSPF_CLI_STAGGERING_STATUS:
            {
                i4RespStatus = OspfCliSetStaggeringStatus
                    (CliHandle, CLI_PTR_TO_I4 (args[0]));
                break;
            }
            case OSPF_CLI_STAGGERING_INTERVAL:
            {
                i4RespStatus = OspfCliSetStaggeringInterval (CliHandle,
                                                             *(INT4 *) args[0]);
                break;
            }
            case OSPF_CLI_VIRT_KEY_ACCEPT_START:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                u4AreaId = *args[0];
                u4NbrId = *args[1];

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[2];
                    i4RespStatus =
                        OspfCliSetIfVirtAuthKeyStartAccept (CliHandle, u4AreaId,
                                                            u4NbrId, i4Key,
                                                            (UINT1 *) args[3],
                                                            u4OspfCxtId);

                }
                break;
            }
            case OSPF_CLI_VIRT_KEY_GENERATE_START:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                u4AreaId = *args[0];
                u4NbrId = *args[1];

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[2];
                    i4RespStatus =
                        OspfCliSetIfVirtAuthKeyStartGenerate (CliHandle,
                                                              u4AreaId, u4NbrId,
                                                              i4Key,
                                                              (UINT1 *) args[3],
                                                              u4OspfCxtId);
                }
                break;
            }
            case OSPF_CLI_VIRT_KEY_GENERATE_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                u4AreaId = *args[0];
                u4NbrId = *args[1];

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[2];
                    i4RespStatus =
                        OspfCliSetIfVirtAuthKeyStopGenerate (CliHandle,
                                                             u4AreaId, u4NbrId,
                                                             i4Key,
                                                             (UINT1 *) args[3],
                                                             u4OspfCxtId);

                }
                break;
            }
            case OSPF_CLI_VIRT_KEY_ACCEPT_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                u4AreaId = *args[0];
                u4NbrId = *args[1];

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) args[2];
                    i4RespStatus =
                        OspfCliSetIfVirtAuthKeyStopAccept (CliHandle, u4AreaId,
                                                           u4NbrId, i4Key,
                                                           (UINT1 *) args[3],
                                                           u4OspfCxtId);

                }
                break;
            }
            case OSPF_CLI_BFD_ADMIN_STATUS:
            {
                i4RespStatus =
                    OspfCliSetBfdAdmState (CliHandle,
                                           (INT4) CLI_PTR_TO_U4 (args[0]));
                break;
            }

            case OSPF_CLI_BFD_STATUS_ALL_INTF:
            {
                i4RespStatus =
                    OspfCliSetBfdStatusAllInt (CliHandle,
                                               (INT4) CLI_PTR_TO_U4 (args[0]));
                break;
            }

            case OSPF_CLI_BFD_STATUS_SPECIFIC_INTF:
            {
                i4RespStatus =
                    OspfCliSetBfdStatusSpecificInt (CliHandle, u4InterfaceIndex,
                                                    (INT4)
                                                    CLI_PTR_TO_U4 (args[0]));
                break;
            }

            case OSPF_CLI_IP_OSPF_BFD:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1OspfCxtName,
                                                           &u4OspfCxtId);
                }
                if (u4InterfaceIndex != 0)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = OspfCliGetCxtIdFromIfIndex
                            (CliHandle, u4InterfaceIndex, &u4OspfCxtId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        OspfCliSetBfdStatusSpecificInt (CliHandle, u4IfIndex,
                                                        (INT4)
                                                        CLI_PTR_TO_U4 (args
                                                                       [0]));
                }
                break;
            }
            case OSPF_CLI_ADD_HOST_METRIC:
            {
                i4RespStatus = OspfCliSetHostMetric (CliHandle,
                                                     (INT4) u4OspfCxtId,
                                                     *args[0],
                                                     OSPF_ZERO,
                                                     *(INT4 *) args[1]);
                break;
            }
            case OSPF_CLI_DEL_HOST_METRIC:
            {
                i4RespStatus = OspfCliDelHostMetric (CliHandle,
                                                     (INT4) u4OspfCxtId,
                                                     *args[0]);
                break;
            }

            default:
                CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
                i4RespStatus = CLI_FAILURE;
                break;
        }
        pOspfCxt = gOsRtr.pOspfCxt;
        UtilOspfResetContext ();
    }
    if ((i4RespStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_OSPF_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", OspfCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RespStatus);

    CliUnRegisterLock (CliHandle);
    OspfUnLock ();

    /*Saving all cli commands for Graceful restart */
    if (pOspfCxt != NULL)
    {
        if ((i4RespStatus == CLI_SUCCESS) && (pOspfCxt->u1RestartSupport == OSPF_RESTART_BOTH) && (gOsRtr.b1GrCsrStatus != TRUE))    /* Dont Save the configurations if 
                                                                                                                                       GR Config restoration is not completed */
        {
            IssCsrSaveCli (pu1TaskName);
        }
    }

    return i4RespStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliGetCxtIdFromCxtName                             */
/*                                                                            */
/* Description       : This function calls a VCM module to get                */
/*                     the context id with respect to Vrf name                */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext                            */
/*                     pu1OspfCxtName - vrf name                              */
/*                                                                            */
/* Output Parameters : pu4OspfCxtId    - Ospf Context ID                       */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliGetCxtIdFromCxtName (tCliHandle CliHandle, UINT1 *pu1OspfCxtName,
                            UINT4 *pu4OspfCxtId)
{
    UNUSED_PARAM (CliHandle);
    /* Call the vcm module to get the context id
     * with respect to the vrf name */

    if (UtilOspfIsVcmSwitchExist (pu1OspfCxtName, pu4OspfCxtId) != OSPF_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%s", OspfCliErrString[CLI_OSPF_INV_ENTRY]);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliGetCxtIdFromIfIndex                             */
/*                                                                            */
/* Description       : This function returns the context id                   */
/*                     with respect to the interface                          */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext                            */
/*                     u4IfIndex      - Interface index                       */
/*                                                                            */
/* Output Parameters : pu4OspfCxtId    - Ospf Context ID                      */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliGetCxtIdFromIfIndex (tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT4 *pu4OspfCxtId)
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4OspfCxtId;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4Port = 0;

    UNUSED_PARAM (CliHandle);

    /* Get IP port from IfIndex */
    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) != NETIPV4_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Scan the interface list in all the context */

    /* Get the first context */
    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        return CLI_FAILURE;
    }

    do
    {
        /* Get the context pointer corresponding to the 
         * context structure */
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

        if (pOspfCxt == NULL)
        {
            /* Invalid context id */
            break;
        }
        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

            if (pInterface->u4IfIndex == u4Port)
            {
                *pu4OspfCxtId = pInterface->pArea->pOspfCxt->u4OspfCxtId;
                return CLI_SUCCESS;
            }
        }

        u4OspfPrevCxtId = u4OspfCxtId;

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    /* No Context available for given interface */
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliGetCxtIdFromNeighborId                          */
/*                                                                            */
/* Description       : This function returns the context id                   */
/*                     with respect to the neighbor                           */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext                            */
/*                     u4NbrId        - Neighbor ID                           */
/*                                                                            */
/* Output Parameters : pu4OspfCxtId    - Ospf Context ID                      */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliGetCxtIdFromNeighborId (tCliHandle CliHandle, UINT4 u4NbrId,
                               UINT4 *pu4OspfCxtId)
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4OspfCxtId;
    tNeighbor          *pNeighbor = NULL;
    tTMO_SLL_NODE      *pLstNode;
    tIPADDR             nbrId;

    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);
    /* Scan the interface list in all the context */

    /* Get the first context */
    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        CliPrintf (CliHandle, "No Context is available \r\n");
        return CLI_FAILURE;
    }

    do
    {
        /* Get the context pointer corresponding to the 
         * context structure */
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

        if (pOspfCxt == NULL)
        {
            /* Invalid context id */
            break;
        }
        TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
        {
            pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

            if (UtilIpAddrComp (nbrId, pNeighbor->nbrId) == OSPF_EQUAL)
            {
                *pu4OspfCxtId =
                    pNeighbor->pInterface->pArea->pOspfCxt->u4OspfCxtId;
                return CLI_SUCCESS;
            }
        }

        u4OspfPrevCxtId = u4OspfCxtId;

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    /* No Context available for given neighbor */
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliGetContext                                      */
/*                                                                            */
/* Description       : This function calls a routine to set the               */
/*                     context id. The function sets a flag for show routines */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext ID                         */
/*                     u4Command      - CLI command                           */
/*                                                                            */
/* Output Parameters : pu4OspfCxtId   - Ospf Context ID                       */
/*                     pu4IfIndex     - Interface If Index                    */
/*                     pu2ShowCmdFlag - Flag to indicate the show command     */
/*                                                                            */
/* Return Value      : OSPF_SUCCESS/OSPF_FAILURE                              */
/******************************************************************************/

INT4
OspfCliGetContext (tCliHandle CliHandle, UINT4 u4Command,
                   UINT4 *pu4OspfCxtId, UINT4 *pu4IfIndex,
                   UINT2 *pu2ShowCmdFlag)
{
    INT4                i4IfIndex;
    UINT4               u4Port;

    *pu2ShowCmdFlag = OSPF_FALSE;

    /* Get the context Id from pCliContext structure */
    *pu4OspfCxtId = CLI_GET_CXT_ID ();

    /* Invalid context Id is obtained in Interface and Global mode */
    if (*pu4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        i4IfIndex = CLI_GET_IFINDEX ();

        /* CLI_GET_IFINDEX returns -1 for non-interface mode */
        if (i4IfIndex != OSPF_INVALID_IFINDEX)
        {
            *pu4IfIndex = i4IfIndex;

            /* Get IP port from IfIndex */
            if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
                != NETIPV4_SUCCESS)
            {
                CliPrintf (CliHandle, "%% INterface not available\r\n");
                return CLI_FAILURE;
            }

            /* Get the context Id from the port */
            if (NetIpv4GetCxtId (u4Port, pu4OspfCxtId) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Interface not mapped to any context\r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            /* This is the Global mode
             * Show flag is set for exec mode*/
            if ((u4Command == OSPF_CLI_NO_ROUTEROSPF) ||
                (u4Command == OSPF_CLI_ROUTEROSPF))
            {
                *pu2ShowCmdFlag = OSPF_FALSE;
            }
            else
            {
                *pu2ShowCmdFlag = OSPF_TRUE;
            }
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetBfdAdmState                               */
/*                                                                            */
/* Description       : This Routine will enable BFD monitoring OSPF globally  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     4FutInt   - BFD_ENABLED/BFD_DISABLED                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetBfdAdmState (tCliHandle CliHandle, INT4 I4FutInt)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfBfdStatus (&u4ErrorCode, I4FutInt) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfBfdStatus (I4FutInt) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/******************************************************************************/
/* Function Name     : OspfCliSetBfdStatusAllInt                              */
/*                                                                            */
/* Description       : This Routine will set BFD for all interfaces           */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4BfdStatus - Bfd Status                               */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetBfdStatusAllInt (tCliHandle CliHandle, INT4 i4BfdStatus)
{
    UINT4               u4ErrorCode = 0;
    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfBfdAllIfState (&u4ErrorCode, i4BfdStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfBfdAllIfState (i4BfdStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetBfdStatusSpecificInt                         */
/*                                                                            */
/* Description       : This Routine will set BFD status for specific interface*/
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex - Interface index                            */
/*                     i4BfdStatus - Bfd status                               */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetBfdStatusSpecificInt (tCliHandle CliHandle, UINT4 u4IfIndex,
                                INT4 i4BfdStatus)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfIfBfdState (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                    i4BfdStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhSetFutOspfIfBfdState (u4IfIpAddr, i4AddrlessIf, i4BfdStatus) ==
         SNMP_FAILURE))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliTrcStr                                          */
/*                                                                            */
/* Description       : Displays the Debug trace level string for the          */
/*                     the input trace level.                                 */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4TrcLvl   - Debug Trace Level                         */
/*                     pac1Trc    - Debug Trace String                        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : VOID                                                   */
/******************************************************************************/

PRIVATE VOID
OspfCliTrcStr (tCliHandle CliHandle, UINT4 u4TrcLvl, const CHR1 * pac1Trc[])
{
    UINT4               u4Idx;
    UINT4               u4NumLevels;
    const CHR1         *pc1Separator = ", ";

    /* sizeof gets munged when pac1Trc[] is passed to a function    */
    /* Hence Iterate through pac1Trc and find the size of pac1Trc   */
    /* A NULL marks the end of pac1Trc                              */
    for (u4Idx = 0; pac1Trc[u4Idx]; u4Idx++);

    u4NumLevels = u4Idx;

    for (u4Idx = 0; u4Idx < u4NumLevels; u4Idx++)
    {
        if (u4TrcLvl & (1 << u4Idx))
        {
            CliPrintf (CliHandle, "%s%s", pac1Trc[u4Idx], pc1Separator);
        }
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfRetransInterval                            */
/*                                                                            */
/* Description       : This Routine will set Retransmit  Interval for         */
/*                     an Interface                                           */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4RetransInterval - Retransmit  Interval to set        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfRetransInterval (tCliHandle CliHandle, UINT4 u4IfIndex,
                             INT4 i4RetransInterval)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it is 
     * not created already, create the interface entry in the OSPF iftable */

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the CREATE 
         * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfRetransInterval (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                        i4RetransInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfRetransInterval (u4IfIpAddr, i4AddrlessIf,
                                     i4RetransInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((i4StatusVal == NOT_IN_SERVICE)
        && (i4RetransInterval == CLI_OSPF_DEF_RXMT_INTERVAL))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetRRDRoute                                     */
/*                                                                            */
/* Description       : This Routine will configure the information to         */
/*                     be applied to routes learnt from Rtm.                  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4FutOspfRRDRouteDest - Route Destination              */
/*                     u4FutOspfRRDRouteMask - Route Mask                     */
/*                     i4Metric - Route metric to be set                      */
/*                     i4MetricType - Route metric type to be set             */
/*                     u4TagValue - Rotue tag to be set                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetRRDRoute (tCliHandle CliHandle,
                    UINT4 u4FutOspfRRDRouteDest,
                    UINT4 u4FutOspfRRDRouteMask,
                    INT4 i4Metric, INT4 i4MetricType, UINT4 u4TagValue)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StatusVal = 0;

    /* check whether the interface index entry is already created
       or not, if not create an entry in the Fut OSPF RRD Route Config table */

    if (nmhGetFutOspfRRDRouteStatus (u4FutOspfRRDRouteDest,
                                     u4FutOspfRRDRouteMask,
                                     &i4StatusVal) == SNMP_FAILURE)
        /*Create an row in futospf RRD Route config table using create and wait */
    {
        if (nmhTestv2FutOspfRRDRouteStatus (&u4ErrorCode,
                                            u4FutOspfRRDRouteDest,
                                            u4FutOspfRRDRouteMask,
                                            CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfRRDRouteStatus (u4FutOspfRRDRouteDest,
                                         u4FutOspfRRDRouteMask,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (nmhSetFutOspfRRDRouteStatus (u4FutOspfRRDRouteDest,
                                     u4FutOspfRRDRouteMask,
                                     NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if ((i4Metric == OSPF_ZERO) && (i4StatusVal == 0))
    {
        i4Metric = CLI_OSPF_AS_EXT_DEF_METRIC;
    }
    /* Checking to determine whether the optional argument given is Metric */
    if (i4Metric != OSPF_ZERO)
    {
        /* Checks whether the passed parameter value falls within the mib range */
        if (nmhTestv2FutOspfRRDRouteMetric (&u4ErrorCode,
                                            u4FutOspfRRDRouteDest,
                                            u4FutOspfRRDRouteMask,
                                            i4Metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetFutOspfRRDRouteMetric (u4FutOspfRRDRouteDest,
                                         u4FutOspfRRDRouteMask,
                                         i4Metric) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Route with this destination doesn't exists");
            return CLI_FAILURE;
        }
    }

    /* Checking to determine whether the optional argument given is Metric Type */
    if (i4MetricType != OSPF_ZERO)
    {
        /* Checks whether the passed parameter value falls within the mib range */
        if (nmhTestv2FutOspfRRDRouteMetricType (&u4ErrorCode,
                                                u4FutOspfRRDRouteDest,
                                                u4FutOspfRRDRouteMask,
                                                i4MetricType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetFutOspfRRDRouteMetricType (u4FutOspfRRDRouteDest,
                                             u4FutOspfRRDRouteMask,
                                             i4MetricType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Route with this destination doesn't exists");
            return CLI_FAILURE;
        }
    }

    /* Checking to determine whether the optional argument given is Tag */
    if (u4TagValue != OSPF_ZERO)
    {
        /* Checks whether the passed parameter value falls within the mib range */
        if (nmhTestv2FutOspfRRDRouteTag (&u4ErrorCode,
                                         u4FutOspfRRDRouteDest,
                                         u4FutOspfRRDRouteMask,
                                         u4TagValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetFutOspfRRDRouteTag (u4FutOspfRRDRouteDest,
                                      u4FutOspfRRDRouteMask,
                                      u4TagValue) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Route with this destination doesn't exists");
            return CLI_FAILURE;
        }
    }

    /*Set the RRD route status active */

    if (nmhTestv2FutOspfRRDRouteStatus (&u4ErrorCode,
                                        u4FutOspfRRDRouteDest,
                                        u4FutOspfRRDRouteMask,
                                        ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRRDRouteStatus (u4FutOspfRRDRouteDest,
                                     u4FutOspfRRDRouteMask,
                                     ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelRRDRoute                                     */
/*                                                                            */
/* Description       : This Routine will Delete a RRD Route entry             */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4FutOspfRRDRouteDest - Route Destination              */
/*                     u4FutOspfRRDRouteMask - Route Mask                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelRRDRoute (tCliHandle CliHandle,
                    UINT4 u4FutOspfRRDRouteDest, UINT4 u4FutOspfRRDRouteMask)
{
    INT4                i4StatusVal = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFutOspfRRDRouteStatus (u4FutOspfRRDRouteDest,
                                     u4FutOspfRRDRouteMask,
                                     &i4StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Trying to delete a non-existing entry\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfRRDRouteStatus (&u4ErrorCode,
                                        u4FutOspfRRDRouteDest,
                                        u4FutOspfRRDRouteMask,
                                        DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRRDRouteStatus (u4FutOspfRRDRouteDest,
                                     u4FutOspfRRDRouteMask,
                                     DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfTransitDelay                               */
/*                                                                            */
/* Description       : This Routine will set Transit Delay for an interface   */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4TransitDelay - Transit Delay                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfTransitDelay (tCliHandle CliHandle, UINT4 u4IfIndex,
                          INT4 i4TransitDelay)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it is 
     * not created already, create the interface entry in the OSPF iftable */

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the CREATE 
         * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfTransitDelay (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                     i4TransitDelay) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfIfTransitDelay (u4IfIpAddr, i4AddrlessIf,
                                  i4TransitDelay) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((i4StatusVal == NOT_IN_SERVICE)
        && (i4TransitDelay == CLI_OSPF_DEF_IF_TRANS_DELAY))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfRtrPriority                                */
/*                                                                            */
/* Description       : This Routine will set Router priority                  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4IfRtrPriority - Router priority                      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfRtrPriority (tCliHandle CliHandle, UINT4 u4IfIndex,
                         INT4 i4IfRtrPriority)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it is 
     * not created already, create the interface entry in the OSPF iftable */

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the CREATE 
         * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfRtrPriority (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                    i4IfRtrPriority) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfRtrPriority (u4IfIpAddr, i4AddrlessIf,
                                 i4IfRtrPriority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((i4StatusVal == NOT_IN_SERVICE)
        && (i4IfRtrPriority == CLI_OSPF_DEF_RTR_PRIORITY))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfHelloInterval                              */
/*                                                                            */
/* Description       : This Routine will set hello interval for a router      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4HelloInterval - Hello interval                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfHelloInterval (tCliHandle CliHandle, UINT4 u4IfIndex,
                           INT4 i4HelloInterval)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it is 
     * not created already, create the interface entry in the OSPF iftable */

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the CREATE 
         * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfHelloInterval (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                      i4HelloInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfHelloInterval (u4IfIpAddr, i4AddrlessIf,
                                   i4HelloInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((i4StatusVal == NOT_IN_SERVICE)
        && (i4HelloInterval == CLI_OSPF_DEF_HELLO_INTERVAL))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfDeadInterval                               */
/*                                                                            */
/* Description       : This Routine will set Dead interval for a router       */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4RtrDeadInterval - Dead interval                      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfDeadInterval (tCliHandle CliHandle, UINT4 u4IfIndex,
                          INT4 i4RtrDeadInterval)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it is 
     * not created already, create the interface entry in the OSPF iftable */

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the CREATE 
         * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfRtrDeadInterval (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                        i4RtrDeadInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfRtrDeadInterval (u4IfIpAddr, i4AddrlessIf,
                                     i4RtrDeadInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((i4StatusVal == NOT_IN_SERVICE)
        && (i4RtrDeadInterval == CLI_OSPF_DEF_RTR_DEAD_INTERVAL))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/********************************************************************************/
/* Function Name     : OspfCliSetAreaAuthTypeInCxt                              */
/*                                                                              */
/* Description       : This Routine will set the Authentication type of an Area */
/*                                                                              */
/* Input Parameters  : CliHandle   - CliContext ID                              */
/*                     u4OspfCxtId - ospf context id                            */
/*                     u4AreaId    - Area ID                                    */
/*                     i4AuthType  - Authentication Type                        */
/*                                                                              */
/* Output Parameters : None                                                     */
/*                                                                              */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                  */
/********************************************************************************/

INT4
OspfCliSetAreaAuthTypeInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                             UINT4 u4AreaId, INT4 i4AuthType)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AdminStat;

    if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
    {
        CliPrintf (CliHandle, "Admin status is disabled or invalid context id");
    }

    /* Checking whether the area entry is already created or not. 
     * Area auth type can be set only when the entry is created.
     */
    if (nmhGetFsMIStdOspfAreaStatus (u4OspfCxtId, u4AreaId, &i4StatusVal)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Area doesn't exists\r\n");
        return CLI_FAILURE;
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Ospf Context Not created\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2OspfAuthType (&u4ErrorCode, u4AreaId, i4AuthType)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfAuthType (u4AreaId, i4AuthType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UtilOspfResetContext ();

    if (nmhSetFsMIStdOspfAreaStatus (u4OspfCxtId, u4AreaId, ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfAuthKey                                    */
/*                                                                            */
/* Description       : This Routine will set authentication for an interface  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     pu1AuthKey - Authentication key to set                 */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfAuthKey (tCliHandle CliHandle, UINT4 u4IfIndex, INT1 *pu1AuthKey)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetString;

    pOctetString =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1AuthKey, STRLEN (pu1AuthKey));
    if (pOctetString == NULL)
    {
        return CLI_FAILURE;
    }

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        SNMP_AGT_FreeOctetString (pOctetString);
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /*Get the Interface Status and check whether the entry is already created
       or not, create an entry in the interface table */
    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }
        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfAuthKey (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                pOctetString) == SNMP_FAILURE)
    {
        SNMP_AGT_FreeOctetString (pOctetString);
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfAuthKey (u4IfIpAddr, i4AddrlessIf, pOctetString) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        SNMP_AGT_FreeOctetString (pOctetString);
        return CLI_FAILURE;
    }

    if ((i4StatusVal == NOT_IN_SERVICE) &&
        (pOctetString->pu1_OctetList != NULL) &&
        (STRCMP (pOctetString->pu1_OctetList, OSPF_CLI_NULL_STR) == 0))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
        }
    }
    SNMP_AGT_FreeOctetString (pOctetString);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfMD5AuthKey                                 */
/*                                                                            */
/* Description       : This Routine will set MD5 authentication for an        */
/*                     interface                                              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4AuthKeyId - Authentication key id                    */
/*                     pu1AuthKey - Authentication key to set                 */
/*                     i4KeyStatus - AUTHKEY_STATUS_VALID /                   */
/*                                   AUTHKEY_STATUS_INVALID                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfMD5AuthKey (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4AuthKeyId,
                        UINT1 *pu1AuthKey, UINT4 u4IfCryptoAuthType,
                        INT4 i4KeyStatus)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    INT4                StatusVal = 0;
    INT4                i4CryptoAuthType = 0;
    INT4                i4AuthType = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;

    if (pu1AuthKey != NULL)
    {
        pOctetString =
            SNMP_AGT_FormOctetString (pu1AuthKey, STRLEN (pu1AuthKey));

        if (pOctetString == NULL)
        {
            return CLI_FAILURE;
        }
    }

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        if (pOctetString != NULL)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
        }
        return CLI_FAILURE;
    }
    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF
         * iftable using the CREATE
         *          * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            if (pOctetString != NULL)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
            }
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            if (pOctetString != NULL)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
            }
            return CLI_FAILURE;
        }
    }
    nmhGetOspfIfAuthType (u4IfIpAddr, i4AddrlessIf, &i4AuthType);
    if (i4AuthType == CRYPT_AUTHENTICATION)
    {
        if (nmhGetFutOspfIfCryptoAuthType
            (u4IfIpAddr, (UINT4) i4AddrlessIf,
             &i4CryptoAuthType) != SNMP_FAILURE)
        {
            if ((i4CryptoAuthType != (INT4) u4IfCryptoAuthType)
                && (i4CryptoAuthType != 0) && (u4IfCryptoAuthType != 0))
            {
                CliPrintf (CliHandle, "\r%% Mismatching CryptoAuth Type \r\n");
                if (pOctetString != NULL)
                {
                    SNMP_AGT_FreeOctetString (pOctetString);
                }

                return CLI_FAILURE;
            }
        }
    }

    if ((i4KeyStatus == AUTHKEY_STATUS_VALID) && (pOctetString != NULL))
    {
        if (u4IfCryptoAuthType == OSPF_AUTH_MD5)
        {
            /* Checks whether the passed parameter value falls within the mib range */

            if (nmhTestv2FutOspfIfMD5AuthKey
                (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf, i4AuthKeyId,
                 pOctetString) == SNMP_FAILURE)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            /* Calls the SNMP set routine to set the value    */
            if (nmhSetFutOspfIfMD5AuthKey (u4IfIpAddr, i4AddrlessIf,
                                           i4AuthKeyId,
                                           pOctetString) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2FutOspfIfAuthKey
                (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf, i4AuthKeyId,
                 pOctetString) == SNMP_FAILURE)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            /* Calls the SNMP set routine to set the value    */
            if (nmhSetFutOspfIfAuthKey (u4IfIpAddr, i4AddrlessIf,
                                        i4AuthKeyId,
                                        pOctetString) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
        }

    }

    /* Check whether the key status is valid or not */
    if (nmhTestv2FutOspfIfMD5AuthKeyStatus
        (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf, i4AuthKeyId,
         i4KeyStatus) == SNMP_FAILURE)
    {
        SNMP_AGT_FreeOctetString (pOctetString);
        return CLI_FAILURE;
    }
    /* Set the Key Status to Valid or In-valid     */
    if (nmhSetFutOspfIfMD5AuthKeyStatus (u4IfIpAddr, i4AddrlessIf, i4AuthKeyId,
                                         i4KeyStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        SNMP_AGT_FreeOctetString (pOctetString);
        return CLI_FAILURE;
    }
    SNMP_AGT_FreeOctetString (pOctetString);

    if ((StatusVal == NOT_IN_SERVICE) && (i4KeyStatus == AUTHKEY_STATUS_DELETE))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfMD5AuthKeyStartAccept                       */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key start accept                 */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StartAcceptTime - Contains the Start Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/
INT4
OspfCliSetIfMD5AuthKeyStartAccept (tCliHandle CliHandle, INT4 KeyId,
                                   UINT1 *pu1StartAcceptTime, UINT4 u4OspfCxtId,
                                   UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    INT4                i4KeyStartAccept = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    /*tTMO_SLL_NODE       *pNode; */
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tIPADDR             ifIpAddr;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE KeyStartTime;

    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);
    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    i4Len = STRLEN (pu1StartAcceptTime);
    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Get the ip Address form the interface */
    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }
    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StartAcceptTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;

    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    /* Converts the DD-MON-YY to YY-MON-DD format */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    KeyStartTime.pu1_OctetList = pOspfKeyTime;
    KeyStartTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);
    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStartAccept = (INT4) GrGetSecondsSinceBase (tm);
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4AddrlessIf);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        if (nmhTestv2FsMIOspfIfMD5AuthKeyStartAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStartAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfIfMD5AuthKeyStartAccept
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStartAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfIfAuthKeyStartAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfIfAuthKeyStartAccept
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfMD5AuthKeyStartGenerate                     */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key start generate               */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StartGenTime - Contains the Start Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/
INT4
OspfCliSetIfMD5AuthKeyStartGenerate (tCliHandle CliHandle, INT4 KeyId,
                                     UINT1 *pu1StartGenTime, UINT4 u4OspfCxtId,
                                     UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    INT4                i4KeyStartGenerate = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tIPADDR             ifIpAddr;
    /* tTMO_SLL_NODE       *pNode; */
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE KeyStartTime;

    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }
    i4Len = (INT4) STRLEN (pu1StartGenTime);

    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Get the ip Address form the interface */
    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }
    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StartGenTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }
    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    KeyStartTime.pu1_OctetList = pOspfKeyTime;
    KeyStartTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);

    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStartGenerate = (INT4) GrGetSecondsSinceBase (tm);
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4AddrlessIf);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        /* Test routine is called for configuring Key Start Generate value */
        if (nmhTestv2FsMIOspfIfMD5AuthKeyStartGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStartGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Start Generate Value */
        if (nmhSetFsMIOspfIfMD5AuthKeyStartGenerate
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStartGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfIfAuthKeyStartGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfIfAuthKeyStartGenerate
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;

}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfMD5AuthKeyStopGenerate                     */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key stop generate               */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StopGenTime - Contains the Stop Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/
INT4
OspfCliSetIfMD5AuthKeyStopGenerate (tCliHandle CliHandle, INT4 KeyId,
                                    UINT1 *pu1StopGenTime, UINT4 u4OspfCxtId,
                                    UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    INT4                i4KeyStopGenerate = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tIPADDR             ifIpAddr;
    /*tTMO_SLL_NODE       *pNode; */
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE KeyStopTime;

    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }
    i4Len = (INT4) STRLEN (pu1StopGenTime);
    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Get the ip Address form the interface */
    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }
    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StopGenTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    KeyStopTime.pu1_OctetList = pOspfKeyTime;
    KeyStopTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);
    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStopGenerate = (INT4) GrGetSecondsSinceBase (tm);
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4AddrlessIf);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }
    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        /* Test routine is called for configuring Key Stop Generate value */
        if (nmhTestv2FsMIOspfIfMD5AuthKeyStopGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStopGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Stop Generate Value */
        if (nmhSetFsMIOspfIfMD5AuthKeyStopGenerate
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStopGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfIfAuthKeyStopGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfIfAuthKeyStopGenerate
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfMD5AuthKeyStopAccept                       */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key Stop accept                 */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StopAcceptTime - Contains the Stop Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/
INT4
OspfCliSetIfMD5AuthKeyStopAccept (tCliHandle CliHandle, INT4 KeyId,
                                  UINT1 *pu1StopAcceptTime, UINT4 u4OspfCxtId,
                                  UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    INT4                i4KeyStopAccept = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tIPADDR             ifIpAddr;
/*    tTMO_SLL_NODE       *pNode;*/
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE KeyStopTime;

    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));
    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }
    i4Len = (INT4) STRLEN (pu1StopAcceptTime);
    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Get the ip Address form the interface */
    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }
    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StopAcceptTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    KeyStopTime.pu1_OctetList = pOspfKeyTime;
    KeyStopTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);

    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStopAccept = (INT4) GrGetSecondsSinceBase (tm);
    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

    pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, (UINT4) i4AddrlessIf);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        /* Test routine is called for configuring Key Stop Accept value */
        if (nmhTestv2FsMIOspfIfMD5AuthKeyStopAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStopAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Stop Accept Value */
        if (nmhSetFsMIOspfIfMD5AuthKeyStopAccept
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             i4KeyStopAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfIfAuthKeyStopAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfIfAuthKeyStopAccept
            ((INT4) u4OspfCxtId, u4IfIpAddr, i4AddrlessIf, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetStaggeringStatus                             */
/*                                                                            */
/* Description       : This Routine will set OSPF route table calculation     */
/*                     staggering status                                      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4StaggingStatus - Staggering status                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetStaggeringStatus (tCliHandle CliHandle, INT4 i4StaggingStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIOspfRTStaggeringStatus (&u4ErrorCode,
                                             i4StaggingStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong staggering status\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMIOspfRTStaggeringStatus (i4StaggingStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetStaggeringInterval                           */
/*                                                                            */
/* Description       : This Routine will set OSPF route table calculation     */
/*                     staggering interval                                    */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4StaggingStatus - Staggering status                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetStaggeringInterval (tCliHandle CliHandle, INT4 i4StaggingInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FutOspfRTStaggeringInterval (&u4ErrorCode,
                                              i4StaggingInterval) ==
        SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }
    if (nmhSetFutOspfRTStaggeringInterval (i4StaggingInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfType                                       */
/*                                                                            */
/* Description       : This Routine will set network type for an interface    */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4IfType   - Network type                              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfType (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4IfType)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;
    UINT4               u4TempIfIndex = 0;
    UINT4               u4OspfCxtId;
    UINT4               u4IfMtuSize = 0;
    UINT4               u4IfSpeed = 0;
    UINT1               u1IfType = 0;
    UINT1               u1OperStat = 0;
    INT4                i4AddrlessIfUn = 0;
    INT4                StatusVal = 0;
    tIPADDR             ifIpAddr;
    tIPADDR             ifIpAddrMask;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF
         * iftable using the
         *          * CREATE AND WAIT  */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    if (OspfCliGetCxtIdFromIfIndex
        (CliHandle, u4IfIndex, &u4OspfCxtId) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid contextId for Interface\r\n");
        return CLI_FAILURE;
    }
    if (i4IfType == 0)
    {
        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

        if (IpifGetIfParamsInCxt (u4OspfCxtId,
                                  ifIpAddr, i4AddrlessIf,
                                  &u4TempIfIndex, ifIpAddrMask, &u4IfMtuSize,
                                  &u4IfSpeed, &u1IfType,
                                  &u1OperStat) == OSPF_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        else
        {
            /* IN ISS, the unnumbered interface is kinda simulated
             * when the vlan IP address is ZERO.
             */
            if (i4AddrlessIf == 0)
            {
                i4IfType = (INT4) u1IfType;
            }
            else
            {
                i4IfType = (INT4) IF_PTOP;
            }
        }
    }

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        i4AddrlessIfUn = u4IfIndex;
        if (nmhGetOspfIfStatus (0, i4AddrlessIfUn, &i4StatusVal) ==
            SNMP_SUCCESS)
        {
            if (i4IfType != IF_PTOP)
            {
                CliPrintf (CliHandle,
                           "\r%% Unnumbered interface type cannot "
                           "be set to other types.\r\n");
            }
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r%% Interface Not Present\r\n");
        return CLI_FAILURE;
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfIfType (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                             i4IfType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfType (u4IfIpAddr, i4AddrlessIf, i4IfType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (StatusVal == NOT_IN_SERVICE)
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfMetricValue                                */
/*                                                                            */
/* Description       : This Routine will set the cost of sending a packet     */
/*                     on an interface                                        */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index of the give interface     */
/*                     i4MetricValue - Metric value for the interface         */
/*                     i4MetricTOS   - Metric TOS                             */
/*                     u1NonTos0Delete - Status to create/delete a row        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfMetricValue (tCliHandle CliHandle, UINT4 u4IfIndex,
                         INT4 i4MetricValue, INT4 i4MetricTOS,
                         UINT1 u1NonTos0Delete)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;
    INT4                StatusVal = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }
    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the 
         * CREATE AND WAIT  */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    if (u1NonTos0Delete == OSPF_FALSE)
    {
        /* Get the Interface Status and check whether the entry is already 
         * created or not, create an entry in the interface table */
        if (nmhGetOspfIfMetricStatus (u4IfIpAddr, i4AddrlessIf,
                                      i4MetricTOS,
                                      &i4StatusVal) == SNMP_FAILURE)
        {
            if (nmhTestv2OspfIfMetricStatus (&u4ErrorCode, u4IfIpAddr,
                                             i4AddrlessIf, i4MetricTOS,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfMetricStatus (u4IfIpAddr, i4AddrlessIf, i4MetricTOS,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        /* Checks whether the passed parameter value falls within the mib range */
        if (nmhTestv2OspfIfMetricValue (&u4ErrorCode, u4IfIpAddr,
                                        i4AddrlessIf, i4MetricTOS,
                                        i4MetricValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfIfMetricValue (u4IfIpAddr,
                                     i4AddrlessIf, i4MetricTOS,
                                     i4MetricValue) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /*Make the row status active after the set */
        if (nmhSetOspfIfMetricStatus (u4IfIpAddr, i4AddrlessIf,
                                      i4MetricTOS, ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2OspfIfMetricStatus (&u4ErrorCode, u4IfIpAddr,
                                         i4AddrlessIf, i4MetricTOS, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfMetricStatus (u4IfIpAddr, i4AddrlessIf,
                                      i4MetricTOS, DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (StatusVal == NOT_IN_SERVICE)
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetAdminStat                                    */
/*                                                                            */
/* Description       : This Routine will enable OSPF routing process          */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4AdminStat - OSPF_ENABLED/OSPF_DISABLED               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetAdminStatInCxt (tCliHandle CliHandle, INT4 i4AdminStat,
                          UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4PrevAdminStat;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    INT4                i4RowStat;

    MEMSET (au1Cmd, 0, sizeof (au1Cmd));

    if (i4AdminStat == OSPF_ENABLED)
    {
        i4RowStat = CREATE_AND_GO;
    }
    else
    {
        /* Delete the OSPF Context */
        i4RowStat = DESTROY;
    }

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    /* Check for context */
    if ((pOspfCxt == NULL) || (i4AdminStat == OSPF_DISABLED))
    {
        if (nmhTestv2FsMIStdOspfStatus
            (&u4ErrorCode, u4OspfCxtId, i4RowStat) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to create or delete context */
        if (nmhSetFsMIStdOspfStatus (u4OspfCxtId, i4RowStat) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4AdminStat == OSPF_DISABLED)
    {
        /* Successfully removed existing configurations 
         * and deleted context */
        return CLI_SUCCESS;
    }

    if (nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4PrevAdminStat)
        == SNMP_FAILURE)
    {
        i4RowStat = DESTROY;
        nmhSetFsMIStdOspfStatus (u4OspfCxtId, i4RowStat);
        return CLI_FAILURE;
    }

    if ((i4AdminStat == OSPF_ENABLED) && (i4PrevAdminStat != i4AdminStat))
    {
        /* Checks whether the passed parameter value falls 
         * within the mib range */
        if (nmhTestv2FsMIStdOspfAdminStat (&u4ErrorCode, u4OspfCxtId,
                                           i4AdminStat) == SNMP_FAILURE)
        {
            i4RowStat = DESTROY;
            nmhSetFsMIStdOspfStatus (u4OspfCxtId, i4RowStat);
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetFsMIStdOspfAdminStat (u4OspfCxtId, i4AdminStat)
            == SNMP_FAILURE)
        {
            i4RowStat = DESTROY;
            nmhSetFsMIStdOspfStatus (u4OspfCxtId, i4RowStat);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_MODE_ROUTER_OSPF, u4OspfCxtId);
    if (i4AdminStat == OSPF_ENABLED)
    {
        CliChangePath ((CONST CHR1 *) au1Cmd);
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetDefaultOriginate                             */
/*                                                                            */
/* Description       : This Routine will generate a default external route    */
/*                     in to OSPF routing domain                              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4Metric   - Metric for the route                      */
/*                     i4MetricType - Metric type for the route               */
/*                     u4FutOspfExtRouteDest - Route destination              */
/*                     u4FutOspfExtRouteMask - Route Mask                     */
/*                     u4FutOspfExtRouteTOS - Route TOS                       */
/*                u1Flag     - OSPF_ENABLED/OSPF_DISABLED                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetDefaultOriginate (tCliHandle CliHandle, INT4 i4Metric,
                            INT4 i4MetricType, UINT4 u4FutOspfExtRouteDest,
                            UINT4 u4FutOspfExtRouteMask,
                            UINT4 u4FutOspfExtRouteTOS, UINT1 u1flag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StatusVal = 0;
    INT4                i4RouteMetric = 0;
    INT4                i4RouteMetricType = 0;

    /* check whether the interface index entry is already created
       or not, if not create an entry in the OSPF external route  table */
    if (nmhGetFutOspfExtRouteStatus
        (u4FutOspfExtRouteDest, u4FutOspfExtRouteMask, u4FutOspfExtRouteTOS,
         &i4StatusVal) == SNMP_FAILURE)
    {
        /*Create an row in ospf external routing table using create and wait */

        if (nmhSetFutOspfExtRouteStatus
            (u4FutOspfExtRouteDest, u4FutOspfExtRouteMask, u4FutOspfExtRouteTOS,
             CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (i4Metric != OSPF_CLI_INV_VALUE)
    {
        if (u1flag == OSPF_DISABLED)
        {
            nmhGetFutOspfExtRouteMetric (u4FutOspfExtRouteDest,
                                         u4FutOspfExtRouteMask,
                                         u4FutOspfExtRouteTOS, &i4RouteMetric);
            if (i4RouteMetric == i4Metric)
            {
                i4Metric = CLI_OSPF_AS_EXT_DEF_METRIC;
            }
            else
            {
                CLI_SET_ERR (CLI_OSPF_MISMATCH_VALUE);
                return CLI_FAILURE;
            }
        }
        if (nmhTestv2FutOspfExtRouteMetric (&u4ErrorCode,
                                            u4FutOspfExtRouteDest,
                                            u4FutOspfExtRouteMask,
                                            u4FutOspfExtRouteTOS,
                                            i4Metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (i4MetricType != OSPF_CLI_INV_VALUE)
    {
        if (u1flag == OSPF_DISABLED)
        {
            nmhGetFutOspfExtRouteMetricType (u4FutOspfExtRouteDest,
                                             u4FutOspfExtRouteMask,
                                             u4FutOspfExtRouteTOS,
                                             &i4RouteMetricType);
            if (i4RouteMetricType == i4MetricType)
            {
                i4MetricType = TYPE_2_METRIC;
            }
            else
            {
                CLI_SET_ERR (CLI_OSPF_MISMATCH_VALUE);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FutOspfExtRouteMetricType (&u4ErrorCode,
                                                u4FutOspfExtRouteDest,
                                                u4FutOspfExtRouteMask,
                                                u4FutOspfExtRouteTOS,
                                                i4MetricType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    /* Calls the SNMP set routine to set the value    */
    if (i4Metric != OSPF_CLI_INV_VALUE)
    {
        if (nmhSetFutOspfExtRouteMetric (u4FutOspfExtRouteDest,
                                         u4FutOspfExtRouteMask,
                                         u4FutOspfExtRouteTOS,
                                         i4Metric) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4MetricType != OSPF_CLI_INV_VALUE)
    {
        if (nmhSetFutOspfExtRouteMetricType (u4FutOspfExtRouteDest,
                                             u4FutOspfExtRouteMask,
                                             u4FutOspfExtRouteTOS,
                                             i4MetricType) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /*Set the external route status active */
    if (nmhSetFutOspfExtRouteStatus (u4FutOspfExtRouteDest,
                                     u4FutOspfExtRouteMask,
                                     u4FutOspfExtRouteTOS,
                                     ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelDefaultOriginate                             */
/*                                                                            */
/* Description       : This Routine will delete  a default external route     */
/*                     added in to OSPF routing domain                        */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4FutOspfExtRouteDest - Route destination              */
/*                     u4FutOspfExtRouteMask - Route Mask                     */
/*                     u4FutOspfExtRouteTOS - Route TOS                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelDefaultOriginate (tCliHandle CliHandle,
                            UINT4 u4FutOspfExtRouteDest,
                            UINT4 u4FutOspfExtRouteMask,
                            UINT4 u4FutOspfExtRouteTOS)
{
    INT4                i4StatusVal = 0;

    /* check whether the interface index entry is already created
       or not, if not create an entry in the OSPF external route  table */
    if (nmhGetFutOspfExtRouteStatus
        (u4FutOspfExtRouteDest, u4FutOspfExtRouteMask, u4FutOspfExtRouteTOS,
         &i4StatusVal) == SNMP_FAILURE)
    {
        /* No default routes in OSPF external database.
         * return success without doing anything
         */
        return CLI_SUCCESS;
    }

    /*Set the external route status active */
    if (nmhSetFutOspfExtRouteStatus (u4FutOspfExtRouteDest,
                                     u4FutOspfExtRouteMask,
                                     u4FutOspfExtRouteTOS,
                                     DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetAreaStub                                     */
/*                                                                            */
/* Description       : This Routine will set Area as Stub Area                */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4AreaId   - Area ID                                   */
/*                     i4ImportAsExtern - Area Type                           */
/*                     i4AreaSummary    - Area Summary Type                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetAreaStub (tCliHandle CliHandle, UINT4 u4AreaId,
                    INT4 i4ImportAsExtern, INT4 i4AreaSummary)
{
    UINT4               u4ErrorCode = 0;
    INT4                StatusVal = 0;
    tAreaId             areaId;

    /* Checking whether the ifindex entry is already created or not and if it 
     * is not created already creating the interface entry in the OSPF 
     * Area table */

    OSPF_CRU_BMC_DWTOPDU (areaId, u4AreaId);

    if (nmhGetOspfAreaStatus (u4AreaId, &StatusVal) == SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF Area table using the CREATE
         * AND WAIT */
        if (nmhTestv2OspfAreaStatus (&u4ErrorCode, u4AreaId,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfAreaStatus (u4AreaId, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    /* Making the area as Stub Area */

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfImportAsExtern (&u4ErrorCode, u4AreaId,
                                     i4ImportAsExtern) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfImportAsExtern (u4AreaId, i4ImportAsExtern) == SNMP_FAILURE)
    {

        if (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS)
        {

            if (u4ErrorCode != CLI_OSPF_BACKBONE_AREA_ERR)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            else
            {
                return CLI_FAILURE;
            }
        }

    }
    /* Setting the no-summary or SendArea-summary depending on whether the 
     * optional argument is specified or not */

    /* Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2OspfAreaSummary (&u4ErrorCode, u4AreaId,
                                  i4AreaSummary) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfAreaSummary (u4AreaId, i4AreaSummary) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetOspfAreaStatus (u4AreaId, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetStubMetric                                   */
/*                                                                            */
/* Description       : This Routine will set Stub Metric for an Stub Area     */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4AreaId   - Area ID                                   */
/*                     i4MetricValue - Metric value for the Stub area         */
/*                     i4MetricTOS   - Metric TOS                             */
/*                     u1NonTos0Delete - Status to create/destroy             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetStubMetric (tCliHandle CliHandle, UINT4 u4AreaId, INT4 i4MetricValue,
                      INT4 i4MetricTOS, UINT1 u1NonTos0Delete)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StubStatus;

    if (u1NonTos0Delete == OSPF_FALSE)
    {
        /* Checking whether the Row  is already created or not and if it is not 
         * created already creating the Row instance in the Ospf Stub Area table */
        if (nmhGetOspfStubStatus (u4AreaId, i4MetricTOS,
                                  &i4StubStatus) == SNMP_FAILURE)
        {
            /* Testing the Row Creation */
            if (nmhTestv2OspfStubStatus (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Creating the row instance for the OSPF Stub Area table using the 
             * CREATE AND WAIT */
            if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else
        {
            if (!IS_VALID_TOS_VALUE (i4MetricTOS))
            {
                CliPrintf (CliHandle,
                           "\r %% Type of Service based routing support is not enabled \r\n");
                return CLI_FAILURE;
            }
        }

        /* Checks whether the passed parameter value falls within the mib range */

        if (nmhTestv2OspfStubMetric (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                     i4MetricValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfStubMetric (u4AreaId, i4MetricTOS,
                                  i4MetricValue) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                  ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (!IS_VALID_TOS_VALUE (i4MetricTOS))
        {
            CliPrintf (CliHandle, "\r%% Invalid TOS value\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2OspfStubStatus (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                     DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                  DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetNetworkArea                                  */
/*                                                                            */
/* Description       : This Routine will set Network addressses for an Area   */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4OspfAddressLessIf - AddressLess Interface index      */
/*                     u4IfIpAddr    - Network address                        */
/*                     u4AreaId      - Area ID for the network                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetNetworkArea (tCliHandle CliHandle, UINT4 u4OspfAddressLessIf,
                       UINT4 u4IfIpAddr, UINT4 u4AreaId)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Index = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4NetMask = 0;
    UINT1               u1IsPrimary = OSPF_FALSE;
    UINT1               u1IsSecondary = OSPF_FALSE;
    UINT4               u4PrimaryIP = 0;
    UINT4               u4SecondaryIP = 0;
    UINT4               u4IsNewRow = OSPF_FALSE;
    INT4                i4StatusVal = 0;
    UINT4               u4RetAreaId = 0;
    INT1                i1RetVal = 0;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    if (u4OspfAddressLessIf != 0)
    {
        u4IfIpAddr = 0;
    }
    if (u4OspfAddressLessIf == 0)
    {
        if (NetIpv4GetIfIndexFromAddrInCxt (pOspfCxt->u4OspfCxtId,
                                            u4IfIpAddr,
                                            &u4Index) == NETIPV4_FAILURE)

        {
            CliPrintf (CliHandle, "%%Invalid  IP address\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (NetIpv4GetPortFromIfIndex (u4OspfAddressLessIf, &u4Index)
            == NETIPV4_FAILURE)
        {
            CliPrintf (CliHandle, "%%Invalid  Interface Index\r\n");
            return CLI_FAILURE;
        }
    }

    if (NetIpv4GetIfInfo (u4Index, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        CliPrintf (CliHandle, "%%Invalid  IP address\r\n");
        return (CLI_FAILURE);
    }

    u4PrimaryIP = NetIpIfInfo.u4Addr;

    if ((NetIpIfInfo.u4Addr == u4IfIpAddr) || (u4OspfAddressLessIf != 0))
    {
        u1IsPrimary = OSPF_TRUE;
        if (nmhGetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                &i4StatusVal) == SNMP_FAILURE)
        {

            if (nmhTestv2OspfIfStatus
                (&u4ErrorCode, u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                 CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                if (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS)
                {
                    if (u4ErrorCode != CLI_OSPF_INTERFACE_NOT_EXISTS)
                    {
                        return CLI_FAILURE;
                    }
                    else if (u4ErrorCode == CLI_OSPF_INTERFACE_NOT_EXISTS)
                    {
                        return CLI_FAILURE;
                    }
                }
                return CLI_FAILURE;
            }
            if (nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                    CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            else
            {
                u4IsNewRow = OSPF_TRUE;
            }
        }
        else
        {
            if (nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                    NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }

    }

    if (u1IsPrimary == OSPF_FALSE)
    {
        if ((nmhGetOspfIfStatus (NetIpIfInfo.u4Addr, (INT4) u4OspfAddressLessIf,
                                 &i4StatusVal) == SNMP_FAILURE)
            || (i4StatusVal != ACTIVE))
        {
            CliPrintf (CliHandle,
                       "%% Primary network should be configured before configuring Secondary network \r\n");
            return CLI_FAILURE;
        }
        while (NetIpv4GetNextSecondaryAddress
               (NetIpIfInfo.u4IfIndex, u4PrevIpAddr, &u4IpAddr,
                &u4NetMask) == NETIPV4_SUCCESS)
        {
            if (u4IpAddr == u4IfIpAddr)
            {
                u1IsSecondary = OSPF_TRUE;
                u4SecondaryIP = u4IfIpAddr;
                break;
            }
            u4PrevIpAddr = u4IpAddr;
        }
    }
    if (u1IsSecondary == OSPF_TRUE)
    {

        if (nmhGetFutOspfSecIfStatus
            (u4PrimaryIP, (INT4) u4OspfAddressLessIf, u4IfIpAddr, u4NetMask,
             &i4StatusVal) == SNMP_FAILURE)
        {
            if (nmhTestv2FutOspfSecIfStatus
                (&u4ErrorCode, NetIpIfInfo.u4Addr, (INT4) u4OspfAddressLessIf,
                 u4IfIpAddr, u4NetMask, CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                if (nmhGetOspfIfAreaId
                    (u4PrimaryIP, (INT4) u4OspfAddressLessIf,
                     &u4RetAreaId) != SNMP_FAILURE)
                {
                    if (u4RetAreaId != u4AreaId)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Secondary should be in the same OSPF Area as that of Primary OSPF network\r\n");
                    }
                }
                return CLI_FAILURE;
            }
            if (nmhSetFutOspfSecIfStatus
                (NetIpIfInfo.u4Addr, (INT4) u4OspfAddressLessIf, u4IfIpAddr,
                 u4NetMask, CREATE_AND_WAIT) == SNMP_FAILURE)

            {
                return CLI_FAILURE;
            }
            else
            {
                u4IsNewRow = OSPF_TRUE;
            }
        }
    }
    if ((u1IsSecondary == OSPF_FALSE) && (u1IsPrimary == OSPF_FALSE))
    {
        CliPrintf (CliHandle, "%% Invalid network \r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the value falls with in range or not */
    if (nmhGetOspfAreaStatus (u4AreaId, &i4StatusVal) == SNMP_FAILURE)
    {
        if (u1IsSecondary == OSPF_TRUE)
        {
            CliPrintf (CliHandle,
                       "%% Secondary should be in the same OSPF Area as that of Primary OSPF network\r\n");
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetFutOspfSecIfStatus (NetIpIfInfo.u4Addr,
                                              (INT4) u4OspfAddressLessIf,
                                              u4IfIpAddr, u4NetMask, DESTROY);
                return CLI_FAILURE;
            }
        }
        if (nmhTestv2OspfAreaStatus (&u4ErrorCode,
                                     u4AreaId, CREATE_AND_GO) == SNMP_FAILURE)
        {
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                        DESTROY);
            }
            return CLI_FAILURE;
        }

        /* Creating the row instance for the OSPF Area table using the CREATE
         * AND WAIT */
        if (nmhSetOspfAreaStatus (u4AreaId, CREATE_AND_GO) == SNMP_FAILURE)
        {
            i1RetVal =
                nmhSetOspfIfStatus (u4IfIpAddr, u4OspfAddressLessIf, DESTROY);
            return CLI_FAILURE;
        }
    }
    if (u1IsSecondary == OSPF_TRUE)
    {
        if (nmhGetOspfIfAreaId
            (u4PrimaryIP, (INT4) u4OspfAddressLessIf,
             &u4RetAreaId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Entry\r\n");
            return CLI_FAILURE;
        }
        if (u4RetAreaId != u4AreaId)
        {
            CliPrintf (CliHandle,
                       "\r%% Secondary should be in the same OSPF Area as that of Primary OSPF network\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2OspfIfAreaId (&u4ErrorCode, u4IfIpAddr,
                                   (INT4) u4OspfAddressLessIf,
                                   u4AreaId) == SNMP_FAILURE)
        {
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                        DESTROY);
            }
            return CLI_FAILURE;
        }
        if (nmhSetOspfIfAreaId
            (u4IfIpAddr, (INT4) u4OspfAddressLessIf, u4AreaId) == SNMP_FAILURE)
        {
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                        DESTROY);
            }
            return CLI_FAILURE;
        }
    }
    /* Unnumbered interface will be of type - PointToPoint */
    if (u4OspfAddressLessIf != 0)
    {
        if (nmhTestv2OspfIfType
            (&u4ErrorCode, u4IfIpAddr, (INT4) u4OspfAddressLessIf,
             IF_PTOP) == SNMP_FAILURE)
        {
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                        DESTROY);
            }
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfType (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                              IF_PTOP) == SNMP_FAILURE)
        {
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                        DESTROY);
            }
            return CLI_FAILURE;
        }
    }
    if (u1IsPrimary == OSPF_TRUE)
    {
        if (nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                ACTIVE) == SNMP_FAILURE)
        {
            if (u4IsNewRow == OSPF_TRUE)
            {
                i1RetVal =
                    nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                        DESTROY);
            }
            return CLI_FAILURE;
        }
    }

    if (u1IsSecondary == OSPF_TRUE)
    {
        if (nmhSetFutOspfSecIfStatus
            (NetIpIfInfo.u4Addr, (INT4) u4OspfAddressLessIf, u4SecondaryIP,
             u4NetMask, ACTIVE) == SNMP_FAILURE)

        {
            i1RetVal =
                nmhSetFutOspfSecIfStatus (NetIpIfInfo.u4Addr,
                                          (INT4) u4OspfAddressLessIf,
                                          u4SecondaryIP, u4NetMask, DESTROY);
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (i1RetVal);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetRFC1583Compatibility                         */
/*                                                                            */
/* Description       : This Routine will set compatibility for RFC1583        */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4FutInt   - OSPF_ENABLED/OSPF_DISABLED                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetRFC1583Compatibility (tCliHandle CliHandle, INT4 i4FutInt)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2FutOspfRFC1583Compatibility (&u4ErrorCode, i4FutInt) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfRFC1583Compatibility (i4FutInt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetRouterId                                     */
/*                                                                            */
/* Description       : This Routine will set Router Id For a Router           */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4RouterId - Router ID for OSPF                        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetRouterId (tCliHandle CliHandle, UINT4 u4RouterId)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfRouterId (&u4ErrorCode, u4RouterId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfRouterId (u4RouterId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelRouterId                                     */
/*                                                                            */
/* Description       : This Routine will reset Router Id For a Router         */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4OspfCxtId - ospf context ID for OSPF                 */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelRouterId (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4RouterId = 0;
    tRouterId           rtrId;

    MEMSET (&rtrId, 0, sizeof (tRouterId));
    /* select valid lowest interface IP Address */
    if (UtilSelectOspfRouterId (u4OspfCxtId, &rtrId) != OSPF_SUCCESS)
    {
        CLI_SET_ERR (CLI_OSPF_NO_INTERFACE_ACTIVE);
        return CLI_FAILURE;
    }
    u4RouterId = OSPF_CRU_BMC_DWFROMPDU (rtrId);
    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfRouterId (&u4ErrorCode, u4RouterId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfRouterId (u4RouterId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutospfRouterIdPermanence (ROUTERID_DYNAMIC) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetNbrPriority                                  */
/*                                                                            */
/* Description       : This Routine will set the Neighbor Priority for a      */
/*                     Neighbor                                               */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIpAddr - Neighbor IP Addr                          */
/*                     i4AddrlessIf - Interface Index via which the neighbor  */
/*                                    can be reached                          */
/*                     u1Flag     - OSPF_ENABLED/OSPF_DISABLED                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetNbrPriority (tCliHandle CliHandle, UINT4 u4IfIpAddr,
                       INT4 i4NbrPriority, INT4 i4AddrlessIf, UINT1 u1Flag)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tIPADDR             nbrIpAddr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4ErrorCode = 0;
    INT4                i4StatusVal = 0;

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4IfIpAddr);

    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    /*Neighbor can be configured only on NBMA or point-to-multipoint networks */
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        if (UtilIpAddrMaskComp (pInterface->ifIpAddr,
                                nbrIpAddr,
                                pInterface->ifIpAddrMask) == OSPF_EQUAL)
        {
            if ((pInterface->u1NetworkType != IF_NBMA) &&
                (pInterface->u1NetworkType != IF_PTOMP))
            {
                CLI_SET_ERR (CLI_OSPF_NBR_INV_IFACE);
                return CLI_FAILURE;
            }
        }
    }

    /*Get the Neighbor Status and check whether the entry is already created
       or not, create an entry in the Neighbor table */

    if (nmhGetOspfNbmaNbrStatus (u4IfIpAddr, i4AddrlessIf,
                                 &i4StatusVal) == SNMP_FAILURE)
    {
        if (u1Flag == OSPF_DISABLED)
        {
            CliPrintf (CliHandle, "\r%% No Such Neighbor exists\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2OspfNbmaNbrStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfNbmaNbrStatus (u4IfIpAddr, i4AddrlessIf,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (u1Flag == OSPF_DISABLED)
        nmhGetOspfNbrPriority (u4IfIpAddr, i4AddrlessIf, &i4NbrPriority);

    if (i4NbrPriority != 0 && u1Flag == OSPF_ENABLED)

    {

        if (nmhTestv2OspfNbrPriority (&u4ErrorCode, u4IfIpAddr,
                                      i4AddrlessIf,
                                      i4NbrPriority) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfNbrPriority (u4IfIpAddr, i4AddrlessIf,
                                   i4NbrPriority) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4NbrPriority != CLI_OSPF_DEF_NBR_PRIORITY && u1Flag == OSPF_DISABLED)
    {
        i4NbrPriority = CLI_OSPF_DEF_NBR_PRIORITY;

        if (nmhTestv2OspfNbrPriority (&u4ErrorCode, u4IfIpAddr,
                                      i4AddrlessIf,
                                      i4NbrPriority) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfNbrPriority (u4IfIpAddr, i4AddrlessIf,
                                   i4NbrPriority) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2OspfNbmaNbrStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                    ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfNbmaNbrStatus (u4IfIpAddr, i4AddrlessIf,
                                 ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

 /******************************************************************************/
/* Function Name     : OspfCliSetIfPollInterval                               */
/*                                                                            */
/* Description       : This Routine will set the Neighbor Poll interval for an*/
/*                     interface                                              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIpAddr - Interface IP Addr                          */
/*                     i4AddrlessIf - Interface Index via which the neighbor  */
/*                                    can be reached                          */
/*                     u1Flag     - OSPF_ENABLED/OSPF_DISABLED                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetIfPollInterval (tCliHandle CliHandle, UINT4 u4NbrAddr,
                          INT4 i4PollInterval, INT4 i4NbrAddrlessIf,
                          UINT1 u1Flag)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pIfLstNode;
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4StatusVal = 0;
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pIfLstNode);
        u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);

        /* Extracting the indices required for setting this object in SNMP (IP
           interface index */
        /* Cfa Util Function is used to get the Address for
         * the Interface Index Specified. This Function returns OSIX_SUCCESS and
         * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */
        if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                          &u4IfIpAddr,
                                          &i4AddrlessIf) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
            return CLI_FAILURE;
        }

        if (nmhGetOspfNbmaNbrStatus (u4NbrAddr, i4NbrAddrlessIf,
                                     &i4StatusVal) == SNMP_FAILURE)
        {
            if (u1Flag == OSPF_DISABLED)
            {
                CliPrintf (CliHandle, "\r%% No Such Neighbor exists\r\n");
                return CLI_FAILURE;
            }
        }
        /* Checks whether the passed parameter value falls within the mib range */
        if (nmhTestv2OspfIfPollInterval (&u4ErrorCode, u4IfIpAddr,
                                         i4AddrlessIf,
                                         i4PollInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfIfPollInterval (u4IfIpAddr, i4AddrlessIf,
                                      i4PollInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /*Test and set the row status of Interface entry to ACTIVE */

        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelNbr                                          */
/*                                                                            */
/* Description       : This Routine will delete the Neighbor                  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIpAddr - Neighbor IP Addr                          */
/*                     i4AddrlessIf - Interface Index via which the neighbor  */
/*                                    can be reached                          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelNbr (tCliHandle CliHandle, UINT4 u4IfIpAddr, INT4 i4AddrlessIf)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StatusVal = 0;

    /*Get the Neighbor Status and check whether the entry is already created
       or not, create an entry in the Neighbor table */

    if (nmhGetOspfNbmaNbrStatus (u4IfIpAddr, i4AddrlessIf,
                                 &i4StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Such Neighbor exists\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2OspfNbmaNbrStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfNbmaNbrStatus (u4IfIpAddr, i4AddrlessIf,
                                 DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetDemandCircuit                                */
/*                                                                            */
/* Description       : This Routine will set the Demand Circuit for an        */
/*                     interface                                              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index                           */
/*                     i4DemandExtn - OSPF_TRUE/OSPF_FALSE                    */
/*                     i4DemandCkt  - OSPF_TRUE/OSPF_FALSE                    */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetDemandCircuit (tCliHandle CliHandle, UINT4 u4IfIndex,
                         INT4 i4DemandExtn, INT4 i4DemandCkt)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                StatusVal = 0;
    INT4                i4AddrlessIf = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it is 
     * not created already, create the interface entry in the OSPF iftable */

    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &StatusVal) ==
        SNMP_FAILURE)
    {
        /* Creating the row instance for the OSPF iftable using the CREATE 
         * AND WAIT */
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }
    /* We need to set the Scalar Object Demand Extensions for enabling the 
     * Demand Circuit for an interface */

    if (nmhTestv2OspfDemandExtensions (&u4ErrorCode, i4DemandExtn) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfDemandExtensions (i4DemandExtn) == SNMP_FAILURE)
    {
        /* always returs success */
        return CLI_FAILURE;
    }
    /* After Setting the global object Demand Extensions , now set the Demand 
     * Circuit for the interface */

    /*Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2OspfIfDemand (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                               i4DemandCkt) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfDemand (u4IfIpAddr, i4AddrlessIf, i4DemandCkt) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((StatusVal == NOT_IN_SERVICE) && (i4DemandCkt == OSPF_FALSE))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfAuthType                                   */
/*                                                                            */
/* Description       : This Routine will set the Authentication Type for an   */
/*                     interface                                              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4IfIndex  - Interface Index                           */
/*                     i4AuthType - Authentication Type for the interface     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfAuthType (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4AuthType,
                      INT4 i4CryptoAuthType)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIpAddr = 0;
    INT4                i4StatusVal = 0;
    INT4                i4AddrlessIf = 0;
/*    INT4                i4Cxtid = gOsRtr.u4OspfCxtId; */
    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    /* Checking whether the ifindex entry is already created or not and if it 
     * is not created already creating the interface entry in the OSPF iftable */
    if (nmhGetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, &i4StatusVal) ==
        SNMP_FAILURE)
    {
        if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2OspfIfAuthType (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                 i4AuthType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfIfAuthType (u4IfIpAddr, i4AddrlessIf,
                              i4AuthType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4CryptoAuthType >= 0)
    {
        /* checks whether the passed parameter value falls within the mib range */
        if (nmhTestv2OspfIfCryptoAuthType
            (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
             i4CryptoAuthType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFutOspfIfCryptoAuthType (u4IfIpAddr, i4AddrlessIf,
                                           i4CryptoAuthType) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((i4StatusVal == NOT_IN_SERVICE) && (i4AuthType == NO_AUTHENTICATION) &&
        (i4CryptoAuthType == 0))
    {
        if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) i4AddrlessIf) ==
            CLI_SUCCESS)
        {
            if (nmhTestv2OspfIfStatus (&u4ErrorCode, u4IfIpAddr, i4AddrlessIf,
                                       DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, i4AddrlessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetVirtualLink                                  */
/*                                                                            */
/* Description       : This Routine will create a virtual link                */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     pArea      - Virtual link area parameters              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetVirtualLink (tCliHandle CliHandle, tAreaStruct * pArea)
{
    UINT4               u4ErrorCode = 0;
    INT4                StatusVal = 0;
    INT4                i4AreaType = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;
    tAreaId             areaId;
    /* INT4                i4Cxtid = gOsRtr.u4OspfCxtId; */

    /* Virtual link cannot be configured on NSSA area */
    if (nmhGetOspfImportAsExtern (pArea->u4AreaId, &i4AreaType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4AreaType == NSSA_AREA)
    {
        CLI_SET_ERR (CLI_OSPF_INV_VIRT_AREA_NSSA);
        return CLI_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (areaId, pArea->u4AreaId);
    if (UtilIpAddrComp (areaId, gNullIpAddr) == OSPF_EQUAL)
    {
        CLI_SET_ERR (CLI_OSPF_INV_VIRT_BACKBONE_AREA);
        return CLI_FAILURE;
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2OspfRouterId (&u4ErrorCode, pArea->u4RouterId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Check the Bit Mask to verify which optional arguments are set */
    if (pArea->u1AreaBitMask & OSPF_VIRT_NO_OPTIONS)
    {
        /* Checking whether the table entry is already created or not and if it 
         * is not created already creating the interface entry in the Virtual 
         * OSPF iftable */

        if (nmhGetOspfVirtIfState (pArea->u4AreaId, pArea->u4RouterId,
                                   &StatusVal) == SNMP_FAILURE)
        {
            /* Creating the row instance for the OSPF Virtual iftable using the 
             * CREATE AND WAIT */

            if (nmhTestv2OspfVirtIfStatus (&u4ErrorCode, pArea->u4AreaId,
                                           pArea->u4RouterId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfVirtIfStatus (pArea->u4AreaId, pArea->u4RouterId,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_HELLO_INT)
    {
        /* Set the Hello interval  */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfHelloInterval (&u4ErrorCode,
                                              pArea->u4AreaId,
                                              pArea->u4RouterId,
                                              pArea->i4IfHelloInterval) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfHelloInterval (pArea->u4AreaId, pArea->u4RouterId,
                                           pArea->i4IfHelloInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_RETRANS_INT)
    {
        /* Set the Re-transmit Interval */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfRetransInterval
            (&u4ErrorCode, pArea->u4AreaId, pArea->u4RouterId,
             pArea->i4IfRetransInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfRetransInterval (pArea->u4AreaId, pArea->u4RouterId,
                                             pArea->i4IfRetransInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_TRANSMIT_DELAY)
    {
        /* Set the Transmit Delay  */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfTransitDelay (&u4ErrorCode, pArea->u4AreaId,
                                             pArea->u4RouterId,
                                             pArea->i4IfTransitDelay) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfTransitDelay (pArea->u4AreaId, pArea->u4RouterId,
                                          pArea->i4IfTransitDelay) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_DEAD_INT)
    {
        /*Set the Dead Interval value */
        /* Checks whether the passed parameter value falls within the mib 
           range */
        if (nmhTestv2OspfVirtIfRtrDeadInterval (&u4ErrorCode,
                                                pArea->u4AreaId,
                                                pArea->u4RouterId,
                                                pArea->i4IfRtrDeadInterval)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfRtrDeadInterval (pArea->u4AreaId, pArea->u4RouterId,
                                             pArea->i4IfRtrDeadInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_AUTH_KEY)
    {

        pOctetString = SNMP_AGT_FormOctetString (pArea->au1AuthKey,
                                                 STRLEN (pArea->au1AuthKey));

        if (pOctetString == NULL)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* Set the Authentication Key */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfAuthKey (&u4ErrorCode, pArea->u4AreaId,
                                        pArea->u4RouterId,
                                        pOctetString) == SNMP_FAILURE)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfAuthKey (pArea->u4AreaId, pArea->u4RouterId,
                                     pOctetString) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }
        SNMP_AGT_FreeOctetString (pOctetString);
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_MD5_KEY)
    {

        pOctetString = SNMP_AGT_FormOctetString (pArea->au1AuthKey,
                                                 STRLEN (pArea->au1AuthKey));
        if (pOctetString == NULL)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (pArea->i4IfCryptoAuthType == OSPF_AUTH_MD5)
        {

            /* Set the MD5 Authentication Key */
            /* Checks whether the passed parameter value falls within the mib 
               range */
            if (nmhTestv2FutOspfVirtIfMD5AuthKey (&u4ErrorCode,
                                                  pArea->u4AreaId,
                                                  pArea->u4RouterId,
                                                  pArea->i4AuthkeyId,
                                                  pOctetString) == SNMP_FAILURE)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }

            /* Calls the SNMP set routine to set the value    */
            if (nmhSetFutOspfVirtIfAuthKey (pArea->u4AreaId,
                                            pArea->u4RouterId,
                                            pArea->i4AuthkeyId,
                                            pOctetString) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            SNMP_AGT_FreeOctetString (pOctetString);
        }
        else
        {
            if (nmhTestv2FutOspfVirtIfAuthKey (&u4ErrorCode,
                                               pArea->u4AreaId,
                                               pArea->u4RouterId,
                                               pArea->i4AuthkeyId,
                                               pOctetString) == SNMP_FAILURE)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }

            /* Calls the SNMP set routine to set the value    */
            if (nmhSetFutOspfVirtIfAuthKey (pArea->u4AreaId,
                                            pArea->u4RouterId,
                                            pArea->i4AuthkeyId,
                                            pOctetString) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            SNMP_AGT_FreeOctetString (pOctetString);
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_AUTH_TYPE)
    {
        /* Set the Authentication Type  */
        /* Checks whether the passed parameter value falls within the mib 
           range */
        if (nmhTestv2OspfVirtIfAuthType (&u4ErrorCode,
                                         pArea->u4AreaId, pArea->u4RouterId,
                                         pArea->i4IfAuthType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfAuthType (pArea->u4AreaId,
                                      pArea->u4RouterId,
                                      pArea->i4IfAuthType) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (pArea->i4IfAuthType == CRYPT_AUTHENTICATION)
        {
            if (nmhTestv2OspfVirtIfCryptoAuthType (&u4ErrorCode,
                                                   pArea->u4AreaId,
                                                   pArea->u4RouterId,
                                                   pArea->i4IfCryptoAuthType) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFutOspfVirtIfCryptoAuthType (pArea->u4AreaId,
                                                   pArea->u4RouterId,
                                                   pArea->i4IfCryptoAuthType) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

    }
    /* After setting all the values in the VirOSPF iftable,make the Row 
     * status as 'Active'. */

    if (nmhTestv2OspfVirtIfStatus (&u4ErrorCode,
                                   pArea->u4AreaId,
                                   pArea->u4RouterId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfVirtIfStatus (pArea->u4AreaId,
                                pArea->u4RouterId, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetDefVirtualLink                               */
/*                                                                            */
/* Description       : This Routine will delete a virtual link                */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     pArea      - Virtual link area parameters              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetDefVirtualLink (tCliHandle CliHandle, tAreaStruct * pArea)
{
    UINT4               u4ErrorCode = 0;
    INT4                StatusVal = 0;
    tSNMP_OCTET_STRING_TYPE OctetString;

    if (nmhGetOspfVirtIfState (pArea->u4AreaId,
                               pArea->u4RouterId, &StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rOSPF: Specified area is not configured\r\n");
        return CLI_FAILURE;
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_HELLO_INT)
    {
        /* Set the Hello interval  */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfHelloInterval (&u4ErrorCode,
                                              pArea->u4AreaId,
                                              pArea->u4RouterId,
                                              pArea->i4IfHelloInterval) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfHelloInterval (pArea->u4AreaId, pArea->u4RouterId,
                                           pArea->i4IfHelloInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_RETRANS_INT)
    {
        /* Set the Re-transmit Interval */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfRetransInterval
            (&u4ErrorCode, pArea->u4AreaId, pArea->u4RouterId,
             pArea->i4IfRetransInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfTransitDelay (pArea->u4AreaId, pArea->u4RouterId,
                                          pArea->i4IfRetransInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_TRANSMIT_DELAY)
    {
        /* Set the Transmit Delay  */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfTransitDelay (&u4ErrorCode, pArea->u4AreaId,
                                             pArea->u4RouterId,
                                             pArea->i4IfTransitDelay) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfRetransInterval (pArea->u4AreaId, pArea->u4RouterId,
                                             pArea->i4IfTransitDelay) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_DEAD_INT)
    {
        /*Set the Dead Interval value */
        /* Checks whether the passed parameter value falls within the mib 
           range */
        if (nmhTestv2OspfVirtIfRtrDeadInterval (&u4ErrorCode,
                                                pArea->u4AreaId,
                                                pArea->u4RouterId,
                                                pArea->i4IfRtrDeadInterval)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfRtrDeadInterval (pArea->u4AreaId, pArea->u4RouterId,
                                             pArea->i4IfRtrDeadInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_AUTH_KEY)
    {

        OctetString.pu1_OctetList = NULL;
        OctetString.i4_Length = 0;

        /* Set the Authentication Key */
        /* Checks whether the passed parameter value falls within the mib 
           range */

        if (nmhTestv2OspfVirtIfAuthKey (&u4ErrorCode, pArea->u4AreaId,
                                        pArea->u4RouterId,
                                        &OctetString) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfAuthKey (pArea->u4AreaId, pArea->u4RouterId,
                                     &OctetString) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_MD5_KEY)
    {

        /* Check if the MD5 key exists */
        if (nmhTestv2FutOspfVirtIfMD5AuthKeyStatus
            (&u4ErrorCode, pArea->u4AreaId, pArea->u4RouterId,
             pArea->i4AuthkeyId, AUTHKEY_STATUS_DELETE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetFutOspfVirtIfMD5AuthKeyStatus (pArea->u4AreaId,
                                                 pArea->u4RouterId,
                                                 pArea->i4AuthkeyId,
                                                 AUTHKEY_STATUS_DELETE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pArea->u1AreaBitMask & OSPF_VIRT_AUTH_TYPE)
    {
        /* Set the Authentication Type  */
        /* Checks whether the passed parameter value falls within the mib 
           range */
        if (nmhTestv2OspfVirtIfAuthType (&u4ErrorCode,
                                         pArea->u4AreaId, pArea->u4RouterId,
                                         NO_AUTHENTICATION) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfVirtIfAuthType (pArea->u4AreaId,
                                      pArea->u4RouterId,
                                      NO_AUTHENTICATION) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    /* After setting all the values in the VirOSPF iftable,make the Row 
     * status as 'Active'. */

    if (nmhTestv2OspfVirtIfStatus (&u4ErrorCode,
                                   pArea->u4AreaId,
                                   pArea->u4RouterId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfVirtIfStatus (pArea->u4AreaId,
                                pArea->u4RouterId, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowNeighborInCxt                               */
/*                                                                            */
/* Description       : This Routine will display OSPF neighbor information    */
/*                     parameters based on the given option                   */
/*                     on a per-interface basis.                              */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                     u4OspfCxtId - Ospf context id                          */
/*                     u4IfIndex   - Interface Index                          */
/*                     u4NbrId     - Neighbor ID                              */
/*                     i4ShowNbrOption - Display Option                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowNeighborInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                          UINT4 u4IfIndex, UINT4 u4NbrId, INT4 i4ShowNbrOption,
                          UINT1 u1PrintNbrHdr)
{
    tTMO_SLL_NODE      *pListNode;
    tNeighbor          *pNeighbor;
    tIPADDR             nbrId;
    tIPADDR             ifIpAddr;
    CHR1               *pu1String = NULL;
    UINT1               au1NbrState[OSPF_MAX_NBR_STATE_STR_LEN];
    UINT1               au1NbrAddr[OSPF_MAX_IP_STR_LEN];
    UINT1               au1NbrId[OSPF_MAX_IP_STR_LEN];
    UINT1               au1backupDesgRtr[OSPF_MAX_IP_STR_LEN];
    UINT1               au1DesgRtr[OSPF_MAX_IP_STR_LEN];
    UINT1               au1AreaId[OSPF_MAX_IP_STR_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1HelperStatus[OSPF_MAX_IP_STR_LEN];
    UINT1               au1HelperER[OSPF_MAX_NBR_STATE_STR_LEN];
    UINT1               au1BfdStatus[BFD_CLI_MAX_STATE];
    UINT1               u1NbrPriority = 0;
    UINT1               u1NbrOptions = 0;
    UINT2               u2BufferLen = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    INT4                i4AddrlessIf = 0;
    INT4                i4AdminStat;
    UINT4               u4DeadTime = 0;
    UINT4               u4IfIpAddr = 0;
    UINT4               u4NbrEvents = 0;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    UINT4               u4NeighborId = 0;
    UINT4               u4IfDRRouterId = 0;
    UINT4               u4IfBDRRouterId = 0;
    UINT4               u4HelperAge = 0;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    UINT4               u4NbrRestartAge = 0;
    /*Checking the optional arguments in the command */
    CliPrintf (CliHandle, "\r\n");
    MEMSET (au1HelperER, 0, OSPF_MAX_NBR_STATE_STR_LEN);
    MEMSET (au1NbrState, 0, OSPF_MAX_NBR_STATE_STR_LEN);
    MEMSET (au1NbrId, 0, OSPF_MAX_IP_STR_LEN);
    MEMSET (au1NbrAddr, 0, OSPF_MAX_IP_STR_LEN);
    MEMSET (au1DesgRtr, 0, OSPF_MAX_IP_STR_LEN);
    MEMSET (au1backupDesgRtr, 0, OSPF_MAX_IP_STR_LEN);
    MEMSET (au1AreaId, 0, OSPF_MAX_IP_STR_LEN);

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u4ShowAllCxt = OSPF_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;

        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                /* Invalid context id or admin status of the 
                 * context is disabled */
                break;
            }

            continue;
        }
        if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
            OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "  Vrf  %s \r\n", au1OspfCxtName);

        if ((!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
            && (u1PrintNbrHdr == TRUE))
        {
            CliPrintf (CliHandle,
                       "\r  %-15s %-5s %-14s %-10s %-15s %-12s%-12s%-14s%-11s%-10s\r\n",
                       "Neighbor-ID", "Pri", "State", "DeadTime",
                       "Address", "Interface", "Helper", "HelperAge",
                       "HelperER", "Bfd");
            CliPrintf (CliHandle,
                       "\r  %-15s %-5s %-14s %-10s %-15s %-12s%-12s%-14s%-11s%-10s\r\n",
                       "-----------", "---", "-----", "--------",
                       "-------", "---------", "---------", "------------",
                       "---------", "-----");
        }

        /* Get the interface ip addr and addrless interface number using
         * interface index */
        /* Cfa Util Function is used to get the ifindex and Address for
         * the Interface Name Specified. This Function returns OSIX_SUCCESS and 
         * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

        if (OspfCliGetIndicesFromIfIndex (u4IfIndex, &u4IfIpAddr,
                                          &i4AddrlessIf) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
            return CLI_FAILURE;
        }

        CLI_MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        if (i4ShowNbrOption & OSPF_SHOW_NBR_INTF)
        {
            if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) == CLI_FAILURE)
            {
                /* Failure returned in fetching of interface name 
                 * will print '-' for interface name 
                 */
                STRCPY (au1IfName, "-");
            }
        }

        TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pListNode, tTMO_SLL_NODE *)
        {
            pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pListNode);

            OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                    (pNeighbor->nbrId));
            STRNCPY (au1NbrId, pu1String, (sizeof (au1NbrId) - 1));

            OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                    (pNeighbor->nbrIpAddr));
            STRNCPY (au1NbrAddr, pu1String, (sizeof (au1NbrAddr) - 1));

            OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                    (pNeighbor->desgRtr));

            STRNCPY (au1DesgRtr, pu1String, (sizeof (au1DesgRtr) - 1));

            OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                    (pNeighbor->backupDesgRtr));

            STRNCPY (au1backupDesgRtr, pu1String,
                     (sizeof (au1backupDesgRtr) - 1));

            if (pNeighbor->pInterface->pArea)
            {
                OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                        (pNeighbor->pInterface->pArea->areaId));

                STRNCPY (au1AreaId, pu1String, (sizeof (au1AreaId) - 1));
            }
            else
            {
                MEMSET (au1AreaId, 0, OSPF_MAX_IP_STR_LEN);
            }
            switch (pNeighbor->u1NbrHelperExitReason)
            {
                case OSPF_HELPER_NONE:
                    STRCPY (au1HelperER, " None");
                    break;
                case OSPF_HELPER_INPROGRESS:
                    STRCPY (au1HelperER, " In Progress");
                    break;
                case OSPF_HELPER_COMPLETED:
                    STRCPY (au1HelperER, " Completed");
                    break;
                case OSPF_HELPER_GRACE_TIMEDOUT:
                    STRCPY (au1HelperER, " Timed Out");
                    break;
                case OSPF_HELPER_TOP_CHG:
                    STRCPY (au1HelperER, " Topology Change");
                    break;
                default:
                    break;
            }

            u1NbrPriority = pNeighbor->u1NbrRtrPriority;
            u1NbrOptions = pNeighbor->nbrOptions;

            u4NbrEvents = pNeighbor->u4NbrEvents;

            if (!(pNeighbor->u1NsmState < MAX_NBR_STATE))
            {
                break;
            }
            STRCPY (au1NbrState, au1DbgNbrState[pNeighbor->u1NsmState]);

            STRNCAT (au1NbrState, "/", OSPF_ONE);

            if ((pNeighbor->pInterface->u1NetworkType == IF_NBMA) ||
                (pNeighbor->pInterface->u1NetworkType == IF_BROADCAST))
            {

                if ((pNeighbor->pInterface->u1IsmState == IFS_DR) ||
                    (pNeighbor->pInterface->u1IsmState == IFS_BACKUP) ||
                    (pNeighbor->pInterface->u1IsmState == IFS_DR_OTHER))
                {
                    u4NeighborId = OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrId);

                    u4IfDRRouterId =
                        IfFindRouterIdFromIfAddr (pNeighbor->pInterface->
                                                  desgRtr,
                                                  pNeighbor->pInterface);
                    u4IfBDRRouterId =
                        IfFindRouterIdFromIfAddr (pNeighbor->pInterface->
                                                  backupDesgRtr,
                                                  pNeighbor->pInterface);

                    if ((u4NeighborId == IP_ANY_ADDR) &&
                        ((pNeighbor->u1NsmState == NBRS_DOWN) ||
                         (pNeighbor->u1NsmState == NBRS_ATTEMPT)))
                    {
                        u2BufferLen =
                            sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                        STRNCAT (au1NbrState, au1DbgIfState[IFS_DOWN],
                                 u2BufferLen <
                                 STRLEN (au1DbgIfState[IFS_DOWN]) ? u2BufferLen
                                 : STRLEN (au1DbgIfState[IFS_DOWN]));
                    }
                    else if (u4NeighborId == u4IfDRRouterId)
                    {
                        u2BufferLen =
                            sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                        STRNCAT (au1NbrState, au1DbgIfState[IFS_DR],
                                 u2BufferLen <
                                 STRLEN (au1DbgIfState[IFS_DR]) ? u2BufferLen :
                                 STRLEN (au1DbgIfState[IFS_DR]));
                    }
                    else if (u4NeighborId == u4IfBDRRouterId)
                    {
                        u2BufferLen =
                            sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                        STRNCAT (au1NbrState, au1DbgIfState[IFS_BACKUP],
                                 u2BufferLen <
                                 STRLEN (au1DbgIfState[IFS_BACKUP]) ?
                                 u2BufferLen :
                                 STRLEN (au1DbgIfState[IFS_BACKUP]));
                    }
                    else
                    {
                        u2BufferLen =
                            sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                        STRNCAT (au1NbrState, au1DbgIfState[IFS_DR_OTHER],
                                 u2BufferLen <
                                 STRLEN (au1DbgIfState[IFS_DR_OTHER]) ?
                                 u2BufferLen :
                                 STRLEN (au1DbgIfState[IFS_DR_OTHER]));
                    }
                }
                else
                {
                    if (!(pNeighbor->pInterface->u1IsmState < MAX_IF_STATE))
                    {
                        break;
                    }
                    u2BufferLen =
                        sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                    STRNCAT (au1NbrState,
                             au1DbgIfState[pNeighbor->pInterface->u1IsmState],
                             u2BufferLen <
                             STRLEN (au1DbgIfState
                                     [pNeighbor->pInterface->
                                      u1IsmState]) ? u2BufferLen :
                             STRLEN (au1DbgIfState
                                     [pNeighbor->pInterface->u1IsmState]));
                }
            }
            else
            {
                if (!(pNeighbor->pInterface->u1IsmState < MAX_IF_STATE))
                {
                    break;
                }
                u2BufferLen = sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                STRNCAT (au1NbrState,
                         au1DbgIfState[pNeighbor->pInterface->u1IsmState],
                         u2BufferLen <
                         STRLEN (au1DbgIfState
                                 [pNeighbor->pInterface->
                                  u1IsmState]) ? u2BufferLen :
                         STRLEN (au1DbgIfState
                                 [pNeighbor->pInterface->u1IsmState]));
            }

            if (TmrGetRemainingTime (gTimerLst,
                                     &(pNeighbor->
                                       inactivityTimer.timerNode),
                                     &(u4DeadTime)) == TMR_SUCCESS)
            {

                u4DeadTime = u4DeadTime / NO_OF_TICKS_PER_SEC;
            }
            else
            {
                u4DeadTime = 0;
            }
            if (pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING)
            {
                if (TmrGetRemainingTime (gTimerLst,
                                         &(pNeighbor->helperGraceTimer.
                                           timerNode),
                                         &(u4HelperAge)) == TMR_SUCCESS)
                {

                    u4HelperAge = u4HelperAge / NO_OF_TICKS_PER_SEC;
                }
                else
                {
                    u4HelperAge = 0;
                }
                STRCPY (au1HelperStatus, "Helping");
            }
            else
            {
                u4HelperAge = 0;
                STRCPY (au1HelperStatus, "Not Helping");
            }

            CLI_MEMSET (au1BfdStatus, 0, BFD_CLI_MAX_STATE);
            if (pNeighbor->u1NbrBfdState != OSPF_BFD_ENABLED)
            {
                STRCPY (au1BfdStatus, " Disabled");
            }
            else
            {
                STRCPY (au1BfdStatus, " Enabled");
            }

            if (!(i4ShowNbrOption & OSPF_SHOW_NBR_INTF))
            {
                u4IfIndex =
                    OspfGetIfIndexFromPort (pNeighbor->pInterface->u4IfIndex);
                if (u4IfIndex == 0)
                {
                    u4IfIndex =
                        OspfGetIfIndexFromPort (pNeighbor->pInterface->
                                                u4AddrlessIf);
                }

                if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) ==
                    CLI_FAILURE)
                {
                    /* Failure returned in fetching of interface name 
                     * will print '-' for interface name 
                     */
                    STRCPY (au1IfName, "-");
                }
            }

            if ((i4ShowNbrOption & OSPF_SHOW_NBR_INTF) &&
                (i4ShowNbrOption & OSPF_SHOW_NBR_ID))
            {
                OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);

                if (UtilIpAddrComp (nbrId, pNeighbor->nbrId) == OSPF_EQUAL)
                {
                    if (i4AddrlessIf == 0)
                    {
                        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

                        if (UtilIpAddrComp
                            (ifIpAddr,
                             pNeighbor->pInterface->ifIpAddr) == OSPF_EQUAL)
                        {

                            if (!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
                            {
                                i4PageStatus = CliPrintf
                                    (CliHandle,
                                     "\r  %-15s %-5d %-14s %-10u %-15s %-12s"
                                     "%-12s%-13u%-11s%-10s\r\n",
                                     au1NbrId, u1NbrPriority, au1NbrState,
                                     u4DeadTime, au1NbrAddr, au1IfName,
                                     au1HelperStatus, u4HelperAge, au1HelperER,
                                     au1BfdStatus);
                            }
                            else
                            {
                                CliPrintf (CliHandle,
                                           " Neighbor %s, interface address %s\r\n",
                                           au1NbrId, au1NbrAddr);
                                CliPrintf (CliHandle,
                                           "   In the area %s via interface %s\r\n",
                                           au1AreaId, au1IfName);
                                CliPrintf (CliHandle,
                                           "   Neighbor priority is %d,  State is %s, "
                                           "%d state changes\r\n",
                                           u1NbrPriority, au1NbrState,
                                           u4NbrEvents);
                                CliPrintf (CliHandle,
                                           "   DR is %s BDR is %s\r\n",
                                           au1DesgRtr, au1backupDesgRtr);
                                i4PageStatus =
                                    CliPrintf (CliHandle,
                                               "   Options is 0x%x\r\n",
                                               u1NbrOptions);
                            }
                        }
                    }
                    else
                    {
                        if ((UINT4) i4AddrlessIf ==
                            pNeighbor->pInterface->u4AddrlessIf)
                        {
                            if (!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
                            {
                                i4PageStatus = CliPrintf
                                    (CliHandle,
                                     "\r  %-15s %-5d %-14s %-10u %-15s %-12s"
                                     "%-12s%-13u%-11s%-10s\r\n",
                                     au1NbrId, u1NbrPriority, au1NbrState,
                                     u4DeadTime, au1NbrAddr, au1IfName,
                                     au1HelperStatus, u4HelperAge, au1HelperER,
                                     au1BfdStatus);
                            }
                            else
                            {
                                CliPrintf (CliHandle,
                                           " Neighbor %s, interface address %s\r\n",
                                           au1NbrId, au1NbrAddr);
                                CliPrintf (CliHandle,
                                           "   In the area %s via interface %s\r\n",
                                           au1AreaId, au1IfName);
                                CliPrintf (CliHandle,
                                           "   Neighbor priority is %d,  State is %s, "
                                           "%d state changes\r\n",
                                           u1NbrPriority, au1NbrState,
                                           u4NbrEvents);
                                CliPrintf (CliHandle,
                                           "   DR is %s BDR is %s\r\n",
                                           au1DesgRtr, au1backupDesgRtr);
                                i4PageStatus =
                                    CliPrintf (CliHandle,
                                               "   Options is 0x%x\r\n",
                                               u1NbrOptions);
                            }
                        }
                    }
                }
            }
            else if (i4ShowNbrOption & OSPF_SHOW_NBR_INTF)
            {
                if (i4AddrlessIf == 0)
                {
                    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

                    if (UtilIpAddrComp
                        (ifIpAddr,
                         pNeighbor->pInterface->ifIpAddr) == OSPF_EQUAL)
                    {
                        if (!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
                        {
                            i4PageStatus = CliPrintf
                                (CliHandle,
                                 "\r  %-15s %-5d %-14s %-10u %-15s %-12s"
                                 "%-12s%-13u%-11s%-10s\r\n",
                                 au1NbrId, u1NbrPriority,
                                 au1NbrState, u4DeadTime, au1NbrAddr,
                                 au1IfName, au1HelperStatus, u4HelperAge,
                                 au1HelperER, au1BfdStatus);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       " Neighbor %s, interface address %s\r\n",
                                       au1NbrId, au1NbrAddr);
                            CliPrintf (CliHandle,
                                       "   In the area %s via interface %s\r\n",
                                       au1AreaId, au1IfName);
                            CliPrintf (CliHandle,
                                       "   Neighbor priority is %d,  State is %s, "
                                       "%d state changes\r\n",
                                       u1NbrPriority, au1NbrState, u4NbrEvents);
                            CliPrintf (CliHandle,
                                       "   DR is %s BDR is %s\r\n",
                                       au1DesgRtr, au1backupDesgRtr);
                            i4PageStatus = CliPrintf
                                (CliHandle, "   Options is 0x%x\r\n",
                                 u1NbrOptions);
                        }
                    }
                }
                else
                {
                    if ((UINT4) i4AddrlessIf ==
                        pNeighbor->pInterface->u4AddrlessIf)
                    {
                        if (!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
                        {
                            i4PageStatus = CliPrintf
                                (CliHandle,
                                 "\r  %-15s %-5d %-14s %-10u %-15s %-12s"
                                 "%-12s%-13u%-11s%-10s\r\n",
                                 au1NbrId, u1NbrPriority, au1NbrState,
                                 u4DeadTime, au1NbrAddr, au1IfName,
                                 au1HelperStatus, u4HelperAge, au1HelperER,
                                 au1BfdStatus);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       " Neighbor %s, interface address %s\r\n",
                                       au1NbrId, au1NbrAddr);
                            CliPrintf (CliHandle,
                                       "   In the area %s via interface %s\r\n",
                                       au1AreaId, au1IfName);
                            CliPrintf (CliHandle,
                                       "   Neighbor priority is %d,  State is %s, "
                                       "%d state changes\r\n",
                                       u1NbrPriority, au1NbrState, u4NbrEvents);
                            CliPrintf (CliHandle,
                                       "   DR is %s BDR is %s\r\n",
                                       au1DesgRtr, au1backupDesgRtr);
                            i4PageStatus = CliPrintf
                                (CliHandle, "   Options is 0x%x\r\n",
                                 u1NbrOptions);
                        }
                    }
                }
            }
            else if (i4ShowNbrOption & OSPF_SHOW_NBR_ID)
            {
                OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);
                if (UtilIpAddrComp (nbrId, pNeighbor->nbrId) == OSPF_EQUAL)
                {
                    if (!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
                    {
                        i4PageStatus = CliPrintf
                            (CliHandle,
                             "\r  %-15s %-5d %-14s %-10u %-15s %-12s"
                             "%-12s%-13u%-11s%-10s\r\n",
                             au1NbrId, u1NbrPriority, au1NbrState, u4DeadTime,
                             au1NbrAddr, au1IfName, au1HelperStatus,
                             u4HelperAge, au1HelperER, au1BfdStatus);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   " Neighbor %s, interface address %s\r\n",
                                   au1NbrId, au1NbrAddr);
                        CliPrintf (CliHandle,
                                   "   In the area %s via interface %s\r\n",
                                   au1AreaId, au1IfName);
                        CliPrintf (CliHandle,
                                   "   Neighbor priority is %d,  State is %s, "
                                   "%d state changes\r\n",
                                   u1NbrPriority, au1NbrState, u4NbrEvents);
                        CliPrintf (CliHandle,
                                   "   DR is %s BDR is %s\r\n",
                                   au1DesgRtr, au1backupDesgRtr);
                        i4PageStatus = CliPrintf
                            (CliHandle, "   Options is 0x%x\r\n", u1NbrOptions);
                    }
                }
            }
            else
            {
                if (!(i4ShowNbrOption & OSPF_SHOW_NBR_DETAIL))
                {
                    i4PageStatus = CliPrintf (CliHandle,
                                              "\r  %-15s %-5d %-14s %-10u %-15s %-12s"
                                              "%-12s%-13u%-11s%-10s\r\n",
                                              au1NbrId,
                                              u1NbrPriority, au1NbrState,
                                              u4DeadTime, au1NbrAddr,
                                              au1IfName,
                                              au1HelperStatus, u4HelperAge,
                                              au1HelperER, au1BfdStatus);
                }
                else
                {
                    CliPrintf (CliHandle,
                               " Neighbor %s, interface address %s\r\n",
                               au1NbrId, au1NbrAddr);
                    CliPrintf (CliHandle,
                               "   In the area %s via interface %s\r\n",
                               au1AreaId, au1IfName);
                    CliPrintf (CliHandle,
                               "   Neighbor priority is %d,  State is %s, "
                               "%d state changes\r\n",
                               u1NbrPriority, au1NbrState, u4NbrEvents);
                    CliPrintf (CliHandle,
                               "   DR is %s BDR is %s\r\n",
                               au1DesgRtr, au1backupDesgRtr);
                    i4PageStatus = CliPrintf
                        (CliHandle, "   Options is 0x%x\r\n", u1NbrOptions);
                    if (pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING)
                    {
                        CliPrintf (CliHandle, "     Acting as helper\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "     Not acting as helper\r\n");
                    }

                    if ((pNeighbor->u1NbrHelperStatus == OSPF_GR_HELPING) &&
                        (TmrGetRemainingTime
                         (gTimerLst,
                          (tTmrAppTimer *) (&(pNeighbor->
                                              helperGraceTimer.timerNode)),
                          &u4NbrRestartAge) == TMR_SUCCESS) &&
                        (u4NbrRestartAge != 0))
                    {
                        CliPrintf (CliHandle,
                                   "     remaining restart-interval: %d\r\n",
                                   u4NbrRestartAge);
                    }

                    if (pNeighbor->u1NbrHelperExitReason
                        == OSPF_RESTART_COMPLETED)
                    {
                        i4PageStatus = CliPrintf (CliHandle,
                                                  "    last NSF restart "
                                                  "completed successfully\r\n");
                    }
                    else if (pNeighbor->u1NbrHelperExitReason
                             == OSPF_RESTART_TIMEDOUT)
                    {
                        i4PageStatus = CliPrintf (CliHandle,
                                                  "    last NSF restart "
                                                  "timed out\r\n");
                    }
                    else if (pNeighbor->u1NbrHelperExitReason
                             == OSPF_RESTART_TOP_CHG)
                    {
                        i4PageStatus =
                            CliPrintf (CliHandle,
                                       "    last NSF restart resulted "
                                       "in topology change\r\n");
                    }
                }
            }

            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }
        }

        /* If u4ShowAllCxt is set to OSPF_TRUE then all context 
         * must be displayed */
        if (u4ShowAllCxt == OSPF_FALSE)
        {
            break;
        }

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    CliPrintf (CliHandle, "\r\n ");
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowRequestListInCxt                            */
/*                                                                            */
/* Description       : This Routine will display all Link State               */
/*                     Advertisements requested by the router                 */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4OspfCxtId - Ospf context id                          */
/*                     u4IfIndex  - Interface Index                           */
/*                     u4NbrId    - Neighbor ID                               */
/*                     i4ShowOption - Display Option                          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowRequestListInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                             UINT4 u4IfIndex, UINT4 u4NbrId, INT4 i4ShowOption)
{
    tNeighbor          *pNeighbor;
    tTMO_SLL_NODE      *pLstNode;
    tIPADDR             nbrId;
    tIPADDR             ifIpAddr;

    INT4                i4AddrlessIf;
    INT4                i4AdminStat;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1RtrIpAddr = NULL;
    UINT4               u4IfIpAddr = 0;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    /*Checking the optional arguments in the command */
    CliPrintf (CliHandle, "\r\n");

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u4ShowAllCxt = OSPF_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;

        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                /* Invalid context id or admin status of the 
                 * context is disabled */
                break;
            }

            continue;
        }

        switch (i4ShowOption)
        {
            case OSPF_CLI_SHOW_LSA_ALL:

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));
                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);

                /*print LSA information for all neighbor structure */
                TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                    i4PageStatus = OspfCliPrintLSAInfo (CliHandle, pNeighbor);
                    if (i4PageStatus == CLI_FAILURE)
                    {
                        break;
                    }
                }

                break;

            case OSPF_CLI_SHOW_LSA_NBR:

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));
                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);
                /*find all the neighbor structure for a given neighbor ID */
                TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);

                    if (UtilIpAddrComp (nbrId, pNeighbor->nbrId) == OSPF_EQUAL)
                    {
                        i4PageStatus = OspfCliPrintLSAInfo (CliHandle,
                                                            pNeighbor);
                        if (i4PageStatus == CLI_FAILURE)
                        {
                            break;
                        }
                    }
                }

                break;

            case OSPF_CLI_SHOW_LSA_IF:

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));
                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);
                /*find all the neighbor structure for a given Interface name */
                TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                    /* Get the interface ip addr and addrless interface number
                     * using interface index */
                    /* Cfa Util Function is used to get the ifindex and Address
                     * for the Interface Name Specified. This Function returns
                     * OSIX_SUCCESS and FAILURE. OSIX_SUCCESS is 0 and 
                     * OSIX_FAILURE is 1 */
                    if (OspfCliGetIndicesFromIfIndex (u4IfIndex, &u4IfIpAddr,
                                                      &i4AddrlessIf) ==
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Interface passed\r\n");
                        return CLI_FAILURE;
                    }
                    if (i4AddrlessIf == 0)
                    {
                        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

                        if (UtilIpAddrComp (ifIpAddr,
                                            pNeighbor->pInterface->ifIpAddr) ==
                            OSPF_EQUAL)
                        {
                            i4PageStatus =
                                OspfCliPrintLSAInfo (CliHandle, pNeighbor);
                            if (i4PageStatus == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        if ((UINT4) i4AddrlessIf ==
                            pNeighbor->pInterface->u4AddrlessIf)
                        {
                            i4PageStatus =
                                OspfCliPrintLSAInfo (CliHandle, pNeighbor);
                            if (i4PageStatus == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                    }
                }

                break;

            case OSPF_CLI_SHOW_LSA_IFNBR:

                /* find addrlessif for given interface name */
                /* Get the interface ip addr and addrless interface number using
                 * interface index */
                /* Cfa Util Function is used to get the ifindex and Address for
                 * the Interface Name Specified. This Function returns 
                 * OSIX_SUCCESS and FAILURE. OSIX_SUCCESS is 0 and 
                 * OSIX_FAILURE is 1 */
                if (OspfCliGetIndicesFromIfIndex (u4IfIndex, &u4IfIpAddr,
                                                  &i4AddrlessIf) ==
                    OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
                    return CLI_FAILURE;
                }

                OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);

                /*find a neighbor structure for given neighbor id and If */

                if ((pNeighbor = GetFindNbrInCxt (pOspfCxt, &nbrId,
                                                  (UINT4) i4AddrlessIf))
                    == NULL)
                {
                    CliPrintf (CliHandle, "\r%% Not able to find the neighbor"
                               "for given neighbor Id and Interface name\r\n");
                    return CLI_FAILURE;
                }

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));
                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);
                i4PageStatus = OspfCliPrintLSAInfo (CliHandle, pNeighbor);
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }
                break;

            default:
                CliPrintf (CliHandle, "\r%% Invalid Options entered\r\n");
                break;
        }

        /* If u4ShowAllCxt is set to OSPF_TRUE then all context 
         * must be displayed */
        if (u4ShowAllCxt == OSPF_FALSE)
        {
            break;
        }

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliPrintLSAInfo                                    */
/*                                                                            */
/* Description       : This Routine will dump all Link State                  */
/*                     Advertisement's detail                                 */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     pNeighbor  - Neighbor parameter to display             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliPrintLSAInfo (tCliHandle CliHandle, tNeighbor * pNeighbor)
{

    CHR1               *pu1NbrIpAddr = NULL;
    CHR1               *pu1IfIpAddr = NULL;
    tLsaReqNode        *pLsaReqNode;
    UINT1               au1IfName[OSPF_IFNAME_LEN];
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4IfIndex;

    MEMSET (au1IfName, 0, OSPF_IFNAME_LEN);

    u4IfIndex = OspfGetIfIndexFromPort (pNeighbor->pInterface->u4IfIndex);

    if (u4IfIndex == 0)
    {
        u4IfIndex =
            OspfGetIfIndexFromPort (pNeighbor->pInterface->u4AddrlessIf);
    }

    if (OspfGetInterfaceNameFromIndex (u4IfIndex, au1IfName) == OSIX_FAILURE)
    {
        STRCPY (au1IfName, "-");
    }

    OSPF_CLI_IPADDR_TO_STR (pu1NbrIpAddr,
                            OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrId));
    CliPrintf (CliHandle, "\r\nNeighbor %s, interface %s ",
               pu1NbrIpAddr, au1IfName);

    OSPF_CLI_IPADDR_TO_STR (pu1IfIpAddr,
                            OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrIpAddr));
    CliPrintf (CliHandle, "address %s\r\n\r\n", pu1IfIpAddr);
    CliPrintf (CliHandle, "\r%-4s %-16s %-16s %-13s %-6s %-8s\r\n",
               "Type", "LS-ID", "ADV-RTR", "SeqNo", "Age", "Checksum");
    CliPrintf (CliHandle, "\r%-4s %-16s %-16s %-13s %-6s %-8s\r\n", "----",
               "-----", "-------", "-----", "---", "--------");
    TMO_SLL_Scan (&(pNeighbor->lsaReqDesc.lsaReqLst), pLsaReqNode,
                  tLsaReqNode *)
    {
        i4PageStatus = OspfCliDumpLsHeader (CliHandle,
                                            (UINT1 *) &(pLsaReqNode->lsHeader));
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliDumpLsHeader                                    */
/*                                                                            */
/* Description       : This routine will print the LSA header information     */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                     pu1LsHeader - LSA Information                          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDumpLsHeader (tCliHandle CliHandle, UINT1 *pu1LsHeader)
{
    tLsHeader           sLsHeader;
    CHR1               *pu1String = NULL;
    UINT1               au1LinkStId[OSPF_MAX_IP_STR_LEN];
    UINT1               au1RouterId[OSPF_MAX_IP_STR_LEN];
    INT4                i4PageStatus = CLI_SUCCESS;

    UtilExtractLsHeaderFromLbuf (pu1LsHeader, &sLsHeader);

    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (sLsHeader.linkStateId));
    STRNCPY (au1LinkStId, pu1String, OSPF_MAX_IP_STR_LEN - 1);

    au1LinkStId[OSPF_MAX_IP_STR_LEN - 1] = '\0';

    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (sLsHeader.advRtrId));
    STRNCPY (au1RouterId, pu1String, OSPF_MAX_IP_STR_LEN - 1);

    au1RouterId[OSPF_MAX_IP_STR_LEN - 1] = '\0';
    i4PageStatus = CliPrintf (CliHandle,
                              "\r %-4d %-16s %-16s 0x%-11x %-6d 0x%-8x\r\n",
                              sLsHeader.u1LsaType, au1LinkStId,
                              au1RouterId, OSIX_NTOHL (sLsHeader.lsaSeqNum),
                              OSIX_NTOHS (sLsHeader.u2LsaAge),
                              OSIX_NTOHS (sLsHeader.u2LsaChksum));
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowRxmtListInCxt                               */
/*                                                                            */
/* Description       : This Routine will display all Link State               */
/*                     Advertisements waiting to be retransmitted             */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId  - ospf context id                         */
/*                     u4IfIndex    - Interface Index                         */
/*                     u4NbrId      - Neighbor ID                             */
/*                     i4ShowOption - Show option                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowRxmtListInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                          UINT4 u4IfIndex, UINT4 u4NbrId, INT4 i4ShowOption)
{
    tNeighbor          *pNeighbor;
    tTMO_SLL_NODE      *pLstNode;
    tIPADDR             nbrId;
    tIPADDR             ifIpAddr;

    INT4                i4AddrlessIf = 0;
    INT4                i4AdminStat;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4IfIpAddr = 0;
    CHR1               *pu1RtrIpAddr = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];

    /*Checking the optional arguments in the command */
    CliPrintf (CliHandle, "\r\n");

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u4ShowAllCxt = OSPF_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;

        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                /* Invalid context id or admin status of the 
                 * context is disabled */
                break;
            }

            continue;
        }

        switch (i4ShowOption)
        {
            case OSPF_CLI_SHOW_LSA_ALL:

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));

                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);

                /*print LSA information for all neighbor structure */
                TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                    i4PageStatus =
                        OspfCliPrintRxmtLSAInfo (CliHandle, pNeighbor);
                    if (i4PageStatus == CLI_FAILURE)
                    {
                        break;
                    }
                }
                break;

            case OSPF_CLI_SHOW_LSA_NBR:

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));

                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);

                /*find all the neighbor structure for a given neighbor ID */
                TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);

                    if (UtilIpAddrComp (nbrId, pNeighbor->nbrId) == OSPF_EQUAL)
                    {
                        i4PageStatus =
                            OspfCliPrintRxmtLSAInfo (CliHandle, pNeighbor);
                        if (i4PageStatus == CLI_FAILURE)
                        {
                            break;
                        }
                    }
                }

                break;

            case OSPF_CLI_SHOW_LSA_IF:

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));

                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);

                /*find all the neighbor structure for a given Interface name */
                TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pLstNode);

                    /* Get the interface ip addr and addrless interface number using
                     * interface index */
                    /* Cfa Util Function is used to get the ifindex and Address for
                     * the Interface Name Specified. This Function returns OSIX_SUCCESS and 
                     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */
                    if (OspfCliGetIndicesFromIfIndex (u4IfIndex, &u4IfIpAddr,
                                                      &i4AddrlessIf) ==
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Interface passed\r\n");
                        return CLI_FAILURE;
                    }
                    if (i4AddrlessIf == 0)
                    {
                        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);

                        if (UtilIpAddrComp (ifIpAddr,
                                            pNeighbor->pInterface->ifIpAddr) ==
                            OSPF_EQUAL)
                        {
                            i4PageStatus =
                                OspfCliPrintRxmtLSAInfo (CliHandle, pNeighbor);
                            if (i4PageStatus == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        if ((UINT4) i4AddrlessIf ==
                            pNeighbor->pInterface->u4AddrlessIf)
                        {
                            i4PageStatus =
                                OspfCliPrintRxmtLSAInfo (CliHandle, pNeighbor);
                            if (i4PageStatus == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                    }
                }

                break;

            case OSPF_CLI_SHOW_LSA_IFNBR:

                /* Get the interface ip addr and addrless interface number using
                 * interface index */
                /* Cfa Util Function is used to get the ifindex and Address for
                 * the Interface Name Specified. This Function returns OSIX_SUCCESS and 
                 * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */
                if (OspfCliGetIndicesFromIfIndex (u4IfIndex, &u4IfIpAddr,
                                                  &i4AddrlessIf) ==
                    OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
                    return CLI_FAILURE;
                }

                OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);

                /*find a neighbor structure for given neighbor id and If */
                if ((pNeighbor = GetFindNbrInCxt (pOspfCxt, &nbrId,
                                                  (UINT4) i4AddrlessIf))
                    == NULL)
                {
                    CliPrintf (CliHandle, "%% Not able to find the neighbor "
                               "for given neighbor Id and Interface name");
                    return CLI_FAILURE;
                }

                OSPF_CLI_IPADDR_TO_STR (pu1RtrIpAddr,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));

                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                    OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle,
                           " OSPF Router with ID (%s) (Vrf  %s )\r\n",
                           pu1RtrIpAddr, au1OspfCxtName);

                i4PageStatus = OspfCliPrintRxmtLSAInfo (CliHandle, pNeighbor);
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }
                break;

            default:
                CliPrintf (CliHandle, "\r%% Invalid Options entered");
                break;

        }

        /* If u4ShowAllCxt is set to OSPF_TRUE then all context 
         * must be displayed */
        if (u4ShowAllCxt == OSPF_FALSE)
        {
            break;
        }

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliPrintRxmtLSAInfo                                */
/*                                                                            */
/* Description       : This Routine will dump all Link State Advertise        */
/*                     Advertisement's detail                                 */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pNeighbor    - Neighbor details to display             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliPrintRxmtLSAInfo (tCliHandle CliHandle, tNeighbor * pNeighbor)
{
    CHR1               *pu1NbrIpAddr = NULL;
    CHR1               *pu1IfIpAddr = NULL;
    CHR1               *pu1LsId = NULL;
    CHR1               *pu1RtrId = NULL;
    UINT1               au1IfName[OSPF_IFNAME_LEN];

    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4RxmtQueueLen = 0;
    UINT4               u4IfIndex;
#ifndef HIGH_PERF_RXMT_LST_WANTED
    tTMO_DLL_NODE      *pLstNode;
#else
    UINT4               u4RxmtIndex = OSPF_INVALID_RXMT_INDEX;
    tRxmtNode          *pRxmtNode = NULL;
#endif
    tLsaInfo           *pLsaInfo;
    tOspfCxt           *pOspfCxt;

    /* Get the context pointer from neighbor */
    pOspfCxt = pNeighbor->pInterface->pArea->pOspfCxt;

    u4RxmtQueueLen = pNeighbor->lsaRxmtDesc.u4LsaRxmtCount;

    if (u4RxmtQueueLen != 0)
    {
        MEMSET (au1IfName, 0, OSPF_IFNAME_LEN);
        u4IfIndex = OspfGetIfIndexFromPort (pNeighbor->pInterface->u4IfIndex);

        if (u4IfIndex == 0)
            u4IfIndex =
                OspfGetIfIndexFromPort (pNeighbor->pInterface->u4AddrlessIf);

        if (OspfGetInterfaceNameFromIndex (u4IfIndex, au1IfName) ==
            OSIX_FAILURE)
        {
            STRCPY (au1IfName, "-");
        }
        OSPF_CLI_IPADDR_TO_STR (pu1NbrIpAddr,
                                OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrId));
        CliPrintf (CliHandle, "\r\nNeighbor %s, interface %s ",
                   pu1NbrIpAddr, au1IfName);

        OSPF_CLI_IPADDR_TO_STR (pu1IfIpAddr,
                                OSPF_CRU_BMC_DWFROMPDU (pNeighbor->nbrIpAddr));

        CliPrintf (CliHandle, "address %s\r\n", pu1IfIpAddr);

        CliPrintf (CliHandle, "Queue length %d\r\n\r\n", u4RxmtQueueLen);

        CliPrintf (CliHandle, "\r%-4s %-16s %-16s %-13s %-6s %-8s\r\n",
                   "Type", "LS-ID", "ADV-RTR", "SeqNo", "Age", "Checksum");
        CliPrintf (CliHandle, "\r%-4s %-16s %-16s %-13s %-6s %-8s\r\n", "----",
                   "-----", "-------", "-----", "---", "--------");

        /* Retransmission List is removed from area list and made part of global
         * structure. */
#ifndef HIGH_PERF_RXMT_LST_WANTED
        TMO_DLL_Scan (&(pOspfCxt->rxmtLst), pLstNode, tTMO_DLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_RXMT_LST (pLstNode);

            if (IS_RXMT_BIT_SET (pNeighbor->i2NbrsTableIndex, pLsaInfo))
            {
                OSPF_CLI_IPADDR_TO_STR (pu1LsId, OSPF_CRU_BMC_DWFROMPDU
                                        (pLsaInfo->lsaId.linkStateId));
                CliPrintf (CliHandle, "\r %-4d %-16s ",
                           pLsaInfo->lsaId.u1LsaType, pu1LsId);

                OSPF_CLI_IPADDR_TO_STR (pu1RtrId, OSPF_CRU_BMC_DWFROMPDU
                                        (pLsaInfo->lsaId.advRtrId));
                i4PageStatus =
                    CliPrintf (CliHandle, "%-16s 0x%-11x %-6d 0x%-8x\r\n",
                               pu1RtrId, pLsaInfo->lsaSeqNum,
                               pLsaInfo->u2LsaAge, pLsaInfo->u2LsaChksum);
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }
            }
        }
#else
        OSPF_GET_DLL_INDEX_IN_RXMT_NODE
            (pNeighbor->au1RxmtInfo, NBR_HEAD_NEXT_DLL_INDEX, u4RxmtIndex);

        if (u4RxmtIndex == OSPF_INVALID_RXMT_INDEX)
        {
            /* No retransmission list in this neighbor */
            return CLI_SUCCESS;
        }

        do
        {
            pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
            pLsaInfo = pRxmtNode->pLsaInfo;

            OSPF_CLI_IPADDR_TO_STR (pu1LsId, OSPF_CRU_BMC_DWFROMPDU
                                    (pLsaInfo->lsaId.linkStateId));
            CliPrintf (CliHandle, "\r %-4d %-16s ",
                       pLsaInfo->lsaId.u1LsaType, pu1LsId);

            OSPF_CLI_IPADDR_TO_STR (pu1RtrId, OSPF_CRU_BMC_DWFROMPDU
                                    (pLsaInfo->lsaId.advRtrId));
            i4PageStatus =
                CliPrintf (CliHandle, "%-16s 0x%-11x %-6d 0x%-8x\r\n",
                           pu1RtrId, pLsaInfo->lsaSeqNum,
                           pLsaInfo->u2LsaAge, pLsaInfo->u2LsaChksum);
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            OSPF_GET_DLL_INDEX_IN_RXMT_NODE
                (pRxmtNode->au1RxmtInfo, NBR_NEXT_DLL_INDEX, u4RxmtIndex);

        }
        while (u4RxmtIndex != OSPF_INVALID_RXMT_INDEX);
#endif
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowInterfaceInCxt                              */
/*                                                                            */
/* Description       : This Routine will show the OSPF Information for        */
/*                     an Interface                                           */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId - Context Id                               */
/*                     i4AddrlessIf - Addressless interface index             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowInterfaceInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                           INT4 i4AddrlessIf)
{
    tInterface         *pInterface;
    tTMO_SLL           *pLst;
    tTMO_SLL_NODE      *pLstNode;
    INT4                i4PageStatus = CLI_SUCCESS;
    INT4                i4AdminStat;
    UINT1               u1Index = 0;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = NULL;
    /* Checking whether optional argument is given or not in the command using 
     * u4IfIndex value. 
     */
    CliPrintf (CliHandle, "\r\n");
    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* GET the first CxtId */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPF_TRUE;
    }
    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;
        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat) ==
             SNMP_FAILURE) || (i4AdminStat ==
                               OSPF_DISABLED) || ((pOspfCxt =
                                                   UtilOspfGetCxt (u4OspfCxtId))
                                                  == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                break;
            }
            continue;
        }

        if (i4AddrlessIf == 0)
        {
            /* User has entered the command without the optional argument interface 
             * name */

            /* Display the OSPF information for all the interfaces in the interface 
             * table. This information can be obtained from the interface structure 
             * 'tinterface' of OSPF stack. */

            for (u1Index = 0; u1Index <= 1; u1Index++)
            {
                if (u1Index == 0)
                {
                    pLst = &(pOspfCxt->sortIfLst);
                }
                else
                {
                    pLst = &(pOspfCxt->virtIfLst);
                }
                TMO_SLL_Scan (pLst, pLstNode, tTMO_SLL_NODE *)
                {
                    pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
                    i4PageStatus =
                        OspfCliPrintOspfInterfaceInfo (CliHandle, pInterface);
                    if (i4PageStatus == CLI_FAILURE)
                    {
                        break;
                    }
                }
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }
            }
        }
        else
        {
            /* User has entered the command with the optional argument interface 
             * name */
            /* Display the OSPF information for the given interface. */

            /* Validate the Interface Index Provided */

            TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
            {
                pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
                if (OspfGetIfIndexFromPort (pInterface->u4IfIndex) ==
                    (UINT4) i4AddrlessIf)
                {
                    OspfCliPrintOspfInterfaceInfo (CliHandle, pInterface);
                    break;
                }
            }
        }

        /* while loop will execute when ShowAllCxt will be OSPF_TRUE */
        if (u4ShowAllCxt != OSPF_TRUE)
        {
            break;
        }

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId) !=
           OSPF_FAILURE);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliVlanShowInterfaceInCxt                          */
/*                                                                            */
/* Description       : This Routine will show the OSPF Information for        */
/*                     an Interface                                           */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId - Context Id                               */
/*                     i4AddrlessIf - Addressless interface index             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowVlanInterfaceInCxt (tCliHandle CliHandle, UINT1 *pu1VrfName,
                               UINT1 *pu1IfName)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfCxtId;
    UINT4               u4IfIndex;
    UINT4               u4NextIfIndex;
    INT4                i4RetVal = CLI_FAILURE;

    if (pu1VrfName != NULL)
    {
        i4RetVal = OspfCliGetCxtIdFromCxtName (CliHandle,
                                               pu1VrfName, &u4OspfCxtId);
    }
    if (CfaCliGetFirstIfIndexFromName (pu1IfName, &u4NextIfIndex) ==
        OSIX_FAILURE)
    {
        return CLI_SUCCESS;
    }
    if (pu1VrfName == NULL)
    {
        i4RetVal = OspfCliGetCxtIdFromIfIndex
            (CliHandle, u4NextIfIndex, &u4OspfCxtId);
    }
    if (i4RetVal == CLI_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        u4IfIndex = u4NextIfIndex;
        if (pu1VrfName == NULL)
        {
            i4RetVal = OspfCliGetCxtIdFromIfIndex
                (CliHandle, u4IfIndex, &u4OspfCxtId);
        }
        if (i4RetVal == CLI_FAILURE)
        {
            continue;
        }
        if ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL)
        {
            continue;
        }
        /* User has entered the command with the optional argument interface 
         * name */
        /* Display the OSPF information for the given interface. */

        /* Validate the Interface Index Provided */

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            if (OspfGetIfIndexFromPort (pInterface->u4IfIndex) ==
                (UINT4) u4IfIndex)
            {
                OspfCliPrintOspfInterfaceInfo (CliHandle, pInterface);
                break;
            }
        }
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliPrintOspfInterfaceInfo                          */
/*                                                                            */
/* Description       : This Routine will print the OSPF Information for       */
/*                     an Interface                                           */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pInterface   - Interface parameter to display          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliPrintOspfInterfaceInfo (tCliHandle CliHandle, tInterface * pInterface)
{
    tTMO_SLL_NODE      *pLstNode;
    tNeighbor          *pNbr;
    tCfaIfInfo          CfaIfInfo;
    CHR1               *pu1String = NULL;
    INT4                i4OspfIfMetricTOS = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4IfIndex;
    UINT4               u4HelloTime;
    UINT4               u4RouterId = 0;
    UINT4               u4IfDRRouterId = 0;
    UINT4               u4IfBDRRouterId = 0;
    UINT4               u4IfDesignatedRouter = 0;
    UINT4               u4IfBackupDesignatedRouter = 0;
    tOspfCxt           *pOspfCxt;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN];
    tAreaId             transitAreaId;
    tRouterId           destRtrId;
    INT4                i4RetValOspfVIState = 0;
    tMd5AuthkeyInfo    *pAuthkeyInfo;

    UINT1               au1IntState[MAX_IF_STATE][OSPF_MAX_IP_STR_LEN] =
        { "down",
        "loopback",
        "waiting",
        "PTOP",
        "DR", "BDR",
        "other"
    };

    pOspfCxt = pInterface->pArea->pOspfCxt;

    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN);
    /* Print all the required values from the interface structure using the 
     * pointer */
/*Virtual interface related details should be displayed only when the virtual link is up*/
    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        IP_ADDR_COPY (transitAreaId, pInterface->transitAreaId);
        IP_ADDR_COPY (destRtrId, pInterface->destRtrId);
        nmhGetFsMIStdOspfVirtIfState ((INT4) pOspfCxt->u4OspfCxtId,
                                      OSPF_CRU_BMC_DWFROMPDU (transitAreaId),
                                      OSPF_CRU_BMC_DWFROMPDU (destRtrId),
                                      &i4RetValOspfVIState);
        /* As per MIB 1850, Possible values for interface state of a virtual
         * link are down (1) and point-to-point (4).
         *  In OSPF implementation, ISM states are:IFS_DOWN (0) and IFS_PTOP (3).
         *  So nmhGetOspfVirtIfState function returns pInterface->u1IsmState +1
         *  To check against the #defined constants, we need to decrement the
         *  returned value by 1 */
        i4RetValOspfVIState = i4RetValOspfVIState - 1;
        if (i4RetValOspfVIState != IFS_PTOP)
        {
            return i4PageStatus;
        }
    }

    if (pInterface->ifStatus == ACTIVE)
    {
        u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
        if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_SUCCESS)
        {
            CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "\r%s is ", au1IfName);
            if (CfaIfInfo.u1IfOperStatus == CFA_IF_UP)
            {
                CliPrintf (CliHandle, "line protocol is up\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "line protocol is down\r\n");
            }
        }

        OSPF_CLI_IPADDR_TO_STR
            (pu1String, OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr));

        CliPrintf (CliHandle, "\r  Internet Address %s, ", pu1String);

        OSPF_CLI_IPADDR_TO_STR
            (pu1String, OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask));

        CliPrintf (CliHandle, "Mask %s, ", pu1String);

        OSPF_CLI_IPADDR_TO_STR
            (pu1String, OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId));

        CliPrintf (CliHandle, "Area %s\r\n", pu1String);

        u4RouterId = OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId);

        OSPF_CLI_IPADDR_TO_STR (pu1String, u4RouterId);
        CliPrintf (CliHandle, "  AS 1, Router ID %s, ", pu1String);

        switch (pInterface->u1NetworkType)
        {
            case IF_BROADCAST:
                CliPrintf (CliHandle, "Network Type BROADCAST, ");
                break;
            case IF_NBMA:
                CliPrintf (CliHandle, "Network Type NBMA, ");
                break;
            case IF_PTOP:
                CliPrintf (CliHandle, "Network Type PointToPoint, ");
                break;
            case IF_PTOMP:
                CliPrintf (CliHandle, "Network Type PointToMultiPoint, ");
                break;
            case IF_LOOPBACK:
                CliPrintf (CliHandle, "Network Type LOOPBACK,");
                break;
            case IF_VIRTUAL:
                CliPrintf (CliHandle, "Network Type VIRTUAL,");
                break;
            default:
                CliPrintf (CliHandle, "Network Type Unknown Network Type, ");
                break;
        }

        CliPrintf (CliHandle, "Cost %d\r\n",
                   pInterface->aIfOpCost[i4OspfIfMetricTOS].u4Value);

        if (pInterface->bDcEndpt == OSPF_TRUE)
        {
            CliPrintf (CliHandle, "  Configured as demand circuit.\r\n");
            CliPrintf (CliHandle, "  Run as demand circuit.\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  demand circuit is disabled \r\n");
        }

        CliPrintf (CliHandle,
                   "  Transmit Delay is %d sec, State %s, Priority %d\r\n",
                   pInterface->u2IfTransDelay,
                   au1IntState[pInterface->u1IsmState],
                   pInterface->u1RtrPriority);

        u4IfDesignatedRouter = OSPF_CRU_BMC_DWFROMPDU (pInterface->desgRtr);

        u4IfDRRouterId =
            IfFindRouterIdFromIfAddr (pInterface->desgRtr, pInterface);

        if ((u4IfDesignatedRouter != IP_ANY_ADDR) &&
            (u4IfDRRouterId != IP_ANY_ADDR))
        {
            OSPF_CLI_IPADDR_TO_STR (pu1String, u4IfDRRouterId);
            CliPrintf (CliHandle, "  Designated RouterId %s, ", pu1String);

            OSPF_CLI_IPADDR_TO_STR (pu1String, u4IfDesignatedRouter);
            CliPrintf (CliHandle, "Interface address %s\r\n", pu1String);
        }
        else
        {
            CliPrintf (CliHandle, "  No designated router on this network\r\n");
        }

        u4IfBackupDesignatedRouter =
            OSPF_CRU_BMC_DWFROMPDU (pInterface->backupDesgRtr);
        u4IfBDRRouterId = IfFindRouterIdFromIfAddr (pInterface->backupDesgRtr,
                                                    pInterface);
        if ((u4IfBackupDesignatedRouter != IP_ANY_ADDR) &&
            (u4IfBDRRouterId != IP_ANY_ADDR))
        {

            OSPF_CLI_IPADDR_TO_STR (pu1String, u4IfBDRRouterId);

            CliPrintf (CliHandle, "  Backup Designated RouterId %s, ",
                       pu1String);

            OSPF_CLI_IPADDR_TO_STR (pu1String, u4IfBackupDesignatedRouter);

            CliPrintf (CliHandle, "Interface address %s\r\n", pu1String);
        }
        else
        {
            CliPrintf (CliHandle,
                       "  No backup designated router on this network\r\n");
        }

        CliPrintf (CliHandle,
                   "  Timer intervals configured, Hello %d, Dead %d, Wait %d,"
                   "  Retransmit %d\r\n",
                   pInterface->u2HelloInterval, pInterface->i4RtrDeadInterval,
                   pInterface->i4RtrDeadInterval, pInterface->u2RxmtInterval);
        if (pInterface->u1NetworkType == IF_NBMA)
        {
            CliPrintf (CliHandle, "  Neighbor Poll Interval is %d\r\n",
                       pInterface->i4PollInterval);
        }

        if (pInterface->bPassive == OSPF_FALSE)
        {
            if (TmrGetRemainingTime (gTimerLst,
                                     &(pInterface->
                                       helloTimer.timerNode),
                                     &(u4HelloTime)) == TMR_SUCCESS)
            {
                u4HelloTime = u4HelloTime / NO_OF_TICKS_PER_SEC;
            }
            else
            {
                u4HelloTime = 0;
            }

            CliPrintf (CliHandle, "  Hello due in %d sec\r\n", u4HelloTime);
        }
        else
        {
            CliPrintf (CliHandle, "  No Hellos (Passive interface)\r\n");
        }

        CliPrintf (CliHandle,
                   "  Neighbor Count is %d, Adjacent neighbor count is %d\r\n",
                   TMO_SLL_Count (&(pInterface->nbrsInIf)),
                   pInterface->u4NbrFullCount);

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pLstNode);

            if (pNbr->u1NsmState == NBRS_FULL)
            {
                OSPF_CLI_IPADDR_TO_STR
                    (pu1String, OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrId));
                i4PageStatus =
                    CliPrintf (CliHandle,
                               "  Adjacent with the neighbor %s\r\n",
                               pu1String);
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }
            }
        }
        if (pInterface->u2AuthType == SIMPLE_PASSWORD)
        {
            CliPrintf (CliHandle,
                       "  Simple password authentication enabled\r\n");
        }
        else if (pInterface->u2AuthType == CRYPT_AUTHENTICATION)
        {
            pAuthkeyInfo = (tMd5AuthkeyInfo *) GetAuthkeyTouse (pInterface);
            if (pAuthkeyInfo != NULL)
            {
                pInterface->pLastAuthkey = pAuthkeyInfo;
            }

            switch (pInterface->u4CryptoAuthType)
            {

                case OSPF_AUTH_MD5:

                    CliPrintf (CliHandle, "md5 authentication enabled\r\n");
                    if (pInterface->pLastAuthkey != NULL)
                    {
                        CliPrintf (CliHandle,
                                   "md5 authentication key is configured\r\n");
                        CliPrintf (CliHandle, " Youngest key id is %d\r\n",
                                   pInterface->pLastAuthkey->u1AuthkeyId);
                    }
                    break;
                case OSPF_AUTH_SHA1:

                    CliPrintf (CliHandle, "sha-1  authentication enabled\r\n");
                    if (pInterface->pLastAuthkey != NULL)
                    {
                        CliPrintf (CliHandle,
                                   "sha-1 authentication key is configured\r\n");
                        CliPrintf (CliHandle, " Youngest key id is %d\r\n",
                                   pInterface->pLastAuthkey->u1AuthkeyId);
                    }
                    break;

                case OSPF_AUTH_SHA2_224:

                    CliPrintf (CliHandle, "sha-224 authentication enabled\r\n");
                    if (pInterface->pLastAuthkey != NULL)
                    {
                        CliPrintf (CliHandle,
                                   "sha-224 authentication key is configured\r\n");
                        CliPrintf (CliHandle, " Youngest key id is %d\r\n",
                                   pInterface->pLastAuthkey->u1AuthkeyId);
                    }
                    break;
                case OSPF_AUTH_SHA2_256:

                    CliPrintf (CliHandle, "sha-256 authentication enabled\r\n");
                    if (pInterface->pLastAuthkey != NULL)
                    {
                        CliPrintf (CliHandle,
                                   "sha-256 authentication key is configured\r\n");
                        CliPrintf (CliHandle, " Youngest key id is %d\r\n",
                                   pInterface->pLastAuthkey->u1AuthkeyId);
                    }
                    break;
                case OSPF_AUTH_SHA2_384:
                    CliPrintf (CliHandle, "sha-384 authentication enabled\r\n");
                    if (pInterface->pLastAuthkey != NULL)
                    {
                        CliPrintf (CliHandle,
                                   "sha-384 authentication key is configured\r\n");
                        CliPrintf (CliHandle, " Youngest key id is %d\r\n",
                                   pInterface->pLastAuthkey->u1AuthkeyId);
                    }
                    break;
                case OSPF_AUTH_SHA2_512:

                    CliPrintf (CliHandle, "sha-512 authentication enabled\r\n");
                    if (pInterface->pLastAuthkey != NULL)
                    {
                        CliPrintf (CliHandle,
                                   "sha-512 authentication key is configured\r\n");
                        CliPrintf (CliHandle, " Youngest key id is %d\r\n",
                                   pInterface->pLastAuthkey->u1AuthkeyId);
                    }
                    break;
                default:
                    CliPrintf (CliHandle,
                               " UnKnown Cryptographic Authentication \r\n");
                    break;
            }
            if (pInterface->pLastAuthkey != NULL)
            {
                OspfPrintKeyTime (pInterface->pLastAuthkey->u4KeyStartAccept,
                                  au1KeyConstantTime);
                CliPrintf (CliHandle, "      Key Start Accept Time  is %s\r\n",
                           au1KeyConstantTime);
                OspfPrintKeyTime (pInterface->pLastAuthkey->u4KeyStartGenerate,
                                  au1KeyConstantTime);
                CliPrintf (CliHandle,
                           "      Key Start Generate Time  is %s\r\n",
                           au1KeyConstantTime);
                if (pInterface->pLastAuthkey->u4KeyStopGenerate > 0)
                {
                    OspfPrintKeyTime (pInterface->pLastAuthkey->
                                      u4KeyStopGenerate, au1KeyConstantTime);
                    CliPrintf (CliHandle,
                               "      Key Stop Generate Time  is %s\r\n",
                               au1KeyConstantTime);
                }
                if (pInterface->pLastAuthkey->u4KeyStopAccept > 0)
                {
                    OspfPrintKeyTime (pInterface->pLastAuthkey->u4KeyStopAccept,
                                      au1KeyConstantTime);
                    CliPrintf (CliHandle,
                               "      Key Stop Accept Time  is %s\r\n",
                               au1KeyConstantTime);
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "      No key configured, using default key id 0\r\n");
            }

        }
        else
        {
            CliPrintf (CliHandle, "  No authentication  Configured\r\n");

        }

        if (STRLEN (pInterface->authKey) != 0)
        {
            CliPrintf (CliHandle,
                       "  Simple Authentication key is configured\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Simple Authentication  Key is not Configured\r\n");
        }
        if (pInterface->u2AuthType != CRYPT_AUTHENTICATION)
        {
            if (pInterface->sortMd5authkeyLst.u4_Count != 0)
            {
                CliPrintf (CliHandle,
                           "  Cryptographic authentication key is configured\r\n");

            }
            else
            {
                CliPrintf (CliHandle,
                           "  Cryptographic authentication key is not configured\n");
            }
        }

        if ((UtilOspfGetVcmAliasName (pOspfCxt->u4OspfCxtId, au1OspfCxtName)) ==
            OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, " Connected to VRF   %s \r\n", au1OspfCxtName);
    }
    if (pInterface->u1BfdIfStatus != OSPF_BFD_ENABLED)
    {
        CliPrintf (CliHandle, " Bfd Disabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Bfd Enable \r\n");
    }
    i4PageStatus = CliPrintf (CliHandle, "\r\n");
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowBorderRouterInCxt                           */
/*                                                                            */
/* Description       : This Routine will show the OSPF Border router          */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId  - Context Id                              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowBorderRouterInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tShowOspfBRCookie   OspfBRCookie;
    tShowOspfBR        *pShowOspfBR;
    UINT1               au1BRInfo[MAX_OSPF_BR_INFO];
    UINT1              *pu1BRInfo = NULL;
    UINT4               u4BufSize = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4HdrFlag = OSPF_TRUE;
    UINT4               u4ShowAllCxt = OSPF_FALSE;

    pu1BRInfo = au1BRInfo;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }
    /* Initialize the cookie */
    MEMSET (&OspfBRCookie, 0, sizeof (tShowOspfBRCookie));

    OspfBRCookie.u4OspfCxtId = u4OspfCxtId;
    /* Allocate memory to hold the Lsa information. */
    u4BufSize = ((sizeof (tShowOspfBR)) +
                 ((OSPF_CLI_MAX_BR - 1) * sizeof (tShowOspfBRInfo)));

    CliPrintf (CliHandle, "\r\n");
    while (i4RetVal == CLI_SUCCESS)
    {
        MEMSET (pu1BRInfo, 0, u4BufSize);
        i4RetVal = OspfCliShowBRInCxt (CliHandle, pu1BRInfo,
                                       u4BufSize, &OspfBRCookie, u4ShowAllCxt);

        pShowOspfBR = (tShowOspfBR *) (VOID *) pu1BRInfo;
        if (pShowOspfBR->u4NoOfBR)
        {
            if (OspfCliDisplayBR (CliHandle, pShowOspfBR, i4HdrFlag) ==
                CLI_FAILURE)
                break;
            if (i4HdrFlag == OSPF_TRUE)
                i4HdrFlag = OSPF_FALSE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDisplayBR                                       */
/*                                                                            */
/* Description       : This Routine will print the OSPF Border router         */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pOspfBR      - Border router info. to print            */
/*                     i4HdrFlag    - Flag to specify printing of header      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDisplayBR (tCliHandle CliHandle, tShowOspfBR * pOspfBR, INT4 i4HdrFlag)
{
    tShowOspfBRInfo    *pOspfBRInfo;
    UINT4               u4Index;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];

    pOspfBRInfo = &(pOspfBR->BRInfo[0]);
    u4OspfPrevCxtId = pOspfBRInfo->u4OspfCxtId;

    if (i4HdrFlag == OSPF_TRUE)
    {
        if ((UtilOspfGetVcmAliasName (pOspfBRInfo->u4OspfCxtId, au1OspfCxtName))
            == OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, " Vrf  %s \r\n", au1OspfCxtName);
        CliPrintf (CliHandle,
                   "\rOSPF Process Border Router Information\r\n\r\n");
        CliPrintf (CliHandle, "\r%-16s %-4s %-5s %-16s %-10s %-10s %-5s\r\n",
                   "Destination", "TOS", "Type", "NextHop", "Cost", "Rt.Type",
                   "Area");
        CliPrintf (CliHandle, "\r%-16s %-4s %-5s %-16s %-10s %-10s %-5s\r\n",
                   "-----------", "---", "----", "-------", "----", "-------",
                   "----");
    }

    for (u4Index = 0; u4Index < pOspfBR->u4NoOfBR; u4Index++, pOspfBRInfo++)
    {
        if (u4OspfPrevCxtId != pOspfBRInfo->u4OspfCxtId)
        {
            if ((UtilOspfGetVcmAliasName
                 (pOspfBRInfo->u4OspfCxtId, au1OspfCxtName)) == OSPF_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, " Vrf  %s \r\n", au1OspfCxtName);
            CliPrintf (CliHandle,
                       "\rOSPF Process Border Router Information\r\n\r\n");
            CliPrintf (CliHandle,
                       "\r%-16s %-4s %-5s %-16s %-10s %-10s %-5s\r\n",
                       "Destination", "TOS", "Type", "NextHop", "Cost",
                       "Rt.Type", "Area");
            CliPrintf (CliHandle,
                       "\r%-16s %-4s %-5s %-16s %-10s %-10s %-5s\r\n",
                       "-----------", "---", "----", "-------", "----",
                       "-------", "----");

        }

        u4OspfPrevCxtId = pOspfBRInfo->u4OspfCxtId;

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfBRInfo->u4OspfBRIpAddr);
        CliPrintf (CliHandle, "%-16s", pu1String);

        CliPrintf (CliHandle, " %-4d", pOspfBRInfo->i4OspfBRTos);

        switch (pOspfBRInfo->i4OspfBRRtrType)
        {
            case DEST_AREA_BORDER:
                CliPrintf (CliHandle, " %-5s", "ABR");
                break;
            case DEST_AS_BOUNDARY:
                CliPrintf (CliHandle, " %-5s", "ASBR");
                break;
            default:
                CliPrintf (CliHandle, " %-5s", "     ");
                break;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfBRInfo->u4OspfBRNextHop);
        CliPrintf (CliHandle, " %-16s", pu1String);

        CliPrintf (CliHandle, " %-10d", pOspfBRInfo->i4OspfBRCost);

        switch (pOspfBRInfo->i4OspfBRRtType)
        {
            case INTRA_AREA:
                CliPrintf (CliHandle, " %-10s", "intraArea");
                break;
            case INTER_AREA:
                CliPrintf (CliHandle, " %-10s", "interArea");
                break;
            case TYPE_1_EXT:
                CliPrintf (CliHandle, " %-10s", "type1Ext");
                break;
            case TYPE_2_EXT:
                CliPrintf (CliHandle, " %-10s", "type2Ext");
                break;
            default:
                CliPrintf (CliHandle, " %-10s", "          ");
                break;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfBRInfo->u4OspfBRArea);
        i4PageStatus = CliPrintf (CliHandle, " %-15s\r\n", pu1String);
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowBRInCxt                                     */
/*                                                                            */
/* Description       : This Routine will fetch the OSPF Border router         */
/*                     information from OSPF database                         */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pu1Buffer    - Buffer to dump the information          */
/*                     u4BufLen     - buffer Length                           */
/*                     pCookie      - Cookie for next index                   */
/*                     u4ShowAllCxt - Flag for AllCxt                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowBRInCxt (tCliHandle CliHandle, UINT1 *pu1Buffer,
                    UINT4 u4BufLen, tShowOspfBRCookie * pCookie,
                    UINT4 u4ShowAllCxt)
{
    tShowOspfBR        *pShowOspfBR;
    tShowOspfBRInfo    *pShowOspfBRInfo;
    UINT4               u4FutOspfRouteIpAddr = 0;
    UINT4               u4FutOspfRouteIpAddrMask = 0;
    UINT4               u4FutOspfRouteIpNextHop = 0;
    UINT4               u4RetValFutOspfRouteAreaId = 0;
    UINT4               u4PrevFutOspfRouteIpAddr = 0;
    UINT4               u4PrevFutOspfRouteIpAddrMask = 0;
    UINT4               u4PrevFutOspfRouteIpNextHop = 0;
    INT4                i4FutOspfRouteIpTos = 0;
    INT4                i4FutOspfRouteDestType = 0;
    INT4                i4RetValFutOspfRouteCost = 0;
    INT4                i4PrevFutOspfRouteIpTos = 0;
    INT4                i4PrevFutOspfRouteDestType = 0;
    INT4                i4RetValFutOspfRouteType = 0;
    UINT4               u4NoOfBR = 0;
    UINT4               u4BRExist = OSPF_FALSE;
    INT4                i4AdminStat;
    UINT4               u4OspfCxtId = 0;
    UINT4               u4OspfPrevCxtId = 0;
    INT4                i4RetVal;

    u4OspfCxtId = pCookie->u4OspfCxtId;
    if (pCookie->u4OspfBRIpAddr)
    {
        u4FutOspfRouteIpAddr = pCookie->u4OspfBRIpAddr;
        u4FutOspfRouteIpAddrMask = pCookie->u4OspfBRIpMask;
        u4FutOspfRouteIpNextHop = pCookie->u4OspfBRNextHop;
        i4FutOspfRouteIpTos = pCookie->i4OspfBRTos;
        i4FutOspfRouteDestType = pCookie->i4OspfBRRtType;
        u4BRExist = OSPF_TRUE;
    }
    else
    {
        if (u4ShowAllCxt == OSPF_TRUE)
        {
            i4RetVal =
                nmhGetFirstIndexFsMIOspfBRRouteTable ((INT4 *) &u4OspfCxtId,
                                                      &u4FutOspfRouteIpAddr,
                                                      &u4FutOspfRouteIpAddrMask,
                                                      (UINT4 *)
                                                      &i4FutOspfRouteIpTos,
                                                      &u4FutOspfRouteIpNextHop,
                                                      &i4FutOspfRouteDestType);
        }
        else
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevFutOspfRouteIpAddr = 0;
            u4PrevFutOspfRouteIpAddrMask = 0;
            i4PrevFutOspfRouteIpTos = 0;
            u4PrevFutOspfRouteIpNextHop = 0;
            i4PrevFutOspfRouteDestType = 0;
            i4RetVal =
                nmhGetNextIndexFsMIOspfBRRouteTable ((INT4) u4OspfPrevCxtId,
                                                     (INT4 *) &u4OspfCxtId,
                                                     u4PrevFutOspfRouteIpAddr,
                                                     &u4FutOspfRouteIpAddr,
                                                     u4PrevFutOspfRouteIpAddrMask,
                                                     &u4FutOspfRouteIpAddrMask,
                                                     i4PrevFutOspfRouteIpTos,
                                                     (UINT4 *)
                                                     &i4FutOspfRouteIpTos,
                                                     u4PrevFutOspfRouteIpNextHop,
                                                     &u4FutOspfRouteIpNextHop,
                                                     i4PrevFutOspfRouteDestType,
                                                     &i4FutOspfRouteDestType);
            if ((u4OspfPrevCxtId != u4OspfCxtId)
                && (u4ShowAllCxt == OSPF_FALSE))
            {
                /* GetNext routine returned next context info and
                 * u4ShowAllCxt is set to false */
                i4RetVal = SNMP_FAILURE;
            }

        }
        if (i4RetVal == SNMP_SUCCESS)
        {
            u4BRExist = OSPF_TRUE;
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);
    pShowOspfBR = (tShowOspfBR *) (VOID *) pu1Buffer;
    pShowOspfBR->u4NoOfBR = 0;
    pShowOspfBRInfo = &(pShowOspfBR->BRInfo[0]);

    if (u4BRExist == OSPF_TRUE)
    {
        u4NoOfBR = (u4BufLen - ((sizeof (tShowOspfBR)) -
                                (sizeof (tShowOspfBRInfo)))) /
            sizeof (tShowOspfBRInfo);

        do
        {
            if (u4ShowAllCxt == OSPF_FALSE && u4OspfPrevCxtId != u4OspfCxtId)
            {
                break;
            }
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevFutOspfRouteIpAddr = u4FutOspfRouteIpAddr;
            u4PrevFutOspfRouteIpAddrMask = u4FutOspfRouteIpAddrMask;
            i4PrevFutOspfRouteIpTos = i4FutOspfRouteIpTos;
            u4PrevFutOspfRouteIpNextHop = u4FutOspfRouteIpNextHop;
            i4PrevFutOspfRouteDestType = i4FutOspfRouteDestType;

            if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
                 == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
            {
                if (u4ShowAllCxt == OSPF_FALSE)
                {
                    break;
                }
                continue;
            }

            if (!u4NoOfBR)
            {
                /* we have run out of buffer; but we have more BRS in the
                 *  database to be returned. Hence set the cookie
                 *  in the return buffer */
                pCookie->u4OspfCxtId = u4OspfCxtId;
                pCookie->u4OspfBRIpAddr = u4FutOspfRouteIpAddr;
                pCookie->u4OspfBRIpMask = u4FutOspfRouteIpAddrMask;
                pCookie->u4OspfBRNextHop = u4FutOspfRouteIpNextHop;
                pCookie->i4OspfBRTos = i4FutOspfRouteIpTos;
                pCookie->i4OspfBRRtType = i4FutOspfRouteDestType;
                return CLI_SUCCESS;
            }

            if (nmhGetFsMIOspfBRRouteCost (u4OspfCxtId,
                                           u4FutOspfRouteIpAddr,
                                           u4FutOspfRouteIpAddrMask,
                                           i4FutOspfRouteIpTos,
                                           u4FutOspfRouteIpNextHop,
                                           i4FutOspfRouteDestType,
                                           &i4RetValFutOspfRouteCost) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetFsMIOspfBRRouteType (u4OspfCxtId,
                                           u4FutOspfRouteIpAddr,
                                           u4FutOspfRouteIpAddrMask,
                                           i4FutOspfRouteIpTos,
                                           u4FutOspfRouteIpNextHop,
                                           i4FutOspfRouteDestType,
                                           &i4RetValFutOspfRouteType) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetFsMIOspfBRRouteAreaId (u4OspfCxtId,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             i4FutOspfRouteDestType,
                                             &u4RetValFutOspfRouteAreaId) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            pShowOspfBRInfo->u4OspfCxtId = u4OspfCxtId;
            pShowOspfBRInfo->u4OspfBRIpAddr = u4FutOspfRouteIpAddr;
            pShowOspfBRInfo->u4OspfBRNextHop = u4FutOspfRouteIpNextHop;
            pShowOspfBRInfo->u4OspfBRArea = u4RetValFutOspfRouteAreaId;
            pShowOspfBRInfo->i4OspfBRRtrType = i4FutOspfRouteDestType;
            pShowOspfBRInfo->i4OspfBRRtType = i4RetValFutOspfRouteType;
            pShowOspfBRInfo->i4OspfBRCost = i4RetValFutOspfRouteCost;
            pShowOspfBRInfo->i4OspfBRTos = i4FutOspfRouteIpTos;

            pShowOspfBR->u4NoOfBR++;
            u4NoOfBR--;

            if (u4NoOfBR)
            {
                /* Increment the pShowOspfBRInfo pointer to store the next
                 *  BR information. */
                pShowOspfBRInfo++;
            }

        }
        while (nmhGetNextIndexFsMIOspfBRRouteTable ((INT4) u4OspfPrevCxtId,
                                                    (INT4 *) &u4OspfCxtId,
                                                    u4PrevFutOspfRouteIpAddr,
                                                    &u4FutOspfRouteIpAddr,
                                                    u4PrevFutOspfRouteIpAddrMask,
                                                    &u4FutOspfRouteIpAddrMask,
                                                    i4PrevFutOspfRouteIpTos,
                                                    (UINT4 *)
                                                    &i4FutOspfRouteIpTos,
                                                    u4PrevFutOspfRouteIpNextHop,
                                                    &u4FutOspfRouteIpNextHop,
                                                    i4PrevFutOspfRouteDestType,
                                                    &i4FutOspfRouteDestType) !=
               SNMP_FAILURE);

    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliShowRoutingTableInCxt                           */
/*                                                                            */
/* Description       : This Routine will show the OSPF Routing                */
/*                     table information                                      */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId  - ospf context id                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowRoutingTableInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tShowOspfRTCookie   OspfRTCookie;
    tShowOspfRT        *pShowOspfRT;
    UINT1               au1RTInfo[MAX_OSPF_RT_INFO];
    UINT1              *pu1RTInfo = NULL;
    UINT4               u4BufSize = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4HdrFlag = OSPF_TRUE;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    UINT4               PrevCxtId = 0;
    UINT4               FHdrFlag = OSPF_TRUE;

    pu1RTInfo = au1RTInfo;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }

    /* Initialize the cookie */
    MEMSET (&OspfRTCookie, 0, sizeof (tShowOspfRTCookie));

    OspfRTCookie.u4OspfCxtId = u4OspfCxtId;

    /* Allocate memory to hold the Lsa information. */
    u4BufSize = ((sizeof (tShowOspfRT)) +
                 ((OSPF_CLI_MAX_RT - 1) * sizeof (tShowOspfRTInfo)));

    while (i4RetVal == CLI_SUCCESS)
    {
        MEMSET (pu1RTInfo, 0, u4BufSize);

        i4RetVal = OspfCliShowRTInCxt (CliHandle, pu1RTInfo,
                                       u4BufSize, &OspfRTCookie, u4ShowAllCxt);
        pShowOspfRT = (tShowOspfRT *) (VOID *) pu1RTInfo;

        if (pShowOspfRT->u4NoOfRT)
        {
            if ((PrevCxtId == (pShowOspfRT->RTInfo[0].u4OspfCxtId) - 1)
                || (FHdrFlag == OSPF_TRUE))
            {
                FHdrFlag = OSPF_FALSE;
                i4HdrFlag = OSPF_TRUE;
            }
            else
            {
                FHdrFlag = OSPF_FALSE;
                i4HdrFlag = OSPF_FALSE;
            }
            if (OspfCliDisplayRT (CliHandle, pShowOspfRT, i4HdrFlag, &PrevCxtId)
                == CLI_FAILURE)
                break;

        }
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDisplayRT                                       */
/*                                                                            */
/* Description       : This Routine will print the OSPF Routing               */
/*                     table information                                      */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pOspfRT      - OSPF Route information                  */
/*                     i4HdrFlag    - Flag to specify printing of Header      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDisplayRT (tCliHandle CliHandle, tShowOspfRT * pOspfRT, INT4 i4HdrFlag,
                  UINT4 *PrevCxtId)
{
    tShowOspfRTInfo    *pOspfRTInfo;
    UINT4               u4Index;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;
    CHR1               *pu1String2 = NULL;
    UINT1               au1IfName[OSPF_IFNAME_LEN];
    UINT1               au1Temp[OSPF_MAX_IP_STR_LEN * 2 + OSPF_IFNAME_LEN + 1];
    UINT4               u4OspfPrevCxtId;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];

    pOspfRTInfo = &(pOspfRT->RTInfo[0]);

    pu1String2 = (CHR1 *) & au1Temp[0];
    u4OspfPrevCxtId = pOspfRTInfo->u4OspfCxtId;
    if (i4HdrFlag == OSPF_TRUE)
    {
        if ((UtilOspfGetVcmAliasName (pOspfRTInfo->u4OspfCxtId, au1OspfCxtName))
            == OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nOSPF Routing Table   Vrf  %s \r\n",
                   au1OspfCxtName);
        CliPrintf (CliHandle, "%-4s/%-28s%-4s%-7s/%-20s%-5s%-10s%-15s\r\n",
                   "Dest", "Mask", "TOS", "NextHop", "Interface", "Cost",
                   "Rt.Type", "Area");
        CliPrintf (CliHandle, "%-33s%-4s%-7s/%-20s%-5s%-10s%-15s\r\n",
                   "---------", "---", "-------", "---------", "----",
                   "-------", "----");
    }

    for (u4Index = 0; u4Index < pOspfRT->u4NoOfRT; u4Index++, pOspfRTInfo++)
    {
        if (pOspfRTInfo->u4OspfCxtId != u4OspfPrevCxtId)
        {
            if ((UtilOspfGetVcmAliasName
                 (pOspfRTInfo->u4OspfCxtId, au1OspfCxtName)) == OSPF_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\r\nOSPF Routing Table   Vrf  %s \r\n",
                       au1OspfCxtName);
            CliPrintf (CliHandle, "%-4s/%-28s%-4s%-7s/%-20s%-5s%-10s%-15s\r\n",
                       "Dest", "Mask", "TOS", "NextHop", "Interface", "Cost",
                       "Rt.Type", "Area");
            CliPrintf (CliHandle, "%-33s%-4s%-7s/%-20s%-5s%-10s%-15s\r\n",
                       "---------", "---", "-------", "---------", "----",
                       "-------", "----");
        }
        u4OspfPrevCxtId = pOspfRTInfo->u4OspfCxtId;

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfRTInfo->u4OspfRTIpAddr);

        MEMSET (au1Temp, 0, OSPF_MAX_IP_STR_LEN * 2 + 1);
        STRCAT (pu1String2, pu1String);
        STRCAT (pu1String2, "/");

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfRTInfo->u4OspfRTIpMask);
        STRCAT (pu1String2, pu1String);
        CliPrintf (CliHandle, "%-33s", pu1String2);

        CliPrintf (CliHandle, "%-4d", pOspfRTInfo->i4OspfRTTos);

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfRTInfo->u4OspfRTNextHop);

        MEMSET (au1Temp, 0, OSPF_MAX_IP_STR_LEN * 2 + 1);
        STRCAT (pu1String2, pu1String);
        STRCAT (pu1String2, "/");

        if (OspfGetInterfaceNameFromIndex
            (pOspfRTInfo->u4OspfRTIfIndex, au1IfName) == OSIX_FAILURE)
        {
            STRCPY (au1IfName, " - ");
        }
        STRCAT (pu1String2, au1IfName);
        CliPrintf (CliHandle, "%-28s", pu1String2);

        CliPrintf (CliHandle, "%-5d", pOspfRTInfo->i4OspfRTCost);

        switch (pOspfRTInfo->i4OspfRTRtType)
        {
            case INTRA_AREA:
                CliPrintf (CliHandle, "%-10s", "IntraArea");
                break;
            case INTER_AREA:
                CliPrintf (CliHandle, "%-10s", "InterArea");
                break;
            case TYPE_1_EXT:
                CliPrintf (CliHandle, "%-10s", "Type1Ext");
                break;
            case TYPE_2_EXT:
                CliPrintf (CliHandle, "%-10s", "Type2Ext");
                break;
            default:
                CliPrintf (CliHandle, "%-10s", "          ");
                break;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfRTInfo->u4OspfRTArea);
        i4PageStatus = CliPrintf (CliHandle, "%-15s\r\n", pu1String);

        if (i4PageStatus == CLI_FAILURE)
            break;
    }
    MEMCPY (PrevCxtId, &u4OspfPrevCxtId, sizeof (UINT4));
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowRTInCxt                                     */
/*                                                                            */
/* Description       : This Routine will fetch the OSPF Routing               */
/*                     table information                                      */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pu1Buffer    - Buffer to dump OSPF Route information   */
/*                     u4BufLen     - Length of the buffer                    */
/*                     pCookie      - Cookie for next index                   */
/*                     u4ShowAllCxt - flag to print the context info          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowRTInCxt (tCliHandle CliHandle, UINT1 *pu1Buffer,
                    UINT4 u4BufLen, tShowOspfRTCookie * pCookie,
                    UINT4 u4ShowAllCxt)
{
    tShowOspfRT        *pShowOspfRT;
    tShowOspfRTInfo    *pShowOspfRTInfo;
    UINT4               u4FutOspfRouteIpAddr = 0;
    UINT4               u4FutOspfRouteIpAddrMask = 0;
    UINT4               u4FutOspfRouteIpNextHop = 0;
    UINT4               u4RetValFutOspfRouteAreaId = 0;
    UINT4               u4PrevFutOspfRouteIpAddr = 0;
    UINT4               u4PrevFutOspfRouteIpAddrMask = 0;
    UINT4               u4PrevFutOspfRouteIpNextHop = 0;
    UINT4               u4FutOspfRtIfIndex = 0;
    INT4                i4FutOspfRouteIpTos = 0;
    INT4                i4RetValFutOspfRouteCost = 0;
    INT4                i4PrevFutOspfRouteIpTos = 0;
    INT4                i4RetValFutOspfRouteType = 0;
    INT4                i4RetValFutOspfRtPort = 0;
    UINT4               u4NoOfRT = 0;
    UINT4               u4RTExist = OSPF_FALSE;
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4OspfCxtId;
    INT4                i4RetVal;
    INT4                i4AdminStat;

    u4OspfCxtId = pCookie->u4OspfCxtId;
    if (pCookie->u4OspfRTIpAddr)
    {
        u4OspfPrevCxtId = pCookie->u4OspfCxtId;
        u4FutOspfRouteIpAddr = pCookie->u4OspfRTIpAddr;
        u4FutOspfRouteIpAddrMask = pCookie->u4OspfRTIpMask;
        u4FutOspfRouteIpNextHop = pCookie->u4OspfRTNextHop;
        i4FutOspfRouteIpTos = pCookie->i4OspfRTTos;
        u4RTExist = OSPF_TRUE;
    }
    else
    {
        if (u4ShowAllCxt == OSPF_TRUE)
        {
            i4RetVal =
                nmhGetFirstIndexFsMIOspfRoutingTable ((INT4 *) &u4OspfCxtId,
                                                      &u4FutOspfRouteIpAddr,
                                                      &u4FutOspfRouteIpAddrMask,
                                                      &i4FutOspfRouteIpTos,
                                                      &u4FutOspfRouteIpNextHop);
        }
        else
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevFutOspfRouteIpAddr = 0;
            u4PrevFutOspfRouteIpAddrMask = 0;
            u4PrevFutOspfRouteIpNextHop = 0;
            i4PrevFutOspfRouteIpTos = 0;

            i4RetVal =
                nmhGetNextIndexFsMIOspfRoutingTable
                ((INT4) u4OspfPrevCxtId,
                 (INT4 *) &u4OspfCxtId,
                 u4PrevFutOspfRouteIpAddr,
                 &u4FutOspfRouteIpAddr,
                 u4PrevFutOspfRouteIpAddrMask,
                 &u4FutOspfRouteIpAddrMask,
                 i4PrevFutOspfRouteIpTos,
                 &i4FutOspfRouteIpTos,
                 u4PrevFutOspfRouteIpNextHop, &u4FutOspfRouteIpNextHop);
            if ((u4OspfPrevCxtId != u4OspfCxtId) &&
                (u4ShowAllCxt == OSPF_FALSE))
            {
                /* GetNext routine returned next context info and
                 * u4ShowAllCxt is set to false */
                i4RetVal = SNMP_FAILURE;
            }
        }

        if (i4RetVal == SNMP_SUCCESS)
        {
            u4RTExist = OSPF_TRUE;
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);
    pShowOspfRT = (tShowOspfRT *) (VOID *) pu1Buffer;
    pShowOspfRT->u4NoOfRT = 0;
    pShowOspfRTInfo = &(pShowOspfRT->RTInfo[0]);

    if (u4RTExist == OSPF_TRUE)
    {
        u4NoOfRT = (u4BufLen - ((sizeof (tShowOspfRT)) -
                                (sizeof (tShowOspfRTInfo)))) /
            sizeof (tShowOspfRTInfo);

        do
        {

            if (u4ShowAllCxt == OSPF_FALSE && u4OspfPrevCxtId != u4OspfCxtId)
            {
                /* Routing table of the specified context id is displayed */
                break;
            }

            /* prepare to get the next indices */

            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevFutOspfRouteIpAddr = u4FutOspfRouteIpAddr;
            u4PrevFutOspfRouteIpAddrMask = u4FutOspfRouteIpAddrMask;
            u4PrevFutOspfRouteIpNextHop = u4FutOspfRouteIpNextHop;
            i4PrevFutOspfRouteIpTos = i4FutOspfRouteIpTos;

            if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
                 == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
            {
                if (u4ShowAllCxt == OSPF_FALSE)
                {
                    /* Invalid context id or admin status of the 
                     * context is disabled */
                    break;
                }

                continue;
            }

            if (!u4NoOfRT)
            {
                /* we have run out of buffer; but we have more Routes in the
                 *  database to be returned. Hence set the cookie
                 *  in the return buffer */
                pCookie->u4OspfCxtId = u4OspfCxtId;
                pCookie->u4OspfRTIpAddr = u4FutOspfRouteIpAddr;
                pCookie->u4OspfRTIpMask = u4FutOspfRouteIpAddrMask;
                pCookie->u4OspfRTNextHop = u4FutOspfRouteIpNextHop;
                pCookie->i4OspfRTTos = i4FutOspfRouteIpTos;
                return CLI_SUCCESS;
            }

            if (nmhGetFsMIOspfRouteType (u4OspfCxtId,
                                         u4FutOspfRouteIpAddr,
                                         u4FutOspfRouteIpAddrMask,
                                         i4FutOspfRouteIpTos,
                                         u4FutOspfRouteIpNextHop,
                                         &i4RetValFutOspfRouteType) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (i4RetValFutOspfRouteType != TYPE_2_EXT)
            {
                if (nmhGetFsMIOspfRouteCost (u4OspfCxtId,
                                             u4FutOspfRouteIpAddr,
                                             u4FutOspfRouteIpAddrMask,
                                             i4FutOspfRouteIpTos,
                                             u4FutOspfRouteIpNextHop,
                                             &i4RetValFutOspfRouteCost) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (nmhGetFsMIOspfRouteType2Cost (u4OspfCxtId,
                                                  u4FutOspfRouteIpAddr,
                                                  u4FutOspfRouteIpAddrMask,
                                                  i4FutOspfRouteIpTos,
                                                  u4FutOspfRouteIpNextHop,
                                                  &i4RetValFutOspfRouteCost) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }

            if (nmhGetFsMIOspfRouteAreaId (u4OspfCxtId,
                                           u4FutOspfRouteIpAddr,
                                           u4FutOspfRouteIpAddrMask,
                                           i4FutOspfRouteIpTos,
                                           u4FutOspfRouteIpNextHop,
                                           &u4RetValFutOspfRouteAreaId) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetFsMIOspfRouteInterfaceIndex (u4OspfCxtId,
                                                   u4FutOspfRouteIpAddr,
                                                   u4FutOspfRouteIpAddrMask,
                                                   i4FutOspfRouteIpTos,
                                                   u4FutOspfRouteIpNextHop,
                                                   &i4RetValFutOspfRtPort)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            /* Right now Future IP gives IfIndex for the given Route parameters.
             * So While displaying we use the interface Index itself.
             */
            u4FutOspfRtIfIndex = (UINT4) i4RetValFutOspfRtPort;

            pShowOspfRTInfo->u4OspfCxtId = u4OspfCxtId;
            pShowOspfRTInfo->u4OspfRTIpAddr = u4FutOspfRouteIpAddr;
            pShowOspfRTInfo->u4OspfRTIpMask = u4FutOspfRouteIpAddrMask;
            pShowOspfRTInfo->u4OspfRTNextHop = u4FutOspfRouteIpNextHop;
            pShowOspfRTInfo->u4OspfRTArea = u4RetValFutOspfRouteAreaId;
            pShowOspfRTInfo->u4OspfRTIfIndex = u4FutOspfRtIfIndex;
            pShowOspfRTInfo->i4OspfRTRtType = i4RetValFutOspfRouteType;
            pShowOspfRTInfo->i4OspfRTCost = i4RetValFutOspfRouteCost;
            pShowOspfRTInfo->i4OspfRTTos = i4FutOspfRouteIpTos;

            pShowOspfRT->u4NoOfRT++;
            u4NoOfRT--;

            if (u4NoOfRT)
            {
                /* Increment the pShowOspfRTInfo pointer to store the next
                 *  route information. */
                pShowOspfRTInfo++;
            }

        }
        while (nmhGetNextIndexFsMIOspfRoutingTable ((INT4) u4OspfPrevCxtId,
                                                    (INT4 *) &u4OspfCxtId,
                                                    u4PrevFutOspfRouteIpAddr,
                                                    &u4FutOspfRouteIpAddr,
                                                    u4PrevFutOspfRouteIpAddrMask,
                                                    &u4FutOspfRouteIpAddrMask,
                                                    i4PrevFutOspfRouteIpTos,
                                                    &i4FutOspfRouteIpTos,
                                                    u4PrevFutOspfRouteIpNextHop,
                                                    &u4FutOspfRouteIpNextHop)
               != SNMP_FAILURE);
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliShowOspfInfoInCxt                               */
/*                                                                            */
/* Description       : This Routine will show the OSPF general information    */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId  - ospf context id                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowOspfInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    INT4                i4AreasCount = 0;
    INT4                i4RetValOspfAreaNSSATranslatorRole;
    INT4                i4RetValOspfAreaNSSATranslatorState = OSPF_ZERO;
    INT4                i4RetValOspfAreaNSSATranslatorStabilityInterval;
    INT4                i4RetValOspfDefaultCost = 0;
    INT4                i4OspfStubTOS = 0;
    tArea              *pArea;
    INT4                i4SpfCount;
    INT4                i4IfCount = 0;
    INT4                i4AdminStat;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4RestartStatus = OSPF_GR_NONE;
    INT4                i4GracePeriod = 0;
    UINT4               u4RestartAge = 0;
    INT4                i4ExitReason = 0;
    INT4                i4StrictLsaCheck = OSPF_FALSE;
    tSNMP_OCTET_STRING_TYPE HelperSupport;
    UINT1               u1HelperSupport;
    INT4                i4RestartAckState = 0;
    INT4                i4GrRetransCount = 0;
    INT4                i4RestartReason = 0;
    INT4                i4Distance = 0;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tSNMP_OCTET_STRING_TYPE NextRouteMapName;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];

    INT4                i4FilterType = 0;
    INT4                i4NextFilterType = 0;
    INT4                i4Value = 0;
    INT1                i1OutCome = 0;

    INT4                i4DefaultPassiveInterface = 0;
    INT4                i4RFC1583Compatibility = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4RetValFutOspfRTStaggeringInterval;
    UINT4               u4RouteDest = 0;
    UINT4               u4RouteMask = 0;
    INT4                i4RouteTos = 0;
    INT4                i4TableObject;
    INT4                i4ScalarObject;
    tSNMP_OCTET_STRING_TYPE RedistRouteMapName;
    INT4                i4ProtoMask;
    CHR1                pc1Redist[32] = { 0 };
    UINT1               au1RedistRouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4ActInterface = 0;
    INT4                i4DfInfOriginate;
    INT4                i4OspfAreaSummary;
    INT4                i4MetricTOS = 0;
    INT4                i4MetricType = 0;
    INT4                i4RetValStubMetric;
    INT4                i4RetValStubMetricType;
    UINT4               u4AreaId;
    tInterface         *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4InterfaceActive = OSPF_FALSE;
    UINT4               u4NotInServiceIntfCnt = 0;
    INT4                i4FutInt;
    UINT4               u4FsMIOspfHostIpAddress = 0;
    INT4                i4FsMIOspfHostTOS = 0;
    INT4                i4RetValOspfHostMetric = 0;
    UINT4               u4RetValOspfHostAreaID = 0;
    INT4                i4NextFsMIOspfHostContextId = 0;
    UINT4               u4NextFsMIOspfHostIpAddress = 0;
    INT4                i4NextFsMIOspfHostTOS = 0;

    MEMSET (pc1Redist, 0, 32);
    RedistRouteMapName.pu1_OctetList = au1RedistRouteMapName;
    RedistRouteMapName.i4_Length = 0;

    HelperSupport.pu1_OctetList = &u1HelperSupport;
    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u4ShowAllCxt = OSPF_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;
        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                /* Invalid context id or admin status of the 
                 * context is disabled */
                break;
            }

            continue;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String,
                                OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId));
        if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
            OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n OSPF Router with ID (%s) (Vrf  %s) \r\n",
                   pu1String, au1OspfCxtName);

        if (pOspfCxt->bTosCapability == OSPF_TRUE)
        {
            CliPrintf (CliHandle, "  Supports multiple TOS routes\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  Supports only single TOS(TOS0) route\r\n");
        }

        if (pOspfCxt->u1OpqCapableRtr == OSPF_ENABLED)
        {
            CliPrintf (CliHandle, "  Opaque LSA Support : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  Opaque LSA Support : Disabled\r\n");
        }
        CliPrintf (CliHandle,
                   "  SPF schedule delay %d millisec, Hold time between two SPF %d millisec\r\n",
                   pOspfCxt->u4SpfInterval, pOspfCxt->u4SpfHoldTimeInterval);

        CliPrintf (CliHandle, "  ABR Type supported is %s\r\n",
                   au1DbgAbrType[pOspfCxt->u4ABRType]);

        if (pOspfCxt->bAreaBdrRtr == OSPF_TRUE)
        {
            CliPrintf (CliHandle, "  It is an Area Border Router\r\n");
        }

        if (pOspfCxt->bAsBdrRtr == OSPF_TRUE)
        {
            CliPrintf (CliHandle,
                       "  Autonomous System Boundary Router : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "  Autonomous System Boundary Router : Disabled\r\n");
        }
        if (pOspfCxt->bNssaAsbrDefRtTrans == OSPF_TRUE)
        {
            CliPrintf (CliHandle,
                       "  P-Bit setting for the default Type-7 LSA that needs to be generated by the ASBR(which is not ABR) is enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "  P-Bit setting for the default Type-7 LSA that needs to be generated by the ASBR(which is not ABR) is disabled\r\n");
        }

        if (nmhGetFsMIOspfRestartSupport (u4OspfCxtId,
                                          &i4RestartStatus) == SNMP_SUCCESS)
        {
            if (i4RestartStatus == OSPF_RESTART_NONE)
            {
                CliPrintf (CliHandle, "  Non-Stop Forwarding disabled\r\n");
            }
            else if (i4RestartStatus == OSPF_RESTART_PLANNED)
            {
                CliPrintf (CliHandle,
                           "  Planned Non-Stop Forwarding enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Planned & Unplanned Non-Stop Forwarding enabled\r\n");
            }
            CliPrintf (CliHandle, "  ");
        }

        if (nmhGetFsMIOspfRestartInterval (u4OspfCxtId,
                                           &i4GracePeriod) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Restart-interval limit: %d\r\n",
                       i4GracePeriod);
            CliPrintf (CliHandle, "  ");
        }

        if (nmhGetFsMIOspfGraceLsaRetransmitCount
            (u4OspfCxtId, &i4GrRetransCount) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Grace LSA Retransmission Count: %d\r\n",
                       i4GrRetransCount);
            CliPrintf (CliHandle, "  ");
        }

        if ((i4RestartStatus != OSPF_RESTART_NONE) &&
            (nmhGetFsMIOspfRestartAge (u4OspfCxtId,
                                       &u4RestartAge) == SNMP_SUCCESS) &&
            (u4RestartAge != 0))
        {
            CliPrintf (CliHandle, "Remaining restart-interval: %d\r\n",
                       u4RestartAge);
            CliPrintf (CliHandle, "  ");
        }
        if (nmhGetFsMIOspfRestartAckState (u4OspfCxtId,
                                           &i4RestartAckState) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Helper Grace LSA ACK :");
            if (i4RestartAckState == OSPF_TRUE)
            {
                CliPrintf (CliHandle, "Required");
            }
            else
            {
                CliPrintf (CliHandle, "Not Required");
            }
            CliPrintf (CliHandle, "\r\n");
        }

        if (nmhGetFsMIOspfRestartReason (u4OspfCxtId,
                                         &i4RestartReason) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "  Restart Reason is:\r\n");
            switch (i4RestartReason)
            {
                case OSPF_GR_UNKNOWN:
                    CliPrintf (CliHandle, "         Unknown\r\n");
                    break;
                case OSPF_GR_SW_RESTART:
                    CliPrintf (CliHandle, "        Software Restart \r\n");
                    break;
                case OSPF_GR_SW_UPGRADE:
                    CliPrintf (CliHandle,
                               "        Software Upgrade/Reload\r\n");
                    break;
                case OSPF_GR_SW_RED:
                    CliPrintf (CliHandle, "        SwitchToRedundant \r\n");
                    break;
                default:
                    break;
            }
        }
        if (nmhGetFsMIOspfRestartExitReason (u4OspfCxtId,
                                             &i4ExitReason) == SNMP_SUCCESS)
        {
            if ((i4ExitReason == OSPF_RESTART_COMPLETED) ||
                (i4ExitReason == OSPF_RESTART_TIMEDOUT) ||
                (i4ExitReason == OSPF_RESTART_TOP_CHG))
            {
                CliPrintf (CliHandle, " Last NSF restart");
            }

            switch (i4ExitReason)
            {
                case OSPF_RESTART_COMPLETED:
                {
                    CliPrintf (CliHandle, "        Completed successfully\r\n");
                    break;
                }
                case OSPF_RESTART_TIMEDOUT:
                {
                    CliPrintf (CliHandle, "        Timed out\r\n");
                    break;
                }
                case OSPF_RESTART_TOP_CHG:
                {
                    CliPrintf (CliHandle,
                               "        Resulted in topology change\r\n");
                    break;
                }
                default:
                    break;
            }
        }
        if (nmhGetFsMIOspfHelperSupport (u4OspfCxtId,
                                         &HelperSupport) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "  Helper is Giving Support for:\r\n");
            if (HelperSupport.i4_Length != 0)
            {
                if (HelperSupport.pu1_OctetList[0] & OSPF_GRACE_UNKNOWN)
                {
                    CliPrintf (CliHandle, "         Unknown\r\n ");
                }
                if (HelperSupport.pu1_OctetList[0] & OSPF_GRACE_SW_RESTART)
                {
                    CliPrintf (CliHandle, "        Software Restart\r\n ");
                }
                if (HelperSupport.
                    pu1_OctetList[0] & OSPF_GRACE_SW_RELOADUPGRADE)
                {
                    CliPrintf (CliHandle,
                               "        Software Reload/Upgrade\r\n ");
                }
                if (HelperSupport.pu1_OctetList[0] & OSPF_GRACE_SW_REDUNDANT)
                {
                    CliPrintf (CliHandle, "        Switch To Redundant");
                }
                if (HelperSupport.pu1_OctetList[0] == 0)
                {
                    CliPrintf (CliHandle, "        NONE");
                }
                CliPrintf (CliHandle, "\r\n");
            }
        }

        if (nmhGetFsMIOspfHelperGraceTimeLimit (u4OspfCxtId,
                                                &i4GracePeriod) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "  Helper Grace Time Limit: %d\r\n",
                       i4GracePeriod);
            CliPrintf (CliHandle, "  ");
        }

        if (nmhGetFsMIOspfRestartStrictLsaChecking (u4OspfCxtId,
                                                    &i4StrictLsaCheck)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Strict LSA checking State Is:");
            if (i4StrictLsaCheck == OSPF_TRUE)
            {
                CliPrintf (CliHandle, "Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "Disabled");
            }
        }

        CliPrintf (CliHandle, "\r\n");

        if (gOsRtr.u4RTStaggeringStatus == OSPF_STAGGERING_ENABLED)
        {
            CliPrintf (CliHandle,
                       "  Route calculation staggering is enabled\r\n");
            if (((nmhGetFsMIOspfRTStaggeringInterval
                  ((INT4) u4OspfCxtId, &u4RetValFutOspfRTStaggeringInterval)) ==
                 SNMP_SUCCESS))
            {
                CliPrintf (CliHandle,
                           "  Route calculation staggering interval is  %d"
                           " milliseconds\r\n",
                           u4RetValFutOspfRTStaggeringInterval);
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "  Route calculation staggering is disabled\r\n");
        }

        /*RRD Protocol Mask Enable */
        nmhGetFsMIOspfRRDStatus (u4OspfCxtId, &i4ScalarObject);
        if (i4ScalarObject == OSPF_ENABLED)
        {
            nmhGetFsMIOspfRRDSrcProtoMaskEnable (u4OspfCxtId, &i4ProtoMask);
            nmhGetFsMIOspfRRDRouteMapEnable (u4OspfCxtId, &RedistRouteMapName);

            RedistRouteMapName.pu1_OctetList[RedistRouteMapName.i4_Length] =
                OSPF_ZERO;

            if (RedistRouteMapName.i4_Length != 0)
            {
                SNPRINTF (pc1Redist, 32, "route-map %s",
                          RedistRouteMapName.pu1_OctetList);
            }

            CliPrintf (CliHandle, "  Redistributing External Routes -\r\n");

            if (i4ProtoMask & STATIC_RT_MASK)
            {
                CliPrintf (CliHandle, "  static");
            }
            if (i4ProtoMask & LOCAL_RT_MASK)
            {
                CliPrintf (CliHandle, "  connected");
            }
            if (i4ProtoMask & RIP_RT_MASK)
            {
                CliPrintf (CliHandle, "  rip");
            }
            if (i4ProtoMask & BGP_RT_MASK)
            {
                CliPrintf (CliHandle, "  bgp");
            }
            if (RedistRouteMapName.i4_Length != 0)
            {
                CliPrintf (CliHandle, " with %s", pc1Redist);
            }
            CliPrintf (CliHandle, " is enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "  Redistributing External Routes is disabled\r\n");
        }

        /*Default passive interface */
        nmhGetFsMIOspfDefaultPassiveInterface (u4OspfCxtId,
                                               &i4DefaultPassiveInterface);

        if (i4DefaultPassiveInterface == OSPF_TRUE)
        {
            CliPrintf (CliHandle, "  Default passive-interface  Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  Default passive-interface  Disabled\r\n");
        }

        /*RFC-1583 Compatibility */
        nmhGetFsMIOspfRFC1583Compatibility (u4OspfCxtId,
                                            &i4RFC1583Compatibility);

        if (i4RFC1583Compatibility == OSPF_ENABLED)
        {
            CliPrintf (CliHandle, "  Rfc1583 compatibility is enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  Rfc1583 compatibility is disabled\r\n");
        }

        if (nmhGetFsMIOspfPreferenceValue (u4OspfCxtId, &i4Distance)
            == SNMP_SUCCESS)
        {

            CliPrintf (CliHandle, "  Administrative Distance is %d\r\n",
                       i4Distance);
        }

        /* Distance output if route map is configured */
        if (UtilOspfSetContext (u4OspfCxtId) != SNMP_FAILURE)
        {

            MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
            RouteMapName.pu1_OctetList = au1RMapName;

            i1OutCome =
                nmhGetFirstIndexFutOspfDistInOutRouteMapTable (&RouteMapName,
                                                               &i4FilterType);
            while (i1OutCome != SNMP_FAILURE)
            {
                if (i4FilterType == FILTERING_TYPE_DISTANCE)
                {
                    if (nmhGetFutOspfDistInOutRouteMapValue
                        (&RouteMapName, i4FilterType, &i4Value) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "  Distance %d route-map %s\r\n",
                                   i4Value, RouteMapName.pu1_OctetList);
                    }
                }

                MEMSET (&NextRouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                MEMSET (au1NextRMapName, 0, RMAP_MAX_NAME_LEN + 4);
                NextRouteMapName.pu1_OctetList = au1NextRMapName;

                i1OutCome =
                    nmhGetNextIndexFutOspfDistInOutRouteMapTable (&RouteMapName,
                                                                  &NextRouteMapName,
                                                                  i4FilterType,
                                                                  &i4NextFilterType);

                i4FilterType = i4NextFilterType;

                MEMCPY (RouteMapName.pu1_OctetList,
                        NextRouteMapName.pu1_OctetList, RMAP_MAX_NAME_LEN + 4);
                RouteMapName.i4_Length = NextRouteMapName.i4_Length;
            }
        }

        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            i4IfCount = TMO_SLL_Count (&(pArea->ifsInArea));
            i4SpfCount = pArea->u2SpfRuns;
            u4IpAddr = OSPF_CRU_BMC_DWFROMPDU (pArea->areaId);
            nmhGetFutOspfAreaNSSATranslatorRole (u4IpAddr,
                                                 &i4RetValOspfAreaNSSATranslatorRole);
            nmhGetFutOspfAreaNSSATranslatorState (u4IpAddr,
                                                  &i4RetValOspfAreaNSSATranslatorState);
            nmhGetFutOspfAreaNSSATranslatorStabilityInterval (u4IpAddr,
                                                              &i4RetValOspfAreaNSSATranslatorStabilityInterval);

            nmhGetFutOspfAreaDfInfOriginate ((UINT4) u4IpAddr,
                                             &i4DfInfOriginate);
            nmhGetOspfAreaSummary ((UINT4) u4IpAddr, &i4OspfAreaSummary);

            OSPF_CLI_IPADDR_TO_STR (pu1String,
                                    OSPF_CRU_BMC_DWFROMPDU (pArea->areaId));
            TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
            {
                pInterface = GET_IF_PTR_FROM_LST (pLstNode);
                if (pInterface->ifStatus == ACTIVE)
                {
                    u4InterfaceActive = OSPF_TRUE;
                }
                if (pInterface->ifStatus == NOT_IN_SERVICE)
                {
                    u4NotInServiceIntfCnt++;
                }
            }
            if ((i4IfCount != 0) && (u4InterfaceActive == OSPF_TRUE))
            {
                CliPrintf (CliHandle, "  Area is %-16s\r\n", pu1String);
                i4ActInterface = 1;
                i4AreasCount++;
            }
            else
            {
                continue;
            }

            if ((pArea == pOspfCxt->pBackbone))
            {
                CliPrintf (CliHandle,
                           "  Number of interfaces in this area is %-10d\r\n",
                           (i4IfCount - u4NotInServiceIntfCnt));
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Number of interfaces in this area is %-10d\r\n",
                           i4IfCount);
            }
            if (pArea->u4AreaType == NSSA_AREA)
            {
                CliPrintf (CliHandle, "  It is NSSA Area \r\n");
            }
            else if (pArea->u4AreaType == STUB_AREA)
            {
                CliPrintf (CliHandle, "  It is STUB Area \r\n");
            }

            if ((pArea->u4AreaType == NSSA_AREA))
            {
                if (i4RetValOspfAreaNSSATranslatorRole == TRNSLTR_ROLE_ALWAYS)
                {
                    if (i4RetValOspfAreaNSSATranslatorState == 1)
                    {

                        CliPrintf (CliHandle, "  Transition-role : Always\r\n");
                        CliPrintf (CliHandle,
                                   "  Transition-state : Enabled\r\n");
                    }
                    else
                    {

                        CliPrintf (CliHandle, "  Transition-role : Always\r\n");
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "  Transition-role : Candidate\r\n");
                }
                if (i4RetValOspfAreaNSSATranslatorState == 2)
                {
                    CliPrintf (CliHandle, "  Transition-state : Elected\r\n");
                }
                else if (i4RetValOspfAreaNSSATranslatorState == 3)
                {
                    CliPrintf (CliHandle, "  Transition-state : Disabled\r\n");
                }

                CliPrintf (CliHandle, "  Stability Interval is %-16d\r\n",
                           i4RetValOspfAreaNSSATranslatorStabilityInterval);
                u4AreaId = OSPF_CRU_BMC_DWFROMPDU (pArea->areaId);

                nmhGetFsMIStdOspfStubMetric (u4OspfCxtId, u4AreaId, i4MetricTOS,
                                             &i4RetValStubMetric);
                nmhGetFsMIStdOspfStubMetricType (u4OspfCxtId, u4AreaId,
                                                 i4MetricType,
                                                 &i4RetValStubMetricType);

                if (i4DfInfOriginate == DEFAULT_INFO_ORIGINATE)
                {
                    CliPrintf (CliHandle,
                               "  Default information originate: ENABLED");
                    CliPrintf (CliHandle,
                               "  with Metric %d and MetricType %d\r\n",
                               i4RetValStubMetric, i4RetValStubMetricType);

                }
                else
                {
                    CliPrintf (CliHandle,
                               "  Default information originate: DISABLED\r\n");
                }
            }
            if ((pArea->u4AreaType == NSSA_AREA) ||
                (pArea->u4AreaType == STUB_AREA))
            {
                nmhGetOspfStubMetric (u4IpAddr, i4OspfStubTOS,
                                      &i4RetValOspfDefaultCost);
                CliPrintf (CliHandle, "  DefaultCost is :%d\r\n",
                           i4RetValOspfDefaultCost);
                if (i4OspfAreaSummary == NO_AREA_SUMMARY)
                {
                    CliPrintf (CliHandle, "  No Area Summary\r\n");
                }
                else if (i4OspfAreaSummary == SEND_AREA_SUMMARY)
                {
                    CliPrintf (CliHandle, "  Send Area Summary\r\n");
                }
            }
            i4PageStatus = CliPrintf (CliHandle,
                                      "  SPF algorithm executed %d times\r\n",
                                      i4SpfCount);
            if (i4PageStatus == CLI_FAILURE)
                break;
        }
        if (i4ActInterface)
        {
            CliPrintf (CliHandle, "  Number of Areas in this router is %d\r\n",
                       i4AreasCount);
        }

        if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i1OutCome =
            nmhGetFirstIndexFutOspfExtRouteTable (&u4RouteDest, &u4RouteMask,
                                                  &i4RouteTos);
        UtilOspfResetContext ();

        if (i1OutCome != SNMP_FAILURE)
        {

            /*Ext Route Table Row Status */
            nmhGetFsMIOspfExtRouteStatus (u4OspfCxtId, u4RouteDest,
                                          u4RouteMask, i4RouteTos,
                                          &i4TableObject);
            if ((i4TableObject == ACTIVE) && (u4RouteDest == IP_ANY_ADDR)
                && (u4RouteMask == IP_ANY_ADDR))
            {
                CliPrintf (CliHandle,
                           "  Default information originate is enabled\r\n");
                /*Route metric */
                nmhGetFsMIOspfExtRouteMetric (u4OspfCxtId, u4RouteDest,
                                              u4RouteMask, i4RouteTos,
                                              &i4TableObject);
                CliPrintf (CliHandle, "         with metric value %d",
                           i4TableObject);
                /*Route metric type */
                nmhGetFsMIOspfExtRouteMetricType (u4OspfCxtId, u4RouteDest,
                                                  u4RouteMask, i4RouteTos,
                                                  &i4TableObject);
                CliPrintf (CliHandle, " & metric type %d\r\n", i4TableObject);
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "  Default information originate is disabled\r\n");
        }
        /* Display for OSPF BFD relating commands */
        if ((nmhGetFsMIOspfBfdStatus ((INT4) u4OspfCxtId, &i4FutInt)
             == SNMP_SUCCESS) && i4FutInt == OSPF_BFD_ENABLED)
        {
            CliPrintf (CliHandle, "  BFD is enabled  \r\n");
            FilePrintf (CliHandle, "  BFD is enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, " BFD is disabled \r\n");
            FilePrintf (CliHandle, " BFD is disabled  \r\n");
        }
        /*Display for OSPF host related commands */
        i1OutCome =
            nmhGetFirstIndexFsMIOspfHostTable (((INT4 *) &u4OspfCxtId),
                                               &u4FsMIOspfHostIpAddress,
                                               &i4FsMIOspfHostTOS);
        CliPrintf (CliHandle, "Host Configuration \r\n");
        CliPrintf (CliHandle,
                   "====================================================== \r\n");
        CliPrintf (CliHandle, "IP Address \t Metric Value \t\t Area ID \r\n");
        FilePrintf (CliHandle, "IP Address \t Metric Value \t\t Area ID \r\n");

        while (i1OutCome == SNMP_SUCCESS)
        {

            OSPF_CLI_IPADDR_TO_STR (pu1String, u4FsMIOspfHostIpAddress);

            CliPrintf (CliHandle, "%s \t\t", pu1String);
            FilePrintf (CliHandle, "%s \t\t", pu1String);

            if (i4FsMIOspfHostTOS != 0)
            {
                printf ("Debug: Error Scenario - TOS != 0\n");
            }

            if (SNMP_SUCCESS ==
                nmhGetFsMIStdOspfHostMetric (((INT4) u4OspfCxtId),
                                             u4FsMIOspfHostIpAddress,
                                             i4FsMIOspfHostTOS,
                                             &i4RetValOspfHostMetric))
            {
                CliPrintf (CliHandle, "%d \t\t", i4RetValOspfHostMetric);
                FilePrintf (CliHandle, "%d \t\t", i4RetValOspfHostMetric);

            }

            if (SNMP_SUCCESS ==
                nmhGetFsMIStdOspfHostAreaID (((INT4) u4OspfCxtId),
                                             u4FsMIOspfHostIpAddress,
                                             i4FsMIOspfHostTOS,
                                             &u4RetValOspfHostAreaID))
            {
                OSPF_CLI_IPADDR_TO_STR (pu1String, u4RetValOspfHostAreaID);

                CliPrintf (CliHandle, "%s  \r\n", pu1String);
                FilePrintf (CliHandle, "%s  \r\n", pu1String);

            }

            i1OutCome =
                nmhGetNextIndexFsMIOspfHostTable ((INT4) u4OspfCxtId,
                                                  &i4NextFsMIOspfHostContextId,
                                                  u4FsMIOspfHostIpAddress,
                                                  &u4NextFsMIOspfHostIpAddress,
                                                  i4FsMIOspfHostTOS,
                                                  &i4NextFsMIOspfHostTOS);

            if (i1OutCome == SNMP_SUCCESS)
            {
                u4OspfCxtId = (UINT4) i4NextFsMIOspfHostContextId;
                u4FsMIOspfHostIpAddress = u4NextFsMIOspfHostIpAddress;
                i4FsMIOspfHostTOS = i4NextFsMIOspfHostTOS;
            }
            else
            {
                break;
            }
        }

        /* If u4ShowAllCxt is set to OSPF_TRUE then all context 
         * must be displayed */
        if (u4ShowAllCxt == OSPF_FALSE)
        {
            break;
        }
        u4OspfPrevCxtId = u4OspfCxtId;
    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   :OspfCliShowOspfRedInfo                    */
/*  Description     :This Routine will show the OSPF           */
/*                   redundancy information                    */
/*                                                             */
/*  Input(s)        :CliHandle    - CliContext ID              */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         :CLI_SUCCESS/CLI_FAILURE                   */
/***************************************************************/
INT4
OspfCliShowOspfRedInfo (tCliHandle CliHandle)
{
    INT4                i4AdminState = OSPF_RED_ADMIN_DISABLE;
    UINT4               u4OsRmState = OSPF_RED_INIT;
    UINT4               u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_NOT_STARTED;
    UINT4               u4HelloSyncCount = 0;
    UINT4               u4LsaSyncCount = 0;

    CliPrintf (CliHandle, "\r\n Redundancy Summary \r\n");
    CliPrintf (CliHandle, " ------------------ \r\n");

    nmhGetFutOspfHotStandbyAdminStatus (&i4AdminState);

    CliPrintf (CliHandle, "  Hotstandby admin status : ");
    if (i4AdminState == OSPF_RED_ADMIN_ENABLE)
    {
        CliPrintf (CliHandle, "Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled \r\n");
    }

    nmhGetFutOspfHotStandbyState ((INT4 *) &u4OsRmState);

    CliPrintf (CliHandle, "  Hotstandby state : ");

    if (u4OsRmState == OSPF_RED_ACTIVE_STANDBY_UP)
    {
        CliPrintf (CliHandle, "Active and Standby Up \r\n");
    }
    else if (u4OsRmState == OSPF_RED_ACTIVE_STANDBY_DOWN)
    {
        CliPrintf (CliHandle, "Active and Standby Down \r\n");
    }
    else if (u4OsRmState == OSPF_RED_STANDBY)
    {
        CliPrintf (CliHandle, "Standby \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Idle \r\n");
    }

    nmhGetFutOspfDynamicBulkUpdStatus ((INT4 *) &u4DynBulkUpdatStatus);

    CliPrintf (CliHandle, "  Hotstandby bulk update status : ");

    if (u4DynBulkUpdatStatus == OSPF_RED_BLKUPDT_INPROGRESS)
    {
        CliPrintf (CliHandle, "In Progress \r\n");
    }
    else if (u4DynBulkUpdatStatus == OSPF_RED_BLKUPDT_COMPLETED)
    {
        CliPrintf (CliHandle, "Completed \r\n");
    }
    else if (u4DynBulkUpdatStatus == OSPF_RED_BLKUPDT_ABORTED)
    {
        CliPrintf (CliHandle, "Aborted \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Not Started \r\n");
    }

    nmhGetFutOspfStanbyHelloSyncCount (&u4HelloSyncCount);

    CliPrintf (CliHandle, "  Number of hello PDUs synced : %d \r\n",
               u4HelloSyncCount);

    nmhGetFutOspfStanbyLsaSyncCount (&u4LsaSyncCount);

    CliPrintf (CliHandle, "  Number of LSAs synced : %d \r\n\r\n",
               u4LsaSyncCount);

    return CLI_SUCCESS;
}

#ifdef RM_WANTED
/***************************************************************/
/*  Function Name   :OspfCliShowOspfRedExtRouteInfo            */
/*  Description     :This Routine will show the OSPF           */
/*                   redundancy external route information     */
/*                                                             */
/*  Input(s)        :CliHandle    - CliContext ID              */
/*                                                              */
/*  Output(s)       :None                                      */
/*  Returns         :CLI_SUCCESS/CLI_FAILURE                   */
/***************************************************************/
INT4
OspfCliShowOspfRedExtRouteInfo (tCliHandle CliHandle)
{

    tInputParams        inParams;
    tOspfCxt           *pOspfCxt = NULL;
    tExtRoute          *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    UINT4               au4Key[2];
    VOID               *pAppSpecPtr = NULL;
    UINT4               u4ContextId = OSPF_ZERO;
    UINT4               u4MaxCxt = 0;
    CHR1               *pu1String = NULL;

    au4Key[0] = 0;
    au4Key[1] = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4ContextId = OSPF_DEFAULT_CXT_ID;
         u4ContextId < u4MaxCxt; u4ContextId++)

    {
        pOspfCxt = UtilOspfGetCxt (u4ContextId);
        if (pOspfCxt == NULL)
        {
            continue;
        }
        if (pOspfCxt->pExtRtRoot == NULL)
        {
            continue;
        }
        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pOspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPF_EXT_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        if (TrieGetFirstNode (&inParams,
                              &pAppSpecPtr,
                              (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nIP Address     IP mask        Source Protocol   Metric     ForwardIP    NextHop      ExtTag\r\n");
            CliPrintf (CliHandle,
                       "\r-----------     -------        ---------------   ------     ---------    -------      ------\r\n");
            do
            {
                pExtRoute = (tExtRoute *) pAppSpecPtr;

                if (NULL == pExtRoute)
                {
                    continue;
                }
                inParams.pRoot = pOspfCxt->pExtRtRoot;

                OSPF_CLI_IPADDR_TO_STR (pu1String,
                                        OSPF_CRU_BMC_DWFROMPDU (pExtRoute->
                                                                ipNetNum));
                CliPrintf (CliHandle, "%s", pu1String);

                OSPF_CLI_IPADDR_TO_STR (pu1String,
                                        OSPF_CRU_BMC_DWFROMPDU (pExtRoute->
                                                                ipAddrMask));
                CliPrintf (CliHandle, "%15s", pu1String);

                CliPrintf (CliHandle, "%15d", pExtRoute->u2SrcProto);
                CliPrintf (CliHandle, "%15d", pExtRoute->aMetric->u4MetricType);

                OSPF_CLI_IPADDR_TO_STR (pu1String,
                                        OSPF_CRU_BMC_DWFROMPDU (pExtRoute->
                                                                extrtParam->
                                                                fwdAddr));
                CliPrintf (CliHandle, "%15s", pu1String);

                OSPF_CLI_IPADDR_TO_STR (pu1String,
                                        OSPF_CRU_BMC_DWFROMPDU (pExtRoute->
                                                                extrtParam->
                                                                fwdNextHop));
                CliPrintf (CliHandle, "%13s", pu1String);

                CliPrintf (CliHandle, "%10d\n",
                           pExtRoute->extrtParam->u4ExtTag);
                pLeafNode = inParams.pLeafNode;
                pExtRoute = NULL;

            }
            while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                    &pAppSpecPtr,
                                    (VOID **) &(inParams.pLeafNode)) !=
                   TRIE_FAILURE);

        }

    }

    return CLI_SUCCESS;
}
#endif
/******************************************************************************/
/* Function Name     : OspfCliSetAreaDataBaseInCxt                            */
/*                                                                            */
/* Description       : This Routine will Set the OSPF                         */
/*                     LSA Database Information filters to be applied         */
/*                     and returns the option value used for display          */
/*                                                                            */
/* Input Parameters  : Database     - Filter to be applied for display of     */
/*                                    LSA database information                */
/*                     pOspfCxt     - pointer to tOspfCxt structure           */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetAreaDataBaseInCxt (tDatabase Database, tOspfCxt * pOspfCxt)
{
    INT4                i4Option = 0;

    /* Option values shud be set as follows based on the combinations 
     * given below
     * LS-TYPE                     -- 1
     * LS-TYPE SELF-ORG            -- 2
     * LS-TYPE ADV-ROUTER          -- 3
     * LS-TYPE LS-ID               -- 4
     * LS-TYPE LS-ID SELF-ORG      -- 5
     * LS-TYPE LS-ID ADV-ROUTER    -- 6
     * ALL LSA's                   -- 7
     * SELF-ORG                    -- 8
     * ADV-ROUTER                  -- 9
     */

    if (Database.i4LsdbType != 0)
    {
        LS_TYPE = (UINT1) Database.i4LsdbType;
        i4Option = 1;
        if (Database.i4DatabaseOptions & LSA_LINK_STATE_ID)
        {
            OSPF_CRU_BMC_DWTOPDU (LS_ID, Database.u4LsId);
            i4Option += 3;
        }
    }
    else
    {
        /* Case without LSA Type */
        i4Option = 7;
    }

    if (Database.i4DatabaseOptions & LSA_SELF_ORG)
    {
        MEMCPY (LS_ROUTER_ID, pOspfCxt->rtrId, sizeof (tIPADDR));
        i4Option += 1;
    }
    if (Database.i4DatabaseOptions & LSA_ADV_ROUTER)
    {
        OSPF_CRU_BMC_DWTOPDU (LS_ADV_ROUTER_ID, Database.u4AdvRouteId);
        i4Option += 2;
    }

    return i4Option;
}

/******************************************************************************/
/* Function Name     : OspfCliSetASBRStatus                                   */
/*                                                                            */
/* Description       : This Routine will set ASBR Status                      */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     i4FutInt     - ASBR status to set OSPF_TRUE/OSPF_FALSE */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetASBRStatus (tCliHandle CliHandle, INT4 i4FutInt)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2OspfASBdrRtrStatus (&u4ErrorCode, i4FutInt) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfASBdrRtrStatus (i4FutInt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowExtDatabaseInCxt                            */
/*                                                                            */
/* Description       : This Routine will show the AS-External Area database   */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pOspfCxt      - pointer to tOspfCxt structure          */
/*                     i4Option      - Display Option                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowExtDatabaseInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                             INT4 i4Option)
{
    tLsaInfo           *pLsaInfo = NULL;
    tLsaInfo           *pNextLsaInfo = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;
    tOspfCxt           *pOspfCxt;
    INT4                i4AdminStat;
    tLsaInfo            LsaInfo;
    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {

        return i4PageStatus;
    }
    MEMSET (&LsaInfo, 0, sizeof (tLsaInfo));

    pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot);

    u1HeaderFlag = OSIX_TRUE;
    while (pLsaInfo != NULL)
    {
        MEMCPY (&LsaInfo, pLsaInfo, sizeof (tLsaInfo));
        pNextLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pOspfCxt->pAsExtLsaRBRoot,
                                        (tRBElem *) & LsaInfo, NULL);
        i4PageStatus = OspfCliFilterLsaInfo (CliHandle, &LsaInfo, i4Option);
        if (i4PageStatus == CLI_FAILURE)
            break;
        pLsaInfo = pNextLsaInfo;
    }
    u1HeaderFlag = OSIX_FALSE;
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliGetAsExternalLsaCntInCxt                        */
/*                                                                            */
/* Description       : This Routine will get the AS External LSA count and    */
/*                     max aged LSA count                                     */
/*                                                                            */
/* Input Parameters  : pOspfCxt      - pointer to tOspfCxt structure          */
/*                     pu4LsaCnt     - LSA Count                              */
/*                     pu4LsaAgedCnt - Max aged LSA count                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliGetAsExternalLsaCntInCxt (tOspfCxt * pOspfCxt, UINT4 *pu4LsaCnt,
                                 UINT4 *pu4LsaAgedCnt)
{
    tLsaInfo           *pLsaInfo;

    pLsaInfo = (tLsaInfo *) RBTreeGetFirst (pOspfCxt->pAsExtLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        *pu4LsaCnt = *pu4LsaCnt + 1;
        if (pLsaInfo->u1LsaFlushed == OSPF_TRUE)
            *pu4LsaAgedCnt = *pu4LsaAgedCnt + 1;
        pLsaInfo =
            (tLsaInfo *) RBTreeGetNext (pOspfCxt->pAsExtLsaRBRoot,
                                        (tRBElem *) pLsaInfo, NULL);
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowOpqAsDatabaseInCxt                          */
/*                                                                            */
/* Description       : This Routine will show the Opaque AS Area database     */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pOspfCxt      - pointer to tOspfCxt structure          */
/*                     i4Option      - Display Option                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowOpqAsDatabaseInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                               INT4 i4Option)
{
    tTMO_SLL           *pLsaLst;
    tLsaInfo           *pLsaInfo;
    tLsaInfo            LsaInfo;
    tTMO_SLL_NODE      *pLsaNode;
    INT4                i4PageStatus = CLI_SUCCESS;
    tOspfCxt           *pOspfCxt;
    INT4                i4AdminStat;

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {

        return i4PageStatus;
    }
    MEMSET (&LsaInfo, 0, sizeof (tLsaInfo));
    pLsaLst = &(pOspfCxt->Type11OpqLSALst);

    TMO_SLL_Scan (pLsaLst, pLsaNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
        MEMCPY (&LsaInfo, pLsaInfo, sizeof (tLsaInfo));
        i4PageStatus = OspfCliFilterLsaInfo (CliHandle, pLsaInfo, i4Option);
        if (i4PageStatus == CLI_FAILURE)
            break;
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliGetOpqAsLsaCntInCxt                             */
/*                                                                            */
/* Description       : This Routine will get the Opaque AS LSA count and      */
/*                     max aged LSA count                                     */
/*                                                                            */
/* Input Parameters  : pOspfCxt      - pointer to tOspfCxt structure          */
/*                     pu4LsaCnt     - LSA Count                              */
/*                     pu4LsaAgedCnt - Max aged LSA count                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliGetOpqAsLsaCntInCxt (tOspfCxt * pOspfCxt, UINT4 *pu4LsaCnt,
                            UINT4 *pu4LsaAgedCnt)
{
    tTMO_SLL           *pLsaLst;
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLsaNode;

    pLsaLst = &(pOspfCxt->Type11OpqLSALst);

    TMO_SLL_Scan (pLsaLst, pLsaNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
        *pu4LsaCnt = *pu4LsaCnt + 1;
        if (pLsaInfo->u1LsaFlushed == OSPF_TRUE)
            *pu4LsaAgedCnt = *pu4LsaAgedCnt + 1;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowDatabaseInCxt                               */
/*                                                                            */
/* Description       : This Routine will display LSA Database information     */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     Database     - LSA Database filter information         */
/*                     u4OspfCxtId  - ospf context id                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowDatabaseInCxt (tCliHandle CliHandle, tDatabase Database,
                          UINT4 u4OspfCxtId)
{
    tLsdbCounter        LsdbCounter;
    tAreaId             OspfAreaId;
    tArea              *pArea;
    CHR1               *pu1String = NULL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Option = 0;
    INT4                i4AdminStat;
    UINT4               u4AreaId;
    UINT4               u4NextAreaId;
    UINT1               u1LsType;
    UINT4               u4LsTypeTotal = 0;
    UINT4               u4LsTypeMaxAgedTotal = 0;
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u4ShowAllCxt = OSPF_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;

        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                /* Invalid context id or admin status of the 
                 * context is disabled */
                break;
            }

            continue;
        }
        MEMSET (&LsdbCounter, 0, sizeof (tLsdbCounter));
        if (!(Database.i4DatabaseOptions & LSA_AREA_DATABASE))
        {
            OSPF_CLI_IPADDR_TO_STR (pu1String,
                                    OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId));
            if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                OSPF_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, " \r\nOSPF Router with ID (%s) (Vrf  %s)\r\n",
                       pu1String, au1OspfCxtName);

            /* Scan thru area database and call OspfCliSetAreaDataBaseInCxt 
             * with that AreaId
             */
            if ((Database.i4LsdbType != AS_EXT_LSA) &&
                (Database.i4LsdbType != TYPE11_OPQ_LSA))
            {
                u4AreaId = 0;

                do
                {
                    if (u4OspfCxtId != u4OspfPrevCxtId)
                    {
                        u4OspfCxtId = u4OspfPrevCxtId;
                        break;
                    }
                    /* Area id got from scanning the area table */
                    Database.u4AreaId = u4AreaId;

                    if (!(Database.i4DatabaseOptions & LSA_DB_SUMMARY))
                    {
                        i4Option =
                            OspfCliSetAreaDataBaseInCxt (Database, pOspfCxt);
                        i4RetVal =
                            OspfCliFindAreaInCxt (CliHandle, u4OspfCxtId,
                                                  Database.u4AreaId,
                                                  Database.i4LsdbType,
                                                  i4Option);
                    }
                    else
                    {
                        i4RetVal =
                            OspfCliShowAreaDataBaseSummaryInCxt (CliHandle,
                                                                 u4OspfCxtId,
                                                                 Database.
                                                                 u4AreaId,
                                                                 &LsdbCounter);
                    }
                    if (i4RetVal == CLI_FAILURE)
                    {
                        break;
                    }
                    u4NextAreaId = u4AreaId;
                    u4OspfPrevCxtId = u4OspfCxtId;
                }
                while (nmhGetNextIndexFsMIStdOspfAreaTable ((INT4)
                                                            u4OspfPrevCxtId,
                                                            (INT4 *)
                                                            &u4OspfCxtId,
                                                            u4NextAreaId,
                                                            &u4AreaId) ==
                       SNMP_SUCCESS);
            }
            if (!(Database.i4DatabaseOptions & LSA_DB_SUMMARY) &&
                (i4RetVal == CLI_SUCCESS))
            {
                i4Option = OspfCliSetAreaDataBaseInCxt (Database, pOspfCxt);
                if ((Database.i4LsdbType == 0) ||
                    (Database.i4LsdbType == AS_EXT_LSA))
                {
                    i4RetVal = OspfCliShowExtDatabaseInCxt (CliHandle,
                                                            u4OspfCxtId,
                                                            i4Option);
                }
                if ((Database.i4LsdbType == 0) ||
                    (Database.i4LsdbType == TYPE11_OPQ_LSA))
                {
                    i4RetVal = OspfCliShowOpqAsDatabaseInCxt (CliHandle,
                                                              u4OspfCxtId,
                                                              i4Option);
                }
            }
            /* Display Total Process database summary */
            if ((i4RetVal == CLI_SUCCESS) &&
                (Database.i4DatabaseOptions & LSA_DB_SUMMARY))
            {
                CliPrintf (CliHandle, "\r\nOSPF Process database summary\r\n");
                CliPrintf (CliHandle, "-----------------------------\r\n");
                CliPrintf (CliHandle, "  %-20s %-10s %-10s\r\n",
                           "LSA Type", "Count", "Maxage");
                CliPrintf (CliHandle, "  %-20s %-10s %-10s\r\n",
                           "--------", "-----", "------");

                for (u1LsType = ROUTER_LSA; u1LsType <= MAX_LSA_TYPE;
                     u1LsType++)
                {
                    switch (u1LsType)
                    {
                        case ROUTER_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Router",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case NETWORK_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Network",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case NETWORK_SUM_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Summary Net",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case ASBR_SUM_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Summary ASBR",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case AS_EXT_LSA:
                            OspfCliGetAsExternalLsaCntInCxt
                                (pOspfCxt,
                                 &(LsdbCounter.au4LsTypeCnt[u1LsType - 1]),
                                 (UINT4 *) &(LsdbCounter.
                                             au4LsTypeMaxAgedCnt[u1LsType -
                                                                 1]));
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Type-5 Ext",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case NSSA_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Type-7 Ext",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case TYPE9_OPQ_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Opaque Link",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case TYPE10_OPQ_LSA:
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Opaque Area",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case TYPE11_OPQ_LSA:
                            OspfCliGetOpqAsLsaCntInCxt
                                (pOspfCxt,
                                 &(LsdbCounter.au4LsTypeCnt[u1LsType - 1]),
                                 (UINT4 *) &(LsdbCounter.
                                             au4LsTypeMaxAgedCnt[u1LsType -
                                                                 1]));
                            i4RetVal =
                                CliPrintf (CliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Opaque AS",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        default:
                            break;
                    }
                    u4LsTypeTotal += LsdbCounter.au4LsTypeCnt[u1LsType - 1];
                    u4LsTypeMaxAgedTotal +=
                        LsdbCounter.au4LsTypeMaxAgedCnt[u1LsType - 1];

                    if (i4RetVal == CLI_FAILURE)
                        break;
                }
                CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n\r\n",
                           "Total", u4LsTypeTotal, u4LsTypeMaxAgedTotal);
            }
        }
        else
        {
            OSPF_CRU_BMC_DWTOPDU (OspfAreaId, Database.u4AreaId);
            pArea = GetFindAreaInCxt (pOspfCxt, &OspfAreaId);
            if (pArea != NULL)
            {
                OSPF_CLI_IPADDR_TO_STR (pu1String,
                                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->
                                                                rtrId));

                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName))
                    == OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle, "\r\nVrf Name:          %s\r\n",
                           au1OspfCxtName);
                CliPrintf (CliHandle, "----------------\r\n");
                CliPrintf (CliHandle,
                           " \r\nOSPF Router with ID (%s) (Vrf  %s)\r\n",
                           pu1String, au1OspfCxtName);

                if (!(Database.i4DatabaseOptions & LSA_DB_SUMMARY))
                {
                    i4Option = OspfCliSetAreaDataBaseInCxt (Database, pOspfCxt);
                    if ((Database.i4LsdbType != AS_EXT_LSA) &&
                        (Database.i4LsdbType != TYPE11_OPQ_LSA))
                    {
                        i4RetVal = OspfCliFindAreaInCxt
                            (CliHandle, u4OspfCxtId, Database.u4AreaId,
                             Database.i4LsdbType, i4Option);
                    }
                    if (i4RetVal == CLI_SUCCESS)
                    {
                        if ((Database.i4LsdbType == 0) ||
                            (Database.i4LsdbType == AS_EXT_LSA))
                        {
                            i4RetVal = OspfCliShowExtDatabaseInCxt (CliHandle,
                                                                    u4OspfCxtId,
                                                                    i4Option);
                        }
                        if ((Database.i4LsdbType == 0) ||
                            (Database.i4LsdbType == TYPE11_OPQ_LSA))
                        {
                            i4RetVal =
                                OspfCliShowOpqAsDatabaseInCxt (CliHandle,
                                                               u4OspfCxtId,
                                                               i4Option);
                        }
                    }
                }
                else
                {
                    i4RetVal = OspfCliShowAreaDataBaseSummaryInCxt (CliHandle,
                                                                    u4OspfCxtId,
                                                                    Database.
                                                                    u4AreaId,
                                                                    &LsdbCounter);
                }
            }
            else
            {
                OSPF_CLI_IPADDR_TO_STR (pu1String, Database.u4AreaId);
                if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName))
                    == OSPF_FAILURE)
                {
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle, "\r\nVrf Name:          %s\r\n",
                           au1OspfCxtName);
                CliPrintf (CliHandle, "----------------\r\n");
                CliPrintf (CliHandle, "\r\nInvalid Area ID: %s\r\n", pu1String);
                i4RetVal = CLI_FAILURE;
            }
        }

        /* If u4ShowAllCxt is set to OSPF_TRUE then all context 
         * must be displayed */
        if (u4ShowAllCxt == OSPF_FALSE)
        {
            break;
        }

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
           != OSPF_FAILURE);

    return i4RetVal;
}

/******************************************************************************/
/* Function Name     : OspfCliShowOspfCounters                                */
/*                                                                            */
/* Description       : This Routine will display the OSPF failure counters    */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowOspfCounters (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "OSPF Counters : \n");
    CliPrintf (CliHandle,
               "-------------------------------------------------- \n");
    CliPrintf (CliHandle, "UtilOspfIsLowPriorityMessageFailCount    : %d\r\n",
               gu4UtilOspfIsLowPriorityMessageFail);
    CliPrintf (CliHandle, "OspfMemReleaseMemBlockFailCount        : %d\r\n",
               gu4OspfMemReleaseMemBlockFail);
    CliPrintf (CliHandle, "UtilOspfGetCxtFailCount            : %d\r\n",
               gu4UtilOspfGetCxtFail);
    CliPrintf (CliHandle, "UtilSelectOspfRouterIdFailCount        : %d\r\n",
               gu4UtilSelectOspfRouterIdFail);
    CliPrintf (CliHandle, "NetIpv4GetCfaIfIndexFromPortFailCount    : %d\r\n",
               gu4NetIpv4GetCfaIfIndexFromPortFail);
    CliPrintf (CliHandle, "VcmIsL3VcExistFailCount            : %d\r\n",
               gu4VcmIsL3VcExistFail);
    CliPrintf (CliHandle, "UtilOspfVcmIsVcExistFailCount        : %d\r\n",
               gu4UtilOspfVcmIsVcExistFail);
    CliPrintf (CliHandle, "IfSetOperStatFailCount            : %d\r\n",
               gu4IfSetOperStatFail);
    CliPrintf (CliHandle, "OSPFSetIfMtuSizeInCxtFailCount        : %d\r\n",
               gu4OSPFSetIfMtuSizeInCxtFail);
    CliPrintf (CliHandle, "OspfSnmpIfMsgAllocFailCount        : %d\r\n",
               gu4OspfSnmpIfMsgAllocFail);
    CliPrintf (CliHandle, "OspfSnmpIfMsgSendFailCount        : %d\r\n",
               gu4OspfSnmpIfMsgSendFail);
    CliPrintf (CliHandle, "OSPFSetIfSpeedInCxtFailCount        : %d\r\n",
               gu4OSPFSetIfSpeedInCxtFail);
    CliPrintf (CliHandle, "RtrDisableInCxtFailCount                : %d\r\n",
               gu4RtrDisableInCxtFail);
    CliPrintf (CliHandle, "FsNpOspfCreateAndDeleteFilterFailCount    : %d\r\n",
               gu4FsNpOspfCreateAndDeleteFilterFail);
    CliPrintf (CliHandle, "GetFindRrdConfRtInfoInCxtFailCount    : %d\r\n",
               gu4GetFindRrdConfRtInfoInCxtFail);
    CliPrintf (CliHandle, "IfacesDelFailCount            : %d\r\n",
               gu4IfacesDelFail);
    CliPrintf (CliHandle, "OspfCreateSocketFailCount        : %d\r\n",
               gu4OspfCreateSocketFail);
    CliPrintf (CliHandle, "AppAllocFailCount            : %d\r\n",
               gu4AppAllocFail);
    CliPrintf (CliHandle, "AppOpqtypeFailCount            : %d\r\n",
               gu4AppOpqtypeFail);
    CliPrintf (CliHandle, "AppInfoInCxtFailCount            : %d\r\n",
               gu4AppInfoInCxtFail);
    CliPrintf (CliHandle, "pOspfCxtFailCountCount            : %d\r\n",
               gu4pOspfCxtFail);
    CliPrintf (CliHandle, "RtmTxRedistributeDisableInCxtFailCount    : %d\r\n",
               gu4RtmTxRedistributeDisableInCxtFail);
    CliPrintf (CliHandle, "CruBufAllocMsgBufChainFailCount        : %d\r\n",
               gu4CruBufAllocMsgBufChainFail);
    CliPrintf (CliHandle, "IpCopyToBufFailCount            : %d\r\n",
               gu4IpCopyToBufFail);
    CliPrintf (CliHandle, "RpsEnqueuePktToRtmFailCount        : %d\r\n",
               gu4RpsEnqueuePktToRtmFail);
    CliPrintf (CliHandle, "RtrShutdownInCxtFailCount        : %d\r\n",
               gu4RtrShutdownInCxtFail);
    CliPrintf (CliHandle, "OspfCxtIdFailCount            : %d\r\n",
               gu4OspfCxtIdFail);
    CliPrintf (CliHandle, "RtrRebootInCxtFailCount            : %d\r\n",
               gu4RtrRebootInCxtFail);
    CliPrintf (CliHandle, "RmApiHandleProtocolEventFailCount    : %d\r\n",
               gu4RmApiHandleProtocolEventFail);
    CliPrintf (CliHandle, "RmReleaseMemoryForMsgFailCount        : %d\r\n",
               gu4RmReleaseMemoryForMsgFail);
    CliPrintf (CliHandle, "RmEnqMsgToRmFromApplFailCount        : %d\r\n",
               gu4RmEnqMsgToRmFromApplFail);
    CliPrintf (CliHandle, "UtilOspfGetFirstCxtIdFailCount        : %d\r\n",
               gu4UtilOspfGetFirstCxtIdFail);
    CliPrintf (CliHandle, "UtilOspfGetNextCxtIdFailCount        : %d\r\n",
               gu4UtilOspfGetNextCxtIdFail);
    CliPrintf (CliHandle, "UtilOsMsgAllocFailCount            : %d\r\n",
               gu4UtilOsMsgAllocFail);
    CliPrintf (CliHandle, "OspfRmSendBulkUpdateFailCount        : %d\r\n",
               gu4OspfRmSendBulkUpdateFail);
    CliPrintf (CliHandle, "OspfRmAllocateBulkUpdatePktFailCount    : %d\r\n",
               gu4OspfRmAllocateBulkUpdatePktFail);
    CliPrintf (CliHandle, "OspfRmSendMsgToRmFailCount        : %d\r\n",
               gu4OspfRmSendMsgToRmFail);
    CliPrintf (CliHandle, "OsRmBulkUpdtRelinquishFailCount        : %d\r\n",
               gu4OsRmBulkUpdtRelinquishFail);

    return CLI_SUCCESS;

}

/******************************************************************************/
/* Function Name     : OspfCliDisplayLsa                                      */
/*                                                                            */
/* Description       : This Routine will display LSA Database information     */
/*                     into the CLI Context given by CliHandle                */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pOspfDb      - LSA Database information                */
/*                     i4ShowOption - Show filter information                 */
/*                     i4AreaId     - Area ID                                 */
/*                     u4AdvRouterId - Advertising router ID                  */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDisplayLsa (tCliHandle CliHandle, tShowOspfDb * pOspfDb,
                   INT4 i4ShowOption, UINT4 u4AreaId, UINT4 u4AdvRouterId)
{
    tShowOspfDbInfo    *pOspfDbInfo;
    UINT4               u4Index;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;

    UNUSED_PARAM (i4ShowOption);
    UNUSED_PARAM (u4AreaId);
    UNUSED_PARAM (u4AdvRouterId);

    pOspfDbInfo = &(pOspfDb->LsaInfo[0]);

    for (u4Index = 0; u4Index < pOspfDb->u4NoOfLsa; u4Index++, pOspfDbInfo++)
    {
        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfDbInfo->u4LsaLsId);
        CliPrintf (CliHandle, "\r\nLSA ID: %s\r\n", pu1String);

        switch (pOspfDbInfo->i4LsaType)
        {
            case ROUTER_LSA:
                CliPrintf (CliHandle, "LSA Type :    Router LSA\r\n");
                break;
            case NETWORK_LSA:
                CliPrintf (CliHandle, "LSA Type :    Network LSA\r\n");
                break;
            case NETWORK_SUM_LSA:
                CliPrintf (CliHandle, "LSA Type :    Summary LSA\r\n");
                break;
            case ASBR_SUM_LSA:
                CliPrintf (CliHandle, "LSA Type :    ASBR Summary\r\n");
                break;
            case TYPE9_OPQ_LSA:
                CliPrintf (CliHandle, "LSA Type :    Opaque Link Area\r\n");
                break;
            case TYPE10_OPQ_LSA:
                CliPrintf (CliHandle,
                           "LSA Type :    Opaque Area Link States\r\n");
                break;
            case TYPE11_OPQ_LSA:
                CliPrintf (CliHandle,
                           "LSA Type :    Opaque AS Link States\r\n");
                break;
            default:
                CliPrintf (CliHandle, "\r\n");
                break;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfDbInfo->u4LsaAreaId);
        CliPrintf (CliHandle, "AreaID : %s\r\n", pu1String);

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfDbInfo->u4LsaRtrId);
        CliPrintf (CliHandle, "Router Id : %s\r\n", pu1String);

        CliPrintf (CliHandle, "Age : %d\r\n", pOspfDbInfo->u4LsaAge);

        CliPrintf (CliHandle, "Sequence : %x\r\n", pOspfDbInfo->i4LsaSeq);

        i4PageStatus = CliPrintf
            (CliHandle, "Checksum : %x\r\n", pOspfDbInfo->i4LsaCksum);

        if (i4PageStatus == CLI_FAILURE)
            break;
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowLsdbDatabase                                */
/*                                                                            */
/* Description       : This Routine will fetch LSA Database information       */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pu1Buffer    - Buffer to dumpt the LSA information     */
/*                     u4BufLen     - Buffer Length                           */
/*                     pCookie      - Cookie for next index                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowLsdbDatabase (tCliHandle CliHandle, UINT1 *pu1Buffer,
                         UINT4 u4BufLen, tShowOspfDbCookie * pCookie)
{
    tShowOspfDb        *pShowOspfDb;
    tShowOspfDbInfo    *pShowOspfDbInfo;
    UINT4               u4OspfLsdbAreaId = 0;
    UINT4               u4OspfLsdbLsId = 0;
    UINT4               u4OspfLsdbRouterId = 0;
    UINT4               u4PrevOspfLsdbAreaId = 0;
    UINT4               u4PrevOspfLsdbLsId = 0;
    UINT4               u4PrevOspfLsdbRouterId = 0;
    UINT4               u4LsaExist = OSPF_FALSE;
    UINT4               u4NoOfLsa = 0;
    INT4                i4RetValOspfLsdbAge = 0;
    INT4                i4OspfLsdbType = 0;
    INT4                i4PrevOspfLsdbType = 0;
    INT4                i4RetValOspfLsdbSequence = 0;
    INT4                i4RetValOspfLsdbChecksum = 0;

    if (pCookie->i4OspfLsdbType)
    {

        u4OspfLsdbAreaId = pCookie->u4OspfLsdbAreaId;
        i4OspfLsdbType = pCookie->i4OspfLsdbType;
        u4OspfLsdbLsId = pCookie->u4OspfLsdbLsId;
        u4OspfLsdbRouterId = pCookie->u4OspfLsdbRouterId;
        u4LsaExist = OSPF_TRUE;
    }
    else
    {
        if (nmhGetFirstIndexOspfLsdbTable (&u4OspfLsdbAreaId, &i4OspfLsdbType,
                                           &u4OspfLsdbLsId,
                                           &u4OspfLsdbRouterId) != SNMP_FAILURE)
        {
            u4LsaExist = OSPF_TRUE;
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);
    pShowOspfDb = (tShowOspfDb *) (VOID *) pu1Buffer;
    pShowOspfDb->u4NoOfLsa = 0;
    pShowOspfDbInfo = &(pShowOspfDb->LsaInfo[0]);

    if (u4LsaExist == OSPF_TRUE)
    {
        u4NoOfLsa = (u4BufLen -
                     ((sizeof (tShowOspfDb)) - (sizeof (tShowOspfDbInfo)))) /
            sizeof (tShowOspfDbInfo);

        do
        {
            if (!u4NoOfLsa)
            {
                /* we have run out of buffer; but we have more LSAs in the
                 * database to be returned. Hence set the cookie in the return
                 * buffer */

                pCookie->u4OspfLsdbAreaId = u4OspfLsdbAreaId;
                pCookie->i4OspfLsdbType = i4OspfLsdbType;
                pCookie->u4OspfLsdbLsId = u4OspfLsdbLsId;
                pCookie->u4OspfLsdbRouterId = u4OspfLsdbRouterId;
                return CLI_SUCCESS;
            }

            if (nmhGetOspfLsdbAge (u4OspfLsdbAreaId,
                                   i4OspfLsdbType, u4OspfLsdbLsId,
                                   u4OspfLsdbRouterId, &i4RetValOspfLsdbAge)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetOspfLsdbSequence (u4OspfLsdbAreaId,
                                        i4OspfLsdbType, u4OspfLsdbLsId,
                                        u4OspfLsdbRouterId,
                                        &i4RetValOspfLsdbSequence) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetOspfLsdbChecksum (u4OspfLsdbAreaId,
                                        i4OspfLsdbType, u4OspfLsdbLsId,
                                        u4OspfLsdbRouterId,
                                        &i4RetValOspfLsdbChecksum) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            pShowOspfDbInfo->u4LsaAreaId = u4OspfLsdbAreaId;
            pShowOspfDbInfo->u4LsaLsId = u4OspfLsdbLsId;
            pShowOspfDbInfo->u4LsaRtrId = u4OspfLsdbRouterId;
            pShowOspfDbInfo->i4LsaType = i4OspfLsdbType;
            pShowOspfDbInfo->u4LsaAge = (UINT4) i4RetValOspfLsdbAge;
            pShowOspfDbInfo->i4LsaSeq = i4RetValOspfLsdbSequence;
            pShowOspfDbInfo->i4LsaCksum = i4RetValOspfLsdbChecksum;

            /* Increment the stored LSA count and decrement the max possible
             *  LSA count */

            pShowOspfDb->u4NoOfLsa++;
            u4NoOfLsa--;

            if (u4NoOfLsa)
            {
                /* Increment the pShowOspfDbInfo pointer to store the next
                 *  Lsa information. */
                pShowOspfDbInfo++;
            }

            u4PrevOspfLsdbAreaId = u4OspfLsdbAreaId;
            u4PrevOspfLsdbRouterId = u4OspfLsdbRouterId;
            u4PrevOspfLsdbLsId = u4OspfLsdbLsId;
            i4PrevOspfLsdbType = i4OspfLsdbType;

        }

        while (nmhGetNextIndexOspfLsdbTable (u4PrevOspfLsdbAreaId,
                                             &u4OspfLsdbAreaId,
                                             i4PrevOspfLsdbType,
                                             &i4OspfLsdbType,
                                             u4PrevOspfLsdbLsId,
                                             &u4OspfLsdbLsId,
                                             u4PrevOspfLsdbRouterId,
                                             &u4OspfLsdbRouterId)
               != SNMP_FAILURE);
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliShowSummaryAddressInCxt                         */
/*                                                                            */
/* Description       : This Routine will show Summary address information     */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId  - ospf context id                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowSummaryAddressInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tShowOspfSACookie   OspfSACookie;
    tShowOspfSA        *pShowOspfSA;
    UINT1               au1SAInfo[MAX_OSPF_SA_INFO];
    UINT1              *pu1SAInfo = NULL;
    UINT4               u4BufSize = 0;
    INT4                i4RetVal = 0;
    INT4                i4HdrFlag = OSPF_TRUE;
    UINT4               u4ShowAllCxt = OSPF_FALSE;

    pu1SAInfo = au1SAInfo;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }

    /* Initialize the cookie */
    MEMSET (&OspfSACookie, 0, sizeof (tShowOspfSACookie));

    OspfSACookie.u4OspfCxtId = u4OspfCxtId;
    /* Allocate memory to hold the Lsa information. */
    u4BufSize = ((sizeof (tShowOspfSA)) +
                 ((OSPF_CLI_MAX_SA - 1) * sizeof (tShowOspfSAInfo)));

    CliPrintf (CliHandle, "\r\n");

    while (i4RetVal == CLI_SUCCESS)
    {
        MEMSET (pu1SAInfo, 0, u4BufSize);

        i4RetVal = OspfCliShowSAInCxt (CliHandle, pu1SAInfo,
                                       u4BufSize, &OspfSACookie, u4ShowAllCxt);
        pShowOspfSA = (tShowOspfSA *) (VOID *) pu1SAInfo;

        if (pShowOspfSA->u4NoOfSA)
        {
            if (OspfCliDisplaySA (CliHandle, pShowOspfSA, i4HdrFlag) ==
                CLI_FAILURE)
                break;

            if (i4HdrFlag == OSPF_TRUE)
                i4HdrFlag = OSPF_FALSE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDisplaySA                                       */
/*                                                                            */
/* Description       : This Routine will print Summary address information    */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     pOspfSA      - OSPF Summary address information        */
/*                     i4HdrFlag    - Flag to specify printing of Header      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDisplaySA (tCliHandle CliHandle, tShowOspfSA * pOspfSA, INT4 i4HdrFlag)
{
    tShowOspfSAInfo    *pOspfSAInfo;
    UINT4               u4Index;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    pOspfSAInfo = &(pOspfSA->SAInfo[0]);
    u4OspfPrevCxtId = pOspfSAInfo->u4OspfCxtId;

    if (i4HdrFlag == OSPF_TRUE)
    {
        if ((UtilOspfGetVcmAliasName (pOspfSAInfo->u4OspfCxtId, au1OspfCxtName))
            == OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, " Vrf  %s ,Summary Address\r\n", au1OspfCxtName);

        CliPrintf (CliHandle,
                   "\r----------------------------------------------\r\n");

        CliPrintf (CliHandle, "%-15s %-15s %-7s %-15s %-15s %-10s\r\n",
                   "Network", "Mask", "LSAType", "Area", "Effect", "Tag");
        CliPrintf (CliHandle, "%-15s %-15s %-7s %-15s %-15s %-10s\r\n",
                   "-------", "----", "-------", "----", "------", "---");
    }

    for (u4Index = 0; u4Index < pOspfSA->u4NoOfSA; u4Index++, pOspfSAInfo++)
    {
        if (pOspfSAInfo->u4OspfCxtId != u4OspfPrevCxtId)
        {

            if ((UtilOspfGetVcmAliasName
                 (pOspfSAInfo->u4OspfCxtId, au1OspfCxtName)) == OSPF_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, " Vrf  %s ,Summary Address\r\n",
                       au1OspfCxtName);
            CliPrintf (CliHandle,
                       "\r----------------------------------------------\r\n");
            CliPrintf (CliHandle, "%-15s %-15s %-7s %-15s %-15s %-10s\r\n",
                       "Network", "Mask", "LSAType", "Area", "Effect", "Tag");
            CliPrintf (CliHandle, "%-15s %-15s %-7s %-15s %-15s %-10s\r\n",
                       "-------", "----", "-------", "----", "------", "---");

        }
        u4OspfPrevCxtId = pOspfSAInfo->u4OspfCxtId;

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfSAInfo->u4OspfSANet);
        CliPrintf (CliHandle, "%-15s", pu1String);

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfSAInfo->u4OspfSAMask);
        CliPrintf (CliHandle, " %-15s", pu1String);

        switch (pOspfSAInfo->i4OspfSALsaType)
        {
            case NETWORK_SUM_LSA:
                CliPrintf (CliHandle, " %-7s", "Summary");
                break;
            case NSSA_LSA:
                CliPrintf (CliHandle, " %-7s", "Type7");
                break;
            default:
                CliPrintf (CliHandle, " %-7s", "       ");
                break;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfSAInfo->u4OspfSAArea);
        CliPrintf (CliHandle, " %-15s", pu1String);

        switch (pOspfSAInfo->i4OspfSAEffect)
        {
            case ADVERTISE_MATCHING:
                CliPrintf (CliHandle, " %-15s", "Advertise");
                break;
            case DO_NOT_ADVERTISE_MATCHING:
                CliPrintf (CliHandle, " %-15s", "DoNotAdvertise");
                break;
            default:
                CliPrintf (CliHandle, " %-15s", "               ");
                break;
        }

        i4PageStatus =
            CliPrintf (CliHandle, " %-10d\r\n", pOspfSAInfo->i4OspfSATag);

        if (i4PageStatus == CLI_FAILURE)
            break;
    }
    CliPrintf (CliHandle, "\r\n");
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowSAInCxt                                     */
/*                                                                            */
/* Description       : This Routine will fetch Summary address information    */
/*                                                                            */
/* Input Parameters  : CliHandle - CliContext ID                              */
/*                     pu1Buffer - Buffer to dump Summary address information */
/*                     u4BufLen  - Buffer Length                              */
/*                     pCookie   - Cookie for next index                      */
/*                     u4ShowAllCxt - flag to display all context             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowSAInCxt (tCliHandle CliHandle, UINT1 *pu1Buffer,
                    UINT4 u4BufLen, tShowOspfSACookie * pCookie,
                    UINT4 u4ShowAllCxt)
{
    tShowOspfSA        *pShowOspfSA;
    tShowOspfSAInfo    *pShowOspfSAInfo;
    UINT4               u4OspfSAAreaId = 0;
    UINT4               u4OspfSANet = 0;
    UINT4               u4OspfSAMask = 0;
    UINT4               u4PrevOspfSAAreaId = 0;
    UINT4               u4PrevOspfSANet = 0;
    UINT4               u4PrevOspfSAMask = 0;
    INT4                i4OspfSALsaType = 0;
    INT4                i4PrevOspfSALsaType = 0;
    INT4                i4RetValOspfSAEffect = 0;
    INT4                i4RetValOspfSATag = 0;
    UINT4               u4NoOfSA = 0;
    UINT4               u4SAExist = OSPF_FALSE;
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4OspfCxtId;
    INT4                i4RetVal;
    INT4                i4AdminStat;

    u4OspfCxtId = pCookie->u4OspfCxtId;

    if (pCookie->u4OspfSANet)
    {
        u4OspfSANet = pCookie->u4OspfSANet;
        u4OspfSAMask = pCookie->u4OspfSAMask;
        u4OspfSAAreaId = pCookie->u4OspfSAAreaId;
        i4OspfSALsaType = pCookie->i4OspfSALsdbType;
        u4SAExist = OSPF_TRUE;
    }
    else
    {
        if (u4ShowAllCxt == OSPF_TRUE)
        {
            i4RetVal =
                nmhGetFirstIndexFsMIStdOspfAreaAggregateTable ((INT4 *)
                                                               &u4OspfCxtId,
                                                               &u4OspfSAAreaId,
                                                               &i4OspfSALsaType,
                                                               &u4OspfSANet,
                                                               &u4OspfSAMask);
        }
        else
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevOspfSANet = 0;
            u4PrevOspfSAMask = 0;
            u4PrevOspfSAAreaId = 0;
            i4PrevOspfSALsaType = 0;

            i4RetVal =
                nmhGetNextIndexFsMIStdOspfAreaAggregateTable ((INT4)
                                                              u4OspfPrevCxtId,
                                                              (INT4 *)
                                                              &u4OspfCxtId,
                                                              u4PrevOspfSAAreaId,
                                                              &u4OspfSAAreaId,
                                                              i4PrevOspfSALsaType,
                                                              &i4OspfSALsaType,
                                                              u4PrevOspfSANet,
                                                              &u4OspfSANet,
                                                              u4PrevOspfSAMask,
                                                              &u4OspfSAMask);

            if ((u4OspfPrevCxtId != u4OspfCxtId) &&
                (u4ShowAllCxt == OSPF_FALSE))
            {
                /* GetNext routine returned next context info and
                 * u4ShowAllCxt is set to false */
                i4RetVal = SNMP_FAILURE;
            }
        }

        if (i4RetVal != SNMP_FAILURE)
        {
            u4SAExist = OSPF_TRUE;
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);
    pShowOspfSA = (tShowOspfSA *) (VOID *) pu1Buffer;
    pShowOspfSA->u4NoOfSA = 0;
    pShowOspfSAInfo = &(pShowOspfSA->SAInfo[0]);

    if (u4SAExist == OSPF_TRUE)
    {
        u4NoOfSA = (u4BufLen - ((sizeof (tShowOspfSA)) -
                                (sizeof (tShowOspfSAInfo)))) /
            sizeof (tShowOspfSAInfo);

        do
        {
            if (u4ShowAllCxt == OSPF_FALSE && u4OspfPrevCxtId != u4OspfCxtId)
            {
                /* Summary address of the specified context id is displayed */
                break;
            }

            /* prepare to get the next indices */

            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevOspfSANet = u4OspfSANet;
            u4PrevOspfSAMask = u4OspfSAMask;
            u4PrevOspfSAAreaId = u4OspfSAAreaId;
            i4PrevOspfSALsaType = i4OspfSALsaType;

            if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
                 == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
            {
                if (u4ShowAllCxt == OSPF_FALSE)
                {
                    /* Invalid context id or admin status of the 
                     * context is disabled */
                    break;
                }

                continue;
            }

            if (!u4NoOfSA)
            {
                /* we have run out of buffer; but we have more SA in the
                 *  database to be returned. Hence set the cookie
                 *  in the return buffer */
                pCookie->u4OspfCxtId = u4OspfCxtId;
                pCookie->u4OspfSANet = u4OspfSANet;
                pCookie->u4OspfSAMask = u4OspfSAMask;
                pCookie->u4OspfSAAreaId = u4OspfSAAreaId;
                pCookie->i4OspfSALsdbType = i4OspfSALsaType;
                return CLI_SUCCESS;

            }

            if (nmhGetFsMIStdOspfAreaAggregateEffect (u4OspfCxtId,
                                                      u4OspfSAAreaId,
                                                      i4OspfSALsaType,
                                                      u4OspfSANet, u4OspfSAMask,
                                                      &i4RetValOspfSAEffect) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetFsMIOspfAreaAggregateExternalTag (u4OspfCxtId,
                                                        u4OspfSAAreaId,
                                                        i4OspfSALsaType,
                                                        u4OspfSANet,
                                                        u4OspfSAMask,
                                                        &i4RetValOspfSATag) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            pShowOspfSAInfo->u4OspfCxtId = u4OspfCxtId;
            pShowOspfSAInfo->u4OspfSANet = u4OspfSANet;
            pShowOspfSAInfo->u4OspfSAMask = u4OspfSAMask;
            pShowOspfSAInfo->u4OspfSAArea = u4OspfSAAreaId;
            pShowOspfSAInfo->i4OspfSALsaType = i4OspfSALsaType;
            pShowOspfSAInfo->i4OspfSAEffect = i4RetValOspfSAEffect;
            pShowOspfSAInfo->i4OspfSATag = i4RetValOspfSATag;

            pShowOspfSA->u4NoOfSA++;
            u4NoOfSA--;

            if (u4NoOfSA)
            {
                /* Increment the pShowOspfSAInfo pointer to store the next
                 * SA information. */
                pShowOspfSAInfo++;
            }

        }
        while (nmhGetNextIndexFsMIStdOspfAreaAggregateTable
               ((INT4) u4OspfPrevCxtId, (INT4 *) &u4OspfCxtId,
                u4PrevOspfSAAreaId, &u4OspfSAAreaId, i4PrevOspfSALsaType,
                &i4OspfSALsaType, u4PrevOspfSANet, &u4OspfSANet,
                u4PrevOspfSAMask, &u4OspfSAMask) != SNMP_FAILURE);
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliSetSummary                                      */
/*                                                                            */
/* Description       : This Routine will set Summary Address                  */
/*                     (ospfAreaAggregateTable)                               */
/*                                                                            */
/* Input Parameters  : CliHandle       - CliContext ID                        */
/*                     u4SummNet       - Summary address network              */
/*                     u4SummMask      - Summary address mask                 */
/*                     u4SummArea      - Summary area address                 */
/*                     i4SummEffect    - Summary Effect                       */
/*                     i4SummLsaType   - Summary Type                         */
/*                     i4SummTag       - Summary TAG                          */
/*                     i4SetSummaryTag - Summary TAG flag                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetSummary (tCliHandle CliHandle, UINT4 u4SummNet,
                   UINT4 u4SummMask, UINT4 u4SummArea,
                   INT4 i4SummEffect, INT4 i4SummLsaType,
                   INT4 i4SummTag, INT4 i4SetSummaryTag)
{
    INT4                i4StatusVal = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetOspfAreaAggregateStatus (u4SummArea, i4SummLsaType, u4SummNet,
                                       u4SummMask,
                                       &i4StatusVal) == SNMP_FAILURE)
    {
        if (nmhTestv2OspfAreaAggregateStatus (&u4ErrorCode, u4SummArea,
                                              i4SummLsaType, u4SummNet,
                                              u4SummMask, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfAreaAggregateStatus (u4SummArea, i4SummLsaType, u4SummNet,
                                           u4SummMask, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2OspfAreaAggregateEffect (&u4ErrorCode, u4SummArea,
                                          i4SummLsaType, u4SummNet, u4SummMask,
                                          i4SummEffect) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfAreaAggregateEffect (u4SummArea, i4SummLsaType,
                                       u4SummNet, u4SummMask,
                                       i4SummEffect) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4SetSummaryTag == OSPF_TRUE)
    {
        if (nmhTestv2FutOspfAreaAggregateExternalTag (&u4ErrorCode, u4SummArea,
                                                      i4SummLsaType, u4SummNet,
                                                      u4SummMask, i4SummTag)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfAreaAggregateExternalTag (u4SummArea,
                                                   i4SummLsaType, u4SummNet,
                                                   u4SummMask, i4SummTag)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2OspfAreaAggregateStatus (&u4ErrorCode, u4SummArea,
                                          i4SummLsaType, u4SummNet,
                                          u4SummMask, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfAreaAggregateStatus (u4SummArea, i4SummLsaType,
                                       u4SummNet, u4SummMask,
                                       ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliFilterLsaInfo                                   */
/*                                                                            */
/* Description       : This routine will Filter the LSA Info                  */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pLsaInfo      - LSA Information                        */
/*                     i4Option      - Filter option value as given below     */
/*                                                                            */
/*                     LS-TYPE                     -- 1                       */
/*                     LS-TYPE SELF-ORG            -- 2                       */
/*                     LS-TYPE ADV-ROUTER          -- 3                       */
/*                     LS-TYPE LS-ID               -- 4                       */
/*                     LS-TYPE LS-ID SELF-ORG      -- 5                       */
/*                     LS-TYPE LS-ID ADV-ROUTER    -- 6                       */
/*                     ALL LSA's                   -- 7                       */
/*                     SELF-ORG                    -- 8                       */
/*                     ADV-ROUTER                  -- 9                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliFilterLsaInfo (tCliHandle CliHandle, tLsaInfo * pLsaInfo, INT4 i4Option)
{
    INT4                i4PageStatus = CLI_SUCCESS;

    switch (i4Option)
    {
        case 1:
            if (pLsaInfo->lsaId.u1LsaType == LS_TYPE)
            {
                i4PageStatus = OspfCliPrintLsaInfo (CliHandle, pLsaInfo);
            }
            break;
        case 2:
            if ((pLsaInfo->lsaId.u1LsaType == LS_TYPE) &&
                (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, LS_ROUTER_ID) ==
                 OSPF_EQUAL))
            {
                i4PageStatus = OspfCliPrintLsaInfo (CliHandle, pLsaInfo);
            }
            break;
        case 3:
            if ((pLsaInfo->lsaId.u1LsaType == LS_TYPE) &&
                (UtilIpAddrComp
                 (pLsaInfo->lsaId.advRtrId, LS_ADV_ROUTER_ID) == OSPF_EQUAL))
            {
                i4PageStatus = OspfCliPrintLsaInfo (CliHandle, pLsaInfo);
            }
            break;
        case 4:
            if ((pLsaInfo->lsaId.u1LsaType == LS_TYPE) &&
                (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId, LS_ID) ==
                 OSPF_EQUAL))
            {
                i4PageStatus = OspfCliPrintLsaInfo (CliHandle, pLsaInfo);
            }
            break;
        case 5:
            if ((pLsaInfo->lsaId.u1LsaType == LS_TYPE) &&
                (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId, LS_ID) ==
                 OSPF_EQUAL)
                && (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, LS_ROUTER_ID) ==
                    OSPF_EQUAL))
            {
                i4PageStatus = OspfCliPrintLsaInfo (CliHandle, pLsaInfo);
            }
            break;
        case 6:
            if ((pLsaInfo->lsaId.u1LsaType == LS_TYPE) &&
                (UtilIpAddrComp (pLsaInfo->lsaId.linkStateId, LS_ID) ==
                 OSPF_EQUAL)
                && (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, LS_ADV_ROUTER_ID)
                    == OSPF_EQUAL))
            {
                i4PageStatus = OspfCliPrintLsaInfo (CliHandle, pLsaInfo);
            }
            break;
        case 7:
            /*Type 5 LSA's are displayed here for option 7 */
            if (pLsaInfo->lsaId.u1LsaType == AS_EXT_LSA)
            {
                if (u1HeaderFlag == OSIX_TRUE)
                {
                    OspfCliPrintLsaSummaryHeader (CliHandle, pLsaInfo,
                                                  pLsaInfo->lsaId.u1LsaType);
                    u1HeaderFlag = OSIX_FALSE;
                }
                i4PageStatus = OspfCliPrintLsaSummary (CliHandle, pLsaInfo);
            }
            break;
        default:
            break;
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliPrintLsaInfo                                    */
/*                                                                            */
/* Description       : This Routine will Print the LSA Info                   */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pLsaInfo      - LSA Information                        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliPrintLsaInfo (tCliHandle CliHandle, tLsaInfo * pLsaInfo)
{
    CHR1               *pu1String = NULL;
    UINT1               u1LsType = 0;
    UINT1               au1LsaType[50];
    UINT1              *pu1LsaData;
    UINT1               u1MetricType;
    UINT1               u1LinkType;
    UINT1               u1NoTosMetric;
    UINT1               u1TlvVal1 = OSPF_ZERO;
    UINT1               au1EncType[11][15] = { {"Packet"}, {"Ethernet"},
    {"ANSI/ETSI PDH"}, {"Reserved"},
    {"SONET"}, {"Reserved"},
    {"Digital Wrapper"},
    {"Lambda"}, {"Fiber"},
    {"Reserved"}, {"Fiber Channel"}
    };
    UINT1               au1ProcType[6][15] = { {"extraTraffic"},
    {"unprotected"},
    {"shared"}, {"dedicated1For1"},
    {"dedicated1Plus1"},
    {"enhanced"}
    };
    UINT2               u2LinkCount;
    UINT2               u2TosMetric;
    UINT2               u2LsType = OSPF_ZERO;
    UINT2               u2EntLen = OSPF_ZERO;
    UINT2               u2Priority = OSPF_ZERO;
    UINT2               u2TlvType = OSPF_ZERO;
    UINT2               u2TlvLen = OSPF_ZERO;
    UINT2               u2Count = OSPF_ZERO;
    UINT2               u2TlvVal = OSPF_ZERO;
    UINT4               u4Checksum = 0;
    UINT4               u4NetMask;
    UINT4               u4Metric;
    UINT4               u4ForwardAddr;
    UINT4               u4Router;
    UINT4               u4ExtRouteTag;
    UINT4               u4DesigRouter;
    UINT4               u4RouterIntf;
    UINT4               u4TlvVal = OSPF_ZERO;
    UINT4               u4Count = OSPF_ZERO;
    INT4                i4Length = 0;
    INT4                i4RouterCount = 0;
    INT4                i4Count = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    INT4                i4LsAge = 0;
    INT4                i4LsSeqNum = 0;
    FLT4                bandWidth;
    tLsHeader           lsHeader;
    UINT1               u1Tos = TOS_0;

    MEMSET (au1LsaType, 0, 30);

    i4LsAge = pLsaInfo->u2LsaAge;
    u1LsType = pLsaInfo->lsaId.u1LsaType;
    i4LsSeqNum = pLsaInfo->lsaSeqNum;
    u4Checksum = pLsaInfo->u2LsaChksum;
    i4Length = pLsaInfo->u2LsaLen;

    if (pLsaInfo->pArea != NULL)
    {
        OSPF_CLI_IPADDR_TO_STR (pu1String,
                                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->
                                                        areaId));
    }

    switch (u1LsType)
    {
        case ROUTER_LSA:
            STRCPY (au1LsaType, "Router Links");
            CliPrintf (CliHandle, "\r                  Router Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "   ------------------"
                       "--------------------\r\n");
            break;
        case NETWORK_LSA:
            STRCPY (au1LsaType, "Network Links");
            CliPrintf (CliHandle, "\r                  Network Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  -------------------"
                       "--------------------\r\n");
            break;
        case NETWORK_SUM_LSA:
            STRCPY (au1LsaType, "Summary Links(Network)");
            CliPrintf (CliHandle, "\r                  Summary Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  -------------------"
                       "--------------------\r\n");
            break;
        case ASBR_SUM_LSA:
            STRCPY (au1LsaType, "Summary Links(AS Boundary Router)");
            CliPrintf (CliHandle,
                       "\r                  ASBR Summary Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  ------------------------"
                       "--------------------\r\n");
            break;
        case AS_EXT_LSA:
            STRCPY (au1LsaType, "AS External Link");
            CliPrintf (CliHandle,
                       "\r                  AS External Link States \r\n");
            CliPrintf (CliHandle,
                       "                  -----------------------\r\n");
            break;
        case NSSA_LSA:
            STRCPY (au1LsaType, "NSSA External Link");
            CliPrintf (CliHandle,
                       "\r                  NSSA External Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "   -------------------------"
                       "--------------------\r\n");
            break;
        case TYPE9_OPQ_LSA:
            STRCPY (au1LsaType, "Opaque Link Area Links");
            CliPrintf (CliHandle,
                       "\r               Opaque Link Area Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "   ----------------------------"
                       "--------------------\r\n");
            break;
        case TYPE10_OPQ_LSA:
            STRCPY (au1LsaType, "Opaque Area Links");
            CliPrintf (CliHandle,
                       "\r                  Opaque Area Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "   ----------------------------"
                       "--------------------\r\n");
            break;
        case TYPE11_OPQ_LSA:
            STRCPY (au1LsaType, "Opaque AS Links");
            CliPrintf (CliHandle,
                       "\r                  Opaque AS Link States\r\n");
            CliPrintf (CliHandle,
                       "                  ---------------------\r\n");
            break;
        default:
            STRCPY (au1LsaType, " ");
            CliPrintf (CliHandle,
                       "\r                                 Link States \r\n");
            CliPrintf (CliHandle,
                       "                                 -----------\r\n");
            break;
    }

    CliPrintf (CliHandle, "LS age             : %d\r\n", i4LsAge);
    CliPrintf (CliHandle, "Options            : (No ToS Capability, ");
    if (!IS_DC_BIT_SET_LSA (pLsaInfo))
    {
        CliPrintf (CliHandle, "DC Bit Not Set)\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "DC)\r\n");
    }
    CliPrintf (CliHandle, "LS Type            : %s\r\n", au1LsaType);

    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.
                                                    linkStateId));
    CliPrintf (CliHandle, "Link State ID      : %s\r\n", pu1String);
    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    CliPrintf (CliHandle, "Advertising Router : %s\r\n", pu1String);
    CliPrintf (CliHandle, "LS Seq Number      : 0x%x\r\n", i4LsSeqNum);
    CliPrintf (CliHandle, "Checksum           : 0x%x\r\n", u4Checksum);
    i4PageStatus = CliPrintf (CliHandle,
                              "Length             : %d\r\n", i4Length);

    switch (u1LsType)
    {
        case ROUTER_LSA:

            pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
            pu1LsaData += sizeof (UINT2);    /*Skip the flag field */
            MEMCPY (&u2LinkCount, pu1LsaData, sizeof (UINT2));
            u2LinkCount = OSIX_NTOHS (u2LinkCount);
            CliPrintf (CliHandle, "Number of links    : %d\r\n", u2LinkCount);
            while (u2LinkCount != 0)
            {
                pu1LsaData += sizeof (UINT2);
                MEMCPY (&u4DesigRouter, pu1LsaData, sizeof (UINT4));
                u4DesigRouter = OSIX_NTOHL (u4DesigRouter);
                pu1LsaData += sizeof (UINT4);
                MEMCPY (&u4RouterIntf, pu1LsaData, sizeof (UINT4));
                u4RouterIntf = OSIX_NTOHL (u4RouterIntf);
                pu1LsaData += sizeof (UINT4);
                u1LinkType = *pu1LsaData;
                pu1LsaData += sizeof (UINT1);
                u1NoTosMetric = *pu1LsaData;
                pu1LsaData += sizeof (UINT1);
                MEMCPY (&u2TosMetric, pu1LsaData, sizeof (UINT2));
                u2TosMetric = OSIX_NTOHS (u2TosMetric);
                if (u1LinkType == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nLink connected to: another Router (point-to-point)\r\n");
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4DesigRouter);
                    CliPrintf (CliHandle,
                               "(Link ID) Neighboring Router ID: %s\r\n",
                               pu1String);
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4RouterIntf);
                    CliPrintf (CliHandle,
                               "(Link Data) Router Interface address: %s\r\n",
                               pu1String);
                }
                if (u1LinkType == 2)
                {
                    CliPrintf (CliHandle,
                               "\r\nLink connected to: a Transit Network\r\n");
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4DesigRouter);
                    CliPrintf (CliHandle,
                               "(Link ID) Designated Router address: %s\r\n",
                               pu1String);
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4RouterIntf);
                    CliPrintf (CliHandle,
                               "(Link Data) Router Interface address: %s\r\n",
                               pu1String);
                }
                if (u1LinkType == 3)
                {
                    CliPrintf (CliHandle,
                               "\r\nLink connected to: a Stub Network\r\n");
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4DesigRouter);
                    CliPrintf (CliHandle,
                               "(Link ID) Network/subnet number: %s\r\n",
                               pu1String);
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4RouterIntf);
                    CliPrintf (CliHandle, "(Link Data) Network Mask: %s\r\n",
                               pu1String);
                }
                if (u1LinkType == 4)
                {
                    CliPrintf (CliHandle,
                               "\r\nLink connected to: a Virtual Link\r\n");
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4DesigRouter);
                    CliPrintf (CliHandle,
                               "(Link ID) Neighboring Router ID: %s\r\n",
                               pu1String);
                    OSPF_CLI_IPADDR_TO_STR (pu1String, u4RouterIntf);
                    CliPrintf (CliHandle,
                               "(Link Data) Router Interface address: %s\r\n",
                               pu1String);
                }
                CliPrintf (CliHandle, "Number of TOS metrics: %d\r\n",
                           u1NoTosMetric);
                CliPrintf (CliHandle, "TOS 0 Metrics: %d\r\n", u2TosMetric);
                CliPrintf (CliHandle, "\r\n");
                u2LinkCount--;
            }
            break;

        case NETWORK_LSA:
            pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
            MEMCPY (&u4NetMask, pu1LsaData, sizeof (UINT4));
            u4NetMask = OSIX_NTOHL (u4NetMask);

            OSPF_CLI_IPADDR_TO_STR (pu1String, u4NetMask);
            CliPrintf (CliHandle, "Network Mask       : %s\r\n", pu1String);

            /*Number of routers attached with this network can be calculated
             * as: (Length of LSA - size of lsaheader - 4bytes of netmask)/
             *      (size of ip address)*/
            i4RouterCount =
                (i4Length - sizeof (tLsHeader) -
                 sizeof (u4NetMask)) / sizeof (UINT4);

            for (i4Count = 0; i4Count < i4RouterCount; i4Count++)
            {
                pu1LsaData += sizeof (UINT4);
                MEMCPY (&u4Router, pu1LsaData, sizeof (UINT4));
                u4Router = OSIX_NTOHL (u4Router);
                OSPF_CLI_IPADDR_TO_STR (pu1String, u4Router);
                CliPrintf (CliHandle, "Attached Router    : %s\r\n", pu1String);
            }
            break;
        case NETWORK_SUM_LSA:
            UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);
            if (lsHeader.lsaOptions & T_BIT_MASK)
            {
                u1Tos = T_BIT_MASK;
            }
            pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
            MEMCPY (&u4NetMask, pu1LsaData, sizeof (UINT4));
            u4NetMask = OSIX_NTOHL (u4NetMask);
            pu1LsaData += sizeof (UINT4);
            pu1LsaData += sizeof (UINT1);
            MEMCPY (&u4Metric, pu1LsaData, sizeof (UINT4));
            u4Metric = OSIX_NTOHL (u4Metric);
            u4Metric = (u4Metric >> 8) & 0x00ffffff;
            pu1LsaData += (3 * sizeof (UINT1));
            OSPF_CLI_IPADDR_TO_STR (pu1String, u4NetMask);
            CliPrintf (CliHandle, "Network Mask       : %s\r\n", pu1String);
            CliPrintf (CliHandle, "   Metric            : %d\r\n", u4Metric);
            CliPrintf (CliHandle, "   TOS               : %d \n", u1Tos);
            break;

        case AS_EXT_LSA:
            pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
            MEMCPY (&u4NetMask, pu1LsaData, sizeof (UINT4));
            u4NetMask = OSIX_NTOHL (u4NetMask);
            pu1LsaData += sizeof (UINT4);
            u1MetricType = *pu1LsaData;
            pu1LsaData += sizeof (UINT1);

            /*Actual size of Metric value is 3 bytes. We will copy for bytes 
             * and strip the fourth byte*/
            MEMCPY (&u4Metric, pu1LsaData, sizeof (UINT4));
            u4Metric = OSIX_NTOHL (u4Metric);
            u4Metric = (u4Metric >> 8) & 0x00ffffff;
            pu1LsaData += (3 * sizeof (UINT1));
            MEMCPY (&u4ForwardAddr, pu1LsaData, sizeof (UINT4));
            u4ForwardAddr = OSIX_NTOHL (u4ForwardAddr);
            pu1LsaData += sizeof (UINT4);
            MEMCPY (&u4ExtRouteTag, pu1LsaData, sizeof (UINT4));
            u4ExtRouteTag = OSIX_NTOHL (u4ExtRouteTag);
            OSPF_CLI_IPADDR_TO_STR (pu1String, u4NetMask);

            CliPrintf (CliHandle, "Network Mask       : %s\r\n", pu1String);
            if (u1MetricType == 0)
            {
                CliPrintf (CliHandle, "Metric Type        : AS-EXT-TYPE1\r\n",
                           u1MetricType);
            }
            else if (u1MetricType == 128)
            {
                CliPrintf (CliHandle, "Metric Type        : AS-EXT-TYPE2\r\n",
                           u1MetricType);
            }
            CliPrintf (CliHandle, "Metric             : %d\r\n", u4Metric);
            OSPF_CLI_IPADDR_TO_STR (pu1String, u4ForwardAddr);
            CliPrintf (CliHandle, "Forward Address    : %s\r\n", pu1String);
            i4PageStatus = CliPrintf (CliHandle,
                                      "External Route Tag : %u\r\n",
                                      u4ExtRouteTag);
            break;
        case NSSA_LSA:
            UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);
            if (lsHeader.lsaOptions & T_BIT_MASK)
            {
                u1Tos = T_BIT_MASK;
            }
            pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
            MEMCPY (&u4NetMask, pu1LsaData, sizeof (UINT4));
            u4NetMask = OSIX_NTOHL (u4NetMask);
            pu1LsaData += sizeof (UINT4);
            u1MetricType = *pu1LsaData;
            pu1LsaData += sizeof (UINT1);
            MEMCPY (&u4Metric, pu1LsaData, sizeof (UINT4));
            u4Metric = OSIX_NTOHL (u4Metric);
            u4Metric = (u4Metric >> 8) & 0x00ffffff;
            pu1LsaData += (3 * sizeof (UINT1));
            MEMCPY (&u4ForwardAddr, pu1LsaData, sizeof (UINT4));
            u4ForwardAddr = OSIX_NTOHL (u4ForwardAddr);
            pu1LsaData += sizeof (UINT4);
            MEMCPY (&u4ExtRouteTag, pu1LsaData, sizeof (UINT4));
            u4ExtRouteTag = OSIX_NTOHL (u4ExtRouteTag);
            OSPF_CLI_IPADDR_TO_STR (pu1String, u4NetMask);
            CliPrintf (CliHandle, "Network Mask       : %s\r\n", pu1String);
            if (u1MetricType == 0)
            {
                CliPrintf (CliHandle, "   Metric Type       : AS-EXT-TYPE1\r\n",
                           u1MetricType);
            }
            else if (u1MetricType == 128)
            {
                CliPrintf (CliHandle, "   Metric Type       : AS-EXT-TYPE2\r\n",
                           u1MetricType);
            }
            CliPrintf (CliHandle, "   Metric            : %d\r\n", u4Metric);
            OSPF_CLI_IPADDR_TO_STR (pu1String, u4ForwardAddr);
            CliPrintf (CliHandle, "   Forward Address   : %s\r\n", pu1String);
            i4PageStatus = CliPrintf (CliHandle,
                                      "   External Route Tag: %u\r\n",
                                      u4ExtRouteTag);
            CliPrintf (CliHandle, "   TOS               : %d \n", u1Tos);
            break;

        case TYPE9_OPQ_LSA:
            break;
        case TYPE10_OPQ_LSA:
            /* Skipping the Header */
            pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
            /* Getting the Ls-Type (one -> Router TLV; two -> Link TLV) */
            MEMCPY (&u2LsType, pu1LsaData, sizeof (UINT2));
            pu1LsaData += sizeof (UINT2);
            u2LsType = OSIX_NTOHS (u2LsType);
            if (u2LsType == OSPF_ONE)
            {
                break;
            }
            /* Copying the entire TLV Length */
            MEMCPY (&u2EntLen, pu1LsaData, sizeof (UINT2));
            u2EntLen = OSIX_NTOHS (u2EntLen);
            pu1LsaData += sizeof (UINT2);

            /* Now we have only opaque sub tlv's. Print it one by one */
            MEMCPY (&u2TlvType, pu1LsaData, sizeof (UINT2));
            u2TlvType = OSIX_NTOHS (u2TlvType);
            pu1LsaData += sizeof (UINT2);
            u4Count = OSPF_ZERO;
            do
            {
                switch (u2TlvType)
                {
                    case OSPF_TYPE10_LINK_TYPE:    /* Link Type */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        MEMCPY (&u1TlvVal1, pu1LsaData, u2TlvLen);
                        if (u1TlvVal1 == OSPF_TYPE10_LINK_P2P)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n   Link connected to "
                                       "Point-to-Point network\r\n");
                        }
                        else if (u1TlvVal1 == OSPF_TYPE10_LINK_MULTI)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n   Link connected to "
                                       "Multi-access network\r\n");
                        }
                        /* Move the pointer at the end of the TLV.
                         * Link Type is only a byte, but for 4 byte alignment 
                         * remaining bytes are filled with zero.
                         * so move upto there */
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_LINK_ID:    /* Link Id  */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        MEMCPY (&u4TlvVal, pu1LsaData, u2TlvLen);
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TlvVal);
                        CliPrintf (CliHandle,
                                   "     Link ID            : %s\r\n",
                                   pu1String);
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_LOCAL_IP:    /* Local Ip */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        for (u2Count = OSPF_ZERO; u2Count < u2TlvLen;
                             u2Count += sizeof (UINT4))
                        {
                            MEMCPY (&u4TlvVal, pu1LsaData, sizeof (UINT4));
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            u4TlvVal = OSIX_NTOHL (u4TlvVal);
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TlvVal);
                            CliPrintf (CliHandle,
                                       "     Interface Address  : %s\r\n",
                                       pu1String);
                        }
                        break;
                    case OSPF_TYPE10_REMOTE_IP:    /* Remote Ip */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        for (u2Count = OSPF_ZERO; u2Count < u2TlvLen;
                             u2Count += sizeof (UINT4))
                        {
                            MEMCPY (&u4TlvVal, pu1LsaData, sizeof (UINT4));
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            u4TlvVal = OSIX_NTOHL (u4TlvVal);
                            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TlvVal);
                            CliPrintf (CliHandle,
                                       "     Neighbor Address   : %s\r\n",
                                       pu1String);
                        }
                        break;
                    case OSPF_TYPE10_TE_METRIC:    /* TE Metric */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        MEMCPY (&u4TlvVal, pu1LsaData, u2TlvLen);
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        CliPrintf (CliHandle,
                                   "     Admin Metric       : %d\r\n",
                                   u4TlvVal);
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_MAX_BW:    /* Max Bw */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        MEMCPY (&bandWidth, pu1LsaData, u2TlvLen);
                        bandWidth = OSIX_NTOHF (bandWidth);
                        CliPrintf (CliHandle,
                                   "     Maximum bandwidth  : %d kbps\r\n",
                                   OSPF_CONVERT_BPS_TO_KBPS (bandWidth));
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_MAX_RES_BW:    /* Max Res Bw */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        MEMCPY (&bandWidth, pu1LsaData, u2TlvLen);
                        u4TlvVal = OSIX_NTOHF (bandWidth);
                        CliPrintf (CliHandle,
                                   "     Maximum reservable bandwidth  : %d "
                                   "kbps\r\n",
                                   OSPF_CONVERT_BPS_TO_KBPS (bandWidth));
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_UNRES_BW:    /* Un Res Bw */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        CliPrintf (CliHandle,
                                   "     Unreserved bandwidth         \r\n");
                        CliPrintf (CliHandle,
                                   "       Number of Priority         : %d\r\n",
                                   OSPF_MAX_PRIORITY_LVL);

                        for (u2Priority = OSPF_ZERO;
                             u2Priority < OSPF_MAX_PRIORITY_LVL;
                             u2Priority += OSPF_ONE)
                        {
                            MEMCPY (&bandWidth, pu1LsaData, sizeof (FLT4));
                            bandWidth = OSIX_NTOHF (bandWidth);
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "\t Priority %d : %d kbps\t", u2Priority,
                                       OSPF_CONVERT_BPS_TO_KBPS (bandWidth));
                            if (u2Priority % 2 == OSPF_ONE)
                            {
                                CliPrintf (CliHandle, "\n\r");
                            }
                        }
                        break;
                    case OSPF_TYPE10_RES_COLOR:    /* Resource color  */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        MEMCPY (&u4TlvVal, pu1LsaData, u2TlvLen);
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        CliPrintf (CliHandle, "     Affinity Bit : 0x%x\r\n",
                                   u4TlvVal);
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;

                    case OSPF_TYPE10_LCL_RMT_ID:    /* Local/Remote Identifiers */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        MEMCPY (&u4TlvVal, pu1LsaData, sizeof (UINT4));
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle, "     Local Identifier   : %d\t",
                                   u4TlvVal);
                        MEMCPY (&u4TlvVal, pu1LsaData, sizeof (UINT4));
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        CliPrintf (CliHandle, "\tRemote Identifier  : %d \r\n",
                                   u4TlvVal);
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_LINK_PROTECTION:    /* Link Protection Type */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        MEMCPY (&u1TlvVal1, pu1LsaData, u2TlvLen);
                        if (u1TlvVal1 == OSPF_ZERO)
                        {
                            break;
                        }
                        CliPrintf (CliHandle,
                                   "     Link Protection    : %s \r\n",
                                   au1ProcType[u1TlvVal1 - 1]);
                        /* Move the pointer at the end of the TLV */
                        /* Link Type is only a byte, but remaining three byte 
                         * should be ignored */
                        pu1LsaData += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        break;
                    case OSPF_TYPE10_IF_DESC:    /* Interface Switching Capability Descriptor */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy Switching Capability Information - one byte */
                        MEMCPY (&u1TlvVal1, pu1LsaData, sizeof (UINT1));
                        CliPrintSwiCapType (CliHandle, u1TlvVal1);
                        pu1LsaData += sizeof (UINT1);
                        u4Count += sizeof (UINT1);
                        /* Copy Encoding Type Information - one byte */
                        MEMCPY (&u1TlvVal1, pu1LsaData, sizeof (UINT1));
                        if (u1TlvVal1 == OSPF_ZERO)
                        {
                            break;
                        }
                        CliPrintf (CliHandle,
                                   "\tEncoding           : %s \r\n",
                                   au1EncType[u1TlvVal1 - 1]);
                        pu1LsaData += sizeof (UINT1);
                        u4Count += sizeof (UINT1);
                        /* Next two byte is Reserved */
                        pu1LsaData += sizeof (UINT2);
                        CliPrintf (CliHandle,
                                   "     Maximum LSP bandwidth       \r\n");
                        CliPrintf (CliHandle,
                                   "       Number of Priority        : %d\r\n",
                                   OSPF_MAX_PRIORITY_LVL);
                        for (u2Priority = OSPF_ZERO;
                             u2Priority < OSPF_MAX_PRIORITY_LVL; u2Priority++)
                        {
                            MEMCPY (&bandWidth, pu1LsaData, sizeof (FLT4));
                            bandWidth = OSIX_NTOHF (bandWidth);
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "\t Priority %d : %d kbps\t", u2Priority,
                                       OSPF_CONVERT_BPS_TO_KBPS (bandWidth));
                            if (u2Priority % 2 == OSPF_ONE)
                            {
                                CliPrintf (CliHandle, "\n\r");
                            }
                        }

                        if (u1TlvVal1 == OSPF_TYPE10_PSC1 ||
                            u1TlvVal1 == OSPF_TYPE10_PSC2 ||
                            u1TlvVal1 == OSPF_TYPE10_PSC3 ||
                            u1TlvVal1 == OSPF_TYPE10_PSC4)
                        {
                            MEMCPY (&bandWidth, pu1LsaData, sizeof (FLT4));
                            bandWidth = OSIX_NTOHF (bandWidth);
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "     Min LSP Bandwidth  : %d kbps\r\n",
                                       OSPF_CONVERT_BPS_TO_KBPS (bandWidth));
                            MEMCPY (&u2TlvVal, pu1LsaData, sizeof (UINT2));
                            u2TlvVal = OSIX_NTOHS (u2TlvVal);
                            /* Move to 4 bytes for padding */
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "     Interface MTU      : %d\r\n",
                                       u2TlvVal);
                        }
                        else if (u1TlvVal1 == OSPF_TYPE10_TDM)
                        {
                            MEMCPY (&bandWidth, pu1LsaData, sizeof (FLT4));
                            bandWidth = OSIX_NTOHF (bandWidth);
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "     Min LSP Bandwidth  : %d kbps\r\n",
                                       OSPF_CONVERT_BPS_TO_KBPS (bandWidth));
                            MEMCPY (&u1TlvVal1, pu1LsaData, sizeof (UINT1));
                            /* Move to 4 bytes for padding */
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "     Indication         : %d\r\n",
                                       u1TlvVal1);
                        }
                        break;
                    case OSPF_TYPE10_SRLG:    /* Shared Risk Link Group */
                        /* copy the TLV Length to u2Tlv Length */
                        MEMCPY (&u2TlvLen, pu1LsaData, sizeof (UINT2));
                        u2TlvLen = OSIX_NTOHS (u2TlvLen);
                        pu1LsaData += sizeof (UINT2);
                        u4Count += sizeof (UINT2);
                        /* Copy the Tlvvalue */
                        for (u2Count = OSPF_ZERO; u2Count < u2TlvLen;
                             u2Count += sizeof (UINT4))
                        {
                            MEMCPY (&u4TlvVal, pu1LsaData, sizeof (UINT4));
                            u4TlvVal = OSIX_NTOHL (u4TlvVal);
                            pu1LsaData += sizeof (UINT4);
                            u4Count += sizeof (UINT4);
                            CliPrintf (CliHandle,
                                       "     SRLG               : %d\r\n",
                                       u4TlvVal);
                        }
                        break;
                    default:
                        break;
                }
                if (u4Count == u2EntLen)
                    break;
                MEMCPY (&u2TlvType, pu1LsaData, sizeof (UINT2));
                u2TlvType = OSIX_NTOHS (u2TlvType);
                pu1LsaData += sizeof (UINT2);
                u4Count += sizeof (UINT2);
            }
            while (u4Count <= u2EntLen);

        default:
            break;
    }
    CliPrintf (CliHandle, "\r\n");
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliPrintLsaSummary                                 */
/*                                                                            */
/* Description       : This Routine will Print the LSA Summary                */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pLsaInfo      - LSA Information                        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliPrintLsaSummary (tCliHandle CliHandle, tLsaInfo * pLsaInfo)
{
    INT4                i4LsAge = 0;
    UINT1               u1LsType = 0;
    UINT1              *pu1LsaData;
    UINT2               u2LinkCount;
    INT4                i4LsSeqNum = 0;
    UINT4               u4Checksum = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;

    u1LsType = pLsaInfo->lsaId.u1LsaType;
    i4LsSeqNum = pLsaInfo->lsaSeqNum;
    u4Checksum = pLsaInfo->u2LsaChksum;

    GET_LSA_AGE (pLsaInfo, &i4LsAge);
    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.
                                                    linkStateId));
    CliPrintf (CliHandle, "%-16s ", pu1String);

    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    if (u1LsType == ROUTER_LSA)
    {
        pu1LsaData = (UINT1 *) (pLsaInfo->pLsa) + sizeof (tLsHeader);
        pu1LsaData += sizeof (UINT2);    /*Skip the flag */
        MEMCPY (&u2LinkCount, pu1LsaData, sizeof (UINT2));
        u2LinkCount = OSIX_NTOHS (u2LinkCount);
        i4PageStatus =
            CliPrintf (CliHandle, "%-16s %-10d 0x%-11x 0x%-8x%-10d\r\n\r\n",
                       pu1String, i4LsAge, i4LsSeqNum, u4Checksum, u2LinkCount);
    }
    else
    {
        i4PageStatus =
            CliPrintf (CliHandle, "%-16s %-10d 0x%-11x 0x%-8x\r\n\r\n",
                       pu1String, i4LsAge, i4LsSeqNum, u4Checksum);
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliFindAreaInCxt                                   */
/*                                                                            */
/* Description       : This Routine will Find the Area from the OSPF          */
/*                     LSA Database Information                               */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pOspfCxt      - pointer to tOspfCxt structure          */
/*                     u4AreaId      - Area ID                                */
/*                     i4Option      - Option to be used in printing the LSA  */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliFindAreaInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                      UINT4 u4AreaId, INT4 i4LsaType, INT4 i4Option)
{
    tOspfCxt           *pOspfCxt;
    tArea              *pArea;
    tLsaInfo           *pLsaInfo;
    tLsaInfo            LsaInfo;
    INT4                i4PageStatus = CLI_FAILURE;
    tAreaId             OspfAreaId;
    INT4                i4AdminStat;

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {
        return i4PageStatus;
    }
    OSPF_CRU_BMC_DWTOPDU (OspfAreaId, u4AreaId);
    pArea = GetFindAreaInCxt (pOspfCxt, &OspfAreaId);
    MEMSET (&LsaInfo, 0, sizeof (tLsaInfo));
    if (pArea != NULL)
    {
        if ((i4Option >= 7) && (i4Option <= 9))
        {
            i4PageStatus =
                OspfCliFilterLsaSummaryInfo (CliHandle, u4OspfCxtId, u4AreaId,
                                             i4Option);
        }
        else
        {
            i4PageStatus = CLI_SUCCESS;
            pLsaInfo = GetFirstLsaBytype (u4OspfCxtId, u4AreaId, i4LsaType);
            if (pLsaInfo == NULL)
            {
                return i4PageStatus;
            }
            do
            {
                MEMCPY (&LsaInfo, pLsaInfo, sizeof (tLsaInfo));

                i4PageStatus =
                    OspfCliFilterLsaInfo (CliHandle, &LsaInfo, i4Option);
                if (i4PageStatus == CLI_FAILURE)
                    break;
            }
            while ((pLsaInfo = GetNextLsaBytype (u4OspfCxtId,
                                                 u4AreaId, &LsaInfo,
                                                 i4LsaType)) != NULL);

        }
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowAreaDataBaseSummaryInCxt                    */
/*                                                                            */
/* Description       : This Routine will show the Area database summary       */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pOspfCxt      - pointer to tOspfCxt structure          */
/*                     u4AreaId      - Area ID                                */
/*                     LsdbCounter   - Counter for LSA database information   */
/*                                     types, aged LSA's...                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowAreaDataBaseSummaryInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                     UINT4 u4AreaId, tLsdbCounter * LsdbCounter)
{
    tArea              *pArea;
    tLsaInfo           *pLsaInfo;
    tLsaInfo            LsaInfo;
    tAreaId             OspfAreaId;
    INT4                i4PageStatus = CLI_FAILURE;
    UINT4               au4LsTypeCnt[MAX_LSA_TYPE];
    UINT4               au4LsTypeMaxAgedCnt[MAX_LSA_TYPE];
    UINT4               u4LsTypeTotal = 0;
    UINT4               u4LsTypeMaxAgedTotal = 0;
    CHR1               *pu1String = NULL;
    UINT1               u1LsType;
    tOspfCxt           *pOspfCxt;
    INT4                i4AdminStat;

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {
        return i4PageStatus;
    }

    OSPF_CRU_BMC_DWTOPDU (OspfAreaId, u4AreaId);

    pArea = GetFindAreaInCxt (pOspfCxt, &OspfAreaId);

    MEMSET (au4LsTypeCnt, 0, sizeof (au4LsTypeCnt));
    MEMSET (au4LsTypeMaxAgedCnt, 0, sizeof (au4LsTypeMaxAgedCnt));

    if (pArea != NULL)
    {
        OSPF_CLI_IPADDR_TO_STR (pu1String, u4AreaId);

        i4PageStatus = CLI_SUCCESS;
        MEMSET (&LsaInfo, 0, sizeof (tLsaInfo));
        for (u1LsType = ROUTER_LSA; u1LsType <= MAX_LSA_TYPE; u1LsType++)
        {
            pLsaInfo =
                GetFirstLsaBytype (u4OspfCxtId, u4AreaId, (INT4) u1LsType);
            if (pLsaInfo == NULL)
            {
                continue;
            }
            do
            {
                MEMCPY (&LsaInfo, pLsaInfo, sizeof (tLsaInfo));
                if ((LsaInfo.lsaId.u1LsaType < MAX_LSA_TYPE)
                    && (LsaInfo.lsaId.u1LsaType != 0))
                {
                    au4LsTypeCnt[LsaInfo.lsaId.u1LsaType - 1]++;

                    if (LsaInfo.u1LsaFlushed == OSPF_TRUE)
                    {
                        au4LsTypeMaxAgedCnt[LsaInfo.lsaId.u1LsaType - 1]++;
                    }
                }
            }
            while ((pLsaInfo = GetNextLsaBytype (u4OspfCxtId,
                                                 u4AreaId, &LsaInfo,
                                                 (INT4) u1LsType)) != NULL);
        }
        /*If router LSA is present, (area is active),
           then only area information will be printed and not for inactive areas. */
        if (au4LsTypeCnt[ROUTER_LSA - 1] != 0)
        {
            CliPrintf (CliHandle, "\r\nArea %s database summary\r\n",
                       pu1String);
            CliPrintf (CliHandle, "--------------------------------\r\n");

            CliPrintf (CliHandle, "  %-20s %-10s %-10s\r\n",
                       "LSA Type", "Count", "Maxage");
            CliPrintf (CliHandle, "  %-20s %-10s %-10s\r\n",
                       "--------", "-----", "------");

            for (u1LsType = ROUTER_LSA; u1LsType <= MAX_LSA_TYPE; u1LsType++)
            {
                switch (u1LsType)
                {
                    case ROUTER_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Router", au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    case NETWORK_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Network", au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    case NETWORK_SUM_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Summary Net",
                                       au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    case ASBR_SUM_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Summary ASBR",
                                       au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    case NSSA_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Type-7 Ext", au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    case TYPE9_OPQ_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Opaque Link",
                                       au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    case TYPE10_OPQ_LSA:
                        i4PageStatus =
                            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                       "Opaque Area",
                                       au4LsTypeCnt[u1LsType - 1],
                                       au4LsTypeMaxAgedCnt[u1LsType - 1]);
                        break;
                    default:
                        break;
                }
                LsdbCounter->au4LsTypeCnt[u1LsType - 1] +=
                    au4LsTypeCnt[u1LsType - 1];
                LsdbCounter->au4LsTypeMaxAgedCnt[u1LsType - 1] +=
                    au4LsTypeMaxAgedCnt[u1LsType - 1];

                u4LsTypeTotal += au4LsTypeCnt[u1LsType - 1];
                u4LsTypeMaxAgedTotal += au4LsTypeMaxAgedCnt[u1LsType - 1];

                if (i4PageStatus == CLI_FAILURE)
                    break;
            }

            CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                       "Subtotal", u4LsTypeTotal, u4LsTypeMaxAgedTotal);
        }
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliGetIndicesFromIfIndex                           */
/*                                                                            */
/* Description       : This Routine will Find IP Address and                  */
/*                     Adddless index from Interface Index.                   */
/*                                                                            */
/* Input Parameters  : u4IfIndex     - Interface Index                        */
/*                     pu4IfIpAddr   - IP address                             */
/*                     pi4AddrlessIf - Address less index                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

UINT4
OspfCliGetIndicesFromIfIndex (UINT4 u4Index, UINT4 *pu4IfIpAddr,
                              INT4 *pi4AddrlessIf)
{
    UINT4               u4Port = 0;

    /* Get IP Address of Interface from Interface Index */
    if (u4Index != 0)
    {
        if (NetIpv4GetPortFromIfIndex (u4Index, &u4Port) != NETIPV4_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (OspfGetIpAddr (u4Port, pu4IfIpAddr) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        if (*pu4IfIpAddr == IP_ANY_ADDR)
        {
            *pi4AddrlessIf = u4Index;
        }
        else
        {
            *pi4AddrlessIf = 0;
        }
    }
    else
    {
        *pi4AddrlessIf = 0;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetRedistribute                                 */
/*                                                                            */
/* Description       : This Routine will set Redistribution status            */
/*                     and Protocol Mask.                                     */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     i4RedistProtoMask - Proto mask                         */
/*                     u4RedistStatus    - Redistribution status              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetRedistribute (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                        INT4 i4RedistProtoMask,
                        UINT4 u4RedistStatus, UINT1 *pu1RouteMapName,
                        INT4 i4MetricValue, INT4 i4MetricType)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4] = { OSPF_ZERO };

    UINT4               u4ErrorCode;
    INT4                i4RedisProto = 0;
    INT4                i4ProtoId = 0;
    INT4                i4Count = 0;
    INT4                i4OspfRRDSrcProtoMaskDisable = 0;

    /* Based on the passed protocol, set the redistribution Mask */
    switch (i4RedistProtoMask)
    {
        case CLI_OSPF_LOCAL_RT_MASK:
            i4RedisProto = LOCAL_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_CONNECTED;
            break;
        case CLI_OSPF_STATIC_RT_MASK:
            i4RedisProto = STATIC_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_STATIC;
            break;
        case CLI_OSPF_RIP_RT_MASK:
            i4RedisProto = RIP_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_RIP;
            break;
        case CLI_OSPF_BGP_RT_MASK:
            i4RedisProto = BGP_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_BGP;
            break;
        case CLI_OSPF_ISISL1_RT_MASK:
            i4RedisProto = ISISL1_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_ISIS;
            break;
        case CLI_OSPF_ISISL2_RT_MASK:
            i4RedisProto = ISISL2_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_ISIS;
            break;
        case CLI_OSPF_ISISL1L2_RT_MASK:
            i4RedisProto = ISIS_RT_MASK;
            i4ProtoId = REDISTRUTE_ARR_ISIS;
            break;
        case CLI_OSPF_ALL_RT_MASK:
            i4RedisProto =
                LOCAL_RT_MASK | STATIC_RT_MASK | RIP_RT_MASK | BGP_RT_MASK |
                ISIS_RT_MASK;
            break;
        default:
            break;
    }

    /* Enabling the Redistribution */

    if (u4RedistStatus == OSPF_ENABLED)
    {
        if (nmhTestv2FsMIOspfRRDStatus
            (&u4ErrorCode, (INT4) u4OspfCxtId, OSPF_ENABLED) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfRRDStatus ((INT4) u4OspfCxtId, OSPF_ENABLED) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (pu1RouteMapName != NULL)
        {
            RouteMapName.pu1_OctetList = au1RMapName;
            RouteMapName.i4_Length = STRLEN (pu1RouteMapName);
            MEMCPY (RouteMapName.pu1_OctetList, pu1RouteMapName,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = OSPF_ZERO;

            if (nmhTestv2FsMIOspfRRDRouteMapEnable
                (&u4ErrorCode, (INT4) u4OspfCxtId,
                 &RouteMapName) == SNMP_FAILURE)

            {
                return CLI_FAILURE;
            }

            if (nmhSetFsMIOspfRRDRouteMapEnable
                ((INT4) u4OspfCxtId, &RouteMapName) == SNMP_FAILURE)

            {
                CLI_SET_ERR (CLI_OSPF_INV_ASSOC_RMAP);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FsMIOspfRRDSrcProtoMaskEnable
            (&u4ErrorCode, (INT4) u4OspfCxtId, i4RedisProto) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfRRDSrcProtoMaskEnable
            ((INT4) u4OspfCxtId, i4RedisProto) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }
        if (i4MetricValue != 0)
        {
            if (i4ProtoId != 0)
            {
                if (nmhTestv2FsMIOspfRRDMetricValue (&u4ErrorCode, u4OspfCxtId,
                                                     i4ProtoId,
                                                     i4MetricValue) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsMIOspfRRDMetricValue (u4OspfCxtId, i4ProtoId,
                                                  i4MetricValue) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            else
            {
                for (i4Count = 1; i4Count <= MAX_PROTO_REDISTRUTE_SIZE;
                     i4Count++)
                {
                    if (nmhTestv2FsMIOspfRRDMetricValue
                        (&u4ErrorCode, u4OspfCxtId, i4Count,
                         i4MetricValue) == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    if (nmhSetFsMIOspfRRDMetricValue (u4OspfCxtId, i4Count,
                                                      i4MetricValue) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
            }
        }
        if (i4MetricType != 0)
        {
            if (i4ProtoId != 0)
            {
                if (nmhTestv2FsMIOspfRRDMetricType (&u4ErrorCode, u4OspfCxtId,
                                                    i4ProtoId,
                                                    i4MetricType) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsMIOspfRRDMetricType (u4OspfCxtId, i4ProtoId,
                                                 i4MetricType) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            else
            {
                for (i4Count = 1; i4Count <= MAX_PROTO_REDISTRUTE_SIZE;
                     i4Count++)
                {
                    if (nmhTestv2FsMIOspfRRDMetricType
                        (&u4ErrorCode, u4OspfCxtId, i4Count,
                         i4MetricType) == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    if (nmhSetFsMIOspfRRDMetricType (u4OspfCxtId, i4Count,
                                                     i4MetricType) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
            }
        }
    }
    else                        /* Disabling the redistribution */
    {
        if (pu1RouteMapName != NULL)
        {
            RouteMapName.pu1_OctetList = au1RMapName;
            RouteMapName.i4_Length = STRLEN (pu1RouteMapName);
            MEMCPY (RouteMapName.pu1_OctetList, pu1RouteMapName,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = OSPF_ZERO;

            if (nmhTestv2FsMIOspfRRDRouteMapEnable
                (&u4ErrorCode, (INT4) u4OspfCxtId,
                 &RouteMapName) == SNMP_FAILURE)

            {
                CLI_SET_ERR (CLI_OSPF_INV_DISASSOC_RMAP);
                return CLI_FAILURE;
            }

            RouteMapName.i4_Length = OSPF_ZERO;
            if (nmhSetFsMIOspfRRDRouteMapEnable
                ((INT4) u4OspfCxtId, &RouteMapName) == SNMP_FAILURE)

            {
                CLI_SET_ERR (CLI_OSPF_INV_DISASSOC_RMAP);
                return CLI_FAILURE;
            }
        }
        if (nmhTestv2FsMIOspfRRDSrcProtoMaskDisable
            (&u4ErrorCode, (INT4) u4OspfCxtId, i4RedisProto) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }

        if (nmhSetFsMIOspfRRDSrcProtoMaskDisable
            ((INT4) u4OspfCxtId, i4RedisProto) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4ProtoId != 0)
        {
            if (nmhTestv2FsMIOspfRRDMetricValue (&u4ErrorCode, u4OspfCxtId,
                                                 i4ProtoId,
                                                 i4MetricValue) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsMIOspfRRDMetricValue
                (u4OspfCxtId, i4ProtoId, i4MetricValue) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhTestv2FsMIOspfRRDMetricType (&u4ErrorCode, u4OspfCxtId,
                                                i4ProtoId,
                                                i4MetricType) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsMIOspfRRDMetricType (u4OspfCxtId, i4ProtoId,
                                             i4MetricType) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else
        {
            for (i4Count = 1; i4Count <= MAX_PROTO_REDISTRUTE_SIZE; i4Count++)
            {
                if (nmhTestv2FsMIOspfRRDMetricValue (&u4ErrorCode, u4OspfCxtId,
                                                     i4Count,
                                                     i4MetricValue) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsMIOspfRRDMetricValue (u4OspfCxtId, i4Count,
                                                  i4MetricValue) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                if (nmhTestv2FsMIOspfRRDMetricType (&u4ErrorCode, u4OspfCxtId,
                                                    i4Count,
                                                    i4MetricType) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsMIOspfRRDMetricType (u4OspfCxtId, i4Count,
                                                 i4MetricType) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
        nmhGetFsMIOspfRRDSrcProtoMaskDisable ((INT4) u4OspfCxtId,
                                              &i4OspfRRDSrcProtoMaskDisable);
        if (i4OspfRRDSrcProtoMaskDisable == (LOCAL_RT_MASK | STATIC_RT_MASK
                                             | RIP_RT_MASK | BGP_RT_MASK))
        {
            if (nmhSetFsMIOspfRRDStatus (u4OspfCxtId, OSPF_DISABLED) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetDistribute                                   */
/*                                                                            */
/* Description       : This Routine Enable or Disable inbound filtering.      */
/*                                                                            */
/* Input Parameters  : 1. CliHandle        -  CLI context ID                  */
/*                     2. pu1RouteMapName  -  Route map name                  */
/*                     3. u1Status         -  Enable/Disable.                 */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetDistribute (tCliHandle cliHandle, UINT1 *pu1RMapName, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_OSPF_RMAP_ASSOC_FAILED;
    INT4                i4RowStatus;

    UNUSED_PARAM (cliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
        {
            return CLI_FAILURE;
        }
        else
        {
            MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                    RouteMapName.i4_Length);
        }
        if (CLI_ENABLE == u1Status)
        {
            if (nmhGetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                         FILTERING_TYPE_DISTRIB_IN,
                                                         &i4RowStatus) !=
                SNMP_SUCCESS)
            {
                if (nmhTestv2FutOspfDistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                     CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    if (nmhSetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                                 FILTERING_TYPE_DISTRIB_IN,
                                                                 CREATE_AND_WAIT)
                        == SNMP_SUCCESS)
                    {
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }
                }
            }
            else
            {
                u4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            if (SNMP_ERR_NO_ERROR == u4ErrorCode)
            {
                if (nmhTestv2FutOspfDistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                     ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                                 FILTERING_TYPE_DISTRIB_IN,
                                                                 ACTIVE) ==
                        SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
        else if (CLI_DISABLE == u1Status)
        {
            u4CliError = CLI_OSPF_INV_ASSOC_RMAP;
            if (nmhGetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                         FILTERING_TYPE_DISTRIB_IN,
                                                         &i4RowStatus) ==
                SNMP_SUCCESS)
            {
                if (nmhTestv2FutOspfDistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                     DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                                 FILTERING_TYPE_DISTRIB_IN,
                                                                 DESTROY) ==
                        SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/******************************************************************************/
/* Function Name     : OspfCliDelNetworkArea                                  */
/*                                                                            */
/* Description       : This Routine will Disable OSPF on an interface         */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     u4OspfAddressLessIf - Addressless interface index      */
/*                     u4IfIpAddr          - OSPF Network address             */
/*                     u4AreaId            - Area ID                          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelNetworkArea (tCliHandle CliHandle, UINT4 u4OspfAddressLessIf,
                       UINT4 u4IfIpAddr, UINT4 u4AreaId)
{
    UINT4               BackboneareaId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RetAreaId = 0;
    INT4                i4StatusVal = 0;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IpAddr = 0;
    INT1                i1SecIpOutCome = SNMP_SUCCESS;
    INT4                i4Val = 0;
    UINT4               u4SecIpAddr = 0;
    UINT4               u4SecIpAddrMask = 0;
    UINT4               u4AreaIfCount = 0;
    INT1                i1Return = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (NetIpIfInfo));

    if (u4OspfAddressLessIf != 0)
    {
        u4IfIpAddr = 0;
    }

    if (nmhGetOspfIfStatus (u4IfIpAddr, u4OspfAddressLessIf,
                            &i4StatusVal) != SNMP_FAILURE)
    {
        if (i4StatusVal == NOT_IN_SERVICE)
        {
            CliPrintf (CliHandle, "\r%s", OspfCliErrString[CLI_OSPF_INV_ENTRY]);
            return CLI_FAILURE;
        }
    }

    if (OspfCliCheckIfDefaultValues (u4IfIpAddr, (UINT4) u4OspfAddressLessIf) ==
        CLI_FAILURE)
    {
        if (nmhTestv2OspfIfStatus
            (&u4ErrorCode, u4IfIpAddr, (INT4) u4OspfAddressLessIf,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        u4RetAreaId = BackboneareaId;

        if (nmhTestv2OspfIfAreaId (&u4ErrorCode, u4IfIpAddr,
                                   (INT4) u4OspfAddressLessIf,
                                   u4RetAreaId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfIfAreaId
            (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
             u4RetAreaId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else
    {
        /* Checking whether the value falls with in range or not */
        if (nmhGetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                &i4StatusVal) == SNMP_FAILURE)
        {
            i1SecIpOutCome = nmhGetNextIndexFutOspfSecIfTable (0, &u4IpAddr,
                                                               0, &i4Val,
                                                               0, &u4SecIpAddr,
                                                               0,
                                                               &u4SecIpAddrMask);
            while (i1SecIpOutCome != SNMP_FAILURE)
            {
                if (u4SecIpAddr == u4IfIpAddr)
                {
                    if (nmhGetOspfIfAreaId
                        (u4IpAddr, u4OspfAddressLessIf,
                         &u4RetAreaId) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Entry\r\n");
                        return CLI_FAILURE;
                    }

                    if (u4RetAreaId != u4AreaId)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Area Id\r\n");
                        return CLI_FAILURE;
                    }
                    if (nmhTestv2FutOspfSecIfStatus
                        (&u4ErrorCode, u4IpAddr, (INT4) u4OspfAddressLessIf,
                         u4SecIpAddr, u4SecIpAddrMask, DESTROY) == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    if (nmhSetFutOspfSecIfStatus
                        (u4IpAddr, (INT4) u4OspfAddressLessIf, u4SecIpAddr,
                         u4SecIpAddrMask, DESTROY) == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }

                    return CLI_SUCCESS;
                }
                i1SecIpOutCome =
                    nmhGetNextIndexFutOspfSecIfTable (u4IpAddr, &u4IpAddr,
                                                      i4Val, &i4Val,
                                                      u4SecIpAddr, &u4SecIpAddr,
                                                      u4SecIpAddrMask,
                                                      &u4SecIpAddrMask);
            }
            if (i1SecIpOutCome == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Entry\r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhGetOspfIfAreaId
                (u4IfIpAddr, u4OspfAddressLessIf, &u4RetAreaId) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Entry\r\n");
                return CLI_FAILURE;
            }

            if (u4RetAreaId != u4AreaId)
            {
                CliPrintf (CliHandle, "\r%% Invalid Area Id\r\n");
                return CLI_FAILURE;
            }

            if (nmhTestv2OspfIfStatus
                (&u4ErrorCode, u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                 DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfIfStatus (u4IfIpAddr, (INT4) u4OspfAddressLessIf,
                                    DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

    }
    /*If OspfAreaIfCount is 0, then delete the Area */
    if (u4AreaId != 0)
    {
        i1Return = nmhGetFutOspfAreaIfCount (u4AreaId, &u4AreaIfCount);
        UNUSED_PARAM (i1Return);
        if (u4AreaIfCount == 0)
        {

            if (nmhTestv2OspfAreaStatus (&u4ErrorCode, u4AreaId,
                                         DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfAreaStatus (u4AreaId, DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelVirtualLink                                  */
/*                                                                            */
/* Description       : This Routine will set Delete a virtual link entry      */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     pArea             - Area Parameter                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelVirtualLink (tCliHandle CliHandle, tAreaStruct * pArea)
{
    UINT4               u4ErrorCode = 0;
    INT4                StatusVal = 0;

    if (nmhGetOspfVirtIfState (pArea->u4AreaId,
                               pArea->u4RouterId, &StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Trying to delete a non-existing entry\r\n");
        return CLI_FAILURE;
    }

    /* Destroying the row instance for the OSPF Virtual iftable */

    if (nmhTestv2OspfVirtIfStatus (&u4ErrorCode, pArea->u4AreaId,
                                   pArea->u4RouterId, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfVirtIfStatus (pArea->u4AreaId, pArea->u4RouterId,
                                DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelArea                                         */
/*                                                                            */
/* Description       : This Routine will Delete an area                       */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     u4AreaId          - Area ID                            */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelArea (tCliHandle CliHandle, UINT4 u4AreaId)
{
    UINT4               u4ErrorCode = 0;
    INT4                StatusVal = 0;

    if (nmhGetOspfAreaStatus (u4AreaId, &StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Area not configured\r\n");
        return CLI_FAILURE;
    }

    /* Destroying the row instance for the OSPF Area Table */

    if (nmhTestv2OspfAreaStatus (&u4ErrorCode, u4AreaId,
                                 DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfAreaStatus (u4AreaId, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Destroy the Entry."
                   "Active Interfaces May be there in the area \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelSummAddr                                     */
/*                                                                            */
/* Description       : This Routine will Delete a Summary Address entry       */
/*                     (ospfAreaAggregateTable)                               */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     u4SummNet         - Summary Network address            */
/*                     u4SummMask        - Summary Network mask               */
/*                     u4SummArea        - Summary Area    address            */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelSummAddr (tCliHandle CliHandle, UINT4 u4SummNet,
                    UINT4 u4SummMask, UINT4 u4SummArea, INT4 i4SummLsaType)
{
    INT4                i4StatusVal = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetOspfAreaAggregateStatus (u4SummArea, i4SummLsaType,
                                       u4SummNet, u4SummMask,
                                       &i4StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Trying to delete a non-existing entry\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2OspfAreaAggregateStatus (&u4ErrorCode, u4SummArea,
                                          i4SummLsaType, u4SummNet,
                                          u4SummMask, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfAreaAggregateStatus (u4SummArea, i4SummLsaType, u4SummNet,
                                       u4SummMask, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelExtSummAddr                                  */
/*                                                                            */
/* Description       : This Routine will Delete a External Summary Address    */
/*                     (futOspfAsExternalAggregationTable)                    */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     u4SummNet         - Summary Network address            */
/*                     u4SummMask        - Summary Network mask               */
/*                     u4SummArea        - Summary Area    address            */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelExtSummAddr (tCliHandle CliHandle, UINT4 u4SummNet,
                       UINT4 u4SummMask, UINT4 u4SummArea)
{
    INT4                i4StatusVal = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFutOspfAsExternalAggregationStatus (u4SummNet,
                                                  u4SummMask, u4SummArea,
                                                  &i4StatusVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Trying to delete a non-existing entry\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfAsExternalAggregationStatus
        (&u4ErrorCode, u4SummNet, u4SummMask, u4SummArea,
         DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfAsExternalAggregationStatus
        (u4SummNet, u4SummMask, u4SummArea, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowVirtualIfInCxt                              */
/*                                                                            */
/* Description       : This Routine will show Virtual interface information   */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     u4OspfCxtId       - context id                         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowVirtualIfInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tShowOspfVICookie   OspfVICookie;
    tShowOspfVI        *pShowOspfVI;
    UINT1               au1VIInfo[MAX_OSPF_VI_INFO + 4];
    UINT1              *pu1VIInfo = NULL;
    UINT4               u4BufSize = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4ShowAllCxt = OSPF_FALSE;

    pu1VIInfo = au1VIInfo;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }

    /* Initialize the cookie */
    MEMSET (&OspfVICookie, 0, sizeof (tShowOspfVICookie));

    OspfVICookie.u4OspfCxtId = u4OspfCxtId;
    /* Allocate memory to hold the Lsa information. */
    u4BufSize = ((sizeof (tShowOspfVI)) +
                 ((OSPF_CLI_MAX_VI - 1) * sizeof (tShowOspfVIInfo)));

    CliPrintf (CliHandle, "\r\n");

    while (i4RetVal == CLI_SUCCESS)
    {
        MEMSET (pu1VIInfo, 0, u4BufSize);

        i4RetVal =
            OspfCliShowVIInCxt (CliHandle, pu1VIInfo, u4BufSize, &OspfVICookie,
                                u4ShowAllCxt);

        pShowOspfVI = (tShowOspfVI *) (VOID *) pu1VIInfo;
        if (pShowOspfVI->u4NoOfVI)
        {
            if (OspfCliDisplayVI (CliHandle, pShowOspfVI) == CLI_FAILURE)
                break;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDisplayVI                                       */
/*                                                                            */
/* Description       : This Routine will Print Virtual interface information  */
/*                                                                            */
/* Input Parameters  : CliHandle         - CLI Context ID                     */
/*                     pOspfVI           - Virtual interface information      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDisplayVI (tCliHandle CliHandle, tShowOspfVI * pOspfVI)
{
    tShowOspfVIInfo    *pOspfVIInfo;
    UINT4               u4Index;
    CHR1               *pu1String;
    INT4                i4OspfVirtIfState;
    INT4                i4OspfVirtNbrState;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4OspfPrevCxtId;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    pOspfVIInfo = &(pOspfVI->VIInfo[0]);
    u4OspfPrevCxtId = pOspfVIInfo->u4OspfCxtId;

    if ((UtilOspfGetVcmAliasName (pOspfVIInfo->u4OspfCxtId,
                                  au1OspfCxtName)) == OSPF_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, " Vrf  %s \r\n", au1OspfCxtName);

    for (u4Index = 0; u4Index < pOspfVI->u4NoOfVI; u4Index++, pOspfVIInfo++)
    {
        if (u4OspfPrevCxtId != pOspfVIInfo->u4OspfCxtId)
        {

            if ((UtilOspfGetVcmAliasName (pOspfVIInfo->u4OspfCxtId,
                                          au1OspfCxtName)) == OSPF_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, " Vrf  %s \r\n", au1OspfCxtName);

        }

        u4OspfPrevCxtId = pOspfVIInfo->u4OspfCxtId;

        /* As per MIB 1850, Possible values for interface state of a virtual 
         * link are down (1) and point-to-point (4).
         *  In OSPF implementation, ISM states are:IFS_DOWN (0) and IFS_PTOP (3).
         *  So nmhGetOspfVirtIfState function returns pInterface->u1IsmState +1
         *  To check against the #defined constants, we need to decrement the
         *  returned value by 1 */

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfVIInfo->u4OspfVINbr);
        CliPrintf (CliHandle, "\r\nVirtual Link to router %s, ", pu1String);

        i4OspfVirtIfState = pOspfVIInfo->i4OspfVIStatus - 1;
        switch (i4OspfVirtIfState)
        {
            case IFS_DOWN:
                CliPrintf (CliHandle, "Interface State is DOWN\r\n");
                break;
            case IFS_PTOP:
                CliPrintf (CliHandle, "Interface State is POINT_TO_POINT\r\n");
                break;
            default:
                CliPrintf (CliHandle, "Interface State is Invalid\r\n");
                break;
        }

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfVIInfo->u4OspfVIAreaId);
        CliPrintf (CliHandle, "  Transit Area %s\r\n", pu1String);

        CliPrintf (CliHandle, "  Transmit Delay is %d sec, ",
                   pOspfVIInfo->i4OspfVITransInt);

        i4OspfVirtNbrState = pOspfVIInfo->i4OspfVNbrState - 1;

        switch (i4OspfVirtNbrState)
        {
            case NBRS_DOWN:
                CliPrintf (CliHandle, "Neighbor State DOWN\r\n");
                break;
            case NBRS_ATTEMPT:
                CliPrintf (CliHandle, "Neighbor State ATTEMPT\r\n");
                break;
            case NBRS_INIT:
                CliPrintf (CliHandle, "Neighbor State INIT\r\n");
                break;
            case NBRS_2WAY:
                CliPrintf (CliHandle, "Neighbor State 2WAY\r\n");
                break;
            case NBRS_EXSTART:
                CliPrintf (CliHandle, "Neighbor State EXCHANGE START\r\n");
                break;
            case NBRS_EXCHANGE:
                CliPrintf (CliHandle, "Neighbor State EXCHANGE\r\n");
                break;
            case NBRS_LOADING:
                CliPrintf (CliHandle, "Neighbor State LOADING\r\n");
                break;
            case NBRS_FULL:
                CliPrintf (CliHandle, "Neighbor State FULL\r\n");
                break;
            default:
                CliPrintf (CliHandle, "\r\n");
                break;
        }

        if (pOspfVIInfo->i4AuthKeyType == CRYPT_AUTHENTICATION)
        {
            switch (pOspfVIInfo->i4CryptoAuthType)
            {
                case OSPF_AUTH_MD5:
                    CliPrintf (CliHandle,
                               "  MD5 Authentication Type Enabled\r\n");
                    break;

                case OSPF_AUTH_SHA1:
                    CliPrintf (CliHandle,
                               "  SHA-1 Authentication Type Enabled\r\n");
                    break;

                case OSPF_AUTH_SHA2_224:
                    CliPrintf (CliHandle,
                               "  SHA-224 Authentication Type Enabled\r\n");
                    break;

                case OSPF_AUTH_SHA2_256:
                    CliPrintf (CliHandle,
                               "  SHA-256 Authentication Type Enabled\r\n");
                    break;

                case OSPF_AUTH_SHA2_384:
                    CliPrintf (CliHandle,
                               "  SHA-384 Authentication Type Enabled\r\n");
                    break;

                case OSPF_AUTH_SHA2_512:
                    CliPrintf (CliHandle,
                               "  SHA-512 Authentication Type Enabled\r\n");
                    break;

                default:
                    CliPrintf (CliHandle, "\r\n");
                    break;
            }
        }
        else if (pOspfVIInfo->i4AuthKeyType == SIMPLE_PASSWORD)
        {
            CliPrintf (CliHandle, "  Simple Authentication Type Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  No Authentication Type Enabled\r\n");
        }
        if (pOspfVIInfo->i4SimpleKeyStatus == 1)
        {
            CliPrintf (CliHandle, "  Simple Authentication Key configured\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "  Simple Authentication Key Not configured\r\n");
        }

        if (pOspfVIInfo->i4Md5KeyStatus == 1)
        {
            CliPrintf (CliHandle, "  Md5 Authentication Key configured\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "  Md5 Authentication Key Not configured\r\n");
        }

        CliPrintf (CliHandle, "  Timer intervals configured, ");
        CliPrintf (CliHandle, "Hello %d, ", pOspfVIInfo->i4OspfVIHelloInt);
        CliPrintf (CliHandle, "Dead %d, ", pOspfVIInfo->i4OspfVIDeadInt);

        i4PageStatus = CliPrintf (CliHandle, "Retransmit %d\r\n",
                                  pOspfVIInfo->i4OspfVIRetransInt);

        if (pOspfVIInfo->i4OspfVIRestartStatus == OSPF_ENABLED)
        {
            CliPrintf (CliHandle, "     Acting as helper\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "     Not acting as helper\r\n");
        }

        if ((pOspfVIInfo->i4OspfVIRestartStatus == OSPF_ENABLED) &&
            (pOspfVIInfo->i4OspfVIRestartAge != 0))
        {
            CliPrintf (CliHandle,
                       "     remaining restart-interval: %d\r\n",
                       pOspfVIInfo->i4OspfVIRestartAge);
        }

        if (pOspfVIInfo->i4OspfVIRstExitReason == OSPF_RESTART_COMPLETED)
        {
            i4PageStatus = CliPrintf (CliHandle, "    last NSF restart "
                                      "completed successfully\r\n");
        }
        else if (pOspfVIInfo->i4OspfVIRstExitReason == OSPF_RESTART_TIMEDOUT)
        {
            i4PageStatus = CliPrintf (CliHandle, "    last NSF restart "
                                      "timed out\r\n");
        }
        else if (pOspfVIInfo->i4OspfVIRstExitReason == OSPF_RESTART_TOP_CHG)
        {
            i4PageStatus =
                CliPrintf (CliHandle,
                           "    last NSF restart resulted "
                           "in topology change\r\n");
        }
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowVIInCxt                                     */
/*                                                                            */
/* Description       : This Routine will fetch Virtual interface information  */
/*                                                                            */
/* Input Parameters  : CliHandle - CLI Context ID                             */
/*                     pu1Buffer - Virtual interface information              */
/*                     u4BufLen  - Buffer Length                              */
/*                     pCookie   - Cookie for next index                      */
/*                     u4ShowAllCxt - Flag for all context                    */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowVIInCxt (tCliHandle CliHandle, UINT1 *pu1Buffer,
                    UINT4 u4BufLen, tShowOspfVICookie * pCookie,
                    UINT4 u4ShowAllCxt)
{
    tShowOspfVI        *pShowOspfVI;
    tShowOspfVIInfo    *pShowOspfVIInfo;
    UINT4               u4OspfVIAreaId = 0;
    UINT4               u4OspfVINbr = 0;
    UINT4               u4PrevOspfVIAreaId = 0;
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4PrevOspfVINbr = 0;
    INT4                i4RetValOspfVITransInt = 0;
    INT4                i4RetValOspfVIRetranInt = 0;
    INT4                i4RetValOspfVIHelloInt = 0;
    INT4                i4RetValOspfVIDeadInt = 0;
    INT4                i4RetValOspfVIState = 0;
    INT4                i4RetValOspfVNbrState = 0;
    UINT4               u4VIExist = OSPF_FALSE;
    UINT4               u4NoOfVI = 0;
    UINT4               u4OspfCxtId = 0;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4AdminStat;
    INT4                i4NbrHelperStatus = 0;
    UINT4               u4NbrRestartAge = 0;
    INT4                i4NbrHelperExitReason = 0;
    INT4                i4AuthKeyType = 0;
    INT4                i4CryptoAuthType = 0;
    tInterface         *pInterface = NULL;
    tAreaId             transitAreaId;
    tRouterId           nbrId;

    u4OspfCxtId = pCookie->u4OspfCxtId;
    if (pCookie->u4OspfVINbr)
    {
        u4OspfVIAreaId = pCookie->u4OspfVIAreaId;
        u4OspfVINbr = pCookie->u4OspfVINbr;
        u4VIExist = OSPF_TRUE;
    }
    else
    {
        if (u4ShowAllCxt == OSPF_TRUE)
        {
            i4RetVal =
                (nmhGetFirstIndexFsMIStdOspfVirtIfTable
                 ((INT4 *) &u4OspfCxtId, &u4OspfVIAreaId, &u4OspfVINbr));
        }
        else
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevOspfVIAreaId = 0;
            u4PrevOspfVINbr = 0;
            i4RetVal =
                (nmhGetNextIndexFsMIStdOspfVirtIfTable
                 ((INT4) u4OspfPrevCxtId, (INT4 *) &u4OspfCxtId,
                  u4PrevOspfVIAreaId, &u4OspfVIAreaId, u4PrevOspfVINbr,
                  &u4OspfVINbr));
        }
        if ((u4OspfPrevCxtId != u4OspfCxtId) && (u4ShowAllCxt == OSPF_FALSE))
        {
            /* GetNext will return next context info and u4ShowAllCxt
               will be OSPF_FALSE */
            i4RetVal = SNMP_FAILURE;
        }
    }
    if (i4RetVal == SNMP_SUCCESS)
    {
        u4VIExist = OSPF_TRUE;
    }
    else
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (u4VIExist);
    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);
    pShowOspfVI = (tShowOspfVI *) (VOID *) pu1Buffer;
    pShowOspfVI->u4NoOfVI = 0;
    pShowOspfVIInfo = &(pShowOspfVI->VIInfo[0]);

    u4NoOfVI = (u4BufLen -
                ((sizeof (tShowOspfVI)) - (sizeof (tShowOspfVIInfo)))) /
        sizeof (tShowOspfVIInfo);

    do
    {
        if (u4ShowAllCxt == OSPF_FALSE && u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Virtual interface of the specified context id is displayed */
            break;
        }
        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevOspfVIAreaId = u4OspfVIAreaId;
        u4PrevOspfVINbr = u4OspfVINbr;
        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) ||
            (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                /* Invalid context id or admin status of the
                   context is disabled */
                break;
            }
            continue;
        }

        if (!u4NoOfVI)
        {
            /* we have run out of buffer; but we have more Virtual links 
             * in the database to be returned. Hence set the cookie in the 
             * return  buffer */

            pCookie->u4OspfCxtId = u4OspfCxtId;
            pCookie->u4OspfVIAreaId = u4OspfVIAreaId;
            pCookie->u4OspfVINbr = u4OspfVINbr;
            return CLI_SUCCESS;
        }

        /*Auth Type */
        if (nmhGetFsMIStdOspfVirtIfAuthType (u4OspfCxtId, u4OspfVIAreaId,
                                             u4OspfVINbr,
                                             &i4AuthKeyType) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhGetFsMIStdOspfVirtIfCryptoAuthType (u4OspfCxtId, u4OspfVIAreaId,
                                                   u4OspfVINbr,
                                                   &i4CryptoAuthType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfVirtIfTransitDelay
            (u4OspfCxtId, u4OspfVIAreaId, u4OspfVINbr,
             &i4RetValOspfVITransInt) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfVirtIfRetransInterval
            (u4OspfCxtId, u4OspfVIAreaId, u4OspfVINbr,
             &i4RetValOspfVIRetranInt) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfVirtIfHelloInterval
            (u4OspfCxtId, u4OspfVIAreaId, u4OspfVINbr,
             &i4RetValOspfVIHelloInt) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfVirtIfRtrDeadInterval
            (u4OspfCxtId, u4OspfVIAreaId, u4OspfVINbr,
             &i4RetValOspfVIDeadInt) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfVirtIfState (u4OspfCxtId, u4OspfVIAreaId,
                                          u4OspfVINbr, &i4RetValOspfVIState)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfVirtNbrState (u4OspfCxtId, u4OspfVIAreaId,
                                           u4OspfVINbr,
                                           &i4RetValOspfVNbrState) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhGetFsMIOspfVirtNbrRestartHelperStatus (u4OspfCxtId,
                                                      u4OspfVIAreaId,
                                                      u4OspfVINbr,
                                                      &i4NbrHelperStatus)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4NbrHelperStatus == OSPF_ENABLED)
        {
            if (nmhGetFsMIOspfVirtNbrRestartHelperAge (u4OspfCxtId,
                                                       u4OspfVIAreaId,
                                                       u4OspfVINbr,
                                                       &u4NbrRestartAge)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhGetFsMIOspfVirtNbrRestartHelperExitReason
            (u4OspfCxtId, u4OspfVIAreaId,
             u4OspfVINbr, &i4NbrHelperExitReason) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4OspfVIAreaId);
        OSPF_CRU_BMC_DWTOPDU (nbrId, u4OspfVINbr);

        if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
            != NULL)
        {
            if (STRLEN (pInterface->authKey) != 0)
            {
                pShowOspfVIInfo->i4SimpleKeyStatus = 1;
            }
        }

        if (pInterface)
        {
            if (TMO_SLL_Count (&(pInterface->sortMd5authkeyLst)) > 0)
            {
                pShowOspfVIInfo->i4Md5KeyStatus = 1;
            }
        }

        pShowOspfVIInfo->u4OspfCxtId = u4OspfCxtId;
        pShowOspfVIInfo->u4OspfVIAreaId = u4OspfVIAreaId;
        pShowOspfVIInfo->u4OspfVINbr = u4OspfVINbr;
        pShowOspfVIInfo->i4OspfVIStatus = i4RetValOspfVIState;
        pShowOspfVIInfo->i4OspfVITransInt = i4RetValOspfVITransInt;
        pShowOspfVIInfo->i4OspfVIRetransInt = i4RetValOspfVIRetranInt;
        pShowOspfVIInfo->i4OspfVIHelloInt = i4RetValOspfVIHelloInt;
        pShowOspfVIInfo->i4OspfVIDeadInt = i4RetValOspfVIDeadInt;
        pShowOspfVIInfo->i4OspfVNbrState = i4RetValOspfVNbrState;
        pShowOspfVIInfo->i4OspfVIRestartStatus = i4NbrHelperStatus;
        pShowOspfVIInfo->i4OspfVIRestartAge = (INT4) u4NbrRestartAge;
        pShowOspfVIInfo->i4OspfVIRstExitReason = i4NbrHelperExitReason;
        pShowOspfVIInfo->i4AuthKeyType = i4AuthKeyType;
        pShowOspfVIInfo->i4CryptoAuthType = i4CryptoAuthType;
        /* Increment the stored Virtual Interface count and decrement the 
         * max possible virtual interface count */

        pShowOspfVI->u4NoOfVI++;
        u4NoOfVI--;

        if (u4NoOfVI)
        {
            /* Increment the pShowOspfVIInfo pointer to store the next
             *  Lsa information. */
            pShowOspfVIInfo++;
        }

    }

    while (nmhGetNextIndexFsMIStdOspfVirtIfTable ((INT4) u4OspfPrevCxtId,
                                                  (INT4 *) &u4OspfCxtId,
                                                  u4PrevOspfVIAreaId,
                                                  &u4OspfVIAreaId,
                                                  u4PrevOspfVINbr,
                                                  &u4OspfVINbr) !=
           SNMP_FAILURE);

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliSetStabilityInterval                            */
/*                                                                            */
/* Description       : This Routine will set stability interval for NSSA area */
/*                                                                            */
/* Input Parameters  : CliHandle - CLI Context ID                             */
/*                     u4AreaId  - NSSA Area ID                               */
/*                     i4NssaAttrib - Stability interval                      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetStabilityInterval (tCliHandle CliHandle, UINT4 u4AreaId,
                             INT4 i4NssaAttrib)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval (&u4ErrorCode,
                                                             u4AreaId,
                                                             i4NssaAttrib) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfAreaNSSATranslatorStabilityInterval (u4AreaId,
                                                          i4NssaAttrib) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetTransRole                                    */
/*                                                                            */
/* Description       : This Routine will set Translation role for a NSSA area */
/*                                                                            */
/* Input Parameters  : CliHandle - CLI Context ID                             */
/*                     u4AreaId  - NSSA Area ID                               */
/*                     i4NssaAttrib - Translation role                        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetTransRole (tCliHandle CliHandle, UINT4 u4AreaId, INT4 i4NssaAttrib)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfAreaNSSATranslatorRole (&u4ErrorCode, u4AreaId,
                                                i4NssaAttrib) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfAreaNSSATranslatorRole (u4AreaId,
                                             i4NssaAttrib) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliShowExtSummaryAddressInCxt                      */
/*                                                                            */
/* Description       : This Routine will show External summary address        */
/*                                                                            */
/* Input Parameters  : CliHandle   - CLI Context ID                           */
/*                     u4OspfCxtId - ospf context id                          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowExtSummaryAddressInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tShowOspfExtSACookie OspfExtSACookie;
    tShowOspfExtSA     *pShowOspfExtSA;
    UINT1               au1ExtSAInfo[MAX_OSPF_EXT_SA_INFO];
    UINT1              *pu1ExtSAInfo = NULL;
    UINT4               u4BufSize = 0;
    INT4                i4RetVal = 0;
    INT4                i4HdrFlag = OSPF_TRUE;
    UINT4               u4ShowAllCxt = OSPF_FALSE;

    pu1ExtSAInfo = au1ExtSAInfo;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }

    /* Initialize the cookie */
    MEMSET (&OspfExtSACookie, 0, sizeof (tShowOspfExtSACookie));

    OspfExtSACookie.u4OspfCxtId = u4OspfCxtId;
    /* Allocate memory to hold the Lsa information. */
    u4BufSize = ((sizeof (tShowOspfExtSA)) +
                 ((OSPF_CLI_MAX_SA - 1) * sizeof (tShowOspfExtSAInfo)));

    CliPrintf (CliHandle, "\r\n");

    while (i4RetVal == CLI_SUCCESS)
    {
        MEMSET (pu1ExtSAInfo, 0, u4BufSize);

        i4RetVal = OspfCliShowExtSAInCxt (CliHandle, pu1ExtSAInfo,
                                          u4BufSize, &OspfExtSACookie,
                                          u4ShowAllCxt);

        pShowOspfExtSA = (tShowOspfExtSA *) (VOID *) pu1ExtSAInfo;
        if (pShowOspfExtSA->u4NoOfExtSA)
        {
            if (OspfCliDisplayExtSA
                (CliHandle, pShowOspfExtSA, i4HdrFlag) == CLI_FAILURE)
            {
                break;
            }

            if (i4HdrFlag == OSPF_TRUE)
                i4HdrFlag = OSPF_FALSE;
        }
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDisplayExtSA                                    */
/*                                                                            */
/* Description       : This Routine will print External summary address       */
/*                                                                            */
/* Input Parameters  : CliHandle  - CLI Context ID                            */
/*                     pOspfExtSA - External summary address information      */
/*                     i4HdrFlag  - Flag to specify printing of Header        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDisplayExtSA (tCliHandle CliHandle,
                     tShowOspfExtSA * pOspfExtSA, INT4 i4HdrFlag)
{
    tShowOspfExtSAInfo *pOspfExtSAInfo;
    UINT4               u4Index;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];

    pOspfExtSAInfo = &(pOspfExtSA->ExtSAInfo[0]);
    u4OspfPrevCxtId = pOspfExtSAInfo->u4OspfCxtId;

    if (i4HdrFlag == OSPF_TRUE)
    {
        if ((UtilOspfGetVcmAliasName
             (pOspfExtSAInfo->u4OspfCxtId, au1OspfCxtName)) == OSPF_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, " Vrf  %s \r\n", au1OspfCxtName);

        CliPrintf (CliHandle,
                   "\rOSPF External Summary Address Configuration Information\r\n");
        CliPrintf (CliHandle,
                   "\r-------------------------------------------------------\r\n");

        CliPrintf (CliHandle, "%-15s%-15s%-15s%-22s%-16s\r\n",
                   "Network", "Mask", "Area", "Effect", "TranslationState");
        CliPrintf (CliHandle, "%-15s%-15s%-15s%-22s%-16s\r\n",
                   "-------", "----", "----", "------", "----------------");
    }

    for (u4Index = 0; u4Index < pOspfExtSA->u4NoOfExtSA;
         u4Index++, pOspfExtSAInfo++)
    {
        if (pOspfExtSAInfo->u4OspfCxtId != u4OspfPrevCxtId)
        {

            if ((UtilOspfGetVcmAliasName
                 (pOspfExtSAInfo->u4OspfCxtId, au1OspfCxtName)) == OSPF_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, " Vrf  %s \r\n", au1OspfCxtName);
            CliPrintf (CliHandle,
                       "\rOSPF External Summary Address Configuration Information\r\n");
            CliPrintf (CliHandle,
                       "\r-------------------------------------------------------\r\n");
            CliPrintf (CliHandle, "%-15s%-15s%-15s%-22s%-16s\r\n",
                       "Network", "Mask", "Area", "Effect", "TranslationState");
            CliPrintf (CliHandle, "%-15s%-15s%-15s%-22s%-16s\r\n",
                       "-------", "----", "----", "------", "----------------");

        }

        u4OspfPrevCxtId = pOspfExtSAInfo->u4OspfCxtId;

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfExtSAInfo->u4OspfExtSANet);
        CliPrintf (CliHandle, "%-15s", pu1String);

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfExtSAInfo->u4OspfExtSAMask);
        CliPrintf (CliHandle, "%-15s", pu1String);

        OSPF_CLI_IPADDR_TO_STR (pu1String, pOspfExtSAInfo->u4OspfExtSAAreaId);
        CliPrintf (CliHandle, "%-15s", pu1String);

        switch (pOspfExtSAInfo->i4OspfExtSAEffect)
        {
            case RAG_ADVERTISE:
                if (pOspfExtSAInfo->u4OspfExtSAAreaId == OSPF_ZERO)
                {
                    CliPrintf (CliHandle, "%-22s", "Type5 Advertise");
                }
                else
                {
                    CliPrintf (CliHandle, "%-22s", "Type7 Advertise");
                }

                break;
            case RAG_DO_NOT_ADVERTISE:
                if (pOspfExtSAInfo->u4OspfExtSAAreaId == OSPF_ZERO)
                {
                    CliPrintf (CliHandle, "%-22s", "Type5 DoNot-Advertise");
                }
                else
                {
                    CliPrintf (CliHandle, "%-22s", "Type7 DoNot-Advertise");
                }

                break;
            case RAG_ALLOW_ALL:
                CliPrintf (CliHandle, "%-22s", "AllowAll");
                break;
            case RAG_DENY_ALL:
                CliPrintf (CliHandle, "%-22s", "DenyAll");
                break;
            default:
                CliPrintf (CliHandle, "%-22s", "                      ");
                break;
        }

        switch (pOspfExtSAInfo->i4OspfExtSATranslation)
        {
            case OSPF_TRUE:
                CliPrintf (CliHandle, "%-8s", "enabled");
                break;
            case OSPF_FALSE:
                CliPrintf (CliHandle, "%-8s", "disabled");
                break;
            default:
                CliPrintf (CliHandle, "%-8s", "        ");
                break;
        }
        i4PageStatus = CliPrintf (CliHandle, "\r\n");

        if (i4PageStatus == CLI_FAILURE)
            break;
    }
    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowExtSAInCxt                                  */
/*                                                                            */
/* Description       : This Routine will fetch External summary address       */
/*                                                                            */
/* Input Parameters  : CliHandle    - CLI Context ID                          */
/*                     pu1Buffer    - Buffer to dump Ext. summary address info*/
/*                     u4BufLen     - Buffer Length                           */
/*                     pCookie      - Cookie for next index                   */
/*                     u4ShowAllCxt - flag to diplay the context info         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowExtSAInCxt (tCliHandle CliHandle, UINT1 *pu1Buffer,
                       UINT4 u4BufLen, tShowOspfExtSACookie * pCookie,
                       UINT4 u4ShowAllCxt)
{
    tShowOspfExtSA     *pShowOspfExtSA;
    tShowOspfExtSAInfo *pShowOspfExtSAInfo;
    UINT4               u4OspfExtSAAreaId = 0;
    UINT4               u4OspfExtSANet = 0;
    UINT4               u4OspfExtSAMask = 0;
    UINT4               u4PrevOspfExtSAAreaId = 0;
    UINT4               u4PrevOspfExtSANet = 0;
    UINT4               u4PrevOspfExtSAMask = 0;
    INT4                i4RetValOspfExtSAEffect = 0;
    INT4                i4RetValOspfExtSATranslation = 0;
    UINT4               u4NoOfExtSA = 0;
    UINT4               u4ExtSAExist = OSPF_FALSE;
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4OspfCxtId;
    INT4                i4RetVal;
    INT4                i4AdminStat;

    u4OspfCxtId = pCookie->u4OspfCxtId;

    if (pCookie->u4OspfExtSANet)
    {
        u4OspfExtSANet = pCookie->u4OspfExtSANet;
        u4OspfExtSAMask = pCookie->u4OspfExtSAMask;
        u4OspfExtSAAreaId = pCookie->u4OspfExtSAAreaId;
        u4ExtSAExist = OSPF_TRUE;
    }
    else
    {
        if (u4ShowAllCxt == OSPF_TRUE)
        {
            i4RetVal =
                nmhGetFirstIndexFsMIOspfAsExternalAggregationTable
                ((INT4 *) &u4OspfCxtId, &u4OspfExtSANet, &u4OspfExtSAMask,
                 &u4OspfExtSAAreaId);
        }
        else
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevOspfExtSANet = 0;
            u4PrevOspfExtSAMask = 0;
            u4PrevOspfExtSAAreaId = 0;

            i4RetVal =
                nmhGetNextIndexFsMIOspfAsExternalAggregationTable
                ((INT4) u4OspfPrevCxtId, (INT4 *) &u4OspfCxtId,
                 u4PrevOspfExtSANet,
                 &u4OspfExtSANet, u4PrevOspfExtSAMask, &u4OspfExtSAMask,
                 u4PrevOspfExtSAAreaId, &u4OspfExtSAAreaId);
            if ((u4OspfPrevCxtId != u4OspfCxtId)
                && (u4ShowAllCxt == OSPF_FALSE))
            {
                /* GetNext routine returned next context info and
                 * u4ShowAllCxt is set to false */
                i4RetVal = SNMP_FAILURE;
            }
        }

        if (i4RetVal != SNMP_FAILURE)
        {
            u4ExtSAExist = OSPF_TRUE;
        }
        else
        {
            return CLI_FAILURE;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);
    pShowOspfExtSA = (tShowOspfExtSA *) (VOID *) pu1Buffer;
    pShowOspfExtSA->u4NoOfExtSA = 0;
    pShowOspfExtSAInfo = &(pShowOspfExtSA->ExtSAInfo[0]);

    if (u4ExtSAExist == OSPF_TRUE)
    {
        u4NoOfExtSA = (u4BufLen - ((sizeof (tShowOspfExtSA)) -
                                   (sizeof (tShowOspfExtSAInfo)))) /
            sizeof (tShowOspfExtSAInfo);

        do
        {
            if (u4ShowAllCxt == OSPF_FALSE && u4OspfPrevCxtId != u4OspfCxtId)
            {
                /* External summary address of the specified 
                 * context id is displayed */
                break;
            }

            /* prepare to get the next indices */

            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevOspfExtSANet = u4OspfExtSANet;
            u4PrevOspfExtSAMask = u4OspfExtSAMask;
            u4PrevOspfExtSAAreaId = u4OspfExtSAAreaId;

            if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
                 == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
            {
                if (u4ShowAllCxt == OSPF_FALSE)
                {
                    /* Invalid context id or admin status of the
                     *  context is disabled */
                    break;
                }
                continue;
            }

            if (!u4NoOfExtSA)
            {
                /* we have run out of buffer; but we have more External SA in the
                 *  database to be returned. Hence set the cookie
                 *  in the return buffer */
                pCookie->u4OspfCxtId = u4OspfCxtId;
                pCookie->u4OspfExtSANet = u4OspfExtSANet;
                pCookie->u4OspfExtSAMask = u4OspfExtSAMask;
                pCookie->u4OspfExtSAAreaId = u4OspfExtSAAreaId;
                return CLI_SUCCESS;
            }

            if (nmhGetFsMIOspfAsExternalAggregationEffect (u4OspfCxtId,
                                                           u4OspfExtSANet,
                                                           u4OspfExtSAMask,
                                                           u4OspfExtSAAreaId,
                                                           &i4RetValOspfExtSAEffect)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetFsMIOspfAsExternalAggregationTranslation (u4OspfCxtId,
                                                                u4OspfExtSANet,
                                                                u4OspfExtSAMask,
                                                                u4OspfExtSAAreaId,
                                                                &i4RetValOspfExtSATranslation)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            pShowOspfExtSAInfo->u4OspfCxtId = u4OspfCxtId;
            pShowOspfExtSAInfo->u4OspfExtSANet = u4OspfExtSANet;
            pShowOspfExtSAInfo->u4OspfExtSAMask = u4OspfExtSAMask;
            pShowOspfExtSAInfo->u4OspfExtSAAreaId = u4OspfExtSAAreaId;
            pShowOspfExtSAInfo->i4OspfExtSAEffect = i4RetValOspfExtSAEffect;
            pShowOspfExtSAInfo->i4OspfExtSATranslation =
                i4RetValOspfExtSATranslation;

            pShowOspfExtSA->u4NoOfExtSA++;
            u4NoOfExtSA--;

            if (u4NoOfExtSA)
            {
                /* Increment the pShowOspfExtSAInfo pointer to store the next
                 * Ext SA information. */
                pShowOspfExtSAInfo++;
            }
        }
        while (nmhGetNextIndexFsMIOspfAsExternalAggregationTable
               ((INT4) u4OspfPrevCxtId, (INT4 *) &u4OspfCxtId,
                u4PrevOspfExtSANet, &u4OspfExtSANet, u4PrevOspfExtSAMask,
                &u4OspfExtSAMask, u4PrevOspfExtSAAreaId,
                &u4OspfExtSAAreaId) != SNMP_FAILURE);
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : OspfCliSetDefaultPassive                               */
/*                                                                            */
/* Description       : This function sets the default passive interface       */
/*                     flag so that all the interfaces created after          */
/*                     this will be passive or non passive.                   */
/*                                                                            */
/* Input Parameters  : CliHandle - CLI Context ID                             */
/*                     i4AdminStat - Status, OSPF_TRUE/OSPF_FALSE             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetDefaultPassive (tCliHandle CliHandle, INT4 i4AdminStat)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FutOspfDefaultPassiveInterface (&u4ErrorCode, i4AdminStat)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfDefaultPassiveInterface (i4AdminStat) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetIfPassive                                    */
/*                                                                            */
/* Description       : This function sets the interface as passive.           */
/*                                                                            */
/* Input Parameters  : CliHandle   - CLI Context ID                           */
/*                     u4IfIndex   - Interface index                          */
/*                     i4AdminStat - Status, OSPF_TRUE/OSPF_FALSE             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetIfPassive (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4AdminStat)
{
    UINT4               u4IfIpAddr = 0;
    INT4                i4AddrlessIf = 0;
    UINT4               u4ErrorCode = 0;

    /* Extracting the indices required for setting this object in SNMP (IP 
       interface index */
    /* Cfa Util Function is used to get the Address for
     * the Interface Index Specified. This Function returns OSIX_SUCCESS and 
     * FAILURE. OSIX_SUCCESS is 0 and OSIX_FAILURE is 1 */

    if (OspfCliGetIndicesFromIfIndex (u4IfIndex,
                                      &u4IfIpAddr,
                                      &i4AddrlessIf) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface  Layer 2 port\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfIfPassive (&u4ErrorCode, u4IfIpAddr,
                                   i4AddrlessIf, i4AdminStat) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfIfPassive (u4IfIpAddr, i4AddrlessIf,
                                i4AdminStat) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliAddExtSummAddr                                  */
/*                                                                            */
/* Description       : This Routine will set AS Ext Agg Table exntry          */
/*                     (futOspfAreaAggregateTable)                            */
/*                                                                            */
/* Input Parameters  : CliHandle     - CLI Context ID                         */
/*                     u4SummNet     - Ext. summary network address           */
/*                     u4SummMask    - Ext. summary network mask              */
/*                     u4SummArea    - Ext. summary area                      */
/*                     i4SummEffect  - Summary effect                         */
/*                     i4SummLsaType - Summary LSA type                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliAddExtSummAddr (tCliHandle CliHandle, UINT4 u4SummNet,
                       UINT4 u4SummMask, UINT4 u4SummArea,
                       INT4 i4SummEffect, INT4 i4SummLsaType)
{
    INT4                i4StatusVal = 0;
    INT4                i4OutCome;
    UINT4               u4RetVal;
    UINT4               u4ErrorCode = 0;
    /* Lsa Type is made as Summary Link since we have no support for NSSA. It
     * should made as input from configuration once NSSA support is added */

    i4OutCome = nmhGetFutOspfAsExternalAggregationStatus (u4SummNet, u4SummMask,
                                                          u4SummArea,
                                                          &i4StatusVal);

    if (i4OutCome == SNMP_SUCCESS)
    {
        /* Entry Already exists with the same index; so change the 
         * status to NOT_IN_SERVICE */
        if (nmhSetFutOspfAsExternalAggregationStatus
            (u4SummNet, u4SummMask, u4SummArea, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* New Entry */
        if (nmhTestv2FutOspfAsExternalAggregationStatus
            (&u4ErrorCode, u4SummNet, u4SummMask, u4SummArea,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfAsExternalAggregationStatus
            (u4SummNet, u4SummMask, u4SummArea,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    u4RetVal = nmhTestv2FutOspfAsExternalAggregationEffect
        (&u4ErrorCode, u4SummNet, u4SummMask, u4SummArea, i4SummEffect);

    if (u4RetVal == SNMP_SUCCESS)
    {
        u4RetVal = nmhSetFutOspfAsExternalAggregationEffect
            (u4SummNet, u4SummMask, u4SummArea, i4SummEffect);
    }

    if (u4RetVal == SNMP_SUCCESS)
    {
        u4RetVal = nmhTestv2FutOspfAsExternalAggregationTranslation
            (&u4ErrorCode, u4SummNet, u4SummMask, u4SummArea, i4SummLsaType);
    }

    if (u4RetVal == SNMP_SUCCESS)
    {
        u4RetVal = nmhSetFutOspfAsExternalAggregationTranslation
            (u4SummNet, u4SummMask, u4SummArea, i4SummLsaType);
    }

    if (u4RetVal == SNMP_SUCCESS)
    {
        u4RetVal = nmhTestv2FutOspfAsExternalAggregationStatus
            (&u4ErrorCode, u4SummNet, u4SummMask, u4SummArea, ACTIVE);
    }

    if (u4RetVal == SNMP_SUCCESS)
    {
        u4RetVal = nmhSetFutOspfAsExternalAggregationStatus
            (u4SummNet, u4SummMask, u4SummArea, ACTIVE);
    }

    else
    {
        if (i4OutCome == SNMP_SUCCESS)
        {
            /* Old Entry */
            u4RetVal = nmhSetFutOspfAsExternalAggregationStatus
                (u4SummNet, u4SummMask, u4SummArea, ACTIVE);
        }
        else
        {
            /* New Entry */
            u4RetVal = nmhSetFutOspfAsExternalAggregationStatus
                (u4SummNet, u4SummMask, u4SummArea, DESTROY);
        }
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetNssaAsbrDfRt                                 */
/*                                                                            */
/* Description       : This Routine will set P bit for DF Type 7 LSA          */
/*                     generated by internal NSSA ASBR                        */
/*                                                                            */
/* Input Parameters  : CliHandle     - CLI Context ID                         */
/*                     i4FutInt      - Status OSPF_TRUE/OSPF_FALSE            */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetNssaAsbrDfRt (tCliHandle CliHandle, INT4 i4FutInt)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2FutOspfNssaAsbrDefRtTrans (&u4ErrorCode, i4FutInt) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfNssaAsbrDefRtTrans (i4FutInt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetStubMetricType                               */
/*                                                                            */
/* Description       : This Routine will set Stub Metric Type for an          */
/*                     STUB/NSSA area                                         */
/*                                                                            */
/* Input Parameters  : CliHandle     - CLI Context ID                         */
/*                     u4AreaId      - Area ID                                */
/*                     i4Metric      - Metric value                           */
/*                     i4StubMetricType - Metric type                         */
/*                     i4MetricTOS Type - Metric TOS                          */
/*                     u1NonTos0Delete  - Status for creation/deletion        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetStubMetricType (tCliHandle CliHandle, UINT4 u4AreaId,
                          INT4 i4Metric, INT4 i4StubMetricType,
                          INT4 i4MetricTOS, UINT1 u1NonTos0Delete)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StubStatus;

    if (u1NonTos0Delete == OSPF_FALSE)
    {
        if (!IS_VALID_TOS_VALUE (i4MetricTOS))
        {
            CliPrintf (CliHandle,
                       "\r %% Type of Service based routing support is not enabled \r\n");
            return CLI_FAILURE;
        }

        /* Checking whether the Row  is already created or not and if it is not 
         * created already creating the Row instance in the Ospf Stub Area table */
        if (nmhGetOspfAreaStatus (u4AreaId, (INT4 *) &u4ErrorCode) ==
            SNMP_FAILURE)
        {
            /* Testing the Row Creation */
            if (nmhTestv2OspfAreaStatus (&u4ErrorCode, u4AreaId,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Creating the row instance for the OSPF Stub Area table using the 
             * CREATE AND WAIT */
            if (nmhSetOspfAreaStatus (u4AreaId,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            /* Set the area type as NSSA_AREA */
            if (nmhTestv2OspfImportAsExtern (&u4ErrorCode, u4AreaId,
                                             NSSA_AREA) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfImportAsExtern (u4AreaId, NSSA_AREA) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;

            }
            /* Checks whether the passed parameter value falls within the mib range */

            if (nmhTestv2OspfAreaSummary (&u4ErrorCode, u4AreaId,
                                          SEND_AREA_SUMMARY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;

            }
            /* Calls the SNMP set routine to set the value    */
            if (nmhSetOspfAreaSummary (u4AreaId, SEND_AREA_SUMMARY) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else
        {
            /* Set the area type as NSSA_AREA */
            if (nmhTestv2OspfImportAsExtern (&u4ErrorCode, u4AreaId,
                                             NSSA_AREA) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfImportAsExtern (u4AreaId, NSSA_AREA) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhTestv2OspfAreaSummary (&u4ErrorCode, u4AreaId,
                                          SEND_AREA_SUMMARY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            /* Calls the SNMP set routine to set the value    */
            if (nmhSetOspfAreaSummary (u4AreaId, SEND_AREA_SUMMARY) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhGetOspfStubStatus (u4AreaId, i4MetricTOS,
                                  &i4StubStatus) == SNMP_FAILURE)
        {
            /* Testing the Row Creation */
            if (nmhTestv2OspfStubStatus (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Creating the row instance for the OSPF Stub Area table using the
             * CREATE AND WAIT */
            if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2OspfStubMetricType (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                         i4StubMetricType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfStubMetricType (u4AreaId, i4MetricTOS,
                                      i4StubMetricType) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2OspfStubMetric (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                     i4Metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfStubMetric (u4AreaId, i4MetricTOS,
                                  i4Metric) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FutOspfAreaDfInfOriginate
            (&u4ErrorCode, u4AreaId, DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfAreaDfInfOriginate (u4AreaId, DEFAULT_INFO_ORIGINATE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetOspfAreaStatus (u4AreaId, ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* Making the Row Status to active after the SET Operation  */
        if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                  ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if ((i4Metric == OSPF_ZERO) && (i4StubMetricType == OSPF_ZERO) &&
            (i4MetricTOS == OSPF_ZERO))
        {
            if (nmhTestv2FutOspfAreaDfInfOriginate
                (&u4ErrorCode, u4AreaId,
                 NO_DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFutOspfAreaDfInfOriginate
                (u4AreaId, NO_DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhTestv2OspfStubStatus (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                         DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                      DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        if (i4Metric != OSPF_ZERO)
        {
            if (nmhTestv2OspfStubMetric (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                         CLI_OSPF_AS_EXT_DEF_METRIC) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Calls the SNMP set routine to set the value    */
            if (nmhSetOspfStubMetric (u4AreaId, i4MetricTOS,
                                      CLI_OSPF_AS_EXT_DEF_METRIC) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
        if (i4StubMetricType != OSPF_ZERO)
        {
            if (nmhTestv2OspfStubMetricType
                (&u4ErrorCode, u4AreaId, i4MetricTOS,
                 TYPE1EXT_METRIC) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfStubMetricType (u4AreaId, i4MetricTOS,
                                          TYPE1EXT_METRIC) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
        if (i4MetricTOS != OSPF_ZERO)
        {
            /* Testing the Row Creation */
            if (nmhTestv2OspfStubStatus (&u4ErrorCode, u4AreaId, i4MetricTOS,
                                         OSPF_CLI_DEF_TOS) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Creating the row instance for the OSPF Stub Area table using the
             * CREATE AND WAIT */
            if (nmhSetOspfStubStatus (u4AreaId, i4MetricTOS,
                                      OSPF_CLI_DEF_TOS) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetABRType                                      */
/*                                                                            */
/* Description       : This Routine will set the Type of ABR                  */
/*                     supported by router                                    */
/*                                                                            */
/* Input Parameters  : CliHandle     - CLI Context ID                         */
/*                     i4AbrType     - ABR type to set                        */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetABRType (tCliHandle CliHandle, INT4 i4AbrType)
{
    UINT4               u4ErrorCode = 0;

    /* Testing the ABR Type */
    if (nmhTestv2FutOspfABRType (&u4ErrorCode, i4AbrType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Setting the ABR Type */
    if (nmhSetFutOspfABRType (i4AbrType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OspfSetTraceValue                                          */
/* Description   : Sets the Trace Value                                      */
/* Input(s)      : i4TrapLevel - TrapLevel                                   */
/*                 u1LoggingCmd - Set or Reset                               */
/* Return Values : CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/

INT4
OspfSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd)
{
    UINT4               u4Trace = 0;
    if (i4TrapLevel == SYSLOG_CRITICAL_LEVEL)
    {
        u4Trace = (OSPF_CRITICAL_TRC);
    }
    else if (i4TrapLevel == SYSLOG_INFO_LEVEL)
    {
        u4Trace = OSPF_NSM_TRC | OSPF_CRITICAL_TRC;
    }
    if (OspfCliSetTraceLevelInCxt
        (1, OSPF_DEFAULT_CXT_ID, (INT4) u4Trace, 0,
         u1LoggingCmd) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetTraceLevelInCxt                              */
/*                                                                            */
/* Description       : This Routine will set the OSPF Trace level             */
/*                                                                            */
/* Input Parameters  : CliHandle     - CLI Context ID                         */
/*                     u4OspfCxtId   - ospf context id                        */
/*                     i4CliTraceVal - Trace level to set                     */
/*                     u1TraceFlag   - Flag to set/reset                      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetTraceLevelInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                           INT4 i4CliTraceVal, INT4 i4CliExtTraceVal,
                           UINT1 u1TraceFlag)
{
    CONST CHR1         *apOspfCliSetTraceLevels[] = {
        "Packet-High-Level-Dump",
        "Packet-Low-Level-Dump",
        "Packet-Hex-Dump",
        "Critical",
        "Function-Entry",
        "Function-Exit",
        "Memory-Allocation-Success",
        "Memory-Allocation-Failure",
        "Hello-Packet",
        "DDP-Packet",
        "LSR-Packet",
        "LSU-Packet",
        "LSAck-Packet",
        "ISM",
        "NSM",
        "RT-Calc",
        "RTM-Module",
        "Interface",
        "NSSA",
        "Route-Aggregation",
        "Configuration",
        "Adjacency-formation",
        "LSDB",
        "PPP",
        NULL
    };
    UINT4               u4ErrCode;
    INT4                i4TraceVal = 0;
    INT4                i4ExtTraceValue = 0;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPF_FALSE;
    tOspfCxt           *pOspfCxt = NULL;

    CliPrintf (CliHandle, "\r\n");
    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        /* GET the first CxtId */
        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPF_TRUE;
    }
    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
        if (pOspfCxt == NULL)
        {
            if (u4ShowAllCxt == OSPF_FALSE)
            {
                break;
            }
            continue;
        }

        /* Get OSPF Trace Option */
        nmhGetFsMIOspfTraceLevel (u4OspfCxtId, &i4TraceVal);

        /* Check whether trace or no trace command called */
        if (u1TraceFlag == CLI_ENABLE)
        {
            if (i4TraceVal & CLI_OSPF_DUMP_HGH_TRC)
            {
                if ((i4CliTraceVal &
                     CLI_OSPF_DUMP_LOW_TRC) ||
                    (i4CliTraceVal & CLI_OSPF_DUMP_HEX_TRC))
                {
                    i4TraceVal &= (~(CLI_OSPF_DUMP_HGH_TRC));
                }
            }
            if (i4TraceVal & CLI_OSPF_DUMP_LOW_TRC)
            {
                if ((i4CliTraceVal &
                     CLI_OSPF_DUMP_HEX_TRC) ||
                    (i4CliTraceVal & CLI_OSPF_DUMP_HGH_TRC))
                {
                    i4TraceVal &= (~(CLI_OSPF_DUMP_LOW_TRC));
                }
            }
            if (i4TraceVal & CLI_OSPF_DUMP_HEX_TRC)
            {
                if ((i4CliTraceVal &
                     CLI_OSPF_DUMP_HGH_TRC) ||
                    (i4CliTraceVal & CLI_OSPF_DUMP_LOW_TRC))
                {
                    i4TraceVal &= (~(CLI_OSPF_DUMP_HEX_TRC));
                }
            }

            i4TraceVal |= i4CliTraceVal;

        }
        else if (u1TraceFlag == CLI_DISABLE)
        {
            if (i4CliTraceVal != 0)
            {
                i4TraceVal &= (~(i4CliTraceVal));
            }
        }

        if (nmhTestv2FsMIOspfTraceLevel (&u4ErrCode, u4OspfCxtId, i4TraceVal)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfTraceLevel (u4OspfCxtId, i4TraceVal) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r Current trace: ");

        if (i4TraceVal == 0)
        {
            CliPrintf (CliHandle, "None\r\n");
        }
        else
        {
            i4TraceVal = i4TraceVal >> 8;
            OspfCliTrcStr (CliHandle, i4TraceVal, apOspfCliSetTraceLevels);
        }

        if ((nmhGetFsMIOspfExtTraceLevel (u4OspfCxtId, &i4ExtTraceValue))
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if ((i4CliExtTraceVal & CLI_OSPF_RESTART_TRC) != 0)
        {
            if (u1TraceFlag == CLI_ENABLE)
            {
                i4ExtTraceValue = i4ExtTraceValue | CLI_OSPF_RESTART_TRC;
            }
            else
            {
                i4ExtTraceValue = i4ExtTraceValue & (~(CLI_OSPF_RESTART_TRC));
            }
        }
        if ((i4CliExtTraceVal & CLI_OSPF_HELPER_TRC) != 0)
        {
            if (u1TraceFlag == CLI_ENABLE)
            {
                i4ExtTraceValue = i4ExtTraceValue | CLI_OSPF_HELPER_TRC;
            }
            else
            {
                i4ExtTraceValue = i4ExtTraceValue & (~(CLI_OSPF_HELPER_TRC));
            }
        }
        if ((i4CliExtTraceVal & CLI_OSPF_REDUNDANCY_TRC) != 0)
        {
            if (u1TraceFlag == CLI_ENABLE)
            {
                i4ExtTraceValue = i4ExtTraceValue | CLI_OSPF_REDUNDANCY_TRC;
            }
            else
            {
                i4ExtTraceValue =
                    i4ExtTraceValue & (~(CLI_OSPF_REDUNDANCY_TRC));
            }
        }

        if (nmhTestv2FsMIOspfExtTraceLevel (&u4ErrCode, u4OspfCxtId,
                                            i4ExtTraceValue) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfExtTraceLevel (u4OspfCxtId, i4ExtTraceValue)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i4ExtTraceValue = 0;
        if ((nmhGetFsMIOspfGlobalExtTraceLevel (&i4ExtTraceValue))
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if ((i4CliExtTraceVal & CLI_OSPF_RESTART_TRC) != 0)
        {
            if (u1TraceFlag == CLI_ENABLE)
            {
                i4ExtTraceValue = i4ExtTraceValue | CLI_OSPF_RESTART_TRC;
            }
            else
            {
                i4ExtTraceValue = i4ExtTraceValue & (~(CLI_OSPF_RESTART_TRC));
            }
        }
        if ((i4CliExtTraceVal & CLI_OSPF_HELPER_TRC) != 0)
        {
            if (u1TraceFlag == CLI_ENABLE)
            {
                i4ExtTraceValue = i4ExtTraceValue | CLI_OSPF_HELPER_TRC;
            }
            else
            {
                i4ExtTraceValue = i4ExtTraceValue & (~(CLI_OSPF_HELPER_TRC));
            }
        }
        if ((i4CliExtTraceVal & CLI_OSPF_REDUNDANCY_TRC) != 0)
        {
            if (u1TraceFlag == CLI_ENABLE)
            {
                i4ExtTraceValue = i4ExtTraceValue | CLI_OSPF_REDUNDANCY_TRC;
            }
            else
            {
                i4ExtTraceValue =
                    i4ExtTraceValue & (~(CLI_OSPF_REDUNDANCY_TRC));
            }
        }

        if (nmhTestv2FsMIOspfGlobalExtTraceLevel (&u4ErrCode, i4ExtTraceValue)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfGlobalExtTraceLevel (i4ExtTraceValue) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* while loop will execute when ShowAllCxt will be OSPF_TRUE */
        if (u4ShowAllCxt != OSPF_TRUE)
        {
            break;
        }

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId) !=
           OSPF_FAILURE);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetOpaqueOption                                 */
/*                                                                            */
/* Description       : This function sets the opaque option for the           */
/*                     particular context                                     */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4OpaqueOption   -   OSPF_ENABLED/OSPF_DISABLED        */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliSetOpaqueOption (tCliHandle CliHandle, INT4 i4OpaqueOption)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfOpaqueOption (&u4ErrCode, i4OpaqueOption)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfOpaqueOption (i4OpaqueOption) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* The following Set routines are specific to OSPF graceful restart */

/******************************************************************************/
/* Function Name     : OspfCliSetRestartSupport                               */
/*                                                                            */
/* Description       : This function sets the restart support for the         */
/*                     particular context                                     */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4RestartSupport -   Restart support                   */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetRestartSupport (tCliHandle CliHandle, INT4 i4RestartSupport)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfRestartSupport (&u4ErrCode, i4RestartSupport)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRestartSupport (i4RestartSupport) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetGracePeriod                                  */
/*                                                                            */
/* Description       : This function sets the grace period for the            */
/*                     particular context                                     */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GracePeriod    -   Grace period                      */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetGracePeriod (tCliHandle CliHandle, INT4 i4GracePeriod)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfRestartInterval (&u4ErrCode, i4GracePeriod)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRestartInterval (i4GracePeriod) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetGrAckState                                   */
/*                                                                            */
/* Description       : This function enables/disables Grace LSA Ack State     */
/*                     in helper                                              */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GrAckState  -   Grace LSA Acknowledegement state     */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetGrAckState (tCliHandle CliHandle, INT4 i4GrAckState)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfRestartAckState (&u4ErrCode, i4GrAckState)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRestartAckState (i4GrAckState) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetGraceRetransCount                            */
/*                                                                            */
/* Description       : This function sets the grace lsa retransmission count  */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GrLsaRetransCount  -   Grace Lsa Retransmission Count  */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetGraceRetransCount (tCliHandle CliHandle, INT4 i4GrLsaRetransCount)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfGraceLsaRetransmitCount
        (&u4ErrCode, i4GrLsaRetransCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfGraceLsaRetransmitCount (i4GrLsaRetransCount) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetRestartReason                                */
/*                                                                            */
/* Description       : This function sets the grace lsa retransmission count  */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GrRestartReason  -   Grace Lsa Restart Reason        */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetRestartReason (tCliHandle CliHandle, INT4 i4GrRestartReason)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfRestartReason (&u4ErrCode, i4GrRestartReason)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRestartReason (i4GrRestartReason) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetHelperSupport                                */
/*                                                                            */
/* Description       : This function enables/disables the helper support      */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4HelperSupport  -   Helper support                    */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* pReturB Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetHelperSupport (tCliHandle CliHandle, INT4 i4HelperSupport,
                         UINT1 u1HelperFlag)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4HelperSupport = i4HelperSupport;

    tSNMP_OCTET_STRING_TYPE HelperBit;
    UINT1               au1HelperSupport[4];
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;

    MEMSET (&HelperBit, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    HelperBit.pu1_OctetList = au1HelperSupport;

    if (u1HelperFlag == CLI_DISABLE)
    {
        if (pOspfCxt == NULL)
        {
            return CLI_FAILURE;
        }
        if (u4HelperSupport != 0)
        {
            u4HelperSupport = (pOspfCxt->u1HelperSupport
                               & (~(u4HelperSupport)));
        }
    }
    else
    {
        u4HelperSupport = ((pOspfCxt->u1HelperSupport) | u4HelperSupport);
    }

    *(UINT4 *) (VOID *) HelperBit.pu1_OctetList = u4HelperSupport;
    HelperBit.i4_Length = 4;

    if (nmhTestv2FutOspfHelperSupport (&u4ErrCode, &HelperBit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfHelperSupport (&HelperBit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetHelperGraceLimitPeriod                       */
/*                                                                            */
/* Description       : This function sets the helper grace limiy              */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GracePeriod    -   Grace period                      */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetHelperGraceLimitPeriod (tCliHandle CliHandle, INT4 i4GraceLimitPeriod)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfHelperGraceTimeLimit (&u4ErrCode, i4GraceLimitPeriod)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfHelperGraceTimeLimit (i4GraceLimitPeriod) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliSetStrictLsaCheck                               */
/*                                                                            */
/* Description       : This function enables/disables the strict LSA          */
/*                     in helper                                              */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4StrictLsaCheck -   Strict LSA check option           */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetStrictLsaCheck (tCliHandle CliHandle, INT4 i4StrictLsaCheck)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfRestartStrictLsaChecking (&u4ErrCode, i4StrictLsaCheck)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfRestartStrictLsaChecking (i4StrictLsaCheck)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : ShowOspfProtocolInfoInCxt                              */
/*                                                                            */
/* Description       : This function displays OSPF routing protocol           */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID
 *                     u4OspfCxtId - VRF Id                                   */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
ShowOspfProtocolInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tArea              *pArea;
    tNeighbor          *pNeighbor;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pListNode;
    CHR1               *pu1String = NULL;
    UINT1               au1IfAlias[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4AreaCnt = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Network = 0;
    UINT4               u4HelloTime = 0;
    UINT4               u4NssaAreaCnt = 0;
    UINT4               u4StubAreaCnt = 0;
    UINT4               u4NrmlAreaCnt = 0;
    INT4                i4Distance = 0;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4AdminStat;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];

    if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nRouting Protocol is \"ospf\"");
    if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) == OSPF_FAILURE)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, " \r\nVrf  %s\r\n", au1OspfCxtName);

    OSPF_CLI_IPADDR_TO_STR (pu1String,
                            OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId));
    CliPrintf (CliHandle, "  Router ID %s\r\n", pu1String);

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        u4AreaCnt++;
        if (pArea->u4AreaType == NSSA_AREA)
        {
            u4NssaAreaCnt++;
        }
        else if (pArea->u4AreaType == STUB_AREA)
        {
            u4StubAreaCnt++;
        }
        else
        {
            u4NrmlAreaCnt++;
        }
    }
    CliPrintf (CliHandle,
               "  Number of areas in this router is %u . %u normal %u stub %u nssa\r\n",
               u4AreaCnt, u4NrmlAreaCnt, u4StubAreaCnt, u4NssaAreaCnt);
    /* Display the OSPF information for all the interfaces in the interface 
     * table. This information can be obtained from the interface structure 
     * 'tinterface' of OSPF stack. */

    CliPrintf (CliHandle, "  Routing for Networks:\r\n");

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pListNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pListNode);
        u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
        u4Network = OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr) &
            OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask);

        OSPF_CLI_IPADDR_TO_STR (pu1String, u4Network);
        CliPrintf (CliHandle, "    %s ", pu1String);
        OSPF_CLI_IPADDR_TO_STR
            (pu1String, OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId));
        CliPrintf (CliHandle, "area %s\r\n", pu1String);
    }

    CliPrintf (CliHandle, "  Passive Interface(s):\r\n");

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pListNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pListNode);
        if ((pOspfCxt->bDefaultPassiveInterface == OSPF_TRUE) ||
            (pInterface->bPassive == OSPF_TRUE))
        {
            u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
            CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfAlias);
            CliPrintf (CliHandle, "    %s\r\n", au1IfAlias);
        }
    }
    if (nmhGetFsMIOspfPreferenceValue ((INT4) u4OspfCxtId, &i4Distance)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "  Routing Information Sources:\r\n");
    CliPrintf (CliHandle,
               "    Gateway         Distance      Last Update(secs)\r\n");

    TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pListNode, tTMO_SLL_NODE *)
    {
        pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pListNode);

        OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                (pNeighbor->nbrIpAddr));
        if (TmrGetRemainingTime (gTimerLst,
                                 &(pNeighbor->
                                   inactivityTimer.timerNode),
                                 &(u4HelloTime)) == TMR_SUCCESS)
        {
            u4HelloTime = u4HelloTime / NO_OF_TICKS_PER_SEC;
        }
        else
        {
            u4HelloTime = 0;
        }

        CliPrintf (CliHandle, "    %-15s %-13d %-u\r\n",
                   pu1String, i4Distance, u4HelloTime);
    }
    CliPrintf (CliHandle, " Distance: (default is %d)\r\n",
               CLI_OSPF_DEFAULT_PREFERENCE);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : ShowOspfProtocolInfo                                   */
/*                                                                            */
/* Description       : This function displays OSPF routing protocol           */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
ShowOspfProtocolInfo (tCliHandle CliHandle)
{
    tArea              *pArea;
    tNeighbor          *pNeighbor;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pListNode;
    CHR1               *pu1String = NULL;
    UINT1               au1IfAlias[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4AreaCnt = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Network = 0;
    UINT4               u4HelloTime = 0;
    UINT4               u4NssaAreaCnt = 0;
    UINT4               u4StubAreaCnt = 0;
    UINT4               u4NrmlAreaCnt = 0;
    INT4                i4Distance = 0;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4AdminStat;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    UINT4               u4OspfPrevCxtId = 0;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;

    CliPrintf (CliHandle, "\r\n");

    /* GET the first CxtId */
    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        u4OspfPrevCxtId = u4OspfCxtId;

        if ((nmhGetFsMIStdOspfAdminStat (u4OspfCxtId, &i4AdminStat)
             == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED) ||
            ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
        {
            continue;
        }

        CliPrintf (CliHandle, "\r\nRouting Protocol is \"ospf\"");
        if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
            OSPF_FAILURE)
        {
            continue;
        }
        CliPrintf (CliHandle, " \r\nVrf  %s\r\n", au1OspfCxtName);

        OSPF_CLI_IPADDR_TO_STR (pu1String,
                                OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId));
        CliPrintf (CliHandle, "  Router ID %s\r\n", pu1String);

        TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
        {
            u4AreaCnt++;
            if (pArea->u4AreaType == NSSA_AREA)
            {
                u4NssaAreaCnt++;
            }
            else if (pArea->u4AreaType == STUB_AREA)
            {
                u4StubAreaCnt++;
            }
            else
            {
                u4NrmlAreaCnt++;
            }
        }
        CliPrintf (CliHandle,
                   "  Number of areas in this router is %u . %u normal %u stub %u nssa\r\n",
                   u4AreaCnt, u4NrmlAreaCnt, u4StubAreaCnt, u4NssaAreaCnt);
        /* Display the OSPF information for all the interfaces in the interface 
         * table. This information can be obtained from the interface structure 
         * 'tinterface' of OSPF stack. */

        CliPrintf (CliHandle, "  Routing for Networks:\r\n");

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pListNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pListNode);
            u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
            u4Network = OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr) &
                OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask);

            OSPF_CLI_IPADDR_TO_STR (pu1String, u4Network);
            CliPrintf (CliHandle, "    %s ", pu1String);
            OSPF_CLI_IPADDR_TO_STR
                (pu1String, OSPF_CRU_BMC_DWFROMPDU (pInterface->pArea->areaId));
            CliPrintf (CliHandle, "area %s\r\n", pu1String);
        }

        CliPrintf (CliHandle, "  Passive Interface(s):\r\n");

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pListNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pListNode);
            if ((pOspfCxt->bDefaultPassiveInterface == OSPF_TRUE) ||
                (pInterface->bPassive == OSPF_TRUE))
            {
                u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
                CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfAlias);
                CliPrintf (CliHandle, "    %s\r\n", au1IfAlias);
            }
        }
        if (nmhGetFsMIOspfPreferenceValue ((INT4) u4OspfCxtId, &i4Distance)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "  Routing Information Sources:\r\n");
        CliPrintf (CliHandle,
                   "    Gateway         Distance      Last Update(secs)\r\n");

        TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pListNode, tTMO_SLL_NODE *)
        {
            pNeighbor = GET_NBR_PTR_FROM_SORT_LST (pListNode);

            OSPF_CLI_IPADDR_TO_STR (pu1String, OSPF_CRU_BMC_DWFROMPDU
                                    (pNeighbor->nbrIpAddr));
            if (TmrGetRemainingTime (gTimerLst,
                                     &(pNeighbor->
                                       inactivityTimer.timerNode),
                                     &(u4HelloTime)) == TMR_SUCCESS)
            {
                u4HelloTime = u4HelloTime / NO_OF_TICKS_PER_SEC;
            }
            else
            {
                u4HelloTime = 0;
            }

            CliPrintf (CliHandle, "    %-15s %-13d %-u\r\n",
                       pu1String, i4Distance, u4HelloTime);
        }
        CliPrintf (CliHandle, " Distance: (default is %d)\r\n",
                   CLI_OSPF_DEFAULT_PREFERENCE);

    }
    while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId) !=
           OSPF_FAILURE);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfGetRouterCfgPrompt                                 */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/

INT1
OspfGetRouterCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4Index;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_ROUTER_OSPF);
    if (STRNCMP (pi1ModeName, CLI_MODE_ROUTER_OSPF, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Index = CLI_ATOI (pi1ModeName);

    CLI_SET_CXT_ID (u4Index);

    STRCPY (pi1DispStr, "(config-router)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssOspfShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the OSPF debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4OspfCxtId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssOspfShowDebugging (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    INT4                i4DbgLevel = 0;
    INT4                i4AdminStat = 0;
    INT4                i4ExtTraceValue = 0;

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
    {

        if (i4AdminStat != OSPF_ENABLED)
        {
            return;
        }
        CliPrintf (CliHandle, "OSPF : Invalid context id");
        return;
    }

    if (nmhGetFsMIOspfTraceLevel ((INT4) u4OspfCxtId, &i4DbgLevel) ==
        SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetFsMIOspfExtTraceLevel ((INT4) u4OspfCxtId, &i4ExtTraceValue) ==
        SNMP_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "\rOSPF :");
    if ((i4DbgLevel & CLI_OSPF_HP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Hello debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_DDP_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n OSPF - Database description debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_LRQ_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Link State Request debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_LSU_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Link State Update debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_LAK_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n OSPF - Link State Acknowledge debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_ISM_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n OSPF - Interface State Machine debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_NSM_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n OSPF - Neighbor State Machine debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_CONFIGURATION_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Configuration debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_ADJACENCY_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n OSPF - Adjacency Formation debugging is on");
    }
    if ((i4DbgLevel & CLI_OSPF_INTERFACE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Interface debugging is on");
    }
    if ((i4ExtTraceValue & CLI_OSPF_RESTART_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Restart debugging is on");
    }
    if ((i4ExtTraceValue & CLI_OSPF_HELPER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Helper debugging is on");
    }
    if ((i4ExtTraceValue & CLI_OSPF_REDUNDANCY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n OSPF - Redundancy debugging is on");
    }

    CliPrintf (CliHandle, "\r\n!\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssOspfShowDebuggingInCxt                          */
/*                                                                           */
/*     DESCRIPTION      : This function prints the OSPF debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4OspfCxtId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssOspfShowDebuggingInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    INT4                i4DbgLevel = 0;
    INT4                i4AdminStat = 0;
    INT4                i4ExtTraceValue = 0;

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4AdminStat)
         == SNMP_FAILURE) || (i4AdminStat == OSPF_DISABLED))
    {

        if (i4AdminStat != OSPF_ENABLED)
        {
            return;
        }
        CliPrintf (CliHandle, "OSPF : Invalid context id");
        return;
    }

    nmhGetFsMIOspfTraceLevel ((INT4) u4OspfCxtId, &i4DbgLevel);
    nmhGetFsMIOspfExtTraceLevel ((INT4) u4OspfCxtId, &i4ExtTraceValue);
    if ((i4DbgLevel == 0) && (i4ExtTraceValue == 0))
    {
        CliPrintf (CliHandle, "\r\nend");
        CliPrintf (CliHandle, "\r\n no debug ip ospf all");
        CliPrintf (CliHandle, "\r\n!\r\n");
        return;
    }
    CliPrintf (CliHandle, "\r\nend");

    if ((i4DbgLevel & CLI_OSPF_HP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf pkt hp");
    }
    if ((i4DbgLevel & CLI_OSPF_DDP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf pkt ddp ");
    }
    if ((i4DbgLevel & CLI_OSPF_LRQ_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf pkt lrq");
    }
    if ((i4DbgLevel & CLI_OSPF_LSU_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf pkt lsu");
    }
    if ((i4DbgLevel & CLI_OSPF_LAK_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf pkt lsa");
    }
    if ((i4DbgLevel & CLI_OSPF_ISM_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module ism");
    }
    if ((i4DbgLevel & CLI_OSPF_NSM_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module nsm");
    }
    if ((i4DbgLevel & CLI_OSPF_CONFIGURATION_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module config");
    }
    if ((i4DbgLevel & CLI_OSPF_ADJACENCY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module adj_formation");
    }
    if ((i4DbgLevel & CLI_OSPF_INTERFACE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module interface");
    }
    if ((i4ExtTraceValue & CLI_OSPF_RESTART_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module restarting-router");
    }
    if ((i4ExtTraceValue & CLI_OSPF_HELPER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module helper");
    }
    if ((i4ExtTraceValue & CLI_OSPF_REDUNDANCY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n debug ip ospf module redundancy");
    }

    CliPrintf (CliHandle, "\r\n!\r\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfShowRunningConfigNbrTblInCxt                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays Nbr table in Ospf           */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4OspfCxtId - ospf context id                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
PRIVATE VOID
OspfShowRunningConfigNbrTblInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    UINT4               u4IpAddr = 0;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4OspfPrevCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4Val;
    INT4                i4PrevVal;
    INT1                i1OutCome;

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/

    u4OspfPrevCxtId = u4OspfCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexOspfNbrTable (&u4IpAddr, &i4Val);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        OspfNbrTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                               i4Val, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        i4PrevVal = i4Val;

        i1OutCome = nmhGetNextIndexFsMIStdOspfNbrTable ((INT4) u4OspfPrevCxtId,
                                                        (INT4 *) &u4OspfCxtId,
                                                        u4PrevIpAddr,
                                                        &u4IpAddr, i4PrevVal,
                                                        &i4Val);
    }
    FilePrintf (CliHandle, "!\r\n");
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : OspfShowRunningConfigInCxt                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the OSPF  Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Module                                           */
/*                        u4OspfCxtId - ospf context id                      */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
OspfShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module,
                            UINT4 u4OspfCxtId)
{
    INT4                i4GlobalAdmin;
    tOspfCxt           *pOspfCxt = NULL;
    UNUSED_PARAM (u4Module);
    FilePrintf (CliHandle, "!\r\n");

    CliRegisterLock (CliHandle, OspfLock, OspfUnLock);
    OspfLock ();

    if ((nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4GlobalAdmin)
         == SNMP_FAILURE) ||
        ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL))
    {
        OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if (i4GlobalAdmin == OSPF_ENABLED)
    {
        OspfShowRunningConfigScalarsInCxt (CliHandle, u4OspfCxtId);
        OspfShowRunningConfigTablesInCxt (CliHandle, u4OspfCxtId);
        CliPrintf (CliHandle, "!\r\n");
        FilePrintf (CliHandle, "!\r\n");
        OspfShowRunningConfigInterfaceInCxt (CliHandle, pOspfCxt);
        OspfShowRunningConfigNbrTblInCxt (CliHandle, u4OspfCxtId);
    }

    OspfUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfShowRunningConfigScalarsInCxt                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ospf for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle    - Handle to the cli context           */
/*                        u4OspfCxtId - ospf context id                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
OspfShowRunningConfigScalarsInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;

    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    INT4                i4ScalarObject;
    INT4                i4ProtoMask;
    CHR1               *pu1String = NULL;
    CHR1                pc1Redist[32] = { 0 };
    UINT4               u4ScalarObject;
    INT4                i4Distance = 0;
    UINT1               au1OspfCxtName[OSPF_MAX_CONTEXT_STR_LEN];
    tSNMP_OCTET_STRING_TYPE HelperSupport;
    UINT1               u1HelperSupport;
    INT4                i4ProtIndex = 0;
    INT4                i4SpfHoldtime = 0;
    INT4                i4SpfDelay = 0;
    UINT4               u4StaggeringInterval;
    MEMSET (pc1Redist, 0, 32);
    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;
    HelperSupport.pu1_OctetList = &u1HelperSupport;

    /*Ospf Admin Status */
    if (nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4ScalarObject)
        == SNMP_FAILURE)
    {
        return;
    }

    if (i4ScalarObject == OSPF_ENABLED)
    {
        if (u4OspfCxtId == OSPF_DEFAULT_CXT_ID)
        {
            CliPrintf (CliHandle, "router ospf \r\n");
            FilePrintf (CliHandle, "router ospf \r\n");
            FilePrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
        }
        else
        {
            /*Getting The Context Name for corrosponding Context Id */
            if ((UtilOspfGetVcmAliasName (u4OspfCxtId, au1OspfCxtName)) ==
                OSPF_FAILURE)
            {
                return;
            }

            CliPrintf (CliHandle, "router ospf vrf  %s \r\n", au1OspfCxtName);
            FilePrintf (CliHandle, "router ospf vrf  %s \r\n", au1OspfCxtName);
            FilePrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
        }
    }
    /*Ospf Router Id */
    nmhGetFsMIStdOspfRouterId (u4OspfCxtId, &u4ScalarObject);
    if (u4ScalarObject != (INT4) 0)
    {
        /* save router-id if it is configured by the user. */
        nmhGetFsMIOspfRouterIdPermanence (u4OspfCxtId, &i4ScalarObject);
        if (i4ScalarObject == ROUTERID_PERMANENT)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4ScalarObject);
            CliPrintf (CliHandle, " router-id %s \r\n", pu1String);
            FilePrintf (CliHandle, " router-id %s \r\n", pu1String);
        }
    }
    if ((nmhGetFsMIOspfSpfHoldtime ((INT4) u4OspfCxtId, &i4SpfHoldtime) ==
         SNMP_SUCCESS)
        && (nmhGetFsMIOspfSpfDelay ((INT4) u4OspfCxtId, &i4SpfDelay) ==
            SNMP_SUCCESS))
    {
        if ((i4SpfDelay != OSPF_DEF_SPF_DELAY)
            || (i4SpfHoldtime != OSPF_DEF_SPF_HOLDTIME))
        {
            CliPrintf (CliHandle, "timers spf %d %d \r\n", i4SpfDelay,
                       i4SpfHoldtime);
            FilePrintf (CliHandle, "timers spf %d %d \r\n", i4SpfDelay,
                        i4SpfHoldtime);

        }
    }
    /*Ospf ASBR Status */
    nmhGetFsMIStdOspfASBdrRtrStatus (u4OspfCxtId, &i4ScalarObject);

    if (i4ScalarObject != OSPF_FALSE)
    {
        CliPrintf (CliHandle, " ASBR Router \r\n");
        FilePrintf (CliHandle, " ASBR Router \r\n");

        /*RRD Protocol Mask Enable */
        nmhGetFsMIOspfRRDStatus (u4OspfCxtId, &i4ScalarObject);
        if (i4ScalarObject == OSPF_ENABLED)
        {
            nmhGetFsMIOspfRRDSrcProtoMaskEnable (u4OspfCxtId, &i4ProtoMask);
            nmhGetFsMIOspfRRDRouteMapEnable (u4OspfCxtId, &RouteMapName);

            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = OSPF_ZERO;

            if (RouteMapName.i4_Length != 0)
            {
                SNPRINTF (pc1Redist, 32, "route-map %s",
                          RouteMapName.pu1_OctetList);
            }

            if (i4ProtoMask & STATIC_RT_MASK)
            {
                CliPrintf (CliHandle, " redistribute static %s ", pc1Redist);
                FilePrintf (CliHandle, " redistribute static %s ", pc1Redist);
                i4ProtIndex = REDISTRUTE_ARR_STATIC;
                CliPrintMetricAndMetricType (CliHandle, i4ProtIndex,
                                             u4OspfCxtId);
            }
            if (i4ProtoMask & LOCAL_RT_MASK)
            {
                CliPrintf (CliHandle, " redistribute connected %s ", pc1Redist);
                FilePrintf (CliHandle, " redistribute connected %s ",
                            pc1Redist);
                i4ProtIndex = REDISTRUTE_ARR_CONNECTED;
                CliPrintMetricAndMetricType (CliHandle, i4ProtIndex,
                                             u4OspfCxtId);
            }
            if (i4ProtoMask & RIP_RT_MASK)
            {
                CliPrintf (CliHandle, " redistribute rip %s ", pc1Redist);
                FilePrintf (CliHandle, " redistribute rip %s ", pc1Redist);
                i4ProtIndex = REDISTRUTE_ARR_RIP;
                CliPrintMetricAndMetricType (CliHandle, i4ProtIndex,
                                             u4OspfCxtId);
            }
            if (i4ProtoMask & BGP_RT_MASK)
            {
                CliPrintf (CliHandle, " redistribute bgp %s ", pc1Redist);
                FilePrintf (CliHandle, " redistribute bgp %s ", pc1Redist);
                i4ProtIndex = REDISTRUTE_ARR_BGP;
                CliPrintMetricAndMetricType (CliHandle, i4ProtIndex,
                                             u4OspfCxtId);
            }
#ifdef ISIS_WANTED
            if ((i4ProtoMask & ISIS_RT_MASK) == ISISL1_RT_MASK)
            {
                CliPrintf (CliHandle, " redistribute isis level-1 %s ",
                           pc1Redist);
                FilePrintf (CliHandle, " redistribute isis level-1 %s ",
                            pc1Redist);
                i4ProtIndex = REDISTRUTE_ARR_ISIS;
                CliPrintMetricAndMetricType (CliHandle, i4ProtIndex,
                                             u4OspfCxtId);
            }
            else if ((i4ProtoMask & ISIS_RT_MASK) == ISIS_RT_MASK)
            {
                CliPrintf (CliHandle, " redistribute isis level-1-2 %s ",
                           pc1Redist);
                FilePrintf (CliHandle, " redistribute isis level-1-2 %s ",
                            pc1Redist);
                i4ProtIndex = REDISTRUTE_ARR_ISIS;
                CliPrintMetricAndMetricType (CliHandle, i4ProtIndex,
                                             u4OspfCxtId);
            }
#endif
        }

    }

    /*RFC-1583 Compatibility */
    nmhGetFsMIOspfRFC1583Compatibility (u4OspfCxtId, &i4ScalarObject);

    if (i4ScalarObject != OSPF_ENABLED)
    {
        CliPrintf (CliHandle, " no compatible rfc1583 \r\n");
        FilePrintf (CliHandle, " no compatible rfc1583 \r\n");
    }

    /*ABR Type */
    nmhGetFsMIOspfABRType (u4OspfCxtId, &i4ScalarObject);

    if (i4ScalarObject == CISCO_ABR)
    {
        CliPrintf (CliHandle, " abr-type cisco \r\n");
        FilePrintf (CliHandle, " abr-type cisco \r\n");
    }
    else if (i4ScalarObject == IBM_ABR)
    {
        CliPrintf (CliHandle, " abr-type ibm \r\n");
        FilePrintf (CliHandle, " abr-type ibm \r\n");
    }

    /*Nssa Asbr DefRtTrans */
    nmhGetFsMIOspfNssaAsbrDefRtTrans (u4OspfCxtId, &i4ScalarObject);

    if (i4ScalarObject == OSPF_TRUE)
    {
        CliPrintf (CliHandle,
                   " set nssa asbr-default-route translator enable \r\n");
        FilePrintf (CliHandle,
                    " set nssa asbr-default-route translator enable \r\n");
    }

    /*Default passive interface */
    nmhGetFsMIOspfDefaultPassiveInterface (u4OspfCxtId, &i4ScalarObject);

    if (i4ScalarObject == OSPF_TRUE)
    {
        CliPrintf (CliHandle, " \r\npassive-interface default \r\n");
        FilePrintf (CliHandle, " \r\npassive-interface default \r\n");
    }

    /* Opaque option */
    if (nmhGetFsMIOspfOpaqueOption (u4OspfCxtId,
                                    &i4ScalarObject) == SNMP_SUCCESS)
    {
        if (i4ScalarObject == OSPF_ENABLED)
        {
            CliPrintf (CliHandle, "capability opaque \r\n");
            FilePrintf (CliHandle, "capability opaque \r\n");
        }
    }

    /* The following configurations are related to OSPF graceful restart */
    /* Restart support */
    if (nmhGetFsMIOspfRestartSupport (u4OspfCxtId, &i4ScalarObject)
        == SNMP_SUCCESS)
    {
        if (i4ScalarObject == OSPF_RESTART_PLANNED)
        {
            CliPrintf (CliHandle, "nsf ietf restart-support plannedOnly \r\n");
            FilePrintf (CliHandle, "nsf ietf restart-support plannedOnly \r\n");
        }
        else if (i4ScalarObject == OSPF_RESTART_BOTH)
        {
            CliPrintf (CliHandle, "nsf ietf restart-support \r\n");
            FilePrintf (CliHandle, "nsf ietf restart-support \r\n");
        }
    }

    /* Restart Interval */
    if (nmhGetFsMIOspfRestartInterval (u4OspfCxtId, &i4ScalarObject)
        == SNMP_SUCCESS)
    {
        if (i4ScalarObject != OSPF_GR_DEF_INTERVAL)
        {
            CliPrintf (CliHandle, "nsf ietf restart-interval %d \r\n",
                       i4ScalarObject);
            FilePrintf (CliHandle, "nsf ietf restart-interval %d \r\n",
                        i4ScalarObject);
        }
    }
    /* Restarter Ack State */
    if (nmhGetFsMIOspfRestartAckState (u4OspfCxtId, &i4ScalarObject)
        == SNMP_SUCCESS)
    {
        if (i4ScalarObject == OSPF_DISABLED)
        {
            CliPrintf (CliHandle, "no nsf ietf grace lsa ack required \r\n");
            FilePrintf (CliHandle, "no nsf ietf grace lsa ack required \r\n");
        }
    }
    /* Grace LSA Retransmission Count */
    if (nmhGetFsMIOspfGraceLsaRetransmitCount (u4OspfCxtId, &i4ScalarObject)
        == SNMP_SUCCESS)
    {
        if (i4ScalarObject != DEF_GRACE_LSA_SENT)
        {
            CliPrintf (CliHandle, "nsf ietf grlsa retrans count %d \r\n",
                       i4ScalarObject);
            FilePrintf (CliHandle, "nsf ietf grlsa retrans count %d \r\n",
                        i4ScalarObject);
        }
    }
    /* Graceful Restart Reason */
    if ((nmhGetFsMIOspfRestartReason (u4OspfCxtId, &i4ScalarObject)
         == SNMP_SUCCESS) && (i4ScalarObject != OSPF_DEFAULT_REASON))
    {
        switch (i4ScalarObject)
        {
            case OSPF_GR_SW_RESTART:
                CliPrintf (CliHandle,
                           "nsf ietf restart-reason softwareRestart\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf restart-reason softwareRestart\r\n");
                break;
            case OSPF_GR_SW_UPGRADE:
                CliPrintf (CliHandle,
                           "nsf ietf restart-reason swReloadUpgrade\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf restart-reason swReloadUpgrade\r\n");
                break;
            case OSPF_GR_SW_RED:
                CliPrintf (CliHandle,
                           "nsf ietf restart-reason switchToRedundant\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf restart-reason switchToRedundant\r\n");
                break;
            default:
                break;
        }
    }

    /* Strict LSA checking */
    if (nmhGetFsMIOspfRestartStrictLsaChecking (u4OspfCxtId, &i4ScalarObject)
        == SNMP_SUCCESS)
    {
        if (i4ScalarObject == OSPF_TRUE)
        {
            CliPrintf (CliHandle, "nsf ietf helper strict-lsa-checking \r\n");
            FilePrintf (CliHandle, "nsf ietf helper strict-lsa-checking \r\n");
        }
    }

    /* Helper Grace Time Limit */
    if (nmhGetFsMIOspfHelperGraceTimeLimit (u4OspfCxtId, &i4ScalarObject)
        == SNMP_SUCCESS)
    {
        if (i4ScalarObject != OSPF_DEF_HELPER_GR_TIME_LIMIT)
        {
            CliPrintf (CliHandle, "nsf ietf helper gracetimelimit %d \r\n",
                       i4ScalarObject);
            FilePrintf (CliHandle, "nsf ietf helper gracetimelimit %d \r\n",
                        i4ScalarObject);
        }
    }

    /* Helper support */
    if (nmhGetFsMIOspfHelperSupport (u4OspfCxtId, &HelperSupport)
        == SNMP_SUCCESS)
    {
        if (HelperSupport.i4_Length != 0)
        {
            if (HelperSupport.pu1_OctetList[0] != OSPF_GRACE_HELPER_ALL)
            {
                if (!(HelperSupport.pu1_OctetList[0] & OSPF_GRACE_UNKNOWN))
                {
                    CliPrintf (CliHandle,
                               "no nsf ietf helper-support unknown \r\n");
                    FilePrintf (CliHandle,
                                "no nsf ietf helper-support unknown \r\n");
                }
                if (!(HelperSupport.pu1_OctetList[0] & OSPF_GRACE_SW_RESTART))
                {
                    CliPrintf (CliHandle,
                               "no nsf ietf helper-support softwareRestart \r\n");
                    FilePrintf (CliHandle,
                                "no nsf ietf helper-support softwareRestart \r\n");
                }
                if (!
                    (HelperSupport.
                     pu1_OctetList[0] & OSPF_GRACE_SW_RELOADUPGRADE))
                {
                    CliPrintf (CliHandle,
                               "no nsf ietf helper-support  swReloadUpgrade \r\n");
                    FilePrintf (CliHandle,
                                "no nsf ietf helper-support swReloadUpgrade \r\n");
                }
                if (!(HelperSupport.pu1_OctetList[0] & OSPF_GRACE_SW_REDUNDANT))
                {
                    CliPrintf (CliHandle,
                               "no nsf ietf helper-support  switchToRedundant\r\n");
                    FilePrintf (CliHandle,
                                "no nsf ietf helper-support switchToRedundant\r\n");
                }
            }

        }
    }
    /* Distance */
    if ((nmhGetFsMIOspfPreferenceValue ((INT4) u4OspfCxtId, &i4Distance) ==
         SNMP_SUCCESS) && (i4Distance != OSPF_DEFAULT_PREFERENCE))
    {
        CliPrintf (CliHandle, "distance %d\r\n", i4Distance);
        FilePrintf (CliHandle, "distance %d\r\n", i4Distance);
    }
    /* Staggering Status */
    if (gOsRtr.u4RTStaggeringStatus != OSPF_STAGGERING_ENABLED)
    {
        CliPrintf (CliHandle, "no route-calculation staggering\r\n");
        FilePrintf (CliHandle, "no route-calculation staggering\r\n");
    }
    /* Staggering Interval */
    else
    {

        if ((nmhGetFsMIOspfRTStaggeringInterval ((INT4) u4OspfCxtId,
                                                 &u4StaggeringInterval) ==
             SNMP_SUCCESS) &&
            (u4StaggeringInterval != OSPF_DEF_STAGGERING_INTERVAL))
        {
            CliPrintf (CliHandle,
                       "route-calculation staggering-interval %d"
                       "\r\n", u4StaggeringInterval);
            FilePrintf (CliHandle,
                        "Route calculation staggering interval is  %d"
                        " milliseconds\r\n", u4StaggeringInterval);

        }
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfShowRunningConfigTablesInCxt                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays table objects in Ospf       */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4OspfCxtId - ospf context id                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfShowRunningConfigTablesInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId)
{
    UINT4               u4IpAddr = 0;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4IpAddr1 = 0;
    UINT4               u4PrevIpAddr1 = 0;
    UINT4               u4SecIpAddr = 0;
    UINT4               u4SecIpAddrMask = 0;
    UINT4               u4PrevSecIpAddr = 0;
    UINT4               u4PrevSecIpAddrMask = 0;
    UINT4               u4Mask = 0;
    UINT4               u4PrevMask = 0;
    INT4                i4Val = 0;
    INT4                i4PrevVal = 0;
    INT4                i4PrevSecVal = 0;
    INT1                i1OutCome = 0;
    INT1                i1SecIpOutCome = SNMP_SUCCESS;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4OspfPrevCxtId = u4OspfCxtId;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tSNMP_OCTET_STRING_TYPE NextRouteMapName;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];

    INT4                i4FilterType = 0;
    INT4                i4NextFilterType = 0;
    INT4                i4Value = 0;
    INT4                i4StatusVal = 0;
    CHR1               *pu1String = NULL;
    tOspfCxt           *pContext = NULL;
    tInterface         *pInterface = NULL;
    tIPADDR             ifIpAddr;
    INT1                i1SecIPCount = 0;
    UINT4               u4AreaId = 0;
    INT4                i4ScalarObject = 0;

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexOspfIfTable (&u4IpAddr, &i4Val);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        OspfIfTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr, i4Val, FALSE,
                              &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4PrevIpAddr = u4IpAddr;
        i4PrevVal = i4Val;
        u4OspfPrevCxtId = u4OspfCxtId;

        if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
        {
            return;
        }
        pContext = gOsRtr.pOspfCxt;
        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IpAddr);
        pInterface = GetFindIfInCxt (pContext, ifIpAddr, i4Val);
        if (pInterface != NULL)
        {
            i1SecIPCount = pInterface->secondaryIP.i1SecIpCount;
        }
        if (i1SecIPCount != 0)
        {
            i4PrevSecVal = 0;
            u4PrevSecIpAddr = 0;
            u4SecIpAddr = 0;
            u4PrevSecIpAddrMask = 0;
            u4SecIpAddrMask = 0;

            i1SecIpOutCome =
                nmhGetNextIndexFutOspfSecIfTable (u4PrevIpAddr, &u4IpAddr,
                                                  i4PrevSecVal, &i4Val,
                                                  u4PrevSecIpAddr, &u4SecIpAddr,
                                                  u4PrevSecIpAddrMask,
                                                  &u4SecIpAddrMask);
            while (i1SecIpOutCome != SNMP_FAILURE)
            {
                if (u4OspfPrevCxtId != u4OspfCxtId)
                {
                    /* Get next gave the next context */
                    break;
                }
                if (nmhGetFutOspfSecIfStatus
                    (u4IpAddr, i4Val, u4SecIpAddr, u4SecIpAddrMask,
                     &i4StatusVal) != SNMP_FAILURE)
                {
                    /*Interface Ip Address */
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecIpAddr);
                    CliPrintf (CliHandle, "network %s ", pu1String);

                    /*Area Id */
                    nmhGetFsMIStdOspfIfAreaId (u4OspfCxtId, u4IpAddr, i4Val,
                                               &u4AreaId);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
                    CliPrintf (CliHandle, "area %s ", pu1String);
                    CliPrintf (CliHandle, "\r\n");
                }
                i4PrevSecVal = i4Val;
                u4PrevSecIpAddr = u4SecIpAddr;
                u4PrevSecIpAddrMask = u4SecIpAddrMask;
                if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
                {
                    return;
                }
                i1SecIPCount--;
                if (i1SecIPCount == 0)
                {
                    break;
                }
                i1SecIpOutCome =
                    nmhGetNextIndexFutOspfSecIfTable (u4PrevIpAddr, &u4IpAddr,
                                                      i4PrevSecVal, &i4Val,
                                                      u4PrevSecIpAddr,
                                                      &u4SecIpAddr,
                                                      u4PrevSecIpAddrMask,
                                                      &u4SecIpAddrMask);
            }
        }
        i1OutCome = nmhGetNextIndexFsMIStdOspfIfTable ((INT4) u4OspfPrevCxtId,
                                                       (INT4 *) &u4OspfCxtId,
                                                       u4PrevIpAddr,
                                                       &u4IpAddr, i4PrevVal,
                                                       &i4Val);
    }
    /*OspfAreaTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexOspfAreaTable (&u4IpAddr);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        OspfAreaTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;

        i1OutCome = nmhGetNextIndexFsMIStdOspfAreaTable ((INT4) u4OspfPrevCxtId,
                                                         (INT4 *) &u4OspfCxtId,
                                                         u4PrevIpAddr,
                                                         &u4IpAddr);
    }

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexFutOspfAreaTable (&u4IpAddr);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        FutOspfAreaTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                   &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4PrevIpAddr = u4IpAddr;
        u4OspfPrevCxtId = u4OspfCxtId;

        i1OutCome = nmhGetNextIndexFsMIOspfAreaTable ((INT4) u4OspfPrevCxtId,
                                                      (INT4 *) &u4OspfCxtId,
                                                      u4PrevIpAddr, &u4IpAddr);
    }

    /*OspfStubAreaTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexOspfStubAreaTable (&u4IpAddr, &i4Val);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        OspfStubAreaTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr, i4Val,
                                    &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        i4PrevVal = i4Val;

        i1OutCome = nmhGetNextIndexFsMIStdOspfStubAreaTable ((INT4)
                                                             u4OspfPrevCxtId,
                                                             (INT4 *)
                                                             &u4OspfCxtId,
                                                             u4PrevIpAddr,
                                                             &u4IpAddr,
                                                             i4PrevVal, &i4Val);
    }

    /*futOspfIfTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexFutOspfIfTable (&u4IpAddr, &i4Val);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        FutOspfIfTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                 i4Val, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        i4PrevVal = i4Val;

        i1OutCome = nmhGetNextIndexFsMIOspfIfTable ((INT4) u4OspfPrevCxtId,
                                                    (INT4 *) &u4OspfCxtId,
                                                    u4PrevIpAddr,
                                                    &u4IpAddr,
                                                    i4PrevVal, &i4Val);
    }

    /*ospfNbrTable */
    /* Neigbor table needs the interface configuration ,say NBMA or P2MP */

    /*futOspfVirtIfMD5AuthTable and ospfVirtIfTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexOspfVirtIfTable (&u4IpAddr, &u4IpAddr1);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        OspfVirtIfTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                  u4IpAddr1, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        u4PrevIpAddr1 = u4IpAddr1;

        i1OutCome = nmhGetNextIndexFsMIStdOspfVirtIfTable ((INT4)
                                                           u4OspfPrevCxtId,
                                                           (INT4 *)
                                                           &u4OspfCxtId,
                                                           u4PrevIpAddr,
                                                           &u4IpAddr,
                                                           u4PrevIpAddr1,
                                                           &u4IpAddr1);
    }

    /*OspfAreaAggregateTable and futOspfAreaAggregateTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexOspfAreaAggregateTable (&u4IpAddr, &i4Val,
                                                        &u4IpAddr1, &u4Mask);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        OspfAreaAggregateTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                         i4Val, u4IpAddr1,
                                         u4Mask, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        i4PrevVal = i4Val;
        u4PrevIpAddr1 = u4IpAddr1;
        u4PrevMask = u4Mask;

        i1OutCome =
            nmhGetNextIndexFsMIStdOspfAreaAggregateTable ((INT4)
                                                          u4OspfPrevCxtId,
                                                          (INT4 *) &u4OspfCxtId,
                                                          u4PrevIpAddr,
                                                          &u4IpAddr, i4PrevVal,
                                                          &i4Val, u4PrevIpAddr1,
                                                          &u4IpAddr1,
                                                          u4PrevMask, &u4Mask);
    }

    /*futOspfAsExternalAggregationTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexFutOspfAsExternalAggregationTable (&u4IpAddr,
                                                                   &u4Mask,
                                                                   &u4IpAddr1);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        FutOspfAsExternalAggregationTableInfoInCxt (CliHandle, u4OspfCxtId,
                                                    u4IpAddr, u4Mask,
                                                    u4IpAddr1, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        u4PrevMask = u4Mask;
        u4PrevIpAddr1 = u4IpAddr1;

        i1OutCome =
            nmhGetNextIndexFsMIOspfAsExternalAggregationTable ((INT4)
                                                               u4OspfPrevCxtId,
                                                               (INT4 *)
                                                               &u4OspfCxtId,
                                                               u4PrevIpAddr,
                                                               &u4IpAddr,
                                                               u4PrevMask,
                                                               &u4Mask,
                                                               u4PrevIpAddr1,
                                                               &u4IpAddr1);
    }

    /*futOspfRRDRouteConfigTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexFutOspfRRDRouteConfigTable (&u4IpAddr, &u4Mask);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        FutOspfRRDRouteConfigTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                             u4Mask, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        u4PrevMask = u4Mask;

        i1OutCome = nmhGetNextIndexFsMIOspfRRDRouteConfigTable ((INT4)
                                                                u4OspfPrevCxtId,
                                                                (INT4 *)
                                                                &u4OspfCxtId,
                                                                u4PrevIpAddr,
                                                                &u4IpAddr,
                                                                u4PrevMask,
                                                                &u4Mask);
    }

    /*futOspfExtRouteTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexFutOspfExtRouteTable (&u4IpAddr, &u4Mask,
                                                      &i4Val);

    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            /* Get next gave the next context */
            break;
        }

        FutOspfExtRouteTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                       u4Mask, i4Val, &u4PagingStatus);

        if (u4PagingStatus == CLI_FAILURE)
            break;

        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevIpAddr = u4IpAddr;
        u4PrevMask = u4Mask;
        i4PrevVal = i4Val;

        i1OutCome = nmhGetNextIndexFsMIOspfExtRouteTable ((INT4)
                                                          u4OspfPrevCxtId,
                                                          (INT4 *) &u4OspfCxtId,
                                                          u4PrevIpAddr,
                                                          &u4IpAddr, u4PrevMask,
                                                          &u4Mask, i4PrevVal,
                                                          &i4Val);
    }

    /* Filters */
    if (UtilOspfSetContext (u4OspfCxtId) != SNMP_FAILURE)
    {

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        i1OutCome =
            nmhGetFirstIndexFutOspfDistInOutRouteMapTable (&RouteMapName,
                                                           &i4FilterType);
        while (i1OutCome != SNMP_FAILURE)
        {
            switch (i4FilterType)
            {
                case FILTERING_TYPE_DISTANCE:
                    if (nmhGetFutOspfDistInOutRouteMapValue
                        (&RouteMapName, i4FilterType, &i4Value) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "distance %d route-map %s\r\n",
                                   i4Value, RouteMapName.pu1_OctetList);
                    }
                    break;
                case FILTERING_TYPE_DISTRIB_IN:
                    CliPrintf (CliHandle,
                               "distribute-list route-map %s in\r\n",
                               RouteMapName.pu1_OctetList);
                    break;
                case FILTERING_TYPE_DISTRIB_OUT:
                    CliPrintf (CliHandle,
                               "distribute-list route-map %s out\r\n",
                               RouteMapName.pu1_OctetList);
                    break;
                default:
                    break;
            }

            MEMSET (&NextRouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1NextRMapName, 0, RMAP_MAX_NAME_LEN + 4);
            NextRouteMapName.pu1_OctetList = au1NextRMapName;

            i1OutCome =
                nmhGetNextIndexFutOspfDistInOutRouteMapTable (&RouteMapName,
                                                              &NextRouteMapName,
                                                              i4FilterType,
                                                              &i4NextFilterType);

            i4FilterType = i4NextFilterType;

            MEMCPY (RouteMapName.pu1_OctetList, NextRouteMapName.pu1_OctetList,
                    RMAP_MAX_NAME_LEN + 4);
            RouteMapName.i4_Length = NextRouteMapName.i4_Length;
        }
    }
    /*If BFD enabled globally on router interface mode, src display it after ospf adjacency has done */
    /* Ospf path monitoring by BFD */
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return;
    }

    nmhGetFsMIOspfBfdStatus ((INT4) u4OspfCxtId, &i4ScalarObject);

    if (i4ScalarObject == OSPF_BFD_ENABLED)
    {
        CliPrintf (CliHandle, " enable bfd \r\n");
        FilePrintf (CliHandle, " enable bfd \r\n");

        /* Monitoing all OSPF interfaces */
        nmhGetFsMIOspfBfdAllIfState ((INT4) u4OspfCxtId, &i4ScalarObject);
        if (i4ScalarObject == OSPF_BFD_ENABLED)
        {
            CliPrintf (CliHandle, " bfd all-interface \r\n");
            FilePrintf (CliHandle, " bfd all-interface \r\n");
        }
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FutOspfAreaTableInfoInCxt                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays futOspfAreaTable   objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FutOspfAreaTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                           UINT4 u4AreaId, UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject;

    /*Area Id */
    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);

    /*Translation role for a NSSA area */
    nmhGetFsMIOspfAreaNSSATranslatorRole (u4OspfCxtId, u4AreaId,
                                          &i4TableObject);
    if (i4TableObject != TRNSLTR_ROLE_CANDIDATE)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "area %s translation-role always\r\n",
                       pu1String);
        FilePrintf (CliHandle, "area %s translation-role always\r\n",
                    pu1String);
    }

    /*stability interval for NSSA area */
    nmhGetFsMIOspfAreaNSSATranslatorStabilityInterval (u4OspfCxtId,
                                                       u4AreaId,
                                                       &i4TableObject);
    if (i4TableObject != NSSA_TRNSLTR_STBLTY_INTRVL)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "area %s stability-interval %d \r\n",
                       pu1String, i4TableObject);
        FilePrintf (CliHandle, "area %s stability-interval %d \r\n",
                    pu1String, i4TableObject);
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfAreaTableInfoInCxt                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays    OspfAreaTable   objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfAreaTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                        UINT4 u4AreaId, UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject;

    /*Ospf Area Status */
    nmhGetFsMIStdOspfAreaStatus (u4OspfCxtId, u4AreaId, &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        /*Area Id */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);

        /* Area's support for importing  AS  external
           link- state advertisements */
        nmhGetFsMIStdOspfImportAsExtern (u4OspfCxtId, u4AreaId, &i4TableObject);
        if (i4TableObject == STUB_AREA)
        {
            /*Ospf Area Summary */
            nmhGetFsMIStdOspfAreaSummary (u4OspfCxtId, u4AreaId,
                                          &i4TableObject);

            /* It seems like sendAreaSummary is the default value as per the code
             * although noAreaSummary is the default value according to the 
             * stdospf mib. This however has to be verified*/

            if (i4TableObject == NO_AREA_SUMMARY)
            {
                *pu4PagingStatus =
                    CliPrintf (CliHandle, "area %s stub no-summary \r\n",
                               pu1String);
                FilePrintf (CliHandle, "area %s stub no-summary \r\n",
                            pu1String);
            }
            else
            {
                *pu4PagingStatus =
                    CliPrintf (CliHandle, "area %s stub \r\n", pu1String);
                FilePrintf (CliHandle, "area %s stub \r\n", pu1String);
            }
        }
        else if (i4TableObject == NSSA_AREA)
        {

            /*Ospf Area Summary */
            nmhGetFsMIStdOspfAreaSummary ((INT4) u4OspfCxtId, u4AreaId,
                                          &i4TableObject);
            if (i4TableObject == SEND_AREA_SUMMARY)
            {
                *pu4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "area %s nssa \r\n",
                                       pu1String);
            }
            else
            {
                *pu4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "area %s nssa no-summary\r\n",
                                       pu1String);
                FilePrintf (CliHandle, "area %s nssa no-summary\r\n",
                            pu1String);
            }

        }

    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfStubAreaTableInfoInCxt                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays  OspfStubAreaTable objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfStubAreaTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                            UINT4 u4AreaId, INT4 i4MetricTOS,
                            UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject;
    INT4                i4MetricValue;
    INT4                i4MetricType;
    INT4                i4DfInfOriginate = 0;
    INT4                i4AreaType = 0;
    /*Ospf Stub Status */
    nmhGetFsMIStdOspfStubStatus (u4OspfCxtId, u4AreaId, i4MetricTOS,
                                 &i4TableObject);
    nmhGetFsMIOspfAreaDfInfOriginate (u4OspfCxtId, u4AreaId, &i4DfInfOriginate);
    /*Area Id */
    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
    nmhGetFsMIStdOspfImportAsExtern (u4OspfCxtId, u4AreaId, &i4AreaType);
    nmhGetFsMIStdOspfStubMetric (u4OspfCxtId, u4AreaId, i4MetricTOS,
                                 &i4MetricValue);
    if (i4DfInfOriginate == DEFAULT_INFO_ORIGINATE)

    {
        if (i4AreaType == NSSA_AREA)
        {
            CliPrintf (CliHandle,
                       "area %s nssa default-information-originate ",
                       pu1String);
            FilePrintf (CliHandle,
                        "area %s nssa default-information-originate ",
                        pu1String);
            /*Ospf Stub Metric */
            if (i4MetricValue != CLI_OSPF_AS_EXT_DEF_METRIC)
            {
                CliPrintf (CliHandle, "metric %d ", i4MetricValue);
                FilePrintf (CliHandle, "metric %d ", i4MetricValue);
            }
        }

        /*Ospf Stub Metric Type */
        nmhGetFsMIStdOspfStubMetricType (u4OspfCxtId, u4AreaId, i4MetricTOS,
                                         &i4TableObject);
        /* Note that if metric type is not mentioned and summary type is
         * send area summary then default metric as per code is comparableCost
         */
        i4MetricType = i4TableObject;
        if (i4TableObject != TYPE1EXT_METRIC)
        {
            CliPrintf (CliHandle, "metric-type %d ", i4MetricType);
            FilePrintf (CliHandle, "metric-type %d ", i4MetricType);

        }

        nmhGetOspfStubStatus (u4AreaId, i4MetricTOS, &i4TableObject);

        if (i4MetricTOS != OSPF_CLI_DEF_TOS)
        {

            if (i4AreaType == NSSA_AREA)
            {

                CliPrintf (CliHandle,
                           "area %s nssa default-information-originate ",
                           pu1String);

                FilePrintf (CliHandle,
                            "area %s nssa default-information-originate ",
                            pu1String);
            }

            CliPrintf (CliHandle, "tos %d ", i4MetricTOS);
            FilePrintf (CliHandle, "tos %d ", i4MetricTOS);
        }

        *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    }
    else if (i4TableObject == ACTIVE)
    {
        if ((i4AreaType == STUB_AREA || i4AreaType == NSSA_AREA)
            && (i4MetricValue != STUB_NSSA_DEFAULT_COST))
        {
            CliPrintf (CliHandle,
                       "area %s default-cost %d\r\n", pu1String, i4MetricValue);
            FilePrintf (CliHandle,
                        "area %s default-cost %d\r\n", pu1String,
                        i4MetricValue);
        }
    }
    /*Note here that there are 2 commands available for setting metric
     * and tos and one command for setting metric type.
     * The commands are listed below :-
     * 
     * *---area <ip_addr> nssa [{ no-summary |default-information-originate 
     * [metric <integer>] [metric-type <integer(1-3)>] 
     * [tos <integer (0-30)>] }]---*
     * 
     * *---area <ip_addr> default-cost <integer> [tos <integer (0-30)>]---*
     * where default-cost implies metric
     * */
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfIfTableInfoInCxt                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays    OspfIfTable     objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfIfTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId, UINT4 u4IpAddr,
                      INT4 i4Index, INT4 IntFlag, UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject = 0;
    INT4                i4TableObject1 = 0;
    UINT4               u4TableObject;
    UINT4               u4IpIfIndex;
    UINT4               u4IfIndex;
    UINT1               au1VlanSwitchName[CFA_CLI_MAX_IF_NAME_LEN];

    /*Ospf Interface Status */
    nmhGetFsMIStdOspfIfStatus (u4OspfCxtId, u4IpAddr, i4Index,
                               (INT4 *) &u4TableObject);
    if (u4TableObject == ACTIVE)
    {
        switch (IntFlag)
        {
            case FALSE:
                /*Interface Ip Address */
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
                CliPrintf (CliHandle, "network %s ", pu1String);
                FilePrintf (CliHandle, "network %s ", pu1String);

                /*Area Id */
                nmhGetFsMIStdOspfIfAreaId (u4OspfCxtId, u4IpAddr, i4Index,
                                           &u4TableObject);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TableObject);
                CliPrintf (CliHandle, "area %s ", pu1String);
                FilePrintf (CliHandle, "area %s ", pu1String);
                UtilOspfGetIfIndex (u4OspfCxtId, u4IpAddr, i4Index,
                                    &u4IpIfIndex);
                u4IfIndex = OspfGetIfIndexFromPort (u4IpIfIndex);

                /*If i4Index is not 0 then its an un numbered interface */
                if ((i4Index != (INT4) 0))
                {
                    if (CfaCliConfGetIfName
                        (u4IfIndex, (INT1 *) au1VlanSwitchName) != OSIX_FAILURE)
                    {
                        *pu4PagingStatus =
                            CliPrintf (CliHandle, "unnum %s\r\n",
                                       au1VlanSwitchName);
                        FilePrintf (CliHandle, "unnum %s\r\n",
                                    au1VlanSwitchName);
                    }
                }
                else
                {
                    *pu4PagingStatus = CliPrintf (CliHandle, " \r\n");
                    FilePrintf (CliHandle, " \r\n");
                }
                break;

            case TRUE:
                /*Interface Type */
                nmhGetFsMIStdOspfIfType (u4OspfCxtId, u4IpAddr, i4Index,
                                         &i4TableObject);
                if (i4TableObject == IF_NBMA)
                {
                    CliPrintf (CliHandle, " ip ospf network non-broadcast\r\n");
                    FilePrintf (CliHandle,
                                " ip ospf network non-broadcast\r\n");
                }
                else if (i4TableObject == IF_PTOP)
                {
                    CliPrintf (CliHandle,
                               " ip ospf network point-to-point\r\n");
                    FilePrintf (CliHandle,
                                " ip ospf network point-to-point\r\n");
                }
                else if (i4TableObject == IF_PTOMP)
                {
                    CliPrintf (CliHandle,
                               " ip ospf network point-to-multipoint\r\n");
                    FilePrintf (CliHandle,
                                " ip ospf network point-to-multipoint\r\n");
                }

                /*Router Priority */
                nmhGetFsMIStdOspfIfRtrPriority (u4OspfCxtId, u4IpAddr, i4Index,
                                                &i4TableObject);
                if (i4TableObject != DEF_RTR_PRIORITY)
                {
                    CliPrintf (CliHandle,
                               " ip ospf priority %d\r\n", i4TableObject);
                    FilePrintf (CliHandle, " ip ospf priority %d\r\n",
                                i4TableObject);
                }
                /*Transit Delay */
                nmhGetFsMIStdOspfIfTransitDelay (u4OspfCxtId, u4IpAddr,
                                                 i4Index, &i4TableObject);
                if (i4TableObject != DEF_IF_TRANS_DELAY)
                {
                    CliPrintf (CliHandle,
                               " ip ospf transmit-delay %d\r\n", i4TableObject);
                    FilePrintf (CliHandle, " ip ospf transmit-delay %d\r\n",
                                i4TableObject);
                }
                /*Retransmit Interval */
                nmhGetFsMIStdOspfIfRetransInterval (u4OspfCxtId, u4IpAddr,
                                                    i4Index, &i4TableObject);
                if (i4TableObject != DEF_RXMT_INTERVAL)
                {
                    CliPrintf (CliHandle,
                               " ip ospf retransmit-interval %d\r\n",
                               i4TableObject);
                    FilePrintf (CliHandle,
                                " ip ospf retransmit-interval %d\r\n",
                                i4TableObject);
                }
                /*Hello Interval */
                nmhGetFsMIStdOspfIfHelloInterval (u4OspfCxtId, u4IpAddr,
                                                  i4Index, &i4TableObject);
                if (i4TableObject != OSPF_DEF_HELLO_INTERVAL)
                {
                    CliPrintf (CliHandle,
                               " ip ospf hello-interval %d\r\n", i4TableObject);
                    FilePrintf (CliHandle, " ip ospf hello-interval %d\r\n",
                                i4TableObject);
                }
                /*Dead Interval */
                nmhGetFsMIStdOspfIfRtrDeadInterval (u4OspfCxtId, u4IpAddr,
                                                    i4Index, &i4TableObject);
                if (i4TableObject != DEF_RTR_DEAD_INTERVAL)
                {
                    CliPrintf (CliHandle,
                               " ip ospf dead-interval %d\r\n", i4TableObject);
                    FilePrintf (CliHandle, " ip ospf dead-interval %d\r\n",
                                i4TableObject);
                }
                /*Demand Circuit */
                nmhGetFsMIStdOspfIfDemand (u4OspfCxtId, u4IpAddr,
                                           i4Index, &i4TableObject);
                if (i4TableObject == OSPF_TRUE)
                {
                    CliPrintf (CliHandle, " ip ospf demand-circuit\r\n");
                    FilePrintf (CliHandle, " ip ospf demand-circuit\r\n");
                }
                nmhGetFsMIOspfIfBfdState ((INT4) u4OspfCxtId, u4IpAddr,
                                          i4Index, &i4TableObject1);
                nmhGetFsMIOspfBfdAllIfState ((INT4) u4OspfCxtId,
                                             &i4TableObject);
                if (i4TableObject == OSPF_BFD_ENABLED)
                {
                    if (i4TableObject1 == OSPF_BFD_DISABLED)
                    {
                        CliPrintf (CliHandle, " ip ospf bfd disable\r\n");
                        FilePrintf (CliHandle, " ip ospf bfd disable\r\n");
                    }
                }
                else
                {
                    if (i4TableObject1 == OSPF_BFD_ENABLED)
                    {
                        CliPrintf (CliHandle, " ip ospf bfd\r\n");
                        FilePrintf (CliHandle, " ip ospf bfd\r\n");
                    }

                }
                break;
            default:
                break;
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FutOspfIfTableInfoInCxt                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays futOspfIfTable     objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FutOspfIfTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                         UINT4 u4IpAddr, INT4 i4Index, UINT4 *pu4PagingStatus)
{
    tInterface         *pInterface;
    UINT4               u4TableObject;
    INT4                i4ScalarObject;
    tIPADDR             ifIpAddr;
    UINT1               au1VlanSwitchName[CFA_CLI_MAX_IF_NAME_LEN];
    tOspfCxt           *pOspfCxt = NULL;

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt == NULL)
    {
        return;
    }

    /*Passive interface */
    /*Checking if all interfaces are passive by default */
    nmhGetFsMIOspfDefaultPassiveInterface (u4OspfCxtId, &i4ScalarObject);
    if (i4ScalarObject != OSPF_TRUE)
    {
        nmhGetFsMIOspfIfPassive (u4OspfCxtId, u4IpAddr, i4Index,
                                 (INT4 *) &u4TableObject);
        if (u4TableObject == OSPF_TRUE)
        {
            OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IpAddr);

            if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                              (UINT4) i4Index)) != NULL)
            {
                u4TableObject = OspfGetIfIndexFromPort (pInterface->u4IfIndex);
                CfaCliConfGetIfName (u4TableObject, (INT1 *) au1VlanSwitchName);
                *pu4PagingStatus =
                    CliPrintf (CliHandle, "passive-interface %s\r\n",
                               au1VlanSwitchName);
                FilePrintf (CliHandle, "passive-interface %s\r\n",
                            au1VlanSwitchName);

            }
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfIfMetricTableInfoInCxt                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays ospfIfMetricTable  objects  */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfIfMetricTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                            UINT4 u4IpAddr, INT4 i4IfIndex, INT4 i4Tos)
{
    INT4                i4TableObject;
    UINT1               u1Flag = FALSE;

    /*Metric Row Status */
    nmhGetFsMIStdOspfIfMetricStatus (u4OspfCxtId, u4IpAddr, i4IfIndex, i4Tos,
                                     &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        /*Metric Value */
        nmhGetFsMIStdOspfIfMetricValue (u4OspfCxtId, u4IpAddr, i4IfIndex,
                                        i4Tos, &i4TableObject);
        if (i4TableObject != 1)
        {
            CliPrintf (CliHandle, " ip ospf cost %d ", i4TableObject);
            FilePrintf (CliHandle, " ip ospf cost %d ", i4TableObject);
            u1Flag = TRUE;
        }
        /*Tos */
        if (i4Tos != TOS_0)
        {
            if (u1Flag == FALSE)
            {
                CliPrintf (CliHandle, " ip ospf cost %d ", i4TableObject);
                FilePrintf (CliHandle, " ip ospf cost %d ", i4TableObject);
                u1Flag = TRUE;
            }
            CliPrintf (CliHandle, "tos %d ", i4Tos);
            FilePrintf (CliHandle, "tos %d ", i4Tos);
        }
        if (u1Flag == TRUE)
        {
            CliPrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfIfAuthInfoInCxt                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays  Ospf Plain Text          */
/*                   Authentication detauls for show running          */
/*                   configuration.                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfIfAuthInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                     UINT4 u4IpAddr, INT4 i4AddrLessIfIndex)
{
    INT4                i4KeyStatus = 0;
    INT4                i4CryptoKeyStatus = 0;
    INT4                i4ResType = 0;
    UINT1               au1AuthKey[MAX_AUTHKEY_LEN + 1];
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt;

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    /*Interface Type */
    nmhGetFsMIStdOspfIfAuthType (u4OspfCxtId, u4IpAddr,
                                 i4AddrLessIfIndex, &i4KeyStatus);
    if (i4KeyStatus == SIMPLE_PASSWORD)
    {
        CliPrintf (CliHandle, "ip ospf authentication simple\r\n");
        FilePrintf (CliHandle, "ip ospf authentication simple\r\n");
    }
    else if (i4KeyStatus == CRYPT_AUTHENTICATION)
    {
        nmhGetFsMIStdOspfIfCryptoAuthType (u4OspfCxtId, u4IpAddr,
                                           i4AddrLessIfIndex,
                                           &i4CryptoKeyStatus);
        switch (i4CryptoKeyStatus)
        {
            case OSPF_AUTH_MD5:

                CliPrintf (CliHandle,
                           "ip ospf authentication message-digest\r\n");
                FilePrintf (CliHandle,
                            "ip ospf authentication message-digest\r\n");
                break;

            case OSPF_AUTH_SHA1:

                CliPrintf (CliHandle, "ip ospf authentication sha-1\r\n");
                FilePrintf (CliHandle, "ip ospf authentication sha-1\r\n");
                break;
            case OSPF_AUTH_SHA2_224:

                CliPrintf (CliHandle, "ip ospf authentication sha-224\r\n");
                FilePrintf (CliHandle, "ip ospf authentication sha-224\r\n");
                break;
            case OSPF_AUTH_SHA2_256:

                CliPrintf (CliHandle, "ip ospf authentication sha-256\r\n");
                FilePrintf (CliHandle, "ip ospf authentication sha-256\r\n");
                break;
            case OSPF_AUTH_SHA2_384:

                CliPrintf (CliHandle, "ip ospf authentication sha-384\r\n");
                FilePrintf (CliHandle, "ip ospf authentication sha-384\r\n");
                break;
            case OSPF_AUTH_SHA2_512:

                CliPrintf (CliHandle, "ip ospf authentication sha-512\r\n");
                FilePrintf (CliHandle, "ip ospf authentication sha-512\r\n");
                break;
            default:

                CliPrintf (CliHandle,
                           "Unknown Cryptographic authentication \r\n");
                break;
        }

    }
    if ((i4KeyStatus != CRYPT_AUTHENTICATION) ||
        ((i4ResType == SET_FLAG) && (MsrGetSaveStatus () == ISS_TRUE)))
    {
        if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
        {
            return;
        }
        pOspfCxt = gOsRtr.pOspfCxt;
        if (pOspfCxt == NULL)
        {
            return;
        }

        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IpAddr);

        if ((pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr,
                                          (UINT4) i4AddrLessIfIndex)) != NULL)
        {
            /* Ospf plain text authentication-key information 
             * will not be displayed for security reasons*/
            MEMSET (au1AuthKey, 0, sizeof (au1AuthKey));
            if (OS_MEM_CMP (pInterface->authKey, au1AuthKey, AUTH_KEY_SIZE)
                != 0)
            {
                MEMCPY (au1AuthKey, pInterface->authKey, AUTH_KEY_SIZE);
                if ((i4ResType == SET_FLAG)
                    && (MsrGetSaveStatus () == ISS_TRUE))
                {
                    CliPrintf (CliHandle, " ip ospf authentication-key %s \r\n",
                               au1AuthKey);
                }
                else
                {
                    CliPrintf (CliHandle, " ip ospf authentication-key \r\n");
                }
                FilePrintf (CliHandle, " ip ospf authentication-key %s \r\n",
                            au1AuthKey);
            }
        }
        UtilOspfResetContext ();
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FutOspfIfMD5AuthTableInfoInCxt                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays  futOspfIfMD5AuthTable      */
/*                        objects for show running configuration.            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FutOspfIfMD5AuthTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                UINT4 u4IpAddr, INT4 i4IfIndex,
                                INT4 i4AuthKeyId, INT4 i4StartAccept,
                                INT4 i4StartGenerate, INT4 i4StopGenerate,
                                INT4 i4StopAccept, INT4 i4CryptoAuthType)
{
    INT4                i4KeyStatus;
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tIPADDR             ifIpAddr;
    tOspfCxt           *pOspfCxt = NULL;
    UINT1               au1AuthKey[MAX_AUTHKEY_LEN + 1];
    UINT1               au1KeyConstantTime[OSPF_DST_TIME_LEN];
    tSNMP_OCTET_STRING_TYPE IfAuthKey;

    MEMSET (au1KeyConstantTime, OSPF_ZERO, OSPF_DST_TIME_LEN);
    MEMSET (au1AuthKey, OSPF_ZERO, sizeof (au1AuthKey));
    MEMSET (&IfAuthKey, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IfAuthKey.pu1_OctetList = au1AuthKey;
    /*MD5 Auth Key Status */
    /* In Case of md5 Authentication Type the key value is stored in
     * the object  futOspfIfMD5AuthKey .This information however 
     * will not be displayed for security reasons*/
    nmhGetFsMIOspfIfMD5AuthKeyStatus (u4OspfCxtId, u4IpAddr, i4IfIndex,
                                      i4AuthKeyId, &i4KeyStatus);
    nmhGetFutOspfIfMD5AuthKey (u4IpAddr, i4IfIndex, i4AuthKeyId, &IfAuthKey);
    if (i4KeyStatus == AUTHKEY_STATUS_VALID)
    {
        switch (i4CryptoAuthType)
        {
            case OSPF_AUTH_MD5:

                CliPrintf (CliHandle, "ip ospf message-digest-key %d "
                           "md5 %s\r\n", i4AuthKeyId, au1AuthKey);
                break;
            case OSPF_AUTH_SHA1:

                CliPrintf (CliHandle, "ip ospf message-digest-key %d "
                           "sha-1 \r\n", i4AuthKeyId);
                break;
            case OSPF_AUTH_SHA2_224:

                CliPrintf (CliHandle, "ip ospf message-digest-key %d "
                           "sha-224 %s\r\n", i4AuthKeyId, au1AuthKey);
                break;
            case OSPF_AUTH_SHA2_256:

                CliPrintf (CliHandle, "ip ospf message-digest-key %d "
                           "sha-256 %s\r\n", i4AuthKeyId, au1AuthKey);
                break;
            case OSPF_AUTH_SHA2_384:

                CliPrintf (CliHandle, "ip ospf message-digest-key %d "
                           "sha-384 %s\r\n", i4AuthKeyId, au1AuthKey);
                break;
            case OSPF_AUTH_SHA2_512:

                CliPrintf (CliHandle, "ip ospf message-digest-key %d "
                           "sha-512 %s\r\n", i4AuthKeyId, au1AuthKey);
                break;
            default:
                break;
        }
        if (i4StartAccept > 0)
        {
            OspfPrintKeyTime (i4StartAccept, au1KeyConstantTime);
            CliPrintf (CliHandle, "ip ospf key %d  start-accept %s ",
                       i4AuthKeyId, au1KeyConstantTime);
        }
        if (i4StartGenerate > 0)
        {
            OspfPrintKeyTime (i4StartGenerate, au1KeyConstantTime);
            CliPrintf (CliHandle, "ip ospf key %d start-generate %s ",
                       i4AuthKeyId, au1KeyConstantTime);
        }

        if (i4StopGenerate > 0)
        {
            OspfPrintKeyTime (i4StopGenerate, au1KeyConstantTime);
            CliPrintf (CliHandle,
                       "ip ospf key %d stop-generate %s ",
                       i4AuthKeyId, au1KeyConstantTime);
        }
        if (i4StopAccept > 0)
        {
            OspfPrintKeyTime (i4StopAccept, au1KeyConstantTime);
            CliPrintf (CliHandle, "ip ospf key %d stop-accept %s ",
                       i4AuthKeyId, au1KeyConstantTime);
        }
        if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
        {
            return;
        }

        pOspfCxt = gOsRtr.pOspfCxt;

        if (pOspfCxt == NULL)
        {
            return;
        }

        OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IpAddr);
        if ((pAuthkeyInfo = GetFindIfAuthkeyInfoInCxt (pOspfCxt,
                                                       ifIpAddr,
                                                       (UINT4)
                                                       i4IfIndex,
                                                       (UINT1)
                                                       i4AuthKeyId)) != NULL)
        {
            MEMSET (au1AuthKey, 0, sizeof (au1AuthKey));
            MEMCPY (au1AuthKey, pAuthkeyInfo->authKey, MAX_AUTHKEY_LEN);
            switch (i4CryptoAuthType)
            {
                case OSPF_AUTH_MD5:

                    FilePrintf (CliHandle, "ip ospf message-digest-key %d "
                                "md5 %s\r\n", i4AuthKeyId, au1AuthKey);
                    break;
                case OSPF_AUTH_SHA1:

                    FilePrintf (CliHandle, "ip ospf message-digest-key %d "
                                "sha-1 %s\r\n", i4AuthKeyId, au1AuthKey);
                    break;
                case OSPF_AUTH_SHA2_224:

                    FilePrintf (CliHandle, "ip ospf message-digest-key %d "
                                "sha-224 %s\r\n", i4AuthKeyId, au1AuthKey);
                    break;
                case OSPF_AUTH_SHA2_256:

                    FilePrintf (CliHandle, "ip ospf message-digest-key %d "
                                "sha-256 %s\r\n", i4AuthKeyId, au1AuthKey);
                    break;
                case OSPF_AUTH_SHA2_384:

                    FilePrintf (CliHandle, "ip ospf message-digest-key %d "
                                "sha-384 %s\r\n", i4AuthKeyId, au1AuthKey);
                    break;
                case OSPF_AUTH_SHA2_512:

                    FilePrintf (CliHandle, "ip ospf message-digest-key %d "
                                "sha-512 %s\r\n", i4AuthKeyId, au1AuthKey);
                    break;
                default:
                    break;
            }
            if (i4StartAccept > 0)
            {
                OspfPrintKeyTime (i4StartAccept, au1KeyConstantTime);
                FilePrintf (CliHandle, "ip ospf key %d start-accept %s ",
                            i4AuthKeyId, au1KeyConstantTime);
            }
            if (i4StartGenerate > 0)
            {
                OspfPrintKeyTime (i4StartGenerate, au1KeyConstantTime);
                FilePrintf (CliHandle, "ip ospf key %d start-generate %s ",
                            i4AuthKeyId, au1KeyConstantTime);
            }
            if (i4StopGenerate > 0)
            {
                OspfPrintKeyTime (i4StopGenerate, au1KeyConstantTime);
                FilePrintf (CliHandle, "ip ospf key %d stop-generate %s ",
                            i4AuthKeyId, au1KeyConstantTime);
            }
            if (i4StopAccept > 0)
            {
                OspfPrintKeyTime (i4StopAccept, au1KeyConstantTime);
                FilePrintf (CliHandle, "ip ospf key %d stop-accept %s ",
                            i4AuthKeyId, au1KeyConstantTime);
            }
        }
        UtilOspfResetContext ();
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfVirtIfTableInfoInCxt                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays futOspfVirtIfMD5AuthTable   */
/*                        and ospfVirtIfTable                                */
/*                        objects for show running configuration.            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfVirtIfTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                          UINT4 u4AreaId, UINT4 u4RouterId,
                          UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT1                i1OutCome;
    INT4                i4TableObject;
    INT4                i4AuthKeyStatus = AUTHKEY_STATUS_INVALID;
    INT4                i4AuthKeyType;
    INT4                i4CryptoAuthType = 0;
    INT4                i4KeyId;
    INT4                i4PrevKeyId;
    UINT4               u4AuthAreaId;
    UINT4               u4PrevAuthAreaId;
    UINT4               u4Neighbor;
    UINT4               u4PrevNeighbor;
    UINT4               u4OspfPrevCxtId;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tAreaId             transitAreaId;
    tRouterId           nbrId;
    INT4                i4StartAccept = 0;
    INT4                i4StartGenerate = 0;
    INT4                i4StopGenerate = 0;
    INT4                i4StopAccept = 0;
    UINT1               au1KeyConstantTime[100];

    MEMSET (au1KeyConstantTime, OSPF_ZERO, 100);
    OSPF_CRU_BMC_DWTOPDU (transitAreaId, u4AreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4RouterId);

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt == NULL)
    {
        /* Invalid context id */
        return;
    }

    nmhGetFsMIStdOspfVirtIfStatus (u4OspfCxtId, u4AreaId, u4RouterId,
                                   &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        nmhGetFsMIStdOspfVirtIfAuthType (u4OspfCxtId, u4AreaId,
                                         u4RouterId, &i4AuthKeyType);

        /*Auth Status */
        u4OspfPrevCxtId = u4OspfCxtId;
        u4PrevAuthAreaId = u4AreaId;
        u4PrevNeighbor = u4RouterId;
        i4PrevKeyId = 0;

        i1OutCome =
            nmhGetFirstIndexFsMIOspfVirtIfMD5AuthTable (((INT4 *) &u4OspfCxtId),
                                                        &u4PrevAuthAreaId,
                                                        &u4PrevNeighbor,
                                                        &i4KeyId);

        while ((i1OutCome != SNMP_FAILURE)
               && (i4AuthKeyType == CRYPT_AUTHENTICATION))
        {
            if ((u4OspfPrevCxtId != u4OspfCxtId) || (i1OutCome == SNMP_FAILURE))
            {
                break;
            }

            nmhGetFsMIOspfVirtIfMD5AuthKeyStartAccept ((INT4) u4OspfCxtId,
                                                       u4AreaId, u4RouterId,
                                                       i4KeyId, &i4StartAccept);
            nmhGetFsMIOspfVirtIfMD5AuthKeyStartGenerate ((INT4) u4OspfCxtId,
                                                         u4AreaId, u4RouterId,
                                                         i4KeyId,
                                                         &i4StartGenerate);
            nmhGetFsMIOspfVirtIfMD5AuthKeyStopGenerate ((INT4) u4OspfCxtId,
                                                        u4AreaId, u4RouterId,
                                                        i4KeyId,
                                                        &i4StopGenerate);
            nmhGetFsMIOspfVirtIfMD5AuthKeyStopAccept ((INT4) u4OspfCxtId,
                                                      u4AreaId, u4RouterId,
                                                      i4KeyId, &i4StopAccept);

            nmhGetFsMIOspfVirtIfMD5AuthKeyStatus (u4OspfCxtId, u4AreaId,
                                                  u4RouterId, i4KeyId,
                                                  &i4AuthKeyStatus);
            /*Area ID */
            CliPrintf (CliHandle, "\r");
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "area %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            nmhGetFsMIStdOspfVirtIfCryptoAuthType (u4OspfCxtId, u4AreaId,
                                                   u4RouterId,
                                                   &i4CryptoAuthType);

            switch (i4CryptoAuthType)
            {
                case OSPF_AUTH_MD5:

                    CliPrintf (CliHandle, "authentication message-digest ");
                    FilePrintf (CliHandle, "authentication message-digest ");
                    break;

                case OSPF_AUTH_SHA1:
                    CliPrintf (CliHandle, "authentication sha-1 ");
                    FilePrintf (CliHandle, "authentication sha-1 ");
                    break;

                case OSPF_AUTH_SHA2_224:
                    CliPrintf (CliHandle, "authentication sha-224 ");
                    FilePrintf (CliHandle, "authentication sha-224 ");
                    break;

                case OSPF_AUTH_SHA2_256:
                    CliPrintf (CliHandle, "authentication sha-256 ");
                    FilePrintf (CliHandle, "authentication sha-256 ");
                    break;

                case OSPF_AUTH_SHA2_384:
                    CliPrintf (CliHandle, "authentication sha-384 ");
                    FilePrintf (CliHandle, "authentication sha-384 ");
                    break;

                case OSPF_AUTH_SHA2_512:
                    CliPrintf (CliHandle, "authentication sha-512 ");
                    FilePrintf (CliHandle, "authentication sha-512 ");
                    break;

                default:
                    break;

            }

            if (i4AuthKeyStatus == AUTHKEY_STATUS_VALID)
                CliPrintf (CliHandle, "message-digest-key %d ", i4KeyId);
            FilePrintf (CliHandle, "message-digest-key %d ", i4KeyId);

            /* In Case of md5 Authentication Type the key value is stored in
             * the object  futOspfVirtIfMD5AuthKey .This information however 
             * will not be displayed for security reasons*/

            switch (i4CryptoAuthType)
            {
                case OSPF_AUTH_MD5:

                    CliPrintf (CliHandle, "md5 \n");
                    FilePrintf (CliHandle, "md5 ");
                    break;

                case OSPF_AUTH_SHA1:
                    CliPrintf (CliHandle, "sha-1 \n");
                    FilePrintf (CliHandle, "sha-1 ");
                    break;

                case OSPF_AUTH_SHA2_224:
                    CliPrintf (CliHandle, "sha-224 \n");
                    FilePrintf (CliHandle, "sha-224 ");
                    break;

                case OSPF_AUTH_SHA2_256:
                    CliPrintf (CliHandle, "sha-256 \n");
                    FilePrintf (CliHandle, "sha-256 ");
                    break;

                case OSPF_AUTH_SHA2_384:
                    CliPrintf (CliHandle, "sha-384 \n");
                    FilePrintf (CliHandle, "sha-384 ");
                    break;

                case OSPF_AUTH_SHA2_512:
                    CliPrintf (CliHandle, "sha-512 \n");
                    FilePrintf (CliHandle, "sha-512 ");
                    break;

                default:
                    break;

            }

            CliPrintf (CliHandle, "\r");
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "area %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "key %d ", i4KeyId);
            FilePrintf (CliHandle, "key %d ", i4KeyId);

            OspfPrintKeyTime (i4StartAccept, au1KeyConstantTime);
            CliPrintf (CliHandle, "start-accept %s ", au1KeyConstantTime);

            CliPrintf (CliHandle, "\r");
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "area %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "key %d ", i4KeyId);
            FilePrintf (CliHandle, "key %d ", i4KeyId);

            OspfPrintKeyTime (i4StartGenerate, au1KeyConstantTime);
            CliPrintf (CliHandle, "start-generate %s ", au1KeyConstantTime);

            if (i4StopGenerate > 0)
            {
                CliPrintf (CliHandle, "\r");
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
                CliPrintf (CliHandle, "area %s ", pu1String);
                FilePrintf (CliHandle, "area %s ", pu1String);

                /*Router Id */
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
                CliPrintf (CliHandle, "virtual-link %s ", pu1String);
                FilePrintf (CliHandle, "virtual-link %s ", pu1String);

                CliPrintf (CliHandle, "key %d", i4KeyId);
                FilePrintf (CliHandle, "key %d", i4KeyId);

                OspfPrintKeyTime (i4StopGenerate, au1KeyConstantTime);
                CliPrintf (CliHandle, " stop-generate %s ", au1KeyConstantTime);
            }
            if (i4StopAccept > 0)
            {
                CliPrintf (CliHandle, "\r");
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
                CliPrintf (CliHandle, "area %s ", pu1String);
                FilePrintf (CliHandle, "area %s ", pu1String);

                /*Router Id */
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
                CliPrintf (CliHandle, "virtual-link %s ", pu1String);
                FilePrintf (CliHandle, "virtual-link %s ", pu1String);

                CliPrintf (CliHandle, "key %d", i4KeyId);
                FilePrintf (CliHandle, "key %d", i4KeyId);

                OspfPrintKeyTime (i4StopAccept, au1KeyConstantTime);
                CliPrintf (CliHandle, " stop-accept %s ", au1KeyConstantTime);
            }

            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevAuthAreaId = u4AreaId;
            u4PrevNeighbor = u4RouterId;
            i4PrevKeyId = i4KeyId;
            i1OutCome =
                nmhGetNextIndexFsMIOspfVirtIfMD5AuthTable ((INT4)
                                                           u4OspfPrevCxtId,
                                                           (INT4 *)
                                                           &u4OspfCxtId,
                                                           u4PrevAuthAreaId,
                                                           &u4AuthAreaId,
                                                           u4PrevNeighbor,
                                                           &u4Neighbor,
                                                           i4PrevKeyId,
                                                           &i4KeyId);

            if ((u4AuthAreaId != u4AreaId) && (u4Neighbor != u4RouterId))
            {
                break;
            }
        }
        if (i4AuthKeyType == NO_AUTHENTICATION)
        {
            /*Area ID */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "\r\narea %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

        }

        if (i4AuthKeyType == SIMPLE_PASSWORD)
        {
            /*Area ID */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "\r\narea %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "authentication simple ");
            FilePrintf (CliHandle, "authentication simple ");
        }

        /*Hello Interval */
        nmhGetFsMIStdOspfVirtIfHelloInterval (u4OspfCxtId, u4AreaId,
                                              u4RouterId, &i4TableObject);
        if (i4TableObject != OSPF_DEF_HELLO_INTERVAL)
        {
            /*Area ID */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "\r\narea %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "hello-interval %d ", i4TableObject);
            FilePrintf (CliHandle, "hello-interval %d ", i4TableObject);
        }

        /*Re-transmit Interval */
        nmhGetFsMIStdOspfVirtIfRetransInterval (u4OspfCxtId, u4AreaId,
                                                u4RouterId, &i4TableObject);
        if (i4TableObject != DEF_RXMT_INTERVAL)
        {
            /*Area ID */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "\r\narea %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "retransmit-interval %d ", i4TableObject);
            FilePrintf (CliHandle, "retransmit-interval %d ", i4TableObject);
        }
        /*Transit Delay */
        nmhGetFsMIStdOspfVirtIfTransitDelay (u4OspfCxtId, u4AreaId,
                                             u4RouterId, &i4TableObject);
        if (i4TableObject != DEF_IF_TRANS_DELAY)
        {
            /*Area ID */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "\r\narea %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "transmit-delay %d ", i4TableObject);
            FilePrintf (CliHandle, "transmit-delay %d ", i4TableObject);
        }
        /*Router Dead Interval */
        nmhGetFsMIStdOspfVirtIfRtrDeadInterval (u4OspfCxtId, u4AreaId,
                                                u4RouterId, &i4TableObject);
        if (i4TableObject != DEF_VIRT_IF_RTR_DEAD_INTERVAL)
        {
            /*Area ID */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AreaId);
            CliPrintf (CliHandle, "\r\narea %s ", pu1String);
            FilePrintf (CliHandle, "area %s ", pu1String);

            /*Router Id */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouterId);
            CliPrintf (CliHandle, "virtual-link %s ", pu1String);
            FilePrintf (CliHandle, "virtual-link %s ", pu1String);

            CliPrintf (CliHandle, "dead-interval %d ", i4TableObject);
            FilePrintf (CliHandle, "dead-interval %d ", i4TableObject);
        }
        if ((pInterface = GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
            != NULL)
        {
            if (STRLEN (pInterface->authKey) != 0)
                CliPrintf (CliHandle, "authentication-key ");
            FilePrintf (CliHandle, "authentication-key ");

        }

        *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");

        FilePrintf (CliHandle, "\r\n");
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfNbrTableInfoInCxt                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays  ospfNbrTable               */
/*                        objects for show running configuration.            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfNbrTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                       UINT4 u4IpAddr, INT4 i4IfIndex, UINT4 *pu4PagingStatus)
{
    INT4                i4TableObject = 0;
    CHR1               *pu1String = NULL;
    UINT1               u1BangStatus = FALSE;

    nmhGetFsMIStdOspfNbmaNbrStatus (u4OspfCxtId, u4IpAddr, i4IfIndex,
                                    &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        /* Checking Whether Nbr is staticaly configured */
        nmhGetFsMIStdOspfNbmaNbrPermanence (u4OspfCxtId, u4IpAddr,
                                            i4IfIndex, &i4TableObject);
        if ((u4OspfCxtId == OSPF_DEFAULT_CXT_ID)
            && (i4TableObject == CONFIGURED_NBR))
        {
            FilePrintf (CliHandle, "! \r\n");
            CliPrintf (CliHandle, "router ospf \r\n");
            FilePrintf (CliHandle, "router ospf \r\n");
            FilePrintf (CliHandle, "\r\n");
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);

            /*Neighbor Priority */
            nmhGetFsMIStdOspfNbrPriority (u4OspfCxtId, u4IpAddr, i4IfIndex,
                                          &i4TableObject);
            CliPrintf (CliHandle, " neighbor %s ", pu1String);
            u1BangStatus = TRUE;
            FilePrintf (CliHandle, " neighbor %s ", pu1String);
            if (i4TableObject != DEF_NBR_PRIORITY)
            {
                CliPrintf (CliHandle, "priority %d ", i4TableObject);
                FilePrintf (CliHandle, "priority %d ", i4TableObject);
            }

            *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");
            if (u1BangStatus == TRUE)
            {
                CliPrintf (CliHandle, "!\r\n");
            }

        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfAreaAggregateTableInfoInCxt                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays                             */
/*                        OspfAreaAggregateTable         objects             */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
OspfAreaAggregateTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                 UINT4 u4SummArea, INT4 i4SummLsaType,
                                 UINT4 u4SummNet, UINT4 u4SummMask,
                                 UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject;

    /*Agg Table Row Status */
    nmhGetFsMIStdOspfAreaAggregateStatus (u4OspfCxtId, u4SummArea,
                                          i4SummLsaType, u4SummNet,
                                          u4SummMask, &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        /*Area ID */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SummArea);
        CliPrintf (CliHandle, "area %s ", pu1String);
        FilePrintf (CliHandle, "area %s ", pu1String);

        /*Network ID */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SummNet);
        CliPrintf (CliHandle, "range %s ", pu1String);
        FilePrintf (CliHandle, "range %s ", pu1String);

        /*Mask */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SummMask);
        CliPrintf (CliHandle, "%s ", pu1String);
        FilePrintf (CliHandle, "%s ", pu1String);

        /*LSA Type */
        if (i4SummLsaType == NSSA_EXTERNAL_LINK)
        {
            CliPrintf (CliHandle, "Type7 ");
            FilePrintf (CliHandle, "Type7 ");
        }
        if (i4SummLsaType == SUMMARY_LINK)
        {
            CliPrintf (CliHandle, "summary ");
            FilePrintf (CliHandle, "summary ");

        }
        /*Aggregate Effect */
        nmhGetFsMIStdOspfAreaAggregateEffect (u4OspfCxtId, u4SummArea,
                                              i4SummLsaType, u4SummNet,
                                              u4SummMask, &i4TableObject);
        if (i4TableObject == DO_NOT_ADV_MATCHING)
        {
            CliPrintf (CliHandle, "not-advertise ");
            FilePrintf (CliHandle, "not-advertise ");
        }
        /*Agg Ext Tag */
        nmhGetFsMIOspfAreaAggregateExternalTag (u4OspfCxtId, u4SummArea,
                                                i4SummLsaType,
                                                u4SummNet, u4SummMask,
                                                &i4TableObject);
        if (i4TableObject != 0)
        {
            CliPrintf (CliHandle, "tag %d ", i4TableObject);
            FilePrintf (CliHandle, "tag %d ", i4TableObject);
        }
        *pu4PagingStatus = CliPrintf (CliHandle, "\r\n", i4TableObject);
        FilePrintf (CliHandle, "\r\n", i4TableObject);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FutOspfAsExternalAggregationTableInfoInCxt         */
/*                                                                           */
/*     DESCRIPTION      : This function displays                             */
/*                        futOspfAsExternalAggregationTable objects          */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FutOspfAsExternalAggregationTableInfoInCxt (tCliHandle CliHandle,
                                            UINT4 u4OspfCxtId, UINT4 u4SummNet,
                                            UINT4 u4SummMask, UINT4 u4SummArea,
                                            UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject;

    /*Ext Agg Table Row Status */
    nmhGetFsMIOspfAsExternalAggregationStatus (u4OspfCxtId, u4SummNet,
                                               u4SummMask,
                                               u4SummArea, &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        /*Network ID */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SummNet);
        CliPrintf (CliHandle, "summary-address %s ", pu1String);
        FilePrintf (CliHandle, "summary-address %s ", pu1String);

        /*Mask */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SummMask);
        CliPrintf (CliHandle, "%s ", pu1String);
        FilePrintf (CliHandle, "%s ", pu1String);

        /*Area ID */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SummArea);
        CliPrintf (CliHandle, "%s ", pu1String);
        FilePrintf (CliHandle, "%s ", pu1String);

        /*Ext Agg Effect */
        nmhGetFsMIOspfAsExternalAggregationEffect (u4OspfCxtId, u4SummNet,
                                                   u4SummMask,
                                                   u4SummArea, &i4TableObject);
        /* Default Value is ADVERTISE hence need not be displayed
         * However the command has to be displayed as there are
         * some mandatory parameters as well*/
        if (i4TableObject == RAG_ALLOW_ALL)
        {
            CliPrintf (CliHandle, "allowAll ");
            FilePrintf (CliHandle, "allowAll ");
        }
        else if (i4TableObject == RAG_DENY_ALL)
        {
            CliPrintf (CliHandle, "denyAll ");
            FilePrintf (CliHandle, "denyAll ");
        }
        else if (i4TableObject == RAG_ADVERTISE)
        {
            CliPrintf (CliHandle, " ");
            FilePrintf (CliHandle, " ");
        }
        else if (i4TableObject == RAG_DO_NOT_ADVERTISE)
        {
            CliPrintf (CliHandle, "not-advertise ");
            FilePrintf (CliHandle, "not-advertise ");
        }
        /*Ext Agg Translation */
        nmhGetFsMIOspfAsExternalAggregationTranslation (u4OspfCxtId,
                                                        u4SummNet, u4SummMask,
                                                        u4SummArea,
                                                        &i4TableObject);
        if (i4TableObject == OSPF_TRUE)
        {
            CliPrintf (CliHandle, "Translation enabled");
            FilePrintf (CliHandle, "Translation enabled");
        }
        *pu4PagingStatus = CliPrintf (CliHandle, " \r\n");
        FilePrintf (CliHandle, " \r\n");
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FutOspfRRDRouteConfigTableInfoInCxt                */
/*                                                                           */
/*     DESCRIPTION      : This function displays                             */
/*                        futOspfRRDRouteConfigTable        objects          */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FutOspfRRDRouteConfigTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                     UINT4 u4RouteDest, UINT4 u4RouteMask,
                                     UINT4 *pu4PagingStatus)
{
    CHR1               *pu1String = NULL;
    INT4                i4TableObject;
    UINT4               u4TagValue = 0;

    /*Ext Agg Table Row Status */
    nmhGetFsMIOspfRRDRouteStatus (u4OspfCxtId, u4RouteDest, u4RouteMask,
                                  &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        /*Dest IP */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouteDest);
        CliPrintf (CliHandle, "redist-config %s ", pu1String);
        FilePrintf (CliHandle, "redist-config %s ", pu1String);

        /*Dest Mask */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4RouteMask);
        CliPrintf (CliHandle, "%s ", pu1String);
        FilePrintf (CliHandle, "%s ", pu1String);

        /*RRD metric */
        nmhGetFsMIOspfRRDRouteMetric (u4OspfCxtId, u4RouteDest,
                                      u4RouteMask, &i4TableObject);
        if (i4TableObject != AS_EXT_DEF_METRIC)
        {
            CliPrintf (CliHandle, "metric-value %d ", i4TableObject);
            FilePrintf (CliHandle, "metric-value %d ", i4TableObject);
        }
        /*RRD metric type */
        nmhGetFsMIOspfRRDRouteMetricType (u4OspfCxtId, u4RouteDest,
                                          u4RouteMask, &i4TableObject);
        if (i4TableObject == TYPE_1_METRIC)
        {
            CliPrintf (CliHandle, "metric-type asExttype1 ");
            FilePrintf (CliHandle, "metric-type asExttype1 ");
        }
        /*RRD Route Tag */
        nmhGetFsMIOspfRRDRouteTag (u4OspfCxtId, u4RouteDest, u4RouteMask,
                                   &u4TagValue);
        if (u4TagValue != DEFAULT_EXTRT_TAG)
        {
            *pu4PagingStatus = CliPrintf (CliHandle, "tag %u\r\n", u4TagValue);
            FilePrintf (CliHandle, "tag %d\r\n", u4TagValue);
        }

        else
        {
            FilePrintf (CliHandle, "\r\n");
            *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FutOspfExtRouteTableInfoInCxt                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays                             */
/*                        futOspfExtRouteTable              objects          */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
FutOspfExtRouteTableInfoInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                               UINT4 u4RouteDest, UINT4 u4RouteMask,
                               INT4 i4RouteTos, UINT4 *pu4PagingStatus)
{
    INT4                i4TableObject;

    /*Ext Route Table Row Status */
    nmhGetFsMIOspfExtRouteStatus (u4OspfCxtId, u4RouteDest,
                                  u4RouteMask, i4RouteTos, &i4TableObject);
    if ((i4TableObject == ACTIVE) && (u4RouteDest == IP_ANY_ADDR) &&
        (u4RouteMask == IP_ANY_ADDR))
    {
        CliPrintf (CliHandle, "default-information originate always ");
        FilePrintf (CliHandle, "default-information originate always ");
        /*Route metric */
        nmhGetFsMIOspfExtRouteMetric (u4OspfCxtId, u4RouteDest, u4RouteMask,
                                      i4RouteTos, &i4TableObject);
        if (i4TableObject != CLI_OSPF_AS_EXT_DEF_METRIC)
        {
            CliPrintf (CliHandle, "metric %d ", i4TableObject);
            FilePrintf (CliHandle, "metric %d ", i4TableObject);
        }
        /*Route metric type */
        nmhGetFsMIOspfExtRouteMetricType (u4OspfCxtId, u4RouteDest,
                                          u4RouteMask, i4RouteTos,
                                          &i4TableObject);
        /* Default Value as per Mib is asexttype1
         * and that per Code is asexttype2
         */
        if (i4TableObject != TYPE_2_METRIC)
        {
            *pu4PagingStatus = CliPrintf (CliHandle, "metric-type 1\r\n");
            FilePrintf (CliHandle, "metric-type 1\r\n");
        }
        else

            *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
        FilePrintf (CliHandle, "\r\n");
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfCliFilterLsaSummaryInfo                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays summary of ospf LSA database*/
/*                        in a particular area*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        pArea     - Ospf Area of which database needs to be*/
/*                                    displayed                              */
/*                        i4Option  - Option for filtering the display       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*****************************************************************************/
INT4
OspfCliFilterLsaSummaryInfo (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                             UINT4 u4AreaId, INT4 i4Option)
{
    INT4                i4HeaderFlag;
    INT4                i4PageStatus;
    INT4                i4LsaType;
    tLsaInfo           *pLsaInfo;
    tLsaInfo            LsaInfo;

    i4HeaderFlag = CLI_FAILURE;
    i4PageStatus = CLI_SUCCESS;
    for (i4LsaType = ROUTER_LSA; i4LsaType <= MAX_LSA_TYPE; i4LsaType++)
    {
        /* get first lsa with context, area, plsa and lsatype as input */
        /*if success, print and proceed to get next */
        i4HeaderFlag = CLI_FAILURE;
        pLsaInfo = GetFirstLsaBytype (u4OspfCxtId, u4AreaId, i4LsaType);
        if (pLsaInfo == NULL)
        {
            continue;
        }
        do
        {
            MEMCPY (&LsaInfo, pLsaInfo, sizeof (tLsaInfo));
            if (LsaInfo.lsaId.u1LsaType == i4LsaType)
            {
                i4PageStatus =
                    OspfCliFilterLsaSummary (CliHandle, &LsaInfo,
                                             i4Option, &i4HeaderFlag);
                if (i4PageStatus == CLI_FAILURE)
                    break;
            }
        }
        while ((pLsaInfo = GetNextLsaBytype (u4OspfCxtId,
                                             u4AreaId, &LsaInfo,
                                             i4LsaType)) != NULL);

        if (i4PageStatus == CLI_FAILURE)
            break;
    }
    return i4PageStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfCliFilterLsaSummary                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays summary of ospf LSA         */
/*                        database                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        pLsaInfo     - Pointer to Lsa information data     */
/*                        i4Option  - Option for filtering the display       */
/*                        pi4HeaderFlad  - Specifies whether the header      */
/*                                         for the output is printed         */
/*                                                                           */
/*     OUTPUT           : pi4HeaderFlag - updated to CLI_SUCCESS, if header  */
/*                                        is printed here                    */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
OspfCliFilterLsaSummary (tCliHandle CliHandle, tLsaInfo * pLsaInfo,
                         INT4 i4Option, INT4 *pi4HeaderFlag)
{
    INT4                i4PageStatus = CLI_SUCCESS;
    INT4                i4LsaType;

    i4LsaType = pLsaInfo->lsaId.u1LsaType;
    switch (i4Option)
    {
        case 7:
            if (*pi4HeaderFlag == CLI_FAILURE)
            {
                OspfCliPrintLsaSummaryHeader (CliHandle, pLsaInfo, i4LsaType);
                *pi4HeaderFlag = CLI_SUCCESS;
            }
            i4PageStatus = OspfCliPrintLsaSummary (CliHandle, pLsaInfo);
            break;
        case 8:
            if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, LS_ROUTER_ID) ==
                OSPF_EQUAL)
            {
                if (*pi4HeaderFlag == CLI_FAILURE)
                {
                    OspfCliPrintLsaSummaryHeader (CliHandle,
                                                  pLsaInfo, i4LsaType);
                    *pi4HeaderFlag = CLI_SUCCESS;
                }
                i4PageStatus = OspfCliPrintLsaSummary (CliHandle, pLsaInfo);
            }
            break;
        case 9:
            if (UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, LS_ADV_ROUTER_ID) ==
                OSPF_EQUAL)
            {
                if (*pi4HeaderFlag == CLI_FAILURE)
                {
                    OspfCliPrintLsaSummaryHeader (CliHandle,
                                                  pLsaInfo, i4LsaType);
                    *pi4HeaderFlag = CLI_SUCCESS;
                }
                i4PageStatus = OspfCliPrintLsaSummary (CliHandle, pLsaInfo);
            }
            break;
        default:
            break;
    }
    return i4PageStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfCliPrintLsaSummaryHeader                       */
/*                                                                           */
/*     DESCRIPTION      : This function prints the header for database       */
/*                        display                                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        pLsaInfo     - Pointer to Lsa information data     */
/*                        i4LsaType  - Type of the LSA                       */
/*                                                                           */
/*     OUTPUT           : pi4HeaderFlag - updated to CLI_SUCCESS, if header  */
/*                                        is printed here                    */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
OspfCliPrintLsaSummaryHeader (tCliHandle CliHandle, tLsaInfo * pLsaInfo,
                              INT4 i4LsaType)
{
    CHR1               *pu1String = NULL;

    if (pLsaInfo->pArea != NULL)
    {
        OSPF_CLI_IPADDR_TO_STR (pu1String,
                                OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->pArea->
                                                        areaId));
    }
    switch (i4LsaType)
    {
        case ROUTER_LSA:
            CliPrintf (CliHandle, "\r                  Router Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  -------------------"
                       "--------------------\r\n");
            break;
        case NETWORK_LSA:
            CliPrintf (CliHandle, "\r                  Network Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  -------------------"
                       "--------------------\r\n");
            break;
        case NETWORK_SUM_LSA:
            CliPrintf (CliHandle, "\r                  Summary Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  -------------------"
                       "--------------------\r\n");
            break;
        case ASBR_SUM_LSA:
            CliPrintf (CliHandle,
                       "\r                  ASBR Summary Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  ------------------------"
                       "--------------------\r\n");
            break;
        case AS_EXT_LSA:
            CliPrintf (CliHandle,
                       "\r                  AS External Link States \r\n");
            CliPrintf (CliHandle,
                       "                  -----------------------\r\n");
            break;
        case NSSA_LSA:
            CliPrintf (CliHandle,
                       "\r                  NSSA External Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  --------------------------"
                       "--------------------\r\n");
            break;
        case TYPE9_OPQ_LSA:
            CliPrintf (CliHandle,
                       "\r               Opaque Link Area Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "               -----------------------------"
                       "--------------------\r\n");
            break;
        case TYPE10_OPQ_LSA:
            CliPrintf (CliHandle,
                       "\r                  Opaque Area Link States ");
            CliPrintf (CliHandle, "(Area %s)\r\n", pu1String);
            CliPrintf (CliHandle, "                  ------------------------"
                       "--------------------\r\n");
            break;
        case TYPE11_OPQ_LSA:
            CliPrintf (CliHandle,
                       "\r                  Opaque AS Link States\r\n");
            CliPrintf (CliHandle,
                       "                  ---------------------\r\n");
            break;
        default:
            CliPrintf (CliHandle,
                       "\r                                 Link States \r\n");
            CliPrintf (CliHandle,
                       "                                 -----------\r\n");
            break;
    }
    if (i4LsaType == ROUTER_LSA)
    {
        CliPrintf (CliHandle, "%-16s %-16s %-10s %-13s %-10s%-10s\r\n",
                   "Link ID", "ADV Router", "Age", "Seq#", "Checksum",
                   "Link count");
        CliPrintf (CliHandle, "%-16s %-16s %-10s %-13s %-10s%-10s\r\n",
                   "-------", "----------", "---", "----", "--------",
                   "----------");
    }
    else
    {
        CliPrintf (CliHandle, "%-16s %-16s %-10s %-13s %-10s\r\n",
                   "Link ID", "ADV Router", "Age", "Seq#", "Checksum");
        CliPrintf (CliHandle, "%-16s %-16s %-10s %-13s %-10s\r\n",
                   "-------", "----------", "---", "----", "--------");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current interface specific  */
/*                        configurations in OSPF                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
OspfShowRunningConfigInterfaceDetails (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT1                i1OutCome;
    INT4                i4AuthKeyId;
    INT4                i4PrevAuthKeyId;
    INT4                i4Tos;
    INT4                i4PrevTos;
    INT4                i4AddrLessIfIndex;
    INT4                i4AddrLessIndex;
    INT4                i4PrevAddrLessIfIndex;
    INT4                i4GlobalAdmin;
    UINT4               u4IpAddr;
    UINT4               u4PrevIpAddr;
    UINT4               u4IfIpAddr;
    UINT4               u4PagingStatus;
    UINT4               u4OspfCxtId;
    UINT4               u4OspfPrevCxtId;
    INT4                i4TableObject = 0;
    INT4                i4StartAccept = 0;
    INT4                i4StartGenerate = 0;
    INT4                i4StopGenerate = 0;
    INT4                i4StopAccept = 0;
    INT4                i4CryptoAuthType = 0;
    UINT1               au1VlanSwitchName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1BangStatus = FALSE;

    CliRegisterLock (CliHandle, OspfLock, OspfUnLock);
    OspfLock ();
    if (OspfCliGetCxtIdFromIfIndex (CliHandle, u4IfIndex, &u4OspfCxtId)
        == CLI_FAILURE)
    {
        OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsMIStdOspfAdminStat ((INT4) u4OspfCxtId, &i4GlobalAdmin)
        == SNMP_FAILURE)
    {
        OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if (i4GlobalAdmin != OSPF_ENABLED)
    {
        OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    /* Getting the corresponding IP address and the addressless interface 
     * for the given cfa interface index*/
    if (OspfCliGetIndicesFromIfIndex (u4IfIndex, &u4IfIpAddr,
                                      &i4AddrLessIfIndex) == OSIX_FAILURE)
    {
        OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    nmhGetFsMIStdOspfIfStatus ((INT4) u4OspfCxtId, u4IfIpAddr,
                               i4AddrLessIfIndex, &i4TableObject);
    if (i4TableObject == ACTIVE)
    {
        CfaCliConfGetIfName (u4IfIndex, (INT1 *) au1VlanSwitchName);
        u4PagingStatus = CliPrintf (CliHandle,
                                    "interface %s \r\n", au1VlanSwitchName);
        FilePrintf (CliHandle, "interface %s \r\n", au1VlanSwitchName);
        u1BangStatus = TRUE;
        if (u4PagingStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    /* ospfIfTable */
    OspfIfTableInfoInCxt (CliHandle, u4OspfCxtId, u4IfIpAddr,
                          i4AddrLessIfIndex, TRUE, &u4PagingStatus);
    /*futOspfIfMD5AuthTable */
    u4OspfPrevCxtId = u4OspfCxtId;
    u4IpAddr = 0;
    i4AddrLessIfIndex = 0;
    i4AuthKeyId = 0;

    i4AddrLessIndex = i4AddrLessIfIndex;
    i1OutCome = nmhGetNextIndexFsMIOspfIfMD5AuthTable ((INT4)
                                                       u4OspfPrevCxtId,
                                                       (INT4 *)
                                                       &u4OspfCxtId,
                                                       u4IfIpAddr,
                                                       &u4IpAddr,
                                                       i4AddrLessIndex,
                                                       &i4AddrLessIfIndex,
                                                       (-1), &i4AuthKeyId);
    nmhGetFsMIOspfIfMD5AuthKeyStartAccept ((INT4) u4OspfCxtId,
                                           u4IfIpAddr, i4AddrLessIndex,
                                           i4AuthKeyId, &i4StartAccept);
    nmhGetFsMIOspfIfMD5AuthKeyStartGenerate ((INT4) u4OspfCxtId,
                                             u4IfIpAddr, i4AddrLessIndex,
                                             i4AuthKeyId, &i4StartGenerate);
    nmhGetFsMIOspfIfMD5AuthKeyStopGenerate ((INT4) u4OspfCxtId,
                                            u4IfIpAddr, i4AddrLessIndex,
                                            i4AuthKeyId, &i4StopGenerate);
    nmhGetFsMIOspfIfMD5AuthKeyStopAccept ((INT4) u4OspfCxtId,
                                          u4IfIpAddr, i4AddrLessIndex,
                                          i4AuthKeyId, &i4StopAccept);

    nmhGetFsMIStdOspfIfCryptoAuthType ((INT4) u4OspfCxtId,
                                       u4IfIpAddr, i4AddrLessIndex,
                                       &i4CryptoAuthType);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            break;
        }

        if (u4IfIpAddr == u4IpAddr)
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevIpAddr = u4IpAddr;
            i4PrevAddrLessIfIndex = i4AddrLessIfIndex;
            i4PrevAuthKeyId = i4AuthKeyId;

            FutOspfIfMD5AuthTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                            i4AddrLessIfIndex, i4AuthKeyId,
                                            i4StartAccept, i4StartGenerate,
                                            i4StopGenerate, i4StopAccept,
                                            i4CryptoAuthType);

            i1OutCome = nmhGetNextIndexFsMIOspfIfMD5AuthTable ((INT4)
                                                               u4OspfPrevCxtId,
                                                               (INT4 *)
                                                               &u4OspfCxtId,
                                                               u4PrevIpAddr,
                                                               &u4IpAddr,
                                                               i4PrevAddrLessIfIndex,
                                                               &i4AddrLessIfIndex,
                                                               i4PrevAuthKeyId,
                                                               &i4AuthKeyId);
            if (u4OspfCxtId != u4OspfPrevCxtId)
            {
                u4OspfCxtId = u4OspfPrevCxtId;
                break;
            }

            nmhGetFsMIOspfIfMD5AuthKeyStartAccept ((INT4) u4OspfCxtId,
                                                   u4IfIpAddr, i4AddrLessIndex,
                                                   i4AuthKeyId, &i4StartAccept);
            nmhGetFsMIOspfIfMD5AuthKeyStartGenerate ((INT4) u4OspfCxtId,
                                                     u4IfIpAddr,
                                                     i4AddrLessIndex,
                                                     i4AuthKeyId,
                                                     &i4StartGenerate);
            nmhGetFsMIOspfIfMD5AuthKeyStopGenerate ((INT4) u4OspfCxtId,
                                                    u4IfIpAddr, i4AddrLessIndex,
                                                    i4AuthKeyId,
                                                    &i4StopGenerate);
            nmhGetFsMIOspfIfMD5AuthKeyStopAccept ((INT4) u4OspfCxtId,
                                                  u4IfIpAddr, i4AddrLessIndex,
                                                  i4AuthKeyId, &i4StopAccept);

            nmhGetFsMIStdOspfIfCryptoAuthType ((INT4) u4OspfCxtId,
                                               u4IfIpAddr, i4AddrLessIndex,
                                               &i4CryptoAuthType);

        }
        else
        {
            break;
        }

    }

    OspfIfAuthInfoInCxt (CliHandle, u4OspfCxtId, u4IfIpAddr, i4AddrLessIfIndex);
    /*ospfIfMetricTable */

    /* Here We are using SI nmhGetFirstIndex routine for Getting 1st index instead 
     * of MI nmhGetNextIndex routine.Because in case when 1st Index is have value
     * 0 , then it will not return this index but next index to it.*/
    u4OspfCxtId = u4OspfPrevCxtId;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i1OutCome = nmhGetFirstIndexOspfIfMetricTable (&u4IpAddr,
                                                   &i4AddrLessIfIndex, &i4Tos);
    UtilOspfResetContext ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4OspfPrevCxtId != u4OspfCxtId)
        {
            break;
        }

        if (u4IfIpAddr == u4IpAddr)
        {
            OspfIfMetricTableInfoInCxt (CliHandle, u4OspfCxtId, u4IpAddr,
                                        i4AddrLessIfIndex, i4Tos);
            break;
        }
        else
        {
            u4OspfPrevCxtId = u4OspfCxtId;
            u4PrevIpAddr = u4IpAddr;
            i4PrevAddrLessIfIndex = i4AddrLessIfIndex;
            i4PrevTos = i4Tos;

            i1OutCome =
                nmhGetNextIndexFsMIStdOspfIfMetricTable ((INT4) u4OspfPrevCxtId,
                                                         (INT4 *) &u4OspfCxtId,
                                                         u4PrevIpAddr,
                                                         &u4IpAddr,
                                                         i4PrevAddrLessIfIndex,
                                                         &i4AddrLessIfIndex,
                                                         i4PrevTos, &i4Tos);
        }
    }
    if (u1BangStatus == TRUE)
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    OspfUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfShowRunningConfigInterfaceInCxt                */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
OspfShowRunningConfigInterfaceInCxt (tCliHandle CliHandle, tOspfCxt * pOspfCxt)
{
    tInterface         *pInterface;
    tTMO_SLL           *pLst;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4IfIndex;
    INT4                i4PagingStatus = CLI_SUCCESS;

    /* List of all interfaces in the order of the IP address and 
     * the addressless index is stored in the structure "pOspfCxt->sortIfLst".
     */

    pLst = &(pOspfCxt->sortIfLst);

    TMO_SLL_Scan (pLst, pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);

        OspfUnLock ();
        CliUnRegisterLock (CliHandle);

        OspfShowRunningConfigInterfaceDetails (CliHandle, u4IfIndex);

        CliRegisterLock (CliHandle, OspfLock, OspfUnLock);
        OspfLock ();

        i4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if (i4PagingStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        FilePrintf (CliHandle, "exit\r\n");
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : OspfSetRouteDistance                                  */
/*                                                                         */
/*   Description   : Sets distance value                                   */
/*                                                                         */
/*   Input(s)      :  1. CliHandle                                         */
/*                    2. i4Distance  - Distance value                      */
/*                    3. pu1RMapName - route map name                      */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
OspfSetRouteDistance (tCliHandle CliHandle, INT4 i4Distance, UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_OSPF_INV_ASSOC_RMAP;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
        {
            return (CLI_FAILURE);
        }
        else
        {
            MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                    RouteMapName.i4_Length);
        }
        if (nmhGetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                     FILTERING_TYPE_DISTANCE,
                                                     &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            if (nmhTestv2FutOspfDistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhSetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                             FILTERING_TYPE_DISTANCE,
                                                             CREATE_AND_WAIT) ==
                    SNMP_SUCCESS)
                {
                    u4ErrorCode = SNMP_ERR_NO_ERROR;
                }
            }
            else
            {
                u4CliError = CLI_OSPF_RMAP_ASSOC_FAILED;
            }
        }
        else
        {
            u4ErrorCode = SNMP_ERR_NO_ERROR;
        }

        if (SNMP_ERR_NO_ERROR == u4ErrorCode)
        {
            if (nmhTestv2FutOspfDistInOutRouteMapValue
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 i4Distance) == SNMP_SUCCESS)
            {
                if (nmhSetFutOspfDistInOutRouteMapValue (&RouteMapName,
                                                         FILTERING_TYPE_DISTANCE,
                                                         i4Distance) ==
                    SNMP_SUCCESS)
                {
                    if (nmhTestv2FutOspfDistInOutRouteMapRowStatus
                        (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         ACTIVE) == SNMP_SUCCESS)
                    {
                        if (nmhSetFutOspfDistInOutRouteMapRowStatus
                            (&RouteMapName, FILTERING_TYPE_DISTANCE,
                             ACTIVE) == SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FutOspfPreferenceValue (&u4ErrorCode, i4Distance) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFutOspfPreferenceValue (i4Distance) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4CliError = CLI_OSPF_INV_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : OspfSetNoRouteDistance                                */
/*                                                                         */
/*   Description   :                                                       */
/*                                                                         */
/*   Input(s)      :  1. CliHandle                                         */
/*                    2. pu1RMapName - route map name                      */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
OspfSetNoRouteDistance (tCliHandle CliHandle, UINT1 *pu1RMapName)
{
    UINT4               u4CliError = CLI_OSPF_INV_DISASSOC_RMAP;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4RowStatus;
    INT4                i4Distance = 0;
    INT1                i1RetVal = 0;

    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
        {
            return (CLI_FAILURE);
        }
        else
        {

            MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                    RouteMapName.i4_Length);
        }
        if (nmhGetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                     FILTERING_TYPE_DISTANCE,
                                                     &i4RowStatus) ==
            SNMP_SUCCESS)
        {
            if (nmhTestv2FutOspfDistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 DESTROY) == SNMP_SUCCESS)
            {
                if (nmhSetFutOspfDistInOutRouteMapRowStatus (&RouteMapName,
                                                             FILTERING_TYPE_DISTANCE,
                                                             DESTROY) ==
                    SNMP_SUCCESS)
                {
                    i1RetVal = nmhGetFutOspfPreferenceValue (&i4Distance);
                    UNUSED_PARAM (i1RetVal);
                    printf ("i4Distance %d\r\n", i4Distance);
                    if (nmhTestv2FutOspfPreferenceValue
                        (&u4ErrorCode, i4Distance) == SNMP_SUCCESS)
                    {
                        if (nmhSetFutOspfPreferenceValue (i4Distance) ==
                            SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
        else
        {
            u4CliError = CLI_OSPF_INV_ASSOC_RMAP;
        }
    }
    else
    {
        if (nmhTestv2FutOspfPreferenceValue (&u4ErrorCode, 0) == SNMP_SUCCESS)
        {
            if (nmhSetFutOspfPreferenceValue (0) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4CliError = CLI_OSPF_INV_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/******************************************************************************/
/* Function Name     : OspfCliShowReqLstForAllVlanInCxt                       */
/*                                                                            */
/* Description       : This Routine will display all Link State               */
/*                     Advertisements requested by the router                 */
/*                     for all ospf interfaces in all the context with the    */
/*                     if name passed in the input "pu1IfName"                */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4OspfCxtId - Ospf context id                          */
/*                     u4IfIndex  - Interface Index                           */
/*                     u4NbrId    - Neighbor ID                               */
/*                     i4ShowOption - Display Option                          */
/*                     pu1IfName - Interface name                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowReqLstForAllVlanInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                  UINT4 u4IfIndex, UINT4 u4NbrId,
                                  INT4 i4ShowOption, UINT1 *pu1IfName)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4OspfIfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4ShowAllCxt = OSPF_FALSE;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        /* Get the Context id from IfIndex */
        i4RetVal = OspfCliGetCxtIdFromIfIndex
            (CliHandle, u4IfIndex, &u4OspfIfCxtId);

        if (u4ShowAllCxt == OSPF_TRUE)
        {
            u4OspfCxtId = u4OspfIfCxtId;
        }

        i4RetVal = OspfCliShowRequestListInCxt
            (CliHandle, u4OspfCxtId, u4IfIndex, u4NbrId, i4ShowOption);

        if (((u4ShowAllCxt == OSPF_FALSE) &&
             (u4OspfCxtId == u4OspfIfCxtId)) || (i4RetVal == CLI_FAILURE))

        {
            return i4RetVal;
        }
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name     : OspfCliShowRxmtLtForAllVlanInCxt                       */
/*                                                                            */
/* Description       : This Routine will display all Link State               */
/*                     Advertisements waiting to be retransmitted             */
/*                     for all ospf interfaces in all the context with the    */
/*                     if name passed in the input "pu1IfName"                */
/*                                                                            */
/* Input Parameters  : CliHandle    - CliContext ID                           */
/*                     u4OspfCxtId  - ospf context id                         */
/*                     u4IfIndex    - Interface Index                         */
/*                     u4NbrId      - Neighbor ID                             */
/*                     i4ShowOption - Show option                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliShowRxmtLtForAllVlanInCxt (tCliHandle CliHandle, UINT4 u4OspfCxtId,
                                  UINT4 u4IfIndex, UINT4 u4NbrId,
                                  INT4 i4ShowOption, UINT1 *pu1IfName)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4OspfIfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4ShowAllCxt = OSPF_FALSE;

    if (u4OspfCxtId == OSPF_INVALID_CXT_ID)
    {
        u4ShowAllCxt = OSPF_TRUE;
    }
    while (CfaCliGetNextIfIndexFromName (pu1IfName, u4IfIndex,
                                         &u4NextIfIndex) != OSIX_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        /* Get the Context id from IfIndex */
        i4RetVal = OspfCliGetCxtIdFromIfIndex
            (CliHandle, u4IfIndex, &u4OspfIfCxtId);

        if (u4ShowAllCxt == OSPF_TRUE)
        {
            u4OspfCxtId = u4OspfIfCxtId;
        }

        i4RetVal = OspfCliShowRxmtListInCxt
            (CliHandle, u4OspfCxtId, u4IfIndex, u4NbrId, i4ShowOption);

        if (((u4ShowAllCxt == OSPF_FALSE) &&
             (u4OspfCxtId == u4OspfIfCxtId)) || (i4RetVal == CLI_FAILURE))

        {
            return i4RetVal;
        }
    }
    return i4RetVal;
}

/******************************************************************************/
/* Function Name     : OspfCliSetSpfTimers                                    */
/*                                                                            */
/* Description       : This function sets SPF algorithm timers                */
/*                                                                            */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                     i4OspfSpfHold - Spf Hold timer                         */
/*                     i4OspfSpfDelay - Spf Delay timer                       */
/*                                                                            */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetSpfTimers (tCliHandle CliHandle, INT4 i4OspfSpfHold,
                     INT4 i4OspfSpfDelay)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FutOspfSpfHoldtime (&u4ErrorCode, i4OspfSpfHold) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2FutOspfSpfDelay (&u4ErrorCode, i4OspfSpfDelay) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfSpfHoldtime (i4OspfSpfHold) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhSetFutOspfSpfDelay (i4OspfSpfDelay) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliResetSpfTimers                                  */
/*                                                                            */
/* Description       : This function resets SPF algorithm timers              */
/*                     Set Hold timer to Default value 10                     */
/*                     Set Delay timer to Default value 1                     */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliResetSpfTimers (tCliHandle CliHandle)
{
    INT4                i4OspfSpfHold;
    INT4                i4OspfSpfDelay;

    i4OspfSpfHold = OSPF_DEF_SPF_HOLDTIME;    /* default values */
    i4OspfSpfDelay = OSPF_DEF_SPF_DELAY;

    if ((OspfCliSetSpfTimers (CliHandle, i4OspfSpfHold, i4OspfSpfDelay))
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : CliPrintSwiCapType                                  */
/*                                                                        */
/*  Description     : Maps and Prints the Switching capability with the   */
/*                    given input                                         */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u1TlvVal1 - value corresponds to switching -        */
/*                    capability                                          */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
CliPrintSwiCapType (tCliHandle CliHandle, UINT1 u1TlvVal1)
{
    switch (u1TlvVal1)
    {
        case OSPF_TYPE10_PSC1:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-1 ");
            break;
        case OSPF_TYPE10_PSC2:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-2 ");
            break;
        case OSPF_TYPE10_PSC3:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-3 ");
            break;
        case OSPF_TYPE10_PSC4:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-4 ");
            break;
        case OSPF_TYPE10_L2SC:
            CliPrintf (CliHandle, "     Switching Cap      : L2SC ");
            break;
        case OSPF_TYPE10_TDM:
            CliPrintf (CliHandle, "     Switching Cap      : TDM ");
            break;
        case OSPF_TYPE10_LSC:
            CliPrintf (CliHandle, "     Switching Cap      : LSC ");
            break;
        case OSPF_TYPE10_FSC:
            CliPrintf (CliHandle, "     Switching Cap      : FSC ");
            break;
        default:
            break;
    }
}

/**************************************************************************/
/*  Function Name   : CliPrintMetricAndMetricType                         */
/*                                                                        */
/*  Description     : prints the matched metric value and its metric type */
/*                    for the given input                                 */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4ProtoIndex - Index for the array corresponds      */
/*                    Protocol -                                          */
/*                    u4OspfCxtId - ospf context id                       */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
CliPrintMetricAndMetricType (tCliHandle CliHandle, INT4 i4ProtoIndex,
                             UINT4 u4OspfCxtId)
{
    INT4                i4ObjectValue = 0;

    if (i4ProtoIndex == 0)
    {
        return;
    }
    nmhGetFsMIOspfRRDMetricValue (u4OspfCxtId, i4ProtoIndex, &i4ObjectValue);
    if (i4ObjectValue != AS_EXT_DEF_METRIC)
    {
        CliPrintf (CliHandle, "metric %d ", i4ObjectValue);
        FilePrintf (CliHandle, "metric %d ", i4ObjectValue);
    }
    nmhGetFsMIOspfRRDMetricType (u4OspfCxtId, i4ProtoIndex, &i4ObjectValue);
    if (i4ObjectValue != TYPE_2_METRIC)
    {
        CliPrintf (CliHandle, "metric-type %d", i4ObjectValue);
        FilePrintf (CliHandle, "metric-type %d", i4ObjectValue);
    }
    CliPrintf (CliHandle, "\r\n");
    FilePrintf (CliHandle, "\r\n");
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfVirtAuthKeyStartAccept                       */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key start accept                 */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StartAcceptTime - Contains the Start Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/

INT4
OspfCliSetIfVirtAuthKeyStartAccept (tCliHandle CliHandle, UINT4 u4AreaId,
                                    UINT4 u4NbrId, INT4 KeyId,
                                    UINT1 *pu1StartAcceptTime,
                                    UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    INT4                i4KeyStartAccept = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tAreaId             areaId;
    tRouterId           nbrId;

    tSNMP_OCTET_STRING_TYPE KeyStartTime;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];

    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);
    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    i4Len = (INT4) STRLEN (pu1StartAcceptTime);
    OSPF_CRU_BMC_DWTOPDU (areaId, u4AreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);
    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StartAcceptTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    /* Converts the DD-MON-YY to YY-MON-DD format */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);
    KeyStartTime.pu1_OctetList = pOspfKeyTime;
    KeyStartTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);
    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStartAccept = (INT4) GrGetSecondsSinceBase (tm);
    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);

    if (pInterface == NULL)
    {
        CliPrintf (CliHandle, "\r%% Failed in getting Virtual Interface\r\n");
        return CLI_FAILURE;

    }
    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        if (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStartAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfVirtIfMD5AuthKeyStartAccept
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStartAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfVirtIfAuthKeyStartAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfVirtIfAuthKeyStartAccept
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfVirtAuthKeyStopGenerate                     */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key start accept                 */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StartAcceptTime - Contains the Start Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/

INT4
OspfCliSetIfVirtAuthKeyStopGenerate (tCliHandle CliHandle, UINT4 u4AreaId,
                                     UINT4 u4NbrId, INT4 KeyId,
                                     UINT1 *pu1StopGenTime, UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    INT4                i4KeyStopGenerate = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tAreaId             areaId;
    tRouterId           nbrId;

    tSNMP_OCTET_STRING_TYPE KeyStopTime;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];

    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);
    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    i4Len = (INT4) STRLEN (pu1StopGenTime);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4AreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);
    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StopGenTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    /* Converts the DD-MON-YY to YY-MON-DD format */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);
    KeyStopTime.pu1_OctetList = pOspfKeyTime;
    KeyStopTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);
    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStopGenerate = (INT4) GrGetSecondsSinceBase (tm);
    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);

    if (pInterface == NULL)
    {
        return CLI_FAILURE;

    }
    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        /* Test routine is called for configuring Key Stop Generate value */
        if (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStopGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Stop Generate Value */
        if (nmhSetFsMIOspfVirtIfMD5AuthKeyStopGenerate
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStopGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfVirtIfAuthKeyStopGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfVirtIfAuthKeyStopGenerate
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfVirtAuthKeyStartGenerate                    */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key start accept                 */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StartAcceptTime - Contains the Start Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/

INT4
OspfCliSetIfVirtAuthKeyStartGenerate (tCliHandle CliHandle, UINT4 u4AreaId,
                                      UINT4 u4NbrId, INT4 KeyId,
                                      UINT1 *pu1StartGenTime, UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    INT4                i4KeyStartGenerate = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tAreaId             areaId;
    tRouterId           nbrId;

    tSNMP_OCTET_STRING_TYPE KeyStartTime;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];

    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);
    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    i4Len = (INT4) STRLEN (pu1StartGenTime);

    OSPF_CRU_BMC_DWTOPDU (areaId, u4AreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);
    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StartGenTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    /* Converts the DD-MON-YY to YY-MON-DD format */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    KeyStartTime.pu1_OctetList = pOspfKeyTime;
    KeyStartTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);
    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStartGenerate = (INT4) GrGetSecondsSinceBase (tm);
    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);

    if (pInterface == NULL)
    {
        return CLI_FAILURE;

    }
    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        /* Test routine is called for configuring Key Start Generate value */
        if (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStartGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Start Generate Value */
        if (nmhSetFsMIOspfVirtIfMD5AuthKeyStartGenerate
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStartGenerate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfVirtIfAuthKeyStartGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfVirtIfAuthKeyStartGenerate
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;

}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : OspfCliSetIfVirtAuthKeyStopAccept                       */
/*                                                                            */
/* DESCRIPTION      : This function Sets the Key start accept                 */
/*                    time in DD-MON-YEAR HH:MM format and converts           */
/*                    to string and sets the value                            */
/*                                                                            */
/* INPUT            : CliHandle - Handle to CLI Session                       */
/*                    pu1StartAcceptTime - Contains the Start Time Hours value */
/*                                                                            */
/* RETURNS          : SUCCESS / FAILURE                                       */
/*                                                                            */
/******************************************************************************/

INT4
OspfCliSetIfVirtAuthKeyStopAccept (tCliHandle CliHandle, UINT4 u4AreaId,
                                   UINT4 u4NbrId, INT4 KeyId,
                                   UINT1 *pu1StopAcceptTime, UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    INT4                i4KeyStopAccept = 0;
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    tUtlTm              tm;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tAreaId             areaId;
    tRouterId           nbrId;
    tSNMP_OCTET_STRING_TYPE KeyStopTime;
    UINT1               pOspfKeyTime[OSPF_DST_TIME_LEN + 1];

    MEMSET (pOspfKeyTime, OSPF_ZERO, OSPF_DST_TIME_LEN + 1);
    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    i4Len = (INT4) STRLEN (pu1StopAcceptTime);

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }
    OSPF_CRU_BMC_DWTOPDU (areaId, u4AreaId);
    OSPF_CRU_BMC_DWTOPDU (nbrId, u4NbrId);
    if (i4Len > OSPF_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = OspfConvertTime (pu1StopAcceptTime, &tm);
    switch (i4CheckVal)
    {
        case OSPF_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPF_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPF_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    /* Converts the DD-MON-YY to YY-MON-DD format */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    KeyStopTime.pu1_OctetList = pOspfKeyTime;
    KeyStopTime.i4_Length = (INT4) STRLEN (pOspfKeyTime);

    /* Converts absolute time to secs from the base year 2000 */
    i4KeyStopAccept = (INT4) GrGetSecondsSinceBase (tm);

    pInterface = GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);

    if (pInterface == NULL)
    {
        return CLI_FAILURE;

    }
    if (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5)
    {
        /* Test routine is called for configuring Key Start Generate value */
        if (nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStopAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        /* Set the Key Start Generate Value */
        if (nmhSetFsMIOspfVirtIfMD5AuthKeyStopAccept
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             i4KeyStopAccept) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhTestv2FsMIOspfVirtIfAuthKeyStopAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Value not valid\r\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsMIOspfVirtIfAuthKeyStopAccept
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot set the value\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;

}

/******************************************************************************/
/* Function Name     : OspfCliCheckIfDefaultValues                            */
/*                                                                            */
/* Description       : This Routine will check for OSPF default values        */
/*                                   on an interface                          */
/*                                                                            */
/* Input Parameters  : u4OspfAddressLessIf - Addressless interface index      */
/*                     u4IfIpAddr          - OSPF Network address             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

PRIVATE INT4
OspfCliCheckIfDefaultValues (UINT4 u4IfIpAddr, UINT4 u4OspfAddressLessIf)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
    UINT4               u4Index = 0;
    INT4                i4MetricValue = 0;
    tAUTHENTICATION     Md5authkey;
    tNetIpv4IfInfo      NetIpv4IfInfo;

    MEMSET (Md5authkey, 0, sizeof (tAUTHENTICATION));

    if (u4OspfAddressLessIf != 0)
    {
        u4IfIpAddr = 0;
    }

    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4IfIpAddr);
    if ((pInterface =
         GetFindIfInCxt (pOspfCxt, ifIpAddr, u4OspfAddressLessIf)) != NULL)
    {
        if (!((pInterface->u2HelloInterval == OSPF_DEF_HELLO_INTERVAL) &&
              (pInterface->i4RtrDeadInterval == DEF_RTR_DEAD_INTERVAL) &&
              (pInterface->u2IfTransDelay == DEF_IF_TRANS_DELAY) &&
              (pInterface->u1RtrPriority == DEF_RTR_PRIORITY) &&
              (pInterface->u2RxmtInterval == DEF_RXMT_INTERVAL) &&
              (pInterface->i4PollInterval == DEF_POLL_INTERVAL) &&
              (pInterface->bDcEndpt == OSPF_FALSE) &&
              (pInterface->u1NetworkType == IF_BROADCAST) &&
              (pInterface->u2AuthType == NO_AUTHENTICATION) &&
              (pInterface->pLastAuthkey == NULL)))
        {
            return CLI_FAILURE;
        }

        if (MEMCMP (pInterface->authKey, Md5authkey, AUTH_KEY_SIZE) != 0)
        {
            return CLI_FAILURE;
        }

        if (pInterface->sortMd5authkeyLst.u4_Count != 0)
        {
            return CLI_FAILURE;
        }

        /* Checking for the default cost */

        if (u4OspfAddressLessIf == 0)
        {
            if (NetIpv4GetIfIndexFromAddrInCxt (pOspfCxt->u4OspfCxtId,
                                                u4IfIpAddr,
                                                &u4Index) == NETIPV4_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            if (NetIpv4GetPortFromIfIndex (u4OspfAddressLessIf, &u4Index)
                == NETIPV4_FAILURE)
            {
                return CLI_FAILURE;
            }
        }

        if (NetIpv4GetIfInfo (u4Index, &NetIpv4IfInfo) == NETIPV4_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (NetIpv4IfInfo.u4IfSpeed == 0)
        {
            i4MetricValue = (INT4) 0xffffffff;
        }
        else if (NetIpv4IfInfo.u4IfSpeed > TEN_POWER_EIGHT)
        {
            i4MetricValue = 1;
        }
        else
        {
            i4MetricValue = (INT4) (TEN_POWER_EIGHT / NetIpv4IfInfo.u4IfSpeed);
        }

        if (pInterface->aIfOpCost[TOS_0].u4Value != (UINT4) i4MetricValue)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : OspfCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPF_SUCCESS/OSPF_FAILURE                              */
/*****************************************************************************/
INT4
OspfCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    INT4                i4Cmd = 0;
    CHR1               *au1OspfShowCmdList[OSPF_DYN_MAX_CMDS]
        = { "snmpwalk mib name ospfNbrTable > ",
        "snmpwalk mib name ospfVirtNbrTable >> ",
        "snmpwalk mib name futOspfNbrTable >> ",
        "snmpwalk mib name futOspfVirtNbrTable >> ",
        "snmpwalk mib name ospfIfTable >> ",
        "snmpwalk mib name futOspfIfTable >> ",
        "snmpwalk mib name futOspfSecIfTable >> ",
        "snmpwalk mib name ospfVirtIfTable >> ",
        "show ip ospf redundancy ext-route-info >> ",
        "show ip ospf retransmission-list >> "
    };
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return OSPF_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < OSPF_DYN_MAX_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile (pu1FileName,
                                       (UINT1 *) au1OspfShowCmdList[i4Cmd])
            == CLI_FAILURE)
        {
            return OSPF_FAILURE;
        }
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : OspfCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPF_SUCCESS/OSPF_FAILURE                              */
/*****************************************************************************/
INT4
OspfCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[OSPF_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, OSPF_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return OSPF_FAILURE;
    }
    MEMSET (ai1Buf, 0, OSPF_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (OspfCliReadLineFromFile (i4Fd, ai1Buf, OSPF_CLI_MAX_GROUPS_LINE_LEN,
                                    &i2ReadLen) != OSPF_CLI_EOF)

    {
        if ((i2ReadLen > 0) &&
            (NULL == STRSTR (ai1Buf, "ospfNbrEvents")) &&
            (NULL == STRSTR (ai1Buf, "ospfIfEvents")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfHelloRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfHelloTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfHelloDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfDdpRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfDdpTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfDdpDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLrqRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLrqTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLsuRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLsuTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLakRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLakTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfIfLakDisd")))
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', OSPF_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : OspfCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPF_SUCCESS/OSPF_FAILURE                              */
/*****************************************************************************/
INT1
OspfCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                         INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (OSPF_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (OSPF_CLI_EOF);
}

/******************************************************************************/
/* Function Name     : OspfCliSetHostMetric                                   */
/*                                                                            */
/* Description       : This Routine will set metric for the host router       */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4ContextId - contxt id for router                     */
/*                     u4HostIpAddr - IP address of host                      */
/*                     i4HostTOS- type of service                             */
/*                     *pi4HostMetric - metric for host                       */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
OspfCliSetHostMetric (tCliHandle CliHandle, INT4 i4ContextId,
                      UINT4 u4HostIpAddr, INT4 i4HostTOS, INT4 i4HostMetric)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetValOspfHostStatus = 0;

    if (nmhGetFsMIStdOspfHostStatus (i4ContextId,
                                     u4HostIpAddr,
                                     i4HostTOS,
                                     &i4RetValOspfHostStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2FsMIStdOspfHostStatus
            (&u4ErrorCode, i4ContextId, u4HostIpAddr, i4HostTOS,
             CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfHostStatus (i4ContextId,
                                         u4HostIpAddr,
                                         i4HostTOS,
                                         CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FsMIStdOspfHostMetric (&u4ErrorCode, i4ContextId,
                                        u4HostIpAddr, i4HostTOS,
                                        i4HostMetric) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_OSPF_INVALID_METRIC);
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFsMIStdOspfHostMetric (i4ContextId, u4HostIpAddr,
                                     i4HostTOS, i4HostMetric) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetFsMIStdOspfHostStatus (i4ContextId,
                                     u4HostIpAddr, i4HostTOS, DESTROY);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMIStdOspfHostStatus
        (&u4ErrorCode, i4ContextId, u4HostIpAddr, i4HostTOS,
         ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetFsMIStdOspfHostStatus (i4ContextId,
                                     u4HostIpAddr, i4HostTOS, DESTROY);

        return CLI_FAILURE;
    }

    if (nmhSetFsMIStdOspfHostStatus (i4ContextId,
                                     u4HostIpAddr,
                                     i4HostTOS, ACTIVE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetFsMIStdOspfHostStatus (i4ContextId,
                                     u4HostIpAddr, i4HostTOS, DESTROY);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : OspfCliDelHostMetric                                   */
/*                                                                            */
/* Description       : This Routine will Destroy host created                 */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4ContextId - contxt id for router                     */
/*                     u4HostIpAddr - IP address of host                      */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
OspfCliDelHostMetric (tCliHandle CliHandle, INT4 i4ContextId,
                      UINT4 u4HostIpAddr)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4RetValOspfHostStatus = 0;

    if (nmhGetFsMIStdOspfHostStatus (i4ContextId,
                                     u4HostIpAddr,
                                     OSPF_ZERO,
                                     &i4RetValOspfHostStatus) == SNMP_SUCCESS)
    {

        if (nmhTestv2FsMIStdOspfHostStatus
            (&u4ErrorCode, i4ContextId, u4HostIpAddr, OSPF_ZERO,
             DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfHostStatus (i4ContextId,
                                         u4HostIpAddr,
                                         OSPF_ZERO, DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    CLI_SET_ERR (CLI_OSPF_NO_HOST);
    return CLI_FAILURE;
}

#endif /* __OSPFCLI_C__ */
