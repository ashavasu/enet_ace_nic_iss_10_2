/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oslrq.c,v 1.20 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             sending and receiving link state request pkts.
 *
 *******************************************************************/

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqRcvLsaReq                                            */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.7                          */
/*                This procedure takes care of the processing of the received*/
/*                link state request packet. Each requested LSA is searched  */
/*                in the database and added to the lsa_req_update_pkt to be  */
/*                sent to the neighbor.                                      */
/*                                                                           */
/* Input        : pLsaReqPkt       : the req pkt                          */
/*                u2Len              : length of the pkt                    */
/*                pNbr               : the nbr from whom the req was        */
/*                                      received                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LrqRcvLsaReq (tCRU_BUF_CHAIN_HEADER * pLsaReqPkt, UINT2 u2Len, tNeighbor * pNbr)
{
    UINT4               u4LsaType;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    UINT2               u2ReqCount;
    tLsUpdate           lsUpdate;
    UINT1               u1SendPktFlg = OSPF_TRUE;
    INT4                i4Offset = 0;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Lsa Request should not be processed if the node is not active */
        return;
    }
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LrqRcvLsaReq\n");

    OSPF_NBR_TRC2 (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "LSReq Rcvd Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    if ((pNbr->u1NsmState != NBRS_EXCHANGE) &&
        (pNbr->u1NsmState != NBRS_LOADING) && (pNbr->u1NsmState != NBRS_FULL))
    {
        u1SendPktFlg = OSPF_FALSE;
    }
    else
    {
        lsUpdate.pLsuPkt = NULL;
        LsuClearUpdate (&lsUpdate);

        /* determine number of requests in packet */

        u2ReqCount = (UINT2) ((u2Len - OS_HEADER_SIZE) / LSA_REQ_SIZE);

        /* skip OSPF header */

        while (u2ReqCount--)
        {

            /* extract request */
            OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pLsaReqPkt,
                                                (LRQ_TYPE_OFFSET +
                                                 (12 * i4Offset)), u4LsaType);
            OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pLsaReqPkt, linkStateId,
                                                (LRQ_STATE_ID_OFFSET +
                                                 (12 * i4Offset)),
                                                MAX_IP_ADDR_LEN);
            OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pLsaReqPkt, advRtrId,
                                                (LRQ_ADV_RTRID_OFFSET +
                                                 (12 * i4Offset)),
                                                MAX_IP_ADDR_LEN);

            OSPF_NBR_TRC3 (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "Req for LSType %d LSId %x AdvRtrId %x\n",
                           u4LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (linkStateId),
                           OSPF_CRU_BMC_DWFROMPDU (advRtrId));

            /* find requested advt in database */

            pLsaInfo = LsuSearchDatabase ((UINT1) u4LsaType,
                                          &linkStateId,
                                          &advRtrId,
                                          (UINT1 *) pNbr->pInterface,
                                          (UINT1 *) pNbr->pInterface->pArea);

            if (pLsaInfo != NULL)
            {
                OSPF_TRC4 (OSPF_LSU_TRC | OSPF_ADJACENCY_TRC,
                           pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "FUNC :LrqRcvLsaReq ,LSA LSType %d linkstateId  = %x LsaAge = %d seqNum = %d\n",
                           pLsaInfo->lsaId.u1LsaType,
                           OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                           pLsaInfo->u2LsaAge, pLsaInfo->lsaSeqNum);

                LsuAddToLsu (pNbr, pLsaInfo, &lsUpdate, TX_MANY);
            }
            else
            {
                OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                           pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "NBRE: Bad Link State Request for Neighbour with Address =%x\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
                GENERATE_NBR_EVENT (pNbr, NBRE_BAD_LS_REQ);
                u1SendPktFlg = OSPF_FALSE;
                break;
            }
            i4Offset++;
        }                        /* end while */
    }                            /* end else */

    if (u1SendPktFlg == OSPF_TRUE)
    {
        LsuSendLsu (pNbr, &lsUpdate);
    }
    else
    {

        INC_DISCARD_LRQ_CNT (pNbr->pInterface);
        if (pNbr->u1NsmState < MAX_NBR_STATE)
        {
            OSPF_NBR_TRC3 (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "LRQ Disc Nbr %x.%d Nbr State %s\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                           pNbr->u4NbrAddrlessIf,
                           au1DbgNbrState[pNbr->u1NsmState]);
        }
    }

    OSPF_NBR_TRC (OSPF_LRQ_TRC | OSPF_FN_EXIT,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: LrqRcvLsaReq :LRQ Processing Over\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqSendLsaReq                                           */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 10.9                          */
/*                This procedure constructs a ls_req_pkt from the top of the */
/*                ls_req_list and transmits it to the neighbor.              */
/*                                                                           */
/* Input        : pNbr            : the nbr to whom req is to be sent       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LrqSendLsaReq (tNeighbor * pNbr)
{

    UINT2               u2ReqCount = 0;
    UINT2               u2RemReqCount = 0;
    tCRU_BUF_CHAIN_HEADER *pLsaReqPkt;
    UINT2               u2PktLen;
    tLsaReqNode        *pLsaReqNode;
    tLsaReqNode        *pPrevLsaReqNode;
    tLsaReqNode        *pPrevPktLsaReqNode = NULL;
    tLsHeader          *pLsHeader = NULL;
    UINT2               u2DigestLen;
    UINT4               u4LRQReqCount = 0;
    UINT4               u4LRQReqPkt = 1;
    UINT4               u4NotToTx = OSPF_TRUE;
    UINT4               u4TmrInterval;
    UINT4               u4LsaType = 0;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Lsa Request should not be send if the node is not active */
        return;
    }

    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LrqSendLsaReq \n");

    OSPF_NBR_TRC2 (OSPF_LRQ_TRC, pNbr,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "LSReq To Be Sent To Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);

    u2DigestLen = DIGEST_LEN_IF_CRYPT (pNbr->pInterface);
    if (LrqIsEmptyLsaReqLst (pNbr) == OSPF_TRUE)
    {

        OSPF_NBR_TRC (OSPF_LRQ_TRC, pNbr,
                      pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "LSReq Lst Empty\n");

        return;
    }
    u2RemReqCount = (UINT2) TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst));
    while ((u4LRQReqPkt <= MAX_OSPF_LRQ_TX) && (u2RemReqCount > 0))
    {
        /* determine no. of requests to be sent in packet */

        u2ReqCount =
            (UINT2) OSPF_MIN (u2RemReqCount,
                              ((IFACE_MTU (pNbr->pInterface) -
                                (OS_HEADER_SIZE +
                                 u2DigestLen)) / LSA_REQ_SIZE));
        u2RemReqCount = (UINT2) (u2RemReqCount - u2ReqCount);
        u2PktLen = (UINT2) (OS_HEADER_SIZE + u2ReqCount * LSA_REQ_SIZE);
        if ((pLsaReqPkt =
             UtilOsMsgAlloc (u2PktLen + MD5_AUTH_DATA_LEN)) == NULL)
        {

            OSPF_NBR_TRC2 (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                           pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "LSReq Alloc Failed Nbr %x.%d\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                           pNbr->u4NbrAddrlessIf);

            return;
        }

        /* leave space for OSPF header */

        OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pLsaReqPkt, 0, 0, OS_HEADER_SIZE);
        pLsaReqNode = NULL;
        u4LRQReqCount = 0;
        for (pPrevLsaReqNode = pPrevPktLsaReqNode; u2ReqCount--;
             pPrevLsaReqNode = pLsaReqNode)
        {
            if (pPrevLsaReqNode != NULL)
            {
                pLsaReqNode =
                    (tLsaReqNode
                     *) (TMO_SLL_Next (&(pNbr->lsaReqDesc.lsaReqLst),
                                       &(pPrevLsaReqNode->nextLsaReqNode)));
            }
            else
            {
                pLsaReqNode = (tLsaReqNode *)
                    (TMO_SLL_First (&(pNbr->lsaReqDesc.lsaReqLst)));
                pLsHeader = &(pLsaReqNode->lsHeader);
            }

            if (pLsaReqNode == NULL)
            {
                break;
            }
            u4NotToTx = OSPF_FALSE;
            u4LsaType = (UINT4) pLsaReqNode->lsHeader.u1LsaType;
            OSPF_CRU_BMC_ASSIGN_4_BYTE (pLsaReqPkt,
                                        (LRQ_TYPE_OFFSET +
                                         (12 * u4LRQReqCount)), u4LsaType);

            OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pLsaReqPkt,
                                               pLsaReqNode->lsHeader.
                                               linkStateId,
                                               (LRQ_STATE_ID_OFFSET +
                                                (12 * u4LRQReqCount)),
                                               MAX_IP_ADDR_LEN);

            OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pLsaReqPkt,
                                               pLsaReqNode->lsHeader.advRtrId,
                                               (LRQ_ADV_RTRID_OFFSET +
                                                (12 * u4LRQReqCount)),
                                               MAX_IP_ADDR_LEN);
            u4LRQReqCount++;

        }
        pPrevPktLsaReqNode = pLsaReqNode;
        if (pLsaReqNode != NULL)
        {
            pNbr->lsaReqDesc.startNextReqPkt =
                TMO_SLL_Next (&(pNbr->lsaReqDesc.lsaReqLst),
                              &(pLsaReqNode->nextLsaReqNode));
        }
        if (u4NotToTx == OSPF_TRUE)
        {
            OSPF_NBR_TRC (OSPF_LRQ_TRC, pNbr,
                          pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "No More LRQ's Left To Send. So Free Buffer and EXIT: LrqSendLsaReq \n");
            UtilOsMsgFree (pLsaReqPkt, NORMAL_RELEASE);
            break;
        }
        u4NotToTx = OSPF_TRUE;
        UtilConstructHdr (pNbr->pInterface, pLsaReqPkt, LSA_REQ_PKT, u2PktLen);
        if (pLsHeader != NULL)
        {
            if (IS_VIRTUAL_IFACE (pNbr->pInterface))
            {
                VifSendRxmtTrap (pNbr, pLsHeader, LSA_REQ_PKT);
            }
            else
            {
                IfSendRxmtTrap (pNbr, pLsHeader, LSA_REQ_PKT);
            }
        }
        PppSendPkt (pLsaReqPkt, (UINT2) (u2PktLen + u2DigestLen),
                    pNbr->pInterface, &(pNbr->nbrIpAddr), RELEASE);
        u4LRQReqPkt++;

    }
    u4TmrInterval = NO_OF_TICKS_PER_SEC * (pNbr->pInterface->u2RxmtInterval);

    TmrRestartTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer),
                     LSA_REQ_RXMT_TIMER,
                     ((pNbr->pInterface->u1NetworkType ==
                       IF_BROADCAST) ? UtilJitter (NO_OF_TICKS_PER_SEC *
                                                   (pNbr->
                                                    pInterface->
                                                    u2RxmtInterval),
                                                   OSPF_JITTER) :
                      u4TmrInterval));

    pNbr->lsaReqDesc.bLsaReqOutstanding = OSPF_TRUE;

    OSPF_NBR_TRC2 (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "LSR Sent To Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: LrqSendLsaReq \n");

}

/*****************************************************************************/
/* Function     : LrqRxmtLsaReq                                              */
/*                                                                           */
/* Description  : This procedure is invoked when the ls_req_rxmt_timer fires.*/
/*                                                                           */
/* Input        : pNbr        : The nbr to whom req is to be                 */
/*                                   retransmitted.                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
LrqRxmtLsaReq (VOID *pArg)
{
    tNeighbor          *pNbr = pArg;
    OSPF_NBR_TRC (OSPF_FN_ENTRY, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "FUNC: LrqRxmtLsaReq \n");

    if (LrqIsEmptyLsaReqLst (pNbr) == OSPF_FALSE)
    {
        LrqSendLsaReq (pNbr);
    }

    OSPF_NBR_TRC2 (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                   pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "LSR Rxmetted To Nbr %x.%d\n",
                   OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr),
                   pNbr->u4NbrAddrlessIf);
    OSPF_NBR_TRC (OSPF_FN_EXIT, pNbr,
                  pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: LrqRxmtLsaReq \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqAddToLsaReqLst                                     */
/*                                                                           */
/* Description  : This procedure adds the specified lsHeader to the         */
/*                lsa_req_list of the specified neighbor. If the length of   */
/*                the ls_req_list exceeds MTU_SIZE/REQ_SIZE and if no        */
/*                ls_req_pkt is outstanding then the lrq_send_ls_req         */
/*                procedure is invoked.                                      */
/*                                                                           */
/* Input        : pNbr            : the nbr whose req list is to be appended*/
/*                pLsHeader      : the lsHeader to be added               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if request list successfully updated            */
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
LrqAddToLsaReqLst (tNeighbor * pNbr, tLsHeader * pLsHeader)
{

    tLsaReqNode        *pLsaReqNode;

    if (LSA_REQ_ALLOC (pLsaReqNode) == NULL)
    {

        OSPF_NBR_TRC (OS_RESOURCE_TRC | OSPF_ADJACENCY_TRC,
                      pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "LSR Alloc Failure\n");

        return (OSPF_FALSE);
    }
    TMO_SLL_Init_Node (&(pLsaReqNode->nextLsaReqNode));
    OS_MEM_CPY (&(pLsaReqNode->lsHeader), pLsHeader, LS_HEADER_SIZE);
    if (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) == 0)
    {
        pNbr->lsaReqDesc.startNextReqPkt = &(pLsaReqNode->nextLsaReqNode);
    }

    TMO_SLL_Add (&(pNbr->lsaReqDesc.lsaReqLst), &(pLsaReqNode->nextLsaReqNode));

    if ((pNbr->lsaReqDesc.bLsaReqOutstanding == OSPF_FALSE) &&
        (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) >
         (IFACE_MTU (pNbr->pInterface) / LSA_REQ_SIZE)))
    {

        LrqSendLsaReq (pNbr);
    }

    OSPF_NBR_TRC (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Req Lst Updated\n");

    return (OSPF_TRUE);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqDeleteFromLsaReqLst                                */
/*                                                                           */
/* Description  : This procedure deletes the specified node from the         */
/*                ls_req_list of the specified neighbor. If the list becomes */
/*                empty and if the state is loading then the neighbor state  */
/*                machine is invoked with the event loading_done. If the top */
/*                of the ls_req_list reaches the start of the next ls_req_pkt*/
/*                to be sent then a new ls_req_pkt is sent by invoking the   */
/*                lrq_send_ls_req procedure.                                 */
/*                                                                           */
/* Input        : pNbr                   : the nbr whose req list is to be  */
/*                                          truncated                        */
/*                pLsaReqNode          : the req node to be deleted from  */
/*                                          list                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LrqDeleteFromLsaReqLst (tNeighbor * pNbr, tLsaReqNode * pLsaReqNode)
{

    if (pLsaReqNode == (tLsaReqNode *) pNbr->lsaReqDesc.startNextReqPkt)
    {
        pNbr->lsaReqDesc.startNextReqPkt =
            TMO_SLL_Next (&(pNbr->lsaReqDesc.lsaReqLst),
                          &(pLsaReqNode->nextLsaReqNode));
    }
    TMO_SLL_Delete (&(pNbr->lsaReqDesc.lsaReqLst),
                    &(pLsaReqNode->nextLsaReqNode));
    LSA_REQ_FREE (pLsaReqNode);
    if ((LrqIsEmptyLsaReqLst (pNbr) == OSPF_TRUE) &&
        (pNbr->u1NsmState == NBRS_LOADING))
    {

        LrqClearLsaReqLst (pNbr);
        OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                   pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "NBRE: Loading done for Neighbour with Address =%s\n",
                   OspfPrintIpAddr (pNbr->nbrIpAddr));
        GENERATE_NBR_EVENT (pNbr, NBRE_LOADING_DONE);
        return;
    }
    if (TMO_SLL_First (&(pNbr->lsaReqDesc.lsaReqLst))
        == pNbr->lsaReqDesc.startNextReqPkt)
    {

        /* requests in prev packet satisfied */

        pNbr->lsaReqDesc.bLsaReqOutstanding = OSPF_FALSE;
        LrqSendLsaReq (pNbr);
    }

    OSPF_NBR_TRC (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Req Lst Updated\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqSearchLsaReqLst                                         */
/*                                                                           */
/* Description  : This procedure searches for the specified LSA in the       */
/*                neighbor's request list. If found it returns a pointer to  */
/*                the singly linked list node containing it. Otherwise it    */
/*                returns 0.                                                 */
/*                                                                           */
/* Input        : pNbr              : nbr whose req lst is to be searched   */
/*                pLsHeader        : the lsHeader which is searched for   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the required LSA node, if found                 */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tLsaReqNode *
LrqSearchLsaReqLst (tNeighbor * pNbr, tLsHeader * pLsHeader)
{

    tLsaReqNode        *pLsaReqNode;
    tLsHeader          *pReqLstHeader;

    TMO_SLL_Scan ((&(pNbr->lsaReqDesc.lsaReqLst)), pLsaReqNode, tLsaReqNode *)
    {

        pReqLstHeader = &(pLsaReqNode->lsHeader);
        if ((UtilIpAddrComp (pReqLstHeader->linkStateId,
                             pLsHeader->linkStateId) == OSPF_EQUAL) &&
            (UtilIpAddrComp (pReqLstHeader->advRtrId,
                             pLsHeader->advRtrId) == OSPF_EQUAL) &&
            (pReqLstHeader->u1LsaType == pLsHeader->u1LsaType))
        {

            return (pLsaReqNode);
        }
    }

    OSPF_NBR_TRC (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "LSA Search Failure In Req Lst\n");

    return ((tLsaReqNode *) NULL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqClearLsaReqLst                                      */
/*                                                                           */
/* Description  : This procedure clears the link state request list of this  */
/*                neighbor.                                                  */
/*                                                                           */
/* Input        : pNbr            : nbr whose req lst is to be cleared      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LrqClearLsaReqLst (tNeighbor * pNbr)
{

    tLsaReqNode        *pLsaReqNode;

    pNbr->lsaReqDesc.bLsaReqOutstanding = OSPF_FALSE;
    TmrDeleteTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer));
    pNbr->lsaReqDesc.startNextReqPkt = NULL;

    while ((pLsaReqNode = (tLsaReqNode *)
            TMO_SLL_Get (&(pNbr->lsaReqDesc.lsaReqLst))) != NULL)
    {
        LSA_REQ_FREE (pLsaReqNode);
    }

    OSPF_NBR_TRC (OSPF_LRQ_TRC | OSPF_ADJACENCY_TRC,
                  pNbr, pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Req Lst Cleared\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LrqIsEmptyLsaReqLst                                   */
/*                                                                           */
/* Description  : Checks whether the LSA request list is empty.              */
/*                                                                           */
/* Input        : pNbr            : nbr whose req lst is to be verified     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if the link state request list of the specified */
/*                neighbor is empty                                          */
/*                OSPF_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/

/* nbr whose req lst is to be verified */
PUBLIC INT1
LrqIsEmptyLsaReqLst (tNeighbor * pNbr)
{
    if (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)))
    {
        return (OSPF_FALSE);
    }
    else
    {
        return (OSPF_TRUE);
    }
}

/*----------------------------------------------------------------------*/
/*                      End of the file  oslrq.c                        */
/*----------------------------------------------------------------------*/
