/************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved     */
/*                                                          */
/*  FILE NAME             : osrmrxls.c                      */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : OSPF                            */
/*  LANGUAGE              : C                               */
/*  FUNCTIONS DEFINED     : OspfRmProcessHello,            */
/*                          OspfRmSendBulkHello,           */
/*  DESCRIPTION           : This file contains definitions  */
/*                          related to bulk update for      */
/*                          High Availability               */
/************************************************************/
/*                                                          */
/* $Id: osrmrxls.c,v 1.15 2014/12/24 11:06:11 siva Exp $     */
/*  Change History                                          */
/*  Version               :                                 */
/*  Date(DD/MM/YYYY)      :                                 */
/*  Modified by           :                                 */
/*  Description of change :                                 */
/************************************************************/
#ifndef _OSREDBLK_
#define _OSREDBLK_

#include "osinc.h"

PRIVATE INT4        OspfRmConstructLsaDescInCxt (tLsaInfo *, tRmMsg *, UINT4 *,
                                                 tArea *, UINT1 *, tLsHeader *);
PRIVATE INT4        OspfRmAllocLsaDescInCxt (tLsaInfo *, UINT1 *);
PRIVATE VOID        OspfRmHandleFuncEqvType9Grace (UINT1 *, tNeighbor *);
PRIVATE VOID        OspfRmAddToIfaceRxmt (tLsaInfo *, tInterface *,
                                          tNeighbor *);
PRIVATE VOID        OspfRmBulkUpdateOffset (tOspfCxt * pOspfCxt,
                                            tLsHeader * pLsHeader,
                                            tRmMsg * pRmMsg, UINT4 *pu4Offset);
PRIVATE tAddrRange *OspfRmFindAssoAreaAddrRangeInCxt (tOspfCxt * pOspfCxt,
                                                      tIPADDR * pAddr,
                                                      tIPADDRMASK * pAddrMask,
                                                      tAreaId * pAreaId);

/*****************************************************************************/
/*                                                                           */
/* Function     :  OspfRmProcessBulkLsuInfo                                  */
/*                                                                           */
/* Description  :  This function process the received Bulk LSU message       */
/*                                                                           */
/* Format of LSU bulk update message                                         */
/* Field              Content                                                */
/* Length             Length of LSA                                          */
/* LSA                LSA Content                                            */
/* Ack flag           OSPF_RED_LSACK_RCVD /OSPF_RED_LSACK_NOT_RCVD           */
/* Age                LSA Age                                                */
/* rxmit time         LSA Rxmit time                                         */
/* Context-ID         Context ID                                             */
/* LSA scope-         Area ID(areaId) in the context for                     */
/* identifier         ROUTER_LSA, NETWORK_LSA, NETWORK_SUM_LSA               */
/*                    ASBR_SUM_LSA, NSSA_LSA, TYPE10_OPQ_LSA.                */
/*                    Interface ID (ifAddr,IfaddrlessIndex) in the           */
/*                    context for TYPE9_OPQ_LSA.                             */
/* AssoPrimitive      For self LSA only.                                     */
/*                                                                           */
/* Input        :  pMsg - Meaaage                                            */
/*                 u2MsgLen - Message length                                 */
/*                                                                           */
/* Output       :  None                                                      */
/*                                                                           */
/* Returns      :  None                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmProcessBulkLsuInfo (tRmMsg * pMsg, UINT2 u2MsgLen)
{
    UINT4               u4Offset = OSPF_RED_MSG_HDR_SIZE;

    /* Current Offset points to sub bulk update type as OSPF_RED_SYNC_LSU_MSG *
     * Increment the offset to point to the LSA contents */
    u4Offset++;
    gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_LSDB_MOD;

    do
    {
        if (OspfRmProcessLSA (pMsg, &u4Offset) == OSIX_FAILURE)
        {
            return;
        }

        gOsRtr.osRedInfo.u4LsaSyncCount++;
    }
    while (u4Offset < u2MsgLen);
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  OspfRmProcessLSA                                          */
/*                                                                           */
/* Description  :  This function process the received Bulk LSU message       */
/*                                                                           */
/* Format of LSU bulk update message                                         */
/* Field              Content                                                */
/* Length             Length of LSA                                          */
/* LSA                LSA Content                                            */
/* Ack flag           OSPF_RED_LSACK_RCVD /OSPF_RED_LSACK_NOT_RCVD           */
/* Age                LSA Age                                                */
/* rxmit time         LSA Rxmit time                                         */
/* Context-ID         Context ID                                             */
/* LSA scope-         Area ID(areaId) in the context for                     */
/* identifier         ROUTER_LSA, NETWORK_LSA, NETWORK_SUM_LSA               */
/*                    ASBR_SUM_LSA, NSSA_LSA, TYPE10_OPQ_LSA.                */
/*                    Interface ID (ifAddr,IfaddrlessIndex) in the           */
/*                    context for TYPE9_OPQ_LSA.                             */
/* AssoPrimitive      For self LSA only.                                     */
/*                                                                           */
/* Input        :  pMsg - Meaaage                                            */
/*                 u2MsgLen - Message length                                 */
/*                                                                           */
/* Output       :  None                                                      */
/*                                                                           */
/* Returns      :  None                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfRmProcessLSA (tRmMsg * pRmMsg, UINT4 *pu4Offset)
{
    tLsHeader           lsHeader;
    tAreaId             areaId;
    tIPADDR             IfIpAddr;
    UINT1              *pLsa = NULL;
    tArea              *pArea = NULL;
    tInterface         *pInterface = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tNeighbor          *pNbr = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u2LsaLen = 0;
    UINT4               u4LsaRemainTime = 0;
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    UINT4               u4IfAddrlessIf = 0;
    INT4                i4RetVal = OSPF_SUCCESS;
    UINT2               u2LsaAge = 0;
    UINT1               u1TrnsltType5 = OSPF_ZERO;
    UINT1               u1InternalLsaType = OSPF_ZERO;
    UINT1               u1LsaAck = 0;
    UINT1               u1LsaType = 0;

    MEMSET (&lsHeader, 0, sizeof (tLsHeader));
    MEMSET (&areaId, 0, sizeof (tAreaId));
    MEMSET (&IfIpAddr, 0, sizeof (tIPADDR));

    OSPF_RED_GET_2_BYTE (pRmMsg, *pu4Offset, u2LsaLen);
    OSPF_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1LsaType);

    if (u1LsaType == ROUTER_LSA)
    {
        LSA_ALLOC (u1LsaType, pLsa, u2LsaLen);
    }
    else
    {
        if (u2LsaLen > MAX_LSA_SIZE)
        {
            OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
            return OSIX_FAILURE;
        }
        LSA_ALLOC (u1LsaType, pLsa, u2LsaLen);
    }
    if (pLsa == NULL)
    {
        OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
        return OSIX_FAILURE;
    }

    OSPF_RED_GET_N_BYTE (pRmMsg, pLsa, *pu4Offset, u2LsaLen);
    OSPF_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1LsaAck);
    OSPF_RED_GET_2_BYTE (pRmMsg, *pu4Offset, u2LsaAge);
    OSPF_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4LsaRemainTime);
    OSPF_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4OspfCxtId);

    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);
    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt == NULL)
    {
        OspfRmSendBulkAbort (RM_PROCESS_FAIL);
        LSA_FREE (u1LsaType, pLsa);
        KW_FALSEPOSITIVE_FIX (pLsa);
        return OSIX_FAILURE;
    }
    pArea = pOspfCxt->pBackbone;
    switch (lsHeader.u1LsaType)
    {
        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case NSSA_LSA:
        case TYPE10_OPQ_LSA:
            OSPF_RED_GET_N_BYTE (pRmMsg, areaId, *pu4Offset, sizeof (tAreaId));
            pArea = GetFindAreaInCxt (pOspfCxt, &areaId);
            if (pArea == NULL)
            {
                i4RetVal = OSPF_FAILURE;
            }
            break;
        case TYPE9_OPQ_LSA:
            OSPF_RED_GET_N_BYTE (pRmMsg, IfIpAddr, *pu4Offset,
                                 sizeof (tIPADDR));
            OSPF_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4IfAddrlessIf);
            pInterface = GetFindIfInCxt (pOspfCxt, IfIpAddr, u4IfAddrlessIf);

            if (pInterface == NULL)
            {
                i4RetVal = OSPF_FAILURE;
            }
            break;
        default:
            break;
    }
    if (i4RetVal == OSPF_FAILURE)
    {
        OspfRmSendBulkAbort (RM_PROCESS_FAIL);
        LSA_FREE (u1LsaType, pLsa);
        return OSIX_FAILURE;
    }
    pLsaInfo = LsuSearchDatabase (lsHeader.u1LsaType,
                                  &(lsHeader.linkStateId),
                                  &(lsHeader.advRtrId),
                                  (UINT1 *) pInterface, (UINT1 *) pArea);

    /* Check the Age of LSA is MAX AGE, */
    if (u2LsaAge == MAX_AGE)
    {
        if (pLsaInfo != NULL)
        {
            if (u1LsaAck != OSPF_RED_LSACK_RCVD)
            {
                pLsaInfo->u2LsaAge = u2LsaAge;
                if ((pLsaInfo->pLsaDesc != NULL) &&
                    (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired != OSPF_TRUE))
                {
                    TmrDeleteTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer));
                    pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
                }
                OspfRmAddToRxmtLst (pLsaInfo, NULL);
            }
            else
            {
                /* Check the Age of LSA is MAX AGE,
                 * This condition will hit only for dynamic LSA update
                 * This condition is always false for bulk LSA update
                 */
                pLsaInfo->u2LsaAge = u2LsaAge;
                LsuDeleteLsaFromDatabase (pLsaInfo, OSPF_TRUE);
            }
        }
        OspfRmBulkUpdateOffset (pOspfCxt, &lsHeader, pRmMsg, pu4Offset);
        LSA_FREE (u1LsaType, pLsa);
        return OSIX_SUCCESS;
    }
    if (pLsaInfo == NULL)
    {
        if ((lsHeader.u1LsaType == TYPE9_OPQ_LSA) &&
            (lsHeader.linkStateId[0] == GRACE_LSA_OPQ_TYPE))
        {
            TMO_SLL_Scan (&(pOspfCxt->sortNbrLst), pNode, tTMO_SLL_NODE *)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNode);

                i4RetVal = UtilIpAddrComp (pNbr->nbrId, lsHeader.advRtrId);

                if (i4RetVal == OSPF_EQUAL)
                {
                    break;
                }
                pNbr = NULL;
            }

            if (pNbr == NULL)
            {
                i4RetVal = OSPF_FAILURE;
            }
            else
            {
                OspfRmHandleFuncEqvType9Grace (pLsa, pNbr);
            }
        }

        if (i4RetVal == OSPF_FAILURE)
        {
            OspfRmSendBulkAbort (RM_PROCESS_FAIL);
            LSA_FREE (u1LsaType, pLsa);
            return OSIX_FAILURE;
        }
        pLsaInfo = LsuAddToLsdb (pArea, lsHeader.u1LsaType,
                                 &(lsHeader.linkStateId),
                                 &(lsHeader.advRtrId), pInterface,
                                 NEW_LSA_INSTANCE, pLsa);
        if (pLsaInfo == NULL)
        {
            i4RetVal = OSPF_FAILURE;
        }
        else
        {
            pLsaInfo->pLsaDesc = NULL;
        }
    }
    if (i4RetVal == OSPF_FAILURE)
    {
        OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
        LSA_FREE (u1LsaType, pLsa);
        return OSIX_FAILURE;
    }

    /* Check if it is self originated LSA */
    i4RetVal = UtilIpAddrComp (pLsaInfo->lsaId.advRtrId, pOspfCxt->rtrId);

    if (i4RetVal == OSPF_EQUAL)
    {

        i4RetVal = OspfRmConstructLsaDescInCxt (pLsaInfo, pRmMsg, pu4Offset,
                                                pArea, pLsa, &lsHeader);
        if (i4RetVal == OSPF_FAILURE)
        {
            OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
            LSA_FREE (u1LsaType, pLsa);
            return OSIX_FAILURE;
        }
        u1InternalLsaType = pLsaInfo->pLsaDesc->u1InternalLsaType;
        u1TrnsltType5 = pLsaInfo->u1TrnsltType5;
    }

    if (LsuInstallLsa (pLsa, pLsaInfo) == OSPF_FAILURE)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "LsuInstallLsa failed\r\n");
    }

    /* Update the LSA age and restart the remaining timer with 
       remaining time */
    pLsaInfo->u2LsaAge = u2LsaAge;
    if (pLsaInfo->pLsaDesc != NULL)
    {
        pLsaInfo->pLsaDesc->u1InternalLsaType = u1InternalLsaType;
        if (lsHeader.u1LsaType == AS_EXT_LSA)
        {
            pLsaInfo->u1TrnsltType5 = u1TrnsltType5;
        }
    }

    TmrRestartTimer (&(pLsaInfo->lsaAgingTimer), LSA_NORMAL_AGING_TIMER,
                     (u4LsaRemainTime));

    if (u1LsaAck != OSPF_RED_LSACK_RCVD)
    {
        OspfRmAddToRxmtLst (pLsaInfo, NULL);
    }
    if (!(pOspfCxt->bOverflowState == OSPF_TRUE) &&
        (pOspfCxt->i4ExtLsdbLimit != NO_LIMIT) &&
        (GET_NON_DEF_ASE_LSA_COUNT_IN_CXT
         (pOspfCxt) == pOspfCxt->i4ExtLsdbLimit))
    {
        OSPF_TRC2 (OS_RESOURCE_TRC,
                   pOspfCxt->u4OspfCxtId,
                   "LSA LSID: %x ADVRTR: %x makes router enter overflow "
                   "state\n",
                   OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                   OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

        RtrEnterOverflowStateInCxt (pOspfCxt);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OspfRmAddToRxmtLst                                        */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update or dynamic update message. This function adds the  */
/*                LSA to the rxmt list. If the pNbr is NULL, then this      */
/*                function is called from bulk update process               */
/*                                                                          */
/* Input        : pLsaInfo    -   Pointer to the LSA info                   */
/*                pNbr  -   Pointer to Neighbor                             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
OspfRmAddToRxmtLst (tLsaInfo * pLsaInfo, tNeighbor * pNbr)
{
    tArea              *pArea = NULL;
    tTMO_SLL_NODE      *pLstIf = NULL;
    tInterface         *pInterface = NULL;

    switch (pLsaInfo->lsaId.u1LsaType)
    {
        case AS_EXT_LSA:
        case TYPE11_OPQ_LSA:

            TMO_SLL_Scan (&(pLsaInfo->pOspfCxt->areasLst), pArea, tArea *)
        {
            if (pArea->u4AreaType != NORMAL_AREA)
            {
                continue;
            }

            TMO_SLL_Scan (&(pArea->ifsInArea), pLstIf, tTMO_SLL_NODE *)
            {
                pInterface = GET_IF_PTR_FROM_LST (pLstIf);

                OspfRmAddToIfaceRxmt (pLsaInfo, pInterface, pNbr);
            }
        }
            break;

        case ROUTER_LSA:
        case NETWORK_LSA:
        case NETWORK_SUM_LSA:
        case NSSA_LSA:
        case TYPE10_OPQ_LSA:

            TMO_SLL_Scan (&(pLsaInfo->pArea->ifsInArea), pLstIf,
                          tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_LST (pLstIf);

            OspfRmAddToIfaceRxmt (pLsaInfo, pInterface, pNbr);
        }
            break;

        default:

            break;
    }
}

/****************************************************************************/
/*                                                                          */
/* Function     : OspfRmAddToIfaceRxmt                                      */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update or dynamic update message. This function adds the  */
/*                LSA to the rxmt list of the neighbors present in the      */
/*                interface. If the pNbr is NULL, then this                 */
/*                function is called from bulk update process               */
/*                                                                          */
/* Input        : pLsaInfo    -   Pointer to the LSA info                   */
/*                pInterface  -   Pointer to Interface                      */
/*                pNeighbour  -   Pointer to Neighbor                       */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

VOID
OspfRmAddToIfaceRxmt (tLsaInfo * pLsaInfo, tInterface * pInterface,
                      tNeighbor * pNeighbour)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tNeighbor          *pNbr = NULL;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNode);

        if ((pNeighbour != NULL) && (pNeighbour == pNbr))
        {
            /* Dynamic update for LSU sent by pNeighbour
             * Do not add to that neighbor's rxmt list
             */
            continue;
        }
        else if ((IS_OPQ_LSA (pLsaInfo->lsaId.u1LsaType)) &&
                 (!(pNbr->nbrOptions & OPQ_BIT_MASK)))
        {
            continue;
        }
        LsuAddToRxmtLst (pNbr, pLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmConstructLsaDescInCxt                                */
/*                                                                           */
/* Description  : This function fetches the AssocPrimitive pointers          */
/*                based on the LSA internal type, and from the respective    */
/*                keys in the RM message                                     */
/*                This function constructs the LSA descriptor for the        */
/*                self orginated LSA's                                       */
/*                                                                           */
/* Input        : pLsaInfo - LSA Info                                        */
/*                pRmMsg  - RM Message                                       */
/*                u4Offset - Current Offset                                  */
/*                pArea - Area pointer                                       */
/*                pu1Lsa  - pointer to buddy memory holding the LSA          */
/*                pLsHeader - pointer to LSA Header                          */
/*                                                                           */
/* Output       : pu4Offset - offset updated to the current read offset      */
/*                                                                           */
/* Returns      : OSPF_FAILURE/OSPF_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmConstructLsaDescInCxt (tLsaInfo * pLsaInfo, tRmMsg * pRmMsg,
                             UINT4 *pu4Offset, tArea * pArea, UINT1 *pu1Lsa,
                             tLsHeader * pLsHeader)
{
    tIPADDR             IfIpAddr;
    tIPADDR             asExtNet;
    tIPADDRMASK         asExtMask;
    tIPADDR             extRouteDest;
    tIPADDR             extRouteMask;
    tAreaId             areaId;
    tMetric             aMetric[OSPF_MAX_METRIC];
    tIPADDR             Addr;
    tIPADDRMASK         addrMask;
    tOspfCxt           *pOspfCxt = pArea->pOspfCxt;
    tOpqLSAInfo        *pOpqLsaInfo = NULL;
    tAddrRange         *pAddrRange = NULL;
    tAsExtAddrRange    *pAsExtAddrRng = NULL;
    tArea              *pLstArea = NULL;
    tExtRoute          *pExtRoute = NULL;
    tInterface         *pIface = NULL;
    UINT1              *pAssoPrimitive = NULL;
    UINT4               u4IfAddrlessIf = 0;
    INT4                i4RetVal = OSPF_SUCCESS;
    UINT1               u1InternalLsaType = OSPF_ZERO;
    UINT1               u1TrnsltType5 = OSPF_ZERO;

    MEMSET (&IfIpAddr, 0, sizeof (tIPADDR));
    MEMSET (&extRouteDest, 0, sizeof (tIPADDR));
    MEMSET (&extRouteMask, 0, sizeof (tIPADDR));
    MEMSET (&areaId, 0, sizeof (tAreaId));
    MEMSET (&aMetric, 0, (sizeof (tMetric) * OSPF_MAX_METRIC));
    MEMSET (&Addr, 0, sizeof (tIPADDR));
    MEMSET (&addrMask, 0, sizeof (tIPADDRMASK));
    MEMSET (&asExtNet, 0, sizeof (tIPADDR));
    MEMSET (&asExtMask, 0, sizeof (tIPADDRMASK));

    OSPF_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1InternalLsaType);
    switch (u1InternalLsaType)
    {
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case INDICATION_LSA:
            SUM_PARAM_ALLOC (pAssoPrimitive);
            if (pAssoPrimitive == NULL)
            {
                i4RetVal = OSPF_FAILURE;
            }
            else
            {
                OSPF_RED_GET_N_BYTE (pRmMsg, pAssoPrimitive, *pu4Offset,
                                     sizeof (tSummaryParam));
            }
            break;
        case ROUTER_LSA:
            pAssoPrimitive = (UINT1 *) pArea;
            break;
        case NETWORK_LSA:
            OSPF_RED_GET_N_BYTE (pRmMsg, IfIpAddr, *pu4Offset,
                                 sizeof (tIPADDR));
            OSPF_RED_GET_4_BYTE (pRmMsg, *pu4Offset, u4IfAddrlessIf);

            pIface = GetFindIfInCxt (pOspfCxt, IfIpAddr, u4IfAddrlessIf);

            if (pIface == NULL)
            {
                i4RetVal = OSPF_FAILURE;
            }
            else
            {
                pAssoPrimitive = (UINT1 *) pIface;
            }
            break;
        case AS_EXT_LSA:
        case NSSA_LSA:
            OSPF_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1TrnsltType5);
            OSPF_RED_GET_N_BYTE (pRmMsg, extRouteDest, *pu4Offset,
                                 sizeof (tIPADDR));
            OSPF_RED_GET_N_BYTE (pRmMsg, extRouteMask, *pu4Offset,
                                 sizeof (tIPADDR));
            pExtRoute = GetFindExtRouteInCxt (pOspfCxt, &extRouteDest,
                                              &extRouteMask);

            pAssoPrimitive = (UINT1 *) pExtRoute;
            break;
        case COND_AS_EXT_LSA:
        case COND_NSSA_LSA:
            OSPF_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1TrnsltType5);

            OSPF_RED_GET_N_BYTE (pRmMsg, asExtNet, *pu4Offset,
                                 sizeof (tIPADDR));
            OSPF_RED_GET_N_BYTE (pRmMsg, asExtMask, *pu4Offset,
                                 sizeof (tIPADDRMASK));
            OSPF_RED_GET_N_BYTE (pRmMsg, areaId, *pu4Offset, sizeof (tAreaId));
            OSPF_RED_GET_N_BYTE (pRmMsg, &(aMetric), *pu4Offset,
                                 (sizeof (tMetric) * OSPF_MAX_METRIC));
            pAsExtAddrRng = GetFindAsExtRngInCxt (pOspfCxt, asExtNet, asExtMask,
                                                  areaId);
            if (pAsExtAddrRng != NULL)
            {
                MEMCPY (&(pAsExtAddrRng->aMetric), &aMetric,
                        (sizeof (tMetric) * OSPF_MAX_METRIC));
                pAssoPrimitive = (UINT1 *) pAsExtAddrRng;
            }

            break;
        case COND_NETWORK_SUM_LSA:
        case AS_TRNSLTD_EXT_RNG_LSA:
            /* address range */
            OSPF_RED_GET_N_BYTE (pRmMsg, areaId, *pu4Offset, sizeof (tAreaId));
            OSPF_RED_GET_N_BYTE (pRmMsg, Addr, *pu4Offset, sizeof (tIPADDR));
            OSPF_RED_GET_N_BYTE (pRmMsg, addrMask, *pu4Offset,
                                 sizeof (tIPADDRMASK));
            OSPF_RED_GET_N_BYTE (pRmMsg, &aMetric, *pu4Offset,
                                 sizeof (tMetric) * OSPF_MAX_METRIC);

            TMO_SLL_Scan (&(pOspfCxt->areasLst), pLstArea, tArea *)
            {
                /* areaId - is the area in which LSA is genereated,
                 * area aggr entry cannot be present in the same Area */
                if (UtilIpAddrComp (areaId, pLstArea->areaId) != OSPF_EQUAL)
                {
                    pAddrRange =
                        OspfRmFindAssoAreaAddrRangeInCxt (pOspfCxt, &Addr,
                                                          &addrMask,
                                                          &(pLstArea->areaId));
                    if (pAddrRange != NULL)
                    {
                        break;
                    }
                }
            }

            if (pAddrRange != NULL)
            {
                MEMCPY (&(pAddrRange->aMetric), &aMetric,
                        (sizeof (tMetric) * OSPF_MAX_METRIC));
                pAssoPrimitive = (UINT1 *) pAddrRange;
            }
            break;
        case TYPE9_OPQ_LSA:
        case TYPE10_OPQ_LSA:
        case TYPE11_OPQ_LSA:

            pOpqLsaInfo = OspfRmConstOpqLsaAssocPrimitive (pOspfCxt, pRmMsg,
                                                           pu4Offset, pu1Lsa,
                                                           pLsHeader);

            if (pOpqLsaInfo == NULL)
            {
                i4RetVal = OSPF_FAILURE;
            }
            pAssoPrimitive = (UINT1 *) pOpqLsaInfo;
            break;
        default:
            break;
    }

    if (i4RetVal == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }

    if (pLsaInfo->pLsaDesc == NULL)
    {
        i4RetVal = OspfRmAllocLsaDescInCxt (pLsaInfo, pAssoPrimitive);
        if (i4RetVal == OSPF_FAILURE)
        {
            if ((u1InternalLsaType == INDICATION_LSA) ||
                (u1InternalLsaType == NETWORK_SUM_LSA) ||
                (u1InternalLsaType == ASBR_SUM_LSA))
            {
                SUM_PARAM_FREE (pAssoPrimitive);
            }
            else if ((u1InternalLsaType == TYPE9_OPQ_LSA) ||
                     (u1InternalLsaType == TYPE10_OPQ_LSA) ||
                     (u1InternalLsaType == TYPE11_OPQ_LSA))
            {
                OPQ_LSA_INFO_FREE (pOpqLsaInfo);
            }
            return OSPF_FAILURE;
        }
    }
    else if (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL)
    {
        switch (u1InternalLsaType)
        {
            case NETWORK_SUM_LSA:
            case ASBR_SUM_LSA:
            case INDICATION_LSA:
                /* Free the old summary param */
                SUM_PARAM_FREE (pLsaInfo->pLsaDesc->pAssoPrimitive);
                break;
            default:
                break;
        }
    }
    pLsaInfo->pLsaDesc->pAssoPrimitive = pAssoPrimitive;
    if (u1InternalLsaType != 0)
    {
        pLsaInfo->pLsaDesc->u1InternalLsaType = u1InternalLsaType;
    }
    if (AS_EXT_LSA == u1InternalLsaType)
    {
        pLsaInfo->u1TrnsltType5 = u1TrnsltType5;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmAllocLsaDesc                                         */
/*                                                                           */
/* Description  : This function allocate and initializes the LSA descriptor  */
/*                                                                           */
/* Input        : pLsaInfo - LSA Info                                        */
/*                pAssoPrimitive - assiciative primitive                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_FAILURE/OSPF_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmAllocLsaDescInCxt (tLsaInfo * pLsaInfo, UINT1 *pAssoPrimitive)
{
    tLsaDesc           *pLsaDesc = NULL;

    if (LSA_DESC_ALLOC (pLsaDesc) == NULL)
    {
        return OSPF_FAILURE;
    }

    TMO_DLL_Init_Node (&(pLsaDesc->lsaDescNode));
    TMO_DLL_Add (&(pLsaInfo->pOspfCxt->origLsaDescLst),
                 &(pLsaDesc->lsaDescNode));
    pLsaDesc->pLsaInfo = pLsaInfo;
    pLsaInfo->pLsaDesc = pLsaDesc;
    pLsaDesc->u1InternalLsaType = pLsaInfo->lsaId.u1LsaType;
    pLsaDesc->u1MinLsaIntervalExpired = OSPF_TRUE;
    pLsaDesc->u1LsaChanged = OSPF_FALSE;
    pLsaDesc->u1SeqNumWrapAround = OSPF_FALSE;
    pLsaDesc->u4LsaGenTime = 0;
    pLsaDesc->pAssoPrimitive = pAssoPrimitive;
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmHandleFuncEqvType9Grace                             */
/*                                                                           */
/* Description  : This function handles functionally equivalent TYPE9_OPQ_LSA*/
/*                When bulk LSU packet is recived.                           */
/*                                                                           */
/* Input        : pLsa - Pointer to LSA                                      */
/*                pNbr - Pointer to neighbor                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmHandleFuncEqvType9Grace (UINT1 *pLsa, tNeighbor * pNbr)
{
    tLsHeader           lsHeader;

    UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    if ((pNbr->pInterface->pArea->pOspfCxt->u1HelperSupport
         & OSPF_GRACE_HELPER_ALL) ||
        (pNbr->pInterface->pArea->pOspfCxt->u1RestartStatus ==
         OSPF_RESTART_NONE))
    {
        GrHelperProcessGraceLSA (pNbr, &lsHeader, pLsa);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmConstOpqLsaAssocPrimitive                            */
/*                                                                           */
/* Description  : This function fetches the AssocPrimitive pointers          */
/*                This function constructs the Associative primitive for     */
/*                self orginated opaque LSA, based the assocprimtive values  */
/*                received from the active node                              */
/*                                                                           */
/* Input        : pOspfCxt - pointer to the OSPF context                     */
/*                pRmMsg  - RM Message                                       */
/*                pu4Offset - Current Offset                                 */
/*                pu1Lsa  - pointer to buddy memory holding the LSA          */
/*                pLsHeader - pointer to LSA Header                          */
/*                                                                           */
/* Output       : pu4Offset - offset updated to the current read offset      */
/*                                                                           */
/* Returns      : pointer to the tOpqLSAInfo (AssocPrimitive for the         */
/*                opaque LSA's)                                              */
/*                                                                           */
/*****************************************************************************/
tOpqLSAInfo        *
OspfRmConstOpqLsaAssocPrimitive (tOspfCxt * pOspfCxt, tRmMsg * pRmMsg,
                                 UINT4 *pu4OffSet, UINT1 *pLsa,
                                 tLsHeader * pLsHeader)
{
    tOpqLSAInfo        *pOpqLsaInfo = NULL;
    UINT1              *pu1TempLsa = NULL;
    UINT4               u4OffSet = *pu4OffSet;
    UINT4               u4IfaceID = OSPF_ZERO;
    UINT4               u4AddrlessIf = OSPF_ZERO;
    UINT4               u4AreaID = OSPF_ZERO;
    UINT2               u2OpqLsaLen = OSPF_ZERO;
    UINT1               u1LSAStatus = OSPF_ZERO;

    OSPF_RED_GET_4_BYTE (pRmMsg, u4OffSet, u4IfaceID);
    OSPF_RED_GET_4_BYTE (pRmMsg, u4OffSet, u4AddrlessIf);
    OSPF_RED_GET_4_BYTE (pRmMsg, u4OffSet, u4AreaID);
    OSPF_RED_GET_2_BYTE (pRmMsg, u4OffSet, u2OpqLsaLen);
    OSPF_RED_GET_1_BYTE (pRmMsg, u4OffSet, u1LSAStatus);

    *pu4OffSet = u4OffSet;

    OPQ_LSA_INFO_ALLOC (pOpqLsaInfo);
    if (pOpqLsaInfo == NULL)
    {
        return NULL;
    }

    if (u2OpqLsaLen > MAX_LSA_SIZE)
    {
        OPQ_LSA_INFO_FREE (pOpqLsaInfo);
        return NULL;
    }

    LSA_ALLOC (TYPE10_OPQ_LSA, (pOpqLsaInfo->pu1LSA), u2OpqLsaLen);
    if (pOpqLsaInfo->pu1LSA == NULL)
    {
        OPQ_LSA_INFO_FREE (pOpqLsaInfo);
        return NULL;
    }

    pOpqLsaInfo->pOspfCxt = pOspfCxt;
    pOpqLsaInfo->u4IfaceID = u4IfaceID;
    pOpqLsaInfo->u4AddrlessIf = u4AddrlessIf;
    pOpqLsaInfo->u4AreaID = u4AreaID;
    pOpqLsaInfo->u2LSALen = u2OpqLsaLen;
    pOpqLsaInfo->u1LSAStatus = u1LSAStatus;
    pu1TempLsa = pLsa;
    pu1TempLsa += LS_HEADER_SIZE;
    MEMCPY (pOpqLsaInfo->pu1LSA, pu1TempLsa, u2OpqLsaLen);
    pOpqLsaInfo->u1OpqLSAType = pLsHeader->u1LsaType;
    MEMCPY (pOpqLsaInfo->LsId, pLsHeader->linkStateId, MAX_IP_ADDR_LEN);
    MEMCPY (pOpqLsaInfo->AdvRtrID, pLsHeader->advRtrId, MAX_IP_ADDR_LEN);
    return pOpqLsaInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmFindAssoAreaAddrRangeInCxt                           */
/*                                                                           */
/* Description  : This function fetches the Address range to be used as      */
/*                Associative primitive for self orginated summary LSA       */
/*                                                                           */
/* Input        : pOspfCxt - pointer to the OSPF context                     */
/*                pAddr,pAddrMask  - identifies the address range pair       */
/*                pAreaId - area associated with the address range           */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : pointer to the address range                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE tAddrRange *
OspfRmFindAssoAreaAddrRangeInCxt (tOspfCxt * pOspfCxt, tIPADDR * pAddr,
                                  tIPADDRMASK * pAddrMask, tAreaId * pAreaId)
{
    UINT1               u1Index;
    tArea              *pArea;

    pArea = GetFindAreaInCxt (pOspfCxt, pAreaId);
    if (pArea == NULL)
    {
        return NULL;
    }

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {

        if ((pArea->aAddrRange[u1Index].areaAggStatus != INVALID) &&
            (UtilIpAddrComp (*pAddr,
                             pArea->aAddrRange[u1Index].ipAddr) == OSPF_EQUAL)
            &&
            (UtilIpAddrComp (*pAddrMask, pArea->aAddrRange[u1Index].ipAddrMask)
             == OSPF_EQUAL))
        {

            return (&(pArea->aAddrRange[u1Index]));
        }
    }

    return NULL;
}
PRIVATE VOID
OspfRmBulkUpdateOffset (tOspfCxt * pOspfCxt, tLsHeader * pLsHeader,
                        tRmMsg * pRmMsg, UINT4 *pu4Offset)
{
    UINT4               u4Len = OSPF_ZERO;
    UINT1               u1InternalLsaType = OSPF_ZERO;

    /* Check if it is self originated LSA */
    if (UtilIpAddrComp (pLsHeader->advRtrId, pOspfCxt->rtrId) != OSPF_EQUAL)
    {
        /* This is not a self orginated LSA */
        return;
    }

    OSPF_RED_GET_1_BYTE (pRmMsg, *pu4Offset, u1InternalLsaType);

    u4Len = *pu4Offset;
    switch (u1InternalLsaType)
    {
        case NETWORK_SUM_LSA:
        case ASBR_SUM_LSA:
        case INDICATION_LSA:
            u4Len = u4Len + sizeof (tSummaryParam);
            break;
        case ROUTER_LSA:
            break;
        case NETWORK_LSA:
            u4Len = u4Len + sizeof (tIPADDR) + sizeof (UINT4);

            break;
        case AS_EXT_LSA:
        case NSSA_LSA:
            u4Len =
                u4Len + sizeof (UINT1) + sizeof (tIPADDR) + sizeof (tIPADDR);
            break;
        case COND_AS_EXT_LSA:
        case COND_NSSA_LSA:
            u4Len =
                u4Len + sizeof (UINT1) + sizeof (tIPADDR) +
                sizeof (tIPADDRMASK) + sizeof (tAreaId) +
                (sizeof (tMetric) * OSPF_MAX_METRIC);
            break;
        case COND_NETWORK_SUM_LSA:
            u4Len = u4Len + sizeof (tIPADDR) + sizeof (tIPADDRMASK)
                + sizeof (tAreaId) + (sizeof (tMetric) * OSPF_MAX_METRIC);
            break;
        case TYPE9_OPQ_LSA:
        case TYPE10_OPQ_LSA:
        case TYPE11_OPQ_LSA:
            u4Len = u4Len + sizeof (UINT4) + sizeof (UINT4)
                + sizeof (UINT4) + sizeof (UINT2) + sizeof (UINT1);
            break;
        default:
            break;
    }

    *pu4Offset = u4Len;
    return;
}
#endif /* _OSREDBLK_ */
