/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: osmiconf.c,v 1.25 2016/03/30 11:24:20 siva Exp $
*
* Description: Protocol Low Level MI Set Routines 
*********************************************************************/

#include  "osinc.h"
#include "ospfcli.h"
#include "fsmioscli.h"
#include "fsmistdospfcli.h"
#include "stdoslow.h"
#include "ospftlow.h"
#include "fsostlow.h"

/* LOW LEVEL  Routines for Table : FsMIStdOspfTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfRouterId
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfRouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfRouterId (INT4 i4FsMIStdOspfContextId,
                           UINT4 u4SetValFsMIStdOspfRouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfRouterId (u4SetValFsMIStdOspfRouterId);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfAdminStat
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfAdminStat (INT4 i4FsMIStdOspfContextId,
                            INT4 i4SetValFsMIStdOspfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfAdminStat (i4SetValFsMIStdOspfAdminStat);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfASBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfASBdrRtrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfASBdrRtrStatus (INT4 i4FsMIStdOspfContextId,
                                 INT4 i4SetValFsMIStdOspfASBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfASBdrRtrStatus (i4SetValFsMIStdOspfASBdrRtrStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfTOSSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfTOSSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfTOSSupport (INT4 i4FsMIStdOspfContextId,
                             INT4 i4SetValFsMIStdOspfTOSSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfTOSSupport (i4SetValFsMIStdOspfTOSSupport);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfExtLsdbLimit
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfExtLsdbLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfExtLsdbLimit (INT4 i4FsMIStdOspfContextId,
                               INT4 i4SetValFsMIStdOspfExtLsdbLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfExtLsdbLimit (i4SetValFsMIStdOspfExtLsdbLimit);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfMulticastExtensions
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfMulticastExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfMulticastExtensions (INT4 i4FsMIStdOspfContextId,
                                      INT4
                                      i4SetValFsMIStdOspfMulticastExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfMulticastExtensions (i4SetValFsMIStdOspfMulticastExtensions);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfExitOverflowInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfExitOverflowInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfExitOverflowInterval (INT4 i4FsMIStdOspfContextId,
                                       INT4
                                       i4SetValFsMIStdOspfExitOverflowInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfExitOverflowInterval
        (i4SetValFsMIStdOspfExitOverflowInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfDemandExtensions
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfDemandExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfDemandExtensions (INT4 i4FsMIStdOspfContextId,
                                   INT4 i4SetValFsMIStdOspfDemandExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfDemandExtensions (i4SetValFsMIStdOspfDemandExtensions);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfStatus
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIStdOspfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfStatus (INT4 i4FsMIStdOspfContextId,
                         INT4 i4SetValFsMIStdOspfStatus)
{
    tOspfCxt           *pOspfCxt = NULL;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdOspfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
#endif
    pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId);

    switch (i4SetValFsMIStdOspfStatus)
    {
        case ACTIVE:
        case CREATE_AND_GO:
        {
            if (pOspfCxt != NULL)
            {
                return SNMP_SUCCESS;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            if (RtrCreateCxt ((UINT4) i4FsMIStdOspfContextId,
                              OSPF_ENABLED) == OSPF_FAILURE)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  i4FsMIStdOspfContextId,
                                  i4SetValFsMIStdOspfStatus));
#endif
                return SNMP_FAILURE;
            }
            if ((pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId)) == NULL)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  i4FsMIStdOspfContextId,
                                  i4SetValFsMIStdOspfStatus));
#endif
                return SNMP_FAILURE;
            }

            /* Admin status of the context will be disabled here.
             * It should be enabled by the Administrator. */
            if (RtrDisableInCxt (pOspfCxt) == OSPF_FAILURE)
            {
#ifdef SNMP_2_WANTED
                SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  i4FsMIStdOspfContextId,
                                  i4SetValFsMIStdOspfStatus));
#endif
                return SNMP_FAILURE;
            }

            pOspfCxt->contextStatus = ACTIVE;
            break;
        }
        case NOT_IN_SERVICE:
        case CREATE_AND_WAIT:
            /* not supported */
            /* For making the ospf status admin enable/disable 
               separate mib object is there */
            return SNMP_SUCCESS;

        case DESTROY:
        {
            if (pOspfCxt == NULL)
            {
                return SNMP_SUCCESS;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;
#endif
            pOspfCxt->contextStatus = DESTROY;
            RtrDeleteCxt (pOspfCxt);
            break;
        }
    }
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FsMIStdOspfContextId,
                      i4SetValFsMIStdOspfStatus));
#endif
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfAreaTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfImportAsExtern
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                setValFsMIStdOspfImportAsExtern
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfImportAsExtern (INT4 i4FsMIStdOspfAreaContextId,
                                 UINT4 u4FsMIStdOspfAreaId,
                                 INT4 i4SetValFsMIStdOspfImportAsExtern)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfImportAsExtern (u4FsMIStdOspfAreaId,
                                         i4SetValFsMIStdOspfImportAsExtern);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfAreaSummary
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                setValFsMIStdOspfAreaSummary
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfAreaSummary (INT4 i4FsMIStdOspfAreaContextId,
                              UINT4 u4FsMIStdOspfAreaId,
                              INT4 i4SetValFsMIStdOspfAreaSummary)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfAreaSummary (u4FsMIStdOspfAreaId,
                                      i4SetValFsMIStdOspfAreaSummary);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfAreaStatus
 Input       :  The Indices
                FsMIStdOspfAreaContextId
                FsMIStdOspfAreaId

                The Object 
                setValFsMIStdOspfAreaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfAreaStatus (INT4 i4FsMIStdOspfAreaContextId,
                             UINT4 u4FsMIStdOspfAreaId,
                             INT4 i4SetValFsMIStdOspfAreaStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfAreaStatus (u4FsMIStdOspfAreaId,
                                     i4SetValFsMIStdOspfAreaStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfStubAreaTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfStubMetric
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                setValFsMIStdOspfStubMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfStubMetric (INT4 i4FsMIStdOspfStubContextId,
                             UINT4 u4FsMIStdOspfStubAreaId,
                             INT4 i4FsMIStdOspfStubTOS,
                             INT4 i4SetValFsMIStdOspfStubMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfStubMetric (u4FsMIStdOspfStubAreaId,
                                     i4FsMIStdOspfStubTOS,
                                     i4SetValFsMIStdOspfStubMetric);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfStubStatus
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                setValFsMIStdOspfStubStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfStubStatus (INT4 i4FsMIStdOspfStubContextId,
                             UINT4 u4FsMIStdOspfStubAreaId,
                             INT4 i4FsMIStdOspfStubTOS,
                             INT4 i4SetValFsMIStdOspfStubStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfStubStatus (u4FsMIStdOspfStubAreaId,
                                     i4FsMIStdOspfStubTOS,
                                     i4SetValFsMIStdOspfStubStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfStubMetricType
 Input       :  The Indices
                FsMIStdOspfStubContextId
                FsMIStdOspfStubAreaId
                FsMIStdOspfStubTOS

                The Object 
                setValFsMIStdOspfStubMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfStubMetricType (INT4 i4FsMIStdOspfStubContextId,
                                 UINT4 u4FsMIStdOspfStubAreaId,
                                 INT4 i4FsMIStdOspfStubTOS,
                                 INT4 i4SetValFsMIStdOspfStubMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfStubContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfStubMetricType (u4FsMIStdOspfStubAreaId,
                                         i4FsMIStdOspfStubTOS,
                                         i4SetValFsMIStdOspfStubMetricType);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfHostTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfHostMetric
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                setValFsMIStdOspfHostMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfHostMetric (INT4 i4FsMIStdOspfHostContextId,
                             UINT4 u4FsMIStdOspfHostIpAddress,
                             INT4 i4FsMIStdOspfHostTOS,
                             INT4 i4SetValFsMIStdOspfHostMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfHostMetric (u4FsMIStdOspfHostIpAddress,
                                     i4FsMIStdOspfHostTOS,
                                     i4SetValFsMIStdOspfHostMetric);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfHostStatus
 Input       :  The Indices
                FsMIStdOspfHostContextId
                FsMIStdOspfHostIpAddress
                FsMIStdOspfHostTOS

                The Object 
                setValFsMIStdOspfHostStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfHostStatus (INT4 i4FsMIStdOspfHostContextId,
                             UINT4 u4FsMIStdOspfHostIpAddress,
                             INT4 i4FsMIStdOspfHostTOS,
                             INT4 i4SetValFsMIStdOspfHostStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfHostStatus (u4FsMIStdOspfHostIpAddress,
                                     i4FsMIStdOspfHostTOS,
                                     i4SetValFsMIStdOspfHostStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfIfTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfAreaId
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfAreaId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfAreaId (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           UINT4 u4SetValFsMIStdOspfIfAreaId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfAreaId (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   u4SetValFsMIStdOspfIfAreaId);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfType (INT4 i4FsMIStdOspfIfContextId,
                         UINT4 u4FsMIStdOspfIfIpAddress,
                         INT4 i4FsMIStdOspfAddressLessIf,
                         INT4 i4SetValFsMIStdOspfIfType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfType (u4FsMIStdOspfIfIpAddress,
                                 i4FsMIStdOspfAddressLessIf,
                                 i4SetValFsMIStdOspfIfType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfAdminStat
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfAdminStat (INT4 i4FsMIStdOspfIfContextId,
                              UINT4 u4FsMIStdOspfIfIpAddress,
                              INT4 i4FsMIStdOspfAddressLessIf,
                              INT4 i4SetValFsMIStdOspfIfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfAdminStat (u4FsMIStdOspfIfIpAddress,
                                      i4FsMIStdOspfAddressLessIf,
                                      i4SetValFsMIStdOspfIfAdminStat);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfRtrPriority
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfRtrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfRtrPriority (INT4 i4FsMIStdOspfIfContextId,
                                UINT4 u4FsMIStdOspfIfIpAddress,
                                INT4 i4FsMIStdOspfAddressLessIf,
                                INT4 i4SetValFsMIStdOspfIfRtrPriority)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfRtrPriority (u4FsMIStdOspfIfIpAddress,
                                        i4FsMIStdOspfAddressLessIf,
                                        i4SetValFsMIStdOspfIfRtrPriority);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfTransitDelay (INT4 i4FsMIStdOspfIfContextId,
                                 UINT4 u4FsMIStdOspfIfIpAddress,
                                 INT4 i4FsMIStdOspfAddressLessIf,
                                 INT4 i4SetValFsMIStdOspfIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfTransitDelay (u4FsMIStdOspfIfIpAddress,
                                         i4FsMIStdOspfAddressLessIf,
                                         i4SetValFsMIStdOspfIfTransitDelay);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfRetransInterval (INT4 i4FsMIStdOspfIfContextId,
                                    UINT4 u4FsMIStdOspfIfIpAddress,
                                    INT4 i4FsMIStdOspfAddressLessIf,
                                    INT4 i4SetValFsMIStdOspfIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfRetransInterval (u4FsMIStdOspfIfIpAddress,
                                            i4FsMIStdOspfAddressLessIf,
                                            i4SetValFsMIStdOspfIfRetransInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfHelloInterval (INT4 i4FsMIStdOspfIfContextId,
                                  UINT4 u4FsMIStdOspfIfIpAddress,
                                  INT4 i4FsMIStdOspfAddressLessIf,
                                  INT4 i4SetValFsMIStdOspfIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfHelloInterval (u4FsMIStdOspfIfIpAddress,
                                          i4FsMIStdOspfAddressLessIf,
                                          i4SetValFsMIStdOspfIfHelloInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfRtrDeadInterval (INT4 i4FsMIStdOspfIfContextId,
                                    UINT4 u4FsMIStdOspfIfIpAddress,
                                    INT4 i4FsMIStdOspfAddressLessIf,
                                    INT4 i4SetValFsMIStdOspfIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfRtrDeadInterval (u4FsMIStdOspfIfIpAddress,
                                            i4FsMIStdOspfAddressLessIf,
                                            i4SetValFsMIStdOspfIfRtrDeadInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfPollInterval
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfPollInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfPollInterval (INT4 i4FsMIStdOspfIfContextId,
                                 UINT4 u4FsMIStdOspfIfIpAddress,
                                 INT4 i4FsMIStdOspfAddressLessIf,
                                 INT4 i4SetValFsMIStdOspfIfPollInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfPollInterval (u4FsMIStdOspfIfIpAddress,
                                         i4FsMIStdOspfAddressLessIf,
                                         i4SetValFsMIStdOspfIfPollInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfAuthKey
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfAuthKey (INT4 i4FsMIStdOspfIfContextId,
                            UINT4 u4FsMIStdOspfIfIpAddress,
                            INT4 i4FsMIStdOspfAddressLessIf,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsMIStdOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfAuthKey (u4FsMIStdOspfIfIpAddress,
                                    i4FsMIStdOspfAddressLessIf,
                                    pSetValFsMIStdOspfIfAuthKey);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfStatus
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfStatus (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           INT4 i4SetValFsMIStdOspfIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfStatus (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   i4SetValFsMIStdOspfIfStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfMulticastForwarding
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfMulticastForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfMulticastForwarding (INT4 i4FsMIStdOspfIfContextId,
                                        UINT4 u4FsMIStdOspfIfIpAddress,
                                        INT4 i4FsMIStdOspfAddressLessIf,
                                        INT4
                                        i4SetValFsMIStdOspfIfMulticastForwarding)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfMulticastForwarding (u4FsMIStdOspfIfIpAddress,
                                                i4FsMIStdOspfAddressLessIf,
                                                i4SetValFsMIStdOspfIfMulticastForwarding);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfDemand
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfDemand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfDemand (INT4 i4FsMIStdOspfIfContextId,
                           UINT4 u4FsMIStdOspfIfIpAddress,
                           INT4 i4FsMIStdOspfAddressLessIf,
                           INT4 i4SetValFsMIStdOspfIfDemand)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfDemand (u4FsMIStdOspfIfIpAddress,
                                   i4FsMIStdOspfAddressLessIf,
                                   i4SetValFsMIStdOspfIfDemand);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfAuthType
 Input       :  The Indices
                FsMIStdOspfIfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                setValFsMIStdOspfIfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfAuthType (INT4 i4FsMIStdOspfIfContextId,
                             UINT4 u4FsMIStdOspfIfIpAddress,
                             INT4 i4FsMIStdOspfAddressLessIf,
                             INT4 i4SetValFsMIStdOspfIfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfAuthType (u4FsMIStdOspfIfIpAddress,
                                     i4FsMIStdOspfAddressLessIf,
                                     i4SetValFsMIStdOspfIfAuthType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIStdOspfIfIpAddress
                FsMIStdOspfAddressLessIf

                The Object 
                retValFsMIStdOspfIfCryptoAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfCryptoAuthType (INT4 i4FsMIStdOspfContextId,
                                   UINT4 u4FsMIStdOspfIfIpAddress,
                                   INT4 i4FsMIStdOspfAddressLessIf,
                                   INT4 i4SetValFsMIStdOspfIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfIfCryptoAuthType (u4FsMIStdOspfIfIpAddress,
                                              (UINT4)
                                              i4FsMIStdOspfAddressLessIf,
                                              i4SetValFsMIStdOspfIfCryptoAuthType);

    UtilOspfResetContext ();
    return i1Return;

}

/* LOW LEVEL Routines for Table : FsMIStdOspfIfMetricTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfMetricValue
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS

                The Object 
                setValFsMIStdOspfIfMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfMetricValue (INT4 i4FsMIStdOspfIfMetricContextId,
                                UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                INT4 i4FsMIStdOspfIfMetricAddressLessIf,
                                INT4 i4FsMIStdOspfIfMetricTOS,
                                INT4 i4SetValFsMIStdOspfIfMetricValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfMetricValue (u4FsMIStdOspfIfMetricIpAddress,
                                        i4FsMIStdOspfIfMetricAddressLessIf,
                                        i4FsMIStdOspfIfMetricTOS,
                                        i4SetValFsMIStdOspfIfMetricValue);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfIfMetricStatus
 Input       :  The Indices
                FsMIStdOspfIfMetricContextId
                FsMIStdOspfIfMetricIpAddress
                FsMIStdOspfIfMetricAddressLessIf
                FsMIStdOspfIfMetricTOS

                The Object 
                setValFsMIStdOspfIfMetricStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfIfMetricStatus (INT4 i4FsMIStdOspfIfMetricContextId,
                                 UINT4 u4FsMIStdOspfIfMetricIpAddress,
                                 INT4 i4FsMIStdOspfIfMetricAddressLessIf,
                                 INT4 i4FsMIStdOspfIfMetricTOS,
                                 INT4 i4SetValFsMIStdOspfIfMetricStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfIfMetricContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfIfMetricStatus (u4FsMIStdOspfIfMetricIpAddress,
                                         i4FsMIStdOspfIfMetricAddressLessIf,
                                         i4FsMIStdOspfIfMetricTOS,
                                         i4SetValFsMIStdOspfIfMetricStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfVirtIfTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfTransitDelay (INT4 i4FsMIStdOspfVirtIfContextId,
                                     UINT4 u4FsMIStdOspfVirtIfAreaId,
                                     UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                     INT4 i4SetValFsMIStdOspfVirtIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfTransitDelay (u4FsMIStdOspfVirtIfAreaId,
                                             u4FsMIStdOspfVirtIfNeighbor,
                                             i4SetValFsMIStdOspfVirtIfTransitDelay);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfRetransInterval (INT4 i4FsMIStdOspfVirtIfContextId,
                                        UINT4 u4FsMIStdOspfVirtIfAreaId,
                                        UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                        INT4
                                        i4SetValFsMIStdOspfVirtIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfRetransInterval (u4FsMIStdOspfVirtIfAreaId,
                                                u4FsMIStdOspfVirtIfNeighbor,
                                                i4SetValFsMIStdOspfVirtIfRetransInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfHelloInterval (INT4 i4FsMIStdOspfVirtIfContextId,
                                      UINT4 u4FsMIStdOspfVirtIfAreaId,
                                      UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                      INT4
                                      i4SetValFsMIStdOspfVirtIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfHelloInterval (u4FsMIStdOspfVirtIfAreaId,
                                              u4FsMIStdOspfVirtIfNeighbor,
                                              i4SetValFsMIStdOspfVirtIfHelloInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfRtrDeadInterval (INT4 i4FsMIStdOspfVirtIfContextId,
                                        UINT4 u4FsMIStdOspfVirtIfAreaId,
                                        UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                        INT4
                                        i4SetValFsMIStdOspfVirtIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfRtrDeadInterval (u4FsMIStdOspfVirtIfAreaId,
                                                u4FsMIStdOspfVirtIfNeighbor,
                                                i4SetValFsMIStdOspfVirtIfRtrDeadInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfAuthKey (INT4 i4FsMIStdOspfVirtIfContextId,
                                UINT4 u4FsMIStdOspfVirtIfAreaId,
                                UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMIStdOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfAuthKey (u4FsMIStdOspfVirtIfAreaId,
                                        u4FsMIStdOspfVirtIfNeighbor,
                                        pSetValFsMIStdOspfVirtIfAuthKey);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfStatus
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfStatus (INT4 i4FsMIStdOspfVirtIfContextId,
                               UINT4 u4FsMIStdOspfVirtIfAreaId,
                               UINT4 u4FsMIStdOspfVirtIfNeighbor,
                               INT4 i4SetValFsMIStdOspfVirtIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfStatus (u4FsMIStdOspfVirtIfAreaId,
                                       u4FsMIStdOspfVirtIfNeighbor,
                                       i4SetValFsMIStdOspfVirtIfStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfAuthType
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfAuthType (INT4 i4FsMIStdOspfVirtIfContextId,
                                 UINT4 u4FsMIStdOspfVirtIfAreaId,
                                 UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                 INT4 i4SetValFsMIStdOspfVirtIfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfVirtIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfVirtIfAuthType (u4FsMIStdOspfVirtIfAreaId,
                                         u4FsMIStdOspfVirtIfNeighbor,
                                         i4SetValFsMIStdOspfVirtIfAuthType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfVirtIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfVirtIfContextId
                FsMIStdOspfVirtIfAreaId
                FsMIStdOspfVirtIfNeighbor

                The Object 
                setValFsMIStdOspfVirtIfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfVirtIfCryptoAuthType (INT4 i4FsMIStdOspfVirtIfContextId,
                                       UINT4 u4FsMIStdOspfVirtIfAreaId,
                                       UINT4 u4FsMIStdOspfVirtIfNeighbor,
                                       INT4
                                       i4SetValFsMIStdOspfVirtIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfVirtIfContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfVirtIfCryptoAuthType (u4FsMIStdOspfVirtIfAreaId,
                                                  u4FsMIStdOspfVirtIfNeighbor,
                                                  i4SetValFsMIStdOspfVirtIfCryptoAuthType);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfNbrTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfNbrPriority
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                setValFsMIStdOspfNbrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfNbrPriority (INT4 i4FsMIStdOspfNbrContextId,
                              UINT4 u4FsMIStdOspfNbrIpAddr,
                              INT4 i4FsMIStdOspfNbrAddressLessIndex,
                              INT4 i4SetValFsMIStdOspfNbrPriority)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfNbrPriority (u4FsMIStdOspfNbrIpAddr,
                                      i4FsMIStdOspfNbrAddressLessIndex,
                                      i4SetValFsMIStdOspfNbrPriority);
    UtilOspfResetContext ();

    return i1Return;
}

 /****************************************************************************
 Function    :  nmhSetFsMIStdOspfNbmaNbrStatus
 Input       :  The Indices
                FsMIStdOspfNbrContextId
                FsMIStdOspfNbrIpAddr
                FsMIStdOspfNbrAddressLessIndex

                The Object 
                setValFsMIStdOspfNbmaNbrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfNbmaNbrStatus (INT4 i4FsMIStdOspfNbrContextId,
                                UINT4 u4FsMIStdOspfNbrIpAddr,
                                INT4 i4FsMIStdOspfNbrAddressLessIndex,
                                INT4 i4SetValFsMIStdOspfNbmaNbrStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfNbrContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfNbmaNbrStatus (u4FsMIStdOspfNbrIpAddr,
                                        i4FsMIStdOspfNbrAddressLessIndex,
                                        i4SetValFsMIStdOspfNbmaNbrStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfAreaAggregateTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfAreaAggregateStatus
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask

                The Object 
                setValFsMIStdOspfAreaAggregateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfAreaAggregateStatus (INT4
                                      i4FsMIStdOspfAreaAggregateContextId,
                                      UINT4 u4FsMIStdOspfAreaAggregateAreaID,
                                      INT4 i4FsMIStdOspfAreaAggregateLsdbType,
                                      UINT4 u4FsMIStdOspfAreaAggregateNet,
                                      UINT4 u4FsMIStdOspfAreaAggregateMask,
                                      INT4
                                      i4SetValFsMIStdOspfAreaAggregateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfAreaAggregateStatus (u4FsMIStdOspfAreaAggregateAreaID,
                                       i4FsMIStdOspfAreaAggregateLsdbType,
                                       u4FsMIStdOspfAreaAggregateNet,
                                       u4FsMIStdOspfAreaAggregateMask,
                                       i4SetValFsMIStdOspfAreaAggregateStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfAreaAggregateEffect
 Input       :  The Indices
                FsMIStdOspfAreaAggregateContextId
                FsMIStdOspfAreaAggregateAreaID
                FsMIStdOspfAreaAggregateLsdbType
                FsMIStdOspfAreaAggregateNet
                FsMIStdOspfAreaAggregateMask

                The Object 
                setValFsMIStdOspfAreaAggregateEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfAreaAggregateEffect (INT4
                                      i4FsMIStdOspfAreaAggregateContextId,
                                      UINT4 u4FsMIStdOspfAreaAggregateAreaID,
                                      INT4 i4FsMIStdOspfAreaAggregateLsdbType,
                                      UINT4 u4FsMIStdOspfAreaAggregateNet,
                                      UINT4 u4FsMIStdOspfAreaAggregateMask,
                                      INT4
                                      i4SetValFsMIStdOspfAreaAggregateEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfAreaAggregateEffect (u4FsMIStdOspfAreaAggregateAreaID,
                                       i4FsMIStdOspfAreaAggregateLsdbType,
                                       u4FsMIStdOspfAreaAggregateNet,
                                       u4FsMIStdOspfAreaAggregateMask,
                                       i4SetValFsMIStdOspfAreaAggregateEffect);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfGlobalTraceLevel
 Input       :  The Indices

                The Object
                setValFsMIOspfGlobalTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfGlobalTraceLevel (INT4 i4SetValFsMIOspfGlobalTraceLevel)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    if (pOspfCxt == NULL)
    {
	return SNMP_FAILURE;
    }
    else
    {  
        if (pOspfCxt->admnStat == OSPF_ENABLED)
	    {
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfGlobalTraceLevel;
    SnmpNotifyInfo.u4OidLen
        = sizeof (FsMIOspfGlobalTraceLevel) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
#endif
    gOsRtr.u4OspfGblTrace = (UINT4) i4SetValFsMIOspfGlobalTraceLevel;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsMIOspfGlobalTraceLevel));
#endif
    return SNMP_SUCCESS;
           }
            /*If OSPF is disabled,retun failure*/
	    return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVrfSpfInterval
 Input       :  The Indices

                The Object
                setValFsMIOspfVrfSpfInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVrfSpfInterval (INT4 i4SetValFsMIOspfVrfSpfInterval)
{
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
    if (pOspfCxt->admnStat == OSPF_ENABLED)
       {

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfVrfSpfInterval;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfVrfSpfInterval) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
#endif
    gOsRtr.u4VrfSpfInterval = (UINT4) i4SetValFsMIOspfVrfSpfInterval;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsMIOspfVrfSpfInterval));
#endif
    return SNMP_SUCCESS;
       }
      /*If OSPF is disabled,retun failure*/
      return SNMP_FAILURE;
    }

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfRFC1583Compatibility
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                setValFsMIOspfRFC1583Compatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRFC1583Compatibility (INT4 i4FsMIOspfContextId,
                                    INT4 i4SetValFsMIOspfRFC1583Compatibility)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfRFC1583Compatibility
        (i4SetValFsMIOspfRFC1583Compatibility);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfTraceLevel
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                setValFsMIOspfTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfTraceLevel (INT4 i4FsMIOspfContextId,
                          INT4 i4SetValFsMIOspfTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfTraceLevel (i4SetValFsMIOspfTraceLevel);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfMinLsaInterval
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                setValFsMIOspfMinLsaInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfMinLsaInterval (INT4 i4FsMIOspfContextId,
                              INT4 i4SetValFsMIOspfMinLsaInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfMinLsaInterval (i4SetValFsMIOspfMinLsaInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfABRType
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                setValFsMIOspfABRType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfABRType (INT4 i4FsMIOspfContextId, INT4 i4SetValFsMIOspfABRType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfABRType (i4SetValFsMIOspfABRType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfNssaAsbrDefRtTrans
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                setValFsMIOspfNssaAsbrDefRtTrans
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfNssaAsbrDefRtTrans (INT4 i4FsMIOspfContextId,
                                  INT4 i4SetValFsMIOspfNssaAsbrDefRtTrans)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfNssaAsbrDefRtTrans (i4SetValFsMIOspfNssaAsbrDefRtTrans);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfDefaultPassiveInterface
 Input       :  The Indices
                FsMIOspfContextId

                The Object 
                setValFsMIOspfDefaultPassiveInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfDefaultPassiveInterface (INT4 i4FsMIOspfContextId,
                                       INT4
                                       i4SetValFsMIOspfDefaultPassiveInterface)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfDefaultPassiveInterface
        (i4SetValFsMIOspfDefaultPassiveInterface);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfSpfHoldtime
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfSpfHoldtime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfSpfHoldtime (INT4 i4FsMIStdOspfContextId,
                           INT4 i4SetValFsMIOspfSpfHoldtime)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfSpfHoldtime (i4SetValFsMIOspfSpfHoldtime);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfSpfDelay
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfSpfDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfSpfDelay (INT4 i4FsMIStdOspfContextId,
                        INT4 i4SetValFsMIOspfSpfDelay)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfSpfDelay (i4SetValFsMIOspfSpfDelay);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRestartSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfRestartSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRestartSupport (INT4 i4FsMIStdOspfContextId,
                              INT4 i4SetValFsMIOspfRestartSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRestartSupport (i4SetValFsMIOspfRestartSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRestartInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfRestartInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsMIOspfRestartInterval (INT4 i4FsMIStdOspfContextId,
                               INT4 i4SetValFsMIOspfRestartInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRestartInterval (i4SetValFsMIOspfRestartInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRestartStrictLsaChecking
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfRestartStrictLsaChecking
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMIOspfRestartStrictLsaChecking
    (INT4 i4FsMIStdOspfContextId, INT4 i4SetValFsMIOspfRestartStrictLsaChecking)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRestartStrictLsaChecking
        (i4SetValFsMIOspfRestartStrictLsaChecking);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfHelperSupport
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfHelperSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfHelperSupport (INT4 i4FsMIStdOspfContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsMIOspfHelperSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfHelperSupport (pSetValFsMIOspfHelperSupport);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtTraceLevel
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfExtTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtTraceLevel (INT4 i4FsMIStdOspfContextId,
                             INT4 i4SetValFsMIOspfExtTraceLevel)
{

    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtTraceLevel (i4SetValFsMIOspfExtTraceLevel);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfHelperGraceTimeLimit
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfHelperGraceTimeLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfHelperGraceTimeLimit (INT4 i4FsMIStdOspfContextId,
                                    INT4 i4SetValFsMIOspfHelperGraceTimeLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfHelperGraceTimeLimit
        (i4SetValFsMIOspfHelperGraceTimeLimit);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRestartAckState
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfRestartAckState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRestartAckState (INT4 i4FsMIStdOspfContextId,
                               INT4 i4SetValFsMIOspfRestartAckState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRestartAckState (i4SetValFsMIOspfRestartAckState);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfGraceLsaRetransmitCount
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfGraceLsaRetransmitCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfGraceLsaRetransmitCount (INT4 i4FsMIStdOspfContextId,
                                       INT4
                                       i4SetValFsMIOspfGraceLsaRetransmitCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfGraceLsaRetransmitCount
        (i4SetValFsMIOspfGraceLsaRetransmitCount);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRestartReason
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfRestartReason
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRestartReason (INT4 i4FsMIStdOspfContextId,
                             INT4 i4SetValFsMIOspfRestartReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRestartReason (i4SetValFsMIOspfRestartReason);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfGlobalExtTraceLevel
 Input       :  The Indices

                The Object
                setValFsMIOspfGlobalExtTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfGlobalExtTraceLevel (INT4 i4SetValFsMIOspfGlobalExtTraceLevel)
{
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfGlobalExtTraceLevel;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfGlobalExtTraceLevel) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
#endif
    gOsRtr.u4OspfGblExtTrace = i4SetValFsMIOspfGlobalExtTraceLevel;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",i4SetValFsMIOspfGlobalExtTraceLevel));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRTStaggeringStatus
 Input       :  The Indices

                The Object
                setValFsMIOspfRTStaggeringStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRTStaggeringStatus (INT4 i4SetValFsMIOspfRTStaggeringStatus)
{
    /* This object is applicable to all context */

    INT1                i1Return = SNMP_FAILURE;

    gOsRtr.u4RTStaggeringStatus = (UINT4) i4SetValFsMIOspfRTStaggeringStatus;

    /* Set the SI object */
    i1Return =
        nmhSetFutOspfRTStaggeringStatus (i4SetValFsMIOspfRTStaggeringStatus);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRTStaggeringInterval
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfRTStaggeringInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRTStaggeringInterval (INT4 i4FsMIStdOspfContextId,
                                    UINT4 u4SetValFsMIOspfRTStaggeringInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfRTStaggeringInterval
        (u4SetValFsMIOspfRTStaggeringInterval);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************                      Function    :  nmhSetFsMIOspfRouterIdPermanence
  Input       :  The Indices
                 FsMIStdOspfContextId
                 The Object
                 setValFsMIOspfRouterIdPermanence
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRouterIdPermanence (INT4 i4FsMIStdOspfContextId,
                                  INT4 i4SetValFsMIOspfRouterIdPermanence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutospfRouterIdPermanence (i4SetValFsMIOspfRouterIdPermanence);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfBfdStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                The Object
                setValFsMIOspfBfdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFsMIOspfBfdStatus (INT4 i4FsMIStdOspfContextId,
                         INT4 i4SetValFsMIOspfBfdStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfBfdStatus (i4SetValFsMIOspfBfdStatus);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfBfdAllIfState
 Input       :  The Indices
                FsMIStdOspfContextId
                The Object
                setValFsMIOspfBfdAllIfState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/
INT1
nmhSetFsMIOspfBfdAllIfState (INT4 i4FsMIStdOspfContextId,
                             INT4 i4SetValFsMIOspfBfdAllIfState)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfBfdAllIfState (i4SetValFsMIOspfBfdAllIfState);
    UtilOspfResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfAreaNSSATranslatorRole
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                setValFsMIOspfAreaNSSATranslatorRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAreaNSSATranslatorRole (INT4 i4FsMIOspfAreaContextId,
                                      UINT4 u4FsMIOspfAreaId,
                                      INT4
                                      i4SetValFsMIOspfAreaNSSATranslatorRole)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfAreaNSSATranslatorRole (u4FsMIOspfAreaId,
                                                    i4SetValFsMIOspfAreaNSSATranslatorRole);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfAreaNSSATranslatorStabilityInterval
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                setValFsMIOspfAreaNSSATranslatorStabilityInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAreaNSSATranslatorStabilityInterval (INT4
                                                   i4FsMIOspfAreaContextId,
                                                   UINT4 u4FsMIOspfAreaId,
                                                   INT4
                                                   i4SetValFsMIOspfAreaNSSATranslatorStabilityInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfAreaNSSATranslatorStabilityInterval (u4FsMIOspfAreaId,
                                                          i4SetValFsMIOspfAreaNSSATranslatorStabilityInterval);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfAreaDfInfOriginate
 Input       :  The Indices
                FsMIOspfAreaContextId
                FsMIOspfAreaId

                The Object 
                setValFsMIOspfAreaDfInfOriginate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAreaDfInfOriginate (INT4
                                  i4FsMIOspfAreaContextId,
                                  UINT4 u4FsMIOspfAreaId,
                                  INT4 i4SetValFsMIOspfAreaDfInfOriginate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfAreaContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfAreaDfInfOriginate (u4FsMIOspfAreaId,
                                         i4SetValFsMIOspfAreaDfInfOriginate);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfHostRouteIfIndex
 Input       :  The Indices
                FsMIOspfHostContextId
                FsMIOspfHostIpAddress
                FsMIOspfHostTOS

                The Object 
                setValFsMIOspfHostRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfHostRouteIfIndex (INT4 i4FsMIOspfHostContextId,
                                UINT4 u4FsMIOspfHostIpAddress,
                                INT4 i4FsMIOspfHostTOS,
                                INT4 i4SetValFsMIOspfHostRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfHostContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfHostRouteIfIndex (u4FsMIOspfHostIpAddress,
                                              i4FsMIOspfHostTOS,
                                              i4SetValFsMIOspfHostRouteIfIndex);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfPassive
 Input       :  The Indices
                FsMIOspfIfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf

                The Object 
                setValFsMIOspfIfPassive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfPassive (INT4 i4FsMIOspfIfContextId,
                         UINT4 u4FsMIOspfIfIpAddress,
                         INT4 i4FsMIOspfAddressLessIf,
                         INT4 i4SetValFsMIOspfIfPassive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfIfPassive (u4FsMIOspfIfIpAddress,
                                       i4FsMIOspfAddressLessIf,
                                       i4SetValFsMIOspfIfPassive);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfBfdState
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfIpAddress
                FsMIOspfAddressLessIf
                The Object
                setValFsMIOspfIfBfdState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFsMIOspfIfBfdState (INT4 i4FsMIStdOspfContextId,
                          UINT4 u4FsMIOspfIfIpAddress,
                          INT4 i4FsMIOspfAddressLessIf,
                          INT4 i4SetValFsMIOspfIfBfdState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfIfBfdState (u4FsMIOspfIfIpAddress,
                                        i4FsMIOspfAddressLessIf,
                                        i4SetValFsMIOspfIfBfdState);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfMD5AuthKey
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                setValFsMIOspfIfMD5AuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfMD5AuthKey (INT4 i4FsMIOspfIfMD5AuthContextId,
                            UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                            INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                            INT4 i4FsMIOspfIfMD5AuthKeyId,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsMIOspfIfMD5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfIfMD5AuthKey (u4FsMIOspfIfMD5AuthIpAddress,
                                          i4FsMIOspfIfMD5AuthAddressLessIf,
                                          i4FsMIOspfIfMD5AuthKeyId,
                                          pSetValFsMIOspfIfMD5AuthKey);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfAuthKey
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                setValFsMIOspfIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfAuthKey (INT4 i4FsMIStdOspfContextId,
                         UINT4 u4FsMIOspfIfAuthIpAddress,
                         INT4 i4FsMIOspfIfAuthAddressLessIf,
                         INT4 i4FsMIOspfIfAuthKeyId,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsMIOspfIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfIfAuthKey (u4FsMIOspfIfAuthIpAddress,
                                       i4FsMIOspfIfAuthAddressLessIf,
                                       i4FsMIOspfIfAuthKeyId,
                                       pSetValFsMIOspfIfAuthKey);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                setValFsMIOspfIfMD5AuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfMD5AuthKeyStartAccept (INT4 i4FsMIOspfIfMD5AuthContextId,
                                       UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                       INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                       INT4 i4FsMIOspfIfMD5AuthKeyId,
                                       INT4
                                       i4SetValFsMIOspfIfMD5AuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStartAccept (u4FsMIOspfIfMD5AuthIpAddress,
                                              i4FsMIOspfIfMD5AuthAddressLessIf,
                                              i4FsMIOspfIfMD5AuthKeyId,
                                              i4SetValFsMIOspfIfMD5AuthKeyStartAccept);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                setValFsMIOspfIfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfAuthKeyStartAccept (INT4 i4FsMIStdOspfContextId,
                                    UINT4 u4FsMIOspfIfAuthIpAddress,
                                    INT4 i4FsMIOspfIfAuthAddressLessIf,
                                    INT4 i4FsMIOspfIfAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsMIOspfIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFutOspfIfAuthKeyStartAccept (u4FsMIOspfIfAuthIpAddress,
                                           i4FsMIOspfIfAuthAddressLessIf,
                                           i4FsMIOspfIfAuthKeyId,
                                           pSetValFsMIOspfIfAuthKeyStartAccept);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                setValFsMIOspfIfMD5AuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfMD5AuthKeyStartGenerate (INT4 i4FsMIOspfIfMD5AuthContextId,
                                         UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                         INT4
                                         i4FsMIOspfIfMD5AuthAddressLessIf,
                                         INT4 i4FsMIOspfIfMD5AuthKeyId,
                                         INT4
                                         i4SetValFsMIOspfIfMD5AuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStartGenerate (u4FsMIOspfIfMD5AuthIpAddress,
                                                i4FsMIOspfIfMD5AuthAddressLessIf,
                                                i4FsMIOspfIfMD5AuthKeyId,
                                                i4SetValFsMIOspfIfMD5AuthKeyStartGenerate);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                setValFsMIOspfIfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfAuthKeyStartGenerate (INT4 i4FsMIStdOspfContextId,
                                      UINT4 u4FsMIOspfIfAuthIpAddress,
                                      INT4 i4FsMIOspfIfAuthAddressLessIf,
                                      INT4 i4FsMIOspfIfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsMIOspfIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
/*    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;
    OspfConvertTime (pSetValFsMIOspfIfAuthKeyStartGenerate->pu1_OctetList , &tm); 
    i4AuthKeyStartGenerate =  GrGetSecondsSinceBase (tm);
*/

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFutOspfIfAuthKeyStartGenerate (u4FsMIOspfIfAuthIpAddress,
                                             i4FsMIOspfIfAuthAddressLessIf,
                                             i4FsMIOspfIfAuthKeyId,
                                             pSetValFsMIOspfIfAuthKeyStartGenerate);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                setValFsMIOspfIfMD5AuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfMD5AuthKeyStopGenerate (INT4 i4FsMIOspfIfMD5AuthContextId,
                                        UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                        INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                        INT4 i4FsMIOspfIfMD5AuthKeyId,
                                        INT4
                                        i4SetValFsMIOspfIfMD5AuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStopGenerate (u4FsMIOspfIfMD5AuthIpAddress,
                                               i4FsMIOspfIfMD5AuthAddressLessIf,
                                               i4FsMIOspfIfMD5AuthKeyId,
                                               i4SetValFsMIOspfIfMD5AuthKeyStopGenerate);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                setValFsMIOspfIfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfAuthKeyStopGenerate (INT4 i4FsMIStdOspfContextId,
                                     UINT4 u4FsMIOspfIfAuthIpAddress,
                                     INT4 i4FsMIOspfIfAuthAddressLessIf,
                                     INT4 i4FsMIOspfIfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsMIOspfIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
/*
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;

    OspfConvertTime (pSetValFsMIOspfIfAuthKeyStopGenerate->pu1_OctetList , &tm); 
    i4AuthKeyStopGenerate =  GrGetSecondsSinceBase (tm);
*/

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFutOspfIfAuthKeyStopGenerate (u4FsMIOspfIfAuthIpAddress,
                                            i4FsMIOspfIfAuthAddressLessIf,
                                            i4FsMIOspfIfAuthKeyId,
                                            pSetValFsMIOspfIfAuthKeyStopGenerate);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                setValFsMIOspfIfMD5AuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfMD5AuthKeyStopAccept (INT4 i4FsMIOspfIfMD5AuthContextId,
                                      UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                      INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                      INT4 i4FsMIOspfIfMD5AuthKeyId,
                                      INT4
                                      i4SetValFsMIOspfIfMD5AuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfIfMD5AuthKeyStopAccept (u4FsMIOspfIfMD5AuthIpAddress,
                                             i4FsMIOspfIfMD5AuthAddressLessIf,
                                             i4FsMIOspfIfMD5AuthKeyId,
                                             i4SetValFsMIOspfIfMD5AuthKeyStopAccept);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                setValFsMIOspfIfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfAuthKeyStopAccept (INT4 i4FsMIStdOspfContextId,
                                   UINT4 u4FsMIOspfIfAuthIpAddress,
                                   INT4 i4FsMIOspfIfAuthAddressLessIf,
                                   INT4 i4FsMIOspfIfAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMIOspfIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFutOspfIfAuthKeyStopAccept (u4FsMIOspfIfAuthIpAddress,
                                          i4FsMIOspfIfAuthAddressLessIf,
                                          i4FsMIOspfIfAuthKeyId,
                                          pSetValFsMIOspfIfAuthKeyStopAccept);

    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfMD5AuthKeyStatus
 Input       :  The Indices
                FsMIOspfIfMD5AuthContextId
                FsMIOspfIfMD5AuthIpAddress
                FsMIOspfIfMD5AuthAddressLessIf
                FsMIOspfIfMD5AuthKeyId

                The Object 
                setValFsMIOspfIfMD5AuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfMD5AuthKeyStatus (INT4 i4FsMIOspfIfMD5AuthContextId,
                                  UINT4 u4FsMIOspfIfMD5AuthIpAddress,
                                  INT4 i4FsMIOspfIfMD5AuthAddressLessIf,
                                  INT4 i4FsMIOspfIfMD5AuthKeyId,
                                  INT4 i4SetValFsMIOspfIfMD5AuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfIfMD5AuthKeyStatus (u4FsMIOspfIfMD5AuthIpAddress,
                                                i4FsMIOspfIfMD5AuthAddressLessIf,
                                                i4FsMIOspfIfMD5AuthKeyId,
                                                i4SetValFsMIOspfIfMD5AuthKeyStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfIfAuthIpAddress
                FsMIOspfIfAuthAddressLessIf
                FsMIOspfIfAuthKeyId

                The Object 
                setValFsMIOspfIfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfIfAuthKeyStatus (INT4 i4FsMIStdOspfContextId,
                               UINT4 u4FsMIOspfIfAuthIpAddress,
                               INT4 i4FsMIOspfIfAuthAddressLessIf,
                               INT4 i4FsMIOspfIfAuthKeyId,
                               INT4 i4SetValFsMIOspfIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfIfAuthKeyStatus (u4FsMIOspfIfAuthIpAddress,
                                             i4FsMIOspfIfAuthAddressLessIf,
                                             i4FsMIOspfIfAuthKeyId,
                                             i4SetValFsMIOspfIfAuthKeyStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfMD5AuthKey
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                setValFsMIOspfVirtIfMD5AuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfMD5AuthKey (INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMIOspfVirtIfMD5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfVirtIfMD5AuthKey (u4FsMIOspfVirtIfMD5AuthAreaId,
                                              u4FsMIOspfVirtIfMD5AuthNeighbor,
                                              i4FsMIOspfVirtIfMD5AuthKeyId,
                                              pSetValFsMIOspfVirtIfMD5AuthKey);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfAuthKey
 Input       :  The Indices
                FsMIOspfVirtIfAuthContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                setValFsMIOspfVirtIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfAuthKey (INT4 i4FsMIOspfVirtIfAuthContextId,
                             UINT4 u4FsMIOspfVirtIfAuthAreaId,
                             UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                             INT4 i4FsMIOspfVirtIfAuthKeyId,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsMIOspfVirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfVirtIfAuthKey (u4FsMIOspfVirtIfAuthAreaId,
                                           u4FsMIOspfVirtIfAuthNeighbor,
                                           i4FsMIOspfVirtIfAuthKeyId,
                                           pSetValFsMIOspfVirtIfAuthKey);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfMD5AuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                setValFsMIOspfVirtIfMD5AuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStartAccept (INT4
                                           i4FsMIOspfVirtIfMD5AuthContextId,
                                           UINT4
                                           u4FsMIOspfVirtIfMD5AuthAreaId,
                                           UINT4
                                           u4FsMIOspfVirtIfMD5AuthNeighbor,
                                           INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                           INT4
                                           i4SetValFsMIOspfVirtIfMD5AuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStartAccept
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId,
         i4SetValFsMIOspfVirtIfMD5AuthKeyStartAccept);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIOspfVirtIfAuthContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                setValFsMIOspfVirtIfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfAuthKeyStartAccept (INT4
                                        i4FsMIOspfVirtIfAuthContextId,
                                        UINT4
                                        u4FsMIOspfVirtIfAuthAreaId,
                                        UINT4
                                        u4FsMIOspfVirtIfAuthNeighbor,
                                        INT4 i4FsMIOspfVirtIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFsMIOspfVirtIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfVirtIfAuthKeyStartAccept
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId, pSetValFsMIOspfVirtIfAuthKeyStartAccept);

    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfMD5AuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                setValFsMIOspfVirtIfMD5AuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStartGenerate (INT4
                                             i4FsMIOspfVirtIfMD5AuthContextId,
                                             UINT4
                                             u4FsMIOspfVirtIfMD5AuthAreaId,
                                             UINT4
                                             u4FsMIOspfVirtIfMD5AuthNeighbor,
                                             INT4
                                             i4FsMIOspfVirtIfMD5AuthKeyId,
                                             INT4
                                             i4SetValFsMIOspfVirtIfMD5AuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStartGenerate
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId,
         i4SetValFsMIOspfVirtIfMD5AuthKeyStartGenerate);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIOspfVirtIfAuthContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                setValFsMIOspfVirtIfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfAuthKeyStartGenerate (INT4
                                          i4FsMIOspfVirtIfAuthContextId,
                                          UINT4
                                          u4FsMIOspfVirtIfAuthAreaId,
                                          UINT4
                                          u4FsMIOspfVirtIfAuthNeighbor,
                                          INT4
                                          i4FsMIOspfVirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsMIOspfVirtIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfAuthKeyStartGenerate
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId, pSetValFsMIOspfVirtIfAuthKeyStartGenerate);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfMD5AuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                setValFsMIOspfVirtIfMD5AuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStopGenerate (INT4
                                            i4FsMIOspfVirtIfMD5AuthContextId,
                                            UINT4
                                            u4FsMIOspfVirtIfMD5AuthAreaId,
                                            UINT4
                                            u4FsMIOspfVirtIfMD5AuthNeighbor,
                                            INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                            INT4
                                            i4SetValFsMIOspfVirtIfMD5AuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStopGenerate
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId,
         i4SetValFsMIOspfVirtIfMD5AuthKeyStopGenerate);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIOspfVirtIfAuthContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                setValFsMIOspfVirtIfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfAuthKeyStopGenerate (INT4
                                         i4FsMIOspfVirtIfAuthContextId,
                                         UINT4
                                         u4FsMIOspfVirtIfAuthAreaId,
                                         UINT4
                                         u4FsMIOspfVirtIfAuthNeighbor,
                                         INT4 i4FsMIOspfVirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValFsMIOspfVirtIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfAuthKeyStopGenerate
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId, pSetValFsMIOspfVirtIfAuthKeyStopGenerate);

    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfMD5AuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                setValFsMIOspfVirtIfMD5AuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStopAccept (INT4
                                          i4FsMIOspfVirtIfMD5AuthContextId,
                                          UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                          UINT4
                                          u4FsMIOspfVirtIfMD5AuthNeighbor,
                                          INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                          INT4
                                          i4SetValFsMIOspfVirtIfMD5AuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStopAccept
        (u4FsMIOspfVirtIfMD5AuthAreaId, u4FsMIOspfVirtIfMD5AuthNeighbor,
         i4FsMIOspfVirtIfMD5AuthKeyId,
         i4SetValFsMIOspfVirtIfMD5AuthKeyStopAccept);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIOspfVirtIfAuthContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                setValFsMIOspfVirtIfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfAuthKeyStopAccept (INT4
                                       i4FsMIOspfVirtIfAuthContextId,
                                       UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                       UINT4
                                       u4FsMIOspfVirtIfAuthNeighbor,
                                       INT4 i4FsMIOspfVirtIfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsMIOspfVirtIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFutOspfVirtIfAuthKeyStopAccept
        (u4FsMIOspfVirtIfAuthAreaId, u4FsMIOspfVirtIfAuthNeighbor,
         i4FsMIOspfVirtIfAuthKeyId, pSetValFsMIOspfVirtIfAuthKeyStopAccept);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfMD5AuthKeyStatus
 Input       :  The Indices
                FsMIOspfVirtIfMD5AuthContextId
                FsMIOspfVirtIfMD5AuthAreaId
                FsMIOspfVirtIfMD5AuthNeighbor
                FsMIOspfVirtIfMD5AuthKeyId

                The Object 
                setValFsMIOspfVirtIfMD5AuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfMD5AuthKeyStatus (INT4 i4FsMIOspfVirtIfMD5AuthContextId,
                                      UINT4 u4FsMIOspfVirtIfMD5AuthAreaId,
                                      UINT4 u4FsMIOspfVirtIfMD5AuthNeighbor,
                                      INT4 i4FsMIOspfVirtIfMD5AuthKeyId,
                                      INT4
                                      i4SetValFsMIOspfVirtIfMD5AuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfVirtIfMD5AuthContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfMD5AuthKeyStatus (u4FsMIOspfVirtIfMD5AuthAreaId,
                                             u4FsMIOspfVirtIfMD5AuthNeighbor,
                                             i4FsMIOspfVirtIfMD5AuthKeyId,
                                             i4SetValFsMIOspfVirtIfMD5AuthKeyStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfVirtIfAuthKeyStatus
 Input       :  The Indices
                FsMIOspfVirtIfAuthContextId
                FsMIOspfVirtIfAuthAreaId
                FsMIOspfVirtIfAuthNeighbor
                FsMIOspfVirtIfAuthKeyId

                The Object 
                setValFsMIOspfVirtIfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfVirtIfAuthKeyStatus (INT4 i4FsMIOspfVirtIfAuthContextId,
                                   UINT4 u4FsMIOspfVirtIfAuthAreaId,
                                   UINT4 u4FsMIOspfVirtIfAuthNeighbor,
                                   INT4 i4FsMIOspfVirtIfAuthKeyId,
                                   INT4 i4SetValFsMIOspfVirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfVirtIfAuthContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfVirtIfAuthKeyStatus (u4FsMIOspfVirtIfAuthAreaId,
                                          u4FsMIOspfVirtIfAuthNeighbor,
                                          i4FsMIOspfVirtIfAuthKeyId,
                                          i4SetValFsMIOspfVirtIfAuthKeyStatus);

    UtilOspfResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfSecIfStatus
 Input       :  The Indices
                FsMIOspfSecIfContextId
                FsMIOspfPrimIpAddr
                FsMIOspfPrimAddresslessIf
                FsMIOspfSecIpAddr
                FsMIOspfSecIpAddrMask

                The Object 
                setValFsMIOspfSecIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfSecIfStatus (INT4 i4FsMIOspfSecIfContextId,
                           UINT4 u4FsMIOspfPrimIpAddr,
                           INT4 i4FsMIOspfPrimAddresslessIf,
                           UINT4 u4FsMIOspfSecIpAddr,
                           UINT4 u4FsMIOspfSecIpAddrMask,
                           INT4 i4SetValFsMIOspfSecIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfSecIfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfSecIfStatus (u4FsMIOspfPrimIpAddr,
                                         i4FsMIOspfPrimAddresslessIf,
                                         u4FsMIOspfSecIpAddr,
                                         u4FsMIOspfSecIpAddrMask,
                                         i4SetValFsMIOspfSecIfStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfAreaAggregateExternalTag
 Input       :  The Indices
                FsMIOspfAreaAggregateContextId
                FsMIOspfAreaAggregateAreaID
                FsMIOspfAreaAggregateLsdbType
                FsMIOspfAreaAggregateNet
                FsMIOspfAreaAggregateMask

                The Object 
                setValFsMIOspfAreaAggregateExternalTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAreaAggregateExternalTag (INT4 i4FsMIOspfAreaAggregateContextId,
                                        UINT4 u4FsMIOspfAreaAggregateAreaID,
                                        INT4 i4FsMIOspfAreaAggregateLsdbType,
                                        UINT4 u4FsMIOspfAreaAggregateNet,
                                        UINT4 u4FsMIOspfAreaAggregateMask,
                                        INT4
                                        i4SetValFsMIOspfAreaAggregateExternalTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext ((UINT4) i4FsMIOspfAreaAggregateContextId) ==
        SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfAreaAggregateExternalTag (u4FsMIOspfAreaAggregateAreaID,
                                               i4FsMIOspfAreaAggregateLsdbType,
                                               u4FsMIOspfAreaAggregateNet,
                                               u4FsMIOspfAreaAggregateMask,
                                               i4SetValFsMIOspfAreaAggregateExternalTag);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfAsExternalAggregationEffect
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                setValFsMIOspfAsExternalAggregationEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAsExternalAggregationEffect (INT4
                                           i4FsMIOspfAsExternalAggregationContextId,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationNet,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationMask,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationAreaId,
                                           INT4
                                           i4SetValFsMIOspfAsExternalAggregationEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId)
        == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfAsExternalAggregationEffect
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId,
         i4SetValFsMIOspfAsExternalAggregationEffect);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfAsExternalAggregationTranslation
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                setValFsMIOspfAsExternalAggregationTranslation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAsExternalAggregationTranslation (INT4
                                                i4FsMIOspfAsExternalAggregationContextId,
                                                UINT4
                                                u4FsMIOspfAsExternalAggregationNet,
                                                UINT4
                                                u4FsMIOspfAsExternalAggregationMask,
                                                UINT4
                                                u4FsMIOspfAsExternalAggregationAreaId,
                                                INT4
                                                i4SetValFsMIOspfAsExternalAggregationTranslation)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId)
        == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfAsExternalAggregationTranslation
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId,
         i4SetValFsMIOspfAsExternalAggregationTranslation);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfAsExternalAggregationStatus
 Input       :  The Indices
                FsMIOspfAsExternalAggregationContextId
                FsMIOspfAsExternalAggregationNet
                FsMIOspfAsExternalAggregationMask
                FsMIOspfAsExternalAggregationAreaId

                The Object 
                setValFsMIOspfAsExternalAggregationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAsExternalAggregationStatus (INT4
                                           i4FsMIOspfAsExternalAggregationContextId,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationNet,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationMask,
                                           UINT4
                                           u4FsMIOspfAsExternalAggregationAreaId,
                                           INT4
                                           i4SetValFsMIOspfAsExternalAggregationStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfAsExternalAggregationContextId)
        == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfAsExternalAggregationStatus
        (u4FsMIOspfAsExternalAggregationNet,
         u4FsMIOspfAsExternalAggregationMask,
         u4FsMIOspfAsExternalAggregationAreaId,
         i4SetValFsMIOspfAsExternalAggregationStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfSetTrap
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIStdOspfSetTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfSetTrap (INT4 i4FsMIStdOspfContextId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsMIStdOspfSetTrap)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfSetTrap (pSetValFsMIStdOspfSetTrap);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfOpaqueOption
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                setValFsMIOspfOpaqueOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfOpaqueOption (INT4 i4FsMIOspfOpaqueContextId,
                            INT4 i4SetValFsMIOspfOpaqueOption)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfOpaqueOption (i4SetValFsMIOspfOpaqueOption);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfAreaIDValid
 Input       :  The Indices
                FsMIOspfOpaqueContextId

                The Object 
                setValFsMIOspfAreaIDValid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfAreaIDValid (INT4 i4FsMIOspfOpaqueContextId,
                           INT4 i4SetValFsMIOspfAreaIDValid)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfOpaqueContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfAreaIDValid (i4SetValFsMIOspfAreaIDValid);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDStatus
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                setValFsMIOspfRRDStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDStatus (INT4 i4FsMIOspfRRDRouteContextId,
                         INT4 i4SetValFsMIOspfRRDStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDStatus (i4SetValFsMIOspfRRDStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDSrcProtoMaskEnable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                setValFsMIOspfRRDSrcProtoMaskEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDSrcProtoMaskEnable (INT4 i4FsMIOspfRRDRouteContextId,
                                     INT4 i4SetValFsMIOspfRRDSrcProtoMaskEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfRRDSrcProtoMaskEnable
        (i4SetValFsMIOspfRRDSrcProtoMaskEnable);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDSrcProtoMaskDisable
 Input       :  The Indices
                FsMIOspfRRDRouteContextId

                The Object 
                setValFsMIOspfRRDSrcProtoMaskDisable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDSrcProtoMaskDisable (INT4 i4FsMIOspfRRDRouteContextId,
                                      INT4
                                      i4SetValFsMIOspfRRDSrcProtoMaskDisable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfRRDSrcProtoMaskDisable
        (i4SetValFsMIOspfRRDSrcProtoMaskDisable);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDRouteMapEnable
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfRRDRouteMapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDRouteMapEnable (INT4 i4FsMIStdOspfContextId,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pSetValFsMIOspfRRDRouteMapEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDRouteMapEnable
        (pSetValFsMIOspfRRDRouteMapEnable);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDRouteMetric
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                setValFsMIOspfRRDRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDRouteMetric (INT4 i4FsMIOspfRRDRouteConfigContextId,
                              UINT4 u4FsMIOspfRRDRouteDest,
                              UINT4 u4FsMIOspfRRDRouteMask,
                              INT4 i4SetValFsMIOspfRRDRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDRouteMetric (u4FsMIOspfRRDRouteDest,
                                            u4FsMIOspfRRDRouteMask,
                                            i4SetValFsMIOspfRRDRouteMetric);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDRouteMetricType
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                setValFsMIOspfRRDRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDRouteMetricType (INT4 i4FsMIOspfRRDRouteConfigContextId,
                                  UINT4 u4FsMIOspfRRDRouteDest,
                                  UINT4 u4FsMIOspfRRDRouteMask,
                                  INT4 i4SetValFsMIOspfRRDRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDRouteMetricType (u4FsMIOspfRRDRouteDest,
                                                u4FsMIOspfRRDRouteMask,
                                                i4SetValFsMIOspfRRDRouteMetricType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDRouteTagType
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                setValFsMIOspfRRDRouteTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDRouteTagType (INT4 i4FsMIOspfRRDRouteConfigContextId,
                               UINT4 u4FsMIOspfRRDRouteDest,
                               UINT4 u4FsMIOspfRRDRouteMask,
                               INT4 i4SetValFsMIOspfRRDRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDRouteTagType (u4FsMIOspfRRDRouteDest,
                                             u4FsMIOspfRRDRouteMask,
                                             i4SetValFsMIOspfRRDRouteTagType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDRouteTag
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                setValFsMIOspfRRDRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDRouteTag (INT4 i4FsMIOspfRRDRouteConfigContextId,
                           UINT4 u4FsMIOspfRRDRouteDest,
                           UINT4 u4FsMIOspfRRDRouteMask,
                           UINT4 u4SetValFsMIOspfRRDRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDRouteTag (u4FsMIOspfRRDRouteDest,
                                         u4FsMIOspfRRDRouteMask,
                                         u4SetValFsMIOspfRRDRouteTag);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDRouteStatus
 Input       :  The Indices
                FsMIOspfRRDRouteConfigContextId
                FsMIOspfRRDRouteDest
                FsMIOspfRRDRouteMask

                The Object 
                setValFsMIOspfRRDRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDRouteStatus (INT4 i4FsMIOspfRRDRouteConfigContextId,
                              UINT4 u4FsMIOspfRRDRouteDest,
                              UINT4 u4FsMIOspfRRDRouteMask,
                              INT4 i4SetValFsMIOspfRRDRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfRRDRouteConfigContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfRRDRouteStatus (u4FsMIOspfRRDRouteDest,
                                            u4FsMIOspfRRDRouteMask,
                                            i4SetValFsMIOspfRRDRouteStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteMetric
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteMetric (INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              INT4 i4SetValFsMIOspfExtRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteMetric (u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            i4SetValFsMIOspfExtRouteMetric);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteMetricType
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteMetricType (INT4 i4FsMIOspfExtRouteContextId,
                                  UINT4 u4FsMIOspfExtRouteDest,
                                  UINT4 u4FsMIOspfExtRouteMask,
                                  INT4 i4FsMIOspfExtRouteTOS,
                                  INT4 i4SetValFsMIOspfExtRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteMetricType (u4FsMIOspfExtRouteDest,
                                                u4FsMIOspfExtRouteMask,
                                                i4FsMIOspfExtRouteTOS,
                                                i4SetValFsMIOspfExtRouteMetricType);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteTag
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteTag (INT4 i4FsMIOspfExtRouteContextId,
                           UINT4 u4FsMIOspfExtRouteDest,
                           UINT4 u4FsMIOspfExtRouteMask,
                           INT4 i4FsMIOspfExtRouteTOS,
                           INT4 i4SetValFsMIOspfExtRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteTag (u4FsMIOspfExtRouteDest,
                                         u4FsMIOspfExtRouteMask,
                                         i4FsMIOspfExtRouteTOS,
                                         i4SetValFsMIOspfExtRouteTag);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteFwdAdr
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteFwdAdr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteFwdAdr (INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              UINT4 u4SetValFsMIOspfExtRouteFwdAdr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteFwdAdr (u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            u4SetValFsMIOspfExtRouteFwdAdr);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteIfIndex
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteIfIndex (INT4 i4FsMIOspfExtRouteContextId,
                               UINT4 u4FsMIOspfExtRouteDest,
                               UINT4 u4FsMIOspfExtRouteMask,
                               INT4 i4FsMIOspfExtRouteTOS,
                               INT4 i4SetValFsMIOspfExtRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteIfIndex (u4FsMIOspfExtRouteDest,
                                             u4FsMIOspfExtRouteMask,
                                             i4FsMIOspfExtRouteTOS,
                                             i4SetValFsMIOspfExtRouteIfIndex);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteNextHop
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteNextHop
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteNextHop (INT4 i4FsMIOspfExtRouteContextId,
                               UINT4 u4FsMIOspfExtRouteDest,
                               UINT4 u4FsMIOspfExtRouteMask,
                               INT4 i4FsMIOspfExtRouteTOS,
                               UINT4 u4SetValFsMIOspfExtRouteNextHop)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteNextHop (u4FsMIOspfExtRouteDest,
                                             u4FsMIOspfExtRouteMask,
                                             i4FsMIOspfExtRouteTOS,
                                             u4SetValFsMIOspfExtRouteNextHop);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfExtRouteStatus
 Input       :  The Indices
                FsMIOspfExtRouteContextId
                FsMIOspfExtRouteDest
                FsMIOspfExtRouteMask
                FsMIOspfExtRouteTOS

                The Object 
                setValFsMIOspfExtRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfExtRouteStatus (INT4 i4FsMIOspfExtRouteContextId,
                              UINT4 u4FsMIOspfExtRouteDest,
                              UINT4 u4FsMIOspfExtRouteMask,
                              INT4 i4FsMIOspfExtRouteTOS,
                              INT4 i4SetValFsMIOspfExtRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIOspfExtRouteContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfExtRouteStatus (u4FsMIOspfExtRouteDest,
                                            u4FsMIOspfExtRouteMask,
                                            i4FsMIOspfExtRouteTOS,
                                            i4SetValFsMIOspfExtRouteStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfGrTable. */
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfGrShutdown
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object
                setValFsMIOspfGrShutdown
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfGrShutdown (INT4 i4FsMIStdOspfContextId,
                          INT4 i4SetValFsMIOspfGrShutdown)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfGrShutdown (i4SetValFsMIOspfGrShutdown);
    UtilOspfResetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfDistInOutRouteMapTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfDistInOutRouteMapValue
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType

                The Object 
                setValFsMIOspfDistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfDistInOutRouteMapValue (INT4 i4FsMIStdOspfContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIOspfDistInOutRouteMapName,
                                      INT4 i4FsMIOspfDistInOutRouteMapType,
                                      INT4
                                      i4SetValFsMIOspfDistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfDistInOutRouteMapValue
        (pFsMIOspfDistInOutRouteMapName,
         i4FsMIOspfDistInOutRouteMapType,
         i4SetValFsMIOspfDistInOutRouteMapValue);
    UtilOspfResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfDistInOutRouteMapName
                FsMIOspfDistInOutRouteMapType

                The Object 
                setValFsMIOspfDistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfDistInOutRouteMapRowStatus (INT4 i4FsMIStdOspfContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIOspfDistInOutRouteMapName,
                                          INT4 i4FsMIOspfDistInOutRouteMapType,
                                          INT4
                                          i4SetValFsMIOspfDistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfDistInOutRouteMapRowStatus
        (pFsMIOspfDistInOutRouteMapName,
         i4FsMIOspfDistInOutRouteMapType,
         i4SetValFsMIOspfDistInOutRouteMapRowStatus);
    UtilOspfResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfPreferenceValue
 Input       :  The Indices
                FsMIStdOspfContextId

                The Object 
                setValFsMIOspfPreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfPreferenceValue (INT4 i4FsMIStdOspfContextId,
                               INT4 i4SetValFsMIOspfPreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilOspfSetContext (i4FsMIStdOspfContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfPreferenceValue (i4SetValFsMIOspfPreferenceValue);
    UtilOspfResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDMetricValue
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId

                The Object 
                setValFsMIOspfRRDMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDMetricValue (INT4 i4FsMIStdOspfContextId,
                              INT4 i4FsMIOspfRRDProtocolId,
                              INT4 i4SetValFsMIOspfRRDMetricValue)
{
    tOspfCxt           *pOspfCxt = NULL;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId);
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDMetricValue;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDMetricValue) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
#endif
    pOspfCxt->au4MetricValue[i4FsMIOspfRRDProtocolId - 1] =
        i4SetValFsMIOspfRRDMetricValue;
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", pOspfCxt->u4OspfCxtId,
                      i4FsMIOspfRRDProtocolId, i4SetValFsMIOspfRRDMetricValue));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfRRDMetricType
 Input       :  The Indices
                FsMIStdOspfContextId
                FsMIOspfRRDProtocolId

                The Object 
                setValFsMIOspfRRDMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfRRDMetricType (INT4 i4FsMIStdOspfContextId,
                             INT4 i4FsMIOspfRRDProtocolId,
                             INT4 i4SetValFsMIOspfRRDMetricType)
{
    tOspfCxt           *pOspfCxt = NULL;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    pOspfCxt = UtilOspfGetCxt (i4FsMIStdOspfContextId);
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfRRDMetricType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfRRDMetricType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = OspfLock;
    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
#endif
    pOspfCxt->au1MetricType[i4FsMIOspfRRDProtocolId - 1] =
        (UINT1) i4SetValFsMIOspfRRDMetricType;
#ifdef SNMP_2_WANTED
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", pOspfCxt->u4OspfCxtId,
                      i4FsMIOspfRRDProtocolId, i4SetValFsMIOspfRRDMetricType));
#endif
    return i1RetVal;
}
