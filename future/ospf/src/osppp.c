/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osppp.c,v 1.34 2017/05/12 13:35:39 siva Exp $
 *
 * Description:This file contains procedures used for transmission
 *             and reception of protocol packets to/from IP.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */
PRIVATE UINT1       gau1AuthPkt[MAX_OSPF_PKT_LEN];
PRIVATE INT4        AuthenticateRcvdPkt
PROTO ((UINT2 u2AuthType,
        tInterface * pInterface,
        tCRU_BUF_CHAIN_HEADER * pPkt, tIPADDR * pSrcIpAddr, UINT2 u2Len));

/*****************************************************************************/
/*                                                                           */
/* Function     : PppSendPkt                                                 */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 8.1                           */
/*                This procedure calls the ospf_send_pkt routine with        */
/*                appropriate params necessary for constructing the IP       */
/*                header.                                                    */
/*                                                                           */
/* Input        : pPkt                       : the pkt to be sent            */
/*                u2PktLen                  : the length of the pkt          */
/*                pInterface                 : the outgoing interface        */
/*                pDestIpAddr              : the destination address         */
/*                u1ReleaseFlag             : whether this buffer can be     */
/*                                              released                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
PppSendPkt (tCRU_BUF_CHAIN_HEADER * pPkt,
            UINT2 u2PktLen,
            tInterface * pInterface, tIPADDR * pDestIpAddr, UINT1 u1ReleaseFlag)
{

    UINT1               u1Type;
    tIPADDR             srcIpAddr;
    UINT1               u1Ttl;
    tCRU_BUF_CHAIN_HEADER *pPktTobSent;
    tIPADDR             destAddr;

    UINT4               u4Port;
    UINT1               u1OperStatus = 0;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT4               u4UnnumAssocIPAddr = 0;
    UINT4               u4IfIndex = 0;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can send the pkt */
        if (u1ReleaseFlag != NO_RELEASE)
        {
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        }
        return;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: PppSendPkt\n");
    if (pInterface->u1NetworkType < MAX_IF_TYPE)
    {
        OSPF_TRC6 (OSPF_PPP_TRC | CONTROL_PLANE_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Packet to be txed on interface If_Index: %d"
                   " Addr: %08x  Addrless_if: %d       Network Type: %s"
                   " to destination: %08x  Len: %d\n",
                   pInterface->u4IfIndex,
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u4AddrlessIf,
                   au1DbgIfType[pInterface->u1NetworkType],
                   OSPF_CRU_BMC_DWFROMPDU (*pDestIpAddr), u2PktLen);
    }

#ifdef RAWSOCK_WANTED
    /* This option is required to specify the default       */
    /* interface to be used for sending multicast packets.  */
    /* Since there is single socket for all interfaces,     */
    /* everytime before sending the packet through any      */
    /* particular interface, interface is enabled with this */
    /* option.                                              */

    if (OspfSetIfSockOptions (pInterface) == OSPF_FAILURE)
    {
        OSPF_TRC (OSPF_PPP_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "osif.c: IfCreate: SetSockOpt Failed\n");
    }
#endif

    if ((pInterface->operStatus == OSPF_DISABLED) ||
        (pInterface->admnStatus == OSPF_DISABLED) ||
        (pInterface->bPassive == OSPF_TRUE))
    {

        /* If the oper status or the admin status is disabled then the
           buffer is released */
        if (u1ReleaseFlag != NO_RELEASE)
        {
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        }
        return;
    }

    /* Process release flag. */

    if (u1ReleaseFlag == NO_RELEASE)
    {
        if ((pPktTobSent = CRU_BUF_Duplicate_BufChain (pPkt)) == NULL)
        {
            OSPF_TRC (OS_RESOURCE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "CRU_BUF_Duplicate_BufChain FAILED in \
                      PppSendPkt.\n");
            return;
        }
    }
    else
    {
        pPktTobSent = pPkt;
    }

    OSPF_CRU_BMC_GET_1_BYTE (pPktTobSent, TYPE_OFFSET_IN_HEADER, u1Type);

    if (UtilIpAddrComp (gNullIpAddr, pInterface->ifIpAddr) == OSPF_EQUAL)
    {

        /*Get cfa index from l3 port */
        u4IfIndex = OspfGetIfIndexFromPort (pInterface->u4IfIndex);

        CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
        CfaGetIfOperStatus (u4UnnumAssocIPIf, &u1OperStatus);
        if ((0 != u4UnnumAssocIPIf) && (u1OperStatus == CFA_IF_UP))
        {
            /* If interface is  unnuumbered then assign the Assosiated ip interface */
            CfaIpIfGetSrcAddressOnInterface (u4UnnumAssocIPIf, (UINT4) 0,
                                             &u4UnnumAssocIPAddr);

            u4UnnumAssocIPAddr = OSIX_HTONL (u4UnnumAssocIPAddr);
            /* convert u4UnnumAssocIPAddr to network byte order and then copy */
            IP_ADDR_COPY (srcIpAddr, (UINT1 *) &u4UnnumAssocIPAddr);
        }
        else
        {
            IP_ADDR_COPY (srcIpAddr, pInterface->pArea->pOspfCxt->rtrId);
        }
    }
    else
    {
        IP_ADDR_COPY (srcIpAddr, pInterface->ifIpAddr);
    }

    if (pInterface->u1NetworkType == IF_PTOP)
    {
        IP_ADDR_COPY (destAddr, gAllSpfRtrs);
    }
    else
    {
        IP_ADDR_COPY (destAddr, *pDestIpAddr);
    }

    u1Ttl = ((pInterface->u1NetworkType == IF_VIRTUAL) ?
             VIRTUAL_LINK_TTL : TTL_ONE);

    u4Port = pInterface->u4IfIndex;

    if (u1Type < MAX_PKT_TYPE)
    {
        OSPF_TRC1 (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "Txmitting %s\n", au1DbgPktType[u1Type]);
    }

    OSPF_OUT_PKT_DUMP (pInterface->pArea->pOspfCxt->u4OspfCxtId, pPktTobSent);    /* Dump the Outgoing Packet */

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: IpifSendPktInCxt\n");
    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC,
              pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Packet to be enqued to IP\n");
    /* Hello message are relinquished if we are contineously sending the
     *  lsupdate or lsack packets  based on u4AckOrUpdatePktSentCtr */

    if ((u1Type == LSA_ACK_PKT) || (u1Type == LS_UPDATE_PKT))
    {
        gOsRtr.u4AckOrUpdatePktSentCtr++;
        if (gOsRtr.u4AckOrUpdatePktSentCtr >= MAX_OSPF_MSG_PROCESSED)
        {
            CpuRelinquishHello ();
            gOsRtr.u4AckOrUpdatePktSentCtr = 0;
        }
    }

    if (IpifSendPktInCxt (pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          pPktTobSent, u4Port, &srcIpAddr,
                          &destAddr, u2PktLen, u1Ttl) == SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "EXIT: IpifSendPktInCxt\n");

        switch (u1Type)
        {
            case HELLO_PKT:
                COUNTER_OP (pInterface->u4HelloTxedCount, 1);
                break;

            case DD_PKT:
                COUNTER_OP (pInterface->u4DdpTxedCount, 1);
                break;

            case LSA_REQ_PKT:
                COUNTER_OP (pInterface->u4LsaReqTxedCount, 1);
                break;

            case LSA_ACK_PKT:
                COUNTER_OP (pInterface->u4LsaAckTxedCount, 1);
                break;

            case LS_UPDATE_PKT:
                COUNTER_OP (pInterface->u4LsaUpdateTxedCount, 1);
                break;

            default:
                break;
        }
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "IP_SEND_PARMS_ALLOC FAILED\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetAssocAuthkeyinfo                                        */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure authentication key information for the      */
/*                 given interface and the authentication key Id             */
/*                header.                                                    */
/*                                                                           */
/* Input        : pInterface             : pointer to the interface          */
/*              : u1Authkeyid            : Key Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the key authentication key info on success      */
/*                NULL  otherwise                                            */
/*****************************************************************************/

PRIVATE tMd5AuthkeyInfo *
GetAssocAuthkeyinfo (tInterface * pInterface, UINT1 u1Authkeyid)
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tTMO_SLL_NODE      *pLstNode;

    TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst), pLstNode, tTMO_SLL_NODE *)
    {
        pAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
        if (pAuthkeyInfo->u1AuthkeyId == u1Authkeyid)
        {
            return (pAuthkeyInfo);
        }
    }
    return (NULL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : AuthenticateRcvdPkt                                        */
/*                                                                           */
/* Description  :                                                            */
/*                This procedure authenticates the received packet           */
/*                                                                           */
/*                                                                           */
/* Input        : u2AuthType         : Authentication Type                   */
/*              : pInterface          : Pointer to the interface             */
/*              : pPkt                                                       */
/*              : pSrcIpAddr                                                 */
/*              : u2Len                                                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the key authentication key info on success      */
/*                NULL  otherwise                                            */
/*****************************************************************************/

INT4
AuthenticateRcvdPkt (UINT2 u2AuthType,
                     tInterface * pInterface,
                     tCRU_BUF_CHAIN_HEADER * pPkt,
                     tIPADDR * pSrcIpAddr, UINT2 u2Len)
{
    tAUTHENTICATION     authKey;
    UINT1               u1RcvdAuthkeyid;
    UINT4               u4RcvdCryptSeqNum;
    tRouterId           headerRtrId;
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    tNeighbor          *pNbr;
    UINT1               pktDigest[OSPF_SHA2_512_DIGEST_LEN];
    UINT1               calDigest[OSPF_SHA2_512_DIGEST_LEN];
    UINT1              *pTempPkt = NULL;
    UINT2               u2OspfLen;
    UINT4               u4Ospf1sTimer = 0;
    tUtlTm              tm;

    /* Get the system time and make it as Ospf 1 s timer */
    UtlGetTime (&tm);
    u4Ospf1sTimer = GrGetSecondsSinceBase (tm);

    OS_MEM_SET (gau1AuthPkt, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    pTempPkt = gau1AuthPkt;
    switch (u2AuthType)
    {
        case NO_AUTHENTICATION:
            OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pPkt, 0, AUTH_KEY_OFFSET_IN_HEADER,
                                             AUTH_KEY_SIZE);
            if (UtilCalculateChksum (pPkt, u2Len))
            {
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Check Sum Error\n");
                return (OSPF_CHKSUM_FAILURE);
            }
            else
            {
                return (SUCCESS);
            }

        case SIMPLE_PASSWORD:
            OSPF_CRU_BMC_GET_STRING (pPkt, authKey, AUTH_KEY_OFFSET_IN_HEADER,
                                     AUTH_KEY_SIZE);
            OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pPkt, 0, AUTH_KEY_OFFSET_IN_HEADER,
                                             AUTH_KEY_SIZE);
            if (UtilCalculateChksum (pPkt, u2Len))
            {
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Check Sum Error\n");
                return (OSPF_FAILURE);
            }
            else
            {
                if (OS_MEM_CMP (authKey, pInterface->authKey, AUTH_KEY_SIZE))
                {
                    return (OSPF_FAILURE);
                }
                return (SUCCESS);
            }

        case CRYPT_AUTHENTICATION:
            OSPF_CRU_BMC_GET_1_BYTE (pPkt, AUTH_KEYID_OFFSET_IN_HEADER,
                                     u1RcvdAuthkeyid);
            pAuthkeyInfo = (tMd5AuthkeyInfo *)
                GetAssocAuthkeyinfo (pInterface, u1RcvdAuthkeyid);
            if (pAuthkeyInfo == NULL)
            {
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Authentication Key ID not found\n");
                return (OSPF_FAILURE);
            }
            if (pAuthkeyInfo->u4KeyStartAccept > u4Ospf1sTimer)
            {
                /* If StartAccept time is greater than system time return failure */
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Key Start Accept > current time\n");
                return (OSPF_FAILURE);
            }
            if ((pAuthkeyInfo->u4KeyStopAccept <= u4Ospf1sTimer)
                && (pInterface->pLastAuthkey != pAuthkeyInfo))
            {
                /* If Stop Accept value is less than system time, Ospf should stop accepting 
                   packets with this key id */
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Key Stop Accept > current time\n");
                return (OSPF_FAILURE);
            }
            OSPF_CRU_BMC_GET_STRING (pPkt, headerRtrId, RTR_ID_OFFSET_IN_HEADER,
                                     MAX_IP_ADDR_LEN);
            pNbr = HpAssociateWithNbr (pSrcIpAddr, &headerRtrId, pInterface);

            if (pNbr == NULL)
            {
                if (((pInterface->u1NetworkType == IF_NBMA) ||
                     (pInterface->u1NetworkType == IF_PTOMP)))
                {
                    return OSPF_FAILURE;
                }
                if ((pNbr = NbrCreate (pSrcIpAddr, &headerRtrId,
                                       pInterface->u4AddrlessIf, pInterface,
                                       DISCOVERED_NBR)) != NULL)
                {

                    /* pNbr->u1NbrRtrPriority = hello.u1RtrPriority; */
                }
            }

            /*   If pNbr == NULL then      
               This must be the first packet we are receiving 
               from the nbr,
               we have not even allocated nbr structure ..
               so allow it to proceed */
            OSPF_CRU_BMC_GET_4_BYTE (pPkt, CSN_OFFSET_IN_HEADER,
                                     u4RcvdCryptSeqNum);
            if (pNbr && pNbr->u4NbrCryptSeqNum > u4RcvdCryptSeqNum)
            {

                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          " nbr-s crypt seq num > rcvd_crypt_seq_num\n");
                return (OSPF_FAILURE);
            }

            /*Here Algorithm specific processing has to be done. */
            switch (pInterface->u4CryptoAuthType)
            {

                case OSPF_AUTH_MD5:
                    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2OspfLen);

                    if (u2Len > (u2OspfLen + MAX_AUTHKEY_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, pTempPkt,
                                                   (UINT4) (u2OspfLen +
                                                            MAX_AUTHKEY_LEN),
                                                   (UINT4) (u2Len - u2OspfLen -
                                                            MAX_AUTHKEY_LEN));

                        CRU_BUF_Delete_BufChainAtEnd (pPkt,
                                                      (UINT4) (u2Len -
                                                               u2OspfLen -
                                                               MAX_AUTHKEY_LEN));
                    }

                    OSPF_CRU_BMC_GET_STRING (pPkt, pktDigest, u2OspfLen,
                                             MAX_AUTHKEY_LEN);

                    CRU_BUF_Delete_BufChainAtEnd (pPkt, MAX_AUTHKEY_LEN);

                    CRU_BUF_Copy_OverBufChain_AtEnd (pPkt,
                                                     pAuthkeyInfo->authKey,
                                                     u2OspfLen,
                                                     MAX_AUTHKEY_LEN);

                    md5_get_keyed_digest (pPkt, pAuthkeyInfo->authKey,
                                          MAX_AUTHKEY_LEN, calDigest);

                    if (u2Len > (u2OspfLen + MAX_AUTHKEY_LEN))
                    {
                        CRU_BUF_Copy_OverBufChain_AtEnd (pPkt, pTempPkt,
                                                         (UINT4) (u2OspfLen +
                                                                  MAX_AUTHKEY_LEN),
                                                         (UINT4) (u2Len -
                                                                  u2OspfLen -
                                                                  MAX_AUTHKEY_LEN));
                    }

                    if (OS_MEM_CMP (pktDigest, calDigest, MAX_AUTHKEY_LEN))
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                                  " Md5 digest not matching\n");
                        return (OSPF_FAILURE);
                    }
                    break;
                case OSPF_AUTH_SHA1:
                    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2OspfLen);

                    if (u2Len > (u2OspfLen + OSPF_SHA1_DIGEST_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, pTempPkt,
                                                   (UINT4) (u2OspfLen +
                                                            OSPF_SHA1_DIGEST_LEN),
                                                   (UINT4) (u2Len - u2OspfLen -
                                                            OSPF_SHA1_DIGEST_LEN));

                        CRU_BUF_Delete_BufChainAtEnd (pPkt,
                                                      (UINT4) (u2Len -
                                                               u2OspfLen -
                                                               OSPF_SHA1_DIGEST_LEN));
                    }
                    OSPF_CRU_BMC_GET_STRING (pPkt, pktDigest, u2OspfLen,
                                             OSPF_SHA1_DIGEST_LEN);
                    OSPF_CRU_BMC_ASSIGN_STRING (pPkt, pAuthkeyInfo->authKey,
                                                (UINT4) (u2OspfLen),
                                                (UINT4) OSPF_SHA1_DIGEST_LEN);
                    UtilGetShaDigest (pPkt, u2OspfLen, pAuthkeyInfo,
                                      OSPF_AUTH_SHA1, calDigest);

                    if (OS_MEM_CMP (pktDigest, calDigest, OSPF_SHA1_DIGEST_LEN))
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                                  " SHA1 digest not matching\n");
                        return (OSPF_FAILURE);
                    }
                    break;
                case OSPF_AUTH_SHA2_224:
                    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2OspfLen);

                    if (u2Len > (u2OspfLen + OSPF_SHA2_224_DIGEST_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, pTempPkt,
                                                   (UINT4) (u2OspfLen +
                                                            OSPF_SHA2_224_DIGEST_LEN),
                                                   (UINT4) (u2Len - u2OspfLen -
                                                            OSPF_SHA2_224_DIGEST_LEN));

                        CRU_BUF_Delete_BufChainAtEnd (pPkt,
                                                      (UINT4) (u2Len -
                                                               u2OspfLen -
                                                               OSPF_SHA2_224_DIGEST_LEN));
                    }
                    OSPF_CRU_BMC_GET_STRING (pPkt, pktDigest, u2OspfLen,
                                             OSPF_SHA2_224_DIGEST_LEN);
                    OSPF_CRU_BMC_ASSIGN_STRING (pPkt, pAuthkeyInfo->authKey,
                                                (UINT4) (u2OspfLen),
                                                (UINT4)
                                                OSPF_SHA2_224_DIGEST_LEN);

                    UtilGetShaDigest (pPkt, u2OspfLen, pAuthkeyInfo,
                                      OSPF_AUTH_SHA2_224, calDigest);
                    if (OS_MEM_CMP
                        (pktDigest, calDigest, OSPF_SHA2_224_DIGEST_LEN))
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                                  " SHA1 digest not matching\n");
                        return (OSPF_FAILURE);
                    }
                    break;
                case OSPF_AUTH_SHA2_256:
                    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2OspfLen);

                    if (u2Len > (u2OspfLen + OSPF_SHA2_256_DIGEST_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, pTempPkt,
                                                   (UINT4) (u2OspfLen +
                                                            OSPF_SHA2_256_DIGEST_LEN),
                                                   (UINT4) (u2Len - u2OspfLen -
                                                            OSPF_SHA2_256_DIGEST_LEN));

                        CRU_BUF_Delete_BufChainAtEnd (pPkt,
                                                      (UINT4) (u2Len -
                                                               u2OspfLen -
                                                               OSPF_SHA2_256_DIGEST_LEN));
                    }
                    OSPF_CRU_BMC_GET_STRING (pPkt, pktDigest, u2OspfLen,
                                             OSPF_SHA2_256_DIGEST_LEN);
                    OSPF_CRU_BMC_ASSIGN_STRING (pPkt, pAuthkeyInfo->authKey,
                                                (UINT4) (u2OspfLen),
                                                (UINT4)
                                                OSPF_SHA2_256_DIGEST_LEN);

                    UtilGetShaDigest (pPkt, u2OspfLen, pAuthkeyInfo,
                                      OSPF_AUTH_SHA2_256, calDigest);
                    if (OS_MEM_CMP
                        (pktDigest, calDigest, OSPF_SHA2_256_DIGEST_LEN))
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                                  " SHA1 digest not matching\n");
                        return (OSPF_FAILURE);
                    }
                    break;
                case OSPF_AUTH_SHA2_384:
                    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2OspfLen);

                    if (u2Len > (u2OspfLen + OSPF_SHA2_384_DIGEST_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, pTempPkt,
                                                   (UINT4) (u2OspfLen +
                                                            OSPF_SHA2_384_DIGEST_LEN),
                                                   (UINT4) (u2Len - u2OspfLen -
                                                            OSPF_SHA2_384_DIGEST_LEN));

                        CRU_BUF_Delete_BufChainAtEnd (pPkt,
                                                      (UINT4) (u2Len -
                                                               u2OspfLen -
                                                               OSPF_SHA2_384_DIGEST_LEN));
                    }
                    OSPF_CRU_BMC_GET_STRING (pPkt, pktDigest, u2OspfLen,
                                             OSPF_SHA2_384_DIGEST_LEN);
                    OSPF_CRU_BMC_ASSIGN_STRING (pPkt, pAuthkeyInfo->authKey,
                                                (UINT4) (u2OspfLen),
                                                (UINT4)
                                                OSPF_SHA2_384_DIGEST_LEN);

                    UtilGetShaDigest (pPkt, u2OspfLen, pAuthkeyInfo,
                                      OSPF_AUTH_SHA2_384, calDigest);
                    if (OS_MEM_CMP
                        (pktDigest, calDigest, OSPF_SHA2_384_DIGEST_LEN))
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                                  " SHA1 digest not matching\n");
                        return (OSPF_FAILURE);
                    }
                    break;
                case OSPF_AUTH_SHA2_512:
                    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2OspfLen);

                    if (u2Len > (u2OspfLen + OSPF_SHA2_512_DIGEST_LEN))
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, pTempPkt,
                                                   (UINT4) (u2OspfLen +
                                                            OSPF_SHA2_512_DIGEST_LEN),
                                                   (UINT4) (u2Len - u2OspfLen -
                                                            OSPF_SHA2_512_DIGEST_LEN));

                        CRU_BUF_Delete_BufChainAtEnd (pPkt,
                                                      (UINT4) (u2Len -
                                                               u2OspfLen -
                                                               OSPF_SHA2_512_DIGEST_LEN));
                    }
                    OSPF_CRU_BMC_GET_STRING (pPkt, pktDigest, u2OspfLen,
                                             OSPF_SHA2_512_DIGEST_LEN);
                    OSPF_CRU_BMC_ASSIGN_STRING (pPkt, pAuthkeyInfo->authKey,
                                                (UINT4) (u2OspfLen),
                                                (UINT4)
                                                OSPF_SHA2_512_DIGEST_LEN);

                    UtilGetShaDigest (pPkt, u2OspfLen, pAuthkeyInfo,
                                      OSPF_AUTH_SHA2_512, calDigest);
                    if (OS_MEM_CMP
                        (pktDigest, calDigest, OSPF_SHA2_512_DIGEST_LEN))
                    {
                        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                                  " SHA1 digest not matching\n");
                        return (OSPF_FAILURE);
                    }
                    break;
                default:
                    break;
            }
            if (pNbr)
                pNbr->u4NbrCryptSeqNum = u4RcvdCryptSeqNum;
            return (SUCCESS);

        default:

            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Unknown AuthType\n");
            return (OSPF_FAILURE);

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppRcvPkt                                                  */
/*                                                                           */
/* Description  : Reference : RFC-2178 Section 8.2                           */
/*                This procedure handles the reception of OSPF packets from  */
/*                IP. This procedure validates the OSPF header field values  */
/*                and associates the packet with one of the neighbors and    */
/*                invokes the appropriate procedure to process the packet    */
/*                based on the packet type.                                  */
/*                                                                           */
/* Input        : pPkt                       : the received pkt              */
/*                u2Len                      : the length of pkt             */
/*                pInterface                  : the incoming interface       */
/*                pSrcIpAddr               : the src ip addr from IP hdr     */
/*                pDestIpAddr              : the dest ip addr from IP hdr    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
PppRcvPkt (tCRU_BUF_CHAIN_HEADER * pPkt,
           UINT2 u2Len,
           tInterface * pInterface, tIPADDR * pSrcIpAddr, tIPADDR * pDestIpAddr)
{

    UINT1               u1Type;
    UINT1               u1Version;
    INT4                i4ReturnStatus;
    tRouterId           headerRtrId;
    tAreaId             areaId;
    UINT1               u1AcptPktFlag = OSPF_FALSE;
    UINT2               u2AuthType;
    tIPADDR            *pNbrSearchField;
    tNeighbor          *pNbr;
    tInterface         *pAssoIface;
    tOspfLowPriQMsg    *pOspfLowPriQMsg = NULL;
    UINT4               u4PktCount;
    UINT4               u4IfIndex = 0;

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* Only Active node can receive the pkt */
        return;
    }

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: PppRcvPkt\n");

    OSPF_TRC4 (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
               pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "Pkt Rcvd IfIndx: %d  Src: %x  Dest: %x  Len: %d\n",
               pInterface->u4IfIndex,
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pSrcIpAddr),
               OSPF_CRU_BMC_DWFROMPDU ((UINT1 *) pDestIpAddr), u2Len);

    OSPF_CRU_BMC_GET_1_BYTE (pPkt, TYPE_OFFSET_IN_HEADER, u1Type);

    /* check protocol admin bStatus */
    if (pInterface->pArea->pOspfCxt->admnStat != OSPF_ENABLED)
    {

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc (Proto Not Enabled)\n");

        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        return;
    }

    OSPF_INC_PKT_DUMP (pInterface->pArea->pOspfCxt->u4OspfCxtId, pPkt);    /* Dump the incoming packet */

    /* check state of interface */
    if (IS_DC_EXT_APPLICABLE_PTOP_IF (pInterface) &&
        (pInterface->u1IsmState != IFS_LOOP_BACK))
    {
        u1AcptPktFlag = OSPF_TRUE;
    }
    if (u1AcptPktFlag == OSPF_FALSE)
    {
        if ((pInterface->u1IsmState == IFS_DOWN) ||
            (pInterface->u1IsmState == IFS_LOOP_BACK))
        {

            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Pkt Disc (If Not UP)\n");

            pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);

            return;
        }
    }

    /* Validate version number field in OSPF header */
    OSPF_CRU_BMC_GET_1_BYTE (pPkt, VERSION_NO_OFFSET_IN_HEADER, u1Version);
    OSPF_CRU_BMC_GET_STRING (pPkt, headerRtrId, RTR_ID_OFFSET_IN_HEADER,
                             MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_GET_STRING (pPkt, areaId, AREA_ID_OFFSET_IN_HEADER,
                             MAX_IP_ADDR_LEN);

    if (u1Version != OSPF_VERSION_NO_2)
    {

        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc (Ver No. Mismatch)\n");

        if ((pAssoIface = PppAssociateWithVirtualIf (pInterface,
                                                     &headerRtrId)) != NULL)
        {
            VifSendConfErrTrap (pAssoIface, VIRT_IF_CONFIG_ERROR_TRAP,
                                OSPF_BAD_VERSION, u1Type);
        }
        else
        {
            IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                               OSPF_BAD_VERSION, u1Type, pSrcIpAddr);
        }

        return;
    }

    /* Validate Router ID field in OSPF header */
    if (UtilIpAddrComp (headerRtrId, pInterface->pArea->pOspfCxt->rtrId) ==
        OSPF_EQUAL)
    {
        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc: self-originated PDU\n ");
        return;
    }

    /* Validate area id field in OSPF header */
    if (UtilIpAddrComp (areaId, pInterface->pArea->areaId) == OSPF_EQUAL)
    {
        /* validate source ip addr */
        if (pInterface->u1NetworkType != IF_PTOP)
        {
            if (UtilIpAddrMaskComp (*pSrcIpAddr,
                                    pInterface->ifIpAddr,
                                    pInterface->ifIpAddrMask) != OSPF_EQUAL)
            {
                pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Pkt Disc (IP Addr Mismatch)\n");
                return;
            }
        }
    }
    else if (UtilIpAddrComp (areaId, gBackboneAreaId) == OSPF_EQUAL)
    {
        /* associate with virtual interface */
        if ((pAssoIface = PppAssociateWithVirtualIf (pInterface,
                                                     &headerRtrId)) == NULL)
        {
            pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);

            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Pkt Disc (Association With Virt If Fails)\n");

            IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                               AREA_MISMATCH, u1Type, pSrcIpAddr);
            return;
        }
        /* u4IfIndex will be used in priority Queue */
        u4IfIndex = pInterface->u4IfIndex;
        pInterface = pAssoIface;
    }
    else
    {
        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc (AreaID MisMatch)\n");
        IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                           AREA_MISMATCH, u1Type, pSrcIpAddr);
        return;
    }

    /* check destination address */
    if ((UtilIpAddrComp (*pDestIpAddr, gAllDRtrs) == OSPF_EQUAL) &&
        (pInterface->u1IsmState != IFS_DR) &&
        (pInterface->u1IsmState != IFS_BACKUP))
    {
        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc (Dest Addr MisMatch)\n");
        return;
    }

    /* check authentication type */
    OSPF_CRU_BMC_GET_2_BYTE (pPkt, AUTH_TYPE_OFFSET_IN_HEADER, u2AuthType);
    if (pInterface->u2AuthType != u2AuthType)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc (Auth Type MisMatch)\n");
        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        if (IS_VIRTUAL_IFACE (pInterface))
        {
            VifSendConfErrTrap (pInterface, VIRT_IF_AUTH_FAILURE_TRAP,
                                AUTH_TYPE_MISMATCH, u1Type);
        }
        else
        {
            IfSendConfErrTrap (pInterface, IF_AUTH_FAILURE_TRAP,
                               AUTH_TYPE_MISMATCH, u1Type, pSrcIpAddr);
        }
        return;
    }

    i4ReturnStatus = AuthenticateRcvdPkt (u2AuthType, pInterface, pPkt,
                                          pSrcIpAddr, u2Len);
    switch (i4ReturnStatus)
    {
        case OSPF_CHKSUM_FAILURE:
            pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);
            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Rcvd Bad Pkt - Discarded \n");
            IfSendBadPktTrap (pInterface, u1Type, pSrcIpAddr);
            return;

        case OSPF_FAILURE:
            pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);
            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Pkt Disc (Auth Failure)\n");

            if (IS_VIRTUAL_IFACE (pInterface))
            {
                VifSendConfErrTrap (pInterface, VIRT_IF_AUTH_FAILURE_TRAP,
                                    OSPF_AUTH_FAILURE, u1Type);
            }
            else
            {
                IfSendConfErrTrap (pInterface, IF_AUTH_FAILURE_TRAP,
                                   OSPF_AUTH_FAILURE, u1Type, pSrcIpAddr);
            }
            return;

        case SUCCESS:
            break;
        default:
            return;
    }

    /* the length needs to be reduced if it is a crypt authentication */
    OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt, 2, u2Len);

    /* commented by DS on 1996-MM-DD */
    /* dump the pkts here if need */

    /* if packet type is hello invoke HpRcvHello procedure */
    if (u1Type == HELLO_PKT)
    {
        COUNTER_OP (pInterface->u4HelloRcvdCount, 1);
        HpRcvHello (pPkt, u2Len, pInterface, pSrcIpAddr);
    }
    else
    {
        /* associate with a neighbor */
        if ((pInterface->u1NetworkType == IF_BROADCAST) ||
            (pInterface->u1NetworkType == IF_NBMA) ||
            (pInterface->u1NetworkType == IF_PTOMP))
        {
            pNbrSearchField = pSrcIpAddr;
        }
        else
        {
            pNbrSearchField = (tIPADDR *) headerRtrId;
        }
        pNbr = HpSearchNbrLst (pNbrSearchField, pInterface);
        if (pNbr == NULL)
        {
            pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
            UtilOsMsgFree (pPkt, NORMAL_RELEASE);

            OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Pkt Disc (Could Not be Associated With Any Nbr)\n");

            if (IS_NBMA_IFACE (pInterface))
            {
                IfSendConfErrTrap (pInterface, IF_CONFIG_ERROR_TRAP,
                                   UNKNOWN_NBMA_NBR, u1Type, pSrcIpAddr);
            }
            return;
        }
        /* For congestion control , restarting inactivty timer as per RFC 4222 */
        TmrRestartTimer (&pNbr->inactivityTimer, INACTIVITY_TIMER,
                         NO_OF_TICKS_PER_SEC *
                         (pNbr->pInterface->i4RtrDeadInterval));

        if (UtilOspfIsLowPriorityOspfPkt (u1Type) == OSPF_TRUE)
        {
            /*allocate memory for pOspfLowPriQMsg */
            if (PRIORITY_QMSG_ALLOC (pOspfLowPriQMsg) == NULL)
            {
                OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC,
                              OSPF_INVALID_CXT_ID, " Alloc Failure\n");
                pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                return;
            }
            OS_MEM_CPY (&(pOspfLowPriQMsg->ospfLowPriQPktInfo.NbrIp),
                        pNbrSearchField, sizeof (tIPADDR));

            pOspfLowPriQMsg->ospfLowPriQPktInfo.pPkt = pPkt;
            pOspfLowPriQMsg->ospfLowPriQPktInfo.u4OspfCxtId =
                pInterface->pArea->pOspfCxt->u4OspfCxtId;

            if (pInterface->u1NetworkType != IF_VIRTUAL)
            {
                u4IfIndex = pInterface->u4IfIndex;
            }

            pOspfLowPriQMsg->ospfLowPriQPktInfo.u4IfIndex = u4IfIndex;
            pOspfLowPriQMsg->ospfLowPriQPktInfo.u1PktType = u1Type;
            pOspfLowPriQMsg->ospfLowPriQPktInfo.u2PktLen = u2Len;
            pOspfLowPriQMsg->ospfLowPriQPktInfo.u1NetworkType =
                pInterface->u1NetworkType;

            pOspfLowPriQMsg->u4MsgType = OSPF_IP_PKT_RCVD;

            if (OsixSendToQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                             (tOsixMsg *) (VOID *) pOspfLowPriQMsg,
                             OSIX_MSG_NORMAL) != OSIX_SUCCESS)
            {
                PRIORITY_QMSG_FREE (pOspfLowPriQMsg);
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                return;
            }
            OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ", &u4PktCount);
            /* Self MSGQ Event is posted for processing of LowPrioMsgs 
             * in Main ReceiveEvent loop */
#ifdef RAWSOCK_WANTED
            if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT)
                != OSIX_SUCCESS)
            {
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Send Evt Failed\n");
            }
            return;
#endif
            /* To support from system.size, the macro OSPF_LOW_PRIORITY_QUEUE_SIZE
             * is replaced with sizing variable
             * FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].u4PreAllocatedUnits/2
             */

            if (u4PktCount ==
                (FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].
                 u4PreAllocatedUnits / 2))
            {
                OspfProcessLowPriQMsg ((const UINT1 *) "OLPQ");
            }
            return;
        }

        switch (u1Type)
        {
            case DD_PKT:
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Received DDP\n");
                COUNTER_OP (pNbr->pInterface->u4DdpRcvdCount, 1);
                DdpRcvDdp (pPkt, u2Len, pNbr);
                break;

            case LSA_REQ_PKT:
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Received LSR\n");
                COUNTER_OP (pNbr->pInterface->u4LsaReqRcvdCount, 1);
                LrqRcvLsaReq (pPkt, u2Len, pNbr);
                break;

            case LS_UPDATE_PKT:
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Received LSU\n");
                COUNTER_OP (pNbr->pInterface->u4LsaUpdateRcvdCount, 1);
                LsuRcvLsUpdate (pPkt, pNbr);
                break;

            case LSA_ACK_PKT:
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Received LAK\n");
                COUNTER_OP (pNbr->pInterface->u4LsaAckRcvdCount, 1);
                LakRcvLsAck (pPkt, u2Len, pNbr);
                break;

            default:
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Rcvd Junk\n");
                pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                IfSendBadPktTrap (pInterface, u1Type, pSrcIpAddr);
                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Pkt Disc (Invalid Pkt Type)\n");
                return;
        }
    }
    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
    OSPF_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Pkt Processing Over\n");
    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: PppRcvPkt\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppAssociateWithVirtualIf                                  */
/*                                                                           */
/* Description  : This procedure associates the received packet with a       */
/*                virtual interface based on the received interface and the  */
/*                router ID of the source of the packet.                     */
/*                                                                           */
/* Input        : pInterface        : the receiving interface                */
/*                pRtrId           : the source router id                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to associated interface, if successfull            */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
PppAssociateWithVirtualIf (tInterface * pInterface, tRouterId * pRtrId)
{

    tInterface         *pLstIf;
    tTMO_SLL_NODE      *pLstNode;

    if ((pInterface->pArea->pOspfCxt->bAreaBdrRtr == OSPF_TRUE) ||
        (pInterface->pArea->pOspfCxt->u4ABRType != STANDARD_ABR))
    {
        TMO_SLL_Scan (&(pInterface->pArea->pOspfCxt->virtIfLst), pLstNode,
                      tTMO_SLL_NODE *)
        {
            pLstIf = GET_IF_PTR_FROM_SORT_LST (pLstNode);
            if ((UtilIpAddrComp (pLstIf->destRtrId, *pRtrId) == OSPF_EQUAL) &&
                (UtilIpAddrComp (pInterface->pArea->areaId,
                                 pLstIf->transitAreaId) == OSPF_EQUAL))
            {
                return pLstIf;
            }
        }
    }
    OSPF_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "Cannot Associate With Virt Nbr\n");
    return (tInterface *) NULL;
}

/*---------------------------------------------------------------------------*/
/*                         End of file osppp.c                               */
/*---------------------------------------------------------------------------*/
