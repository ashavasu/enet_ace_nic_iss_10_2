/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osmain.c,v 1.88 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains the ospf task main loop.
 *             and the initialisation routines.
 *
 *******************************************************************/
#ifndef  OSMAIN_C
#define  OSMAIN_C

#include "osinc.h"
#include "osglob.h"
#include "osmain.h"
#include "osprot.h"
#ifdef OPQAPSIM_WANTED
#include "oashdr.h"
#endif

/* Proto types of the functions private to this file only */

PRIVATE INT4 OspfMemInit PROTO ((VOID));
PRIVATE VOID        OspfProcessRMapHandler (tOsixMsg *);
#ifdef HIGH_PERF_RXMT_LST_WANTED
PRIVATE VOID OspfRxmtNodeMemInit PROTO ((VOID));
PRIVATE VOID OspfRestartNbrAdjaceny PROTO ((UINT4 u4OspfCxtId));
#endif
PRIVATE VOID OspfFreeMsgsInQueue PROTO ((VOID));

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
tOsixQId            gu4OspfVrfMsgQId;
#endif
/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFTaskMain                                               */
/*                                                                           */
/* Description  : Main function of OSPF.                                     */
/*                                                                           */
/* Input        : pTaskId                                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OSPFTaskMain (INT1 *pTaskId)
{

    UINT4               u4Event;
    UINT4               u41sTicks = 1 * NO_OF_TICKS_PER_SEC;
    INT4                i4RetVal = OSPF_FAILURE;
    tOspfCxt           *pOspfCxt = NULL;
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    INT4                i4SockId = -1;
    INT4                i4Count = 0;
    UINT1               u1Registerflag = 0;
#endif
#ifdef ROUTEMAP_WANTED
    UINT2               u2Index = 0;
#endif

#ifdef OPQAPSIM_WANTED
    oasInit ();
#endif

    UNUSED_PARAM (pTaskId);

    if (OspfTaskInit () == OSPF_FAILURE)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! TASK INIT FAILURE  !!!!! \n");
        OSPF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Initializes the ism schedule queue */
    IsmInitSchedQueue ();
    /* Restore the global information from non-volatile storage */
    GrRestoreGlobalRestartInfo ();

    if (RtrCreateCxt (OSPF_DEFAULT_CXT_ID, OSPF_DISABLED) == OSPF_FAILURE)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! TASK INIT FAILURE  !!!!! \n");
        OSPF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if ((pOspfCxt = UtilOspfGetCxt (OSPF_DEFAULT_CXT_ID)) == NULL)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! TASK INIT FAILURE  !!!!! \n");
        OSPF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    pOspfCxt->contextStatus = ACTIVE;
    if (UtilOspfGetVcmSystemModeExt (OSPF_PROTOCOL_ID) == VCM_SI_MODE)
    {
        gOsRtr.pOspfCxt = pOspfCxt;
        gOsRtr.u4OspfCxtId = OSPF_DEFAULT_CXT_ID;
    }
    gi4OspfSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "OSPF", SYSLOG_CRITICAL_LEVEL);
#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_OSPF2, OspfSendRouteMapUpdateMsg);
    for (; u2Index < MAX_OSPF_RMAP_MSGS; u2Index++)
    {
        gOsRmapMsg[u2Index] = NULL;
    }
#endif

    OspfRmInit ();

    i4RetVal = OspfRmRegisterWithRM ();

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! OSPF registration with RM module failed  !!!!!\n");
        OSPF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate ISS module regarding module start */
    IssSetModuleSystemControl (OSPF_MODULE_ID, MODULE_START);

    /* Indicate the status of initialization to the main routine */
    OSPF_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        OsixReceiveEvent (OSPF_TIMER_EVENT | OSPF_MSGQ_IF_EVENT |
                          OSPF_RTM_RTMSG_EVENT | OSPF_PKT_ARRIVAL_EVENT |
                          HELLO_TIMER_EXP_EVENT | TMO1_TIMER_01_EXP_EVENT |
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
                          OSPF_NLH_PKT_ARRIVAL_EVENT |
#endif
#endif
                          OSPF_RED_SUB_BULK_UPD_EVENT, OSIX_WAIT, (UINT4) 0,
                          (UINT4 *) &(u4Event));
        OspfLock ();

        OSPF_GBL_TRC1 (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                       "Rx Event %d\n", u4Event);

#ifndef LNXIP4_WANTED
        if (u4Event & OSPF_MSGQ_IF_EVENT)
        {
            OspfProcessQMsg ((const UINT1 *) "OSPQ");
        }
#endif
        /* Process the Routes received from RTM */
        if (u4Event & OSPF_RTM_RTMSG_EVENT)
        {
            OspfProcessRtmRts ();
        }

        /* Process the subbulk update */
        if (u4Event & OSPF_RED_SUB_BULK_UPD_EVENT)
        {
            OspfRmProcessBulkReq ();
        }
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
        /* Process the Packets received on Socket */
        if (u4Event & OSPF_NLH_PKT_ARRIVAL_EVENT)
        {
            IpifGetNLHelloPkt ();
            /* Add the Socket Descriptor to Select utility 
             * for Packet Reception */
            /* This check must be done because thro. 
             * CLI we might delete the socket and initialized to -1*/
            if (gOsRtr.i4NLHelloSockId != -1)
            {
                if (SelAddFd (gOsRtr.i4NLHelloSockId, OspfPacketOnNLHelloSocket)
                    != OSIX_SUCCESS)
                {
                    OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                                  "Adding Hello Socket Descriptor to Select utility Failed \r\n ");
                }
            }
        }
#endif
#endif

#ifdef RAWSOCK_WANTED
        /* Process the Packets received on Socket */
        if (u4Event & OSPF_PKT_ARRIVAL_EVENT)
        {
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            tOspfVrfSock       *pQueMsg = NULL;
            while (OsixReceiveFromQ (SELF, (const UINT1 *) "OSPFLNXVRF",
                                     OSIX_NO_WAIT, 0,
                                     (tOsixMsg **) (&pQueMsg)) == OSIX_SUCCESS)
            {
                u1Registerflag = 0;
                i4SockId = pQueMsg->i4SockFd;

                IpifGetPkt (i4SockId);
                MemReleaseMemBlock (VRF_SOCK_FD, (UINT1 *) pQueMsg);
                pQueMsg = NULL;

                if (i4SockId != -1)
                {
                    for (i4Count = 0; i4Count < SYS_DEF_MAX_NUM_CONTEXTS;
                         i4Count++)
                    {
                        if (gOsRtr.ai4SockId[i4Count] == i4SockId)
                        {
                            u1Registerflag = 1;
                            break;
                        }
                    }
                    if (u1Registerflag == 1)
                    {
                        if (SelAddFd (i4SockId, OspfPacketOnSocket) !=
                            OSIX_SUCCESS)
                        {
                            OSPF_GBL_TRC (OSPF_CRITICAL_TRC,
                                          OSPF_INVALID_CXT_ID,
                                          "Adding Socket Descriptor to Select utility Failed \r\n ");
                        }
                    }
                }
            }
#else
            IpifGetPkt (gOsRtr.ai4SockId[0]);
            /* Add the Socket Descriptor to Select utility 
             * for Packet Reception */
            /* This check must be done because thro. 
             * CLI we might delete the socket and initialized to -1*/
            if (gOsRtr.ai4SockId[VCM_DEFAULT_CONTEXT] != -1)
            {
                if (SelAddFd
                    (gOsRtr.ai4SockId[VCM_DEFAULT_CONTEXT],
                     OspfPacketOnSocket) != OSIX_SUCCESS)
                {
                    OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                                  "Adding Socket Descriptor to Select utility Failed \r\n ");
                }
            }
#endif
        }
#endif
        if (u4Event & TMO1_TIMER_01_EXP_EVENT)
        {
            gOspf1sTimer++;

            /* One second timer is expired, hence the timer node can be
             * removed from the timer list and then the timer can be
             * started.
             */
            TmrGetNextExpiredTimer (gOnesecTimerLst);
            TmrStartTimer (gOnesecTimerLst, &(gOnesecTimerNode),
                           (UINT4) u41sTicks);

        }

        if (u4Event & HELLO_TIMER_EXP_EVENT)
        {

            OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                          "Rx Timer Event\n");

            TmrHandleHelloExpiry ();
        }
#ifdef LNXIP4_WANTED
        if (u4Event & OSPF_MSGQ_IF_EVENT)
        {
            OspfProcessQMsg ((const UINT1 *) "OSPQ");
        }
#endif
        if (u4Event & OSPF_TIMER_EVENT)
        {

            OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                          "Rx Timer Event\n");

            TmrHandleExpiry ();
        }

        /* Any event posted to the ISM process queue is executed now */
        IsmProcessSchedQueue ();

        /* Mutual exclusion flag OFF */
        OspfUnLock ();
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTaskInit                                             */
/*                                                                           */
/* Description  : OSPF initialization routine.                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if initialization succeeds                        */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OspfTaskInit (void)
{

    tOsixQId            u4MsgQId;
    tOsixQId            u4MsgLowPriQId;
    UINT4               u41sTicks = 1;
    tVcmRegInfo         VcmRegInfo;

    OSPF_GBL_TRC (INIT_SHUT_TRC, OSPF_INVALID_CXT_ID, "OSPF Init Entered\n");

    if (OsixSemCrt (OSPF_MUT_EXCL_SEM_NAME, &gOspfSemId) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC1 (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                       "Semaphore Creation failure for %s \n",
                       OSPF_MUT_EXCL_SEM_NAME);
        return OSPF_FAILURE;
    }

    OsixSemGive (gOspfSemId);

    /* Create the Semaphore for RTM RouteList */
    if (OsixSemCrt (OSPF_RTMRT_LIST_SEM, &(gOsRtr.RtmLstSemId)) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      "Creation of RTM ROUTE Semaphore failed. \n");
        return OSPF_FAILURE;
    }
    OsixSemGive (gOsRtr.RtmLstSemId);
    /* Register With VCM Module */
    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = OspfVcmCallbackFn;
    VcmRegInfo.u1InfoMask |= (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ);
    VcmRegInfo.u1ProtoId = OSPF_PROTOCOL_ID;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                  "VCM registration failed \n");
        return OSPF_FAILURE;

    }

    /* Initialize the temporary RTM Route List */
    TMO_SLL_Init (&(gOsRtr.RtmRtLst));

    gOsRtr.u1RouteCalcCompleted = OSPF_ROUTE_CALC_COMPLETED;
    gOsRtr.u4VrfSpfInterval = OSPF_DEF_VRF_SPF_INTERVAL;

#ifdef HIGH_PERF_RXMT_LST_WANTED
    OspfRxmtNodeMemInit ();
    gOsRtr.pRxmtLstRBRoot = RBTreeCreateEmbedded (OSPF_OFFSET (tRxmtNode,
                                                               RbNode),
                                                  RbCompareRxmtInfo);
    if (gOsRtr.pRxmtLstRBRoot == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      "Creation of RBTree for Retransmission List For LSAS Failed. \n");
        return OSPF_FAILURE;
    }
#endif /* HIGH_PERF_RXMT_LST_WANTED */

#ifdef TRACE_WANTED
    /*OSPF_TRC_FLAG = 0; */
#endif

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
    RegisterOSPFMibs ();
#endif

    /* Create buffer pools for data structures */
    if (OspfMemInit () == OSPF_FAILURE)
    {

        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "Memory Pool Creation Failed\n");

        OspfMemClear ();
        return OSPF_FAILURE;
    }

    gOsRtr.pRtrLsaCheck =
        RBTreeCreateEmbedded (OSPF_OFFSET (tRtrLsaRtInfo, RbNode),
                              GrCompareRouterLsaEntry);
    if (gOsRtr.pRtrLsaCheck == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "RtrLsaRtInfo RB Tree Creation Failed\n");
        return OSPF_FAILURE;
    }

    gOsRtr.pNwLsaCheck =
        RBTreeCreateEmbedded (OSPF_OFFSET (tNtLsaRtInfo, RbNode),
                              GrCompareNetworkLsaEntry);
    if (gOsRtr.pNwLsaCheck == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "NwLsaRtInfo RB Tree Creation Failed\n");
        return OSPF_FAILURE;
    }

    if (OsixCreateQ ((const UINT1 *) "OSPQ",
                     FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].
                     u4PreAllocatedUnits, (UINT4) 0,
                     (tOsixQId *) & (u4MsgQId)) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      "OSPQ Creation Failed\n");
        return OSPF_FAILURE;
    }

    if (OsixCreateQ
        ((const UINT1 *) "OLPQ",
         (FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].u4PreAllocatedUnits /
          2), (UINT4) 0, (tOsixQId *) & (u4MsgLowPriQId)) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      "OSPF LowPriority queue creation Failed\n");
        return OSPF_FAILURE;
    }

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    /* Create the OSPF VRF Packet arrival Q */
    if (OsixCreateQ
        ((const UINT1 *) "OSPFLNXVRF", 16, OSIX_LOCAL,
         &(gu4OspfVrfMsgQId)) != OSIX_SUCCESS)

    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      "OSPF LINUX VRF  queue creation Failed \n");
        return OSPF_FAILURE;
    }
#endif

    /* The following routine initialises the timer descriptor structure */
    TmrInitTimerDesc ();

#ifdef DEBUG_WANTED
    gp_ospf_output_file = stdout;
#endif

    gOsRtr.bIgnoreExpiredTimers = OSPF_FALSE;
    gOsRtr.u4MaxMtuSize = 0;
    gOsRtr.u4AsExtRouteAggrCnt = 0;
    gOsRtr.u4RTstaggeredCtxId = OSPF_INVALID_CXT_ID;
    gOsRtr.u4RTStaggeringStatus = OSPF_STAGGERING_ENABLED;
    gOsRtr.b1GrCsrStatus = FALSE;

    /* Associate the global timer list with the Event */
    if (TmrCreateTimerList ((CONST UINT1 *) OSPF_TASK_NAME, OSPF_TIMER_EVENT,
                            NULL,
                            (tTimerListId *) & (gTimerLst)) != TMR_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Tmr Lst Creation Failed\n");
        return OSPF_FAILURE;
    }
    /* Create a Semaphore for the rt manuplation routines(osrtr.c) */
    if (OsixCreateSem ((const UINT1 *) "RTSM", 1 /* InitialCount */ ,
                       OSIX_SEM_FIFO,
                       (tOsixSemId *) & (gOsRtr.OspfRtSemId)) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Semaphore Creation Failed\n");
        return OSPF_FAILURE;
    }
    if (OsixCreateSem ((const UINT1 *) "CXTSM", 1 /* InitialCount */ ,
                       OSIX_SEM_FIFO,
                       (tOsixSemId *) & (gOsRtr.OspfCxtSemId)) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Semaphore Creation Failed\n");
        OsixSemDel (gOsRtr.OspfRtSemId);
        return OSPF_FAILURE;
    }
    /* Create 1 Sec Timer here, associate TMO1_TIMER_01_EXP_EVENT with it */
    if (TmrCreateTimerList
        ((CONST UINT1 *) OSPF_TASK_NAME, TMO1_TIMER_01_EXP_EVENT, NULL,
         (tTimerListId *) & (gOnesecTimerLst)) != TMR_SUCCESS)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Creation of 1 Sec Tmr Failed\n");
        OsixSemDel (gOsRtr.OspfRtSemId);
        OsixSemDel (gOsRtr.OspfCxtSemId);
        return OSPF_FAILURE;
    }
    if (TmrCreateTimerList
        ((CONST UINT1 *) OSPF_TASK_NAME, HELLO_TIMER_EXP_EVENT, NULL,
         (tTimerListId *) & (gHelloTimerLst)) != TMR_SUCCESS)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Creation of 1 Sec Tmr Failed\n");
        OsixSemDel (gOsRtr.OspfRtSemId);
        OsixSemDel (gOsRtr.OspfCxtSemId);
        return OSIX_FAILURE;
    }

    if (TmrStartTimer (gOnesecTimerLst, &(gOnesecTimerNode),
                       (UINT4) u41sTicks) != TMR_SUCCESS)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Failed to start 1 Sec Tmr \n");
        TmrDeleteTimerList (gOnesecTimerLst);
        TmrDeleteTimerList (gTimerLst);
        OsixSemDel (gOsRtr.OspfRtSemId);
        OsixSemDel (gOsRtr.OspfCxtSemId);
        return OSPF_FAILURE;
    }

    gOspf1sTimer = 0;

    gOsRtr.pIfHashTable = TMO_HASH_Create_Table (IF_HASH_TABLE_SIZE,
                                                 NULL, FALSE);

    if (gOsRtr.pIfHashTable == NULL)
    {
        return OSPF_FAILURE;
    }

    TMO_DLL_Init (&(gOsRtr.vrfSpfRtcList));    /* vrf spf list */

    gOsRtr.u4ActiveCxtCount = 0;
    gOsRtr.u4AckOrUpdatePktSentCtr = 0;
    gOsRtr.u4RcvPktCtr = 0;
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfMemInit                                              */
/*                                                                           */
/* Description  : Allocates all the memory that is required for OSPF.        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if memory allocation successful                   */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
OspfMemInit (void)
{
    INT4                i4RetVal;
#ifdef CLI_WANTED
    if (CsrMemAllocation (OSPF_MODULE_ID) == CSR_FAILURE)
    {
        return OSPF_FAILURE;
    }
#endif

    i4RetVal = OspfSizingMemCreateMemPools ();
    if (i4RetVal == OSIX_FAILURE)
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfMemClear                                             */
/*                                                                           */
/* Description  : Clears all the free queues.                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfMemClear (void)
{
#ifdef CLI_WANTED
    CsrMemDeAllocation (OSPF_MODULE_ID);
#endif
    OspfSizingMemDeleteMemPools ();

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfSnmpIfHandler                                       */
/*                                                                           */
/* Description  : Routine to invoke the other SNMP event associated routines.*/
/*                The SNMP events are posted to the event queue.             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfSnmpIfHandler (tOspfSnmpIfParam * pOspfIfParam)
{
    tInterface         *pInterface;
    INT4                i4RetStat = OSPF_FAILURE;
    tNeighbor          *pTempNbr = NULL;

    switch (pOspfIfParam->u1OpCode)
    {
        case OSPF_SET_PROTOCOL_STATUS:
            i4RetStat = RtrSetProtocolStatusInCxt
                (pOspfIfParam->u4OspfCxtId,
                 (UINT1) (pOspfIfParam->param.admnStatus.bOspfStatus));
            break;

#ifdef TOS_SUPPORT
        case OSPF_SET_TOS_SUPPORT:
            i4RetStat = RtrSetTosSupportInCxt (pOspfIfParam->pOspfCxt, (UINT1)
                                               (pOspfIfParam->param.tos_support.
                                                bTosStatus));
            break;
#endif /* TOS_SUPPORT */

        case OSPF_SET_AS_BDR_RTR_STATUS:
            i4RetStat = RtrSetAsbrStatusInCxt (pOspfIfParam->pOspfCxt, (UINT1)
                                               (pOspfIfParam->param.asbr_status.
                                                bStatus));
            break;

        case OSPF_DELETE_AREA:
            i4RetStat = AreaDelete (pOspfIfParam->param.area_param.pArea);
            break;

        case OSPF_DELETE_HOST:
            i4RetStat = HostDelete (pOspfIfParam->param.host_param.pHost);
            break;

        case OSPF_SIGNAL_LSA_REGEN:
            i4RetStat = OlsSignalLsaRegenInCxt (pOspfIfParam->pOspfCxt,
                                                pOspfIfParam->param.sig_param.
                                                u1Type,
                                                pOspfIfParam->param.sig_param.
                                                pStruct);
            break;

        case OSPF_REGENERATE_LSA:
            i4RetStat = OlsifParamChangeInCxt (pOspfIfParam->pOspfCxt,
                                               pOspfIfParam->param.sig_param.
                                               pStruct,
                                               pOspfIfParam->param.sig_param.
                                               u1Type);
            break;

        case OSPF_EXTRT_DELETE:
            i4RetStat = ExtrtDeleteInCxt (pOspfIfParam->pOspfCxt,
                                          (tExtRoute *) (VOID *) pOspfIfParam->
                                          param.sig_param.pStruct);
            break;

        case OSPF_EXTRT_ADD:
            i4RetStat = ExtrtActivateInCxt (pOspfIfParam->pOspfCxt,
                                            (tExtRoute *) (VOID *)
                                            pOspfIfParam->param.sig_param.
                                            pStruct);
            break;

        case OSPF_IF_ACTIVATE:
            i4RetStat = IfActivate (pOspfIfParam->param.if_param.pInterface);
            break;

        case OSPF_IF_INACTIVATE:
            i4RetStat = IfInactivate (pOspfIfParam->param.if_param.pInterface);
            break;

        case OSPF_DELETE_IF:
            i4RetStat = IfDelete (pOspfIfParam->param.if_param.pInterface);
            break;

        case OSPF_SET_IF_AREA_ID:
        {
            tAreaId             areaId;

            OSPF_CRU_BMC_DWTOPDU (areaId,
                                  pOspfIfParam->param.if_param.u4AreaId);

            i4RetStat =
                IfSetAreaId (pOspfIfParam->param.if_param.pInterface, &areaId);
        }
            break;

        case OSPF_SET_ADMN_STATUS:
            i4RetStat =
                IfSetAdmnStat (pOspfIfParam->param.admn_param.pInterface,
                               pOspfIfParam->param.admn_param.u1Status);
            break;

        case OSPF_DELETE_IF_METRIC:
            i4RetStat = IfDeleteIfMetric ((pOspfIfParam->param.if_param.
                                           pInterface)->pArea);
            break;

        case OSPF_CREATE_VIRT_IF:
            if ((VifCreateInCxt (pOspfIfParam->pOspfCxt,
                                 &(pOspfIfParam->param.vif_param.areaId),
                                 &(pOspfIfParam->param.vif_param.nbrId),
                                 pOspfIfParam->param.vif_param.rowStatus)) !=
                NULL)
            {
                i4RetStat = OSPF_SUCCESS;
            }
            break;

        case OSPF_ACTIVATE_NBR:
            i4RetStat = NbrActivate (pOspfIfParam->param.nbr_param.pNbr);
            break;

        case OSPF_INACTIVATE_NBR:
            i4RetStat = NbrInactivate (pOspfIfParam->param.nbr_param.pNbr);
            break;

        case OSPF_DELETE_NBR:
            pTempNbr = pOspfIfParam->param.nbr_param.pNbr;
            pTempNbr->bIsBfdDisable = OSIX_FALSE;
            i4RetStat = NbrDelete (pOspfIfParam->param.nbr_param.pNbr);
            break;

        case OSPF_NBR_LL_DOWN:
            i4RetStat = NsmRunNsm (pOspfIfParam->param.nbr_param.pNbr,
                                   NBRE_LL_DOWN);
            break;

        case OSPF_ACTIVATE_VIRT_IF:
            if ((pInterface =
                 GetFindVirtIfInCxt (pOspfIfParam->pOspfCxt,
                                     &
                                     (pOspfIfParam->param.
                                      vif_param.areaId),
                                     &(pOspfIfParam->param.
                                       vif_param.nbrId))) != NULL)
            {
                pInterface->ifStatus = ACTIVE;
                OspfIfUp (pInterface);
                i4RetStat = OSPF_SUCCESS;
            }
            break;

        case OSPF_INACTIVATE_VIRT_IF:
            if ((pInterface =
                 GetFindVirtIfInCxt (pOspfIfParam->pOspfCxt,
                                     &
                                     (pOspfIfParam->param.
                                      vif_param.areaId),
                                     &(pOspfIfParam->param.
                                       vif_param.nbrId))) != NULL)
            {
                pInterface->ifStatus = NOT_IN_SERVICE;
                IfDisable (pInterface);
                i4RetStat = OSPF_SUCCESS;
            }
            break;

        case OSPF_DISABLE_SRC_PROTO:
            i4RetStat = RtmSrcProtoDisableInCxt (pOspfIfParam->pOspfCxt,
                                                 pOspfIfParam->param.
                                                 srcProtoDisParam.
                                                 u4SrcProtoDisMask);
            break;

        case SET_RRD_CONFIG_RECORD:

            i4RetStat = RtmSetRRDConfigRecordInCxt (pOspfIfParam->pOspfCxt,
                                                    &(pOspfIfParam->param.
                                                      RRDConfigRecParam.
                                                      rrdDestIPAddr),
                                                    &(pOspfIfParam->param.
                                                      RRDConfigRecParam.
                                                      rrdDestAddrMask));
            break;

        case DELETE_RRD_CONFIG_RECORD:
            i4RetStat = RtmDelRRDConfigRecordInCxt (pOspfIfParam->pOspfCxt,
                                                    &(pOspfIfParam->param.
                                                      RRDConfigRecParam.
                                                      rrdDestIPAddr),
                                                    &(pOspfIfParam->param.
                                                      RRDConfigRecParam.
                                                      rrdDestAddrMask));
            break;

        case OSPF_CHNG_OPQ_STATUS:
            i4RetStat = OpqChkAndChangeStatusInCxt (pOspfIfParam->pOspfCxt);
            break;

        case OSPF_SET_EXT_LSDB_LIMIT:
            i4RetStat = RtrSetExtLsdbLimitInCxt (pOspfIfParam->pOspfCxt);
            break;

        case OSPF_SET_EXIT_OVERFLOW_INT:
            i4RetStat =
                RtrSetExitOverflowIntervalInCxt (pOspfIfParam->pOspfCxt);
            break;

        case OSPF_CHNG_ROUTERID:
            i4RetStat = RtrSetRouterIdInCxt (pOspfIfParam->pOspfCxt,
                                             pOspfIfParam->param.rtrId_param.
                                             rtrId);
            break;

        case OSPF_SET_IF_TYPE:
            i4RetStat = IfSetType (pOspfIfParam->param.if_param.pInterface,
                                   pOspfIfParam->param.if_param.u1Type);
            break;

        case OSPF_CHANGE_AREA_SUMMARY:
            i4RetStat =
                AreaSendSummaryStatusChange (pOspfIfParam->param.area_param.
                                             pArea);
            break;

        case OSPF_TRNSLTR_ROLE_CHANGE:
            i4RetStat =
                NssaFsmTrnsltrRlChng (pOspfIfParam->param.area_param.pArea);
            break;

        case OSPF_SET_IF_DEMAND:
            i4RetStat = IfSetDemand (pOspfIfParam->param.if_param.pInterface,
                                     pOspfIfParam->param.if_param.u1Demand);
            break;

        case OSPF_ACTIVE_AREA_ADDR_RANGE:
            i4RetStat =
                AreaActiveAddrRange (pOspfIfParam->param.area_param.pArea,
                                     pOspfIfParam->param.area_param.
                                     u1AggregateIndex);
            break;

        case OSPF_DELETE_AREA_ADDR_RANGE:
            i4RetStat =
                AreaDeleteAddrRange (pOspfIfParam->param.area_param.pArea,
                                     pOspfIfParam->param.area_param.
                                     u1AggregateIndex);
            break;

        case SET_AREA_AGGR_EFFECT:
            i4RetStat = AreaSetAggrEffect (pOspfIfParam->param.area_param.pArea,
                                           pOspfIfParam->param.area_param.
                                           u1AggregateIndex,
                                           pOspfIfParam->param.area_param.
                                           u1AggregateEffect);
            break;

        case OSPF_SET_IF_PASSIVE:
            i4RetStat = IfSetPassive (pOspfIfParam->param.if_param.pInterface);
            break;

        case OSPF_SET_ALL_IF_PASSIVE:
            i4RetStat = IfAllPassive (pOspfIfParam->pOspfCxt);
            break;

        case SET_AREA_AGGR_TAG:
            i4RetStat =
                AreaSetAddrRangeTag (pOspfIfParam->param.area_param.pArea,
                                     pOspfIfParam->param.area_param.
                                     u1AggregateIndex);
            break;

        case OSPF_CHNG_AREA_TYPE:
            i4RetStat =
                AreaChangeAreaType ((tArea *) (VOID *) pOspfIfParam->param.
                                    sig_param.pStruct,
                                    pOspfIfParam->param.sig_param.u1Type);
            break;

        case OSPF_SET_ASBR_TRANS_STATUS:
            i4RetStat =
                OlsModifyNssaAsbrDefRtTransInCxt (pOspfIfParam->pOspfCxt);
            break;

        case OSPF_AS_EXT_AGG:
            i4RetStat = RagHandleAsExtAggInCxt
                (pOspfIfParam->pOspfCxt,
                 (tAsExtAddrRange *) (VOID *) pOspfIfParam->param.sig_param.
                 pStruct, pOspfIfParam->param.sig_param.u1Type);
            break;

        case OSPF_SET_ABR_TYPE:
            i4RetStat =
                RtrSetABRTypeInCxt (pOspfIfParam->pOspfCxt,
                                    pOspfIfParam->param.abrtype_param.
                                    u4ABRType);
            break;
            /* Graceful restart related operations */
        case OSPF_GR_SET_RESTART_SUPPORT:
            i4RetStat = GrSetRestartSupport (pOspfIfParam->pOspfCxt,
                                             pOspfIfParam->param.
                                             restartSupportParam.
                                             u1RestartSupport);
            break;
        case OSPF_GR_SET_GRACE_PERIOD:
            i4RetStat = GrSetGraceperiod (pOspfIfParam->pOspfCxt,
                                          pOspfIfParam->param.
                                          gracePeriodParam.u4GracePeriod);
            break;
        case OSPF_GR_PERFORM_RESTART:
            GrShutdownProcess ();
            i4RetStat = OSPF_SUCCESS;
            break;
        case OSPF_GR_SET_HELPER_SUPPORT:
            i4RetStat = GrSetHelperSupport (pOspfIfParam->pOspfCxt,
                                            pOspfIfParam->param.helper_param.
                                            u1HelperSupport);
            break;
        case OSPF_GR_SET_STRICT_LSA_CHECK:
            i4RetStat = GrSetStrictLsaCheck (pOspfIfParam->pOspfCxt,
                                             pOspfIfParam->param.helper_param.
                                             u1StrictLsaCheck);
            break;
        case OSPF_GR_SET_ACK_STATE:
            i4RetStat = GrSetGraceAckState (pOspfIfParam->pOspfCxt,
                                            pOspfIfParam->param.restart_param.
                                            u4GraceAckState);
            break;
        case OSPF_GR_SET_GRACELSA_MAXCOUNT:
            i4RetStat = GrSetGraceLsaTxCount (pOspfIfParam->pOspfCxt,
                                              pOspfIfParam->param.restart_param.
                                              u1GrLsaMaxTxCount);
            break;
        case OSPF_GR_SET_GR_RESTARTREASON:
            i4RetStat = GrSetGrRestartReason (pOspfIfParam->pOspfCxt,
                                              pOspfIfParam->param.restart_param.
                                              u1RestartReason);
            break;
        case OSPF_GR_SET_HELPER_GRTIMELIMIT:
            i4RetStat = GrSetHelperGrTimeLimit (pOspfIfParam->pOspfCxt,
                                                pOspfIfParam->param.
                                                helper_param.
                                                u4HelperGrTimeLimit);
            break;
        default:
            break;

    }
    return i4RetStat;
}

PUBLIC VOID
OspfProcessQMsg (const UINT1 au1QName[4])
{
    tOspfQMsg          *pOspfQMsg = NULL;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    INT4                i4RetVal = 0;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif /* MBSM_WANTED */
    UINT4               i4MsgRetVal = 0;
    UINT4               u4LowPriMessageCount = 0;
    tOsixMsg           *pOsixMsg = NULL;
    UINT1               u1HPMsgCnt = 0;
#ifdef BFD_WANTED
    tNeighbor          *pNbr = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tIPADDR             NbrAddr;
#endif
    OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                       &u4LowPriMessageCount);

    while (((i4MsgRetVal = OsixReceiveFromQ ((UINT4) 0, au1QName,
                                             OSIX_NO_WAIT, (UINT4) 0,
                                             &pOsixMsg)) ==
            OSIX_SUCCESS) || (u4LowPriMessageCount != 0))
    {
        pOspfQMsg = (tOspfQMsg *) pOsixMsg;
        if (i4MsgRetVal == OSIX_SUCCESS)
        {

            OSPF_GBL_TRC1 (CONTROL_PLANE_TRC | OSPF_CONFIGURATION_TRC,
                           OSPF_INVALID_CXT_ID, "Rx SNMP Msg %x\n", pOspfQMsg);

            if (UtilOspfIsLowPriorityMessage (pOspfQMsg->u4MsgType)
                == OSPF_TRUE)
            {
                OspfSendMsgToLowPriorityQ (pOspfQMsg);
            }
            else
            {
                u1HPMsgCnt++;
                if (u1HPMsgCnt >= MAX_OSPF_HP_MSGS)
                {

                    if (gOsRtr.u4RTstaggeredCtxId == OSPF_INVALID_CXT_ID)
                    {
                        /* process the low priority messages only when
                         * RTC is not relinquished 
                         */
                        if (u4LowPriMessageCount != 0)
                        {
                            OspfProcessLowPriQMsg ((const UINT1 *) "OLPQ");
                        }
                    }
                    u1HPMsgCnt = 0;
                }
                switch (pOspfQMsg->u4MsgType)
                {

                    case OSPF_RTM_IF_MSG:
                        RtmMsgHandler (pOspfQMsg->pOspfRtmIfParam);

                        break;

                    case OSPF_ROUTEMAP_UPDATE_MSG:
                        OspfProcessRMapHandler (pOspfQMsg->pOspfRMapParam);
                        break;

                    case OSPF_APP_IF_MSG:
                        AppOspfIfHandler (&(pOspfQMsg->appOspfIfParam));
                        break;

                    case OSPF_IP_IF_MSG:
                        OspfProcessIfStateChg (&(pOspfQMsg->ospfIpIfParam));
                        break;

                    case OSPF_IP_PKT_RCVD:
#ifndef RAWSOCK_WANTED
                        IpifDeqPkt (&(pOspfQMsg->ospfPktInfo));
#endif
                        break;
                    case OSPF_VCM_MSG_RCVD:
                        VcmMsgHandler (&(pOspfQMsg->ospfVcmInfo));

                        break;
#ifdef HIGH_PERF_RXMT_LST_WANTED
                    case OSPF_RXMT_NODE_ALLOC_FAIL_MSG:
                        OspfRestartNbrAdjaceny (pOspfQMsg->u4OspfCxtId);
                        break;
#endif /* HIGH_PERF_RXMT_LST_WANTED */

                    case OSPF_RED_IF_MSG:

                        OspfRmProcessRmMsg (&(pOspfQMsg->unOspfMsgIfParam.
                                              ospfRmIfParam));
                        break;
#ifdef BFD_WANTED
                    case OSPF_BFD_NBR_DOWN:

                        if ((pOspfCxt =
                             UtilOspfGetCxt (pOspfQMsg->u4OspfCxtId)) != NULL)
                        {
                            MEMCPY (&NbrAddr,
                                    &pOspfQMsg->OspfBfdMsgInfo.NbrAddr,
                                    sizeof (tIPADDR));
                            /* Passing the AddrLessIf parameter as zero since
                             * path is monitored for Numbered interface.*/
                            if ((pNbr = GetFindNbrInCxt (pOspfCxt,
                                                         (tIPADDR *) NbrAddr,
                                                         OSPF_ZERO)) != NULL)
                            {
                                /* BFD detect the failure in the path, so restarting 
                                 * router is going down, change the helper state 
                                 * as not helping, before calling NbrUpdateState 
                                 * with NBR_DOWN indication*/
                                if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
                                {
                                    pNbr->u1NbrHelperStatus =
                                        OSPF_GR_NOT_HELPING;
                                }
                                NbrUpdateState (pNbr, NBRS_DOWN);
                                if ((pNbr->pInterface->u1NetworkType ==
                                     IF_BROADCAST)
                                    || (pNbr->pInterface->u1NetworkType ==
                                        IF_PTOP))
                                {
                                    NbrDelete (pNbr);
                                }

                            }
                        }
                        break;
#endif
#ifdef MBSM_WANTED
                    case MBSM_MSG_CARD_INSERT:

                        i4ProtoId =
                            pOspfQMsg->unOspfMsgIfParam.MbsmCardUpdate.
                            mbsmProtoMsg.i4ProtoCookie;
                        pSlotInfo =
                            &(pOspfQMsg->unOspfMsgIfParam.MbsmCardUpdate.
                              mbsmProtoMsg.MbsmSlotInfo);
                        i4RetVal = OspfMbsmUpdateCardInsertion (pSlotInfo);
                        MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                        MbsmProtoAckMsg.i4SlotId =
                            MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                        MbsmProtoAckMsg.i4RetStatus = i4RetVal;
                        MbsmSendAckFromProto (&MbsmProtoAckMsg);

                        break;

                    case MBSM_MSG_CARD_REMOVE:

                        i4ProtoId =
                            pOspfQMsg->unOspfMsgIfParam.MbsmCardUpdate.
                            mbsmProtoMsg.i4ProtoCookie;
                        pSlotInfo =
                            &(pOspfQMsg->unOspfMsgIfParam.MbsmCardUpdate.
                              mbsmProtoMsg.MbsmSlotInfo);
                        MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                        MbsmProtoAckMsg.i4SlotId =
                            MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
                        MbsmSendAckFromProto (&MbsmProtoAckMsg);
                        break;
#endif
                    default:
                        break;
                }
            }
            QMSG_FREE (pOspfQMsg);
        }
        else if (u4LowPriMessageCount != 0)
        {
            OspfProcessLowPriQMsg ((const UINT1 *) "OLPQ");
        }

        OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                           &u4LowPriMessageCount);
    }
}

PUBLIC VOID
CpuRelinquishHello ()
{
    TmrHandleHelloExpiry ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfProcessLowPriQMsg                         */
/*                                                                           */
/* Description  : This function gets the packets from the low priority queue */
/*                and processes them. This function is called only when the  */
/*                OSPF queue is empty or the low priority queue is full.     */
/*                Only 5% of the packets present in the low priority queue   */
/*                are processed and only if the OSPF queue is empty, the     */
/*                next 5% of the packets are considered for processing.      */
/*                                                                           */
/* Input        : au1QName: the low priority queue name                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfProcessLowPriQMsg (const UINT1 au1QName[4])
{
    tOspfLowPriQMsg    *pOspfLowPriQMsg = NULL;
    UINT4               u4LowPriMessageCount = 0;
    UINT4               u4IncrCtr = 0;
    tOsixMsg           *pOsixMsg = NULL;

    OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) au1QName,
                       &u4LowPriMessageCount);

    if (gOsRtr.u1RouteCalcCompleted != OSPF_ROUTE_CALC_COMPLETED)
    {
        /* route calculation is suspended 
         * Now processing of low prioirty message can trigger route calc
         * so return without processing packets from low prioirty Q */
        return;

    }

    while (((OsixReceiveFromQ
             ((UINT4) 0, au1QName, OSIX_NO_WAIT, (UINT4) 0,
              &pOsixMsg) == OSIX_SUCCESS)) && (u4IncrCtr <= MAX_OSPF_LP_MSGS))
    {
        pOspfLowPriQMsg = (tOspfLowPriQMsg *) pOsixMsg;
        u4IncrCtr++;

        switch (pOspfLowPriQMsg->u4MsgType)
        {

            case OSPF_IP_IF_MSG:
                OspfProcessIfStateChg (&
                                       (pOspfLowPriQMsg->ospfLowPriQIpIfParam));
                break;

            case OSPF_IP_PKT_RCVD:
                OspfProcessIPPacket (&(pOspfLowPriQMsg->ospfLowPriQPktInfo));
                break;

            case OSPF_VCM_MSG_RCVD:
                VcmMsgHandler (&(pOspfLowPriQMsg->ospfLowPriQVcmInfo));
                break;
            default:
                break;
        }
        PRIORITY_QMSG_FREE (pOspfLowPriQMsg);
    }
    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Send Evt Failed\n");
    }
    CpuRelinquishHello ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfProcessIPPacket                          */
/*                                                                           */
/* Description  : This function processes the packets with respect to        */
/*                thier types                             */
/*                                                                           */
/* Input        : pOspfLowPriPktInfo - the new packet structure                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OspfProcessIPPacket (tOspfLowPriPktInfo * pOspfLowPriPktInfo)
{
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;
    tInterface         *pVirtIface = NULL;
    tNeighbor          *pNbr = NULL;

    if ((pOspfCxt = UtilOspfGetCxt (pOspfLowPriPktInfo->u4OspfCxtId)) == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC |
                  OSPF_INTERFACE_TRC, pOspfLowPriPktInfo->u4OspfCxtId,
                  "OSPF Pkt Disc, Associated Ospf Context Not Found\n");
        UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
        return;
    }
    if ((pInterface = UtilFindIfInCxt (pOspfCxt,
                                       pOspfLowPriPktInfo->u4IfIndex)) == NULL)
    {
        UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
        return;
    }

    if (pOspfLowPriPktInfo->u1NetworkType == IF_VIRTUAL)
    {
        if ((pVirtIface = PppAssociateWithVirtualIf (pInterface,
                                                     (tRouterId *)
                                                     pOspfLowPriPktInfo->
                                                     NbrIp)) == NULL)
        {
            pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
            UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);

            OSPF_TRC (CONTROL_PLANE_TRC,
                      pInterface->pArea->pOspfCxt->u4OspfCxtId,
                      "Pkt Disc -Association With Virt If Fails\n");

            return;
        }
        pInterface = pVirtIface;

    }

    pNbr = HpSearchNbrLst (&pOspfLowPriPktInfo->NbrIp, pInterface);

    if (pNbr == NULL)
    {
        pInterface->pArea->pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_PPP_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc (Could Not be Associated With Any Nbr)\n");
        return;
    }

    switch (pOspfLowPriPktInfo->u1PktType)
    {

        case DD_PKT:
            COUNTER_OP (pNbr->pInterface->u4DdpRcvdCount, 1);
            DdpRcvDdp (pOspfLowPriPktInfo->pPkt, pOspfLowPriPktInfo->u2PktLen,
                       pNbr);
            UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
            break;

        case LSA_REQ_PKT:
            COUNTER_OP (pNbr->pInterface->u4LsaReqRcvdCount, 1);
            LrqRcvLsaReq (pOspfLowPriPktInfo->pPkt,
                          pOspfLowPriPktInfo->u2PktLen, pNbr);
            UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
            break;

        case LS_UPDATE_PKT:
            COUNTER_OP (pNbr->pInterface->u4LsaUpdateRcvdCount, 1);
            LsuRcvLsUpdate (pOspfLowPriPktInfo->pPkt, pNbr);
            UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
            break;

        default:
            UtilOsMsgFree (pOspfLowPriPktInfo->pPkt, NORMAL_RELEASE);
            break;
    }

}

PUBLIC VOID
OspfSendMsgToLowPriorityQ (tOspfQMsg * pOspfQMsg)
{
    tOspfLowPriQMsg    *pOspfLowPriQMsg = NULL;
    UINT4               u4PktCount = 0;

    if (PRIORITY_QMSG_ALLOC (pOspfLowPriQMsg) == NULL)
    {
        return;
    }

    if (pOspfQMsg->u4MsgType == OSPF_IP_IF_MSG)
    {
        pOspfLowPriQMsg->u4MsgType = OSPF_IP_IF_MSG;
        pOspfLowPriQMsg->ospfLowPriQIpIfParam.u4BitMap =
            pOspfQMsg->ospfIpIfParam.u4BitMap;

        OS_MEM_CPY (&(pOspfLowPriQMsg->ospfLowPriQIpIfParam.OspfNetIpIfInfo),
                    &(pOspfQMsg->ospfIpIfParam.OspfNetIpIfInfo),
                    sizeof (tNetIpv4IfInfo));
    }
    else if (pOspfQMsg->u4MsgType == OSPF_VCM_MSG_RCVD)
    {
        pOspfLowPriQMsg->u4MsgType = OSPF_VCM_MSG_RCVD;
        pOspfLowPriQMsg->ospfLowPriQVcmInfo.u4IpIfIndex =
            pOspfQMsg->ospfVcmInfo.u4IpIfIndex;
        pOspfLowPriQMsg->ospfLowPriQVcmInfo.u4VcmCxtId =
            pOspfQMsg->ospfVcmInfo.u4VcmCxtId;
        pOspfLowPriQMsg->ospfLowPriQVcmInfo.u1BitMap =
            pOspfQMsg->ospfVcmInfo.u1BitMap;
    }
    else
    {
        /* Invalid message type */
        PRIORITY_QMSG_FREE (pOspfLowPriQMsg);
        return;
    }

    if ((OsixSendToQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                      (tOsixMsg *) (VOID *) pOspfLowPriQMsg,
                      OSIX_MSG_NORMAL)) != OSIX_SUCCESS)
    {
        PRIORITY_QMSG_FREE (pOspfLowPriQMsg);
        return;
    }

    OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ", &u4PktCount);

    /* To support from system.size , the macro OSPF_LOW_PRIORITY_QUEUE_SIZE
     * is replaced with sizing variable
     * FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].u4PreAllocatedUnits/2
     */
    if (u4PktCount ==
        (FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].u4PreAllocatedUnits /
         2))
    {
        OspfProcessLowPriQMsg ((const UINT1 *) "OLPQ");
    }
}

PUBLIC INT4
OspfRtmCallbackFn (tRtmRespInfo * pRespInfo, tRtmMsgHdr * pRtmHdr)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tOspfQMsg          *pOspfQMsg;
    tExpRtNode         *pRtmRouteMsg = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    tInterface         *pInterface = NULL;
    UINT4               u4IpAddr = 0;
    UINT4               u4NetMask = 0;
    tOspfCxt           *pOspfCxt = NULL;
    if (pRtmHdr->u1MessageType == RTM_REGISTRATION_ACK_MESSAGE)
    {
        if ((pBuf =
             CRU_BUF_Allocate_MsgBufChain ((UINT4) sizeof (tRtmRegnAckMsg),
                                           0)) == NULL)
        {
            return OSPF_FAILURE;
        }
        MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pBuf, "OspfRtmbkFn");
        if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pRespInfo->pRegnAck, 0,
                                       (UINT4) sizeof (tRtmRegnAckMsg)) ==
            CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return OSPF_FAILURE;
        }

    }
    else if (pRtmHdr->u1MessageType == RTM_ROUTE_UPDATE_ACK_MESSAGE)
    {
        return OspfRtmRouteUptAckMsg (pRespInfo);

    }
    else
    {
        if ((pOspfCxt =
             UtilOspfGetCxt (pRespInfo->pRtInfo->u4ContextId)) == NULL)
        {
            return (OSPF_FAILURE);
        }

        TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pIfNode, tTMO_SLL_NODE *)
        {
            /* Ospf should not redistribute ospf enabled interfaces 
             * routes in External LSAs list */
            pInterface = GET_IF_PTR_FROM_SORT_LST (pIfNode);
            u4IpAddr = OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr);
            u4NetMask = OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddrMask);
            if (pRespInfo->pRtInfo->u4DestNet == (u4IpAddr & u4NetMask))
            {
                return (OSPF_SUCCESS);
            }
        }

        if (RTM_ROUTE_ALLOC (pRtmRouteMsg) == NULL)
        {
            return (OSPF_FAILURE);
        }
        MEMCPY (&(pRtmRouteMsg->RtInfo), pRespInfo->pRtInfo,
                sizeof (tNetIpv4RtInfo));

        /* Take the DataLock for accessing RTM Route List */
        OsixSemTake (gOsRtr.RtmLstSemId);
        TMO_SLL_Add (&gOsRtr.RtmRtLst, &(pRtmRouteMsg->NextRt));
        /* Release the DataLock for accessing RTM Route List */
        OsixSemGive (gOsRtr.RtmLstSemId);

        if (OsixSendEvent (SELF, (const UINT1 *) "OSPF",
                           OSPF_RTM_RTMSG_EVENT) != OSIX_SUCCESS)
        {
            OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                          "Send Evt Failed\n");
        }
        return (OSPF_SUCCESS);
    }
    /* If OSPF is shutdown, do not allocate message from OSPF queue as queue id
     * will be 0 which will result in crash. To handle this, explicit check for
     * queue id is made here. RTM Deregister is purposefully avoided during GR.
     * Hence, that flow is not disturbed */

    if (QMSG_QID == 0)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (OSPF_FAILURE);
    }

    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (OSPF_FAILURE);
    }

    MEMCPY (IP_GET_MODULE_DATA_PTR (pBuf), pRtmHdr, sizeof (tRtmMsgHdr));

    pOspfQMsg->u4MsgType = OSPF_RTM_IF_MSG;
    pOspfQMsg->pOspfRtmIfParam = pBuf;

    if (OsixSendToQ ((UINT4) 0, (const UINT1 *) "OSPQ",
                     (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        QMSG_FREE (pOspfQMsg);
        return (OSPF_FAILURE);
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Send Evt Failed\n");
    }

    return (OSPF_SUCCESS);
}

PUBLIC INT4
OspfLock (VOID)
{
    if (OsixSemTake (gOspfSemId) != OSIX_SUCCESS)
    {
        OSPF_GBL_TRC1 (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                       "TakeSem failure for %s \n", OSPF_MUT_EXCL_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

PUBLIC INT4
OspfUnLock (VOID)
{
    OsixSemGive (gOspfSemId);
    return SNMP_SUCCESS;
}

#ifdef HIGH_PERF_RXMT_LST_WANTED
PRIVATE VOID
OspfRxmtNodeMemInit (VOID)
{
    UINT4               u4RxmtIndex;
    tRxmtNode          *pRxmtNode;

    /* Initialise the list of free rxmtnode memory */
    TMO_DLL_Init (&(gOsRtr.rxmtNodeFreeMemLst));
    for (u4RxmtIndex = 0; u4RxmtIndex < MAX_RXMT_NODES; u4RxmtIndex++)
    {
        pRxmtNode = &(gOsRtr.aRxmtNode[u4RxmtIndex]);
        OS_MEM_SET (pRxmtNode, 0, sizeof (tRxmtNode));
        /* Add the Rxmtnode memory to the list of free memory */
        TMO_DLL_Add (&(gOsRtr.rxmtNodeFreeMemLst), (tTMO_DLL_NODE *) pRxmtNode);
    }
}
PRIVATE VOID
OspfRestartNbrAdjaceny (UINT4 u4OspfCxtId)
{
    tOspfCxt           *pOspfCxt;
    tTMO_SLL_NODE      *pLstNode;
    tNeighbor          *pNbr;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pNbrNode;

    if ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL)
    {
        return;
    }
    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            NsmRestartAdj (pNbr);
        }
    }

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
            NsmRestartAdj (pNbr);
        }
    }
}
#endif /* HIGH_PERF_RXMT_LST_WANTED */

#ifdef ROUTEMAP_WANTED
/*****************************************************************************/
/* Function     : OspfSendRouteMapUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module, this function posts an event to OSPF     */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS if Route Map update Msg Sent to OSPF          */
/*                successfully                                               */
/*                OSPF_FAILURE otherwise                                     */
/*****************************************************************************/
PUBLIC INT4
OspfSendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    tOsixMsg           *pBuf;
    tOspfQMsg          *pOspfQMsg;
    INT4                i4OutCome;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + 4];

    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status);    /* message size is hdr+ size of
                                                     * RouteMap Name+Status */

    if (IssGetModuleSystemControl (OSPF_MODULE_ID) == MODULE_SHUTDOWN)
    {
        return OSPF_FAILURE;
    }

    if (pu1RMapName == NULL)
    {
        return OSPF_FAILURE;
    }

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    STRNCPY (au1NameBuf, pu1RMapName, (sizeof (au1NameBuf) - 1));

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, 0);
    if (pBuf == NULL)
    {
        return OSPF_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "OspfSndRtMsg");
    /*** copy status to offset 0 ***/
    if ((i4OutCome = CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Status, 0,
                                                sizeof (u4Status))) !=
        CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSPF_FAILURE;
    }

    /*** copy map name to offset 4 ***/
    if ((i4OutCome =
         CRU_BUF_Copy_OverBufChain (pBuf, au1NameBuf, sizeof (u4Status),
                                    RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSPF_FAILURE;
    }

    /*** post to OSPF queue ***/

    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSPF_FAILURE;
    }

    pOspfQMsg->u4MsgType = OSPF_ROUTEMAP_UPDATE_MSG;
    pOspfQMsg->pOspfRMapParam = pBuf;

    if (OsixSendToQ ((UINT4) 0, (const UINT1 *) "OSPQ",
                     (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        QMSG_FREE (pOspfQMsg);
        return OSPF_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Send Evt Failed\n");
    }

    return i4OutCome;
}
#endif /*ROUTEMAP_WANTED */

/*****************************************************************************/
/* Function     : OspfProcessRMapHandler                                     */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module.                                          */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
OspfProcessRMapHandler (tOsixMsg * pBuf)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status = 0;
    UINT1               u1Status = FILTERNIG_STAT_DEFAULT;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2Index = 0;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (pBuf == NULL)
    {
        return;
    }

    if (gOsRtr.u4RTstaggeredCtxId != OSPF_INVALID_CXT_ID)
    {
        /* This means that route map progress is relinquished in some context.
         * So don't process the routemap message now. Defer the route
         * map processing by reposting the event to Queue, Since it was already
         * dequeued.
         */
        for (; u2Index < MAX_OSPF_RMAP_MSGS; u2Index++)
        {
            if (gOsRmapMsg[u2Index] == NULL)
            {
                gOsRmapMsg[u2Index] = pBuf;
                break;
            }
        }
        return;
    }

    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) &u4Status, 0, sizeof (u4Status)) != sizeof (u4Status))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN + 4));
    if (CRU_BUF_Copy_FromBufChain (pBuf, au1RMapName, sizeof (u4Status),
                                   RMAP_MAX_NAME_LEN) != RMAP_MAX_NAME_LEN)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (u4Status != 0)
    {
        u1Status = FILTERNIG_STAT_ENABLE;
    }
    else
    {
        u1Status = FILTERNIG_STAT_DISABLE;
    }

    /* Scan through all the context and change the status */
    for (u4ContextId = 0; u4ContextId < u4MaxCxt; u4ContextId++)
    {
        if (gOsRtr.apOspfCxt[u4ContextId] == NULL)
        {
            /* Get the next context id */
            continue;
        }
        pOspfCxt = gOsRtr.apOspfCxt[u4ContextId];

        if (pOspfCxt->pDistributeInFilterRMap != NULL)
        {
            if (STRCMP
                (pOspfCxt->pDistributeInFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pOspfCxt->pDistributeInFilterRMap->u1Status = u1Status;
            }
        }

        if (pOspfCxt->pDistanceFilterRMap != NULL)
        {
            if (STRCMP
                (pOspfCxt->pDistanceFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pOspfCxt->pDistanceFilterRMap->u1Status = u1Status;
            }
        }
        /* Apply Rmap Rules for dropped route enries */
        UtilOspfApplyRMapRule (pOspfCxt);
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
}

VOID
OspfFreeMsgsInQueue ()
{
    tOspfQMsg          *pOspfQMsg = NULL;
    tOspfLowPriQMsg    *pOspfLowPriQMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;
    const UINT1         au1Qname[] = "OSPQ";
    const UINT1         au1LowQname[] = "OLPQ";

    while (OsixReceiveFromQ ((UINT4) 0, au1Qname, OSIX_NO_WAIT,
                             (UINT4) 0, &pOsixMsg) == OSIX_SUCCESS)
    {

        pOspfQMsg = (tOspfQMsg *) pOsixMsg;

        switch (pOspfQMsg->u4MsgType)
        {
            case OSPF_RTM_IF_MSG:
                CRU_BUF_Release_MsgBufChain (pOspfQMsg->pOspfRtmIfParam, 0);

                break;

            case OSPF_ROUTEMAP_UPDATE_MSG:
                CRU_BUF_Release_MsgBufChain (pOspfQMsg->pOspfRMapParam, FALSE);
                break;

            case OSPF_IP_PKT_RCVD:
#ifndef RAWSOCK_WANTED
                UtilOsMsgFree (pOspfQMsg->ospfPktInfo.pPkt, NORMAL_RELEASE);
#endif
                break;

            case OSPF_RED_IF_MSG:
                OspfRmProcessRmMsg (&(pOspfQMsg->unOspfMsgIfParam.
                                      ospfRmIfParam));
                if (pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.u1Event
                    == RM_MESSAGE)
                {
                    RM_FREE (pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.pFrame);
                }
                else if ((pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.u1Event ==
                          RM_PEER_UP) ||
                         (pOspfQMsg->unOspfMsgIfParam.ospfRmIfParam.u1Event ==
                          RM_PEER_DOWN))
                {
                    OspfRmRelRmMsgMem ((UINT1 *) pOspfQMsg->unOspfMsgIfParam.
                                       ospfRmIfParam.pFrame);
                }
                break;

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_REMOVE:
            case MBSM_MSG_CARD_INSERT:
#endif
            default:
                break;
        }

        QMSG_FREE (pOspfQMsg);
    }

    while (OsixReceiveFromQ ((UINT4) 0, au1LowQname, OSIX_NO_WAIT,
                             (UINT4) 0, &pOsixMsg) == OSIX_SUCCESS)
    {
        pOspfLowPriQMsg = (tOspfLowPriQMsg *) pOsixMsg;

        if (pOspfLowPriQMsg->u4MsgType == OSPF_IP_PKT_RCVD)
        {
            UtilOsMsgFree (pOspfLowPriQMsg->ospfLowPriQPktInfo.pPkt,
                           NORMAL_RELEASE);
        }
        PRIORITY_QMSG_FREE (pOspfLowPriQMsg);
    }
}

PUBLIC INT4
OspfShutdown ()
{
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    OspfLock ();

    OspfFreeMsgsInQueue ();

    /* Delete all context */
    for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
    {
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

        if (pOspfCxt != NULL)
        {
            GrDeleteCxt (gOsRtr.apOspfCxt[u4OspfCxtId]);
        }
    }

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
    UnRegisterOSPFMibs ();
#endif

    TMO_SLL_Init (&(gOsRtr.RtmRtLst));

#ifdef HIGH_PERF_RXMT_LST_WANTED
    RBTreeDelete (gOsRtr.pRxmtLstRBRoot);
    gOsRtr.pRxmtLstRBRoot = NULL;
#endif
    RBTreeDelete (gOsRtr.pRtrLsaCheck);
    RBTreeDelete (gOsRtr.pNwLsaCheck);
    OspfRmDeRegisterWithRM ();
    VcmDeRegisterHLProtocol (OSPF_PROTOCOL_ID);
    /* Stop all the global timers and delete the timer list */
    TmrStopTimer (gOnesecTimerLst, &(gOnesecTimerNode));
    TmrDeleteTimerList (gTimerLst);
    TmrDeleteTimerList (gOnesecTimerLst);

    /* Delete the interface hash table */
    TMO_HASH_Delete_Table (gOsRtr.pIfHashTable, NULL);

    /* Clear all the memory */
    OspfMemClear ();

    OspfUnLock ();

    return OSIX_SUCCESS;
}

INT4
OspfInit ()
{
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u41sTicks = 1 * NO_OF_TICKS_PER_SEC;
    INT4                i4RetVal = OSPF_FAILURE;
    tVcmRegInfo         VcmRegInfo;

    OspfLock ();

    /* Initialize the temporary RTM Route List */
    TMO_SLL_Init (&(gOsRtr.RtmRtLst));

    gOsRtr.u1RouteCalcCompleted = OSPF_ROUTE_CALC_COMPLETED;
    gOsRtr.u4VrfSpfInterval = OSPF_DEF_VRF_SPF_INTERVAL;

#ifdef HIGH_PERF_RXMT_LST_WANTED
    OspfRxmtNodeMemInit ();

    gOsRtr.pRxmtLstRBRoot = RBTreeCreateEmbedded (OSPF_OFFSET (tRxmtNode,
                                                               RbNode),
                                                  RbCompareRxmtInfo);
    if (gOsRtr.pRxmtLstRBRoot == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      "Creation of RBTree for Retransmission List For LSAS Failed. \n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }
#endif /* HIGH_PERF_RXMT_LST_WANTED */
    gOsRtr.pRtrLsaCheck =
        RBTreeCreateEmbedded (OSPF_OFFSET (tRtrLsaRtInfo, RbNode),
                              GrCompareRouterLsaEntry);
    if (gOsRtr.pRtrLsaCheck == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "RtrLsaRtInfo RB Tree Creation Failed\n");
        return OSPF_FAILURE;
    }

    gOsRtr.pNwLsaCheck =
        RBTreeCreateEmbedded (OSPF_OFFSET (tNtLsaRtInfo, RbNode),
                              GrCompareNetworkLsaEntry);
    if (gOsRtr.pNwLsaCheck == NULL)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "NwLsaRtInfo RB Tree Creation Failed\n");
        return OSPF_FAILURE;
    }

#ifdef TRACE_WANTED
    /*OSPF_TRC_FLAG = 0; */
#endif

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
    RegisterOSPFMibs ();
#endif

    /* Create buffer pools for data structures */
    if (OspfMemInit () == OSPF_FAILURE)
    {
        OSPF_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                      "Memory Pool Creation Failed\n");
        OspfMemClear ();
        OspfUnLock ();
        return OSIX_FAILURE;
    }

    /* The following routine initialises the timer descriptor structure */
    TmrInitTimerDesc ();

#ifdef DEBUG_WANTED
    gp_ospf_output_file = stdout;
#endif

    gOsRtr.bIgnoreExpiredTimers = OSPF_FALSE;
    gOsRtr.u4MaxMtuSize = 0;
    gOsRtr.u4AsExtRouteAggrCnt = 0;
    gOsRtr.u4RTstaggeredCtxId = OSPF_INVALID_CXT_ID;
    gOsRtr.u4RTStaggeringStatus = OSPF_STAGGERING_ENABLED;

    /* Associate the global timer list with the Event */
    if (TmrCreateTimerList ((CONST UINT1 *) OSPF_TASK_NAME, OSPF_TIMER_EVENT,
                            NULL,
                            (tTimerListId *) & (gTimerLst)) != TMR_SUCCESS)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Tmr Lst Creation Failed\n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }
    /* Create 1 Sec Timer here, associate TMO1_TIMER_01_EXP_EVENT with it */
    if (TmrCreateTimerList
        ((CONST UINT1 *) OSPF_TASK_NAME, TMO1_TIMER_01_EXP_EVENT, NULL,
         (tTimerListId *) & (gOnesecTimerLst)) != TMR_SUCCESS)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Creation of 1 Sec Tmr Failed\n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }
    if (TmrCreateTimerList
        ((CONST UINT1 *) OSPF_TASK_NAME, HELLO_TIMER_EXP_EVENT, NULL,
         (tTimerListId *) & (gHelloTimerLst)) != TMR_SUCCESS)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Creation of 1 Sec Tmr Failed\n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }

    if (TmrStartTimer (gOnesecTimerLst, &(gOnesecTimerNode),
                       (UINT4) u41sTicks) != TMR_SUCCESS)
    {

        OSPF_GBL_TRC (CONTROL_PLANE_TRC | OSPF_RTMODULE_TRC,
                      OSPF_INVALID_CXT_ID, "Failed to start 1 Sec Tmr \n");
        TmrDeleteTimerList (gOnesecTimerLst);
        TmrDeleteTimerList (gTimerLst);
        OsixDeleteSem (OSIX_TRUE, (const UINT1 *) "RTSM");
        OsixDeleteSem (OSIX_TRUE, (const UINT1 *) "CXTSM");
        OspfUnLock ();
        return OSIX_FAILURE;
    }

    gOspf1sTimer = 0;

    gOsRtr.pIfHashTable = TMO_HASH_Create_Table (IF_HASH_TABLE_SIZE,
                                                 NULL, FALSE);

    TMO_DLL_Init (&(gOsRtr.vrfSpfRtcList));    /* vrf spf list */

    gOsRtr.u4ActiveCxtCount = 0;

    /* Initializes the ism schedule queue */
    IsmInitSchedQueue ();

    if (RtrCreateCxt (OSPF_DEFAULT_CXT_ID, OSPF_DISABLED) == OSPF_FAILURE)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! TASK INIT FAILURE  !!!!! \n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }
    if ((pOspfCxt = UtilOspfGetCxt (OSPF_DEFAULT_CXT_ID)) == NULL)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! TASK INIT FAILURE  !!!!! \n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }

    pOspfCxt->contextStatus = ACTIVE;

#ifndef MI_WANTED
    gOsRtr.pOspfCxt = pOspfCxt;
    gOsRtr.u4OspfCxtId = OSPF_DEFAULT_CXT_ID;
#endif
    IssSetModuleSystemControl (OSPF_MODULE_ID, MODULE_START);

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_OSPF2, OspfSendRouteMapUpdateMsg);
#endif

    OspfRmInit ();

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
    if (OspfCreateNLHelloSocket () == OSPF_FAILURE)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! TASK INIT FAILURE  !!!!! \n");
        OSPF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif
#endif

    i4RetVal = OspfRmRegisterWithRM ();

    if (i4RetVal != OSPF_SUCCESS)
    {
        OSPF_GBL_TRC (OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                      " !!!!! OSPF registration with RM module failed  !!!!!\n");
        OspfUnLock ();
        return OSIX_FAILURE;
    }

    /* Register With VCM Module */
    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = OspfVcmCallbackFn;
    VcmRegInfo.u1InfoMask |= (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ);
    VcmRegInfo.u1ProtoId = OSPF_PROTOCOL_ID;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, OSPF_INVALID_CXT_ID,
                  "VCM registration failed \n");
        OspfUnLock ();
        return OSPF_FAILURE;

    }

    OspfUnLock ();
    return OSIX_SUCCESS;
}

PUBLIC INT4
OspfShutdownProtocol ()
{
    UINT4               u4OspfCxtId = OSPF_INVALID_CXT_ID;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    OspfLock ();

    OspfFreeMsgsInQueue ();

    /* Delete all context */
    for (u4OspfCxtId = 0; u4OspfCxtId < u4MaxCxt; u4OspfCxtId++)
    {
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

        if (pOspfCxt != NULL)
        {
            RtrDeleteCxt (pOspfCxt);
        }
    }
    if (gOsRtr.apOspfCxt[OSPF_DEFAULT_CXT_ID] != NULL)
    {
        RtrDeleteCxt (gOsRtr.apOspfCxt[OSPF_DEFAULT_CXT_ID]);
    }

    OspfRmDeRegisterWithRM ();
    VcmDeRegisterHLProtocol (OSPF_PROTOCOL_ID);
    /* Stop all the global timers and delete the timer list */
    TmrStopTimer (gOnesecTimerLst, &(gOnesecTimerNode));
    TmrDeleteTimerList (gTimerLst);
    OspfUnLock ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfUpdateGrCsrStatus                                      */
/*                                                                           */
/* Description  : This function is to update the OSPF GR CSR status          */
/*               (Config Save Restoration status)                            */
/*                                                                           */
/* Input        : u1GrStatus - IN_PROGRESS/COMPLETED                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfUpdateGrCsrStatus (UINT1 u1GrStatus)
{
    OspfLock ();
    if (u1GrStatus == GR_CSR_IN_PROGRESS)
    {
        gOsRtr.b1GrCsrStatus = TRUE;
    }
    else
    {
        gOsRtr.b1GrCsrStatus = FALSE;
    }
    OspfUnLock ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRtmRouteUptAckMsg                                      */
/*                                                                           */
/* Description  : This procedure gets the Route Update Ack from RTM          */
/*                                                                           */
/* Input        : pRespInfo -   ptr to the RTM message buffer                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_SUCCESS, if processing is successful                   */
/*                RTM_FAILURE, otherwise                                     */
/*****************************************************************************/
INT1
OspfRtmRouteUptAckMsg (tRtmRespInfo * pRespInfo)
{
    tNetIpv4RtInfo     *pNetIpRtInfo = NULL;
    CHR1               *pu1DstIpAddr = NULL;

    pNetIpRtInfo = pRespInfo->pRtInfo;

    CLI_CONVERT_IPADDR_TO_STR (pu1DstIpAddr, pNetIpRtInfo->u4DestNet);

    if (pRespInfo->pRegnAck->i4Status == IP_SUCCESS)
    {
        OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC | OSPF_ADJACENCY_TRC,
                   pRespInfo->pRtInfo->u4ContextId,
                   "Route --> %s successfully updated in RTM\n", pu1DstIpAddr);

    }
    else if (pRespInfo->pRegnAck->i4Status == IP_FAILURE)
    {
        OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC | OSPF_ADJACENCY_TRC,
                   pRespInfo->pRtInfo->u4ContextId,
                   "Route --> %s are not added to RTM  \n", pu1DstIpAddr);
    }
    else if (pRespInfo->pRegnAck->i4Status == IP_SUCCESS_NOT_ADD_TO_HW)
    {
        OSPF_TRC1 (OSPF_RT_TRC | OSPF_RTMODULE_TRC | OSPF_ADJACENCY_TRC,
                   pRespInfo->pRtInfo->u4ContextId,
                   "Route --> %s are not added to HW  \n", pu1DstIpAddr);
    }

    return RTM_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OspfMemReleaseMemBlock                            */
/*  Description     : Stops any stray timer and releases a block        */
/*                    back to a specified Pool/Heap                     */
/*  Input(s)        :                                                   */
/*                  : PoolId   - Pool to which to be released.          */
/*  Output(s)       : pu1Block - Pointer to block being released.       */
/*  Returns         : MEM_SUCCESS/MEM_FAILURE                           */
/************************************************************************/
UINT4
OspfMemReleaseMemBlock (tMemPoolId PoolId, UINT1 *pu1Block)
{
    if (pu1Block == NULL)
    {
        gu4OspfMemReleaseMemBlockFail++;
        return MEM_FAILURE;
    }

    /* Takes care of any stray timers before releasing the actual memory */
    if (PoolId == AREA_QID)
    {
        tArea              *pArea = (tArea *) (VOID *) pu1Block;
        TmrDeleteTimer (&(pArea->nssaStbltyIntrvlTmr));
    }
    else if (PoolId == CONTEXT_QID)
    {
        tOspfCxt           *pOspfCxt = (tOspfCxt *) (VOID *) pu1Block;
        TmrDeleteTimer (&(pOspfCxt->runRtTimer));
        TmrDeleteTimer (&(pOspfCxt->trapLimitTimer));
#ifdef DEBUG_WANTED
        TmrDeleteTimer (&(pOspfCxt->dumpTimer));
#endif
        TmrDeleteTimer (&(pOspfCxt->exitOverflowTimer));
        TmrDeleteTimer (&(pOspfCxt->graceTimer));
        TmrDeleteTimer (&(pOspfCxt->distanceTimer));
    }
    else if (PoolId == IF_QID)
    {
        tInterface         *pInterface = (tInterface *) (VOID *) pu1Block;
        TmrDeleteTimer (&(pInterface->helloTimer));
        TmrDeleteTimer (&(pInterface->waitTimer));
        TmrDeleteTimer (&(pInterface->pollTimer));
        TmrDeleteTimer (&(pInterface->delLsAck.delAckTimer));
    }
    else if (PoolId == LSA_DESC_QID)
    {
        tLsaDesc           *pLsaDesc = (tLsaDesc *) (VOID *) pu1Block;
        TmrDeleteTimer (&(pLsaDesc->minLsaIntervalTimer));
    }
    else if (PoolId == LSA_INFO_QID)
    {
        tLsaInfo           *pLsaInfo = (tLsaInfo *) (VOID *) pu1Block;
        TmrDeleteTimer (&(pLsaInfo->lsaAgingTimer));
        TmrDeleteTimer (&(pLsaInfo->dnaLsaSplAgingTimer));
    }
    else if (PoolId == NBR_QID)
    {
        tNeighbor          *pNeighbor = (tNeighbor *) (VOID *) pu1Block;
        TmrDeleteTimer (&(pNeighbor->inactivityTimer));
        TmrDeleteTimer (&(pNeighbor->helperGraceTimer));
        TmrDeleteTimer (&(pNeighbor->lsaRxmtDesc.lsaRxmtTimer));
        TmrDeleteTimer (&(pNeighbor->lsaReqDesc.lsaReqRxmtTimer));
        TmrDeleteTimer (&(pNeighbor->dbSummary.ddTimer));
    }

    return (MemReleaseMemBlock (PoolId, pu1Block));
}

#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file  osmain.c                       */
/*-----------------------------------------------------------------------*/
