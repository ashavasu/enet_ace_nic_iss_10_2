/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osrmtxad.c,v 1.10 2017/09/21 13:48:47 siva Exp $
 *
 * Description: This file contains definitions related to
 *        bulk update for High Availability
 *
 *******************************************************************/
#ifndef _OSREDBLK_
#define _OSREDBLK_

#include "osinc.h"

PRIVATE UINT4       OspfRmGetHelloLength (tNeighbor * pNbr);
PRIVATE UINT4       OspfRmGetNbrLength (tNeighbor * pNbr);
PRIVATE tInterface *OspfRmGetLastBulkUpdateInterface (VOID);
PRIVATE tInterface *OspfRmGetLastBulkUpdateVirtIntf (VOID);
PRIVATE VOID        OspfRmConstructNbrState (tRmMsg * pMsg,
                                             UINT4 *pu4Offset,
                                             tNeighbor * pNbr);

/***************************************************************/
/*  Function Name   :OspfRmGetHelloLength                      */
/*  Description     :This function returns the Hello           */
/*                   message length                            */
/*  Input(s)        :pNbr - Neighbor                           */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         : Hello message length                     */
/***************************************************************/
UINT4
OspfRmGetHelloLength (tNeighbor * pNbr)
{
    return (pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen +
            OSPF_RED_SYN_HELLO_HDR_SIZE);
}

/***************************************************************/
/*  Function Name   :OspfRmGetNbrLength                        */
/*  Description     :This function is invoked from             */
/*                   OspfRmSendBulkNbrState to get the Nbr     */
/*                   message length                            */
/*  Input(s)        :pNbr - Neighbor                           */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Returns         : Nbr message length                       */
/***************************************************************/
UINT4
OspfRmGetNbrLength (tNeighbor * pNbr)
{
    UINT4               u4NbrMsgLen = 0;

    u4NbrMsgLen = OSPF_RED_BULK_SYN_NBR_FIXED_SIZE;

    if (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
    {
        u4NbrMsgLen += MAX_IP_ADDR_LEN;
    }

    if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
    {
        u4NbrMsgLen += sizeof (UINT4);
    }
    return u4NbrMsgLen;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmConstructHello                                       */
/*                                                                           */
/* Description  : This function is used to construct the hello message       */
/*                                                                           */
/*                                                                           */
/* Input        :                                                            */
/*               pu4Offset  - Offset from which message has to be constructed*/
/*               pNbr       - pointer to Neighbor                            */
/*                                                                           */
/*                                                                           */
/* Output       :                                                            */
/*                pMsg       - Pointer to Output Message                     */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

VOID
OspfRmConstructHello (tRmMsg * pMsg, UINT4 *pu4Offset, tNeighbor * pNbr)
{
    UINT4               u4Offset = *pu4Offset;

    /* Hello Packet info Format */
    /* --------------------------------------------
       | Neighbor router Id (4 bytes)             | 
       --------------------------------------------
       | Hello Packet Source IP Address (4 bytes) |
       --------------------------------------------
       | Interface Address Less IfIndex (4 bytes) |
       --------------------------------------------
       | Interface IP Address (4 bytes)           | 
       --------------------------------------------
       | Context Id (4 bytes)                     |
       --------------------------------------------
       | Hello Packet Length (2 bytes)            |
       --------------------------------------------
       | Hello Packet (Hello Packet Length)       |
       --------------------------------------------
     */

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->nbrId, u4Offset, MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg,
                                pNbr->lastRcvHello.
                                helloPktHdr.SrcIpAddr,
                                u4Offset, MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                pNbr->lastRcvHello.helloPktHdr.u4AddrlessIf);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg,
                                pNbr->lastRcvHello.
                                helloPktHdr.IfIpAddr,
                                u4Offset, MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                pNbr->lastRcvHello.helloPktHdr.u4ContextId);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_2_BYTE (pMsg, u4Offset,
                                pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen);
    u4Offset = u4Offset + sizeof (UINT2);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg,
                                pNbr->lastRcvHello.au1Pkt, u4Offset,
                                pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen);
    u4Offset = u4Offset + pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen;

    if ((pNbr->pInterface->u2AuthType == CRYPT_AUTHENTICATION) &&
        (pNbr->pInterface->u4CryptoAuthType == OSPF_AUTH_MD5))
    {
        OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                    pNbr->pInterface->u4CryptSeqNum);

        u4Offset = u4Offset + sizeof (UINT4);

    }
    *pu4Offset = u4Offset;

    gOsRtr.osRedInfo.u4HelloSyncCount++;
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmConstructNbrState                                    */
/*                                                                           */
/* Description  : This function is used to construct the Nbr State message   */
/*                                                                           */
/*                                                                           */
/* Input        :                                                            */
/*               pu4Offset  - Offset from which message has to be constructed*/
/*               pNbr       - pointer to Neighbor                            */
/*                                                                           */
/*                                                                           */
/* Output       :                                                            */
/*                pMsg       - Pointer to Output Message                     */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmConstructNbrState (tRmMsg * pMsg, UINT4 *pu4Offset, tNeighbor * pNbr)
{
    UINT4               u4DeadTime = 0;
    UINT4               u4Offset = *pu4Offset;

    /* Neighbor State info bulk update format */
    /* -----------------------------------------------------------------------
       | Sub Bulk Update Type : OSPF_RED_SYNC_NBR_STATE_MSG (1 byte)         |
       -----------------------------------------------------------------------
       | Context Id (4 bytes)                                                |
       -----------------------------------------------------------------------
       | Interface Address Less IfIndex (4 bytes)                            |
       -----------------------------------------------------------------------
       | Interface IP Address (4 bytes)                                      | 
       -----------------------------------------------------------------------
       | neighbor options     (1 bytes)                                      | 
       -----------------------------------------------------------------------
       | Nbr(IP Addr or Router Id) based on Network Type (4 byte)            |
       -----------------------------------------------------------------------
       | Network Type (broadcast/P2P/P2MP/NBMA/Virtual Interface)(1 byte)    | 
       -----------------------------------------------------------------------
       | Transit AreaId (only when Network tpe is Virtual Interface)(4 byte) | 
       -----------------------------------------------------------------------
       | Neighbor State (NSM state) (1 byte)                                 |
       -----------------------------------------------------------------------
       | Neighbor GR Helper State (1 byte)                                   | 
       -----------------------------------------------------------------------
       | Remaining Grace Time (Only when neighbor is GR Helper) (4 bytes)    |
       -----------------------------------------------------------------------
     */
    /* Fill the Sub Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, OSPF_RED_SYNC_NBR_STATE_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                pNbr->pInterface->pArea->pOspfCxt->u4OspfCxtId);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pNbr->pInterface->u4AddrlessIf);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->pInterface->ifIpAddr,
                                u4Offset, MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pNbr->nbrOptions);
    u4Offset = u4Offset + sizeof (UINT1);

    if ((pNbr->pInterface->u1NetworkType == IF_BROADCAST) ||
        (pNbr->pInterface->u1NetworkType == IF_NBMA) ||
        (pNbr->pInterface->u1NetworkType == IF_PTOMP))
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->nbrIpAddr, u4Offset,
                                    MAX_IP_ADDR_LEN);
    }
    else
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->nbrId, u4Offset,
                                    MAX_IP_ADDR_LEN);
    }

    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset,
                                pNbr->pInterface->u1NetworkType);
    u4Offset = u4Offset + sizeof (UINT1);

    if (pNbr->pInterface->u1NetworkType == IF_VIRTUAL)
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pNbr->pInterface->transitAreaId,
                                    u4Offset, MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    }

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pNbr->u1NsmState);
    u4Offset = u4Offset + sizeof (UINT1);

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pNbr->u1NbrHelperStatus);
    u4Offset = u4Offset + sizeof (UINT1);

    if (pNbr->u1NbrHelperStatus == OSPF_GR_HELPING)
    {
        if ((TmrGetRemainingTime (gTimerLst,
                                  &(pNbr->
                                    helperGraceTimer.timerNode),
                                  &u4DeadTime) != TMR_SUCCESS))
        {
            u4DeadTime = 0;
        }
        OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, u4DeadTime);
        u4Offset = u4Offset + sizeof (UINT4);
    }
    *pu4Offset = u4Offset;
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmAllocateBulkUpdatePkt                                */
/*                                                                           */
/* Description  : This function is used to allocate the bulk update message  */
/*                and construct the bulk update header                       */
/*                                                                           */
/* Input        :NONE                                                        */
/*                                                                           */
/* Output       :                                                            */
/*               pBuf       - Pointer to the allocated message buffer        */
/*               pu4Offset  - pointer to Current Offset Value                */
/*                                                                           */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfRmAllocateBulkUpdatePkt (tRmMsg ** ppBuf, UINT4 *pu4Offset)
{
    UINT4               u4Offset = OSPF_ZERO;
    tRmMsg             *pBuf = NULL;

    if ((pBuf = UtilOsMsgAlloc (OSPF_RED_IFACE_MTU)) == NULL)
    {
        OSPF_EXT_GBL_TRC (OS_RESOURCE_TRC, OSPF_INVALID_CXT_ID,
                          "Hello Syn Alloc Failed \n");
        OspfRmSendBulkAbort (RM_MEMALLOC_FAIL);
        gu4UtilOsMsgAllocFail++;
        return OSPF_FAILURE;
    }

    /* Initialize offset */
    u4Offset = OSPF_ZERO;
    /* Fill the Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, u4Offset, OSPF_RED_BULK_UPDATE_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Increment offset to pass the total packet length which
     * will be filled later 
     */
    u4Offset = u4Offset + sizeof (UINT2);
    *pu4Offset = u4Offset;
    *ppBuf = pBuf;
    return OSPF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendBulkUpdatePkt                                    */
/*                                                                           */
/* Description  : This function is used to send out the bulk update message  */
/*                                                                           */
/*                                                                           */
/* Input        :                                                            */
/*               pBuf       - Pointer to the allocated message buffer        */
/*               pu4Offset  - pointer to Current Offset Value                */
/* Output       : NONE                                                       */
/*                                                                           */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfRmSendBulkUpdate (tRmMsg * pBuf, UINT4 u4PktLen)
{
    if (u4PktLen == OSPF_RED_MSG_HDR_SIZE)
    {
        /* Bulk update packet has only RM Header 
         * No information is construced so free the buffer
         */
        if (pBuf != NULL)
        {
            RM_FREE (pBuf);
            pBuf = NULL;
        }
        return OSPF_SUCCESS;
    }
    /* Set the total payload length in the packet at the 
     * offset OSPF_RED_BULK_LEN_OFFSET
     */
    RM_DATA_ASSIGN_2_BYTE (pBuf, OSPF_RED_BULK_LEN_OFFSET, (UINT2) u4PktLen);
    /* Send the bulk update packet */
    if (OspfRmSendMsgToRm (pBuf, (UINT2) u4PktLen) == OSPF_FAILURE)
    {

        pBuf = NULL;
        gu4OspfRmSendMsgToRmFail++;
        return OSPF_FAILURE;
    }
    pBuf = NULL;
    return OSPF_SUCCESS;
}

/******************************************************************/
/*  Function Name   :OspfRmSendBulkNbrInfo                        */
/*  Description     :This function is invoked from RM in active   */
/*                   node when RM gets the bulk update request    */
/*                   from standby. This function will get the     */
/*                   neighbors in each context                    */
/*                   pOspfCxt->sortNbrLst and do the following    */
/*                    1.gets the Last received hello and          */
/*                      interface information from the neighbor   */
/*                    2.gets the neighbor state                   */
/*                    3.constructs the CRU message                */
/*                    4.appends next neigbors hello packet and    */
/*                      neighbor state in the CRU buf if the      */
/*                      message size doesn't exceed the RM        */
/*                      channel's MTU.                            */
/*                    5.The starting of the CRU will be filled    */
/*                      with number of hellos appended in this    */
/*                      CRU message                               */
/*                    6.If the nbr list is not empty it will      */
/*                      store the neighbor address in OsRtr       */
/*                      structure and sends the sub bulk update   */
/*                      event to self                             */
/*  Input(s)        :None                                         */
/*  Output(s)       :None                                         */
/*  <OPTIONAL Fields>:                                            */
/*  Global Variables Referred :                                   */
/*  Global variables Modified :                                   */
/*  Returns         :OSPF_SUCCESS/OSPF_FAILURE                    */
/******************************************************************/
VOID
OspfRmSendBulkNbrInfo (VOID)
{
    tRmMsg             *pBuf = NULL;
    tTMO_SLL_NODE      *pCurrIfNode = NULL;
    tTMO_SLL_NODE      *pPrevIfNode = NULL;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4PktSent = OSPF_SUCCESS;
    INT4                i4SendSubBulkEvent = OSPF_FALSE;
    UINT4               u4CurrentContextId = OSPF_ZERO;
    UINT4               u4ContextId = OSPF_ZERO;
    UINT4               u4Offset = 0;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    /* Allocate the Bulk Update Packet */
    if (OspfRmAllocateBulkUpdatePkt (&pBuf, &u4Offset) == OSPF_FAILURE)
    {
        return;
    }
    if (gOsRtr.osRedInfo.u4LastSynCxtId != OSPF_INVALID_CXT_ID)
    {
        /* Get the Last bulk update info */
        u4CurrentContextId = gOsRtr.osRedInfo.u4LastSynCxtId;
        if ((pInterface = OspfRmGetLastBulkUpdateInterface ()) != NULL)
        {
            pPrevIfNode = &(pInterface->nextSortIf);
        }
    }

    for (u4ContextId = u4CurrentContextId;
         (u4ContextId < u4MaxCxt); u4ContextId++)
    {
        if ((pOspfCxt = gOsRtr.apOspfCxt[u4ContextId]) == NULL)
        {
            continue;
        }

        while ((pCurrIfNode =
                TMO_SLL_Next (&(pOspfCxt->sortIfLst), pPrevIfNode)) != NULL)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pCurrIfNode);
            pPrevIfNode = pCurrIfNode;
            if (OspfRmSendIfInfo (pInterface, &pBuf, &u4Offset) == OSPF_FAILURE)
            {
                i4SendSubBulkEvent = OSPF_TRUE;
                /* Sending bulk update packet failed */
                i4PktSent = OSPF_FAILURE;
                break;
            }
            if (OspfRmSendNbrInfo (pInterface, &pBuf, &u4Offset) ==
                OSPF_FAILURE)
            {
                i4SendSubBulkEvent = OSPF_TRUE;
                /* Sending bulk update packet failed */
                i4PktSent = OSPF_FAILURE;
                break;
            }
            /* Check for relinquishing the bulk update process */
            if (OsRmBulkUpdtRelinquish () == OSIX_SUCCESS)
            {
                /* relinquish and send sub bulk event */
                i4SendSubBulkEvent = OSPF_TRUE;
                break;
            }
        }
        pPrevIfNode = NULL;

        if (i4SendSubBulkEvent == OSPF_TRUE)
        {
            /* relinquish and send sub bulk event */
            break;
        }
    }
    /* Send out the constructed packet */
    if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
    {
        /* Sending bulk update packet failed */
        i4PktSent = OSPF_FAILURE;
    }

    if (i4SendSubBulkEvent == OSPF_TRUE)
    {
        if ((pOspfCxt != NULL) && (pInterface != NULL))
        {

            if (i4PktSent == OSPF_FAILURE)
            {
                /* Sending bulk update packet failed 
                 * So resend the bulk update packet 
                 * for the all the interface in the context */
                IP_ADDR_COPY (&(gOsRtr.osRedInfo.lastSynIfIpAddr), gNullIpAddr);
                gOsRtr.osRedInfo.u4LastSynIfAddrlessIf = 0;
            }
            else
            {
                /* Store the Last bulk update info */
                IP_ADDR_COPY (&(gOsRtr.osRedInfo.lastSynIfIpAddr),
                              pInterface->ifIpAddr);
                gOsRtr.osRedInfo.u4LastSynIfAddrlessIf =
                    pInterface->u4AddrlessIf;
            }
            /* Store the Last Context info */
            gOsRtr.osRedInfo.u4LastSynCxtId = pOspfCxt->u4OspfCxtId;
            gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_NBR_MOD;
        }
    }
    else
    {
        /* Bulk update for neighbor is completed */

        /* Reset Last Nbr bulk update info */
        OspfRmInitNbrBulkUpdateFlags ();
        /* Update the next bulk update should be sent for virtual neighbor */
        gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_VIRT_NBR_MOD;
    }
    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmGetLastBulkUpdateInterface                           */
/*                                                                           */
/* Description  : This function is used to get the last interface pointer    */
/*                to resume the bulk update                                  */
/*                                                                           */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Global data structure referred: gOsRtr.osRedInfo                          */
/*                                                                           */
/* Returns      : if interface is found, then pInterface is returned         */
/*              : else NULL is returned                                      */
/*                                                                           */
/*****************************************************************************/
tInterface         *
OspfRmGetLastBulkUpdateInterface (VOID)
{
    if (gOsRtr.apOspfCxt[gOsRtr.osRedInfo.u4LastSynCxtId] != NULL)
    {
        return (GetFindIfInCxt
                (gOsRtr.apOspfCxt[gOsRtr.osRedInfo.u4LastSynCxtId],
                 gOsRtr.osRedInfo.lastSynIfIpAddr,
                 gOsRtr.osRedInfo.u4LastSynIfAddrlessIf));
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmGetLastBulkUpdateVirtIntf                            */
/*                                                                           */
/* Description  : This function is used to get the last interface pointer    */
/*                to resume the bulk update                                  */
/*                                                                           */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Global data structure referred: gOsRtr.osRedInfo                          */
/*                                                                           */
/* Returns      : if interface is found, then pInterface is returned         */
/*              : else NULL is returned                                      */
/*                                                                           */
/*****************************************************************************/
tInterface         *
OspfRmGetLastBulkUpdateVirtIntf (VOID)
{
    if (gOsRtr.apOspfCxt[gOsRtr.osRedInfo.u4LastSynCxtId] != NULL)
    {
        return (GetFindVirtIfInCxt
                (gOsRtr.apOspfCxt[gOsRtr.osRedInfo.u4LastSynCxtId],
                 &gOsRtr.osRedInfo.lastSynVirtTransitAreaId,
                 &gOsRtr.osRedInfo.lastSynVirtRtrId));
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendNbrInfo                                          */
/*                                                                           */
/* Description  : This function is used to construct the neighbor bulk update*/
/*                for all the neighbors present in a given interface         */
/*                Constructed bulk update will have the hello information    */
/*                received from the neigbhbor & neighbor state information.  */
/*                Bulk update packet will be sent out from this function only*/
/*                when the packet length has reached the                     */
/*                max MTU size OSPF_RED_IFACE_MTU                            */
/*                                                                           */
/* Input        : pInterface - interface pointer, information about all the  */
/*                             neighbors in this interface will be used to   */
/*                             construct the bulk update                     */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Output       :                                                            */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmSendNbrInfo (tInterface * pInterface, tRmMsg ** ppRmMsgBuf,
                   UINT4 *pu4Offset)
{
    tNeighbor          *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tRmMsg             *pBuf = *ppRmMsgBuf;
    UINT4               u4Offset = *pu4Offset;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        if (pNbr->lastRcvHello.helloPktHdr.u2HelloPktLen == 0)
        {
            continue;
        }
        if ((OspfRmGetHelloLength (pNbr) + u4Offset) >= OSPF_RED_IFACE_MTU)
        {
            /* Reached the max MTU, so send out the packet 
             * and allocate a new packet buffer */
            if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
            {
                *pu4Offset = u4Offset;
                gu4OspfRmSendBulkUpdateFail++;
                return OSPF_FAILURE;
            }
            if (OspfRmAllocateBulkUpdatePkt (ppRmMsgBuf, &u4Offset) ==
                OSPF_FAILURE)
            {
                *pu4Offset = u4Offset;
                gu4OspfRmAllocateBulkUpdatePktFail++;
                return OSPF_FAILURE;
            }
            pBuf = *ppRmMsgBuf;
        }
        /* Construct the Neighbor Hello */
        /* Fill the Sub Message Type */
        OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, u4Offset,
                                    OSPF_RED_SYNC_HELLO_PDU_MSG);
        u4Offset = u4Offset + sizeof (UINT1);

        OspfRmConstructHello (pBuf, &u4Offset, pNbr);

        if ((OspfRmGetNbrLength (pNbr) + u4Offset) >= OSPF_RED_IFACE_MTU)
        {
            /* Reached the max MTU, so send out the packet 
             * and allocate a new packet buffer */
            if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
            {
                *pu4Offset = u4Offset;
                gu4OspfRmSendBulkUpdateFail++;
                return OSPF_FAILURE;
            }
            if (OspfRmAllocateBulkUpdatePkt (ppRmMsgBuf, &u4Offset) ==
                OSPF_FAILURE)
            {
                *pu4Offset = u4Offset;
                gu4OspfRmAllocateBulkUpdatePktFail++;
                return OSPF_FAILURE;
            }
            pBuf = *ppRmMsgBuf;
        }
        if (pNbr->u1NsmState == NBRS_FULL)
        {
            /* Construct the Neighbor State Info */
            OspfRmConstructNbrState (pBuf, &u4Offset, pNbr);
        }

    }
    *pu4Offset = u4Offset;
    return OSPF_SUCCESS;
}

/******************************************************************/
/*  Function Name   :OspfRmSendBulkVirtNbrInfo                    */
/*  Description     :This function is invoked from RM in active   */
/*                   node when RM gets the bulk update request    */
/*                   from standby. This function will get the     */
/*                   virtual neighbors in each context            */
/*                   and sends out the bulk update packet for the */
/*                   virtual neighbors                            */
/*  Input(s)        :None                                         */
/*  Output(s)       :None                                         */
/*  Returns         :None                                         */
/******************************************************************/
VOID
OspfRmSendBulkVirtNbrInfo (VOID)
{
    tRmMsg             *pBuf = NULL;
    tTMO_SLL_NODE      *pCurrIfNode = NULL;
    tTMO_SLL_NODE      *pPrevIfNode = NULL;
    tInterface         *pInterface = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    INT4                i4PktSent = OSPF_SUCCESS;
    INT4                i4SendSubBulkEvent = OSPF_FALSE;
    UINT4               u4CurrentContextId = OSPF_ZERO;
    UINT4               u4ContextId = OSPF_ZERO;
    UINT4               u4Offset = 0;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    /* Allocate the Bulk Update Packet */
    if (OspfRmAllocateBulkUpdatePkt (&pBuf, &u4Offset) == OSPF_FAILURE)
    {
        return;
    }
    if (gOsRtr.osRedInfo.u4LastSynCxtId != OSPF_INVALID_CXT_ID)
    {
        /* Get the Last bulk update info */
        u4CurrentContextId = gOsRtr.osRedInfo.u4LastSynCxtId;
        if ((pInterface = OspfRmGetLastBulkUpdateVirtIntf ()) != NULL)
        {
            pPrevIfNode = &(pInterface->nextSortIf);
        }
    }

    for (u4ContextId = u4CurrentContextId;
         (u4ContextId < u4MaxCxt); u4ContextId++)
    {
        if ((pOspfCxt = gOsRtr.apOspfCxt[u4ContextId]) == NULL)
        {
            continue;
        }
        while ((pCurrIfNode =
                TMO_SLL_Next (&(pOspfCxt->virtIfLst), pPrevIfNode)) != NULL)
        {
            pInterface = GET_IF_PTR_FROM_SORT_LST (pCurrIfNode);
            pPrevIfNode = pCurrIfNode;

            if (OspfRmSendNbrInfo (pInterface, &pBuf, &u4Offset) ==
                OSPF_FAILURE)
            {
                i4SendSubBulkEvent = OSPF_TRUE;
                /* Sending bulk update packet failed */
                i4PktSent = OSPF_FAILURE;
                break;
            }
            /* Check for relinquishing the bulk update process */
            if (OsRmBulkUpdtRelinquish () == OSIX_SUCCESS)
            {
                /* relinquish and send sub bulk event */
                i4SendSubBulkEvent = OSPF_TRUE;
                break;
            }
        }
        pPrevIfNode = NULL;
    }
    /* Send out the constructed packet */
    if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
    {
        /* Sending bulk update packet failed */
        i4PktSent = OSPF_FAILURE;
    }

    if (i4SendSubBulkEvent == OSPF_TRUE)
    {
        if ((pOspfCxt != NULL) && (pInterface != NULL))
        {

            if (i4PktSent == OSPF_FAILURE)
            {
                /* Sending bulk update packet failed 
                 * So resend the bulk update packet 
                 * for the all the interface in the context */
                IP_ADDR_COPY (&(gOsRtr.osRedInfo.lastSynVirtTransitAreaId),
                              gNullIpAddr);
                IP_ADDR_COPY (&(gOsRtr.osRedInfo.lastSynVirtRtrId),
                              gNullIpAddr);
            }
            else
            {
                /* Store the Last bulk update info */
                IP_ADDR_COPY (&(gOsRtr.osRedInfo.lastSynVirtTransitAreaId),
                              pInterface->transitAreaId);
                IP_ADDR_COPY (&(gOsRtr.osRedInfo.lastSynVirtRtrId),
                              pInterface->destRtrId);
            }
            /* Store the Last Context info */
            gOsRtr.osRedInfo.u4LastSynCxtId = pOspfCxt->u4OspfCxtId;
            gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_VIRT_NBR_MOD;
        }
    }
    else
    {
        /* Bulk update for neighbor is completed */

        /* Reset Last bulk update info */
        OspfRmInitVirtNbrBulkUpdateFlags ();
        /* initialize the Lsa bulk update flags */
        /* Update the next bulk update should be sent for virtual neighbor */
        gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_LSDB_MOD;
    }
    gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendIfInfo                                           */
/*                                                                           */
/* Description  : This function is used to construct the interface           */
/*                bulk update                                                */
/*                Bulk update packet will be sent out from this function only*/
/*                when the packet length has reached the                     */
/*                max MTU size OSPF_RED_IFACE_MTU                            */
/*                                                                           */
/* Input        : pInterface - interface pointer, information about all this */
/*                             interface will be used to construct the bulk  */
/*                             update                                        */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Output       :                                                            */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmSendIfInfo (tInterface * pInterface, tRmMsg ** ppRmMsgBuf,
                  UINT4 *pu4Offset)
{
    tRmMsg             *pBuf = *ppRmMsgBuf;
    UINT4               u4Offset = *pu4Offset;

    if ((u4Offset + OSPF_RED_SYN_IF_STATE_SIZE) >= OSPF_RED_IFACE_MTU)
    {
        /* Reached the max MTU, so send out the packet 
         * and allocate a new packet buffer */
        if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
        {
            *pu4Offset = u4Offset;
            gu4OspfRmSendBulkUpdateFail++;
            return OSPF_FAILURE;
        }
        if (OspfRmAllocateBulkUpdatePkt (ppRmMsgBuf, &u4Offset) == OSPF_FAILURE)
        {
            *pu4Offset = u4Offset;
            gu4OspfRmAllocateBulkUpdatePktFail++;
            return OSPF_FAILURE;
        }
        pBuf = *ppRmMsgBuf;
    }
    /* Fill the Sub Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, u4Offset, OSPF_RED_SYNC_IFSTATE_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    OspfRmConstructIfState (pBuf, &u4Offset, pInterface, OSPF_ZERO);
    *pu4Offset = u4Offset;
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmConstructIfState                                     */
/*                                                                           */
/* Description  : This function is used to construct the Interface state     */
/*                DR and BDR on the interface.                               */
/*                                                                           */
/* Input        :                                                            */
/*               pu4Offset  - Offset from which message has to be constructed*/
/*               pInterface - pointer to Interface                           */
/*                                                                           */
/*                                                                           */
/* Output       :                                                            */
/*                pMsg       - Pointer to Output Message                     */
/*                                                                           */
/* Returns      : NONE                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmConstructIfState (tRmMsg * pMsg, UINT4 *pu4Offset,
                        tInterface * pInterface, UINT4 u4BitMask)
{
    UINT4               u4Offset = *pu4Offset;

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                pInterface->pArea->pOspfCxt->u4OspfCxtId);
    u4Offset = u4Offset + sizeof (UINT4);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pInterface->u1NetworkType);
    u4Offset = u4Offset + sizeof (UINT1);
    if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pInterface->transitAreaId, u4Offset,
                                    MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;

        OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pInterface->destRtrId, u4Offset,
                                    MAX_IP_ADDR_LEN);
        u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    }

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pInterface->ifIpAddr, u4Offset,
                                MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pInterface->u4AddrlessIf);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pInterface->ifIpAddrMask, u4Offset,
                                MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pInterface->operStatus);
    u4Offset += sizeof (tOSPFSTATUS);
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, u4BitMask);
    u4Offset += sizeof (UINT4);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pInterface->u1IsmState);
    u4Offset = u4Offset + sizeof (UINT1);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pInterface->desgRtr, u4Offset,
                                MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pInterface->backupDesgRtr, u4Offset,
                                MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pInterface->u4MtuSize);
    u4Offset += sizeof (UINT4);
    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset,
                                pInterface->aIfOpCost[TOS_0].u4Value);
    u4Offset += sizeof (UINT4);
    *pu4Offset = u4Offset;
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : OspfRmSendBulkTmrInfo                                     */
/*                                                                          */
/* Description  : This function is called when active node get the bulk     */
/*                request message or sub bulk update event. This function   */
/*                is used to sync up the exit database overflow state timer */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/
VOID
OspfRmSendBulkTmrInfo (VOID)
{
    tOspfCxt           *pOspfCxt = NULL;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = OSPF_ZERO;
    UINT4               u4RemTime = OSPF_ZERO;
    UINT4               u4ContextId = OSPF_DEFAULT_CXT_ID;
    UINT4               u4CurrentCxtId = OSPF_DEFAULT_CXT_ID;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (OspfRmAllocateBulkUpdatePkt (&pMsg, &u4Offset) == OSPF_FAILURE)
    {
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
        return;
    }

    for (u4ContextId = u4CurrentCxtId; u4ContextId < u4MaxCxt; u4ContextId++)
    {
        pOspfCxt = UtilOspfGetCxt (u4ContextId);

        if (pOspfCxt == NULL)
        {
            /* Get the next context */
            OSPF_EXT_TRC1 (OSPF_REDUNDANCY_TRC, OSPF_DEFAULT_CXT_ID,
                           "OspfRmSendBulkLsaInfo : "
                           "Context id : %u not available\r\n", u4ContextId);
            continue;
        }
        if (pOspfCxt->bOverflowState == OSPF_TRUE)
        {
            if ((u4Offset + OSPF_RED_SYNC_DB_TMR_MSG_SIZE) >=
                OSPF_RED_IFACE_MTU)
            {
                /* Reached the max MTU, so send out the packet
                 * and allocate a new packet buffer */
                if (OspfRmSendBulkUpdate (pMsg, u4Offset) == OSPF_FAILURE)
                {
                    gOsRtr.osRedInfo.u4DynBulkUpdatStatus =
                        OSPF_RED_BLKUPDT_ABORTED;
                    break;
                }
                if (OspfRmAllocateBulkUpdatePkt (&pMsg, &u4Offset) ==
                    OSPF_FAILURE)
                {
                    gOsRtr.osRedInfo.u4DynBulkUpdatStatus =
                        OSPF_RED_BLKUPDT_ABORTED;
                    break;
                }
            }
            OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset,
                                        OSPF_RED_SYNC_DBOVERFLOW_TMR_MSG);
            u4Offset = u4Offset + sizeof (UINT1);
            OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pOspfCxt->u4OspfCxtId);
            u4Offset = u4Offset + sizeof (UINT4);
            TmrGetRemainingTime (gTimerLst,
                                 (tTmrAppTimer *) & (pOspfCxt->
                                                     exitOverflowTimer.
                                                     timerNode), &u4RemTime);
            OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, u4RemTime);
            u4Offset = u4Offset + sizeof (UINT4);
        }
    }

    if (OspfRmSendBulkUpdate (pMsg, u4Offset) == OSPF_FAILURE)
    {
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
    }

    if (gOsRtr.osRedInfo.u4DynBulkUpdatStatus != OSPF_RED_BLKUPDT_ABORTED)
    {
        gOsRtr.osRedInfo.u1BulkUpdModuleStatus = OSPF_RED_DEF_CXT_MOD;
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_INPROGRESS;
    }
}

/******************************************************************/
/*  Function Name   :OspfRmSendBulkExtRouteInfo                   */
/*  Description     :This function is invoked from RM in active   */
/*                   node when RM gets the bulk update request    */
/*                   from standby. This function will get the     */
/*                   external redistribute routes                 */
/*                   and sends out the bulk update packet for the */
/*                   redistributed routes                         */
/*  Input(s)        :None                                         */
/*  Output(s)       :None                                         */
/*  Returns         :None                                         */
/******************************************************************/
VOID
OspfRmSendBulkExtRouteInfo (VOID)
{
    tRmMsg             *pBuf = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tExtRoute          *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    VOID               *pAppSpecPtr = NULL;
    UINT4               au4Key[2];
    UINT4               u4ContextId = OSPF_ZERO;
    UINT4               u4Offset = 0;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = OSPF_MIN (MAX_OSPF_CONTEXTS_LIMIT,
                         FsOSPFSizingParams[MAX_OSPF_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    /* Allocate the Bulk Update Packet */
    if (OspfRmAllocateBulkUpdatePkt (&pBuf, &u4Offset) == OSPF_FAILURE)
    {
        return;
    }
    for (u4ContextId = OSPF_DEFAULT_CXT_ID;
         u4ContextId < u4MaxCxt; u4ContextId++)
    {
        pOspfCxt = UtilOspfGetCxt (u4ContextId);
        if (pOspfCxt == NULL)
        {
            continue;
        }
        au4Key[0] = 0;
        au4Key[1] = 0;
        inParams.Key.pKey = (UINT1 *) au4Key;
        inParams.pRoot = pOspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPF_EXT_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        if (TrieGetFirstNode (&inParams,
                              &pAppSpecPtr,
                              (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
        {
            do
            {
                pExtRoute = (tExtRoute *) pAppSpecPtr;
                if (OspfRmSendExtRtInfo (pOspfCxt, pExtRoute, &pBuf, &u4Offset)
                    == OSPF_FAILURE)
                {
                    return;
                }
                inParams.pRoot = pOspfCxt->pExtRtRoot;
                inParams.i1AppId = OSPF_EXT_RT_ID;
                inParams.pLeafNode = NULL;
                au4Key[0] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipNetNum));
                au4Key[1] = OSIX_HTONL (OSPF_CRU_BMC_DWFROMPDU
                                        (pExtRoute->ipAddrMask));
                inParams.Key.pKey = (UINT1 *) au4Key;
                pLeafNode = inParams.pLeafNode;
                pExtRoute = NULL;
            }
            while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                    &pAppSpecPtr,
                                    (VOID **) &(inParams.pLeafNode)) !=
                   TRIE_FAILURE);

        }

    }

    /* Send out the constructed packet */
    if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
    {
        return;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendExtRtInfo                                        */
/*                                                                           */
/* Description  : This function is used to construct the bulk update for     */
/*                external redistributed routes                              */
/*                Bulk update packet will be sent out from this function only*/
/*                when the packet length has reached the                     */
/*                max MTU size OSPF_RED_IFACE_MTU                            */
/*                                                                           */
/* Input        : pOspfCxt   - OSPF Context pointer                          */
/*                pExtRoute  - pointer to external redistributed routes      */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Output       :                                                            */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
OspfRmSendExtRtInfo (tOspfCxt * pOspfCxt, tExtRoute * pExtRoute,
                     tRmMsg ** ppRmMsgBuf, UINT4 *pu4Offset)
{
    tRmMsg             *pBuf = *ppRmMsgBuf;
    UINT4               u4Offset = *pu4Offset;

    if ((u4Offset + sizeof (tExtRoute)) >= OSPF_RED_IFACE_MTU)
    {
        /* Reached the max MTU, so send out the packet 
         * and allocate a new packet buffer */
        if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
        {
            *pu4Offset = u4Offset;
            return OSPF_FAILURE;
        }
        if (OspfRmAllocateBulkUpdatePkt (ppRmMsgBuf, &u4Offset) == OSPF_FAILURE)
        {
            *pu4Offset = u4Offset;
            return OSPF_FAILURE;
        }
        pBuf = *ppRmMsgBuf;
    }
    /* Fill the Sub Message Type */
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, u4Offset, OSPF_RED_SYNC_EXT_RT_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    OspfRmConstructExtRt (pOspfCxt, pBuf, &u4Offset, pExtRoute);
    *pu4Offset = u4Offset;
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmConstructExtRt                                       */
/*                                                                           */
/* Description  : This function is used to construct the bulk update for     */
/*                external redistributed routes                              */
/*                                                                           */
/* Input        : pOspfCxt   - OSPF Context pointer                          */
/*                pExtRoute  - pointer to external redistributed routes      */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Output       :                                                            */
/*                ppRmMsgBuf - Bulk update buffer                            */
/*                pu4Offset  - Current write offset in the bulk update buffer*/
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmConstructExtRt (tOspfCxt * pOspfCxt, tRmMsg * pMsg,
                      UINT4 *pu4Offset, tExtRoute * pExtRoute)
{
    UINT4               u4Offset = *pu4Offset;

    OSPF_CRU_BMC_ASSIGN_4_BYTE (pMsg, u4Offset, pOspfCxt->u4OspfCxtId);
    u4Offset = u4Offset + sizeof (UINT4);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pExtRoute->ipNetNum, u4Offset,
                                MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, pExtRoute->ipAddrMask, u4Offset,
                                MAX_IP_ADDR_LEN);
    u4Offset = u4Offset + MAX_IP_ADDR_LEN;

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, (UINT1 *) &(pExtRoute->aMetric), u4Offset,
                                (sizeof (tMibMetric) * OSPF_MAX_METRIC));
    u4Offset = u4Offset + (sizeof (tMibMetric) * OSPF_MAX_METRIC);

    OSPF_CRU_BMC_ASSIGN_STRING (pMsg, (UINT1 *) &(pExtRoute->extrtParam),
                                u4Offset,
                                (sizeof (tExtrtParam) * OSPF_MAX_METRIC));
    u4Offset = u4Offset + (sizeof (tExtrtParam) * OSPF_MAX_METRIC);

    OSPF_CRU_BMC_ASSIGN_2_BYTE (pMsg, u4Offset, pExtRoute->u2SrcProto);
    u4Offset = u4Offset + sizeof (UINT2);
    *pu4Offset = u4Offset;
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pMsg, u4Offset, pExtRoute->u1Level);
    u4Offset = u4Offset + sizeof (UINT1);
    *pu4Offset = u4Offset;
    return;
}

/******************************************************************/
/*  Function Name   :OspfRmSendBulkDefCxtInfo                     */
/*                                                                */
/*  Description     :This function is invoked from RM in active   */
/*                   node when RM gets the bulk update request    */
/*                   from standby. This function will get the     */
/*                   send the bulk update packet when default     */
/*                   context is deleted                           */
/*  Input(s)        :None                                         */
/*  Output(s)       :None                                         */
/*  Returns         :None                                         */
/******************************************************************/
VOID
OspfRmSendBulkDefCxtInfo (VOID)
{
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = 0;

    /* Allocate the Bulk Update Packet */
    if (OspfRmAllocateBulkUpdatePkt (&pBuf, &u4Offset) == OSPF_FAILURE)
    {
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
        return;
    }

    /* Fill the message sub type */

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, u4Offset, OSPF_RED_SYNC_DEF_CXT_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Send out the constructed packet */
    if (OspfRmSendBulkUpdate (pBuf, u4Offset) == OSPF_FAILURE)
    {
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_ABORTED;
    }
    else
    {
        gOsRtr.osRedInfo.u4DynBulkUpdatStatus = OSPF_RED_BLKUPDT_COMPLETED;
    }

    return;
}
#endif /* _OSREDBLK_ */
