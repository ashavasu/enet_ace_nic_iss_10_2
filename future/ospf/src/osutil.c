/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osutil.c,v 1.15 2015/11/20 10:47:04 siva Exp $
 *
 * Description: This file contains utility routines.
 *
 *******************************************************************/

#include "osinc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID        UtilAuthenticatePkt
PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, tInterface * pInterface, UINT2 u2Len));
PRIVATE INT4 UtilMod255 PROTO ((INT4 i4Sum));

static UINT1        au1_bit_count[256] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindMaskLen                                         */
/*                                                                           */
/* Description  : This procedure finds the length of the given mask.         */
/*                                                                           */
/* Input        : u4Mask  : Mask whose length is to be determined.          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Length of the mask.                                        */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilFindMaskLen (UINT4 u4Mask)
{
    return ((UINT1) (au1_bit_count[u4Mask & 0xff]
                     + au1_bit_count[(u4Mask >> 8) & 0xff]
                     + au1_bit_count[(u4Mask >> 16) & 0xff]
                     + au1_bit_count[(u4Mask >> 24) & 0xff]));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilConstructHdr                                         */
/*                                                                           */
/* Description  : This procedure constructs the ospf header for packets to be*/
/*                transmitted on the specified interface. This procedure will*/
/*                be called after the packet is fully constructed.           */
/*                                                                           */
/* Input        : pInterface       : interface over which the packets are   */
/*                                    transmitted                            */
/*                pBuf             : buffer which holds the header          */
/*                u1Type           : type                                   */
/*                u2Len            : length                                 */
/*                                                                           */
/* Output       : pBuf, buffer which holds the header                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilConstructHdr (tInterface * pInterface,
                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Type, UINT2 u2Len)
{

    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, VERSION_NO_OFFSET, OSPF_VERSION_NO_2);
    OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, TYPE_OFFSET, u1Type);
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, PKT_LENGTH_OFFSET, u2Len);
    OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pBuf, gOsRtr.rtrId, RTR_ID_OFFSET,
                                       MAX_IP_ADDR_LEN);
    OSPF_CRU_BMC_APPEND_STRING_IN_CRU (pBuf, GET_IF_AREA_ID (pInterface),
                                       AREA_ID_OFFSET, MAX_IP_ADDR_LEN);

    /* checksum field is set to 0 before calculation */
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, CHKSUM_OFFSET, 0);
    OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_TYPE_OFFSET, pInterface->u2AuthType);

    UtilAuthenticatePkt (pBuf, pInterface, u2Len);

}

/*******************************************************************************
* Function : GetAuthkeyTouse                                                 *
* Input    : pInterface                                                       *
* Output   : None                                                              *
* Returns  : pKeyTouse - The MD5 authentication key to use                   *
*******************************************************************************/
PRIVATE tMd5AuthkeyInfo *
GetAuthkeyTouse (tInterface * pInterface)
{
    tMd5AuthkeyInfo    *pNode = NULL;
    tMd5AuthkeyInfo    *pExpiredNode = NULL;
    tMd5AuthkeyInfo    *pKeyTouse = NULL;
    UINT4               newDiff;
    UINT4               u4KeyStartGenDiff = (UINT4) -1;
    UINT4               u4Ospf1sTimer = 0;
    tUtlTm              tm;

    /* Getting the system time */
    UtlGetTime (&tm);
    u4Ospf1sTimer = GrGetSecondsSinceBase (tm);    /* Converting time in string to u4 value */
    TMO_SLL_Scan (&(pInterface->sortMd5authkeyLst), pNode, tMd5AuthkeyInfo *)
    {

        if (pNode->u4KeyStartGenerate <= u4Ospf1sTimer)    /* Comparison is made with system time */
        {
            /* If atlease one key with start time less than the system time 
               is present, then delete the expired key */
            if (pExpiredNode != NULL)
            {
                /*        TMO_SLL_Delete (&(pInterface->sortMd5authkeyLst),
                   (tTMO_SLL_NODE *)&(pExpiredNode));
                   MD5AUTH_FREE (pExpiredNode); */
            }
            if ((pNode->u4KeyStopGenerate < u4Ospf1sTimer) &&    /* Comparison is made with system time */
                (pNode->u4KeyStopAccept < u4Ospf1sTimer))
            {
                pExpiredNode = pNode;
            }

            else if ((pNode->u4KeyStopGenerate == 0) || (pNode->u4KeyStopGenerate > u4Ospf1sTimer))    /* Comparison is made with system time */
            {
                /* If more than one unexpired key is available then use
                   most recent key having start time maximum */

                newDiff = u4Ospf1sTimer - pNode->u4KeyStartGenerate;
                if (newDiff < u4KeyStartGenDiff)
                {
                    u4KeyStartGenDiff = newDiff;
                    pKeyTouse = pNode;
                }
            }
        }
    }

    /* If the last key is expired, that key can be used, until the new key 
       is configured */
    if ((pExpiredNode != NULL) && (pKeyTouse == NULL))
    {
        return pExpiredNode;
    }
    return pKeyTouse;
}

/*******************************************************************************
* Function : AppendMd5authDigest                                             *
* Input    : pBuf                                                             *
*            u2Len                                                            *
*            pAuthkeyInfo                                                    *
* Output   : None                                                              *
* Returns  : void                                                              *
*******************************************************************************/
PRIVATE void
AppendMd5authDigest (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2Len, tMd5AuthkeyInfo * pAuthkeyInfo)
{
    UINT1               digest[MAX_AUTHKEY_LEN];

    OSPF_CRU_BMC_ASSIGN_STRING (pBuf, pAuthkeyInfo->authKey, u2Len,
                                MAX_AUTHKEY_LEN);
    md5_get_keyed_digest (pBuf, pAuthkeyInfo->authKey, MAX_AUTHKEY_LEN, digest);
    OSPF_CRU_BMC_ASSIGN_STRING (pBuf, digest, u2Len, MAX_AUTHKEY_LEN);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilAuthenticatePkt                                      */
/*                                                                           */
/* Description  : This procedure authenticates the given ospf packet         */
/*                                                                           */
/* Input        :  pBuf                     : Pointer to the buffer having  */
/*                                             the OSPF packet.              */
/*              :  pInterface               : Pointer to interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
UtilAuthenticatePkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                     tInterface * pInterface, UINT2 u2Len)
{
    UINT2               u2Chksum;
    tMd5AuthkeyInfo    *pAuthkeyInfo;

    OSPF_TRC (OSPF_FN_ENTRY, "FUNC: UtilAuthenticatePkt\n");

    switch (pInterface->u2AuthType)
    {
        case NO_AUTHENTICATION:
            /* calculate checksum for the packet */
            OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pBuf, 0, AUTH_KEY_OFFSET_IN_HEADER,
                                             AUTH_KEY_SIZE);
            u2Chksum = UtilCalculateChksum (pBuf, u2Len);
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, CHKSUM_OFFSET_IN_HEADER,
                                        u2Chksum);
            break;

        case SIMPLE_PASSWORD:
            /* calculate checksum for the packet */
            OSPF_CRU_BUF_FILL_CHAR_IN_CHAIN (pBuf, 0, AUTH_KEY_OFFSET_IN_HEADER,
                                             AUTH_KEY_SIZE);
            u2Chksum = UtilCalculateChksum (pBuf, u2Len);
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, CHKSUM_OFFSET_IN_HEADER,
                                        u2Chksum);
            OSPF_CRU_BMC_ASSIGN_STRING (pBuf, pInterface->authKey,
                                        AUTH_KEY_OFFSET_IN_HEADER,
                                        AUTH_KEY_SIZE);
            break;

        case CRYPT_AUTHENTICATION:
            pAuthkeyInfo = GetAuthkeyTouse (pInterface);

            if (pAuthkeyInfo == NULL && pInterface->pLastAuthkey)
            {
                pAuthkeyInfo = pInterface->pLastAuthkey;
            }
            else
            {
                pInterface->pLastAuthkey = pAuthkeyInfo;
            }

            if (pAuthkeyInfo == NULL)
            {
                OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_1_OFFSET, 0);
                OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_2_OFFSET, 0);
                OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_3_OFFSET, 0);
                OSPF_CRU_BMC_ASSIGN_4_BYTE (pBuf, AUTH_4_OFFSET, 0);

                OSPF_TRC (OSPF_FN_EXIT, "EXIT: UtilAuthenticatePkt\n");
                return;
            }
            OSPF_CRU_BMC_ASSIGN_2_BYTE (pBuf, AUTH_1_OFFSET, (UINT2) 0);
            OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_2_OFFSET,
                                        pAuthkeyInfo->u1AuthkeyId);
            OSPF_CRU_BMC_ASSIGN_1_BYTE (pBuf, AUTH_3_OFFSET, MD5_AUTH_DATA_LEN);
            OSPF_CRU_BMC_ASSIGN_4_BYTE (pBuf, AUTH_4_OFFSET,
                                        pInterface->u4CryptSeqNum);
            AppendMd5authDigest (pBuf, u2Len, pAuthkeyInfo);
            pInterface->u4CryptSeqNum++;
            break;

        default:
            break;
    }
    OSPF_TRC (OSPF_FN_EXIT, "FUNC: UtilAuthenticatePkt\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOsMsgAlloc                                          */
/*                                                                           */
/* Description  : This procedure allocates memory for the size               */
/*                u4Size + size of the IP header                            */
/*                                                                           */
/* Input        : u4Size   : size of the buffer required.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the CRU_BUF_CHAIN_HEADER in case of successful  */
/*                allocation.                                                */
/*                NULL, otherwise.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC tCRU_BUF_CHAIN_HEADER *
UtilOsMsgAlloc (UINT4 u4Size)
{

    tCRU_BUF_CHAIN_HEADER *pMsg;
    UINT4               u4AlignByte;    /*no of byte to be appeneded for alignment */
    UINT4               u4AlignSize;    /*smallest size of an aligned variable */

    u4AlignSize = sizeof (UINT4);
    u4AlignByte = (u4AlignSize - (ETH_HEADER_SIZE % u4AlignSize)) % u4AlignSize;
    /* Set valid offset to IP header size. */
    /* Add u4AlignByte with Ethernet header size to make IP header start from
     * aligned byte*/
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((u4Size + IP_HEADER_SIZE +
                                       ETH_HEADER_SIZE + u4AlignByte),
                                      IP_HEADER_SIZE + ETH_HEADER_SIZE +
                                      u4AlignByte);

    return pMsg;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOsMsgFree                                           */
/*                                                                           */
/* Description  : This procedure releases memory in the buffer chain         */
/*                                                                           */
/* Input        : msg                  : pointer to the CRU_BUF_CHAIN_HEADER */
/*                u1ReleaseFlag      : Indicates the type of release,      */
/*                                       either NORMAL or FORCED.            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilOsMsgFree (tCRU_BUF_CHAIN_HEADER * msg, UINT1 u1ReleaseFlag)
{
    CRU_BUF_Release_MsgBufChain (msg, u1ReleaseFlag);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrComp                                          */
/*                                                                           */
/* Description  : This procedure compares two ip addresses                   */
/*                                                                           */
/* Input        : addr1   :   IP address 1                                   */
/*                addr2   :   IP address 2                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if the IP addresses are equal                  */
/*                OSPF_GREATER, if IP address 1 is greater                   */
/*                OSPF_LESS, if IP address 2 is greater                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrComp (tIPADDR addr1, tIPADDR addr2)
{

    UINT4               u4Addr1 = 0;
    UINT4               u4Addr2 = 0;

    /* returns OSPF_EQUAL if they are equal 
     * OSPF_GREATER if addr1 is greater
     * OSPF_LESS if addr1 is lesser
     */

    if ((addr1 == NULL) && (addr2 == NULL))
        return OSPF_EQUAL;

    if (addr1 == NULL)
        return OSPF_LESS;
    if (addr2 == NULL)
        return OSPF_GREATER;

    u4Addr1 = OSPF_CRU_BMC_DWFROMPDU (addr1);
    u4Addr2 = OSPF_CRU_BMC_DWFROMPDU (addr2);

    if (u4Addr1 > u4Addr2)
        return OSPF_GREATER;
    else if (u4Addr1 < u4Addr2)
        return OSPF_LESS;

    return (OSPF_EQUAL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrMaskComp                                     */
/*                                                                           */
/* Description  : This procedure compares two ip addresses after applying    */
/*                the mask.                                                  */
/*                                                                           */
/* Input        : addr1    :  IP address 1                                   */
/*                addr2    :  IP address 2                                   */
/*                mask     :  mask to be applied on IP addresses             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if the IP addresses are equal                  */
/*                OSPF_GREATER, if IP address 1 is greater                   */
/*                OSPF_LESS, if IP address 2 is greater                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrMaskComp (tIPADDR addr1, tIPADDR addr2, tIPADDR mask)
{

    UINT4               u4Addr1;
    UINT4               u4Addr2;

    /* 
     * Compares two IP adresses after applying the specified mask.
     * returns OSPF_EQUAL   if they are equal 
     *         OSPF_GREATER if addr1 is greater
     *         OSPF_LESS    if addr1 is lesser
     */

    u4Addr1 = OSPF_CRU_BMC_DWFROMPDU (addr1) & OSPF_CRU_BMC_DWFROMPDU (mask);
    u4Addr2 = OSPF_CRU_BMC_DWFROMPDU (addr2) & OSPF_CRU_BMC_DWFROMPDU (mask);

    if (u4Addr1 > u4Addr2)
        return OSPF_GREATER;
    else if (u4Addr1 < u4Addr2)
        return OSPF_LESS;

    return (OSPF_EQUAL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrMaskCopy                                     */
/*                                                                           */
/* Description  : This procedure copies IP address from the source to the    */
/*                destination, after MASK is applied on the source address.  */
/*                                                                           */
/* Input        : dest      :  destination address                           */
/*                src       :  source address                                */
/*                mask      :  mask to be applied to both the addresses      */
/*                                                                           */
/* Output       : destination address                                        */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilIpAddrMaskCopy (tIPADDR dest, tIPADDR src, tIPADDR mask)
{

    UINT4               u4Src;
    UINT4               u4Mask;

    u4Src = OSPF_CRU_BMC_DWFROMPDU (src);
    u4Mask = OSPF_CRU_BMC_DWFROMPDU (mask);
    OSPF_CRU_BMC_DWTOPDU (((UINT1 *) dest), (u4Src & u4Mask));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrIndComp                                      */
/*                                                                           */
/* Description  : This procedure compares the ip address and the ip addrless */
/*                ind fields in the two interfaces.                          */
/*                                                                           */
/* Input        : ifIpAddr1      : Interface address of the IP address 1   */
/*                u4AddrlessIf1  : Interface Index 1                       */
/*                ifIpAddr2      : Interface address of the IP address 2   */
/*                u4AddrlessIf2  : Interface Index 2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if they are equal                              */
/*                OSPF_GREATER, if interface1 is greater                     */
/*                OSPF_LESS, if interface2 is greater                        */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrIndComp (tIPADDR ifIpAddr1,
                   UINT4 u4AddrlessIf1, tIPADDR ifIpAddr2, UINT4 u4AddrlessIf2)
{

    UINT1               u1Result;

    u1Result = UtilIpAddrComp (ifIpAddr1, ifIpAddr2);
    if (u1Result == OSPF_EQUAL)
    {
        if (u4AddrlessIf1 > u4AddrlessIf2)
        {
            u1Result = OSPF_GREATER;
        }
        else if (u4AddrlessIf1 < u4AddrlessIf2)
        {
            u1Result = OSPF_LESS;
        }
    }
    return u1Result;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilLsaIdComp                                           */
/*                                                                           */
/* Description  : This procedure compares two LSA IDs                        */
/*                                                                           */
/* Input        : u1LsaType1       :  LSA type 1                           */
/*                linkStateId1     :  Link state Id 1                      */
/*                advRtrId1        :  Advertising Router Id 1              */
/*                u1LsaType2       :  LSA type 2                           */
/*                linkStateId2     :  Link state Id 2                      */
/*                advRtrId2        :  Advertising Router Id 2              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :  OSPF_EQUAL, if the triplets are equal                     */
/*                 OSPF_GREATER, if triplet1 is greater                      */
/*                 OSPF_LESS, if triplet2 is greater                         */
/*                 triplet is <Lsa type, Link state Id, Adv. Rtr Id>         */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilLsaIdComp (UINT1 u1LsaType1,
               tLINKSTATEID
               linkStateId1,
               tRouterId advRtrId1,
               UINT1 u1LsaType2, tLINKSTATEID linkStateId2, tRouterId advRtrId2)
{

    if (u1LsaType1 < u1LsaType2)
    {
        return OSPF_LESS;
    }
    else if (u1LsaType1 > u1LsaType2)
    {
        return OSPF_GREATER;
    }
    switch (UtilIpAddrComp (linkStateId1, linkStateId2))
    {
        case OSPF_LESS:
            return OSPF_LESS;

        case OSPF_GREATER:
            return OSPF_GREATER;

        case OSPF_EQUAL:
            return (UtilIpAddrComp (advRtrId1, advRtrId2));

        default:
            return INVALID;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindAssoAreaAddrRange                             */
/*                                                                           */
/* Description  : This procedure associates the specified IP addr with one of*/
/*                the router's configured areas based on which address range */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pAddr        : IP address                                 */
/*              : pAreaId      : Area Id                                    */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddrRange  *
UtilFindAssoAreaAddrRange (tIPADDR * pAddr, tAreaId * pAreaId)
{

    UINT1               u1Index;
    tArea              *pArea;

    pArea = GetFindArea (pAreaId);
    if (pArea == NULL)
    {
        return NULL;
    }

    for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
    {

        if ((pArea->aAddrRange[u1Index].areaAggStatus !=
             INVALID)
            &&
            (UtilIpAddrMaskComp
             (*pAddr, pArea->aAddrRange[u1Index].ipAddr,
              pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL))
        {

            return (&(pArea->aAddrRange[u1Index]));
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindAssoArea                                        */
/*                                                                           */
/* Description  : This procedure associates the specified IP addr with one of*/
/*                the router's configured areas based on which address range */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pAddr         :  IP address                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to AREA, on success                                */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tArea       *
UtilFindAssoArea (tIPADDR * pAddr)
{

    UINT1               u1Index;
    tArea              *pArea;

    TMO_SLL_Scan (&(gOsRtr.areasLst), pArea, tArea *)
    {

        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {

            if ((pArea->aAddrRange[u1Index].areaAggStatus !=
                 INVALID)
                &&
                (UtilIpAddrMaskComp
                 (*pAddr, pArea->aAddrRange[u1Index].ipAddr,
                  pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL))
            {

                return (pArea);
            }
        }
    }

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilHashGetValue                                        */
/*                                                                           */
/* Description  : This procedure gets the hash value for the key             */
/*                                                                           */
/* Input        : u4HashSize    :  Max. no. of buckets                     */
/*                pKey           :  Pointer to information to be hashed     */
/*                u1KeyLen      :  Length of the information that is to    */
/*                                   be hashed.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hash Bucket value                                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
UtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen)
{

    UINT4               h = 0;
    UINT4               g;
    UINT1              *p;

    for (p = pKey; u1KeyLen; p++, u1KeyLen--)
    {

        h = (h << 4) + (*p);
        g = h & 0xf0000000;
        if (g)
        {

            h = h ^ (g >> 24);
            h = h ^ g;
            g = h & 0xf0000000;
        }
    }
    return h % u4HashSize;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilVirtIfIndComp                                      */
/*                                                                           */
/* Description  : compares the two virtual interface indices.                */
/*                                                                           */
/* Input        : areaId1    :  Area Id 1                                   */
/*                nbrId1     :  Neighbor Id 1                               */
/*                areaId2    :  Area Id 2                                   */
/*                nbrId2     :  Neighbor Id 2                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if the couples are equal                       */
/*                OSPF_GREATER, if couple1 is greater                        */
/*                OSPF_LESS, if couple2 is greater                           */
/*                couple is <Area Id, Neighbor Id>                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilVirtIfIndComp (tAreaId areaId1,
                   tRouterId nbrId1, tAreaId areaId2, tRouterId nbrId2)
{

    if (areaId1 == NULL)
        return OSPF_LESS;
    if (areaId2 == NULL)
        return OSPF_GREATER;

    switch (UtilIpAddrComp (areaId1, areaId2))
    {
        case OSPF_EQUAL:
            if (nbrId1 == NULL)
                return OSPF_LESS;
            if (nbrId2 == NULL)
                return OSPF_GREATER;
            return (UtilIpAddrComp (nbrId1, nbrId2));

        case OSPF_LESS:
            return OSPF_LESS;

        case OSPF_GREATER:
            return OSPF_GREATER;

        default:
            return INVALID;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindIf                                               */
/*                                                                           */
/* Description  : Determines the interface associated with index value       */
/*                u4IfIndex.                                               */
/*                                                                           */
/* Input        : u4IfIndex   : Index value whose interface is to be       */
/*                                determined.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the interface if exists.                        */
/*                NULL, otherwise.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC tInterface  *
UtilFindIf (UINT4 u4IfIndex)
{
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pLstNode;

    /*
     * Instead of looking at the hash table, search is performed onthe
     * sorted interface list 
     */
    TMO_SLL_Scan (&(gOsRtr.sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {

        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pInterface->u4IfIndex == u4IfIndex)
        {
            return pInterface;
        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilExtractLsHeaderFromPkt                            */
/*                                                                           */
/* Description  : Extracts the lsHeader information from OSPF packet        */
/*                (CRU_BUF_CHAIN).                                           */
/*                                                                           */
/* Input        : pPkt           : pointer to CRU_BUF_CHAIN_HEADER having   */
/*                                  OSPF packet.                             */
/*                pLsHeader     : Pointer to lsHeader                     */
/*                                                                           */
/* Output       : Filled lsHeader                                           */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilExtractLsHeaderFromPkt (tCRU_BUF_CHAIN_HEADER * pPkt,
                            tLsHeader * pLsHeader,
                            INT4 i4Offset, INT4 i4PktType)
{
    /* PktType = 0 DDP packet 
     * PktType = 1 LAK Packet */
    if (i4PktType == 0)
    {                            /* for the DDP packet header processing */
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_AGE_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaAge);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_OPTIONS_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaOptions);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_TYPE_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u1LsaType);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->linkStateId,
                                            (DDP_LSA_ID_OFFSET +
                                             (20 * i4Offset)), MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->advRtrId,
                                            (DDP_LSA_ADV_RTR_ID_OFFSET +
                                             (20 * i4Offset)), MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_SEQ_NUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaSeqNum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_CHKSUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaChksum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (DDP_LSA_LEN_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaLen);
    }
    else                        /* for the LAK packet header processing */
    {
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (LSA_AGE_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaAge);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (LSA_OPTIONS_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaOptions);
        OSPF_CRU_BMC_EXTRACT_1_BYTE_IN_CRU (pPkt,
                                            (LSA_TYPE_OFFSET + (20 * i4Offset)),
                                            pLsHeader->u1LsaType);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->linkStateId,
                                            (LSA_ID_OFFSET + (20 * i4Offset)),
                                            MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_STRING_IN_CRU (pPkt, pLsHeader->advRtrId,
                                            (LSA_ADV_RTR_ID_OFFSET +
                                             (20 * i4Offset)), MAX_IP_ADDR_LEN);
        OSPF_CRU_BMC_EXTRACT_4_BYTE_IN_CRU (pPkt,
                                            (LSA_SEQ_NUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->lsaSeqNum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (LSA_CHKSUM_OFFSET +
                                             (20 * i4Offset)),
                                            pLsHeader->u2LsaChksum);
        OSPF_CRU_BMC_EXTRACT_2_BYTE_IN_CRU (pPkt,
                                            (LSA_LEN_OFFSET + (20 * i4Offset)),
                                            pLsHeader->u2LsaLen);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilExtractLsHeaderFromLbuf                           */
/*                                                                           */
/* Description  : Extracts LSA information from linear buffer                */
/*                                                                           */
/* Input        : pLsa           : Pointer to linear buffer                 */
/*                pLsHeader     : Pointer to lsHeader                     */
/*                                                                           */
/* Output       : Filled lsHeader                                           */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilExtractLsHeaderFromLbuf (UINT1 *pLsa, tLsHeader * pLsHeader)
{

    pLsHeader->u2LsaAge = LGET2BYTE (pLsa);
    pLsHeader->lsaOptions = LGET1BYTE (pLsa);
    pLsHeader->u1LsaType = LGET1BYTE (pLsa);
    LGETSTR (pLsa, pLsHeader->linkStateId, MAX_IP_ADDR_LEN);
    LGETSTR (pLsa, pLsHeader->advRtrId, MAX_IP_ADDR_LEN);
    pLsHeader->lsaSeqNum = LGET4BYTE (pLsa);
    pLsHeader->u2LsaChksum = LGET2BYTE (pLsa);
    pLsHeader->u2LsaLen = LGET2BYTE (pLsa);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpAddrNComp                                        */
/*                                                                           */
/* Description  : Compares a byte wise comparison to a max of u1Len.        */
/*                                                                           */
/* Input        : pIpAddr1     :  IP address 1                             */
/*                pIpAddr2     :  IP address 2                             */
/*                u1Len         :  n no. of bytes to be compared            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :  OSPF_EQUAL, if the IP addresses are equal                 */
/*                 OSPF_GREATER, if IP address 1 is greater                  */
/*                 OSPF_LESS, if IP address 2 is greater                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
UtilIpAddrNComp (tIPADDR * pIpAddr1, tIPADDR * pIpAddr2, UINT1 u1Len)
{

    UINT1               u1Index;

    for (u1Index = 0; u1Index < u1Len; u1Index++)
    {

        if (*((UINT1 *) ((UINT1 *) pIpAddr1 + u1Index)) <
            *((UINT1 *) ((UINT1 *) pIpAddr2 + u1Index)))
        {
            return OSPF_LESS;
        }
        else if (*((UINT1 *) ((UINT1 *) pIpAddr1 + u1Index)) >
                 *((UINT1 *) ((UINT1 *) pIpAddr2 + u1Index)))
        {
            return OSPF_GREATER;
        }
    }
    return OSPF_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilCalculateChksum                                      */
/*                                                                           */
/* Description  : This procedure calculates the checksum for the given OSPF  */
/*                packet. If the length of the packet is odd a dummy byte is */
/*                added at the end before calling the exact checksum calcula-*/
/*                tion routine.                                              */
/*                                                                           */
/* Input        : pBuf                         : pointer to message         */
/*                u2Len                        : length of LSA              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Checksum                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT2
UtilCalculateChksum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len)
{

    return (UtlIpCSumCruBuf (pBuf, u2Len, 0));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilMod255                                               */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                a modulo 255 operator using rule of 9's which is much      */
/*                faster than generic '%' (C mod operator)                   */
/*                                                                           */
/* Input        : i4Sum            : value whose mod is to be found         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Result of the mod operation                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
UtilMod255 (INT4 i4Sum)
{

    register UINT1     *pu1_ptr = (UINT1 *) &i4Sum;
    register INT4       i4Res = 0;
    INT1                i1Neg = OSPF_FALSE;

    if (i4Sum < 0)
    {

        i4Sum = -i4Sum;
        i1Neg = OSPF_TRUE;
    }
    i4Res = *pu1_ptr++;
    i4Res += *pu1_ptr++;
    i4Res += *pu1_ptr++;
    i4Res += *pu1_ptr;

    i4Res = (i4Res & 0xff) + (i4Res >> 8);
    i4Res = (i4Res & 0xff) + (i4Res >> 8);

    if (i4Res == 255)
        i4Res = 0;
    if (i1Neg == OSPF_TRUE)
        i4Res = ~i4Res;

    return (i4Res);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilComputeLsaFletChksum                               */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                Generates a fletcher's checksum and inserts in lsa.This    */
/*                procedure is written specifically for the OSPF lsa format. */
/*                The lsa should be present in a linear buffer.              */
/*                                                                           */
/* Input        : pLsa          : pointer to LSA                            */
/*                u2Len         : length of LSA                             */
/*                                                                           */
/* Output       : LSA updated with the computed fletched checksum            */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilComputeLsaFletChksum (UINT1 *pLsa, UINT2 u2Len)
{

    register INT4       i4C0 = 0;
    register INT4       i4C1 = 0;
    register INT4       i4Cmul;
    register UINT1     *pCurr;
    register UINT1     *pEnd;
    INT4                i4X;
    INT4                i4Y;

    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER) = 0;
    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER + 1) = 0;

    pEnd = pLsa + u2Len;

    /* age field is exempted from checksum calculation */

    for (pCurr = pLsa + AGE_SIZE_IN_LS_HEADER; pCurr < pEnd; pCurr++)
    {

        i4C0 += *pCurr;
        i4C1 += i4C0;
    }

    i4C0 = UtilMod255 (i4C0);
    i4C1 = UtilMod255 (i4C1);

    i4Cmul = (u2Len - CHKSUM_OFFSET_IN_LS_HEADER) * (long) i4C0;
    i4X = UtilMod255 (i4Cmul - i4C0 - i4C1);
    i4Y = UtilMod255 (i4C1 - i4Cmul);

    if (i4X == 0)
        i4X = 255;
    if (i4Y == 0)
        i4Y = 255;

    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER) = (UINT1) (i4X & 0xff);
    *(pLsa + CHKSUM_OFFSET_IN_LS_HEADER + 1) = (UINT1) (i4Y & 0xff);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilVerifyLsaFletChksum                                */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure verifies the checksum of the lsa. This proc-*/
/*                edure is written specifically for the OSPF lsa format. The */
/*                lsa is assumed to be in a linear buffer. This procedure    */
/*                returns OSPF_TRUE for success and OSPF_FALSE for checksum  */
/*                failure.                                                   */
/*                                                                           */
/* Input        : pLsa         : pointer to LSA                             */
/*                u2Len        : length of LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if checksum is ok                               */
/*                OSPF_FALSE, if checksum is not ok                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
UtilVerifyLsaFletChksum (UINT1 *pLsa, UINT2 u2Len)
{

    register UINT4      u4C0 = 0;
    register UINT4      u4C1 = 0;
    register UINT1     *pCurr;
    UINT2               u2ByteCount = 0;

    /* age field is exempted from checksum calculation */

    pCurr = pLsa + AGE_SIZE_IN_LS_HEADER;
    u2Len -= AGE_SIZE_IN_LS_HEADER;
    while (u2Len)
    {
        u2ByteCount = u2Len > 20 ? 20 : u2Len;
        u2Len -= u2ByteCount;
        do
        {
            u4C0 += *pCurr;
            u4C1 += u4C0;
            pCurr++;
        }
        while (--u2ByteCount);

        /* mod 255 adjustment */

        u4C0 = UtilMod255 (u4C0);
        u4C1 = UtilMod255 (u4C1);
    }
    if (!u4C0 && !u4C1)
    {

        return (OSPF_TRUE);
    }

    return (OSPF_FALSE);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindIsTransitArea                                */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure verifies if area is transit are-*/
/*                                                                           */
/* Input        : pArea         : pointer to area                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TRUE, if transit area                               */
/*                OSPF_FALSE, if not transit area                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
UtilFindIsTransitArea (tArea * pArea)
{
    tTMO_SLL_NODE      *pLstNode;
    tInterface         *pInterface;

    TMO_SLL_Scan (&(gOsRtr.virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if (UtilIpAddrComp (pInterface->transitAreaId, pArea->areaId)
            == OSPF_EQUAL)
        {
            return OSPF_TRUE;
        }
    }

    return OSPF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilJitter                                                */
/*                                                                           */
/* Description  : Determines the jitter value based on the jitter percent    */
/*                and adds it to the value to be jittered.                   */
/*                                                                           */
/* Input        : u4Value            :  value to be jittered                */
/*                u4JitterPcent     :  jitter percent                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Modified value after jittering                             */
/*                                                                           */
/*****************************************************************************/

UINT4
UtilJitter (UINT4 u4Value, UINT4 u4JitterPcent)
{
    UINT4               u4Temp;
    static UINT4        su4Next = 1;

    su4Next = su4Next * 1103515245 + 12345;
    u4Temp =
        (((100
           - u4JitterPcent) +
          ((su4Next / 65536) % u4JitterPcent)) * (u4Value)) / 100;
    if (u4Temp == 0)
    {
        u4Temp = 1;
    }
    return (u4Temp);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilAddrMaskCmpe                                        */
/*                                                                           */
/* Description  : This procedure compares two ip address after applying the  */
/*                mask.                                                      */
/*                                                                           */
/* Input        : addr1 : IP address 1                                       */
/*                mask1 : IP address mask 1                                  */
/*                addr2 : IP address 2                                       */
/*                mask2 : IP address mask 2                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_EQUAL, if both are equal                              */
/*                OSPF_GREATER, if addr1 and mask1 is greater                */
/*                OSPF_LESS, if addr2 and mask2 is greater                   */
/*****************************************************************************/

PUBLIC UINT1
UtilAddrMaskCmpe (UINT4 addr1, UINT4 mask1, UINT4 addr2, UINT4 mask2)
{

    UINT4               u4Addr1, u4Addr2;

    /* returns OSPF_EQUAL if they are equal
     *         OSPF_GREATER if addr1 and mask1 is greater
     *         OSPF_LESS if addr1 and mask1 is lesser
     */

    u4Addr1 = (addr1) & (mask1);
    u4Addr2 = (addr2) & (mask2);

    if (u4Addr1 > u4Addr2)
        return OSPF_GREATER;
    else if (u4Addr1 < u4Addr2)
        return OSPF_LESS;

    return (OSPF_EQUAL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetFindHost                                                */
/*                                                                           */
/* Description  : This procedure Create the host Entry with default values   */
/*                otherwise returns the already present Entry                */
/*                                                                           */
/* Input        : pHostAddr : Pointer to Host Ip Address.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to Host Entry Created or Already present           */
/*                Otherwise returns NULL if the creation of new host         */
/*                entry failes.                                              */
/*****************************************************************************/
PUBLIC tHost       *
SetFindHost (tIPADDR * pHostAddr)
{
    tHost              *pHost;

    if ((pHost = GetFindHost (pHostAddr)) == NULL)
    {
        /* If entry is not found new one is created */
        if ((pHost = HostAdd (pHostAddr, 0)) == NULL)
        {
            return NULL;
        }
    }
    return pHost;
}

PUBLIC VOID
AuthKeyCopy (UINT1 *pu1Dst, UINT1 *pu1Src, INT4 i4Len)
{
    INT4                i4Count;

    for (i4Count = 0; i4Count < MAX_AUTHKEY_LEN; i4Count++)
    {
        if (i4Count < i4Len)
            *(pu1Dst + i4Count) = *(pu1Src + i4Count);
        else
            *(pu1Dst + i4Count) = (UINT1) 0;
    }

}

PUBLIC tMd5AuthkeyInfo *
Md5AuthKeyInfoCreate ()
{
    tMd5AuthkeyInfo    *pAuthkeyInfo;
    if (MD5AUTH_ALLOC (&(pAuthkeyInfo)) == MEM_FAILURE)
        return NULL;

    UINT4               u4Ospf1sTimer = 0;
    tUtlTm              tm;

    /* Getting the system time for u4Ospf1sTimer */
    UtlGetTime (&tm);
    u4Ospf1sTimer = GrGetSecondsSinceBase (tm);

    pAuthkeyInfo->u4KeyStartAccept = u4Ospf1sTimer;
    pAuthkeyInfo->u4KeyStartGenerate = u4Ospf1sTimer;
    pAuthkeyInfo->u4KeyStopGenerate = DEF_KEY_STOP_GENERATE;
    pAuthkeyInfo->u4KeyStopAccept = DEF_KEY_STOP_ACCEPT;
    pAuthkeyInfo->u1AuthkeyStatus = AUTHKEY_STATUS_VALID;
    return (pAuthkeyInfo);
}

PUBLIC VOID
SortInsertMd5AuthKeyInfo (tTMO_SLL * pMd5authkeyLst,
                          tMd5AuthkeyInfo * pAuthkeyInfo)
{
    UINT1               u1AuthkeyId;
    tTMO_SLL_NODE      *pPrev = NULL;
    tMd5AuthkeyInfo    *pTmpAuthkeyInfo;
    tTMO_SLL_NODE      *pLstNode;

    u1AuthkeyId = pAuthkeyInfo->u1AuthkeyId;
    TMO_SLL_Scan (pMd5authkeyLst, pLstNode, tTMO_SLL_NODE *)
    {
        pTmpAuthkeyInfo = (tMd5AuthkeyInfo *) pLstNode;
        if (pTmpAuthkeyInfo->u1AuthkeyId < u1AuthkeyId)
            pPrev = pLstNode;
        if (pTmpAuthkeyInfo->u1AuthkeyId > u1AuthkeyId)
            break;
    }
    TMO_SLL_Insert (pMd5authkeyLst, pPrev, (tTMO_SLL_NODE *) pAuthkeyInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RrdConfRtInfoCreate                                     */
/*                                                                           */
/* Description     : This procedure allocates memory for                     */
/*                   tRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : pRrdDestIPAddr    - Dest IP Address                     */
/*                   pRrdDestAddrMask  - Dest IP Addr Mask                   */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to the new entry, if successful                 */
/*                   NULL, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tRedistrConfigRouteInfo *
RrdConfRtInfoCreate (tIPADDR * pRrdDestIPAddr, tIPADDRMASK * pRrdDestAddrMask)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;
    tRedistrConfigRouteInfo *pLstRrdConfRtInfo;
    tRedistrConfigRouteInfo *pPrvRrdConfRtInfo;
    UINT1               u1FoundFlag;

    u1FoundFlag = OSPF_FALSE;
    pPrvRrdConfRtInfo = NULL;

    if ((REDISTR_CONFIG_INFO_ALLOC (&pRrdConfRtInfo)) == MEM_FAILURE)
    {

        return NULL;
    }

    IP_ADDR_COPY (pRrdConfRtInfo->rrdDestIPAddr, *pRrdDestIPAddr);
    IP_ADDR_COPY (pRrdConfRtInfo->rrdDestAddrMask, *pRrdDestAddrMask);

    /* Initialize to the default values */
    pRrdConfRtInfo->u4RrdRouteMetricValue = AS_EXT_DEF_METRIC;
    pRrdConfRtInfo->u1RrdRouteMetricType = TYPE_2_METRIC;
    pRrdConfRtInfo->u1RedistrTagType = MANUAL_TAG;
    pRrdConfRtInfo->u4ManualTagValue = 0;

    /* Add to the SLL in ascending order */
    TMO_SLL_Scan (&(gOsRtr.RedistrRouteConfigLst), pLstRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {

        switch (UtilIpAddrComp (pRrdConfRtInfo->rrdDestIPAddr,
                                pLstRrdConfRtInfo->rrdDestIPAddr))
        {
            case OSPF_LESS:
                u1FoundFlag = OSPF_TRUE;
                break;

            case OSPF_EQUAL:
                switch (UtilIpAddrComp (pRrdConfRtInfo->rrdDestAddrMask,
                                        pLstRrdConfRtInfo->rrdDestAddrMask))
                {
                    case OSPF_LESS:
                        u1FoundFlag = OSPF_TRUE;
                        break;

                    case OSPF_EQUAL:

                        REDISTR_CONFIG_INFO_FREE (pRrdConfRtInfo);
                        return (pLstRrdConfRtInfo);

                    case OSPF_GREATER:
                        pPrvRrdConfRtInfo = pLstRrdConfRtInfo;
                        break;
                }                /* switch 2 */
                break;

            case OSPF_GREATER:
                pPrvRrdConfRtInfo = pLstRrdConfRtInfo;
                break;
        }                        /* switch 1 */

        if (u1FoundFlag == OSPF_TRUE)
            break;
    }

    TMO_SLL_Insert (&(gOsRtr.RedistrRouteConfigLst),
                    &(pPrvRrdConfRtInfo->nextRrdConfigRoute),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    return pRrdConfRtInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RrdConfRtInfoDeleteAll                                 */
/*                                                                           */
/* Description     : This procedure deallocates memory for                     */
/*                   tRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : None                                                  */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RrdConfRtInfoDeleteAll (void)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;

    TMO_SLL_Scan (&(gOsRtr.RedistrRouteConfigLst), pRrdConfRtInfo,
                  tRedistrConfigRouteInfo *)
    {
        RrdConfRtInfoDelete (&pRrdConfRtInfo->rrdDestIPAddr,
                             &pRrdConfRtInfo->rrdDestAddrMask);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RrdConfRtInfoDelete                                     */
/*                                                                           */
/* Description     : This procedure deallocates memory for                   */
/*                   tRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : pRrdDestIPAddr    - Dest IP Address                     */
/*                   pRrdDestAddrMask  - Dest IP Addr Mask                   */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSPF_SUCCESS / OSPF_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RrdConfRtInfoDelete (tIPADDR * pRrdDestIPAddr, tIPADDRMASK * pRrdDestAddrMask)
{
    tRedistrConfigRouteInfo *pRrdConfRtInfo;

    if ((pRrdConfRtInfo = GetFindRrdConfRtInfo (pRrdDestIPAddr,
                                                pRrdDestAddrMask)) == NULL)
    {
        return OSPF_FAILURE;
    }

    TMO_SLL_Delete (&(gOsRtr.RedistrRouteConfigLst),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    REDISTR_CONFIG_INFO_FREE (pRrdConfRtInfo);

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilFindAssoAllAreaAddrRange                             */
/*                                                                           */
/* Description  : This procedure associates the specified IP addr with one of*/
/*                the router's configured areas based on which address range */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pAddr        : IP address                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tAddrRange  *
UtilFindAssoAllAreaAddrRange (tIPADDR * pAddr)
{

    UINT1               u1Index;
    tArea              *pArea;

    TMO_SLL_Scan (&(gOsRtr.areasLst), pArea, tArea *)
    {
        for (u1Index = 0; u1Index < MAX_ADDR_RANGES_PER_AREA; u1Index++)
        {

            if ((pArea->aAddrRange[u1Index].areaAggStatus !=
                 INVALID)
                &&
                (UtilIpAddrMaskComp
                 (*pAddr, pArea->aAddrRange[u1Index].ipAddr,
                  pArea->aAddrRange[u1Index].ipAddrMask) == OSPF_EQUAL))
            {

                return (&(pArea->aAddrRange[u1Index]));
            }
        }
    }

    return NULL;
}

/************************************************************************
 *  Function Name   : OspfConvertTime
 *  Description     : This function will validate the time
                      by parsing through the string against DD-MON-YEAR,HH:MM
                      format and convert the time in string to structure.
 *  Input           : pu1TimeStr -
 *                    tm - Time structure to set or get time of key constants.
 *  Output          : None
 *  Returns         : OSPF_SUCCESS/OSPF_FAILURE
 ************************************************************************/

INT4
OspfConvertTime (UINT1 *pu1TimeStr, tUtlTm * tm)
{
    UINT1               au1Temp[OSPF_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (au1Temp, OSPF_ZERO, sizeof (au1Temp));
    /* Parsing through the function  for date */
    if (pu1CurrPtr != NULL)
    {
        /* Checking for date */
        MEMCPY (au1Temp, pu1BasePtr, 2);
        if ((ISDIGIT (pu1BasePtr[OSPF_ZERO]))    /* Check if date is an integer */
            && (ISDIGIT (pu1BasePtr[OSPF_ONE])))
        {
            tm->tm_mday = ATOI (au1Temp);
            tm->tm_yday = tm->tm_mday;
            /* Checking for the validity of Date */
            if ((tm->tm_mday < OSPF_ONE)
                || (tm->tm_mday > OSPF_MAX_MONTH_DAYS + 1))
            {
                return OSPF_INAVLID_DATE;
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INAVLID_DATE;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-MON-YEAR,HH:MM" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPF_ZERO] == '-')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "MON-YEAR,HH:MM" */
        }
        else
        {
            return OSPF_FAILURE;
        }
        MEMSET (au1Temp, OSPF_ZERO, sizeof (au1Temp));
        MEMCPY (au1Temp, pu1BasePtr, 3);
        /* Month Validations  and check if date is valid for respective month */
        if (STRCASECMP (au1Temp, "Jan") == 0)
        {
            tm->tm_mon = 0;
        }
        else if (STRCASECMP (au1Temp, "Feb") == 0)
        {

            tm->tm_mon = 1;
            tm->tm_yday += OSPF_MAX_MONTH_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Mar") == 0)
        {
            tm->tm_mon = 2;
            tm->tm_yday += OSPF_MAX_MONTH_DAYS;

        }
        else if (STRCASECMP (au1Temp, "Apr") == 0)
        {
            if (tm->tm_mday > OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INAVLID_DATE;
            }
            tm->tm_mon = 3;
            tm->tm_yday += OSPF_MAX_MAR_DAYS;
        }
        else if (STRCASECMP (au1Temp, "May") == 0)
        {
            tm->tm_mon = 4;
            tm->tm_yday += OSPF_MAX_APR_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Jun") == 0)
        {
            if (tm->tm_mday >= OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INAVLID_DATE;
            }
            tm->tm_mon = 5;
            tm->tm_yday += OSPF_MAX_MAY_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Jul") == 0)
        {
            tm->tm_mon = 6;
            tm->tm_yday += OSPF_MAX_JUN_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Aug") == 0)
        {
            tm->tm_mon = 7;
            tm->tm_yday += OSPF_MAX_JUL_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Sep") == 0)
        {
            if (tm->tm_mday >= OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INAVLID_DATE;
            }
            tm->tm_mon = 8;
            tm->tm_yday += OSPF_MAX_AUG_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Oct") == 0)
        {
            tm->tm_mon = 9;
            tm->tm_yday += OSPF_MAX_SEP_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Nov") == 0)
        {
            if (tm->tm_mday >= OSPF_MIN_MONTH_DAYS + 1)
            {
                return OSPF_INAVLID_DATE;
            }
            tm->tm_mon = 10;
            tm->tm_yday += OSPF_MAX_OCT_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Dec") == 0)
        {
            tm->tm_mon = 11;
            tm->tm_yday += OSPF_MAX_NOV_DAYS;
        }
        else
        {
            return OSPF_INAVLID_MONTH;
        }
        /* End of Month Validation */
    }
    else
    {
        return OSPF_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-YEAR,HH:MM" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPF_ZERO] == '-')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "YEAR,HH:MM" */
        }
        else
        {
            return OSPF_FAILURE;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[OSPF_ZERO]))    /* Check if year is integer or not */
            && (ISDIGIT (pu1CurrPtr[OSPF_ONE]))
            && (ISDIGIT (pu1CurrPtr[2])) && (ISDIGIT (pu1CurrPtr[3])))
        {
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
            MEMCPY (au1Temp, pu1CurrPtr, 4);
            tm->tm_year = ATOI (au1Temp);

            /* Checking for the validity of Year */
            if (tm->tm_year < OSPF_BASE_YEAR)
            {
                return OSPF_INAVLID_YEAR;
            }
            if (tm->tm_mon >= 2)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    tm->tm_yday += OSPF_MAX_LEAP_FEB_DAYS;

                }
                else
                {
                    tm->tm_yday += OSPF_MIN_LEAP_FEB_DAYS;
                }
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INAVLID_YEAR;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');    /*pu1CurrPtr postioned at ",HH:MM" */

    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPF_ZERO] == ',')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /*pu1CurrPtr postioned at "HH:MM" */
        }
        else
        {
            return OSPF_FAILURE;
        }
        if ((ISDIGIT (pu1CurrPtr[OSPF_ZERO]))    /* check if hour is integer */
            && (ISDIGIT (pu1CurrPtr[OSPF_ONE])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_hour = ATOI (au1Temp);
            /* Checking for the validity of Hour */
            if (tm->tm_hour > OSPF_MAX_HOUR)
            {
                return OSPF_INAVLID_HOUR;
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INAVLID_HOUR;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":MM" */

    if (pu1CurrPtr != NULL)
    {
        /* Checking for Separator */
        if (pu1CurrPtr[OSPF_ZERO] == ':')
        {
            pu1CurrPtr++;        /* pu1CurrPtr postioned at "MM" */
        }
        else
        {
            return OSPF_INAVLID_HOUR;
        }
    }

    if (pu1CurrPtr != NULL)
    {

        if ((ISDIGIT (pu1CurrPtr[OSPF_ZERO]))    /* Check if min is integer or not */
            && (ISDIGIT (pu1CurrPtr[OSPF_ONE])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_min = ATOI (au1Temp);
            /* Checking for the validity of Min */
            if (tm->tm_min > OSPF_MAX_MIN)
            {
                return OSPF_INAVLID_MIN;
            }
            MEMSET (au1Temp, OSPF_ZERO, OSPF_TMP_NUM_STR);
        }
        else
        {
            return OSPF_INAVLID_MIN;
        }
    }
    else
    {
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/************************************************************************
 *  Function Name   : OspfPrintKeyTime
 *  Description     : This function will convert the time in structure to
                      string.
 *  Input           : u4Secs -Seconds
 *  Output          : pOspfKeyTime- Returns the Value in array
 *  Returns         : NONE
 ************************************************************************/
VOID
OspfPrintKeyTime (INT4 u4Secs, UINT1 *pOspfKeyTime)
{
    tUtlTm              tm;
    UINT1               au1Mon[12];

    MEMSET (au1Mon, OSPF_ZERO, sizeof (au1Mon));
    MEMSET (&tm, OSPF_ZERO, sizeof (tUtlTm));

    UtlGetTimeForSeconds (u4Secs, &tm);    /* Tm structure is filled for secs */

/* Month is filled in array and year_day is reduced to date in this process*/
    if (tm.tm_mon == 0)
    {
        STRCPY (au1Mon, "Jan");
        tm.tm_yday = tm.tm_yday;
    }
    else if (tm.tm_mon == 1)
    {
        STRCPY (au1Mon, "Feb");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MONTH_DAYS;
    }
    else if (tm.tm_mon == 2)
    {
        STRCPY (au1Mon, "Mar");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MONTH_DAYS;
    }
    else if (tm.tm_mon == 3)
    {
        STRCPY (au1Mon, "Apr");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MAR_DAYS;
    }
    else if (tm.tm_mon == 4)
    {
        STRCPY (au1Mon, "May");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_APR_DAYS;
    }
    else if (tm.tm_mon == 5)
    {
        STRCPY (au1Mon, "Jun");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_MAY_DAYS;
    }
    else if (tm.tm_mon == 6)
    {
        STRCPY (au1Mon, "Jul");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_JUN_DAYS;
    }
    else if (tm.tm_mon == 7)
    {
        STRCPY (au1Mon, "Aug");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_JUL_DAYS;
    }
    else if (tm.tm_mon == 8)
    {
        STRCPY (au1Mon, "Sep");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_AUG_DAYS;
    }
    else if (tm.tm_mon == 9)
    {
        STRCPY (au1Mon, "Oct");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_SEP_DAYS;
    }
    else if (tm.tm_mon == 10)
    {
        STRCPY (au1Mon, "Nov");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_OCT_DAYS;
    }
    else if (tm.tm_mon == 11)
    {
        STRCPY (au1Mon, "Dec");
        tm.tm_yday = tm.tm_yday - OSPF_MAX_NOV_DAYS;
    }

    if (tm.tm_mon >= 2)            /* In the case of Leap Year for the month of Feb Year_Day is reduced */
    {
        if (IS_LEAP (tm.tm_year))
        {
            tm.tm_yday = tm.tm_yday - OSPF_MAX_LEAP_FEB_DAYS;
        }
        else
        {
            tm.tm_yday = tm.tm_yday - OSPF_MIN_LEAP_FEB_DAYS;
        }
    }
    /* Values are printed in the structure */
    SPRINTF ((CHR1 *) pOspfKeyTime,
             "%.2u %s %4u %.2u:%.2u \r\n\r\n",
             tm.tm_yday, au1Mon, tm.tm_year, tm.tm_hour, tm.tm_min);

}

/*---------------------------------------------------------------------------*/
/*                          End of file osutil.c                             */
/*---------------------------------------------------------------------------*/
