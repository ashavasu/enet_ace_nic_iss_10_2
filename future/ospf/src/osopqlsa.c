/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osopqlsa.c,v 1.10 2014/10/29 12:51:00 siva Exp $
 *
 * Description:This file contains utility routines associated with
 *             OSPF Opaque LSA.
 *             
 *
 *
 ******************************************************************************/
#include "osinc.h"
#include "osprot.h"
/******************************************************************************
Function        :   olsGetType9OpqLsa

Description     :   This function checks for the existence of a Type 9 LSA for 
                    the given combinaiton of u4FutOspfType9LsdbAreaId,
                    u4FutOspfType9LsdbIfIpAddress,
                    u4FutOspfType9LsdbIfAddressLessIf,
                    u4FutOspfType9LsdbLsid, u4FutOspfType9LsdbRouterId. In case
                    of existence of a LSA the pointer to the LSA Descriptor
                    is returned otherwise NULL is returned.

Input           :   u4FutOspfType9LsdbIfIpAddress
                    u4FutOspfType9LsdbIfAddressLessIf                    
                    u4FutOspfType9LsdbLsid
                    u4FutOspfType9LsdbRouterId 

Output          :   None

Returns         :   Pointer to the LSA Info in case of existence of a type 9 LSA
                    otherwise NULL. 
******************************************************************************/
tLsaInfo           *
olsGetType9OpqLsaInCxt (tOspfCxt * pOspfCxt,
                        UINT4 u4FutOspfType9LsdbIfIpAddress,
                        UINT4 u4FutOspfType9LsdbIfAddressLessIf,
                        UINT4 u4FutOspfType9LsdbLsid,
                        UINT4 u4FutOspfType9LsdbRouterId)
{
    tIPADDR             ifIpAddr;
    tInterface         *pInterface;
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLsaNode;

    OSPF_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfType9LsdbIfIpAddress);
    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfType9LsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4FutOspfType9LsdbRouterId);

    pInterface = GetFindIfInCxt (pOspfCxt,
                                 ifIpAddr, u4FutOspfType9LsdbIfAddressLessIf);
    if (pInterface != NULL)
    {
        TMO_SLL_Scan (&(pInterface->Type9OpqLSALst), pLsaNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
            if (UtilLsaIdComp
                (TYPE9_OPQ_LSA, linkStateId, advRtrId,
                 pLsaInfo->lsaId.u1LsaType,
                 pLsaInfo->lsaId.linkStateId,
                 pLsaInfo->lsaId.advRtrId) == OSPF_EQUAL)
            {
                return (pLsaInfo);
            }
        }
    }
    return NULL;
}

/******************************************************************************
Function        :   olsGetType11OpqLsa

Description     :   This function checks for the existence of a Type 11 LSA for 
                    the given combinaiton of i4FutOspfType11LsdbOpaqueType,
                    u4FutOspfType11LsdbLsid, u4FutOspfType11LsdbRouterId. In 
                    case of existence of a LSA the pointer to the LSA Descriptor
                    is returned otherwise NULL is returned.

Input           :   i4FutOspfType11LsdbOpaqueType,
                    u4FutOspfType11LsdbLsid,
                    u4FutOspfType11LsdbRouterId

Output          :   None

Returns         :   Pointer to the LSA Info in case of existence of a type 11 
                    LSA otherwise NULL. 
******************************************************************************/
tLsaInfo           *
olsGetType11OpqLsaInCxt (tOspfCxt * pOspfCxt,
                         INT4 i4FutOspfType11LsdbOpaqueType,
                         UINT4 u4FutOspfType11LsdbLsid,
                         UINT4 u4FutOspfType11LsdbRouterId)
{
    tLINKSTATEID        linkStateId;
    tRouterId           advRtrId;
    tLsaInfo           *pLsaInfo;
    tTMO_SLL_NODE      *pLsaNode;

    /* The following assignemnt is to avoid compilation warning.
       The value of i4FutOspfType11LsdbOpaqueType is part of the
       u4FutOspfType11LsdbLsid, and hence it is not required to be
       used explicitly and not used in this routine. */
    UNUSED_PARAM(i4FutOspfType11LsdbOpaqueType);

    OSPF_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfType11LsdbLsid);
    OSPF_CRU_BMC_DWTOPDU (advRtrId, u4FutOspfType11LsdbRouterId);
    TMO_SLL_Scan (&(pOspfCxt->Type11OpqLSALst), pLsaNode, tTMO_SLL_NODE *)
    {

        pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
        if (UtilLsaIdComp (TYPE11_OPQ_LSA, linkStateId, advRtrId,
                           pLsaInfo->lsaId.u1LsaType,
                           pLsaInfo->lsaId.linkStateId,
                           pLsaInfo->lsaId.advRtrId) == OSPF_EQUAL)
        {
            return (pLsaInfo);
        }
    }
    return NULL;
}

                                                                                                            /******************************************************************************/
/* LOW LEVEL Routines for all objects */
PUBLIC INT1
GetIsNextType11LsaTableIndex (tLINKSTATEID * pCurrLinkStateId,
                              UINT1 u1LsIdLen,
                              tRouterId * pCurrAdvRtrId,
                              UINT1 u1AdvRtrIdLen, tLsaInfo * pLsaInfo)
{
    /* compare link state id */
    switch (UtilIpAddrNComp (&(pLsaInfo->lsaId.linkStateId),
                             pCurrLinkStateId, u1LsIdLen))
    {
        case OSPF_LESS:
            return OSPF_FALSE;
        case OSPF_GREATER:
            return OSPF_TRUE;
        case OSPF_EQUAL:
            if (u1LsIdLen < MAX_IP_ADDR_LEN)
            {
                return OSPF_TRUE;
            }
    }
    /* compare advertising router id */
    switch (UtilIpAddrNComp (&(pLsaInfo->lsaId.advRtrId),
                             pCurrAdvRtrId, u1AdvRtrIdLen))
    {
        case OSPF_LESS:
            return OSPF_FALSE;
        case OSPF_GREATER:
            return OSPF_TRUE;
        case OSPF_EQUAL:
            if (u1AdvRtrIdLen < MAX_IP_ADDR_LEN)
            {
                return OSPF_TRUE;
            }
    }
    return OSPF_FALSE;
}

/*----------------------------------------------------------------------------*/
/*                          End of file osopqlsa.c                            */
/*----------------------------------------------------------------------------*/
