/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osopqset.c,v 1.15 2016/03/30 11:24:20 siva Exp $
 *
 * Description:This file contains the Snmp low level set routines for the
 *             the Opaque LSA support related objects in the 
 *             FutureOSPF Enterprises MIB.
 *
 *******************************************************************/
#include "osinc.h"
#include "fsmioscli.h"
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfOpaqueOption
 Input       :  The Indices

                The Object 
                setValFutOspfOpaqueOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfOpaqueOption (INT4 i4SetValFutOspfOpaqueOption)
{
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;
    UINT4               u4ArrayIndx;
    tAppInfo           *pAppInfo;
    tOspfCxt           *pOspfCxt = gOsRtr.pOspfCxt;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pOspfCxt->u1OpqCapableRtr != (UINT1) i4SetValFutOspfOpaqueOption)
    {

        if (i4SetValFutOspfOpaqueOption == OSPF_DISABLED)
        {
            for (u4ArrayIndx = 0; u4ArrayIndx < MAX_APP_REG; u4ArrayIndx++)
            {
                pAppInfo = APP_INFO_IN_CXT (pOspfCxt, (UINT1) u4ArrayIndx);
                if (pAppInfo != NULL)
                {
                    if (pAppInfo->u1Status == OSPF_VALID)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
        }

        pOspfCxt->u1OpqCapableRtr = (UINT1) i4SetValFutOspfOpaqueOption;

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            return SNMP_FAILURE;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsMIOspfOpaqueOption;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIOspfOpaqueOption) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = OspfLock;
        SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
        SnmpNotifyInfo.u4Indices = 1;
#endif
        pOspfIfParam->u1OpCode = OSPF_CHNG_OPQ_STATUS;
        pOspfIfParam->pOspfCxt = pOspfCxt;
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            i1RetVal = SNMP_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.i1ConfStatus = i1RetVal;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gOsRtr.u4OspfCxtId,
                          i4SetValFutOspfOpaqueOption));
#endif
    }

    return i1RetVal;
}

/* ChangeForPorting - Application is always expected to provide Area Id for */
/* Type10 Opq Lsa. Function is stub.                                        */
/****************************************************************************
 Function    :  nmhSetFutOspfAreaIDValid
 Input       :  The Indices

                The Object 
                

 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfAreaIDValid (INT4 i4SetValFutOspfAreaIDValid)
{
    UNUSED_PARAM (i4SetValFutOspfAreaIDValid);

    return SNMP_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                          End of file osopqset.c                            */
/*----------------------------------------------------------------------------*/
