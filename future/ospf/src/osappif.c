/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osappif.c,v 1.30 2017/09/21 13:48:45 siva Exp $
 *
 * Description:This file contains procedures related to Opaque Module
 *             
 *
 *******************************************************************/

#include  "osinc.h"
#include "osappif.h"
#include "osbufif.h"

PRIVATE VOID OspfDeleteAllOpqLsasInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE VOID OspfDeleteType10Lsas PROTO ((tArea * pArea));
PRIVATE VOID OspfDeleteType11LsasInCxt PROTO ((tOspfCxt * pOspfCxt));
PRIVATE UINT1      *IsValidInputFromOpqLsaInfo PROTO
    ((tOpqLSAInfo * pOpqLSAInfo));

/***************************************************************************/
/*  Function    :  OpqAppToOpqModuleSend                                   */
/*  Input       :  pOpqLSAInfo,                                            */
/*  Output      :  None                                                    */
/*  Description :  Application generates an Opaque LSA which is passed to  */
/*                 the Opaque Module.The Send Message is posted to the APPQ*/
/*  Returns     :  OSPF_SUCCESS or OSPF_FAILURE                            */
/****************************************************************************/
INT4
OpqAppToOpqModuleSendInCxt (UINT4 u4OspfCxtId, tOpqLSAInfo * pOpqLSAInfo)
{

    tAppOspfIfParam    *pAppOspfIfParam;
    tOpqLSAInfo        *pOpqLsaInfo;
    tOspfQMsg          *pMsg = NULL;    /* passed on to Opq module through APPQ */

    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: OpqAppToOpqModuleSend\n");

    if (OPQ_LSA_INFO_ALLOC (pOpqLsaInfo) == NULL)
    {
        return OSPF_FAILURE;
    }

    /* initialise the fields */

    OS_MEM_CPY (pOpqLsaInfo, pOpqLSAInfo, sizeof (tOpqLSAInfo));

    if (pOpqLSAInfo->u2LSALen > MAX_LSA_SIZE)
    {
        OPQ_LSA_INFO_FREE (pOpqLsaInfo);
        OSPF_EXT_TRC (OS_RESOURCE_TRC, pOpqLsaInfo->pOspfCxt->u4OspfCxtId,
                      "OPQ LSA alloc failed\r\n");
        return OSPF_FAILURE;
    }

    LSA_ALLOC (TYPE10_OPQ_LSA, pOpqLsaInfo->pu1LSA, pOpqLSAInfo->u2LSALen);
    if (pOpqLsaInfo->pu1LSA == NULL)
    {
        OPQ_LSA_INFO_FREE (pOpqLsaInfo);
        return OSPF_FAILURE;
    }
    OS_MEM_CPY (pOpqLsaInfo->pu1LSA, pOpqLSAInfo->pu1LSA,
                pOpqLSAInfo->u2LSALen);

    pMsg = OSPF_APP_IF_MSG_ALLOC ();

    if (pMsg == NULL)
    {
        OspfOpqLSAFree (pOpqLsaInfo);
        return OSPF_FAILURE;
    }

    /* entries of tAppOspfIfParam filled up */
    pAppOspfIfParam = (tAppOspfIfParam *) OSPF_APP_IF_MSG_GET_PARAMS_PTR (pMsg);
    pAppOspfIfParam->u1AppOpCode = SND_OPQ_LSA_FROM_APP_TO_OSPF;
    pAppOspfIfParam->AppParam.AppTxOpqLsaParam.pOpqLSAInfo = pOpqLsaInfo;
    pAppOspfIfParam->u4OspfCxtId = u4OspfCxtId;

    if (OSPF_APP_IF_MSG_SEND (pMsg) != OSIX_SUCCESS)
    {
        OSPF_APP_IF_MSG_FREE (pMsg);
        OspfOpqLSAFree (pOpqLsaInfo);
        return OSPF_FAILURE;
    }
    /* event sent to AppOspfIfHandler */
    OSPF_APP_IF_EVENT_SEND ();
    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: OpqAppToOpqModuleSend\n");
    KW_FALSEPOSITIVE_FIX (pOpqLsaInfo);
    return OSPF_SUCCESS;

}

/****************************************************************************/
/*  Function    :  OpqAppDeRegister                                         */
/*  Input       :  u1AppOpqType                                             */
/*  Output      :  None                                                     */
/*  Description :  Application Deregisters by posting a message to the APPQ */
/*  Returns     :  OSPF_SUCCESS or OSPF_FAILURE                             */
/****************************************************************************/
INT4
OpqAppDeRegisterInCxt (UINT4 u4OspfCxtId, UINT1 u1AppOpqType)
{
    tAppOspfIfParam    *pAppOspfIfParam = NULL;
    tOspfQMsg          *pMsg = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: OpqAppDeRegister\n");
    /* Posting of deregister message to the queue */
    pMsg = OSPF_APP_IF_MSG_ALLOC ();

    if (pMsg == NULL)
    {
        return OSPF_FAILURE;
    }

    pAppOspfIfParam = (tAppOspfIfParam *) OSPF_APP_IF_MSG_GET_PARAMS_PTR (pMsg);
    pAppOspfIfParam->u1AppOpCode = OPQ_APP_DEREG;
    pAppOspfIfParam->AppParam.AppDeRegParm.u1AppOpqtype = u1AppOpqType;
    pAppOspfIfParam->u4OspfCxtId = u4OspfCxtId;

    if (OSPF_APP_IF_MSG_SEND (pMsg) != OSIX_SUCCESS)
    {
        OSPF_APP_IF_MSG_FREE (pMsg);
        return OSPF_FAILURE;
    }
    /* event sent to AppOspfIfHandler */
    OSPF_APP_IF_EVENT_SEND ();

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: OpqAppDeRegister\n");
    return OSPF_SUCCESS;
}

/***************************************************************************/
/* Function    :  OpqAppRegisterInCxt                                      */
/* Input       :  u1AppOpqType,                                            */
/*                u1OpqLSATypesRegistered                                  */
/*                u4InfoFromOSPF                                           */
/*                OpqAppCallBackFn                                         */
/* Output      :  pu4OpqAppID                                              */
/* Description :  Application is registering with the Opaque Module        */
/* Returns     :  OSPF_SUCCESS or OSPF_FAILURE                             */
/***************************************************************************/
INT4
OpqAppRegisterInCxt (UINT4 u4OspfCxtId, UINT1 u1AppOpqType,
                     UINT1 u1OpqLSATypesRegistered,
                     UINT4 u4InfoFromOSPF, VOID *OpqAppCallBackFn)
{
    tAppInfo           *pAppInfo = NULL;
    tOspfQMsg          *pMsg = NULL;
    tAppOspfIfParam    *pAppOspfIfParam = NULL;
    tOspfCxt           *pOspfCxt = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC: OpqAppRegisterInCxt\n");

    OspfLock ();

    if ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL)
    {
        OspfUnLock ();
        return OSPF_FAILURE;
    }

    /* Check for Opaque Capability */
    if (pOspfCxt->u1OpqCapableRtr == OSPF_FALSE)
    {
        OspfUnLock ();
        return OSPF_FAILURE;
    }

    if (IS_VALID_OPQ_LSA_TYPE_SUPPORT (u1OpqLSATypesRegistered))
    {
        OspfUnLock ();
        return OSPF_FAILURE;
    }
    if (!(u1AppOpqType < MAX_APP_REG))
    {
        OspfUnLock ();
        return OSPF_FAILURE;
    }
    pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppOpqType);

    if (pAppInfo != NULL)
    {
        OspfUnLock ();
        return OSPF_FAILURE;
    }

    if (APP_ALLOC (pAppInfo) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                  u4OspfCxtId, "APP Alloc Failure\n");
        OspfUnLock ();
        return OSPF_FAILURE;
    }

    pOspfCxt->pAppInfo[u1AppOpqType] = pAppInfo;

    pAppInfo->u4AppId = (UINT4) u1AppOpqType;
    pAppInfo->u1OpqType = u1AppOpqType;
    pAppInfo->u1LSATypesRegistered = u1OpqLSATypesRegistered;
    pAppInfo->u4InfoFromOSPF = u4InfoFromOSPF;
    pAppInfo->OpqAppCallBackFn = OpqAppCallBackFn;
    pAppInfo->u1Status = OSPF_VALID;

    OspfUnLock ();

    pMsg = OSPF_APP_IF_MSG_ALLOC ();

    if (pMsg == NULL)
    {
        return OSPF_FAILURE;
    }

    pAppOspfIfParam = (tAppOspfIfParam *) OSPF_APP_IF_MSG_GET_PARAMS_PTR (pMsg);
    pAppOspfIfParam->u1AppOpCode = OPQ_APP_REG;
    pAppOspfIfParam->AppParam.AppRegParm.u1AppOpqType = u1AppOpqType;
    pAppOspfIfParam->u4OspfCxtId = u4OspfCxtId;

    if (OSPF_APP_IF_MSG_SEND (pMsg) != OSIX_SUCCESS)
    {
        OSPF_APP_IF_MSG_FREE (pMsg);
        return OSPF_FAILURE;
    }
    /* event sent to AppOspfIfHandler */
    OSPF_APP_IF_EVENT_SEND ();

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT: OpqAppRegisterInCxt\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/* Function     : AppOspfIfHandler                                           */
/* Description  : Routine to deque messages enqued by application and take   */
/*                appropriate action.                                        */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/

PUBLIC VOID
AppOspfIfHandler (tAppOspfIfParam * pAppOspfIfParam)
{
    UINT4               u4OpqAppId;
    tOspfCxt           *pOspfCxt = NULL;
    tAppInfo           *pAppInfo = NULL;
    tOpqLSAInfo        *pOpqLSAInfo = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pAppOspfIfParam->u4OspfCxtId,
              "FUNC: AppOspfIfHandler \n ");
    /* Check for received messages */
    if ((pOspfCxt = UtilOspfGetCxt (pAppOspfIfParam->u4OspfCxtId)) == NULL)
    {
        OPQ_LSA_MSG_FREE (pAppOspfIfParam);
        return;
    }
    if (pOspfCxt->u1OpqCapableRtr == OSPF_FALSE)
    {
        OPQ_LSA_MSG_FREE (pAppOspfIfParam);
        return;
    }
    if (pAppOspfIfParam->u1AppOpCode == SND_OPQ_LSA_FROM_APP_TO_OSPF)
    {
        pOpqLSAInfo = pAppOspfIfParam->AppParam.AppTxOpqLsaParam.pOpqLSAInfo;
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, pOpqLSAInfo->LsId[0]);

        if (pAppInfo == NULL)
        {
            OspfOpqLSAFree (pOpqLSAInfo);
            return;
        }
        u4OpqAppId = (UINT4) pAppInfo->u1OpqType;

        if ((pAppInfo->u4AppId != u4OpqAppId)
            || (pAppInfo->u1Status == OSPF_INVALID))
        {
            OspfOpqLSAFree (pOpqLSAInfo);
            return;
        }

        switch (pOpqLSAInfo->u1OpqLSAType)
        {

            case TYPE9_OPQ_LSA:
                if (!IS_TYPE9 (pAppInfo->u1LSATypesRegistered))
                {
                    OspfOpqLSAFree (pOpqLSAInfo);
                    return;
                }
                break;
            case TYPE10_OPQ_LSA:
                if (!IS_TYPE10 (pAppInfo->u1LSATypesRegistered))
                {
                    OspfOpqLSAFree (pOpqLSAInfo);
                    return;
                }
                break;
            case TYPE11_OPQ_LSA:
                if (!IS_TYPE11 (pAppInfo->u1LSATypesRegistered))
                {
                    OspfOpqLSAFree (pOpqLSAInfo);
                    return;
                }
                break;
            default:
                OspfOpqLSAFree (pOpqLSAInfo);
                return;
        }

    }

    switch (pAppOspfIfParam->u1AppOpCode)
    {

        case SND_OPQ_LSA_FROM_APP_TO_OSPF:
            pOpqLSAInfo->pOspfCxt = pOspfCxt;
            MsgFrmAPPQSndToOpqMod (pOpqLSAInfo);
            break;

        case OPQ_APP_DEREG:
            OpqAppDeRegMsgFrmQInCxt (pOspfCxt, pAppOspfIfParam->AppParam.
                                     AppDeRegParm.u1AppOpqtype);
            break;

        case OPQ_APP_REG:
            OpqAppRegMsgFrmQInCxt (pOspfCxt, pAppOspfIfParam->AppParam.
                                   AppRegParm.u1AppOpqType);
            break;

    }

    OSPF_TRC (OSPF_FN_EXIT, pAppOspfIfParam->u4OspfCxtId,
              "EXIT: AppOspfIfHandler\n");
}

/****************************************************************************/
/* Function     :  OpqAppRegMsgFrmQ                                         */
/* Input        :  u4OpqAppID                                               */
/*                i1OpqStatus                                               */
/* Output       :  None                                                     */
/* Description  :  The Application Registeration message processing happens */
/* Returns      :  OSPF_SUCCESS or OSPF_FAILURE                             */
/****************************************************************************/
INT4
OpqAppRegMsgFrmQInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqtype)
{
    tAppInfo           *pAppInfo = NULL;
    tArea              *pArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tNeighbor          *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    tInterface         *pInterface = NULL;

    OSPF_TRC1 (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
               "FUNC: OpqAppRegMsgFrmQ \n APPLICATION AppId:%d", u1AppOpqtype);
    if (!(u1AppOpqtype < MAX_APP_REG))
    {
        return OSPF_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        /* Scan All the interfaces in the Area */
        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_LST (pLstNode);
            /* If the interface is Point to Point type,
             * Give the Neighbor IP address information */
            if (pInterface->u1NetworkType == IF_PTOP)
            {
                if ((pNbrNode = TMO_SLL_First (&pInterface->nbrsInIf)) != NULL)
                {
                    pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
                    NbrSendNbrInfoInfoToOpqApp (pNbr);
                }
            }
        }
        pLsaInfo = LsuSearchDatabase (ROUTER_LSA, &(pOspfCxt->rtrId),
                                      &(pOspfCxt->rtrId), NULL,
                                      (UINT1 *) pArea);
        if (pLsaInfo != NULL)
        {
            LsuSendLsaInfoInfoToOpqApp (pLsaInfo, NEW_LSA);
        }

        TMO_SLL_Scan (&(pArea->networkLsaLst), pLsaNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = GET_LSA_INFO_PTR_FROM_LST (pLsaNode);
            LsuSendLsaInfoInfoToOpqApp (pLsaInfo, NEW_LSA);
        }
    }

    pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppOpqtype);

    if (IS_TYPE11 (pAppInfo->u1LSATypesRegistered))
    {
        OspfGiveType11LsaToAppInCxt (pOspfCxt, u1AppOpqtype);
    }

    if (IS_TYPE9 (pAppInfo->u1LSATypesRegistered))
    {
        OspfGiveType9LsaToAppInCxt (pOspfCxt, u1AppOpqtype);
    }

    if (IS_TYPE10 (pAppInfo->u1LSATypesRegistered))
    {
        OspfGiveType10LsaToAppInCxt (pOspfCxt, u1AppOpqtype);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: OpqAppRegMsgFrmQ\n");
    return OSPF_SUCCESS;
}

/****************************************************************************/
/* Function    :  OpqAppDeRegMsgFrmQ                                        */
/* Input       :  u1AppOpqType                                                */
/* Output      :  None                                                      */
/*Description  :  The Application generated LSAs on Deregisteration are     */
/*                flushed from LSDB                                         */
/* Returns     :  OSPF_SUCCESS or OSPF_FAILURE                              */
/****************************************************************************/
INT4
OpqAppDeRegMsgFrmQInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqtype)
{
    tAppInfo           *pAppInfo = NULL;
    UINT4               u4OpqAppId;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OpqAppDeRegMsgFrmQ\n");
    if (!(u1AppOpqtype < MAX_APP_REG))
    {
        gu4AppOpqtypeFail++;
        return OSPF_FAILURE;
    }
    /* Checking for valid array index */
    pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppOpqtype);
    if (pAppInfo == NULL)
    {
        gu4AppInfoInCxtFail++;
        return OSPF_FAILURE;
    }
    u4OpqAppId = (UINT4) u1AppOpqtype;

    if ((pAppInfo->u4AppId != u4OpqAppId)
        || (pAppInfo->u1Status == OSPF_INVALID))
    {
        return OSPF_FAILURE;
    }

    pOspfCxt->pAppInfo[u1AppOpqtype] = NULL;

    if (IS_TYPE11 (pAppInfo->u1LSATypesRegistered))
    {
        OspfFlushType11LsaInCxt (pOspfCxt, u1AppOpqtype);
    }

    if (IS_TYPE9 (pAppInfo->u1LSATypesRegistered))
    {
        OspfFlushType9LsaInCxt (pOspfCxt, u1AppOpqtype);
    }
    if (IS_TYPE10 (pAppInfo->u1LSATypesRegistered))
    {
        OspfFlushType10LsaInCxt (pOspfCxt, u1AppOpqtype);
    }

    APP_FREE (pAppInfo);
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: OpqAppDeRegMsgFrmQ\n");
    return OSPF_SUCCESS;
}

/****************************************************************************/
/* Function    :  OspfDeRegMsgToOpqApp                                      */
/* Input       :  pOspfCxt                                                  */
/* Output      :  None                                                      */
/*Description  :  Ospf module will going shutdown and send Deregistration   */
/*           message to Registered Application                */
/* Returns     :  OSPF_SUCCESS or OSPF_FAILURE                              */
/****************************************************************************/
INT4
OspfDeRegMsgToOpqApp (tOspfCxt * pOspfCxt)
{
    tAppInfo           *pAppInfo = NULL;
    VOID                (*fnptr) (tOsToOpqApp *) = NULL;
    tOsToOpqApp         ospfTeMsg;
    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId,
              "FUNC: OspfDeRegMsgToOpqApp\n");

    /* Checking for valid array index */
    pAppInfo = APP_INFO_IN_CXT (pOspfCxt, OSPF_TE_OPQ_TYPE);
    if (pAppInfo == NULL)
    {
        gu4AppInfoInCxtFail++;
        return OSPF_FAILURE;
    }
    if ((pAppInfo->u4AppId != (UINT4) OSPF_TE_OPQ_TYPE)
        || (pAppInfo->u1Status == OSPF_INVALID))
    {
        return OSPF_FAILURE;
    }
    /* Send Deregestration message to OSPFTE module */
    ospfTeMsg.u4MsgSubType = OSPF_REGISTER_WITH_OSPFTE;

    fnptr = (VOID (*)(tOsToOpqApp *)) pAppInfo->OpqAppCallBackFn;
    /* call the call back function to send message to
     * Opaque application */
    if (fnptr != NULL)
    {
        fnptr (&ospfTeMsg);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId,
              "EXIT: OspfDeRegMsgToOpqApp\n");
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/* Function    :  MsgFrmAPPQSndToOpqMod                                      */
/* Input       :  pOpqLSAInfo,                                               */
/* Output      :  None                                                       */
/* Description :  Application generates an Opaque LSA.Message is in APPQ     */
/*                which is to be passed to the Opaque Module                 */
/* Returns     :  OSPF_SUCCESS or OSPF_FAILURE                               */
/*****************************************************************************/
INT4
MsgFrmAPPQSndToOpqMod (tOpqLSAInfo * pOpqLSAInfo)
{
    tLsaInfo           *pLsaInfo = NULL;
    tAppInfo           *pAppInfo;
    UINT1              *pPtr;
    UINT4               u4OspfCxtId = OSPF_ZERO;

    u4OspfCxtId = pOpqLSAInfo->pOspfCxt->u4OspfCxtId;

    OSPF_TRC (OSPF_FN_ENTRY, u4OspfCxtId, "FUNC:MsgFrmAPPQSndToOpqMod \n");

    if (OSPF_IS_NODE_ACTIVE () != OSPF_TRUE)
    {
        /* node is not active so return */
        OspfOpqLSAFree (pOpqLSAInfo);
        return (OSPF_SUCCESS);
    }
    pAppInfo = APP_INFO_IN_CXT (pOpqLSAInfo->pOspfCxt, pOpqLSAInfo->LsId[0]);

    /* checking for validity of Opaque type          */
    if (pAppInfo->u1OpqType == (pOpqLSAInfo->LsId[0]))
    {
        if (((pPtr = IsValidInputFromOpqLsaInfo (pOpqLSAInfo)) != NULL) ||
            (pOpqLSAInfo->u1OpqLSAType == TYPE11_OPQ_LSA))
        {
            if (pOpqLSAInfo->u1OpqLSAType == TYPE9_OPQ_LSA)
            {
                pLsaInfo = LsuSearchDatabase (pOpqLSAInfo->u1OpqLSAType,
                                              &(pOpqLSAInfo->LsId),
                                              &(pOpqLSAInfo->pOspfCxt->rtrId),
                                              pPtr, NULL);
            }
            else if (pOpqLSAInfo->u1OpqLSAType == TYPE11_OPQ_LSA)
            {
                pLsaInfo = LsuSearchDatabase (pOpqLSAInfo->u1OpqLSAType,
                                              &(pOpqLSAInfo->LsId),
                                              &(pOpqLSAInfo->pOspfCxt->rtrId),
                                              NULL,
                                              (UINT1 *) pOpqLSAInfo->pOspfCxt->
                                              pBackbone);
            }
            else
            {
                pLsaInfo = LsuSearchDatabase (pOpqLSAInfo->u1OpqLSAType,
                                              &(pOpqLSAInfo->LsId),
                                              &(pOpqLSAInfo->pOspfCxt->rtrId),
                                              NULL, pPtr);
            }

            switch (pOpqLSAInfo->u1LSAStatus)
            {
                case NEW_LSA:
                    if (pLsaInfo != NULL)
                    {
                        if ((pLsaInfo->pLsaDesc != NULL) &&
                            (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL))
                        {
                            OspfOpqLSAFree ((tOpqLSAInfo *) (VOID *)
                                            pLsaInfo->pLsaDesc->pAssoPrimitive);
                            pLsaInfo->pLsaDesc->pAssoPrimitive =
                                (UINT1 *) pOpqLSAInfo;
                        }
                        pLsaInfo->u1LSAStatus = pOpqLSAInfo->u1LSAStatus;
                        pLsaInfo->u1LsaFlushed = OSPF_FALSE;
                    }
                    OlsSignalLsaRegenInCxt (pOpqLSAInfo->pOspfCxt,
                                            SIG_OPQ_LSA_GEN,
                                            (UINT1 *) pOpqLSAInfo);
                    break;
                case NEW_LSA_INSTANCE:

                    if (pLsaInfo == NULL)
                    {
                        OspfOpqLSAFree (pOpqLSAInfo);
                        return (OSPF_FAILURE);
                    }

                    if (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL)
                    {
                        OspfOpqLSAFree ((tOpqLSAInfo *) (VOID *)
                                        pLsaInfo->pLsaDesc->pAssoPrimitive);
                        pLsaInfo->pLsaDesc->pAssoPrimitive =
                            (UINT1 *) pOpqLSAInfo;
                    }
                    else
                    {
                        OspfOpqLSAFree (pOpqLSAInfo);
                        return (OSPF_FAILURE);
                    }

                    OlsSignalLsaRegenInCxt (pOpqLSAInfo->pOspfCxt,
                                            SIG_OPQ_LSA_NEXT_INSTANCE,
                                            (UINT1 *) pOpqLSAInfo);

                    break;
                case FLUSHED_LSA:

                    if (pLsaInfo != NULL)
                    {
                        if (pLsaInfo->pOspfCxt->u1OspfRestartState ==
                            OSPF_GR_NONE)
                        {
                            pLsaInfo->u1LSAStatus = FLUSHED_LSA;
                            AgdFlushOut (pLsaInfo);
                        }
                        else
                        {
                            /* This router is performing Graceful restart 
                               Delete the LSA without flushing */
                            GrDeleteLsaFromDatabase (pLsaInfo);

                        }
                    }
                    OspfOpqLSAFree (pOpqLSAInfo);
                    break;
                default:
                    OspfOpqLSAFree (pOpqLSAInfo);
                    break;
            }                    /* end of switch */
        }
        else
        {
            OspfOpqLSAFree (pOpqLSAInfo);
        }                        /* end of if */
    }
    else
    {
        OspfOpqLSAFree (pOpqLSAInfo);
    }                            /*end of if */

    OSPF_TRC (OSPF_FN_EXIT, u4OspfCxtId, "EXIT:MsgFrmAPPQSndToOpqMod");

    return OSPF_SUCCESS;
}

/****************************************************************************/
/*Function      :OspfOpqLSAFree                                             */
/*Input         :pOpqLSAInfo                                                */
/*Output        :None                                                       */
/*Description   : This function frees the buffer allocated for tOpqLSAInfo  */
/*               structure.This freeing routine is passed to the application*/
/*               by the Opaque module.                                      */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfOpqLSAFree (tOpqLSAInfo * pOpqLSAInfo)
{
    if (pOpqLSAInfo->pu1LSA != NULL)
    {
        LSA_FREE (TYPE10_OPQ_LSA, pOpqLSAInfo->pu1LSA);
    }
    OPQ_LSA_INFO_FREE (pOpqLSAInfo);
}                                /* end of function */

/****************************************************************************/
/*Function      :opqStatusChng                                              */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function is invoked on any dynamic change in         */
/*                the OpaqueCapability of the router.I                    */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
opqStatusChngInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;
    tInterface         *pInterface;
    tNeighbor          *pNbr;
    tTMO_SLL_NODE      *pIfNode;
    tTMO_SLL_NODE      *pNbrNode;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfCxt->u4OspfCxtId, "FUNC: opqStatusChng\n");
    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_LST (pIfNode);

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                if ((pNbr != NULL) && (pNbr->u1NsmState > NBRS_EXSTART))
                {
                    OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                               pOspfCxt->u4OspfCxtId,
                               "NBRE: Sequence number mismatch for Neighbour with Address = %x\n",
                               OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
                    GENERATE_NBR_EVENT (pNbr, NBRE_SEQ_NUM_MISMATCH);
                }
            }
        }
    }
    if (pOspfCxt->u1OpqCapableRtr == OSPF_FALSE)
    {
        OspfDeleteAllOpqLsasInCxt (pOspfCxt);
    }

    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: opqStatusChng\n");
}

/****************************************************************************/
/*Function      :OspfGiveType11LsaToAppInCxt                               */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function gives the Type 11 Lsa to Application        */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfGiveType11LsaToAppInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqType)
{
    tTMO_SLL_NODE      *pType11LSANode;
    tLsaInfo           *pType11LSAInfo;
    tTMO_SLL_NODE      *pTempLSANode;

    /* Type11 Opaque LSAs of a specific Opaque Type are accessed */
    OSPF_DYNM_SLL_SCAN (&(pOspfCxt->Type11OpqLSALst),
                        pType11LSANode, pTempLSANode, tTMO_SLL_NODE *)
    {

        pType11LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType11LSANode);
        /* Checking for Opaque Type */
        if ((pType11LSAInfo->lsaId.linkStateId[0]) == u1AppOpqType)
        {
            LsuSendLsaInfoInfoToOpqApp (pType11LSAInfo, NEW_LSA);
        }                        /* End of If */
    }                            /* End of SLL Scan */
}

/****************************************************************************/
/*Function      :OspfGiveType10LsaToAppInCxt                               */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function gives the Type 10 Lsa to Application        */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfGiveType10LsaToAppInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqType)
{
    tTMO_SLL_NODE      *pType10LSANode;
    tTMO_SLL_NODE      *pTempLSANode;
    tLsaInfo           *pType10LSAInfo;
    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        OSPF_DYNM_SLL_SCAN (&(pArea->Type10OpqLSALst),
                            pType10LSANode, pTempLSANode, tTMO_SLL_NODE *)
        {

            pType10LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType10LSANode);

            if ((pType10LSAInfo->lsaId.linkStateId[0]) == u1AppOpqType)
            {
                LsuSendLsaInfoInfoToOpqApp (pType10LSAInfo, NEW_LSA);
            }
        }                        /*End Of SLL Scan */
    }
}

/****************************************************************************/
/*Function      :OspfGiveType9LsaToAppInCxt                                 */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function gives the Type 9 Lsa to Application        */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfGiveType9LsaToAppInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqType)
{
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pType9LSANode;
    tLsaInfo           *pType9LSAInfo;
    tTMO_SLL_NODE      *pTempLSANode;
    tInterface         *pInterface;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        OSPF_DYNM_SLL_SCAN (&(pInterface->Type9OpqLSALst),
                            pType9LSANode, pTempLSANode, tTMO_SLL_NODE *)
        {
            pType9LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType9LSANode);
            if ((pType9LSAInfo->lsaId.linkStateId[0]) == u1AppOpqType)
            {
                LsuSendLsaInfoInfoToOpqApp (pType9LSAInfo, NEW_LSA);
            }
        }
    }                            /* End Of SLL Scan */
}

/****************************************************************************/
/*Function      :OspfFlushType11LsaInCxt                                    */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function Flushed the Type 11 Lsa generated by  Application   */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfFlushType11LsaInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqType)
{
    tTMO_SLL_NODE      *pType11LSANode;
    tLsaInfo           *pType11LSAInfo;
    tTMO_SLL_NODE      *pTempLSANode;

    /* Type11 Opaque LSAs of a specific Opaque Type are accessed */
    OSPF_DYNM_SLL_SCAN (&(pOspfCxt->Type11OpqLSALst),
                        pType11LSANode, pTempLSANode, tTMO_SLL_NODE *)
    {

        pType11LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType11LSANode);
        /* Checking for Opaque Type */
        if ((pType11LSAInfo->lsaId.linkStateId[0]) == u1AppOpqType)
        {
            if (UtilIpAddrComp (pType11LSAInfo->lsaId.advRtrId,
                                pOspfCxt->rtrId) == OSPF_EQUAL)
            {
                if (pType11LSAInfo->pOspfCxt->u1OspfRestartState
                    == OSPF_GR_NONE)
                {
                    AgdFlushOut (pType11LSAInfo);
                }
                else
                {
                    /* This router is performing Graceful restart 
                       Delete the LSA without flushing */
                    GrDeleteLsaFromDatabase (pType11LSAInfo);
                }
            }                    /* End of If */
        }                        /* End of If */

    }                            /* End of SLL Scan */

}

/****************************************************************************/
/*Function      :OspfFlushType10LsaInCxt                               */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function gives the Type 10 Lsa to Application        */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfFlushType10LsaInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqType)
{
    tTMO_SLL_NODE      *pType10LSANode;
    tTMO_SLL_NODE      *pTempLSANode;
    tLsaInfo           *pType10LSAInfo;
    tArea              *pArea;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        OSPF_DYNM_SLL_SCAN (&(pArea->Type10OpqLSALst),
                            pType10LSANode, pTempLSANode, tTMO_SLL_NODE *)
        {

            pType10LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType10LSANode);

            if ((pType10LSAInfo->lsaId.linkStateId[0]) == u1AppOpqType)
            {
                if (UtilIpAddrComp (pType10LSAInfo->lsaId.advRtrId,
                                    pOspfCxt->rtrId) == OSPF_EQUAL)
                {
                    if (pType10LSAInfo->pOspfCxt->u1OspfRestartState
                        == OSPF_GR_NONE)
                    {
                        AgdFlushOut (pType10LSAInfo);
                    }
                    else
                    {
                        /* This router is performing Graceful restart 
                           Delete the LSA without flushing */
                        GrDeleteLsaFromDatabase (pType10LSAInfo);
                    }
                }
            }

        }                        /*End Of SLL Scan */

    }

}

/****************************************************************************/
/*Function      :OspfFlushType9Lsa                                    */
/*Input         :None                                                       */
/*Output        :None                                                       */
/*Description   : This function gives the Type 9 Lsa to Application        */
/*Returns       :None                                                       */
/****************************************************************************/
VOID
OspfFlushType9LsaInCxt (tOspfCxt * pOspfCxt, UINT1 u1AppOpqType)
{
    tTMO_SLL_NODE      *pLstNode;
    tTMO_SLL_NODE      *pType9LSANode;
    tLsaInfo           *pType9LSAInfo;
    tTMO_SLL_NODE      *pTempLSANode;
    tInterface         *pInterface;

    TMO_SLL_Scan (&(pOspfCxt->sortIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = GET_IF_PTR_FROM_SORT_LST (pLstNode);

        OSPF_DYNM_SLL_SCAN (&(pInterface->Type9OpqLSALst),
                            pType9LSANode, pTempLSANode, tTMO_SLL_NODE *)
        {
            pType9LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType9LSANode);
            if ((pType9LSAInfo->lsaId.linkStateId[0]) == u1AppOpqType)
            {
                if (UtilIpAddrComp (pType9LSAInfo->lsaId.advRtrId,
                                    pOspfCxt->rtrId) == OSPF_EQUAL)
                {
                    if (pType9LSAInfo->pOspfCxt->u1OspfRestartState
                        == OSPF_GR_NONE)
                    {
                        AgdFlushOut (pType9LSAInfo);
                    }
                    else
                    {
                        /* This router is performing Graceful restart 
                           Delete the LSA without flushing */
                        GrDeleteLsaFromDatabase (pType9LSAInfo);
                    }
                }
            }

        }

    }                            /* End Of SLL Scan */

}

/****************************************************************************/
/*Function      : OspfDeleteAllOpqLsasInCxt                                 */
/*Input         : None                                                      */
/*Output        : None                                                      */
/*Description   : This function removes All Opaque Lsas from the router     */
/*                database                                                  */
/*Returns       : None                                                      */
/****************************************************************************/
PRIVATE VOID
OspfDeleteAllOpqLsasInCxt (tOspfCxt * pOspfCxt)
{
    tArea              *pArea;
    tInterface         *pInterface;
    tTMO_SLL_NODE      *pIfNode;

    TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
    {
        TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
        {
            pInterface = GET_IF_PTR_FROM_LST (pIfNode);
            OspfDeleteType9Lsas (pInterface);
        }
        OspfDeleteType10Lsas (pArea);
    }
    OspfDeleteType11LsasInCxt (pOspfCxt);
}

/****************************************************************************/
/*Function      : OspfDeleteType9Lsas                                       */
/*Input         : pInterface                                                */
/*Output        : None                                                      */
/*Description   : This function removes the Type 9 Lsa from the router      */
/*                database                                                  */
/*Returns       : None                                                      */
/****************************************************************************/
PUBLIC VOID
OspfDeleteType9Lsas (tInterface * pInterface)
{
    tTMO_SLL_NODE      *pType9LSANode;
    tLsaInfo           *pType9LSAInfo = NULL;
    while (TMO_SLL_Count (&(pInterface->Type9OpqLSALst)) != 0)
    {
        pType9LSANode = TMO_SLL_First (&(pInterface->Type9OpqLSALst));
        pType9LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType9LSANode);
        LsuDeleteLsaFromDatabase (pType9LSAInfo, OSPF_TRUE);
    }
}

/****************************************************************************/
/*Function      : OspfDeleteType10Lsas                                      */
/*Input         : pInterface                                                */
/*Output        : None                                                      */
/*Description   : This function removes the Type 10 Lsa from the router     */
/*                database                                                  */
/*Returns       : None                                                      */
/****************************************************************************/
PRIVATE VOID
OspfDeleteType10Lsas (tArea * pArea)
{
    tTMO_SLL_NODE      *pType10LSANode;
    tLsaInfo           *pType10LSAInfo;

    while (TMO_SLL_Count (&(pArea->Type10OpqLSALst)) != 0)
    {
        pType10LSANode = TMO_SLL_First (&(pArea->Type10OpqLSALst));
        pType10LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType10LSANode);
        LsuDeleteLsaFromDatabase (pType10LSAInfo, OSPF_TRUE);
    }
}

/****************************************************************************/
/*Function      : OspfDeleteType11Lsas                                      */
/*Input         : pInterface                                                */
/*Output        : None                                                      */
/*Description   : This function removes the Type 11 Lsa from the router     */
/*                database                                                  */
/*Returns       : None                                                      */
/****************************************************************************/
PRIVATE VOID
OspfDeleteType11LsasInCxt (tOspfCxt * pOspfCxt)
{
    tTMO_SLL_NODE      *pType11LSANode;
    tLsaInfo           *pType11LSAInfo;

    while (TMO_SLL_Count (&(pOspfCxt->Type11OpqLSALst)) != 0)
    {
        pType11LSANode = TMO_SLL_First (&(pOspfCxt->Type11OpqLSALst));
        pType11LSAInfo = GET_LSA_INFO_PTR_FROM_LST (pType11LSANode);
        LsuDeleteLsaFromDatabase (pType11LSAInfo, OSPF_TRUE);
    }
}

INT4
OpqChkAndChangeStatusInCxt (tOspfCxt * pOspfCxt)
{

    UINT4               u4ArrayIndx;
    tAppInfo           *pAppInfo;

    if (pOspfCxt->u1OpqCapableRtr == OSPF_TRUE)
    {
        opqStatusChngInCxt (pOspfCxt);
    }
    else
    {
        for (u4ArrayIndx = 0; u4ArrayIndx < MAX_APP_REG; u4ArrayIndx++)
        {
            pAppInfo = APP_INFO_IN_CXT (pOspfCxt, (UINT1) u4ArrayIndx);
            if (pAppInfo != NULL)
            {
                if (pAppInfo->u1Status == OSPF_VALID)
                {
                    return OSPF_FAILURE;
                }
            }
        }
        opqStatusChngInCxt (pOspfCxt);
        pOspfCxt->u1RestartSupport = OSPF_RESTART_NONE;
    }
    return OSPF_SUCCESS;
}

PUBLIC tOspfQMsg   *
OpqOspfIfMsgAlloc (VOID)
{
    tOspfQMsg          *pOspfQMsg = NULL;
    QMSG_ALLOC (pOspfQMsg);
    return pOspfQMsg;
}

PUBLIC INT4
OpqOspfIfMsgSend (tOspfQMsg * pOspfQMsg)
{
    pOspfQMsg->u4MsgType = OSPF_APP_IF_MSG;

    return (OsixSendToQ ((UINT4) 0, (const UINT1 *) "OSPQ",
                         (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL));
}

/****************************************************************************/
/*Function      :IsValidInputFromOpqLsaInfo                                   */
/*Input         :pOpqLSAInfo                                                */
/*Output        :None                                                       */
/*Returns       :pLsaInfo                                                   */
/*Description   : This function gets the pLsaInfo from tOpqLsaInfo.         */

/****************************************************************************/
PRIVATE UINT1      *
IsValidInputFromOpqLsaInfo (tOpqLSAInfo * pOpqLSAInfo)
{

    tInterface         *pInterface;
    tArea              *pTmpArea;
    tAreaId             AreaId;
    tIPADDR             IfaceID;

    if (pOpqLSAInfo->u1OpqLSAType == TYPE9_OPQ_LSA)
    {
        OSPF_CRU_BMC_DWTOPDU (IfaceID, pOpqLSAInfo->u4IfaceID);

        pInterface =
            GetFindIfInCxt (pOpqLSAInfo->pOspfCxt, IfaceID,
                            pOpqLSAInfo->u4AddrlessIf);
        /* Check if it is a Valid interface */
        if (pInterface == NULL)
        {
            OSPF_TRC (OSPF_CRITICAL_TRC, pOpqLSAInfo->pOspfCxt->u4OspfCxtId,
                      "FAILURE: Invalid Interface Address\n");
            OSPF_TRC (OSPF_FN_EXIT, pOpqLSAInfo->pOspfCxt->u4OspfCxtId,
                      "EXIT :GetLsaInfoFromOpqLsaInfo\n");
            return NULL;
        }
        else
        {
            return (UINT1 *) pInterface;
        }

    }

    if (pOpqLSAInfo->u1OpqLSAType == TYPE10_OPQ_LSA)
    {
        OSPF_CRU_BMC_DWTOPDU (AreaId, pOpqLSAInfo->u4AreaID);
        pTmpArea = GetFindAreaInCxt (pOpqLSAInfo->pOspfCxt, &AreaId);
        if (pTmpArea == NULL)
        {
            OSPF_TRC (OSPF_CRITICAL_TRC, pOpqLSAInfo->pOspfCxt->u4OspfCxtId,
                      "FAILURE: Invalid area Address\n");
            OSPF_TRC (OSPF_FN_EXIT, pOpqLSAInfo->pOspfCxt->u4OspfCxtId,
                      "EXIT :GetLsaInfoFromOpqLsaInfo\n");
            return NULL;
        }
        else
        {
            return (UINT1 *) pTmpArea;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  IsOpqApplicationRegisteredInCxt
 Input       :  pOspfCxt - Pointer to tOspfCxt structure
 Description :  This is used to find whether opaque application is registered
 Returns     :  OSPF_SUCCESS/OSPF_FAILURE
****************************************************************************/
INT1
IsOpqApplicationRegisteredInCxt (tOspfCxt * pOspfCxt)
{
    tAppInfo           *pAppInfo;
    UINT4               u4ArrayIndx;

    if (pOspfCxt == NULL)
    {
        return OSPF_FAILURE;
    }
    for (u4ArrayIndx = 0; u4ArrayIndx < MAX_APP_REG; u4ArrayIndx++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, (UINT1) u4ArrayIndx);
        if (pAppInfo != NULL)
        {
            if (pAppInfo->u1Status == OSPF_VALID)
            {
                return OSPF_SUCCESS;
            }
        }
    }

    return OSPF_FAILURE;

}

/****************************************************************************
 Function    :  AsbrSendAsbrInfoToOpqApp
 Input       :  pOspfCxt - Pointer to tOspfCxt structure
                u4AsbrStatus - ASBR Status that should be passed to Opq Appln 
 Description :  This function sends ASBR status Info to registered 
                Opq Application with u4MsgSubType value set as 
                OSPF_TE_ASBR_STATUS_INFO. 
 Returns     :  NONE
****************************************************************************/
PUBLIC VOID
AsbrSendAsbrInfoToOpqApp (tOspfCxt * pOspfCxt, UINT4 u4AsbrStatus)
{
    tAppInfo           *pAppInfo;
    VOID                (*fnptr) (tOsToOpqApp *) = NULL;
    tOsToOpqApp         ospfTeMsg;
    UINT1               u1AppId;

    MEMSET (&ospfTeMsg, 0, sizeof (tOsToOpqApp));

    /* Scan the list of application and give the ASBR Info if they have 
     * registered for receiving that information */
    for (u1AppId = 0; u1AppId < MAX_APP_REG; u1AppId++)
    {
        pAppInfo = APP_INFO_IN_CXT (pOspfCxt, u1AppId);
        if ((pAppInfo != NULL) && (pAppInfo->u1Status == OSPF_VALID))
        {
            /* If the Opaque application is registered
             * for recieving ASBR information then give it */
            if (pAppInfo->u4InfoFromOSPF & OSPF_ASBR_INFO_TO_OPQ_APP)
            {
                ospfTeMsg.u4MsgSubType = OSPF_TE_ASBR_STATUS_INFO;
                ospfTeMsg.asbrInfo.u4ASBdrRtrStatus = u4AsbrStatus;

                fnptr = (VOID (*)(tOsToOpqApp *)) pAppInfo->OpqAppCallBackFn;
                /* call the call back function to send message to
                 *                  * Opaque application */
                if (fnptr != NULL)
                {
                    fnptr (&ospfTeMsg);
                }
            }
        }
    }

    return;
}

/*------------------------------------------------------------------------*/
/*                      End of file osappif.c                             */
/*------------------------------------------------------------------------*/
