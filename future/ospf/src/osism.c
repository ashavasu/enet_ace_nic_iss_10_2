/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osism.c,v 1.17 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             interface state machine.
 *
 *******************************************************************/

#include "osinc.h"

/* Contains static include */
#include "osism.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmRunIsm                                                */
/*                                                                           */
/* Description  : Using the state of the interface and the interface event as*/
/*                index we obtain an integer from the state event table. This*/
/*                integer when indexed into an array of function pointers    */
/*                specifies the actual function to be invoked.               */
/*                                                                           */
/* Input        : pInterface       :  pointer to interface                  */
/*                u1IfEvent       :  event                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmRunIsm (tInterface * pInterface, UINT1 u1IfEvent)
{
    if (!((u1IfEvent < MAX_IF_EVENT) &&
          (pInterface->u1IsmState < MAX_IF_STATE)))
    {
        return;
    }
    if (pInterface->u1NetworkType < MAX_IF_TYPE)
    {
        OSPF_TRC5 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "ISM If %x.%d N/W Type %s CS %s Evt %s\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u4AddrlessIf,
                   au1DbgIfType[pInterface->u1NetworkType],
                   au1DbgIfState[pInterface->u1IsmState],
                   au1DbgIfEvent[u1IfEvent]);
    }

    /* SEM Function is being executed now */
    (*a_ism_func[au1_ism_table[u1IfEvent][pInterface->u1IsmState]])
        (pInterface);

    if (pInterface->u1IsmState < MAX_IF_STATE)
    {
        OSPF_TRC3 (CONTROL_PLANE_TRC | OSPF_ISM_TRC,
                   pInterface->pArea->pOspfCxt->u4OspfCxtId,
                   "ISM If %x.%d NS %s\n",
                   OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
                   pInterface->u4AddrlessIf,
                   au1DbgIfState[pInterface->u1IsmState]);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmInvalid                                                */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.3                           */
/*               This function is invoked when an event which is not expected*/
/*               in a particular interface state is said to have occurred.   */
/*               This procedure generates appropriate error message.         */
/*                                                                           */
/* Input        : pInterface        : pointer to interface                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmInvalid (tInterface * pInterface)
{

    COUNTER_OP (pInterface->aIfEvents, 1);

    OSPF_TRC2 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d Invalid Event\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmIfUp                                                  */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.3                           */
/*                This procedure starts the hello timer for this interface.  */
/*                The next state depends on the type of the network. If the  */
/*                network is NBMA then neighbor event start is generated for */
/*                all neighbors.                                             */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmIfUp (tInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode;
    tNeighbor          *pNbr = (tNeighbor *) NULL;
    tOpqLSAInfo         opqLSAInfo;
    UINT2               u2HelloInterval;
    UINT1               u1PollFlag = OSPF_FALSE;

    OSPF_TRC2 (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "FUNC:IsmIfUp : If %x.%d \n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);

    u2HelloInterval = pInterface->u2HelloInterval;

    if (IS_DC_EXT_APPLICABLE_PTOP_IF (pInterface))
    {
        /* since PTOP_IF, only one neighbour will be in nbrsInIf list */
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);
        }
        if (pNbr != NULL && pNbr->u1NsmState >= NBRS_INIT)
        {
            IfUpdateState (pInterface, IFS_PTOP);
        }
        else
        {
            u1PollFlag = OSPF_TRUE;
        }
    }
    else if ((pInterface->u1NetworkType == IF_PTOP) ||
             (pInterface->u1NetworkType == IF_PTOMP))
    {
        IfUpdateState (pInterface, IFS_PTOP);
    }
    else if (pInterface->u1NetworkType == IF_VIRTUAL)
    {
        IfUpdateState (pInterface, IFS_PTOP);
    }
    else if (pInterface->u1RtrPriority == INELIGIBLE_RTR_PRIORITY)
    {
        IfUpdateState (pInterface, IFS_DR_OTHER);
    }
    else
    {

        /* multi-access */

        IfUpdateState (pInterface, IFS_WAITING);
        TmrSetTimer (&(pInterface->waitTimer), WAIT_TIMER,
                     NO_OF_TICKS_PER_SEC * (pInterface->i4RtrDeadInterval));
        if (pInterface->u1NetworkType == IF_NBMA)
        {
            u1PollFlag = OSPF_TRUE;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {

                pNbr = GET_NBR_PTR_FROM_NBR_LST (pNbrNode);

                /* Eligible neighbors are present at the start of the nbr list */

                if (pNbr->u1NbrRtrPriority == INELIGIBLE_RTR_PRIORITY)
                {
                    break;
                }
                OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                           pInterface->pArea->pOspfCxt->u4OspfCxtId,
                           "NBRE: Eligible neighbors are present with Address = %x\n",
                           OSPF_CRU_BMC_DWFROMPDU (pNbr->nbrIpAddr));
                GENERATE_NBR_EVENT (pNbr, NBRE_START);
            }
        }
        else
        {

            /* broadcast */

            u2HelloInterval = (UINT2) UtilJitter (u2HelloInterval, OSPF_JITTER);
        }
    }

    if ((pInterface->bPassive == OSPF_FALSE) ||
        (pInterface->u1NetworkType == IF_VIRTUAL))
    {
        if ((u1PollFlag == OSPF_TRUE) ||
            (IS_DC_EXT_APPLICABLE_PTOMP_IF (pInterface)))
        {
            TmrRestartTimer (&(pInterface->pollTimer), POLL_TIMER,
                             NO_OF_TICKS_PER_SEC *
                             (pInterface->i4PollInterval));
        }

        TmrRestartTimer (&(pInterface->helloTimer), HELLO_TIMER,
                         NO_OF_TICKS_PER_SEC * (u2HelloInterval));

    }

    /* If the instance is in unplanned restart state, send the grace
     * LSA */
    if (pInterface->pArea->pOspfCxt->u1RestartStatus == OSPF_RESTART_UNPLANNED)
    {
        MEMSET (&opqLSAInfo, 0, sizeof (tOpqLSAInfo));
        pInterface->pArea->pOspfCxt->u1RestartReason = OSPF_GR_UNKNOWN;
        if ((GrUtilConstructGraceLSA (pInterface->pArea->pOspfCxt,
                                      pInterface, &opqLSAInfo, NEW_LSA))
            == OSPF_SUCCESS)
        {
            OpqAppToOpqModuleSendInCxt
                (pInterface->pArea->pOspfCxt->u4OspfCxtId, &opqLSAInfo);
            LSA_FREE (TYPE10_OPQ_LSA, opqLSAInfo.pu1LSA);
        }
    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT:IsmIfUp \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmElectDr                                               */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.3                           */
/*                This procedure elects a designated router and a backup     */
/*                designated router for the attatched multi-access network.  */
/*                The next state depends on the result of the election.      */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmElectDr (tInterface * pInterface)
{

    HpElectDesgRtr (pInterface);

    OSPF_TRC4 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "DR/BDR Elected. If %x.%d DR %x BDR %x \n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf,
               OSPF_CRU_BMC_DWFROMPDU (pInterface->desgRtr),
               OSPF_CRU_BMC_DWFROMPDU (pInterface->backupDesgRtr));

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmIfReset                                               */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.3                           */
/*                This procedure resets all interface variables and disables */
/*                all associated timers. Also neighbor event kill_nbr is     */
/*                generated for all neighbors. The next state is down.       */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmIfReset (tInterface * pInterface)
{

    IfUpdateState (pInterface, IFS_DOWN);
    IfCleanup (pInterface, IF_CLEANUP_DOWN);
    OspfRmSendIfState (pInterface, OSPF_ZERO);

    OSPF_TRC2 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d Reset\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmIfDown                                                */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.3                           */
/*                This procedure does not perform any action. The next state */
/*                is down.                                                   */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmIfDown (tInterface * pInterface)
{

    IfUpdateState (pInterface, IFS_DOWN);

    OSPF_TRC2 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d DN\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmIfLoop                                                */
/*                                                                           */
/* Description  : Reference : RFC-2178 section 9.3                           */
/*                This procedure resets all interface variables and disables */
/*                all associated timers. Also neighbor event kill_nbr is     */
/*                generated for all neighbors. The next state is loop_back.  */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmIfLoop (tInterface * pInterface)
{

    IfUpdateState (pInterface, IFS_LOOP_BACK);
    IfCleanup (pInterface, IF_CLEANUP_DOWN);

    OSPF_TRC2 (OSPF_ISM_TRC, pInterface->pArea->pOspfCxt->u4OspfCxtId,
               "If %x.%d LoopBack\n",
               OSPF_CRU_BMC_DWFROMPDU (pInterface->ifIpAddr),
               pInterface->u4AddrlessIf);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmResetVariables                                        */
/*                                                                           */
/* Description  : All variables in the interface structure are reset.        */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmResetVariables (tInterface * pInterface)
{

    SET_NULL_IP_ADDR (pInterface->desgRtr);
    SET_NULL_IP_ADDR (pInterface->backupDesgRtr);
    LakClearDelayedAck (pInterface);
    LsuClearUpdate (&pInterface->lsUpdate);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmDisableIfTimers                                      */
/*                                                                           */
/* Description  : The helloTimer, pollTimer and waitTimer associated with */
/*                this interface are disabled.                               */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmDisableIfTimers (tInterface * pInterface)
{

    TmrHelloDeleteTimer (&pInterface->helloTimer);
    TmrDeleteTimer (&pInterface->pollTimer);
    TmrDeleteTimer (&pInterface->waitTimer);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmInitSchedQueue                                       */
/*                                                                           */
/* Description  : Initializes the schedule queue.                            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmInitSchedQueue (VOID)
{

    UINT1               u1Index;

    for (u1Index = 0; u1Index < MAX_ISM_SCHED_QUEUE_SIZE; u1Index++)
    {
        gOsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface = NULL;
        gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1Event = 0;
    }
    gOsRtr.ismSchedQueue.u1FreeEntry = 0;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmInitSchedQueueInCxt                                     */
/*                                                                           */
/* Description  : Initializes the ism schedule queue for the given context   */
/*                                                                           */
/* Input        : pOspfCxt - Ospf Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmInitSchedQueueInCxt (tOspfCxt * pOspfCxt, tInterface * pInterface)
{
    UINT1               u1Index;

    if (pOspfCxt != NULL)
    {
        for (u1Index = 0; ((u1Index < gOsRtr.ismSchedQueue.u1FreeEntry) &&
                           (u1Index < MAX_ISM_SCHED_QUEUE_SIZE)); u1Index++)
        {
            if ((gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                 == OSPF_TRUE) &&
                (gOsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface->
                 pArea->pOspfCxt == pOspfCxt))
            {
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                    = OSPF_FALSE;
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface = NULL;
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1Event = 0;
            }
        }
    }
    else if (pInterface != NULL)
    {
        /* Interface is deleted */
        for (u1Index = 0; ((u1Index < gOsRtr.ismSchedQueue.u1FreeEntry) &&
                           (u1Index < MAX_ISM_SCHED_QUEUE_SIZE)); u1Index++)
        {
            if ((gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                 == OSPF_TRUE) &&
                (gOsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface
                 == pInterface))
            {
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                    = OSPF_FALSE;
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface = NULL;
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1Event = 0;
            }
        }
    }
    else
    {
        for (u1Index = 0; u1Index < MAX_ISM_SCHED_QUEUE_SIZE; u1Index++)
        {
            gOsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface = NULL;
            gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1Event = 0;
        }

        gOsRtr.ismSchedQueue.u1FreeEntry = 0;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmProcessSchedQueue                                    */
/*                                                                           */
/* Description  : This procedure scans the schedule queue and schedules the  */
/*                ism with the appropriate events.                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmProcessSchedQueue (VOID)
{

    UINT1               u1Index;

    for (u1Index = 0; u1Index < gOsRtr.ismSchedQueue.u1FreeEntry; u1Index++)
    {

        for (u1Index = 0; ((u1Index < gOsRtr.ismSchedQueue.u1FreeEntry) &&
                           (u1Index < MAX_ISM_SCHED_QUEUE_SIZE)); u1Index++)
        {
            if (gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                == OSPF_TRUE)
            {
                GENERATE_IF_EVENT ((gOsRtr.ismSchedQueue.aIfEvents[u1Index].
                                    pInterface),
                                   (gOsRtr.ismSchedQueue.aIfEvents[u1Index].
                                    u1Event));
                gOsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid =
                    OSPF_FALSE;
            }
        }
        gOsRtr.ismSchedQueue.u1FreeEntry = 0;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsmSchedule                                               */
/*                                                                           */
/* Description  : This procedure adds the specified interface and event to   */
/*                the schedule queue and updates the next free entry field in*/
/*                the queue.                                                 */
/*                                                                           */
/* Input        : pInterface       : the interface whose state is to be     */
/*                                    updated                                */
/*                u1Event          : the interface event that has occurred  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsmSchedule (tInterface * pInterface, UINT1 u1Event)
{

    tIsmSchedNode      *pSchedNode;
    UINT1               u1FreeEntry;

    if (gOsRtr.ismSchedQueue.u1FreeEntry < MAX_ISM_SCHED_QUEUE_SIZE)
    {

        u1FreeEntry = gOsRtr.ismSchedQueue.u1FreeEntry;
        pSchedNode = &(gOsRtr.ismSchedQueue.aIfEvents[u1FreeEntry]);
        pSchedNode->pInterface = pInterface;
        pSchedNode->u1Event = u1Event;
        pSchedNode->u1IsNodeValid = OSPF_TRUE;
        gOsRtr.ismSchedQueue.u1FreeEntry++;
    }
    else
    {
        return;
    }
}

/*------------------------------------------------------------------------*/
/*                        End of the file  osism.c                        */
/*------------------------------------------------------------------------*/
