/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osfsip.c,v 1.71 2017/09/21 13:48:46 siva Exp $
 *
 * Description:This file contains procedures related to
 *             OSPF & FUTURE_IP Integration 
 *
 *******************************************************************/

#include "osinc.h"
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#include "lnxip.h"
#endif
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>    /* for NF_ACCEPT */
#include <errno.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#endif /* LNXIP4_WANTED */
#endif /* RAWSOCK_HELLO_PKT_WANTED */

#ifdef RAWSOCK_WANTED
PRIVATE UINT1       gau1TxPktBuf[MAX_OSPF_PKT_LEN];
PRIVATE UINT1       gau1RxPktBuf[MAX_OSPF_PKT_LEN];
#endif /* RAWSOCK_WANTED */

#ifdef LNXIP4_WANTED
PRIVATE INT4        OspfSendPktOnUnNumberedIntf
PROTO ((UINT1 *pu1RawPkt, UINT2 u2Len, UINT4 u4Port));
#ifdef RAWSOCK_HELLO_PKT_WANTED
PRIVATE INT4        OSPFNLHelloProcessPkt PROTO ((struct nfq_data * pNfqData));
PRIVATE INT4        OSPFNLHelloCallBack
PROTO ((struct nfq_q_handle * pQHandle,
        struct nfgenmsg * nfMsg, struct nfq_data * nfData, void *data));
struct nfq_handle  *gpHandle;
struct nfq_q_handle *gpQHandle;
#endif /* RAWSOCK_HELLO_PKT_WANTED */
#endif /* LNXIP4_WANTED */
PRIVATE VOID OspfStateHandler PROTO ((tOspfIpIfParam * pOspfIfParams,
                                      tNetIpv4IfInfo * pNetIpIfInfo,
                                      tInterface * pInterface));
extern UINT4        FsMIStdOspfRouterId[12];

/*-------------------PORTABLE PORTION START HERE -----------------------*/

/*----------------------------------------------------------*/
/* Procedures to be modified while integrating FutureOSPF   */
/* with IIIrd Party IP Implementation START HERE !!!        */
/*----------------------------------------------------------*/

#ifndef RAWSOCK_WANTED
/* OUTGOING BUFFER from OSPF --> IP */
/***************************************************************************/
/*                                                                         */
/*     Function Name : IpifSendPktInCxt                                    */
/*                                                                         */
/*     Description   : This function sends the OSPF pkt to FutureIP        */
/*                     by calling the function IpEnquePktToIpFromHL        */
/*                                                                         */
/*     Input(s)      :  pPkt          : Pointer to the buffer.            */
/*                      u4Port        : Interface number of the interface.*/
/*                      pSrcIpAddr  : Source IP Addr.                   */
/*                      pDestIpAddr : Destination IP Addr.              */
/*                      u2Len         : Length of the OSPF pdu.           */
/*                      u1TimeToLive: Time to Live.                     */
/*                      u4OspfCxtId: OSPF Context ID                       */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  SUCCESS/FAILURE.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
IpifSendPktInCxt (UINT4 u4OspfCxtId,
                  tCRU_BUF_CHAIN_HEADER * pPkt,
                  UINT4 u4Port,
                  tIPADDR * pSrcIpAddr,
                  tIPADDR * pDestIpAddr, UINT2 u2Len, UINT1 u1TimeToLive)
{
    t_IP_SEND_PARMS    *pIpSendParms;
    INT4                i4RtrnStats;
    pIpSendParms = (t_IP_SEND_PARMS *) IP_GET_MODULE_DATA_PTR (pPkt);

    if (pIpSendParms == NULL)
    {

        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        return (OSPF_FAILURE);
    }

    pIpSendParms->u1Cmd = IP_LAYER4_DATA;
    pIpSendParms->u4ContextId = u4OspfCxtId;
    pIpSendParms->u1Tos = OSPF_PKT_TOS;
    pIpSendParms->u2Len = u2Len;
    pIpSendParms->u2Id = 0;
    pIpSendParms->u1Df = 0;
    pIpSendParms->u1Olen = 0;
    pIpSendParms->u1Ttl = u1TimeToLive;
    pIpSendParms->u1Proto = FS_OSPF_PROTO;
    pIpSendParms->u2Port = (UINT2) u4Port;
    pIpSendParms->u4Src = OSPF_CRU_BMC_DWFROMPDU (*pSrcIpAddr);
    pIpSendParms->u4Dest = OSPF_CRU_BMC_DWFROMPDU (*pDestIpAddr);

    i4RtrnStats = IpEnquePktToIpFromHLWithCxtId (pPkt);
    return i4RtrnStats;
}
#else
/* OUTGOING BUFFER from OSPF --> IP through RAW SOCKS */
/***************************************************************************/
/*                                                                         */
/*     Function Name : IpifSendPktInCxt                                    */
/*                                                                         */
/*     Description   : This function sends the OSPF pkt to LinuxIP         */
/*                     using sendto call                                   */
/*                                                                         */
/*     Input(s)      :  pPkt          : Pointer to the buffer.             */
/*                      pSrcIpAddr  : Source IP Addr.                      */
/*                      pDestIpAddr : Destination IP Addr.                 */
/*                      u2Len         : Length of the OSPF pdu.            */
/*                      u1TimeToLive: Time to Live.                        */
/*                      u4OspfCxtId: OSPF Context ID                       */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  SUCCESS/FAILURE.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
IpifSendPktInCxt (UINT4 u4OspfCxtId,
                  tCRU_BUF_CHAIN_HEADER * pPkt,
                  UINT4 u4Port,
                  tIPADDR * pSrcIpAddr,
                  tIPADDR * pDestIpAddr, UINT2 u2Len, UINT1 u1TimeToLive)
{
    struct sockaddr_in  DestAddr;
    struct in_addr      SrcIfAddr;
    struct in_addr      DestIfAddr;
    struct msghdr       PktInfo;
    struct ip_mreqn     Mreqn;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    tIpHeader          *pIpHdr = NULL;
    UINT4               u4SrcAddr = 0;
    UINT4               u4DestAddr = 0;
    INT4                i4SendBytes = 0;
    UINT1              *pu1RawPkt = NULL;
    UINT1               au1Cmsg[OSPF_CMSG_DATA_LEN];    /* Control Msg Data */

#ifdef LNXIP4_WANTED

    struct iovec        Iov;
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;

    OS_MEM_SET (&Mreqn, OSPF_ZERO, sizeof (struct ip_mreqn));
    OS_MEM_SET (&DestAddr, OSPF_ZERO, sizeof (struct sockaddr_in));
    OS_MEM_SET (&SrcIfAddr, OSPF_ZERO, sizeof (struct in_addr));
    OS_MEM_SET (&DestIfAddr, OSPF_ZERO, sizeof (struct in_addr));
    OS_MEM_SET (&PktInfo, OSPF_ZERO, sizeof (struct msghdr));
    OS_MEM_SET (au1Cmsg, OSPF_ZERO, OSPF_CMSG_DATA_LEN);
    OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    OS_MEM_SET (&Iov, 0, sizeof (struct iovec));

    if ((pOspfCxt = UtilOspfGetCxt (u4OspfCxtId)) == NULL)
    {

        OSPF_TRC (CONTROL_PLANE_TRC,
                  u4OspfCxtId, "Pkt Disc: Invalid Context \n ");
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    if ((pInterface = UtilFindIfInCxt (pOspfCxt, u4Port)) == NULL)
    {

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                  u4OspfCxtId, "OSPF Pkt Disc Associated If Not Found\n");

        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    MEMCPY (&u4SrcAddr, pSrcIpAddr, sizeof (tIPADDR));
    MEMCPY (&u4DestAddr, pDestIpAddr, sizeof (tIPADDR));

    pu1RawPkt = gau1TxPktBuf;

    pIpHdr = (tIpHeader *) (VOID *) pu1RawPkt;
    pIpHdr->u1VerHdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = OSPF_PKT_TOS;
    pIpHdr->u2Totlen = CRU_HTONS (u2Len + IP_HDR_LEN);
    pIpHdr->u2Id = 0;
    pIpHdr->u2FlOffs = 0;
    pIpHdr->u1Ttl = u1TimeToLive;
    pIpHdr->u1Proto = FS_OSPF_PROTO;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u4Src = u4SrcAddr;
    pIpHdr->u4Dest = u4DestAddr;
    pIpHdr->u2Cksum =
        OSIX_HTONS (OspfIpCalcCheckSum ((UINT1 *) pIpHdr, IP_HDR_LEN));

    if (CRU_BUF_Copy_FromBufChain (pPkt, pu1RawPkt + IP_HDR_LEN, 0,
                                   u2Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    if (IS_UNNUMBERED_PTOP_IFACE (pInterface))
    {
        if (OspfSendPktOnUnNumberedIntf (pu1RawPkt, u2Len, u4Port) ==
            OSPF_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pPkt, 0);
            OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
            return OSPF_FAILURE;
        }
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_SUCCESS;
    }

    SrcIfAddr.s_addr = u4SrcAddr;
    DestIfAddr.s_addr = u4DestAddr;

    if ((UtilIpAddrComp (*pDestIpAddr, gAllSpfRtrs) == OSPF_EQUAL)
        || (UtilIpAddrComp (*pDestIpAddr, gAllDRtrs) == OSPF_EQUAL))
    {
        Mreqn.imr_multiaddr = DestIfAddr;
        Mreqn.imr_address = SrcIfAddr;
        Mreqn.imr_ifindex = (UINT2) u4Port;
        if (OSPF_SET_SOCK_OPT
            ((gOsRtr.ai4SockId[u4OspfCxtId]), IPPROTO_IP, IP_MULTICAST_IF,
             (char *) &(Mreqn), sizeof (Mreqn)) == OSPF_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pPkt, 0);
            OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
            return OSPF_FAILURE;
        }

    }

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = u4DestAddr;
    DestAddr.sin_port = (UINT2) u4Port;

    PktInfo.msg_name = (void *) &DestAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pu1RawPkt;
    Iov.iov_len = u2Len + IP_HDR_LEN;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *) CMSG_DATA
        (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = (UINT2) u4Port;
    pIpPktInfo->ipi_addr.s_addr = u4DestAddr;

    if ((i4SendBytes =
         OSPF_SEND_MSG (gOsRtr.ai4SockId[u4OspfCxtId], &PktInfo, 0)) < 0)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }
#else /* IP_WANTED */
    UNUSED_PARAM (u4OspfCxtId);

    OS_MEM_SET (&Mreqn, OSPF_ZERO, sizeof (struct ip_mreqn));
    OS_MEM_SET (&DestAddr, OSPF_ZERO, sizeof (struct sockaddr_in));
    OS_MEM_SET (&SrcIfAddr, OSPF_ZERO, sizeof (struct in_addr));
    OS_MEM_SET (&DestIfAddr, OSPF_ZERO, sizeof (struct in_addr));
    OS_MEM_SET (&PktInfo, OSPF_ZERO, sizeof (struct msghdr));
    OS_MEM_SET (au1Cmsg, OSPF_ZERO, OSPF_CMSG_DATA_LEN);
    OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);

    MEMCPY (&u4SrcAddr, pSrcIpAddr, sizeof (tIPADDR));
    MEMCPY (&u4DestAddr, pDestIpAddr, sizeof (tIPADDR));

    pu1RawPkt = gau1TxPktBuf;

    pIpHdr = (tIpHeader *) (VOID *) pu1RawPkt;
    pIpHdr->u1VerHdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = OSPF_PKT_TOS;
    pIpHdr->u2Totlen = CRU_HTONS (u2Len + IP_HDR_LEN);
    pIpHdr->u2Id = 0;
    pIpHdr->u2FlOffs = 0;
    pIpHdr->u1Ttl = u1TimeToLive;
    pIpHdr->u1Proto = FS_OSPF_PROTO;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u4Src = u4SrcAddr;
    pIpHdr->u4Dest = u4DestAddr;
    pIpHdr->u2Cksum = OspfIpCalcCheckSum ((UINT1 *) pIpHdr, IP_HDR_LEN);

    if (CRU_BUF_Copy_FromBufChain (pPkt, pu1RawPkt + IP_HDR_LEN, 0,
                                   u2Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    SrcIfAddr.s_addr = u4SrcAddr;
    DestIfAddr.s_addr = u4DestAddr;

    if ((UtilIpAddrComp (*pDestIpAddr, gAllSpfRtrs) == OSPF_EQUAL)
        || (UtilIpAddrComp (*pDestIpAddr, gAllDRtrs) == OSPF_EQUAL))
    {
        Mreqn.imr_multiaddr = DestIfAddr;
        Mreqn.imr_address = SrcIfAddr;
        Mreqn.imr_ifindex = (UINT2) u4Port;

        if (OSPF_SET_SOCK_OPT
            ((gOsRtr.ai4SockId[u4OspfCxtId]), IPPROTO_IP, IP_MULTICAST_IF,
             (char *) &(Mreqn), sizeof (Mreqn)) == OSPF_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pPkt, 0);
            OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
            return OSPF_FAILURE;
        }
    }

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = u4DestAddr;
    DestAddr.sin_port = (UINT2) u4Port;

    PktInfo.msg_name = (void *) &DestAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *) CMSG_DATA
        (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = (UINT2) u4Port;
    pIpPktInfo->ipi_addr.s_addr = u4DestAddr;

    if ((i4SendBytes =
         OSPF_SEND_MSG (gOsRtr.ai4SockId[u4OspfCxtId], &PktInfo, 0)) < 0)
    {

        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }

    if ((i4SendBytes = OSPF_SEND_TO ((gOsRtr.ai4SockId[u4OspfCxtId]), pu1RawPkt,
                                     (u2Len + IP_HDR_LEN), 0,
                                     ((struct sockaddr *) &DestAddr),
                                     (sizeof (struct sockaddr_in))))
        == OSPF_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pPkt, 0);
        OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return OSPF_FAILURE;
    }
#endif

    CRU_BUF_Release_MsgBufChain (pPkt, 0);
    OS_MEM_SET (gau1TxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : OspfIpCalcCheckSum                                     */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
EXPORT UINT2
OspfIpCalcCheckSum (UINT1 *pBuf, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (VOID *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}
#endif

/* INCOMING BUFFER FROM IP --> OSPF */
/***************************************************************************/
/*                                                                         */
/*     Function Name : IpifRcvPkt                                        */
/*                                                                         */
/*     Description   :                                                     */
/*                 The following function is given as a call back function */
/*                 while registering with FutureIP, so that when           */
/*                 FutureIP receives an OSPF Packet                        */
/*                 it just calls this function and passes the buffer.      */
/*                 This function inturn enqueues the buffer to the OSPF    */
/*                 Task.                                                   */
/*                 The OSPF Task then dequeues the buffer in its Task Main */
/*                 Function and does furthur processing.                   */
/*                                                                         */
/*     Input(s)      : pBuf    : Pointer to the buffer.                    */
/*                                - Necessary -                            */
/*                                                                         */
/*                     u2Len  : Length of the OSPF Pdu.                   */
/*                                - Not necessary -                        */
/*                                                                         */
/*                     u4IfIndex : Interface Index.                       */
/*                                - Not necessary -                        */
/*                                                                         */
/*                     IfaceId : Interface Id(CRU Iface Structure).        */
/*                                - Necessary -                            */
/*                                                                         */
/*                     u1_Flag : Flag to indicate Unicast/Mcst/Bcst        */
/*                                - Not necessary -                        */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOID.                                              */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
IpifRcvPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
            UINT4 u4IfIndex, tCRU_INTERFACE IfaceId, UINT1 u1Flag)
{
    tOspfQMsg          *pOspfQMsg;
    UINT4               ipAddr;
    UINT4               u4ContextId;
    tIPADDR             srcIpAddr;
    tIPADDR             destIpAddr;
    UINT1               u1Hlen = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2Datalen = 0;
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u1Flag);

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID, "FUNC: IpifRcvPkt\n");
    /* Set the iface id in the preamble of the buffer */

    OSPF_IP_PKT_GET_SRC (pBuf, ipAddr);
    OSPF_CRU_BMC_DWTOPDU (srcIpAddr, ipAddr);
    IP_PKT_GET_DEST (pBuf, ipAddr);
    OSPF_CRU_BMC_DWTOPDU (destIpAddr, ipAddr);
    OSPF_CRU_BMC_GET_1_BYTE (pBuf, IP_PKT_OFF_HLEN, u1Hlen);
    OSPF_CRU_BMC_GET_2_BYTE (pBuf, IP_PKT_OFF_LEN, u2PktLen);
    u2Datalen = (UINT2) (u2PktLen - ((u1Hlen & 0x0f) * 4));

    OSPF_CRU_BMC_Set_InterfaceId (pBuf, IfaceId);

    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        UtilOsMsgFree (pBuf, NORMAL_RELEASE);
        OSPF_GBL_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                      OSPF_INVALID_CXT_ID, " Alloc Failure\n");
        return;
    }
    pOspfQMsg->u4MsgType = OSPF_IP_PKT_RCVD;
    IP_ADDR_COPY (pOspfQMsg->ospfPktInfo.srcIpAddr, srcIpAddr);
    IP_ADDR_COPY (pOspfQMsg->ospfPktInfo.dstIpAddr, destIpAddr);
    pOspfQMsg->ospfPktInfo.u4IfIndex = u4IfIndex;
    pOspfQMsg->ospfPktInfo.u4PktLen = (UINT4) u2Datalen;
    pOspfQMsg->ospfPktInfo.u4Hlen = (UINT4) u1Hlen;
    pOspfQMsg->ospfPktInfo.pPkt = pBuf;
    if (NetIpv4GetCxtId (u4IfIndex, &u4ContextId) == NETIPV4_FAILURE)
    {
        QMSG_FREE (pOspfQMsg);
        UtilOsMsgFree (pBuf, NORMAL_RELEASE);
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Invalid Context\n");
        return;
    }
    pOspfQMsg->ospfPktInfo.u4ContextId = u4ContextId;

    /* Enqueue the buffer to the OSPF Task */

    if (OsixSendToQ
        ((UINT4) 0, (const UINT1 *) "OSPQ",
         (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {

        QMSG_FREE (pOspfQMsg);
        UtilOsMsgFree (pBuf, NORMAL_RELEASE);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, u4ContextId,
                  "Enqueue To OSPF Failed\n");
        return;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, u4ContextId,
                  "Send Evt Failed\n");
    }
    OSPF_TRC (CONTROL_PLANE_TRC, u4ContextId, "Pkt Enqueued To OSPF\n");
    OSPF_TRC (OSPF_FN_EXIT, u4ContextId, "EXIT: IpifRcvPkt\n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : OspfIfStateChgHdlr                                  */
/*                                                                         */
/*     Description   : Function for handling iface attribute changes,      */
/*                     intimated by IP.                                    */
/*                     This function is given as a call back function      */
/*                     while registering with FutureIP and whenever IP     */
/*                     senses change in Iface configurations, it intimates */
/*                     OSPF by calling this function.                      */
/*                                                                         */
/*     Input(s)      : u4IfIndex: Interface Index                         */
/*                     u4BitMap : Circuit Attribute Identifier BitMap.    */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
OspfIfStateChgHdlr (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4BitMap)
{
    tOspfQMsg          *pOspfQMsg;

    if ((u4BitMap &
         (OPER_STATE_MASK | IFACE_DELETED | IFACE_MTU_MASK | IF_SPEED_BIT_MASK |
          IF_SECIP_DELETED)) == 0)
    {
        return;
    }

    if (u4BitMap == IF_SECIP_DELETED)
    {
        OspfSecIpNetworkDel (pNetIpIfInfo);
        /*delete the secondary ip OSPF network - as the secondary IP is removed at the interface level */
        return;
    }
    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                  pNetIpIfInfo->u4ContextId, " Alloc Failure\n");
        return;
    }

    pOspfQMsg->u4MsgType = OSPF_IP_IF_MSG;
    MEMCPY (&(pOspfQMsg->ospfIpIfParam.OspfNetIpIfInfo),
            pNetIpIfInfo, sizeof (tNetIpv4IfInfo));
    pOspfQMsg->ospfIpIfParam.u4BitMap = u4BitMap;

    if (OsixSendToQ
        ((UINT4) 0, (const UINT1 *) "OSPQ",
         (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        QMSG_FREE (pOspfQMsg);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                  pNetIpIfInfo->u4ContextId, "Enqueue To OSPF Failed\n");
        return;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                  pNetIpIfInfo->u4ContextId, "Send Evt Failed\n");
    }
    OSPF_TRC (CONTROL_PLANE_TRC, pNetIpIfInfo->u4ContextId,
              "Pkt Enqueued To OSPF\n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : OspfProcessIfStateChg                               */
/*                                                                         */
/*     Description   : Obtains Interface from ospf context                 */
/*                     in the case of numbered and unnumbered loopback     */
/*     Input(s)      : pOspfIfParams                                       */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
OspfProcessIfStateChg (tOspfIpIfParam * pOspfIfParams)
{
    tIPADDR             ipAddr;
    tIPADDR             ipMask;
    UINT4               u4AddrlessIf = 0;    /* This has to be assigned with 
                                               a value if a unnumbered P-P link exists */
    tInterface         *pInterface;
    tNetIpv4IfInfo      NetIpIfInfo;
    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4IfIndex;
    UINT1               u1IfType;
    UINT4               u4UnnumAssocIPIf;
    UINT4               u4SetValOspfRouterId = 0;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tTMO_SLL_NODE      *pIfNode;
    tArea              *pArea = NULL;
    tRouterId           rtrId;

    MEMSET (&rtrId, 0, sizeof (tRouterId));

    OSPF_TRC (OSPF_FN_ENTRY, pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
              "FUNC: OspfIfStateChgHdlr\n");

    MEMCPY (&NetIpIfInfo, &(pOspfIfParams->OspfNetIpIfInfo),
            sizeof (tNetIpv4IfInfo));

    OSPF_CRU_BMC_DWTOPDU (ipAddr, NetIpIfInfo.u4Addr);
    OSPF_CRU_BMC_DWTOPDU (ipMask, NetIpIfInfo.u4NetMask);

    if ((pOspfCxt = UtilOspfGetCxt (NetIpIfInfo.u4ContextId)) == NULL)
    {
        OSPF_TRC1 (ALL_FAILURE_TRC | OSPF_CRITICAL_TRC,
                   pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                   "Ospf context for the IfIndx %d is not created\n",
                   NetIpIfInfo.u4IfIndex);
        gu4UtilOspfGetCxtFail++;
        return;
    }

    if (UtilIpAddrComp (ipAddr, gNullIpAddr) == OSPF_EQUAL)
    {
        u4AddrlessIf = NetIpIfInfo.u4IfIndex;
    }
    UNUSED_PARAM (u4AddrlessIf);
    /* Dynamic Rtr ID assignment is allowed only if GR is not in progress */
    if (pOspfCxt->u1RestartExitReason != OSPF_RESTART_INPROGRESS)
    {
        if (pOspfCxt->i4RouterIdStatus == ROUTERID_DYNAMIC)
        {
            if (OS_MEM_CMP (pOspfCxt->rtrId, rtrId, MAX_IPV4_ADDR_LEN) == 0)
            {
                if (UtilSelectOspfRouterId (pOspfCxt->u4OspfCxtId,
                                            &rtrId) == OSPF_SUCCESS)
                {
                    IP_ADDR_COPY (&(pOspfCxt->rtrId), &(rtrId));
                    u4SetValOspfRouterId =
                        OSPF_CRU_BMC_DWFROMPDU (pOspfCxt->rtrId);
                    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_2_WANTED
                    SnmpNotifyInfo.pu4ObjectId = FsMIStdOspfRouterId;
                    SnmpNotifyInfo.u4OidLen =
                        sizeof (FsMIStdOspfRouterId) / sizeof (UINT4);
                    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
                    SnmpNotifyInfo.u1RowStatus = FALSE;
                    SnmpNotifyInfo.pLockPointer = OspfLock;
                    SnmpNotifyInfo.pUnLockPointer = OspfUnLock;
                    SnmpNotifyInfo.u4Indices = 1;
                    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p",
                                      gOsRtr.u4OspfCxtId,
                                      u4SetValOspfRouterId));
#endif
                }
            }
        }
    }

    /* Get the Pointer to Interface Block */
    pInterface = UtilFindIfInCxt (pOspfCxt, NetIpIfInfo.u4IfIndex);
    if (pInterface == (tInterface *) NULL)
    {
        CfaGetIfType (NetIpIfInfo.u4CfaIfIndex, &u1IfType);
        if (u1IfType == CFA_LOOPBACK)
        {
            TMO_SLL_Scan (&(pOspfCxt->areasLst), pArea, tArea *)
            {
                TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
                {
                    pInterface = GET_IF_PTR_FROM_LST (pIfNode);
                    if (pInterface->u4AddrlessIf != 0)
                    {
                        u4IfIndex =
                            OspfGetIfIndexFromPort (pInterface->u4IfIndex);
                        CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
                        if (NetIpIfInfo.u4CfaIfIndex == u4UnnumAssocIPIf)
                        {
                            OSPF_TRC1 (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                                       pOspfIfParams->OspfNetIpIfInfo.
                                       u4ContextId,
                                       "Process Interface Information Change "
                                       "Notification from IP with IfIndex %d\n",
                                       NetIpIfInfo.u4CfaIfIndex);
                            OspfStateHandler (pOspfIfParams, &NetIpIfInfo,
                                              pInterface);
                        }
                    }
                }
            }
        }
    }
    else
    {
        OspfStateHandler (pOspfIfParams, &NetIpIfInfo, pInterface);
    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
              "EXIT: OspfIfStateChgHdlr\n");

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : OspfStateHandler                                    */
/*                                                                         */
/*     Description   : Process Interface Information Change Notification   */
/*                     from IP                                             */
/*     Input(s)      : pOspfIfParams                                       */
/*                     pNetIpIfInfo                                        */
/*                     pInterface                                          */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
PRIVATE VOID
OspfStateHandler (tOspfIpIfParam * pOspfIfParams, tNetIpv4IfInfo * pNetIpIfInfo,
                  tInterface * pInterface)
{
    tIPADDR             ipAddr;
    tIPADDR             ipMask;
    tOspfCxt           *pOspfCxt = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
              "FUNC: OspfIfStateChgHdlr\n");

    OSPF_CRU_BMC_DWTOPDU (ipAddr, pNetIpIfInfo->u4Addr);
    OSPF_CRU_BMC_DWTOPDU (ipMask, pNetIpIfInfo->u4NetMask);

    if ((pOspfCxt = UtilOspfGetCxt (pNetIpIfInfo->u4ContextId)) == NULL)
    {
        OSPF_TRC1 (ALL_FAILURE_TRC | OSPF_CRITICAL_TRC,
                   pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                   "Ospf context for the IfIndx %d is not created\n",
                   pNetIpIfInfo->u4IfIndex);
        gu4UtilOspfGetCxtFail++;
        return;
    }

    if ((OS_MEM_CMP (ipAddr, pInterface->ifIpAddr, sizeof (tIPADDR)) != 0)
        && (pNetIpIfInfo->u4IfIndex == pInterface->u4IfIndex))
    {
        /* IP Address has changed so delete the OSPF Interface */
        OSPF_TRC (CONTROL_PLANE_TRC, pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                  "Iface Address Chg Notification\n");
        OSPF_TRC (CONTROL_PLANE_TRC, pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                  "Delete the OSPF Interface\n");
        IfDelete (pInterface);
        return;
    }

    if ((OS_MEM_CMP (ipMask, pInterface->ifIpAddrMask,
                     sizeof (tIPADDRMASK)) != 0))
    {
        IfSetOperStat (pInterface, OSPF_DISABLED);
        OS_MEM_CPY (&pInterface->ifIpAddrMask, &ipMask, sizeof (tIPADDRMASK));
        if (pOspfIfParams->u4BitMap != OPER_STATE_MASK)
        {
            /* If this is not an oper status change, then make the
             * interface up */
            IfSetOperStat (pInterface, OSPF_ENABLED);
        }
    }

    switch (pOspfIfParams->u4BitMap)
    {

        case OPER_STATE_MASK:
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                      "OperState Chg Notification\n");
            /* RM interface sync up will be done in IfUpdateState internally */
            IfSetOperStat (pInterface, (UINT1) pNetIpIfInfo->u4Oper);
            break;

        case IFACE_DELETED:
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                      "Iface Deleted Chg Notification\n");
            OspfRmSendIfState (pInterface, pOspfIfParams->u4BitMap);
            IfDelete (pInterface);
            break;

        case IFACE_MTU_MASK:
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                      "Iface MTU Chg Notification\n");
            OSPFSetIfMtuSizeInCxt (pInterface, pNetIpIfInfo->u4Mtu);
            OspfRmSendIfState (pInterface, pOspfIfParams->u4BitMap);
            break;
        case IFACE_SPEED_MASK:
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                      "Iface MTU Chg Notification\n");
            OSPFSetIfSpeedInCxt (pOspfCxt, pNetIpIfInfo->u4IfSpeed, pInterface);
            OspfRmSendIfState (pInterface, pOspfIfParams->u4BitMap);
            break;
        case IF_SECIP_DELETED:
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
                      "Iface Deleted Chg Notification\n");

            break;

        default:
            return;

    }
    OSPF_TRC (OSPF_FN_EXIT, pOspfIfParams->OspfNetIpIfInfo.u4ContextId,
              "EXIT: OspfIfStateChgHdlr\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifJoinMcastGroup                                      */
/*                                                                           */
/* Description  : This procedure calls the IP module to join the specified   */
/*                multicast group in  the specified interface                */
/*                                                                           */
/* Input        :  pMcastGroupAddr : multicast group address                 */
/*                 pInterface       : pointer to the interface               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IpifJoinMcastGroup (tIPADDR * pMcastGroupAddr, tInterface * pInterface)
{
#if defined BSDCOMP_SLI_WANTED
    UINT4               u4Mcastaddr = 0;
    struct ip_mreqn     mreqn;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: IpifJoinMcastGroup\n");

    u4Mcastaddr = OSPF_CRU_BMC_DWFROMPDU ((void *) pMcastGroupAddr);

    memset (&mreqn, 0, sizeof (mreqn));

    if (u4Mcastaddr)
        MEMCPY (&mreqn.imr_multiaddr.s_addr, pMcastGroupAddr, sizeof (tIPADDR));

    if (pInterface->u4IfIndex)
        mreqn.imr_ifindex = (INT4) pInterface->u4IfIndex;
    else
        MEMCPY (&(mreqn.imr_address),
                ((struct in_addr *) (VOID *) (pInterface->ifIpAddr)),
                sizeof (struct in_addr));

    if (setsockopt
        (gOsRtr.ai4SockId[pInterface->pArea->pOspfCxt->u4OspfCxtId], IPPROTO_IP,
         IP_ADD_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        OSPF_TRC (OSPF_FN_EXIT | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "ERROR: IpifLeaveMcastGroup Could not Join the Mcast group \n");
    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: IpifJoinMcastGroup\n");

#else
    UNUSED_PARAM (pMcastGroupAddr);
    UNUSED_PARAM (pInterface);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifLeaveMcastGroup                                        */
/*                                                                           */
/* Description  : This procedure calls the IP module to leave the specified  */
/*                multicast group in  the specified interface                */
/*                                                                           */
/* Input        :  pMcastGroupAddr : multicast group address                 */
/*                 pInterface      : pointer to the interface                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IpifLeaveMcastGroup (tIPADDR * pMcastGroupAddr, tInterface * pInterface)
{
#if defined BSDCOMP_SLI_WANTED
    UINT4               u4Mcastaddr = 0;
    struct ip_mreqn     mreqn;

    OSPF_TRC (OSPF_FN_ENTRY, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "FUNC: IpifLeaveMcastGroup\n");
    u4Mcastaddr = OSPF_CRU_BMC_DWFROMPDU ((void *) pMcastGroupAddr);

    memset (&mreqn, 0, sizeof (mreqn));

    if (u4Mcastaddr)
        MEMCPY (&mreqn.imr_multiaddr.s_addr, pMcastGroupAddr, sizeof (tIPADDR));

    if (pInterface->u4IfIndex)
        mreqn.imr_ifindex = (INT4) pInterface->u4IfIndex;
    else
        MEMCPY (&(mreqn.imr_address),
                ((struct in_addr *) (VOID *) (pInterface->ifIpAddr)),
                sizeof (struct in_addr));

    if (((pInterface->pArea) == NULL) ||
        ((pInterface->pArea->pOspfCxt) == NULL))
    {
        return;
    }

    if (setsockopt (gOsRtr.ai4SockId[pInterface->pArea->pOspfCxt->u4OspfCxtId],
                    IPPROTO_IP, IP_DROP_MEMBERSHIP, (void *) &mreqn,
                    sizeof (mreqn)) < 0)
    {
        OSPF_TRC (OSPF_FN_EXIT | OSPF_CRITICAL_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "ERROR: IpifLeaveMcastGroup Could not leave the Mcast group \n");
    }

    OSPF_TRC (OSPF_FN_EXIT, pInterface->pArea->pOspfCxt->u4OspfCxtId,
              "EXIT: IpifLeaveMcastGroup\n");
#else
    UNUSED_PARAM (pMcastGroupAddr);
    UNUSED_PARAM (pInterface);
#endif
}

/* This is an util function which is called from other files in OSPF
 * and this inturn calls the function provided by IP.
 * The utility is to get Interface related parameters
 */
/*****************************************************************************/
/*                                                                           */
/* Function     : IpifGetIfParamsInCxt                                         */
/*                                                                           */
/* Description  : the procedure calls IP module to get the interface         */
/*                specific parameters                                        */
/*                                                                           */
/* Input        : pIpAddr          : IP address                            */
/*                * pAddrlessIf    : address less index                    */
/*                                                                           */
/* Output       : * pIfIndex       : IP handle                             */
/*                * pIfIpAddrMask: IP address mask                       */
/*                * pIfMtuSize    : size of mtu                           */
/*                * pIfSpeed       : metric cost                           */
/*                * p_if_oper_stat   : operational status of Iface           */
/*                * u4OspfCxtId        : OSPF Context Id                     */
/*                                                                           */
/* Returns      : SUCCESS, if packet is successfully transferred             */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IpifGetIfParamsInCxt (UINT4 u4OspfCxtId,
                      tIPADDR pIpAddr,
                      UINT4 u4AddrlessIf,
                      UINT4 *pIfIndex,
                      tIPADDRMASK pIfIpAddrMask,
                      UINT4 *pIfMtuSize,
                      UINT4 *pIfSpeed, UINT1 *pu1IfType, UINT1 *pu1IfOperStat)
{

    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4               u4IpAddr;
    UINT4               u4IfIndex;

    if (*(UINT4 *) (VOID *) (pIpAddr) == 0)
    {
        if (NetIpv4GetPortFromIfIndex (u4AddrlessIf, &u4IfIndex)
            != NETIPV4_SUCCESS)
        {
            return OSPF_FAILURE;
        }
    }
    else
    {
        u4IpAddr = OSPF_CRU_BMC_DWFROMPDU (pIpAddr);
        if (NetIpv4GetIfIndexFromAddrInCxt (u4OspfCxtId,
                                            u4IpAddr,
                                            &u4IfIndex) == NETIPV4_FAILURE)
            return (OSPF_FAILURE);
    }

    if (NetIpv4GetIfInfo (u4IfIndex, &NetIpv4IfInfo) == NETIPV4_FAILURE)
        return (OSPF_FAILURE);

    if ((NetIpv4IfInfo.u4IfType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
        (NetIpv4IfInfo.u4IfType == CRU_VLAN_INTERFACE_TYPE) ||
        (NetIpv4IfInfo.u4IfType == CRU_LAGG_INTERFACE_TYPE))
    {
        *pu1IfType = IF_BROADCAST;
    }

    else if (NetIpv4IfInfo.u4IfType == CRU_FRMRL_PHYS_INTERFACE_TYPE)
    {
        *pu1IfType = IF_NBMA;
    }
    else if (NetIpv4IfInfo.u4IfType == CRU_X25_PHYS_INTERFACE_TYPE)
    {
        *pu1IfType = IF_PTOP;
    }
    else if (NetIpv4IfInfo.u4IfType == CRU_PPP_PHYS_INTERFACE_TYPE)
    {
        *pu1IfType = IF_PTOP;
    }
    else if (NetIpv4IfInfo.u4IfType == CFA_LOOPBACK)
    {
        *pu1IfType = IF_LOOPBACK;
    }
    else if (NetIpv4IfInfo.u4IfType == CFA_PSEUDO_WIRE)
    {
        *pu1IfType = IF_PTOP;
    }
    else
    {
        return OSPF_FAILURE;
    }

    OSPF_CRU_BMC_DWTOPDU (pIfIpAddrMask, NetIpv4IfInfo.u4NetMask);
    *pIfIndex = u4IfIndex;
    *pIfMtuSize = NetIpv4IfInfo.u4Mtu;
    *pIfSpeed = NetIpv4IfInfo.u4IfSpeed;
    *pu1IfOperStat = (NetIpv4IfInfo.u4Oper == IPIF_OPER_ENABLE) ?
        OSPF_ENABLED : OSPF_DISABLED;

    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : ValidIpInterfaceInCxt                                    */
/*                                                                           */
/* Description  : The procedure calls IP module to check whether the    */
/*                interface exists at IP level                   */
/*                                                                           */
/* Input        : pIpAddr          : IP address                            */
/*                u4AddrlessIf    : address less index                    */
/*                u4OspfCxtId: OSPF Context ID                               */
/*                                                                           */
/* Output       : None                            */
/*                                                                           */
/* Returns      : SNMP_SUCCESS             */
/*                SNMP_FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
ValidIpInterfaceInCxt (UINT4 u4OspfCxtId, tIPADDR pIpAddr, UINT4 u4AddrlessIf)
{
    UINT4               ipTempAddr;
    INT4                i4IfIndex = IP_FAILURE;

    if (*(UINT4 *) (VOID *) (pIpAddr) == 0)
    {
        if (NetIpv4GetPortFromIfIndex (u4AddrlessIf, (UINT4 *) &i4IfIndex)
            != NETIPV4_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        ipTempAddr = OSPF_CRU_BMC_DWFROMPDU (pIpAddr);
        if (NetIpv4GetIfIndexFromAddrInCxt (u4OspfCxtId,
                                            ipTempAddr, (UINT4 *) &i4IfIndex)
            == NETIPV4_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/******************************************************************************/
/*                                                                           */
/* Function     : OSPFSetIfSpeedSize                                       */
/*                                                                           */
/* Description  : This function is invoked when we receive indication either */
/*                from lower levels or the network management the change in  */
/*                the speed value.                                        */
/*                                                                           */
/* Input        : ipAddr        :   IP Address to uniquely identify the     */
/*                                   Interface block.                        */
/*                u4AddrlessIf   : Interface Index.                        */
/*                u4IfSpeed      : Speed size.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS/OSPF_FAILURE.                                           */
/*                                                                           */
/*****************************************************************************/

INT1
OSPFSetIfSpeedInCxt (tOspfCxt * pOspfCxt, UINT4 u4IfSpeed,
                     tInterface * pInterface)
{
    tOspfQMsg          *pMessage;
    tOspfSnmpIfParam   *pOspfIfParam;

    if (pInterface != NULL)
    {
        if (u4IfSpeed == 0)
        {
            pInterface->aIfOpCost[TOS_0].u4Value = 0xffff;
        }
        else if (u4IfSpeed > TEN_POWER_EIGHT)
        {
            pInterface->aIfOpCost[TOS_0].u4Value = 1;
        }
        else
        {
            pInterface->aIfOpCost[TOS_0].u4Value = TEN_POWER_EIGHT / u4IfSpeed;
        }

        if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
        {
            pOspfIfParam = (tOspfSnmpIfParam *)
                OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
        }
        else
        {
            gu4OspfSnmpIfMsgAllocFail++;
            return OSPF_FAILURE;
        }
        pOspfIfParam->u1OpCode = OSPF_SIGNAL_LSA_REGEN;
        pOspfIfParam->param.sig_param.pStruct = (UINT1 *) pInterface;
        pOspfIfParam->param.sig_param.u1Type = SIG_IF_METRIC_PARAM_CHANGE;
        pOspfIfParam->pOspfCxt = pOspfCxt;
        if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
        {
            OSPF_SNMP_IF_MSG_FREE (pMessage);
            OSPF_TRC (OS_RESOURCE_TRC | OSPF_CONFIGURATION_TRC |
                      OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                      "OSPF_SNMP_IF_MSG_SEND Failure\n");
            gu4OspfSnmpIfMsgSendFail++;
            return OSPF_FAILURE;
        }
        return OSPF_SUCCESS;
    }
    gu4OSPFSetIfSpeedInCxtFail++;
    return OSPF_FAILURE;
}

/******************************************************************************/
/*                                                                           */
/* Function     : OSPFSetIfMtuSize                                       */
/*                                                                           */
/* Description  : This function is invoked when we receive indication either */
/*                from lower levels or the network management the change in  */
/*                the MTU Size value.                                        */
/*                                                                           */
/* Input        : ipAddr        :   IP Address to uniquely identify the     */
/*                                   Interface block.                        */
/*                u4AddrlessIf   : Interface Index.                        */
/*                u1_mtu_size      : Mtu size.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS/FAILURE.                                           */
/*                                                                           */
/*****************************************************************************/

INT1
OSPFSetIfMtuSizeInCxt (tInterface * pInterface, UINT4 u4MtuSize)
{

    if (pInterface != NULL)
    {
        pInterface->u4MtuSize = u4MtuSize;
        return SUCCESS;
    }
    gu4OSPFSetIfMtuSizeInCxtFail++;
    return OSPF_FAILURE;
}

/*----------------------------------------------------------*/
/* Procedures to be modified while integrating FutureOSPF   */
/* with IIIrd Party IP Implementation END HERE !!!          */
/*----------------------------------------------------------*/

#ifndef RAWSOCK_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : IpifDeqPkt                                               */
/*                                                                           */
/* Description  : This procedure processes the message queue and calls       */
/*                PppRcvPkt which processes the packet received from IP.   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpifDeqPkt (tOspfIpPktInfo * pOspfIpIfParam)
{

    tIPADDR             srcIpAddr;
    tIPADDR             destIpAddr;
    tCRU_BUF_CHAIN_HEADER *pPkt;
    tInterface         *pInterface;
    UINT2               u2Datalen = 0;
    UINT1               u1Hlen = 0;
    UINT4               u4Port;
    tOspfCxt           *pOspfCxt = NULL;

    OSPF_TRC (OSPF_FN_ENTRY, pOspfIpIfParam->u4ContextId,
              "FUNC: IpifDeqPkt \n");

    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_INTERFACE_TRC,
              pOspfIpIfParam->u4ContextId, "OSPF Pkt Rcvd\n");

    IP_ADDR_COPY (srcIpAddr, pOspfIpIfParam->srcIpAddr);
    IP_ADDR_COPY (destIpAddr, pOspfIpIfParam->dstIpAddr);
    u2Datalen = (UINT2) pOspfIpIfParam->u4PktLen;
    u1Hlen = (UINT1) pOspfIpIfParam->u4Hlen;
    pPkt = pOspfIpIfParam->pPkt;

    if ((pOspfCxt = UtilOspfGetCxt (pOspfIpIfParam->u4ContextId)) == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC |
                  OSPF_INTERFACE_TRC, pOspfIpIfParam->u4ContextId,
                  "OSPF Pkt Disc, Associated Ospf Context Not Found\n");
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        return;
    }

    OSPF_IP_PKT_MOVE_TO_DATA (pPkt, ((u1Hlen & 0x0f) * 4));
    u4Port = pOspfIpIfParam->u4IfIndex;

    if ((pInterface = UtilFindIfInCxt (pOspfCxt, u4Port)) == NULL)
    {

        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_INTERFACE_TRC | OSPF_CRITICAL_TRC,
                  pOspfIpIfParam->u4ContextId,
                  "OSPF Pkt Disc Associated If Not Found\n");

        pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        return;
    }

    /* If the interface is Passive interface 
     * No need to process any OSPF protocol packet */
    if (pInterface->bPassive == OSPF_TRUE)
    {
        OSPF_TRC (CONTROL_PLANE_TRC, pOspfIpIfParam->u4ContextId,
                  "Pkt Disc: Dropping Packets received on Passive Interface \n ");
        pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        return;
    }

    PppRcvPkt (pPkt, u2Datalen, pInterface, &srcIpAddr, &destIpAddr);

    OSPF_TRC (OSPF_FN_EXIT, pOspfIpIfParam->u4ContextId, "EXIT: IpifDeqPkt \n");

}
#endif

#ifdef RAWSOCK_WANTED
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFNLHelloProcessPkt                                      */
/*                                                                           */
/* Description  : This procedure processes the Hello packet received         */
/*                from netfilter queue                                       */
/*                                                                           */
/* Input        : Pointer to nfq_data                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Packet id                                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
OSPFNLHelloProcessPkt (struct nfq_data *pNfqData)
{
    INT4                i4Id = 0;
    struct nfqnl_msg_packet_hdr *pHdr;
    UINT1              *pu1Data;
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;
    tIPADDR             SrcIpAddr;
    tIPADDR             DestIpAddr;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4ContextId = 0;
    INT4                i4RecvBytes = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2Datalen = 0;
    UINT1               u1Hlen = 0;

    OS_MEM_SET (&SrcIpAddr, OSPF_ZERO, MAX_IP_ADDR_LEN);
    OS_MEM_SET (&DestIpAddr, OSPF_ZERO, MAX_IP_ADDR_LEN);

    pHdr = nfq_get_msg_packet_hdr (pNfqData);
    if (pHdr != NULL)
    {
        i4Id = OSIX_NTOHL (pHdr->packet_id);
    }

    i4RecvBytes = nfq_get_payload (pNfqData, &pu1Data);

    /* processing packet received from the socket */
    /* Copy the recvd linear buf to cru buf */
    if ((pPkt = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
    {
        OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                      "EXIT : Unable to allocate buf\n");
        OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return i4Id;
    }
    MEMSET (pPkt->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pPkt, "OSPFNLHelloProPkt");
    if (CRU_BUF_Copy_OverBufChain (pPkt, pu1Data, 0, i4RecvBytes) ==
        CRU_FAILURE)
    {
        OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                      "Pkt Disc: Cannot copy OverBufChain \n ");
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        return i4Id;
    }

    u4IfIndex = nfq_get_indev (pNfqData);

    OSPF_IP_PKT_GET_SRC (pPkt, u4IpAddr);
    OSPF_CRU_BMC_DWTOPDU (SrcIpAddr, u4IpAddr);
    IP_PKT_GET_DEST (pPkt, u4IpAddr);
    OSPF_CRU_BMC_DWTOPDU (DestIpAddr, u4IpAddr);

    NetIpv4GetCxtId (u4IfIndex, &u4ContextId);
    if ((pOspfCxt = UtilOspfGetCxt (u4ContextId)) == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC,
                  u4ContextId, "Pkt Disc: Invalid Context \n ");
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    }

    if ((pInterface = UtilFindIfInCxt (pOspfCxt, u4IfIndex)) == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                  u4ContextId, "OSPF Pkt Disc Associated If Not Found\n");

        pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    }

    if (UtilIpAddrComp (SrcIpAddr, pInterface->ifIpAddr) == OSPF_EQUAL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc: dropping self-originated PDU\n ");
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return i4Id;
    }
    /* If the interface is Passive interface
     * No need to process any OSPF protocol packet 
     */
    if (pInterface->bPassive == OSPF_TRUE)
    {
        OSPF_TRC (CONTROL_PLANE_TRC,
                  pInterface->pArea->pOspfCxt->u4OspfCxtId,
                  "Pkt Disc: dropping Packets received on passive interfaece \n ");
        pOspfCxt->u4OspfPktsDisd++;
        UtilOsMsgFree (pPkt, NORMAL_RELEASE);
        OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
        return i4Id;
    }

    OSPF_CRU_BMC_GET_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1Hlen);
    OSPF_CRU_BMC_GET_2_BYTE (pPkt, IP_PKT_OFF_LEN, u2PktLen);
    u2Datalen = u2PktLen - ((u1Hlen & 0x0f) * 4);
    OSPF_IP_PKT_MOVE_TO_DATA (pPkt, ((u1Hlen & 0x0f) * 4));

    COUNTER_OP (pInterface->u4HelloRcvdCount, 1);
    HpRcvHello (pPkt, u2Datalen, pInterface, &SrcIpAddr);
    return i4Id;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OSPFNLHelloCallBack                                        */
/*                                                                           */
/* Description  : This procedure is the callback function for the            */
/*                net filter queue for processing hello packets              */
/*                                                                           */
/* Input        : pointers to nfq_q_handle, nfgenmsg, nfq_data and           */
/*                data pointer                                               */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 on error; >= 0 otherwise                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
OSPFNLHelloCallBack (struct nfq_q_handle *pQHandle,
                     struct nfgenmsg *nfMsg,
                     struct nfq_data *nfData, void *data)
{
    UNUSED_PARAM (nfMsg);
    UNUSED_PARAM (data);
    UINT4               u4Id = OSPFNLHelloProcessPkt (nfData);
    return nfq_set_verdict (pQHandle, u4Id, NF_DROP, 0, NULL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCreateNLHelloSocket                                    */
/*                                                                           */
/* Description  : This procedure creates the Netlink Hello socket            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfCreateNLHelloSocket (VOID)
{

    OSPF_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
              "Opening library handle\n");

    gpHandle = nfq_open ();
    if (gpHandle == NULL)
    {
        OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                      "EXIT : error during nfq_open()\n");
        return OSPF_FAILURE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
              "Unbinding existing nf_queue handler for AF_INET (if any)\n");
    if (nfq_unbind_pf (gpHandle, AF_INET) < 0)
    {
        OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                      "Error during nfq_unbind_pf()\n");
        return OSPF_FAILURE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
              "Binding nfnetlink_queue as nf_queue handler for AF_INET\n");
    if (nfq_bind_pf (gpHandle, AF_INET) < 0)
    {
        OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                      "Error during nfq_bind_pf()\n");
        return OSPF_FAILURE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
              "Binding this socket to queue '0'\n");
    gpQHandle =
        nfq_create_queue (gpHandle, OSPF_NF_QUEUE, &OSPFNLHelloCallBack, NULL);
    if (gpQHandle == NULL)
    {
        OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                      "Error during nfq_create_queue()\n");
        return OSPF_FAILURE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
              "Setting copy_packet mode\n");
    if (nfq_set_mode (gpQHandle, NFQNL_COPY_PACKET, 0xffff) < 0)
    {
        OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                      "Can't set packet_copy mode\n");
        return OSPF_FAILURE;
    }

    gOsRtr.i4NLHelloSockId = nfq_fd (gpHandle);

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (gOsRtr.i4NLHelloSockId, OspfPacketOnNLHelloSocket) !=
        OSIX_SUCCESS)
    {
        nfq_close (gpHandle);
        gOsRtr.i4NLHelloSockId = -1;
        return OSPF_FAILURE;
    }

    if (OspfSetNLHelloSockOptions () == OSPF_FAILURE)
    {
        /* Socket has to be closed */
        OspfCloseNLHelloSocket ();
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCreateNLHelloSocket                                    */
/*                                                                           */
/* Description  : This procedure gets the pkt from socket and calls          */
/*                nfq_handle_packet which processes the packet received.     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpifGetNLHelloPkt (VOID)
{
    INT4                i4RecvBytes = 0;
    char               *pRecvPkt = NULL;

    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);

    pRecvPkt = (char *) gau1RxPktBuf;
    while ((i4RecvBytes =
            recv (gOsRtr.i4NLHelloSockId, pRecvPkt, MAX_OSPF_PKT_LEN, 0)) > 0)
    {
        nfq_handle_packet (gpHandle, pRecvPkt, i4RecvBytes);
        continue;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
              "closing library handle\n");
}

#endif
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifGetPkt                                                 */
/*                                                                           */
/* Description  : This procedure gets the pkt from socket and calls          */
/*                PppRcvPkt which processes the packet received from IP.     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpifGetPkt (INT4 i4SockId)
{
    struct sockaddr_in  RecvNode;
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;
    tIPADDR             SrcIpAddr;
    tIPADDR             DestIpAddr;
    UINT4               u4IfIndex = 0;
    UINT4               u4LowPriMessageCount = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4ContextId = 0;
    INT4                i4RecvNodeLen = sizeof (RecvNode);
    INT4                i4RecvBytes = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2Datalen = 0;
    UINT1               u1Hlen = 0;
    UINT1              *pRecvPkt = NULL;

#ifdef LNXIP4_WANTED
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[OSPF_CMSG_DATA_LEN];
#else
    struct cmsghdr      CmsgInfo;
#endif

    OS_MEM_SET (&SrcIpAddr, OSPF_ZERO, MAX_IP_ADDR_LEN);
    OS_MEM_SET (&DestIpAddr, OSPF_ZERO, MAX_IP_ADDR_LEN);
    OS_MEM_SET (&RecvNode, OSPF_ZERO, i4RecvNodeLen);
    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
    OS_MEM_SET (&PktInfo, OSPF_ZERO, sizeof (struct msghdr));

#ifdef LNXIP4_WANTED

    OS_MEM_SET (au1Cmsg, OSPF_ZERO, OSPF_CMSG_DATA_LEN);

    pRecvPkt = gau1RxPktBuf;

    PktInfo.msg_name = (VOID *) &RecvNode;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pRecvPkt;
    Iov.iov_len = gOsRtr.u4MaxMtuSize;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = OSPF_CMSG_DATA_LEN;

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = OSPF_CMSG_DATA_LEN;

    OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                       &u4LowPriMessageCount);

    while (((i4RecvBytes = recvmsg (i4SockId, &PktInfo, 0)) > 0)
           || (u4LowPriMessageCount != 0))
    {
        if (i4RecvBytes > 0)
        {
            /* processing packet received from the socket */
            /* Copy the recvd linear buf to cru buf */
            if ((pPkt = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
            {
                OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                              "EXIT : Unable to allocate buf\n");
                OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                return;
            }
            MEMSET (pPkt->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
            CRU_BUF_UPDATE_MODULE_INFO (pPkt, "OSPFIpGetPkt");

            if (CRU_BUF_Copy_OverBufChain (pPkt, pRecvPkt, 0, i4RecvBytes) ==
                CRU_FAILURE)
            {
                OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                              "Pkt Disc: Cannot copy OverBufChain \n ");
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                continue;
            }

            if ((pIpPktInfo =
                 (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo)))
                == NULL)
            {
                OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                              "Pkt Disc: Control Msg data is NULL \n ");
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                continue;
            }
            u4IfIndex = (UINT4) pIpPktInfo->ipi_ifindex;

            OSPF_IP_PKT_GET_SRC (pPkt, u4IpAddr);
            OSPF_CRU_BMC_DWTOPDU (SrcIpAddr, u4IpAddr);
            IP_PKT_GET_DEST (pPkt, u4IpAddr);
            OSPF_CRU_BMC_DWTOPDU (DestIpAddr, u4IpAddr);

            NetIpv4GetCxtId (u4IfIndex, &u4ContextId);
            if ((pOspfCxt = UtilOspfGetCxt (u4ContextId)) == NULL)
            {

                OSPF_TRC (CONTROL_PLANE_TRC,
                          u4ContextId, "Pkt Disc: Invalid Context \n ");
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                return;
            }

            if ((pInterface = UtilFindIfInCxt (pOspfCxt, u4IfIndex)) == NULL)
            {

                OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                          u4ContextId,
                          "OSPF Pkt Disc Associated If Not Found\n");

                pOspfCxt->u4OspfPktsDisd++;
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                return;
            }

            if (UtilIpAddrComp (SrcIpAddr, pInterface->ifIpAddr) == OSPF_EQUAL)
            {
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Pkt Disc: dropping self-originated PDU\n ");
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                return;
            }

            /* If the interface is Passive interface 
             * No need to process any OSPF protocol packet */
            if (pInterface->bPassive == OSPF_TRUE)
            {
                OSPF_TRC (CONTROL_PLANE_TRC,
                          pInterface->pArea->pOspfCxt->u4OspfCxtId,
                          "Pkt Disc: dropping Packets received on passive interfaece \n ");
                pOspfCxt->u4OspfPktsDisd++;
                UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                return;
            }

            OSPF_CRU_BMC_GET_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1Hlen);
            OSPF_CRU_BMC_GET_2_BYTE (pPkt, IP_PKT_OFF_LEN, u2PktLen);
            u2Datalen = u2PktLen - ((u1Hlen & 0x0f) * 4);
            OSPF_IP_PKT_MOVE_TO_DATA (pPkt, ((u1Hlen & 0x0f) * 4));

            PppRcvPkt (pPkt, u2Datalen, pInterface, &SrcIpAddr, &DestIpAddr);
        }
        else if (u4LowPriMessageCount != 0)
        {
            /* processing packet in the low priority Q */
            OspfProcessLowPriQMsg ((const UINT1 *) "OLPQ");
        }

        OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                           &u4LowPriMessageCount);

        /* Re-initilizing the variables */
        OS_MEM_SET (au1Cmsg, 0, OSPF_CMSG_DATA_LEN);

        PktInfo.msg_name = (VOID *) &RecvNode;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in);
        Iov.iov_base = pRecvPkt;
        Iov.iov_len = gOsRtr.u4MaxMtuSize;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = OSPF_CMSG_DATA_LEN;

        pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
        pCmsgInfo->cmsg_level = SOL_RAW;
        pCmsgInfo->cmsg_type = IP_PKTINFO;
        pCmsgInfo->cmsg_len = OSPF_CMSG_DATA_LEN;

        /* During relinquish when staggering is present, 
         * if we receive packets greater than 
         * MAX_OSPF_MSG_PROCESSED, we do not receive more and set
         * gOsRtr.u4RcvPktCtr to 0 
         */

        gOsRtr.u4RcvPktCtr++;
        if ((pInterface->pArea->pOspfCxt->u4OspfCxtId ==
             gOsRtr.u4RTstaggeredCtxId)
            && (gOsRtr.u4RcvPktCtr >= MAX_OSPF_MSG_PROCESSED))
        {
            gOsRtr.u4RcvPktCtr = 0;
            break;
        }

    }                            /*While End */
#else /* IP_WANTED */
    OS_MEM_SET (&CmsgInfo, 0, sizeof (struct cmsghdr));

    pRecvPkt = gau1RxPktBuf;

    PktInfo.msg_control = (VOID *) &CmsgInfo;

    OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                       &u4LowPriMessageCount);

    while (((i4RecvBytes = OSPF_RECV_FROM ((i4SockId), pRecvPkt,
                                           (gOsRtr.u4MaxMtuSize),
                                           OSPF_RAWSOCK_NON_BLOCK,
                                           ((struct sockaddr *) &RecvNode),
                                           (&i4RecvNodeLen))) > 0) ||
           (u4LowPriMessageCount != 0))
    {
        if (i4RecvBytes > 0)
        {
            if (OSPF_RECV_MSG (i4SockId, &PktInfo, 0) > 0)
            {

                if ((pPkt =
                     CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
                {
                    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                                  "EXIT : Unable to allocate buf\n");
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }
                MEMSET (pPkt->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
                CRU_BUF_UPDATE_MODULE_INFO (pPkt, "OSPFIpGetPkt");

                if (CRU_BUF_Copy_OverBufChain (pPkt, pRecvPkt, 0, i4RecvBytes)
                    == CRU_FAILURE)
                {
                    OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                                  "Pkt Disc: Cannot copy OverBufChain \n ");
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    continue;
                }

                if ((pIpPktInfo =
                     (tInPktinfo *) (VOID *)
                     CMSG_DATA (CMSG_FIRSTHDR (&PktInfo))) == NULL)
                {
                    OSPF_GBL_TRC (CONTROL_PLANE_TRC, OSPF_INVALID_CXT_ID,
                                  "Pkt Disc: Control Msg data is NULL \n ");
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    continue;

                }

                u4IfIndex = (UINT4) pIpPktInfo->ipi_ifindex;

                OSPF_IP_PKT_GET_SRC (pPkt, u4IpAddr);
                OSPF_CRU_BMC_DWTOPDU (SrcIpAddr, u4IpAddr);
                IP_PKT_GET_DEST (pPkt, u4IpAddr);
                OSPF_CRU_BMC_DWTOPDU (DestIpAddr, u4IpAddr);

                NetIpv4GetCxtId (u4IfIndex, &u4ContextId);
                if ((pOspfCxt = UtilOspfGetCxt (u4ContextId)) == NULL)
                {

                    OSPF_TRC (CONTROL_PLANE_TRC,
                              u4ContextId, "Pkt Disc: Invalid Context \n ");
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                if ((pInterface =
                     UtilFindIfInCxt (pOspfCxt, u4IfIndex)) == NULL)
                {

                    OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC,
                              u4ContextId,
                              "OSPF Pkt Disc Associated If Not Found\n");

                    pOspfCxt->u4OspfPktsDisd++;
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                if (UtilIpAddrComp (SrcIpAddr, pInterface->ifIpAddr) ==
                    OSPF_EQUAL)
                {
                    OSPF_TRC (CONTROL_PLANE_TRC,
                              pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              "Pkt Disc: dropping self-originated PDU\n ");
                    pOspfCxt->u4OspfPktsDisd++;
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                /* If the interface is Passive interface 
                 * No need to process any OSPF protocol packet */
                if (pInterface->bPassive == OSPF_TRUE)
                {
                    OSPF_TRC (CONTROL_PLANE_TRC,
                              pInterface->pArea->pOspfCxt->u4OspfCxtId,
                              "Pkt Disc: dropping Packets received on passive interfaece \n ");
                    pOspfCxt->u4OspfPktsDisd++;
                    UtilOsMsgFree (pPkt, NORMAL_RELEASE);
                    OS_MEM_SET (gau1RxPktBuf, OSPF_ZERO, MAX_OSPF_PKT_LEN);
                    return;
                }

                OSPF_CRU_BMC_GET_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1Hlen);
                OSPF_CRU_BMC_GET_2_BYTE (pPkt, IP_PKT_OFF_LEN, u2PktLen);
                u2Datalen = u2PktLen - ((u1Hlen & 0x0f) * 4);
                OSPF_IP_PKT_MOVE_TO_DATA (pPkt, ((u1Hlen & 0x0f) * 4));

                PppRcvPkt (pPkt, u2Datalen, pInterface, &SrcIpAddr,
                           &DestIpAddr);
            }
        }
        else if (u4LowPriMessageCount != 0)
        {
            /* processing packet in the low priority Q */
            OspfProcessLowPriQMsg ((const UINT1 *) "OLPQ");
        }

        OsixGetNumMsgsInQ ((UINT4) 0, (const UINT1 *) "OLPQ",
                           &u4LowPriMessageCount);

        /* Re-initilizing the variables */
        OS_MEM_SET (&CmsgInfo, 0, sizeof (struct cmsghdr));
    }
#endif
}

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfSetNLHelloSockOptions                                  */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfSetNLHelloSockOptions (VOID)
{

    if ((OSPF_FCNTL ((gOsRtr.i4NLHelloSockId), F_SETFL, O_NONBLOCK)) < 0)
    {
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}
#endif
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfSetRawSockOptions                                      */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfSetRawSockOptions (UINT4 u4OspfCxtId)
{
    INT4                i4OptnVal = OSPF_TRUE;
    UINT1               u1OptnVal = OSPF_TRUE;

    if ((OSPF_FCNTL ((gOsRtr.ai4SockId[u4OspfCxtId]), F_SETFL, O_NONBLOCK)) < 0)
    {
        return OSPF_FAILURE;
    }

    if (OSPF_SET_SOCK_OPT
        ((gOsRtr.ai4SockId[u4OspfCxtId]), IPPROTO_IP, IP_HDRINCL, (&i4OptnVal),
         (sizeof (INT4))) == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }

    /* Setting IP_PKTINFO raw socket option is not supported presently
     * in FutureSLI. When FutureSLI supports the commented code can be used
     */

    if (OSPF_SET_SOCK_OPT
        ((gOsRtr.ai4SockId[u4OspfCxtId]), IPPROTO_IP, IP_PKTINFO, (&u1OptnVal),
         (sizeof (UINT1))) == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }

    return OSPF_SUCCESS;
}

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCloseNLHelloSocket                                     */
/*                                                                           */
/* Description  : Close NL Hello Socket                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfCloseNLHelloSocket (VOID)
{
    /* Remove the Socket Descriptor added to Select utility 
     * for Packet Reception */
    SelRemoveFd (gOsRtr.i4NLHelloSockId);

    nfq_close (gpHandle);
    gOsRtr.i4NLHelloSockId = -1;
}
#endif
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCreateSocket                                           */
/*                                                                           */
/* Description  : Create Socket                                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfCreateSocket (UINT4 u4OspfCxtId)
{
    INT4                i4SockFd;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4SockFd = LnxVrfGetSocketFd (u4OspfCxtId, AF_INET, SOCK_RAW,
                                  FS_OSPF_PROTO, LNX_VRF_OPEN_SOCKET);
#else
    i4SockFd = OSPF_SOCKET (AF_INET, SOCK_RAW, FS_OSPF_PROTO);
#endif
    if (i4SockFd == OSPF_FAILURE)
    {
        return OSPF_FAILURE;
    }

    gOsRtr.ai4SockId[u4OspfCxtId] = i4SockFd;
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (gOsRtr.ai4SockId[u4OspfCxtId], OspfPacketOnSocket) !=
        OSIX_SUCCESS)
    {
        OSPF_CLOSE (gOsRtr.ai4SockId[u4OspfCxtId]);
        gOsRtr.ai4SockId[u4OspfCxtId] = -1;
        return OSPF_FAILURE;
    }

    if (OspfSetRawSockOptions (u4OspfCxtId) == OSPF_FAILURE)
    {
        /* Socket has to be closed */
        OspfCloseSocket (u4OspfCxtId);
        return OSPF_FAILURE;
    }
    return OSPF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCloseSocket                                            */
/*                                                                           */
/* Description  : Close Socket                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfCloseSocket (UINT4 u4OspfCxtId)
{
    /* Remove the Socket Descriptor added to Select utility 
     * for Packet Reception */
    SelRemoveFd (gOsRtr.ai4SockId[u4OspfCxtId]);

    OSPF_CLOSE (gOsRtr.ai4SockId[u4OspfCxtId]);
    gOsRtr.ai4SockId[u4OspfCxtId] = -1;
}

/*****************************************************************************/
/* Function     : OspfPacketOnSocket                                         */
/*                                                                           */
/* Description  : Indicates OSPF Task about Packet received on the socket    */
/*                                                                           */
/* Input        : OSPF Socket Descriptor                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
OspfPacketOnSocket (INT4 i4SockFd)
{
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (gOsRtr.u4ActiveCxtCount == 0)
    {
        return;
    }
    tOspfVrfSock       *pOspfVrfSktFd;
    pOspfVrfSktFd = (tOspfVrfSock *) MemAllocMemBlk (VRF_SOCK_FD);
    if (pOspfVrfSktFd == NULL)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, i4SockFd,
                  "Memory allocation for the queue message failed\n");
        return;
    }
    MEMSET (pOspfVrfSktFd, 0, sizeof (tOspfVrfSock));
    pOspfVrfSktFd->i4SockFd = i4SockFd;

    if (OsixSendToQ
        (SELF, (const UINT1 *) "OSPFLNXVRF",
         (tOsixMsg *) (VOID *) pOspfVrfSktFd, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, i4SockFd,
                  "Enqueue To LINUX OSPF for VRF Failed\n");
        return;
    }
    OsixSendEvent (SELF, OSPF_TASK_NAME, OSPF_PKT_ARRIVAL_EVENT);
#else
    UNUSED_PARAM (i4SockFd);

    /* Notify OSPF about Packet Reception */
    OsixSendEvent (SELF, OSPF_TASK_NAME, OSPF_PKT_ARRIVAL_EVENT);
#endif
}

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
/*****************************************************************************/
/* Function     : OspfPacketOnNLHelloSocket                                  */
/*                                                                           */
/* Description  : Indicates OSPF Task about Packet received on the socket    */
/*                                                                           */
/* Input        : OSPF Socket Descriptor                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
OspfPacketOnNLHelloSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    /* Notify OSPF about Packet Reception */
    OsixSendEvent (SELF, OSPF_TASK_NAME, OSPF_NLH_PKT_ARRIVAL_EVENT);
}

#endif
#endif
#endif /* RAWSOCK_WANTED */

PUBLIC INT1
OspfNbrLlDownInCxt (tOspfCxt * pOspfCxt,
                    UINT4 u4OspfNbrIpAddr, UINT4 u4AddrlessIf)
{
    tIPADDR             nbrIpAddr;
    tNeighbor          *pNbr = NULL;
    tOspfSnmpIfParam   *pOspfIfParam;
    tOspfQMsg          *pMessage;

    OSPF_CRU_BMC_DWTOPDU (nbrIpAddr, u4OspfNbrIpAddr);

    if ((pNbr = GetFindNbrInCxt (pOspfCxt, &(nbrIpAddr), u4AddrlessIf)) == NULL)
    {
        return (OSPF_FAILURE);
    }
    if ((pMessage = OSPF_SNMP_IF_MSG_ALLOC ()) != NULL)
    {
        pOspfIfParam = (tOspfSnmpIfParam *)
            OSPF_SNMP_IF_MSG_GET_PARAMS_PTR (pMessage);
    }
    else
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_CRITICAL_TRC, pOspfCxt->u4OspfCxtId,
                  "OSPF_SNMP_IF_MSG_ALLOC Failure\n");
        return (OSPF_FAILURE);
    }

    if (pNbr->pInterface->u1NetworkType == IF_PTOMP)
    {
        pNbr->u4HelloSuppressPtoMp = OSPF_TRUE;
    }

    pOspfIfParam->u1OpCode = OSPF_NBR_LL_DOWN;
    pOspfIfParam->param.nbr_param.pNbr = pNbr;

    if (OSPF_SNMP_IF_MSG_SEND (pMessage) == SNMP_FAILURE)
    {
        OSPF_SNMP_IF_MSG_FREE (pMessage);
        return OSPF_FAILURE;
    }

    OSPF_TRC (CONTROL_PLANE_TRC, pOspfCxt->u4OspfCxtId, "Message enqueued\n");
    OSPF_TRC (OSPF_FN_EXIT, pOspfCxt->u4OspfCxtId, "EXIT: OspfNbrLlDown\n");
    return SUCCESS;
}

#ifdef RAWSOCK_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfSetIfSockOptions                                      */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
OspfSetIfSockOptions (tInterface * pInterface)
{
    /* In VxWorks, Socket option must be set for each interface to handle 
     * the multicast traffic. Hence the same function is written in osvxip.c
     * But with Future IP, this is not required. So this function is a stub*/

    UNUSED_PARAM (pInterface);
    return OSPF_SUCCESS;
}
#endif

/* Following function is used in CLI callback functions. */
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfGetIpAddr                                              */
/*                                                                           */
/* Description  : Function to Get IP address of an Interface                 */
/*                                                                           */
/* Input        : u4IfIndex                                                  */
/*                                                                           */
/* Output       : IP address                                                 */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
OspfGetIpAddr (UINT4 u4IfIndex, UINT4 *pu4IpAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4IpAddr = NetIpIfInfo.u4Addr;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Description  : Function to Get corresponding Interface Index for a port   */
/*                                                                           */
/* Input        : u2Port                                                     */
/*                                                                           */
/* Output       : IfIndex                                                    */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
OspfGetIfIndexFromPort (UINT4 u4Port)
{
    UINT4               u4CfaIfIndex;

    /* Get the CfaInterface Index */
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        gu4NetIpv4GetCfaIfIndexFromPortFail++;
        return OSIX_FAILURE;
    }
    return u4CfaIfIndex;
}

/* Following function is used in CLI callback functions. */
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfGetInterfaceNameFromIndex                              */
/*                                                                           */
/* Description  : Function to Get Name From Index                            */
/*                                                                           */
/* Input        : u4IfIndex                                                  */
/*                                                                           */
/* Output       : Name of the interface.                                     */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
OspfGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4Index, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    STRNCPY (pu1Alias, NetIpIfInfo.au1IfName, STRLEN (NetIpIfInfo.au1IfName));
    pu1Alias[STRLEN (NetIpIfInfo.au1IfName)] = '\0';
    return OSIX_SUCCESS;
}

/* Following function is used as  VCM callback functions. */
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfVcmCallbackFn                                          */
/*                                                                           */
/* Description  : Function to Indicate Context/Interface mapping deletion to */
/*                OSPF. This fucntion is registered with VCM module          */
/*                as a call back function                                    */
/*                                                                           */
/* Input        : u4IfIndex   - Ip Interface Index                           */
/*                u4VcmCxtId  - Context Id                                   */
/*                u1BitMap    - Bit Map to identify the change               */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfVcmCallbackFn (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1BitMap)
{

    tOspfQMsg          *pOspfQMsg;

    if ((u1BitMap & (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ)) == 0)
    {
        return;
    }
    if (QMSG_ALLOC (pOspfQMsg) == NULL)
    {
        OSPF_TRC (OS_RESOURCE_TRC | OSPF_ISM_TRC | OSPF_CRITICAL_TRC,
                  u4VcmCxtId, " Alloc Failure\n");
        return;
    }

    pOspfQMsg->u4MsgType = OSPF_VCM_MSG_RCVD;
    pOspfQMsg->ospfVcmInfo.u4IpIfIndex = u4IpIfIndex;
    pOspfQMsg->ospfVcmInfo.u4VcmCxtId = u4VcmCxtId;
    pOspfQMsg->ospfVcmInfo.u1BitMap = u1BitMap;

    if (OsixSendToQ
        ((UINT4) 0, (const UINT1 *) "OSPQ",
         (tOsixMsg *) (VOID *) pOspfQMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        QMSG_FREE (pOspfQMsg);
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, u4VcmCxtId,
                  "Enqueue To OSPF Failed\n");
        return;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) "OSPF", OSPF_MSGQ_IF_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPF_TRC (CONTROL_PLANE_TRC | OSPF_CRITICAL_TRC, u4VcmCxtId,
                  "Send Evt Failed\n");
    }
    OSPF_TRC (CONTROL_PLANE_TRC, u4VcmCxtId, "Msg From VCM Enqueued To OSPF\n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : VcmMsgHandler                                       */
/*                                                                         */
/*     Description   : Process Interface tontext Mapping Information       */
/*                       Change Notification and context deletion indication */
/*                     from IP                                             */
/*     Input(s)      : pOspfVcmInfo                                        */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
VcmMsgHandler (tOspfVcmInfo * pOspfVcmInfo)
{
    tInterface         *pInterface;
    tOspfCxt           *pOspfCxt;

    if (gOsRtr.apOspfCxt[pOspfVcmInfo->u4VcmCxtId] == NULL)
    {
        return;
    }
    pOspfCxt = gOsRtr.apOspfCxt[pOspfVcmInfo->u4VcmCxtId];

    switch (pOspfVcmInfo->u1BitMap)
    {
        case VCM_CXT_STATUS_CHG_REQ:
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfVcmInfo->u4VcmCxtId,
                      "Context Deleted Chg Notification from VCM\n");

            RtrDeleteCxt (pOspfCxt);

            break;
        case VCM_IF_MAP_CHG_REQ:
            if ((pInterface =
                 UtilFindIfInCxt (pOspfCxt, pOspfVcmInfo->u4IpIfIndex)) == NULL)
            {
                return;
            }
            OSPF_TRC (CONTROL_PLANE_TRC,
                      pOspfVcmInfo->u4VcmCxtId,
                      "Iface Deleted Chg Notification from VCM\n");

            IfDelete (pInterface);

            break;
        default:
            return;

    }

    return;

}

/***************************************************************************/
/*                                                                         */
/*     Function Name : OspfShutDownProcess                                 */
/*                                                                         */
/*     Description   : This routing triggers planned graceful shutdown     */
/*                     This is called from ISS module.                     */
/*                                                                         */
/*     Input(s)      : None                                                */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/

VOID
OspfShutDownProcess (VOID)
{

    tOspfCxt           *pOspfCxt = NULL;
    UINT4               u4OspfCxtId = 0;
    UINT4               u4PrevOspfCxtId = 0;

    OSPF_GBL_TRC (OSPF_FN_ENTRY, OSPF_INVALID_CXT_ID,
                  "FUNC: OspfShutDownProcess\r\n");
    OspfLock ();

    if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == OSPF_SUCCESS)
    {
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);
    }

    while (pOspfCxt != NULL)
    {

        u4PrevOspfCxtId = u4OspfCxtId;
        /*Check any one context is having restart support. */
        if ((pOspfCxt->u1RestartSupport == OSPF_RESTART_PLANNED)
            || (pOspfCxt->u1RestartSupport == OSPF_RESTART_BOTH))
        {
            /*Planned GR */
            break;
        }

        if (UtilOspfGetNextCxtId (u4PrevOspfCxtId, &u4OspfCxtId) ==
            OSPF_FAILURE)
        {
            pOspfCxt = NULL;
            break;
        }
        pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    }

    if (pOspfCxt != NULL)
    {
        if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
        {
            GrInitiateRestart (OSPF_RESTART_PLANNED, OSPF_GR_SW_RESTART);
        }
        else if (OSPF_IS_STANDBY_UP () == OSPF_TRUE)
        {
            /* Delete the global VRF SPF timer */
            TmrDeleteTimer (&(gOsRtr.vrfSpfTimer));
            GrShutdownProcess ();
        }
    }
    else
    {
        /* Delete the global VRF SPF timer */
        TmrDeleteTimer (&(gOsRtr.vrfSpfTimer));

        GrShutdownProcess ();
    }

    OspfUnLock ();

    OSPF_GBL_TRC (OSPF_FN_EXIT, OSPF_INVALID_CXT_ID,
                  "FUNC: OspfShutDownProcess\r\n");
}

#ifdef LNXIP4_WANTED
/***************************************************************************/
/*                                                                         */
/*     Function Name : OspfSendPktOnUnNumberedIntf                         */
/*                                                                         */
/*     Description   : This module opens a new packet socket and sends the */
/*                     pkt in the link layer level.                        */
/*                                                                         */
/*     Input(s)      : None                                                */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
INT4
OspfSendPktOnUnNumberedIntf (UINT1 *pu1RawPkt, UINT2 u2Len, UINT4 u4Port)
{
    struct sockaddr_ll  DestAddrUnNumbIntf;
    tMacAddr            DestMacAddr = { 0x1, 0x0, 0x5e, 0x0, 0x0, 0x5 };
    INT4                i4UnNumPfSock = -1;
    INT4                i4SendBytes = 0;

    OS_MEM_SET (&DestAddrUnNumbIntf, 0, sizeof (struct sockaddr_ll));

    /* Opens a Packet Socket for sending the pkt */
    i4UnNumPfSock = socket (PF_PACKET, SOCK_DGRAM, 0);
    if (i4UnNumPfSock < 0)
    {
        return OSPF_FAILURE;
    }

    DestAddrUnNumbIntf.sll_family = PF_PACKET;
    DestAddrUnNumbIntf.sll_ifindex = (UINT2) u4Port;
    DestAddrUnNumbIntf.sll_protocol = IP_HTONS (ETH_P_IP);
    DestAddrUnNumbIntf.sll_halen = CFA_ENET_ADDR_LEN;
    MEMCPY (DestAddrUnNumbIntf.sll_addr, DestMacAddr, CFA_ENET_ADDR_LEN);

    if ((i4SendBytes = OSPF_SEND_TO ((i4UnNumPfSock), pu1RawPkt,
                                     (u2Len + IP_HDR_LEN), 0,
                                     ((struct sockaddr *) &DestAddrUnNumbIntf),
                                     (sizeof (struct sockaddr_ll))))
        == OSPF_FAILURE)
    {
        close (i4UnNumPfSock);
        return OSPF_FAILURE;
    }

    close (i4UnNumPfSock);
    return OSPF_SUCCESS;
}
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file osfsip.c                      */
/*-----------------------------------------------------------------------*/
