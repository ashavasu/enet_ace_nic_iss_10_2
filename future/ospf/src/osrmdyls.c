/************************************************************/
/* $Id: osrmdyls.c,v 1.5 2014/12/30 12:42:19 siva Exp $    */
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved     */
/*                                                          */
/*  FILE NAME             : osrmdyls.c                      */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : OSPF                            */
/*  LANGUAGE              : C                               */
/*  DESCRIPTION           : This file contains definitions  */
/*                          related to dynamic update for   */
/*                          High Availability               */
/************************************************************/
/*                                                          */
/*  Change History                                          */
/*  Version               :                                 */
/*  Date(DD/MM/YYYY)      :                                 */
/*  Modified by           :                                 */
/*  Description of change :                                 */
/************************************************************/

#ifndef _OSREDDYN_
#define _OSREDDYN_

#include "osinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmSendLsAckInfo                                       */
/*                                                                           */
/* Description  : This function is called when active node gets a LSA ack    */
/*                from all the neighbors. This function sends the LSA header */
/*                information.                                               */
/*                                                                           */
/* Input        : pLsaInfo  -  Pointer to LSA info                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
OspfRmSendLsAckInfo (tLsaInfo * pLsaInfo)
{
    tLsaId              lsaId;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ZERO;
    UINT4               u4MsgLenOffset = OSPF_RED_MESSAGE_LENGTH_OFFSET;
    INT4                i4RetVal = OSPF_FAILURE;
    UINT1               u1MsgType = OSPF_RED_SYNC_LSACK_MSG;

    if (!((pLsaInfo->u4RxmtCount == 0) &&
          (OSPF_IS_STANDBY_UP () == OSPF_TRUE) &&
          (gOsRtr.osRedInfo.u4DynBulkUpdatStatus >=
           OSPF_RED_BLKUPDT_INPROGRESS) &&
          (gOsRtr.osRedInfo.u1BulkUpdModuleStatus >= OSPF_RED_LSDB_MOD)))
    {
        return;
    }

    /* Format of LSU bulk update message
     * Field              Content           size in bytes
     ------------       ---------         -------------
     * Message type       LSA sub type        1
     * Message Length     Length of Msg       2
     * Lsa info           LSA ID              12
     * Context info       Context Id          4
     * LSA scope ID                           4 + 4
     *                    Interface index for TYPE9_OPQ_LSA
     *                    Unused for TYPE11_OPQ_LSA AS_EXT_LSA
     *                    Area-Id Index for Others.
     *                                          
     */

    /*  Update macro OSPF_RED_DYN_ACK_MSG_SIZE if any change
       in above message format
     */

    pMsg = OspfRmAllocForRmMsg (OSPF_RED_DYN_ACK_MSG_SIZE);

    if (pMsg == NULL)
    {
        return;
    }

    MEMCPY (&lsaId, &pLsaInfo->lsaId, sizeof (tLsaId));

    /* Copy the message type, length */
    OSPF_RED_PUT_1_BYTE (pMsg, u4Offset, u1MsgType);
    OSPF_RED_PUT_2_BYTE (pMsg, u4Offset, 0);

    /* Copy the context Id info */
    OSPF_RED_PUT_4_BYTE (pMsg, u4Offset, pLsaInfo->pOspfCxt->u4OspfCxtId);

    OSPF_RED_PUT_N_BYTE (pMsg, &lsaId, u4Offset, sizeof (tLsaId));

    if (lsaId.u1LsaType == TYPE9_OPQ_LSA)
    {
        OSPF_RED_PUT_N_BYTE (pMsg, pLsaInfo->pInterface->ifIpAddr, u4Offset,
                             sizeof (tIPADDR));

        OSPF_RED_PUT_4_BYTE (pMsg, u4Offset,
                             pLsaInfo->pInterface->u4AddrlessIf);
    }
    else if ((lsaId.u1LsaType == TYPE11_OPQ_LSA) ||
             (lsaId.u1LsaType == AS_EXT_LSA))
    {
        OSPF_RED_PUT_N_BYTE (pMsg, gNullIpAddr, u4Offset, sizeof (tIPADDR));

        OSPF_RED_PUT_4_BYTE (pMsg, u4Offset, 0);
    }
    else
    {
        OSPF_RED_PUT_N_BYTE (pMsg, pLsaInfo->pArea->areaId, u4Offset,
                             sizeof (tAreaId));

        OSPF_RED_PUT_4_BYTE (pMsg, u4Offset, 0);
    }

    /* Update the message length */
    OSPF_RED_PUT_2_BYTE (pMsg, u4MsgLenOffset, u4Offset);

    i4RetVal = OspfRmSendMsgToRm (pMsg, (UINT2) u4Offset);

    if (i4RetVal != OSPF_SUCCESS)
    {
        /* Log */
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfRmProcessLsAckInfo                                    */
/*                                                                           */
/* Description  : This function process the received dynamic LSA ack info    */
/*                sent by the active node and deletes the LSA from Rxmitlist */
/*                                                                           */
/* Input        : *pMsg    -     pointer to LSA Ack Information message      */
/*                                                                           */
/* Output       :  None.                                                     */
/*                                                                           */
/* Returns      :  VOID                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
OspfRmProcessLsAckInfo (tRmMsg * pMsg)
{

    tAreaId             areaId;
    tIPADDR             ifIpAddr;
    tLsaId              lsaId;
    tOspfCxt           *pOspfCxt = NULL;
    tArea              *pArea = NULL;
    tInterface         *pInterface = NULL;
    tLsaInfo           *pLsaInfo = NULL;
    UINT4               u4Offset = OSPF_RED_MSG_HDR_SIZE;
    UINT4               u4OspfCxtId = ZERO;
    UINT4               u4AddrlessIf = ZERO;

    /* Format of LSU bulk update message
     * Field              Content           size in bytes
     ------------       ---------         -------------
     * Message type       LSA sub type        1
     * Message Length     Length of Msg       2
     * Lsa info           LSA ID              12
     * Context info       Context Id          4
     * LSA scope ID                           4 + 4
     *                    Interface index for TYPE9_OPQ_LSA
     *                    Unused for TYPE11_OPQ_LSA AS_EXT_LSA
     *                    Area-Id Index for Others.
     *
     */

    /*  Update macro OSPF_RED_DYN_ACK_MSG_SIZE if any change
       in above message format
     */

    /* Copy the context Id info */
    OSPF_RED_GET_4_BYTE (pMsg, u4Offset, u4OspfCxtId);

    OSPF_RED_GET_N_BYTE (pMsg, &lsaId, u4Offset, sizeof (tLsaId));

    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt == NULL)
    {
        return;
    }

    if (lsaId.u1LsaType == TYPE9_OPQ_LSA)
    {
        /* Copy Interface info */
        OSPF_RED_GET_N_BYTE (pMsg, ifIpAddr, u4Offset, sizeof (tIPADDR));

        OSPF_RED_GET_4_BYTE (pMsg, u4Offset, u4AddrlessIf);

        pInterface = GetFindIfInCxt (pOspfCxt, ifIpAddr, u4AddrlessIf);

        if (pInterface == NULL)
        {
            return;
        }
    }
    else if ((lsaId.u1LsaType == TYPE11_OPQ_LSA) ||
             (lsaId.u1LsaType == AS_EXT_LSA))
    {
        /* Backbone area can be passed. This will be only used to 
           get the context in search database and If will not be NULL */
        pArea = pOspfCxt->pBackbone;
    }
    else
    {
        /* Copy Area ID */
        OSPF_RED_GET_N_BYTE (pMsg, areaId, u4Offset, sizeof (tAreaId));

        pArea = UtilFindAssoAreaInCxt (pOspfCxt, &areaId);

        if (pArea == NULL)
        {
            return;
        }
    }

    pLsaInfo = LsuSearchDatabase (lsaId.u1LsaType, &(lsaId.linkStateId),
                                  &(lsaId.advRtrId), (UINT1 *) pInterface,
                                  (UINT1 *) pArea);

    if (pLsaInfo != NULL)
    {
        LsuDeleteFromAllRxmtLst (pLsaInfo);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OsRmDynLsSendLsa                                           */
/*                                                                           */
/* Description  : This function is called when active node adds a new LSA    */
/*                to the DB. The active node syncs the originated LSA to the */
/*                standby node                                               */
/*                                                                           */
/* Input        : pLsaDesc - LSA Descriptor                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
OsRmDynLsSendLsa (tLsaInfo * pLsaInfo)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2PktLen = 0;

    if (OSPF_IS_STANDBY_UP () == OSPF_FALSE)
    {
        return OSIX_SUCCESS;
    }

    /* If it is a self-originated LSA and LSA Desc is NULL, then do
     * not send the LSU. This occurs when an invalid self-originated
     * LSA is received from the neighbor
     */
    if ((IS_SELF_ORIGINATED_LSA_IN_CXT (pLsaInfo)) &&
        (pLsaInfo->pLsaDesc == NULL))
    {
        return OSIX_SUCCESS;
    }

    pRmMsg = OspfRmAllocForRmMsg (OSPF_RED_IFACE_MTU);

    if (pRmMsg == NULL)
    {
        OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "Failed to allocate RM message "
                       "for synchronizing LSA "
                       "LSA Type : %d, Link state id : %x "
                       "Advertising router id : %x\r\n",
                       pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->
                                               lsaId.linkStateId),
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

        return OSIX_FAILURE;
    }

    OSPF_RED_PUT_1_BYTE (pRmMsg, u2PktLen, OSPF_RED_SYNC_LSU_MSG);
    OSPF_RED_PUT_2_BYTE (pRmMsg, u2PktLen, 0);

    OspfRmConstructLsaMsg (pLsaInfo, &pRmMsg, &u2PktLen);

    RM_DATA_ASSIGN_2_BYTE (pRmMsg, OSPF_RED_MESSAGE_LENGTH_OFFSET, u2PktLen);

    if (OspfRmSendMsgToRm (pRmMsg, u2PktLen) == OSPF_FAILURE)
    {
        OSPF_EXT_TRC3 (OSPF_REDUNDANCY_TRC,
                       pLsaInfo->pOspfCxt->u4OspfCxtId,
                       "Failed to transmit RM message "
                       "for synchronizing self originated LSA "
                       "LSA Type : %d, Link state id : %x "
                       "Advertising router id : %x\r\n",
                       pLsaInfo->lsaId.u1LsaType,
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->
                                               lsaId.linkStateId),
                       OSPF_CRU_BMC_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                            */
/* Function     : OspfRmProcessMd5SeqNo                                       */
/*                                                                            */
/* Description  : This function process the received md5 seq number info      */
/*                sent by the active node and updates the interface structure */
/*                                                                            */
/* Input        : *pMsg    -     pointer to LSA Ack Information message       */
/*                                                                            */
/* Output       :  None.                                                      */
/*                                                                            */
/* Returns      :  VOID                                                       */
/*                                                                            */
/*****************************************************************************/
VOID
OspfRmProcessMd5SeqNo (tRmMsg * pMsg)
{

    UINT4               u4OspfCxtId = OSPF_ZERO;
    UINT4               u4AddrlessIf = OSPF_ZERO;
    UINT4               u4CryptoSeqNo = OSPF_ZERO;
    tIPADDR             IfIpAddr;
    tOspfCxt           *pOspfCxt = NULL;
    tInterface         *pInterface = NULL;
    UINT4               u4Offset = OSPF_RED_MSG_HDR_SIZE;

    /* Get the context Id info */
     
      OSPF_CRU_BMC_GET_4_BYTE (pMsg, u4Offset, u4OspfCxtId);
      u4Offset += sizeof (UINT4);
    
    /* Get the address info */
      OSPF_CRU_BMC_GET_4_BYTE (pMsg, u4Offset, u4AddrlessIf);
          u4Offset += sizeof (UINT4);

     OSPF_CRU_BMC_GET_STRING (pMsg, IfIpAddr, u4Offset, MAX_IP_ADDR_LEN);
         u4Offset += MAX_IP_ADDR_LEN;
      
      /*Get the interface seq no */
	 OSPF_CRU_BMC_GET_4_BYTE (pMsg, u4Offset, u4CryptoSeqNo);
	     u4Offset += sizeof (UINT4);


    pOspfCxt = UtilOspfGetCxt (u4OspfCxtId);

    if (pOspfCxt == NULL)
    {
        return;
    }

    if ((pInterface = GetFindIfInCxt (pOspfCxt, IfIpAddr,
                                      u4AddrlessIf)) == NULL)
    {

        OSPF_EXT_GBL_TRC (OSPF_REDUNDANCY_TRC, OSPF_INVALID_CXT_ID,
                          "Processing md5 seq no From Active:\n ");
        return;
    }
    if((pInterface->u2AuthType == CRYPT_AUTHENTICATION)
             && (pInterface->u4CryptoAuthType == OSPF_AUTH_MD5))
    
    {
            /*Get the hello packet seqno */
            pInterface->u4CryptSeqNum = u4CryptoSeqNo + 1;
    }
    return; 
}
#endif /* _OSREDDYN_ */
