/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lasz.c,v 1.3 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _LASZ_C
#include "lahdrs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
LaSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LA_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsLASizingParams[i4SizingId].u4StructSize,
                                     FsLASizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(LAMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            LaSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
LaSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsLASizingParams);
    IssSzRegisterModulePoolId (pu1ModName, LAMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
LaSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LA_MAX_SIZING_ID; i4SizingId++)
    {
        if (LAMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (LAMemPoolIds[i4SizingId]);
            LAMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
