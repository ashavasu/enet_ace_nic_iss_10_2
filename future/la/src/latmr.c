/*****************************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
* $Id: latmr.c,v 1.50 2017/11/16 14:26:55 siva Exp $
* Description: This file contains functions belonging to the
*              Timer Task and other timer functionalities   
*              in Link Aggregation module.
 *****************************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : latmr.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Timer Task                                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains functions belonging to the  */
/*                            Timer Task and other timer functionalities of  */
/*                            LA.                                            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*****************************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"
#ifdef SNMP_2_WANTED
#include "fslawr.h"
#include "stdlawr.h"
#endif

INT4                gi4LaTaskCreated = LA_FALSE;

/*****************************************************************************/
/* Function Name      : LaTimerInit                                          */
/*                                                                           */
/* Description        : This function creates a Timer Task and a Timer List  */
/*                      for the timers in the LA module.                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaGlobalInfo.LaTmrTaskId,                           */
/*                      gLaGlobalInfo.LaTimerListId                          */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaTimerInit (VOID)
{

    /* Creating the LA Timer List and storing the Timer List Id in the LA global
     * structure */

    if (LA_CREATE_TMR_LIST (LA_TIMER_TASK_NAME,
                            LA_TMR_EXPIRY_EVENT,
                            NULL, &(LA_TIMER_LIST_ID)) != LA_TMR_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC |
                ALL_FAILURE_TRC, "TMR: Timer List Creation FAILED\n");
        return LA_FAILURE;
    }

    LA_TRC (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
            "TMR: LA Timer Initialized SUCCESSFULLY\n");
    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaTimerDeInit                                        */
/*                                                                           */
/* Description        : This function stops any timer which is running and   */
/*                      de-initialises all the timer related entities such   */
/*                      as Timer Task, Timer List etc after stopping all the */
/*                      timers and deleting Cache and Marker tables.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaGlobalInfo.LaTmrTaskId,                           */
/*                      gLaGlobalInfo.LaTimerListId                          */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaTimerDeInit (VOID)
{
    /* Deleting the Timer List */
    if (LA_DEL_TMR_LIST (LA_TIMER_LIST_ID) != LA_TMR_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC |
                ALL_FAILURE_TRC, "TMR: Timer List Deletion FAILED\n");
        return LA_FAILURE;
    }
    LA_TIMER_LIST_ID = NULL;
    LA_TRC (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
            "TMR: LA Timer De-Initialized SUCCESSFULLY\n");
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaTaskInit                                           */
/*                                                                           */
/* Description        : This function allocates memory pools for the task    */
/*                      queue and creates the protocol semaphore.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All variables in the GLobal structure are            */
/*                      initialised.                                         */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
LaTaskInit (VOID)
{
    LA_MEMSET (&gLaGlobalInfo, 0, sizeof (tLaGlobalInfo));

    if (LA_CREATE_SEMAPHORE (LA_PROTOCOL_SEMAPHORE, LA_BINARY_SEM, LA_SEM_FLAGS,
                             &gLaProtocolSemId) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LaTaskInit: LA Protocol semaphore creation failed in LaTaskInit\n");
        return LA_FAILURE;
    }

    if (LA_CREATE_SEMAPHORE
        ((const UINT1 *) "LaSy", LA_BINARY_SEM, LA_SEM_FLAGS,
         &gLaSyncSemId) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LaTaskInit: LA Protocol semaphore creation failed in LaTaskInit\n");
        return LA_FAILURE;
    }

    /* Create Control PDU's processing Queue */
    if (LA_CREATE_QUEUE (LA_CTRLQ_NAME, OSIX_MAX_Q_MSG_LEN, LA_CTRLQ_DEPTH,
                         &(LA_CTRLQ_ID)) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                "LaTaskInit: Control Task Q Creation FAILED\n");

        return LA_FAILURE;
    }

    if (LA_CREATE_QUEUE (LA_INTFQ_NAME, OSIX_MAX_Q_MSG_LEN, LA_INTFQ_DEPTH,
                         &(LA_INTFQ_ID)) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                "LaTaskInit: Interface Q Creation FAILED\n");

        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMain                                               */
/*                                                                           */
/* Description        : This function is the entry point function of the LA  */
/*                      Task. This function receives an event from the       */
/*                      FSAP Timer Task on expiry of a timer.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaMain (INT1 *pi1Param)
{
    UINT4               u4Events;
#ifdef SYSLOG_WANTED
    INT4                i4SysLogId;
#endif

    UNUSED_PARAM (pi1Param);

    if (LaTaskInit () != LA_SUCCESS)
    {
        LaHandleInitFailure ();

        /* Indicate the status of initialization to the main routine */

        LA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (LaHandleInit () != LA_SUCCESS)
    {
        LaHandleInitFailure ();
        LaRedDeRegisterWithRM ();
        /* Indicate the status of initialization to the main routine */
        LA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (LA_GET_TASK_ID (&LA_TASK_ID) != OSIX_SUCCESS)
    {
        LaHandleInitFailure ();
        LaRedDeRegisterWithRM ();
        /* Indicate the status of initialization to the main routine */
        LA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "LA", SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        LaHandleInitFailure ();
        /* Indicate the status of initialization to the main routine */
        LA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    LA_SYSLOG_ID = i4SysLogId;
#endif

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterFSLA ();
    RegisterSTDLA ();
#endif
    /* Indicate the status of initialization to the main routine */
    LA_INIT_COMPLETE (OSIX_SUCCESS);

    /* Waiting in a blocking, continuous call in order to receive a timer expiry
     * event */

    while (1)
    {
        if (LA_RECEIVE_EVENT (LA_TASK_ID, LA_ALL_EVENTS, OSIX_WAIT, &u4Events)
            == LA_OSIX_SUCCESS)
        {

            if (u4Events & LA_TMR_EXPIRY_EVENT)
            {
                LA_LOCK ();

                LaTimerHandler ();

                LA_UNLOCK ();
            }

            if (u4Events & LA_PKT_ENQ_EVENT)
            {
                LaHandlePktEnqEvent ();
            }

            if (u4Events & LA_CONTROL_EVENT)
            {
                LaHandleCtrlEvents ();
            }

#ifdef L2RED_WANTED
            if (u4Events & LA_RED_BULK_UPD_EVENT)
            {
                LA_LOCK ();
                LaRedHandleBulkUpdateEvent ();
                LA_UNLOCK ();
            }
#endif
        }
    }                            /* End of While */
}

/*****************************************************************************/
/* Function Name      : LaTimerHandler                                       */
/*                                                                           */
/* Description        : This function is called whenever a timer expiry      */
/*                      message is received by the LA Timer Task. The        */
/*                      different timer expiry handlers are called according */
/*                      to the timer type                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaTimerListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaTimerHandler (VOID)
{
    tLaAppTmrNode      *pLaAppTmrNode;
    tLaLacPortEntry    *pPortEntry = NULL;
#ifdef DLAG_WANTED
    tLaLacAggEntry     *pAggEntry = NULL;
#endif
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    UINT1               u1HRTmrFlag = 0;
#endif
    if ((LA_INITIALIZED () != TRUE) || (LA_MODULE_STATUS == LA_DISABLED))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA Module status disabled\r\n");
        return;
    }

    while ((pLaAppTmrNode =
            (tLaAppTmrNode *) LA_GET_NEXT_EXPRD_TMR (LA_TIMER_LIST_ID)) != NULL)
    {
        pLaAppTmrNode->LaTmrFlag = LA_TMR_STOPPED;

        /*Call the corresponding timer expiry handler based on the timer type */
        switch (pLaAppTmrNode->LaAppTimer.u4Data)
        {

            case LA_CACHE_TMR_TYPE:

                if (LaHandleCacheTmrExpiry
                    ((tLaCacheEntry *) pLaAppTmrNode->pEntry) != LA_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                                 ALL_FAILURE_TRC,
                                 "TMR: Port: %d Cache Tmr Expiry Handler FAILED\n",
                                 ((tLaCacheEntry *) (pLaAppTmrNode->pEntry))->
                                 u2PortIndex);
                }
                break;

            case LA_TICK_TMR_TYPE:

                LaLacTickTmrExpiry ((tLaLacPortEntry *) pLaAppTmrNode->pEntry);
                break;

            case LA_PERIODIC_TMR_TYPE:
                LaLacPeriodicTxMachine (pLaAppTmrNode->pEntry,
                                        LA_PTXM_EVENT_PTMR_EXPIRED);
#ifdef L2RED_WANTED
                /* HITLESS RESTART */
                if ((LA_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) &&
                    (u1HRTmrFlag == 0))
                {
                    /* Periodic timer has been made to expire after processing
                     * steady state pkt request from RM. This flag indicates 
                     * to send steady tail msg after sending all the steady 
                     * state packets of LA to RM. */
                    u1HRTmrFlag = 1;
                }
#endif
                break;

            case LA_CURRENT_WHILE_TMR_TYPE:
                pPortEntry = (tLaLacPortEntry *) pLaAppTmrNode->pEntry;

                /* When ISSU is in progress restart the current while timer */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    if (LaStartTimer (&pPortEntry->CurrentWhileTmr)
                        == LA_FAILURE)
                    {
                        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                "CURRENT_WHILE_TMR: ISSU-Maintenance Mode: "
                                "Restarting LaStartTimer returned failure\n");
                        return;
                    }
                    LA_TRC (CONTROL_PLANE_TRC,
                            "CURRENT_WHILE_TMR: "
                            "ISSU-MM Current While Timer is restarted\n");
                    return;
                }

                pPortEntry->u1TimerCount++;

                if (pPortEntry->u1TimerCount == LA_CURRWHILE_SPLIT_INTERVALS)
                {
                    pPortEntry->u1TimerCount = 0;

                    /* Send message to standby, so that the dynamic info for
                     * this port is cleaned there. */
                    LaRedSyncUpCurrWhileTmrExpiry
                        (pPortEntry->u2PortIndex, LA_CURRWHILE_SPLIT_INTERVALS);

                    /* All the split intervals are completed and hence
                     * send CurrentWhileTimerExp event to Receive Sem */
                    LaLacRxMachine (pPortEntry,
                                    LA_RXM_EVENT_CURRENT_WHILE_EXP, NULL);
                }
                else
                {
                    /* ReStart the timer for the next split interval. */
                    if (LaStartTimer (&pPortEntry->CurrentWhileTmr) ==
                        LA_FAILURE)
                    {
                        return;
                    }

                    if ((pPortEntry->LaRxmState == LA_RXM_STATE_CURRENT) ||
                        (pPortEntry->LaRxmState == LA_RXM_STATE_EXPIRED))
                    {
                        /* Split intervals of current while timer has to be
                         * informed to the standby
                         */
                        LaRedSyncUpCurrWhileTmrExpiry
                            (pPortEntry->u2PortIndex, pPortEntry->u1TimerCount);
                    }
                }

                break;

            case LA_WAIT_WHILE_TMR_TYPE:
                pPortEntry = (tLaLacPortEntry *) pLaAppTmrNode->pEntry;

                /* When ISSU is in progress restart the Wait while timer */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    if (LaStartTimer (&pPortEntry->WaitWhileTmr) == LA_FAILURE)
                    {
                        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                "WAIT_WHILE_TMR: ISSU-Maintenance Mode: "
                                "Restarting LaStartTimer returned failure\n");
                        return;
                    }
                    LA_TRC (CONTROL_PLANE_TRC,
                            "WAIT_WHILE_TMR: "
                            "ISSU-MM Wait While Timer is restarted\n");
                    return;
                }

                /*Sync the WAIT_WHILE_TMR expiry to the standby node */
                LaRedSyncUpWaitWhileTmrExpiry (pPortEntry->u2PortIndex);

                if (LaLacMuxControlMachine (pLaAppTmrNode->pEntry,
                                            LA_MUX_EVENT_WAIT_WHILE_EXP)
                    != LA_SUCCESS)
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                 "TMR: Port: %d WaitWhile timer Expiry Handling FAILED\n",
                                 ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                                 u2PortIndex);
                }

                break;

            case LA_HOLD_WHILE_TMR_TYPE:

                break;

            case LA_MARKER_RATE_TMR_TYPE:

                if (LaAggMaintainMarkerRate
                    (((tLaLacAggEntry *) pLaAppTmrNode->pEntry)->u2AggIndex,
                     LA_MARKER_SET) != LA_SUCCESS)
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                 "TMR: Port: %d MarkerRate Tmr Expiry Handler FAILED\n",
                                 ((tLaLacAggEntry *) (pLaAppTmrNode->pEntry))->
                                 u2AggIndex);
                }
                break;

#ifdef LA_CHURN_WANTED
            case ACTOR_CHURN_TMR_TYPE:
            {
                pPortEntry = pLaAppTmrNode->pEntry;
                /*Actor churn == True */
                LaLacActorChurnMachine (pPortEntry, LA_EVENT_ACTOR_CHURN);

                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             " Actor Churn Timer Expiry  for Port : %d\n",
                             ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                             u2PortIndex);
                break;
            }
            case PARTNER_CHURN_TMR_TYPE:
            {
                pPortEntry = pLaAppTmrNode->pEntry;
                /*Partner churn == True */
                LaLacPartnerChurnMachine (pPortEntry, LA_EVENT_PARTNER_CHURN);

                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "Partner Churn Timer Expiry for Port : %d\n",
                             ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                             u2PortIndex);
                break;
            }
#endif
            case LA_MARKER_TMR_TYPE:

                if (LaAggPurgeEntry ((tLaLacPortEntry *) pLaAppTmrNode->pEntry)
                    != LA_SUCCESS)
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                 "TMR: Port: %d Marker Tmr Expiry Handler FAILED\n",
                                 ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                                 u2PortIndex);
                }
                break;

#ifdef DLAG_WANTED
            case LA_DLAG_PERIODIC_TMR_TYPE:
                pAggEntry = pLaAppTmrNode->pEntry;

                /* Send Periodic Sync PDU */
                LaDLAGTxPeriodicSyncPdu (pAggEntry);
                /* Keep Alive Mechanism */

                /* Active-Standby DLAG */
                LaDLAGKeepAlive (pAggEntry);

                break;

            case LA_DLAG_MS_SELECTION_WAIT_TMR_TYPE:
                pAggEntry = pLaAppTmrNode->pEntry;
                LaDLAGElectorSelectionProcess (pAggEntry);
                break;

            case LA_AA_DLAG_PERIODIC_TMR_TYPE:

                /* Send Consolidated Periodic Sync PDU
                 * For Active-Active DLAG */
                LaActiveDLAGTxPeriodicSyncOrAddDelPdu ();

                /* Call the Keep Alive COunter if this is Master */
                if (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER)
                {

                    LaActiveDLAGKeepAlive ();
                }
#endif

            case LA_AA_MCLAG_PERIODIC_TMR_TYPE:

#ifdef ICCH_WANTED
                /* Send Consolidated Periodic Sync PDU
                 * For MCLAG */
                LaActiveMCLAGTxPeriodicSyncOrAddDelPdu ();

                /* Call the Keep Alive COunter if this is Master */
                if (LaIcchGetRolePlayed () == LA_ICCH_MASTER)
                {
                    LaActiveMCLAGKeepAlive ();
                }
#endif
                break;

            case LA_RECOVERY_TMR_TYPE:
                /* Recovery timer Expired */
                pPortEntry = pLaAppTmrNode->pEntry;

                /* When ISSU is in progress restart the current while timer */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    if (LaStartTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
                    {
                        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                "RECOVERY_TMR: ISSU-Maintenance Mode: "
                                "Restarting LaStartTimer returned failure\n");
                        return;
                    }
                    LA_TRC (CONTROL_PLANE_TRC,
                            "RECOVERY_TMR: "
                            "ISSU-MM Recovery Timer is restarted\n");
                    return;
                }

                LaHandleRecoveryTmrExpiry (pPortEntry);
                break;

            default:
                LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "TMR: Invalid Timer Type\n");
                break;

        }                        /*End of switch */
    }                            /*End of while */
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    if (u1HRTmrFlag == 1)
    {
        /* sending steady state tail msg to RM */
        LaRedHRSendStdyStTailMsg ();
        LA_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
#endif
}

/*****************************************************************************/
/* Function Name      : LaStartTimer                                         */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be started by any module/function. THe function      */
/*                      allocates a timer node and starts the timer for the  */
/*                      specified duration for that interface (u2PortIndex)  */
/*                                                                           */
/* Input(s)           : u4TimerType - The type of the timer that is started  */
/*                      *pLaAppTmrNode - Pointer to the timer node           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaTimerListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaStartTimer (tLaAppTmrNode * pLaAppTmrNode)
{
    tLaLacPortEntry    *pPortEntry = NULL;
#ifdef DLAG_WANTED
    tLaLacAggEntry     *pAggEntry = NULL;
#endif
    UINT4               u4TimerType;
    UINT4               u4Duration = 0;

    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        /*Start the timers only in Active Node */
        return LA_SUCCESS;
    }

    /* If the structure entry is null, then return failure */
    if (pLaAppTmrNode == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: Timer Node Null Pointer\n");
        return LA_FAILURE;
    }

    if (pLaAppTmrNode->LaTmrFlag != LA_TMR_STOPPED)
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "TMR: Stopping Running Timer of type %d\n before Restarting",
                     pLaAppTmrNode->LaAppTimer.u4Data);
        if (LaStopTimer (pLaAppTmrNode) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: Unable to STOP the RUNNING Timer\n");
            return LA_FAILURE;
        }
    }

    /*Based on the Timer type, start the corresponding timer for the node */
    u4TimerType = pLaAppTmrNode->LaAppTimer.u4Data;

    switch (u4TimerType)
    {

        case LA_CACHE_TMR_TYPE:

            u4Duration = LA_CACHE_TIMEOUT * SYS_TIME_TICKS_IN_A_SEC;
            LA_TRC_ARG1 (DATA_PATH_TRC, "Starting Cache Timer for Port : %d\n",
                         ((tLaCacheEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);

            break;

        case LA_MARKER_TMR_TYPE:

            u4Duration = LA_MARKER_TIMEOUT * SYS_TIME_TICKS_IN_A_SEC;

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting Marker Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);
            break;

        case LA_TICK_TMR_TYPE:

            u4Duration = SYS_TIME_TICKS_IN_A_SEC;

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting Tick Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);

            break;

        case LA_TX_SCHEDULER_TMR_TYPE:

            u4Duration = LA_TX_SCHEDULING_TICKS;

            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Starting Tx Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);

            break;

        case LA_MARKER_RATE_TMR_TYPE:

            u4Duration = LA_MARKER_RATE_TIMEOUT * SYS_TIME_TICKS_IN_A_SEC;

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting Marker Rate Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);

            break;

        case LA_PERIODIC_TMR_TYPE:
            pPortEntry = pLaAppTmrNode->pEntry;
            u4Duration = pPortEntry->u4PeriodicTime * SYS_TIME_TICKS_IN_A_SEC;
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting Periodic Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);
            break;

        case LA_WAIT_WHILE_TMR_TYPE:
            pPortEntry = pLaAppTmrNode->pEntry;
            /* When ISSU is in progress increased timer valuse for ISSU */
            if (IssuGetMaintModeOperation () == OSIX_TRUE)
            {
                u4Duration = (pPortEntry->u4WaitWhileTime *
                              SYS_TIME_TICKS_IN_A_SEC) + ISSU_TIMER_VALUE;
            }
            else
            {
                u4Duration =
                    pPortEntry->u4WaitWhileTime * SYS_TIME_TICKS_IN_A_SEC;
            }
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting WaitWhile Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);
            break;

        case LA_HOLD_WHILE_TMR_TYPE:
            pPortEntry = pLaAppTmrNode->pEntry;
            u4Duration = pPortEntry->u4HoldWhileTime * SYS_TIME_TICKS_IN_A_SEC;
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting HoldWhile Timer for Port : %d\n",
                         ((tLaLacPortEntry *) (pLaAppTmrNode->pEntry))->
                         u2PortIndex);
            break;

        case LA_CURRENT_WHILE_TMR_TYPE:
            pPortEntry = pLaAppTmrNode->pEntry;

            /* When ISSU is in progress increased timer valuse for ISSU */
            if (IssuGetMaintModeOperation () == OSIX_TRUE)
            {
                u4Duration = (pPortEntry->u4CurrentWhileTime *
                              SYS_TIME_TICKS_IN_A_SEC) + ISSU_TIMER_VALUE;
            }
            else
            {
                u4Duration =
                    pPortEntry->u4CurrentWhileTime * SYS_TIME_TICKS_IN_A_SEC;
            }
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting CurrentWhile Timer for Port : %d\n",
                         pPortEntry->u2PortIndex);
            break;

#ifdef DLAG_WANTED
        case LA_DLAG_PERIODIC_TMR_TYPE:
            pAggEntry = pLaAppTmrNode->pEntry;
            u4Duration = pAggEntry->u4DLAGPeriodicSyncTime;
            LA_CONVERT_MSECS_TO_SYS_TICKS (u4Duration);
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting D-LAG Periodic sync timer for Port-channel "
                         ": %u\n", pAggEntry->u2AggIndex);
            break;

        case LA_DLAG_MS_SELECTION_WAIT_TMR_TYPE:
            pAggEntry = pLaAppTmrNode->pEntry;
            u4Duration = pAggEntry->u4DLAGMSSelectionWaitTime;
            LA_CONVERT_MSECS_TO_SYS_TICKS (u4Duration);
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting Master slave selection wait Timer for "
                         "Port-channel : %u\n", pAggEntry->u2AggIndex);
            break;

        case LA_AA_DLAG_PERIODIC_TMR_TYPE:

            /* Get the Periodic sync time from Global structure */
            u4Duration =
                LA_DLAG_GLOBAL_INFO.u4GlobalDLAGPeriodicSyncTime *
                SYS_TIME_TICKS_IN_A_SEC;

            LA_TRC (CONTROL_PLANE_TRC,
                    "Starting Global Active-Active DLAG Periodic Sync Timer\n");
            break;
#endif
#ifdef ICCH_WANTED
        case LA_AA_MCLAG_PERIODIC_TMR_TYPE:

            /* Get the Periodic sync time from Global structure */
            u4Duration =
                LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime *
                SYS_TIME_TICKS_IN_A_SEC;

            LA_TRC (CONTROL_PLANE_TRC,
                    "Starting Global MCLAG Periodic Sync Timer\n");
            break;
#endif
        case LA_RECOVERY_TMR_TYPE:

            pPortEntry = pLaAppTmrNode->pEntry;
            /* Get the Recovery time from Global structure */
            /* And When ISSU is in progress increased timer valuse for ISSU */
            if (IssuGetMaintModeOperation () == OSIX_TRUE)
            {
                u4Duration = (gu4RecoveryTime *
                              SYS_TIME_TICKS_IN_A_SEC) + ISSU_TIMER_VALUE;
            }
            else
            {
                /* Get the Recovery time from Global structure */
                u4Duration = (gu4RecoveryTime * SYS_TIME_TICKS_IN_A_SEC);
            }
            if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_THRSHLD_EXCEED)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) LA_SYSLOG_ID,
                              "Reason for Error Recovery timer Start is Threadhold Exceed for Port= %u",
                              pPortEntry->u2PortIndex));
            }
            else if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) LA_SYSLOG_ID,
                              "Reason for Error Recovery timer Start is Port Defaulted for Port= %u",
                              pPortEntry->u2PortIndex));
            }

            else if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) LA_SYSLOG_ID,
                              "Reason for Error Recovery timer Start is H/w Failure for Port= %u",
                              pPortEntry->u2PortIndex));
            }

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Starting Recovery Timer for Port : %d\r\n",
                         pPortEntry->u2PortIndex);
            break;

        default:
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: Invalid Timer Type\n");
            return LA_FAILURE;

    }
    if (LA_START_TMR (LA_TIMER_LIST_ID,
                      &pLaAppTmrNode->LaAppTimer, u4Duration) != LA_TMR_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: Starting Timer FAILED\n");
        return LA_FAILURE;
    }
    LA_TRC (CONTROL_PLANE_TRC, "TMR: Started Timer\r\n");
    pLaAppTmrNode->LaTmrFlag = LA_TMR_RUNNING;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaStopTimer                                          */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be stopped by any module/function. The function      */
/*                      removes the timer node from the Timer List.          */
/*                                                                           */
/* Input(s)           : *pTmr - Pointer to timer node of any type            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaTimerListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaStopTimer (tLaAppTmrNode * pLaAppTmrNode)
{
    if (pLaAppTmrNode == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: Timer Node Null Pointer\n");
        return LA_FAILURE;
    }

    pLaAppTmrNode->LaTmrFlag = LA_TMR_STOPPED;

    if (LA_STOP_TMR (LA_TIMER_LIST_ID, &pLaAppTmrNode->LaAppTimer)
        == LA_TMR_FAILURE)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: Stopping Timer FAILED\n");
        return LA_FAILURE;
    }

    LA_TRC (CONTROL_PLANE_TRC, "TMR: Timer Stopped SUCCESSFULLY\r\n");
    return LA_SUCCESS;

}

/* End of file */
